

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.wsdl.Binding;
import javax.wsdl.BindingOperation;
import javax.wsdl.Definition;
import javax.wsdl.Message;
import javax.wsdl.PortType;
import javax.wsdl.Service;
import javax.xml.namespace.QName;

import org.apache.axis.Constants;
import org.apache.axis.encoding.TypeMapping;
import org.apache.axis.encoding.TypeMappingRegistry;
import org.apache.axis.encoding.TypeMappingRegistryImpl;
import org.apache.axis.wsdl.gen.Generator;
import org.apache.axis.wsdl.gen.NoopFactory;
import org.apache.axis.wsdl.gen.Parser;
import org.apache.axis.wsdl.symbolTable.BindingEntry;
import org.apache.axis.wsdl.symbolTable.ElementDecl;
import org.apache.axis.wsdl.symbolTable.Parameter;
import org.apache.axis.wsdl.symbolTable.Parameters;
import org.apache.axis.wsdl.symbolTable.SchemaUtils;
import org.apache.axis.wsdl.symbolTable.SymbolTable;
import org.apache.axis.wsdl.symbolTable.Type;
import org.apache.axis.wsdl.symbolTable.TypeEntry;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.text.StringUtils;
import simple.util.ConversionSet;
import simple.util.MapBackedSet;

import com.usatech.ec.ECDataHandler;
import com.usatech.ec2.EC2DataHandler;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.PaymentActionType;
import com.usatech.layers.common.constants.ReceiptResult;
import com.usatech.layers.common.constants.TranDeviceResultType;
import com.usatech.layers.common.messagedata.AuthorizeMessageData;
import com.usatech.layers.common.messagedata.Byteable;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageDataUtils;
import com.usatech.layers.common.messagedata.MessageDirection;
import com.usatech.layers.common.messagedata.PlainCardReader;
import com.usatech.layers.common.messagedata.ReplenishMessageData;
import com.usatech.layers.common.messagedata.Sale;
import com.usatech.layers.common.messagedata.WS2AuthorizeByMessageData;
import com.usatech.layers.common.messagedata.WS2AuthorizeMessageData;
import com.usatech.layers.common.messagedata.WS2AuthorizeResponseMessageData;
import com.usatech.layers.common.messagedata.WS2CardInfoResponseMessageData;
import com.usatech.layers.common.messagedata.WS2CashMessageData;
import com.usatech.layers.common.messagedata.WS2ChargeByMessageData;
import com.usatech.layers.common.messagedata.WS2ChargeMessageData;
import com.usatech.layers.common.messagedata.WS2GetCardMessageData;
import com.usatech.layers.common.messagedata.WS2ReplenishMessageData;
import com.usatech.layers.common.messagedata.WS2RequestMessageData;
import com.usatech.layers.common.messagedata.WS2ResponseMessageData;
import com.usatech.layers.common.messagedata.WS2SaleMessageData;
import com.usatech.layers.common.messagedata.WS2TokenizeMessageData;
import com.usatech.layers.common.messagedata.WS2TokenizeMessageData.ExpirationDateContainer;
import com.usatech.layers.common.messagedata.WS2TokenizeResponseMessageData;
import com.usatech.layers.common.messagedata.WS2UntokenizeMessageData;
import com.usatech.layers.common.messagedata.WSAuthorizeMessageData;
import com.usatech.layers.common.messagedata.WSAuthorizeResponseMessageData;
import com.usatech.layers.common.messagedata.WSCashMessageData;
import com.usatech.layers.common.messagedata.WSChargeMessageData;
import com.usatech.layers.common.messagedata.WSEncryptingCardReader;
import com.usatech.layers.common.messagedata.WSMessageData;
import com.usatech.layers.common.messagedata.WSRequestMessageData;
import com.usatech.layers.common.messagedata.WSResponseMessageData;
import com.usatech.layers.common.messagedata.WSSale;
import com.usatech.layers.common.messagedata.WSSaleMessageData;

public class WsdlToMessageData extends NoopFactory {
	protected class StubGenerator implements Generator {
		protected final Object o;

		public StubGenerator(Object o) {
			this.o = o;
		}

		@Override
		public void generate() throws IOException {
			System.out.println("Processing " + o);
		}
	}

	@Override
	public Generator getGenerator(Definition definition, SymbolTable symbolTable) {
		return new StubGenerator(definition);
	}

	@Override
	public Generator getGenerator(TypeEntry type, SymbolTable symbolTable) {
		return new StubGenerator(type);
	}

	@Override
	public Generator getGenerator(Service service, SymbolTable symbolTable) {
		return new StubGenerator(service);
	}

	@Override
	public Generator getGenerator(Binding binding, SymbolTable symbolTable) {
		return new StubGenerator(binding);
	}

	@Override
	public Generator getGenerator(PortType portType, SymbolTable symbolTable) {
		return new StubGenerator(portType);
	}

	@Override
	public Generator getGenerator(Message message, SymbolTable symbolTable) {
		return new StubGenerator(message);
	}

	@Override
	public void generatorPass(Definition def, SymbolTable symbolTable) {
		// TODO Auto-generated method stub

	}

	protected static final Pattern replacePrefixPattern = Pattern.compile("^EC\\d*");
	public static void main(String[] args) throws Exception {
		// String suffix = "2"; // "";
		// File wsdlFile = new File("../NetLayer/resources/wsdl/ePortConnect" + suffix + ".wsdl");
		File wsdlFile = new File(args[0]);

		Parser parser = new Parser();
		// parser.setFactory(new WsdlToMessageData());
		parser.run("file:" + wsdlFile.getAbsolutePath());
		File baseDirectory = new File("./src/");
		if(!baseDirectory.exists()) {
			throw new Exception("File " + baseDirectory.getAbsolutePath() + " does not exist");
		}
		Definition def = parser.getCurrentDefinition();
		String packageName;
		if(def.getTargetNamespace() != null && def.getTargetNamespace().startsWith("urn:"))
			packageName = reversePieces(def.getTargetNamespace().substring(4), '.');
		else
			packageName = "com.usatech.ec2";
		String prefix = StringUtils.substringAfterLast(packageName, ".").toUpperCase();
		dataHandlerClass = Class.forName(packageName + '.' + prefix + "DataHandler");
		String suffix = prefix.substring(2);
		Map<String, String> genList = new HashMap<String, String>();
		for(Object b : def.getBindings().values()) {
			Binding bnd = (Binding) b;
			BindingEntry bindingEntry = parser.getSymbolTable().getBindingEntry(bnd.getQName());
			for(Object o : bnd.getBindingOperations()) {
				BindingOperation bop = (BindingOperation) o;
				String requestName = bop.getOperation().getInput().getMessage().getQName().getLocalPart();
				if(genList.containsKey(requestName))
					continue;
				Parameters parameters = parser.getSymbolTable().getOperationParameters(bop.getOperation(), "", bindingEntry);
				String responseName = parameters.returnParam.getType().getQName().getLocalPart();
				if(StringUtils.isBlank(responseName))
					responseName = bop.getOperation().getOutput().getMessage().getQName().getLocalPart();
				if(responseName.startsWith(prefix))
					responseName = responseName.substring(prefix.length());
				String responseClassName = genList.get(responseName);
				if(responseClassName == null) {
					responseClassName = generateMessageData(suffix, baseDirectory, parser.getSymbolTable(), responseName, MessageDirection.SERVER_TO_CLIENT, null, parameters.returnParam);
					genList.put(responseName, responseClassName);
				}

				Parameter[] p = new Parameter[parameters.list.size()];
				for(int i = 0; i < p.length; i++)
					p[i] = (Parameter) parameters.list.get(i);
				generateMessageData(suffix, baseDirectory, parser.getSymbolTable(), requestName, MessageDirection.CLIENT_TO_SERVER, responseClassName, p);
				genList.put(requestName, null);
			}
		}
		// System.out.println(def);

	}

	protected static TypeMappingRegistry tmr = new TypeMappingRegistryImpl();
	protected static TypeMapping tm = (TypeMapping) tmr.getTypeMapping(Constants.URI_SOAP11_ENC);
	protected static final AtomicInteger[] lastMessageTypeValues = new AtomicInteger[256];
	protected final static Set<String> SENSITIVE_PROPERTIES = new ConversionSet<String, String>(new MapBackedSet<String>(new ConcurrentHashMap<String, Object>(10, 0.75f, 1))) {
		@Override
		protected boolean isReversible() {
			return true;
		}

		@Override
		protected String convertTo(String object1) {
			return object1 == null ? null : object1.toUpperCase();
		}

		@Override
		protected String convertFrom(String object0) {
			return object0 == null ? null : object0.toUpperCase();
		}
	};
	protected final static Set<String> CARD_PROPERTIES = new ConversionSet<String, String>(new MapBackedSet<String>(new ConcurrentHashMap<String, Object>(10, 0.75f, 1))) {
		@Override
		protected boolean isReversible() {
			return true;
		}

		@Override
		protected String convertTo(String object1) {
			return object1 == null ? null : object1.toUpperCase();
		}

		@Override
		protected String convertFrom(String object0) {
			return object0 == null ? null : object0.toUpperCase();
		}
	};
	static {
		SENSITIVE_PROPERTIES.add("PASSWORD");
		SENSITIVE_PROPERTIES.add("NEWPASSWORD");
		SENSITIVE_PROPERTIES.add("CREDENTIAL");
		SENSITIVE_PROPERTIES.add("NEWCREDENTIAL");
		SENSITIVE_PROPERTIES.add("PASSCODE");
		SENSITIVE_PROPERTIES.add("NEWPASSCODE");

		SENSITIVE_PROPERTIES.add("EXPIRATIONDATE");
		SENSITIVE_PROPERTIES.add("SECURITYCODE");
		SENSITIVE_PROPERTIES.add("BILLINGPOSTALCODE");
		SENSITIVE_PROPERTIES.add("BILLINGADDRESS");
		SENSITIVE_PROPERTIES.add("TOKENHEX");

		CARD_PROPERTIES.add("CARDDATA");
		CARD_PROPERTIES.add("CARDNUMBER");
		CARD_PROPERTIES.add("ENCRYPTEDCARDDATAHEX");
	}

	protected static String reversePieces(String orig, char delimiter) {
		StringBuilder sb = new StringBuilder();
		int p = orig.length();
		while(true) {
			int next = orig.lastIndexOf(delimiter, p - 1);
			if(next < 0) {
				sb.append(orig, 0, p);
				return sb.toString();
			}
			sb.append(orig, next + 1, p).append(delimiter);
			p = next;
			next = orig.lastIndexOf(delimiter, p - 1);
		}
	}
	protected static boolean isSale(Parameter[] parameters) {
		for(Parameter p : parameters)
			if("tranId".equalsIgnoreCase(p.getName()))
				return true;
		return false;
	}

	protected static String generateMessageData(String suffix, File baseDirectory, SymbolTable symbolTable, String operationName, MessageDirection direction, String responseClassName, Parameter... parameters) throws IOException, IntrospectionException, IllegalAccessException, InvocationTargetException, ParseException {
		String enumNameSuffix = fromJavaName(operationName);
		String messageTypeName = "WS" + suffix + '_' + enumNameSuffix;
		MessageType messageType = ConvertUtils.convertSafely(MessageType.class, messageTypeName, null);
		if(messageType == null) {
			System.out.println("Please add MessageType:");
			System.out.print('\t');
			System.out.print(messageTypeName);
			System.out.print("(new byte[]{");
			byte majorMessageTypeValue;
			switch(direction) {
				case CLIENT_TO_SERVER:
					majorMessageTypeValue = 2;
					break;
				case SERVER_TO_CLIENT:
					majorMessageTypeValue = 3;
					break;
				case INTRA_SERVER:
				default:
					majorMessageTypeValue = 0;
					break;
			}
			System.out.print(majorMessageTypeValue);
			System.out.print(", ");
			if(lastMessageTypeValues[majorMessageTypeValue] == null) {
				for(MessageType mt : MessageType.values()) {
					byte[] b = mt.getValue();
					if(b != null && b.length == 2) {
						if(lastMessageTypeValues[b[0] & 0xFF] == null)
							lastMessageTypeValues[b[0] & 0xFF] = new AtomicInteger(b[1] & 0xFF);
						else if(lastMessageTypeValues[b[0] & 0xFF].get() < (b[1] & 0xFF))
							lastMessageTypeValues[b[0] & 0xFF].set(b[1] & 0xFF);
					}
				}
				if(lastMessageTypeValues[majorMessageTypeValue] == null)
					lastMessageTypeValues[majorMessageTypeValue] = new AtomicInteger();
			}
			byte minorMessageTypeValue = (byte) lastMessageTypeValues[majorMessageTypeValue].getAndIncrement();
			System.out.print(minorMessageTypeValue);
			System.out.print("}, \"Web Service - ");
			System.out.print(StringUtils.capitalizeAllWords(enumNameSuffix.replace('_', ' ').toLowerCase()));
			System.out.print("\", false, false),");
			System.out.println();
			StringBuilder sb = new StringBuilder();
			sb.append("MessageData_");
			StringUtils.appendHex(sb, majorMessageTypeValue);
			StringUtils.appendHex(sb, minorMessageTypeValue);
			return sb.toString();
		}
		System.out.println("Create Message Data: " + operationName + ": ");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("messageType", messageType);
		StringBuilder ancestry = new StringBuilder();
		StringBuilder defineFields = new StringBuilder();
		StringBuilder writeFields = new StringBuilder();
		StringBuilder readFields = new StringBuilder();
		StringBuilder toStringFields = new StringBuilder();
		StringBuilder publishFields = new StringBuilder();
		StringBuilder initFields = new StringBuilder();
		// StringBuilder validateFields = new StringBuilder();
		Set<Class<?>> imports = new TreeSet<Class<?>>(new Comparator<Class<?>>() {
			public int compare(Class<?> o1, Class<?> o2) {
				if(o1 == o2)
					return 0;
				if(o1 == null)
					return -1;
				if(o2 == null)
					return 1;
				return o1.getName().compareToIgnoreCase(o2.getName());
			}
		});
		toStringFields.append("\n\t\t\t.append(\", protocol=\").append(getProtocol())");
		Class<? extends MessageData> parent;
		Set<Class<?>> extraInterfaces = new LinkedHashSet<Class<?>>();
		Map<Class<?>, String> delegates = new LinkedHashMap<Class<?>, String>();
		if(messageType.isAuthRequest()) {
			if(messageType.toString().matches("(^|.*_)REPLENISH($|_.*)")) {
				imports.add(PaymentActionType.class);
				publishFields.append("\tpublic PaymentActionType getPaymentActionType() {\n\t\treturn PaymentActionType.REPLENISHMENT;\n\t}\n\n");
				if(!messageType.toString().contains("_CASH_")) {
					imports.add(ConvertUtils.class);
					publishFields.append("\tpublic boolean isAuthOnly() {\n\t\treturn ConvertUtils.getBooleanSafely(getAttribute(\"authOnly\"), false);\n\t}\n\n\tpublic void setAuthOnly(boolean authOnly) {\n\t\tsetAttribute(\"authOnly\", authOnly ? \"true\" : null);\n\t}\n\n");
				} else {
					publishFields.append("\tpublic boolean isAuthOnly() {\n\t\treturn false;\n\t}\n\n");
				}
				parent = suffix.equals("2") ? WS2ReplenishMessageData.class : WSAuthorizeMessageData.class;
			} else if(messageType.toString().matches("(^|.*_)CHARGE($|_.*)"))
				parent = suffix.equals("2") ? WS2ChargeMessageData.class : WSChargeMessageData.class;
			else if(messageType.toString().matches("(^|.*_)TOKENIZE($|_.*)")) {
				parent = WS2TokenizeMessageData.class;
			} else
				parent = suffix.equals("2") ? WS2AuthorizeMessageData.class : WSAuthorizeMessageData.class;
			switch(messageType) {
				case WS_AUTH_V_3_REQUEST:
				case WS_CHARGE_V_3_REQUEST:
					imports.add(CardReaderType.class);
					extraInterfaces.add(PlainCardReader.class);
					publishFields.append("\tpublic String getAccountData() {\n\t\treturn getCardData();\n\t}\n\n\tpublic void setAccountData(String accountData) {\n\t\tsetCardData(accountData);\n\t}\n\n");
					publishFields.append("\tpublic CardReaderType getCardReaderType() {\n\t\treturn CardReaderType.GENERIC;\n\t}\n\n");
					publishFields.append("\tpublic CardReader getCardReader() {\n\t\treturn this;\n\t}\n\n");
					break;
				case WS_AUTH_V_3_1_REQUEST:
				case WS_CHARGE_V_3_1_REQUEST:
					imports.add(CardReaderType.class);
					imports.add(WSEncryptingCardReader.class);
					defineFields.append("\tprotected final WSEncryptingCardReader cardReader = new WSEncryptingCardReader();\n");
					publishFields.append("\tpublic CardReaderType getCardReaderType() {\n\t\treturn cardReader.getCardReaderType();\n\t}\n\n");
					publishFields.append("\tpublic CardReader getCardReader() {\n\t\treturn cardReader;\n\t}\n\n");
					delegates.put(WSEncryptingCardReader.class, "cardReader");
					initFields.append("\n\t\tcardReader.registerValidators(this);");
					/*
					imports.add(InvalidByteValueException.class);
					defineFields.append("\tprotected CardReaderType cardReaderType;\n");
					publishFields.append("\tpublic CardReaderType getCardReaderType() throws InvalidByteValueException {\n\t\tif(cardReaderType == null)\n\t\t\tINVALID_CARD_READER_TYPE.attempt(this);\n\t\treturn cardReaderType;\n\t}\n\n");
					imports.add(IllegalArgumentException.class);
					defineFields.append("\tprotected byte[] keySerialNum;\n");
					defineFields.append("\tprotected byte[] encryptedData;\n");
					publishFields.append("\tpublic byte[] getKeySerialNum() throws IllegalArgumentException {\n\t\tif(keySerialNum == null)\n\t\t\tINVALID_KSN_HEX.attempt(this);\n\t\treturn keySerialNum;\n\t}\n\n");
					publishFields.append("\tpublic byte[] getEncryptedData() {\n\t\tif(encryptedData == null)\n\t\t\tINVALID_ENCRYPTED_CARD_DATA_HEX.attempt(this);\n\t\treturn encryptedData;\n\t}\n\n");
					publishFields.append("\tpublic int getDecryptedLength() {\n\t\treturn getDecryptedCardDataLen();\n\t}\n\n");
					*/

					break;
				case WS2_TOKENIZE_PLAIN_REQUEST:
					imports.add(CardReaderType.class);
					extraInterfaces.add(PlainCardReader.class);
					extraInterfaces.add(ExpirationDateContainer.class);
					publishFields.append("\tpublic String getAccountData() {\n\t\treturn getCardNumber();\n\t}\n\n\tpublic void setAccountData(String accountData) {\n\t\tsetCardNumber(accountData);\n\t}\n\n");
					publishFields.append("\tpublic CardReaderType getCardReaderType() {\n\t\treturn CardReaderType.GENERIC;\n\t}\n\n");
					publishFields.append("\tpublic CardReader getCardReader() {\n\t\treturn this;\n\t}\n\n");
					break;
				default:
					if(messageType.toString().contains("_PLAIN_")) {
						imports.add(CardReaderType.class);
						extraInterfaces.add(PlainCardReader.class);
						publishFields.append("\tpublic String getAccountData() {\n\t\treturn getCardData();\n\t}\n\n\tpublic void setAccountData(String accountData) {\n\t\tsetCardData(accountData);\n\t}\n\n");
						publishFields.append("\tpublic CardReaderType getCardReaderType() {\n\t\treturn CardReaderType.GENERIC;\n\t}\n\n");
						publishFields.append("\tpublic CardReader getCardReader() {\n\t\treturn this;\n\t}\n\n");
					} else if(messageType.toString().contains("_ENCRYPTED_")) {
						imports.add(CardReaderType.class);
						imports.add(WSEncryptingCardReader.class);
						defineFields.append("\tprotected final WSEncryptingCardReader cardReader = new WSEncryptingCardReader();\n");
						publishFields.append("\tpublic CardReaderType getCardReaderType() {\n\t\treturn cardReader.getCardReaderType();\n\t}\n\n");
						publishFields.append("\tpublic CardReader getCardReader() {\n\t\treturn cardReader;\n\t}\n\n");
						delegates.put(WSEncryptingCardReader.class, "cardReader");
						initFields.append("\n\t\tcardReader.registerValidators(this);");
					} else if(messageType.toString().contains("_CONSUMER_CARD_ID_")) {
						imports.add(EntryType.class);
						if(ReplenishMessageData.class.isAssignableFrom(parent)) {
							parent = WS2AuthorizeByMessageData.class;
							extraInterfaces.add(ReplenishMessageData.class);
							imports.add(TranDeviceResultType.class);
							imports.add(ReceiptResult.class);
							publishFields.append("\tpublic long getBatchId() {\n\t\treturn 0;\n\t}\n\n");
							publishFields.append("\tpublic TranDeviceResultType getTransactionResult() {\n\t\treturn TranDeviceResultType.SUCCESS;\n\t}\n\n");
							publishFields.append("\tpublic ReceiptResult getReceiptResult() {\n\t\treturn ReceiptResult.UNAVAILABLE;\n\t}\n\n");
						} else if(WSSale.class.isAssignableFrom(parent)) {
							parent = WS2ChargeByMessageData.class;
						} else
							parent = WS2AuthorizeByMessageData.class;
						publishFields.append("\tpublic EntryType getEntryType() {\n\t\treturn EntryType.CARD_ID_CREDENTIALS;\n\t}\n\n");
					} else if(messageType.toString().contains("_CARD_ID_")) {
						imports.add(EntryType.class);
						if(ReplenishMessageData.class.isAssignableFrom(parent)) {
							parent = WS2AuthorizeByMessageData.class;
							extraInterfaces.add(ReplenishMessageData.class);
							imports.add(TranDeviceResultType.class);
							imports.add(ReceiptResult.class);
							publishFields.append("\tpublic long getBatchId() {\n\t\treturn 0;\n\t}\n\n");
							publishFields.append("\tpublic TranDeviceResultType getTransactionResult() {\n\t\treturn TranDeviceResultType.SUCCESS;\n\t}\n\n");
							publishFields.append("\tpublic ReceiptResult getReceiptResult() {\n\t\treturn ReceiptResult.UNAVAILABLE;\n\t}\n\n");
						} else if(WSSale.class.isAssignableFrom(parent)) {
							parent = WS2ChargeByMessageData.class;
						} else
							parent = WS2AuthorizeByMessageData.class;
						publishFields.append("\tpublic EntryType getEntryType() {\n\t\treturn EntryType.CARD_ID;\n\t}\n\n");
					} else if(messageType.toString().contains("_CASH_")) {
						imports.add(EntryType.class);
						if(ReplenishMessageData.class.isAssignableFrom(parent)) {
							extraInterfaces.add(ReplenishMessageData.class);
							imports.add(TranDeviceResultType.class);
							imports.add(ReceiptResult.class);
							publishFields.append("\tpublic long getBatchId() {\n\t\treturn 0;\n\t}\n\n");
							publishFields.append("\tpublic TranDeviceResultType getTransactionResult() {\n\t\treturn TranDeviceResultType.SUCCESS;\n\t}\n\n");
							publishFields.append("\tpublic ReceiptResult getReceiptResult() {\n\t\treturn ReceiptResult.UNAVAILABLE;\n\t}\n\n");
						}
						parent = WS2AuthorizeByMessageData.class;
						publishFields.append("\tpublic EntryType getEntryType() {\n\t\treturn EntryType.CASH;\n\t}\n\n");
					}
					break;
			}
		} else if(messageType == MessageType.WS2_TOKEN_RESPONSE) {
			parent = WS2TokenizeResponseMessageData.class;
		} else if(messageType.isAuthResponse()) {
			parent = suffix.equals("2") ? WS2AuthorizeResponseMessageData.class : WSAuthorizeResponseMessageData.class;
		} else if(messageType == MessageType.WS2_CARD_INFO) {
			parent = WS2CardInfoResponseMessageData.class;
		} else if(messageType == MessageType.WS2_UNTOKENIZE_REQUEST) {
			parent = WS2UntokenizeMessageData.class;
		} else if(direction == MessageDirection.SERVER_TO_CLIENT) {
			parent = suffix.equals("2") ? WS2ResponseMessageData.class : WSResponseMessageData.class;
		} else if(direction == MessageDirection.CLIENT_TO_SERVER) {
			if(isSale(parameters)) {
				if(messageType.toString().matches("(^|.*_)CASH($|_.*)"))
					parent = suffix.equals("2") ? WS2CashMessageData.class : WSCashMessageData.class;
				else
					parent = suffix.equals("2") ? WS2SaleMessageData.class : WSSaleMessageData.class;
			} else if(messageType.toString().contains("_GET_CARD_")) {
				parent = WS2GetCardMessageData.class;
				if(messageType.toString().contains("_ENCRYPTED_")) {
					imports.add(CardReaderType.class);
					imports.add(WSEncryptingCardReader.class);
					defineFields.append("\tprotected final WSEncryptingCardReader cardReader = new WSEncryptingCardReader();\n");
					publishFields.append("\tpublic CardReaderType getCardReaderType() {\n\t\treturn cardReader.getCardReaderType();\n\t}\n\n");
					publishFields.append("\tpublic CardReader getCardReader() {\n\t\treturn cardReader;\n\t}\n\n");
					delegates.put(WSEncryptingCardReader.class, "cardReader");
					initFields.append("\n\t\tcardReader.registerValidators(this);");
				} else {
					imports.add(CardReaderType.class);
					extraInterfaces.add(PlainCardReader.class);
					publishFields.append("\tpublic String getAccountData() {\n\t\treturn getCardData();\n\t}\n\n\tpublic void setAccountData(String accountData) {\n\t\tsetCardData(accountData);\n\t}\n\n");
					publishFields.append("\tpublic CardReaderType getCardReaderType() {\n\t\treturn CardReaderType.GENERIC;\n\t}\n\n");
					publishFields.append("\tpublic CardReader getCardReader() {\n\t\treturn this;\n\t}\n\n");
				}
			} else {
				parent = suffix.equals("2") ? WS2RequestMessageData.class : WSRequestMessageData.class;
			}
		} else {
			parent = WSMessageData.class;
		}
		for(Parameter p : parameters) {
			System.out.print("\t" + p.getName() + " ");
			handleParameter(symbolTable, p.getName(), p.getType(), false, 1, defineFields, writeFields, readFields, toStringFields, publishFields, imports, messageType, delegates, parent, direction == MessageDirection.CLIENT_TO_SERVER ? DataHandler.class : dataHandlerClass, direction);
			System.out.println();
		}
		ancestry.append(" extends ");
		ancestry.append(parent.getSimpleName());
		imports.add(parent);
		boolean first = true;
		for(Class<?> ei : extraInterfaces) {
			if(first) {
				ancestry.append(" implements ");
				first = false;
			} else
				ancestry.append(", ");
			ancestry.append(ei.getSimpleName());
			imports.add(ei);
		}
		if(!StringUtils.isBlank(responseClassName) && WSRequestMessageData.class.isAssignableFrom(parent)) {
			publishFields.append("\tpublic ").append(responseClassName).append(" createResponse() {\n\t\t").append(responseClassName).append(" response = new ").append(responseClassName)
				.append("();\n\t\tresponse.setCharset(charset);\n\t\tresponse.setDirection(MessageDirection.SERVER_TO_CLIENT);\n\t\tresponse.setProtocol(protocol);\n\t\tresponse.setSerialNumber(serialNumber);\n\t\treturn response;\n\t}\n\n");
		}
		params.put("ancestry", ancestry.toString());
		params.put("defineFields", defineFields.toString());
		params.put("writeFields", writeFields.toString());
		params.put("readFields", readFields.toString());
		params.put("toStringFields", toStringFields.toString());
		params.put("publishFields", publishFields.toString());
		params.put("initFields", initFields.toString());
		imports.addAll(extraInterfaces);
		StringBuilder importString = new StringBuilder();
		for(Class<?> clazz : imports) {
			if(shouldImport(clazz)) {
				String cn = clazz.getCanonicalName();
				if(cn != null)
					importString.append("import ").append(cn).append(";\n");
			}
		}
		params.put("imports", importString.toString());
		PrintWriter writer = new PrintWriter(new FileWriter(new File(baseDirectory, "com/usatech/layers/common/messagedata/MessageData_" + messageType.getHex() + ".java")));
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(WsdlToMessageData.class.getResourceAsStream("MessageDataTemplate.txt")));
			Pattern replacePattern = Pattern.compile("\\$\\{([A-Za-z0-9_.\\(\\)\\[\\]]+)\\}");
			String line;
			while((line = reader.readLine()) != null) {
				Matcher matcher = replacePattern.matcher(line);
				int pos = 0;
				while(matcher.find()) {
					writer.print(line.substring(pos, matcher.start()));
					writer.print(ReflectionUtils.getProperty(params, matcher.group(1)));
					pos = matcher.end();
				}
				writer.println(line.substring(pos));
			}
			writer.flush();
		} finally {
			writer.close();
		}
		return new StringBuilder().append("MessageData_").append(messageType.getHex()).toString();
	}

	protected static boolean shouldImport(Class<?> clazz) {
		if(clazz.getPackage() != null && !clazz.getPackage().getName().equals("java.lang") && (!clazz.getPackage().getName().equals(MessageData.class.getPackage().getName()) || clazz.getEnclosingClass() != null))
			return true;
		return false;
	}
	protected static String fromJavaName(String name) {
		StringBuilder sb = new StringBuilder();
		char last = '_';
		for(int i = 0; i < name.length(); i++) {
			char ch = name.charAt(i);
			if(Character.isDigit(ch)) {
				if(last != '_' && !Character.isDigit(last))
					sb.append('_');
				sb.append(ch);
				last = ch;
			} else if(Character.isLetter(ch)) {
				if(last != '_' && (Character.isDigit(last) || (Character.isUpperCase(ch) && Character.isLowerCase(last))))
					sb.append('_');
				sb.append(Character.toUpperCase(ch));
				last = ch;
			} else {
				sb.append('_');
				last = '_';
			}
		}
		return sb.toString();
	}

	protected static Map<Class<?>, String> bufferOpSuffixes = new HashMap<Class<?>, String>();
	protected static Map<Class<?>, String> utilsOpSuffixes = new HashMap<Class<?>, String>();
	static {
		bufferOpSuffixes.put(byte.class, "");
		bufferOpSuffixes.put(short.class, "Short");
		bufferOpSuffixes.put(int.class, "Int");
		bufferOpSuffixes.put(long.class, "Long");
		bufferOpSuffixes.put(float.class, "Float");
		bufferOpSuffixes.put(double.class, "Double");
		bufferOpSuffixes.put(char.class, "Char");

		bufferOpSuffixes.put(Byte.class, "");
		bufferOpSuffixes.put(Short.class, "Short");
		bufferOpSuffixes.put(Integer.class, "Int");
		bufferOpSuffixes.put(Long.class, "Long");
		bufferOpSuffixes.put(Float.class, "Float");
		bufferOpSuffixes.put(Double.class, "Double");
		bufferOpSuffixes.put(Character.class, "Char");
		
		utilsOpSuffixes.put(byte[].class, "LongBytes");
		utilsOpSuffixes.put(BigDecimal.class, "StringAmount");
		utilsOpSuffixes.put(BigInteger.class, "StringInt");
		utilsOpSuffixes.put(Date.class, "Date");
		utilsOpSuffixes.put(Calendar.class, "Timestamp");
		utilsOpSuffixes.put(Currency.class, "Currency");
		utilsOpSuffixes.put(Boolean.class, "Boolean");
	}

	protected static Class<?> dataHandlerClass;

	protected static StringBuilder appendValue(StringBuilder writeFields, Class<?> cls, boolean sensitive, boolean card, String getMethod, Set<Class<?>> imports) {
		if(sensitive) {
			imports.add(MessageDataUtils.class);
			writeFields.append("maskSensitiveData ? MessageDataUtils.");
			if(cls.equals(byte[].class)) {
				writeFields.append("summarizeBytes");
			} else if(cls.equals(String.class)) {
				writeFields.append("maskString");
			} else {
				writeFields.append("maskString(String.valueOf");
			}
			writeFields.append('(');
			writeFields.append(getMethod).append(") : ");
		} else if(card) {
			imports.add(MessageResponseUtils.class);
			writeFields.append("maskSensitiveData ? MessageResponseUtils.");
			if(cls.equals(byte[].class)) {
				writeFields.append("maskFully");
			} else if(cls.equals(String.class)) {
				writeFields.append("maskTrackData");
			} else {
				writeFields.append("maskTrackData(String.valueOf");
			}
			writeFields.append('(');
			writeFields.append(getMethod).append(") : ");
		}

		writeFields.append(getMethod);
		return writeFields;
	}

	protected static void handleParameter(SymbolTable symbolTable, String name, TypeEntry type, boolean multiple, int indent, StringBuilder defineFields, StringBuilder writeFields, StringBuilder readFields, StringBuilder toStringFields, StringBuilder publishFields, Set<Class<?>> imports, MessageType messageType, Map<Class<?>, String> delegates, Class<?> parentCls, Class<?> dataHandlerClass,
			MessageDirection direction)
			throws IntrospectionException {
		Class<?> cls = tm.getClassForQName(type.getQName());
		String fieldName = StringUtils.toJavaName(name);
		String methodSuffix = StringUtils.capitalizeFirst(fieldName);
		if(cls != null) {
			if(byte[].class.isAssignableFrom(cls) && dataHandlerClass != null) {
				cls = dataHandlerClass;
			}
			/*
							if(DataHandler.class.isAssignableFrom(cls)) {
							// ignore this for now
							} else if(DataSource.class.isAssignableFrom(cls)) {
							// ignore this for now
							} else */
			{
				if((Sale.class.isAssignableFrom(parentCls) || AuthorizeMessageData.class.isAssignableFrom(parentCls)) && fieldName.equals("amount"))
					cls = ConvertUtils.convertToWrapperClass(cls);
				imports.add(cls);
				String getMethodPrefix;
				if(cls.equals(boolean.class) || cls.equals(Boolean.class))
					getMethodPrefix = "is";
				else
					getMethodPrefix = "get";
				PropertyDescriptor pd;
				if(multiple) {
					imports.add(List.class);
					imports.add(ArrayList.class);
					String dataTypeName = ConvertUtils.convertToWrapperClass(cls).getSimpleName();
					defineFields.append("\tprotected final List<").append(dataTypeName).append("> ").append(fieldName).append(" = new ArrayList<").append(dataTypeName).append(">();\n");
					publishFields.append("\tpublic List<").append(dataTypeName).append("> ").append(getMethodPrefix).append(methodSuffix).append("() {\n\t\treturn ").append(fieldName).append(";\n\t}\n\n");
				} else if(parentCls != null && (pd = ReflectionUtils.findPropertyDescriptor(parentCls, fieldName)) != null) {
					if(!pd.getPropertyType().isAssignableFrom(cls)) {
						handleParameter(symbolTable, fieldName + StringUtils.capitalizeFirst(cls.getSimpleName()), type, multiple, indent, defineFields, writeFields, readFields, toStringFields, publishFields, imports, messageType, delegates, parentCls, dataHandlerClass, direction);
						return;
					}
					if(pd.getReadMethod() == null || Modifier.isAbstract(pd.getReadMethod().getModifiers())) {
						defineFields.append("\tprotected ").append(cls.getSimpleName()).append(' ').append(fieldName);
						if(direction == MessageDirection.SERVER_TO_CLIENT)
							appendDefault(defineFields, cls);
						defineFields.append(";\n");
						publishFields.append("\tpublic ").append(cls.getSimpleName()).append(' ').append(getMethodPrefix).append(methodSuffix).append("() {\n\t\treturn ").append(fieldName).append(";\n\t}\n\n");
					}
					if(pd.getWriteMethod() == null || Modifier.isAbstract(pd.getWriteMethod().getModifiers()))
						publishFields.append("\tpublic void set").append(methodSuffix).append('(').append(cls.getSimpleName()).append(' ').append(fieldName).append(") {\n\t\tthis.").append(fieldName).append(" = ").append(fieldName).append(";\n\t}\n\n");
				} else {
					boolean delegateUsed = false;
					if(delegates != null)
						for(Map.Entry<Class<?>, String> entry : delegates.entrySet()) {
							pd = ReflectionUtils.findPropertyDescriptor(entry.getKey(), fieldName);
							if(pd != null) {
								if(ECDataHandler.class.isAssignableFrom(pd.getPropertyType()) || EC2DataHandler.class.isAssignableFrom(pd.getPropertyType())) {
									publishFields.append("\tpublic ").append(cls.getSimpleName()).append(' ').append(getMethodPrefix).append(methodSuffix).append("() {\n\t\treturn ").append(entry.getValue()).append('.').append(getMethodPrefix).append(methodSuffix).append("().getFileBytes();\n\t}\n\n");
									publishFields.append("\tpublic void set").append(methodSuffix).append('(').append(cls.getSimpleName()).append(' ').append(fieldName).append(") {\n\t\t").append(entry.getValue()).append('.').append(getMethodPrefix).append(methodSuffix).append("().setFileBytes(").append(fieldName).append(");\n\t}\n\n");
								} else {
									publishFields.append("\tpublic ").append(cls.getSimpleName()).append(' ').append(getMethodPrefix).append(methodSuffix).append("() {\n\t\treturn ").append(entry.getValue()).append('.').append(getMethodPrefix).append(methodSuffix).append("();\n\t}\n\n");
									publishFields.append("\tpublic void set").append(methodSuffix).append('(').append(cls.getSimpleName()).append(' ').append(fieldName).append(") {\n\t\t").append(entry.getValue()).append(".set").append(methodSuffix).append('(').append(fieldName).append(");\n\t}\n\n");
								}
								delegateUsed = true;
								break;
							}
						}
					if(!delegateUsed) {
						defineFields.append("\tprotected ").append(cls.getSimpleName()).append(' ').append(fieldName);
						if(direction == MessageDirection.SERVER_TO_CLIENT)
							appendDefault(defineFields, cls);
						defineFields.append(";\n");
						publishFields.append("\tpublic ").append(cls.getSimpleName()).append(' ').append(getMethodPrefix).append(methodSuffix).append("() {\n\t\treturn ").append(fieldName).append(";\n\t}\n\n");
						publishFields.append("\tpublic void set").append(methodSuffix).append('(').append(cls.getSimpleName()).append(' ').append(fieldName).append(") {\n\t\tthis.").append(fieldName).append(" = ").append(fieldName).append(";\n\t}\n\n");
					}
				}
				if(DataHandler.class.isAssignableFrom(cls)) {
					// ignore this for now
				} else if(DataSource.class.isAssignableFrom(cls)) {
					// ignore this for now
				} else {
				boolean sensitive = SENSITIVE_PROPERTIES.contains(fieldName);
				boolean card = CARD_PROPERTIES.contains(fieldName);
				toStringFields.append("\n\t\t\t.append(\", ").append(fieldName).append("=\").append(");
				String suffix;
				if(sensitive) {
					imports.add(MessageDataUtils.class);
					if(multiple) {
						String dataTypeName = ConvertUtils.convertToWrapperClass(cls).getSimpleName();
						toStringFields.append("'[');\n\t\tfor(").append(dataTypeName).append(" item : ");
						StringBuilder sb = new StringBuilder();
						sb.append(")\n\t\t\tsb.append(");
						sb.append("MessageDataUtils.");
						if(cls.equals(byte[].class)) {
							sb.append("summarizeBytes(item)");
						} else if(cls.equals(String.class)) {
							sb.append("maskString(item)");
						} else {
							sb.append("maskString(String.valueOf(item))");
						}
						sb.append(").append(\", \");\n\t\tsb.setLength(sb.length() - 1);\n\t\tsb.append(']'");
						suffix = sb.toString();
					} else {
						toStringFields.append("MessageDataUtils.");
						if(cls.equals(byte[].class)) {
							toStringFields.append("summarizeBytes");
							suffix = ")";
						} else if(cls.equals(String.class)) {
							toStringFields.append("maskString");
							suffix = ")";
						} else {
							toStringFields.append("maskString(String.valueOf");
							suffix = "))";
						}
						toStringFields.append('(');
					}
				} else if(card) {
					imports.add(MessageResponseUtils.class);
					if(multiple) {
						String dataTypeName = ConvertUtils.convertToWrapperClass(cls).getSimpleName();
						toStringFields.append("'[');\n\t\tfor(").append(dataTypeName).append(" item : ");
						StringBuilder sb = new StringBuilder();
						sb.append(")\n\t\t\tsb.append(");
						sb.append("MessageResponseUtils.");
						if(cls.equals(byte[].class)) {
							sb.append("maskFully(item)");
						} else if(cls.equals(String.class)) {
							sb.append("maskTrackData(item)");
						} else {
							sb.append("maskTrackData(String.valueOf(item))");
						}
						sb.append(").append(\", \");\n\t\tsb.setLength(sb.length() - 1);\n\t\tsb.append(']'");
						suffix = sb.toString();
					} else {
						toStringFields.append("MessageResponseUtils.");
						if(cls.equals(byte[].class)) {
							toStringFields.append("maskFully");
							suffix = ")";
						} else if(cls.equals(String.class)) {
							toStringFields.append("maskTrackData");
							suffix = ")";
						} else {
							toStringFields.append("maskTrackData(String.valueOf");
							suffix = "))";
						}
						toStringFields.append('(');
					}
				} else if(cls.equals(byte[].class)) {
					toStringFields.append("MessageDataUtils.toSummary(");
					suffix = ", 256)";
				} else if(cls.equals(byte.class)) {
					suffix = " & 0xFF";
				} else if(cls.equals(Calendar.class)) {
					toStringFields.append("MessageDataUtils.toString(");
					suffix = ")";
				} else
					suffix = "";
				toStringFields.append(getMethodPrefix).append(methodSuffix).append("()").append(suffix).append(')');
				String setMethod;
				String getMethod;
				if(multiple) {
					readFields.append("\t\t").append(fieldName).append(".clear();\n");
					setMethod = new StringBuilder().append("for(int i = 0, n = ProcessingUtils.readByteInt(data); i < n; i++)\n\t\t\t").append(fieldName).append(".add").toString();
					String dataTypeName = ConvertUtils.convertToWrapperClass(cls).getSimpleName();
					writeFields.append("\t\tProcessingUtils.writeByteInt(reply, ").append(getMethodPrefix).append(methodSuffix).append("().size());\n\t\tfor(")
						.append(dataTypeName).append(" item : ").append(getMethodPrefix).append(methodSuffix).append("())\n");
					getMethod = "item";
				} else {
					setMethod = "set" + methodSuffix;
					getMethod = new StringBuilder().append(getMethodPrefix).append(methodSuffix).append("()").toString();
				}
				if(Enum.class.isAssignableFrom(cls)) {
					Method valueMethod = ReflectionUtils.findMethod(cls, "getValue", ReflectionUtils.ZERO_PARAMS);
					if(!Modifier.isStatic(valueMethod.getModifiers())) {
						getMethod += ".getValue()";
						cls = valueMethod.getReturnType();
					}
				}

				String bufferOpSuffix = bufferOpSuffixes.get(cls);
				if(bufferOpSuffix != null) {
					readFields.append("\t\t").append(setMethod).append("(data.get").append(bufferOpSuffix).append("());\n");
					writeFields.append("\t\treply.put").append(bufferOpSuffix).append('(');
					appendValue(writeFields, cls, sensitive, card, getMethod, imports);
					writeFields.append(");\n");
				} else {
					String utilsOpSuffix = utilsOpSuffixes.get(cls);
					if(utilsOpSuffix != null) {
						imports.add(ProcessingUtils.class);
						readFields.append("\t\t").append(setMethod).append("(ProcessingUtils.read").append(utilsOpSuffix).append("(data));\n");
						writeFields.append("\t\tProcessingUtils.write").append(utilsOpSuffix).append("(reply, ");
						appendValue(writeFields, cls, sensitive, card, getMethod, imports).append(");\n");
					} else if(cls.equals(String.class)) {
						imports.add(ProcessingUtils.class);
						readFields.append("\t\t").append(setMethod).append("(ProcessingUtils.readLongString(data, charset));\n");
						writeFields.append("\t\tProcessingUtils.writeLongString(reply, ");
						appendValue(writeFields, cls, sensitive, card, getMethod, imports).append(", charset);\n");
					} else if(cls.equals(boolean.class)) {
						readFields.append("\t\t").append(setMethod).append("(data.get() != 0);\n");
						writeFields.append("\t\treply.put(").append(getMethod).append("() ? 1 : 0);\n");
					} else if(Date.class.isAssignableFrom(cls)) {
						imports.add(ProcessingUtils.class);
						readFields.append("\t\t").append(setMethod).append("(ProcessingUtils.readDate(data));\n");
						writeFields.append("\t\tProcessingUtils.writeDate(reply, ").append(getMethod).append("());\n");
					} else if(Number.class.isAssignableFrom(cls)) {
						imports.add(ProcessingUtils.class);
						imports.add(ConvertUtils.class);
						imports.add(ConvertException.class);
						readFields.append("\t\ttry {\n\t\t\t").append(setMethod).append("(ConvertUtils.convert(").append(cls.getSimpleName()).append(".class, ProcessingUtils.readShortString(data, charset)));\n\t\t} catch(ConvertException e) {\n\t\t\tthrow createParseException(\"Could not convert '").append(fieldName).append("' to a number\", data.position(), e);\n\t\t}\n");
						writeFields.append("\t\tProcessingUtils.writeShortString(reply, ").append(getMethod).append(" == null ? null : ");
						appendValue(writeFields, cls, sensitive, card, getMethod, imports).append(".toString(), charset);\n");
					} else {
						imports.add(ProcessingUtils.class);
						imports.add(ConvertUtils.class);
						imports.add(ConvertException.class);
						readFields.append("\t\ttry {\n\t\t\t").append(setMethod).append("(ConvertUtils.convert(").append(cls.getSimpleName()).append(".class, ProcessingUtils.readLongString(data, charset)));\n\t\t} catch(ConvertException e) {\n\t\t\tthrow createParseException(\"Could not convert '").append(fieldName).append("' to a number\", data.position(), e);\n\t\t}\n");
						writeFields.append("\t\tProcessingUtils.writeLongString(reply, ").append(getMethod).append(" == null ? null : ");
						appendValue(writeFields, cls, sensitive, card, getMethod, imports).append(".toString(), charset);\n");
					}
				}
				}
			}
			System.out.print(cls.getName());
		} else {
			int dims = type.getDimensions().length() / 2;
			if(dims > 0) {
				for(int i = 0; i < dims; i++)
					System.out.print('[');
				Type subType = symbolTable.getType(type.getComponentType());
				handleParameter(symbolTable, name, subType, true, indent, defineFields, writeFields, readFields, toStringFields, publishFields, imports, messageType, null, indent == 1 ? parentCls : null, dataHandlerClass, direction);
			} else {
				System.out.println('{');
				StringBuilder subWriteFields;
				StringBuilder subReadFields;
				StringBuilder subToStringFields;
				StringBuilder subDefineFields;
				StringBuilder subPublishFields;
				if(multiple) {
					imports.add(List.class);
					imports.add(ArrayList.class);
					imports.add(Collections.class);
					String itemName = StringUtils.singular(fieldName);
					String methodBase = StringUtils.capitalizeFirst(itemName);
					String dataTypeName = methodBase;
					imports.add(Byteable.class);
					defineFields.append("\tprotected final List<").append(dataTypeName).append("> ").append(fieldName).append(" = new ArrayList<").append(dataTypeName).append(">();\n");
					defineFields.append("\tprotected final List<").append(dataTypeName).append("> ").append(fieldName).append("Unmod = Collections.unmodifiableList(").append(dataTypeName).append(");\n");
					defineFields.append("\tpublic class ").append(dataTypeName).append(" implements Byteable {\n");

					readFields.append("\t\t").append(fieldName).append(".clear();\n\t\tfor(int i = 0, n = ProcessingUtils.readByteInt(data); i < n; i++)\n\t\t\tadd").append(methodBase).append(".readData(data);");
					writeFields.append("\t\tProcessingUtils.writeByteInt(reply, get").append(methodSuffix).append("().size());\n\t\tfor(")
						.append(dataTypeName).append(" item : get").append(methodSuffix).append("())\n\t\t\titem.writeData(reply, maskSensitiveData);\n");
					
					publishFields.append("\tpublic List<").append(dataTypeName).append("> get").append(methodSuffix).append("() {\n\t\treturn ").append(fieldName).append("Unmod;\n\t}\n\n");
					publishFields.append("\tpublic ").append(dataTypeName).append(" add").append(methodBase)
						.append("() {\n\t\t").append(dataTypeName).append(' ').append(itemName)
						.append(" = new ").append(dataTypeName).append('(').append(fieldName).append(".size());\n\t\t")
						.append(fieldName).append(".add(").append(itemName).append(");\n\t\treturn ").append(itemName).append(";\n\t}\n\n");

					toStringFields.append("\n\t\t\t.append(\", ").append(fieldName).append("=\").append(get").append(methodSuffix).append("())");

					subWriteFields = new StringBuilder();
					subReadFields = new StringBuilder();
					subToStringFields = new StringBuilder();
					subDefineFields = defineFields;
					subPublishFields = new StringBuilder();
				} else {
					subWriteFields = writeFields;
					subReadFields = readFields;
					subToStringFields = toStringFields;
					subDefineFields = defineFields;
					subPublishFields = publishFields;
					/*
					if(indent == 1) {
						// create delegate
						StringBuilder sb = new StringBuilder();
						String ns = type.getQName().getNamespaceURI();
						if(!StringUtils.isBlank(ns)) {
							String[] h;
							try {
								h = StringUtils.split(new URL(ns).getHost(), '.');
								for(int i = h.length - 1; i >= 0; i--)
									sb.append(h[i]).append('.');
							} catch(MalformedURLException e) {
								e.printStackTrace();
							}

						}
						sb.append(type.getQName().getLocalPart());
						try {
							Class<?> delegateCls = Class.forName(sb.toString());
							imports.add(delegateCls);
							defineFields.append("\tprotected final ").append(delegateCls.getSimpleName()).append(" delegate = new ").append(delegateCls.getSimpleName()).append("();\n\n");
							publishFields.append("\tpublic ").append(delegateCls.getSimpleName()).append(" getDelegate() {\n\t\treturn delegate;\n\t}\n\n");
							delegates.put(delegateCls, "delegate");
						} catch(ClassNotFoundException e) {
							e.printStackTrace();
						}
					}
					*/
				}
				
				int a = 0;
				for(TypeEntry curr = type; curr != null; curr = SchemaUtils.getComplexElementExtensionBase(curr.getNode(), symbolTable)) {
					for(Object ced : SchemaUtils.getContainedElementDeclarations(curr.getNode(), symbolTable)) {
						ElementDecl elem = (ElementDecl) ced;
						for(int i = 0; i <= indent; i++)
							System.out.print('\t');
						QName qname = elem.getQName();
						String subname;
						if(qname != null && !StringUtils.isBlank(qname.getLocalPart())) {
							int pos = qname.getLocalPart().lastIndexOf(SymbolTable.ANON_TOKEN);
							if(pos >= 0)
								subname = qname.getLocalPart().substring(pos + SymbolTable.ANON_TOKEN.length());
							else
								subname = qname.getLocalPart();
						} else {
							subname = "arg" + a++;
						}
						System.out.print(subname);
						System.out.print(' ');
						if(indent > 1 && !StringUtils.isBlank(name))
							subname = name + "/" + subname;
						handleParameter(symbolTable, subname, elem.getType(), false, indent + 1, subDefineFields, subWriteFields, subReadFields, subToStringFields, subPublishFields, imports, messageType, delegates, indent == 1 ? parentCls : null, dataHandlerClass, direction);
						System.out.println();
					}
				}
				if(multiple) {
					defineFields.append("\t\tpublic void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {\n");
					defineFields.append(subWriteFields);
					defineFields.append("\t\t}\n\n\t\tpublic void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {\n");
					defineFields.append(subReadFields);
					defineFields.append("\t\t}\n\n");
					defineFields.append(subPublishFields);
					defineFields.append("\t\tpublic String toString() {\n\t\t\tStringBuilder sb = new StringBuilder();\n\t\t\tsb.append('[')");
					defineFields.append(subToStringFields);
					defineFields.append(".append('[');\n\t\t\treturn sb.toString();\n\t\t}\n\t}\n\n");
				}
				for(int i = 0; i < indent; i++)
					System.out.print('\t');
				System.out.print('}');
			}
		}
	}

	protected static StringBuilder appendDefault(StringBuilder sb, Class<?> cls) {
		if(EC2DataHandler.class.isAssignableFrom(cls) || ECDataHandler.class.isAssignableFrom(cls))
			sb.append(" = EMPTY_DATA_HANDLER");
		else if(String.class.equals(cls))
			sb.append(" = \"\"");
		return sb;
	}
}
