import java.math.BigDecimal;
import java.util.Date;
import java.util.Locale;

import com.usatech.layers.common.util.CampaignUtils;

import simple.bean.ConvertUtils;
import simple.translator.Translator;
import simple.translator.TranslatorFactory;


public class CampaignUtilsTest {

	/**
	 * @param args
	 */
	public static void main(String args[]) throws Exception{
		String month="1-10,20-31";
		System.out.println(CampaignUtils.validateCampaignRecurScheduleMonth(month));
		String hour="13:00-16:00,9:00-13:00,20:00-23:59";
		System.out.println(CampaignUtils.validateCampaignRecurScheduleHour(hour));
		
		Date aDate=CampaignUtils.HOUR_MIN_FORMAT.parse("9:00");
		String dateFormated=CampaignUtils.READ_HOUR_MIN_FORMAT.format(aDate);
		System.out.println("dateFormated:"+dateFormated);
		//System.out.println(ConvertUtils.getStringSafely(null));
		
		Translator translator = TranslatorFactory.getDefaultFactory().getTranslator(Locale.getDefault(), "prepaid.usatech.com");
		String campaignDesc="campaignDesc";
		String discountPercent="0.10";
		Date endDate=new Date();
		String attentionType="NEW";
		String recurSchedule="D|9:00-11:00,14:00-16:00";
		String readRecur=CampaignUtils.readRecurSchedule(recurSchedule);
		System.out.println(readRecur);
		recurSchedule="D|";
		readRecur=CampaignUtils.readRecurSchedule(recurSchedule);
		System.out.println(readRecur);
		recurSchedule="M:12,9-10,3|15:00-15:15,22:00-23:00";
		readRecur=CampaignUtils.readRecurSchedule(recurSchedule);
		System.out.println(readRecur);
		recurSchedule="M:12,9-10,3|";
		readRecur=CampaignUtils.readRecurSchedule(recurSchedule);
		System.out.println(readRecur);
		recurSchedule="W:1,7|9:00-11:00,13:00-16:00";
		readRecur=CampaignUtils.readRecurSchedule(recurSchedule);
		System.out.println(readRecur);
		
		recurSchedule="S:12/31/2013,01/01/2014,01/02/2014|08:00-11:00,13:00-17:00";
		readRecur=CampaignUtils.readRecurSchedule(recurSchedule);
		System.out.println(readRecur);
		
		recurSchedule="W:1,4|";
		readRecur=CampaignUtils.readRecurSchedule(recurSchedule);
		System.out.println(readRecur);
		
		String campaignInfo = translator.translate("prepaid-promo-campaign-detail", "Every purchase is {1,PERCENT} off{3,MATCH,SOON= until {2,DATE,MM/dd/yyyy}} {4} at all participating locations.", campaignDesc, discountPercent, endDate, attentionType, readRecur);
		System.out.println("campaignInfo:"+campaignInfo);
		int campaignTypeId=2;
		String threshold="20";
		BigDecimal cashback=new BigDecimal(threshold).multiply(new BigDecimal(discountPercent));
		if(campaignTypeId==2){
			campaignInfo = translator.translate("prepaid-promo-campaign-replenish-detail", "For every {0,CURRENCY} replenished, you will earn {1,CURRENCY} replenish bonus for the cards assigned to the campaign.", threshold, cashback);
			System.out.println("replenish campaignInfo:"+campaignInfo);
		}
		campaignTypeId=3;
		if(campaignTypeId==3){
			campaignInfo = translator.translate("prepaid-promo-campaign-spend-detail", "For every {0,CURRENCY} spent, you will earn {1,CURRENCY} bonus cash for the cards assigned to the campaign.", threshold, cashback);
			System.out.println("spent campaignInfo:"+campaignInfo);
		}
		
	}

}
