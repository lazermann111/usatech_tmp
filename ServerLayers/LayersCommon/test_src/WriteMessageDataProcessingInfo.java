import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import simple.text.StringUtils;

import com.usatech.layers.common.constants.MessageType;


public class WriteMessageDataProcessingInfo {
	protected static class MessageTypeProcessingInfo {
		protected MessageType messageType;
		protected String netlayerProcessorName;
		protected String netlayerProcessorClass;
		protected String applayerQueueName;
		public MessageType getMessageType() {
			return messageType;
		}
		public void setMessageType(MessageType messageType) {
			this.messageType = messageType;
		}
		public String getNetlayerProcessorName() {
			return netlayerProcessorName;
		}
		public void setNetlayerProcessorName(String netlayerProcessorName) {
			this.netlayerProcessorName = netlayerProcessorName;
		}
		public String getNetlayerProcessorClass() {
			return netlayerProcessorClass;
		}
		public void setNetlayerProcessorClass(String netlayerProcessorClass) {
			this.netlayerProcessorClass = netlayerProcessorClass;
		}
		public String getApplayerQueueName() {
			return applayerQueueName;
		}
		public void setApplayerQueueName(String applayerQueueName) {
			this.applayerQueueName = applayerQueueName;
		}
		
	}
	/**
	 * @param args
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException, IOException {
		Properties props = new Properties();
		props.load(new FileReader("D:\\Development\\Java Projects\\NetLayer\\src\\NetLayerService.properties"));
		SortedMap<String,MessageTypeProcessingInfo> infos = getMessageTypeProcessingInfo(props);
		PrintWriter out = new PrintWriter("D:\\Documents\\MessageTypeProcessingInfo.csv");
		StringUtils.writeCSVLine(new String[] {"Message Type", "Message Type Name", "Processor Name", "Processor Class", "Queue Name", "Is Init", "Is Legacy"}, out);
		for(MessageTypeProcessingInfo info : infos.values()) {
			StringUtils.writeCSVLine(new Object[] {
					info.getMessageType().getHex(),
					info.getMessageType().getTitle(),
					info.getNetlayerProcessorName(),
					info.getNetlayerProcessorClass(),
					info.getApplayerQueueName(),
					info.getMessageType().isInitMessage(),
					!info.getMessageType().isMessageIdExpected(),
			}, out);
		}
		out.flush();
		out.close();
	}

	protected static SortedMap<String,MessageTypeProcessingInfo> getMessageTypeProcessingInfo(Properties properties) {
		//String[] serviceNames = new String[] {"Inbound","Auth","InboundLayer3","InboundUnifiedLayer" };
		Pattern processorQueuePattern = Pattern.compile("(com\\.usatech\\.networklayer\\.processors\\.\\w+(?:\\.\\w+)*?)\\.queueKey");
		Pattern processorClassPattern = Pattern.compile("(com\\.usatech\\.networklayer\\.processors\\.\\w+(?:\\.\\w+)*?)\\.class");
		Pattern typeProcessorPattern = Pattern.compile("simple\\.app\\.Service\\.(\\w+)\\.processor\\((\\w+)\\)");
		Pattern stripQuotesPattern = Pattern.compile("\\s*\\\"([^\"]+)\\\"\\s*");
		
		Map<String,String> processorQueues = new HashMap<String,String>();
		Map<String,String> processorClasses = new HashMap<String,String>();
		Map<String,String> typeProcessors = new HashMap<String,String>();
		for(String p : properties.stringPropertyNames()) {
			Matcher matcher = processorQueuePattern.matcher(p);
			if(matcher.matches()) {
				String processor = matcher.group(1);
				String queue = properties.getProperty(p);
				String old = processorQueues.put(processor, queue);
				if(old != null)
					System.out.println("Found duplicate entry for '" + p + '\'');
			} else {
				matcher = processorClassPattern.matcher(p);
				if(matcher.matches()) {
					String processor = matcher.group(1);
					String cls = properties.getProperty(p);
					String old = processorClasses.put(processor, cls);
					if(old != null)
						System.out.println("Found duplicate entry for '" + p + '\'');
				} else {
					matcher = typeProcessorPattern.matcher(p);
					if(matcher.matches()) {
						String type = matcher.group(2);
						String processor = properties.getProperty(p);
						matcher = stripQuotesPattern.matcher(processor);
						if(!matcher.matches())
							System.out.println("Invalid processor pattern for entry for '" + p + '\'');
						else {
							processor = matcher.group(1);
							String old = typeProcessors.put(type, processor);
							if(old != null && !processor.equals(old))
								System.out.println("Found inconsistent entry for '" + p + '\'');
						}
					}
				}		
			}
		}
		SortedMap<String,MessageTypeProcessingInfo> infos = new TreeMap<String,MessageTypeProcessingInfo>();	
		for(MessageType messageType : MessageType.values()) { 
			MessageTypeProcessingInfo info = new MessageTypeProcessingInfo();
			info.setMessageType(messageType);
			String processor = typeProcessors.get(messageType.getHex());
			String queue = processor == null ? null : processorQueues.get(processor);
			String cls = processor == null ? null : processorClasses.get(processor);
			info.setNetlayerProcessorName(processor);
			info.setNetlayerProcessorClass(cls);
			info.setApplayerQueueName(queue);
			infos.put(messageType.getHex(),info);
		}
		return infos;
	}
}
