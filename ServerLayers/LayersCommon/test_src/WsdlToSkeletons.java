


import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.wsdl.Binding;
import javax.wsdl.BindingOperation;
import javax.wsdl.Definition;
import javax.wsdl.Message;
import javax.wsdl.PortType;
import javax.wsdl.Service;
import javax.xml.namespace.QName;

import org.apache.axis.Constants;
import org.apache.axis.encoding.TypeMapping;
import org.apache.axis.encoding.TypeMappingDelegate;
import org.apache.axis.encoding.TypeMappingRegistry;
import org.apache.axis.encoding.TypeMappingRegistryImpl;
import org.apache.axis.wsdl.gen.Generator;
import org.apache.axis.wsdl.gen.NoopFactory;
import org.apache.axis.wsdl.gen.Parser;
import org.apache.axis.wsdl.symbolTable.BindingEntry;
import org.apache.axis.wsdl.symbolTable.ElementDecl;
import org.apache.axis.wsdl.symbolTable.Parameter;
import org.apache.axis.wsdl.symbolTable.Parameters;
import org.apache.axis.wsdl.symbolTable.SchemaUtils;
import org.apache.axis.wsdl.symbolTable.SymbolTable;
import org.apache.axis.wsdl.symbolTable.Type;
import org.apache.axis.wsdl.symbolTable.TypeEntry;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.lang.InvalidValueException;
import simple.text.StringUtils;
import simple.util.ConversionSet;
import simple.util.MapBackedSet;

import com.usatech.ec.ECDataHandler;
import com.usatech.ec2.EC2DataHandler;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.messagedata.AuthorizeMessageData;
import com.usatech.layers.common.messagedata.Byteable;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageDataUtils;
import com.usatech.layers.common.messagedata.MessageDirection;
import com.usatech.layers.common.messagedata.Sale;

public class WsdlToSkeletons extends NoopFactory {
	protected class StubGenerator implements Generator {
		protected final Object o;

		public StubGenerator(Object o) {
			this.o = o;
		}

		@Override
		public void generate() throws IOException {
			System.out.println("Processing " + o);
		}
	}

	@Override
	public Generator getGenerator(Definition definition, SymbolTable symbolTable) {
		return new StubGenerator(definition);
	}

	@Override
	public Generator getGenerator(TypeEntry type, SymbolTable symbolTable) {
		return new StubGenerator(type);
	}

	@Override
	public Generator getGenerator(Service service, SymbolTable symbolTable) {
		return new StubGenerator(service);
	}

	@Override
	public Generator getGenerator(Binding binding, SymbolTable symbolTable) {
		return new StubGenerator(binding);
	}

	@Override
	public Generator getGenerator(PortType portType, SymbolTable symbolTable) {
		return new StubGenerator(portType);
	}

	@Override
	public Generator getGenerator(Message message, SymbolTable symbolTable) {
		return new StubGenerator(message);
	}

	@Override
	public void generatorPass(Definition def, SymbolTable symbolTable) {
		// TODO Auto-generated method stub

	}

	protected static final Pattern replacePrefixPattern = Pattern.compile("^EC\\d*");
	public static void main(String[] args) throws Exception {
		File wsdlFile = new File(args[0]);
		// String suffix = "2";
		// File wsdlFile = new File("../NetLayer/resources/wsdl/ePortConnect" + suffix + ".wsdl");
		Parser parser = new Parser();
		// parser.setFactory(new WsdlToMessageData());
		parser.run("file:" + wsdlFile.getAbsolutePath());
		/*
		// Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(wsdlFile);
		// parser.run("file:" + wsdlFile.getAbsolutePath(), doc);

		for(Object k : parser.getSymbolTable().getHashMap().keySet()) {
			BindingEntry be = parser.getSymbolTable().getBindingEntry((QName) k);
			System.out.println(be);
		}*/
		// File baseDirectory = new File("../TestClient/gen_src/");
		File baseDirectory = new File("../NetLayer/src/");
		if(!baseDirectory.exists()) {
			throw new Exception("File " + baseDirectory.getAbsolutePath() + " does not exist");
		}
		Definition def = parser.getCurrentDefinition();
		String packageName;
		if(def.getTargetNamespace() != null && def.getTargetNamespace().startsWith("urn:"))
			packageName = reversePieces(def.getTargetNamespace().substring(4), '.');
		else
			packageName = "com.usatech.ec2";
		String prefix = StringUtils.capitalizeFirst(StringUtils.substringAfterLast(packageName, "."));

		File skeletonFile = new File(baseDirectory, packageName.replace('.', '/') + '/' + prefix + "Skeleton.java");
		skeletonFile.getParentFile().mkdirs();
		StringBuilder inter = new StringBuilder();
		StringBuilder methods = new StringBuilder();
		Set<Class<?>> imports = new LinkedHashSet<Class<?>>();
		// imports.add(Class.forName("com.usatech.ec" + suffix + ".EC" + suffix + "ServiceAPI"));
		PrintWriter writer = new PrintWriter(new FileWriter(skeletonFile));
		try {
			writer.append("package ").append(packageName).append(";\n\n");
			writer.append("import com.usatech.eportconnect.ECRequestHandler;\n");
			writer.append("import com.usatech.layers.common.messagedata.*;\n");
			writer.append("import simple.lang.InvalidValueException;\n");
			writer.append("import simple.translator.Translator;\n");
			writer.append("import simple.translator.TranslatorFactory;\n");
			writer.append("import simple.app.ServiceException;\n");
			writer.append("import com.usatech.eportconnect.ECSession;\n");
			writer.append("import com.usatech.eportconnect.ECSessionManager;\n");
			writer.append("import static com.usatech.eportconnect.ECAxisServlet.SOAP_PROTOCOL;\n\n");
			writer.append("public class ").append(prefix).append("Skeleton {\n\n");
			writer.append("\tprotected static Translator getTranslator() throws ServiceException {\n");
			writer.append("\t\treturn TranslatorFactory.getDefaultFactory().getTranslator(null, null);\n");
			writer.append("\t}\n\n");
			Class<?> hessionAPI = Class.forName(packageName + '.' + prefix.toUpperCase() + "ServiceAPI");
			imports.add(InvalidValueException.class);
			String suffix = prefix.substring(2);
			Map<String, String> genList = new HashMap<String, String>();
			for(Object b : def.getBindings().values()) {
				Binding bnd = (Binding) b;
				BindingEntry bindingEntry = parser.getSymbolTable().getBindingEntry(bnd.getQName());
				for(Object o : bnd.getBindingOperations()) {
					BindingOperation bop = (BindingOperation) o;
					String requestName = bop.getOperation().getInput().getMessage().getQName().getLocalPart();
					if(genList.containsKey(requestName))
						continue;
					Parameters parameters = parser.getSymbolTable().getOperationParameters(bop.getOperation(), "", bindingEntry);
					String responseName = parameters.returnParam.getType().getQName().getLocalPart();
					if(StringUtils.isBlank(responseName))
						responseName = bop.getOperation().getOutput().getMessage().getQName().getLocalPart();
					// Class<?> cls = tm.getClassForQName(parameters.returnParam.getType().getQName());
					// Type type = parser.getSymbolTable().getType(bop.getOperation().getInput().getMessage().getQName());
					MessageType requestMessageType = getMessageType(suffix, requestName, MessageDirection.CLIENT_TO_SERVER);
					MessageType responseMessageType = getMessageType(suffix, responseName.startsWith(prefix.toUpperCase()) ? responseName.substring(prefix.length()) : responseName, MessageDirection.SERVER_TO_CLIENT);
					Class<?> requestMessageClass = Class.forName("com.usatech.layers.common.messagedata.MessageData_" + requestMessageType.getHex());
					Class<?> requestClass = Class.forName(packageName + '.' + StringUtils.capitalizeFirst(bop.getName()));
					String responseType = parameters.returnParam.getType().getQName().getLocalPart();
					Class<?> responseClass = Class.forName(packageName + '.' + responseType);

					// SOAP
					writer.append("\tpublic ").append(StringUtils.capitalizeFirst(bop.getName())).append("Response ").append(bop.getName()).append('(').append(StringUtils.capitalizeFirst(bop.getName())).append(' ').append(bop.getName()).append(") {\n");
					writer.append("\t\tMessageData_").append(requestMessageType.getHex()).append(" requestData = new MessageData_").append(requestMessageType.getHex()).append("();\n");
					writer.append("\t\trequestData.setProtocol(SOAP_PROTOCOL);\n");

					// Hessian
					Method hessionMethod = ReflectionUtils.findMethod(hessionAPI, bop.getName(), new Class<?>[parameters.list.size()]);
					methods.append("\tpublic ").append(hessionMethod.getReturnType().getSimpleName()).append(" ").append(bop.getName()).append('(');
					Class<?>[] paramTypes = hessionMethod.getParameterTypes();
					for(int i = 0; i < paramTypes.length; i++) {
						Parameter p = (Parameter) parameters.list.get(i);
						String fieldName = StringUtils.toJavaName(p.getName());
						methods.append(paramTypes[i].getSimpleName()).append(' ').append(fieldName).append(", ");
						imports.add(paramTypes[i]);
					}
					methods.setLength(methods.length() - 2);
					methods.append(") {\n");
					methods.append("\t\tMessageData_").append(requestMessageType.getHex()).append(" requestData = new MessageData_").append(requestMessageType.getHex()).append("();\n");
					methods.append("\t\trequestData.setProtocol(HESSIAN_PROTOCOL);\n");
					
					// set properties
					for(Object l : parameters.list) {
						Parameter p = (Parameter) l;
						Class<?> cls = tm.getClassForQName(p.getType().getQName());
						String fieldName = StringUtils.toJavaName(p.getName());
						String methodSuffix = StringUtils.capitalizeFirst(fieldName);
						String getMethodPrefix;
						if(cls.equals(boolean.class) || cls.equals(Boolean.class))
							getMethodPrefix = "is";
						else
							getMethodPrefix = "get";
						Method method = requestClass.getMethod(getMethodPrefix + methodSuffix);
						if(method != null && !cls.isAssignableFrom(method.getReturnType())) {
							cls = method.getReturnType();
							
						}
						String setMethodName = "set" + methodSuffix;
						if(!ReflectionUtils.hasMethod(requestMessageClass, setMethodName, ReflectionUtils.ANY_ONE_PARAM)) {
							String option2 = setMethodName + StringUtils.capitalizeFirst(cls.getSimpleName());
							if(ReflectionUtils.hasMethod(requestMessageClass, option2, ReflectionUtils.ANY_ONE_PARAM))
								setMethodName = option2;
						}
						writer.append("\t\trequestData.").append(setMethodName).append("(").append(bop.getName()).append(".").append(getMethodPrefix).append(methodSuffix).append("());\n");

						methods.append("\t\trequestData.").append(setMethodName).append("(").append(fieldName).append(");\n");
					}

					boolean closeLaterOnOK = false;
					for(PropertyDescriptor pd : ReflectionUtils.getPropertyDescriptors(responseClass)) {
						if(pd.getName().equals("class"))
							continue;
						if(DataHandler.class.isAssignableFrom(pd.getPropertyType()))
							closeLaterOnOK = true;
					}

					// SOAP
					writer.append("\t\tMessageData_").append(responseMessageType.getHex()).append(" responseData = requestData.createResponse();\n");
					writer.append("\t\tECSession session = ECSessionManager.startSession(requestData, responseData);\n");
					writer.append("\t\tif(session != null) {\n");
					writer.append("\t\t\ttry {\n");
					writer.append("\t\t\t\ttry {\n");
					writer.append("\t\t\t\t\trequestData.validate(getTranslator());\n");
					writer.append("\t\t\t\t\tresponseData = ECRequestHandler.").append(bop.getName()).append("(requestData, responseData, session);\n");
					writer.append("\t\t\t\t} catch(InvalidValueException e) {\n");
					writer.append("\t\t\t\t\tsession.getLog().error(\"Request failed validation\", e);\n");
					writer.append("\t\t\t\t\tresponseData.setReturnMessage(e.getMessage());\n");
					writer.append("\t\t\t\t} catch(Exception e) {\n");
					writer.append("\t\t\t\t\tsession.getLog().error(\"Error processing ePortConnect message\", e);\n");
					writer.append("\t\t\t\t\tresponseData.setReturnMessage(\"Error\");\n");
					writer.append("\t\t\t\t}\n");
					writer.append("\t\t\t\tif(session.getReplyMessage() != responseData)\n");
					writer.append("\t\t\t\t\tsession.setReplyMessage(responseData);\n");
					writer.append("\t\t\t} finally {\n");
					if(closeLaterOnOK) {
						writer.append("\t\t\t\t// if return code is RES_OK, then FileBlockInputStream will end the session\n");
						writer.append("\t\t\t\tif(responseData.getReturnCode() != ");
						if(!packageName.equals("com.usatech.ec"))
							writer.append("com.usatech.ec.");
						writer.append("ECResponse.RES_OK)\n\t");
					}
					writer.append("\t\t\t\tECSessionManager.endSession(session);\n");
					writer.append("\t\t\t}\n");
					writer.append("\t\t}\n");
					writer.append("\t\t").append(packageName).append(".xsd.").append(responseType).append(" responseReturn = new ").append(packageName).append(".xsd.").append(responseType).append("();\n");
					
					Set<String> notNillables = new HashSet<String>();
					for(TypeEntry curr = parameters.returnParam.getType(); curr != null; curr = SchemaUtils.getComplexElementExtensionBase(curr.getNode(), parser.getSymbolTable())) {
						for(Object ced : SchemaUtils.getContainedElementDeclarations(curr.getNode(), parser.getSymbolTable())) {
							ElementDecl elem = (ElementDecl) ced;
							if(!elem.getNillable() && elem.getQName() != null && !StringUtils.isBlank(elem.getQName().getLocalPart())) {
								QName qname = elem.getQName();
								String subname;
								int pos = qname.getLocalPart().lastIndexOf(SymbolTable.ANON_TOKEN);
								if(pos >= 0)
									subname = qname.getLocalPart().substring(pos + SymbolTable.ANON_TOKEN.length());
								else
									subname = qname.getLocalPart();

								notNillables.add(subname);
							}
						}
					}
					for(PropertyDescriptor pd : ReflectionUtils.getPropertyDescriptors(responseClass)) {
						if(pd.getName().equals("class"))
							continue;
						writer.append("\t\tresponseReturn.set").append(StringUtils.capitalizeFirst(pd.getName())).append('(');
						if(notNillables.contains(pd.getName()) && pd.getPropertyType().equals(String.class))
							writer.append("responseData.").append(pd.getReadMethod().getName()).append("() == null ? \"\" : ");
						writer.append("responseData.").append(pd.getReadMethod().getName()).append("());\n");
					}
					writer.append("\t\t").append(StringUtils.capitalizeFirst(bop.getName())).append("Response responseMsg = new ").append(StringUtils.capitalizeFirst(bop.getName())).append("Response();\n");
					writer.append("\t\tresponseMsg.set_return(responseReturn);\n");
					writer.append("\t\treturn responseMsg;\n");
					writer.append("\t}\n\n");
					
					// Hessian
					methods.append("\t\tMessageData_").append(responseMessageType.getHex()).append(" responseData = requestData.createResponse();\n");
					methods.append("\t\tECSession session = ECSessionManager.startSession(requestData, responseData);\n");
					methods.append("\t\tif(session != null) {\n");
					methods.append("\t\t\ttry {\n");
					methods.append("\t\t\t\ttry {\n");
					methods.append("\t\t\t\t\trequestData.validate(getTranslator());\n");
					methods.append("\t\t\t\t\tresponseData = ECRequestHandler.").append(bop.getName()).append("(requestData, responseData, session);\n");
					methods.append("\t\t\t\t} catch(InvalidValueException e) {\n");
					methods.append("\t\t\t\t\tsession.getLog().error(\"Request failed validation\", e);\n");
					methods.append("\t\t\t\t\tresponseData.setReturnMessage(e.getMessage());\n");
					methods.append("\t\t\t\t} catch(Exception e) {\n");
					methods.append("\t\t\t\t\tsession.getLog().error(\"Error processing ePortConnect message\", e);\n");
					methods.append("\t\t\t\t\tresponseData.setReturnMessage(\"Error\");\n");
					methods.append("\t\t\t\t}\n");
					methods.append("\t\t\t\tif(session.getReplyMessage() != responseData)\n");
					methods.append("\t\t\t\t\tsession.setReplyMessage(responseData);\n");
					methods.append("\t\t\t} finally {\n");
					if(closeLaterOnOK) {
						methods.append("\t\t\t\t// if return code is RES_OK, then FileBlockInputStream will end the session\n");
						methods.append("\t\t\t\tif(responseData.getReturnCode() != ");
						if(!packageName.equals("com.usatech.ec"))
							methods.append("com.usatech.ec.");
						methods.append("ECResponse.RES_OK)\n\t");
					}
					methods.append("\t\t\t\tECSessionManager.endSession(session);\n");
					methods.append("\t\t\t}\n");
					methods.append("\t\t}\n");
					methods.append("\t\t").append(responseName).append(" response = new ").append(responseName).append("();\n");
					for(PropertyDescriptor pd : ReflectionUtils.getPropertyDescriptors(responseClass)) {
						if(pd.getName().equals("class"))
							continue;
						methods.append("\t\tresponse.set").append(StringUtils.capitalizeFirst(pd.getName())).append("(responseData.").append(pd.getReadMethod().getName()).append("());\n");
					}

					methods.append("\t\treturn response;\n");
					methods.append("\t}\n\n");

					genList.put(requestName, null);

					inter.append("\tpublic static MessageData_").append(responseMessageType.getHex()).append(" ").append(bop.getName()).append("(MessageData_").append(requestMessageType.getHex()).append(" requestData, MessageData_").append(responseMessageType.getHex()).append(" responseData, ECSession session) {\n\t\t//TODO:Implement me\n\t\treturn responseData;\n\t}\n\n");
				}
			}
			writer.append("}\n");
			writer.flush();
		} finally {
			writer.close();
		}
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder importString = new StringBuilder();
		for(Class<?> clazz : imports) {
			if(shouldImport(clazz)) {
				String cn = clazz.getCanonicalName();
				if(cn != null)
					importString.append("import ").append(cn).append(";\n");
			}
		}
		params.put("imports", importString.toString());
		params.put("methods", methods.toString());
		params.put("namePrefix", prefix.toUpperCase());
		params.put("uriSuffix", prefix.toLowerCase());
		params.put("serialVersionUID", -8684799447422928604L + prefix.hashCode());

		File hessianFile = new File(baseDirectory, "com/usatech/eportconnect/" + prefix.toUpperCase() + "HessianServlet.java");
		hessianFile.getParentFile().mkdirs();
		writer = new PrintWriter(new FileWriter(hessianFile));
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(WsdlToSkeletons.class.getResourceAsStream("HessianServletTemplate.txt")));
			Pattern replacePattern = Pattern.compile("\\$\\{([A-Za-z0-9_.\\(\\)\\[\\]]+)\\}");
			String line;
			while((line = reader.readLine()) != null) {
				Matcher matcher = replacePattern.matcher(line);
				int pos = 0;
				while(matcher.find()) {
					writer.print(line.substring(pos, matcher.start()));
					writer.print(ReflectionUtils.getProperty(params, matcher.group(1)));
					pos = matcher.end();
				}
				writer.println(line.substring(pos));
			}
			writer.flush();
		} finally {
			writer.close();
		}
		System.out.println("--- For ECRequestHandler ------------------");
		System.out.println(inter);
	}

	protected static String reversePieces(String orig, char delimiter) {
		StringBuilder sb = new StringBuilder();
		int p = orig.length();
		while(true) {
			int next = orig.lastIndexOf(delimiter, p - 1);
			if(next < 0) {
				sb.append(orig, 0, p);
				return sb.toString();
			}
			sb.append(orig, next + 1, p).append(delimiter);
			p = next;
			next = orig.lastIndexOf(delimiter, p - 1);
		}
	}

	protected static TypeMappingRegistry tmr = new TypeMappingRegistryImpl();
	protected static TypeMapping tm = (TypeMapping) tmr.getTypeMapping(Constants.URI_SOAP11_ENC);
	protected static final AtomicInteger[] lastMessageTypeValues = new AtomicInteger[256];
	protected final static Set<String> SENSITIVE_PROPERTIES = new ConversionSet<String, String>(new MapBackedSet<String>(new ConcurrentHashMap<String, Object>(10, 0.75f, 1))) {
		@Override
		protected boolean isReversible() {
			return true;
		}

		@Override
		protected String convertTo(String object1) {
			return object1 == null ? null : object1.toUpperCase();
		}

		@Override
		protected String convertFrom(String object0) {
			return object0 == null ? null : object0.toUpperCase();
		}
	};
	protected final static Set<String> CARD_PROPERTIES = new ConversionSet<String, String>(new MapBackedSet<String>(new ConcurrentHashMap<String, Object>(10, 0.75f, 1))) {
		@Override
		protected boolean isReversible() {
			return true;
		}

		@Override
		protected String convertTo(String object1) {
			return object1 == null ? null : object1.toUpperCase();
		}

		@Override
		protected String convertFrom(String object0) {
			return object0 == null ? null : object0.toUpperCase();
		}
	};
	static {
		SENSITIVE_PROPERTIES.add("PASSWORD");
		SENSITIVE_PROPERTIES.add("NEWPASSWORD");
		SENSITIVE_PROPERTIES.add("CREDENTIAL");
		SENSITIVE_PROPERTIES.add("NEWCREDENTIAL");
		SENSITIVE_PROPERTIES.add("PASSCODE");
		SENSITIVE_PROPERTIES.add("NEWPASSCODE");
		CARD_PROPERTIES.add("CARDDATA");
		((TypeMappingDelegate) tm).setDoAutoTypes(true);
	}

	protected static boolean isSale(Parameter[] parameters) {
		for(Parameter p : parameters)
			if("tranId".equalsIgnoreCase(p.getName()))
				return true;
		return false;
	}

	protected static MessageType getMessageType(String suffix, String operationName, MessageDirection direction) {
		String enumNameSuffix = fromJavaName(operationName);
		String messageTypeName = "WS" + suffix + '_' + enumNameSuffix;
		MessageType messageType = ConvertUtils.convertSafely(MessageType.class, messageTypeName, null);
		if(messageType == null) {
			System.out.println("Please add MessageType:");
			System.out.print('\t');
			System.out.print(messageTypeName);
			System.out.print("(new byte[]{");
			byte majorMessageTypeValue;
			switch(direction) {
				case CLIENT_TO_SERVER:
					majorMessageTypeValue = 2;
					break;
				case SERVER_TO_CLIENT:
					majorMessageTypeValue = 3;
					break;
				case INTRA_SERVER:
				default:
					majorMessageTypeValue = 0;
					break;
			}
			System.out.print(majorMessageTypeValue);
			System.out.print(", ");
			if(lastMessageTypeValues[majorMessageTypeValue] == null) {
				for(MessageType mt : MessageType.values()) {
					byte[] b = mt.getValue();
					if(b != null && b.length == 2) {
						if(lastMessageTypeValues[b[0] & 0xFF] == null)
							lastMessageTypeValues[b[0] & 0xFF] = new AtomicInteger(b[1] & 0xFF);
						else if(lastMessageTypeValues[b[0] & 0xFF].get() < (b[1] & 0xFF))
							lastMessageTypeValues[b[0] & 0xFF].set(b[1] & 0xFF);
					}
				}
				if(lastMessageTypeValues[majorMessageTypeValue] == null)
					lastMessageTypeValues[majorMessageTypeValue] = new AtomicInteger();
			}
			byte minorMessageTypeValue = (byte) lastMessageTypeValues[majorMessageTypeValue].getAndIncrement();
			System.out.print(minorMessageTypeValue);
			System.out.print("}, \"Web Service - ");
			System.out.print(StringUtils.capitalizeAllWords(enumNameSuffix.replace('_', ' ').toLowerCase()));
			System.out.print("\", false, false),");
			System.out.println();
		}
		return messageType;
	}


	protected static boolean shouldImport(Class<?> clazz) {
		if(clazz.getPackage() != null && !clazz.getPackage().getName().equals("java.lang") && !clazz.getPackage().getName().equals(MessageData.class.getPackage().getName()))
			return true;
		return false;
	}
	protected static String fromJavaName(String name) {
		StringBuilder sb = new StringBuilder();
		char last = '_';
		for(int i = 0; i < name.length(); i++) {
			char ch = name.charAt(i);
			if(Character.isDigit(ch)) {
				if(last != '_' && !Character.isDigit(last))
					sb.append('_');
				sb.append(ch);
				last = ch;
			} else if(Character.isLetter(ch)) {
				if(last != '_' && (Character.isDigit(last) || (Character.isUpperCase(ch) && Character.isLowerCase(last))))
					sb.append('_');
				sb.append(Character.toUpperCase(ch));
				last = ch;
			} else {
				sb.append('_');
				last = '_';
			}
		}
		return sb.toString();
	}

	protected static Map<Class<?>, String> bufferOpSuffixes = new HashMap<Class<?>, String>();
	protected static Map<Class<?>, String> utilsOpSuffixes = new HashMap<Class<?>, String>();
	static {
		bufferOpSuffixes.put(byte.class, "");
		bufferOpSuffixes.put(short.class, "Short");
		bufferOpSuffixes.put(int.class, "Int");
		bufferOpSuffixes.put(long.class, "Long");
		bufferOpSuffixes.put(float.class, "Float");
		bufferOpSuffixes.put(double.class, "Double");
		bufferOpSuffixes.put(char.class, "Char");

		bufferOpSuffixes.put(Byte.class, "");
		bufferOpSuffixes.put(Short.class, "Short");
		bufferOpSuffixes.put(Integer.class, "Int");
		bufferOpSuffixes.put(Long.class, "Long");
		bufferOpSuffixes.put(Float.class, "Float");
		bufferOpSuffixes.put(Double.class, "Double");
		bufferOpSuffixes.put(Character.class, "Char");
		
		utilsOpSuffixes.put(byte[].class, "LongBytes");
		utilsOpSuffixes.put(BigDecimal.class, "StringAmount");
		utilsOpSuffixes.put(BigInteger.class, "StringInt");
		utilsOpSuffixes.put(Date.class, "Date");
		utilsOpSuffixes.put(Calendar.class, "Timestamp");
		utilsOpSuffixes.put(Currency.class, "Currency");
		utilsOpSuffixes.put(Boolean.class, "Boolean");
	}

	protected static StringBuilder appendValue(StringBuilder writeFields, Class<?> cls, boolean sensitive, boolean card, String getMethod, Set<Class<?>> imports) {
		if(sensitive) {
			imports.add(MessageDataUtils.class);
			writeFields.append("maskSensitiveData ? MessageDataUtils.");
			if(cls.equals(byte[].class)) {
				writeFields.append("summarizeBytes");
			} else if(cls.equals(String.class)) {
				writeFields.append("maskString");
			} else {
				writeFields.append("maskString(String.valueOf");
			}
			writeFields.append('(');
			writeFields.append(getMethod).append(") : ");
		} else if(card) {
			imports.add(MessageResponseUtils.class);
			writeFields.append("maskSensitiveData ? MessageResponseUtils.");
			if(cls.equals(byte[].class)) {
				writeFields.append("maskFully");
			} else if(cls.equals(String.class)) {
				writeFields.append("maskTrackData");
			} else {
				writeFields.append("maskTrackData(String.valueOf");
			}
			writeFields.append('(');
			writeFields.append(getMethod).append(") : ");
		}

		writeFields.append(getMethod);
		return writeFields;
	}

	protected static void handleParameter(SymbolTable symbolTable, String name, TypeEntry type, boolean multiple, int indent, StringBuilder defineFields, StringBuilder writeFields, StringBuilder readFields, StringBuilder toStringFields, StringBuilder publishFields, Set<Class<?>> imports, MessageType messageType, Map<Class<?>, String> delegates, Class<?> parentCls) throws IntrospectionException {
		Class<?> cls = tm.getClassForQName(type.getQName());
		String fieldName = StringUtils.toJavaName(name);
		String methodSuffix = StringUtils.capitalizeFirst(fieldName);
		if(cls != null) {
			if(DataHandler.class.isAssignableFrom(cls)) {
				// ignore this for now
			} else if(DataSource.class.isAssignableFrom(cls)) {
				// ignore this for now
			} else {
				if((Sale.class.isAssignableFrom(parentCls) || AuthorizeMessageData.class.isAssignableFrom(parentCls)) && fieldName.equals("amount"))
					cls = ConvertUtils.convertToWrapperClass(cls);
				imports.add(cls);
				String getMethodPrefix;
				if(cls.equals(boolean.class) || cls.equals(Boolean.class))
					getMethodPrefix = "is";
				else
					getMethodPrefix = "get";
				PropertyDescriptor pd;
				if(multiple) {
					imports.add(List.class);
					imports.add(ArrayList.class);
					String dataTypeName = ConvertUtils.convertToWrapperClass(cls).getSimpleName();
					defineFields.append("\tprotected final List<").append(dataTypeName).append("> ").append(fieldName).append(" = new ArrayList<").append(dataTypeName).append(">();\n");
					publishFields.append("\tpublic List<").append(dataTypeName).append("> ").append(getMethodPrefix).append(methodSuffix).append("() {\n\t\treturn ").append(fieldName).append(";\n\t}\n\n");
				} else if(parentCls != null && (pd = ReflectionUtils.findPropertyDescriptor(parentCls, fieldName)) != null) {
					if(!pd.getPropertyType().isAssignableFrom(cls)) {
						handleParameter(symbolTable, fieldName + StringUtils.capitalizeFirst(cls.getSimpleName()), type, multiple, indent, defineFields, writeFields, readFields, toStringFields, publishFields, imports, messageType, delegates, parentCls);
						return;
					}
					if(pd.getReadMethod() == null || Modifier.isAbstract(pd.getReadMethod().getModifiers())) {
						defineFields.append("\tprotected ").append(cls.getSimpleName()).append(' ').append(fieldName).append(";\n");
						publishFields.append("\tpublic ").append(cls.getSimpleName()).append(' ').append(getMethodPrefix).append(methodSuffix).append("() {\n\t\treturn ").append(fieldName).append(";\n\t}\n\n");
					}
					if(pd.getWriteMethod() == null || Modifier.isAbstract(pd.getWriteMethod().getModifiers()))
						publishFields.append("\tpublic void set").append(methodSuffix).append('(').append(cls.getSimpleName()).append(' ').append(fieldName).append(") {\n\t\tthis.").append(fieldName).append(" = ").append(fieldName).append(";\n\t}\n\n");
				} else {
					boolean delegateUsed = false;
					if(delegates != null)
						for(Map.Entry<Class<?>, String> entry : delegates.entrySet()) {
							pd = ReflectionUtils.findPropertyDescriptor(entry.getKey(), fieldName);
							if(pd != null) {
								if(ECDataHandler.class.isAssignableFrom(pd.getPropertyType()) || EC2DataHandler.class.isAssignableFrom(pd.getPropertyType())) {
									publishFields.append("\tpublic ").append(cls.getSimpleName()).append(' ').append(getMethodPrefix).append(methodSuffix).append("() {\n\t\treturn ").append(entry.getValue()).append('.').append(getMethodPrefix).append(methodSuffix).append("().getFileBytes();\n\t}\n\n");
									publishFields.append("\tpublic void set").append(methodSuffix).append('(').append(cls.getSimpleName()).append(' ').append(fieldName).append(") {\n\t\t").append(entry.getValue()).append('.').append(getMethodPrefix).append(methodSuffix).append("().setFileBytes(").append(fieldName).append(");\n\t}\n\n");
								} else {
									publishFields.append("\tpublic ").append(cls.getSimpleName()).append(' ').append(getMethodPrefix).append(methodSuffix).append("() {\n\t\treturn ").append(entry.getValue()).append('.').append(getMethodPrefix).append(methodSuffix).append("();\n\t}\n\n");
									publishFields.append("\tpublic void set").append(methodSuffix).append('(').append(cls.getSimpleName()).append(' ').append(fieldName).append(") {\n\t\t").append(entry.getValue()).append(".set").append(methodSuffix).append('(').append(fieldName).append(");\n\t}\n\n");
								}
								delegateUsed = true;
								break;
							}
						}
					if(!delegateUsed) {
						defineFields.append("\tprotected ").append(cls.getSimpleName()).append(' ').append(fieldName).append(";\n");
						publishFields.append("\tpublic ").append(cls.getSimpleName()).append(' ').append(getMethodPrefix).append(methodSuffix).append("() {\n\t\treturn ").append(fieldName).append(";\n\t}\n\n");
						publishFields.append("\tpublic void set").append(methodSuffix).append('(').append(cls.getSimpleName()).append(' ').append(fieldName).append(") {\n\t\tthis.").append(fieldName).append(" = ").append(fieldName).append(";\n\t}\n\n");
					}
				}

				boolean sensitive = SENSITIVE_PROPERTIES.contains(fieldName);
				boolean card = CARD_PROPERTIES.contains(fieldName);
				toStringFields.append("\n\t\t\t.append(\", ").append(fieldName).append("=\").append(");
				String suffix;
				if(sensitive) {
					imports.add(MessageDataUtils.class);
					if(multiple) {
						String dataTypeName = ConvertUtils.convertToWrapperClass(cls).getSimpleName();
						toStringFields.append("'[');\n\t\tfor(").append(dataTypeName).append(" item : ");
						StringBuilder sb = new StringBuilder();
						sb.append(")\n\t\t\tsb.append(");
						sb.append("MessageDataUtils.");
						if(cls.equals(byte[].class)) {
							sb.append("summarizeBytes(item)");
						} else if(cls.equals(String.class)) {
							sb.append("maskString(item)");
						} else {
							sb.append("maskString(String.valueOf(item))");
						}
						sb.append(").append(\", \");\n\t\tsb.setLength(sb.length() - 1);\n\t\tsb.append(']'");
						suffix = sb.toString();
					} else {
						toStringFields.append("MessageDataUtils.");
						if(cls.equals(byte[].class)) {
							toStringFields.append("summarizeBytes");
							suffix = ")";
						} else if(cls.equals(String.class)) {
							toStringFields.append("maskString");
							suffix = ")";
						} else {
							toStringFields.append("maskString(String.valueOf");
							suffix = "))";
						}
						toStringFields.append('(');
					}
				} else if(card) {
					imports.add(MessageResponseUtils.class);
					if(multiple) {
						String dataTypeName = ConvertUtils.convertToWrapperClass(cls).getSimpleName();
						toStringFields.append("'[');\n\t\tfor(").append(dataTypeName).append(" item : ");
						StringBuilder sb = new StringBuilder();
						sb.append(")\n\t\t\tsb.append(");
						sb.append("MessageResponseUtils.");
						if(cls.equals(byte[].class)) {
							sb.append("maskFully(item)");
						} else if(cls.equals(String.class)) {
							sb.append("maskTrackData(item)");
						} else {
							sb.append("maskTrackData(String.valueOf(item))");
						}
						sb.append(").append(\", \");\n\t\tsb.setLength(sb.length() - 1);\n\t\tsb.append(']'");
						suffix = sb.toString();
					} else {
						toStringFields.append("MessageResponseUtils.");
						if(cls.equals(byte[].class)) {
							toStringFields.append("maskFully");
							suffix = ")";
						} else if(cls.equals(String.class)) {
							toStringFields.append("maskTrackData");
							suffix = ")";
						} else {
							toStringFields.append("maskTrackData(String.valueOf");
							suffix = "))";
						}
						toStringFields.append('(');
					}
				} else if(cls.equals(byte[].class)) {
					toStringFields.append("MessageDataUtils.toSummary(");
					suffix = ", 256)";
				} else if(cls.equals(byte.class)) {
					suffix = " & 0xFF";
				} else if(cls.equals(Calendar.class)) {
					toStringFields.append("MessageDataUtils.toString(");
					suffix = ")";
				} else
					suffix = "";
				toStringFields.append(getMethodPrefix).append(methodSuffix).append("()").append(suffix).append(')');
				String setMethod;
				String getMethod;
				if(multiple) {
					readFields.append("\t\t").append(fieldName).append(".clear();\n");
					setMethod = new StringBuilder().append("for(int i = 0, n = ProcessingUtils.readByteInt(data); i < n; i++)\n\t\t\t").append(fieldName).append(".add").toString();
					String dataTypeName = ConvertUtils.convertToWrapperClass(cls).getSimpleName();
					writeFields.append("\t\tProcessingUtils.writeByteInt(reply, ").append(getMethodPrefix).append(methodSuffix).append("().size());\n\t\tfor(")
						.append(dataTypeName).append(" item : ").append(getMethodPrefix).append(methodSuffix).append("())\n");
					getMethod = "item";
				} else {
					setMethod = "set" + methodSuffix;
					getMethod = new StringBuilder().append(getMethodPrefix).append(methodSuffix).append("()").toString();
				}
				if(Enum.class.isAssignableFrom(cls)) {
					Method valueMethod = ReflectionUtils.findMethod(cls, "getValue", ReflectionUtils.ZERO_PARAMS);
					if(!Modifier.isStatic(valueMethod.getModifiers())) {
						getMethod += ".getValue()";
						cls = valueMethod.getReturnType();
					}
				}

				String bufferOpSuffix = bufferOpSuffixes.get(cls);
				if(bufferOpSuffix != null) {
					readFields.append("\t\t").append(setMethod).append("(data.get").append(bufferOpSuffix).append("());\n");
					writeFields.append("\t\treply.put").append(bufferOpSuffix).append('(');
					appendValue(writeFields, cls, sensitive, card, getMethod, imports);
					writeFields.append(");\n");
				} else {
					String utilsOpSuffix = utilsOpSuffixes.get(cls);
					if(utilsOpSuffix != null) {
						imports.add(ProcessingUtils.class);
						readFields.append("\t\t").append(setMethod).append("(ProcessingUtils.read").append(utilsOpSuffix).append("(data));\n");
						writeFields.append("\t\tProcessingUtils.write").append(utilsOpSuffix).append("(reply, ");
						appendValue(writeFields, cls, sensitive, card, getMethod, imports).append(");\n");
					} else if(cls.equals(String.class)) {
						imports.add(ProcessingUtils.class);
						readFields.append("\t\t").append(setMethod).append("(ProcessingUtils.readLongString(data, charset));\n");
						writeFields.append("\t\tProcessingUtils.writeLongString(reply, ");
						appendValue(writeFields, cls, sensitive, card, getMethod, imports).append(", charset);\n");
					} else if(cls.equals(boolean.class)) {
						readFields.append("\t\t").append(setMethod).append("(data.get() != 0);\n");
						writeFields.append("\t\treply.put(").append(getMethod).append("() ? 1 : 0);\n");
					} else if(Date.class.isAssignableFrom(cls)) {
						imports.add(ProcessingUtils.class);
						readFields.append("\t\t").append(setMethod).append("(ProcessingUtils.readDate(data));\n");
						writeFields.append("\t\tProcessingUtils.writeDate(reply, ").append(getMethod).append("());\n");
					} else if(Number.class.isAssignableFrom(cls)) {
						imports.add(ProcessingUtils.class);
						imports.add(ConvertUtils.class);
						imports.add(ConvertException.class);
						readFields.append("\t\ttry {\n\t\t\t").append(setMethod).append("(ConvertUtils.convert(").append(cls.getSimpleName()).append(".class, ProcessingUtils.readShortString(data, charset)));\n\t\t} catch(ConvertException e) {\n\t\t\tthrow createParseException(\"Could not convert '").append(fieldName).append("' to a number\", data.position(), e);\n\t\t}\n");
						writeFields.append("\t\tProcessingUtils.writeShortString(reply, ").append(getMethod).append(" == null ? null : ");
						appendValue(writeFields, cls, sensitive, card, getMethod, imports).append(".toString(), charset);\n");
					} else {
						imports.add(ProcessingUtils.class);
						imports.add(ConvertUtils.class);
						imports.add(ConvertException.class);
						readFields.append("\t\ttry {\n\t\t\t").append(setMethod).append("(ConvertUtils.convert(").append(cls.getSimpleName()).append(".class, ProcessingUtils.readLongString(data, charset)));\n\t\t} catch(ConvertException e) {\n\t\t\tthrow createParseException(\"Could not convert '").append(fieldName).append("' to a number\", data.position(), e);\n\t\t}\n");
						writeFields.append("\t\tProcessingUtils.writeLongString(reply, ").append(getMethod).append(" == null ? null : ");
						appendValue(writeFields, cls, sensitive, card, getMethod, imports).append(".toString(), charset);\n");
					}
				}
			}
			System.out.print(cls.getName());
		} else {
			int dims = type.getDimensions().length() / 2;
			if(dims > 0) {
				for(int i = 0; i < dims; i++)
					System.out.print('[');
				Type subType = symbolTable.getType(type.getComponentType());
				handleParameter(symbolTable, name, subType, true, indent, defineFields, writeFields, readFields, toStringFields, publishFields, imports, messageType, null, indent == 1 ? parentCls : null);
			} else {
				System.out.println('{');
				StringBuilder subWriteFields;
				StringBuilder subReadFields;
				StringBuilder subToStringFields;
				StringBuilder subDefineFields;
				StringBuilder subPublishFields;
				if(multiple) {
					imports.add(List.class);
					imports.add(ArrayList.class);
					imports.add(Collections.class);
					String itemName = StringUtils.singular(fieldName);
					String methodBase = StringUtils.capitalizeFirst(itemName);
					String dataTypeName = methodBase;
					imports.add(Byteable.class);
					defineFields.append("\tprotected final List<").append(dataTypeName).append("> ").append(fieldName).append(" = new ArrayList<").append(dataTypeName).append(">();\n");
					defineFields.append("\tprotected final List<").append(dataTypeName).append("> ").append(fieldName).append("Unmod = Collections.unmodifiableList(").append(dataTypeName).append(");\n");
					defineFields.append("\tpublic class ").append(dataTypeName).append(" implements Byteable {\n");

					readFields.append("\t\t").append(fieldName).append(".clear();\n\t\tfor(int i = 0, n = ProcessingUtils.readByteInt(data); i < n; i++)\n\t\t\tadd").append(methodBase).append(".readData(data);");
					writeFields.append("\t\tProcessingUtils.writeByteInt(reply, get").append(methodSuffix).append("().size());\n\t\tfor(")
						.append(dataTypeName).append(" item : get").append(methodSuffix).append("())\n\t\t\titem.writeData(reply, maskSensitiveData);\n");
					
					publishFields.append("\tpublic List<").append(dataTypeName).append("> get").append(methodSuffix).append("() {\n\t\treturn ").append(fieldName).append("Unmod;\n\t}\n\n");
					publishFields.append("\tpublic ").append(dataTypeName).append(" add").append(methodBase)
						.append("() {\n\t\t").append(dataTypeName).append(' ').append(itemName)
						.append(" = new ").append(dataTypeName).append('(').append(fieldName).append(".size());\n\t\t")
						.append(fieldName).append(".add(").append(itemName).append(");\n\t\treturn ").append(itemName).append(";\n\t}\n\n");

					toStringFields.append("\n\t\t\t.append(\", ").append(fieldName).append("=\").append(get").append(methodSuffix).append("())");

					subWriteFields = new StringBuilder();
					subReadFields = new StringBuilder();
					subToStringFields = new StringBuilder();
					subDefineFields = defineFields;
					subPublishFields = new StringBuilder();
				} else {
					subWriteFields = writeFields;
					subReadFields = readFields;
					subToStringFields = toStringFields;
					subDefineFields = defineFields;
					subPublishFields = publishFields;
					if(indent == 1) {
						// create delegate
						StringBuilder sb = new StringBuilder();
						String ns = type.getQName().getNamespaceURI();
						if(!StringUtils.isBlank(ns)) {
							String[] h;
							try {
								h = StringUtils.split(new URL(ns).getHost(), '.');
								for(int i = h.length - 1; i >= 0; i--)
									sb.append(h[i]).append('.');
							} catch(MalformedURLException e) {
								e.printStackTrace();
							}

						}
						sb.append(type.getQName().getLocalPart());
						try {
							Class<?> delegateCls = Class.forName(sb.toString());
							imports.add(delegateCls);
							defineFields.append("\tprotected final ").append(delegateCls.getSimpleName()).append(" delegate = new ").append(delegateCls.getSimpleName()).append("();\n\n");
							publishFields.append("\tpublic ").append(delegateCls.getSimpleName()).append(" getDelegate() {\n\t\treturn delegate;\n\t}\n\n");
							delegates.put(delegateCls, "delegate");
						} catch(ClassNotFoundException e) {
							e.printStackTrace();
						}
					}
				}
				
				int a = 0;
				for(TypeEntry curr = type; curr != null; curr = SchemaUtils.getComplexElementExtensionBase(curr.getNode(), symbolTable)) {
					for(Object ced : SchemaUtils.getContainedElementDeclarations(curr.getNode(), symbolTable)) {
						ElementDecl elem = (ElementDecl) ced;
						for(int i = 0; i <= indent; i++)
							System.out.print('\t');
						QName qname = elem.getQName();
						String subname;
						if(qname != null && !StringUtils.isBlank(qname.getLocalPart())) {
							int pos = qname.getLocalPart().lastIndexOf(SymbolTable.ANON_TOKEN);
							if(pos >= 0)
								subname = qname.getLocalPart().substring(pos + SymbolTable.ANON_TOKEN.length());
							else
								subname = qname.getLocalPart();
						} else {
							subname = "arg" + a++;
						}
						System.out.print(subname);
						System.out.print(' ');
						if(indent > 1 && !StringUtils.isBlank(name))
							subname = name + "/" + subname;
						handleParameter(symbolTable, subname, elem.getType(), false, indent + 1, subDefineFields, subWriteFields, subReadFields, subToStringFields, subPublishFields, imports, messageType, delegates, indent == 1 ? parentCls : null);
						System.out.println();
					}
				}
				if(multiple) {
					defineFields.append("\t\tpublic void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {\n");
					defineFields.append(subWriteFields);
					defineFields.append("\t\t}\n\n\t\tpublic void readData(ByteBuffer data) throws ParseException, BufferUnderflowException {\n");
					defineFields.append(subReadFields);
					defineFields.append("\t\t}\n\n");
					defineFields.append(subPublishFields);
					defineFields.append("\t\tpublic String toString() {\n\t\t\tStringBuilder sb = new StringBuilder();\n\t\t\tsb.append('[')");
					defineFields.append(subToStringFields);
					defineFields.append(".append('[');\n\t\t\treturn sb.toString();\n\t\t}\n\t}\n\n");
				}
				for(int i = 0; i < indent; i++)
					System.out.print('\t');
				System.out.print('}');
			}
		}
	}
}
