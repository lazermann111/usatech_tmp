import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import simple.lang.EnumValueLookup;
import simple.lang.InvalidByteValueException;
import simple.text.StringUtils;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.constants.EventDetailType;
import com.usatech.layers.common.constants.MessageType;

/**
 *
 */

/**
 * @author Brian S. Krug
 *
 */
public class ConstantsTest {

	/**
	 * @param args
	 * @throws InvalidByteValueException
	 */
	public static void main(String[] args) throws Exception {
		System.setProperty("simple.io.logging.bridge", "simple.io.logging.SimpleBridge");
		//testLiteral();
		//testEventDetailTypes();
		testConstantTypes();
	}

	public static void testMessageTypes() throws InvalidByteValueException {
		for(int i = 0xC0; i < 0xCC; i++) {
			MessageType mt = MessageType.getByValue((byte)i);
			System.out.print(mt.getHex());
			System.out.print('\t');
			System.out.print(mt.name());
			System.out.print('\t');
			System.out.println(mt.getDescription());
		}

	}
	public static void testEventDetailTypes() throws InvalidByteValueException {
		for(int i = 1; i < 4; i++) {
			EventDetailType mt = EventDetailType.getByValue((byte)i);
			System.out.print(mt.getValue());
			System.out.print('\t');
			System.out.print(mt.name());
			System.out.print('\t');
			System.out.println(mt.ordinal());
		}

	}
	public static void testConstantTypes() throws IOException, URISyntaxException, ClassNotFoundException {
		final String pkgName = "com.usatech.layers.common.constants";
		URL url = MessageType.class.getClassLoader().getResource(pkgName.replace('.', '/'));
		File dir = new File(url.toURI());
		String[] files = dir.list(new FilenameFilter() {
			/**
			 * @see java.io.FilenameFilter#accept(java.io.File, java.lang.String)
			 */
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".class") && name.indexOf('$') < 0;
			}
		});
		for(String s : files) {
			String cn = pkgName + '.' + s.substring(0, s.length() - 6);
			System.out.println("Loading '" + cn + "'...");
			Class<?> type = Class.forName(cn);
			if(type.isEnum()) {
				Enum<?>[] values = EnumValueLookup.getEnumConstants(type.asSubclass(Enum.class));
				System.out.print("Loaded {");
				for(Enum<?> en : values) {
					System.out.print(" " + en.toString() + ",");
				}
				System.out.println(" }");
			}
		}

	}
	/*public static void testPropertyListParser() {
		String[] pls = new String[] {"12", "12|35-50|57|89|105|110-115", "16-20", "21-7", "20|22|11|7", "25|25|20-30"};

		for(String p : pls) {
			System.out.println("Parsing property index list: `" + p + "'");
			Set<Integer> set;
			try {
				set = ProcessingUtils.parsePropertyList(p);
			} catch(ParseException e) {
				System.out.println("Parse Exception");
				e.printStackTrace();
				continue;
			}
			System.out.println("Got: " + set);
		}
	}*/
	public static void testLiteral() {
		System.out.println("Session Attribute Prefix: " + StringUtils.toHex(ProcessingConstants.SESSION_ATTRIBUTE_PREFIX.getBytes()));
	}
}
