

import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.Writer;
import java.math.BigInteger;
import java.sql.Connection;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.BasicDeviceInfo;
import com.usatech.layers.common.DeviceInfo;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.ProcessingUtils.PropertyValueHandler;
import com.usatech.layers.common.constants.CRCType;
import com.usatech.layers.common.constants.CRCType.Calculator;
import com.usatech.layers.common.constants.CardType;
import com.usatech.layers.common.constants.DeniedReason;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.PaymentActionType;
import com.usatech.layers.common.constants.TLVTag;
import com.usatech.layers.common.messagedata.KioskLineItem;
import com.usatech.layers.common.messagedata.LineItem;
import com.usatech.layers.common.messagedata.MessageDataUtils;
import com.usatech.layers.common.util.CampaignUtils;

import simple.db.BasicDataSourceFactory;
import simple.db.DataLayerMgr;
import simple.db.config.ConfigLoader;
import simple.io.BufferStream;
import simple.io.ByteArrayUtils;
import simple.io.HeapBufferStream;
import simple.io.IOUtils;
import simple.io.Log;
import simple.io.TLVParser;
import simple.lang.InvalidValueException;
import simple.swt.UIUtils;
import simple.test.UnitTest;
import simple.text.StringUtils;

public class LayersCommonTest extends UnitTest {
	public final static String OPER_DEV = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb011.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=usadev03.world)))";
	public final static String OPER_INT = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=intdb011.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=usadev02.world)))";
	public final static String OPER_ECC = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=eccscan01.usatech.com)(PORT=1525))(ADDRESS=(PROTOCOL=TCP)(HOST=eccscan02.usatech.com)(PORT=1525)))(CONNECT_DATA=(SERVICE_NAME=usaecc_db.world)))";
	public final static String OPER_PROD_31 = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.120)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.121)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.122)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.220)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.221)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.222)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)))";

	@Before
	public void setUp() throws Exception {
		setupLog();
	}

	@Test
	public void testBuildPropertyList() throws Exception {
		DeviceInfo deviceInfo = new BasicDeviceInfo("TD001144") {
			protected void unlock() {
			}

			protected void lock() {
			}
		};
		deviceInfo.setDeviceSerialCd("VJ000000004");
		deviceInfo.setDeviceType(DeviceType.EDGE);
		Long messageId = System.currentTimeMillis();
		String propertyFileContents = "1201!\n1205!\n";
		
		BasicDataSourceFactory bdsf = new BasicDataSourceFactory();
		String username = "SYSTEM"; // System.getProperty("dbUserName", System.getProperty("user.name").toUpperCase());
		String password = UIUtils.promptForPassword("Enter the OPER password for " + username, "Database Login", null);
		bdsf.addDataSource("OPER", "oracle.jdbc.driver.OracleDriver", OPER_INT, username, password);
		DataLayerMgr.setDataSourceFactory(bdsf);
		ConfigLoader.loadConfig("file:../AppLayer/src/applayer-data-layer.xml", 0L);
		Map<Integer, String> propertyValueList = new LinkedHashMap<Integer, String>();
		try (Connection conn = bdsf.getDataSource("OPER").getConnection()) {
			final Set<Integer> propertyList = new HashSet<Integer>();
			ProcessingUtils.parsePropertyList(new StringReader(propertyFileContents), deviceInfo.getDeviceCharset(), new PropertyValueHandler<RuntimeException>() {
				public void handleProperty(int propertyIndex, String propertyValue) throws RuntimeException {
					propertyList.add(propertyIndex);
				}
			});
			// get current values
			AppLayerUtils.populatePropertyValueList(deviceInfo, messageId, propertyList, propertyValueList, true, conn);
		}
		// rewrite blob
		BufferStream bufferStream = new HeapBufferStream(false);
		Writer writer = new OutputStreamWriter(bufferStream.getOutputStream(), deviceInfo.getDeviceCharset());
		MessageDataUtils.appendEfficientPropertyValueList(writer, propertyValueList, false);
		writer.flush();
		writer.close();
		log.info("Got:\n'" + IOUtils.readFully(bufferStream.getInputStream()) + "'");
	}

	@Test
	public void testCardTypes() {
		for(EntryType entryType : EntryType.values()) {
			for(PaymentActionType paymentActionType : PaymentActionType.values()) {
				List<CardType> cts = CardType.findAll(entryType, paymentActionType);
				log.info(entryType + " " + paymentActionType + ": " + (cts == null ? "NONE" : Arrays.toString(cts.toArray())));
			}
		}
	}

	@Test
	public void testTLVTags() {
		String[] tags = new String[] {
 "5F34", "0102", "9F0F", "DF8167", "DF8169", "DF816F", "DF23", "DF22", "4F", "DF8168", "DF816A", "DF69", "DF8201" 
		};
		for(String tag : tags) {
			byte[] source = ByteArrayUtils.fromHex(tag);	
			int offset = 0;
			int start = 0;
			int limit = source.length;
			if((source[offset++] & 0x1F) == 0x1F)
				while(true) {
					if(offset >= limit)
						log.warn("Unfinished tag '" + StringUtils.toHex(source, start, limit) + "' at position " + (start + 1) + "; need at least one more byte");
					if((source[offset++] & 0x80) != 0x80)
						break;
				}
			TLVTag en;
			try {
				en = TLVTag.getByValue(source);
				log.info("Tag '" + StringUtils.toHex(source, start, limit) + "' is " + en);
			} catch(InvalidValueException e) {
				log.warn("Tag '" + StringUtils.toHex(source, start, limit) + "' was NOT found!");
			}
			if(offset != limit)
				log.warn("Tag '" + StringUtils.toHex(source, start, limit) + "' is invalid");
			else
				log.info("Tag '" + StringUtils.toHex(source, start, limit) + "' checks out okay");
		}
		TLVTag[] tagEnums = TLVTag.values();
		for(TLVTag tag : tagEnums) {
			byte[] source = tag.getValue();
			int offset = 0;
			int start = 0;
			int limit = source.length;
			if((source[offset++] & 0x1F) == 0x1F)
				while(true) {
					if(offset >= limit)
						log.warn("Unfinished tag '" + StringUtils.toHex(source, start, limit) + "' at position " + (start + 1) + "; need at least one more byte");
					if((source[offset++] & 0x80) != 0x80)
						break;
				}
			if(offset != limit)
				log.warn("Tag '" + StringUtils.toHex(source, start, limit) + "' is invalid");
			else
				log.info("Tag '" + StringUtils.toHex(source, start, limit) + "' checks out okay");
		}
	}

	@Test
	public void testTLVTag() {
		TLVParser p = new TLVParser();
		log.info(p.toString());
	}
	@Test
	public void testToMajorCurrency() {
		int minorCurrencyFactor = 100;
		int[] amounts = new int[] { 50, 100, 10000, 2000, 505 };
		for(int a : amounts) {
			System.out.println("Minor: " + a + "; Major: " + InteractionUtils.toMajorCurrency(BigInteger.valueOf(a), minorCurrencyFactor).toString());
		}

	}

	@Test
	public void testCRCSum() throws Exception {
		Calculator calc = CRCType.CRC16SUM.newCalculator();
		byte[][] samples = new byte[][] {
				{0x00, 0x01},
				{(byte)0xFF, (byte)0x9D},
				{(byte)0xFF, (byte)0xFF},
				{(byte)0x55, (byte)0x55},
				{(byte)0xA5, (byte)0xA5},
				{(byte)0xAA, (byte)0xAA},
				{(byte)0xAB, (byte)0xCD}				
		};
		for(byte[] sample : samples) {
			//log.info("Calculating sum crc for 100 occurances of 0x" + StringUtils.toHex(sample));
			BigInteger n = BigInteger.valueOf((0xFF & sample[1]) + 256 * (0xFF & sample[0])).multiply(BigInteger.valueOf(100));
			log.info("0x" + StringUtils.toHex(sample) + " * 100 = " + n + " (0x" + StringUtils.toHex(n.shortValue()) + ")");
			for(int i = 0; i < 100; i++)
				calc.update(sample);
			byte[] result = calc.getValue();
			calc.reset();
			log.info("Sum crc for 100 occurances of 0x" + StringUtils.toHex(sample) + " = 0x" + StringUtils.toHex(result)); 			
		}
	}
	
	@Test
	public void testCRCSum1() throws Exception {
		Calculator calc = CRCType.CRC16SUM.newCalculator();
		byte[][] samples = new byte[][] {
				{0x01},
				{(byte)0x9D},
				{(byte)0xFF},
				{(byte)0x55},
				{(byte)0xA5},
				{(byte)0xAA},
				{(byte)0xCD}				
		};
		for(byte[] sample : samples) {
			//log.info("Calculating sum crc for 100 occurances of 0x" + StringUtils.toHex(sample));
			//BigInteger n = BigInteger.valueOf((0xFF & sample[1]) + 256 * (0xFF & sample[0])).multiply(BigInteger.valueOf(100));
			//log.info("0x" + StringUtils.toHex(sample) + " * 100 = " + n + " (0x" + StringUtils.toHex(n.shortValue()) + ")");
			for(int i = 0; i < 100; i++)
				calc.update(sample);
			byte[] result = calc.getValue();
			calc.reset();
			log.info("Sum crc for 100 occurances of 0x" + StringUtils.toHex(sample) + " = 0x" + StringUtils.toHex(result)); 			
		}
	}
	
	@Test
	public void testCRCSum2() throws Exception {
		byte[][] samples = new byte[][] {/*
				{0x00, 0x01},
				{(byte)0xFF, (byte)0x9D},
				{(byte)0xFF, (byte)0xFF},
				{(byte)0x55, (byte)0x55},
				{(byte)0xA5, (byte)0xA5},
				{(byte)0xAA, (byte)0xAA},*/
				{(byte)0xAB, (byte)0xCD}				
		};
		for(byte[] sample : samples) {
			//log.info("Calculating sum crc for 100 occurances of 0x" + StringUtils.toHex(sample));
			BigInteger n = BigInteger.valueOf((0xFF & sample[1]) + 256 * (0xFF & sample[0]));
			int crc = 0;
			for(int i = 1; i <= 100; i++) {
				BigInteger bi = n.multiply(BigInteger.valueOf(i));
				crc = (crc + (0xFF & sample[0]));
				crc = (crc + (0xFF & sample[1]));

				log.info("0x" + StringUtils.toHex(sample) + " * " + i + " = " + bi + " (0x" + StringUtils.toHex(bi.shortValue()) + "); crc = " + crc + " (0x" + StringUtils.toHex(crc) + ")");
			}			
		}
		/*
		 * 	String[] tracks = {
	
	0x0001                  0x0064
	0xFF9D                 0xD954
	0xFFFF                  0xFF9C
	0x5555                  0x5534
	0xA5A5                 0xB474
	0xAAAA               0xAA68
	0xABCD                0x1C14
		}
		 */
	}
	
	@Test
	public void testRecurSchedFormatting() throws Exception {
		String[] scheds = new String[] {
			"D|6:00-7:00,18:30-22:45",
			"W:2,4,6|",
			"S:3/14/2014,6/14/2014|11:00-13:00",
			"M:3,12,28",
		};
		for(String s : scheds) {
			log.info("Schedule '" + s + "' translates to:\n" + CampaignUtils.readRecurSchedule(s));
		}

	}
	@Test
	public void testCleanTrack() throws Exception {
		String[] tracks = {
				"639621*************=*************",
				";639621*************=*************?",
				"6396210123456789012=3456789012345",
				";6396210123456789012=3456789012345?",
				
		};
		for(String t : tracks) {
			log.info("Track '" + t + "' cleaned = " + MessageResponseUtils.cleanTrack(t));
		}
	}

	@Test
	public void testTranDetailsParsing() throws Exception {
		String tranDetails = "A0|200|641|1|ePort Mobile Purchase|611|38|1||406|96|1|";
		List<? extends LineItem> lineItems = KioskLineItem.parseLineItems(tranDetails);
		log.info(lineItems.toString());
	}
	@Test
	public void printDeniedReasonTranslations() throws Exception {
		String[] authorities = new String[] {
				"aramark",
				"blackboard",
				"interchange",
				"internal",
				"prepaid",
				"permission",
				"posgateway",
				"sprout",
				"vzm2p",
		};
		DeniedReason[] deniedReasons = DeniedReason.values();
		String[] deniedReasonNames = new String[deniedReasons.length];
		for(int i = 0; i < deniedReasons.length; i++)
			deniedReasonNames[i] = deniedReasons[i].toString();

		Arrays.sort(deniedReasonNames);
		StringBuilder sb = new StringBuilder("Translations=");
		for(String dr : deniedReasonNames)
			for(String a : authorities)
				sb.append("\nclient.message.auth.").append(a).append(".denied-").append(dr).append('=');

		log.info(sb.toString());
	}
	
	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
}
