import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import com.usatech.layers.common.BCDInt;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.SessionCache;
import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.messagedata.MessageDataUtils;
import com.usatech.layers.common.messagedata.MessageData_C2;
import com.usatech.layers.common.messagedata.MessageData_C2.GenericTracksCardReader;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.io.ByteArrayUtils;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;

/**
 *
 */

/**
 * @author Brian S. Krug
 *
 */
public class ProcessingUtilsTest {

	/**
	 * @param args
	 * @throws ServiceException 
	 */
	public static void main(String[] args) throws ServiceException {
		testFormatAuthResponseMessage();
		// testFormatAuthResponseMessage();
		// System.out.print(ProcessingUtils.readLongInt(ByteBuffer.wrap(ByteArrayUtils.fromHex("521CAAE8"))));
		// testNumbers2();
		/*
		System.out.println("Call ID: 166107571");
		testGetBCDTime(1324349896959L);
		testGetBCDTime(1324349896928L);
		System.out.println("Call ID: 166107122");
		testGetBCDTime(1324306728960L);
		System.out.println("Call ID: 166106714");
		testGetBCDTime(1324263496960L);*/
	}

	public static void testCardNumber() {
		String card = "228102093:";
		String cn = MessageResponseUtils.getCardNumber(card);
		System.out.println("Track '" + card + "' = '" + cn + "'");
	}

	public static void testFormatAuthResponseMessage() {
		// DS=02\nNL=02\nP1= This Purchase\ris Free up to $5\r\nP2=  Compliments\r    of Isis!\r\n
		// P1=%d More Tap%s for\r Free Purchase\r\n
		// P1=   Next Isis\rPurchase is Free\r\n
		String[] msgs = new String[] {
				// " This Purchase is Free up to $5   Compliments     of Isis!",
				// "2 More Taps for  Free Purchase",
				// "   Next Isis Purchase is Free",
				"This Purchase\nis Fee up to $5\nCompliments\nof Isis",
				"This Purchase\nis Free up to $5\nCompliments\nof Softcard",/*			
				"This Purchase is Free up to $5 Compliments of Isis!",
				"This Purchase\nis Free up to $5\nCompliments\nof Isis!",
				"2 More Taps for Free Purchase",
				"1 More Tap for Free Purchase",
				"Next Isis Purchase is Free",
				"Happy Harry's Jappy Joy Joy",
				"Thank you for using your marvelous MORE card! Go to getmore.usatech.com for more details on how to save big bucks!",
				"Small",
				"   SPACES   \t TABS \t   AND       OTHERS   "*/
		};
		for(String msg : msgs) {
			System.out.println("Original:   " + msg);
			System.out.println("Egde:       " + StringUtils.preparePrintable(MessageDataUtils.formatForDisplay(msg, 16, 2, Justification.CENTER)));
			System.out.println("Right Just: " + StringUtils.preparePrintable(MessageDataUtils.formatForDisplay(msg, 16, 2, Justification.RIGHT)));
			System.out.println("Left Just:  " + StringUtils.preparePrintable(MessageDataUtils.formatForDisplay(msg, 16, 2, Justification.LEFT)));
			System.out.println("Kiosk:      " + StringUtils.preparePrintable(StringUtils.prepareDisplayable(msg, true)));
			System.out.println("=======================================================================================");
		}
	}
	public static void testGetBCDTime(long time) {
		ByteBuffer buffer = ByteBuffer.allocate(7);
		ProcessingUtils.writeByteTimestamp(buffer, time);
		System.out.print("Time: ");
		System.out.println(time);
		System.out.print("Date: ");
		System.out.println(new Date(time));
		System.out.print("Timestamp Bytes: ");
		System.out.println(StringUtils.toHex(buffer, 0, 7));
		System.out.println();
	}
	public static void testNetLayerSessionCache() throws ServiceException {
		SessionCache.getOrCreateNetLayerId(Collections.singletonMap("netLayerDesc", (Object) "NetworkLayer at usanet23:14108"));
	}

	public static void testMaskBytes() {
		MessageData_C2 dataC2 = new MessageData_C2();
		dataC2.setAmount(new BigDecimal(200));
		dataC2.setCardReaderType(CardReaderType.GENERIC);
		dataC2.setEntryType(EntryType.SWIPE);
		dataC2.setMessageId(System.currentTimeMillis() / 1000);
		dataC2.setTransactionId(System.currentTimeMillis() / 1000);
		GenericTracksCardReader gcr = (GenericTracksCardReader) dataC2.getCardReader();
		gcr.setAccountData2("4121630071281885=13121543213961456789?");
		ByteBuffer buffer = ByteBuffer.allocate(1100);
		dataC2.writeData(buffer, false);
		buffer.flip();
		byte[] array = new byte[buffer.remaining()];
		buffer.get(array);
		System.out.print(StringUtils.toHex(array));
		System.out.print(" = ");
		System.out.print(StringUtils.toHex(MessageResponseUtils.maskAnyCardNumbers(array)));
		System.out.println();		
	}
		
	public static void testNumbers() {
		int[] tests = new int[] {1, 5, 16, 0x12345678, 255, 16980, 43214234, 45161672, -3};
		for(int i = 0; i < tests.length; i++) {
			System.out.print(tests[i]);
			System.out.print('=');
			String hex = ProcessingUtils.decimalToHex(tests[i], 4);
			System.out.print(hex);
			System.out.print("; bcd=");
			byte[] bytes = new byte[4];
			try {
				ProcessingUtils.writeBCDInt(ByteBuffer.wrap(bytes), new BCDInt(tests[i], hex));
				System.out.print(Arrays.toString(bytes));
			} catch(IllegalArgumentException e) {
				System.out.print("ERROR: " + e.getMessage());
			}
			System.out.print("; bcd-from-bytes=");
			ByteArrayUtils.writeUnsignedLong(bytes, 0, tests[i]);
			try {
				System.out.print(ProcessingUtils.readBCDInt(ByteBuffer.wrap(bytes)));
			} catch(NumberFormatException e) {
				System.out.print("ERROR: " + e.getMessage());
			}
			System.out.println();
		}

	}

	public static void testNumbers2() {
		int[] tests = new int[] { 0x01, 0x09, 0x10, 0x23, 0x67, 0x99 };
		for(int i = 0; i < tests.length; i++) {
			System.out.print(tests[i]);
			System.out.print('=');
			String hex = ProcessingUtils.decimalToHex(tests[i], 4);
			System.out.print(hex);
			System.out.print("; bcd=");
			byte[] bytes = new byte[1];
			bytes[0] = (byte) tests[i];

			int bcd = ProcessingUtils.readByteAsBCD(ByteBuffer.wrap(bytes));
			System.out.print(bcd);
			System.out.println();
		}

	}

	public static void testReadTimestamp() {
		int[] offsetQuarters = new int[] {-48, 0, -16, -20, -24, 13 };
		ByteBuffer buffer = ByteBuffer.wrap(new byte[5]);
		ProcessingUtils.writeLongInt(buffer, System.currentTimeMillis() / 1000);
		for(int oq : offsetQuarters) {
			buffer.position(4);
			buffer.put((byte)oq);
			buffer.flip();
			Calendar cal = ProcessingUtils.readTimestamp(buffer);
			System.out.println("OffsetQuarters=" + oq + "; time=" + ConvertUtils.formatObject(cal, "DATE:MM/dd/yyyy HH:mm:ss z"));
		}

	}
}
