import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.junit.Test;

import simple.io.Log;

import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.sprout.SproutAccountResponse;
import com.usatech.layers.common.sprout.SproutAccountTransactions;
import com.usatech.layers.common.sprout.SproutExcpetion;
import com.usatech.layers.common.sprout.SproutLoadDetail;
import com.usatech.layers.common.sprout.SproutLoadOrRefundResponse;
import com.usatech.layers.common.sprout.SproutPurchaseDetail;
import com.usatech.layers.common.sprout.SproutPurchaseItem;
import com.usatech.layers.common.sprout.SproutPurchaseResponse;
import com.usatech.layers.common.sprout.SproutReversalDetail;
import com.usatech.layers.common.sprout.SproutReversalResponse;
import com.usatech.layers.common.sprout.SproutTransaction;
import com.usatech.layers.common.sprout.SproutTransactionType;
import com.usatech.layers.common.sprout.SproutUtils;


public class SproutUtilsTest {
	private static final Log log = Log.getLog();
	public static String testMerchantId="7est7ing";
	
	protected void init(){
		SproutUtils.setSproutUrl("https://bytevampire-cert.herokuapp.com");
		SproutUtils.setSproutAuthorizationToken("cbf35bb7eb1fe7eacfbad3ffe313eebadc00d35a615ce372306bd8a6a0fab6df");
		SproutUtils.setHostNameVerifier(new NoopHostnameVerifier());
	}
	
	@Test
	public void testGetAccountExists() throws Exception {
		init();
		String sproutCardNumber="6277229030281164";
		SproutUtils sproutUtils=new SproutUtils();
		SproutAccountResponse account=sproutUtils.getAccount(sproutCardNumber);
		log.info(account);
		log.info(MessageResponseUtils.maskAnyCardNumbers(account.toString()));
		assert(account.getAccount_id().equals("30000000338925"));
	}
	
	@Test
	public void testGetAccountNotExists() throws Exception {
		init();
		//String sproutCardNumber="62772290302811641";
		String sproutCardNumber="6277229030281081";
		SproutUtils sproutUtils=new SproutUtils();
		try{
		SproutAccountResponse account=sproutUtils.getAccount(sproutCardNumber);
		}catch(SproutExcpetion e){
			assert(e.getHttpStatus()==404);
		}
	}
	
	@Test
	public void testCreateAccount() throws Exception {
		init();
		//String sproutCardNumber="6277229030281222";
		//String sproutCardNumber="6277229030281842";
		String sproutCardNumber="6277229030281081";
		SproutUtils sproutUtils=new SproutUtils();
		int httpStatus=404;
		try{
			SproutAccountResponse account=sproutUtils.getAccount(sproutCardNumber);
			log.info(account);
			httpStatus=200;
		}catch(SproutExcpetion e){
			httpStatus=e.getHttpStatus();
		}
		if(httpStatus==404){
			log.info("not exists, create");
			try{
				SproutAccountResponse account=sproutUtils.createAccout(sproutCardNumber, "default", 999999, "merchant_8@usatech.com");
				log.info(account);
			}catch(SproutExcpetion e){
				httpStatus=e.getHttpStatus();
				log.info(httpStatus+" "+e.getMessage());
			}
		}
	}

	@Test
	public void testGetAccountTransactions() throws Exception {
		init();
		String accountId="30000000337513";
		SproutUtils sproutUtils=new SproutUtils();
		SproutAccountTransactions accountTrans=sproutUtils.getAccountTransactions(accountId,"2015-06");
		log.info(accountTrans);
	}
	
	@Test
	public void testPurchase() throws Exception {
		init();
		String sproutCardNumber="6277229030431041";
		SproutUtils sproutUtils=new SproutUtils();
		int httpStatus=404;
		String external_reference_id=String.valueOf(System.currentTimeMillis());
		String vampire_pos_id="5091188488";
		List<SproutPurchaseItem> items=new ArrayList<SproutPurchaseItem>();
		
		SproutPurchaseItem item=new SproutPurchaseItem();
		item.setCoil_name("coke");
		item.setPrice(new BigDecimal("1.00"));
		items.add(item);
		try{
			SproutPurchaseResponse purchase=sproutUtils.purchase(sproutCardNumber, external_reference_id, vampire_pos_id, items);
			log.info(purchase);
			httpStatus=201;
		}catch(SproutExcpetion e){
			httpStatus=e.getHttpStatus();
			log.info(httpStatus+" "+e.getMessage());
		}
	}
	
	@Test
	public void testGetPurchaseDetails() throws Exception {
		init();
		String accountId="30000000337513";
		SproutUtils sproutUtils=new SproutUtils();
		SproutAccountTransactions accountTrans=sproutUtils.getAccountTransactions(accountId,"2015-05");
		List<SproutTransaction> trans=accountTrans.getTransactions();
		for(SproutTransaction aTran: trans){
			if(aTran.getTransaction_type()!=null && SproutTransactionType.getByValue(aTran.getTransaction_type()).equals(SproutTransactionType.PURCHASE)){
				SproutPurchaseDetail purchase=sproutUtils.getPurchaseDetailsByUrl(aTran.getUrl());
				log.info(purchase);
			}
		}
	}
	
	@Test
	public void testLoadCash() throws Exception {
		init();
		String accountId="30000000337521";
		SproutUtils sproutUtils=new SproutUtils();
		SproutLoadOrRefundResponse loadResp=null;
		try{
			loadResp=sproutUtils.load(accountId, new BigDecimal("10"), null, null, "5091188488", "cash");
		}catch(SproutExcpetion e){
			int httpStatus=e.getHttpStatus();
			log.info(httpStatus+" "+e.getMessage());
		}
		log.info(loadResp);
	}
	
	@Test
	public void testLoadCredit() throws Exception {
		init();
		String accountId="30000000337521";
		SproutUtils sproutUtils=new SproutUtils();
		SproutLoadOrRefundResponse loadResp=null;
		try{
			loadResp=sproutUtils.load(accountId, new BigDecimal("10"), "8", testMerchantId, "5091188488", "card");
		}catch(SproutExcpetion e){
			int httpStatus=e.getHttpStatus();
			log.info(httpStatus+" "+e.getMessage());
		}
		log.info(loadResp);
	}
	
	@Test
	public void testGetLoadDetail() throws Exception {
		init();
		String accountId="30000000337521";
		SproutUtils sproutUtils=new SproutUtils();
		SproutAccountTransactions accountTrans=sproutUtils.getAccountTransactions(accountId,"2015-06");
		List<SproutTransaction> trans=accountTrans.getTransactions();
		for(SproutTransaction aTran: trans){
			if(aTran.getTransaction_type()!=null && SproutTransactionType.getByValue(aTran.getTransaction_type()).equals(SproutTransactionType.LOAD)){
				SproutLoadDetail load=sproutUtils.getLoadDetailByUrl(aTran.getUrl());
				log.info(load);
			}
		}
	}
	
	@Test
	public void testLoadCreditReversal() throws Exception {
		init();
		String accountId="30000000337521";
		SproutUtils sproutUtils=new SproutUtils();
		SproutLoadOrRefundResponse loadResp=null;
		long externalReferenceId=System.currentTimeMillis();
		try{
			loadResp=sproutUtils.load(accountId, new BigDecimal("10"), String.valueOf(externalReferenceId), testMerchantId, "5091188488", "card");
		}catch(SproutExcpetion e){
			int httpStatus=e.getHttpStatus();
			log.info(httpStatus+" "+e.getMessage());
		}
		if(loadResp!=null){
			log.info(loadResp);
			long newExternalReferenceId=System.currentTimeMillis();
			SproutReversalResponse reveralResp=null;
			try{
				reveralResp=sproutUtils.reversal(String.valueOf(newExternalReferenceId), String.valueOf(externalReferenceId), "load", "5091188488");
			}catch(SproutExcpetion e){
				int httpStatus=e.getHttpStatus();
				log.info(httpStatus+" "+e.getMessage());
			}
			log.info(reveralResp);
			if(reveralResp!=null){
				SproutReversalDetail reversalDetail=null;
				try{
					reversalDetail=sproutUtils.getReversalDetailByUrl(reveralResp.getUrl());
				}catch(SproutExcpetion e){
					int httpStatus=e.getHttpStatus();
					log.info(httpStatus+" "+e.getMessage());
				}
				log.info(reversalDetail);
				
			}
		}
	}
	
	public static void main(String[] args) throws Exception{
		//SproutUtils.setSproutUrl("https://api.bytevampire.com");
		//SproutUtils.setSproutAuthorizationToken("30a1a578bc10c11863dd89da92464c03278badff4d143e6bd8337ac10690b4da");
		
		String createdTime="2015-05-28 20:32:32.438245+00:00";
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSSXXX");
		Date date=dateFormat.parse(createdTime);
		System.out.println(date);
	}
	
	@Test
	public void testPurchaseRefund() throws Exception {
		init();
		String sproutCardNumber="6277229030431058";
		SproutUtils sproutUtils=new SproutUtils();
		int httpStatus=404;
		String external_reference_id=String.valueOf(System.currentTimeMillis());
		String vampire_pos_id="5091188488";
		List<SproutPurchaseItem> items=new ArrayList<SproutPurchaseItem>();
		
		SproutPurchaseItem item=new SproutPurchaseItem();
		item.setCoil_name("coke");
		item.setPrice(new BigDecimal("1.00"));
		items.add(item);
		try{
			SproutPurchaseResponse purchase=sproutUtils.purchase(sproutCardNumber, external_reference_id, vampire_pos_id, items);
			log.info(purchase);
			httpStatus=201;
			SproutLoadOrRefundResponse resp =sproutUtils.refund(String.valueOf(System.currentTimeMillis()), purchase.getTransaction_id(), "Test refund");
			log.info(resp);
		}catch(SproutExcpetion e){
			httpStatus=e.getHttpStatus();
			log.info(httpStatus+" "+e.getMessage());
		}
	}

}
