package com.usatech.perftest;

import simple.io.Log;

import com.usatech.ec2.EC2ClientUtils;
import com.usatech.ec2.EC2ExecutionException;

public class GetCardIdTest {
	private static final Log log = Log.getLog();

	public static void main(String[] args) throws Exception {
		EC2ClientUtils.setVirtualDeviceSerialCd("V1-1");
		EC2ClientUtils.setEportConnectUrl("https://ec.usatech.com:9443/hessian/ec2");
		for(String card : new String[] {
				"4564365555555553",
				"4032766666666664",
				"4525806666666662",
				"4988335555555554",
				"4263536666666661",
				"5430495555555557",
				"5569235555555550",
				"5284005555555551",
				"5432675555555552",
				"5101085555555554",
				"4427802641004797",
				"4427806666666661",
				"5000000010001005",
				"5000200010002009",
				"5000300020003003",
				"5488549999999804",
				"6500004569851268",
				"6599990597863212",
				"6221260004598744",
				"6858009904315687",
				"9400780009541230",
				"9988029902389457",
				"4005551122334450",
				"5471140000000003",
				"6011000993029986",
				"38555565010005",
				"373235387881007"})
			printCardAndId(card);
	}

	public static void printCardAndId(String card) {
		try {
			log.info("Card " + card + ": " + getCardId(card));
		} catch(EC2ExecutionException e) {
			log.error("Card  " + card + " failed", e);
		}
	}
	public static long getCardId(String card) throws EC2ExecutionException {
		return EC2ClientUtils.getGlobalAccountId(card);
	}

}
