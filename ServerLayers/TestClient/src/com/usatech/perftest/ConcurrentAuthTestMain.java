package com.usatech.perftest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Properties;

import simple.app.Base;
import simple.app.MainWithConfig;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.event.TaskListener;
import simple.event.WriteCSVTaskListener;
import simple.io.Log;
import simple.results.Results;

import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageData_C2;
import com.usatech.layers.common.messagedata.MessageData_C2.GenericTracksCardReader;
import com.usatech.test.ClientEmulator;

public class ConcurrentAuthTestMain extends MainWithConfig {
	private static final Log log = Log.getLog();

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new ConcurrentAuthTestMain().run(args);
	}

	@Override
	protected void registerDefaultCommandLineArguments() {
		registerCommandLineSwitch('p', "propertiesFile", true, true, "properties-file", "The properties file to use");
		registerCommandLineSwitch('t', "threads", false, true, "threads", "Number of threads to run");
		//registerCommandLineSwitch('s', "sessions", false, true, "sessions", "Number of sessions per thread");
		registerCommandLineSwitch('n', "messages", false, true, "messages", "Number of messages per session");
		//registerCommandLineSwitch('c', "command", false, true, "command", "The Hex representation of the command to use");
		registerCommandLineSwitch('h', "host", false, true, "host", "Host to connect to");
		registerCommandLineSwitch('P', "port", false, true, "port", "Port");
		registerCommandLineSwitch('c', "card", false, true, "card", "The track data to use");
		registerCommandLineSwitch('v', "protocol", true, true, "protocol", "The protocol version to use (0-8)");
		registerCommandLineSwitch('o', "output", true, true, "output", "Directory for output");
	}

	@Override
	protected void registerDefaultActions() {
		// no actions
	}
	@Override
	public void run(String[] args) {
		Map<String,Object> argMap;
		try {
			argMap = parseArguments(args);
		} catch(IOException e) {
			finishAndExit(e.getMessage(), 100, true, null);
			return;
		}
		int threads;
		final int messages;
		final String host;
		final int port;
		final int protocol;
		final String cardNumber;
		String outputDir;
		try {
			threads = ConvertUtils.getInt(argMap.get("threads"), 10);
			messages = ConvertUtils.getInt(argMap.get("messages"), 10);
			host = ConvertUtils.getStringSafely(argMap.get("host"), "usaapd1");
			port = ConvertUtils.getInt(argMap.get("port"), 14109);
			protocol = ConvertUtils.getIntSafely(argMap.get("protocol"), 7);
			outputDir = ConvertUtils.getString(argMap.get("output"), "./");
			cardNumber = ConvertUtils.getString(argMap.get("card"), "375019001001880");
			if(!outputDir.endsWith("/")){
				outputDir+="/";
			}
		} catch(ConvertException e) {
			log.warn("Error", e);
			finishAndExit(e.getMessage(), 200, true, null);
			return;
		} catch(NumberFormatException e) {
			finishAndExit("Could not convert command to a hex number: " + e.getMessage(), 250, true, null);
			return;
		}
		Properties properties;
		try {
			properties = getProperties(ConvertUtils.getStringSafely(argMap.get("properties-file")), getClass(), null);
		} catch(IOException e) {
			log.warn("Error", e);
			finishAndExit("Could not read properties file", 120, e);
			return;
		}
		for(Map.Entry<String,Object> entry : argMap.entrySet()) {
			properties.put(entry.getKey(), entry.getValue());
		}
		try {
			Base.configureDataSourceFactory(properties, null);
		} catch(ServiceException e) {
			log.warn("Error", e);
			finishAndExit(e.getMessage(), 300, true, null);
			return;
		}
		try {
			Base.configureDataLayer(properties);
		} catch(ServiceException e) {
			log.warn("Error", e);
			finishAndExit(e.getMessage(), 400, true, null);
			return;
		}
		Results results;
		try {
			results = DataLayerMgr.executeQuery("GET_AUTH_DEVICES", null);
		} catch(SQLException e) {
			log.warn("Error", e);
			finishAndExit(e.getMessage(), 500, true, null);
			return;
		} catch(DataLayerException e) {
			log.warn("Error", e);
			finishAndExit(e.getMessage(), 600, true, null);
			return;
		}
		final String[] deviceNames = new String[threads];
		final byte[][] encryptKeys = new byte[threads][];
		for(int i = 0; i < threads; i++) {
			if(!results.next()) {
				finishAndExit("Not enough results in GET_AUTH_DEVICES: Need " + threads + " but only got " + i, 720, true, null);
				return;
			}
			try {
				deviceNames[i] = results.getValue("DEVICE_NAME", String.class);
				encryptKeys[i] = results.getValue("ENCRYPTION_KEY", byte[].class);
			} catch(ConvertException e) {
				log.warn("Error", e);
				finishAndExit(e.getMessage(), 700, true, null);
				return;
			}
		}

		File file = new File(outputDir+"ConcurrentAuthPerformanceTest_" + new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date()) + ".csv");
		final TaskListener taskListener;
		try {
			taskListener = new WriteCSVTaskListener(new PrintWriter(new FileOutputStream(file)));
		} catch(FileNotFoundException e) {
			log.warn("Error", e);
			finishAndExit(e.getMessage(), 800, true, null);
			return;
		}
		class Runner extends Thread {
			protected final ClientEmulator ce;
			protected final MessageData_C2 message;
			public Runner(int index) {
				super("Runner #" + (index+1));
				ce = new ClientEmulator(host, port, deviceNames[index], encryptKeys[index]);
				message = new MessageData_C2();
				message.setAmount(BigDecimal.valueOf(7));
				message.setCardReaderType(CardReaderType.GENERIC);
				GenericTracksCardReader crt = (GenericTracksCardReader)message.getCardReader();
				crt.setAccountData2(cardNumber);
				message.setEntryType(EntryType.SWIPE);
			}

			@Override
			public void run() {
				StringBuilder details = new StringBuilder();
				for(int s = 0; s < messages; s++) {
					long startTime = 0;
					try {
						ce.setMessageNumber(0);
						ce.startCommunication(protocol);
						details.setLength(0);
						details.append("Sent:\n").append(message);
						startTime = System.currentTimeMillis();
						MessageData reply = ce.sendMessage(message);
						long endTime=System.currentTimeMillis();
						details.append("\nReceived:\n").append(reply);
						taskListener.taskRan("SentMessage", details.toString(), startTime, endTime);
						log.debug(details);
						ce.stopCommunication();
					} catch(Exception e) {
						long endTime = System.currentTimeMillis();
						if(startTime == 0)
							startTime = endTime;
						details.append("\nERROR:\n").append(e.getMessage());
						taskListener.taskRan("ERROR", details.toString(), startTime, endTime);
						log.warn("Error during session", e);
					}
				}
			}
		};
		Thread[] threadArray = new Thread[threads];
		for(int i = 0; i < threads; i++) {
			threadArray[i] = new Runner(i);
			threadArray[i].start();
		}
		for(int i = 0; i < threads; i++) {
			try {
				threadArray[i].join();
			} catch(InterruptedException e) {
				log.warn("Thread interrupted", e);
			}
		}
		taskListener.flush();
		Log.finish();
	}
}
