package com.usatech.perftest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

import simple.app.Base;
import simple.app.MainWithConfig;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.event.TaskListener;
import simple.event.WriteCSVTaskListener;
import simple.io.Log;
import simple.results.Results;

import com.usatech.networklayer.ReRixMessage;
import com.usatech.test.BaseMessage;
import com.usatech.test.ClientEmulator;
import com.usatech.test.CommandLineUtil;
import com.usatech.test.TestClientUtil;
import com.usatech.test.V4Message;

public class LoadTestMain extends MainWithConfig {
	private static final Log log = Log.getLog();

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new LoadTestMain().run(args);
	}

	@Override
	protected void registerDefaultCommandLineArguments() {
		registerCommandLineSwitch('p', "propertiesFile", true, true, "properties-file", "The properties file to use");
		registerCommandLineSwitch('t', "threads", false, true, "threads", "Number of threads to run");
		registerCommandLineSwitch('s', "sessions", false, true, "sessions", "Number of sessions per thread");
		registerCommandLineSwitch('n', "messages", false, true, "messages", "Number of messages per session");
		registerCommandLineSwitch('c', "command", false, true, "command", "The message type: eg. C0, C1, A0, etc. Please refer to the interactive command interface.");
		registerCommandLineSwitch('h', "host", false, true, "host", "Host to connect to");
		registerCommandLineSwitch('P', "port", false, true, "port", "Port");
		registerCommandLineSwitch('v', "protocol", true, true, "protocol", "The protocol version to use (0-8)");
		registerCommandLineSwitch('o', "output", true, true, "output", "Directory for output");
	}

	@Override
	protected void registerDefaultActions() {
		// no actions
	}
	@Override
	public void run(String[] args) {
		Map<String,Object> argMap;
		try {
			argMap = parseArguments(args);
		} catch(IOException e) {
			finishAndExit(e.getMessage(), 100, true, null);
			return;
		}
		int threads;
		final int sessions;
		final int messages;
		final String messageType;
		final String host;
		final int port;
		final int protocol;
		String outputDir;
		try {
			threads = ConvertUtils.getInt(argMap.get("threads"));
			sessions = ConvertUtils.getInt(argMap.get("sessions"));
			messages = ConvertUtils.getInt(argMap.get("messages"));
			messageType = ConvertUtils.getStringSafely(argMap.get("command"));
			host = ConvertUtils.getStringSafely(argMap.get("host"));
			port = ConvertUtils.getInt(argMap.get("port"));
			protocol = ConvertUtils.getIntSafely(argMap.get("protocol"), 7);
			outputDir = ConvertUtils.getString(argMap.get("output"), "./");
			if(!outputDir.endsWith("/")){
				outputDir+="/";
			}
		} catch(ConvertException e) {
			finishAndExit(e.getMessage(), 200, true, null);
			return;
		} catch(NumberFormatException e) {
			finishAndExit("Could not convert command to a hex number: " + e.getMessage(), 250, true, null);
			return;
		}
		Properties properties;
		try {
			properties = getProperties(ConvertUtils.getStringSafely(argMap.get("properties-file")), getClass(), null);
		} catch(IOException e) {
			finishAndExit("Could not read properties file", 120, e);
			return;
		}
		for(Map.Entry<String,Object> entry : argMap.entrySet()) {
			properties.put(entry.getKey(), entry.getValue());
		}
		try {
			Base.configureDataSourceFactory(properties, null);
		} catch(ServiceException e) {
			finishAndExit(e.getMessage(), 300, true, null);
			return;
		}
		try {
			Base.configureDataLayer(properties);
		} catch(ServiceException e) {
			finishAndExit(e.getMessage(), 400, true, null);
			return;
		}
		Results results;
		try {
			results = DataLayerMgr.executeQuery("GET_ACTIVE_DEVICES", new Object[] {1 + (threads * sessions / 3)});
		} catch(SQLException e) {
			finishAndExit(e.getMessage(), 500, true, null);
			return;
		} catch(DataLayerException e) {
			finishAndExit(e.getMessage(), 600, true, null);
			return;
		}
		final List<String> deviceNames = new ArrayList<String>();
		final List<byte[]> encryptKeys = new ArrayList<byte[]>();
		while(results.next()) {
			try {
				deviceNames.add(results.getValue("DEVICE_NAME", String.class));
				encryptKeys.add(results.getValue("ENCRYPTION_KEY", byte[].class));
			} catch(ConvertException e) {
				finishAndExit(e.getMessage(), 700, true, null);
				return;
			}
		}
		final AtomicInteger index = new AtomicInteger();
		BaseMessage sMsg;
		try{
			sMsg=CommandLineUtil.inputMessageType(messageType);
		}catch(Exception e){
			finishAndExit("Invalid message type '" + messageType + "'", 750, true, null);
			return;
		}
		if(sMsg==null){
			finishAndExit("Invalid message type '" + messageType + "'", 750, true, null);
			return;
		}
		File file = new File(outputDir+"LayerPerformanceTest_" + new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date()) + ".csv");
		final TaskListener taskListener;
		try {
			taskListener = new WriteCSVTaskListener(new PrintWriter(new FileOutputStream(file)));
		} catch(FileNotFoundException e) {
			finishAndExit(e.getMessage(), 800, true, null);
			return;
		}
		class Runner extends Thread {
			protected final BaseMessage message;
			public Runner() throws IOException, ConvertException {
				super();
				message = CommandLineUtil.inputMessageType(messageType);
			}

			@Override
			public void run() {
				long messageId = 0;
				long lastMessageId = 0;
				for(int s = 0; s < sessions; s++) {
					int messageNumber = -1;
					int deviceIndex=index.getAndIncrement() % deviceNames.size();
					ClientEmulator ce = new ClientEmulator(host, port, deviceNames.get(deviceIndex), encryptKeys.get(deviceIndex));
					taskListener.taskStarted("Session", "Device=" + ce.getEvNumber() + "; Messages=" + messages);
					long startTime=0;
					try {
						ce.startCommunication(protocol);
						ReRixMessage sentMsg=null;
						String details=null;
						for(int m = 0; m < messages; m++) {
							messageNumber = TestClientUtil.generateMessageNumber(messageNumber);
							message.setMessageNumber(messageNumber);

							lastMessageId = messageId;
							if (message instanceof V4Message)
								((V4Message) message).setLastMessageId(lastMessageId);

							messageId = TestClientUtil.generateMessageId(messageId);
							message.setMessageId(messageId);

							sentMsg=message.createMessage();
							startTime=System.currentTimeMillis();
							ReRixMessage responseMsg=ce.sendMessage(sentMsg, 1);
							long endTime=System.currentTimeMillis();
							details="Sent:\n"+CommandLineUtil.printMessage(message)+"Received:\n"+message.readResponseString(responseMsg);
							taskListener.taskRan("SentMessage", details, startTime, endTime);
						}
						ce.stopCommunication();
					} catch(Exception e) {
						long endTime=System.currentTimeMillis();
						String details="ERROR:\n"+e.getMessage();
						taskListener.taskRan("SentMessage", details, startTime, endTime);
						log.warn("Error during session for '" + ce.getEvNumber() + "' for message " + messageId, e);
					}
					taskListener.taskEnded("Session");
				}
			}
		};
		log.info("Starting up " + threads + " threads...");
		Thread[] threadArray = new Thread[threads];
		for(int i = 0; i < threads; i++) {
			try {
				threadArray[i] = new Runner();
			}catch(Exception e){
				finishAndExit("Invalid message type '" + messageType + "'", 750, true, null);
				return;
			}
			threadArray[i].start();
		}
		for(int i = 0; i < threads; i++) {
			try {
				threadArray[i].join();
			} catch(InterruptedException e) {
				log.warn("Thread interrupted", e);
			}
		}
		taskListener.flush();
		Log.finish();
	}
}
