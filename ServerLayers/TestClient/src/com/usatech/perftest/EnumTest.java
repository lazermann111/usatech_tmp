/**
 *
 */
package com.usatech.perftest;

import simple.bean.ConvertUtils;

import com.usatech.layers.common.constants.AuthorityAttrEnum;

/**
 * @author Brian S. Krug
 *
 */
public class EnumTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		AuthorityAttrEnum a = AuthorityAttrEnum.ATTR_ACTION_SUB_TYPE;
		System.out.println("Attribute '" + a + "' = '" + ConvertUtils.getStringSafely(a)+ "'");

	}

}
