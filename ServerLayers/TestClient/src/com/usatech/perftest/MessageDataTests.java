/**
 *
 */
package com.usatech.perftest;

import simple.text.StringUtils;

import com.usatech.layers.common.messagedata.MessageData;

/**
 * @author Brian S. Krug
 *
 */
public class MessageDataTests {

	/**
	 * @param args
	 * @throws ClassNotFoundException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		String classPrefix = MessageData.class.getPackage().getName() + '.';
		for(int d = 0xC0; d < 0xCC; d++) {
			String className = classPrefix + StringUtils.toHex((byte)d) + "MessageData";
			Class<?> clazz = Class.forName(className);
			System.out.println("Class " + clazz.getName() + " exists");
			MessageData md = clazz.asSubclass(MessageData.class).newInstance();
			System.out.println("Class " + clazz.getName() + " implements MessageData");
			if(md.getMessageType().getMajorValue() != (byte) d)
				System.out.println("Class " + clazz.getName() + " supports the WRONG MessageType");
			else
				System.out.println("Class " + clazz.getName() + " supports the correct MessageType");
			System.out.println(md);
		}
	}

}
