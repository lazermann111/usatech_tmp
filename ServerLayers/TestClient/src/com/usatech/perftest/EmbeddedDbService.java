package com.usatech.perftest;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.InetAddress;
import java.nio.CharBuffer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.derby.drda.NetworkServerControl;
import org.apache.derby.iapi.error.StandardException;
import org.apache.derby.iapi.jdbc.DRDAServerStarter;
import org.apache.derby.iapi.services.monitor.Monitor;
import org.apache.derby.iapi.services.property.PropertyUtil;
import org.apache.derby.impl.drda.NetworkServerControlImpl;

import simple.app.Service;
import simple.app.ServiceException;
import simple.app.ServiceStatus;
import simple.app.ServiceStatusInfo;
import simple.app.ServiceStatusListener;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.ExtendedDataSource;
import simple.io.ConfigSource;
import simple.io.Log;
import simple.util.MapBackedSet;

import com.usatech.layers.common.ProcessingUtils;

public class EmbeddedDbService implements Service {
	private static final Log log = Log.getLog();
	protected final String serviceName;
	protected NetworkServerControlImpl networkServerControl;
	//protected NetworkServerControl networkServerControl;
	protected final ReentrantLock lock = new ReentrantLock();
	protected final Set<ServiceStatusListener> serviceStatusListeners = new MapBackedSet<ServiceStatusListener>(new ConcurrentHashMap<ServiceStatusListener, Object>()); //new LinkedHashSet<ServiceStatusListener>();
	protected int numThreads;
	protected boolean started;
	protected int starts;
	protected String serverLogFile;
	protected String username;
	protected String password;
	protected int port;
	protected boolean remoteAllowed;
	protected InetAddress address;
	protected long startUpDelay = 1000;
	protected String dataSourceName;
	protected ConfigSource initializeScript;

	public EmbeddedDbService(String serviceName) throws Exception {
		super();
		this.serviceName = serviceName;
		this.serverLogFile = getDerbyHome() + "derbynet.log";
	}

	public boolean addServiceStatusListener(ServiceStatusListener listener) {
		boolean changed = serviceStatusListeners.add(listener);
		if(changed) {
			fireServiceStatusChanged(null, started ? ServiceStatus.STARTED : ServiceStatus.STOPPED, starts);
		}
		return changed;
	}

	public boolean removeServiceStatusListener(ServiceStatusListener listener) {
		return serviceStatusListeners.remove(listener);
	}

	protected void fireServiceStatusChanged(Thread thread, ServiceStatus serviceStatus, Number iterationCount) {
		for(ServiceStatusListener l : serviceStatusListeners) {
			l.serviceStatusChanged(new ServiceStatusInfo(this, thread, serviceStatus, iterationCount != null ? iterationCount.longValue() : 0L, null));
		}
	}

	public int getNumThreads() {
		return numThreads;
	}

	public String getServiceName() {
		return serviceName;
	}

	public int pauseThreads() throws ServiceException {
		return 0;
	}

	public int restartThreads(long timeout) throws ServiceException {
		lock.lock();
		try {
			if(this.numThreads == 0)
				return 0;
			checkServer();
			if(started) {
				internalShutdown();
			}
			internalStartup();
			return this.numThreads;
		} catch(Exception e) {
			throw new ServiceException("Could not start threads on derby network server", e);
		} finally {
			lock.unlock();
		}
	}

	public int startThreads(int numThreads) throws ServiceException {
		if(numThreads <= 0)
			return 0;
		lock.lock();
		try {
			this.numThreads += numThreads;
			checkServer();
			if(!started) {
				internalStartup();
			} else {
				log.debug("Setting max threads to " + this.numThreads);
				networkServerControl.netSetMaxThreads(this.numThreads);
				//networkServerControl.netSetTimeSlice(1000);
			}
		} catch(Exception e) {
			throw new ServiceException("Could not start threads on derby network server", e);
		} finally {
			lock.unlock();
		}
		return numThreads;
	}

	public int stopAllThreads(boolean force, long timeout) throws ServiceException {
		lock.lock();
		try {
			if(started) {
				checkServer();
				internalShutdown();
				int n = this.numThreads;
				this.numThreads = 0;
				return n;
			} else
				return 0;
		} catch(Exception e) {
			throw new ServiceException("Could not shutdown", e);
		} finally {
			lock.unlock();
		}
	}

	public int stopThreads(int numThreads, boolean force, long timeout) throws ServiceException {
		if(numThreads <= 0)
			return 0;
		lock.lock();
		try {
			int origThreads = this.numThreads;
			this.numThreads -= numThreads;
			if(!started) return 0;
			checkServer();
			if(this.numThreads > 0) {
				log.debug("Setting max threads to " + this.numThreads);
				networkServerControl.netSetMaxThreads(this.numThreads);
				//networkServerControl.netSetTimeSlice(1000);
				return numThreads;
			} else {
				this.numThreads = 0;
				internalShutdown();
				return origThreads;
			}
		} catch(Exception e) {
			throw new ServiceException("Could not shutdown", e);
		} finally {
			lock.unlock();
		}
	}

	protected void internalShutdown() throws Exception {
		//DriverManager.getConnection("jdbc:derby:;shutdown=true");
		//networkServerControl.shutdown(); //DriverManager.getConnection("jdbc:derby:;shutdown=true");
		log.debug("Stopping Derby DB...");
		getServerStarter().stop();
		started = false;
		fireServiceStatusChanged(null, ServiceStatus.STOPPED, starts);
	}

	protected DRDAServerStarter getServerStarter() throws StandardException {
		return (DRDAServerStarter)Monitor.findSystemModule("org.apache.derby.iapi.jdbc.DRDAServerStarter");
	}
	protected void internalStartup() throws Exception {
		//networkServerControl.start(new PrintWriter(getServerLogFile()));
		log.info("Loading Embedded Driver to start Derby DB...");
		Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
		checkDataSource();
		long delay = getStartUpDelay();
		if(delay > 0)
			Thread.sleep(delay);//give it time to come up?
		log.debug("Setting max threads to " + this.numThreads);
		started = true;
		fireServiceStatusChanged(null, ServiceStatus.STARTED, ++starts);
		networkServerControl.netSetMaxThreads(this.numThreads);
		//networkServerControl.netSetTimeSlice(1000);
	}

	/**
	 * @throws SQLException
	 *
	 */
	protected void checkDataSource() throws DataLayerException, SQLException {
		String dsn = getDataSourceName();
		if(dsn != null) {
			Connection conn = null;
			try {
				conn = DataLayerMgr.getConnection(dsn);
			} catch(SQLException e) {
				boolean createDB = false;
				for(Throwable t = e; t != null; t = t.getCause()) {
					if(t instanceof SQLException && "XJ004".equals(((SQLException)t).getSQLState())) {
						createDB = true;
						break;
					}
				}
				if(createDB) {
					// create database
					ExtendedDataSource ds = DataLayerMgr.getDataSourceFactory().getDataSource(dsn);
					Properties info = new Properties();
					String user = ds.getUsername();
		        	if(!info.containsKey("user") && user != null && user.trim().length() > 0) {
		        	    info.put("user", user);
		        	}
		        	String password = ds.getPassword();
		        	if(!info.containsKey("password") && password != null && password.trim().length() > 0) {
		        	    info.put("password", password);
		        	}
		        	info.put("create", "true");
		        	ConfigSource src = getInitializeScript();
					conn = DriverManager.getConnection(ds.getUrl(), info);
					try {
						// execute initialization script
						if(src != null) {
							Reader reader = new InputStreamReader(src.getInputStream());
							// read each statement (look for ';') and execute it
							CharBuffer buffer = CharBuffer.allocate(8192); // This ought to be enough!
							//buffer.limit(1024);// read less than capacity
							int pos = 0;
							while(true) {
								int r = reader.read(buffer);
								if(r < 0) {
									// end of stream
									if(buffer.position() > 0) {
										buffer.flip();
										executeUpdate(conn, buffer.toString());
									}
									break;
								} else {
									buffer.flip();
									while(buffer.hasRemaining()) {
										char ch = buffer.get();
										switch(ch) {
											case '/':
												char ch2 = buffer.get(buffer.position());
												switch(ch2) {
													case '/': // single-line comment, find \n or \r
														while(buffer.hasRemaining()) {
															char ch3 = buffer.get();
															if(ch3 == '\r' || ch3 == '\n')
																break;
														}
														break;
													case '*': // multi-line comment find */
														if(!buffer.hasRemaining())
															throw new SQLException("Unclosed multi-line comment at character " + (pos + buffer.position()));
														char ch3 = buffer.get();
														while(true) {
															if(!buffer.hasRemaining())
																throw new SQLException("Unclosed multi-line comment at character " + (pos + buffer.position()));
															if('*' == ch3) {
																ch3 = buffer.get();
																if(ch3 == '/') break;
															} else
																ch3 = buffer.get();

														}
														break;
												}
												break;
											case '\'': case '"':
												//find next quote
												while(true) {
													if(!buffer.hasRemaining())
														throw new SQLException("Unclosed quote (" + ch + ") at character " + (pos + buffer.position()));
													if(ch == buffer.get()) break;
												}
												break;
											case ';':
												// execute sql
												int limit = buffer.limit();
												int position = buffer.position();
												buffer.position(0);
												buffer.limit(position - 1);  // don't include ';'
												executeUpdate(conn, buffer.toString());
												pos += position;
												buffer.limit(limit);
												buffer.position(position);
												buffer.compact();
												buffer.flip();
												break;
										}
									}
								}
								buffer.limit(buffer.capacity());
							}
							log.debug("Script complete; Calling commit");
							conn.commit();
						}
					} catch(IOException e1) {
						throw new SQLException("Could not read initializeScript " + src + " to create objects in new database. Database has been created through.", e1);
					} finally {
						ProcessingUtils.closeDbConnection(log, conn);
					}
					return;
				}
				throw e;
			} finally {
				ProcessingUtils.closeDbConnection(log, conn);
			}
		}
	}

	protected static int executeUpdate(Connection conn, String sql) throws SQLException {
		if((sql=sql.trim()).length() == 0)
			return 0;
		log.debug("Executing SQL: " + sql);
		long start = System.currentTimeMillis();
		Statement st = conn.createStatement();
		try {
			int result = st.executeUpdate(sql);
			log.debug("SQL took " + (System.currentTimeMillis() - start) + " milliseconds");
			return result;
		} finally {
			st.close();
		}
	}
	public int unpauseThreads() throws ServiceException {
		return 0;
	}

	protected static String getDerbyHome() {
		String derbyHome = System.getProperty("derby.system.home");
		if(derbyHome == null) return "";
		if((derbyHome=derbyHome.trim()).length() == 0) return "";
		if(derbyHome.endsWith("/") || derbyHome.endsWith("\\")) return derbyHome;
		return derbyHome + '/';
	}

	protected boolean isServerStarted() {
		try {
			networkServerControl.ping();
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	protected void checkServer() throws Exception {
		if(networkServerControl == null) {
			//networkServerControl = new NetworkServerControl(getUsername(), getPassword());
			networkServerControl = new NetworkServerControlImpl(getUsername(), getPassword());
		}
	}

	public int getPort() {
		return getDerbyPort();
	}

	public static int getDerbyPort() {
		return ConvertUtils.getIntSafely(PropertyUtil.getSystemProperty("derby.drda.portNumber"), NetworkServerControl.DEFAULT_PORTNUMBER);
	}

	public String getServerLogFile() {
		return serverLogFile;
	}

	public void setServerLogFile(String serverLogFile) {
		this.serverLogFile = serverLogFile;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		lock.lock();
		try {
			this.username = username;
			if(networkServerControl != null) {
				if(started)
					try {
						internalShutdown();
					} catch(Exception e) {
						log.warn("Could not shutdown; setting to null anyway", e);
						started = false;
					}
				networkServerControl = null;
			}
		} finally {
			lock.unlock();
		}
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		lock.lock();
		try {
			this.password = password;
			if(networkServerControl != null) {
				if(started)
					try {
						internalShutdown();
					} catch(Exception e) {
						log.warn("Could not shutdown; setting to null anyway", e);
						started = false;
					}
				networkServerControl = null;
			}
		} finally {
			lock.unlock();
		}
	}
/*
	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		lock.lock();
		try {
			this.port = port;
			if(networkServerControl != null) {
				if(started)
					try {
						internalShutdown();
					} catch(Exception e) {
						log.warn("Could not shutdown; setting to null anyway", e);
						started = false;
					}
				networkServerControl = null;
			}
		} finally {
			lock.unlock();
		}
	}

	public boolean isRemoteAllowed() {
		return remoteAllowed;
	}

	public void setRemoteAllowed(boolean remoteAllowed) throws UnknownHostException {
		lock.lock();
		try {
			this.remoteAllowed = remoteAllowed;
			address = InetAddress.getByAddress(remoteAllowed ? new byte[]{0,0,0,0} : new byte[]{127,0,0,1});
			if(networkServerControl != null) {
				if(started)
					try {
						internalShutdown();
					} catch(Exception e) {
						log.warn("Could not shutdown; setting to null anyway", e);
						started = false;
					}
				networkServerControl = null;
			}
		} finally {
			lock.unlock();
		}
	}
	*/

	public long getStartUpDelay() {
		return startUpDelay;
	}

	public void setStartUpDelay(long startUpDelay) {
		this.startUpDelay = startUpDelay;
	}
	/**
	 * @see simple.app.Service#shutdown(long)
	 */
	public boolean shutdown(long wait) throws ServiceException {
		stopAllThreads(false, wait);
		return true;
	}

	public String getDataSourceName() {
		return dataSourceName;
	}

	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}

	public ConfigSource getInitializeScript() {
		return initializeScript;
	}

	public void setInitializeScript(ConfigSource initializeScript) {
		this.initializeScript = initializeScript;
	}

	@Override
	public Future<Integer> stopThreads(int numThreads, boolean force) throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Future<Integer> stopAllThreads(boolean force) throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Future<Boolean> shutdown() throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Future<Boolean> prepareShutdown() throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}
}
