package com.usatech.perftest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import simple.app.Base;
import simple.app.MainWithConfig;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.event.TaskListener;
import simple.event.WriteCSVTaskListener;
import simple.io.ByteArrayUtils;
import simple.io.Log;
import simple.results.Results;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;
import simple.text.ThreadSafeDateFormat;
import simple.util.concurrent.CustomThreadFactory;

import com.usatech.layers.common.CRC;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.constants.AuthResponseCodeC3;
import com.usatech.layers.common.constants.AuthResponseType;
import com.usatech.layers.common.constants.CRCType;
import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.FileType;
import com.usatech.layers.common.constants.GenericRequestType;
import com.usatech.layers.common.constants.GenericResponseClientActionCode;
import com.usatech.layers.common.constants.GenericResponseResultCode;
import com.usatech.layers.common.constants.InitializationReason;
import com.usatech.layers.common.constants.ReceiptResult;
import com.usatech.layers.common.constants.SaleResult;
import com.usatech.layers.common.constants.SaleType;
import com.usatech.layers.common.messagedata.AuthorizeMessageData;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageData_C0;
import com.usatech.layers.common.messagedata.MessageData_C2;
import com.usatech.layers.common.messagedata.MessageData_C2.GenericTracksCardReader;
import com.usatech.layers.common.messagedata.MessageData_C3;
import com.usatech.layers.common.messagedata.MessageData_C3.AuthResponseTypeData;
import com.usatech.layers.common.messagedata.MessageData_C3.AuthorizationAuthResponse;
import com.usatech.layers.common.messagedata.MessageData_C4;
import com.usatech.layers.common.messagedata.MessageData_C4.Format0LineItem;
import com.usatech.layers.common.messagedata.MessageData_C8;
import com.usatech.layers.common.messagedata.MessageData_C9;
import com.usatech.layers.common.messagedata.MessageData_CA;
import com.usatech.layers.common.messagedata.MessageData_CA.UpdateStatusRequest;
import com.usatech.layers.common.messagedata.MessageData_CB;
import com.usatech.layers.common.messagedata.MessageDirection;
import com.usatech.test.ClientEmulator;

public class DeviceLoadTest extends MainWithConfig {
	private static final Log log = Log.getLog();
	protected static final DateFormat DEX_HEADER_DATE_FORMAT = new ThreadSafeDateFormat(new SimpleDateFormat("HHmmss-MMddyy"));
	protected static long DEX_TIME_BASE;
	protected interface Specifics {
		String getOutputFilePrefix();

		void execute(int index, String host, int port, int protocol, TaskListener taskListener);		
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new DeviceLoadTest().run(args);
	}

	@Override
	protected void registerDefaultCommandLineArguments() {
		registerCommandLineSwitch('n', "messages", false, true, "messages", "Number of messages per session");
		registerCommandLineSwitch('r', "rate", false, true, "rate", "The number of milliseconds between the start of each message");
		registerCommandLineSwitch('h', "host", false, true, "host", "Host to connect to");
		registerCommandLineSwitch('p', "port", false, true, "port", "The base Port");
		registerCommandLineSwitch('a', "add-port", true, true, "addPort", "The multiplier of the instance (1-4) which is added to the base port to get the port used");
		registerCommandLineSwitch('v', "protocol", true, true, "protocol", "The protocol version to use (0-8)");
		registerCommandLineSwitch('o', "output", true, true, "output", "Directory for output");
		registerCommandLineSwitch('c', "command", false, true, "command", "The message type: eg. C0, C1, A0, etc.");
		registerCommandLineSwitch('i', "propertiesFile", true, true, "properties-file", "The properties file to use");
	}

	@Override
	protected void registerDefaultActions() {
		// no actions
	}
	@Override
	public void run(String[] args) {
		Map<String,Object> argMap;
		try {
			argMap = parseArguments(args);
		} catch(IOException e) {
			finishAndExit(e.getMessage(), 100, true, null);
			return;
		}
		final int messages;
		final String host;
		final int port;
		final int protocol;
		final int addPort;
		final double rate;
		String outputDir;
		final String command;
		try {
			messages = ConvertUtils.getInt(argMap.get("messages"));
			rate = ConvertUtils.getDouble(argMap.get("rate"));
			host = ConvertUtils.getStringSafely(argMap.get("host"));
			port = ConvertUtils.getInt(argMap.get("port"));
			addPort = ConvertUtils.getInt(argMap.get("addPort"), 0);
			protocol = ConvertUtils.getIntSafely(argMap.get("protocol"), 7);
			outputDir = ConvertUtils.getString(argMap.get("output"), "./");
			if(!outputDir.endsWith("/")){
				outputDir+="/";
			}
			command = ConvertUtils.getString(argMap.get("command"), true).toUpperCase();
		} catch(ConvertException e) {
			finishAndExit(e.getMessage(), 200, true, null);
			return;
		} catch(NumberFormatException e) {
			finishAndExit("Could not convert argument to a number: " + e.getMessage(), 250, true, null);
			return;
		}
		Properties properties;
		try {
			properties = getProperties(ConvertUtils.getStringSafely(argMap.get("properties-file")), getClass(), null);
		} catch(IOException e) {
			finishAndExit("Could not read properties file", 120, e);
			return;
		}
		for(Map.Entry<String,Object> entry : argMap.entrySet()) {
			properties.put(entry.getKey(), entry.getValue());
		}
		
		final Specifics specifics;
		try {
			specifics = getSpecifics(command, properties, messages);
		} catch(Exception e) {
			finishAndExit("Could not read create Specifics", 150, e);
			return;
		}
		if(specifics == null) {
			finishAndExit("Command '" + command + "' is not implemented", 350, true, null);
			return;
		}
		
		final AtomicInteger index = new AtomicInteger();
		File file = new File(outputDir+specifics.getOutputFilePrefix() + new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date()) + ".csv");
		final TaskListener taskListener;
		try {
			taskListener = new WriteCSVTaskListener(new PrintWriter(new FileOutputStream(file)));
		} catch(FileNotFoundException e) {
			finishAndExit(e.getMessage(), 800, true, null);
			return;
		}
		final Runnable executeTask = new Runnable() {
			@Override
			public void run() {
				int i = index.incrementAndGet();
				int p;
				if(addPort != 0)
					p = port + (addPort * ((i-1) % 4));
				else
					p = port;
				
				specifics.execute(i, host, p, protocol, taskListener);
			}
		};
		final ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(1500, new CustomThreadFactory("Executor-", false));
		//
		final long startTime = System.currentTimeMillis() + 1000;
		DEX_TIME_BASE = startTime - (startTime % 1000);
		for(int i = 0; i < messages; i++) {
			long target = (int) (i * rate) + startTime;
			long time = System.currentTimeMillis();
			if(time + 2000 < target)
				try {
					Thread.sleep(target - time - 2000);
				} catch(InterruptedException e) {
				}
			executor.schedule(executeTask, target - time, TimeUnit.MILLISECONDS);
		}
		/*
		final AtomicInteger iteration = new AtomicInteger();
		final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(10, new CustomThreadFactory("Scheduler-", false));
		Runnable scheduleTask = new Runnable() {
			@Override
			public void run() {
				if(iteration.incrementAndGet() > messages) {
					scheduler.shutdown();
					throw new RuntimeException("All done");
				}
				executor.execute(executeTask);
			}
		};
		
		scheduler.scheduleAtFixedRate(scheduleTask, 1000, rate, TimeUnit.MILLISECONDS);
		try {
			scheduler.awaitTermination(10, TimeUnit.MINUTES);
		} catch(InterruptedException e) {
			log.info("While waiting for termination", e);
		}
		*/
		executor.shutdown();
		try {
			executor.awaitTermination(10, TimeUnit.MINUTES);
		} catch(InterruptedException e) {
			log.info("While waiting for termination", e);
		}
		taskListener.flush();
		String path;
		try {
			path = file.getCanonicalPath();
		} catch(IOException e) {
			path = file.getAbsolutePath();
		}
		log.info("Wrote task stats to '" + path + "'");
		Log.finish();
	}

	protected Specifics getSpecifics(String command, Properties properties, int messages) throws Exception {
		if("C0".equals(command)) {
			final InitializationReason reasonCode = InitializationReason.COMMUNICATION_FAILURE;
			final long protocolComplianceRevision = 4001005;
			final int propertyListVersion = 1;
			final DeviceType deviceType = DeviceType.EDGE;
			final String deviceFirmwareVersion = "Client Emulator Performance Test 1.1 firmware version";
			final String deviceInfo = "Client Emulator 1.1 Device Info";
			final String terminalInfo = "Client Emulator 1.1 Terminal Info";
			final String deviceName = "TD000000";
			final byte[] encryptionKey = ByteArrayUtils.fromHex("6DF7E5A6FBC84C50A79F76762826C0DC");
			
			return new Specifics() {			
				@Override
				public String getOutputFilePrefix() {
					return "InitPerformanceTest_";
				}
				
				protected MessageData getMessageData(int index) {
					String serial = "EE19" + StringUtils.pad(Integer.toString(index), '0', 7, Justification.RIGHT);
					MessageData_C0 dataC0 = new MessageData_C0();
					dataC0.setDeviceFirmwareVersion(deviceFirmwareVersion);
					dataC0.setDeviceInfo(deviceInfo);
					dataC0.setDeviceSerialNum(serial);
					dataC0.setDeviceType(deviceType);
					dataC0.setPropertyListVersion(propertyListVersion);
					dataC0.setProtocolComplianceRevision(protocolComplianceRevision);
					dataC0.setReasonCode(reasonCode);
					dataC0.setTerminalInfo(terminalInfo);
					return dataC0;
				}
				
				@Override
				public void execute(int index, String host, int port, int protocol, TaskListener taskListener) {
					long startTime = System.currentTimeMillis();
					ClientEmulator ce = new ClientEmulator(host, port, deviceName, encryptionKey);
					MessageData data = getMessageData(index);
					String details;
					try {
						ce.startCommunication(protocol);
						try {
							MessageData_CB replyCB = (MessageData_CB)ce.sendMessage(data);
							details = "Sent:\n"+data+"\nReceived:\n"+replyCB;
						} finally {
							ce.stopCommunication();
						}
					} catch(Exception e) {
						details="ERROR:\n"+e.getMessage();
						log.warn("Error sending message: " + data, e);
					}
					long endTime = System.currentTimeMillis();
					taskListener.taskRan("SentMessage-C0", details, startTime, endTime);
				}
			};
		}
		if("AUTHSALE".equals(command)) {
			final String card = ";5454545454545454=11011015432112345601?";
			return new AbstractNonInitSpecifics(properties, messages) {			
				@Override
				public String getOutputFilePrefix() {
					return "AuthSalePerformanceTest_";
				}
				
				protected AuthorizeMessageData getAuthData(int index) {
					MessageData_C2 dataC2 = new MessageData_C2();
					dataC2.setAmount(BigDecimal.valueOf(1 + (index % 512)));
					dataC2.setCardReaderType(CardReaderType.GENERIC);
					dataC2.setEntryType(EntryType.SWIPE);
					dataC2.setTransactionId(lastMasterIds.get(index%deviceNames.size()).incrementAndGet());
					((GenericTracksCardReader)dataC2.getCardReader()).setAccountData2(card);
					return dataC2;
				}
				protected MessageData getSaleData(AuthorizeMessageData authData, int index) {
					BigDecimal saleAmount = ConvertUtils.convertSafely(BigDecimal.class, authData.getAmount(), BigDecimal.ONE);
					MessageData_C4 md = new MessageData_C4();
					md.setBatchId(1);
					md.setDirection(MessageDirection.CLIENT_TO_SERVER);
					md.setReceiptResult(ReceiptResult.UNAVAILABLE);
					md.setSaleResult(SaleResult.SUCCESS);
					Calendar cal = Calendar.getInstance();
					//cal.setTimeInMillis(transactionId*1000);
					md.setSaleSessionStart(cal);
					md.setSaleType(SaleType.ACTUAL);
					md.setTransactionId(authData.getTransactionId());
					md.setLineItemFormat((byte)0);
					Format0LineItem li = (Format0LineItem)md.addLineItem();
					li.setComponentNumber(1);
					li.setItem(200);
					li.setDescription(StringUtils.toHex(index));
					li.setDuration(2);
					li.setPrice(saleAmount);
					li.setQuantity(1);
					li.setSaleResult(SaleResult.SUCCESS);
					md.setSaleAmount(saleAmount);
					return md;
				}
				
				@Override
				public void execute(int index, String host, int port, int protocol, TaskListener taskListener) {
					ClientEmulator ce = new ClientEmulator(host, port+1, deviceNames.get(index%deviceNames.size()), encryptionKeys.get(index%deviceNames.size()));
					AuthorizeMessageData authData = getAuthData(index);
					MessageData reply = sendData(ce, authData, protocol, taskListener);
					if(reply == null)
						return;
					boolean doSale;
					switch(reply.getMessageType()) {
						case AUTH_RESPONSE_4_1:
							AuthResponseTypeData artd = ((MessageData_C3)reply).getAuthResponseTypeData();
							AuthResponseType art = ((MessageData_C3)reply).getAuthResponseType();
							switch(art) {
								case AUTHORIZATION:
									AuthResponseCodeC3 rc = ((AuthorizationAuthResponse)artd).getResponseCode();
									switch(rc) {
										case SUCCESS: case CONDITIONAL_SUCCESS:
											doSale = true;
											break;
										default:
											doSale = false;
											log.info("Cannot process sale because auth was rejected");
									}
									break;
								default:
									doSale = false;
									log.info("Cannot process sale because auth response was " + art);
							}
							break;
						default:
							doSale = false;
							log.info("Cannot process sale because response was " + reply.getMessageType());
					}
					if(doSale) {
						ce.setPort(port);
						MessageData saleData = getSaleData(authData, index);
						sendData(ce, saleData, protocol, taskListener);
					}
				}
			};
		}
		if("ACTIVATE".equals(command)) {
			return new AbstractSingleMessageSpecifics(properties, messages) {			
				@Override
				public String getOutputFilePrefix() {
					return "ActivationPerformanceTest_";
				}
				@Override
				protected MessageData getData(int index) {
					MessageData_CA dataCA = new MessageData_CA();
					dataCA.setRequestType(GenericRequestType.ACTIVATION_REQUEST);		
					return dataCA;
				}
			};
		}
		if("FILETRANSFER".equals(command)) {
			return new AbstractMultiMessageSpecifics(properties, messages) {			
				@Override
				public String getOutputFilePrefix() {
					return "FileTransferTest_";
				}
				@Override
				protected MessageData getData(int index, int sequence, MessageData previousReply) {
					final byte[] content = new byte[200];
					Arrays.fill(content, (byte)(33 + index % 90));
					CRCType.Calculator crcCalc = CRCType.CRC16FMM.newCalculator();
					crcCalc.update(content);
					switch(sequence) {
						case 0:
							MessageData_CA dataCA = new MessageData_CA();
							dataCA.setRequestType(GenericRequestType.UPDATE_STATUS_REQUEST);		
							((UpdateStatusRequest)dataCA.getRequestTypeData()).setStatusCodeBitmap(0x04);		
							return dataCA;
						case 1:
							MessageData_C8 dataC8 = new MessageData_C8();
							dataC8.setCrc(new CRC(CRCType.CRC16FMM, crcCalc.getValue()));
							dataC8.setCreationTime(Calendar.getInstance());
							dataC8.setEventId(System.currentTimeMillis() / 1000);
							dataC8.setFileName("Test File #" + index);
							dataC8.setFilesRemaining(1);
							dataC8.setFileType(FileType.LOG_FILE);
							dataC8.setTotalBytes(content.length);
							dataC8.setTotalPackets(1);
							return dataC8;
						case 2:
							MessageData_C9 dataC9 = new MessageData_C9();
							dataC9.setPacketNum(0);
							dataC9.setContent(content);
							return dataC9;
						default:
							return null;
					}
					
				}
			};
		}
		if("DEX".equals(command)) {
			return new AbstractMultiMessageSpecifics(properties, messages) {
				@Override
				public String getOutputFilePrefix() {
					return "DEXTest_";
				}

				@Override
				protected MessageData getData(int index, int sequence, MessageData previousReply) {
					StringBuilder sb = new StringBuilder();
					final long eventTime = DEX_TIME_BASE + (index * 1000);
					sb.append("DEX-").append("????????").append('-').append(DEX_HEADER_DATE_FORMAT.format(new Date(eventTime))).append("-SCHEDULED--SC00\r\nDXS*\r\n");
					/*sb.append("DXS*API0000001*VA*V1/2*1\r\nST*001*0001\r\n");
					sb.append("ID1*API0*STXXX*0011*0**0\r\nID4*2*3030*0\r\nID5*060930*0750\r\nID7***API\r\nCB1*API0*ST/130*0015\r\nVA1*38445*1051*0*0*0*0*0*0\r\nVA2*0*1*0*0\r\nVA3*5*1*0*0\r\nTA2*0*0*0*0\r\nCA1*0*0*0CA2*7980*375*0*0\r\nCA3*0*0*0*0*4220*0*3920*3*0*3683\r\nCA4*0*0*17290*15840\r\nCA7*0*0*0*0\r\nCA9*0*6780\r\nCA10*0*0\r\nCA15*0\r\nBA1*0*0*0\r\nDA1*EE100018426*EportEdge*3030\r\nDA2*30465*676*0*0\r\nDA4*0*0\r\nPA1*CAN**CAN\r\nPA2*18002*235\r\n");
					sb.append("PA1*100*150*100*0\r\nPA2*261*22790*0*0*0*0\r\nPA4*65536\r\nPA5*060930*0750\r\nPA1*101*5*101*0\r\nPA2*0*0*0*0*0*0\r\nPA4*0\r\nPA5*000000*0000\r\nPA1*102*150*102*0\r\nPA2*82*6855*0*0*0*0\r\nPA4*0\r\nPA5*060804*2255\r\nPA1*103*150*103*0\r\nPA2*0*0*0*0*0*0\r\nPA4*0\r\nPA5*000000*0000\r\nPA1*104*5*104*0\r\nPA2*32*160*0*0*0*0\r\nPA4*0\r\nPA5*050604*0356\r\nPA1*105*5*105*0\r\nPA2*0*0*0*0*0*0\r\nPA4*0\r\nPA5*000000*0000\r\nPA1*106*5*106*0");
					sb.append("EA5*050325*0451\r\nEA7*0*185\r\nG85*865\r\nSE*0*0001\r\nDXE*1*1\r\n");
					*/
					//sb.append("DXS*0000000000*VA*V1/1*1\r\nST*001*0001\r\nID1*0**9985***\r\nID4*2*1\r\nVA1*4752880*52934*4752880*52934\r\nVA2*0*0*0*0\r\nCA1*000802120275*VN4510 MDB  *313**0\r\nBA1*002080104256*VN2500/AE24 *3460**0\r\nCA2*0*0*0*0\r\nCA3*5269670*1069195*511875*36886*5269670*1069195*511875*36886\r\nCA4*531120*12535*531120*12535\r\nCA5*0\r\nCA6*0\r\nDA1*0*0*0**0\r\nDA2*5165*38*5165*38*0\r\nTA2*0*0*0*0\r\nLS*0001\r\nPA1*1*135*\r\nPA2*15986*1603250*15986*1603250\r\nPA5*120830*1905*2856\r\nPA1*2*135*\r\nPA2*9338*937375*9338*937375\r\nPA5*120801*2126*1767\r\nPA1*3*125*\r\nPA2*3138*316510*3138*316510\r\nPA5*120727*1639*458\r\nPA1*4*135*\r\nPA2*2367*228755*2367*228755\r\nPA5*120727*1715*570\r\nPA1*5*135*\r\nPA2*3138*315480*3138*315480\r\nPA5*120727*1802*1302\r\nPA1*6*125*\r\nPA2*1193*148975*1193*148975\r\nPA5*120727*1802*590\r\nPA1*7*150*\r\nPA2*6530*410625*6530*410625\r\nPA5*120727*1804*815\r\nPA1*8*150*\r\nPA2*8567*545625*8567*545625\r\nPA5*120727*1804*1285\r\nPA1*9*150*\r\nPA2*2677*246285*2677*246285\r\nPA5*120801*0501*2470\r\nLE*0001\r\nEA2*DO*1935\r\nEA2*CR**0\r\nEA7*32*32\r\nMA5*SWITCH*UNLOCK*1,4,6**10\r\nMA5*SEL1*1*15481*16300\r\nMA5*SEL2*2*8837*9408\r\nMA5*SEL3*3*2561*2660\r\nMA5*SEL4*4*2045*2188\r\nMA5*SEL5*5*2830*3266\r\nMA5*SEL6*6*1107*1121\r\nMA5*SEL7*7*6079*6261\r\nMA5*SEL8*8*7962*8279\r\nMA5*SEL9*9*2602*3537\r\nMA5*ERROR*CJ9*\r\nMA5*ERROR*CJ1*\r\nMA5*ERROR*DS*\r\nMA5*ERROR*DS*\r\nMA5*ERROR*DS*\r\nMA5*ERROR*DS*\r\nMA5*ERROR*DS*\r\nMA5*ERROR*TS*\r\nMA5*ERROR*TS*\r\nMA5*ERROR*TS*\r\nMA5*ERROR*TS*\r\nMA5*ERROR*TS*\r\nMA5*ERROR*BOPN*\r\nMA5*ERROR*BOPN*\r\nMA5*ERROR*BOPN*\r\nMA5*ERROR*BOPN*\r\nMA5*ERROR*BOPN*\r\nMA5*TUBE1**20*104*52*11\r\nG85*093C\r\nSE*77*0001\r\nDXE*1*1");
					sb.append("Test #").append(index);
					sb.append("\r\nDXE*1*1\r\n");
					final byte[] content = sb.toString().getBytes(ProcessingConstants.US_ASCII_CHARSET);
					CRCType.Calculator crcCalc = CRCType.CRC16FMM.newCalculator();
					crcCalc.update(content);
					int len = 1000;
					switch(sequence) {
						case 0:
							MessageData_CA dataCA = new MessageData_CA();
							dataCA.setRequestType(GenericRequestType.UPDATE_STATUS_REQUEST);
							((UpdateStatusRequest) dataCA.getRequestTypeData()).setStatusCodeBitmap(0x04);
							return dataCA;
						case 1:
							MessageData_C8 dataC8 = new MessageData_C8();
							dataC8.setCrc(new CRC(CRCType.CRC16FMM, crcCalc.getValue()));
							dataC8.setCreationTime(Calendar.getInstance());
							dataC8.setEventId(eventTime / 1000);
							// dataC8.setFileName("Test File #" + index);
							dataC8.setFilesRemaining(1);
							dataC8.setFileType(FileType.DEX_FILE);
							dataC8.setTotalBytes(content.length);
							dataC8.setTotalPackets((int) Math.ceil(content.length / (double) len));
							return dataC8;
						case 2:
							MessageData_C9 dataC9 = new MessageData_C9();
							dataC9.setPacketNum(0);
							if(content.length <= len)
								dataC9.setContent(content);
							else {
								byte[] tmp = new byte[len];
								System.arraycopy(content, 0, tmp, 0, tmp.length);
								dataC9.setContent(tmp);
							}
							return dataC9;
						default:
							if((sequence - 2) * len < content.length) {
								dataC9 = new MessageData_C9();
								dataC9.setPacketNum(0);
								byte[] tmp = new byte[len];
								System.arraycopy(content, (sequence - 2) * len, tmp, 0, tmp.length);
								dataC9.setContent(tmp);
								return dataC9;
							}
							return null;
					}

				}
			};
		}
		if("USR".equals(command)) {
			return new AbstractMultiMessageSpecifics(properties, messages) {			
				@Override
				public String getOutputFilePrefix() {
					return "USRTest_";
				}
				@Override
				protected MessageData getData(int index, int sequence, MessageData previousReply) {
					switch(sequence) {
						case 0:
							MessageData_CA dataCA = new MessageData_CA();
							dataCA.setRequestType(GenericRequestType.UPDATE_STATUS_REQUEST);		
							((UpdateStatusRequest)dataCA.getRequestTypeData()).setStatusCodeBitmap(0x00);		
							return dataCA;
						default:
							if(previousReply instanceof MessageData_CB) {
								MessageData_CB replyCB = (MessageData_CB)previousReply;
								switch(replyCB.getServerActionCode()) {
									case DISCONNECT: case NO_ACTION: case STOP_FILE_TRANSFER:
										return null;
									case PROCESS_PROP_LIST: case PERFORM_SETTLEMENT: case REBOOT: case REINITIALIZE: case SEND_COMP_IDENTITY: case UPLOAD_PROPERTY_VALUE_LIST:
										MessageData_CB dataCB = new MessageData_CB();
										dataCB.setClientActionCode(GenericResponseClientActionCode.NO_ACTION);
										dataCB.setResultCode(GenericResponseResultCode.OKAY);
										return dataCB;
									default:
										dataCA = new MessageData_CA();
										dataCA.setRequestType(GenericRequestType.UPDATE_STATUS_REQUEST);		
										((UpdateStatusRequest)dataCA.getRequestTypeData()).setStatusCodeBitmap(0x00);		
										return dataCA;
								}
							} else {
								MessageData_CB dataCB = new MessageData_CB();
								dataCB.setClientActionCode(GenericResponseClientActionCode.NO_ACTION);
								dataCB.setResultCode(GenericResponseResultCode.OKAY);
								return dataCB;
							}						
					}					
				}
			};
		}
		return null;
	}
	
	protected abstract class AbstractNonInitSpecifics implements Specifics {
		protected final List<String> deviceNames = new ArrayList<String>();
		protected final List<byte[]> encryptionKeys = new ArrayList<byte[]>();
		protected final List<AtomicLong> lastMasterIds = new ArrayList<AtomicLong>();
		protected AbstractNonInitSpecifics(Properties properties, int messages) throws ServiceException, SQLException, DataLayerException, ConvertException {
			Base.configureDataSourceFactory(properties, null);
			Base.configureDataLayer(properties);
			Results results = DataLayerMgr.executeQuery("GET_ACTIVE_DEVICES", new Object[] {messages});
			while(results.next()) {
				deviceNames.add(results.getValue("DEVICE_NAME", String.class));
				encryptionKeys.add(results.getValue("ENCRYPTION_KEY", byte[].class));
				lastMasterIds.add(new AtomicLong(results.getValue("MASTER_ID", Long.class)));	
			}
		}
		protected MessageData sendData(ClientEmulator ce, MessageData data, int protocol, TaskListener taskListener) {
			long startTime = System.currentTimeMillis();
			String details;
			MessageData reply;
			try {
				ce.startCommunication(protocol);
				try {
					reply = ce.sendMessage(data);
					details = "Sent:\n"+data+"\nReceived:\n"+reply;
				} finally {
					ce.stopCommunication();
				}
			} catch(Exception e) {
				details="ERROR:\n"+e.getMessage();
				reply = null;
				log.warn("Error sending message: " + data, e);
			}
			long endTime = System.currentTimeMillis();
			taskListener.taskRan("SentMessage-" + data.getMessageType().getHex(), details, startTime, endTime);
			return reply;
		}
		protected MessageData sendDataInSession(ClientEmulator ce, MessageData data, int protocol, TaskListener taskListener) {
			long startTime = System.currentTimeMillis();
			String details;
			MessageData reply;
			try {
				reply = ce.sendMessage(data);
				details = "Sent:\n"+data+"\nReceived:\n"+reply;
			} catch(Exception e) {
				details="ERROR:\n"+e.getMessage();
				reply = null;
				log.warn("Error sending message: " + data, e);
			}
			long endTime = System.currentTimeMillis();
			taskListener.taskRan("SentMessage-" + data.getMessageType().getHex(), details, startTime, endTime);
			return reply;
		}
	}
	
	protected abstract class AbstractSingleMessageSpecifics extends AbstractNonInitSpecifics {
		protected AbstractSingleMessageSpecifics(Properties properties, int messages) throws ServiceException, SQLException, DataLayerException, ConvertException {
			super(properties, messages);
		}

		@Override
		public void execute(int index, String host, int port, int protocol, TaskListener taskListener) {
			ClientEmulator ce = new ClientEmulator(host, port, deviceNames.get(index%deviceNames.size()), encryptionKeys.get(index%deviceNames.size()));
			sendData(ce, getData(index), protocol, taskListener);
		}

		protected abstract MessageData getData(int index) ;
	}
	
	protected abstract class AbstractMultiMessageSpecifics extends AbstractNonInitSpecifics {
		protected AbstractMultiMessageSpecifics(Properties properties, int messages) throws ServiceException, SQLException, DataLayerException, ConvertException {
			super(properties, messages);
		}

		@Override
		public void execute(int index, String host, int port, int protocol, TaskListener taskListener) {
			ClientEmulator ce = new ClientEmulator(host, port, deviceNames.get(index%deviceNames.size()), encryptionKeys.get(index%deviceNames.size()));
			try {
				try {
					ce.startCommunication(protocol);
					MessageData data;
					MessageData reply = null;
					for(int s = 0; (data=getData(index, s, reply)) != null; s++)
						reply = sendDataInSession(ce, data, protocol, taskListener);
				} finally {
					ce.stopCommunication();
				}
			} catch(Exception e) {
				String details="ERROR:\n"+e.getMessage();
				log.warn("Error opening session to " + host + ":" + port, e);
				long endTime = System.currentTimeMillis();
				taskListener.taskRan("SentMessage-???", details, endTime, endTime);	
			}	
		}

		protected abstract MessageData getData(int index, int sequence, MessageData previousReply) ;
	}
}
