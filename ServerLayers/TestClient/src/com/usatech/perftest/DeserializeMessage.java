/**
 *
 */
package com.usatech.perftest;

import java.io.IOException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;

import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.GenericResponseServerActionCode;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageDataFactory;
import com.usatech.layers.common.messagedata.MessageData_CB;
import com.usatech.layers.common.messagedata.MessageData_CB.PropertyValueListAction;
import com.usatech.layers.common.messagedata.MessageDirection;

import simple.io.ByteArrayUtils;
import simple.lang.InvalidByteArrayException;
import simple.lang.InvalidByteValueException;
import simple.text.StringUtils;

/**
 * @author Brian S. Krug
 *
 */
public class DeserializeMessage {

	/**
	 * @param args
	 * @throws IOException
	 * @throws InvalidByteArrayException
	 * @throws InvalidByteValueException
	 * @throws IllegalArgumentException
	 * @throws BufferUnderflowException
	 * @throws ParseException
	 */
	public static void main(String[] args) throws BufferUnderflowException, IllegalArgumentException, InvalidByteValueException, InvalidByteArrayException, IOException, ParseException {
		String[] messageHex = new String[] {
 "00C794151a7954443030313134342D4346472D313837353738", "",
				//"", ""
		};
		ByteBuffer buffer = ByteBuffer.allocate(1100);
		int i = 0;
		for(String m : messageHex) {
			//MessageData message = MessageDataUtils.readMessage(ByteBuffer.wrap(ByteArrayUtils.fromHex(m)), MessageDirection.SERVER_TO_CLIENT, DeviceType.EDGE);
			//MessageDirection direction = MessageDirection.SERVER_TO_CLIENT;
			MessageDirection direction = ((i++ % 2) == 0 ? MessageDirection.CLIENT_TO_SERVER : MessageDirection.SERVER_TO_CLIENT);
			if(m.length() == 0) continue;
			MessageData message = MessageDataFactory.readMessage(ByteBuffer.wrap(ByteArrayUtils.fromHex(m)), direction, DeviceType.EDGE);
			switch(direction) {
				case CLIENT_TO_SERVER:
					System.out.print("=> ");
					break;
				case SERVER_TO_CLIENT:
					System.out.print("<= ");
					break;
			}
			System.out.print(m);
			System.out.println(':');
			System.out.println(message);
			if(message.getMessageType() == MessageType.GENERIC_RESPONSE_4_1) {
				MessageData_CB messageCB = (MessageData_CB) message;
				if(messageCB.getServerActionCode() == GenericResponseServerActionCode.PROCESS_PROP_LIST)
					System.out.println(((PropertyValueListAction) messageCB.getServerActionCodeData()).getPropertyValues());
			}
			System.out.println();
			buffer.clear();
			message.writeData(buffer, false);
			buffer.flip();
			String h = StringUtils.toHex(buffer, buffer.position(), buffer.remaining());
			if(!m.equalsIgnoreCase(h)) {
				System.out.print("!!! Read / Write Incompatible: ");
				System.out.println(h);			
			}
		}
	}

}
