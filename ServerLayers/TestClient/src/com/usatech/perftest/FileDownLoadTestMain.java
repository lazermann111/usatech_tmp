package com.usatech.perftest;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

import simple.app.Base;
import simple.app.MainWithConfig;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;

import com.usatech.test.ClientEmulator;

public class FileDownLoadTestMain extends MainWithConfig {
	private static final Log log = Log.getLog();

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new FileDownLoadTestMain().run(args);
	}

	@Override
	protected void registerDefaultCommandLineArguments() {
		registerCommandLineSwitch('p', "propertiesFile", true, true, "properties-file", "The properties file to use");
		registerCommandLineSwitch('t', "threads", false, true, "threads", "Number of threads to run");
		registerCommandLineSwitch('s', "sessions", false, true, "sessions", "Number of sessions per thread");
		registerCommandLineSwitch('h', "host", false, true, "host", "Host to connect to");
		registerCommandLineSwitch('P', "port", false, true, "port", "Port");
		registerCommandLineSwitch('v', "protocol", true, true, "protocol", "The protocol version to use (0-8)");
		registerCommandLineSwitch('o', "output", true, true, "output", "Directory for output");
	}

	@Override
	protected void registerDefaultActions() {
		// no actions
	}
	@Override
	public void run(String[] args) {
		Map<String,Object> argMap;
		try {
			argMap = parseArguments(args);
		} catch(IOException e) {
			finishAndExit(e.getMessage(), 100, true, null);
			return;
		}
		int threads;
		final int sessions;
		final String host;
		final int port;
		final int protocol;
		String outputDir;
		try {
			threads = ConvertUtils.getInt(argMap.get("threads"));
			sessions = ConvertUtils.getInt(argMap.get("sessions"));
			host = ConvertUtils.getStringSafely(argMap.get("host"));
			port = ConvertUtils.getInt(argMap.get("port"));
			protocol = ConvertUtils.getIntSafely(argMap.get("protocol"), 7);
			outputDir = ConvertUtils.getString(argMap.get("output"), "./");
			if(!outputDir.endsWith("/")){
				outputDir+="/";
			}
		} catch(ConvertException e) {
			finishAndExit(e.getMessage(), 200, true, null);
			return;
		} catch(NumberFormatException e) {
			finishAndExit("Could not convert command to a hex number: " + e.getMessage(), 250, true, null);
			return;
		}
		Properties properties;
		try {
			properties = getProperties(ConvertUtils.getStringSafely(argMap.get("properties-file")), getClass(), null);
		} catch(IOException e) {
			finishAndExit("Could not read properties file", 120, e);
			return;
		}
		for(Map.Entry<String,Object> entry : argMap.entrySet()) {
			properties.put(entry.getKey(), entry.getValue());
		}
		try {
			Base.configureDataSourceFactory(properties, null);
		} catch(ServiceException e) {
			finishAndExit(e.getMessage(), 300, true, null);
			return;
		}
		try {
			Base.configureDataLayer(properties);
		} catch(ServiceException e) {
			finishAndExit(e.getMessage(), 400, true, null);
			return;
		}
		Results results;
		try {
			results = DataLayerMgr.executeQuery("GET_AUTH_DEVICES", new Object[] {1 + (threads * sessions / 3)});
		} catch(SQLException e) {
			finishAndExit(e.getMessage(), 500, true, null);
			return;
		} catch(DataLayerException e) {
			finishAndExit(e.getMessage(), 600, true, null);
			return;
		}
		final List<String> deviceNames = new ArrayList<String>();
		final List<byte[]> encryptKeys = new ArrayList<byte[]>();
		while(results.next()) {
			try {
				deviceNames.add(results.getValue("DEVICE_NAME", String.class));
				encryptKeys.add(results.getValue("ENCRYPTION_KEY", byte[].class));
			} catch(ConvertException e) {
				finishAndExit(e.getMessage(), 700, true, null);
				return;
			}
		}
		final AtomicInteger index = new AtomicInteger();
		class Runner extends Thread {
			public Runner() {
				super();
			}

			@Override
			public void run() {
				for(int s = 0; s < sessions; s++) {
					int deviceIndex=index.getAndIncrement() % deviceNames.size();
					ClientEmulator ce = new ClientEmulator(host, port, deviceNames.get(deviceIndex), encryptKeys.get(deviceIndex));
					log.info("Using device '" + ce.getEvNumber() + "'");
					try {
						ce.startCommunication(protocol);
						ce.testGenReqUSR();
						ce.testFileDownload();
						ce.testShortFileTransferPropertyIndexList();
						ce.stopCommunication();
					} catch(Exception e) {
						log.warn("Error during session for '" + ce.getEvNumber() + "' for message " + ce.getMessageNumber(), e);
					}
				}
			}
		};
		log.info("Starting up " + threads + " threads...");
		Thread[] threadArray = new Thread[threads];
		for(int i = 0; i < threads; i++) {
			threadArray[i] = new Runner();
			threadArray[i].start();
		}
		for(int i = 0; i < threads; i++) {
			try {
				threadArray[i].join();
			} catch(InterruptedException e) {
				log.warn("Thread interrupted", e);
			}
		}
		Log.finish();
	}
}
