package com.usatech.perftest;

import java.nio.ByteBuffer;
import java.util.Collections;

import org.junit.Test;

import simple.bean.ConvertException;
import simple.text.StringUtils;
import simple.util.CollectionUtils;
import simple.util.OptimizedIntegerRangeSet;

import com.usatech.layers.common.constants.AuthResponseActionCode;
import com.usatech.layers.common.constants.AuthResponseType;
import com.usatech.layers.common.constants.DeviceProperty;
import com.usatech.layers.common.constants.GenericResponseResultCode;
import com.usatech.layers.common.constants.GenericResponseServerActionCode;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageData_C3;
import com.usatech.layers.common.messagedata.MessageData_C3.ProcessPropertyListCommand;
import com.usatech.layers.common.messagedata.MessageData_CB;
import com.usatech.layers.common.messagedata.MessageData_CB.PropertyValueListAction;
import com.usatech.layers.common.messagedata.MessageData_CB.ReconnectAction;
import com.usatech.layers.common.messagedata.MessageData_CB.TraceEventsAction;
import com.usatech.layers.common.messagedata.MessageDirection;

public class MessageTest {

	/**
	 * @param args
	 * @throws ConvertException
	 */
	public static void main(String[] args) throws ConvertException {
		printCBReboot();
	}

	protected static void printCBReboot() {
		MessageData_CB cbData = new MessageData_CB();
		cbData.setResultCode(GenericResponseResultCode.SUSPENDED);
		cbData.setServerActionCode(GenericResponseServerActionCode.REBOOT);
		long rejectMs = 60 * 60 * 1000L;
		long reconnectTime = rejectMs / 1000 + 5 * 60;
		((ReconnectAction) cbData.getServerActionCodeData()).setReconnectTime((int) (reconnectTime > 65535 ? 0 : reconnectTime));
		printMessage(cbData);
	}

	protected static void printC3() {
		long time = System.currentTimeMillis();
		MessageData_C3 replyC3 = new MessageData_C3();
		replyC3.setMessageId(time / 1000);
		replyC3.setDirection(MessageDirection.SERVER_TO_CLIENT);
		replyC3.setAuthResponseType(AuthResponseType.CONTROL);
		replyC3.setCommandCode(AuthResponseActionCode.PROCESS_PROP_LIST);
		((ProcessPropertyListCommand) replyC3.getCommandCodeData()).setPropertyValues(Collections.singletonMap(DeviceProperty.MASTER_ID.getValue(), String.valueOf(time / 1000)));
		printMessage(replyC3);
		
	}
	protected static void printMessage(MessageData data) {
		ByteBuffer buffer = ByteBuffer.allocate(1100);
		buffer.clear();
		data.writeData(buffer, false);
		buffer.flip();
		String h = StringUtils.toHex(buffer, buffer.position(), buffer.remaining());
		System.out.println("Hex:");
		System.out.print("\t");
		System.out.println(h);
	}

	@Test
	public void junit_printCBAuthAmountProp() throws Exception {
		MessageData_CB dataCB = new MessageData_CB();
		dataCB.setServerActionCode(GenericResponseServerActionCode.PROCESS_PROP_LIST);
		dataCB.setResultCode(GenericResponseResultCode.OKAY);
		((PropertyValueListAction) dataCB.getServerActionCodeData()).setPropertyValues(Collections.singletonMap(1200, "1234"));

		printMessage(dataCB);
	}

	@Test
	public void junit_printCBTraceEvents() throws Exception {
		MessageData_CB dataCB = new MessageData_CB();
		dataCB.setDirection(MessageDirection.SERVER_TO_CLIENT);
		dataCB.setServerActionCode(GenericResponseServerActionCode.TRACE_EVENTS);
		dataCB.setResultCode(GenericResponseResultCode.OKAY);
		TraceEventsAction tea = (TraceEventsAction) dataCB.getServerActionCodeData();
		tea.setMaxBytes(200 * 1024L);
		tea.setMaxSeconds(4 * 60 * 60L);
		tea.setOptionsBitmap(0x01);
		printMessage(dataCB);
	}

	@Test
	public void junit_printCBAllProps() throws Exception {
		MessageData_CB dataCB = new MessageData_CB();
		dataCB.setDirection(MessageDirection.SERVER_TO_CLIENT);
		dataCB.setServerActionCode(GenericResponseServerActionCode.PROCESS_PROP_LIST);
		dataCB.setResultCode(GenericResponseResultCode.OKAY);
		PropertyValueListAction pvla = (PropertyValueListAction) dataCB.getServerActionCodeData();
		OptimizedIntegerRangeSet props = new OptimizedIntegerRangeSet();
		props.addRange(0, 2000);
		pvla.setPropertyValues(CollectionUtils.singleValueMap(props, (String) null));
		printMessage(dataCB);
	}
}
