package com.usatech.perftest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import simple.app.MainWithConfig;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.event.TaskListener;
import simple.event.WriteCSVTaskListener;
import simple.io.ByteArrayUtils;
import simple.io.Log;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;
import simple.util.concurrent.CustomThreadFactory;

import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.InitializationReason;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageData_CB;
import com.usatech.test.ClientEmulator;

public class InitTestMain extends MainWithConfig {
	private static final Log log = Log.getLog();

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new InitTestMain().run(args);
	}

	@Override
	protected void registerDefaultCommandLineArguments() {
		registerCommandLineSwitch('n', "messages", false, true, "messages", "Number of messages per session");
		registerCommandLineSwitch('r', "rate", false, true, "rate", "The number of milliseconds between the start of each message");
		registerCommandLineSwitch('h', "host", false, true, "host", "Host to connect to");
		registerCommandLineSwitch('p', "port", false, true, "port", "The base Port");
		registerCommandLineSwitch('a', "add-port", false, true, "addPort", "The multiplier of the instance (1-4) which is added to the base port to get the port used");
		registerCommandLineSwitch('v', "protocol", true, true, "protocol", "The protocol version to use (0-8)");
		registerCommandLineSwitch('o', "output", true, true, "output", "Directory for output");
	}

	@Override
	protected void registerDefaultActions() {
		// no actions
	}
	@Override
	public void run(String[] args) {
		Map<String,Object> argMap;
		try {
			argMap = parseArguments(args);
		} catch(IOException e) {
			finishAndExit(e.getMessage(), 100, true, null);
			return;
		}
		final int messages;
		final String host;
		final int port;
		final int protocol;
		final int addPort;
		int rate;
		String outputDir;
		final String deviceName = "TD000000";
		final byte[] encryptionKey = ByteArrayUtils.fromHex("6DF7E5A6FBC84C50A79F76762826C0DC");
		try {
			messages = ConvertUtils.getInt(argMap.get("messages"));
			rate = ConvertUtils.getInt(argMap.get("rate"));
			host = ConvertUtils.getStringSafely(argMap.get("host"));
			port = ConvertUtils.getInt(argMap.get("port"));
			addPort = ConvertUtils.getInt(argMap.get("addPort"), 0);
			protocol = ConvertUtils.getIntSafely(argMap.get("protocol"), 7);
			outputDir = ConvertUtils.getString(argMap.get("output"), "./");
			if(!outputDir.endsWith("/")){
				outputDir+="/";
			}
		} catch(ConvertException e) {
			finishAndExit(e.getMessage(), 200, true, null);
			return;
		} catch(NumberFormatException e) {
			finishAndExit("Could not convert command to a hex number: " + e.getMessage(), 250, true, null);
			return;
		}
		final AtomicInteger index = new AtomicInteger();
		File file = new File(outputDir+"InitPerformanceTest_" + new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date()) + ".csv");
		final TaskListener taskListener;
		try {
			taskListener = new WriteCSVTaskListener(new PrintWriter(new FileOutputStream(file)));
		} catch(FileNotFoundException e) {
			finishAndExit(e.getMessage(), 800, true, null);
			return;
		}
		final InitializationReason reasonCode = InitializationReason.COMMUNICATION_FAILURE;
		final long protocolComplianceRevision = 4001005;
		final int propertyListVersion = 3;
		final DeviceType deviceType = DeviceType.EDGE;
		final String deviceFirmwareVersion = "Client Emulator Performance Test 1.1 firmware version";
		final String deviceInfo = "Client Emulator 1.1 Device Info";
		final String terminalInfo = "Client Emulator 1.1 Terminal Info";
		final Runnable executeTask = new Runnable() {
			@Override
			public void run() {
				long startTime = System.currentTimeMillis();
				int i = index.incrementAndGet();
				int p;
				if(addPort != 0)
					p = port + (addPort * ((i-1) % 4));
				else
					p = port;
				ClientEmulator ce = new ClientEmulator(host, p, deviceName, encryptionKey);
				String serial = "EE19" + StringUtils.pad(Integer.toString(i), '0', 7, Justification.RIGHT);
				MessageData data = ce.constructInitV4Message(reasonCode, protocolComplianceRevision, propertyListVersion, deviceType, deviceFirmwareVersion, serial, deviceInfo, terminalInfo);
				String details;
				try {
					ce.startCommunication(protocol);
					try {
						MessageData_CB replyCB = (MessageData_CB)ce.sendMessage(data);
						details = "Sent:\n"+data+"\nReceived:\n"+replyCB;
					} finally {
						ce.stopCommunication();
					}
				} catch(Exception e) {
					details="ERROR:\n"+e.getMessage();
					log.warn("Error during init of '" + serial + "' for message " + data.getMessageId(), e);
				}
				long endTime = System.currentTimeMillis();
				taskListener.taskRan("SentMessage-C0", details, startTime, endTime);
			}
		};
		final ThreadPoolExecutor executor = new ThreadPoolExecutor(1500, Integer.MAX_VALUE, 1000, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(), new CustomThreadFactory("Executor-", false));
		//
		final long startTime = System.currentTimeMillis() + 1000;
		for(int i = 0; i < messages; ) {
			long time = System.currentTimeMillis();
			int target = Math.min(messages, ((int)(time - startTime)) / rate);
			if(i < target) {
				do {
					executor.execute(executeTask);
				} while(++i < target) ;
			} else {
				long next = ((long)(i * rate)) + startTime - time;
				if(next > 0)
					try {
						Thread.sleep(next);
					} catch(InterruptedException e) {
					}
			}
		}
		/*
		final AtomicInteger iteration = new AtomicInteger();
		final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(10, new CustomThreadFactory("Scheduler-", false));
		Runnable scheduleTask = new Runnable() {
			@Override
			public void run() {
				if(iteration.incrementAndGet() > messages) {
					scheduler.shutdown();
					throw new RuntimeException("All done");
				}
				executor.execute(executeTask);
			}
		};
		
		scheduler.scheduleAtFixedRate(scheduleTask, 1000, rate, TimeUnit.MILLISECONDS);
		try {
			scheduler.awaitTermination(10, TimeUnit.MINUTES);
		} catch(InterruptedException e) {
			log.info("While waiting for termination", e);
		}
		*/
		executor.shutdown();
		try {
			executor.awaitTermination(10, TimeUnit.MINUTES);
		} catch(InterruptedException e) {
			log.info("While waiting for termination", e);
		}
		taskListener.flush();
		String path;
		try {
			path = file.getCanonicalPath();
		} catch(IOException e) {
			path = file.getAbsolutePath();
		}
		log.info("Wrote task stats to '" + path + "'");
		Log.finish();
	}
}
