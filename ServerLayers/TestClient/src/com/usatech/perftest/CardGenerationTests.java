package com.usatech.perftest;

import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import simple.io.ByteArrayUtils;
import simple.io.Log;
import simple.lang.InvalidValueException;
import simple.security.SecureHash;


public class CardGenerationTests {
	private static final Log log = Log.getLog();

	public static void main(String[] args) throws NoSuchAlgorithmException, InvalidValueException {
		// 1001667470
		// String card = "6396210016*****3014";
		String card = "6396210********3014";
		byte[] hash = ByteArrayUtils.fromHex("5B2C360FE0BD436260B6D45DDDEB410B78E089262BA75268C01F80C59A363D1D");
		String guess = guessCard(card, hash);
		log.info("Found card " + guess + " for " + card);
	}

	public static String guessCard(String cardNumber, byte[] acctCdHash) throws InvalidValueException, NoSuchAlgorithmException {
		byte[] cardArray = cardNumber.getBytes();
		int start = 0;
		for(; start < cardArray.length; start++)
			if(cardArray[start] == '*')
				break;
		int end = cardArray.length - 1;
		for(; end >= start; end--)
			if(cardArray[end] == '*')
				break;
		int baseChecksum = addChecksum(cardArray, 0, start) + addChecksum(cardArray, end + 1, cardArray.length);
		if(log.isDebugEnabled())
			log.debug("Must guess " + (end - start + 1) + " digits for " + cardNumber);
		for(int i = start; i <= end; i++)
			cardArray[i] = '0';
		int max = (int) Math.pow(10, (end - start + 1));
		int n;
		for(n = 1; n < max; n++) {
			int checksum = baseChecksum + addChecksum(cardArray, start, end + 1);
			if((checksum % 10) == 0) {
				byte[] guessHash = SecureHash.getHash(cardArray, null, "SHA-256/1000"); // "SHA-256/0");
				if(Arrays.equals(acctCdHash, guessHash)) {
					if(log.isDebugEnabled())
						log.debug("Found account cd for " + cardNumber);
					return new String(cardArray);
				}
			}
			String s = Integer.toString(n);
			getBytesFromString(s, 0, s.length(), cardArray, end - s.length() + 1);
		}
		throw new InvalidValueException("Could NOT find account cd after " + n + " guesses for " + cardNumber, cardNumber);
	}

	@SuppressWarnings("deprecation")
	protected static void getBytesFromString(String s, int srcBegin, int srcEnd, byte[] dst, int dstBegin) {
		s.getBytes(srcBegin, srcEnd, dst, dstBegin);
	}

	public static int calcDigit(String accountCd, int index) {
		int[] a = new int[accountCd.length()];
		for(int i = 0; i < a.length; i++)
			a[i] = Character.getNumericValue(accountCd.charAt(i));

		int len = a.length;
		int checksum = 0;
		int tmp;

		for(int i = (len % 2); i < len; i += 2) {
			if(i == index)
				continue;
			tmp = a[i] * 2;
			while(tmp > 9)
				tmp = (tmp % 10) + (tmp / 10);
			checksum += tmp;
		}
		for(int i = ((len + 1) % 2); i < len; i += 2) {
			if(i == index)
				continue;
			tmp = a[i];
			while(tmp > 9)
				tmp = (tmp % 10) + (tmp / 10);
			checksum += tmp;
		}

		checksum = (10 - (checksum % 10)) % 10;
		if(((len - index) % 2) == 0)
			return (checksum % 2) == 0 ? checksum / 2 : (9 + checksum) / 2;
		return checksum;
	}

	public static int addChecksum(byte[] cardArray, int start, int end) {
		int len = cardArray.length;
		int checksum = 0;
		int tmp;

		for(int i = start + ((len + start) % 2); i < end; i += 2) {
			tmp = Character.getNumericValue(cardArray[i]) * 2;
			while(tmp > 9)
				tmp = (tmp % 10) + (tmp / 10);
			checksum += tmp;
		}
		for(int i = start + ((len + start + 1) % 2); i < end; i += 2) {
			tmp = Character.getNumericValue(cardArray[i]);
			checksum += tmp;
		}

		return checksum;
	}
}
