/**
 *
 */
package com.usatech.perftest;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import simple.io.ByteArrayUtils;
import simple.io.Log;
import simple.text.StringUtils;

import com.usatech.util.crypto.Crypt;
import com.usatech.util.crypto.CryptUtil;
import com.usatech.util.crypto.crc.CRC;
import com.usatech.util.crypto.crc.LE_0000;

/**
 * @author Brian S. Krug
 *
 */
public class EncryptTest {
	private static Log log;
	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		System.setProperty("simple.io.logging.bridge", "simple.io.logging.SimpleBridge");
        log = Log.getLog();
        //testEncrypt();
		testDecrypt();
        //testCRC();
		// testDecryptAndCRC();
		Log.finish();
	}
	public static void testEncryptAndSend() throws Exception {
		String deviceName = "EV035194";
		byte[] key = "3022848002877358".getBytes();

		Crypt crypt = CryptUtil.getCrypt("AES_CBC_CRC16");//"AES_CBC_CRC16"
		byte[] unencrypted = new byte[32];
		for(int i = 0; i < unencrypted.length; i++) {
			unencrypted[i] = (byte)i;
		}
		log.debug("Unencrypted: " + StringUtils.toHex(unencrypted));
	    log.debug("Encrypting with key: " + StringUtils.toHex(key));
		byte[] encrypted = crypt.encrypt(unencrypted, key);
		log.debug("Encrypted: " + StringUtils.toHex(encrypted));
		byte[] decrypted = crypt.decrypt(encrypted, key);
		log.debug("Decrypted: " + StringUtils.toHex(decrypted));
		Socket socket = new Socket("localhost", 443);
		socket.setSoTimeout(10000);

		InputStream input = new  BufferedInputStream(socket.getInputStream());
		OutputStream output = new  BufferedOutputStream(socket.getOutputStream());

		output.write(7); //protocol version
		output.write(0); // first byte of length
		output.write(8 + encrypted.length); // second byte of length
		output.write(deviceName.getBytes());
		output.write(encrypted);
		output.flush();

		int protocol = input.read();

	}
	public static void testDecrypt() throws Exception {
		//String deviceName = "TD000486";
		byte[] key = ByteArrayUtils.fromHex("8C84A29A2072442D6BCE812958A58227");
		Crypt crypt = CryptUtil.getCrypt("AES_CBC_CRC16");//"AES_CBC_CRC16"
		byte[] encrypted = ByteArrayUtils
				.fromHex("B08EA55FF0F65825FC5CB924AD13D62CA077F3F4B64E93401E266BBDD49AD629411C876CD2001288E3279D6BBF366575AFC9C3F542CAC4D9D84F56FAB214851847AEB769056BFA2CAB2F7287B57ACD4C1342278CC6001C818CCCC8E51597AF2D443BBBCADC2E157807C080FE98DF1BDCA6ACA7A875D02D5BEA31B9C4B4ABF1B1410FAE00058D7C599E45F7CF83685C7225E5D0A3990300D87886D8E5E57E57847F1B3EB5928F7AEB991C6ED2FE5B7561330191AC1D11128A60E26AE493705A392F18B14E8BB38FC3D59C0C84A8B3D392828020EEDDAEABC28974941F2F6AE4546B5BE0F22A1B4F0FB5FE8693C8975350");
		log.debug("Encrypted: " + StringUtils.toHex(encrypted));
		byte[] decrypted = crypt.decrypt(encrypted, key);
		log.debug("Decrypted: " + StringUtils.toHex(decrypted));
	}
	public static void testEncrypt() throws Exception {
		//String deviceName = "TD000486";
		byte[] key = ByteArrayUtils.fromHex("F9354CF879C2D5E2E5223F6FFC50079C");

		Crypt crypt = CryptUtil.getCrypt("TEA_CRC16");//"AES_CBC_CRC16"
		byte[] unencrypted = ByteArrayUtils.fromHex("00A04CD95DF143000001F400343830323131363938353039383838343D2A2A2A2A2A2A2A2A2A2A2A2A2A2A2A");
		log.debug("Unencrypted: " + StringUtils.toHex(unencrypted));
		byte[] encrypted = crypt.encrypt(unencrypted, key);
		log.debug("Encrypted: " + StringUtils.toHex(encrypted));
		byte[] decrypted = crypt.decrypt(encrypted, key);
		log.debug("Decrypted: " + StringUtils.toHex(decrypted));
	}
	public static void testCRC() throws Exception {
		CRC crc = new LE_0000();
		byte[] unencrypted = ByteArrayUtils.fromHex("00A04CD95DF143000001F400343830323131363938353039383838343D2A2A2A2A2A2A2A2A2A2A2A2A2A2A2A");
		log.debug("Unencrypted: " + StringUtils.toHex(unencrypted));
		log.debug("CRC: " + (short)crc.calcCRC(unencrypted));
	}

	public static void testDecryptAndCRC() throws Exception {
		// String deviceName = "TD000486";
		byte[] key = ByteArrayUtils.fromHex("676BF0572F2E9467506B50878FAB93A2"); // "C34DFA94CBC0A54164ED24DACE23C265"); // "960969DAD05148CCE302AF2D4F15E04C");// "1480E07AEF819D47106184D6BCF6C85F");
		Crypt crypt = CryptUtil.getCrypt("TEA_CRC16");// "AES_CBC_CRC16"
		byte[] encrypted = ByteArrayUtils
				.fromHex("84DF125BDE61F900");
		log.debug("Encrypted: " + StringUtils.toHex(encrypted));
		byte[] decrypted = crypt.decrypt(encrypted, key);
		log.debug("Decrypted: " + StringUtils.toHex(decrypted));
		CRC crc = new LE_0000();
		log.debug("CRC: " + (short) crc.calcCRC(decrypted));
	}
}
