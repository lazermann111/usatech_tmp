package com.usatech.perftest;

import com.usatech.tools.deploy.IPTablesUpdateTask;

public class AnyTest {

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		IPTablesUpdateTask iptTask = new IPTablesUpdateTask();
		iptTask.allowTcpOut("yum.postgresql.org", 80);
		System.out.println(iptTask.printAddCommands());
	}

}
