/**
 *
 */
package com.usatech.perftest;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.event.TaskListener;
import simple.results.Results;
import simple.results.Results.Aggregate;

import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;

/**
 * @author Brian S. Krug
 *
 */
public class UpdateDerby implements MessageChainTask {
	protected TaskListener taskListener;

	/**
	 * @see com.usatech.app.MessageChainTask#process(com.usatech.app.MessageChainTaskInfo)
	 */
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		long start = System.currentTimeMillis();
		String taskDetails;
		try {
			Connection conn = DataLayerMgr.getConnection("EMBED");
			try {
				long id = ConvertUtils.getLongSafely(taskInfo.getStep().getAttributes().get("id"), 1) % 10000;
				Map<String,Object> params = new HashMap<String, Object>();
				params.put("id", id);
				params.put("text", Math.random() * id);
				double r = Math.random();
				int rows;
				if(r < 0.7) {
					rows = DataLayerMgr.executeUpdate(conn, "UPDATE_DERBY", params);
					if(rows < 1) {
						rows = DataLayerMgr.executeUpdate(conn, "INSERT_DERBY", params);
						taskDetails = "Inserted " + rows + " rows";
					} else
						taskDetails = "Updated " + rows + " rows";
				} else if(r < 0.8) {
					rows = DataLayerMgr.executeUpdate(conn, "DELETE_DERBY", params);
					taskDetails = "Deleted " + rows + " rows";
				} else {
					Results results = DataLayerMgr.executeQuery(conn, "DELETE_DERBY", params);
					taskDetails = "Selected " + results.getAggregate("id", null, Aggregate.COUNT) + " rows";
					results.close();
				}
			} finally {
				conn.commit();
				conn.close();
			}
		} catch(SQLException e) {
			throw new ServiceException(e);
		} catch(DataLayerException e) {
			throw new ServiceException(e);
		}
		TaskListener tl = getTaskListener();
		if(tl != null) {
			tl.taskRan("UpdateDerby", taskDetails, start, System.currentTimeMillis());
			tl.flush();
		}
		return 0;
	}

	public TaskListener getTaskListener() {
		return taskListener;
	}

	public void setTaskListener(TaskListener taskListener) {
		this.taskListener = taskListener;
	}

}
