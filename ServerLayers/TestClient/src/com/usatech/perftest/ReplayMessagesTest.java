package com.usatech.perftest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.nio.ByteBuffer;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;

import simple.app.Base;
import simple.app.MainWithConfig;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.Call;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.event.TaskListener;
import simple.event.WriteCSVTaskListener;
import simple.io.ByteArrayUtils;
import simple.io.Log;
import simple.results.Results;
import simple.util.concurrent.ConcurrentReadonlyCycle;
import simple.util.concurrent.CustomThreadFactory;
import simple.util.sheet.SheetUtils;
import simple.util.sheet.SheetUtils.RowValuesIterator;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.GenericResponseServerActionCode;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageDataFactory;
import com.usatech.layers.common.messagedata.MessageData_2B;
import com.usatech.layers.common.messagedata.MessageData_5E;
import com.usatech.layers.common.messagedata.MessageData_7E;
import com.usatech.layers.common.messagedata.MessageData_8E;
import com.usatech.layers.common.messagedata.MessageData_90;
import com.usatech.layers.common.messagedata.MessageData_93;
import com.usatech.layers.common.messagedata.MessageData_9A5F;
import com.usatech.layers.common.messagedata.MessageData_A0;
import com.usatech.layers.common.messagedata.MessageData_A6;
import com.usatech.layers.common.messagedata.MessageData_AA;
import com.usatech.layers.common.messagedata.MessageData_AC;
import com.usatech.layers.common.messagedata.MessageData_AD;
import com.usatech.layers.common.messagedata.MessageData_C0;
import com.usatech.layers.common.messagedata.MessageData_C2;
import com.usatech.layers.common.messagedata.MessageData_C7;
import com.usatech.layers.common.messagedata.MessageData_C9;
import com.usatech.layers.common.messagedata.MessageData_CB;
import com.usatech.layers.common.messagedata.MessageDirection;
import com.usatech.networklayer.NetworkLayerException;
import com.usatech.test.ClientEmulator;

/** Replays messages from a csv or Excel spreadsheet. Create the spreadsheet from a SQL query like:
 * <code>SELECT 
      round(EXTRACT(DAY FROM m.OFFSET_INTERVAL) * 86400000 
             + EXTRACT(HOUR FROM m.OFFSET_INTERVAL) * 3600000
             + EXTRACT(MINUTE FROM m.OFFSET_INTERVAL) * 60000
             + EXTRACT(SECOND FROM m.OFFSET_INTERVAL)  * 1000) as offset, m.*
  FROM (
        SELECT DS.GLOBAL_SESSION_CD SESSION_ID, ds.created_utc_ts - FIRST_VALUE(ds.created_utc_ts) OVER (ORDER BY ds.created_utc_ts) OFFSET_INTERVAL,
               encode (dm.INBOUND_MESSAGE, 'hex') MESSAGE,
               (case nl.SERVER_PORT_NUM when 14107 then 4 else (case ds.DEVICE_TYPE_ID when 13 then 7 else 5 end)end) PROTOCOL,
               nl.SERVER_PORT_NUM PORT,
               ds.DEVICE_TYPE_ID DEVICE_TYPE,
               dm.inbound_message_ts
        FROM main.DEVICE_SESSION DS
        JOIN main.DEVICE_MESSAGE DM ON DS.global_session_cd	 = DM.global_session_cd
        JOIN main.NET_LAYER NL ON DS.NET_LAYER_ID = NL.NET_LAYER_ID
        WHERE ds.created_utc_ts BETWEEN (CURRENT_TIMESTAMP AT TIME ZONE 'UTC') - interval '1 day'  AND CURRENT_TIMESTAMP AT TIME ZONE 'UTC' and dm.INBOUND_MESSAGE is not  null and 
        ) m
   ORDER BY M.OFFSET_INTERVAL, M.SESSION_ID, M.inbound_message_ts;</code>
   
 * @author bkrug
 *
 */
public class ReplayMessagesTest extends MainWithConfig {
	private static final Log log = Log.getLog();

	protected class ReplayMessage {
		public final MessageData data;
		public final int replies;
		public ReplayMessage(MessageData data, int replies) {
			this.data = data;
			this.replies = replies;
		}
	}
	protected class ReplaySession implements Runnable {
		protected DeviceCredential deviceCredential;
		protected final List<ReplayMessage> messages = new ArrayList<ReplayMessage>();
		protected long offset;
		protected byte protocol;
		protected int port;
		protected final String host;
		protected String sessionId;
		protected DeviceType deviceType;
		protected final TaskListener taskListener;
		
		public ReplaySession(String host, TaskListener taskListener) {
			this.host = host;
			this.taskListener = taskListener;
		}
		public void addMessage(MessageData data, int replies) {
			this.messages.add(new ReplayMessage(data, replies));
		}
		public long getOffset() {
			return offset;
		}
		public void setOffset(long offset) {
			this.offset = offset;
		}
		public byte getProtocol() {
			return protocol;
		}
		public void setProtocol(byte protocol) {
			this.protocol = protocol;
		}
		public int getPort() {
			return port;
		}
		public void setPort(int port) {
			this.port = port;
		}
		public String getSessionId() {
			return sessionId;
		}
		public void setSessionId(String sessionId) {
			this.sessionId = sessionId;
		}
		public DeviceType getDeviceType() {
			return deviceType;
		}
		public void setDeviceType(DeviceType deviceType) {
			this.deviceType = deviceType;
		}
		public boolean validate() {
			return true;
		}
		@Override
		public void run() {
			ClientEmulator client = new ClientEmulator(host, port, deviceCredential.deviceName, deviceCredential.encryptionKey);
			try {
				client.startCommunication(protocol);
				StringBuilder details = new StringBuilder();
				long startTime = 0;
				try {
					for(ReplayMessage message : messages) {						
						details.setLength(0);
						startTime = System.currentTimeMillis();
						client.transmitMessage(message.data);
						details.append("Sent:\n").append(message.data); // so that we log the updated messageId
						if(message.replies == -1) {
							MessageData reply = client.receiveReply();
							details.append("\nReceived:\n").append(reply);
							switch(reply.getMessageType()) {
								case TERMINAL_UPDATE_STATUS:
									if(((MessageData_90) reply).getTerminalUpdateStatus() != 0) {
										reply = client.receiveReply();
										details.append("\nReceived:\n").append(reply);
									}
									break;
								case GENERIC_RESPONSE_4_1: 
									if(((MessageData_CB)reply).getServerActionCode() != GenericResponseServerActionCode.PROCESS_PROP_LIST)
										break;
								case SET_ID_NUMBER_AND_KEY:
									deviceCredential.encryptionKey = client.getEncryptionKey();
									break;
							}
						} else {
							for(int i = 0; i < message.replies; i++) {
								MessageData reply = client.receiveReply();
								details.append("\nReceived:\n").append(reply);
								if(reply != null)
									switch(reply.getMessageType()) {
										case GENERIC_RESPONSE_4_1: 
											if(((MessageData_CB)reply).getServerActionCode() != GenericResponseServerActionCode.PROCESS_PROP_LIST)
												break;
										case SET_ID_NUMBER_AND_KEY:
											deviceCredential.encryptionKey = client.getEncryptionKey();
											break;
									}
							}
						}
						long endTime = System.currentTimeMillis();
						taskListener.taskRan(deviceCredential.deviceName + " / " + client.getLocalPort(), details.toString(), startTime, endTime);
						log.debug(details);						
					}
				} catch(Exception e) {
					long endTime = System.currentTimeMillis();
					if(startTime == 0)
						startTime = endTime;
					details.append("\nERROR:\n").append(e.getMessage());
					taskListener.taskRan("ERROR", details.toString(), startTime, endTime);
					log.warn("Error while processing session " + sessionId + " for " + deviceCredential.deviceName + "", e);
				} finally {
					client.stopCommunication();
				}
			} catch(IOException e) {
				log.warn("Error while processing session " + sessionId + " for " + deviceCredential.deviceName + "", e);
			} catch(SecurityException e) {
				log.warn("Error while processing session " + sessionId + " for " + deviceCredential.deviceName + "", e);
			} catch(IllegalArgumentException e) {
				log.warn("Error while processing session " + sessionId + " for " + deviceCredential.deviceName + "", e);
			} catch(ClassNotFoundException e) {
				log.warn("Error while processing session " + sessionId + " for " + deviceCredential.deviceName + "", e);
			} catch(NetworkLayerException e) {
				log.warn("Error while processing session " + sessionId + " for " + deviceCredential.deviceName + "", e);
			} catch(NoSuchMethodException e) {
				log.warn("Error while processing session " + sessionId + " for " + deviceCredential.deviceName + "", e);
			} catch(IllegalAccessException e) {
				log.warn("Error while processing session " + sessionId + " for " + deviceCredential.deviceName + "", e);
			} catch(InvocationTargetException e) {
				log.warn("Error while processing session " + sessionId + " for " + deviceCredential.deviceName + "", e);
			}
		}
		public DeviceCredential getDeviceCredential() {
			return deviceCredential;
		}
		public void setDeviceCredential(DeviceCredential deviceCredential) {
			this.deviceCredential = deviceCredential;
		}
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new ReplayMessagesTest().run(args);
	}
	
	protected static class DeviceCredential {
		public final String deviceName;
		public final String deviceSerialCd;
		public volatile byte[] encryptionKey;
		public DeviceCredential(String deviceName, String deviceSerialCd, byte[] encryptionKey) {
			this.deviceName = deviceName;
			this.deviceSerialCd = deviceSerialCd;
			this.encryptionKey = encryptionKey;
		}
	}

	protected final Map<DeviceType,ConcurrentReadonlyCycle<DeviceCredential>> devices = new HashMap<DeviceType, ConcurrentReadonlyCycle<DeviceCredential>>();
	protected void setDeviceCredentials(DeviceType deviceType, DeviceCredential... deviceCredentials) {
		devices.put(deviceType, new ConcurrentReadonlyCycle<DeviceCredential>(Arrays.asList(deviceCredentials)));
	}
	protected static final Pattern CONTENT_FILLIN_PATTERN = Pattern.compile("\\<(\\d+) bytes\\>");
	protected static byte[] fillInContent(byte[] content) {
		Matcher matcher = TRACK_DATA_FILLIN_PATTERN.matcher(new String(content, ProcessingConstants.US_ASCII_CHARSET));
		if(matcher.matches()) {
			int len;
			try {
				len = Integer.parseInt(matcher.group(1));
			} catch(NumberFormatException e) {
				len = -1;
			}
			if(len >= 0 && len <= 1024) {
				byte[] filledIn = new byte[len];
				Arrays.fill(filledIn, (byte)'*');
				return filledIn;
			}
		}
		return content;
	}

	protected static final Pattern TRACK_DATA_FILLIN_PATTERN = Pattern.compile("([^0-9A-Za-z]*)([0-9A-Za-z]{2})([*@]+)([0-9A-Za-z]{4})($|[^0-9A-Za-z].*)");
	protected static String fillInTrackData(String trackData) {
		Matcher matcher = TRACK_DATA_FILLIN_PATTERN.matcher(trackData);
		if(matcher.matches()) {
			String prefix = matcher.group(2);
			int len = matcher.group(3).length() + 6;
			String middle = matcher.group(3);
			String suffix = matcher.group(4);
			StringBuilder sb = new StringBuilder(trackData);
			int checksum = 0;
			for(int i = 0; i < prefix.length(); i++) {
				int tmp = Character.getNumericValue(prefix.charAt(i));
				if(((len - i) % 2) == 0) {
					tmp = tmp * 2;
				}
				while(tmp > 9)
					tmp = (tmp % 10) + (tmp / 10);
				checksum += tmp;
			}
			int offset = matcher.start(3);
			Random rand = new Random();
			for(int i = 0; i < middle.length() - 1; i++) {
				char ch;
				switch(middle.charAt(i)) {
					case '@':
						ch = (char)('A' + rand.nextInt(26));
						break;
					case '*':
						ch = (char)('0' + rand.nextInt(10));
						break;	
					 default:
						 ch = middle.charAt(i);
				}
				sb.setCharAt(offset + i, ch);
				int tmp = Character.getNumericValue(ch);
				if(((len - i - prefix.length()) % 2) == 0) {
					tmp = tmp * 2;
				}
				while(tmp > 9)
					tmp = (tmp % 10) + (tmp / 10);
				checksum += tmp;
			}
			for(int i = 0; i < suffix.length(); i++) {
				int tmp = Character.getNumericValue(suffix.charAt(i));
				if(((len - i - prefix.length() - middle.length()) % 2) == 0) {
					tmp = tmp * 2;
				}
				while(tmp > 9)
					tmp = (tmp % 10) + (tmp / 10);
				checksum += tmp;
			}
			int tmp = (10 - (checksum % 10)) % 10;
			sb.setCharAt(offset + middle.length() - 1, (char)('0' + tmp));
			return sb.toString();
		}
		return trackData;
	}

	@Override
	protected void registerDefaultCommandLineArguments() {
		registerCommandLineSwitch('f', "file", true, true, "file", "The csv or excel input file that contains all the messages to be replayed in order by sessionId, offset. The file must have the following columns:\n"
				+ "\tSESSION_ID\tA string unique for that session. More than one message can be sent in a session by having more than one line with this value\n"
				+ "\tOFFSET\tThe number of milliseconds after the test starts to replay this message\n"
				+ "\tMESSAGE\tThe content of the message to send in hex\n"
			    + "\tPROTOCOL\tThe transmission protocol version to use [0-8]\n"
				+ "\tPORT\tThe port number to use\n"
				+ "\tDEVICE_TYPE\tThe device type of the device to use\n"
		);
		/*registerCommandLineSwitch('h', "host", false, true, "host", "Host to connect to");
		registerCommandLineSwitch('o', "output", true, true, "output", "Directory for output");*/
		registerCommandLineSwitch('p', "propertiesFile", true, true, "properties-file", "The properties file to use");
		registerCommandLineSwitch('e', "export", true, true, "export", "The command to exort messages to CSV file");
	}

	@Override
	protected void registerDefaultActions() {
		// no actions
	}
	@Override
	public void run(String[] args) {
		Map<String,Object> argMap;
		try {
			argMap = parseArguments(args);
		} catch(IOException e) {
			finishAndExit(e.getMessage(), 100, true, null);
			return;
		}
		String inputFile = null;
		String host = null;
		String outputDir = null;
		
		try {
			inputFile = ConvertUtils.getString(argMap.get("file"), true);
		} catch (ConvertException e1) {
			// TODO Auto-generated catch block
		}
		
		Properties properties;
		try {
			properties = getProperties(ConvertUtils.getStringSafely(argMap.get("properties-file")), getClass(), null);
		} catch(IOException e) {
			log.info("Properties file not found, using hard coded device credentials");
			properties = null;
		}
	
		
		if(properties != null && !properties.isEmpty()) {
			try {
				
				try {
					if (inputFile == null ||inputFile.isEmpty())
						inputFile = ConvertUtils.getString(properties.get("file"), true);
					host = ConvertUtils.getStringSafely(properties.get("host"));
					outputDir = ConvertUtils.getString(properties.get("output"), "./");
					if(!outputDir.endsWith("/")){
						outputDir+="/";
					}
				} catch(ConvertException e) {
					finishAndExit(e.getMessage(), 200, true, null);
					return;
				} catch(NumberFormatException e) {
					finishAndExit("Could not convert argument to a number: " + e.getMessage(), 250, true, null);
					return;
				}
				
				Base.configureDataSourceFactory(properties, null);
				Base.configureDataLayer(properties);
				
				if (argMap.containsKey("export")) {
					inputFile = exportToCSV(argMap);
				}
				
				Map<String,Object> params = new HashMap<String, Object>();
				String [] devNames = ConvertUtils.getString(properties.get("devices"), null).split(" ");
				params.put("devices",devNames);
				log.info("Devices: " + Arrays.toString(devNames));
				Results results = DataLayerMgr.executeQuery("GET_DEVICE_CREDENTIALS", params);
				Map<DeviceType,List<DeviceCredential>> deviceCredentials = new HashMap<DeviceType, List<DeviceCredential>>();
				while(results.next()) {
					DeviceType deviceType = results.getValue("DEVICE_TYPE", DeviceType.class);
					List<DeviceCredential> list = deviceCredentials.get(deviceType);
					if(list == null) {
						list = new ArrayList<DeviceCredential>();
						deviceCredentials.put(deviceType, list);
					}
					list.add(new DeviceCredential(results.getValue("DEVICE_NAME", String.class), results.getValue("DEVICE_SERIAL_CD", String.class), results.getValue("ENCRYPTION_KEY", byte[].class)));
				}
				for(Map.Entry<DeviceType,List<DeviceCredential>> entry : deviceCredentials.entrySet()) {
					devices.put(entry.getKey(), new ConcurrentReadonlyCycle<DeviceCredential>(entry.getValue()));
				}
			} catch(SQLException e) {
				finishAndExit("Could not load device credentials from database: " + e.getMessage(), 350, true, null);
				return;
			} catch(ServiceException e) {
				finishAndExit("Could not load device credentials from database: " + e.getMessage(), 350, true, null);
				return;
			} catch(DataLayerException e) {
				finishAndExit("Could not load device credentials from database: " + e.getMessage(), 350, true, null);
				return;
			} catch(ConvertException e) {
				finishAndExit("Could not load device credentials from database: " + e.getMessage(), 350, true, null);
				return;
			}
		} else {
			setDeviceCredentials(DeviceType.G4, 
					new DeviceCredential("EV034858", null, ByteArrayUtils.fromHex("34373438343130383837313935383532")));
			setDeviceCredentials(DeviceType.GX, 
					new DeviceCredential("EV035194", null, ByteArrayUtils.fromHex("33303232383438303032383737333538")),
					new DeviceCredential("EV050271", null, ByteArrayUtils.fromHex("32303538393434353132313732343937")),
					new DeviceCredential("EV050265", null, ByteArrayUtils.fromHex("32373439323335323134323336303537")));
			setDeviceCredentials(DeviceType.ESUDS,
					new DeviceCredential("EV100033", null, ByteArrayUtils.fromHex("31323530393531313638343430323035")),
					new DeviceCredential("EV100032", null, ByteArrayUtils.fromHex("31363634343833333238313131333738")),
					new DeviceCredential("EV050278", null, ByteArrayUtils.fromHex("31323831303332313932373432373532")));
			setDeviceCredentials(DeviceType.MEI,
					new DeviceCredential("EV050229", null, ByteArrayUtils.fromHex("37333034363432353632303434393139")));
			setDeviceCredentials(DeviceType.KIOSK,
					new DeviceCredential("EV100135", null, ByteArrayUtils.fromHex("5AABA60FB84F58F4AB789CC3F564DF16")),
					new DeviceCredential("EV100136", null, ByteArrayUtils.fromHex("A89D78FA44D0343F8ED5D755B4ED4673")),
					new DeviceCredential("EV100137", null, ByteArrayUtils.fromHex("926BE35A4354899374088F14CDD66E80")));
			setDeviceCredentials(DeviceType.EDGE,
					new DeviceCredential("TD000582", null, ByteArrayUtils.fromHex("7164689FFBCA7872DFA80C8617748F37")),
					new DeviceCredential("TD000021", null, ByteArrayUtils.fromHex("9B7D6A1E14808F6BF2BF3273988DB79E")),
					new DeviceCredential("TD000065", null, ByteArrayUtils.fromHex("7D64C6709D624862BF056F6C087FCA3E")));
		}
		
		
		File file = new File(outputDir+"ReplayMessagesTest_" + new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date()) + ".csv");
		final TaskListener taskListener;
		try {
			taskListener = new WriteCSVTaskListener(new PrintWriter(new FileOutputStream(file)));
		} catch(FileNotFoundException e) {
			finishAndExit(e.getMessage(), 800, true, null);
			return;
		}
		RowValuesIterator rvIter;
		try {
			rvIter = SheetUtils.getExcelRowValuesIterator(new FileInputStream(inputFile));
		} catch(FileNotFoundException e) {
			finishAndExit(e.getMessage(), 202, true, null);
			return;
		} catch(IOException e) {
			finishAndExit(e.getMessage(), 203, true, null);
			return;
		}
		final ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(1500, new CustomThreadFactory("Executor-", false));
		final long start = System.currentTimeMillis() + 1000;
		ReplaySession replySession = null;
		long lastOffset = 0;
		while(rvIter.hasNext()) {
			Map<String,Object> values = rvIter.next();
			try {
				String sessionId = ConvertUtils.getString(values.get("SESSION_ID"), true);
				byte[] message = ByteArrayUtils.fromHex(ConvertUtils.getString(values.get("MESSAGE"), true));
				String deviceSerialCd = null;
				if(replySession == null || !replySession.getSessionId().equals(sessionId)) {
					if(replySession != null)
						executor.schedule(replySession, System.currentTimeMillis() - start + replySession.getOffset(), TimeUnit.MILLISECONDS);
					replySession = new ReplaySession(host, taskListener);
					replySession.setDeviceType(ConvertUtils.convertRequired(DeviceType.class, values.get("DEVICE_TYPE")));
					//replySession.setDeviceName(ConvertUtils.getString(values.get("DEVICE_NAME"), true));
					//replySession.setEncryptionKey(ByteArrayUtils.fromHex(ConvertUtils.getString(values.get("ENCRYPTION_KEY"), true)));
					DeviceType dt = replySession.getDeviceType();
					if (devices.get(dt) == null) { 
						replySession = null;
						log.warn("Could not find credentials for Device Type, row #" + rvIter.getCurrentRowNum());
						continue;
					}
					DeviceCredential dc = devices.get(dt).next();
					replySession.setDeviceCredential(dc);
					replySession.setOffset(ConvertUtils.getLong(values.get("OFFSET")));
					replySession.setPort(ConvertUtils.getInt(values.get("PORT")));
					replySession.setProtocol(ConvertUtils.getByte(values.get("PROTOCOL")));
					if(replySession.getPort() == 14107 && replySession.getProtocol() != 4) {
						log.warn("Adjusting protocol " + replySession.getProtocol() + " to protocol 4 for port 14107 for sessionId " + sessionId);
						replySession.setProtocol((byte)4);
					} else if(replySession.getPort() != 14107 && replySession.getProtocol() == 4) {
						log.warn("Adjusting protocol " + replySession.getProtocol() + " to protocol 5 for port " + replySession.getPort() + " for sessionId " + sessionId);
						replySession.setProtocol((byte)5);						
					}
					replySession.setSessionId(sessionId);
					lastOffset = Math.max(lastOffset, replySession.getOffset());
					deviceSerialCd = dc.deviceSerialCd;
				}
				MessageData data = MessageDataFactory.readMessage(ByteBuffer.wrap(message), MessageDirection.CLIENT_TO_SERVER, replySession.getDeviceType());
				int replies = 1;
				switch(data.getMessageType()) {
					case LOCAL_AUTH_BATCH_2_0:
						MessageData_2B data2B = (MessageData_2B)data;
						if(data2B.getCreditCardMagstripe() != null && data2B.getCreditCardMagstripe().indexOf('*') >= 0)
							data2B.setCreditCardMagstripe(fillInTrackData(data2B.getCreditCardMagstripe()));
						break;
					case AUTH_REQUEST_2_0:
						MessageData_5E data5E = (MessageData_5E)data;
						if(data5E.getAccountData() != null && data5E.getAccountData().indexOf('*') >= 0)
							data5E.setAccountData(fillInTrackData(data5E.getAccountData()));
						break;
					case LOCAL_AUTH_BATCH_BCD_2_0:
						MessageData_93 data93 = (MessageData_93)data;
						if(data93.getCreditCardMagstripe() != null && data93.getCreditCardMagstripe().indexOf('*') >= 0)
							data93.setCreditCardMagstripe(fillInTrackData(data93.getCreditCardMagstripe()));
						break;
					case ESUDS_LOCAL_AUTH_BATCH:
						MessageData_9A5F data9A5F = (MessageData_9A5F)data;
						if(data9A5F.getCreditCardMagstripe() != null && data9A5F.getCreditCardMagstripe().indexOf('*') >= 0)
							data9A5F.setCreditCardMagstripe(fillInTrackData(data9A5F.getCreditCardMagstripe()));
						break;
					case AUTH_REQUEST_3_0:
						MessageData_A0 dataA0 = (MessageData_A0)data;
						if(dataA0.getAccountData() != null && dataA0.getAccountData().indexOf('*') >= 0)
							dataA0.setAccountData(fillInTrackData(dataA0.getAccountData()));
						break;
					case AUTH_REQUEST_3_1:
						MessageData_AC dataAC = (MessageData_AC)data;
						switch(dataAC.getCardReaderType()) {
							case MAGTEK_MAGNESAFE:
								//MessageData_AC.MagTekCardReader mtcr = (MessageData_AC.MagTekCardReader)dataAC.getCardReader();
								//mtcr.setDecryptedLength(decryptedLength);
								//mtcr.setKeySerialNum(keySerialNum);
								//mtcr.setEncryptedData(encryptedData);
								break;											
						}
						break;							
					case PERMISSION_REQUEST_3_1:
						MessageData_AA dataAA = (MessageData_AA)data;
						if(dataAA.getAccountData() != null && dataAA.getAccountData().indexOf('*') >= 0)
							dataAA.setAccountData(fillInTrackData(dataAA.getAccountData()));
						break;
					case AUTH_REQUEST_4_1:
						MessageData_C2 dataC2 = (MessageData_C2)data;
						switch(dataC2.getCardReaderType()) {
							case GENERIC:
								MessageData_C2.GenericTracksCardReader gcr = (MessageData_C2.GenericTracksCardReader)dataC2.getCardReader();
								if(gcr.getAccountData1() != null && gcr.getAccountData1().indexOf('*') >= 0)
									gcr.setAccountData1(fillInTrackData(gcr.getAccountData1()));
								if(gcr.getAccountData2() != null && gcr.getAccountData2().indexOf('*') >= 0)
									gcr.setAccountData2(fillInTrackData(gcr.getAccountData2()));
								if(gcr.getAccountData3() != null && gcr.getAccountData3().indexOf('*') >= 0)
									gcr.setAccountData3(fillInTrackData(gcr.getAccountData3()));
								break;
							case MAGTEK_MAGNESAFE:
								//MessageData_C2.MagTekCardReader mtcr = (MessageData_C2.MagTekCardReader)dataC2.getCardReader();
								//mtcr.setDecryptedLength(decryptedLength);
								//mtcr.setKeySerialNum(keySerialNum);
								//mtcr.setEncryptedData(encryptedData);
								break;											
						}
						break;
					case SHORT_FILE_XFER_4_1:
						MessageData_C7 dataC7 = (MessageData_C7)data;
						if(dataC7.getContent() != null)
							dataC7.setContent(fillInContent(dataC7.getContent()));
						break;
					case FILE_XFER_4_1:
						MessageData_C9 dataC9 = (MessageData_C9)data;
						if(dataC9.getContent() != null)
							dataC9.setContent(fillInContent(dataC9.getContent()));
						break;
					case FILE_XFER_2_0:
						MessageData_7E data7E = (MessageData_7E)data;
						if(data7E.getContent() != null)
							data7E.setContent(fillInContent(data7E.getContent()));
						break;
					case FILE_XFER_3_0:
						MessageData_A6 dataA6 = (MessageData_A6)data;
						if(dataA6.getContent() != null)
							dataA6.setContent(fillInContent(dataA6.getContent()));
						break;
					case TERMINAL_UPDATE_STATUS_REQUEST:
						replies = -1;
						break;
					case GENERIC_ACK:
						replies = 0;
						break;
					case INITIALIZATION_3_0:
						MessageData_8E data8E = (MessageData_8E) data;
						if(deviceSerialCd != null) {
							byte[] dsnBytes;
							switch(replySession.getDeviceType()) {
								case KIOSK:
									dsnBytes = ByteArrayUtils.fromHex(deviceSerialCd.substring(2));
									break;
								default:
									dsnBytes = deviceSerialCd.getBytes(data.getCharset());
							}
							data8E.setDeviceSerialNumBytes(dsnBytes);
						}
						break;
					case INITIALIZATION_3_1:
						MessageData_AD dataAD = (MessageData_AD) data;
						if(deviceSerialCd != null) {
							dataAD.setDeviceSerialNum(deviceSerialCd);
						}
						break;
					case INITIALIZATION_4_1:
						MessageData_C0 dataC0 = (MessageData_C0) data;
						if(deviceSerialCd != null) {
							dataC0.setDeviceSerialNum(deviceSerialCd);
						}
						break;				
				}
				replySession.addMessage(data, replies);
			} catch(ConvertException e) {
				log.warn("Could not parse row #" + rvIter.getCurrentRowNum(), e);
			}
		}
		if(replySession != null)
			executor.schedule(replySession, start + replySession.getOffset() - System.currentTimeMillis(), TimeUnit.MILLISECONDS);
		
		executor.shutdown();
		try {
			executor.awaitTermination(start + lastOffset + 300000 - System.currentTimeMillis(), TimeUnit.MILLISECONDS);
		} catch(InterruptedException e) {
			log.info("While waiting for termination", e);
		}
		taskListener.flush();
		String path;
		try {
			path = file.getCanonicalPath();
		} catch(IOException e) {
			path = file.getAbsolutePath();
		}
		log.info("Wrote task stats to '" + path + "'");
		Log.finish();
	}

	private String exportToCSV(Map<String, Object> argMap) {
		String outFile = null;
		Connection connection = null;
		try {
			outFile = ConvertUtils.getString(argMap.get("export"), null);
			connection = DataLayerMgr.getConnectionForCall("GET_DEVICE_MESSAGES");
			Call call = DataLayerMgr.getGlobalDataLayer().findCall("GET_DEVICE_MESSAGES");

			BaseConnection baseConnection = connection.unwrap(BaseConnection.class);
			CopyManager copyManager = new CopyManager(baseConnection);

			copyManager.copyOut("COPY (" + call.getSql() + ") TO STDOUT WITH HEADER CSV",
					new FileOutputStream(outFile));
		} catch (Exception e) {
			log.error("Failed exporting to file " + outFile, e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return outFile;
	}
}
