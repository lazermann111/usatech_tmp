package com.usatech.test;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;

import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
/**
 * 7C File Transfer Start --> 7D File transfer start ack
 * @author yhe
 *
 */
public class N7CMessage extends LegacyMessage {
	protected int MAX_PAYLOAD=1997;
	protected int numberOfPacketsInFileTransfer;
	protected long totalNumberOfBytesInFileTransfer;
	protected int fileTransferGroupNumber;
	protected int fileType;
	protected String fileName;
	protected BufferedInputStream input;

	public N7CMessage() {
		super();
		dataType=0x7C;
		fileType=9;//log
		fileTransferGroupNumber=1;
	}
	
	public BufferedInputStream retrieveInputStream(){
		return input;
	}
	
	@Override
	public void getParams()throws Exception{
		super.getParams();
		File f;
		if(userInputTokens==null){
			f=CommandLineUtil.getFileTransfer(-1, null);
		}else{
			f=CommandLineUtil.getFileTransfer(-1, userInputTokens.nextToken());
		}
		fileName=f.getName();
		totalNumberOfBytesInFileTransfer=f.length();
		numberOfPacketsInFileTransfer=TestClientUtil.calcNumOfBlocks(MAX_PAYLOAD,totalNumberOfBytesInFileTransfer);
		input=new BufferedInputStream(new FileInputStream(f));
		CommandLineUtil.pw.println("Successfully loaded file fileName:"+fileName);
	}
	@Override
	public ReRixMessage createMessage() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		ProcessingUtils.writeByteInt(bb, numberOfPacketsInFileTransfer);
		ProcessingUtils.writeLongInt(bb, totalNumberOfBytesInFileTransfer);
		ProcessingUtils.writeByteInt(bb, fileTransferGroupNumber);
		ProcessingUtils.writeByteInt(bb, fileType);
		ProcessingUtils.writeString(bb,fileName, ProcessingConstants.US_ASCII_CHARSET);
		return writeMessageEnd(bb);
	}

	@Override
	public String readResponseString(ReRixMessage msg) throws InvalidByteValueException {
		ByteBuffer bb=ByteBuffer.wrap(msg.getData());
		StringBuilder sb=readResponseHeader(bb);
		printResponse(sb, "FileTransferGroupNumber", ProcessingUtils.readByteInt(bb));
		return sb.toString();
	}

	public int getFileTransferGroupNumber() {
		return fileTransferGroupNumber;
	}

	public void setFileTransferGroupNumber(int fileTransferGroupNumber) {
		this.fileTransferGroupNumber = fileTransferGroupNumber;
	}

	public int getFileType() {
		return fileType;
	}

	public void setFileType(int fileType) {
		this.fileType = fileType;
	}
}
