package com.usatech.test;

import simple.lang.EnumValueLookup;
import simple.lang.InvalidValueException;

import com.usatech.layers.common.constants.EntryType;

public enum Vend3CardReaderDataSource {
	MAG_STRIPE(new byte [] { (byte)0x0c }, EntryType.SWIPE),
	CONTACTLESS(new byte [] { (byte)0x00 }, EntryType.EMV_CONTACTLESS),
	CONTACT(new byte [] { (byte)0x01 }, EntryType.EMV_CONTACT),
	CONTACT_FALLBACK_TO_MAGSTRIPE(new byte [] { (byte)0x80 }, EntryType.SWIPE);

	private static final EnumValueLookup<Vend3CardReaderDataSource, byte []> lookup = new EnumValueLookup<>(Vend3CardReaderDataSource.class);
	private EntryType entryType;
	private byte [] value;
	
	private Vend3CardReaderDataSource(byte [] value, EntryType entryType) {
		this.value = value;
		this.entryType = entryType;
	}
	
	public byte [] getValue() {
		return value;
	}
	public EntryType trackDataSourceEntryType() {
		return entryType;
	}
	
	public static Vend3CardReaderDataSource getByValue(byte[] value) throws InvalidValueException {
		return lookup.getByValue(value);
	}
	public static EntryType entryTypeForValue(byte value) throws InvalidValueException {
		return getByValue(new byte [] {value}).trackDataSourceEntryType();
	}
}