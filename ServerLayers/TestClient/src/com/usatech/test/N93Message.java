package com.usatech.test;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Date;

import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
/**
 * 93 Local Auth Batch v2.0 --> 71h
 * @author yhe
 *
 */
public class N93Message extends LegacyMessage {
	protected long transactionId=System.currentTimeMillis()/1000;
	protected Date dateAndTime=new Date();
	protected int cardType;
	protected int amount;//US dollar in pennies 3 bytes
	protected int tax;// 2 bytes
	protected int numberOfBytesInMagstripe;//no user input, get by creditCardMagstripe string len
	protected String creditCardMagstripe;
	protected int transactionResult;
	protected int numberOfVends;
	protected int vendByteLength;
	protected ArrayList<VendedItemsDescriptionBlock> vendedItemBlock=new ArrayList<VendedItemsDescriptionBlock>(5) ;
	
	public N93Message() {
		super();
		dataType=0x93;
		cardType='C';
		amount=250;
		tax=50;
		numberOfBytesInMagstripe=16;
		creditCardMagstripe="0123456789123456";
		transactionResult='S';
		numberOfVends=1;
		vendByteLength=0;
	}
	
	@Override
	public void getParams() throws Exception{
		super.getParams();
		numberOfBytesInMagstripe=creditCardMagstripe.length();
		for(int i=0; i<numberOfVends; i++){
			VendedItemsDescriptionBlock vBlock=new VendedItemsDescriptionBlock(userInputTokens);
			vBlock.getParams();
			vendedItemBlock.add(vBlock);
		}
	}

	@Override
	public ReRixMessage createMessage() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		ProcessingUtils.writeLongInt(bb,transactionId);
		bb.put(TestClientUtil.getBCDDateTime(dateAndTime));
		ProcessingUtils.writeByteInt(bb,cardType);
		ProcessingUtils.write3ByteInt(bb,amount);
		ProcessingUtils.writeShortInt(bb,tax);
		ProcessingUtils.writeByteInt(bb,numberOfBytesInMagstripe);
		ProcessingUtils.writeString(bb,creditCardMagstripe, ProcessingConstants.US_ASCII_CHARSET);
		ProcessingUtils.writeByteInt(bb,transactionResult);
		int transactionDetail=(byte)numberOfVends|((byte)vendByteLength<<6);
		ProcessingUtils.writeByteInt(bb,transactionDetail);
		for(VendedItemsDescriptionBlock vBlock: vendedItemBlock){
			vBlock.writeMessage(bb);
		}
		return writeMessageEnd(bb);
	}

	@Override
	public String readResponseString(ReRixMessage msg)throws InvalidByteValueException{
		return readResponseString71(msg);
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public int getCardType() {
		return cardType;
	}

	public void setCardType(int cardType) {
		this.cardType = cardType;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getTax() {
		return tax;
	}

	public void setTax(int tax) {
		this.tax = tax;
	}

	public String getCreditCardMagstripe() {
		return creditCardMagstripe;
	}

	public void setCreditCardMagstripe(String creditCardMagstripe) {
		this.creditCardMagstripe = creditCardMagstripe;
	}

	public int getTransactionResult() {
		return transactionResult;
	}

	public void setTransactionResult(int transactionResult) {
		this.transactionResult = transactionResult;
	}

	public int getNumberOfVends() {
		return numberOfVends;
	}

	public void setNumberOfVends(int numberOfVends) {
		this.numberOfVends = numberOfVends;
	}

	public ArrayList<VendedItemsDescriptionBlock> getVendedItemBlock() {
		return vendedItemBlock;
	}

	public void setVendedItemBlock(ArrayList<VendedItemsDescriptionBlock> vendedItemBlock) {
		this.vendedItemBlock = vendedItemBlock;
	}

}
