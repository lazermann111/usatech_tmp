package com.usatech.test;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import simple.app.Base;
import simple.io.Log;
import simple.text.FixedWidthColumn;
import simple.text.MultiRecordFixedWidthParser;
import simple.text.StringUtils.Justification;
import simple.text.ThreadSafeDateFormat;

import com.usatech.app.AbstractAttributeDatasetHandler;
import com.usatech.app.AttributeConversionException;

public class ECFtoCNFConverter extends Base {
	private static final Log log = Log.getLog();
	protected final MultiRecordFixedWidthParser<String> parser = new MultiRecordFixedWidthParser<String>();
	protected final DecimalFormat amountFormat = new DecimalFormat("000000000.00");
	protected final DecimalFormat countFormat = new DecimalFormat("000000");
	protected final ThreadSafeDateFormat confirmDateFormat = new ThreadSafeDateFormat(new SimpleDateFormat("EEEE, MMMM d, yyyy  hh:mm a z"));
	protected final ThreadSafeDateFormat standardDateFormat = new ThreadSafeDateFormat(new SimpleDateFormat("yyyyMMddHHmmss"));
	protected final ThreadSafeDateFormat createDateFormat = new ThreadSafeDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
	protected final ThreadSafeDateFormat createTimeFormat = new ThreadSafeDateFormat(new SimpleDateFormat("HH:mm:ss"));
	protected final ThreadSafeDateFormat receivedFileFormat = new ThreadSafeDateFormat(new SimpleDateFormat("yyMMdd_HHmmss"));
	protected static final Pattern ECF_FILE_PATTERN = Pattern.compile("(P)(\\d{4})(\\d{6})(\\d{6})\\.(\\w+)");
	
	protected class CNFWriter extends AbstractAttributeDatasetHandler<IOException> {
		protected final PrintWriter writer;
		protected final String fileName;
		public CNFWriter(PrintWriter writer, String fileName) {
			this.writer = writer;
			this.fileName = fileName;
		}

		@Override
		public void handleDatasetStart(String[] columnNames) throws IOException {
		}
		@Override
		public void handleRowEnd() throws IOException {
			if(log.isInfoEnabled())
				log.info("Handling row: " + data);
			try {
				String recordType = getDetailAttribute("recordType", String.class, true);
				if("FHR".equals(recordType)) {
					Date fileDate = standardDateFormat.parse(getDetailAttribute("fileCreateDate", String.class, true));
					writer.println();
					writer.println("Elavon, Inc.===>PRODUCTION_SYSTEM<===");
					writer.println("24hr. Operations number - 865-403-8663");
					writer.println("Transmission confirmation for ***> usatech");
					writer.print("Confirmation date and time: ");
					writer.println(confirmDateFormat.format(fileDate));
					writer.println();
				} else if("FTR".equals(recordType)) {
					writer.println("Record Type........ FTR");
					writer.print("Purchases Count.... ");
					writer.println(countFormat.format(getDetailAttribute("fileSaleCount", Number.class, true)));
					writer.print("Purchases Amount... ");
					writer.println(amountFormat.format(getDetailAttribute("fileSaleAmount", Number.class, true)));
					writer.print("Return Count....... ");
					writer.println(countFormat.format(getDetailAttribute("fileRefundCount", Number.class, true)));
					writer.print("Return Amount...... ");
					writer.println(amountFormat.format(getDetailAttribute("fileRefundAmount", Number.class, true)));
					writer.print("Total Count........ ");
					writer.println(countFormat.format(getDetailAttribute("fileTotalCount", Number.class, true)));
					writer.print("Net Amount......... ");
					writer.print(amountFormat.format(getDetailAttribute("fileTotalAmount", Number.class, true)));
					writer.print(' ');
					writer.println(getDetailAttribute("fileNetSign", String.class, true));
					writer.print("Record Count....... ");
					writer.println(countFormat.format(getDetailAttribute("fileRecordCount", Number.class, true)));
					Date fileDate = standardDateFormat.parse(getDetailAttribute("fileCreateDate", String.class, true));
					writer.print("Create Date........ ");
					writer.println(createDateFormat.format(fileDate));
					writer.print("Create Time........ ");
					writer.println(createTimeFormat.format(fileDate));
					writer.println();
					Matcher matcher = ECF_FILE_PATTERN.matcher(fileName);
					if(matcher.matches()) {
						writer.print("usatech_");
						writer.print(matcher.group(5));
						writer.print('-');
						writer.print(matcher.group(2));
						writer.print('_');
						writer.print(matcher.group(1));
						writer.print('.');
						writer.print(matcher.group(3));
						writer.print('_');
						writer.print(matcher.group(4));
					} else {
						writer.print("usatech_edc-");
						writer.print("8009");
						writer.print("_P.");
						writer.print(receivedFileFormat.format(fileDate));	
					}
					writer.println(" file received.");
					writer.println();
				}
			} catch(AttributeConversionException e) {
				throw new IOException(e);
			} catch(ParseException e) {
				throw new IOException(e);
			}
		}
		@Override
		public void handleDatasetEnd() throws IOException {
			writer.flush();
			writer.close();
		}
	}
	public ECFtoCNFConverter() {
		parser.setSelectorColumn("recordType", 3, Justification.LEFT, ' ', String.class);
		parser.setRecordColumns("FHR", new FixedWidthColumn[] {
				new FixedWidthColumn(14, Justification.LEFT, ' ', "fileCreateDate", String.class),
				new FixedWidthColumn(2, Justification.LEFT, ' ', "version", String.class),
				new FixedWidthColumn(4, Justification.LEFT, ' ', "destinationId", String.class),
				new FixedWidthColumn(25, Justification.LEFT, ' ', "sendingInstitutionName", String.class),
				new FixedWidthColumn(32, Justification.LEFT, ' ', "fileName", String.class),
				new FixedWidthColumn(120, Justification.LEFT, ' ', "reserved0", String.class),
		});
		parser.setRecordColumns("BHR", new FixedWidthColumn[] {
				new FixedWidthColumn(16, Justification.LEFT, ' ', "merchantId", String.class),
				new FixedWidthColumn(25, Justification.LEFT, ' ', "merchantDBA", String.class),
				new FixedWidthColumn(13, Justification.LEFT, ' ', "merchantCity", String.class),
				new FixedWidthColumn(2, Justification.LEFT, ' ', "merchantState", String.class),
				new FixedWidthColumn(9, Justification.LEFT, ' ', "merchantZip", String.class),
				new FixedWidthColumn(3, Justification.LEFT, ' ', "merchantCountry", String.class),
				new FixedWidthColumn(4, Justification.LEFT, ' ', "reserved1", String.class),
				new FixedWidthColumn(8, Justification.LEFT, ' ', "settlementDate", String.class),
				new FixedWidthColumn(11, Justification.RIGHT, '0', "batchNumber", Long.class),
				new FixedWidthColumn(4, Justification.LEFT, ' ', "networkIdentifier", String.class),
				new FixedWidthColumn(11, Justification.RIGHT, '0', "batchReferenceNumber", Long.class),
				new FixedWidthColumn(4, Justification.LEFT, ' ', "clientGroup", String.class),
				new FixedWidthColumn(12, Justification.LEFT, ' ', "mpsNumber", String.class),
				new FixedWidthColumn(2, Justification.LEFT, ' ', "batchResponseCode", String.class),
				new FixedWidthColumn(1, Justification.LEFT, ' ', "batchType", String.class),
				new FixedWidthColumn(72, Justification.LEFT, ' ', "reserved2", String.class),
		});
		parser.setRecordColumns("DTR", new FixedWidthColumn[] {
				new FixedWidthColumn(1, Justification.LEFT, ' ', "transactionCode", String.class),
				new FixedWidthColumn(2, Justification.LEFT, ' ', "cardType", String.class),
				new FixedWidthColumn(20, Justification.LEFT, ' ', "cardNumber", String.class),
				new FixedWidthColumn(4, Justification.LEFT, ' ', "expirationDate", String.class),
				new FixedWidthColumn(10, Justification.RIGHT, '0', "settlementAmount", Long.class),
				new FixedWidthColumn(14, Justification.LEFT, ' ', "authorizationDate", String.class),
				new FixedWidthColumn(6, Justification.LEFT, ' ', "approvalCode", String.class),
				new FixedWidthColumn(2, Justification.LEFT, ' ', "posEntryMode", String.class),
				new FixedWidthColumn(1, Justification.LEFT, ' ', "authSourceCode", String.class),
				new FixedWidthColumn(1, Justification.LEFT, ' ', "cardHolderIdMethod", String.class),
				new FixedWidthColumn(1, Justification.LEFT, ' ', "cardPresentIndicator", String.class),
				new FixedWidthColumn(11, Justification.RIGHT, '0', "referenceNumber", String.class),
				new FixedWidthColumn(1, Justification.LEFT, ' ', "visaACI", String.class),
				new FixedWidthColumn(15, Justification.LEFT, ' ', "cardTransactionId", String.class),
				new FixedWidthColumn(4, Justification.LEFT, ' ', "cardValidationCode", String.class),
				new FixedWidthColumn(1, Justification.LEFT, ' ', "modeIndicator", String.class),
				new FixedWidthColumn(1, Justification.LEFT, ' ', "cardholdActivatedTerminal", String.class),
				new FixedWidthColumn(1, Justification.LEFT, ' ', "posCapability", String.class),
				new FixedWidthColumn(2, Justification.LEFT, ' ', "responseCode", String.class),
				new FixedWidthColumn(3, Justification.LEFT, ' ', "currencyCode", String.class),
				new FixedWidthColumn(8, Justification.RIGHT, '0', "authAmount", Long.class),
				new FixedWidthColumn(1, Justification.LEFT, ' ', "avsResult", String.class),
				new FixedWidthColumn(1, Justification.LEFT, ' ', "purchaseIdentifierFormatCode", String.class),
				new FixedWidthColumn(2, Justification.LEFT, ' ', "debitNetworkId", String.class),
				new FixedWidthColumn(4, Justification.LEFT, ' ', "debitSettlementDate", String.class),
				new FixedWidthColumn(4, Justification.LEFT, ' ', "merchantCategory", String.class),
				new FixedWidthColumn(4, Justification.LEFT, ' ', "itemNumber", String.class),
				new FixedWidthColumn(1, Justification.LEFT, ' ', "msdiIndicator", String.class),
				new FixedWidthColumn(1, Justification.LEFT, ' ', "ucafIndicator", String.class),
				new FixedWidthColumn(8, Justification.RIGHT, '0', "debitSurcharge", Long.class),
				new FixedWidthColumn(13, Justification.RIGHT, '0', "waybillNumber", Long.class),
				new FixedWidthColumn(15, Justification.LEFT, ' ', "terminalCd", String.class),
				new FixedWidthColumn(4, Justification.LEFT, ' ', "ecsChargeType", String.class),
				new FixedWidthColumn(8, Justification.RIGHT, '0', "convenienceFee", Long.class),
				new FixedWidthColumn(1, Justification.LEFT, ' ', "cvvResult", String.class),
				new FixedWidthColumn(2, Justification.LEFT, ' ', "accountLevelProcessing", String.class),
				new FixedWidthColumn(19, Justification.LEFT, ' ', "reserved3", String.class),
			});
		parser.setRecordColumns("BTR", new FixedWidthColumn[] {
				new FixedWidthColumn(6, Justification.RIGHT, '0', "batchSaleCount", Integer.class),
				new FixedWidthColumn(11, Justification.RIGHT, '0', "batchSaleAmount", Long.class),
				new FixedWidthColumn(6, Justification.RIGHT, '0', "batchRefundCount", Integer.class),
				new FixedWidthColumn(11, Justification.RIGHT, '0', "batchRefundAmount", Long.class),
				new FixedWidthColumn(6, Justification.RIGHT, '0', "batchTotalCount", Integer.class),
				new FixedWidthColumn(11, Justification.RIGHT, '0', "batchTotalAmount", Long.class),
				new FixedWidthColumn(1, Justification.LEFT, ' ', "batchNetSign", String.class),
				new FixedWidthColumn(11, Justification.RIGHT, '0', "batchNumber", Long.class),
				new FixedWidthColumn(134, Justification.LEFT, ' ', "reserved4", String.class),
		});
		parser.setRecordColumns("FTR", new FixedWidthColumn[] {
				new FixedWidthColumn(6, Justification.RIGHT, '0', "fileSaleCount", Integer.class),
				new FixedWidthColumn(11, Justification.RIGHT, '0', "fileSaleAmount", Long.class),
				new FixedWidthColumn(6, Justification.RIGHT, '0', "fileRefundCount", Integer.class),
				new FixedWidthColumn(11, Justification.RIGHT, '0', "fileRefundAmount", Long.class),
				new FixedWidthColumn(6, Justification.RIGHT, '0', "fileTotalCount", Integer.class),
				new FixedWidthColumn(11, Justification.RIGHT, '0', "fileTotalAmount", Long.class),
				new FixedWidthColumn(1, Justification.LEFT, ' ', "fileNetSign", String.class),
				new FixedWidthColumn(6, Justification.RIGHT, '0', "fileRecordCount", Integer.class),
				new FixedWidthColumn(14, Justification.LEFT, ' ', "fileCreateDate", String.class),
				new FixedWidthColumn(125, Justification.LEFT, ' ', "reserved5", String.class),
		});		
		parser.setRecordColumns("MAA", new FixedWidthColumn[] {
				new FixedWidthColumn(25, Justification.LEFT, ' ', "merchantDBAName", String.class),
				new FixedWidthColumn(13, Justification.LEFT, ' ', "merchantCity", String.class),
				new FixedWidthColumn(2, Justification.LEFT, ' ', "merchantState", String.class),
				new FixedWidthColumn(9, Justification.LEFT, ' ', "merchantPostal", String.class),
				new FixedWidthColumn(3, Justification.LEFT, ' ', "merchantCountry", String.class),
				new FixedWidthColumn(30, Justification.LEFT, ' ', "merchantAddress", String.class),
				new FixedWidthColumn(115, Justification.LEFT, ' ', "reserved6", String.class),
		});			
		parser.setRecordColumns("DMA", new FixedWidthColumn[] {
				new FixedWidthColumn(25, Justification.LEFT, ' ', "purchaseIdentifier", String.class),
				new FixedWidthColumn(1, Justification.LEFT, ' ', "avsResult", String.class),
				new FixedWidthColumn(2, Justification.RIGHT, '0', "installmentSequence", Integer.class),
				new FixedWidthColumn(2, Justification.RIGHT, '0', "installmentCount", Integer.class),
				new FixedWidthColumn(10, Justification.RIGHT, '0', "totalAuthorizedAmount", Long.class),
				new FixedWidthColumn(10, Justification.LEFT, ' ', "customerServiceNumber", String.class),
				new FixedWidthColumn(8, Justification.LEFT, ' ', "shipDate", String.class),
				new FixedWidthColumn(139, Justification.LEFT, ' ', "reserved7", String.class),
		});	
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new ECFtoCNFConverter().run(args);
	}
	
	@Override
	public Properties getProperties(String propertiesFile, Class<?> appClass, Properties defaults) throws IOException {
		return new Properties(defaults);
	}

	@Override
	protected void execute(Map<String, Object> argMap, Properties properties) {
		String tmp = properties.getProperty("inputDirectory");
		if(tmp == null || (tmp=tmp.trim()).length() == 0) {
			finishAndExit("Input Directory not provided", 101, null);
			return;
		}
		File inputDirectory = new File(tmp);
		if(!inputDirectory.isDirectory()) {
			finishAndExit("Input Directory '" + getFilePathSafetly(inputDirectory) + "' is not a directory", 103, null);
			return;
		}
		tmp = properties.getProperty("outputDirectory");
		if(tmp == null || (tmp=tmp.trim()).length() == 0) {
			finishAndExit("Output Directory not provided", 102, null);
			return;
		}
		File outputDirectory = new File(tmp);
		if(!outputDirectory.isDirectory()) {
			finishAndExit("Output Directory '" + getFilePathSafetly(outputDirectory) + "' is not a directory", 104, null);
			return;
		}
		tmp = properties.getProperty("processedDirectory", getFilePathSafetly(inputDirectory) + "/processed");
		if(tmp == null || (tmp=tmp.trim()).length() == 0) {
			finishAndExit("Processed Directory not provided", 106, null);
			return;
		}
		File processedDirectory = new File(tmp);
		if(!processedDirectory.exists())
			processedDirectory.mkdirs();
		if(!processedDirectory.isDirectory()) {
			finishAndExit("Processed Directory '" + getFilePathSafetly(processedDirectory) + "' is not a directory", 108, null);
			return;
		}
		
		tmp = properties.getProperty("ecfPattern", "P8009\\d{12}\\.uat");
		final Pattern filenamePattern = Pattern.compile(tmp);
		File[] ecfFiles = inputDirectory.listFiles(new FilenameFilter() {			
			@Override
			public boolean accept(File dir, String name) {
				return filenamePattern.matcher(name).matches();
			}
		});
		
		for(File ecfFile : ecfFiles) {
			parseECFFile(ecfFile, outputDirectory, processedDirectory);
		}
	}
	
	protected void parseECFFile(File ecfFile, File outputDirectory, File processedDirectory) {
		File cnfFile = new File(outputDirectory,  ecfFile.getName().replaceAll(".\\w{3}$", ".cnf"));
		try {
			FileWriter writer = new FileWriter(cnfFile);
			log.debug("Parsing ECF file '" + getFilePathSafetly(ecfFile) + "' into CNF file '" + getFilePathSafetly(cnfFile) + "'");
			Reader reader = new FileReader(ecfFile);
			try {
				parser.parse(reader, new CNFWriter(new PrintWriter(writer), ecfFile.getName()));
			} finally {
				reader.close();
			}
			log.debug("Successfully parsed ECF file '" + getFilePathSafetly(ecfFile) + "' into CNF file '" + getFilePathSafetly(cnfFile) + "'");
			if(processedDirectory != null) {
				File newFile = new File(processedDirectory, ecfFile.getName());
				if(ecfFile.renameTo(newFile))
					log.debug("Moved processed ECF file '" + getFilePathSafetly(ecfFile) + "' to '" + newFile +"'");
				else
					log.warn("Could not move processed ECF file '" + getFilePathSafetly(ecfFile) + "' to '" + newFile +"'");
			}			
		} catch(IOException e) {
			log.error("Failed to parse ECF file '" + getFilePathSafetly(ecfFile) + "' or to write CNF file '" + getFilePathSafetly(cnfFile) + "'", e);
		}
	}
	protected String getFilePathSafetly(File file) {
		try {
			return file.getCanonicalPath();
		} catch(IOException e) {
			return file.getAbsolutePath();
		}
	}
	@Override
	protected void registerDefaultCommandLineArguments() {
		registerCommandLineSwitch('i', "input-dir", false, true, "inputDirectory", "The input directory that holds the ECF files");
		registerCommandLineSwitch('o', "output-dir", false, true, "outputDirectory", "The output directory into which the ACS files will be put");
		registerCommandLineSwitch('e', "ecf-pattern", true, true, "ecfPattern", "The regular expression used to find ECF files ['P8009\\d{12}\\.uat' by default]");
		registerCommandLineSwitch('p', "processed-dir", true, true, "processedDirectory", "The directory into which ECF files are moved once successfully processed");
	}
}
