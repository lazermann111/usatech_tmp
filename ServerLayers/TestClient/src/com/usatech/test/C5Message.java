package com.usatech.test;

import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.util.Calendar;
import java.util.StringTokenizer;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
/**
 * Settlement C5-->CB response
 * @author yhe
 *
 */
public class C5Message extends V4Message {
	protected long batchId;
	protected long newBatchId;
	//transactionId of parent event or next master id
	protected long eventId;
	//2 Fill 3 scheduled
	protected int eventType;
	protected Calendar timeOfCreation;
	protected long timeOfCreationUTCTime;
	protected int timeOfCreationUTCOffset;
	protected int contentFormat;
	protected SubMessage payload;
	public C5Message() {
		super();
		dataType=0xC5;
		batchId=0;
		newBatchId=1;
		eventId=System.currentTimeMillis()/1000;
		eventType=2;
		timeOfCreation=Calendar.getInstance();
		timeOfCreationUTCTime=System.currentTimeMillis();
		timeOfCreationUTCOffset=Calendar.getInstance().get(Calendar.ZONE_OFFSET);
		contentFormat=0;
	}
	@Override
	public void getParams() throws Exception{
		super.getParams();
		timeOfCreation=TestClientUtil.getTimeByUTCAndOffset(timeOfCreationUTCTime, timeOfCreationUTCOffset);
		if(contentFormat==0){
			payload=new PayloadFormat0(userInputTokens);
		}else if (contentFormat==1){
			payload=new PayloadFormat1(userInputTokens);
		}
		if(payload!=null){
			payload.getParams();
		}

	}
	protected class PayloadFormat0 extends SubMessage{
		protected long iCashVendedItems;
		protected BigDecimal iCashVendedAmount;
		protected long iCashlessVendedItems;
		protected BigDecimal iCashlessVendedAmount;
		
		public PayloadFormat0(StringTokenizer userInputTokens){
			super(userInputTokens);
			iCashVendedItems=1;
			iCashVendedAmount=new BigDecimal(100);
			iCashlessVendedItems=1;
			iCashlessVendedAmount=new BigDecimal(100);
		}

		@Override
		public void writeMessage(ByteBuffer bb){
			ProcessingUtils.writeLongInt(bb,iCashVendedItems);
			ProcessingUtils.writeStringAmount(bb,iCashVendedAmount);
			ProcessingUtils.writeLongInt(bb,iCashlessVendedItems);
			ProcessingUtils.writeStringAmount(bb,iCashlessVendedAmount);
		}

		public long getICashVendedItems() {
			return iCashVendedItems;
		}

		public void setICashVendedItems(long cashVendedItems) {
			iCashVendedItems = cashVendedItems;
		}

		public BigDecimal getICashVendedAmount() {
			return iCashVendedAmount;
		}

		public void setICashVendedAmount(BigDecimal cashVendedAmount) {
			iCashVendedAmount = cashVendedAmount;
		}

		public long getICashlessVendedItems() {
			return iCashlessVendedItems;
		}

		public void setICashlessVendedItems(long cashlessVendedItems) {
			iCashlessVendedItems = cashlessVendedItems;
		}

		public BigDecimal getICashlessVendedAmount() {
			return iCashlessVendedAmount;
		}

		public void setICashlessVendedAmount(BigDecimal cashlessVendedAmount) {
			iCashlessVendedAmount = cashlessVendedAmount;
		}

	}
	
	protected class PayloadFormat1 extends SubMessage{
		protected long iCashVendedItems;
		protected BigDecimal iCashVendedAmount;
		protected long iCashlessVendedItems;
		protected BigDecimal iCashlessVendedAmount;
		protected long aCashVendedItems;
		protected BigDecimal aCashVendedAmount;
		protected long aCashlessVendedItems;
		protected BigDecimal aCashlessVendedAmount;
		public PayloadFormat1(StringTokenizer userInputTokens){
			super(userInputTokens);
			iCashVendedItems=1;
			iCashVendedAmount=new BigDecimal(100);
			iCashlessVendedItems=1;
			iCashlessVendedAmount=new BigDecimal(100);
			aCashVendedItems=1;
			aCashVendedAmount=new BigDecimal(100);
			aCashlessVendedItems=1;
			aCashlessVendedAmount=new BigDecimal(100);
		}
		
		@Override
		public void writeMessage(ByteBuffer bb){
			ProcessingUtils.writeLongInt(bb,iCashVendedItems);
			ProcessingUtils.writeStringAmount(bb,iCashVendedAmount);
			ProcessingUtils.writeLongInt(bb,iCashlessVendedItems);
			ProcessingUtils.writeStringAmount(bb,iCashlessVendedAmount);
			ProcessingUtils.writeLongInt(bb,aCashVendedItems);
			ProcessingUtils.writeStringAmount(bb,aCashVendedAmount);
			ProcessingUtils.writeLongInt(bb,aCashlessVendedItems);
			ProcessingUtils.writeStringAmount(bb,aCashlessVendedAmount);
		}

		public long getACashVendedItems() {
			return aCashVendedItems;
		}

		public void setACashVendedItems(long cashVendedItems) {
			aCashVendedItems = cashVendedItems;
		}

		public BigDecimal getACashVendedAmount() {
			return aCashVendedAmount;
		}

		public void setACashVendedAmount(BigDecimal cashVendedAmount) {
			aCashVendedAmount = cashVendedAmount;
		}

		public long getACashlessVendedItems() {
			return aCashlessVendedItems;
		}

		public void setACashlessVendedItems(long cashlessVendedItems) {
			aCashlessVendedItems = cashlessVendedItems;
		}

		public BigDecimal getACashlessVendedAmount() {
			return aCashlessVendedAmount;
		}

		public void setACashlessVendedAmount(BigDecimal cashlessVendedAmount) {
			aCashlessVendedAmount = cashlessVendedAmount;
		}

		public long getICashVendedItems() {
			return iCashVendedItems;
		}

		public void setICashVendedItems(long cashVendedItems) {
			iCashVendedItems = cashVendedItems;
		}

		public BigDecimal getICashVendedAmount() {
			return iCashVendedAmount;
		}

		public void setICashVendedAmount(BigDecimal cashVendedAmount) {
			iCashVendedAmount = cashVendedAmount;
		}

		public long getICashlessVendedItems() {
			return iCashlessVendedItems;
		}

		public void setICashlessVendedItems(long cashlessVendedItems) {
			iCashlessVendedItems = cashlessVendedItems;
		}

		public BigDecimal getICashlessVendedAmount() {
			return iCashlessVendedAmount;
		}

		public void setICashlessVendedAmount(BigDecimal cashlessVendedAmount) {
			iCashlessVendedAmount = cashlessVendedAmount;
		}
	}
	@Override
	public ReRixMessage createMessage() {
		ByteBuffer bb=ByteBuffer.allocate(1065);
		writeMessageHeader(bb);
		ProcessingUtils.writeLongInt(bb,batchId);
		ProcessingUtils.writeLongInt(bb,newBatchId);
		ProcessingUtils.writeLongInt(bb,eventId);
		ProcessingUtils.writeByteInt(bb,eventType);
		ProcessingUtils.writeTimestamp(bb,timeOfCreation);
		ProcessingUtils.writeByteInt(bb,contentFormat);
		if(payload!=null){
			payload.writeMessage(bb);
		}
		return writeMessageEnd(bb);
	}
	public long getBatchId() {
		return batchId;
	}
	public void setBatchId(long batchId) {
		this.batchId = batchId;
	}
	public long getNewBatchId() {
		return newBatchId;
	}
	public void setNewBatchId(long newBatchId) {
		this.newBatchId = newBatchId;
	}
	public int getEventType() {
		return eventType;
	}
	public void setEventType(int eventType) {
		this.eventType = eventType;
	}
	public int getContentFormat() {
		return contentFormat;
	}
	public void setContentFormat(int contentFormat) {
		this.contentFormat = contentFormat;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception{
		C5Message sMsg=new C5Message();
		ClientConnectUtil conn=new ClientConnectUtil();
		conn.setProtocol(7);
		conn.startCommunication();
		sMsg.getParams();
		ReRixMessage sentMsg=sMsg.createMessage();
		ReRixMessage responseMsg=conn.sendMessage(sentMsg, sMsg.encryptKey);
		sMsg.readResponse(responseMsg);
		conn.stopCommunication();
	}
	public long getEventId() {
		return eventId;
	}
	public void setEventId(long eventId) {
		this.eventId = eventId;
	}
	public SubMessage getPayload() {
		return payload;
	}
	public long getTimeOfCreationUTCTime() {
		return timeOfCreationUTCTime;
	}
	public void setTimeOfCreationUTCTime(long timeOfCreationUTCTime) {
		this.timeOfCreationUTCTime = timeOfCreationUTCTime;
	}
	public int getTimeOfCreationUTCOffset() {
		return timeOfCreationUTCOffset;
	}
	public void setTimeOfCreationUTCOffset(int timeOfCreationUTCOffset) {
		this.timeOfCreationUTCOffset = timeOfCreationUTCOffset;
	}
	public void setPayload(SubMessage payload) {
		this.payload = payload;
	}
}
