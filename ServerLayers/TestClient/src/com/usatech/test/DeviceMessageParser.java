/**
 *
 */
package com.usatech.test;

import java.io.IOException;
import java.io.PrintStream;
import java.nio.ByteBuffer;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import simple.app.Base;
import simple.app.MainWithConfig;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.ByteArrayUtils;
import simple.io.Log;
import simple.results.Results;

import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageDataFactory;
import com.usatech.layers.common.messagedata.MessageDirection;


/**
 * @author Brian S. Krug
 *
 */
public class DeviceMessageParser extends MainWithConfig {
	private static final Log log = Log.getLog();
	@Override
	protected void registerDefaultCommandLineArguments() {
		registerCommandLineSwitch('p', "propertiesFile", true, true, "properties-file", "The properties file to use");
		registerCommandLineSwitch('m', "messageIds", true, true, "message-ids", "The list of message ids to retrieve");
		}
	@Override
	protected void registerDefaultActions() {
		//None
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new DeviceMessageParser().run(args);
	}

	@Override
	public void run(String[] args) {
		Map<String,Object> argMap;
		try {
			argMap = parseArguments(args);
		} catch(IOException e) {
			Log.finish();
			System.err.println(e.getMessage());
			printUsage(System.err);
			System.exit(100);
			return;
		}
		Properties properties;
		try {
			properties = getProperties(ConvertUtils.getStringSafely(argMap.get("properties-file")), getClass(), null);
		} catch(IOException e) {
			Log.finish();
			System.err.println("Could not read properties file");
			e.printStackTrace();
			System.exit(120);
			return;
		}
		for(Map.Entry<String,Object> entry : argMap.entrySet()) {
			properties.put(entry.getKey(), entry.getValue());
		}

		log.info("Loading configuration...");
		try {
			Base.configureDataSourceFactory(properties, null);
	        Base.configureDataLayer(properties);
	    } catch(ServiceException e) {
			Log.finish();
			System.err.println("Could not configure the data layer");
			e.printStackTrace();
			System.exit(150);
			return;
		}
	    Number[] messageIds;
		try {
			messageIds = ConvertUtils.convertRequired(Number[].class, properties.get("message-ids"));
		} catch(ConvertException e) {
			Log.finish();
			System.err.println("Could not convert message ids");
			e.printStackTrace();
			System.exit(180);
			return;
		}
	    Map<String,Object> params = new LinkedHashMap<String, Object>();
	    params.put("deviceMessageIds", messageIds);
		Results results;
		try {
			results = DataLayerMgr.executeQuery("GET_SPECIFIC_DEVICE_MESSAGES", params);
		} catch(SQLException e) {
			System.err.println("Error while getting device messages");
			e.printStackTrace();
			System.exit(160);
			return;
		} catch(DataLayerException e) {
			System.err.println("Error while getting device messages");
			e.printStackTrace();
			System.exit(170);
			return;
		}
		PrintStream ps = System.out;
		for(int i = 1; results.next(); i++) {
			ps.print("Message #");
			ps.print(i+1);
			ps.print(": ");
			ps.print(results.getValue("deviceMessageId"));
			ps.print(" [DeviceName=");
			ps.print(results.getValue("deviceName"));
			ps.print("; SessionId=");
			ps.print(results.getValue("deviceSessionid"));
			ps.print("; Inbound UTC=");
			ps.print(results.getValue("inboundMessageUTC"));
			ps.print("; Outbound UTC=");
			ps.print(results.getValue("outboundMessageUTC"));
			ps.print("; Duration (millis)=");
			ps.print(results.getValue("messageDurationMillis"));
			String inboundHex;
			String outboundHex;
			try {
				inboundHex = results.getValue("inboundMessage", String.class);
				outboundHex = results.getValue("outboundMessage", String.class);
			} catch(ConvertException e) {
				log.warn("Could not convert inbound or outbound message to a String", e);
				inboundHex = "ERROR";
				outboundHex = "ERROR";
			}
			ps.print("; Inbound Message=");
			ps.print(inboundHex);
			ps.print("; Outbound Message=");
			ps.print(outboundHex);
			ps.println(']');
			MessageData inboundMessage = null;
			MessageData outboundMessage = null;
			try {
				inboundMessage = MessageDataFactory.readMessage(ByteBuffer.wrap(ByteArrayUtils.fromHex(inboundHex)), MessageDirection.CLIENT_TO_SERVER);
			} catch(RuntimeException e) {
				log.warn("Could not parse message", e);
			}
			try {
				outboundMessage = MessageDataFactory.readMessage(ByteBuffer.wrap(ByteArrayUtils.fromHex(outboundHex)), MessageDirection.SERVER_TO_CLIENT);
			} catch(RuntimeException e) {
				log.warn("Could not parse message", e);
			}
			ps.print("--> ");
			if(inboundMessage != null)
				ps.println(inboundMessage);
			else
				ps.println(inboundHex);
			ps.print("<-- ");
			if(outboundMessage != null)
				ps.println(outboundMessage);
			else
				ps.println(outboundHex);

			ps.println("------------------------");
		}
		ps.println();
		Log.finish();
        System.exit(0);
	}

	protected void printDeviceInfo(PrintStream out, Map<String,Object> params) {
		out.print(params.get("deviceName"));
		out.print(": [");
		boolean first = true;
		for(Map.Entry<String,Object> entry : params.entrySet()) {
			if(!"deviceName".equals(entry.getKey())) {
				if(first)
					first = false;
				else
					out.print("; ");
				out.print(entry.getKey());
				out.print('=');
				out.print(entry.getValue());
			}
		}
		out.print(']');
	}
}
