package com.usatech.test;

import java.beans.IntrospectionException;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TimeZone;

import simple.bean.ConvertException;
import simple.bean.ReflectionUtils;
import simple.io.Log;

public class CommandLineUtil {
	private static final Log log = Log.getLog();
	protected static PrintWriter pw=new PrintWriter(System.out, true);
	protected static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	public static final HashMap<String, BaseMessage> clientMessageMap=new HashMap<String, BaseMessage>();
    public static int interactiveCount=0;
    public static final java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("HHmmssMMddyyyy");
	protected static Set<String> EXCLUDE_INPUT_PROPERTIES = Collections.singleton("lastMessageId");
	static{
		//EDGE message
		clientMessageMap.put("C0", new C0Message());//Initialization(C0)
		clientMessageMap.put("C1", new C1Message());//Component Identification(C1)
		clientMessageMap.put("C2", new C2Message());//Authorization(C2)
		clientMessageMap.put("C4", new C4Message());//Sale(C4)
		clientMessageMap.put("C5", new C5Message());//Settlement(C5)
		clientMessageMap.put("C6", new C6Message());//Generic Event(C6)
		clientMessageMap.put("C7", new C7Message());//Short File Transfer (C7 input file path)
		clientMessageMap.put("C8", new C8Message());//File Transfer (C8 input file path, send related C9)
		clientMessageMap.put("C9", new C9Message());
		clientMessageMap.put("C7M", new C7MessageManual());//Short File Transfer (manaul C7)
		clientMessageMap.put("C8M", new C8MessageManual());//File Transfer Start (manaul C8, also send related C9)
		clientMessageMap.put("C9M", new C9MessageManual());
		clientMessageMap.put("CA", new CAMessage());//Generic Request(CA)
		clientMessageMap.put("CB", new CBMessage());//Generic Response(CB)
		
		//v3
		clientMessageMap.put("A0", new A0Message());//A1 Authorization
		clientMessageMap.put("A2", new A2Message());//A2 Network Authorization Batch v2.1
		clientMessageMap.put("A2Kiosk", new A2KioskMessage());//A2 Network Authorization Batch v2.1-- kiosk specific transaction detail
		clientMessageMap.put("92", new N92Message());// 92 Terminal Update Status Request
		clientMessageMap.put("A3", new A3Message());// A3 Local auth batch v2.2
		clientMessageMap.put("A3Kiosk", new A3KioskMessage());// A3 Local auth batch v2.2 kiosk alternate transaction detail
		clientMessageMap.put("2A", new N2AMessage());// 2A Network Authorization Batch 2.0
		clientMessageMap.put("2AKiosk", new N2AKioskMessage());
		clientMessageMap.put("2B", new N2BMessage());//2B Local auth v2.0
		clientMessageMap.put("2BKiosk", new N2BKioskMessage());
		clientMessageMap.put("2E", new N2EMessage());//2E Generic Data Log
		clientMessageMap.put("2F", new N2FMessage());//2F Generic Ack
		clientMessageMap.put("5C", new N5CMessage());//5C I Am Alive Alert
		clientMessageMap.put("5E", new N5EMessage());//5E Auth Request v2.0
		clientMessageMap.put("75", new N75Message());//75 Device Control Ack
		clientMessageMap.put("80", new N80Message());//80 Kill File Transfer
		clientMessageMap.put("82", new N82Message());//82 Client to Server Request
		clientMessageMap.put("86", new N86Message());//86 Gx Counters
		clientMessageMap.put("8E", new N8EMessage());//8E Initialization v3.0
		clientMessageMap.put("91", new N91Message());//91 Generic NAK
		clientMessageMap.put("93", new N93Message());//93 Local auth v2.0 BCD Time Format
		clientMessageMap.put("94", new N94Message());//94 Cash Sale Detail 2.0
		clientMessageMap.put("96", new N96Message());//96 Cash Sale Detail 3.0
		clientMessageMap.put("99", new N99Message());//99 Client Version
		clientMessageMap.put("A8", new A8Message());//A8 Counters 2.0
		clientMessageMap.put("AA", new AAMessage());//AA Permission Request 1.0
		clientMessageMap.put("AC", new ACMessage());//AC Authorization Request V3.1
		clientMessageMap.put("AD", new ADMessage());//AD Initialization v3.1
		clientMessageMap.put("B9", new B9Message());//B9 Sony Info Log v2.0
		clientMessageMap.put("7C", new N7CMessage());//7C file transfer start
		clientMessageMap.put("A4", new A4Message());//A4 file transfer start v1.1
		clientMessageMap.put("9B", new N9BMessage());
		clientMessageMap.put("A5", new A5Message());
		clientMessageMap.put("A7", new A7Message());
		//Esuds message
		clientMessageMap.put("9A47", new N9A47Message());//9A47 Room Status
		clientMessageMap.put("9A41", new N9A41Message());//9A41 Room Status With Starting Port Number
		clientMessageMap.put("9A6A", new N9A6AMessage());//9A6A Washer/Dryer Labels
		clientMessageMap.put("9A62", new N9A62Message());//9A62 Washer/Dryer Diagnostics
		clientMessageMap.put("9A63", new N9A63Message());//9A63 Room Model Info/Layout 2.0
		clientMessageMap.put("9A5E", new N9A5EMessage());//9A5E Actual Purchase Network Auth Batch
		clientMessageMap.put("9A5F", new N9A5FMessage());//9A5F eSuds Local Auth Batch
		clientMessageMap.put("9A60", new N9A60Message());//9A60 Intended Purchases Network Auth Batch
	
		//add hex for testing if you know the command hex
		clientMessageMap.put("Hex", new HexMessage());
		clientMessageMap.put("0005", new NetLayerHealthCheck());
		clientMessageMap.put("0006", new AppLayerHealthCheck());
	}
	
	public static void getUserInput(Object sMsg) throws IntrospectionException, IOException, ParseException, InvocationTargetException, InstantiationException, ConvertException, IllegalAccessException{
		Map<String, ?> aMap = ReflectionUtils.toPropertyMap(sMsg, EXCLUDE_INPUT_PROPERTIES);
		aMap.remove("class");
		aMap.remove("messageTypeName");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String userInput;
		for(Map.Entry<String,?> entry : aMap.entrySet()) {
			if(!((entry.getValue() instanceof SubMessage)||(entry.getValue() instanceof ArrayList))){
				userInput=getUserInput("Input "+sMsg.getClass().getSimpleName()+":\""+entry.getKey()+"\" Default["+entry.getValue()+"]:");
				interactiveCount++;
				if(!userInput.equals("")){
					ReflectionUtils.setProperty(sMsg, entry.getKey(), userInput, true);
				}
			}
		}
	}
	
	public static void getUserInputByArg(Object sMsg, StringTokenizer userInputTokens) throws IntrospectionException, IOException, ParseException, InvocationTargetException, InstantiationException, ConvertException, IllegalAccessException{
		Map<String, ?> aMap = ReflectionUtils.toPropertyMap(sMsg, EXCLUDE_INPUT_PROPERTIES);
		aMap.remove("class");
		aMap.remove("messageTypeName");
		String userInput;
		for(Map.Entry<String,?> entry : aMap.entrySet()) {
			if(!((entry.getValue()==null)||(entry.getValue() instanceof SubMessage)||(entry.getValue() instanceof ArrayList))){
				userInput=userInputTokens.nextToken();
				if(!userInput.trim().equals("")){
					ReflectionUtils.setProperty(sMsg, entry.getKey(), userInput, true);
				}
			}
		}
	}
	
	public static boolean confirmSendMessage(BaseMessage sMsg)throws IntrospectionException, IOException{
		String msg=printMessage(sMsg);
		pw.println("Message Sent as follows:");
		pw.println(sMsg.getMessageTypeName());
		pw.print(msg);
		String userInput=getUserInput("Please confirm sending: Default[Y] or N:");
		if(userInput.equalsIgnoreCase("N")){
			return false;
		}else if (userInput.equalsIgnoreCase("Y")||userInput.equals("")){
			return true;
		}else{
			pw.println("Unknown confirm input:"+userInput);
			return false;
		}
	}
	public static void logSendMessage(BaseMessage sMsg)throws IntrospectionException, IOException{
		String msg=printMessage(sMsg);
		log.info("Message Sent as follows:");
		log.info(sMsg.getMessageTypeName());
		log.info(msg);
	}
	public static boolean confirmContinue() throws IOException{
		String userInput=getUserInput("Continue to send message in the same connection? Default[Y] or N:");
		if (userInput.equalsIgnoreCase("N")){
			return false;
		}else if(userInput.equalsIgnoreCase("Y")||userInput.equals("")){
			return true;
		}else{
			pw.println("Unknown confirm input:"+userInput);
			return false;
		}
	}
	
	public static BaseMessage inputMessageType() throws IOException, ConvertException{
		pw.println("Here are the available message types you can send:");
		pw.println(clientMessageMap.keySet());
		pw.println();
		String userInput=getUserInput("Please input message type to send:");
		return clientMessageMap.get(userInput);
	}
	
	public static BaseMessage inputMessageType(String userInput) throws IOException, ConvertException{
		return clientMessageMap.get(userInput);
	}
	
	public static String printMessage(Object sMsg)throws IntrospectionException{
		return printMessage(0, sMsg);
	}
	public static String printMessage(int indent, Object sMsg)throws IntrospectionException{
		StringBuilder sb=new StringBuilder();
		String indentStr="";
		if(indent>0){
			char[] indentSpace=new char[indent];
			for(int i=0; i<indent; i++){
				indentSpace[i]=' ';
			}
			indentStr=new String(indentSpace);
		}
		Map<String, ?> aMap = ReflectionUtils.toPropertyMap(sMsg, EXCLUDE_INPUT_PROPERTIES);
		aMap.remove("class");
		for(Map.Entry<String,?> entry : aMap.entrySet()) {
			Object valueObj=entry.getValue();
			if(valueObj instanceof SubMessage){
				sb.append(indentStr+"Key:"+entry.getKey()+"\n"); 
				sb.append(printMessage(2, valueObj));
			}else if (valueObj instanceof ArrayList){
				sb.append(indentStr+"Key:"+entry.getKey()+"\n");
				for(SubMessage msg: (ArrayList<SubMessage>)valueObj){
					sb.append(printMessage(2, msg));
					sb.append("----\n");
				}
			}
			else{
				sb.append(indentStr+"Key:"+entry.getKey()+" Value:"+valueObj+"\n");
			}
		}
		return sb.toString();
	}
	
	public static int getBitMapValue(ArrayList<String> values, StringTokenizer userInputTokens) throws IOException{
		int bitMapValue=0;
		if(userInputTokens==null){
			for(int i=0; i<values.size(); i++){
				String userInput=getUserInput("Set \""+values.get(i)+"\" Default[N] or Y:");
				if(userInput.equalsIgnoreCase("Y")){
					bitMapValue+=(int)Math.pow(2, i);
				}else if (!(userInput.equalsIgnoreCase("N")||userInput.equals(""))){
					pw.println("Unknown confirm input:"+userInput+" Set to N for "+values.get(i));
				}
			}
		}else{
			for(int i=0; i<values.size(); i++){
				String userInput=userInputTokens.nextToken();
				if(userInput.equalsIgnoreCase("Y")){
					bitMapValue+=(int)Math.pow(2, i);
				}else if (!(userInput.equalsIgnoreCase("N")||userInput.equals(""))){
					log.error("Unknown confirm input:"+userInput+" Set to N for "+values.get(i));
				}
			}
		}
		
		return bitMapValue;
	}
	
	public static File getFileTransfer()throws Exception{
		return getFileTransfer(C8Message.MAX_PAYLOAD,null );
	}
	
	public static File getFileTransferByPath(String filePath)throws Exception{
		return getFileTransfer(C8Message.MAX_PAYLOAD,filePath );
	}
	public static File getFileTransfer(int maxLen, String filePath) throws Exception{
		String userInput;
		if(filePath==null){
			userInput=getUserInput("Please input the file path for the file transfer");
		}else{
			userInput=filePath;
		}
		File file=new File(userInput);
		if(maxLen>0&&file.length()>maxLen){
			throw new Exception("Invalid file. File size:"+file.length()+" exceeds the max length:"+maxLen);
		}
		return file;
	}
	public static String getUserInput(String promptString) throws IOException{
		pw.println(promptString);
		String userInput=br.readLine();
		userInput=userInput.trim();
		return userInput;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception{
		C2Message sMsg=new C2Message();
		sMsg.getParams();
		getUserInput(sMsg);
		boolean confirmResult=confirmSendMessage(sMsg);
		if(confirmResult){
			pw.println("SENT");
		}else{
			pw.println("CANCELLED");
		}
	}
	
	public static Calendar getCalendarByMilliSeconds(long timeMilli, String timeZoneCode){
		Calendar returnCalendar = Calendar.getInstance();
		returnCalendar.clear();
		returnCalendar.setTimeInMillis(timeMilli);
		returnCalendar.setTimeZone(TimeZone.getTimeZone(timeZoneCode));
		//returnCalendar.set(Calendar.DST_OFFSET, 0);
		returnCalendar.set(Calendar.ZONE_OFFSET, returnCalendar.getTimeZone().getOffset(returnCalendar.getTimeInMillis()));
		return returnCalendar;
	}
	
	public static Calendar getCalendarByDate(Date date, String timeZoneCode){
		return getCalendarByMilliSeconds(date.getTime(), timeZoneCode);
	}
	
	public static Date getDateByUserInput(String promptString) throws IOException, ParseException{
		String time=CommandLineUtil.getUserInput(promptString);
		return dateFormat.parse(time);
	}

}
