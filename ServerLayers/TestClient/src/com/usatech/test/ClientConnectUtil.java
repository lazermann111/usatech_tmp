package com.usatech.test;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import simple.io.ByteArrayUtils;
import simple.io.Log;

import com.usatech.networklayer.NetworkLayerException;
import com.usatech.networklayer.ReRixMessage;
import com.usatech.networklayer.net.TransmissionProtocol;
import com.usatech.networklayer.net.TransmissionProtocolV7;
/**
 * Class to start stop connection and send message to Netlayer
 * @author yhe
 *
 */
public class ClientConnectUtil {
	private static final Log log = Log.getLog();
	protected String host = "localhost";
	protected int port = 443;
	protected Socket socket;
	protected DataInputStream input;
	protected DataOutputStream output;
	TransmissionProtocol protocol;
	int protocolVersion;
	protected static final byte     PACKET_DELIMITER            = 0x0A;
	public ClientConnectUtil(){
		protocolVersion=7;
		protocol=new TransmissionProtocolV7(30000, 30000);
	}
	public ClientConnectUtil(String host, int port, int protocolVersion){
		this.host=host;
		this.port=port;
		this.protocolVersion=protocolVersion;
	}
	public void setProtocol(int protocolVersion)throws ClassNotFoundException,NoSuchMethodException,IllegalAccessException,InvocationTargetException,NetworkLayerException{
		int readTimeout = 30000;
		int writeTimeout = 30000;
		Class<?>[] parameterTypes = { Integer.TYPE, Integer.TYPE };
		Object[] constructorArgs = { new Integer(readTimeout), new Integer(writeTimeout) };
		String className = "com.usatech.networklayer.net.TransmissionProtocolV" + protocolVersion;
		try	{
			Class<?> protocolClass = Class.forName(className);
			Constructor<?> constructor = protocolClass.getConstructor(parameterTypes);
			protocol = (TransmissionProtocol) constructor.newInstance(constructorArgs);
		} catch(InstantiationException e) {
			throw new NetworkLayerException("Failed to create TransmissionProtocol " + className, e);
		}
	}
	
	public void setProtocol()throws ClassNotFoundException,NoSuchMethodException,IllegalAccessException,InvocationTargetException,NetworkLayerException{
		setProtocol(protocolVersion);
	}
	
	public void startCommunication() throws ClassNotFoundException, NetworkLayerException, UnknownHostException, IOException, SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		if(protocol==null){
			setProtocol();
		}
		socket = new Socket(host, port);
		socket.setSoTimeout(10000);

		input = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
		output = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
	}
	public void stopCommunication() throws IOException {
		input.close();
		output.close();
		socket.close();
	}
	
	public ReRixMessage sendMessage(ReRixMessage msg, String encryptKey) throws Exception{
		byte[] key = ByteArrayUtils.fromHex(encryptKey);
		protocol.transmitToServer(msg, output,key);
		output.flush();
		try{
			int protocolVersionResponse=(int)input.readByte();
			log.info("Response Protocol version:"+protocolVersionResponse);
			if(protocolVersion!=protocolVersionResponse){
				setProtocol(protocolVersionResponse);
			}
			return protocol.receiveFromServer(input, msg.getEVNumber(), key);
		}catch(SocketTimeoutException e){
			log.debug("Server returns no response and connection timed out.");
			return null;
		}
	}
	
	public ReRixMessage sendMessageV4(ReRixMessage msg, String encryptKey ) throws Exception{
		byte[] key = ByteArrayUtils.fromHex(encryptKey);
		output.write(msg.getData());
		output.write(PACKET_DELIMITER);
		output.flush();
		try{
			setProtocol(4);
			return protocol.receiveFromServer(input, msg.getEVNumber(), key);
		}catch(SocketTimeoutException e){
			log.debug("Server returns no response and connection timed out.");
			return null;
		}
	}
	
	public void sendMessageWithNoRead(ReRixMessage msg, String encryptKey) throws Exception{
		byte[] key = ByteArrayUtils.fromHex(encryptKey);
		protocol.transmitToServer(msg, output,key);
		output.flush();
	}
	
	public void setHost(String host) {
		this.host = host;
	}
	public void setPort(int port) {
		this.port = port;
	}
}
