package com.usatech.test;

import java.nio.ByteBuffer;

import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
/**
 * A0 --> A1h response
 * @author yhe
 *
 */
public class A0Message extends LegacyMessage {
	private static final Log log = Log.getLog();
	protected long transactionId=System.currentTimeMillis()/1000;
	protected int cardType;
	protected long amount;//US dollar in pennies
	protected int pinLength;
	protected String pin;
	protected String creditCardMagstripe;

	public A0Message(){
		super();
		dataType=0xA0;
		cardType='C';
		amount=150;
		pinLength=6;
		pin="abcdef";
		creditCardMagstripe="0123456789123456";
	}

	@Override
	public ReRixMessage createMessage() throws Exception {
		pinLength=pin.length();
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		ProcessingUtils.writeLongInt(bb,transactionId);
		ProcessingUtils.writeByteInt(bb,cardType);
		ProcessingUtils.writeLongInt(bb,amount);
		ProcessingUtils.writeByteInt(bb,pinLength);
		ProcessingUtils.writeShortString(bb,pin, ProcessingConstants.US_ASCII_CHARSET);
		ProcessingUtils.writeString(bb,creditCardMagstripe, ProcessingConstants.US_ASCII_CHARSET);
		return writeMessageEnd(bb);
	}
	
	@Override
	public void verifyResponse(){
		long responseTransactionId=ConvertUtils.getLongSafely(responseMap.get("transactionId"), -1);
		if(transactionId!=responseTransactionId){
			log.error("Response transactionId:"+responseTransactionId+" does not match transactionId:"+transactionId);
		}
	}

	@Override
	public String readResponseString(ReRixMessage msg)throws InvalidByteValueException{
		return readResponseStringA1(msg);
	}
	public int getCardType() {
		return cardType;
	}

	public void setCardType(int cardType) {
		this.cardType = cardType;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getCreditCardMagstripe() {
		return creditCardMagstripe;
	}

	public void setCreditCardMagstripe(String creditCardMagstripe) {
		this.creditCardMagstripe = creditCardMagstripe;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

}
