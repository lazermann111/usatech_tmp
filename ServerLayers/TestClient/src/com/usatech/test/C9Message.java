package com.usatech.test;

import java.nio.ByteBuffer;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
/**
 * File Transfer C9-->CB response
 * @author yhe
 *
 */
public class C9Message extends V4Message {
	protected int blockNum;
	protected byte[] blockPayload;

	public C9Message() {
		super();
		dataType=0xC9;
		blockNum=0;
		blockPayload=null;
	}

	@Override
	public ReRixMessage createMessage() {
		int payLoadLen=blockPayload==null?0:blockPayload.length;
		ByteBuffer bb=ByteBuffer.allocate(10+payLoadLen);
		writeMessageHeader(bb);
		ProcessingUtils.writeShortInt(bb,blockNum);
		ProcessingUtils.writeShortInt(bb, blockPayload.length);
		bb.put(blockPayload);
		return writeMessageEnd(bb);
	}
	public int getBlockNum() {
		return blockNum;
	}

	public void setBlockNum(int blockNum) {
		this.blockNum = blockNum;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception{
		ClientConnectUtil conn=new ClientConnectUtil();
		conn.setProtocol(7);
		conn.startCommunication();
		C8Message sMsg=new C8Message();
		sMsg.getParams();
		ReRixMessage sentMsg=sMsg.createMessage();
		ReRixMessage responseMsg=conn.sendMessage(sentMsg, sMsg.encryptKey);
		sMsg.readResponse(responseMsg);
		C9Message sMsg2=new C9Message();
		sMsg2.getParams();
		sentMsg=sMsg2.createMessage();
		responseMsg=conn.sendMessage(sentMsg, sMsg2.encryptKey);
		sMsg2.readResponse(responseMsg);
		conn.stopCommunication();

	}

}
