package com.usatech.test;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class KioskItem extends SubMessage{
	protected int itemId;
	protected String amount;
	protected String quantity;
	protected String description;
	protected boolean isToPromptUser;
	protected StringTokenizer userInputTokens;
	public KioskItem(StringTokenizer userInputTokens){
		super(userInputTokens);
		itemId=302;
		amount="150";
		quantity="1";
		description="Tier 2";
	}
	public void writeMessage(ByteBuffer bb){
		//not implemented
	}
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public static String createKioskItems(String formatCode, ArrayList<KioskItem> kioskItem){
		StringBuilder sb=new StringBuilder();
		sb.append(formatCode);
		for(KioskItem kItem: kioskItem){
			sb.append("|").append(kItem.getItemId());
			sb.append("|").append(kItem.getAmount());
			sb.append("|").append(kItem.getQuantity());
			sb.append("|").append(kItem.getDescription());
		}
		return sb.toString();
	}
}