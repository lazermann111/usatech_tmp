package com.usatech.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import simple.io.ByteArrayUtils;
import simple.io.Log;
import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;

import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.TLVTag;
import com.usatech.layers.common.util.Vend3ARParsing;
import com.usatech.layers.common.util.Vend3StatusCode;
import com.usatech.layers.common.util.VendXEMVData;
import com.usatech.test.EMVTestMain.Crc16;

public 	class Vend3 {
	private static final Log log = Log.getLog();

	public static final VivotechTrackDataSource DEFAULT_VIVOTECH_TRACK_DATA_SOURCE = VivotechTrackDataSource.VIVOTECH_CONTACTLESS_SOURCE;
	
	public static final byte CA_PUBLIC_KEY_MGMT_FLAG_CONTACTLESS = 0x01;
	public static final byte CA_PUBLIC_KEY_MGMT_FLAG_CONTACT = 0x02;

	private List<CardReaderDataBlob>responseList = new ArrayList<>();
    private List<VendXBlobHandler>blobHandlerList = new ArrayList<VendXBlobHandler>();
    
	private static byte [] NULL_TERM = new byte [] { 0 };

	static int counter = 0;
    static int bufferCounter = 0;
    
	private byte [] DISPLAY_IMAGE_CMD = new byte [] { (byte)0x83, 0x0e };
    
//	public static String DEST_FILENAME = "usaTechLogo.png";
//	public static String SOURCE_FILENAME = "/Users/jscarpaci/Documents/USATech/emv/vendIII/images/usaTechLogo.png";
	public static String DEST_FILENAME = "usaTechLogo2.png";
	public static String SOURCE_FILENAME = "/Users/jscarpaci/Documents/USATech/emv/vendIII/images/usaTechLogo2.png";
	
    public class TransferFile {
    	private String destFilename;
    	private String sourceFilename;
    	private File sourceFile;
    	private long fileIdx;
    	private long sourceFilelength;
    	
    	private byte [] fileContent;

//    	private static final long MAX_CONTENT_LENGTH = 4000;
    	private static final long MAX_CONTENT_LENGTH = 10;
    	private byte [] TRANSFER_FILE_CMD = new byte [] { (byte)0x83, 0x24 };
    	
    	public TransferFile(String destFilename, String sourceFilename) throws IOException {
    		this.destFilename = destFilename;
    		this.sourceFilename = sourceFilename;
    		this.sourceFile = new File(getSourceFilename());
    		if (!getSourceFile().exists()) {
    			throw new FileNotFoundException(getSourceFilename());
    		}
    		this.fileIdx = 0;
    		this.sourceFilelength = getSourceFile().length();
    		
			Path path = FileSystems.getDefault().getPath(getSourceFile().getAbsolutePath());
			this.fileContent = Files.readAllBytes(path);
    		
    	}

    	public byte [] destinationFilename() {
//    		byte [] asBytes = StringUtils.toHex(getDestFilename(), 0, getDestFilename().length());
    		byte [] asBytes = getDestFilename().getBytes();
    		return nullTerminate(asBytes);
    	}
    	
    	public byte [] totalFileDataBytes() {
    		String filelength = new Long(getSourceFilelength()).toString();
//    		byte [] asBytes = StringUtils.toHexByte(filelength, 0, filelength.length());
    		byte [] asBytes = filelength.getBytes();
    		return nullTerminate(asBytes);
    	}
    	
    	public byte [] flagNeitherFirstNorLast() {
    		return new byte [] { 0x30, 0x00 };
    	}
    	public byte [] flagLast() {
    		return new byte [] { 0x31, 0x00 };
    	}
    	public byte [] flagFirst() {
    		return new byte [] { 0x32, 0x00 };
    	}
    	public byte [] flagFirstAndLast() {
    		return new byte [] { 0x33, 0x00 };
    	}
    	
//    	private byte [] nullTerminate(byte [] asBytes) {
//    		byte [] result = new byte[asBytes.length + 1];
//    		System.arraycopy(asBytes, 0, result, 0, asBytes.length);
//    		System.arraycopy(NULL_TERM, 0, result, asBytes.length, NULL_TERM.length);
//    		return result;
//    	}
    	
    	public boolean sendNext() {
    		if (getFileIdx() < getSourceFilelength()) {
	    		byte [] filename = destinationFilename();
	    		byte [] totalFileBytes = totalFileDataBytes();
	    		byte [] flag = flagToSend();
	    		byte [] fileSection = nextFileSection();
	    		int lengthLessCrc = HEADER_LENGTH + filename.length + totalFileBytes.length + flag.length + fileSection.length;
	    		byte [] cmd = new byte [lengthLessCrc];
	    		System.arraycopy(PREAMBLE, 0, cmd, 0, PREAMBLE.length);
	    		System.arraycopy(TRANSFER_FILE_CMD, 0, cmd, PREAMBLE.length, TRANSFER_FILE_CMD.length);
	    		
	    		// wants payload length
	    		int payloadLength = filename.length + totalFileBytes.length + flag.length + fileSection.length;
	    		byte lsbLength = (byte)(payloadLength & 0xff);
	    		byte msbLength = (byte)((payloadLength >> 8) & 0xff);
	    		int lengthOffset = PREAMBLE.length + TRANSFER_FILE_CMD.length; 
	    		cmd[lengthOffset] = msbLength;
	    		cmd[lengthOffset + 1] = lsbLength;
	    		
	    		int dataOffset = lengthOffset + 2;
	    		System.arraycopy(filename, 0, cmd, dataOffset, filename.length);
	    		System.arraycopy(totalFileBytes, 0, cmd, dataOffset + filename.length, totalFileBytes.length);
	    		System.arraycopy(flag, 0, cmd, dataOffset + filename.length + totalFileBytes.length, flag.length);
	    		System.arraycopy(fileSection, 0, cmd, dataOffset + filename.length + totalFileBytes.length + flag.length, fileSection.length);
	    		
	    		// ship it
	    		sendCmdAndCalcCrc(cmd);
//	    		log.info("would send: " + StringUtils.toHex(cmd));
	    		
	    		// adjust fileIdx
	    		long idx = getFileIdx() + fileSection.length;
	    		setFileIdx(idx);
    		}
    		boolean isMore = (getFileIdx() < getSourceFilelength());
    		return isMore;
    	}
    	
    	private byte [] flagToSend() {
    		byte [] result = flagNeitherFirstNorLast();
    		if (getSourceFilelength() < MAX_CONTENT_LENGTH) {
    			result = flagFirstAndLast();
    		} else if (getFileIdx() == 0) {
    			result = flagFirst();
    		} else if ((getSourceFilelength() - getFileIdx()) <= MAX_CONTENT_LENGTH) {
    			result = flagLast();
    		}
    		return result;
    	}
    	
		private byte[] nextFileSection() {
			long length = 0;
			long difference = getSourceFilelength() - getFileIdx(); 
			if (difference >= MAX_CONTENT_LENGTH) {
				length = MAX_CONTENT_LENGTH;
			} else {
				length = difference;
			}
			byte [] result = new byte [(int)length];
			System.arraycopy(getFileContent(), (int)getFileIdx(), result, 0, (int)length);
			return result;
		}

		public String getDestFilename() {
			return destFilename;
		}

		public String getSourceFilename() {
			return sourceFilename;
		}
		
		private File getSourceFile() {
			return sourceFile;
		}

		public long getFileIdx() {
			return fileIdx;
		}

		public void setFileIdx(long fileIdx) {
			this.fileIdx = fileIdx;
		}

		public long getSourceFilelength() {
			return sourceFilelength;
		}

		public byte[] getFileContent() {
			return fileContent;
		}
    }
    
    public interface VendXBlobHandler {
    	public void message(IdtechVendXDataBlob responseMsg);
    }
    
    public enum Command {
    	SET_MISC((byte)0x01),
    	ACTIVATE_TRANSACTION((byte)0x02),
    	GET_TRANSACTION_RESULT((byte)0x03),
    	SET_CONFIGURATION((byte)0x04),
    	CANCEL_TRANSACTION((byte)0x05),
    	GET_MODULE_VERSION((byte)0x09),
    	CONTROL_LED((byte)0x0a),
    	CONTROL_BUZZER((byte)0x0b),
    	GET_SET_SERIAL_NUMBER((byte)0x12),
    	FLUSH_TRACK_DATA_GR((byte)0x17),
    	PING((byte)0x18),
    	CONTROL_ANTENNA((byte)0x28),
    	GET_FIRMWARE_VERSION((byte)0x29),
    	MIFARE_VARIOUS((byte)0x2c),
    	SET_BAUD_RATE((byte)0x30),
    	DEVICE_ADMIN((byte)0x77),
    	MASTER_KEY((byte)0x80),
    	GET_DUKPT_PIN((byte)0x81),
    	GET_MEMORTY_STATUS((byte)0x82),
    	DISPLAY((byte)0x83),
    	EMV_TRANSACTION((byte)0x84),
    	PUBLIC_KEY((byte)0xd0),
    	LED_CONFIG((byte)0xf0),
    	;
    	
    	private byte command;
    	Command(byte command) {
    		this.command = command;
    	}
    	public byte getValue() {
    		return command;
    	}
    	
        protected final static EnumByteValueLookup<Command> lookup = new EnumByteValueLookup<Command>(Command.class);
        public static Command getByValue(byte value) throws InvalidByteValueException {
        	return lookup.getByValue(value);
        }
    }
    
    
	public static boolean isGoodEmvData(byte cmdByte, byte statusCode) {
		boolean result = (cmdByte == IdtechVendXDataBlob.CMD_ACTIVATE_TRANSACTION) && 
						(statusCode == Vend3StatusCode.OK || statusCode == Vend3StatusCode.REQUEST_ONLINE_AUTH || statusCode == Vend3StatusCode.REQUEST_ONLINE_AUTH_CONTACT_EMV_ONLY);
		return result;
	}
	
	public static boolean isFailedWithAuthRequest(byte statusCode, byte errorCode) {
		boolean result = (statusCode == Vend3StatusCode.FAILED && errorCode == 0x00);
		return result;
	}
	
	public static final int DATA_START_OFFSET = 14;
	public static final int CRC_LENGTH = 2;

	private static final int HEADER_LENGTH = DATA_START_OFFSET;         // PREAMBLE (10), COMMAND (1), SUB COMMAND (1), DATA LENGTH MSB (1), DATA LENGTH LSB (1) 
	private byte PREAMBLE [] = {
		0x56, 0x69, 0x56, 0x4f, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00
	};

	// 56 69 56 4F 74 65 63 68 32 00 18 01 00 00 B3 CD
	private byte PING_CMD[] = {0x56, 0x69, 0x56, 0x4f, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x18, 0x01, 0x00, 0x00, (byte)0xb3, (byte)0xcd};
	@SuppressWarnings("unused")
	private byte PING_CMD_NO_CRC[] = {0x56, 0x69, 0x56, 0x4f, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x18, 0x01, 0x00, 0x00};
	
	// 56 69 56 4F 74 65 63 68 32 00 04 00 00 0B 9F 66 04 A0 00 40 00 FF FC 01 00 0F 78
	private byte SET_EMV_MASTERCARD_AND_VISA [] = {0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x04, 0x00, 
													0x00, 0x0B, (byte)0x9F, 0x66, 0x04, (byte)0xA0, 0x00, 0x40, 0x00, (byte)0xFF, (byte)0xFC, 0x01, 0x00, 0x0F, 0x78};
	
	private byte SET_EMV_AMEX_NO_CRC [] = { 0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00,  0x04, 0x00,
			0x00, 0x04, (byte)0xDF, 0x51, 0x01, (byte)0xC0
	};
	
	private byte LIST_AMEX_KEYS_CONTACTLESS [] = { 0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, (byte)0xD0, 0x27, 
			0x00, 0x06, 0x01, (byte)0xA0, 0x00, 0x00, 0x00, 0x25, (byte)0xA8, (byte)0xAB
	};
	private byte LIST_AMEX_KEYS_CONTACT_NO_CRC [] = { 0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, (byte)0xD0, 0x27, 
			0x00, 0x06, 0x02, (byte)0xA0, 0x00, 0x00, 0x00, 0x25
	};
	// 56 69 56 4F 74 65 63 68 32 00 D0 27 00 06 01 A0 00 00 01 52 E9 96
	private byte LIST_DISCOVER_KEYS_NO_CRC [] = { 0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, (byte)0xD0, 0x27, 
			0x00, 0x06, 0x00, (byte)0xA0, 0x00, 0x00, 0x01, 0x52
	};
	private byte LIST_DISCOVER_CONTACTLESS_KEYS_NO_CRC [] = { 0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, (byte)0xD0, 0x27, 
			0x00, 0x06, 0x01, (byte)0xA0, 0x00, 0x00, 0x01, 0x52
	};
	private byte LIST_DISCOVER_CONTACT_KEYS_NO_CRC [] = { 0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, (byte)0xD0, 0x27, 
			0x00, 0x06, 0x02, (byte)0xA0, 0x00, 0x00, 0x01, 0x52
	};
	
	
	// same as 7) Set Configurable Discover Contact AID", () -> reader.sendSetConfigurableDiscoverContactAID()
	// Create user AID (Discover)
	// 56 69 56 4F 74 65 63 68 32 00 04 02 00 1C FF E4 01 08 9F 06 05 A0 00 00 01 52 FF E2 01 0D FF E1 01 01 FF E5 01 10 FF E7 01 02 36 DA
//	private byte CREATE_DISCOVER_CONTACT_AID [] = { 0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x04, 0x02, 
//		0x00, 0x1C, (byte)0xFF, (byte)0xE4, 0x01, 0x08, (byte)0x9F, 0x06, 0x05, (byte)0xA0, 0x00, 0x00, 0x01, 0x52, (byte)0xFF, (byte)0xE2, 
//		0x01, 0x0D, (byte)0xFF, (byte)0xE1, 0x01, 0x01, (byte)0xFF, (byte)0xE5, 0x01, 0x10, (byte)0xFF, (byte)0xE7, 0x01, 0x02, 0x36, (byte)0xDA
//	};
	
//	private byte SET_CONFIGURABLE_AID_AMEX_CONTACT_NO_CRC [] = { 0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x04, 0x02,
//		0x00, 0x11,	// length
//		(byte)0xff, (byte)0xe4, 0x01, 0x08,		// Group Number TLV.  Must be first in TLV list
//		(byte)0x9f, 0x06, 0x06, (byte)0xa0, 0x00, 0x00, 0x00, 0x25, 0x01,		// Application Identifier
//		(byte)0xff, (byte)0xe7, 0x01, 0x02					// Interface Support tlv set to Contact
//	};
	private byte SET_CONFIGURABLE_AID_AMEX_CONTACT [] = { 0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x04, 0x02, 
		0x00, 0x1D, (byte)0xFF, (byte)0xE4, 0x01, 0x08, (byte)0x9F, 0x06, 0x06, (byte)0xA0, 0x00, 0x00, 0x00, 0x25, 0x01, (byte)0xFF, 
		(byte)0xE2, 0x01, 0x02, (byte)0xFF, (byte)0xE7, 0x01, 0x02, (byte)0xFF, (byte)0xE1, 0x01, 0x01, (byte)0xFF, (byte)0xE5, 0x01, 0x10, (byte)0xDD, (byte)0xB1
	};
	private byte SET_CONFIGURABLE_AID_DISCOVER_CONTACT [] = { 0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x04, 0x02, 
		0x00, 0x1C,  (byte)0xFF, (byte)0xE4, 0x01, 0x08,  (byte)0x9F, 0x06, 0x05, (byte)0xA0, 0x00, 0x00, 0x01, 0x52,  (byte)0xFF,  
		(byte)0xE2, 0x01, 0x0D, (byte)0xFF, (byte)0xE1, 0x01, 0x01, (byte)0xFF, (byte)0xE5, 0x01, 0x10,  (byte)0xFF,  (byte)0xE7, 
		0x01, 0x02, 0x36, (byte)0xDA
	};
	private byte SET_CONFIGURABLE_AID_DISCOVER_CONTACTLESS_NO_CRC [] = { 0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x04, 0x02, 
			0x00, 0x1C,  (byte)0xFF,  (byte)0xE4, 0x01, 0x08,  (byte)0x9F, 0x06, 0x05, (byte)0xA0, 0x00, 0x00, 0x01, 0x52, (byte)0xFF,  
			(byte)0xE2, 0x01, 0x0D,  (byte)0xFF, (byte)0xE1, 0x01, 0x01, (byte)0xFF, (byte)0xE5, 0x01, 0x10, (byte)0xFF, (byte)0xE7, 
			0x01, 0x01
		};
//	private byte ACTIVATE_TXN_DISCOVER [] = { 0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x02, 0x01, 
//		0x00, 0x24, 0x0F, (byte)0x9F, 0x02, 0x06, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, (byte)0x9C, 0x01, 0x00, (byte)0x9A, 0x03, 0x09, 0x02, 0x11, (byte)0x9F, 
//		0x21, 0x03, 0x10, 0x20, 0x30, 0x5F, 0x57, 0x01, 0x00, (byte)0xDF, 0x22, 0x01, 0x00, (byte)0xDF, 0x11, 0x01, 0x01, (byte)0x8A, 0x70
//	};
	
	private byte SET_CA_PUBLIC_KEY_AMEX_CONTACTLESS [] = {
		0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, (byte)0xD0, 0x23,
		0x00, 0x00,	// length - MUST FILL IN LATER
		0x01,	// flags set to apply to contactless emv keys, but only know since the card reader was looking for C8 per Pauline's direction
		(byte)0xA0, 0x00, 0x00, 0x00, 0x25,	// amex RID
		(byte)0xC8,		// keyIndex per Pauline's direction
		0x01,		// Certification Authority Hash Algorithm indicator - SHA-1 - only algorithm supported but should pass down to devices for future proofing
		0x01,		// Certification Authority Public Key Algorithm Indicator - RSA - only support algorithm
		0x33, (byte)0xBD, 0x7A, 0x05, (byte)0x9F, (byte)0xAB, 0x09, 0x49, 0x39, (byte)0xB9, 0x0A, (byte)0x8F, 0x35, (byte)0x84, 0x5C, (byte)0x9D, (byte)0xC7, 0x79, (byte)0xBD, 0x50, // CA Public Key Checksum
		0x00, 0x00, 0x00, 0x03,		// 	CA Public Key Exponent
		0x00, 0x00 	// CA Public Key Modulus length - MUST FIGURE OUT or Calculate
	};
	private byte AMEX_CONTACTLESS_CA_PUBLIC_KEY_MODULUS [] = {
		(byte)0xBF, 0x0C, (byte)0xFC, (byte)0xED, 0x70, (byte)0x8F, (byte)0xB6, (byte)0xB0, 0x48, (byte)0xE3, 0x01, 0x43, 0x36, (byte)0xEA, 0x24, 
		(byte)0xAA, 0x00, 0x7D, 0x79, 0x67, (byte)0xB8, (byte)0xAA, 0x4E, 0x61, 0x3D, 0x26, (byte)0xD0, 0x15, (byte)0xC4, (byte)0xFE, 0x78, 0x05, 
		(byte)0xD9, (byte)0xDB, 0x13, 0x1C, (byte)0xED, 0x0D, 0x2A, (byte)0x8E, (byte)0xD5, 0x04, (byte)0xC3, (byte)0xB5, (byte)0xCC, (byte)0xD4, 
		(byte)0x8C, 0x33, 0x19, (byte)0x9E, 0x5A, 0x5B, (byte)0xF6, 0x44, (byte)0xDA, 0x04, 0x3B, 0x54, (byte)0xDB, (byte)0xF6, 0x02, 0x76, (byte)0xF0, 
		0x5B, 0x17, 0x50, (byte)0xFA, (byte)0xB3, (byte)0x90, (byte)0x98, (byte)0xC7, 0x51, 0x1D, 0x04, (byte)0xBA, (byte)0xBC, 0x64, (byte)0x94, (byte)0x82, 
		(byte)0xDD, (byte)0xCF, 0x7C, (byte)0xC4, 0x2C, (byte)0x8C, 0x43, 0x5B, (byte)0xAB, (byte)0x8D, (byte)0xD0, (byte)0xEB, 0x1A, 0x62, 0x0C, 0x31, 0x11, 
		0x1D, 0x1A, (byte)0xAA, (byte)0xF9, (byte)0xAF, 0x65, 0x71, (byte)0xEE, (byte)0xBD, 0x4C, (byte)0xF5, (byte)0xA0, (byte)0x84, (byte)0x96, (byte)0xD5, 
		0x7E, 0x7A, (byte)0xBD, (byte)0xBB, 0x51, (byte)0x80, (byte)0xE0, (byte)0xA4, 0x2D, (byte)0xA8, 0x69, (byte)0xAB, (byte)0x95, (byte)0xFB, 0x62, 0x0E, 
		(byte)0xFF, 0x26, 0x41, (byte)0xC3, 0x70, 0x2A, (byte)0xF3, (byte)0xBE, 0x0B, 0x0C, 0x13, (byte)0x8E, (byte)0xAE, (byte)0xF2, 0x02, (byte)0xE2, 0x1D
	};
	
	private byte SET_CA_PUBLIC_KEY_AMEX_CONTACT [] = {
		0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, (byte)0xD0, 0x23,
		0x00, 0x00,	// length - MUST FILL IN LATER
		0x02,	// flags set to apply to contact emv keys (0x02), but only know since the card reader was looking for C8 per Pauline's direction for contactless
		(byte)0xA0, 0x00, 0x00, 0x00, 0x25,	// amex RID
		(byte)0xC9,		// keyIndex per examining emv parameter download file.  There are a total of 6 Amex RIDs with a keyIndex of 97, 98, 99, C8, C9, CA
		0x01,		// Certification Authority Hash Algorithm indicator - SHA-1 - only algorithm supported but should pass down to devices for future proofing
		0x01,		// Certification Authority Public Key Algorithm Indicator - RSA - only support algorithm
		(byte)0x8E, (byte)0x8D, (byte)0xFF, 0x44, 0x3D, 0x78, (byte)0xCD, (byte)0x91, (byte)0xDE, (byte)0x88, (byte)0x82, 0x1D, 0x70, (byte)0xC9, (byte)0x8F, 0x06, 0x38, (byte)0xE5, 0x1E, 0x49, // CA Public Key Checksum
		0x00, 0x00, 0x00, 0x03,		// 	CA Public Key Exponent
		0x00, 0x00 	// CA Public Key Modulus length - MUST Calculate
	};
	private byte AMEX_CONTACT_CA_PUBLIC_KEY_MODULUS [] = {
		(byte)0xB3, 0x62, (byte)0xDB, 0x57, 0x33, (byte)0xC1, 0x5B, (byte)0x87, (byte)0x97, (byte)0xB8, (byte)0xEC, (byte)0xEE, 0x55, (byte)0xCB, 0x1A, 0x37, 0x1F, 0x76, 0x0E, 
		0x0B, (byte)0xED, (byte)0xD3, 0x71, 0x5B, (byte)0xB2, 0x70, 0x42, 0x4F, (byte)0xD4, (byte)0xEA, 0x26, 0x06, 0x2C, 0x38, (byte)0xC3, (byte)0xF4, (byte)0xAA, (byte)0xA3, 
		0x73, 0x2A, (byte)0x83, (byte)0xD3, 0x6E, (byte)0xA8, (byte)0xE9, 0x60, 0x2F, 0x66, (byte)0x83, (byte)0xEE, (byte)0xCC, 0x6B, (byte)0xAF, (byte)0xF6, 0x3D, (byte)0xD2, 
		(byte)0xD4, (byte)0x90, 0x14, (byte)0xBD, (byte)0xE4, (byte)0xD6, (byte)0xD6, 0x03, (byte)0xCD, 0x74, 0x42, 0x06, (byte)0xB0, 0x5B, 0x4B, (byte)0xAD, 0x0C, 0x64, (byte)0xC6, 
		0x3A, (byte)0xB3, (byte)0x97, 0x6B, 0x5C, (byte)0x8C, (byte)0xAA, (byte)0xF8, 0x53, (byte)0x95, 0x49, (byte)0xF5, (byte)0x92, 0x1C, 0x0B, 0x70, 0x0D, 0x5B, 0x0F, (byte)0x83, 
		(byte)0xC4, (byte)0xE7, (byte)0xE9, 0x46, 0x06, (byte)0x8B, (byte)0xAA, (byte)0xAB, 0x54, 0x63, 0x54, 0x4D, (byte)0xB1, (byte)0x8C, 0x63, (byte)0x80, 0x11, 0x18, (byte)0xF2, 
		0x18, 0x2E, (byte)0xFC, (byte)0xC8, (byte)0xA1, (byte)0xE8, 0x5E, 0x53, (byte)0xC2, (byte)0xA7, (byte)0xAE, (byte)0x83, (byte)0x9A, 0x5C, 0x6A, 0x3C, (byte)0xAB, (byte)0xE7, 
		0x37, 0x62, (byte)0xB7, 0x0D, 0x17, 0x0A, (byte)0xB6, 0x4A, (byte)0xFC, 0x6C, (byte)0xA4, (byte)0x82, (byte)0x94, 0x49, 0x02, 0x61, 0x1F, (byte)0xB0, 0x06, 0x1E, 0x09, 
		(byte)0xA6, 0x7A, (byte)0xCB, 0x77, (byte)0xE4, (byte)0x93, (byte)0xD9, (byte)0x98, (byte)0xA0, (byte)0xCC, (byte)0xF9, 0x3D, (byte)0x81, (byte)0xA4, (byte)0xF6, (byte)0xC0, 
		(byte)0xDC, 0x6B, 0x7D, (byte)0xF2, 0x2E, 0x62, (byte)0xDB		
	};
	private byte SET_CA_PUBLIC_KEY_HEADER [] = {
		0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, (byte)0xD0, 0x23,
		0x00, 0x00	// length - MUST FILL IN LATER
	};
		
	private byte LIST_ALL_CA_PUBLIC_KEYS_NO_CRC [] = {
			0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, (byte)0xD0, 0x26, 
			0x00, 0x01, 0x07
	};
	
	// Create new group 09
	// 56 69 56 4F 74 65 63 68 32 00 04 03 00 0A FF E4 01 09 9F 33 03 00 08 E8 88 7D
	private byte CREATE_NEW_GROUP_09 [] = { 0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x04, 0x03, 
		0x00, 0x0A, (byte)0xFF, (byte)0xE4, 0x01, 0x09, (byte)0x9F, 0x33, 0x03, 0x00, 0x08, (byte)0xE8, (byte)0x88, 0x7D
	};
	
	// 56 69 56 4F 74 65 63 68 32 00 02 01 00 01 20 46 EE
	private byte ACTIVATE_TRANSACTION [] = {0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x02, 0x01, 0x00, 0x01, 0x20, 0x46, (byte)0xEE};
	
	// 56 69 56 4F 74 65 63 68 32 00 04 00 00 04 FF F7 01 01 98 3E
	private byte BURST_MODE_ENABLE [] = {0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x04, 0x00, 0x00, 0x04, (byte)0xFF, (byte)0xF7, 0x01, 0x01, (byte)0x98, 0x3E};
	
	// 56 69 56 4F 74 65 63 68 32 00 04 00 00 04 FF F7 01 00 B9 2E
	private byte BURST_MODE_DISABLE [] = {0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x04, 0x00, 0x00, 0x04, (byte)0xFF, (byte)0xF7, 0x01, 0x00, (byte)0xB9, 0x2E};
	
	// 56 69 56 4F 74 65 63 68 32 00 01 01 00 01 00 F6 24
	private byte SET_POLL_MODE_AUTOPOLL [] = {0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x01, 0x01, 0x00, 0x01, 0x00, (byte)0xF6, 0x24};
	
	// 56 69 56 4F 74 65 63 68 32 00 01 01 00 01 01 D7 34
	private byte SET_POLL_MODE_ON_DEMAND [] = {0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x01, 0x01, 0x00, 0x01, 0x01, (byte)0xD7, 0x34};
	
	/*
	  56 69 56 4F 74 65 63 68 32 00 04 00 00 AE 5F 2A 02 08 40 9C 01 00 9F 02 06 00 00 00 00 00 01 9F 03 06 00 00 
	  00 00 00 00 9F 1A 02 08 40 9F 1B 04 00 00 17 70 9F 33 03 00 08 E8 9F 35 01 22 9F 40 05 60 00 00 30 00 9F 66 
	  04 80 00 00 00 DF 63 01 00 DF 64 01 00 DF 65 01 00 DF 66 01 00 FF F0 03 00 00 00 FF F1 06 00 00 00 01 00 00 
	  FF F2 08 30 30 30 30 30 30 30 30 FF F3 02 03 FF FF F4 03 01 00 01 FF F5 06 00 00 00 00 80 00 FF F7 01 02 FF 
	  F8 01 00 FF F9 01 03 FF FA 02 03 E8 FF FB 01 00 FF FC 01 01 FF FD 05 F8 50 AC F8 00 FF FE 05 F8 50 AC A0 00 
	  FF FF 05 00 00 00 00 00 44 54
	 */
	private byte RESTORE_DEFAULT_CONFIGURATION_WITH_LCD [] = {
	  0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x04, 0x00, 0x00, (byte)0xAE, 0x5F, 0x2A, 0x02, 
	  0x08, 0x40, (byte)0x9C, 0x01, 0x00, (byte)0x9F, 0x02, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, (byte)0x9F, 0x03, 0x06, 
	  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, (byte)0x9F, 0x1A, 0x02, 0x08, 0x40, (byte)0x9F, 0x1B, 0x04, 0x00, 0x00, 0x17, 
	  0x70, (byte)0x9F, 0x33, 0x03, 0x00, 0x08, (byte)0xE8, (byte)0x9F, 0x35, 0x01, 0x22, (byte)0x9F, 0x40, 0x05, 0x60, 0x00, 0x00, 
	  0x30, 0x00, (byte)0x9F, 0x66, 0x04, (byte)0x80, 0x00, 0x00, 0x00, (byte)0xDF, 0x63, 0x01, 0x00, (byte)0xDF, 0x64, 0x01, 0x00, 
	  (byte)0xDF, 0x65, 0x01, 0x00, (byte)0xDF, 0x66, 0x01, 0x00, (byte)0xFF, (byte)0xF0, 0x03, 0x00, 0x00, 0x00, (byte)0xFF, (byte)0xF1, 0x06, 
	  0x00, 0x00, 0x00, 0x01, 0x00, 0x00, (byte)0xFF, (byte)0xF2, 0x08, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 
	  (byte)0xFF, (byte)0xF3, 0x02, 0x03, (byte)0xFF, (byte)0xFF, (byte)0xF4, 0x03, 0x01, 0x00, 0x01, (byte)0xFF, (byte)0xF5, 0x06, 0x00, 0x00, 0x00, 
	  0x00, (byte)0x80, 0x00, (byte)0xFF, (byte)0xF7, 0x01, 0x02, (byte)0xFF, (byte)0xF8, 0x01, 0x00, (byte)0xFF, (byte)0xF9, 0x01, 0x03, (byte)0xFF, (byte)0xFA, 
	  0x02, 0x03, (byte)0xE8, (byte)0xFF, (byte)0xFB, 0x01, 0x00, (byte)0xFF, (byte)0xFC, 0x01, 0x01, (byte)0xFF, (byte)0xFD, 0x05, (byte)0xF8, 0x50, (byte)0xAC, 
	  (byte)0xF8, 0x00, (byte)0xFF, (byte)0xFE, 0x05, (byte)0xF8, 0x50, (byte)0xAC, (byte)0xA0, 0x00, (byte)0xFF, (byte)0xFF, 0x05, 0x00, 0x00, 0x00, 0x00, 
	  0x00, 0x44, 0x54
	};
	
	/*
	 * Commands to enable emv contact transactions - 9/14/15
		1.      Set param to default:  (aka "Send Reset to Default" (upgrade FW 2.1.1 to 2.1.2)
		56 69 56 4F 74 65 63 68 32 00 04 09 00 00 87 30 
		2.      Disable GR Backward compatibilities: (aka "Send Disable GR Backward Compatibility")
		56 69 56 4F 74 65 63 68 32 00 04 00 00 04 DF 59 01 00 6A BF
		3.      Enable CT (aka "Send Enable CT")
		56 69 56 4F 74 65 63 68 32 00 04 00 00 05 FF F3 02 9F FF 88 BC 
		4.      Power cycle the reader
		5.      Activate transaction and then insert EMV CT test card (aka "Send Activate Txn 2")
		
		as of 11/3/15 the sequence is:
		8, 10, 11, cycle power, 12
		
		when stuck in "Please wait" the following brought the reader back to Welcome:
		14, 13
		
		56 69 56 4F 74 65 63 68 32 00 02 01 00 24 0F 9F 02 06 00 00 00 00 01 00 9C 01 00 9A 03 09 02 11 9F 21 03 10 20 30 5F 57 01 00
		DF 22 01 00 DF 11 01 01 8A 70		 
	 */
	private byte RESET_TO_DEFAULT [] = {0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x04, 0x09, 0x00, 0x00, (byte)0x87, 0x30};
	private byte DISABLE_GR_BACKWARD_COMPAT [] = {
		0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x04, 0x00, 0x00, 0x04, (byte)0xDF, 0x59, 0x01, 0x00, 0x6A, (byte)0xBF	
	};
	private byte ENABLE_CT [] = {
		0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x04, 0x00, 0x00, 0x05, (byte)0xFF, (byte)0xF3, 0x02, (byte)0x9F, (byte)0xFF, (byte)0x88, (byte)0xBC
	};
	
//	private byte ACTIVATE_TRANSACTION_2 [] = {
//		0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x02, 0x01, 0x00, 0x24, 0x0F, (byte)0x9F, 0x02, 0x06, 0x00, 0x00, 
//		0x00, 0x00, 0x01, 0x00, (byte)0x9C, 0x01, 0x00, (byte)0x9A, 0x03, 0x09, 0x02, 0x11, (byte)0x9F, 0x21, 0x03, 0x10, 0x20, 0x30, 0x5F, 0x57, 
//		0x01, 0x00, (byte)0xDF, 0x22, 0x01, 0x00, (byte)0xDF, 0x11, 0x01, 0x01, (byte)0x8A, 0x70
//	};		

	private int TLV_AMOUNT_AUTHED_OFFSET = 18;
	private byte ACTIVATE_TRANSACTION_2_WITHOUT_DATE [] = {
		0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x02, 0x01, 
		0x00, 0x24, 
		0x0F, 			// timeout
		(byte)0x9F, 0x02, 0x06, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00,		// TLV Authorized amount 
		(byte)0x9C, 0x01, 0x00, (byte)0x9F, 0x21, 0x03, 0x10, 0x20, 0x30, 0x5F, 0x57, 
		0x01, 0x00, (byte)0xDF, 0x22, 0x01, 0x00, (byte)0xDF, 0x11, 0x01, 0x01
	};		
	
	
	// 0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x02, 0x01, 0x00, 0x1D, 0x0A, 0x9F, 0x02, 0x06, 0x00, 0x00, 
	// 0x00, 0x00, 0x00, 0x01, 0x9C, 0x01, 0x00, 0xFF, 0xEE, 0x06, 0x0C, 0x9F, 0x22, 0x02, 0x01, 0x00, 0x9F, 0x26, 0x04, 0x00, 0x00, 0x00, 0x02
	private byte ACT_APPLE_VAS_NO_CRC [] = {
		0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x02, 0x01, 
		0x00, 0x1D, 0x0A, (byte)0x9F, 0x02, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 
		(byte)0x9C, 0x01, 0x00, (byte)0xFF, (byte)0xEE, 0x06, 0x0C, (byte)0x9F, 0x22, 0x02, 0x01, 0x00, (byte)0x9F, 0x26, 0x04, 0x00, 0x00, 0x00, 0x02
	};
	
	// 0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x02, 0x01, 0x00, 0x29, 0x0A, 0x9F, 0x02, 0x06, 0x00, 0x00, 
	// 0x00, 0x00, 0x00, 0x01, 0x9C, 0x01, 0x00, 0xFF, 0xEE, 0x06, 0x18, 0x9F, 0x22, 0x02, 0x01, 0x00, 0x9F, 0x26, 0x04, 0x00, 
	// 0x00, 0x00, 0x02, 0x9F, 0x2B, 0x05, 0x02, 0xFF, 0xFF, 0xFF, 0xFF, 0xDF, 0x01, 0x01, 0x01
	private byte ACT_WITH_OPTIONAL_PARMS_APPLE_VAS_NO_CRC [] = {
			0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x02, 0x01, 
			0x00, 0x29, 0x0A, (byte)0x9F, 0x02, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, (byte)0x9C, 0x01, 0x00, (byte)0xFF, (byte)0xEE, 0x06, 0x18, 
			(byte)0x9F, 0x22, 0x02, 0x01, 0x00, (byte)0x9F, 0x26, 0x04, 0x00, 0x00, 0x00, 0x02, (byte)0x9F, 0x2B, 0x05, 0x02, (byte)0xFF, (byte)0xFF, 
			(byte)0xFF, (byte)0xFF, (byte)0xDF, 0x01, 0x01, 0x01
	};
	
	private byte GET_APPLE_VAS_MERCHANT_1 [] = {
		0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x03, 0x11, 0x00, 0x01, 0x01, (byte)0xF3, 0x6B			
	};
	private byte GET_APPLE_VAS_MERCHANT_2 [] = {
		0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x03, 0x11, 0x00, 0x01, 0x02, (byte)0x90, 0x5B			
	};
	private byte CLEAR_APPLE_VAS_MERCHANT_RECORD_1 [] = {
		0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x04, 0x11, 0x00, 0x63, 0x01, 0x00, 0x00, 0x00, 0x00, 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
		0x00
	};
	private byte CLEAR_APPLE_VAS_MERCHANT_RECORD_2 [] = {
		0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x04, 0x11, 0x00, 0x63, 0x02, 0x00, 0x00, 0x00, 0x00, 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
		0x00	
	};
	
	private byte SET_APPLE_VAS_MERCHANT_RECORD_1_VAS_PROD [] = {
		0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x04, 0x11, 0x00, 0x63, 0x01, 0x01, 0x3C, (byte)0xC7, 0x0E, 
		(byte)0xD8, (byte)0x9A, (byte)0x9D, 0x43, 0x54, (byte)0xBE, (byte)0x98, 0x30, (byte)0xAB, 0x58, (byte)0xD8, (byte)0x9C, 0x6F, (byte)0xE7, (byte)0xE6, 0x2B, (byte)0xAC, (byte)0xA9, 0x39, 
		(byte)0xD2, (byte)0xA6, (byte)0x85, 0x1D, (byte)0xFC, 0x60, 0x2E, (byte)0xA7, (byte)0x98, (byte)0xF7, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x25, 
		(byte)0xB1			
	};
	
	private byte SET_APPLE_VAS_USAT_MERCHANT_RECORD_1_VAS_PROD_NO_CRC [] = {
		0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x04, 0x11, 
		0x00, 0x63, 
		0x01,	// Cfg index 
		0x01,	// ID present 
		0x1c, (byte)0xbf, 0x33, 0x1c, (byte)0xd3, (byte)0x89, (byte)0x82, (byte)0x88, (byte)0xfb, (byte)0x82, (byte)0xc5, (byte)0x89, 0x04, (byte)0xd4, 0x26, 
		0x11, (byte)0xcf, (byte)0xcf, (byte)0xf0, 0x78, 0x6a, (byte)0xdd, 0x0c, 0x6b, 0x40, 0x69, 0x25, 0x7c, (byte)0xbf, 0x0c, 0x3b, (byte)0xc8,
		
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	};
	private byte SET_APPLE_VAS_USAT_MERCHANT_RECORD_2_VAS_PROD_NO_CRC [] = {
		0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x04, 0x11, 
		0x00, 0x63, 
		0x02,	// Cfg index 
		0x01,	// ID present 
		0x1c, (byte)0xbf, 0x33, 0x1c, (byte)0xd3, (byte)0x89, (byte)0x82, (byte)0x88, (byte)0xfb, (byte)0x82, (byte)0xc5, (byte)0x89, 0x04, (byte)0xd4, 0x26, 
		0x11, (byte)0xcf, (byte)0xcf, (byte)0xf0, 0x78, 0x6a, (byte)0xdd, 0x0c, 0x6b, 0x40, 0x69, 0x25, 0x7c, (byte)0xbf, 0x0c, 0x3b, (byte)0xc8,
		
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	};
	
	
	private byte SET_APPLE_VAS_MERCHANT_RECORD_6_VAS_PROD [] = {
		0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x04, 0x11, 0x00, 0x63, 0x06, 0x01, 0x3C, (byte)0xC7, 0x0E,
		(byte)0xD8, (byte)0x9A, (byte)0x9D, 0x43, 0x54, (byte)0xBE, (byte)0x98, 0x30, (byte)0xAB, 0x58, (byte)0xD8, (byte)0x9C, 0x6F, (byte)0xE7, (byte)0xE6, 0x2B, (byte)0xAC, (byte)0xA9, 0x39,
		(byte)0xD2, (byte)0xA6, (byte)0x85, 0x1D, (byte)0xFC, 0x60, 0x2E, (byte)0xA7, (byte)0x98, (byte)0xF7, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, (byte)0xB5,
		(byte)0xC7	
	};
	
	private byte DISPLAY_ONLINE_AUTHORIZATION_RESULT_GOOD_NO_CRC [] = {
		0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, Command.GET_TRANSACTION_RESULT.getValue(), 0x03, 0x00, 0x01, 0x00
	};
	private byte DISPLAY_ONLINE_AUTHORIZATION_RESULT_FAILED_NO_CRC [] = {
		0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, Command.GET_TRANSACTION_RESULT.getValue(), 0x03, 0x00, 0x01, 0x01
	};
	
	private byte CANCEL_TRANSACTION_NO_CRC [] = {
		0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x05, 0x01, 0x00, 0x00
	};
	
	// 56 69 56 4F 74 65 63 68 32 00 02 07 00 01 00 BD ED
	private byte CONTINUE_TRANSACTION_FOR_CARDHOLDER_VERIFICATION [] = {
		0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x02, 0x07, 0x00, 0x01, 0x00, (byte)0xbd, (byte)0xed 
	};
	
	// 56 69 56 4F 74 65 63 68 32 00 02 06 00 05 00 8A 02 30 30 85 A8
	private byte CONTINUE_TXN_FOR_ONLINE_AUTHORIZATION [] = {
		0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x02, 0x06, 0x00, 0x05, 0x00, (byte)0x8A, 0x02, 0x30, 0x30, (byte)0x85, (byte)0xA8	
	};
	
	private byte GET_FIRMWARE_VERSION_NO_CRC [] = {
		0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x29, 0x00, 0x00, 0x00 
	};
	
	private byte GET_SERIAL_NUMBER_NO_CRC [] = {
		0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x12, 0x01, 0x00, 0x00 
	};
	
	private byte GET_MODULE_VERSION_FULL_NO_CRC [] = {
		0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x09, 0x20, 0x00, 0x00 
	};
	
	private byte GET_BOOTLOADER_VERSION_NO_CRC [] = {
		0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x77, 0x01, 0x00, 0x00
	};
	
	private byte GET_CONFIGURATION_NO_CRC [] = {
		0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x03, 0x02, 0x00, 0x00 
	};
	private byte GET_ALL_GROUPS [] = { 
		0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x03, 0x07, 0x00, 0x00, (byte)0xAB, 0x7A
	};
	private byte GET_ALL_AIDS [] = {
		0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, 0x03, 0x05, 0x00, 0x00, (byte)0xCB, 0x14	
	};
	
	private byte START_CUSTOM_DISPLAY_MODE [] = {
		0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, Command.DISPLAY.getValue(), 0x08, 0x00, 0x00, (byte)0xa2, (byte)0x8b  
	};
	
	private byte STOP_CUSTOM_DISPLAY_MODE [] = {
		0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, Command.DISPLAY.getValue(), 0x09, 0x00, 0x00, (byte)0x92, (byte)0xbc  
	};
	
	private byte LIST_DIRECTORY [] = {
		0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, Command.DISPLAY.getValue(), 0x22, 0x00, 0x04, 0x31, 0x30, 0x00, 0x00, (byte)0x9D, (byte)0x9B
	};
	
	// 56 69 56 4F 74 65 63 68 32 00 83 23 00 00 95 FD
	private byte GET_DRIVE_FREE_SPACE [] = {
		0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, Command.DISPLAY.getValue(), 0x23, 0x00, 0x00, (byte)0x95, (byte)0xFD
	};
	
	private Socket socket;
	private OutputStream outputStream;
	private InputStream inputStream;
//	private BufferedWriter output;
//	private BufferedReader input;
	
	private String host;
	private int port;
	
	private VendXTxnManager vendXTxnManager;
	
	public Vend3(String host, int port) {
		this.host = host;
		this.port = port;
	}
	
	public boolean open() throws UnknownHostException, IOException {
		boolean opened = false;
		try {
			socket = new Socket(getHost(), getPort());
			inputStream = getSocket().getInputStream();
			outputStream = getSocket().getOutputStream();
			opened = true;
		} catch (ConnectException e) {
			/*
			 * See tcp_serial_redirect.py at https://github.com/bewest/pyserial/blob/master/pyserial/examples/
			 * and start with the following:
			 * 
			 * #!/bin/sh
			 * ./tcp_serial_redirect.py --port=/dev/tty.usbserial --baud=9600 --localport=5555
			 */
			log.info("Is the SERIAL to TCP/IP shim running?");
//			throw e;
		}
		return opened;
	}

	public void close(){
		if (getSocket() != null) {
			try {
				getSocket().close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void resetCounter() {
		counter = 0;
		bufferCounter = 0;
	}

	public void startCustomDisplayMode() {
		resetCounter();
		System.out.println("Sending START_CUSTOM_DISPLAY_MODE");
		sendCmd(START_CUSTOM_DISPLAY_MODE);
		log.info("Done");
	}

	public void stopCustomDisplayMode() {
		resetCounter();
		System.out.println("Sending STOP_CUSTOM_DISPLAY_MODE");
		sendCmd(STOP_CUSTOM_DISPLAY_MODE);
		log.info("Done");
	}
	
	public void listDirectory() {
		resetCounter();
		System.out.println("Sending LIST_DIRECTORY");
		addBlobHandler(new VendXBlobHandler() {
			@Override
			public void message(IdtechVendXDataBlob responseMsg) {
				if (responseMsg.getCmdByte() == Command.DISPLAY.getValue()) {
					String listing = new String(responseMsg.getDataBuffer());
					listing = listing.replace(',', '\n');
					log.info("Directory Listing:\n" + listing);
					removeBlobHandler(this);
				}
			}
		});
		
		sendCmd(LIST_DIRECTORY);
		log.info("Done");
	}
	
	public void getDriveFreeSpace() {
		resetCounter();
		System.out.println("Sending GET_DRIVE_FREE_SPACE");
		addBlobHandler(new VendXBlobHandler() {
			@Override
			public void message(IdtechVendXDataBlob responseMsg) {
				if (responseMsg.getCmdByte() == Command.DISPLAY.getValue()) {
					StringBuffer sb = new StringBuffer("Drive Free Space - Free: ");
					String hex = StringUtils.toHex(responseMsg.getDataBuffer(), 0, responseMsg.getDataBuffer().length);
				    for (int i = 0; i < hex.length(); i+=2) {
				        String str = hex.substring(i, i+2);
				        if (!str.equals("00")) {
				        	sb.append((char)Integer.parseInt(str, 16));
				        } else {
				        	sb.append(", Used: ");
				        }
				    }
					log.info(sb.toString());
				}
				removeBlobHandler(this);
			}
		});
		sendCmd(GET_DRIVE_FREE_SPACE);
		log.info("Done");
	}
	
	public void displayImage() {
		resetCounter();
		byte [] xOrigin = new byte [] { 0x00, 0x00 };
		byte [] yOrigin = new byte [] { 0x00, 0x00 };
//		byte [] flag = new byte [] { 0x04, 0x00 };		// Center image on screen (ignore xOrigin, yOrigin), clear screen before display
		byte [] flag = new byte [] { 0x35, 0x00 };		// Center image on screen (ignore xOrigin, yOrigin), clear screen before display
		byte [] fileToDisplay = nullTerminate(DEST_FILENAME.getBytes());
		
//		int lengthLessCrc = HEADER_LENGTH + filename.length + totalFileBytes.length + flag.length + fileSection.length;
//		byte [] cmd = new byte [lengthLessCrc];
//		System.arraycopy(PREAMBLE, 0, cmd, 0, PREAMBLE.length);
//		System.arraycopy(TRANSFER_FILE_CMD, 0, cmd, PREAMBLE.length, TRANSFER_FILE_CMD.length);
//		
//		// wants payload length
//		int payloadLength = filename.length + totalFileBytes.length + flag.length + fileSection.length;
//		byte lsbLength = (byte)(payloadLength & 0xff);
//		byte msbLength = (byte)((payloadLength >> 8) & 0xff);
//		int lengthOffset = PREAMBLE.length + TRANSFER_FILE_CMD.length; 
//		cmd[lengthOffset] = msbLength;
//		cmd[lengthOffset + 1] = lsbLength;
//		
//		int dataOffset = lengthOffset + 2;
//		System.arraycopy(filename, 0, cmd, dataOffset, filename.length);
//		System.arraycopy(totalFileBytes, 0, cmd, dataOffset + filename.length, totalFileBytes.length);
//		System.arraycopy(flag, 0, cmd, dataOffset + filename.length + totalFileBytes.length, flag.length);
//		System.arraycopy(fileSection, 0, cmd, dataOffset + filename.length + totalFileBytes.length + flag.length, fileSection.length);
//		
//		// ship it
//		sendCmdAndCalcCrc(cmd);

		byte [] cmd = constructHeader(new int [] {xOrigin.length, yOrigin.length, flag.length, fileToDisplay.length}, DISPLAY_IMAGE_CMD);
		
		int dataOffset = PREAMBLE.length + DISPLAY_IMAGE_CMD.length + 2;
		System.arraycopy(xOrigin, 0, cmd, dataOffset, xOrigin.length);
		System.arraycopy(yOrigin, 0, cmd, dataOffset + xOrigin.length, yOrigin.length);
		System.arraycopy(flag, 0, cmd, dataOffset + xOrigin.length + yOrigin.length, flag.length);
		System.arraycopy(fileToDisplay, 0, cmd, dataOffset + xOrigin.length + yOrigin.length + flag.length, fileToDisplay.length);
		// ship it
		sendCmdAndCalcCrc(cmd);
		
	}
	
	private byte [] constructHeader(int [] lengthList, byte [] cmdBytes) {
		int payloadLength = 0;
		for (int length: lengthList) {
			payloadLength = payloadLength + length;
		}
		int lengthLessCrc = HEADER_LENGTH + payloadLength;
		
		byte [] cmd = new byte [lengthLessCrc];
		System.arraycopy(PREAMBLE, 0, cmd, 0, PREAMBLE.length);
		System.arraycopy(cmdBytes, 0, cmd, PREAMBLE.length, cmdBytes.length);
		
		byte lsbLength = (byte)(payloadLength & 0xff);
		byte msbLength = (byte)((payloadLength >> 8) & 0xff);
		int lengthOffset = PREAMBLE.length + DISPLAY_IMAGE_CMD.length; 
		cmd[lengthOffset] = msbLength;
		cmd[lengthOffset + 1] = lsbLength;
		return cmd;
	}
	
	public void fileTransfer() {
		resetCounter();
		System.out.println("Sending FILE TRANSFER");
//		String destFilename = "testfile1.x";
//		String sourceFilename = "/Users/jscarpaci/tmp/testfile.x";
//		String destFilename = "testfile2.x";
//		String sourceFilename = "/Users/jscarpaci/tmp/testfile2.x";
		try {
			final TransferFile transferFile = new TransferFile(DEST_FILENAME, SOURCE_FILENAME);
			VendXBlobHandler handler = new VendXBlobHandler() {
				@Override
				public void message(IdtechVendXDataBlob responseMsg) {
					boolean isMore = transferFile.sendNext();
					if (isMore) {
						log.info("sending next chunk");
					} else {
						log.info("done sending");
						removeBlobHandler(this);
					}
				}
			};
			addBlobHandler(handler);
			boolean isMore = transferFile.sendNext();
			if (!isMore) {
				removeBlobHandler(handler);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void getBootloaderVersion() {
		resetCounter();
		System.out.print("Sending GET_BOOTLOADER_VERSION_NO_CRC...");
		sendCmdAndCalcCrc(GET_BOOTLOADER_VERSION_NO_CRC);
		log.info("Done");
	}

	public void getFirmwareVersion() {
		resetCounter();
		System.out.print("Sending GET_FIRMWARE_VERSION_NO_CRC...");
		sendCmdAndCalcCrc(GET_FIRMWARE_VERSION_NO_CRC);
		log.info("Done");
	}
	
	public void getSerialNumber() {
		resetCounter();
		System.out.println("Sending GET_SERIAL_NUMBER_NO_CRC");
		sendCmdAndCalcCrc(GET_SERIAL_NUMBER_NO_CRC);
		log.info("Done");
	}

	public void getModuleVersionFull() {
		resetCounter();
		System.out.print("Sending GET_MODULE_VERSION_FULL_NO_CRC...");
		sendCmdAndCalcCrc(GET_MODULE_VERSION_FULL_NO_CRC);
		log.info("Done");
	}
	
	public void getConfiguration() {
		resetCounter();
		log.info("Sending GET_CONFIGURATION_NO_CRC");
		sendCmdAndCalcCrc(GET_CONFIGURATION_NO_CRC);
		log.info("Done");
	}
	public void getAllGroups() {
		log.info("Sending GET_ALL_GROUPS");
		sendCmd(GET_ALL_GROUPS);
		log.info("Done");
	}
	public void getAllAIDs() {
		log.info("Sending GET_ALL_AIDS");
		sendCmd(GET_ALL_AIDS);
		log.info("Done");
	}
	public void sendContinueTxnForOnlineAuthorizationSuccess() {
		resetCounter();
		System.out.print("Sending CONTINUE_TXN_FOR_ONLINE_AUTHORIZATION...");
		sendCmd(CONTINUE_TXN_FOR_ONLINE_AUTHORIZATION);
		log.info("Done");
	}
	
	public void sendContinueTransactionForCardholderVerification() {
		resetCounter();
		System.out.print("Sending CONTINUE_TRANSACTION_FOR_CARDHOLDER_VERIFICATION...");
		sendCmd(CONTINUE_TRANSACTION_FOR_CARDHOLDER_VERIFICATION);
		log.info("Done");
	}
	
	public void sendCancelTransaction()  {
		resetCounter();
		System.out.print("Sending CANCEL_TRANSACTION...");
		sendCmdAndCalcCrc(CANCEL_TRANSACTION_NO_CRC);
		log.info("Done");
	}
	
	public void sendResetToDefault()  {
		resetCounter();
		System.out.print("Sending RESET_TO_DEFAULT...");
		sendCmd(RESET_TO_DEFAULT);
		log.info("Done");
	}
	
	public void sendDisableGRBackwardCompat()  {
		resetCounter();
		System.out.print("Sending DISABLE_GR_BACKWARD_COMPAT...");
		sendCmd(DISABLE_GR_BACKWARD_COMPAT);
		log.info("Done");
	}
	
	public void sendEnableCT()  {
		resetCounter();
		System.out.print("Sending ENABLE_CT...");
		sendCmd(ENABLE_CT);
		log.info("Done");
	}
	
//	public void sendActivateTransaction2()  {
//		resetCounter();
//		System.out.print("Sending ACTIVATE_TRANSACTION_2...");
//		sendCmd(ACTIVATE_TRANSACTION_2);
//		log.info("Done");
//	}
	// (byte)0x9A, 0x03, 0x09, 0x02, 0x11, 
	public void sendActivateTransaction2(String authorizedAmount)  {
		resetCounter();
		System.out.print("Sending ACTIVATE_TRANSACTION_2 with current date...");
		byte [] amountAuthed = new byte [] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
		try {
			String paddedAmount = StringUtils.pad(authorizedAmount, '0', amountAuthed.length, Justification.RIGHT);
			byte [] tmp = ByteArrayUtils.fromHex(paddedAmount);
			System.arraycopy(tmp, 0, amountAuthed, amountAuthed.length - tmp.length, tmp.length);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		int end = ACTIVATE_TRANSACTION_2_WITHOUT_DATE.length;
		byte [] cmd = new byte [end + 5];
		System.arraycopy(ACTIVATE_TRANSACTION_2_WITHOUT_DATE, 0, cmd, 0, end);
		// copy in the auth amount
		System.arraycopy(amountAuthed, 0, cmd, TLV_AMOUNT_AUTHED_OFFSET, amountAuthed.length);
		cmd[end] = TLVTag.TRANSACTION_DATE.getValue()[0];
		cmd[end + 1] = 0x03;
		cmd[end + 2] = (byte)(cal.get(Calendar.YEAR) - 2000);
		cmd[end + 3] = (byte)(cal.get(Calendar.MONTH) + 1);
		cmd[end + 4] = (byte)(cal.get(Calendar.DAY_OF_MONTH));
		sendCmdAndCalcCrc(cmd);
		log.info("Done");
	}
	
	public void sendActAppleVASNoOptional() {
		resetCounter();
		System.out.println("Sending ACT_APPLE_VAS_NO_CRC");
		sendCmdAndCalcCrc(ACT_APPLE_VAS_NO_CRC);
		log.info("Done");
	}
	
	public void sendActAppleVASWithOptionalParms() {
		resetCounter();
		System.out.println("Sending ACT_WITH_OPTIONAL_PARMS_APPLE_VAS_NO_CRC");
		sendCmdAndCalcCrc(ACT_WITH_OPTIONAL_PARMS_APPLE_VAS_NO_CRC);
		log.info("Done");
	}
	
	public void sendGetAppleVASMerchant1() {
		resetCounter();
		System.out.println("Sending GET_APPLE_VAS_MERCHANT_1");
		sendCmd(GET_APPLE_VAS_MERCHANT_1);
		log.info("Done");
	}
	public void sendGetAppleVASMerchant2() {
		resetCounter();
		System.out.println("Sending GET_APPLE_VAS_MERCHANT_2");
		sendCmd(GET_APPLE_VAS_MERCHANT_2);
		log.info("Done");
	}
	public void sendClearAppleVASMerchantRecord1() {
		resetCounter();
		System.out.println("NOTE: THESE EXAMPLES DID NOT WORK.  Sending CLEAR_APPLE_VAS_MERCHANT_RECORD_1");
		sendCmd(CLEAR_APPLE_VAS_MERCHANT_RECORD_1);
		log.info("Done");		
	}
	public void sendClearAppleVASMerchantRecord2() {
		resetCounter();
		System.out.println("NOTE: THESE EXAMPLES DID NOT WORK. Sending CLEAR_APPLE_VAS_MERCHANT_RECORD_2");
		sendCmdAndCalcCrc(CLEAR_APPLE_VAS_MERCHANT_RECORD_2);
		log.info("Done");		
	}
	public void sendSetAppleVASMerchantRecord1VASProd() {
		resetCounter();
		System.out.println("Sending SET_APPLE_VAS_MERCHANT_RECORD_1_VAS_PROD");
		sendCmd(SET_APPLE_VAS_MERCHANT_RECORD_1_VAS_PROD);
		log.info("Done");
	}
	public void sendSetAppleVASUSATMerchantRecord1() {
		//SET_APPLE_VAS_USAT_MERCHANT_RECORD_1_VAS_PROD		
		resetCounter();
		System.out.println("Sending SET_APPLE_VAS_USAT_MERCHANT_RECORD_1_VAS_PROD_NO_CRC");
		sendCmdAndCalcCrc(SET_APPLE_VAS_USAT_MERCHANT_RECORD_1_VAS_PROD_NO_CRC);
		log.info("Done");
	}
	public void sendSetAppleVASUSATMerchantRecord2() {
		//SET_APPLE_VAS_USAT_MERCHANT_RECORD_2_VAS_PROD		
		resetCounter();
		System.out.println("Sending SET_APPLE_VAS_USAT_MERCHANT_RECORD_2_VAS_PROD_NO_CRC");
		sendCmdAndCalcCrc(SET_APPLE_VAS_USAT_MERCHANT_RECORD_2_VAS_PROD_NO_CRC);
		log.info("Done");
	}
	public void sendSetAppleVASMerchantRecord6VASProd() {
		resetCounter();
		System.out.println("Sending SET_APPLE_VAS_MERCHANT_RECORD_6_VAS_PROD");
		sendCmd(SET_APPLE_VAS_MERCHANT_RECORD_6_VAS_PROD);
		log.info("Done");
	}
	
	
	public void sendDisplayAuthorizationFailed() {
		resetCounter();
		log.info("Sending DISPLAY_ONLINE_AUTHORIZATION_RESULT_FAILED_NO_CRC");
		sendCmdAndCalcCrc(DISPLAY_ONLINE_AUTHORIZATION_RESULT_FAILED_NO_CRC);
		log.info("Done");
	}

	public void sendDisplayAuthorizationSuccess() {
		resetCounter();
		log.info("Sending DISPLAY_ONLINE_AUTHORIZATION_RESULT_GOOD_NO_CRC");
		sendCmdAndCalcCrc(DISPLAY_ONLINE_AUTHORIZATION_RESULT_GOOD_NO_CRC);
		log.info("Done");
	}

	// RESTORE_DEFAULT_CONFIGURATION_WITH_LCD
	public void sendRestoreDefaultConfigurationWithLCD()  {
		resetCounter();
		System.out.print("Sending RESTORE_DEFAULT_CONFIGURATION_WITH_LCD...");
		sendCmd(RESTORE_DEFAULT_CONFIGURATION_WITH_LCD);
		log.info("Done");
	}
	
	public void sendSetPollModeAutoPoll()  {
		resetCounter();
		System.out.print("Sending SET_POLL_MODE_AUTOPOLL...");
		sendCmd(SET_POLL_MODE_AUTOPOLL);
		log.info("Done");
	}
	
	public void sendSetPollModeOnDemand()  {
		resetCounter();
		System.out.print("Sending SET_POLL_MODE_ON_DEMAND...");
		sendCmd(SET_POLL_MODE_ON_DEMAND);
		log.info("Done");
	}
	
	public void sendBurstModeEnable()  {
		resetCounter();
		System.out.print("Sending BURST_MODE_ENABLE...");
		sendCmd(BURST_MODE_ENABLE);
		log.info("Done");
	}
	
	public void sendBurstModeDisable()  {
		resetCounter();
		System.out.print("Sending BURST_MODE_DISABLE...");
		sendCmd(BURST_MODE_DISABLE);
		log.info("Done");
	}
	
	public void sendActivateTransaction()  {
		resetCounter();
		System.out.print("Sending ACTIVATE_TRANSACTION...");
		sendCmd(ACTIVATE_TRANSACTION);
		log.info("Done");
	}
	
	public void sendSetEMVForVisaAndMastercard()  {
		resetCounter();
		System.out.print("Sending SET_EMV_MASTERCARD_AND_VISA...");
		sendCmd(SET_EMV_MASTERCARD_AND_VISA);
		log.info("Done");
	}
	public void sendSetEMVForAmex() {
		//SET_EMV_AMEX_NO_CRC
		resetCounter();
		System.out.println("Sending SET_EMV_AMEX_NO_CRC");
		sendCmdAndCalcCrc(SET_EMV_AMEX_NO_CRC);
		log.info("Done");
	}
	public void sendListAmexKeysContactless() {
		resetCounter();
		System.out.println("Sending LIST_AMEX_KEYS_CONTACTLESS");
		sendCmd(LIST_AMEX_KEYS_CONTACTLESS);
		log.info("Done");
	}
	public void sendListAmexKeysContact() {
		//LIST_AMEX_KEYS_CONTACT_NO_CRC		
		resetCounter();
		System.out.println("Sending LIST_AMEX_KEYS_CONTACT_NO_CRC");
		sendCmdAndCalcCrc(LIST_AMEX_KEYS_CONTACT_NO_CRC);
		log.info("Done");
	}
	public void sendListDiscoverKeys() {
		resetCounter();
		System.out.println("Sending LIST_DISCOVER_KEYS_NO_CRC");
		sendCmdAndCalcCrc(LIST_DISCOVER_KEYS_NO_CRC);
		log.info("Done");
	}
	public void sendListDiscoverContactKeys() {
		resetCounter();
		System.out.println("Sending LIST_DISCOVER_CONTACT_KEYS_NO_CRC");
		sendCmdAndCalcCrc(LIST_DISCOVER_CONTACT_KEYS_NO_CRC);
		log.info("Done");
	}
	public void sendListDiscoverContactlessKeys() {
		resetCounter();
		System.out.println("Sending LIST_DISCOVER_CONTACTLESS_KEYS_NO_CRC");
		sendCmdAndCalcCrc(LIST_DISCOVER_CONTACTLESS_KEYS_NO_CRC);
		log.info("Done");
	}
	
	// same as 7) Set Configurable Discover Contact AID", () -> reader.sendSetConfigurableDiscoverContactAID()
//	public void sendCreateDiscoverContactAID() {
//		//CREATE_DISCOVER_AID		
//		resetCounter();
//		System.out.println("Sending CREATE_DISCOVER_CONTACT_AID");
//		sendCmd(CREATE_DISCOVER_CONTACT_AID);
//		log.info("Done");
//	}
	
	public void sendSetConfigurableAmexContactAID() {
		resetCounter();
		System.out.println("Sending SET_CONFIGURABLE_AID_AMEX_CONTACT");
		sendCmd(SET_CONFIGURABLE_AID_AMEX_CONTACT);
		log.info("Done");
	}
	public void sendSetConfigurableDiscoverContactAID() {
		resetCounter();
		System.out.println("Sending SET_CONFIGURABLE_AID_DISCOVER_CONTACT");
		sendCmd(SET_CONFIGURABLE_AID_DISCOVER_CONTACT);
		log.info("Done");
	}
	// SET_CONFIGURABLE_AID_DISCOVER_CONTACTLESS_NO_CRC
	public void sendSetConfigurableDiscoverContactlessAID() {
		resetCounter();
		System.out.println("Sending SET_CONFIGURABLE_AID_DISCOVER_CONTACTLESS_NO_CRC");
		sendCmdAndCalcCrc(SET_CONFIGURABLE_AID_DISCOVER_CONTACTLESS_NO_CRC);
		log.info("Done");
	}
	
	
//	public void sendActivateTxnDiscover() {
//		//ACTIVATE_TXN_DISCOVER
//		resetCounter();
//		System.out.println("Sending ACTIVATE_TXN_DISCOVER");
//		sendCmd(ACTIVATE_TXN_DISCOVER);
//		log.info("Done");
//	}
	
	public void sendGetAllCAPublicKeyRIDs() {
		resetCounter();
		System.out.println("Sending LIST_ALL_CA_PUBLIC_KEYS_NO_CRC");
		sendCmdAndCalcCrc(LIST_ALL_CA_PUBLIC_KEYS_NO_CRC);
		log.info("Done");
	}
	
	public void setCAPublicKeyAmexContact() {
		setCAPublicKeyWithHeaderAndModulus(SET_CA_PUBLIC_KEY_AMEX_CONTACT, AMEX_CONTACT_CA_PUBLIC_KEY_MODULUS, "SET_CA_PUBLIC_KEY_AMEX_CONTACT");
	}
	public void setCAPublicKeyAmexContactless() {
		setCAPublicKeyWithHeaderAndModulus(SET_CA_PUBLIC_KEY_AMEX_CONTACTLESS, AMEX_CONTACTLESS_CA_PUBLIC_KEY_MODULUS, "SET_CA_PUBLIC_KEY_AMEX_CONTACTLESS");
	}
	private byte [] setCAPublicKeyWithHeaderAndModulus(byte [] header, byte [] modulus, String sendingText) {
		int publicKeyLength = modulus.length;
		int cmdLength = header.length + publicKeyLength;
		byte [] cmd = new byte [cmdLength];
		System.arraycopy(header, 0, cmd, 0, header.length);
		System.arraycopy(modulus, 0, cmd, header.length, publicKeyLength);
		
		setCAPublicKeyDataLengthAndKeyLength(cmd, publicKeyLength, cmdLength);
		resetCounter();
		System.out.println("Sending " + sendingText);
		sendCmdAndCalcCrc(cmd);
		log.info("Done");
		return cmd;
	}
	/*
	 * Takes the publicKeyLength and cmdLength, separates them into bytes and places in appropriate location
	 * in the cmd array.
	 */
	private void setCAPublicKeyDataLengthAndKeyLength(byte [] cmd, int publicKeyLength, int cmdLength) {
		int MSB_LENGTH_OFFSET = 12;
		int LSB_LENGTH_OFFSET = 13;
		
		int MSB_PUBLIC_KEY_MODULUS_LENGTH_OFFSET = 47;
		int LSB_PUBLIC_KEY_MODULUS_LENGTH_OFFSET = 48;
		int dataLength = cmdLength - DATA_START_OFFSET;
		byte msbDataLength = (byte)((dataLength >> 8) & (byte)0xff);
		byte lsbDataLength = (byte)(dataLength & 0xff);
		byte msbPublicKeyLength = (byte)((publicKeyLength >> 8) & 0xff);
		byte lsbPublicKeyLength = (byte)(publicKeyLength & 0xff);
		cmd[MSB_LENGTH_OFFSET] = msbDataLength;
		cmd[LSB_LENGTH_OFFSET] = lsbDataLength;
		cmd[MSB_PUBLIC_KEY_MODULUS_LENGTH_OFFSET] = msbPublicKeyLength;
		cmd[LSB_PUBLIC_KEY_MODULUS_LENGTH_OFFSET] = lsbPublicKeyLength;
		
	}
	public void sendCAPublicKey(byte contactlessContactFlag, EMVCAKeyComponents components) {
//			private byte SET_CA_PUBLIC_KEY_AMEX_CONTACT [] = {
//					0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00, (byte)0xD0, 0x23,
//					0x00, 0x00,	// length - MUST FILL IN LATER
//					0x02,	// flags set to apply to contact emv keys (0x02), but only know since the card reader was looking for C8 per Pauline's direction for contactless
//					(byte)0xA0, 0x00, 0x00, 0x00, 0x25,	// amex RID
			
//					(byte)0xC9,		// keyIndex per examining emv parameter download file.  There are a total of 6 Amex RIDs with a keyIndex of 97, 98, 99, C8, C9, CA
//					0x01,		// Certification Authority Hash Algorithm indicator - SHA-1 - only algorithm supported but should pass down to devices for future proofing
//					0x01,		// Certification Authority Public Key Algorithm Indicator - RSA - only support algorithm
//					(byte)0x8E, (byte)0x8D, (byte)0xFF, 0x44, 0x3D, 0x78, (byte)0xCD, (byte)0x91, (byte)0xDE, (byte)0x88, (byte)0x82, 0x1D, 0x70, (byte)0xC9, (byte)0x8F, 0x06, 0x38, (byte)0xE5, 0x1E, 0x49, // CA Public Key Checksum
//					0x00, 0x00, 0x00, 0x03,		// 	CA Public Key Exponent
//					0x00, 0x00 	// CA Public Key Modulus length - MUST Calculate
//				};
		int FLAG_LENGTH = 1;
		int INDICATOR_LENGTH = 1;
		int MODULUS_LENGTH_LENGTH = 2;
		int EXPONENT_LENGTH = 4;
		
		int cmdLength = 0;
		cmdLength = SET_CA_PUBLIC_KEY_HEADER.length + FLAG_LENGTH + components.getAid().length + components.getIndex().length + 
				INDICATOR_LENGTH + INDICATOR_LENGTH + components.getChecksum().length + EXPONENT_LENGTH + 
				MODULUS_LENGTH_LENGTH + components.getModulus().length;
		byte [] cmd = new byte [cmdLength];
		System.arraycopy(SET_CA_PUBLIC_KEY_HEADER, 0, cmd, 0, SET_CA_PUBLIC_KEY_HEADER.length);
		cmd[SET_CA_PUBLIC_KEY_HEADER.length] = contactlessContactFlag;
		
		int currentOffset = SET_CA_PUBLIC_KEY_HEADER.length + FLAG_LENGTH;
		System.arraycopy(components.getAid(), 0, cmd, currentOffset, components.getAid().length);
		currentOffset += components.getAid().length;
		
		System.arraycopy(components.getIndex(), 0, cmd, currentOffset, components.getIndex().length);
		currentOffset += components.getIndex().length;
		
		cmd[currentOffset] = components.getHashAlgorithmIndicator();
		currentOffset += INDICATOR_LENGTH;
		cmd[currentOffset] = components.getPublicKeyAlgorithmIndicator();
		currentOffset += INDICATOR_LENGTH;
		
		System.arraycopy(components.getChecksum(), 0, cmd, currentOffset, components.getChecksum().length);
		currentOffset += components.getChecksum().length;
		
		System.arraycopy(components.getExponent(), 0, cmd, currentOffset + (EXPONENT_LENGTH - components.getExponent().length), components.getExponent().length);
//		currentOffset += components.getExponent().length;
		currentOffset += EXPONENT_LENGTH;
		currentOffset += MODULUS_LENGTH_LENGTH;
		System.arraycopy(components.getModulus(), 0, cmd, currentOffset, components.getModulus().length);
		log.info("MODULUS length: " + components.getModulus().length);
		
		setCAPublicKeyDataLengthAndKeyLength(cmd, components.getModulus().length, cmdLength);

		resetCounter();
		System.out.println("Sending CA Public key " + components.diagString());
		sendCmdAndCalcCrc(cmd);
		log.info("Done");
	}
		
	public void sendCreateNewGroup09() {
		//CREATE_NEW_GROUP_09
		resetCounter();
		System.out.print("Sending CREATE_NEW_GROUP_09...");
		sendCmd(CREATE_NEW_GROUP_09);
		log.info("Done");
	}
	
	public void sendPing()  {
		resetCounter();
		System.out.print("Sending PING_CMD...");
		sendCmd(PING_CMD);
		log.info("Done");
	}
	public void sendCmdAndCalcCrc(byte [] cmd) {
		int crc = Crc16.calculateCrc16(cmd);
		byte [] cmdWithCrc = new byte [cmd.length + 2];
		System.arraycopy(cmd, 0, cmdWithCrc, 0, cmd.length);
		byte msbCrc = (byte)((crc & 0xff00) >> 8);
		byte lsbCrc = (byte)(crc & 0x00ff);
//		log.info(String.format("msbCrc: %02X, lsbCrc: %02X", (int)msbCrc, (int)lsbCrc));
		
		cmdWithCrc[cmdWithCrc.length - 2] = lsbCrc;
		cmdWithCrc[cmdWithCrc.length - 1] = msbCrc;
		
//		log.info("sendCmdAndCalcCrc: " + StringUtils.toHex(cmdWithCrc));
		sendCmd(cmdWithCrc);
	}
	
	public void sendCmd(byte [] cmd) {
		try {
			getOutputStream().write(cmd);
			if (getVendXTxnManager() != null) {
				getVendXTxnManager().messageSent(cmd);
			}
			System.out.print("cmd: ");
			for (int ii = 0; ii < cmd.length; ii++) {
				System.out.print(StringUtils.toHex(cmd[ii]) + " ");
			}
			System.out.println();
			getOutputStream().flush();
		} catch (IOException e) {
			e.printStackTrace();
			log.info("Cmd Failed.  Cmd: " + new String(cmd));
		}
	}
	
	public static boolean crc16Check(IdtechVendXDataBlob responseMsg) {
		boolean goodCrc = false;

		if (responseMsg.getCrcBuffer() != null && responseMsg.getCrcBuffer().length > 1) {
			int crcDataLen = responseMsg.getHeaderBuffer().length + responseMsg.getDataBuffer().length;
			byte [] crcCheckData = new byte[crcDataLen];
			System.arraycopy(responseMsg.getHeaderBuffer(), 0, crcCheckData, 0, responseMsg.getHeaderBuffer().length);
			System.arraycopy(responseMsg.getDataBuffer(), 0, crcCheckData, responseMsg.getHeaderBuffer().length, responseMsg.getDataBuffer().length);
			
			int crc = Crc16.calculateCrc16(crcCheckData);
			byte msbCrc = (byte)((crc & 0xff00) >> 8);
			byte lsbCrc = (byte)(crc & 0x00ff);
			byte expectedMsbCrc = responseMsg.getCrcBuffer()[0];
			byte expectedLsbCrc = responseMsg.getCrcBuffer()[1];
			if (msbCrc == expectedMsbCrc && lsbCrc == expectedLsbCrc) {
				goodCrc = true;
			}
		} else {
			log.warn("Expected CRC empty.");
		}
		
		return goodCrc;
	}
			
	public Thread listenAndDump() {
        Thread listener = new Thread(new Runnable() {
			
			@Override
			public void run() {
				boolean dumpingBuffer = true;
				int LINE_LENGTH = 16;
		        int buffer [] = new int [LINE_LENGTH];
//		        byte bytebuffer [] = new byte [LINE_LENGTH];
		        
				int c = 0;
		        resetCounter();
		        log.info("Listening...");
		        try {
			        IdtechVendXDataBlob responseMsg = new IdtechVendXDataBlob(EMVTestMain.getDefaultCardReaderType(), EMVTestMain.getDefaultEntryType());
					while ((c = getInputStream().read()) != -1) {
				    	if (dumpingBuffer) {
					    	String value = String.format("%02X", c);
					    	System.out.print(value + " ");
				    	}
						try {
					    	boolean isCompleteMessage = responseMsg.append(c);
					    	if (isCompleteMessage) {
					    		boolean goodCrc = crc16Check(responseMsg);
					    		if (goodCrc) {
							        applyCardReaderTypeAndEntryType(responseMsg);
					    			responseList.add(responseMsg);
					    			invokeBlobHandlerList(responseMsg);
						    		byte commandByte = responseMsg.getCmdByte();
						    		//byte status = responseMsg.getStatusCode();
						    		if (commandByte == Vend3.Command.ACTIVATE_TRANSACTION.getValue()) {
						    			activateTransactionResponseHandler(responseMsg);
						    		} else if (commandByte == Vend3.Command.GET_TRANSACTION_RESULT.getValue()) {
						    			dumpDataBaseline(cmdByteStatusCodeDiagString("GET_TRANSACTION: ", responseMsg), responseMsg);
						    		} else if (commandByte == Vend3.Command.PING.getValue()) {
						    			pingResponseHandler(responseMsg);
						    		} else if (commandByte == Vend3.Command.GET_SET_SERIAL_NUMBER.getValue()) {
						    			serialNumberHandler(responseMsg);
						    		} else {
						    			try {
						    				String cmdStatusString = cmdByteStatusCodeDiagString("Unhandled response command byte " + Vend3.Command.getByValue(responseMsg.getCmdByte()), responseMsg);
						    				dumpDataBaseline(cmdStatusString, responseMsg);		
						    			} catch (InvalidByteValueException e) {
						    				log.warn("Command byte not a command: " + StringUtils.toHex(responseMsg.getCmdByte()));
						    			}			
						    		}
						    		if (getVendXTxnManager() != null) {
						    			getVendXTxnManager().messageReceived(responseMsg);
						    		}
					    		} else {
					    			log.warn("Bad crc check");
					    		}
					    		responseMsg = new IdtechVendXDataBlob(EMVTestMain.getDefaultCardReaderType(), EMVTestMain.getDefaultEntryType());
					    	}
					    	buffer[bufferCounter] = c;
					    	
					    	if (dumpingBuffer) {
//						    	bytebuffer[bufferCounter] = (byte)c;
//						    	byte [] tmpBuf = new byte [bufferCounter];
//						    	System.arraycopy(bytebuffer, 0, tmpBuf, 0, bufferCounter);
//						    	System.err.println(StringUtils.toHex(tmpBuf));
					    	}
					    	bufferCounter++;
					    	counter++;
					    	if ((counter % 16) == 0) {
					    		if (dumpingBuffer) {
						    		dumpBufferAsAscii(buffer, bufferCounter);
						    		log.info("");
					    		}
					    		bufferCounter = 0;
					    	}
					    } catch (IOException e) {
					    	log.info("Catching io exception.  Thread will continue, but resetting EMVCardReaderData.");
					    	bufferCounter = 0;
					    	responseMsg = new IdtechVendXDataBlob(EMVTestMain.getDefaultCardReaderType(), EMVTestMain.getDefaultEntryType());
					    	log.info("");
					    	e.printStackTrace();
						} catch (NullPointerException e) {
							log.info("listenAndDump() inner NPE:");
							e.printStackTrace();
						}
					}
				} catch (IOException e) {
					log.info("Catching socket exeption.  Thread will terminate.");
					e.printStackTrace();
				} catch (NullPointerException e) {
					log.info("listenAndDump() NPE:");
					e.printStackTrace();
				} finally {
					log.info("Counter is: " + counter);
				}
			}

			private void applyCardReaderTypeAndEntryType(IdtechVendXDataBlob responseMsg) throws IOException {
				
				if (responseMsg.getEMVData() != null && responseMsg.getEMVData().getTlvData() != null) {
					byte [] trackDataSource = Vend3ARParsing.parseTrackDataSource(responseMsg.getEMVData().getTlvData());
					if (trackDataSource != null && trackDataSource.length > 0) {
						VivotechTrackDataSource dataSource = DEFAULT_VIVOTECH_TRACK_DATA_SOURCE;
						try {
							dataSource = VivotechTrackDataSource.getByValue(trackDataSource[0]);
							responseMsg.setCardReaderType(CardReaderType.IDTECH_VEND3AR);
							responseMsg.setEntryType(dataSource.getEntryType());
						} catch (InvalidByteValueException e) {
							log.info("applyCardReaderTypeAndEntryType(): Defaulting datasource to " + dataSource);
						}
					} else {
						throw new IOException("responseMsg.parseMap() is null");
					}
				}
			}

		});
        listener.start();
        return listener;
	}
	
	private void pingResponseHandler(IdtechVendXDataBlob responseMsg) {
		log.info(cmdByteStatusCodeDiagString("Ping response", responseMsg));
	}

	private void serialNumberHandler(IdtechVendXDataBlob responseMsg) {
		log.info(cmdByteStatusCodeDiagString("Ping response", responseMsg));
		log.info("Serial Number: " + new String(responseMsg.getDataBuffer()));
	}
	
	private void activateTransactionResponseHandler(IdtechVendXDataBlob responseMsg) throws IOException {
		log.info(cmdByteStatusCodeDiagString("Activate Transaction response", responseMsg));
		byte status = responseMsg.getStatusCodeByte();
		String statusCode = StringUtils.toHex(status);
		if (status == Vend3StatusCode.FAILED || status == Vend3StatusCode.TIMEOUT) {
			// failure cases
			dumpFailingStatusCodeData(responseMsg);
		} else if (status == Vend3StatusCode.REQUEST_SIGNATURE) {
			log.warn("Signature requested.  Is vend3 is misconfigured?");
			String cmdStatusString = cmdByteStatusCodeDiagString("Signature requested.  Is vend3 is misconfigured: ", responseMsg);
			log.warn(" Error code: " + StringUtils.toHex(responseMsg.getErrorCodeByte()));
			dumpDataBaseline(cmdStatusString, responseMsg);
		} else if (status == Vend3StatusCode.REQUEST_ONLINE_PIN) {
			log.warn("Online PIN requested.  Is vend3 is misconfigured?");
			String cmdStatusString = cmdByteStatusCodeDiagString("Online PIN requested.  Is vend3 is misconfigured: ", responseMsg);
			log.warn(" Error code: " + StringUtils.toHex(responseMsg.getErrorCodeByte()));
			dumpDataBaseline(cmdStatusString, responseMsg);
		} else if (status == Vend3StatusCode.REQUEST_ONLINE_AUTH_CONTACT_EMV_ONLY) {
			VendXEMVResponseData contactEmvData = IdtechVendXDataBlob.parseIdtechVendIIIContactRequestOnlineAuthEmvData(responseMsg.getRawData());
			responseMsg.setEMVData(contactEmvData);
			String bit55DataSize = "Not available";
			if (responseMsg.getEMVData() != null) {
				bit55DataSize = "" + VendXEMVData.bit55DataSize(responseMsg.getEMVData().getTlvData());
			}
			log.info("Complete CONTACT EMV message.  Cmd byte: " + StringUtils.toHex(responseMsg.getCmdByte()) 
    				+ ", Status Code: " + statusCode + " (" + StringUtils.toHex(responseMsg.getStatusCodeByte()) +"), Bit 55 Chip TLV length: " + bit55DataSize);
		} else if (status == Vend3StatusCode.REQUEST_ONLINE_AUTH || status == Vend3StatusCode.OK) {
    		String errorCode = StringUtils.toHex(responseMsg.getErrorCodeByte());
    		String bit55DataSize = "Not available";
    		VendXEMVResponseData emvData = IdtechVendXDataBlob.parseIdtechVendIIIContactlessEMVData(responseMsg.getDataBuffer());
    		responseMsg.setEMVData(emvData);
    		if (responseMsg.getEMVData().getTlvData() != null) {
    			bit55DataSize = "" + VendXEMVData.bit55DataSize(responseMsg.getEMVData().getTlvData());
    		}
    		log.info("Complete CONTACTLESS EMV message.  Cmd byte: " + StringUtils.toHex(responseMsg.getCmdByte()) 
    				+ ", Status Code: " + statusCode + " (" + StringUtils.toHex(responseMsg.getStatusCodeByte()) +"), Error Code: " + errorCode + ", Bit 55 Chip TLV length: " + bit55DataSize);
    		if (!Vend3.isGoodEmvData(responseMsg.getCmdByte(), responseMsg.getStatusCodeByte())) {
    			log.warn("Status Code Not Good EMV Data: " + statusCode);
//    		} else {
//    			getIdtechBlobList().add(new IdtechVendARDataBlob(CardReaderType.IDTECH_VEND3AR_CONTACTLESS_EMV_MAGSTRIPE, EntryType.EMV_CONTACTLESS, responseMsg.getDataBuffer()));
    		}
		} else {
			String errorCode = StringUtils.toHex(responseMsg.getErrorCodeByte());
    		log.info("Complete message.  Message type UNKNOWN.  Cmd byte: " + StringUtils.toHex(responseMsg.getCmdByte()) 
    				+ ", Status Code: " + statusCode + " (" + StringUtils.toHex(responseMsg.getStatusCodeByte()) +"), Error Code: " + errorCode + ". Not adding to responseList");
		}
	}
	
	///////////////// Diagnostic Utiilties ////////////////////
	private void dumpFailingStatusCodeData(IdtechVendXDataBlob responseMsg) {
		try {
			String cmdStatusString = cmdByteStatusCodeDiagString("Failing status code for " + Vend3.Command.getByValue(responseMsg.getCmdByte()), responseMsg);
			log.warn(" Error code: " + StringUtils.toHex(responseMsg.getErrorCodeByte()));
			dumpDataBaseline(cmdStatusString, responseMsg);
		} catch (InvalidByteValueException e) {
			log.warn("Command byte not a command: " + StringUtils.toHex(responseMsg.getCmdByte()));
		}			
	}
	
	private void dumpDataBaseline(String cmdStatusString, IdtechVendXDataBlob responseMsg) {
		log.info(cmdStatusString);
		log.info("Header - " + StringUtils.toHex(responseMsg.getHeaderBuffer()));
		String dataString = StringUtils.toHex(responseMsg.getDataBuffer());
		if (dataString == null || dataString.length() == 0) {
			dataString = "<No Data>";
		}
		log.info("Data - " + dataString);
		log.info("Crc - " + StringUtils.toHex(responseMsg.getCrcBuffer()));

	}
	
	private String cmdByteStatusCodeDiagString(String prefix, IdtechVendXDataBlob responseMsg) {
//		return "\n" + prefix + " - Cmd Byte: " + StringUtils.toHex(responseMsg.getCmdByte()) + ", Status Code: " + Vend3.StatusCode.getByValue(responseMsg.getStatusCodeByte())  + " (" + StringUtils.toHex(responseMsg.getStatusCodeByte()) + ")";
		return prefix + " - Cmd Byte: " + StringUtils.toHex(responseMsg.getCmdByte()) + ", Status Code: " + StringUtils.toHex(responseMsg.getStatusCodeByte());
	}
	
	public static void dumpBufferAsAscii(int[] buffer, int bufferCounter) {
		for (int ii = 0; ii < bufferCounter; ii++) {
			char value = (char)buffer[ii];
			if (!isAscii(value) || isSpecialCharacter(value)) {
				value = '.';
			}
	    	System.out.print(value);
		}
	}
	
	public static boolean isAscii(char c) {
		boolean result = (c >= 32 && c < 127);
		return result;
	}
	
	public static boolean isSpecialCharacter(char c) {
		boolean result = (c == '\n' || c == '\r' || c == '\t');
		return result;
	}
	

	public Socket getSocket() {
		return socket;
	}

	public String getHost() {
		return host;
	}

	public int getPort() {
		return port;
	}

	public InputStream getInputStream() {
		return inputStream;
	}
	public OutputStream getOutputStream() {
		if (outputStream == null) {
			log.warn("Has  tcp_serial_redirect.py been started? outputStream is null");
		}
		return outputStream;
	}
	
//    public List<IdtechVendXDataBlob> getResponseList() {
//    	return responseList;
//    }

	public List<CardReaderDataBlob> getResponseList() {
		return responseList;
	}
	
	public VendXTxnManager getVendXTxnManager() {
		return vendXTxnManager;
	}
	public void applyVendXTxnManager(VendXTxnManager vendXTxnManager) {
		this.vendXTxnManager = vendXTxnManager;
	}
	public void removeVendXTxnManager() {
		this.vendXTxnManager = null;
	}

	public void addBlobHandler(VendXBlobHandler handler) {
		blobHandlerList.add(handler);
	}
	public void removeBlobHandler(VendXBlobHandler handler) {
		List<VendXBlobHandler>newHandlerList = new ArrayList<>();
		for (VendXBlobHandler h: blobHandlerList) {
			if (!h.equals(handler)) {
				newHandlerList.add(h);
			}
		}
		blobHandlerList = newHandlerList;
	}
	private void invokeBlobHandlerList(IdtechVendXDataBlob responseMsg) {
		for (VendXBlobHandler h: blobHandlerList) {
			h.message(responseMsg);
		}
	}
	
	public static byte [] nullTerminate(byte [] asBytes) {
		byte [] result = new byte[asBytes.length + 1];
		System.arraycopy(asBytes, 0, result, 0, asBytes.length);
		System.arraycopy(NULL_TERM, 0, result, asBytes.length, NULL_TERM.length);
		return result;
	}
	
}
