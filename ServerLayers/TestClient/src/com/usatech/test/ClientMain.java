package com.usatech.test;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.StringTokenizer;

import simple.app.Base;
import simple.app.MainWithConfig;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Log;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
/**
 * Interactive command line interface to send V4.1 message to Netlayer.
 * @author yhe
 *
 */
public class ClientMain extends MainWithConfig {
	private static final Log log = Log.getLog();
	private static PrintWriter pw=new PrintWriter(System.out, true);
	public static String USER_INPUT_DELIM="|";
	private int messageNumber = -1;
	private long messageId = 0;
	private long lastMessageId = 0;
	private String evNumber = null;
	private String encryptKey = null;
	
	private void populateMessageData(BaseMessage msg)
	{
		messageNumber = TestClientUtil.generateMessageNumber(messageNumber);
		msg.setMessageNumber(messageNumber);
	
		lastMessageId = messageId;
		if (msg instanceof V4Message)
			((V4Message) msg).setLastMessageId(lastMessageId);
		
		messageId = TestClientUtil.generateMessageId(messageId);
		msg.setMessageId(messageId);
		
		if (evNumber != null && evNumber.length() == 8)
			msg.setEvNumber(evNumber);
		else
			evNumber = msg.getEvNumber();
		
		if (encryptKey != null && encryptKey.length() == 32)
			msg.setEncryptKey(encryptKey);
		else
			encryptKey = msg.getEncryptKey();
	}
	
	@Override
	protected void registerDefaultCommandLineArguments() {
		registerCommandLineSwitch('P', "propertiesFile", true, true, "properties-file", "The properties file to use");
		registerCommandLineSwitch('h', "host", false, true, "host", "Host to connect to");
		registerCommandLineSwitch('p', "port", false, true, "port", "Port");
		registerCommandLineSwitch('d', "evNumber", true, true, "evNumber", "Device name");
		registerCommandLineSwitch('e', "encryptKey", true, true, "encryptKey", "Device encryption key");
		registerCommandLineSwitch('m', "messageId", true, true, "messageId", "Last Message ID");
		registerCommandLineSwitch('i', "userInput", true, true, "userInput", "A pipe delimited userInput in the format of messageType|arg1|arg2|... to not override default, please use space to replace arg eg. A0|150| | | | | | | | |");
		registerCommandLineSwitch('f', "userInputFile", true, true, "userInputFile", "File path of the a file that includes multiple messages that needs to be sent in one session. Each line is a message in pipe delimited format. eg. /home/user/testclient.test");
		registerCommandLineSwitch('v', "protocol", true, true, "protocol", "Protocol version. Default[7]");
		registerCommandLineSwitch('t', "timeZoneOffset", true, true, "timeZoneOffset", "timeZone offset value in minutes( eg. EDT is -240)");
	}
	@Override
	protected void registerDefaultActions() {
		// no actions
	}
	
	public static ArrayList<StringTokenizer> getMessageTokensFromFile(String filePath) throws FileNotFoundException, IOException{
		ArrayList<StringTokenizer> msgs=new ArrayList<StringTokenizer>(5);
		BufferedReader in= new BufferedReader(new FileReader(filePath));
		String msg;
		while((msg=in.readLine())!=null){
			msgs.add(new StringTokenizer(msg, USER_INPUT_DELIM)); 
		}
		return msgs;

	}
	
	public BaseMessage sendMessageByUserInput(ClientConnectUtil conn,StringTokenizer userInputTokens, BaseMessage previousMessage , long timeZoneOffset) throws Exception{
		BaseMessage sMsg=CommandLineUtil.inputMessageType(userInputTokens.nextToken());
		// sMsg.timeZoneOffset=timeZoneOffset;
		if(sMsg==null){ 
			log.error("Unknown message type.");
		}else{
			sMsg.previousMessage=previousMessage;
			log.info("MessageType:"+sMsg.getMessageTypeName());
			//handle userInput messages
			sMsg.setUserInputTokens(userInputTokens);
			sMsg.getParams();
			populateMessageData(sMsg);
			CommandLineUtil.logSendMessage(sMsg);
			ReRixMessage sentMsg=sMsg.createMessage();
			ReRixMessage responseMsg=conn.sendMessage(sentMsg, sMsg.encryptKey);
			sMsg.logResponse(responseMsg);
			if(sMsg instanceof C8Message){
				handleC8Message((C8Message)sMsg, conn, true);
			}else if (sMsg instanceof N7CMessage){
				handle7COrA4Message((N7CMessage)sMsg, conn, false);
			}
			if(sMsg.hasResponseToServer){
				ReRixMessage sentMsgRep=sMsg.createResponseMessage();
				conn.sendMessageWithNoRead(sentMsgRep, sMsg.encryptKey);
			}
			handleResponse(sMsg, responseMsg, conn, pw);
		}
		
		return sMsg;
	}
	
	@Override
	public void run(String[] args) {
		Map<String,Object> argMap;
		try {
			argMap = parseArguments(args);
		} catch(IOException e) {
			finishAndExit(e.getMessage(), 100, true, null);
			return;
		}
		String host;
		int port;
		int protocol;
		String userInputStr;
		String userInputFile;		
		int commandMode=0;// 0 interactive, 1 userInput, 2, userInputFile
		long timeZoneOffset=TestClientUtil.getServerTimeZoneOffset();
		boolean useDB=false;
		try {
			String proFile=ConvertUtils.getStringSafely(argMap.get("properties-file"));
			useDB=(proFile!=null);
			if(useDB){
				Properties properties;
				try {
					properties = getProperties(proFile, getClass(), null);
				} catch(IOException e) {
					finishAndExit("Could not read properties file", 120, e);
					return;
				}
				for(Map.Entry<String,Object> entry : argMap.entrySet()) {
					properties.put(entry.getKey(), entry.getValue());
				}
				try {
					Base.configureDataSourceFactory(properties, null);
				} catch(ServiceException e) {
					finishAndExit(e.getMessage(), 300, true, null);
					return;
				}
				try {
					Base.configureDataLayer(properties);
				} catch(ServiceException e) {
					finishAndExit(e.getMessage(), 400, true, null);
					return;
				}
			}
			host = ConvertUtils.getStringSafely(argMap.get("host"));
			port = ConvertUtils.getInt(argMap.get("port"));
			protocol = ConvertUtils.getIntSafely(argMap.get("protocol"), 7);
			userInputStr = ConvertUtils.getStringSafely(argMap.get("userInput"));
			userInputFile = ConvertUtils.getStringSafely(argMap.get("userInputFile"));
			evNumber = ConvertUtils.getStringSafely(argMap.get("evNumber"));
			encryptKey = ConvertUtils.getStringSafely(argMap.get("encryptKey"));
			messageId = ConvertUtils.getLongSafely(argMap.get("messageId"), 0);
			String inputTimeOffset=ConvertUtils.getStringSafely(argMap.get("timeZoneOffset"));
			if(inputTimeOffset!=null){
				timeZoneOffset=ConvertUtils.getInt(inputTimeOffset)*60*1000;
			}
			
		} catch(ConvertException e) {
			finishAndExit(e.getMessage(), 200, true, null);
			return;
		} 
		if(userInputStr!=null){
			commandMode=1;
		}else if(userInputFile!=null){
			commandMode=2;
		}
		
		try{
			ClientConnectUtil conn=new ClientConnectUtil(host, port, protocol);
			conn.startCommunication();
			if(commandMode==0){
				BaseMessage previousMessage=null;
				while(true){
					BaseMessage sMsg=CommandLineUtil.inputMessageType();
					// sMsg.useDB=useDB;
					// sMsg.timeZoneOffset=timeZoneOffset;
					if(sMsg==null){ 
						pw.println("Unknown message type.");
						break;
					}
					populateMessageData(sMsg);
					sMsg.previousMessage=previousMessage;
					previousMessage=sMsg;
					sMsg.getParams();
					boolean confirmResult=CommandLineUtil.confirmSendMessage(sMsg);
					if(confirmResult){
						if(protocol==4){
							ReRixMessage sentMsg=sMsg.createMessage();
							ReRixMessage responseMsg=conn.sendMessageV4(sentMsg, sMsg.encryptKey);
							sMsg.readResponse(responseMsg, pw);
							handleResponse(sMsg, responseMsg, conn, pw);	
						}else{
							ReRixMessage sentMsg=sMsg.createMessage();
							ReRixMessage responseMsg=conn.sendMessage(sentMsg, sMsg.encryptKey);
							sMsg.readResponse(responseMsg, pw);
							handleResponse(sMsg, responseMsg, conn, pw);	
						}
					}else{
						pw.println("Message Cancelled.");
					}
					//Add logic for C8Message which will have more C9Message 
					if(sMsg instanceof C8Message){
						handleC8Message((C8Message)sMsg, conn, false);
					}else if (sMsg instanceof N7CMessage){
						handle7COrA4Message((N7CMessage)sMsg, conn, false);
					}
					if(sMsg.hasResponseToServer){
						ReRixMessage sentMsgRep=sMsg.createResponseMessage();
						conn.sendMessageWithNoRead(sentMsgRep, sMsg.encryptKey);
					}
					if(!CommandLineUtil.confirmContinue()){
						break;
					}
				}
			}else{
				if(commandMode==1){
					StringTokenizer userInputTokens=new StringTokenizer(userInputStr, USER_INPUT_DELIM); 
					sendMessageByUserInput(conn, userInputTokens, null, timeZoneOffset);
				}else{
					ArrayList<StringTokenizer> userInputTokensList=getMessageTokensFromFile(userInputFile);
					BaseMessage msg=null;
					for(StringTokenizer userInputTokens:userInputTokensList){
						msg=sendMessageByUserInput(conn, userInputTokens, msg, timeZoneOffset);
					}
				}
			}
			conn.stopCommunication();
			pw.println("Command finished.");
		}catch(NoSuchElementException e){
			pw.println("Pipe delimited user input is missing required arguments. Please use interactive command mode to view the required arguments.");
			finishAndExit(e.getMessage(), 300, true, null);
			return;
		}catch(Exception e){
			pw.println("Command error occurs.");
			e.printStackTrace();
			finishAndExit(e.getMessage(), 300, true, null);
			return;
		}
		
	}
	
	public void handleC8Message(C8Message sMsg,ClientConnectUtil conn, boolean byUserInput) throws Exception{
		BufferedInputStream bi=sMsg.retrieveInputStream();
		for(int i=0;i<sMsg.numOfBlocksInFileTransfer;i++){
			C9Message c9=new C9Message();
			populateMessageData(c9);
			c9.setEvNumber(sMsg.getEvNumber());
			c9.setEncryptKey(sMsg.getEncryptKey());
			c9.blockNum=i;
			long payloadSize = i == sMsg.numOfBlocksInFileTransfer - 1 && sMsg.totalNumOfBytesInFileTransfer % C8Message.MAX_PAYLOAD > 0 
				? sMsg.totalNumOfBytesInFileTransfer % C8Message.MAX_PAYLOAD : C8Message.MAX_PAYLOAD;
			byte[] readBytes=new byte[(int) payloadSize];
			bi.read(readBytes);
			c9.blockPayload=readBytes;
			ReRixMessage sentMsg=c9.createMessage();
			ReRixMessage responseMsg=conn.sendMessage(sentMsg, sMsg.encryptKey);
			if(byUserInput){
				c9.logResponse(responseMsg);
			}else{
				c9.readResponse(responseMsg, pw);
			}
		}
	}
	
	private void handleResponse(BaseMessage sMsg, ReRixMessage responseMsg, ClientConnectUtil conn, Writer writer) throws Exception
	{
		if(sMsg instanceof NetLayerHealthCheck){
			return;
		}
		if(responseMsg==null){
			log.info("No response.");
		}else{
			ByteBuffer bb = ByteBuffer.wrap(responseMsg.getData());
			messageNumber = ProcessingUtils.readByteInt(bb);
			int dataType = ProcessingUtils.readByteInt(bb);
			switch (dataType)
			{
			case 0xC7:
				handleIncomingC7Message(sMsg, bb, conn, pw);
				break;
			case 0xC8:
				handleIncomingC8Message(sMsg, bb, conn, pw);
				break;
			}
		}
	}
	
	private void handleIncomingC7Message(BaseMessage sMsg, ByteBuffer bb, ClientConnectUtil conn, Writer writer) throws Exception
	{	
		messageId = ProcessingUtils.readLongInt(bb);
		Calendar timeOfCreation = ProcessingUtils.readTimestamp(bb);
		long eventId = ProcessingUtils.readLongInt(bb);
		int filesRemaining = ProcessingUtils.readByteInt(bb);
		int fileType = ProcessingUtils.readShortInt(bb);
		String fileName = ProcessingUtils.readShortString(bb, ProcessingConstants.US_ASCII_CHARSET);
		int payloadLength = ProcessingUtils.readShortInt(bb);
		byte[] payload = new byte[payloadLength];
		bb.get(payload);
		File file = new File(fileName);
		BufferedOutputStream bo = null;
		try
		{
			bo = new BufferedOutputStream(new FileOutputStream(file, false));
			bo.write(payload);
			
			CBMessage genericResponse = new CBMessage();
			populateMessageData(genericResponse);			
			ReRixMessage sentMsg = genericResponse.createMessage();
			ReRixMessage responseMsg = conn.sendMessage(sentMsg, sMsg.getEncryptKey());
		}
		catch (Exception e)
		{
			throw e;
		}
		finally
		{
			if (bo != null)
			{
				bo.flush();
				bo.close();
			}			
		}
	}
	
	private void handleIncomingC8Message(BaseMessage sMsg, ByteBuffer bb, ClientConnectUtil conn, Writer writer) throws Exception
	{	
		messageId = ProcessingUtils.readLongInt(bb);
		Calendar timeOfCreation = ProcessingUtils.readTimestamp(bb);
		long eventId = ProcessingUtils.readLongInt(bb);
		int filesRemaining = ProcessingUtils.readByteInt(bb);
		int blocks = ProcessingUtils.readShortInt(bb);
		long bytes = ProcessingUtils.readLongInt(bb);
		int fileType = ProcessingUtils.readShortInt(bb);
		int crcType = ProcessingUtils.readByteInt(bb);
		int crc = ProcessingUtils.readShortInt(bb);
		String fileName = ProcessingUtils.readShortString(bb, ProcessingConstants.US_ASCII_CHARSET);
		long bytesReceived = 0;
		File file = new File(fileName);
		BufferedOutputStream bo = null;
		
		try
		{
			bo = new BufferedOutputStream(new FileOutputStream(file, false));
			
			while (true)
			{			
				CBMessage genericResponse = new CBMessage();
				populateMessageData(genericResponse);			
				ReRixMessage sentMsg = genericResponse.createMessage();
				ReRixMessage responseMsg = conn.sendMessage(sentMsg, sMsg.getEncryptKey());
				
				if (bytesReceived >= bytes)
					break;
				
				ByteBuffer responseBb = ByteBuffer.wrap(responseMsg.getData());
				messageNumber = ProcessingUtils.readByteInt(responseBb);
				int dataType = ProcessingUtils.readByteInt(responseBb);
				if (dataType != 0xC9)
					throw new Exception("Received message type " + (dataType & 0xFF) + " but expected a File Transfer");
				messageId = ProcessingUtils.readLongInt(responseBb);
				int blockNumber = ProcessingUtils.readShortInt(responseBb);
				int payloadLength = ProcessingUtils.readShortInt(responseBb);
				byte[] payload = new byte[payloadLength];
				responseBb.get(payload);
				bo.write(payload);
				bytesReceived += payloadLength;
				writer.write("Received file transfer block # " + blockNumber + "\n");
				writer.flush();
			}
		}
		catch (Exception e)
		{
			throw e;
		}
		finally
		{
			if (bo != null)
			{
				bo.flush();
				bo.close();
			}			
		}
	}
	
	public void handle7COrA4Message(N7CMessage sMsg,ClientConnectUtil conn, boolean byUserInput) throws Exception{
		BufferedInputStream bi=sMsg.retrieveInputStream();
		for(int i=0;i<sMsg.numberOfPacketsInFileTransfer;i++){
			N7EMessage n7E;
			if(sMsg.dataType==0x7C){
				n7E=new N7EMessage();
			}else{
				n7E=new A6Message();
			}
			populateMessageData(n7E);
			n7E.setEvNumber(sMsg.getEvNumber());
			n7E.setEncryptKey(sMsg.getEncryptKey());
			n7E.setFileTransferGroupNumber(sMsg.getFileTransferGroupNumber());
			n7E.packetNumber=i;
			byte[] readBytes=new byte[sMsg.MAX_PAYLOAD];
			int actualRead=bi.read(readBytes, 0, readBytes.length);
			n7E.packetPayload=new String(readBytes, 0, actualRead);
			ReRixMessage sentMsg=n7E.createMessage();
			ReRixMessage responseMsg=conn.sendMessage(sentMsg, sMsg.encryptKey);
			if(byUserInput){
				n7E.logResponse(responseMsg);
			}else{
				n7E.readResponse(responseMsg, pw);
			}
		}
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new ClientMain().run(args);
	}
	
	//Added for silk performer.Art comments below:
	// SilkPerformer does not have the capability to pass in
	// a String Array into a Java object. I broke this up
	// into eight parameters in this special method for Silk.
	public void fromSilk (String s1, String s2, String s3, String s4,
		String s5, String s6, String s7, String s8) {
		 
		// String array of eight strings.
		String[] myArgs = new String[8];
		 
		// Set the array values.
		myArgs[0] = s1;
		myArgs[1] = s2;
		myArgs[2] = s3;
		myArgs[3] = s4;
		myArgs[4] = s5;
		myArgs[5] = s6;
		myArgs[6] = s7;
		myArgs[7] = s8;
		 
		// Call the run method normally.
		run(myArgs);
	}

}
