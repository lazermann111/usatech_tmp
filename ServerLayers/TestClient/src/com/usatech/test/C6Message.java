package com.usatech.test;

import java.nio.ByteBuffer;
import java.util.Calendar;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
/**
 * C6 Generic event --> CB response
 * @author yhe
 *
 */
public class C6Message extends V4Message {
	protected Calendar timeOfEvent;
	protected long timeOfEventUTCTime;
	protected int timeOfEventUTCOffset;
	protected int componentNum;
	protected int eventType;
	protected long eventId;
	protected String eventData;
	public C6Message() {
		super();
		dataType=0xC6;
		timeOfEvent=Calendar.getInstance();
		timeOfEventUTCTime=System.currentTimeMillis();
		timeOfEventUTCOffset=Calendar.getInstance().get(Calendar.ZONE_OFFSET);
		componentNum=0;
		eventType=32769;
		eventId=System.currentTimeMillis()/1000;
		eventData="Test Client event data";
	}
	@Override
	public void getParams() throws Exception{
		super.getParams();
		timeOfEvent=TestClientUtil.getTimeByUTCAndOffset(timeOfEventUTCTime, timeOfEventUTCOffset);
	}
	public int getComponentNum() {
		return componentNum;
	}

	public void setComponentNum(int componentNum) {
		this.componentNum = componentNum;
	}

	public int getEventType() {
		return eventType;
	}

	public void setEventType(int eventType) {
		this.eventType = eventType;
	}

	public String getEventData() {
		return eventData;
	}

	public void setEventData(String eventData) {
		this.eventData = eventData;
	}

	@Override
	public ReRixMessage createMessage() {
		ByteBuffer bb=ByteBuffer.allocate(20+(eventData==null?0:eventData.length()));
		writeMessageHeader(bb);
		ProcessingUtils.writeTimestamp(bb,timeOfEvent);
		ProcessingUtils.writeByteInt(bb,componentNum);
		ProcessingUtils.writeShortInt(bb,eventType);
		ProcessingUtils.writeLongInt(bb,eventId);
		ProcessingUtils.writeLongString(bb,eventData, ProcessingConstants.US_ASCII_CHARSET);
		return writeMessageEnd(bb);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception{
		ClientConnectUtil conn=new ClientConnectUtil();
		conn.setProtocol(7);
		conn.startCommunication();
		C6Message sMsg=new C6Message();
		sMsg.getParams();
		ReRixMessage sentMsg=sMsg.createMessage();
		ReRixMessage responseMsg=conn.sendMessage(sentMsg, sMsg.encryptKey);
		sMsg.readResponse(responseMsg);
		conn.stopCommunication();
	}

	public long getEventId() {
		return eventId;
	}

	public void setEventId(long eventId) {
		this.eventId = eventId;
	}

	public long getTimeOfEventUTCTime() {
		return timeOfEventUTCTime;
	}

	public void setTimeOfEventUTCTime(long timeOfEventUTCTime) {
		this.timeOfEventUTCTime = timeOfEventUTCTime;
	}

	public int getTimeOfEventUTCOffset() {
		return timeOfEventUTCOffset;
	}

	public void setTimeOfEventUTCOffset(int timeOfEventUTCOffset) {
		this.timeOfEventUTCOffset = timeOfEventUTCOffset;
	}

}
