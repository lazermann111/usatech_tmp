package com.usatech.test;

import java.nio.ByteBuffer;

import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
/**
 * 2B local auth v2.0 --> 71h response
 * @author yhe
 *
 */
public abstract class N2BMessageBase extends LegacyMessage {
	protected long transactionId=System.currentTimeMillis()/1000;
	protected long dateAndTime=System.currentTimeMillis()/1000;
	protected int cardType;
	protected int amount;//US dollar in pennies 3 bytes
	protected int tax;// 2 bytes
	protected int numberOfBytesInMagstripe;//no user input, get by creditCardMagstripe string len
	protected String creditCardMagstripe;
	protected int transactionResult;

	public N2BMessageBase(){
		super();
		dataType=0x2B;
		cardType='C';
		amount=150;
		tax=50;
		numberOfBytesInMagstripe=16;
		creditCardMagstripe="0123456789123456";
		transactionResult='S';
	}
	
	public void createMessage2B(ByteBuffer bb){
		ProcessingUtils.writeLongInt(bb,transactionId);
		ProcessingUtils.writeLongInt(bb,dateAndTime);
		ProcessingUtils.writeByteInt(bb,cardType);
		ProcessingUtils.write3ByteInt(bb,amount);
		ProcessingUtils.writeShortInt(bb,tax);
		ProcessingUtils.writeByteInt(bb,numberOfBytesInMagstripe);
		ProcessingUtils.writeString(bb,creditCardMagstripe, ProcessingConstants.US_ASCII_CHARSET);
		ProcessingUtils.writeByteInt(bb,transactionResult);
	}
	
	@Override
	public String readResponseString(ReRixMessage msg)throws InvalidByteValueException{
		return readResponseString71(msg);
	}

	public int getTransactionResult() {
		return transactionResult;
	}

	public void setTransactionResult(int transactionResult) {
		this.transactionResult = transactionResult;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getTax() {
		return tax;
	}

	public void setTax(int tax) {
		this.tax = tax;
	}

	public int getCardType() {
		return cardType;
	}

	public void setCardType(int cardType) {
		this.cardType = cardType;
	}

	public String getCreditCardMagstripe() {
		return creditCardMagstripe;
	}

	public void setCreditCardMagstripe(String creditCardMagstripe) {
		this.creditCardMagstripe = creditCardMagstripe;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
}
