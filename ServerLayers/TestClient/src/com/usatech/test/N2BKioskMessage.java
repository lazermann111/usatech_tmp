package com.usatech.test;

import java.nio.ByteBuffer;
import java.util.ArrayList;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
/**
 * 2B --> 71h response alternate transaction detail for kiosk
 * @author yhe
 *
 */
public class N2BKioskMessage extends N2BMessageBase {
	protected String formatCode;
	protected int itemNumber;//not in spec but needed for input
	protected ArrayList<KioskItem> kioskItem=new ArrayList<KioskItem>(5);

	public N2BKioskMessage(){
		super();
		formatCode="A0";
		itemNumber=1;
	}

	@Override
	public void getParams() throws Exception{
		super.getParams();
		for(int i=0; i<itemNumber; i++){
			KioskItem kItem=new KioskItem(userInputTokens);
			kItem.getParams();
			kioskItem.add(kItem);
		}
	}
	@Override
	public ReRixMessage createMessage() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		createMessage2B(bb);
		// kiosk specific transaction detail
		ProcessingUtils.writeString(bb, KioskItem.createKioskItems(formatCode, kioskItem), ProcessingConstants.US_ASCII_CHARSET);
		return writeMessageEnd(bb);
	}

	public String getFormatCode() {
		return formatCode;
	}

	public void setFormatCode(String formatCode) {
		this.formatCode = formatCode;
	}

	public int getItemNumber() {
		return itemNumber;
	}

	public void setItemNumber(int itemNumber) {
		this.itemNumber = itemNumber;
	}

	public ArrayList<KioskItem> getKioskItem() {
		return kioskItem;
	}

	public void setKioskItem(ArrayList<KioskItem> kioskItem) {
		this.kioskItem = kioskItem;
	}
	
}
