package com.usatech.test;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.AuthResponseCodeC3;
import com.usatech.layers.common.constants.AuthPermissionActionCode;
import com.usatech.layers.common.constants.PermissionResponseCodeC3;
import com.usatech.layers.common.constants.AuthResponseType;
import com.usatech.networklayer.ReRixMessage;
/**
 * Authorization request C2-->C3 response
 * @author yhe
 *
 */
public class C3Message extends V4Message {
	protected AuthResponseType responseType;
	protected SubMessage response;

	public static class AuthResponseControl extends SubMessage {
		protected int actionCode;
		protected int value;
		/**
		 * @param userInputTokens
		 */
		public AuthResponseControl(StringTokenizer userInputTokens) {
			super(userInputTokens);
		}
		/**
		 * @see com.usatech.test.SubMessage#writeMessage(java.nio.ByteBuffer)
		 */
		@Override
		public void writeMessage(ByteBuffer bb) {
		}
		public int getActionCode() {
			return actionCode;
		}
		public void setActionCode(int actionCode) {
			this.actionCode = actionCode;
		}
		public int getValue() {
			return value;
		}
		public void setValue(int value) {
			this.value = value;
		}
	}
	public static class AuthResponseAuthorization extends SubMessage {
		protected AuthResponseCodeC3 responseCode;
		protected BigDecimal balanceAmount;
		protected BigDecimal resultAmount;
		protected String authorityAuthorizationCode;
		protected String resultMessage;

		/**
		 * @param userInputTokens
		 */
		public AuthResponseAuthorization(StringTokenizer userInputTokens) {
			super(userInputTokens);
		}
		/**
		 * @see com.usatech.test.SubMessage#writeMessage(java.nio.ByteBuffer)
		 */
		@Override
		public void writeMessage(ByteBuffer bb) {
		}
		public AuthResponseCodeC3 getResponseCode() {
			return responseCode;
		}
		public void setResponseCode(AuthResponseCodeC3 responseCode) {
			this.responseCode = responseCode;
		}
		public BigDecimal getBalanceAmount() {
			return balanceAmount;
		}
		public void setBalanceAmount(BigDecimal balanceAmount) {
			this.balanceAmount = balanceAmount;
		}
		public BigDecimal getResultAmount() {
			return resultAmount;
		}
		public void setResultAmount(BigDecimal resultAmount) {
			this.resultAmount = resultAmount;
		}
		public String getAuthorityAuthorizationCode() {
			return authorityAuthorizationCode;
		}
		public void setAuthorityAuthorizationCode(String authorityAuthorizationCode) {
			this.authorityAuthorizationCode = authorityAuthorizationCode;
		}
		public String getResultMessage() {
			return resultMessage;
		}
		public void setResultMessage(String resultMessage) {
			this.resultMessage = resultMessage;
		}
	}
	public static class AuthResponsePermission extends SubMessage {
		protected PermissionResponseCodeC3 responseCode;
		protected AuthPermissionActionCode actionCode;
		protected final Set<String> actionCodeBitmap = new HashSet<String>();

		/**
		 * @param userInputTokens
		 */
		public AuthResponsePermission(StringTokenizer userInputTokens) {
			super(userInputTokens);
		}
		/**
		 * @see com.usatech.test.SubMessage#writeMessage(java.nio.ByteBuffer)
		 */
		@Override
		public void writeMessage(ByteBuffer bb) {
		}
		public PermissionResponseCodeC3 getResponseCode() {
			return responseCode;
		}
		public void setResponseCode(PermissionResponseCodeC3 responseCode) {
			this.responseCode = responseCode;
		}
		public AuthPermissionActionCode getActionCode() {
			return actionCode;
		}
		public void setActionCode(AuthPermissionActionCode actionCode) {
			this.actionCode = actionCode;
		}
		public Set<String> getActionCodeBitmap() {
			return actionCodeBitmap;
		}
	}
	public C3Message(){
		super();
		dataType=0xC3;
	}

	/**
	 * @throws InvalidByteValueException
	 * @see com.usatech.test.V4Message#read(java.nio.ByteBuffer)
	 */
	@Override
	protected void read(ByteBuffer data) throws IOException, InvalidByteValueException {
		super.read(data);
		setResponseType(AuthResponseType.getByValue(data.get()));
		SubMessage response = getResponse();
		if(response instanceof AuthResponseControl) {
			AuthResponseControl arc = (AuthResponseControl)response;
			arc.setActionCode(ProcessingUtils.readByteInt(data));
			arc.setValue(ProcessingUtils.readShortInt(data));
		} else if(response instanceof AuthResponseAuthorization) {
			AuthResponseAuthorization ara = (AuthResponseAuthorization)response;
			ara.setResponseCode(AuthResponseCodeC3.getByValue(data.get()));
			ara.setBalanceAmount(ProcessingUtils.readStringAmount(data));
			ara.setResultAmount(ProcessingUtils.readStringAmount(data));
			ara.setAuthorityAuthorizationCode(ProcessingUtils.readShortString(data, ProcessingConstants.US_ASCII_CHARSET));
			ara.setResultMessage(ProcessingUtils.readShortString(data, ProcessingConstants.US_ASCII_CHARSET));
		} else if(response instanceof AuthResponsePermission) {
			AuthResponsePermission arp = (AuthResponsePermission)response;
			arp.setResponseCode(PermissionResponseCodeC3.getByValue(data.get()));
			arp.setActionCode(AuthPermissionActionCode.getByValue(data.get()));
			if(data.hasRemaining()) {
				int bitmap = ProcessingUtils.readShortInt(data);
				for(int i = 0; i < 16; i++) {
					int mask = 1 << i;
					if((bitmap & mask) == mask) {
						arp.getActionCodeBitmap().add(getPermissionBitIndexDesc(i));
					}
				}
			}
		}
	}
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageTypeName())
			.append(": [messageNumber=").append(getMessageNumber())
			.append("; responseType=").append(getResponseType());
		SubMessage response = getResponse();
		if(response instanceof AuthResponseControl) {
			AuthResponseControl arc = (AuthResponseControl)response;
			sb.append(": [actionCode=").append(arc.getActionCode())
				.append("; value=").append(arc.getValue());
		} else if(response instanceof AuthResponseAuthorization) {
			AuthResponseAuthorization ara = (AuthResponseAuthorization)response;
			sb.append(": [responseCode=").append(ara.getResponseCode())
				.append("; balanceAmount=").append(ara.getBalanceAmount())
				.append("; resultAmount=").append(ara.getResultAmount())
				.append("; authorityAuthorizationCode=").append(ara.getAuthorityAuthorizationCode())
				.append("; resultMessage=").append(ara.getResultMessage());
		} else if(response instanceof AuthResponsePermission) {
			AuthResponsePermission arp = (AuthResponsePermission)response;
			sb.append(": [responseCode=").append(arp.getResponseCode())
			.append("; actionCode=").append(arp.getActionCode())
			.append("; actionCodeBitmap=").append(arp.getActionCodeBitmap());
		}
		sb.append("]]");

		return sb.toString();
	}
	/**
	 * @param i
	 * @return
	 */
	protected String getPermissionBitIndexDesc(int i) {
		switch(i) {
			case 0:
				return "Settlement: Fill Card";
			case 1:
				return "Capture DEX";
			case 2:
				return "Call In to Server";
			default:
				return "UNKNOWN (" + i + ")";
		}
	}

	/**
	 * @see com.usatech.test.BaseMessage#createMessage()
	 */
	@Override
	public ReRixMessage createMessage() throws Exception {
		return null;
	}

	public AuthResponseType getResponseType() {
		return responseType;
	}

	public void setResponseType(AuthResponseType responseType) {
		this.responseType = responseType;
		switch(responseType) {
			case CONTROL:
				setResponse(new AuthResponseControl(null));
				break;
			case AUTHORIZATION:
				setResponse(new AuthResponseAuthorization(null));
				break;
			case PERMISSION:
				setResponse(new AuthResponsePermission(null));
				break;
			default:
				setResponse(null);
		}
	}

	public SubMessage getResponse() {
		return response;
	}

	public void setResponse(SubMessage response) {
		this.response = response;
	}
}
