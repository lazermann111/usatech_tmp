package com.usatech.test;
import simple.io.ByteArrayUtils;
import simple.lang.InvalidByteValueException;

import com.usatech.networklayer.ReRixMessage;
/**
 * If you know the hex of the message, use this for testing.
 * 
 select * from engine.machine_cmd_inbound_hist where machine_id='EV035037' and inbound_command like '2E%'
 002E00840000011CDBB5BF23
 * @author yhe
 *
 */
public class HexMessage extends LegacyMessage {
	protected String commandHex;

	public HexMessage() {
		super();
		dataType=0x2E;//fake it
	}
	@Override
	public ReRixMessage createMessage() throws Exception {
		byte[] data= ByteArrayUtils.fromHex(commandHex);
		return new ReRixMessage(data, evNumber);
	}

	public String getCommandHex() {
		return commandHex;
	}
	public void setCommandHex(String commandHex) {
		this.commandHex = commandHex;
	}

	@Override
	public String readResponseString(ReRixMessage msg) throws InvalidByteValueException {
		// TODO Auto-generated method stub
		return null;
	}

}
