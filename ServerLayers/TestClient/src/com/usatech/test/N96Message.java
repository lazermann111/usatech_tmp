package com.usatech.test;

import java.nio.ByteBuffer;
import java.util.ArrayList;

import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
/**
 * 96 Cash sale detail v3.0 -->95
 * @author yhe
 *
 */
public class N96Message extends LegacyMessage {
	protected long cashSaleBatchId;
	protected int numberOfcashSaleVendBlock;//not in spec but need to know how many
	protected ArrayList<CashVendDescriptionBlock> cashVendDescriptionBlock=new ArrayList<CashVendDescriptionBlock>(5) ;
	
	public N96Message(){
		super();
		dataType=0x96;
		cashSaleBatchId=1234567;
		numberOfcashSaleVendBlock=1;
	}
	
	@Override
	public void getParams() throws Exception{
		super.getParams();
		for(int i=0; i<numberOfcashSaleVendBlock; i++){
			CashVendDescriptionBlock cBlock=new CashVendDescriptionBlock(userInputTokens);
			cBlock.getParams();
			cashVendDescriptionBlock.add(cBlock);
		}
	}
	
	@Override
	public ReRixMessage createMessage() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		ProcessingUtils.writeLongInt(bb, cashSaleBatchId);
		for(CashVendDescriptionBlock cBlock: cashVendDescriptionBlock){
			cBlock.writeMessage(bb);
		}
		return writeMessageEnd(bb);
	}

	@Override
	public String readResponseString(ReRixMessage msg) throws InvalidByteValueException {
		return readResponseString95(msg);
	}

	public long getCashSaleBatchId() {
		return cashSaleBatchId;
	}

	public void setCashSaleBatchId(long cashSaleBatchId) {
		this.cashSaleBatchId = cashSaleBatchId;
	}

	public int getNumberOfcashSaleVendBlock() {
		return numberOfcashSaleVendBlock;
	}

	public void setNumberOfcashSaleVendBlock(int numberOfcashSaleVendBlock) {
		this.numberOfcashSaleVendBlock = numberOfcashSaleVendBlock;
	}

	public ArrayList<CashVendDescriptionBlock> getCashVendDescriptionBlock() {
		return cashVendDescriptionBlock;
	}

	public void setCashVendDescriptionBlock(ArrayList<CashVendDescriptionBlock> cashVendDescriptionBlock) {
		this.cashVendDescriptionBlock = cashVendDescriptionBlock;
	}

}
