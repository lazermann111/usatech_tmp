package com.usatech.test;

import java.nio.ByteBuffer;

import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
/**
 * 2E generic data log -->2Fh
 * @author yhe
 *
 */
public class N2EMessage extends LegacyMessage {
	protected String genericData;
	public N2EMessage() {
		super();
		dataType=0x2E;
		genericData="Application specific data parsed by server test";
	}

	@Override
	public ReRixMessage createMessage() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		ProcessingUtils.writeString(bb,genericData, ProcessingConstants.US_ASCII_CHARSET);
		return writeMessageEnd(bb);
	}

	@Override
	public String readResponseString(ReRixMessage msg) throws InvalidByteValueException {
		return readResponseString2F(msg);
	}

	public String getGenericData() {
		return genericData;
	}

	public void setGenericData(String genericData) {
		this.genericData = genericData;
	}

}
