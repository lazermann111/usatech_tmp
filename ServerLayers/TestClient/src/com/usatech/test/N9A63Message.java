package com.usatech.test;

import java.nio.ByteBuffer;
import java.util.ArrayList;

import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.networklayer.ReRixMessage;
/**
 * 9A63 Room Model Info/Layout v2.0 --> 9A46
 * @author yhe
 *
 */
public class N9A63Message extends ESudsMessage {
	protected int numberOfWasherDryerModelInfo;//not in spec but needed for input
	protected ArrayList<WasherDryerModelInfo> washerDryerModelInfo=new ArrayList<WasherDryerModelInfo>(5);
	public N9A63Message() {
		super();
		dataType2=0x63;
		numberOfWasherDryerModelInfo=1;
	}

	@Override
	public void getParams()throws Exception{
		super.getParams();
		for(int i=0; i<numberOfWasherDryerModelInfo; i++){
			WasherDryerModelInfo wdInfo=new WasherDryerModelInfo(userInputTokens);
			wdInfo.getParams();
			washerDryerModelInfo.add(wdInfo);
		}
	}
	@Override
	public ReRixMessage createMessage() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		for(WasherDryerModelInfo wdInfo: washerDryerModelInfo){
			wdInfo.writeMessage(bb);
		}
		return writeMessageEnd(bb);
	}
	
	@Override
	public StringBuilder readResponseHeader(ByteBuffer bb)throws InvalidByteValueException{
		StringBuilder sb=new StringBuilder();
		responseMessageNumber=ProcessingUtils.readByteInt(bb);
		printResponse(sb, "MessageNumber", responseMessageNumber);
		byte[] dataTypes=new byte[2];
		dataTypes[0]=(byte)ProcessingUtils.readByteInt(bb);
		dataTypes[1]=(byte)ProcessingUtils.readByteInt(bb);
		printResponse(sb, "DataType", MessageType.getMessageDescriptionSafely(dataTypes));
		return sb;
	}
	
	@Override
	public String readResponseString(ReRixMessage msg) throws InvalidByteValueException {
		ByteBuffer bb=ByteBuffer.wrap(msg.getData());
		StringBuilder sb=readResponseHeader(bb);
		return sb.toString();
	}

	public ArrayList<WasherDryerModelInfo> getWasherDryerModelInfo() {
		return washerDryerModelInfo;
	}

	public void setWasherDryerModelInfo(ArrayList<WasherDryerModelInfo> washerDryerModelInfo) {
		this.washerDryerModelInfo = washerDryerModelInfo;
	}

}
