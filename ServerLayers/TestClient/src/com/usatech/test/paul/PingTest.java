package com.usatech.test.paul;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.usatech.test.ClientEmulator;

public class PingTest {

	public static void main(String args[]) throws Exception {
		int n = 1;
		ExecutorService threadPool = Executors.newFixedThreadPool(n);
		for(int i=0; i<n; i++) {
			threadPool.submit(new Runnable() {
				@Override
				public void run() {
					while(true) {
						try {
							ClientEmulator.testLBPing();
							Thread.sleep(5);
						} catch (Exception e) {
							e.printStackTrace();
							System.exit(1);
						}
					}
				}
			});
		}
	}
}
