package com.usatech.test;

import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

import simple.io.ByteArrayUtils;

import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.InitializationReason;
import com.usatech.layers.common.constants.SaleType;
import com.usatech.tools.LuhnUtils;

public class DevTest {
	protected static final String CC_DATA = "4011361100000012=180810110000000";
	

	private static int port = 14108;
	private static int authPort = 14109;
	private static int netLayerFormat = 7;
	
	private static String defaultDeviceName = "TD000000";
	private static byte[] defaultEncryptKey = ByteArrayUtils.fromHex("6DF7E5A6FBC84C50A79F76762826C0DC"); // default G9 key

	// LOCAL
//	private static String host = "127.0.0.1";
//	private static String deviceName = "TD001225";
//	private static byte[] encryptKey = ByteArrayUtils.fromHex("524CB8CAD1F525A979B0BB9D3A9F7E4D");
//	private static String serialNumber = "VJ000000400";
//	private static String SP_DATA = "6396210011704726834=9912001610088";
	
	// INT
	private static String host = "intnet11";
	private static String deviceName = "TD001750";
	private static byte[] encryptKey = ByteArrayUtils.fromHex("5C696466B09CD478341F235F60D372B1");
	private static String serialNumber = "VJ000000343";
	private static String SP_DATA = "6396210007419392581=9912006287901";
	
	// ECC
//	private static String host = "eccnet0x-virtual.usatech.com";
//	private static String deviceName = "TD004100";
//	private static byte[] encryptKey = ByteArrayUtils.fromHex("323DC11E233829D45CA86815C6BEED0C");
//	private static String serialNumber = "VJ600000408";

	// PROD
//	private static String host = "usanet31";
//	private static String deviceName = "TD363232";
//	private static byte[] encryptKey = ByteArrayUtils.fromHex("A0D874A56842DF42BAA948CFF3D671FD");
//	private static String serialNumber = "VJ600000001";

//	private static String host = "eccnet11";
//	private static String deviceName = "TD002800";
//	private static byte[] encryptKey = ByteArrayUtils.fromHex("B7A6B5DD45809F7CF9BF677460C08E67");
//	private static String serialNumber = "VJ600000042";

	//	private static int propertyListVersion = 20;

	public static void main(String args[]) throws Exception {
			long transactionId = System.currentTimeMillis() / 1000;
			
			ClientEmulator authClient = new ClientEmulator(host, authPort, deviceName, encryptKey);
			authClient.startCommunication(netLayerFormat);
			authClient.testAuthV4(EntryType.getByValue('S'), "6277229033682500", BigDecimal.valueOf(150), transactionId);
			//authClient.testAuthV4(EntryType.getByValue('S'), CC_DATA, BigDecimal.valueOf(100), transactionId);
			authClient.stopCommunication();
			
//			System.out.println(transactionId);
			
//			TimeUnit.MILLISECONDS.sleep(500);
//			
//			ClientEmulator tranClient = new ClientEmulator(host, port, deviceName, encryptKey);
//			tranClient.startCommunication(netLayerFormat);
////			tranClient.testUpdateStatusRequest();
////			tranClient.testUpdateStatusResponse();
////			tranClient.testSale(transactionId, SaleType.ACTUAL, 3, false);
//			tranClient.testSingleSale(transactionId, SaleType.ACTUAL, BigDecimal.valueOf(200), false);
////			//tranClient.testFailedSale(transactionId, SaleType.ACTUAL);
//			tranClient.stopCommunication();
			
//			TimeUnit.MILLISECONDS.sleep(120000);
//			
//			
//			ClientEmulator ce = new ClientEmulator(host, port, deviceName, encryptKey);
//			ce.startCommunication(netLayerFormat);
////			ce.testInitV4(serialNumber, InitializationReason.NEW_DEVICE, propertyListVersion);
////			ce.testComponents(serialNumber);
//			ce.testUpdateStatusRequest();
//			ce.testUpdateStatusResponse();
//			ce.testModemProperties();
//			ce.testUpdateStatusRequest();
//			ce.testUpdateStatusResponse();
////			ce.testUpdateStatusRequest();
//			//ce.testComponents(serialNumber);
////			ce.testUpdateStatusResponse();
////			ce.testUpdateStatusResponse();
////			ce.testUpdateStatusResponse();
//			ce.stopCommunication();
			
//			ClientEmulator ce = new ClientEmulator(host, port, deviceName, encryptKey);
//			ce.startCommunication(netLayerFormat);
//			ce.testUpdateStatusRequest();
//			ce.testUpdateStatusResponse();
//			ce.testModemProperties();
//			ce.testUpdateStatusResponse();
//			ce.testUpdateStatusResponse();
//			ce.testUpdateStatusResponse();
//			ce.startCommunication(netLayerFormat);
//			ce.testAuthV4(EntryType.getByValue('S'), "123456789", BigDecimal.valueOf(1000), transactionId);
//			ce.testAuthV4(EntryType.getByValue('S'), CC_DATA, BigDecimal.valueOf(1000), transactionId);
//			ce.stopCommunication();
//			ce.startCommunication(netLayerFormat);
//			ce.testCancelSale(transactionId);
//			ce.startCommunication(netLayerFormat);
//			ce.testSale(transactionId, SaleType.ACTUAL, 3, false);
//			ce.stopCommunication();
			
//			ce.testUpdateStatusRequest();
//			ce.testUpdateStatusResponse();
//			ce.testModemProperties();
//			ce.testUpdateStatusRequest();
//			ce.testUpdateStatusResponse();
			

//			ce.sendUpdateStatusResponse();
//			ce.sendUpdateStatusResponse();
//			ce.testComponents(serialNumber);
//			long transactionId = System.currentTimeMillis() / 1000;
//			ce.stopCommunication();
//			ce.setPort(authPort);
//			ce.startCommunication(netLayerFormat);
//			ce.testAuthV4(EntryType.getByValue('S'), CC_DATA, BigDecimal.valueOf(1000), transactionId);
//			ce.stopCommunication();
//			ce.setPort(port);
//			ce.startCommunication(netLayerFormat);
//			ce.testSale(transactionId);
//			ce.testSingleSale(++transactionId, SaleType.CASH, new BigDecimal(100), false);
//			ce.testFillSettlement(++transactionId);
//			ce.testDexFileTransfer(serialNumber, dex);
//			ce.testModemProperties();
//			ce.testGenReqUSR();
//			ce.testFileDownload();
	}
}
