package com.usatech.test;

import java.nio.ByteBuffer;

import simple.lang.InvalidByteValueException;

import com.usatech.networklayer.ReRixMessage;
/**
 * 5C I am live alert -->none
 * @author yhe
 *
 */
public class N5CMessage extends BaseMessage {
	public N5CMessage() {
		super();
		dataType=0x5C;
	}

	@Override
	public ReRixMessage createMessage() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		return writeMessageEnd(bb);
	}

	@Override
	public String readResponseString(ReRixMessage msg) throws InvalidByteValueException {
		return "No Response.\n";
	}

}
