package com.usatech.test;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.ParseException;
import java.util.StringTokenizer;

import com.usatech.layers.common.ProcessingUtils;
/**
 * used by 96
 * @author yhe
 *
 */
public class CashVendDescriptionBlock extends SubMessage {
	protected Long dateTimeOfCashSale=-1L;
	//protected long dateAndTimeOfCashSale=System.currentTimeMillis()/1000;
	protected int amountOfItemVended;
	protected int columnNumberOfCashSale;
	public CashVendDescriptionBlock(StringTokenizer userInputTokens) {
		super(userInputTokens);
		amountOfItemVended=250;
		columnNumberOfCashSale=3;
	}
	@Override
	public void getParams()throws Exception{
		super.getParams();
	}
	@Override
	public void setTimeWithTimeZoneOffset() throws IOException, ParseException{
		if(dateTimeOfCashSale==-1){
			dateTimeOfCashSale=TestClientUtil.getDeviceLocalTimeByOffset(timeZoneOffset)/1000;
		}
	}
	@Override
	public void writeMessage(ByteBuffer bb) {
		ProcessingUtils.writeLongInt(bb,dateTimeOfCashSale);
		ProcessingUtils.write3ByteInt(bb,amountOfItemVended );
		ProcessingUtils.writeByteInt(bb,columnNumberOfCashSale );
	}

	public int getAmountOfItemVended() {
		return amountOfItemVended;
	}

	public void setAmountOfItemVended(int amountOfItemVended) {
		this.amountOfItemVended = amountOfItemVended;
	}

	public int getColumnNumberOfCashSale() {
		return columnNumberOfCashSale;
	}

	public void setColumnNumberOfCashSale(int columnNumberOfCashSale) {
		this.columnNumberOfCashSale = columnNumberOfCashSale;
	}
	public Long getDateTimeOfCashSale() {
		return dateTimeOfCashSale;
	}
	public void setDateTimeOfCashSale(Long dateTimeOfCashSale) {
		this.dateTimeOfCashSale = dateTimeOfCashSale;
	}

}
