package com.usatech.test;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;

import simple.app.Base;
import simple.app.MainWithConfig;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.event.TaskListener;
import simple.event.WriteCSVTaskListener;
import simple.io.ByteArrayUtils;
import simple.io.Log;
import simple.io.TLVParser;
import simple.lang.InvalidValueException;
import simple.results.Results;
import simple.text.StringUtils;

import com.usatech.layers.common.constants.AuthResponseCodeC3;
import com.usatech.layers.common.constants.AuthResponseType;
import com.usatech.layers.common.constants.AuthorityAction;
import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.FileType;
import com.usatech.layers.common.constants.GenericRequestType;
import com.usatech.layers.common.constants.PermissionResponseCodeC3;
import com.usatech.layers.common.constants.TLVTag;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageData_C2;
import com.usatech.layers.common.messagedata.MessageData_C2.GenericRawCardReader;
import com.usatech.layers.common.messagedata.MessageData_C2.GenericTracksCardReader;
import com.usatech.layers.common.messagedata.MessageData_C3;
import com.usatech.layers.common.messagedata.MessageData_C7;
import com.usatech.layers.common.messagedata.MessageData_CA;
import com.usatech.layers.common.messagedata.MessageData_CA.UpdateStatusRequest;
import com.usatech.layers.common.util.Vend3ARParsing;
import com.usatech.layers.common.util.Vend3ARTransactionResponseData;
import com.usatech.layers.common.util.VendXEMVData;
import com.usatech.layers.common.util.VendXParsing;

public class EMVTestMain extends MainWithConfig {
	private static final Log log = Log.getLog();
    
	public static String LOCAL_TEST_DATA = "test-data/"; 
	
	public static String AMEX_CARD_SWIPE_AES = LOCAL_TEST_DATA + "Amex-card-Swipe-ENCRYPTION-AES-docklight-rev3-NEO-v1.00.040-20151221-nospace-ascii.txt";
	public static String VISA_CDET_TAPPED_AES = LOCAL_TEST_DATA + "Visa-CDET-Test-Card-#13-(ver-2.1,-qVSDC)-Tapped,-ENCRYPTION-AES-(docklight-rev3),-NEO-v1.00.040,-20160105-nospace-ascii.txt";
	public static String MC_PAYPASS_TAPPED_AES = LOCAL_TEST_DATA + "UL-BCT,-MC-Contactless_SS8M_MP_91_V2_2,-ENCRYPTION-AES-(docklight-rev3),-NEO-v1.00.040,-20160108-nospace-ascii.bin";
	public static String MC_CONTACTLESS_COMBO_TAPPED_AES = LOCAL_TEST_DATA + "UL-BCT,-MC-Contactless_SS8M_Combo_01_V1_1,-ENCRYPTION-AES-(docklight-rev3),-NEO-v1.00.040,-20160108-nospace-ascii.bin";
	
	public static String MC_CONTACTLESS_COMBO_AES_2016_01_22 = LOCAL_TEST_DATA + "UL-BCT, MC Contactless_SS8M_Combo_01_V1_1, ENCRYPTION AES (docklight rev3) Tag FFFC0100 , NEO v1.00.040, 20160122 copy-used-to-work.ascii.txt";
	public static String VENDI_REV_46_2016_01_28 = LOCAL_TEST_DATA + "Vendi rev 046, encryption enabled, result of activate transaction, 2016-01-27_171150-TEST_KEY.txt";
	
	private static final String SAVED_DATA_PATHNAME = LOCAL_TEST_DATA + "saved-data/";
	
	private static final FilenameFilter standardFilter = (dir, name) -> !name.equals(".DS_Store") && !name.equals("CVS") && !name.equals("archive");

	private static CardReaderType defaultCardReaderType = null;
	private static EntryType defaultEntryType = null;

	private static Vend3 vend3Reader;
	private static VendXTxnManager vendXTxnManager;

    private static String[] thisArgs;
    
    private static Thread socketListener;
    private static TLVParser TLV_PARSER = new TLVParser();
    
    private static List<EMVCAKeyComponents>keyList = new ArrayList<>();
    
    private interface ReplTask { public void saMethod() throws IOException, SQLException, DataLayerException, ConvertException, InvalidValueException; }
    private static class ReplTaskDef {
    	private Integer index;
    	private String displayText;
    	private ReplTask sam;
    	
    	public ReplTaskDef(Integer index, String displayText, ReplTask sam) {
    		this.index = index;
    		this.displayText = displayText;
    		this.sam = sam;
    	}

    	public Integer getIndex() {
    		return index;
    	}
    	
		public String getDisplayText() {
			return displayText;
		}

		public ReplTask getSam() {
			return sam;
		}
    }
    
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		thisArgs = args;
		// Creating and issuing an EMV message
		setDefaultCardReaderType(CardReaderType.IDTECH_VEND3AR);
		setDefaultEntryType(EntryType.EMV_CONTACTLESS);
		
		// remember to start /Users/jscarpaci/Documents/USATech/emv/vendIII/scripts/tcp-serial-startup.sh
		// to handle serial i/o and present it as a tcp/ip socket for this program 
		// 
		String hostname = "localhost";
		int port = 5555;
		EMVTestMain.vend3Reader = new Vend3(hostname, port);
		try {
			boolean opened = getVend3Reader().open();
			if (opened) {
				socketListener = getVend3Reader().listenAndDump();
				System.out.println("Listening on hostname: " + hostname + ", port: " + port);
			} else {
				System.out.println("Unable to open connection to reader.  Start tcp_serial_redirect.py and run 98 for connection.");
			}
			replLoop(getVend3Reader());
			getVend3Reader().close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static Map<Integer, ReplTaskDef>listToMap(List<ReplTaskDef>replTaskList) {
		Map<Integer, ReplTaskDef>map = new HashMap<>();
		for (ReplTaskDef task: replTaskList) {
			map.put(task.getIndex(), task);
		}
		return map;
	}
	
	private static void baseReplLoop(Scanner scanner, String title, List<ReplTaskDef>replTaskList) {
		Map<Integer, ReplTaskDef>map = listToMap(replTaskList);
		boolean done = false;
		do {
			System.out.println(title);
			for (ReplTaskDef task: replTaskList) {
				System.out.println(task.getDisplayText());
			}
			String keyed = scanner.next();
			try {
				Integer cmdValue = new Integer(keyed);
				ReplTaskDef task = map.get(cmdValue);
				if (task != null) {
					if (cmdValue.equals(0)) {
						System.out.println(title + " is done!");
						done = true;
					} else {
						System.out.println("Keyed value: " + cmdValue + ", executing '" + task.getDisplayText() + "'...");
						task.getSam().saMethod();
					}
				} else {
					System.out.println(cmdValue + " is not one of the options. Please try again.");
				}
			} catch (Exception e) {
				System.err.println("Exception thrown: " + e);
			}
		} while (!done);
		
	}
	
	private static void replLoopVendiTest(Scanner scanner, Vend3 reader) {
		List<ReplTaskDef>replTaskList = new ArrayList<ReplTaskDef>();
		replTaskList.add(new ReplTaskDef(0, "0) Exit VENDI Repl", () -> System.out.println("Done VENDI Test Repl!")));
		replTaskList.add(new ReplTaskDef(1, "1) Send Visa CDET Tapped AES Encrypted Test", () -> sendVendiVisaCDETTapped()));
		replTaskList.add(new ReplTaskDef(2, "2) Send Amex Swiped AES Encryption Test", () -> sendAmexSwiped()));
		replTaskList.add(new ReplTaskDef(3, "3) Send MC Paypass AES Encryption Test (MC-Contactless_SS8M_MP_91_V2_2)", () -> sendMasterCardPaypass()));
		replTaskList.add(new ReplTaskDef(4, "4) Send MC Combo AES Encryption Test (MC-Contactless_SS8M_Combo_01_V1_1)", () -> sendMasterCardCombo()));
		replTaskList.add(new ReplTaskDef(5, "5) Send Jan 22 MC Combo AES Encryption Test (MC-Contactless_SS8M_Combo_01_V1_1)", () -> sendMasterCardComboJan22()));
		replTaskList.add(new ReplTaskDef(6, "6) Send Vendi Rev 046 2016-01-27_171150 TEST_KEY", () -> sendVendiRev46()));
		replTaskList.add(new ReplTaskDef(98, "98) Attempt reader connect.  Make sure tcp_serial_redirect.py is running.", () -> reader.open()));
		replTaskList.add(new ReplTaskDef(99, "99) List Options", () -> doNothing()));
		
		baseReplLoop(scanner, "Vendi Test Repl", replTaskList);
	}
	
	
	private static void replLoopVend3(Scanner scanner, Vend3 reader) {
		List<ReplTaskDef>replTaskList = new ArrayList<ReplTaskDef>();
		String title = "Vend3 Test Repl";
		replTaskList.add(new ReplTaskDef(0, "0) Exit", () -> System.out.println(title)));
		replTaskList.add(new ReplTaskDef(1, "1) Ping reader", () -> reader.sendPing()));
		replTaskList.add(new ReplTaskDef(2, "2) Activate Transaction", () -> reader.sendActivateTransaction()));
		replTaskList.add(new ReplTaskDef(3, "3) Set EMV For Visa and MC", () -> reader.sendSetEMVForVisaAndMastercard()));
		replTaskList.add(new ReplTaskDef(4, "4) Burst Mode Enable", () -> reader.sendBurstModeEnable()));
		replTaskList.add(new ReplTaskDef(5, "5) Burst Mode Disable", () -> reader.sendBurstModeDisable()));
		replTaskList.add(new ReplTaskDef(6, "6) Set Poll Mode Auto Poll", () -> reader.sendSetPollModeAutoPoll()));
		replTaskList.add(new ReplTaskDef(7, "7) Set Poll Mode On Demand", () -> reader.sendSetPollModeOnDemand()));
		replTaskList.add(new ReplTaskDef(8, "8) Reset All Parameters", () -> reader.sendResetToDefault()));
		replTaskList.add(new ReplTaskDef(9, "9) Restore Default Config with LCD", () -> reader.sendRestoreDefaultConfigurationWithLCD()));
		replTaskList.add(new ReplTaskDef(10, "10) Disable GR Backward Compatibility", () -> reader.sendDisableGRBackwardCompat()));
		replTaskList.add(new ReplTaskDef(11, "11) Enable CT", () -> reader.sendEnableCT()));
		replTaskList.add(new ReplTaskDef(12, "12) Activate Txn 2", () -> sendActTransactionWithAmount(scanner, reader)));
		replTaskList.add(new ReplTaskDef(13, "13) Cancel Txn", () -> reader.sendCancelTransaction()));
		replTaskList.add(new ReplTaskDef(14, "14) Continue Txn For Cardholder Verification (request signature or PIN)", () -> reader.sendContinueTransactionForCardholderVerification()));
		replTaskList.add(new ReplTaskDef(15, "15) sendContinueTxnForOnlineAuthorizationSuccess Continue CT Txn for Online Authorization", () -> reader.sendContinueTxnForOnlineAuthorizationSuccess()));
		replTaskList.add(new ReplTaskDef(16, "16) SetEMVForAmex", () -> reader.sendSetEMVForAmex()));
		
		
		replTaskList.add(new ReplTaskDef(20, "20) Get Firmware Version", () -> reader.getFirmwareVersion()));
		replTaskList.add(new ReplTaskDef(21, "21) Get Module Version Full", () -> reader.getModuleVersionFull()));
		replTaskList.add(new ReplTaskDef(22, "22) Get Bootloader Version", () -> reader.getBootloaderVersion()));
		replTaskList.add(new ReplTaskDef(23, "23) Get Serial Number", () -> reader.getSerialNumber()));
		replTaskList.add(new ReplTaskDef(25, "25) Get Configuration", () -> reader.getConfiguration()));
		replTaskList.add(new ReplTaskDef(26, "26) Get All Groups", () -> reader.getAllGroups()));
		
		replTaskList.add(new ReplTaskDef(30, "30) Vend3 Config AIDs and CA Public Keys etc. Repl", () -> replLoopVend3Config(scanner, reader)));
		replTaskList.add(new ReplTaskDef(50, "50) Execute Authorization for item in responseList (ClientEmulator)", () -> executeAuthorizeForResponseListMsg(scanner)));
		replTaskList.add(new ReplTaskDef(51, "51) Execute Auth & Sale for item in responseList (Quick Connect)", () -> executeAuthorizeAndSaleQuickConnectForResponseListMsg(scanner)));
		replTaskList.add(new ReplTaskDef(52, "52) Parse and dump responseList item", () -> parseAndDumpResponseList(scanner)));
		
		replTaskList.add(new ReplTaskDef(60, "60) Hex Dump responseList", () -> dumpResponseList()));
		replTaskList.add(new ReplTaskDef(61, "61) Clear responseList", () -> emptyResponseList()));
		replTaskList.add(new ReplTaskDef(98, "98) Attempt reader connect.  Make sure tcp_serial_redirect.py is running.", () -> reader.open()));
		replTaskList.add(new ReplTaskDef(99, "99) List Options", () -> doNothing()));
		baseReplLoop(scanner, title, replTaskList);

	}
	
	private static void replLoopVend3Config(Scanner scanner, Vend3 reader) {
		List<ReplTaskDef>replTaskList = new ArrayList<ReplTaskDef>();
		String title = "Vend3 Config AIDs and CA Public Keys etc. Repl";
		replTaskList.add(new ReplTaskDef(0, "0) Exit", () -> System.out.println(title)));
		replTaskList.add(new ReplTaskDef(1, "1) Get All AIDs", () -> reader.getAllAIDs()));

		replTaskList.add(new ReplTaskDef(2, "2) Set AMEX CA Public Key Contactless", () -> reader.setCAPublicKeyAmexContactless()));
		replTaskList.add(new ReplTaskDef(3, "3) Set AMEX CA Public Key Contact", () -> reader.setCAPublicKeyAmexContact()));

		replTaskList.add(new ReplTaskDef(4, "4) List Amex Keys Contactless", () -> reader.sendListAmexKeysContactless()));
		replTaskList.add(new ReplTaskDef(5, "5) List Amex Keys Contact", () -> reader.sendListAmexKeysContact()));
		replTaskList.add(new ReplTaskDef(6, "6) Set Configurable AMEX Contact AID", () -> reader.sendSetConfigurableAmexContactAID()));
		replTaskList.add(new ReplTaskDef(7, "7) Set Configurable Discover Contact AID", () -> reader.sendSetConfigurableDiscoverContactAID()));
		replTaskList.add(new ReplTaskDef(8, "8) Set Configurable Discover Contactless AID", () -> reader.sendSetConfigurableDiscoverContactlessAID()));
		
		replTaskList.add(new ReplTaskDef(9, "9) List Discover Keys", () -> reader.sendListDiscoverKeys()));
		replTaskList.add(new ReplTaskDef(10, "10) List Discover Contact Keys", () -> reader.sendListDiscoverContactKeys()));
		replTaskList.add(new ReplTaskDef(11, "11) List Discover Contactless Keys", () -> reader.sendListDiscoverContactlessKeys()));
		// same as 7) Set Configurable Discover Contact AID", () -> reader.sendSetConfigurableDiscoverContactAID()
//		replTaskList.add(new ReplTaskDef(12, "12) Create Discover Contact AID", () -> reader.sendCreateDiscoverContactAID()));
		
		replTaskList.add(new ReplTaskDef(20, "20) Get All CA Public Key RIDs", () -> reader.sendGetAllCAPublicKeyRIDs()));
		replTaskList.add(new ReplTaskDef(21, "21) Load EMV param download file to keyList", () -> loadEMVParamsToKeyList()));
		replTaskList.add(new ReplTaskDef(22, "22) List EMV param keyList", () -> listEMVParamKeyList()));
		replTaskList.add(new ReplTaskDef(23, "23) Apply CA Public Key keyList item to Vend3 reader", () -> applyCAPublicKeyListItem(scanner, reader)));
		replTaskList.add(new ReplTaskDef(24, "24) Create new Group 09", () -> reader.sendCreateNewGroup09()));
		
		baseReplLoop(scanner, title, replTaskList);
	}
	
	private static void replLoopVend3Display(Scanner scanner, Vend3 reader) {
		List<ReplTaskDef>replTaskList = new ArrayList<ReplTaskDef>();
		String title = "Vend3 Display Repl";
		replTaskList.add(new ReplTaskDef(0, "0) Exit", () -> System.out.println(title)));
		replTaskList.add(new ReplTaskDef(1, "1) Start Custom Display Mode", () -> reader.startCustomDisplayMode()));
		replTaskList.add(new ReplTaskDef(2, "2) Stop Custom Display Mode", () -> reader.stopCustomDisplayMode()));
		replTaskList.add(new ReplTaskDef(3, "3) List Directory", () -> reader.listDirectory()));
		replTaskList.add(new ReplTaskDef(4, "4) Get Drive Free Space", () -> reader.getDriveFreeSpace()));
		replTaskList.add(new ReplTaskDef(5, "5) Transfer " + Vend3.DEST_FILENAME, () -> reader.fileTransfer()));
		replTaskList.add(new ReplTaskDef(6, "6) Display Image " + Vend3.DEST_FILENAME, () -> reader.displayImage()));
		baseReplLoop(scanner, title, replTaskList);
	}

	private static void replLoopVend3AppleVAS(Scanner scanner, Vend3 reader) {
		List<ReplTaskDef>replTaskList = new ArrayList<ReplTaskDef>();
		String title = "Vend3 Apple VAS Repl";
		replTaskList.add(new ReplTaskDef(0, "0) Exit", () -> System.out.println(title)));
		replTaskList.add(new ReplTaskDef(1, "1) Reset All Parameters", () -> reader.sendResetToDefault()));
		replTaskList.add(new ReplTaskDef(2, "2) ACT No Optional Parms Apple VAS", () -> reader.sendActAppleVASNoOptional()));
		replTaskList.add(new ReplTaskDef(3, "3) ACT With Optional Parms Apple VAS", () -> reader.sendActAppleVASWithOptionalParms()));
		replTaskList.add(new ReplTaskDef(4, "4) Get Apple VAS Merchant 1", () -> reader.sendGetAppleVASMerchant1()));
		replTaskList.add(new ReplTaskDef(5, "5) Get Apple VAS Merchant 2", () -> reader.sendGetAppleVASMerchant2()));
		replTaskList.add(new ReplTaskDef(6, "6) Clear Apple VAS Merchant Record 1", () -> reader.sendClearAppleVASMerchantRecord1()));
		replTaskList.add(new ReplTaskDef(7, "7) Clear Apple VAS Merchant Record 2", () -> reader.sendClearAppleVASMerchantRecord2()));
		replTaskList.add(new ReplTaskDef(8, "8) Set Apple VAS Merchant Record 1 VAS Prod", () -> reader.sendSetAppleVASMerchantRecord1VASProd()));
		replTaskList.add(new ReplTaskDef(9, "9) Set Apple VAS Merchant Record 6 VAS Prod", () -> reader.sendSetAppleVASMerchantRecord6VASProd()));
		replTaskList.add(new ReplTaskDef(10, "10) Set Apple VAS USAT Merchant Record 1", () -> reader.sendSetAppleVASUSATMerchantRecord1()));
		replTaskList.add(new ReplTaskDef(11, "11) Set Apple VAS USAT Merchant Record 2", () -> reader.sendSetAppleVASUSATMerchantRecord2()));
		replTaskList.add(new ReplTaskDef(99, "99) List Options", () -> doNothing()));
		baseReplLoop(scanner, title, replTaskList);
	}
	
	public static void miscReplLoop(Scanner scanner, Vend3 reader) {
		List<ReplTaskDef>replTaskList = new ArrayList<ReplTaskDef>();
		String title = "Misc Test Repl";
		replTaskList.add(new ReplTaskDef(0, "0) Exit", () -> System.out.println(title)));
		replTaskList.add(new ReplTaskDef(1, "1) Dump EMV Parameter file", () -> dumpEMVParameterFile()));
		replTaskList.add(new ReplTaskDef(70, "70) Dump all TLVs for List of Responses", () -> dumpAllTlvsForList()));
		replTaskList.add(new ReplTaskDef(72, "72) Dump track data for List of Responses", () -> dumpTrackDataForList()));
		replTaskList.add(new ReplTaskDef(80, "80) Send gdi file to server", () -> sendGdiFileToServer()));
		replTaskList.add(new ReplTaskDef(81, "81) Send process updates request from TD000925 to server", () -> sendProcessUpdatesRequest()));
		replTaskList.add(new ReplTaskDef(90, "90) Test track two data and others", () -> testTrackTwoDataAndOthers()));
		replTaskList.add(new ReplTaskDef(91, "91) Test tlv parse", () -> testTlvParse()));
		replTaskList.add(new ReplTaskDef(92, "92) Test tlv reassembly", () -> testTlvReassembly()));
		replTaskList.add(new ReplTaskDef(93, "93) Test testAssembleBit55TlvData", () -> testAssembleBit55TlvData()));
		replTaskList.add(new ReplTaskDef(98, "98) Attempt reader connect.  Make sure tcp_serial_redirect.py is running.", () -> reader.open()));
		replTaskList.add(new ReplTaskDef(99, "99) List Options", () -> doNothing()));
		baseReplLoop(scanner, title, replTaskList);
	}
	
	public static void cardDataStorageReplLoop(Scanner scanner, Vend3 reader) {
		List<ReplTaskDef>replTaskList = new ArrayList<ReplTaskDef>();
		replTaskList.add(new ReplTaskDef(0, "0) Exit", () -> System.out.println("Done Test Repl!")));
		replTaskList.add(new ReplTaskDef(1, "1) Save file from responseList", () -> saveAFileInteraction(scanner)));
		replTaskList.add(new ReplTaskDef(2, "2) Load file into responseList from " + SAVED_DATA_PATHNAME, () -> readAFileInteraction(scanner)));
		replTaskList.add(new ReplTaskDef(3, "3) List saved transaction data at " + SAVED_DATA_PATHNAME, () -> listSavedTransactionData()));
		replTaskList.add(new ReplTaskDef(6, "6) Parse and dump responseList item", () -> parseAndDumpResponseList(scanner)));
		replTaskList.add(new ReplTaskDef(7, "7) List items in responseList", () -> listResponseList()));
		replTaskList.add(new ReplTaskDef(8, "8) Hex Dump responseList", () -> dumpResponseList()));
		replTaskList.add(new ReplTaskDef(9, "9) Clear responseList", () -> emptyResponseList()));
		replTaskList.add(new ReplTaskDef(20, "20) Adjust for EMV_Tool_20160204.exe Vendi data difference", () -> adjustForEMVTOOL20160412DifferenceForVendi(scanner)));
		replTaskList.add(new ReplTaskDef(21, "21) Adjust for OTIDukpt tool response output formatting", () -> adjustOTIDukptToolResponseOutput(scanner)));
		replTaskList.add(new ReplTaskDef(30, "30) TLV Parse arbitrary hex string", () -> tlvParseArbitraryHexString(scanner)));
		replTaskList.add(new ReplTaskDef(40, "40) Load EMV param download file to keyList", () -> loadEMVParamsToKeyList()));
		replTaskList.add(new ReplTaskDef(41, "41) List EMV param keyList", () -> listEMVParamKeyList()));
		replTaskList.add(new ReplTaskDef(42, "42) Apply keyList item to Vend3 reader", () -> applyCAPublicKeyListItem(scanner, reader)));

		replTaskList.add(new ReplTaskDef(50, "50) Execute Authorization for item in responseList (ClientEmulator)", () -> executeAuthorizeForResponseListMsg(scanner)));
		replTaskList.add(new ReplTaskDef(51, "51) Execute Authorization for item in responseList (Quick Connect)", () -> executeAuthorizeAndSaleQuickConnectForResponseListMsg(scanner)));
		replTaskList.add(new ReplTaskDef(52, "52) Execute Auth & Sale for VendIII Readiness Test Data", () -> executeAuthorizationAndSaleViaQuickConnectVendIIIReadinessTestData()));
		replTaskList.add(new ReplTaskDef(77, "77) Task at moment", () -> taskAtMoment()));
		replTaskList.add(new ReplTaskDef(99, "99) List Options", () -> doNothing()));
		baseReplLoop(scanner, "Card Data Storage Repl", replTaskList);
	}

	public static void replLoop(Vend3 reader) {
		Scanner scanner = new Scanner(System.in);

		List<ReplTaskDef>replTaskList = new ArrayList<ReplTaskDef>();
		replTaskList.add(new ReplTaskDef(0, "0) Exit", () -> System.out.println("Done Test Repl!")));
		replTaskList.add(new ReplTaskDef(1, "1) Card Data Storage Repl", () -> cardDataStorageReplLoop(scanner, reader)));
		replTaskList.add(new ReplTaskDef(2, "2) Vendi -> replLoopVendiTest()", () -> replLoopVendiTest(scanner, reader)));
		replTaskList.add(new ReplTaskDef(3, "3) Vend3 -> replLoopVend3()", () -> replLoopVend3(scanner, reader)));
		replTaskList.add(new ReplTaskDef(4, "4) Start Vend3 Transaction Manager", () -> startTransactionManagerForVend3()));
		replTaskList.add(new ReplTaskDef(5, "5) Stop Vend3 Transaction Manager", () -> stopTransactionManagerForVend3()));
		replTaskList.add(new ReplTaskDef(6, "6) Inject responseList item into Vend3 Transaction Manager", () -> injectResponseListItemIntoTxnManager(scanner)));
		replTaskList.add(new ReplTaskDef(7, "7) Change Default CardReaderType and EntryType", () -> changeCardReaderTypeAndEntryType(scanner)));
		replTaskList.add(new ReplTaskDef(8, "8) Load file into responseList from " + SAVED_DATA_PATHNAME, () -> readAFileInteraction(scanner)));		
		replTaskList.add(new ReplTaskDef(9, "9) Misc -> miscReplLoop()", () -> miscReplLoop(scanner, reader)));
		replTaskList.add(new ReplTaskDef(10, "10) Vend 3 Display Manipulation", () -> replLoopVend3Display(scanner, reader)));
		replTaskList.add(new ReplTaskDef(11, "11) Vend 3 Apple VAS", () -> replLoopVend3AppleVAS(scanner, reader)));
		replTaskList.add(new ReplTaskDef(99, "99) List Options", () -> doNothing()));
		baseReplLoop(scanner, "Main Test Repl", replTaskList);
		System.out.println("Program terminating.");
		if (scanner != null) {
			scanner.close();
		}
	}

	
//	private static Object taskAtMoment() {
////		String string = "5669564f7465636832000300025f03ffee120a629949012c0004600002000000500c436f6d626f3031207631203157c1203acada6d38821c1bd590e544a78519590433b3b192750f98df58d752715c482e5aa1085413cccccccc40125ac1104c06e517e06c4ba59f904c2bcb01cad4820219808407a0000000041010950504000000009a031408109c01005f24032512315f25030401015f2a0208405f3401019f02060000000000019f03060000000000009f0607a00000000410109f0702ffc09f090200029f0d0500000000009f0e0500000000009f0f0500000000009f10120110900009248000000000000000000001ff9f1a0208409f1e0830303030303030309f21031240039f26089c1b202a54ffa5909f2701409f33030008e89f34031f03029f3501259f3602000f9f370415f02f929f3901919f420209789f5301009f5d030105009f6d0200019f6e050056818200df76050400000000df81290810f0f00030f00d00ff8106179f42020978df8115060000000000ff9f6e050056818200ff810581e59f02060000000000019f03060000000000009f26089c1b202a54ffa5905f240325123182021980500c436f6d626f303120763120315aa1085413cccccccc40125ac1104c06e517e06c4ba59f904c2bcb01cad45f3401019f3602000f9f090200029f2701409f34031f03028407a00000000410109f1e0830303030303030309f10120110900009248000000000000000000001ff9f33030008e89f1a0208409f3501259505040000000057c1203acada6d38821c1bd590e544a78519590433b3b192750f98df58d752715c482e9f5301005f2a0208409a031408109c01009f370415f02f92410d";
//		String string = "5669564f7465636832000323023a03ffee120a629949012c000460000c000000500c436f6d626f3031207631203156c1409b005ff737715a5b41983366a6b2dfb5cd932d9908603b4b5a6ee32032ca8a6cc31bb61267593df046acdbdbb4b81e19ac5e963f495749315868e1dc02b7621c820219808407a0000000041010950500000000009a031601099c01005f2a0208409f02060000000000019f03060000000000009f0607a00000000410109f090200029f1a0208409f1e0830303030303030309f21030038469f33030000e89f34030000009f3501259f3602000f9f3704f3b686cc9f3901919f5301009f5d030105009f6bc1208af15a951a407a3ede4e90e136194d09b81eec5076df053a1f353a7892aa85599f6d0200019f6e050056818200df81290830f0f00030f00d00ff81064cdf812ac120d7d807ea92e4eb7ebb599ae37cafadcfe8c89a5b484049375b568d02c21049e9df812bc110ec1dc92ed41fa47c8d87f8a8506e72c5df8115060000000000ff9f6e050056818200ff81058183500c436f6d626f303120763120318407a00000000410109f6d02000156c1409b005ff737715a5b41983366a6b2dfb5cd932d9908603b4b5a6ee32032ca8a6cc31bb61267593df046acdbdbb4b81e19ac5e963f495749315868e1dc02b7621c9f6bc1208af15a951a407a3ede4e90e136194d09b81eec5076df053a1f353a7892aa8559ffee013cdf300100df31c120f31b4e6febc610133443b981adc64d88d91cc595ac11e3337c405672432f9b4bdf32c110e26db5459612f8f8bd92c7c5fbf4581845e0";
//		StringBuffer sb = new StringBuffer();
//		for (int ii = 0; ii < string.length() / 2; ii++) {
//			int jj = ii * 2;
//			sb.append(string.charAt(jj));
//			sb.append(string.charAt(jj + 1));
//			sb.append(" ");
//		}
//		System.out.println(sb.toString());
//		return null;
//	}
	private static Object taskAtMoment() {
//		byte [] fileContent = new byte [] {
//			0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0x61, 0x62, 0x63, 0x63, 0x63, 0x63, 0x63, 0x63, 0x63, 0x63, 0x63, 0x64
//		};
//		
//		Path path = FileSystems.getDefault().getPath("/Users/jscarpaci/tmp/testfile.x");
//		try {
//			Files.write(path, fileContent, new OpenOption [0]);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
//		// lists environment variables in effort to figure out why eclipse misbehaves on Mac
//		Map<String, String> env = System.getenv();
//        for (String envName : env.keySet()) {
//            System.out.format("%s=%s%n", envName, env.get(envName));
//        }

//		ClientEmulatorControl cec = new ClientEmulatorControl();
//		try {
//			String [] filenameList = new String [] { 
//					"IDTECH_VEND3AR-EMV_CONTACT-visa_advt_v601baseline_card_20160303_326.bin",
//					"IDTECH_VEND3AR-EMV_CONTACT-mc_ppc_map03v2.1_20160303_2500.bin",
//					"IDTECH_VEND3AR-EMV_CONTACT-amex_global6.2aeips20_20160303_300.bin",
//					"IDTECH_VEND3AR-EMV_CONTACT-disc_acquirer_e2d_card01_20160303_326.bin"
//				};
//			
//			boolean result = cec.authAndSaleIdtechVend(filenameList);
//			if (result) {
//				log.info("Successfully executed auth and sale for: " + String.join(", ", filenameList));
//			}
//		} catch (Exception e) {
//			log.info("Unable to successfully call ClientEmulatorControl.junit_authAndSaleVendX_DUKPT: ", e);
//		}
        
		return null;
	}

	
	private static Object sendActTransactionWithAmount(Scanner scanner, Vend3 reader) {
		System.out.println("Please enter amount to authorize: ");
		String keyed = scanner.next();
		
		if (keyed != null && keyed.length() > 1) {
//				reader.sendActivateTransaction2("0326");
			reader.sendActivateTransaction2(keyed);
		} else {
			log.info("Authorization not executed. No auth amount entered.");
		}
		return null;
	}

	private static Object tlvParseArbitraryHexString(Scanner scanner) {
		Pattern delimiter = scanner.delimiter();
		try {
			System.out.println("Please enter hex string to be TLV parsed:");
			scanner.useDelimiter("\n");
			String keyed = scanner.next();
			if (keyed != null && keyed.length() > 1) {
					String spaceStrippedKeyed = keyed.replaceAll(" ", "");
					byte [] asBytes = ByteArrayUtils.fromHex(spaceStrippedKeyed);
//					Map<byte [], byte []>map = TLV_PARSER.parse(asBytes, 0, asBytes.length);
					
					byte [][]forcedTagLengthList = new byte[11][0];
					forcedTagLengthList[0] = new byte [] {(byte)0xff, (byte)0xe0};
					forcedTagLengthList[1] = new byte [] {(byte)0xff, (byte)0xe3};
					forcedTagLengthList[2] = new byte [] {(byte)0xff, (byte)0xe4};
					forcedTagLengthList[3] = new byte [] {(byte)0xff, (byte)0xe5};
					forcedTagLengthList[4] = new byte [] {(byte)0xff, (byte)0xe6};
					forcedTagLengthList[5] = new byte [] {(byte)0xff, (byte)0xe7};
					forcedTagLengthList[6] = new byte [] {(byte)0xff, (byte)0xe1};
					forcedTagLengthList[7] = new byte [] {(byte)0xff, (byte)0xe2};
					forcedTagLengthList[8] = new byte [] {(byte)0xff, (byte)0xe8};
					forcedTagLengthList[9] = new byte [] {(byte)0xff, (byte)0xe9};
					forcedTagLengthList[10] = new byte [] {(byte)0xff, (byte)0xea};
					
					Map<byte [], byte []>map = new TLVParserLoose().parse(asBytes, 0, asBytes.length, forcedTagLengthList);
					TLVTag.dumpTlvParseMap(map);
			} else {
				log.info("No data entered.");
			}
		} catch (IOException e) {
			log.info("Could not TLV parse input.", e);
		} finally {
			scanner.useDelimiter(delimiter);
		}
		log.info("Exiting.");
		return null;
	}
	
    public static void loadEMVParamsToKeyList() {
    	keyList = convertEMVParametersToComponents();
    }
	public static List<EMVCAKeyComponents> convertEMVParametersToComponents() {
		List<EMVCAKeyComponents>keyList = new ArrayList<>();
		try {
			Map<String, String>lineMap = new HashMap<>();
			Stream<String> lines = Files.lines(Paths.get(EMVCAKeyComponents.FILENAME));
			Stream<String> keyLines = lines.filter(	s -> s.startsWith("key") );
			keyLines.forEachOrdered(line -> { 
				String [] parts = line.split("=");
				String key = parts[0];
				lineMap.put(key, parts[1]);
				if (key.equals("keyChecksum")) {
					keyList.add(new EMVCAKeyComponents(lineMap.get("keyAID"), lineMap.get("keyIndex"), lineMap.get("keyModulus"), lineMap.get("keyExponent"), lineMap.get("keyChecksum")));
					log.info("Added keyAID: " + keyList.get(keyList.size() - 1).diagString());
					lineMap.clear();
				}
			});
			lines.close();
		} catch (IOException e) {
			log.info("Could not convertEMVParametersToComponents.", e);
			e.printStackTrace();
		}
		return keyList;
	}

	private static Object listEMVParamKeyList() {
		System.out.println("EMV Parameter keyList:");
		if (getKeyList() != null && getKeyList().size() > 0) {
			int ii = 0; 
			for (EMVCAKeyComponents comp: getKeyList()) {
				System.out.println("" + ii + ") " + comp.diagString());
				ii++;
			}
		} else {
			log.info("EMV params keyList is empty.");
		}
		return null;
	}

	private static Object applyCAPublicKeyListItem(Scanner scanner, Vend3 reader) {
		System.out.println("Apply EMV Parameter CA Public Key keyList:");
		if (getKeyList() != null && getKeyList().size() > 0) {
			Integer keySelected = itemSelectedRepl(scanner, getKeyList().toArray(), "Select keyList item number to apply:");
			if (keySelected != -1) {
				System.out.println("key item selected: " + getKeyList().get(keySelected).toString());
				String [] flagList = {"Contact", "Contactless"};
				Integer flagIdx = itemSelectedRepl(scanner, flagList, "Select Contact or Contactless:");
				if (flagIdx != -1) {
					byte contactlessContactFlag = (flagIdx == 0) ? Vend3.CA_PUBLIC_KEY_MGMT_FLAG_CONTACT : Vend3.CA_PUBLIC_KEY_MGMT_FLAG_CONTACTLESS;
					reader.sendCAPublicKey(contactlessContactFlag, getKeyList().get(keySelected));
				} else {
					log.info("Contact / Contactless Flag not selected.  Aborting.");
				}
			}
		} else {
			log.info("EMV params keyList is empty.");
		}
		return null;
	}


	private static Object adjustOTIDukptToolResponseOutput(Scanner scanner) {
        
		File savedDir = new File(SAVED_DATA_PATHNAME);
		if (savedDir.exists()) {
			File [] fileList = savedDir.listFiles(standardFilter);
			Integer fileSelectedIdx = itemSelectedRepl(scanner, fileList, "Select file to OTI Dukpt adjust.");
			if (fileSelectedIdx != -1) {
				File file = fileList[fileSelectedIdx];
				CardReaderType cardReaderType = IdtechVendXDataBlob.cardReaderTypeFromFilename(file.getName());

				switch (cardReaderType) {
					case OTI_EMV:
						String startFilename = file.getName();
						try {
							byte [] fileContent = fileContentForFilename(startFilename);
							byte [] noSpaceContent = new byte [fileContent.length];
							int jj = 0;
							for (int ii = 0; ii < fileContent.length; ii++) {
								if (fileContent[ii] != 0x20 && fileContent[ii] != 0x0d && fileContent[ii] != 0x0a) {
									noSpaceContent[jj] = fileContent[ii];
									jj++;
								}
							}
							byte []newFileContent = new byte[jj];
//							int FC_OFFSET = 9;
							int ETX_LENGTH = 1;
							int LRC_LENGTH = 1;
							System.arraycopy(noSpaceContent, 0, newFileContent, 0, jj);
							byte [] binaryBytes = ByteArrayUtils.fromHex(new String(newFileContent));
							
//							int emvToolFormatBytesLength = binaryBytes.length - FC_OFFSET - ETX_LENGTH - LRC_LENGTH;
//							byte [] dataBytes = new byte[emvToolFormatBytesLength];
//							System.arraycopy(binaryBytes, FC_OFFSET, dataBytes, 0, emvToolFormatBytesLength);
							int LENGTH_OFFSET = 1;
							int STX_LENGTH = 1;
							int UNIT_LENGTH = 1;
							int OPCODE_LENGTH = 1;
							int messageLength = TLVTag.parseTlvLength(binaryBytes, LENGTH_OFFSET);
							int lengthLength = TLVTag.lengthOfLength(messageLength);
							int dataOffset = STX_LENGTH + lengthLength + UNIT_LENGTH + OPCODE_LENGTH;
							int dataLength = binaryBytes.length - dataOffset - LRC_LENGTH - ETX_LENGTH;
							byte [] dataBytes = new byte[dataLength];
							System.arraycopy(binaryBytes, dataOffset, dataBytes, 0, dataLength);
							Map<byte [], byte []>successTemplate = TLV_PARSER.parse(dataBytes, 0, dataBytes.length);
							byte [] successValue = successTemplate.get(TLVTag.OTI_SUCCESS_TEMPLATE.getValue());
							
							System.out.println("successValue: " + StringUtils.toHex(successValue));
							String outputfilename = SAVED_DATA_PATHNAME + startFilename + ".bin";
							Path writePath = FileSystems.getDefault().getPath(outputfilename);
							Files.write(writePath, successValue, new OpenOption[0]);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						break;
					default:
						log.info("Not an OTI_EMV data file.");
						break;
				}
			}
		}
        
        
		return null;
	}

	private static Object injectResponseListItemIntoTxnManager(Scanner scanner) {
		if (getVendXTxnManager() != null) {
				if (getVend3Reader().getResponseList() != null && getVend3Reader().getResponseList().size() > 0) {
				Integer responseListItemSelectedIdx = getResponseListItemSelection(scanner, "Select responseList item to inject.");
				if (responseListItemSelectedIdx != -1) {
					IdtechVendXDataBlob responseMsg = (IdtechVendXDataBlob)getVend3Reader().getResponseList().get(responseListItemSelectedIdx);
					getVendXTxnManager().messageReceived(responseMsg);
				}			
			} else {
				System.out.println("responseList is empty.  Put something in the list first.");
			}
		} else {
			System.out.println("VendX transaction manager is not active.");
		}

		return null;
	}


	private static Object listResponseList() {
		if (getVend3Reader().getResponseList() != null && getVend3Reader().getResponseList().size() > 0) {
			int ii = 0;
			for (CardReaderDataBlob data: getVend3Reader().getResponseList()) {
				System.out.println(ii + ") " + data.toStringId());
				ii++;
			}
		} else {
			System.out.println("responseList is empty.");
		}
		System.out.println();
		return null;
	}

	private static Object changeCardReaderTypeAndEntryType(Scanner scanner) {
		System.out.println("Current Default CardReaderType: " + getDefaultCardReaderType());
		Integer cardReaderTypeIdx = itemSelectedRepl(scanner, CardReaderType.values(), "Select CardReaderType:");
		if (cardReaderTypeIdx != -1) {
			setDefaultCardReaderType(CardReaderType.values()[cardReaderTypeIdx]);
		}
		System.out.println("Current Default EntryType: " + getDefaultEntryType());
		Integer entryTypeIdx = itemSelectedRepl(scanner, EntryType.values(), "Select EntryType:");
		if (entryTypeIdx != -1) {
			setDefaultEntryType(EntryType.values()[entryTypeIdx]);
		}
		return null;
	}

	private static Object startTransactionManagerForVend3() {
		setVendXTxnManager(new VendXTxnManager(getVend3Reader()));
		getVend3Reader().applyVendXTxnManager(getVendXTxnManager());
		return null;
	}
	private static Object stopTransactionManagerForVend3() {
		if (getVendXTxnManager() != null) {
			getVendXTxnManager().stopTransaction();
			getVend3Reader().removeVendXTxnManager();
		}
		return null;
	}

	private static Object listSavedTransactionData() {
		File savedDir = new File(SAVED_DATA_PATHNAME);
		if (savedDir.exists()) {
			File [] fileList = savedDir.listFiles(standardFilter);
			int ii = 0;
			for (File file: fileList) {
				CardReaderType cardReaderType = IdtechVendXDataBlob.cardReaderTypeFromFilename(file.getName());
				EntryType entryType = IdtechVendXDataBlob.entryTypeFromFilename(file.getName());
				System.out.println(ii + ") " + file.getName() + " CardReaderType: " + cardReaderType + ", EntryType: " + entryType);
				ii++;
			}
			System.out.println();
		} else {
			System.out.println(SAVED_DATA_PATHNAME + " is empty.");
		}
		return null;
	}

	public static byte [] fileContentForFilename(String filename) throws IOException {
		Path path = FileSystems.getDefault().getPath(SAVED_DATA_PATHNAME, filename);
		byte [] fileContent = Files.readAllBytes(path);
		return fileContent;
	}

	private static Object adjustForEMVTOOL20160412DifferenceForVendi(Scanner scanner) throws IOException {
		File savedDir = new File(SAVED_DATA_PATHNAME);
		if (savedDir.exists()) {
			File [] fileList = savedDir.listFiles(standardFilter);
			Integer fileSelectedIdx = itemSelectedRepl(scanner, fileList, "Select file to adjust.");
			if (fileSelectedIdx != -1) {
				File file = fileList[fileSelectedIdx];
				CardReaderType cardReaderType = IdtechVendXDataBlob.cardReaderTypeFromFilename(file.getName());
				
				switch (cardReaderType) {
					case IDTECH_VENDX_ENCRYPTED_EMV:
					case IDTECH_VENDX_ENHANCED_ENCRYPTED_MSR:
						String startFilename = file.getName();
						byte [] fileContent = fileContentForFilename(startFilename);
						byte PREAMBLE [] = {
								0x56, 0x69, 0x56, 0x4f, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00
						};
						byte [] resultContent = new byte [PREAMBLE.length + fileContent.length + 2];
						System.arraycopy(PREAMBLE, 0, resultContent, 0, PREAMBLE.length);
						System.arraycopy(fileContent, 0, resultContent, PREAMBLE.length, fileContent.length);
						
						int crc = Crc16.calculateCrc16(fileContent);
						byte msbCrc = (byte)((crc & 0xff00) >> 8);
						byte lsbCrc = (byte)(crc & 0x00ff);
						resultContent[resultContent.length - 2] = msbCrc;
						resultContent[resultContent.length - 1] = lsbCrc;
						
						file.delete();
						Path path = FileSystems.getDefault().getPath(SAVED_DATA_PATHNAME, startFilename);
						Files.write(path, resultContent, StandardOpenOption.CREATE_NEW);

						break;
					default:
						log.info("cardReaderData does not have a populated emvData attribute.");
						break;
				}
			}
		} else {
			System.out.println(SAVED_DATA_PATHNAME + " does not exist.");
		}
		
		return null;
	}


	
	private static Object readAFileInteraction(Scanner scanner) throws IOException {
		File savedDir = new File(SAVED_DATA_PATHNAME);
		if (savedDir.exists()) {
			File [] fileList = savedDir.listFiles(standardFilter);
			Integer fileSelectedIdx = itemSelectedRepl(scanner, fileList, "Select file to load.");
			if (fileSelectedIdx != -1) {
				File file = fileList[fileSelectedIdx];
				CardReaderType cardReaderType = IdtechVendXDataBlob.cardReaderTypeFromFilename(file.getName());
				EntryType entryType = IdtechVendXDataBlob.entryTypeFromFilename(file.getName());

				byte [] fileContent = fileContentForFilename(file.getName());
				log.info("fileContent: " + StringUtils.toHex(fileContent));
//				CardReaderDataBlob cardReaderData = null;
//				if (cardReaderType.equals(CardReaderType.OTI_EMV)) {
//					cardReaderData = new OTIDataBlob(fileContent, entryType, file.getName());
//				} else {
//					cardReaderData = IdtechVendXDataBlob.parseVendXFromFile(fileContent, cardReaderType, entryType, file.getName());
//				}
				CardReaderDataBlob cardReaderData = CardReaderDataBlob.getInstance(cardReaderType, fileContent, entryType, file.getName());
				if (cardReaderData != null) {
					getVend3Reader().getResponseList().add(cardReaderData);
				}
			}
		} else {
			System.out.println(SAVED_DATA_PATHNAME + " does not exist.");
		}
		return null;
	}
	
	private static Integer getResponseListItemSelection(Scanner scanner, String title) {
		String [] responseList = new String [getVend3Reader().getResponseList().size()];
		for (int ii = 0; ii < getVend3Reader().getResponseList().size(); ii++) {
			responseList[ii] = getVend3Reader().getResponseList().get(ii).toStringId();
		}
		Integer responseListItemSelectedIdx = itemSelectedRepl(scanner, responseList, title);
		return responseListItemSelectedIdx;
	}
	
	private static Object saveAFileInteraction(Scanner scanner) throws IOException {
		if (getVend3Reader().getResponseList() != null && getVend3Reader().getResponseList().size() > 0) {
			Integer responseListItemSelectedIdx = getResponseListItemSelection(scanner, "Select responseList item to save.");
			if (responseListItemSelectedIdx != -1) {
				CardReaderDataBlob dataBlob = getVend3Reader().getResponseList().get(responseListItemSelectedIdx);
				if (dataBlob instanceof IdtechVendXDataBlob) {
					IdtechVendXDataBlob crd = (IdtechVendXDataBlob)dataBlob;
					int length = crd.getHeaderBuffer().length + crd.getDataBuffer().length + crd.getCrcBuffer().length;
					byte [] fileContent = new byte[length];
					System.arraycopy(crd.getHeaderBuffer(), 0, fileContent, 0, crd.getHeaderBuffer().length);
					System.arraycopy(crd.getDataBuffer(), 0, fileContent, crd.getHeaderBuffer().length, crd.getDataBuffer().length);
					System.arraycopy(crd.getCrcBuffer(), 0, fileContent, crd.getHeaderBuffer().length + crd.getDataBuffer().length, crd.getCrcBuffer().length);
					
					Integer cardReaderTypeIdx = itemSelectedRepl(scanner, CardReaderType.values(), "Select CardReaderType:");
					CardReaderType cardReaderType = CardReaderType.values()[cardReaderTypeIdx];
					
					Integer entryTypeIdx = itemSelectedRepl(scanner, EntryType.values(), "Select EntryType:");
					EntryType entryType = EntryType.values()[entryTypeIdx];
					
					System.out.println("Enter base filename (no spaces): ");
					String keyedFilename = scanner.next();
					
					String filename = IdtechVendXDataBlob.assembleFilenameFor(cardReaderType, entryType, keyedFilename);
					
					File saveDir = new File(SAVED_DATA_PATHNAME);
					if (!saveDir.exists()) {
						saveDir.mkdirs();
					}
					
					Path path = FileSystems.getDefault().getPath(SAVED_DATA_PATHNAME, filename);
					Files.write(path, fileContent, StandardOpenOption.CREATE_NEW);
					System.out.println("filename is: " + filename + " - it has been saved.");
				} else {
					System.out.println("Save is only supported for Vendi and Vend3 card data.");
				}
			}			
		} else {
			System.out.println("responseList is empty.  Put something in the list first.");
		}

		return null;
	}

	private static Integer itemSelectedRepl(Scanner scanner, Object[] entryList, String prompt) {
		boolean done = false;
		Integer result = null; 
		do {
			int ii = 0;
			System.out.println(ii + ") Exit");
			for (ii = 0; ii < entryList.length; ii++) {
				System.out.println((ii+1) + ") " + entryList[ii]);
			}
			System.out.println(prompt);
			String keyed = scanner.next();
			try {
				Integer cmdValue = new Integer(keyed);
				Integer cmdIdx = cmdValue - 1;
				if (cmdIdx < entryList.length) {
					done = true;
					result = cmdIdx;
				} else if (cmdValue == 0) {
					result = -1;
					done = true;
					System.out.println("No selection made.");
				} else {
					System.out.println(cmdValue + " is not one of the options. Please try again.");
				}
			} catch (Exception e) {
				System.err.println("Exception thrown: " + e);
			}
		} while (!done);
		return result;
	}

	private static Object dumpEMVParameterFile() throws IOException {
		Results results = null;
		if (!configureDataLayer()) {
			return null;
		}
		Map<String, Object> params = new HashMap<String, Object>();
		Long fileTransferId = new Long(53068);
		params.put("fileTransferId", fileTransferId);
		try {
			results = DataLayerMgr.executeQuery("GET_EMV_PARAMETER_FILE", params);
			if (results.next()) {
				String fileTransferName = results.getValue("fileTransferName", String.class);
				byte [] fileContent = results.getValue("fileTransferContent", byte[].class);
				Path path = FileSystems.getDefault().getPath("/Users/jscarpaci/tmp/", fileTransferName);
				Files.write(path, fileContent, StandardOpenOption.CREATE_NEW);
			}
		} catch (SQLSyntaxErrorException e) {
			e.printStackTrace();
			System.err.println("Have simple.db.datasource.OPER.username and simple.db.datasource.OPER.password been set to values that work for DEVICE.FILE_TRANSFER?");
		} catch (SQLException | DataLayerException | ConvertException e1) {
			e1.printStackTrace();
		}

		return null;
	}

	private static Object parseAndDumpResponseList(Scanner scanner) throws IOException, InvalidValueException {
		if (getVend3Reader().getResponseList() != null && getVend3Reader().getResponseList().size() > 0) {
			Integer responseListItemSelectedIdx = getResponseListItemSelection(scanner, "Select responseList item to dump.");
			if (responseListItemSelectedIdx != -1) {
				CardReaderDataBlob blob = getVend3Reader().getResponseList().get(responseListItemSelectedIdx);
				System.out.println(blob.toStringId());
				System.out.println();
				if (blob.getDataBuffer() != null && blob.getDataBuffer().length > 0) {
					switch (blob.getCardReaderType()) {
						case IDTECH_VEND3AR: {
								Vend3ARTransactionResponseData data = Vend3ARParsing.parseAnyARResponseData(blob.entireContents());
								System.out.println("Track2 Data: " + StringUtils.toHex(data.getTrackTwoData()));
								Map<byte [], byte []>tlvMap = TLV_PARSER.parse(data.getTlvData(), 0, data.getTlvData().length);
								TLVTag.dumpTlvParseMap(tlvMap);
							}
							break;
						case IDTECH_VENDX_ENCRYPTED_EMV: {
								Map<byte [], byte []>tlvMap = TLV_PARSER.parse(blob.getDataBuffer(), 1, blob.getDataBuffer().length);
//								Map<byte [], byte []>tlvMap = TLV_PARSER.parse(blob.getDataBuffer(), 3, blob.getDataBuffer().length - 3);
								TLVTag.dumpTlvParseMap(tlvMap);
							}
							break;
						case IDTECH_VENDX_ENHANCED_ENCRYPTED_MSR: {
							Map<byte [], byte []>tlvMap = TLV_PARSER.parse(blob.getDataBuffer(), 1, blob.getDataBuffer().length);
							TLVTag.dumpTlvParseMap(tlvMap);
							}
							break;
						case OTI_EMV:{
							OTIDataBlob otiBlob = (OTIDataBlob)blob;
							Map<byte [], byte []>tlvMap = TLV_PARSER.parse(otiBlob.getFileContent(), 0, otiBlob.getFileContent().length);
							TLVTag.dumpTlvParseMap(tlvMap);
						}
							break;
						case IDTECH_VENDX_NON_ENCRYPTED:{
							Map<byte [], byte []>tlvMap = TLV_PARSER.parse(blob.getDataBuffer(), 0, blob.getDataBuffer().length);
							TLVTag.dumpTlvParseMap(tlvMap);
						}
							break;
						default:
							System.out.println("Unexpected CardReaderType: " + blob.getCardReaderType());
							break;
					}
				} else {
					System.out.println("data buffer is empty.");
				}
			}

		} else {
			System.out.println("The responseList is empty.  Get data into list and try this again.");
		}
		return null;
	}

	private static byte [] loadVendiFile(String filename) throws IOException {
		byte []asBytes = null;
		FileInputStream fin = null;
		File file = null;
		file = new File(filename);
		if (file.exists()) { 
            try {
				fin = new FileInputStream(file);
	            byte fileContent[] = new byte[(int)file.length()];
	            // Reads up to certain bytes of data from this input stream into an array of bytes.
	            fin.read(fileContent);
	            String asString = new String(fileContent);
	            System.out.println("asString: " + asString);
	            byte []asBytesWithResponseFrameHeader = ByteArrayUtils.fromHex(asString);
	            
	            // remove the response frame header
	            int DATA_BYTE_OFFSET = 14;
	            int resultLength = asBytesWithResponseFrameHeader.length - DATA_BYTE_OFFSET;
	            asBytes = new byte[resultLength];
	            System.arraycopy(asBytesWithResponseFrameHeader, DATA_BYTE_OFFSET, asBytes, 0, resultLength);
	            System.out.println("asBytes as hex: " + StringUtils.toHex(asBytes));
    		} finally {
    			if (fin != null)
    				try {
    					fin.close();
    				} catch (IOException e) {
    					// TODO Auto-generated catch block
    					e.printStackTrace();
    				}
    		}
		} else {
			System.out.println("Filename " + filename + " does not exist.");
		}
		return asBytes;
	}
	
	private static void doNothing() {
		
	}
	
	private static void sendVendiRev46() throws IOException, SQLException, DataLayerException, ConvertException, InvalidValueException {
		//VENDI_REV_46_2016_01_28
		byte [] asBytes = loadVendiFile(VENDI_REV_46_2016_01_28);
		executeAuthorizeIdtechReader(new IdtechVendXDataBlob(CardReaderType.IDTECH_VENDX_ENCRYPTED_EMV, EntryType.EMV_CONTACTLESS, asBytes));		
	}
	
	private static void sendMasterCardComboJan22() throws IOException, SQLException, DataLayerException, ConvertException, InvalidValueException {
		//MC_CONTACTLESS_COMBO_AES_2016_01_22
		byte [] asBytes = loadVendiFile(MC_CONTACTLESS_COMBO_AES_2016_01_22);
		executeAuthorizeIdtechReader(new IdtechVendXDataBlob(CardReaderType.IDTECH_VENDX_ENCRYPTED_EMV, EntryType.EMV_CONTACTLESS, asBytes));		
	}
	
	private static void sendMasterCardCombo() throws IOException, SQLException, DataLayerException, ConvertException, InvalidValueException  {
		byte [] asBytes = loadVendiFile(MC_CONTACTLESS_COMBO_TAPPED_AES);
		executeAuthorizeIdtechReader(new IdtechVendXDataBlob(CardReaderType.IDTECH_VENDX_ENCRYPTED_EMV, EntryType.EMV_CONTACTLESS, asBytes));		
	}

	private static void sendMasterCardPaypass() throws IOException, SQLException, DataLayerException, ConvertException, InvalidValueException  {
		byte [] asBytes = loadVendiFile(MC_PAYPASS_TAPPED_AES);
		executeAuthorizeIdtechReader(new IdtechVendXDataBlob(CardReaderType.IDTECH_VENDX_ENCRYPTED_EMV, EntryType.EMV_CONTACTLESS, asBytes));		
	}
	
	private static void sendAmexSwiped() throws IOException, SQLException, DataLayerException, ConvertException, InvalidValueException {
		// this file should be in the TestClient project under the test-data folder
		byte [] asBytes = loadVendiFile(AMEX_CARD_SWIPE_AES);
		executeAuthorizeIdtechReader(new IdtechVendXDataBlob(CardReaderType.IDTECH_VENDX_ENHANCED_ENCRYPTED_MSR, EntryType.SWIPE, asBytes));
	}
	
	private static void sendVendiVisaCDETTapped() throws IOException, SQLException, DataLayerException, ConvertException, InvalidValueException {
		// this file should be in the TestClient project under the test-data folder
		byte [] asBytes = loadVendiFile(VISA_CDET_TAPPED_AES);
		IdtechVendXDataBlob blob = new IdtechVendXDataBlob(CardReaderType.IDTECH_VENDX_ENCRYPTED_EMV, EntryType.EMV_CONTACTLESS, asBytes);
		executeAuthorizeIdtechReader(blob);
	}
	
	private static void sendGdiFileToServer()  {
		try {
			Parms parms = Parms.getParmsInstance();
			
//			String gdi = "A\nCFG=2\nSPEC=3002004\nREASON=0\nFW=BSK_Test_1\nBL=Bootloader = N/A\nDIAG=Diagnostic = N/A\nBEZEL=BT:B\rAV:2.5.1\rMN:54D908\rSN:100-0491BSK\rBV:Boot4.2.1\rHV:7.0.2\r\nMODEM=TYPE:208\rCGMM:GHI 467\rCCID:12345678901234567890\rCGSN:20\rCGMI:TelNotIt\rCGMR:1.2.3\r\n";
//			String gdi = "A\nCFG=2\nSPEC=3002004\nREASON=0\nFW=BSK_Test_1\nBL=Bootloader = N/A\nDIAG=Diagnostic = N/A\nBEZEL=BT:B\rAV:2.5.1\rMN:Saturn 6500-G6A\rSN:100-0491BSK\rBV:Boot4.2.1\rHV:7.0.2\r\nMODEM=TYPE:208\rCGMM:GHI 467\rCCID:12345678901234567890\rCGSN:20\rCGMI:TelNotIt\rCGMR:1.2.3\r\n";
			
//			String gdi = "A\nCFG=2\nSPEC=3002004\nREASON=0\nFW=BSK_Test_1\nBL=Bootloader = N/A\nDIAG=Diagnostic = N/A\nBEZEL=BT:B\rAV:2.5.1\rMN:IDTech Vend3\rSN:100-0491BSK\rBV:Boot4.2.1\rHV:7.0.2\r\nMODEM=TYPE:208\rCGMM:GHI 467\rCCID:12345678901234567890\rCGSN:20\rCGMI:TelNotIt\rCGMR:1.2.3\r\n";
//			String gdi = "A\nCFG=2\nSPEC=3002004\nREASON=0\nFW=BSK_Test_1\nBL=Bootloader = N/A\nDIAG=Diagnostic = N/A\nBEZEL=BT:B\rAV:2.5.1\rMI:IdTech\rMN:IDTech Vend3\rSN:100-0491BSK\rBV:Boot4.2.1\rHV:7.0.2\r\nMODEM=TYPE:208\rCGMM:GHI 467\rCCID:12345678901234567890\rCGSN:20\rCGMI:TelNotIt\rCGMR:1.2.3\r\n";
			String gdi = "A\nCFG=2\nSPEC=3002004\nREASON=0\nFW=BSK_Test_1\nBL=Bootloader = N/A\nDIAG=Diagnostic = N/A\nBEZEL=BT:B\rAV:2.5.1\rBT:E\rMN:EMEA UI VEND3\rSN:100-0491BSK\rBV:Boot4.2.1\rHV:7.0.2\r\nMODEM=TYPE:208\rCGMM:GHI 467\rCCID:12345678901234567890\rCGSN:20\rCGMI:TelNotIt\rCGMR:1.2.3\r\n";
			
			// Saturn 6500-G6A
			
			MessageData_CA dataCA = new MessageData_CA();
			dataCA.setRequestType(GenericRequestType.UPDATE_STATUS_REQUEST);
			((UpdateStatusRequest) dataCA.getRequestTypeData()).setStatusCodeBitmap(0x01);
	
			MessageData_C7 dataC7 = new MessageData_C7();
			dataC7.setContent(gdi.getBytes());
			dataC7.setCreationTime(Calendar.getInstance());
			dataC7.setFileType(FileType.GENERIC_DEVICE_INFORMATION);
	
			ClientEmulator ce = new ClientEmulator(parms.getHost(), parms.getPort(), parms.getDeviceName(), parms.getEncryptKey());
			log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
			ce.setPort(parms.getSalePort());
			ce.startCommunication(7);
			try {
				ce.sendMessage(dataCA);
				ce.sendMessage(dataC7);
			} finally {
				ce.stopCommunication();
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Send GDI File to server failed");
		}
	}

	private static void testTrackTwoDataAndOthers() throws IOException, InvalidValueException {
		if (getVend3Reader().getResponseList() != null && getVend3Reader().getResponseList().size() > 0) {
			int msgNumber = 0;
			CardReaderDataBlob dataBlob = getVend3Reader().getResponseList().get(msgNumber);
			if (dataBlob instanceof IdtechVendXDataBlob) {
				IdtechVendXDataBlob msg = (IdtechVendXDataBlob)dataBlob;
				String result = new String(msg.getEMVData().getTrackTwoData());
				System.out.println("Msg #" + msgNumber + " track two data: " + result);
				
				String panSeqNumber = VendXEMVData.encode(msg.parseMap().get(TLVTag.PAN_SEQUENCE_NUMBER.getValue()));
				System.out.println("PAN SEQ NUMBER: " + panSeqNumber);
				String applicationIdentifier = VendXEMVData.encode(msg.parseMap().get(TLVTag.APPLICATION_IDENTIFIER.getValue()));
				System.out.println("Application Identifier: " + applicationIdentifier);
				
				String emvData = VendXEMVData.encodeChipCardData(msg.getEMVData().getTlvData());
				System.out.println("EMV Data: " + emvData);
			} else {
				System.out.println("testTrackTwoDataAndOthers only supported for IdtechVendXDataBlob");
			}
		} else {
			System.out.println("There are no vend3 responses in the list.");
		}
	}

	// 93:
	private static void testAssembleBit55TlvData() throws IOException {
		if (getVend3Reader().getResponseList() != null && getVend3Reader().getResponseList().size() > 0) {
			int msgNumber = 0;
			CardReaderDataBlob dataBlob = getVend3Reader().getResponseList().get(msgNumber);
			if (dataBlob instanceof IdtechVendXDataBlob) {
				IdtechVendXDataBlob msg = (IdtechVendXDataBlob)dataBlob;
				VendXEMVData emvData = msg.getEMVData();
				TLVParser tlvParser = new TLVParser();
				Map<String, byte []>map = tlvParser.parseData(emvData.getTlvData(), 0, emvData.getTlvData().length);
//				StringBuilder buffer = new StringBuilder(513);
//				for (byte [] tag: VendXParsing.getRequestAuthorizationTagList()) {
//					byte [] value = map.get(tag);
//					if (value != null) {
//						TLVParser.formatTagValueToHex(buffer, tag, value);
//					}
//				}
				String buffer = VendXParsing.extractChaseChipCardData(map, AuthorityAction.AUTHORIZATION);
				System.out.println("Msg #" + msgNumber + " bit 55 data length: " + buffer.length() + " - " + buffer);
			} else {
				System.out.println("testAssembleBit55TlvData  only supported for IdtechVendXDataBlob");
			}
		} else {
			System.out.println("There are no vend3 responses in the list.");			
		}		
	}
	
	// testing reassembly
	private static void testTlvReassembly() throws IOException, InvalidValueException  {
		if (getVend3Reader().getResponseList() != null && getVend3Reader().getResponseList().size() > 0) {
			int msgNumber = 0;
			CardReaderDataBlob dataBlob = getVend3Reader().getResponseList().get(msgNumber);
			if (dataBlob instanceof IdtechVendXDataBlob) {
				IdtechVendXDataBlob msg = (IdtechVendXDataBlob)dataBlob;
				VendXEMVData emvData = msg.getEMVData();
				TLVParser tlvParser = new TLVParser();
				msg.dumpData(msgNumber);
				
				List<TLVTag> exclusionList = new ArrayList<>(); 
				exclusionList.add(TLVTag.TRACK1_EQUIV_DATA_MCHIP_CARD);
				
				ByteBuffer buffer = ByteBuffer.allocate(emvData.getTlvData().length);
				Map<byte [], byte []>map = tlvParser.parse(emvData.getTlvData(), 0, emvData.getTlvData().length);
				for (Map.Entry<byte [], byte []>entry: map.entrySet()) {
		    		byte [] key = entry.getKey();
		    		if (key != null && key.length > 0 && key[0] != 0) {
		    			if (!exclusionList.contains(TLVTag.getByValue(key))) {
		    				reassembleTlvData(key, map, buffer);
		    			} else {
		    				System.out.println("Found TRACK1_EQUIV_DATA_MCHIP_CARD.  Excluding");
		    			}
		    		}
				}
				
				byte [] originalTlvBytes = emvData.getTlvData();
				int bufferPos = buffer.position();
				buffer.position(0);
				byte [] reassembledTlvBytes = new byte [bufferPos];
				buffer.get(reassembledTlvBytes, 0, bufferPos);
	
				System.out.println("Reassembled bytes chip card tlv length: " + VendXEMVData.bit55DataSize(reassembledTlvBytes));
				
				// testing that it catches bad data
	//			byte pokeAtIt = reassembledTlvBytes[reassembledTlvBytes.length - 1];
	//			pokeAtIt = (byte)(pokeAtIt + 1);
	//			reassembledTlvBytes[reassembledTlvBytes.length - 1] = pokeAtIt;			
				
				if (exclusionList != null && exclusionList.size() > 0) {
					if (areTlvBytesEquivalent(originalTlvBytes, reassembledTlvBytes, exclusionList)) {
						System.out.println("TLVs are equivalent excepting exclusions.");
					} else {
						System.out.println("TLVs are NOT equivalent excepting exclusions");
					}
					System.out.println("Test TLV Reassembly with exclusion complete. " + exclusionList);
				} else {
					boolean tlvBytesAreEquivalent = areTlvBytesAreEquivalent(originalTlvBytes, reassembledTlvBytes);
					boolean crossCheckTlvBytesEquivalent = false;
					if (tlvBytesAreEquivalent) {
						crossCheckTlvBytesEquivalent = areTlvBytesAreEquivalent(reassembledTlvBytes, originalTlvBytes);
						if (!crossCheckTlvBytesEquivalent) {
							System.out.println("Failed cross check of reassembled vs original check");
						}
					} else {
						System.out.println("Failed original vs reassembled check");
					}
					System.out.println("Test TLV Reassembly complete");
					System.out.println("Input buffer: " + StringUtils.toHex(originalTlvBytes));
					System.out.println("Outputbuffer: " + StringUtils.toHex(reassembledTlvBytes));
					if (tlvBytesAreEquivalent && crossCheckTlvBytesEquivalent) {
						System.out.println("TLVs are equivalent");
					} else {
						System.out.println("The TLVs do NOT match.");
					}
				}
			} else {
				System.out.println("testTlvReassembly only supported for IdtechVendXDataBlob");
			}
		} else {
			System.out.println("There are no vend3 responses in the list.");
		}
	}

	private static void reassembleTlvData(byte [] key, Map<byte [], byte[]>map, ByteBuffer buffer) {
		byte [] value = map.get(key);
		if (value != null) {
			TLVParser.formatTagValue(buffer, key, value);
		}
	}
	private static boolean areTlvBytesEquivalent(byte[] lhsTlvBytes, byte[] rhsTlvBytes, List<TLVTag> exclusionList) throws IOException, InvalidValueException {
		TLVParser lhsParser = new TLVParser();
		Map<byte [], byte[]>lhsMap = lhsParser.parse(lhsTlvBytes, 0, lhsTlvBytes.length);
		TLVParser rhsParser = new TLVParser();
		Map<byte [], byte[]>rhsMap = rhsParser.parse(rhsTlvBytes, 0, rhsTlvBytes.length);
		
		boolean failed = false;
		for (Map.Entry<byte [], byte []>entry: lhsMap.entrySet()) {
    		byte [] key = entry.getKey();
    		if (!exclusionList.contains(TLVTag.getByValue(key))) {
	    		failed = tlvMapCompare(key, lhsMap, rhsMap);
	    		if (failed) {
	    			break;
	    		}
    		}
		}
		return !failed;
	}
	private static boolean areTlvBytesAreEquivalent(byte[] lhsTlvBytes, byte[] rhsTlvBytes) throws IOException {
		TLVParser lhsParser = new TLVParser();
		Map<byte [], byte[]>lhsMap = lhsParser.parse(lhsTlvBytes, 0, lhsTlvBytes.length);
		TLVParser rhsParser = new TLVParser();
		Map<byte [], byte[]>rhsMap = rhsParser.parse(rhsTlvBytes, 0, rhsTlvBytes.length);
		
		boolean failed = false;
		for (Map.Entry<byte [], byte []>entry: lhsMap.entrySet()) {
    		byte [] key = entry.getKey();
    		failed = tlvMapCompare(key, lhsMap, rhsMap);
    		if (failed) {
    			break;
    		}
		}
		return !failed;
	}
	private static boolean tlvMapCompare(byte [] key, Map<byte [], byte[]>lhsMap, Map<byte [], byte []> rhsMap) {
		boolean failed = false;
		if (key != null && key.length > 0 && key[0] != 0) {
			byte [] lhsValue = lhsMap.get(key);
			byte [] rhsValue = rhsMap.get(key);
			if (rhsValue == null) {
				System.out.println("rhsValue does not exist for key: " + key);
				failed = true;
			} else if (rhsValue.length != lhsValue.length) {
				System.out.println("rhsValue's length does not match lhsValue's length for key: " + key);
				failed = true;
			} else {
				int ii = 0;
				for (; ii < lhsValue.length && !failed;) {
					if (lhsValue[ii] != rhsValue[ii]) {
						failed = true;
					} else {
						ii++;
					}
				}
				if (failed) {
					System.out.println("rhsValue does not match lhsValue at index: " + ii + ", lhsValue[ii]: " + lhsValue[ii] + ", rhsValue[ii]: " + rhsValue[ii]);
				}
			}
		}
		return failed;
	}
	
	private static void testTlvParse() throws IOException {
		if (getVend3Reader().getResponseList() != null && getVend3Reader().getResponseList().size() > 0) {
			int msgNumber = 0;
			CardReaderDataBlob dataBlob = getVend3Reader().getResponseList().get(msgNumber);
			if (dataBlob instanceof IdtechVendXDataBlob) {
				IdtechVendXDataBlob originalMsg = (IdtechVendXDataBlob)dataBlob;
				IdtechVendXDataBlob msg = new IdtechVendXDataBlob(originalMsg.getCardReaderType(), originalMsg.getEntryType());
				msg.setEMVData(originalMsg.getEMVData());
				try {
					Map<byte [], byte[]> parseMap = msg.parseMap(); 
					byte [] panSeqNumber = parseMap.get(TLVTag.PAN_SEQUENCE_NUMBER.getValue());
					String panSeqNumberString = VendXEMVData.encode(panSeqNumber);
					System.out.println("Msg #" + msgNumber + " panSeqNumber is: " + panSeqNumberString);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				System.out.println("testTlvParse only supported for IdtechVendXDataBlob");
			}
		} else {
			System.out.println("responseList is empty.  Perform a txn first.");
		}
		
	}
	
	private static void dumpAllTlvsForList() {
		int counter = 0;
		for (CardReaderDataBlob blob: getVend3Reader().getResponseList()) {
			try {
				if (blob instanceof IdtechVendXDataBlob)
					((IdtechVendXDataBlob)blob).dumpAllTags(counter);
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Problem parsing item " + counter + " in the list");
			}
			counter++;
		}
	}
	
	private static void dumpTrackDataForList() throws IOException {
		System.out.println("Dumping track data...");
		int ii = 0;
		for (CardReaderDataBlob blob: getVend3Reader().getResponseList()) {
			if (blob instanceof IdtechVendXDataBlob) {
				IdtechVendXDataBlob msg = (IdtechVendXDataBlob)blob;
				msg.dumpTrackData(ii);
				msg.dumpTrackEquivalentData();
			}
			ii++;
		}
		System.out.println("Dumping track data done");
	}
	
	private static void dumpResponseList() {
		if (getVend3Reader().getResponseList() == null || getVend3Reader().getResponseList().size() < 1) {
			System.out.println("responseList is empty.");
		} else {
			int ii = 0;
			for (CardReaderDataBlob msg: getVend3Reader().getResponseList()) {
				//msg.dumpData(ii);
				msg.dumpCompleteDataMsg(ii);
				ii++;
			}
		}
	}

	private static void emptyResponseList() {
		getVend3Reader().getResponseList().clear();
	}
	
	static public class Crc16 {

	    private static int crcTable[] = {
	        0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
	        0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
	        0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
	        0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
	        0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
	        0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
	        0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
	        0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
	        0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
	        0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
	        0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
	        0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
	        0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
	        0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
	        0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
	        0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
	        0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
	        0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
	        0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
	        0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
	        0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
	        0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
	        0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
	        0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
	        0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
	        0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
	        0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
	        0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
	        0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
	        0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
	        0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
	        0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
	    };

	    public static int calculateCrc16(byte []data) {
	        return Crc16.calculcateCrc16(0x0000ffff, data);
	    }

	    public static int calculcateCrc16(int newCrc, byte []data) {
	        int length = data.length;
	        for (int index = 0; index < length; index++) {
	            int crcTableIndexLhs = ((newCrc >> 8) & 0xff);
	            int crcTableIndexRhs = (data[index] & 0xff);
	            int idx = ((crcTableIndexLhs ^ crcTableIndexRhs) & 0xff);
	            newCrc = ( (newCrc << 8) ^ crcTable[idx] ) & 0xffff;
	        }

	        return newCrc;
	    }
	}

	
	public static void executeAuthorizeForFirstResposeListMsg() {
		
		CardReaderDataBlob msg;
		if (getVend3Reader().getResponseList() != null && getVend3Reader().getResponseList().size() > 0) {
			msg = getVend3Reader().getResponseList().get(0);
			try {
				executeAuthorize(msg);
			} catch (IOException | SQLException | DataLayerException | ConvertException | InvalidValueException | NumberFormatException e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("No Vend3 responses are available");
		}
	}
	public static void executeAuthorizeForResponseListMsg(Scanner scanner) {
		CardReaderDataBlob msg;
		if (getVend3Reader().getResponseList() != null && getVend3Reader().getResponseList().size() > 0) {
			String [] responseStringList = new String [getVend3Reader().getResponseList().size()];
			int ii = 0;
			for (CardReaderDataBlob data: getVend3Reader().getResponseList()) {
				responseStringList[ii] = data.toStringId();
				ii++;
			}
			Integer responseListIdx = itemSelectedRepl(scanner, responseStringList, "Select responseList item number:");
			if (responseListIdx != -1) {
				msg = getVend3Reader().getResponseList().get(responseListIdx);
				try {
					executeAuthorize(msg);
				} catch (IOException | SQLException | DataLayerException | ConvertException | InvalidValueException | NumberFormatException e) {
					e.printStackTrace();
				}
			}
		} else {
			System.out.println("responseList is empty.");
		}
	}
	public static void executeAuthorizeAndSaleQuickConnectForResponseListMsg(Scanner scanner) {
		CardReaderDataBlob msg;
		if (getVend3Reader().getResponseList() != null && getVend3Reader().getResponseList().size() > 0) {
			String [] responseStringList = new String [getVend3Reader().getResponseList().size()];
			int ii = 0;
			for (CardReaderDataBlob data: getVend3Reader().getResponseList()) {
				responseStringList[ii] = data.toStringId();
				ii++;
			}
			Integer responseListIdx = itemSelectedRepl(scanner, responseStringList, "Select responseList item number:");
			if (responseListIdx != -1) {
				msg = getVend3Reader().getResponseList().get(responseListIdx);
				try {
//					executeAuthorize(msg);
					ClientEmulatorControl cec = new ClientEmulatorControl();
					try {
						if (!org.apache.commons.lang.StringUtils.isEmpty(msg.getFilename())) {
							String [] filenameList = new String [] { 
									msg.getFilename()
								};
							
							boolean result = cec.authAndSaleIdtechVend(filenameList);
							if (result) {
								log.info("Successfully executed auth and sale for: " + String.join(", ", filenameList));
							}
						} else {
							log.info("Filename is required to send the data sample.");
						}
					} catch (Exception e) {
						log.info("Unable to successfully call ClientEmulatorControl.junit_authAndSaleVendX_DUKPT: ", e);
					}
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
			}
		} else {
			System.out.println("responseList is empty.");
		}
	}
	public static void executeAuthorizationAndSaleViaQuickConnectVendIIIReadinessTestData() {
		ClientEmulatorControl cec = new ClientEmulatorControl();
		try {
			String [] filenameList = new String [] { 
					"IDTECH_VEND3AR-EMV_CONTACT-visa_advt_v601baseline_card_20160303_326.bin",
					"IDTECH_VEND3AR-EMV_CONTACT-mc_ppc_map03v2.1_20160303_2500.bin",
					"IDTECH_VEND3AR-EMV_CONTACT-amex_global6.2aeips20_20160303_300.bin",
					"IDTECH_VEND3AR-EMV_CONTACT-disc_acquirer_e2d_card01_20160303_326.bin"
				};
			
			boolean result = cec.authAndSaleIdtechVend(filenameList);
			if (result) {
				log.info("Successfully executed auth and sale for: " + String.join(", ", filenameList));
			}
		} catch (Exception e) {
			log.info("Unable to successfully call ClientEmulatorControl.junit_authAndSaleVendX_DUKPT: ", e);
		}
		
	}
	
	private static void sendProcessUpdatesRequest() {
		
		Parms parms;
		try {
			parms = Parms.getParmsInstance();
			if (parms != null) {
				ClientEmulator ce;
				ce = new ClientEmulator(parms.getHost(), parms.getPort(), parms.getDeviceName(), parms.getEncryptKey());
				String card = ";371449635398431=10121015432112345678?:";
				BigDecimal amount = new BigDecimal("1.00");
				ce.testAuthV4(EntryType.EMV_CONTACTLESS, card, amount);
				
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
	}

	
	static class Parms {
		private String host;
		private int port;
		private int salePort;
		private String deviceName;
		private byte [] encryptKey;
		
		private static Parms parms;
		
		static {
			try {
				parms = getParmsInstance();
			} catch (SQLException | DataLayerException | ConvertException e) {
				e.printStackTrace();
				System.out.println("Initialization failed.");
			}
		}
		
		public Parms(String host, int port, int salePort, String deviceName, byte[] encryptKey) {
			super();
			this.host = host;
			this.port = port;
			this.salePort = salePort;
			this.deviceName = deviceName;
			this.encryptKey = encryptKey;
		}

		public static Parms getParmsInstance() throws SQLException, DataLayerException, ConvertException {
			if (parms == null) {
				EMVTestMain emvTestMain = new EMVTestMain();
	
				String deviceName = "";
				byte [] encryptKey = null;
				Results results = emvTestMain.getAuthDevices(thisArgs);
				try {
					results = DataLayerMgr.executeQuery("GET_AUTH_DEVICES", null);
				} catch(SQLException e) {
					log.warn("Error", e);
					throw e;
				} catch(DataLayerException e) {
					log.warn("Error", e);
					throw e;
				}
				// just get the first one
				if (results.next()) {
					try {
						deviceName = results.getValue("DEVICE_NAME", String.class);
						encryptKey = results.getValue("ENCRYPTION_KEY", byte[].class);
					} catch(ConvertException e) {
						log.warn("Error", e);
						throw e;
					}
				}
				// before I fixed COMPUTERNAME and USERNAME environment variables
				parms = new Parms("localhost", 14109, 14108, deviceName, encryptKey);
				
				// localhost environment
//				parms = new Parms("localhost", 14108, 14109, deviceName, encryptKey);
				
				//String host = "10.0.0.67"; int port = 14108; // dev
//				parms = new Parms("10.0.0.67", 14108, 14109, deviceName, encryptKey);
				
				//String host = "10.0.0.68"; int port = 14108; // int
				// This worked with the TD001581 / VJ100063332 device that was set up on 2/3/16
//				parms = new Parms("10.0.0.68", 14108, 14109, deviceName, encryptKey); 

//				parms = new Parms("192.168.4.63", 14108, 14109, deviceName, encryptKey);	// ecc 
				
			}			
			return parms;
		}
		
		public String getHost() {
			return host;
		}
		public int getPort() {
			return port;
		}
		public int getSalePort() {
			return salePort;
		}
		public String getDeviceName() {
			return deviceName;
		}
		public byte[] getEncryptKey() {
			return encryptKey;
		}
		
	}

	public static MessageData_C3 executeAuthorize(CardReaderDataBlob msg) throws IOException, SQLException, DataLayerException, ConvertException, InvalidValueException {
		BigDecimal amount = msg.amountAuthorized();
//		BigDecimal amount = new BigDecimal("100");

		MessageData_C3 replyC3 = null;

		//byte [] emvData = (msg.getEntryType() == EntryType.EMV_CONTACTLESS) ? msg.getEMVData().getEmvData() : msg.getContactEmvData();
		byte [] rawData = msg.getRawData();
		
		Parms parms = Parms.getParmsInstance();
		if (parms != null) {
			ClientEmulator ce;
			MessageData_C2 message;
			ce = new ClientEmulator(parms.getHost(), parms.getPort(), parms.getDeviceName(), parms.getEncryptKey());
			message = new MessageData_C2();
			message.setAmount(amount);
			message.setCardReaderType(msg.getCardReaderType());

			GenericRawCardReader crt = (GenericRawCardReader)message.getCardReader();
			crt.setRawData(rawData);
			
			message.setEntryType(msg.getEntryType());
			long transactionId = new Date().getTime() / 1000;
			message.setTransactionId(transactionId);
				
			System.out.println("rawData length: " + rawData.length);

			int protocol = 7;
			
			StringBuilder details = new StringBuilder();
			long startTime = 0;
			try {
				ce.setMessageNumber(0);
				ce.startCommunication(protocol);
				details.setLength(0);
				details.append("Sent:\n").append(message);
				startTime = System.currentTimeMillis();
				MessageData reply = ce.sendMessage(message);
				details.append("\nReceived:\n").append(reply);
				log.debug(details);
				ce.stopCommunication();
				
				replyC3 = (MessageData_C3)reply;
				
			} catch(Exception e) {
				long endTime = System.currentTimeMillis();
				if(startTime == 0)
					startTime = endTime;
				details.append("\nERROR:\n").append(e.getMessage());
				log.warn("Error during session", e);
			} finally {
				System.out.println("authorize details: " + details.toString());
			}
		}
		return replyC3;
	}

	public static void executeAuthorizeIdtechReader(CardReaderDataBlob blob) throws IOException, SQLException, DataLayerException, ConvertException, InvalidValueException {
		BigDecimal amount = new BigDecimal("1.00");
		
		Parms parms = Parms.getParmsInstance();
		if (parms != null) {
			ClientEmulator ce;
			MessageData_C2 message;
			ce = new ClientEmulator(parms.getHost(), parms.getPort(), parms.getDeviceName(), parms.getEncryptKey());
			message = new MessageData_C2();
			message.setAmount(amount);
			message.setCardReaderType(blob.getCardReaderType());
			message.setEntryType(blob.getEntryType());

//			GenericRawCardReader crt = (GenericRawCardReader)message.getCardReader();
			
			long transactionId = new Date().getTime() / 1000;
			message.setTransactionId(transactionId);
				
			int protocol = 7;
			
			StringBuilder details = new StringBuilder();
			long startTime = 0;
			try {
				ce.setMessageNumber(0);
				ce.startCommunication(protocol);
				details.setLength(0);
				details.append("Sent:\n").append(message);
				startTime = System.currentTimeMillis();
				MessageData reply = ce.sendMessage(message);
				details.append("\nReceived:\n").append(reply);
				log.debug(details);
				ce.stopCommunication();
			} catch(Exception e) {
				long endTime = System.currentTimeMillis();
				if(startTime == 0)
					startTime = endTime;
				details.append("\nERROR:\n").append(e.getMessage());
				log.warn("Error during session", e);
			} finally {
				System.out.println("authorize details: " + details.toString());
			}
		}
		 
	}

	private static boolean configureDataLayer() {
		EMVTestMain etm = new EMVTestMain();
		return etm.configureDataLayer(thisArgs);
	}
	private boolean configureDataLayer(String[] args) {
		boolean results = false;
		
		Map<String,Object> argMap;
		try {
			argMap = parseArguments(args);
		} catch(IOException e) {
			finishAndExit(e.getMessage(), 100, true, null);
			return results;
		}

		Properties properties;
		try {
			properties = getProperties(ConvertUtils.getStringSafely(argMap.get("properties-file")), getClass(), null);
		} catch(IOException e) {
			log.warn("Error", e);
			finishAndExit("Could not read properties file", 120, e);
			return results;
		}
		for(Map.Entry<String,Object> entry : argMap.entrySet()) {
			properties.put(entry.getKey(), entry.getValue());
		}
		try {
			Base.configureDataSourceFactory(properties, null);
		} catch(ServiceException e) {
			log.warn("Error", e);
			finishAndExit(e.getMessage(), 300, true, null);
			return results;
		}
		try {
			Base.configureDataLayer(properties);
		} catch(ServiceException e) {
			log.warn("Error", e);
			finishAndExit(e.getMessage(), 400, true, null);
			return results;
		}
		return true;
	}
	
	private Results getAuthDevices(String[] args) {
		Results results = null;

		if (!configureDataLayer(args)) {
			return results;
		}
		try {
			results = DataLayerMgr.executeQuery("GET_AUTH_DEVICES", null);
			
		} catch(SQLException e) {
			log.warn("Error", e);
			finishAndExit(e.getMessage(), 500, true, null);
			return null;
		} catch(DataLayerException e) {
			log.warn("Error", e);
			finishAndExit(e.getMessage(), 600, true, null);
			return null;
		}
		return results;
	}

	@Override
	protected void registerDefaultCommandLineArguments() {
		registerCommandLineSwitch('p', "propertiesFile", true, true, "properties-file", "The properties file to use");
		registerCommandLineSwitch('t', "threads", false, true, "threads", "Number of threads to run");
		registerCommandLineSwitch('n', "messages", false, true, "messages", "Number of messages per session");
		registerCommandLineSwitch('h', "host", false, true, "host", "Host to connect to");
		registerCommandLineSwitch('P', "port", false, true, "port", "Port");
		registerCommandLineSwitch('c', "card", false, true, "card", "The track data to use");
		registerCommandLineSwitch('v', "protocol", true, true, "protocol", "The protocol version to use (0-8)");
		registerCommandLineSwitch('o', "output", true, true, "output", "Directory for output");
	}

	@Override
	protected void registerDefaultActions() {
		// no actions
	}
	@Override
	public void run(String[] args) {
		Map<String,Object> argMap;
		try {
			argMap = parseArguments(args);
		} catch(IOException e) {
			finishAndExit(e.getMessage(), 100, true, null);
			return;
		}
		int threads;
		final int messages;
		final String host;
		final int port;
		final int protocol;
		final String cardNumber;
		String outputDir;
		try {
			threads = ConvertUtils.getInt(argMap.get("threads"), 10);
			messages = ConvertUtils.getInt(argMap.get("messages"), 10);
			host = ConvertUtils.getStringSafely(argMap.get("host"), "usaapd1");
			port = ConvertUtils.getInt(argMap.get("port"), 14109);
			protocol = ConvertUtils.getIntSafely(argMap.get("protocol"), 7);
			outputDir = ConvertUtils.getString(argMap.get("output"), "./");
			cardNumber = ConvertUtils.getString(argMap.get("card"), "375019001001880");
			if(!outputDir.endsWith("/")){
				outputDir+="/";
			}
		} catch(ConvertException e) {
			log.warn("Error", e);
			finishAndExit(e.getMessage(), 200, true, null);
			return;
		} catch(NumberFormatException e) {
			finishAndExit("Could not convert command to a hex number: " + e.getMessage(), 250, true, null);
			return;
		}
		if (!configureDataLayer()) {
			return;
		}
		Results results;
		try {
			results = DataLayerMgr.executeQuery("GET_AUTH_DEVICES", null);
		} catch(SQLException e) {
			log.warn("Error", e);
			finishAndExit(e.getMessage(), 500, true, null);
			return;
		} catch(DataLayerException e) {
			log.warn("Error", e);
			finishAndExit(e.getMessage(), 600, true, null);
			return;
		}
		final String[] deviceNames = new String[threads];
		final byte[][] encryptKeys = new byte[threads][];
		for(int i = 0; i < threads; i++) {
			if(!results.next()) {
				finishAndExit("Not enough results in GET_AUTH_DEVICES: Need " + threads + " but only got " + i, 720, true, null);
				return;
			}
			try {
				deviceNames[i] = results.getValue("DEVICE_NAME", String.class);
				encryptKeys[i] = results.getValue("ENCRYPTION_KEY", byte[].class);
			} catch(ConvertException e) {
				log.warn("Error", e);
				finishAndExit(e.getMessage(), 700, true, null);
				return;
			}
		}

		File file = new File(outputDir+"EMVTestMain_" + new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date()) + ".csv");
		final TaskListener taskListener;
		try {
			taskListener = new WriteCSVTaskListener(new PrintWriter(new FileOutputStream(file)));
		} catch(FileNotFoundException e) {
			log.warn("Error", e);
			finishAndExit(e.getMessage(), 800, true, null);
			return;
		}
		class Runner extends Thread {
			protected final ClientEmulator ce;
			protected final MessageData_C2 message;
			public Runner(int index) {
				super("Runner #" + (index+1));
				ce = new ClientEmulator(host, port, deviceNames[index], encryptKeys[index]);
				message = new MessageData_C2();
				message.setAmount(BigDecimal.valueOf(150));
				message.setCardReaderType(CardReaderType.GENERIC);
				GenericTracksCardReader crt = (GenericTracksCardReader)message.getCardReader();
				crt.setAccountData2(cardNumber);
				message.setEntryType(EntryType.SWIPE);
				long transactionId = new Date().getTime() / 1000;
				message.setTransactionId(transactionId);
				// emv add
				String validationData = ""; 
				message.setValidationData(validationData);
			}

			@Override
			public void run() {
				StringBuilder details = new StringBuilder();
				for(int s = 0; s < messages; s++) {
					long startTime = 0;
					try {
						ce.setMessageNumber(0);
						ce.startCommunication(protocol);
						details.setLength(0);
						details.append("Sent:\n").append(message);
						startTime = System.currentTimeMillis();
						MessageData reply = ce.sendMessage(message);
						long endTime=System.currentTimeMillis();
						details.append("\nReceived:\n").append(reply);
						taskListener.taskRan("SentMessage", details.toString(), startTime, endTime);
						log.debug(details);
						ce.stopCommunication();
					} catch(Exception e) {
						long endTime = System.currentTimeMillis();
						if(startTime == 0)
							startTime = endTime;
						details.append("\nERROR:\n").append(e.getMessage());
						taskListener.taskRan("ERROR", details.toString(), startTime, endTime);
						log.warn("Error during session", e);
					}
				}
			}
		};
		Thread[] threadArray = new Thread[threads];
		for(int i = 0; i < threads; i++) {
			threadArray[i] = new Runner(i);
			threadArray[i].start();
		}
		for(int i = 0; i < threads; i++) {
			try {
				threadArray[i].join();
			} catch(InterruptedException e) {
				log.warn("Thread interrupted", e);
			}
		}
		taskListener.flush();
		Log.finish();
	}

	public static Thread getSocketListener() {
		return socketListener;
	}
	
	public static List<EMVCAKeyComponents> getKeyList() {
		return keyList;
	}
	
	public static Vend3 getVend3Reader() {
		return vend3Reader;
	}

	public static VendXTxnManager getVendXTxnManager() {
		return vendXTxnManager;
	}

	public static void setVendXTxnManager(VendXTxnManager vendXTxnManager) {
		EMVTestMain.vendXTxnManager = vendXTxnManager;
	}

	public static CardReaderType getDefaultCardReaderType() {
		return defaultCardReaderType;
	}

	public static void setDefaultCardReaderType(CardReaderType defaultCardReaderType) {
		EMVTestMain.defaultCardReaderType = defaultCardReaderType;
	}

	public static EntryType getDefaultEntryType() {
		return defaultEntryType;
	}

	public static void setDefaultEntryType(EntryType defaultEntryType) {
		EMVTestMain.defaultEntryType = defaultEntryType;
	}
	
	private static boolean authorizationResult(MessageData_C3 reply) {
		boolean result = false;
		AuthResponseType responseType = reply.getAuthResponseType();
		switch(responseType) {
			case AUTHORIZATION:
				break;
			case AUTHORIZATION_V2:
			{
				MessageData_C3.AuthorizationV2AuthResponse response = (MessageData_C3.AuthorizationV2AuthResponse)reply.getAuthResponseTypeData();
				AuthResponseCodeC3 responseCode = response.getResponseCode();
				result = (responseCode != null && (responseCode.equals(AuthResponseCodeC3.SUCCESS) || responseCode.equals(AuthResponseCodeC3.CONDITIONAL_SUCCESS)));
			}
				break;
			case CONTROL:
				break;
			case PERMISSION:
			{
				MessageData_C3.PermissionAuthResponse response = (MessageData_C3.PermissionAuthResponse)reply.getAuthResponseTypeData();
				PermissionResponseCodeC3 responseCode = response.getResponseCode();
				result = responseCode.equals(PermissionResponseCodeC3.SUCCESS);
			}	
				break;
			default:
				break;
		}

		return result;
	}
	
	@Before
	public void before() throws Exception {
		try {
			EMVTestMain.thisArgs = new String [] {"-p", "EMVTestMain.properties", "-t", "1", "-n", "1", "-s", "10", "-h", "localhost", "-P", "14109", "-c", "4055011111111111=10121015432112345678"};
			Parms.getParmsInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void junitSuccessfulTest() throws Exception {
		String name = "IDTECH_VEND3AR_CONTACTLESS_EMV_MAGSTRIPE-EMV_CONTACTLESS-qVSDC_TEST_CARD_SUCCESS_AUTH.bin";
		CardReaderType cardReaderType = IdtechVendXDataBlob.cardReaderTypeFromFilename(name);
		EntryType entryType = IdtechVendXDataBlob.entryTypeFromFilename(name);

		byte [] fileContent = fileContentForFilename(name);
		IdtechVendXDataBlob cardReaderData = IdtechVendXDataBlob.parseVendXFromFile(fileContent, cardReaderType, entryType, name);
		MessageData_C3 reply = executeAuthorize(cardReaderData);
		boolean result = authorizationResult(reply);
		
		assertTrue(result);
		log.info("junitSuccessfulTest");
	}
}