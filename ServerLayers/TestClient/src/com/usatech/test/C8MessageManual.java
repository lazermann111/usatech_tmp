package com.usatech.test;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;

import com.usatech.networklayer.ReRixMessage;

public class C8MessageManual extends C8Message {
	protected String c9FileContent;
	public C8MessageManual() {
		super();
		c9FileContent="Test Client block payload.";
	}
	public void getParams()throws Exception{
		super.getParams();
		totalNumOfBytesInFileTransfer=c9FileContent.length();
		numOfBlocksInFileTransfer=TestClientUtil.calcNumOfBlocks(MAX_PAYLOAD,totalNumOfBytesInFileTransfer);
		input=new BufferedInputStream(new ByteArrayInputStream(c9FileContent.getBytes()));
	}
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception{
		ClientConnectUtil conn=new ClientConnectUtil();
		conn.setProtocol(7);
		conn.startCommunication();
		C8MessageManual sMsg=new C8MessageManual();
		sMsg.getParams();
		ReRixMessage sentMsg=sMsg.createMessage();
		ReRixMessage responseMsg=conn.sendMessage(sentMsg,sMsg.encryptKey);
		sMsg.readResponse(responseMsg);
		conn.stopCommunication();

	}
	public String getC9FileContent() {
		return c9FileContent;
	}
	public void setC9FileContent(String fileContent) {
		c9FileContent = fileContent;
	}

}
