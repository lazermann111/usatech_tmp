package com.usatech.test;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.ParseException;
import java.util.StringTokenizer;

public abstract class SubMessage{
	protected StringTokenizer userInputTokens;
	protected long timeZoneOffset;
	public SubMessage(StringTokenizer userInputTokens){
		this.userInputTokens=userInputTokens;
	}
	public void getParams()throws Exception{
		if(userInputTokens==null){
			CommandLineUtil.getUserInput(this);
		}else{
			CommandLineUtil.getUserInputByArg(this, userInputTokens);
		}
		setTimeWithTimeZoneOffset();
	}
	public abstract void writeMessage(ByteBuffer bb);
	
	public void setTimeWithTimeZoneOffset() throws IOException, ParseException{}
		
}