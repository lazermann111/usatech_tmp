package com.usatech.test;
import java.io.IOException;
import java.io.Writer;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.StringTokenizer;

import simple.io.Log;
import simple.lang.InvalidByteArrayException;
import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.networklayer.ReRixMessage;
/**
 * Base message.
 * @author yhe
 *
 */
public abstract class BaseMessage {
	private static final Log log = Log.getLog();
	protected StringTokenizer userInputTokens;
	protected String evNumber = "";
	protected String encryptKey = "";

	protected int messageNumber;
	protected long messageId;
	protected int dataType;
	protected boolean hasResponseToServer=false;
	protected int responseMessageNumber;

	//For link with previous message
	protected BaseMessage previousMessage;
	protected HashMap<String, Object> responseMap=new HashMap<String, Object>();

	public BaseMessage(){
	}
	public void getParams()throws Exception{
		if(userInputTokens==null){
			CommandLineUtil.getUserInput(this);
		}else{
			CommandLineUtil.getUserInputByArg(this, userInputTokens);
		}
	}
	public abstract ReRixMessage createMessage() throws Exception;

	public ReRixMessage createResponseMessage() throws Exception{
		//not implemented, only implemented by one which needs to send a response to server
		return null;
	}

	public String getMessageTypeName(){
		return MessageType.getMessageDescriptionSafely((byte)dataType);
	}

	public void writeMessageHeader(ByteBuffer bb){
		ProcessingUtils.writeByteInt(bb, messageNumber);
		ProcessingUtils.writeByteInt(bb, dataType);
	}

	public ReRixMessage writeMessageEnd(ByteBuffer bb){
		bb.flip();
		byte[] data=new byte[bb.remaining()];
		bb.get(data);
		return new ReRixMessage(data, evNumber);
	}
	public StringBuilder readResponseHeader(ByteBuffer bb)throws InvalidByteValueException{
		StringBuilder sb=new StringBuilder();
		responseMessageNumber=ProcessingUtils.readByteInt(bb);
		printResponse(sb, "MessageNumber", responseMessageNumber);
		byte dataTypeByte=(byte)ProcessingUtils.readByteInt(bb);
		printResponse(sb, "DataType", MessageType.getByValue(dataTypeByte));
		return sb;
	}

	public abstract String readResponseString(ReRixMessage msg) throws InvalidByteValueException;

	public void readResponse(ReRixMessage msg)throws InvalidByteValueException{
		log.info(readResponseString(msg));
	}

	/**
	 * method that only implemented for messages that need to verify response message parameter match the sending one
	 * default one does nothing
	 */
	public void verifyResponse(){}
	public void readResponse(ReRixMessage msg, Writer writer)throws InvalidByteValueException, IOException{
		writer.write("Message Response:\n");
		if(msg==null){
			writer.write("No response.\n");
		}else{
			writer.write(readResponseString(msg));
		}
		writer.flush();
		verifyResponse();
	}

	public void logResponse(ReRixMessage msg)throws InvalidByteValueException, IOException{
		log.info("Message Response:\n");
		if(msg==null){
			log.info("No response.\n");
		}else{
			log.info(readResponseString(msg));
		}
		verifyResponse();
	}
	//simple print
	public static void printResponse(StringBuilder sb, String key, Object value){
		sb.append(key+":"+value+"\n");
	}
	public String getEvNumber() {
		return evNumber;
	}
	public void setEvNumber(String evNumber) {
		this.evNumber = evNumber;
	}
	public String getEncryptKey() {
		return encryptKey;
	}
	public void setEncryptKey(String encryptKey) {
		this.encryptKey = encryptKey;
	}
	public int getMessageNumber() {
		return messageNumber;
	}
	public void setMessageNumber(int messageNumber) {
		this.messageNumber = messageNumber;
	}
	public void setUserInputTokens(StringTokenizer userInputTokens) {
		this.userInputTokens = userInputTokens;
	}
	public long getMessageId() {
		return messageId;
	}
	public void setMessageId(long messageId) {
		this.messageId = messageId;
	}

	protected void read(ByteBuffer data) throws IOException, InvalidByteValueException {
		// do nothing
	}
	public static BaseMessage readMessage(ByteBuffer data) throws BufferUnderflowException, InvalidByteValueException, InvalidByteArrayException, IOException {
		int messageNumber = ProcessingUtils.readByteInt(data);
		MessageType messageType = MessageType.getByValue(data);
		BaseMessage message = createMessage(messageType);
		if(message != null) {
			message.setMessageNumber(messageNumber);
			message.read(data);
		}
		return message;
	}
	protected static BaseMessage createMessage(MessageType messageType) {
		switch(messageType) {
			case INITIALIZATION_4_1:
				return new C0Message();
			case AUTH_REQUEST_4_1:
				return new C2Message();
			case AUTH_RESPONSE_4_1:
				return new C3Message();
			case GENERIC_REQUEST_4_1:
				return new CAMessage();
			case GENERIC_RESPONSE_4_1:
				return new CBMessage();
			default:
				return null;
		}
	}
}