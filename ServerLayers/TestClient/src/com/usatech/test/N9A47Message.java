package com.usatech.test;

import java.nio.ByteBuffer;

import simple.bean.ConvertUtils;
import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
/**
 * 9A47 Room Status --> 2F
 * @author yhe
 *
 */
public class N9A47Message extends ESudsMessage {
	protected int numberOfBlocks;//not in spec but needed for input
	protected int[] washerDryerStatusByteBlockPort;
	public N9A47Message() {
		super();
		dataType2=0x47;
		numberOfBlocks=1;
	}
	
	@Override
	public void getParams() throws Exception{
		super.getParams();
		washerDryerStatusByteBlockPort=new int[numberOfBlocks];
		for(int i=0; i<numberOfBlocks; i++){
			if(userInputTokens==null){
				washerDryerStatusByteBlockPort[i]=ConvertUtils.getInt(CommandLineUtil.getUserInput("Please input washerDryerStatusByteBlockPort"+i),0);
			}else{
				washerDryerStatusByteBlockPort[i]=ConvertUtils.getInt(userInputTokens.nextToken(), 0);
			}
		}
	}

	@Override
	public ReRixMessage createMessage() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		for(int i=0; i<numberOfBlocks; i++){
			ProcessingUtils.writeByteInt(bb, washerDryerStatusByteBlockPort[i]);
		}
		return writeMessageEnd(bb);
	}

	@Override
	public String readResponseString(ReRixMessage msg) throws InvalidByteValueException {
		return readResponseString2F(msg);
	}

	public int getNumberOfBlocks() {
		return numberOfBlocks;
	}

	public void setNumberOfBlocks(int numberOfBlocks) {
		this.numberOfBlocks = numberOfBlocks;
	}

}
