package com.usatech.test;

import java.nio.ByteBuffer;

import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
/**
 * 7E file transfer -->7F file transfer ack
 * @author yhe
 *
 */
public class N7EMessage extends LegacyMessage {
	protected int fileTransferGroupNumber;
	protected int packetNumber;
	protected String packetPayload;
	public N7EMessage() {
		super();
		dataType=0x7E;
		fileTransferGroupNumber=1;
		packetNumber=0;
		packetPayload="0123456789";
	}

	@Override
	public ReRixMessage createMessage() throws Exception {
		int payLoadLen=packetPayload==null?0:packetPayload.length();
		ByteBuffer bb=ByteBuffer.allocate(4+payLoadLen);
		writeMessageHeader(bb);
		ProcessingUtils.writeByteInt(bb,fileTransferGroupNumber);
		ProcessingUtils.writeByteInt(bb,packetNumber);
		bb.put(packetPayload.getBytes());
		return writeMessageEnd(bb);
	}

	@Override
	public String readResponseString(ReRixMessage msg) throws InvalidByteValueException {
		ByteBuffer bb=ByteBuffer.wrap(msg.getData());
		StringBuilder sb=readResponseHeader(bb);
		printResponse(sb, "FileTransferGroupNumber", ProcessingUtils.readByteInt(bb));
		printResponse(sb, "PacketNumber", ProcessingUtils.readByteInt(bb));
		return sb.toString();
	}

	public int getFileTransferGroupNumber() {
		return fileTransferGroupNumber;
	}

	public void setFileTransferGroupNumber(int fileTransferGroupNumber) {
		this.fileTransferGroupNumber = fileTransferGroupNumber;
	}

}
