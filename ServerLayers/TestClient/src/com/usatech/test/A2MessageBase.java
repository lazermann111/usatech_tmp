package com.usatech.test;


import simple.lang.InvalidByteValueException;

import com.usatech.networklayer.ReRixMessage;
/**
 * A2 --> 71h response
 * @author yhe
 *
 */
public abstract class A2MessageBase extends LegacyMessage {
	protected long transactionId=System.currentTimeMillis()/1000;
	protected long amount;//US dollar in pennies
	protected int transactionResult;

	public A2MessageBase(){
		super();
		dataType=0xA2;
		amount=150;
		transactionResult='S';
	}
	
	@Override
	public String readResponseString(ReRixMessage msg)throws InvalidByteValueException{
		return readResponseString71(msg);
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public int getTransactionResult() {
		return transactionResult;
	}

	public void setTransactionResult(int transactionResult) {
		this.transactionResult = transactionResult;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
}
