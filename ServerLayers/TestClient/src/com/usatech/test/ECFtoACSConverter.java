package com.usatech.test;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Pattern;

import simple.app.Base;
import simple.io.Log;
import simple.text.FixedWidthColumn;
import simple.text.MultiRecordFixedWidthParser;
import simple.text.StringUtils;
import simple.text.ThreadSafeDateFormat;
import simple.text.StringUtils.Justification;

import com.usatech.app.AbstractAttributeDatasetHandler;
import com.usatech.app.AttributeConversionException;

public class ECFtoACSConverter extends Base {
	private static final Log log = Log.getLog();
	protected final MultiRecordFixedWidthParser<String> parser = new MultiRecordFixedWidthParser<String>();
	protected final DecimalFormat batchAmountFormat = new DecimalFormat("#######0.00");
	protected final ThreadSafeDateFormat batchDateFormat = new ThreadSafeDateFormat(new SimpleDateFormat("ssmmHHyyyyMMdd"));
	protected final ThreadSafeDateFormat settleDateFormat = new ThreadSafeDateFormat(new SimpleDateFormat("yyyyMMdd"));
    protected final ThreadSafeDateFormat settleTimeFormat = new ThreadSafeDateFormat(new SimpleDateFormat("mmHH"));
    
	protected class ACSWriter extends AbstractAttributeDatasetHandler<IOException> {
		protected final Writer writer;
		protected final AtomicLong batchAmount = new AtomicLong();
		protected final Object batchAmountText = new Object() {
			@Override
			public String toString() {
				return batchAmountFormat.format(batchAmount.get() / 100.0);
			}
		};
		protected final AtomicInteger batchCount = new AtomicInteger();
		protected final List<Object[]> rowCache = new ArrayList<Object[]>();
		protected Date settlementDate;
		protected String batchNumber;
		protected String terminalNumber;
		 
		public ACSWriter(Writer writer) {
			this.writer = writer;
		}

		@Override
		public void handleDatasetStart(String[] columnNames) throws IOException {
		}
		@Override
		public void handleRowEnd() throws IOException {
			if(log.isDebugEnabled())
				log.debug("Handling row: " + data);
			try {
				String recordType = getDetailAttribute("recordType", String.class, true);
				if("BHR".equals(recordType)) {
					settlementDate = new Date();
					batchAmount.set(0);
					batchCount.set(0);
					batchNumber = getDetailAttribute("batchNumber", String.class, true);
					terminalNumber = "001060999" + StringUtils.pad(getDetailAttribute("merchantId", String.class, true), '0', 13, Justification.LEFT);
					cacheRow(new Object[] {
						"BH",
						settleDateFormat.format(settlementDate),
						terminalNumber,
						StringUtils.pad(getDetailAttribute("batchNumber", String.class, true), '0', 3, Justification.RIGHT),
						"0000", //FI# ???
						"0000", //Agent # ????
						getDetailAttribute("merchantId", String.class, true),
						batchNumber,
						settleTimeFormat.format(settlementDate),
						"H",
						"N",
						"NIFKVGYA",
						batchAmount,
						batchCount,
						"0000", // Chain ???
						"R", // Sic Cat
						"O", // Batch Status						
					});
				} else if("DTR".equals(recordType)) {
					String transactionCode = getDetailAttribute("transactionCode", String.class, true);
					long settleAmount = getDetailAttribute("settlementAmount", Long.class, true);
					batchAmount.addAndGet(settleAmount);
					String tranType;
					switch(transactionCode.charAt(0)) {
						case 'S':
							tranType = "01"; break; // Sale
						case 'X':
							tranType = "37"; break; // Void
						case 'R':
							tranType = "20"; break; // Return
						default:
							throw new IOException("Invalid transaction code '" + transactionCode + "'");
					}
					cacheRow(new Object[] {
							"SI",
							settleDateFormat.format(settlementDate),
							terminalNumber,
							StringUtils.pad(batchNumber, '0', 3, Justification.RIGHT),
							StringUtils.pad(String.valueOf(batchCount.incrementAndGet()), '0', 3, Justification.RIGHT),
							tranType, 
							getDetailAttribute("cardNumber", String.class, true),
							getDetailAttribute("cardType", String.class, true),
							batchAmountFormat.format(settleAmount / 100.0),
							getDetailAttribute("approvalCode", String.class, true),
							getDetailAttribute("authorizationDate", String.class, true).substring(2, 8),
							getDetailAttribute("authorizationDate", String.class, true).substring(8),
							null, // Custom Data ???
							"S", //Input Method ???
							getDetailAttribute("authSourceCode", String.class, true),
							settleAmount == 0 ? "Y" : "N", //Void Indicator
							null, // VPS2K Data ???
							getDetailAttribute("avsResult", String.class, true),
							batchAmountFormat.format(getDetailAttribute("authAmount", Long.class, true) / 100.0),
							getDetailAttribute("referenceNumber", String.class, true),						
						});
				} else if("BTR".equals(recordType)) {
					flushRows();
				}
			} catch(AttributeConversionException e) {
				throw new IOException(e);
			}
		}
		@Override
		public void handleDatasetEnd() throws IOException {
			writer.flush();
			writer.close();
		}
		protected void writeRow(Object[] values) throws IOException {
			for(Object val : values) {
				if(val != null)
					writer.write(val.toString());
				writer.write('^');
			}
			writer.write("\r\n");
		}
		protected void cacheRow(Object[] values) {
			rowCache.add(values);
		}
		protected void flushRows() throws IOException {
			Iterator<Object[]> iter = rowCache.iterator();
			while(iter.hasNext()) {
				writeRow(iter.next());
				iter.remove();
			}
		}
	}
	public ECFtoACSConverter() {
		parser.setSelectorColumn("recordType", 3, Justification.LEFT, ' ', String.class);
		parser.setRecordColumns("FHR", new FixedWidthColumn[] {
				new FixedWidthColumn(14, Justification.LEFT, ' ', "fileCreateDate", String.class),
				new FixedWidthColumn(2, Justification.LEFT, ' ', "version", String.class),
				new FixedWidthColumn(4, Justification.LEFT, ' ', "destinationId", String.class),
				new FixedWidthColumn(25, Justification.LEFT, ' ', "sendingInstitutionName", String.class),
				new FixedWidthColumn(32, Justification.LEFT, ' ', "fileName", String.class),
				new FixedWidthColumn(120, Justification.LEFT, ' ', "reserved0", String.class),
		});
		parser.setRecordColumns("BHR", new FixedWidthColumn[] {
				new FixedWidthColumn(16, Justification.LEFT, ' ', "merchantId", String.class),
				new FixedWidthColumn(25, Justification.LEFT, ' ', "merchantDBA", String.class),
				new FixedWidthColumn(13, Justification.LEFT, ' ', "merchantCity", String.class),
				new FixedWidthColumn(2, Justification.LEFT, ' ', "merchantState", String.class),
				new FixedWidthColumn(9, Justification.LEFT, ' ', "merchantZip", String.class),
				new FixedWidthColumn(3, Justification.LEFT, ' ', "merchantCountry", String.class),
				new FixedWidthColumn(4, Justification.LEFT, ' ', "reserved1", String.class),
				new FixedWidthColumn(8, Justification.LEFT, ' ', "settlementDate", String.class),
				new FixedWidthColumn(11, Justification.RIGHT, '0', "batchNumber", Long.class),
				new FixedWidthColumn(4, Justification.LEFT, ' ', "networkIdentifier", String.class),
				new FixedWidthColumn(11, Justification.RIGHT, '0', "batchReferenceNumber", Long.class),
				new FixedWidthColumn(4, Justification.LEFT, ' ', "clientGroup", String.class),
				new FixedWidthColumn(12, Justification.LEFT, ' ', "mpsNumber", String.class),
				new FixedWidthColumn(2, Justification.LEFT, ' ', "batchResponseCode", String.class),
				new FixedWidthColumn(1, Justification.LEFT, ' ', "batchType", String.class),
				new FixedWidthColumn(72, Justification.LEFT, ' ', "reserved2", String.class),
		});
		parser.setRecordColumns("DTR", new FixedWidthColumn[] {
				new FixedWidthColumn(1, Justification.LEFT, ' ', "transactionCode", String.class),
				new FixedWidthColumn(2, Justification.LEFT, ' ', "cardType", String.class),
				new FixedWidthColumn(20, Justification.LEFT, ' ', "cardNumber", String.class),
				new FixedWidthColumn(4, Justification.LEFT, ' ', "expirationDate", String.class),
				new FixedWidthColumn(10, Justification.RIGHT, '0', "settlementAmount", Long.class),
				new FixedWidthColumn(14, Justification.LEFT, ' ', "authorizationDate", String.class),
				new FixedWidthColumn(6, Justification.LEFT, ' ', "approvalCode", String.class),
				new FixedWidthColumn(2, Justification.LEFT, ' ', "posEntryMode", String.class),
				new FixedWidthColumn(1, Justification.LEFT, ' ', "authSourceCode", String.class),
				new FixedWidthColumn(1, Justification.LEFT, ' ', "cardHolderIdMethod", String.class),
				new FixedWidthColumn(1, Justification.LEFT, ' ', "cardPresentIndicator", String.class),
				new FixedWidthColumn(11, Justification.RIGHT, '0', "referenceNumber", String.class),
				new FixedWidthColumn(1, Justification.LEFT, ' ', "visaACI", String.class),
				new FixedWidthColumn(15, Justification.LEFT, ' ', "cardTransactionId", String.class),
				new FixedWidthColumn(4, Justification.LEFT, ' ', "cardValidationCode", String.class),
				new FixedWidthColumn(1, Justification.LEFT, ' ', "modeIndicator", String.class),
				new FixedWidthColumn(1, Justification.LEFT, ' ', "cardholdActivatedTerminal", String.class),
				new FixedWidthColumn(1, Justification.LEFT, ' ', "posCapability", String.class),
				new FixedWidthColumn(2, Justification.LEFT, ' ', "responseCode", String.class),
				new FixedWidthColumn(3, Justification.LEFT, ' ', "currencyCode", String.class),
				new FixedWidthColumn(8, Justification.RIGHT, '0', "authAmount", Long.class),
				new FixedWidthColumn(1, Justification.LEFT, ' ', "avsResult", String.class),
				new FixedWidthColumn(1, Justification.LEFT, ' ', "purchaseIdentifierFormatCode", String.class),
				new FixedWidthColumn(2, Justification.LEFT, ' ', "debitNetworkId", String.class),
				new FixedWidthColumn(4, Justification.LEFT, ' ', "debitSettlementDate", String.class),
				new FixedWidthColumn(4, Justification.LEFT, ' ', "merchantCategory", String.class),
				new FixedWidthColumn(4, Justification.LEFT, ' ', "itemNumber", String.class),
				new FixedWidthColumn(1, Justification.LEFT, ' ', "msdiIndicator", String.class),
				new FixedWidthColumn(1, Justification.LEFT, ' ', "ucafIndicator", String.class),
				new FixedWidthColumn(8, Justification.RIGHT, '0', "debitSurcharge", Long.class),
				new FixedWidthColumn(13, Justification.RIGHT, '0', "waybillNumber", Long.class),
				new FixedWidthColumn(15, Justification.LEFT, ' ', "terminalCd", String.class),
				new FixedWidthColumn(4, Justification.LEFT, ' ', "ecsChargeType", String.class),
				new FixedWidthColumn(8, Justification.RIGHT, '0', "convenienceFee", Long.class),
				new FixedWidthColumn(1, Justification.LEFT, ' ', "cvvResult", String.class),
				new FixedWidthColumn(2, Justification.LEFT, ' ', "accountLevelProcessing", String.class),
				new FixedWidthColumn(19, Justification.LEFT, ' ', "reserved3", String.class),
			});
		parser.setRecordColumns("BTR", new FixedWidthColumn[] {
				new FixedWidthColumn(6, Justification.RIGHT, '0', "batchSaleCount", Integer.class),
				new FixedWidthColumn(11, Justification.RIGHT, '0', "batchSaleAmount", Long.class),
				new FixedWidthColumn(6, Justification.RIGHT, '0', "batchRefundCount", Integer.class),
				new FixedWidthColumn(11, Justification.RIGHT, '0', "batchRefundAmount", Long.class),
				new FixedWidthColumn(6, Justification.RIGHT, '0', "batchTotalCount", Integer.class),
				new FixedWidthColumn(11, Justification.RIGHT, '0', "batchTotalAmount", Long.class),
				new FixedWidthColumn(1, Justification.LEFT, ' ', "batchNetSign", String.class),
				new FixedWidthColumn(11, Justification.RIGHT, '0', "batchNumber", Long.class),
				new FixedWidthColumn(134, Justification.LEFT, ' ', "reserved4", String.class),
		});
		parser.setRecordColumns("FTR", new FixedWidthColumn[] {
				new FixedWidthColumn(6, Justification.RIGHT, '0', "fileSaleCount", Integer.class),
				new FixedWidthColumn(11, Justification.RIGHT, '0', "fileSaleAmount", Long.class),
				new FixedWidthColumn(6, Justification.RIGHT, '0', "fileRefundCount", Integer.class),
				new FixedWidthColumn(11, Justification.RIGHT, '0', "fileRefundAmount", Long.class),
				new FixedWidthColumn(6, Justification.RIGHT, '0', "fileTotalCount", Integer.class),
				new FixedWidthColumn(11, Justification.RIGHT, '0', "fileTotalAmount", Long.class),
				new FixedWidthColumn(1, Justification.LEFT, ' ', "fileNetSign", String.class),
				new FixedWidthColumn(6, Justification.RIGHT, '0', "fileRecordCount", Integer.class),
				new FixedWidthColumn(14, Justification.LEFT, ' ', "fileCreateDate", String.class),
				new FixedWidthColumn(125, Justification.LEFT, ' ', "reserved5", String.class),
		});		
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new ECFtoACSConverter().run(args);
	}
	
	@Override
	public Properties getProperties(String propertiesFile, Class<?> appClass, Properties defaults) throws IOException {
		return new Properties(defaults);
	}

	@Override
	protected void execute(Map<String, Object> argMap, Properties properties) {
		String tmp = properties.getProperty("inputDirectory");
		if(tmp == null || (tmp=tmp.trim()).length() == 0) {
			finishAndExit("Input Directory not provided", 101, null);
			return;
		}
		File inputDirectory = new File(tmp);
		if(!inputDirectory.isDirectory()) {
			finishAndExit("Input Directory '" + getFilePathSafetly(inputDirectory) + "' is not a directory", 103, null);
			return;
		}
		tmp = properties.getProperty("outputDirectory");
		if(tmp == null || (tmp=tmp.trim()).length() == 0) {
			finishAndExit("Output Directory not provided", 102, null);
			return;
		}
		File outputDirectory = new File(tmp);
		if(!outputDirectory.isDirectory()) {
			finishAndExit("Output Directory '" + getFilePathSafetly(outputDirectory) + "' is not a directory", 104, null);
			return;
		}
		tmp = properties.getProperty("processedDirectory", getFilePathSafetly(inputDirectory) + "/processed");
		if(tmp == null || (tmp=tmp.trim()).length() == 0) {
			finishAndExit("Processed Directory not provided", 106, null);
			return;
		}
		File processedDirectory = new File(tmp);
		if(!processedDirectory.exists())
			processedDirectory.mkdirs();
		if(!processedDirectory.isDirectory()) {
			finishAndExit("Processed Directory '" + getFilePathSafetly(processedDirectory) + "' is not a directory", 108, null);
			return;
		}
		
		tmp = properties.getProperty("ecfPattern", "P8009\\d{12}\\.dep");
		final Pattern filenamePattern = Pattern.compile(tmp);
		File[] ecfFiles = inputDirectory.listFiles(new FilenameFilter() {			
			@Override
			public boolean accept(File dir, String name) {
				return filenamePattern.matcher(name).matches();
			}
		});
		
		for(File ecfFile : ecfFiles) {
			parseECFFile(ecfFile, outputDirectory, processedDirectory);
		}
	}
	
	protected void parseECFFile(File ecfFile, File outputDirectory, File processedDirectory) {
		long time = System.currentTimeMillis();
		File acsFile;
		do {
			acsFile = new File(outputDirectory, batchDateFormat.format(new Date(time)) + ".EDC");
			time += 1000;
		} while(acsFile.exists()) ;
		try {
			FileWriter writer = new FileWriter(acsFile);
			log.debug("Parsing ECF file '" + getFilePathSafetly(ecfFile) + "' into ACS file '" + getFilePathSafetly(acsFile) + "'");
			Reader reader = new FileReader(ecfFile);
			try {
				parser.parse(reader, new ACSWriter(writer));
			} finally {
				reader.close();
			}
			log.debug("Successfully parsed ECF file '" + getFilePathSafetly(ecfFile) + "' into ACS file '" + getFilePathSafetly(acsFile) + "'");
			if(processedDirectory != null) {
				File newFile = new File(processedDirectory, ecfFile.getName());
				if(ecfFile.renameTo(newFile))
					log.debug("Moved processed ECF file '" + getFilePathSafetly(ecfFile) + "' to '" + newFile +"'");
				else
					log.warn("Could not move processed ECF file '" + getFilePathSafetly(ecfFile) + "' to '" + newFile +"'");
			}			
		} catch(IOException e) {
			log.error("Failed to parse ECF file '" + getFilePathSafetly(ecfFile) + "' or to write ACS file '" + getFilePathSafetly(acsFile) + "'", e);
		}
	}
	protected String getFilePathSafetly(File file) {
		try {
			return file.getCanonicalPath();
		} catch(IOException e) {
			return file.getAbsolutePath();
		}
	}
	@Override
	protected void registerDefaultCommandLineArguments() {
		registerCommandLineSwitch('i', "input-dir", false, true, "inputDirectory", "The input directory that holds the ECF files");
		registerCommandLineSwitch('o', "output-dir", false, true, "outputDirectory", "The output directory into which the ACS files will be put");
		registerCommandLineSwitch('e', "ecf-pattern", true, true, "ecfPattern", "The regular expression used to find ECF files ['P8009\\d{12}\\.dep' by default]");
		registerCommandLineSwitch('p', "processed-dir", true, true, "processedDirectory", "The directory into which ECF files are moved once successfully processed");
	}
}
