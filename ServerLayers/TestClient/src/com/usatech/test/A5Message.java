package com.usatech.test;

import java.nio.ByteBuffer;

import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
public class A5Message extends BaseMessage {
	int group;
	public A5Message() {
		super();
		dataType=0xA5;
	}

	@Override
	public ReRixMessage createMessage() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		ProcessingUtils.writeByteInt(bb, group);
		return writeMessageEnd(bb);
	}

	public int getGroup() {
		return group;
	}

	public void setGroup(int group) {
		this.group = group;
	}

	@Override
	public String readResponseString(ReRixMessage msg) throws InvalidByteValueException {
		// TODO Auto-generated method stub
		return null;
	}
}
