package com.usatech.test;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.StringTokenizer;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
/**
 *
 * @author yhe
 *
 */
public class C1Message extends V4Message {
	protected BaseComponentInfoBlock baseComponent;
	protected int numOfComponentInfoBlocks;
	protected ArrayList<ComponentInfoBlock> componentInfo=new ArrayList<ComponentInfoBlock>(5);

	protected class BaseComponentInfoBlock extends SubMessage{
		//only base component type is allowed
		protected int componentType;
		protected String defaultCurrency;
		protected final InformationBlock infoBlock;
		public BaseComponentInfoBlock(StringTokenizer userInputTokens){
			super(userInputTokens);
			infoBlock = new InformationBlock(userInputTokens);
			componentType=201;//edge
			defaultCurrency="USD";
		}
		public void getParams()throws Exception{
			super.getParams();
			infoBlock.getParams();
		}
		public void writeMessage(ByteBuffer bb){
			ProcessingUtils.writeShortInt(bb, componentType );
			ProcessingUtils.writeString(bb, defaultCurrency, ProcessingConstants.US_ASCII_CHARSET);
			infoBlock.writeMessage(bb);
		}
		public int retrieveLength(){
			return 5+infoBlock.retrieveLength();
		}
		public int getComponentType() {
			return componentType;
		}
		public void setComponentType(int componentType) {
			this.componentType = componentType;
		}
		public String getDefaultCurrency() {
			return defaultCurrency;
		}
		public void setDefaultCurrency(String defaultCurrency) {
			this.defaultCurrency = defaultCurrency;
		}
		public InformationBlock getInfoBlock() {
			return infoBlock;
		}
	}
	protected class InformationBlock extends SubMessage{
		protected String manufacture;
		protected String model;
		protected String serialNum;
		protected String revision;
		protected String label;
		public InformationBlock(StringTokenizer userInputTokens){
			super(userInputTokens);
			manufacture="Test Client manufacture";
			model="Test Client model";
			serialNum="Test Client serialNum";
			revision="Test Client revision";
			label="Test Client label";
		}
		public void writeMessage(ByteBuffer bb){
			ProcessingUtils.writeShortString(bb, manufacture, ProcessingConstants.US_ASCII_CHARSET);
			ProcessingUtils.writeShortString(bb, model, ProcessingConstants.US_ASCII_CHARSET);
			ProcessingUtils.writeShortString(bb, serialNum, ProcessingConstants.US_ASCII_CHARSET);
			ProcessingUtils.writeShortString(bb, revision, ProcessingConstants.US_ASCII_CHARSET);
			ProcessingUtils.writeShortString(bb, label, ProcessingConstants.US_ASCII_CHARSET);
		}
		public int retrieveLength(){
			int len=5;
			if(manufacture!=null){
				len+=manufacture.length();
			}
			if(model!=null){
				len+=model.length();
			}
			if(serialNum!=null){
				len+=serialNum.length();
			}
			if(revision!=null){
				len+=revision.length();
			}
			if(label!=null){
				len+=label.length();
			}
			return len;
		}
		public String getManufacture() {
			return manufacture;
		}
		public void setManufacture(String manufacture) {
			this.manufacture = manufacture;
		}
		public String getModel() {
			return model;
		}
		public void setModel(String model) {
			this.model = model;
		}
		public String getSerialNum() {
			return serialNum;
		}
		public void setSerialNum(String serialNum) {
			this.serialNum = serialNum;
		}
		public String getRevision() {
			return revision;
		}
		public void setRevision(String revision) {
			this.revision = revision;
		}
		public String getLabel() {
			return label;
		}
		public void setLabel(String label) {
			this.label = label;
		}
	}

	protected class ComponentInfoBlock extends SubMessage{
		protected int componentType;
		protected int componentNum;
		protected InformationBlock infoBlock;
		public ComponentInfoBlock(StringTokenizer userInputTokens){
			super(userInputTokens);
			componentType=201;//edge
			componentNum=0;
		}
		public void getParams()throws Exception{
			super.getParams();
			infoBlock=new InformationBlock(userInputTokens);
			infoBlock.getParams();
		}

		public void writeMessage(ByteBuffer bb){
			ProcessingUtils.writeShortInt(bb, componentType );
			ProcessingUtils.writeByteInt(bb, componentNum );
			infoBlock.writeMessage(bb);
		}

		public int retrieveLength(){
			return 3+infoBlock.retrieveLength();
		}
		public int getComponentType() {
			return componentType;
		}
		public void setComponentType(int componentType) {
			this.componentType = componentType;
		}
		public int getComponentNum() {
			return componentNum;
		}
		public void setComponentNum(int componentNum) {
			this.componentNum = componentNum;
		}
		public InformationBlock getInfoBlock() {
			return infoBlock;
		}
		public void setInfoBlock(InformationBlock infoBlock) {
			this.infoBlock = infoBlock;
		}

	}
	public C1Message() {
		super();
		dataType=0xC1;
		numOfComponentInfoBlocks=1;
	}

	@Override
	public void getParams() throws Exception{
		super.getParams();
		baseComponent=new BaseComponentInfoBlock(userInputTokens);
		baseComponent.getParams();
		for(int i=0; i<numOfComponentInfoBlocks; i++){
			ComponentInfoBlock com=new ComponentInfoBlock(userInputTokens);
			com.getParams();
			componentInfo.add(com);
		}
	}

	@Override
	public ReRixMessage createMessage() {
		int len=7+baseComponent.retrieveLength();
		for(ComponentInfoBlock com: componentInfo){
			len+=com.retrieveLength();
		}
		ByteBuffer bb=ByteBuffer.allocate(len);
		writeMessageHeader(bb);
		baseComponent.writeMessage(bb);
		bb.put((byte) componentInfo.size());
		for(ComponentInfoBlock com: componentInfo){
			com.writeMessage(bb);
		}
		return writeMessageEnd(bb);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception{
		ClientConnectUtil conn=new ClientConnectUtil();
		conn.setProtocol(7);
		conn.startCommunication();
		C1Message sMsg=new C1Message();
		sMsg.getParams();
		ReRixMessage sentMsg=sMsg.createMessage();
		ReRixMessage responseMsg=conn.sendMessage(sentMsg, sMsg.encryptKey);
		sMsg.readResponse(responseMsg);
		conn.stopCommunication();
	}

	public BaseComponentInfoBlock getBaseComponent() {
		return baseComponent;
	}

	public int getNumOfComponentInfoBlocks() {
		return numOfComponentInfoBlocks;
	}

	public void setNumOfComponentInfoBlocks(int numOfComponentInfoBlocks) {
		this.numOfComponentInfoBlocks = numOfComponentInfoBlocks;
	}

	public ArrayList<ComponentInfoBlock> getComponentInfo() {
		return componentInfo;
	}

	public void setComponentInfo(ArrayList<ComponentInfoBlock> componentInfo) {
		this.componentInfo = componentInfo;
	}

}
