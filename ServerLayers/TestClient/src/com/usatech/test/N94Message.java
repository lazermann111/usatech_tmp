package com.usatech.test;

import java.nio.ByteBuffer;
import java.util.Date;

import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;

public class N94Message extends LegacyMessage {
	protected long cashSaleBatchId;
	protected Date dateTimeOfCashSale=new Date();
	protected int amountOfCashSale;//3 bytes
	protected int columnNumberOfCashSale;
	
	public N94Message(){
		super();
		dataType=0x94;
		cashSaleBatchId=1234567;
		amountOfCashSale=350;
		columnNumberOfCashSale=5;
	}
	
	@Override
	public ReRixMessage createMessage() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		ProcessingUtils.writeLongInt(bb, cashSaleBatchId);
		bb.put(TestClientUtil.getBCDDateTime(dateTimeOfCashSale));
		ProcessingUtils.write3ByteInt(bb, amountOfCashSale);
		ProcessingUtils.writeByteInt(bb, columnNumberOfCashSale);
		return writeMessageEnd(bb);
	}

	@Override
	public String readResponseString(ReRixMessage msg) throws InvalidByteValueException {
		return readResponseString95(msg);
	}

	public long getCashSaleBatchId() {
		return cashSaleBatchId;
	}

	public void setCashSaleBatchId(long cashSaleBatchId) {
		this.cashSaleBatchId = cashSaleBatchId;
	}

	public int getAmountOfCashSale() {
		return amountOfCashSale;
	}

	public void setAmountOfCashSale(int amountOfCashSale) {
		this.amountOfCashSale = amountOfCashSale;
	}

	public int getColumnNumberOfCashSale() {
		return columnNumberOfCashSale;
	}

	public void setColumnNumberOfCashSale(int columnNumberOfCashSale) {
		this.columnNumberOfCashSale = columnNumberOfCashSale;
	}

}
