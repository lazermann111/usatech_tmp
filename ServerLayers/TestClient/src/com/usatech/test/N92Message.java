package com.usatech.test;

import java.nio.ByteBuffer;

import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
/**
 * 92 terminal update status request--> 90h response
 * @author yhe
 *
 */
public class N92Message extends BaseMessage {

	public N92Message() {
		super();
		dataType=0x92;
	}

	@Override
	public ReRixMessage createMessage() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		return writeMessageEnd(bb);
	}

	@Override
	public String readResponseString(ReRixMessage msg) throws InvalidByteValueException {
		ByteBuffer bb=ByteBuffer.wrap(msg.getData());
		StringBuilder sb=readResponseHeader(bb);
		int terminalUpdateStatus=ProcessingUtils.readByteInt(bb);
		String tStatus;
		if(terminalUpdateStatus==0){
			tStatus="Nothing to Send";
		}else if (terminalUpdateStatus==1){
			tStatus="15 sec. wait";
		}else{
			tStatus="Reserved:"+terminalUpdateStatus;
		}
		printResponse(sb, "TerminalUpdateStatus", tStatus);
		return sb.toString();
	}

}
