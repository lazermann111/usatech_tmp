package com.usatech.test;

import java.nio.ByteBuffer;

import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.ABPermissionActionCode;
import com.usatech.layers.common.constants.PermissionResponseCodeAB;
import com.usatech.networklayer.ReRixMessage;
/**
 * AA Permission Request V1.0 --> AB response
 * AB response is not the same as spec
 * @author yhe
 *
 */
public class AAMessage extends LegacyMessage {
	private static final Log log = Log.getLog();
	protected long transactionId=System.currentTimeMillis()/1000;
	protected int entryMethod;
	protected int validationDataLength;
	protected String validationData;
	protected String accountData;
	
	public AAMessage() {
		super();
		dataType=0xAA;
		entryMethod='S';
		validationDataLength=16;
		validationData="0123456789123456";
		accountData="1234567";
	}

	@Override
	public ReRixMessage createMessage() throws Exception {
		validationDataLength=validationData.length();
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		ProcessingUtils.writeLongInt(bb, transactionId);
		ProcessingUtils.writeByteInt(bb, entryMethod);
		ProcessingUtils.writeByteInt(bb, validationDataLength);
		ProcessingUtils.writeString(bb, validationData, ProcessingConstants.US_ASCII_CHARSET);
		ProcessingUtils.writeString(bb, accountData, ProcessingConstants.US_ASCII_CHARSET);
		return writeMessageEnd(bb);
	}

	@Override
	public String readResponseString(ReRixMessage msg) throws InvalidByteValueException {
		ByteBuffer bb=ByteBuffer.wrap(msg.getData());
		StringBuilder sb=readResponseHeader(bb);
		long transactionId=ProcessingUtils.readLongInt(bb);
		printResponse(sb, "TransactionId", transactionId);
		responseMap.put("transactionId", transactionId);
		int responseCode=ProcessingUtils.readByteInt(bb);
		printResponse(sb, "ResponseCode", PermissionResponseCodeAB.getByValue((byte)responseCode));
		/**
		if(responseCode==0||responseCode==1){
			int actionCode=ProcessingUtils.readByteInt(bb);
			printResponse(sb, "ActionCode", ABPermissionActionCode.getByValue((byte)actionCode));
			if(actionCode==5){
				printResponse(sb, "ActionCodeParameters", ProcessingUtils.readByteInt(bb));
			}
		}
		*/
		return sb.toString();
	}
	
	@Override
	public void verifyResponse(){
		long responseTransactionId=ConvertUtils.getLongSafely(responseMap.get("transactionId"), -1);
		if(transactionId!=responseTransactionId){
			log.error("Response transactionId:"+responseTransactionId+" does not match transactionId:"+transactionId);
		}
	}

	public int getEntryMethod() {
		return entryMethod;
	}

	public void setEntryMethod(int entryMethod) {
		this.entryMethod = entryMethod;
	}

	public String getValidationData() {
		return validationData;
	}

	public void setValidationData(String validationData) {
		this.validationData = validationData;
	}

	public String getAccountData() {
		return accountData;
	}

	public void setAccountData(String accountData) {
		this.accountData = accountData;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

}
