package com.usatech.test;

import java.nio.ByteBuffer;

import com.usatech.layers.common.ProcessingUtils;
import simple.lang.InvalidByteValueException;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.networklayer.ReRixMessage;

public class NetLayerHealthCheck extends BaseMessage {
	protected int dataType2;
	public NetLayerHealthCheck() {
		super();
		dataType=0x00;
		dataType2=0x05;
	}

	@Override
	public ReRixMessage createMessage() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		return writeMessageEnd(bb);
	}
	
	@Override
	public void writeMessageHeader(ByteBuffer bb){
		ProcessingUtils.writeByteInt(bb, messageNumber);
		ProcessingUtils.writeByteInt(bb, dataType);
		ProcessingUtils.writeByteInt(bb, dataType2);
	}
	
	@Override
	public String getMessageTypeName(){
		byte[] dataTypes=new byte[2];
		dataTypes[0]=(byte)dataType;
		dataTypes[1]=(byte)dataType2;
		return MessageType.getMessageDescriptionSafely(dataTypes);
	}
	
	@Override
	public String readResponseString(ReRixMessage msg) throws InvalidByteValueException {
		ByteBuffer bb=ByteBuffer.wrap(msg.getData());
		StringBuilder sb=new StringBuilder();
		responseMessageNumber=ProcessingUtils.readByteInt(bb);
		printResponse(sb, "MessageNumber", responseMessageNumber);
		return sb.toString();
	}
}
