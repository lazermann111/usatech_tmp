package com.usatech.test;

import java.nio.ByteBuffer;
import java.util.Date;

import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
/**
 * 82 Client to Server Request -->2Fh
 * @author yhe
 *
 */
public class N82Message extends LegacyMessage {
	/**
	 * 0 unix time 	-->84h
	 * 1 BCD time 	-->85h
	 * 2 Gx Configuration in Gx Poke --> 88h spec says but unifiedLayer send back 2F
	 */
	protected int requestNumber;
	public N82Message() {
		super();
		dataType=0x82;
		requestNumber=0;
		hasResponseToServer=true;
	}

	@Override
	public ReRixMessage createMessage() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		ProcessingUtils.writeByteInt(bb, requestNumber);
		return writeMessageEnd(bb);
	}
	
	@Override
	public ReRixMessage createResponseMessage() throws Exception {
		N2FMessage m2F=new N2FMessage();
		m2F.setAckedMessageNumber(responseMessageNumber);
		return m2F.createMessage();
	}
	/**
	 * 84h response for unix time
	 * @param msg
	 * @return
	 * @throws InvalidByteValueException
	 */
	public String readResponseString84(ReRixMessage msg) throws InvalidByteValueException {
		ByteBuffer bb=ByteBuffer.wrap(msg.getData());
		StringBuilder sb=readResponseHeader(bb);
		printResponse(sb, "TimeInUnixFormat", new Date(ProcessingUtils.readLongInt(bb)*1000));
		return sb.toString();
	}
	
	//BCD time
	public String readResponseString85(ReRixMessage msg) throws InvalidByteValueException {
		ByteBuffer bb=ByteBuffer.wrap(msg.getData());
		StringBuilder sb=readResponseHeader(bb);
		String bcdTime=TestClientUtil.getBCDTime(bb);
		printResponse(sb, "TimeInAsciiFormatBCD",bcdTime);
		return sb.toString();
	}
	//Gx Poke
	public String readResponseString88(ReRixMessage msg) throws InvalidByteValueException {
		ByteBuffer bb=ByteBuffer.wrap(msg.getData());
		StringBuilder sb=readResponseHeader(bb);
		int memoryCode=ProcessingUtils.readByteInt(bb);
		printResponse(sb, "MemoryCode", memoryCode);
		printResponse(sb, "StartingAddress", ProcessingUtils.readString(bb, 4, ProcessingConstants.US_ASCII_CHARSET));
		printResponse(sb, "NumberOfBytesToRead", ProcessingUtils.readString(bb, 4, ProcessingConstants.US_ASCII_CHARSET));
		return sb.toString();
	}
	@Override
	public String readResponseString(ReRixMessage msg) throws InvalidByteValueException {
		if(requestNumber==0){
			return readResponseString84(msg);
		}else if(requestNumber==1){
			return readResponseString85(msg);
		}else{
			//spec says 88 but 2F is returned
			return readResponseString2F(msg);
		}
	}

	public int getRequestNumber() {
		return requestNumber;
	}

	public void setRequestNumber(int requestNumber) {
		this.requestNumber = requestNumber;
	}

}
