/**
 *
 */
package com.usatech.test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.nio.ByteBuffer;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import simple.app.Base;
import simple.app.MainWithConfig;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.NotEnoughRowsException;
import simple.io.ByteArrayUtils;
import simple.io.Log;
import simple.io.PassThroughOutputStream;
import simple.results.BeanException;
import simple.results.Results;

import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageDataFactory;
import com.usatech.layers.common.messagedata.MessageDirection;


/**
 * @author Brian S. Krug
 *
 */
public class DeviceMessageViewer extends MainWithConfig {
	private static final Log log = Log.getLog();
	@Override
	protected void registerDefaultCommandLineArguments() {
		registerCommandLineSwitch('p', "propertiesFile", true, true, "properties-file", "The properties file to use");
		registerCommandLineSwitch('f', "outputFile", true, true, "output-file-prefix", "The prefix of the file to which output is written");
		}
	@Override
	protected void registerDefaultActions() {
		//None
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new DeviceMessageViewer().run(args);
	}

	@Override
	public void run(String[] args) {
		Map<String,Object> argMap;
		try {
			argMap = parseArguments(args);
		} catch(IOException e) {
			Log.finish();
			System.err.println(e.getMessage());
			printUsage(System.err);
			System.exit(100);
			return;
		}
		Properties properties;
		try {
			properties = getProperties(ConvertUtils.getStringSafely(argMap.get("properties-file")), getClass(), null);
		} catch(IOException e) {
			Log.finish();
			System.err.println("Could not read properties file");
			e.printStackTrace();
			System.exit(120);
			return;
		}
		for(Map.Entry<String,Object> entry : argMap.entrySet()) {
			properties.put(entry.getKey(), entry.getValue());
		}

		log.info("Loading configuration...");
		try {
			Base.configureDataSourceFactory(properties, null);
	        Base.configureDataLayer(properties);
	    } catch(ServiceException e) {
			Log.finish();
			System.err.println("Could not configure the data layer");
			e.printStackTrace();
			System.exit(150);
			return;
		}
	    String outputFile = properties.getProperty("output-file-prefix");
	    if(outputFile != null)
	    	outputFile = outputFile.trim();
	    else
	    	outputFile = "";
	    PrintStream out = System.out;
		log.info("Configuration loaded. Listening on Standard Input for commands");
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		StringBuilder fileName = new StringBuilder(outputFile);
		try {
			OUTER: while(true) {
				String line;
				Calendar startDate;
				Calendar endDate;
				fileName.setLength(outputFile.length());
				out.println("Enter the device name or serial number:");
				line = in.readLine();
				if(line == null)
					break;
				line = line.trim();
				if(line.length() == 0) {
					out.println("Exit?");
					String line2 = in.readLine();
					if(line2 == null)
						break;
					line2 = line2.trim();
					if(line2.length() > 0 && line2.charAt(0) == 'Y' || line2.charAt(0) == 'y')
						break OUTER;
					continue;
				}
				String deviceName = line;
				fileName.append(deviceName).append('_');
				while(true) {
					out.println("Enter the start time in UTC:");
					line = in.readLine();
					if(line == null)
						break OUTER;
					line = line.trim();
					if(line.length() == 0) {
						fileName.append("all");
						startDate = null;
					} else {
						try {
							startDate = ConvertUtils.convert(Calendar.class, line);
						} catch(ConvertException e1) {
							out.println("Could not convert '" + line + "' to a date");
							continue;
						}
						fileName.append(line.replaceAll("[^\\w\\-]", "_"));
					}
					break;
				}
				fileName.append("_to_");
				while(true) {
					out.println("Enter the end time in UTC:");
					line = in.readLine();
					if(line == null)
						break OUTER;
					line = line.trim();
					if(line.length() == 0) {
						fileName.append("all");
						endDate = null;
					} else {
						try {
							endDate = ConvertUtils.convert(Calendar.class, line);
						} catch(ConvertException e1) {
							out.println("Could not convert '" + line + "' to a date");
							continue;
						}
						fileName.append(line.replaceAll("[^\\w\\-]", "_"));
					}
					break;
				}
				fileName.append(".log");
				PrintStream ps;
				FileOutputStream fos;
				if(outputFile != null && (outputFile=outputFile.trim()).length() > 0) {
			    	try {
						fos = new FileOutputStream(fileName.toString());
					} catch(FileNotFoundException e) {
						Log.finish();
						System.err.println("Could not find output file '" + fileName + "'");
						e.printStackTrace();
						System.exit(160);
						return;
					}
			    	PassThroughOutputStream ptos = new PassThroughOutputStream();
			    	ptos.addOutputStream(out);
			    	ptos.addOutputStream(fos);
			    	ps = new PrintStream(ptos);
			    } else {
			    	ps = out;
			    	fos = null;
			    }

				Map<String,Object> params = new LinkedHashMap<String, Object>();
				params.put("deviceSearch", deviceName);
				try {
					DataLayerMgr.selectInto("SEARCH_FOR_DEVICE", params);
				} catch(NotEnoughRowsException e) {
					System.err.println("Could not find device '" + line + "'");
					continue;
				} catch(SQLException e) {
					System.err.println("Error while searching for the device");
					e.printStackTrace();
					continue;
				} catch(DataLayerException e) {
					System.err.println("Error while searching for the device");
					e.printStackTrace();
					continue;
				} catch(BeanException e) {
					System.err.println("Error while searching for the device");
					e.printStackTrace();
					continue;
				}
				params.remove("deviceSearch");
				ps.print("Looking up recent sessions for Device ");
				printDeviceInfo(ps, params);
				ps.println();

				params.put("startTime", startDate == null ? null : ConvertUtils.getMillisUTC(startDate));
				params.put("endTime", endDate == null ? null : ConvertUtils.getMillisUTC(endDate));
				Results results;
				try {
					results = DataLayerMgr.executeQuery("GET_RECENT_DEVICE_MESSAGES", params);
				} catch(SQLException e) {
					System.err.println("Error while get recent messages for the device");
					e.printStackTrace();
					continue;
				} catch(DataLayerException e) {
					System.err.println("Error while get recent messages for the device");
					e.printStackTrace();
					continue;
				}
				for(int i = 1; results.next(); i++) {
					ps.print("Message #");
					ps.print(i);
					ps.print(": ");
					ps.print(results.getValue("deviceMessageId"));
					ps.print(" [SessionId=");
					ps.print(results.getValue("deviceSessionid"));
					ps.print("; Inbound UTC=");
					ps.print(results.getValue("inboundMessageUTC"));
					ps.print("; Outbound UTC=");
					ps.print(results.getValue("outboundMessageUTC"));
					ps.print("; Duration (millis)=");
					ps.print(results.getValue("messageDurationMillis"));
					String inboundHex;
					String outboundHex;
					try {
						inboundHex = results.getValue("inboundMessage", String.class);
						outboundHex = results.getValue("outboundMessage", String.class);
					} catch(ConvertException e) {
						log.warn("Could not convert inbound or outbound message to a String", e);
						inboundHex = "ERROR";
						outboundHex = "ERROR";
					}
					ps.print("; Inbound Message=");
					ps.print(inboundHex);
					ps.print("; Outbound Message=");
					ps.print(outboundHex);
					ps.println(']');
					MessageData inboundMessage = null;
					MessageData outboundMessage = null;
					try {
						inboundMessage = MessageDataFactory.readMessage(ByteBuffer.wrap(ByteArrayUtils.fromHex(inboundHex)), MessageDirection.CLIENT_TO_SERVER);
					} catch(RuntimeException e) {
						log.warn("Could not parse message", e);
					}
					try {
						outboundMessage = MessageDataFactory.readMessage(ByteBuffer.wrap(ByteArrayUtils.fromHex(outboundHex)), MessageDirection.SERVER_TO_CLIENT);
					} catch(RuntimeException e) {
						log.warn("Could not parse message", e);
					}
					ps.print("--> ");
					if(inboundMessage != null)
						ps.println(inboundMessage);
					else
						ps.println(inboundHex);
					ps.print("<-- ");
					if(outboundMessage != null)
						ps.println(outboundMessage);
					else
						ps.println(outboundHex);
					ps.println("------------------------");
				}
				ps.println();
				if(fos != null) {
					fos.close();
				}
			}
		} catch(IOException e) {
			System.err.println("Could not read from input stream; exitting...");
			e.printStackTrace();
		}
		Log.finish();
        System.exit(0);
	}

	protected void printDeviceInfo(PrintStream out, Map<String,Object> params) {
		out.print(params.get("deviceName"));
		out.print(": [");
		boolean first = true;
		for(Map.Entry<String,Object> entry : params.entrySet()) {
			if(!"deviceName".equals(entry.getKey())) {
				if(first)
					first = false;
				else
					out.print("; ");
				out.print(entry.getKey());
				out.print('=');
				out.print(entry.getValue());
			}
		}
		out.print(']');
	}
}
