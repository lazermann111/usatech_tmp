package com.usatech.test;

import static simple.text.MessageFormat.format;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicLong;

import com.usatech.layers.common.CRC;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.AEAdditionalDataType;
import com.usatech.layers.common.constants.CRCType;
import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.CardType;
import com.usatech.layers.common.constants.ComponentType;
import com.usatech.layers.common.constants.DeviceProperty;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.EventType;
import com.usatech.layers.common.constants.FileType;
import com.usatech.layers.common.constants.GenericRequestType;
import com.usatech.layers.common.constants.GenericResponseClientActionCode;
import com.usatech.layers.common.constants.GenericResponseResultCode;
import com.usatech.layers.common.constants.GenericResponseServerActionCode;
import com.usatech.layers.common.constants.InitializationReason;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.ReceiptResult;
import com.usatech.layers.common.constants.SaleResult;
import com.usatech.layers.common.constants.SaleType;
import com.usatech.layers.common.constants.SettlementFormat;
import com.usatech.layers.common.constants.TranDeviceResultType;
import com.usatech.layers.common.messagedata.Component;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageDataFactory;
import com.usatech.layers.common.messagedata.MessageData_2A;
import com.usatech.layers.common.messagedata.MessageData_8F;
import com.usatech.layers.common.messagedata.MessageData_A0;
import com.usatech.layers.common.messagedata.MessageData_A4;
import com.usatech.layers.common.messagedata.MessageData_A6;
import com.usatech.layers.common.messagedata.MessageData_AA;
import com.usatech.layers.common.messagedata.MessageData_AD;
import com.usatech.layers.common.messagedata.MessageData_AE;
import com.usatech.layers.common.messagedata.MessageData_C0;
import com.usatech.layers.common.messagedata.MessageData_C1;
import com.usatech.layers.common.messagedata.MessageData_C2;
import com.usatech.layers.common.messagedata.MessageData_C2.GenericTracksCardReader;
import com.usatech.layers.common.messagedata.MessageData_C4;
import com.usatech.layers.common.messagedata.MessageData_C4.Format0LineItem;
import com.usatech.layers.common.messagedata.MessageData_C5;
import com.usatech.layers.common.messagedata.MessageData_C5.Format0Content;
import com.usatech.layers.common.messagedata.MessageData_C5.Format1Content;
import com.usatech.layers.common.messagedata.MessageData_C7;
import com.usatech.layers.common.messagedata.MessageData_C8;
import com.usatech.layers.common.messagedata.MessageData_C9;
import com.usatech.layers.common.messagedata.MessageData_CA;
import com.usatech.layers.common.messagedata.MessageData_CA.FileTransferRequest;
import com.usatech.layers.common.messagedata.MessageData_CA.UpdateStatusRequest;
import com.usatech.layers.common.messagedata.MessageData_CB;
import com.usatech.layers.common.messagedata.MessageData_CB.PropertyValueListAction;
import com.usatech.layers.common.messagedata.MessageDirection;
import com.usatech.networklayer.NetworkLayerException;
import com.usatech.networklayer.ReRixMessage;
import com.usatech.networklayer.net.TransmissionProtocol;
import com.usatech.networklayer.net.TransmissionProtocolV4;
import com.usatech.util.crypto.CRCException;
import com.usatech.util.crypto.Crypt;
import com.usatech.util.crypto.CryptException;
import com.usatech.util.crypto.CryptUtil;

import simple.io.ByteArrayUtils;
import simple.io.FromHexInputStream;
import simple.io.Log;
import simple.io.UUEncode;
import simple.text.StringUtils;

public class ClientEmulator {
	private static Log log = Log.getLog();
	protected String host = "usaapd1";
	protected int port = 443;
	protected String evNumber = "EV035179";
	protected String serialNumber = "EE100000179";
	protected byte[] encryptionKey;
	protected int msg = 0;
	protected int protocolVersion = 3;
	protected TransmissionProtocol protocol;
	protected Socket socket;
	protected class LoggingInputStream extends BufferedInputStream {
		protected final StringBuilder sb = new StringBuilder();
		public LoggingInputStream(InputStream in) {
			super(in);
		}
		@Override
		public int read() throws IOException {
			int r = super.read();
			if(r > -1)
				StringUtils.appendHex(sb, (byte)r);
			return r;
		}
		@Override
		public int read(byte[] b, int off, int len) throws IOException {
			int r = super.read(b, off, len);
			if(r > 0)
				StringUtils.appendHex(sb, b, off, r);
			return r;
		}
		public String drainLog() {
			String s = sb.toString();
			sb.setLength(0);
			return s;
		}
		
	}
	protected DataInputStream input;
	protected LoggingInputStream inputLogging;
	protected DataOutputStream output;
	protected boolean useTestClient = false;
	protected long masterId = System.currentTimeMillis() / 1000;
	protected static char[] counterPadding = "00000000".toCharArray();
	protected static Crypt[] crypts = new Crypt[10];
	protected AtomicLong messageId = new AtomicLong((System.currentTimeMillis() / 1000));
	protected String timeZoneGuid;

	static {
		crypts[1] = new Crypt() {
			public byte calcCheckSum(byte[] data) {
				byte sum = 0;
				for(int i = 0; i < data.length; i++)
					sum += data[i];
				return sum;
			}
			@Override
			public byte[] decrypt(byte[] encrypted, byte[] key) throws InvalidKeyException, CRCException, CryptException {
				byte[] decrypted = new byte[encrypted.length - 1];
				System.arraycopy(encrypted, 0, decrypted, 0, decrypted.length);
				byte ck = calcCheckSum(decrypted);
				if(ck != encrypted[encrypted.length-1])
					throw new CRCException("Expected 0x" + StringUtils.toHex(encrypted[encrypted.length-1]) + " check sum; but calculated 0x" + StringUtils.toHex(ck));
				return decrypted;
			}
			
			@Override
			public byte[] decrypt(byte[] encrypted, byte[] key, int blockSize) throws InvalidKeyException, CRCException, CryptException {
				return decrypt(encrypted, key);
			}

			@Override
			public byte[] decrypt(byte[] encrypted, byte[] key, int unencryptedLength, int blockSize) throws InvalidKeyException, CryptException {
				try {
					return decrypt(encrypted, key);
				} catch(CRCException e) {
					throw new CryptException(e.getMessage());
				}
			}

			@Override
			public byte[] encrypt(byte[] unencrypted, byte[] key) throws InvalidKeyException, CryptException {
				byte[] encrypted = new byte[unencrypted.length + 1];
				System.arraycopy(unencrypted, 0, encrypted, 0, unencrypted.length);
				encrypted[encrypted.length-1] = calcCheckSum(unencrypted);
				return encrypted;
			}

			@Override
			public byte[] encrypt(byte[] unencrypted, byte[] key, int blockSize) throws InvalidKeyException, CRCException, CryptException {
				return encrypt(unencrypted, key);
			}

			@Override
			public byte[] encrypt(byte[] unencrypted, byte[] key, byte padding) throws InvalidKeyException, CryptException {
				return encrypt(unencrypted, key);
			}

			@Override
			public byte[] encrypt(byte[] unencrypted, byte[] key, byte padding, int blockSize) throws InvalidKeyException, CryptException {
				return encrypt(unencrypted, key);
			}
		};
		crypts[3] = CryptUtil.getCrypt("RIJNDAEL");
		crypts[4] = CryptUtil.getCrypt("TEA_CRC16");
		crypts[5] = CryptUtil.getCrypt("RIJNDAEL_CRC16");
		crypts[6] = CryptUtil.getCrypt("RIJNDAEL_CRC16");
		crypts[7] = CryptUtil.getCrypt("AES_CBC_CRC16");
		crypts[8] = CryptUtil.getCrypt("AES_CBC_CRC16");
	}

	public ClientEmulator() {
		messageId = new AtomicLong(System.currentTimeMillis() / 1000);
	}
	
	public ClientEmulator(String host, int port, String evNumber, byte[] encryptionKey) {
		this(host, port, evNumber, encryptionKey, (System.currentTimeMillis() / 1000));
	}

	public ClientEmulator(String host, int port, String evNumber, byte[] encryptionKey, long startMessageId) {
		this.host = host;
		this.port = port;
		this.evNumber = evNumber;
		this.encryptionKey = encryptionKey;
		messageId = new AtomicLong(startMessageId);
		log.info(format("New ClientEmulator: host={0}, port={1} deviceName={2}, messageId={3}", host, port, evNumber, messageId.get()));
	}

	/**
	 * @param argshttps://ecclb001.usatech.com:9443/lbadmin/splash.php
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		//int netLayerFormat = 7;
		//String host = "127.0.0.1"; int port = 14107; int netLayerFormat = 4;
		String host = "127.0.0.1"; int port = 14108; int netLayerFormat = 7;
		//String host = "127.0.0.1"; int port = 14109; int netLayerFormat = 7;
		//String host = "127.0.0.1"; int port = 443;
		//String host = "10.0.0.67"; int port = 14108; // dev
		//String host = "10.0.0.68"; int port = 14109; int netLayerFormat = 7; // int
		//String host = "10.0.0.67"; int port = 14107; // dev
		//String host = "10.0.0.68"; int port = 14107; // int
		//String host = "192.168.4.63"; int port = 14107; // ecc
		//String host = "192.168.4.63"; int port = 14109; int netLayerFormat = 7; // ecc
		//String host = "devnet01"; int port = 14108;
		//String host = "usadev01"; int port = 14108;
		//String host = "usaapd1"; int port = 443;
		//String host = "intnet01"; int port = 14108;
		//String host = "eccnet11"; int port = 14108;
		//String host = "192.168.79.163"; int port = 14107; // prod
		//String evNumber = "EV034575"; byte[] encryptKey = ByteArrayUtils.fromHex("36303032343432323431323536373531"); // local
		//String evNumber = "EV100136"; byte[] encryptKey = ByteArrayUtils.fromHex("31393331353730393435313631353131"); // dev env
		//String evNumber = "EV035194"; byte[] encryptKey = ByteArrayUtils.fromHex("33303232383438303032383737333538"); // dev env
		//String evNumber = "EV000000"; byte[] encryptKey = ByteArrayUtils.fromHex("39323635303237373339323734343031"); String serialNumber = "G5060363"; // usadev04 Init test
		//String evNumber = "EV000000"; byte[] encryptKey = ByteArrayUtils.fromHex("39323635303237373339323734343031"); String serialNumber = "G5064659"; // eccdb Init test
		//String evNumber = "EV036214"; byte[] encryptKey = ByteArrayUtils.fromHex("3DED24176D5AA19DDBFD4B6A87991E89"); // usadev04 Isis test
		//String evNumber = "TD000473"; byte[] encryptKey = ByteArrayUtils.fromHex("5CC314181C29D1E658D89BFBC165A99A"); // usadev04 Isis test
		//String evNumber = "EV050272"; byte[] encryptKey = ByteArrayUtils.fromHex("31313231323130373231323335363230"); // usadev04
		//String evNumber = "EV035194"; byte[] encryptKey = ByteArrayUtils.fromHex("33303232383438303032383737333538"); // usadev03
		//String evNumber = "EV035425"; byte[] encryptKey = ByteArrayUtils.fromHex("38363638313139303431393334333630"); // usadev02
		//String evNumber = "EV100395"; byte[] encryptKey = ByteArrayUtils.fromHex("A01AD19C63B1FBFD2DC93F8ADACDD65B"); // usadev02 Joe Simpkins' device
		//String evNumber = "TD000021"; byte[] encryptKey = ByteArrayUtils.fromHex("BF2326717CBF35BF00503E60BFBFBFBF"); // dev env
		//String evNumber = "TD000021"; byte[] encryptKey = ByteArrayUtils.fromHex("BF2326717CBF35BF00503E60BFBFBFBF"); // int env
		//String evNumber = "EV083449"; byte[] encryptKey = ByteArrayUtils.fromHex("37303534333633393131393039353830"); // ecc env
		String evNumber = "TD000064"; byte[] encryptKey = ByteArrayUtils.fromHex("FF498E36484E3A4A2419AEA24F7699D0"); // local env
		//String evNumber = "TD000064"; byte[] encryptKey = ByteArrayUtils.fromHex("03BFBF6250BF31BFBF4ABFBFBFBF41BF"); // dev env
		//String evNumber = "TD000000"; byte[] encryptKey = ByteArrayUtils.fromHex("6DF7E5A6FBC84C50A79F76762826C0DC");
		//String evNumber = "TD000065"; byte[] encryptKey = ByteArrayUtils.fromHex("F97516265EA7CD533964F92DD870AC3A"); // dev env
		//String evNumber = "TD000065"; byte[] encryptKey = ByteArrayUtils.fromHex("097803BEEFC4BFA9BE6DAF23F95CF9E5"); // local env - 989F4EB959B67F1196D82A8408B20066
		//String evNumber = "TD000065"; byte[] encryptKey = ByteArrayUtils.fromHex("5EF6E31B196B06E980AC73CFD30E6273"); // int env		//
		//String evNumber = "TD000152"; byte[] encryptKey = ByteArrayUtils.fromHex("4FCA19319F7ECEAB6EDB2DC1CAC78F3A");
		//String evNumber = "TD000154"; byte[] encryptKey = ByteArrayUtils.fromHex("3A2F8B551A448FD5B0F2131A056CBB22");
		//String evNumber = "TD000183"; byte[] encryptKey = ByteArrayUtils.fromHex("74924183DD7F74A686C6E5CB2752EF8B");
		//String evNumber = "TD000271"; byte[] encryptKey = ByteArrayUtils.fromHex("1FC3D7D502D821D194AAB08582C44CA2"); // local env
		//String evNumber = "TD000277"; byte[] encryptKey = ByteArrayUtils.fromHex("AFC38DBD61EE584BAF32CF8564844905"); // local env
		//String evNumber = "EV035578"; byte[] encryptKey = "1559166976192394".getBytes();
		//String evNumber = "TD000023"; byte[] encryptKey = ByteArrayUtils.fromHex("D4134A1743C9573429309EB006445123"); // Certification
		//String evNumber = "EV035179"; byte[] encryptKey = ByteArrayUtils.fromHex("31363538323234363430383635373633"); // local env
		//String evNumber = "TD061769"; byte[] encryptKey = ByteArrayUtils.fromHex("C7E1D7D9D658EA41AC2DF4F188812712"); // prod
		//String evNumber = "TD000630"; byte[] encryptKey = ByteArrayUtils.fromHex("18B5BFB3EF057942636A101B6AC3F881"); // local
		//String evNumber = "EV260861"; byte[] encryptKey = ByteArrayUtils.fromHex("6AD3ED2DEEF1C6B54D195E77149B85E2"); // prod NAMA
		//String evNumber = "EV260861"; byte[] encryptKey = ByteArrayUtils.fromHex("6AD3ED2DEEF1C6B54D195E77149B85E2"); // NAMA test
		
		//String evNumber = "EV035194"; byte[] encryptKey = ByteArrayUtils.fromHex("33303232383438303032383737333538"); // local
		//String evNumber = "TD000908"; byte[] encryptKey = ByteArrayUtils.fromHex("173C4AFA1C8B9352C38F4D2246680FF9"); // local
		//String evNumber = "TD000022"; byte[] encryptKey = ByteArrayUtils.fromHex("1529A1B819A7CE6BA9BFCA2209423DFC"); // local EE100000002 
		//String evNumber = "TD000109"; byte[] encryptKey = ByteArrayUtils.fromHex("B5E30B1FC325F053A6A6FF69CE568440"); // ECC EE100000002

		//testLBPing();
		ClientEmulator ce = new ClientEmulator(host, port, evNumber, encryptKey);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		//for (int i = 1; i < 100; i++)
		{
			ce.startCommunication(netLayerFormat);
		try {
			//ce.testInitV32(serialNumber);
			
			long transactionId = System.currentTimeMillis() / 1000;
			
			//Legacy USR
			//MessageData_90 updateStatus = (MessageData_90) ce.testLegacyUSR();	
			
			ce.testDexFileTransfer("EE100000014");

			//Gx Isis test
			//ce.testAuthV32(transactionId, 0, CardType.CREDIT_ISIS.getValue(), 150, null, 0, null, "6011000995500001=20121015432112345678", null, AEAdditionalDataType.ISIS_SMARTTAP.getValue(), new String(ByteArrayUtils.fromHex("DF5310025A58313739353637353438333143460EDF21091234567890123456789000DF51051122334455"))); //Auth v3.2
			/*ce.testAuthV32(transactionId, 0, CardType.CREDIT_ISIS.getValue(), 150, null, 0, null, "6011000995500004=20121015432112345678", null, AEAdditionalDataType.NO_ADDITIONAL_DATA.getValue(), null); //Auth v3.2
			ce.stopCommunication();
			ce.startCommunication(netLayerFormat);
			ce.testBatchV2(transactionId, 125, TranDeviceResultType.SUCCESS_NO_RECEIPT);*/
			
			//Gx NAMA MORE test
			/*ce.testAuthV32(transactionId, 0, CardType.CREDIT_SWIPE.getValue(), 150, null, 0, null, "6396212001326520184=9912002401168", null, AEAdditionalDataType.NO_ADDITIONAL_DATA.getValue(), null); //Auth v3.2
			ce.stopCommunication();
			ce.startCommunication(netLayerFormat);
			ce.testBatchV2(transactionId, 125, TranDeviceResultType.SUCCESS_NO_RECEIPT);*/
			
			//Edge Isis test
			/*ce.testAuthV4(EntryType.getByValue('I'), "6011000995500000=20121015432112345678", BigDecimal.valueOf(150), transactionId);
			ce.stopCommunication();
			ce = new ClientEmulator(host, 14108, evNumber, encryptKey);
			ce.startCommunication(7);
			ce.testSale(transactionId, SaleType.ACTUAL, 3, false);*/
			
			//ce.testAuthV4(EntryType.getByValue('S'), "6396212009553417644=9912000418445", BigDecimal.valueOf(15000), transactionId);
			//ce.testAuthV32(transactionId, 0, CardType.CREDIT_SWIPE.getValue(), 15000, null, 0, null, "6396212009553417644=9912000418445", null, AEAdditionalDataType.NO_ADDITIONAL_DATA.getValue(), null); //Auth v3.2
			// partial approval
			//ce.testAuthV32(transactionId, 0, CardType.CREDIT_SWIPE.getValue(), 278, null, 0, null, "4788250000028291=20121015432112345678", null, AEAdditionalDataType.NO_ADDITIONAL_DATA.getValue(), null); //Auth v3.2
			
			/*ce.testAuthV4(EntryType.getByValue('S'), "6396212003060603432=9912009398652", BigDecimal.valueOf(1), transactionId); //ECC
			ce.stopCommunication();
			ce = new ClientEmulator(host, 14108, evNumber, encryptKey);
			ce.startCommunication(netLayerFormat);
			ce.testSale(transactionId);*/
			
			//ce.testAuthV4(EntryType.getByValue('S'), "6396212001601650417=9912004726530", BigDecimal.valueOf(5000), transactionId); //Int
			//ce.testAuthV4(EntryType.getByValue('S'), "6396210007411504779=9912002912200", BigDecimal.valueOf(100), transactionId); //Local gift
			//ce.testAuthV4(EntryType.getByValue('S'), "6396212008164062674=9912004970462", BigDecimal.valueOf(100), transactionId); //Local prepaid, 0817, 126262
			//ce.testAuthV4(EntryType.getByValue('S'), "100001", BigDecimal.valueOf(100), transactionId); //Aramark Quantum Aramark Server
			
			/*ce.testAuthV4(EntryType.getByValue('S'), "4055011111111111=20121015432112345678", BigDecimal.valueOf(200), transactionId);
			ce.stopCommunication();
			ce = new ClientEmulator(host, port, evNumber, encryptKey);
			ce.startCommunication(netLayerFormat);
			ce.testSingleSale(transactionId, SaleType.ACTUAL, new BigDecimal(100), false);*/
			
			/*MessageData_Unknown mdu = new MessageData_Unknown(MessageType.GENERIC_EVENT_4_1.getValue());*/
			/*ce.testGenReqUSR();
			MessageData_C6 dataC6 = new MessageData_C6();
			dataC6.setComponentNumber(2);
			dataC6.setMessageId(ce.messageId++);
			dataC6.setEventType(EventType.COMPONENT_DETAILS);
			dataC6.setEventData("CNUM:16101234567\nTCP/IP Stack Version:v1.0.2\n");
			dataC6.setEventId(System.currentTimeMillis() / 1000);
			dataC6.setEventTime(Calendar.getInstance());
			ce.sendMessage(dataC6);*/
			/*ByteBuffer buffer = ByteBuffer.allocate(1024);
			dataC6.writeData(buffer, false);
			byte[] contentBytes = new byte[buffer.position() - 3];
			buffer.flip();
			buffer.position(2);
			buffer.get(contentBytes);*/
			//contentBytes[11] = 7;
			/*mdu.setContentBytes(contentBytes);
			ce.sendMessage(mdu);*/
			//*
			//ce.testTerminalUSR();
			//ce.sendMessage("", 1);
			//lastMasterId = 1236105797;
			//ce.testGenRespUSR();
			//ce.testGenReqUSR(0);
			//ce.testCompIdent();
			//*
			//ce.testGenReqUSR(1);
			//ce.messageId = 1245942790;
			//ce.testGenReqUSR();
			//ce.testGenReqCfgFile();
			//ce.testComponents();
			//ce.testGenReqUSR();
			//ce.testFileDownload2();
			//ce.testGenRespUSR();
			//ce.testModemProperties();
			//ce.testGenReqUSR();
			//ce.testShortFileTransferPropertyIndexList();
			//ce.testGenRespUSR();
			//ce.testGenRespUSR();
			//ce.testFileDownload2();
			//ce.testFileDownload2();
			//ce.testFileDownload2();
			//ce.testFileDownload2();
			//ce.testPropertiesLongFileTransfer();
			//ce.testGenRespUSR();
			//ce.testGenRespUSR();
			//ce.testModemProperty1();
			//ce.testGenRespUSR();
			//ce.testGenRespUSR();
			//ce.testModemProperties();
			//ce.testGenRespUSR();
			//ce.testGenRespUSR();
			//ce.testShortFileTransferBlank();
			//ce.testGenReqUSR();
			//ce.testFileDownload();
			//ce.testGenReqUSR();
			//ce.testFileDownload();
			//ce.testFileDownload();
			//ce.testFileDownload();
			//ce.testFileDownload();
			//ce.testFileDownload();
			//ce.testGenReqUSR();
			//ce.testGenRespUSR();
			//ce.testGenRespUSR();
			//ce.testGenRespUSR();
			//ce.testGenReqAR();
			//ce.testScheduledSettlement(1242341057);
			//ce.testFillSettlement2(1250628545);
			//ce.testGenReqUSR();
			//ce.testGenRespNoAction();
			//ce.testGenRespNoAction();
			//ce.testGenRespNoAction();
			//ce.testGenRespUSR();
			//ce.testGenRespUSR();
			//*
			//ce.testAuth((byte) 4, 'S', "testNullTranId"); //error
			//ce.masterId = 1;
			//ce.testAuthV4('S', ";5454545454545454=10121015432112345601?"); //MC
			//ce.testAuthV4('S', ";5454545454545454=10121015432112345601?\r"); //MC
			//ce.testAuthV4('S', ";5454545454545454=10121015432112345601?\00FF"); //MC
			//ce.testAuthV4('S', "6396210002890012558=0108123456789"); //Internal
			//ce.testAuthV4('S', "6396210002890012558=0108123456789", 100000000); //Internal
			//ce.testAuthV4('S', "6396210002790012558=0108123456789"); //Internal - bad acct
			//ce.testAuthV4('S', "6011230002890012=0108123456789"); //Discover
			//ce.testAuthV4('S', "4300123412341017=12100001"); //CC Swipe
			//ce.testAuthV4('S', "23454"); //Error Bin
			//ce.testAuthV4('S', ""); //Unmatched
			//ce.testAuth((byte) 3, 'C', ";5454545454545454=10121015432112345601?"); //VIsa

			//ce.testAuthV4('S', "639621150020000037=371216872"); //Internal - Edge Driver Card
			//ce.testAuthV4('S', "639621150020000045=371216224"); //Internal - Edge Driver Card
			//ce.testAuthV4('S', "639621152480000309=991245921"); //prod driver card
			//ce.testAuthV4('S', "639621150020002447=991294404"); //local driver card
			//ce.testAuthV4('S', "639621150020000910=991294404"); //local driver card
			//ce.testAuthV4('S', "%B4264281044023371^COWAN/PAUL W^090310100000000000000000000000000000359000000?"); // Track 1
			//ce.testAuthV4('S', "375019001001880"); // Blackboard -- success
			//ce.testAuthV4('S', "6329082000000000053"); // Blackboard -- insufficient funds
			//ce.testAuthV4('S', "6329082000000000055"); // Blackboard -- success

			//ce.testAuthPermission(EntryType.SWIPE, "639621150020000037=371216872"); //Internal - Eport Driver Card
			//ce.testAuthPermission(EntryType.SWIPE, "639621150020000045=371216224"); //Internal - Eport Driver Card
			//ce.testAuthPermission(EntryType.SWIPE, "639621150020001377=371216872"); //Internal - Eport Driver Card
			//^(;?)6396210([0-9]{11})([0-9]{1})=([0-9]{4})([0-9]{2})([0-9]{5})([0-9]{2})(\??)$
			// */
			//ce.testGenReqUSR();
			//ce.testShortFileTransferPropertyIndexList();
			//ce.testLongFileTransfer();
			//ce.testCashSale(System.currentTimeMillis() / 1000);
			//ce.testSale(System.currentTimeMillis() / 1000);
			//ce.testSale(1252680898, SaleType.INTENDED, 2);
			//ce.testSale(1263396578, SaleType.ACTUAL, 3);
			//ce.testCancelSale(1249324228);
			//ce.testGenReqUSR();
			//ce.testGenRespUSR();
			//ce.testShortFileTransferPropertyIndexList();
			//ce.testLongFileTransferBadLength();
			//ce.testLongFileTransferBadCRC();
			//ce.testAuthV4('S', "6396210002890012558=0108123456789"); //Internal
			//ce.testAuthV4('S', "639621130020000030=010812345"); //Internal

			//ce.testAuthV4('S', "639621130020001130=010812345"); //Internal
			//ce.testAuthV4('S', "6396210003890012558=0108123456789"); //Internal
			//ce.testAuth((byte)4, 'S', "375019001001880"); // Blackboard -- success
			//ce.testGenReqUSR(1);
			//ce.testGenReqUSR(1);
			//ce.testGenRespUSR();
			//ce.testGenRespUSR();

			//ce.testCashSale();
			//ce.testGenReqUSR(0);
			//ce.testGenRespUSR();
			//*
			//ce.testGenRespUSR();
			//ce.testAuthV3('C', 546, "4300123412341017=12100001", null); //CC Swipe
			//ce.testAuthV3('C', 546, "639621150020002507=371216872", null); //Driver Card
			//ce.testAuth((byte) 4, 'C', "4300123412341017=12100001"); //CC RFID
			//ce.testAuth((byte) 4, 'S', "639621130020001130=010812345"); //Internal
			//*/

			//ce.testInitV4(InitializationReason.APPLICATION_UPGRADE, 10);
			//ce.testInitV4(InitializationReason.NEW_DEVICE, 12);
			//ce.testComponents();
			//ce.testAuthV4('S', ";5454545454545454=10121015432112345601?"); //MC

			//ce.testGenRespUSR();
			//ce.testGenRespUSR();
			//ce.testGenRespUSR();
			/*
			messageId = 1235073942;
			ce.testGenReqUSR();
			messageId = 0;
			ce.testGenReqUSR();
			//for(int i = 0; i < 3; i++)
			//duplicate message
			/*
			String message = constructAuthV4Message(new Date(), 'S', 11, "6396210002890012558=0108123456789", null);
			ReRixMessage response = ce.sendMessage(message, 1);
			if(response != null)
				log.info("RESPONSE:\n" + new C2Message().readResponseString(response));
			response = ce.sendMessage(message, 1);
			if(response != null)
				log.info("RESPONSE:\n" + new C2Message().readResponseString(response));
			// */
			/*
			ce.testAuth((byte) 4, 'S', "6396210002890012558=0108123456789"); //Internal
			ce.testAuth((byte) 4, 'S', "6396210002890012558=0108123456789"); //Internal
			ce.testAuth((byte) 4, 'S', "6396210003890012558=0108123456789"); //Internal
//^(;?)6396210([0-9]{11})([0-9]{1})=([0-9]{4})([0-9]{2})([0-9]{5})([0-9]{2})(\??)$
		//testAuthPaymentech();
		//testGenReqUSR();
		//testAuthFHMS((byte)4);
		//testAuthPaymentech();
		*/ //*
		//Aramark Cards
		//ce.testAuthV4('S', "222222"); //success $50
		//ce.testAuthV4('S', "444444"); //Locked
		//ce.testAuthV4('S', "555555"); // $0

		//Blackboard
		//ce.testAuthV4('S', "375019001001880"); // success
		//ce.testAuthV4( 'S', "6329082000000000053"); // insufficient funds
		//ce.testAuthV4('S', "6329082000000000055"); // success
		//*/


		//	ce.testAuthV3('C', 234, "6010567006507449=00010004000070777731", null); // First Data
			//testPing();
		//testLBPing();
		//testAppLayerHealth();
		//testGenReqUSR();

		//testEncrypt();

		//testPing();
		//testInitV4();

		//testLocalBatch();
		//testAuthInternal((byte)4);
		//testAuthFHMS((byte)4);
		//testAuth((byte)4, 'M', "CURRENCY");
		//testAuthAquaFill();
		//testAuthFHMSDev();
		//testGenReqUSR();
		//testGenReqAR();
		//testGenReqUSR();
		//testAuthInternal();
		//testPingDev();
		//testGenResponseDev();
		//testAuthInternalDev();
		//testPing();
		//sendFills(5, 3);
		//testLocalBatch();
		//testAuth();
		//testAuthPaymentech();
		//testAuthBlackboard();
		//testAuthAramark();
		//testAuthAquaFill();
		} finally {
			ce.stopCommunication();
		}
		}
	}
	
	public int getMessageNumber() {
		return msg;
	}
	public void setMessageNumber(int messageNumber) {
		this.msg = messageNumber;
	}
	public static void testLBPingCert() throws Exception {
		String host = "eccnet01";
		int port = 443;
		String evNumber = "LD-BLNCR";
		ClientEmulator ce = new ClientEmulator(host, port, evNumber, null);
		ce.startCommunication(0);
		ce.msg = 13;
		for(int i = 0; i < 1; i++) {
			ce.sendMessage("AABBCCDD", 1);
		}
		ce.stopCommunication();
	}
	public static void testLBPingDev() throws Exception {
		String host = "usaapd1";
		int port = 443;
		String evNumber = "LD-BLNCR";
		ClientEmulator ce = new ClientEmulator(host, port, evNumber, null);
		ce.startCommunication(0);
		ce.msg = 13;
		for(int i = 0; i < 1; i++) {
			ce.sendMessage("AABBCCDD", 1);
		}
		ce.stopCommunication();
	}
	public static void testLBPing() throws Exception {
		String host = "127.0.0.1";
		int port = 14108;
		String evNumber = "LD-BLNCR";
		ClientEmulator ce = new ClientEmulator(host, port, evNumber, null);
		ce.startCommunication(0);
		ce.msg = 13;
		ce.sendMessage("AABBCCDD", 1);
		ce.stopCommunication();
	}
	
	public static void testLBPingInt() throws Exception {
		String host = "intnet11";
		int port = 14108;
		String evNumber = "LD-BLNCR";
		ClientEmulator ce = new ClientEmulator(host, port, evNumber, null);
		ce.startCommunication(0);
		ce.msg = 13;
		ce.sendMessage("AABBCCDD", 1);
		ce.stopCommunication();
	}
	public static void testAppLayerHealth() throws Exception {
		String host = "127.0.0.1";
		int port = 443;
		String evNumber = "EV035194";
		ClientEmulator ce = new ClientEmulator(host, port, evNumber, null);
		ce.startCommunication(1);
		for(int i = 0; i < 1; i++) {
			ce.sendMessage("00060003", 1);
		}
		ce.stopCommunication();
	}
	public static void testAppLayerHealthDev() throws Exception {
		String host = "usaapd1";
		int port = 443;
		String evNumber = "EV035194";
		ClientEmulator ce = new ClientEmulator(host, port, evNumber, null);
		ce.startCommunication(1);
		for(int i = 0; i < 1; i++) {
			ce.sendMessage("00060003", 1);
		}
		ce.stopCommunication();
	}
	public static void testEncrypt() throws Exception {
		Crypt crypt = CryptUtil.getCrypt("AES_CBC_CRC16");
		byte[] key = "m����LP��vv(&��".getBytes();
		byte[] unencrypted = new byte[32];
		for(int i = 0; i < unencrypted.length; i++) {
			unencrypted[i] = (byte)i;
		}
		//log.debug(StringUtils.toHex(unencrypted))
		byte[] encrypted = crypt.encrypt(unencrypted, key);

	}
	public static void testPing() throws Exception {
		String host = "127.0.0.1";
		int port = 443;
		String evNumber = "EV035194";
		String serialNumber = "G5060281";
		byte[] encryptKey = "3022848002877358".getBytes();
		String card = "4300123412341017=12100001";
		ClientEmulator ce = new ClientEmulator(host, port, evNumber, encryptKey);
		ce.startCommunication(0);
		for(int i = 0; i < 5; i++) {
			ce.sendMessage("", 1);
		}
		ce.stopCommunication();
	}
	public static void testPingDev() throws Exception {
		String host = "usaapd1";
		int port = 443;
		String evNumber = "EV035194";
		String serialNumber = "G5060281";
		byte[] encryptKey = "3022848002877358".getBytes();
		String card = "4300123412341017=12100001";
		ClientEmulator ce = new ClientEmulator(host, port, evNumber, encryptKey);
		ce.startCommunication(0);
		for(int i = 0; i < 5; i++) {
			ce.sendMessage("", 1);
		}
		ce.stopCommunication();
	}
	public static void testGenResponseDev() throws Exception {
		String host = "usaapd1";
		int port = 443;
		String evNumber = "EV035194";
		String serialNumber = "G5060281";
		byte[] encryptKey = "3022848002877358".getBytes();
		String card = "4300123412341017=12100001";
		ClientEmulator ce = new ClientEmulator(host, port, evNumber, encryptKey);
		ce.startCommunication(7);
		ce.sendMessage(ce.constructGenRespV4_1Message(0, "Round-trip Test", 0), 1);
		ce.stopCommunication();
	}
	public MessageData testGenReqUSR() throws Exception {
		return testGenReqUSR(0);
	}
	public MessageData testGenReqUSR(int bitmap) throws Exception {
		MessageData_CA data = new MessageData_CA();
		data.setRequestType(GenericRequestType.UPDATE_STATUS_REQUEST);
		((UpdateStatusRequest)data.getRequestTypeData()).setStatusCodeBitmap(bitmap);
		return sendMessage(data);
	}
	public MessageData testGenReqOK() throws Exception {
		MessageData_CB okay = new MessageData_CB();
		okay.setClientActionCode(GenericResponseClientActionCode.NO_ACTION);
		okay.setResultCode(GenericResponseResultCode.OKAY);
		return sendMessage(okay);
	}
	public MessageData testGenReqAppLayerError() throws Exception {
		MessageData_CB okay = new MessageData_CB();
		okay.setClientActionCode(GenericResponseClientActionCode.NO_ACTION);
		okay.setResultCode(GenericResponseResultCode.APPLAYER_ERROR);
		return sendMessage(okay);
	}
	public MessageData testGenReqCfgFile() throws Exception {
		MessageData_CA data = new MessageData_CA();
		data.setRequestType(GenericRequestType.FILE_TRANSFER_REQUEST);
		((FileTransferRequest)data.getRequestTypeData()).setFileType(FileType.CONFIGURATION_FILE);
		return sendMessage(data);
	}
	public void testFileDownload() throws Exception {
		MessageData response = testGenReqUSR(0);
		do {
			if(response == null) {
				log.warn("NULL RESPONSE -- exitting");
				break;
			}
			log.info("RESPONSE:\n" + response);
			MessageType messageType = response.getMessageType();
			switch(messageType) {
				case FILE_XFER_START_4_1:
				case SHORT_FILE_XFER_4_1:
					response = testGenReqOK();
					continue;
				case FILE_XFER_4_1:
					response = testGenReqOK();
					continue;
				case GENERIC_RESPONSE_4_1:
					MessageData_CB msg = (MessageData_CB) response;
					switch(msg.getServerActionCode()) {
						case SEND_UPDATE_STATUS_REQUEST_IMMEDIATE:
						case SEND_UPDATE_STATUS_REQUEST:
							response = testGenReqUSR(0);
							continue;
						default:
							testGenReqOK();
					}
			}
			break;
		} while(true);
	}
	public void testFileDownload2() throws Exception {
		MessageData_CA data = new MessageData_CA();
		data.setRequestType(GenericRequestType.UPDATE_STATUS_REQUEST);
		MessageData reply = sendMessage(data);
		do {
			if(reply != null)
				log.info("RESPONSE:\n" + reply);
			switch(reply.getMessageType()) {
				case FILE_XFER_START_4_1:
				case SHORT_FILE_XFER_4_1:
				case FILE_XFER_4_1:
					MessageData_CB okay = new MessageData_CB();
					okay.setClientActionCode(GenericResponseClientActionCode.NO_ACTION);
					okay.setResultCode(GenericResponseResultCode.OKAY);
					reply = sendMessage(okay);
					continue;
			}
			break;
		} while(true);
	}
	public void testFileRequestByG9(FileType fileType, String fileName)  throws Exception {
		MessageData_CA data = new MessageData_CA();
		data.setRequestType(GenericRequestType.FILE_TRANSFER_REQUEST);
		FileTransferRequest fileTransferRequest = (FileTransferRequest)data.getRequestTypeData();
		fileTransferRequest.setFileType(fileType);
		fileTransferRequest.setFileName(fileName);
		MessageData reply = sendMessage(data);
		log.info("RESPONSE:\n" + reply);
	}
	public void testGenRespUSR() throws Exception {
		MessageData_CB data = new MessageData_CB();
		data.setClientActionCode(GenericResponseClientActionCode.UPDATE_STATUS_REQUEST);
		data.setResultCode(GenericResponseResultCode.OKAY);
		sendMessage(data);
	}
	public void testGenRespNoAction() throws Exception {
		MessageData_CB data = new MessageData_CB();
		data.setClientActionCode(GenericResponseClientActionCode.NO_ACTION);
		data.setResultCode(GenericResponseResultCode.OKAY);
		sendMessage(data);
	}
	public void testGenReqPropertyList() throws Exception {
		ReRixMessage response = sendMessage(constructGenReq_PropertyList_Message(), 1);
		if(response != null)
			log.info("RESPONSE:\n" + new CAMessage().readResponseString(response));
	}
	public void testCancelSale() throws Exception {
		ReRixMessage response = sendMessage(constructCancelSaleMessage(), 1);
		if(response != null)
			log.info("RESPONSE:\n" + new CAMessage().readResponseString(response));
	}
	public void testCashSale() throws Exception {
		ReRixMessage response = sendMessage(constructCashSaleMessage(), 1);
		if(response != null)
			log.info("RESPONSE:\n" + new CAMessage().readResponseString(response));
	}
	public void testSale(long transactionId) throws Exception {
		ReRixMessage response = sendMessage(constructSaleMessage(transactionId), 1);
		if(response != null)
			log.info("RESPONSE:\n" + new CAMessage().readResponseString(response));
	}
	public void testInvalid(String hex) throws Exception {
		ReRixMessage response = sendMessage(hex, 1);
		if(response != null)
			log.info("RESPONSE:\n" + new CAMessage().readResponseString(response));
	}
	public void testCompIdent(String serialNumber) throws Exception {
		ReRixMessage response = sendMessage(constructCompIdentMessage(), 1);
		if(response != null)
			log.info("RESPONSE:\n" + new CAMessage().readResponseString(response));
	}
	public void testCompIdent() throws Exception {
		testCompIdent(serialNumber);
	}

	public void testTerminalUSR() throws Exception {
		sendMessage(new byte[] {(byte)messageId.getAndIncrement(), (byte)0x92}, 1);
	}
	/**
	 * @return
	 */
	public String constructCompIdentMessage(String serialNumber) {
		ByteBuffer buffer = ByteBuffer.allocate(1100);
		buffer.put(MessageType.COMPONENT_ID_4_1.getValue());
		ProcessingUtils.writeLongInt(buffer, messageId.getAndIncrement());
		ProcessingUtils.writeShortInt(buffer, 201);
		ProcessingUtils.writeString(buffer, "USD", ProcessingConstants.US_ASCII_CHARSET);
		ProcessingUtils.writeShortString(buffer, "USA Technologies", ProcessingConstants.US_ASCII_CHARSET);
		ProcessingUtils.writeShortString(buffer, "ePort", ProcessingConstants.US_ASCII_CHARSET);
		ProcessingUtils.writeShortString(buffer, serialNumber, ProcessingConstants.US_ASCII_CHARSET);
		ProcessingUtils.writeShortString(buffer, "v.1", ProcessingConstants.US_ASCII_CHARSET);
		ProcessingUtils.writeShortString(buffer, null, ProcessingConstants.US_ASCII_CHARSET);
		ProcessingUtils.writeByteInt(buffer, 1);
		ProcessingUtils.writeShortInt(buffer, 200);
		ProcessingUtils.writeByteInt(buffer, 1);
		ProcessingUtils.writeShortString(buffer, "Dell", ProcessingConstants.US_ASCII_CHARSET);
		ProcessingUtils.writeShortString(buffer, "Precision", ProcessingConstants.US_ASCII_CHARSET);
		ProcessingUtils.writeShortString(buffer, "000", ProcessingConstants.US_ASCII_CHARSET);
		ProcessingUtils.writeShortString(buffer, "v.1", ProcessingConstants.US_ASCII_CHARSET);
		ProcessingUtils.writeShortString(buffer, "Testing", ProcessingConstants.US_ASCII_CHARSET);


		buffer.flip();
		byte[] bytes = new byte[buffer.remaining()];
		buffer.get(bytes);
		return StringUtils.toHex(bytes);
	}
	
	public String constructCompIdentMessage() {
		return constructCompIdentMessage(serialNumber);
	}
	/**
	 * @return
	 */
	public String constructCancelSaleMessage() {
		StringBuilder sb = new StringBuilder();
		sb.append("C4");
		String msgId = Long.toHexString(messageId.getAndIncrement());
		padLeft(sb, msgId, 8);
		padLeft(sb, Long.toHexString(masterId), 8);
		padLeft(sb, Long.toHexString(1), 8); //batch_id
		sb.append("41");// 'A'
		padLeft(sb, Long.toHexString((System.currentTimeMillis() - 10000) / 1000), 8); //sale start time
		padLeft(sb, Integer.toHexString(-16), 2); //offset
		sb.append("01");// cancelled by user
		sb.append("00");// amount
		sb.append("55");// reciept unavailable
		sb.append("00");//format =0
		sb.append("00");//no blocks
		return sb.toString();
	}

	public String constructCancelSaleMessage(long transactionId) {
		StringBuilder sb = new StringBuilder();
		sb.append("C4");
		String msgId = Long.toHexString(messageId.getAndIncrement());
		padLeft(sb, msgId, 8);
		padLeft(sb, Long.toHexString(transactionId), 8);
		padLeft(sb, Long.toHexString(1), 8); //batch_id
		sb.append("41");// 'A'
		padLeft(sb, Long.toHexString((System.currentTimeMillis() - 10000) / 1000), 8); //sale start time
		padLeft(sb, Integer.toHexString(-16), 2); //offset
		sb.append("01");// cancelled by user
		sb.append("00");// amount
		sb.append("55");// reciept unavailable
		sb.append("00");//format =0
		sb.append("00");//no blocks
		return sb.toString();
	}
	public String constructCashSaleMessage() {
		StringBuilder sb = new StringBuilder();
		sb.append("00");
		sb.append("C4");
		String msgId = Long.toHexString(messageId.getAndIncrement());
		padLeft(sb, msgId, 8);
		padLeft(sb, Long.toHexString(masterId), 8);
		padLeft(sb, Long.toHexString(1), 8); //batch_id
		sb.append("43");// 'C'
		padLeft(sb, Long.toHexString((System.currentTimeMillis()- 2000) / 1000), 8); //sale start time
		padLeft(sb, Integer.toHexString(-16), 2); //offset
		sb.append("00");// success
		sb.append("03323530");// amount
		sb.append("55");// reciept unavailable
		sb.append("00");//format =0
		sb.append("01");//n of blocks =0
		sb.append("01");//component 0
		padLeft(sb, Integer.toHexString(200), 4); //item
		sb.append("00");// success
		sb.append("0001");// qty
		sb.append("03323530");// amount
		appendShortString(sb, "BK");
		sb.append("0002");// seconds
		return sb.toString();
	}
	public static void appendShortString(StringBuilder sb, String text) {
		byte[] bytes = text.getBytes();
		padLeft(sb, Integer.toHexString(bytes.length), 2);
		StringUtils.appendHex(sb, bytes, 0, bytes.length);
	}
	public static void testGenReqUSRDev() throws Exception {
		String host = "usaapd1";
		int port = 443;
		String evNumber = "EV035194";
		String serialNumber = "G5060281";
		byte[] encryptKey = "3022848002877358".getBytes();
		String card = "4300123412341017=12100001";
		ClientEmulator ce = new ClientEmulator(host, port, evNumber, encryptKey);
		ce.startCommunication(7);
		ReRixMessage response = ce.sendMessage(ce.constructGenReq_USR_V4_1Message(0), 1);
		if(response != null)
			log.info("RESPONSE:\n" + new CAMessage().readResponseString(response));

		response = ce.sendMessage(ce.constructGenReq_USR_V4_1Message(0), 1);
		if(response != null)
			log.info("RESPONSE:\n" + new CAMessage().readResponseString(response));
		ce.stopCommunication();
	}
	public void testGenReqAR() throws Exception {
		MessageData_CA data = new MessageData_CA();
		data.setRequestType(GenericRequestType.ACTIVATION_REQUEST);
		sendMessage(data);
	}
	public void testShortFileTransfer() throws Exception {
		ReRixMessage response = sendMessage(constructShortFileTransferMessage(), 1);
		if(response != null)
			log.info("RESPONSE:\n" + new CAMessage().readResponseString(response));
	}
	public void testShortFileTransferPropertyIndexList() throws Exception {
		MessageData_C7 data = new MessageData_C7();
		String content = "0-13|23|25-26|50-67|70|71|79-87|102-108";
		byte[] bytes = content.getBytes(ProcessingConstants.US_ASCII_CHARSET);
		data.setContent(bytes);
		data.setCreationTime(Calendar.getInstance());
		data.setEventId(0);
		data.setFileName(null);
		data.setFilesRemaining(0);
		data.setFileType(FileType.PROPERTY_LIST_REQUEST);
		sendMessage(data);
	}
	public void testShortFileTransferBlank() throws Exception {
		MessageData_C7 data = new MessageData_C7();
		byte[] bytes = new byte[0];
		data.setContent(bytes);
		data.setCreationTime(Calendar.getInstance());
		data.setEventId(0);
		data.setFileName(null);
		data.setFilesRemaining(0);
		data.setFileType(FileType.DEX_FILE);
		sendMessage(data);
	}
	public void testPropertiesLongFileTransfer() throws Exception {
		byte[] total = new byte[700];
		byte[] copy = "300=15\n301=2\n5000=".getBytes(ProcessingConstants.US_ASCII_CHARSET);
		System.arraycopy(copy, 0, total, 0, copy.length);
		for(int i = copy.length; i < total.length; i++)
			total[i] = (byte)(48 + (i % 10));
		MessageData_C8 dataC8 = new MessageData_C8();
		dataC8.setCreationTime(Calendar.getInstance());
		dataC8.setEventId(masterId++);
		dataC8.setFileName("Properties-Long-Test");
		dataC8.setFilesRemaining(1);
		dataC8.setFileType(FileType.PROPERTY_LIST);
		dataC8.setTotalBytes(total.length);
		byte[] bytes = new byte[256];
		dataC8.setTotalPackets((int)Math.ceil(total.length / (double) bytes.length));
		CRC crc = new CRC(CRCType.CRC16Server, CRCType.CRC16Server.calculate(new ByteArrayInputStream(total)));
		dataC8.setCrc(crc);
		sendMessage(dataC8);
		int len;
		InputStream in = new ByteArrayInputStream(total);
		MessageData_C9 dataC9 = new MessageData_C9();
		try {
			for(int i = 0; (len=in.read(bytes)) >= 0; i++) {
				if(len == 0) {
					i--;
					continue;
				}
				if(len != bytes.length) {
					byte[] tmp = new byte[len];
					System.arraycopy(bytes, 0, tmp, 0, len);
					dataC9.setContent(tmp);
				} else {
					dataC9.setContent(bytes);
				}
				dataC9.setPacketNum(i);
				sendMessage(dataC9);
			}
		} finally {
			in.close();
		}

	}
	public void testDexFileTransfer(String deviceSerialNum, String dex) throws Exception {
		byte[] total;
		if (dex == null) {
			StringBuilder sb = new StringBuilder();		
			sb.append("DEX-").append(deviceSerialNum).append('-');
			DateFormat df = new SimpleDateFormat("HHmmss-MMddyy");
			sb.append(df.format(new Date())).append("-SCHEDULED--SC00\r\n");
			/*sb.append("DXS*API0000001*VA*V1/2*1\r\nST*001*0001\r\n");
			sb.append("ID1*API0*STXXX*0011*0**0\r\nID4*2*3030*0\r\nID5*060930*0750\r\nID7***API\r\nCB1*API0*ST/130*0015\r\nVA1*38445*1051*0*0*0*0*0*0\r\nVA2*0*1*0*0\r\nVA3*5*1*0*0\r\nTA2*0*0*0*0\r\nCA1*0*0*0CA2*7980*375*0*0\r\nCA3*0*0*0*0*4220*0*3920*3*0*3683\r\nCA4*0*0*17290*15840\r\nCA7*0*0*0*0\r\nCA9*0*6780\r\nCA10*0*0\r\nCA15*0\r\nBA1*0*0*0\r\nDA1*EE100018426*EportEdge*3030\r\nDA2*30465*676*0*0\r\nDA4*0*0\r\nPA1*CAN**CAN\r\nPA2*18002*235\r\n");
			sb.append("PA1*100*150*100*0\r\nPA2*261*22790*0*0*0*0\r\nPA4*65536\r\nPA5*060930*0750\r\nPA1*101*5*101*0\r\nPA2*0*0*0*0*0*0\r\nPA4*0\r\nPA5*000000*0000\r\nPA1*102*150*102*0\r\nPA2*82*6855*0*0*0*0\r\nPA4*0\r\nPA5*060804*2255\r\nPA1*103*150*103*0\r\nPA2*0*0*0*0*0*0\r\nPA4*0\r\nPA5*000000*0000\r\nPA1*104*5*104*0\r\nPA2*32*160*0*0*0*0\r\nPA4*0\r\nPA5*050604*0356\r\nPA1*105*5*105*0\r\nPA2*0*0*0*0*0*0\r\nPA4*0\r\nPA5*000000*0000\r\nPA1*106*5*106*0");
			sb.append("EA5*050325*0451\r\nEA7*0*185\r\nG85*865\r\nSE*0*0001\r\nDXE*1*1\r\n");
			*/
			// sb.append("DXS*0000000000*VA*V1/1*1\r\nST*001*0001\r\nID1*0**9985***\r\nID4*2*1\r\nVA1*4752880*52934*4752880*52934\r\nVA2*0*0*0*0\r\nCA1*000802120275*VN4510 MDB  *313**0\r\nBA1*002080104256*VN2500/AE24 *3460**0\r\nCA2*0*0*0*0\r\nCA3*5269670*1069195*511875*36886*5269670*1069195*511875*36886\r\nCA4*531120*12535*531120*12535\r\nCA5*0\r\nCA6*0\r\nDA1*0*0*0**0\r\nDA2*5165*38*5165*38*0\r\nTA2*0*0*0*0\r\nLS*0001\r\nPA1*1*135*\r\nPA2*15986*1603250*15986*1603250\r\nPA5*120830*1905*2856\r\nPA1*2*135*\r\nPA2*9338*937375*9338*937375\r\nPA5*120801*2126*1767\r\nPA1*3*125*\r\nPA2*3138*316510*3138*316510\r\nPA5*120727*1639*458\r\nPA1*4*135*\r\nPA2*2367*228755*2367*228755\r\nPA5*120727*1715*570\r\nPA1*5*135*\r\nPA2*3138*315480*3138*315480\r\nPA5*120727*1802*1302\r\nPA1*6*125*\r\nPA2*1193*148975*1193*148975\r\nPA5*120727*1802*590\r\nPA1*7*150*\r\nPA2*6530*410625*6530*410625\r\nPA5*120727*1804*815\r\nPA1*8*150*\r\nPA2*8567*545625*8567*545625\r\nPA5*120727*1804*1285\r\nPA1*9*150*\r\nPA2*2677*246285*2677*246285\r\nPA5*120801*0501*2470\r\nLE*0001\r\nEA2*DO*1935\r\nEA2*CR**0\r\nEA7*32*32\r\nMA5*SWITCH*UNLOCK*1,4,6**10\r\nMA5*SEL1*1*15481*16300\r\nMA5*SEL2*2*8837*9408\r\nMA5*SEL3*3*2561*2660\r\nMA5*SEL4*4*2045*2188\r\nMA5*SEL5*5*2830*3266\r\nMA5*SEL6*6*1107*1121\r\nMA5*SEL7*7*6079*6261\r\nMA5*SEL8*8*7962*8279\r\nMA5*SEL9*9*2602*3537\r\nMA5*ERROR*CJ9*\r\nMA5*ERROR*CJ1*\r\nMA5*ERROR*DS*\r\nMA5*ERROR*DS*\r\nMA5*ERROR*DS*\r\nMA5*ERROR*DS*\r\nMA5*ERROR*DS*\r\nMA5*ERROR*TS*\r\nMA5*ERROR*TS*\r\nMA5*ERROR*TS*\r\nMA5*ERROR*TS*\r\nMA5*ERROR*TS*\r\nMA5*ERROR*BOPN*\r\nMA5*ERROR*BOPN*\r\nMA5*ERROR*BOPN*\r\nMA5*ERROR*BOPN*\r\nMA5*ERROR*BOPN*\r\nMA5*TUBE1**20*104*52*11\r\nG85*093C\r\nSE*77*0001\r\nDXE*1*1");
			/*sb.append("DXS*0000000000*VA*V1/1*1\r\nST*001*0001\r\nID1*0**9985\u0000\u0001\u0002***\r\nID4*2*1\r\nVA1*4752880*52934*4752880*52934*>*<*&*'*\"\r\nVA2*0*0*0*0\r\nCA1*000802120275*VN4510 MDB  *313**0\r\nBA1*002080104256*VN2500/AE24 *3460**0\r\nCA2*0*0*0*0\r\nCA3*5269670*1069195*511875*36886*5269670*1069195*511875*36886\r\nCA4*531120*12535*531120*12535\r\nCA5*0\r\nCA6*0\r\nDA1*0*0*0**0\r\nDA2*5165*38*5165*38*0\r\nTA2*0*0*0*0\r\nLS*0001\r\nPA1*1*135*\r\nPA2*15986*1603250*15986*1603250\r\nPA5*:120830*:1905*:2856\r\nPA1*2*135*\r\nPA2*9338*937375*9338*937375\r\nPA5*120801*2126*1767\r\nPA1*3*125*\r\nPA2*3138*316510*3138*316510\r\nPA5*120727*1639*458\r\nPA1*4*135*\r\nPA2*2367*228755*2367*228755\r\nPA5*120727*1715*570\r\nPA1*5*135*\r\nPA2*3138*315480*3138*315480\r\nPA5*120727*1802*1302\r\nPA1*6*125*\r\nPA2*1193*148975*1193*148975\r\nPA5*120727*1802*590\r\nPA1*7*150*\r\nPA2*6530*410625*6530*410625\r\nPA5*120727*1804*815\r\nPA1*8*150*\r\nPA2*8567*545625*8567*545625\r\nPA5*120727*1804*1285\r\nPA1*9*150*\r\nPA2*2677*246285*2677*246285\r\nPA5*120801*0501*2470\r\nLE*0001\r\nEA2*DO*1935\r\nEA2*CR**0\r\nEA7*32*32\r\nMA5*SWITCH*UNLOCK*1,4,6**10\r\nMA5*SEL1*1*15481*16300\r\nMA5*SEL2*2*8837*9408\r\nMA5*SEL3*3*2561*2660\r\nMA5*SEL4*4*2045*2188\r\nMA5*SEL5*5*2830*3266\r\nMA5*SEL6*6*1107*1121\r\nMA5*SEL7*7*6079*6261\r\nMA5*SEL8*8*7962*8279\r\nMA5*SEL9*9*2602*3537\r\nMA5*ERROR*CJ9*");
			sb.append(System.currentTimeMillis());
			sb.append("*\r\nMA5*ERROR*CJ1*\r\nMA5*ERROR*DS*");
			sb.append(System.currentTimeMillis());
			sb.append("*\r\nMA5*ERROR*DS*\r\nMA5*ERROR*DS*\r\nMA5*ERROR*DS*\r\nMA5*ERROR*DS*\r\nMA5*ERROR*TS*\r\nMA5*ERROR*TS*\r\nMA5*ERROR*TS*\r\nMA5*ERROR*TS*\r\nMA5*ERROR*TS*\r\nMA5*ERROR*BOPN*\r\nMA5*ERROR*BOPN*\r\nMA5*ERROR*BOPN*\r\nMA5*ERROR*BOPN*\r\nMA5*ERROR*BOPN*\r\nMA5*TUBE1**20*104*52*11\r\n");
			sb.append("EA1*EAP*\r\n");
			sb.append("EA1*EB*\r\n");
			sb.append("EA1*EBE*\r\n");
			sb.append("EA1*EBJ*\r\n");
			sb.append("EA1*EC*\r\n");
			sb.append("G85*093C\r\nSE*77*0001\r\nDXE*1*1");*/
			sb.append("DXS*\r\nEA1*EK2M*160718*0957***ePort DEX- SC81, EC52: Please check cable @ VMC end.\r\nDXE*1*1");
			total = sb.toString().getBytes(ProcessingConstants.US_ASCII_CHARSET);
		} else
			total = dex.getBytes(ProcessingConstants.US_ASCII_CHARSET);
		MessageData_C8 dataC8 = new MessageData_C8();
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(0);
		dataC8.setCreationTime(cal);
		dataC8.setEventId(masterId++);
		//dataC8.setFileName();
		dataC8.setFilesRemaining(1);
		dataC8.setFileType(FileType.DEX_FILE);
		dataC8.setTotalBytes(total.length);
		byte[] bytes = new byte[256];
		dataC8.setTotalPackets((int)Math.ceil(total.length / (double) bytes.length));
		CRC crc = new CRC(CRCType.CRC16Server, CRCType.CRC16Server.calculate(new ByteArrayInputStream(total)));
		dataC8.setCrc(crc);
		sendMessage(dataC8);
		int len;
		InputStream in = new ByteArrayInputStream(total);
		MessageData_C9 dataC9 = new MessageData_C9();
		try {
			for(int i = 0; (len=in.read(bytes)) >= 0; i++) {
				if(len == 0) {
					i--;
					continue;
				}
				if(len != bytes.length) {
					byte[] tmp = new byte[len];
					System.arraycopy(bytes, 0, tmp, 0, len);
					dataC9.setContent(tmp);
				} else {
					dataC9.setContent(bytes);
				}
				dataC9.setPacketNum(i);
				sendMessage(dataC9);
			}
		} finally {
			in.close();
		}

	}
	public void testDexFileTransfer(String deviceSerialNum) throws Exception {
		testDexFileTransfer(deviceSerialNum, null);
	}
	public void testLongFileTransfer() throws Exception {
		File file = new File("E:\\TEMP\\tim-concurrent-collections-root-1.0.2-src.zip");
		ReRixMessage response = sendMessage(constructStartFileTransferMessage(file), 1);
		if(response != null)
			log.info("RESPONSE:\n" + new CAMessage().readResponseString(response));
		byte[] bytes = new byte[1024];
		int len;
		FileInputStream in = new FileInputStream(file);
		try {
			for(int i = 0; (len=in.read(bytes)) >= 0; i++) {
				response = sendMessage(constructFileTransferMessage(bytes, len, i), 1);
				if(response != null)
					log.info("RESPONSE:\n" + new CAMessage().readResponseString(response));
			}
		} finally {
			in.close();
		}

	}
	public void testLongFileTransferBadLength() throws Exception {
		testLongFileTransferBadLength(new File("E:\\TEMP\\tim-concurrent-collections-root-1.0.2-src.zip"));
	}

	public void testLongFileTransferBadLength(File file) throws Exception {
		ReRixMessage response = sendMessage(constructStartFileTransferMessage(file), 1);
		if(response != null)
			log.info("RESPONSE:\n" + new CAMessage().readResponseString(response));
		byte[] bytes = new byte[200];
		int len;
		FileInputStream in = new FileInputStream(file);
		try {
			for(int i = 0; (len=in.read(bytes)) >= 0 && i < 3; i++) {
				if(i < 1)
					response = sendMessage(constructFileTransferMessage(bytes, len, i), 1);
				else
					response = sendMessage(constructFileTransferBadLengthMessage(bytes, len, i), 1);
				if(response != null)
					log.info("RESPONSE:\n" + new CAMessage().readResponseString(response));
			}
		} finally {
			in.close();
		}

	}
	public void testLongFileTransferBadCRC() throws Exception {
		testLongFileTransferBadCRC(new File("E:\\TEMP\\cvs.usatech.com.cer"));
	}

	public void testLongFileTransferBadCRC(File file) throws Exception {
		long length = file.length();
		byte[] bytes = new byte[200];
		MessageData_C8 dataC8 = new MessageData_C8();
		dataC8.setCreationTime(Calendar.getInstance());
		dataC8.setEventId(0);
		dataC8.setFileName(file.getName());
		dataC8.setFilesRemaining(0);
		dataC8.setFileType(FileType.LOG_FILE);
		dataC8.setTotalBytes(length);
		dataC8.setTotalPackets((int) Math.ceil(length / (double) bytes.length));
		CRCType crcType = CRCType.CRC16Server;
		byte[] crcValue;
		FileInputStream in = new FileInputStream(file);
		try {
			crcValue = crcType.calculate(in);
		} finally {
			in.close();
		}
		crcValue[0] = (byte) ~crcValue[0]; // make it bad
		dataC8.setCrc(new CRC(crcType, crcValue));
		MessageData reply = sendMessage(dataC8);
		int len;
		MessageData_C9 dataC9 = new MessageData_C9();
		in = new FileInputStream(file);
		try {
			for(int i = 0; reply.getMessageType() == MessageType.GENERIC_RESPONSE_4_1 && ((MessageData_CB) reply).getResultCode() == GenericResponseResultCode.OKAY && (len = in.read(bytes)) >= 0; i++) {
				if(len == 0) {
					i--;
					continue;
				}
				if(len != bytes.length) {
					byte[] tmp = new byte[len];
					System.arraycopy(bytes, 0, tmp, 0, len);
					dataC9.setContent(tmp);
				} else {
					dataC9.setContent(bytes);
				}
				dataC9.setPacketNum(i);
				reply = sendMessage(dataC9);
			}
		} finally {
			in.close();
		}
	}
	public void testInitV4(InitializationReason reason, int propertyListVersion) throws Exception {
		/*ReRixMessage response = sendMessage(constructInitV4Message((byte)0, 4001005, 0, (byte)13,
				"Client Emulator 1.0 firmware version",
				"EE100000179",
				"Client Emulator 1.0 Device Info",
				"Client Emulator 1.0 Terminal Info"), 1);
		*/
		MessageData_CB dataCB = (MessageData_CB)sendMessage(constructInitV4Message(reason, 4001009, propertyListVersion, DeviceType.EDGE,
				"Client Emulator 1.1 firmware version",
				/*"EE199900101",*/
				"VJ100000001",
				"Client Emulator 1.1 Device Info",
				"Client Emulator 1.1 Terminal Info"));
		if(dataCB != null && dataCB.getServerActionCode() == GenericResponseServerActionCode.PROCESS_PROP_LIST) {
			Map<Integer, String> propertyValues = ((PropertyValueListAction) dataCB.getServerActionCodeData()).getPropertyValues();
			String deviceName = propertyValues.get(51);
			String encryptionKeyHex = propertyValues.get(52);
			log.info("Got DeviceName: " + deviceName + "; Encryption Key: " + encryptionKeyHex);
			if(deviceName != null && deviceName.length() > 0 && encryptionKeyHex != null && encryptionKeyHex.length() > 0) {
				this.evNumber = deviceName;
				this.encryptionKey = ByteArrayUtils.fromHex(encryptionKeyHex);
			}
		}
	}
	
	public void testInitV4(String serialNumber, InitializationReason reason, int propertyListVersion) throws Exception {
		testInitV4 (serialNumber, reason, propertyListVersion, "2.04.005jI");
	}	
	
	public void testInitV4(String serialNumber, InitializationReason reason, int propertyListVersion, String deviceFirmwareVersion) throws Exception {
		MessageData_CB dataCB = (MessageData_CB)sendMessage(constructInitV4Message(reason, 4001009, propertyListVersion, DeviceType.EDGE,
				deviceFirmwareVersion,
				serialNumber,
				"Client Emulator 1.1 Device Info",
				"Client Emulator 1.1 Terminal Info"));
		if(dataCB != null && dataCB.getServerActionCode() == GenericResponseServerActionCode.PROCESS_PROP_LIST) {
			Map<Integer, String> propertyValues = ((PropertyValueListAction) dataCB.getServerActionCodeData()).getPropertyValues();
			String deviceName = propertyValues.get(51);
			String encryptionKeyHex = propertyValues.get(52);
			log.info("Got DeviceName: " + deviceName + "; Encryption Key: " + encryptionKeyHex);
			if(deviceName != null && deviceName.length() > 0 && encryptionKeyHex != null && encryptionKeyHex.length() > 0) {
				this.evNumber = deviceName;
				this.encryptionKey = ByteArrayUtils.fromHex(encryptionKeyHex);
			}
		}
	}	
	
	public void testComponents(String serialNumber) throws Exception {
		testComponents(serialNumber, "NEO v1.00.088.1");
	}
	
	public void testComponents(String serialNumber, String bezelRevision) throws Exception {
		MessageData_C1 dataC1 = new MessageData_C1();
		dataC1.setBaseComponentType(ComponentType.EPORT_EDGE_G9);
		dataC1.setCharset(ProcessingConstants.US_ASCII_CHARSET);
		dataC1.setDefaultCurrency(Currency.getInstance("USD"));
		dataC1.getBaseComponent().setLabel("0");
		dataC1.getBaseComponent().setManufacturer("USA Technologies");
		dataC1.getBaseComponent().setModel("Client Emulator V1.1");
		dataC1.getBaseComponent().setRevision("1.1");
		dataC1.getBaseComponent().setSerialNumber(serialNumber);
		Component comp = dataC1.addComponent();
		((MessageData_C1.ComponentData)comp).setComponentNumber(1);
		comp.setComponentType(ComponentType.VENDING_MACHINE);
		comp.setLabel("TE0001");
		comp.setManufacturer("Dell");
		comp.setModel("Precision");
		comp = dataC1.addComponent();
		((MessageData_C1.ComponentData)comp).setComponentNumber(2);
		comp.setComponentType(ComponentType.GPRS_MODEM);
		comp.setLabel("310380137444000");
		comp.setManufacturer("EMULATION");
		comp.setModel("EMU-1");
		comp.setRevision("01.01.001");
		comp.setSerialNumber("89310380106030979589");
		comp = dataC1.addComponent();
		((MessageData_C1.ComponentData)comp).setComponentNumber(3);
		comp.setComponentType(ComponentType.BEZEL);
		comp.setLabel("0");
		comp.setManufacturer("IdTech");
		comp.setModel("Vendi");
		comp.setRevision(bezelRevision);
		comp.setSerialNumber("603T355905");		
		sendMessage(dataC1);
	}
	
	public void testComponents(String deviceSerialNumber, ComponentType deviceType, String modemSerialNumber) throws Exception {
		MessageData_C1 dataC1 = new MessageData_C1();
		dataC1.setBaseComponentType(ComponentType.EPORT_EDGE_G9);
		dataC1.setCharset(ProcessingConstants.US_ASCII_CHARSET);
		dataC1.setDefaultCurrency(Currency.getInstance("USD"));
		dataC1.getBaseComponent().setLabel("0");
		dataC1.getBaseComponent().setManufacturer("USA Technologies");
		dataC1.getBaseComponent().setModel("Client Emulator V1.1");
		dataC1.getBaseComponent().setRevision("1.1");
		dataC1.getBaseComponent().setSerialNumber(deviceSerialNumber);
		dataC1.getBaseComponent().setComponentType(deviceType);
		Component comp = dataC1.addComponent();
		((MessageData_C1.ComponentData)comp).setComponentNumber(1);
		comp.setComponentType(ComponentType.HSPA5_ESEYE_MODEM);
		comp.setLabel("310380137444000");
		comp.setManufacturer("Telit");
		comp.setModel("000000000000000");
		comp.setRevision("01");
		comp.setSerialNumber(modemSerialNumber);
		sendMessage(dataC1);
	}

	
	public void testComponents() throws Exception {
		testComponents(serialNumber, "NEO v1.00.088.1");
	}
	
	public void testUpdateStatusRequest() throws Exception {
		MessageData_CA ca = new MessageData_CA();
		ca.setRequestType(GenericRequestType.UPDATE_STATUS_REQUEST);
		sendMessage(ca);
	}
	
	public void testUpdateStatusResponse() throws Exception {
		MessageData_CB ca = new MessageData_CB();
		ca.setClientActionCode(GenericResponseClientActionCode.UPDATE_STATUS_REQUEST);
		sendMessage(ca);
	}

	public void sendUpdateStatusResponse() throws Exception {
		MessageData_CB cb = new MessageData_CB();
		//cb.setMessageId(messageId.getAndIncrement());
		cb.setResultCode(GenericResponseResultCode.OKAY);
		cb.setClientActionCode(GenericResponseClientActionCode.UPDATE_STATUS_REQUEST);
		sendMessage(cb);
	}
	
	public void testFillSettlement2(long eventId) throws Exception {
		MessageData_C5 md = new MessageData_C5();
		md.setBatchId(0);
		md.setCreationTime(Calendar.getInstance());
		md.setDirection(MessageDirection.CLIENT_TO_SERVER);
		md.setEventId(eventId);
		md.setEventType(EventType.FILL);
		//md.setMessageId(messageId.getAndIncrement());
		//md.setMessageNumber(0); //we will ignore this anyway
		md.setNewBatchId(1);
		md.setContentFormat(SettlementFormat.FORMAT_1);
		Format1Content sfd = (Format1Content)md.getContentFormatData();
		sfd.setCashItems(2);
		sfd.setCashAmount(new BigDecimal(22));
		sfd.setCashlessItems(3);
		sfd.setCashlessAmount(new BigDecimal(33));
		sfd.setAppCashItems(7);
		sfd.setAppCashAmount(new BigDecimal(789));
		sfd.setAppCashlessItems(11);
		sfd.setAppCashlessAmount(new BigDecimal(2390));
		sendMessage(md);
	}
	
	public String constructFillSettlementMessage(long eventId) {
		MessageData_C5 md = new MessageData_C5();
		md.setBatchId(0);
		md.setCreationTime(Calendar.getInstance());
		md.setDirection(MessageDirection.CLIENT_TO_SERVER);
		md.setEventId(eventId);
		md.setEventType(EventType.FILL);
		//md.setMessageId(messageId.getAndIncrement());
		//md.setMessageNumber(0); //we will ignore this anyway
		md.setNewBatchId(1);
		md.setContentFormat(SettlementFormat.FORMAT_0);
		Format0Content sfd = (Format0Content)md.getContentFormatData();
		sfd.setCashItems(2);
		sfd.setCashAmount(new BigDecimal(22));
		sfd.setCashlessItems(3);
		sfd.setCashlessAmount(new BigDecimal(33));
		ByteBuffer bb = ByteBuffer.allocate(1024);
		md.writeData(bb, false);
		return StringUtils.toHex(bb.array(), 1, bb.position() - 1);
	}
	public String constructScheduledSettlementMessage(long eventId) {
		MessageData_C5 md = new MessageData_C5();
		md.setBatchId(3);
		md.setCreationTime(Calendar.getInstance());
		md.setDirection(MessageDirection.CLIENT_TO_SERVER);
		md.setEventId(eventId);
		md.setEventType(EventType.SCHEDULED);
		//md.setMessageId(messageId.getAndIncrement());
		//md.setMessageNumber(0); //we will ignore this anyway
		md.setNewBatchId(4);
		md.setContentFormat(SettlementFormat.FORMAT_0);
		Format0Content sfd = (Format0Content)md.getContentFormatData();
		sfd.setCashItems(4);
		sfd.setCashAmount(new BigDecimal(44));
		sfd.setCashlessItems(5);
		sfd.setCashlessAmount(new BigDecimal(55));
		ByteBuffer bb = ByteBuffer.allocate(1024);
		md.writeData(bb, false);
		return StringUtils.toHex(bb.array(), 1, bb.position() - 1);
	}
	public String constructSaleMessage(long transactionId) {
		MessageData_C4 md = new MessageData_C4();
		md.setBatchId(1);
		md.setDirection(MessageDirection.CLIENT_TO_SERVER);
		md.setMessageId(messageId.getAndIncrement());
		//md.setMessageNumber(0); //we will ignore this anyway
		md.setReceiptResult(ReceiptResult.UNAVAILABLE);
		md.setSaleAmount(new BigDecimal(595));
		md.setSaleResult(SaleResult.SUCCESS);
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(transactionId*1000);
		md.setSaleSessionStart(cal);
		md.setSaleType(SaleType.ACTUAL);
		md.setTransactionId(transactionId);
		md.setLineItemFormat((byte)0);
		Format0LineItem li = (Format0LineItem)md.addLineItem();
		li.setComponentNumber(1);
		li.setItem(200);
		li.setDescription("011E");
		//li.setDescription("286");
		li.setDuration(3);
		li.setPrice(new BigDecimal(110));
		li.setQuantity(1);
		li.setSaleResult(SaleResult.SUCCESS);
		li = (Format0LineItem)md.addLineItem();
		li.setComponentNumber(1);
		li.setItem(200);
		li.setDescription("002F");
		li.setDuration(3);
		li.setPrice(new BigDecimal(110));
		li.setQuantity(1);
		li.setSaleResult(SaleResult.SUCCESS);
		li = (Format0LineItem)md.addLineItem();
		li.setComponentNumber(1);
		li.setItem(200);
		li.setDescription("001E");
		//li.setDescription("30"); //decimal
		li.setDuration(7);
		li.setPrice(new BigDecimal(125));
		li.setQuantity(3);
		li.setSaleResult(SaleResult.SUCCESS);
		ByteBuffer bb = ByteBuffer.allocate(1024);
		md.writeData(bb, false);
		//return StringUtils.toHex(bb.array(), 1, bb.position() - 1);
		return StringUtils.toHex(bb.array(), 0, bb.position());
	}

	public void testSale(long transactionId, SaleType saleType, int items, boolean convFee) throws Exception {
		MessageData_C4 md = new MessageData_C4();
		md.setBatchId(1);
		md.setDirection(MessageDirection.CLIENT_TO_SERVER);
		//md.setMessageId(messageId.getAndIncrement());
		//md.setMessageNumber(0); //we will ignore this anyway
		md.setReceiptResult(ReceiptResult.UNAVAILABLE);
		md.setSaleResult(SaleResult.SUCCESS);
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(transactionId*1000);
		md.setSaleSessionStart(cal);
		md.setSaleType(saleType);
		md.setTransactionId(transactionId);
		md.setLineItemFormat((byte)0);
		Format0LineItem li;
		BigDecimal total = BigDecimal.ZERO;
		int n = 0;
		for(int i = 0; i < items; i++) {
			li = (Format0LineItem)md.addLineItem();
			li.setComponentNumber(1);
			li.setItem(200);
			switch(i % 3) {
				case 0:
					li.setDescription("0025");
					li.setDuration(2);
					li.setPrice(new BigDecimal(10));
					li.setQuantity(1);
					li.setSaleResult(SaleResult.SUCCESS);
					break;
				case 1:
					li.setDescription("000C");
					li.setDuration(3);
					li.setPrice(new BigDecimal(115));
					li.setQuantity(2);
					li.setSaleResult(SaleResult.SUCCESS);
					break;
				case 2:
					li.setDescription("2B0D");
					li.setDuration(7);
					li.setPrice(new BigDecimal(125));
					li.setQuantity(3);
					li.setSaleResult(SaleResult.SUCCESS);
					break;
			}
			total = total.add(li.getPrice().multiply(BigDecimal.valueOf(li.getQuantity())));
			n += li.getQuantity();
		}
		if(convFee) {
			li = (Format0LineItem)md.addLineItem();
			li.setComponentNumber(1);
			li.setItem(203);
			li.setDuration(0);
			li.setPrice(new BigDecimal(7));
			li.setQuantity(n);
			li.setSaleResult(SaleResult.SUCCESS);
			total = total.add(li.getPrice().multiply(BigDecimal.valueOf(li.getQuantity())));
		}
		md.setSaleAmount(total);

		sendMessage(md);
	}

	public void testSingleSale(long transactionId, SaleType saleType, BigDecimal saleTotal, boolean convFee) throws Exception {
		MessageData_C4 md = new MessageData_C4();
		md.setBatchId(1);
		md.setDirection(MessageDirection.CLIENT_TO_SERVER);
		//md.setMessageId(messageId.getAndIncrement());
		//md.setMessageNumber(0); //we will ignore this anyway
		md.setReceiptResult(ReceiptResult.UNAVAILABLE);
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(transactionId*1000);
		md.setSaleSessionStart(cal);
		md.setSaleType(saleType);
		md.setTransactionId(transactionId);
		md.setLineItemFormat((byte)0);
		if(saleTotal.signum() == 0) {
			md.setSaleResult(SaleResult.CANCELLED_BY_USER);
			/*md.setSaleResult(SaleResult.SUCCESS);
			Format0LineItem li = (Format0LineItem)md.addLineItem();
			li.setComponentNumber(1);
			li.setItem(200);
			li.setDescription("001C");
			li.setDuration(2);
			li.setPrice(saleTotal);
			li.setQuantity(1);
			li.setSaleResult(SaleResult.SUCCESS);*/
		} else {
			md.setSaleResult(SaleResult.SUCCESS);
			Format0LineItem li = (Format0LineItem)md.addLineItem();
			li.setComponentNumber(1);
			li.setItem(200);
			li.setDescription("001C");
			li.setDuration(2);
			li.setPrice(saleTotal);
			li.setQuantity(1);
			li.setSaleResult(SaleResult.SUCCESS);
			if(convFee) {
				li = (Format0LineItem)md.addLineItem();
				li.setComponentNumber(1);
				li.setItem(203);
				li.setDuration(0);
				li.setPrice(new BigDecimal(7));
				li.setQuantity(1);
				li.setSaleResult(SaleResult.SUCCESS);
				saleTotal = saleTotal.add(li.getPrice().multiply(BigDecimal.valueOf(li.getQuantity())));
			}
		}
		md.setSaleAmount(saleTotal);
		sendMessage(md);
	}
	
	public void testFailedSale(long transactionId, SaleType saleType) throws Exception {
		MessageData_C4 md = new MessageData_C4();
		md.setBatchId(1);
		md.setDirection(MessageDirection.CLIENT_TO_SERVER);
		//md.setMessageId(messageId.getAndIncrement());
		//md.setMessageNumber(0); //we will ignore this anyway
		md.setReceiptResult(ReceiptResult.UNAVAILABLE);
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(transactionId*1000);
		md.setSaleSessionStart(cal);
		md.setSaleType(saleType);
		md.setTransactionId(transactionId);
		md.setLineItemFormat((byte)0);
		md.setSaleResult(SaleResult.CANCELLED_MACHINE_FAILURE);
		md.setSaleAmount(BigDecimal.ZERO);
		sendMessage(md);
	}

	/**
	 * @param i
	 * @param string
	 * @param j
	 * @return
	 */
	public String constructGenRespV4_1Message(int resultCode, String responseMessage, int actionCode) {
		StringBuilder sb = new StringBuilder();
		sb.append("CB");
		String msgId = Long.toHexString(messageId.getAndIncrement());
		padLeft(sb, msgId, 8);
		appendHex(sb, resultCode, 2);
		if(responseMessage == null) {
			appendHex(sb, 0, 2);
		} else {
			appendHex(sb, responseMessage.length(), 2);
			sb.append(toHex(responseMessage.getBytes()));
		}
		appendHex(sb, actionCode, 2);
		return sb.toString();
	}

	public String constructGenReq_PropertyList_Message() {
		StringBuilder sb = new StringBuilder();
		sb.append("CA");
		String msgId = Long.toHexString(messageId.getAndIncrement());
		padLeft(sb, msgId, 8);
		appendHex(sb, 2, 2);
		//byte[] propList = "0".getBytes(ProcessingConstants.US_ASCII_CHARSET);
		byte[] propList = "0-13|23|25-26|50-67|70|71|79-87|102-108".getBytes(ProcessingConstants.US_ASCII_CHARSET);
		appendHex(sb, propList.length, 4);
		StringUtils.appendHex(sb, propList, 0, propList.length);
		return sb.toString();
	}
	public String constructGenReq_USR_V4_1Message(int bitmap) {
		StringBuilder sb = new StringBuilder();
		sb.append("CA");
		String msgId = Long.toHexString(messageId.getAndIncrement());
		padLeft(sb, msgId, 8);
		appendHex(sb, 0, 2);
		appendHex(sb, bitmap, 4);
		return sb.toString();
	}

	public String constructGenResp_USR_V4_1Message(GenericResponseClientActionCode actionCode) {
		StringBuilder sb = new StringBuilder();
		sb.append("CB");
		String msgId = Long.toHexString(messageId.getAndIncrement());
		padLeft(sb, msgId, 8);
		appendHex(sb, GenericResponseResultCode.OKAY.getValue(), 2);
		appendHex(sb, 0, 2); //message
		appendHex(sb, actionCode.getValue(), 2);
		return sb.toString();
	}
	public String constructGenReq_AR_V4_1Message() {
		StringBuilder sb = new StringBuilder();
		sb.append("CA");
		String msgId = Long.toHexString(messageId.getAndIncrement());
		padLeft(sb, msgId, 8);
		appendHex(sb, 3, 2);
		sb.append("0000");
		return sb.toString();
	}
	
	public String constructShortFileTransferMessage() {
		StringBuilder sb = new StringBuilder();
		sb.append("C7");
		String msgId = Long.toHexString(messageId.getAndIncrement());
		padLeft(sb, msgId, 8);
		padLeft(sb, Long.toHexString(System.currentTimeMillis()- 2000), 8); // start time
		padLeft(sb, Integer.toHexString(-16), 2); //offset
		sb.append("00000000"); //event id
		sb.append("00"); //files remaining
		sb.append("0009"); //file type
		sb.append("00"); //file name
		String content = "This is a test to see if we can do a short file transfer. We will see if this works!";
		byte[] bytes = content.getBytes(ProcessingConstants.US_ASCII_CHARSET);
		padLeft(sb, Integer.toHexString(bytes.length), 4); //file payload
		StringUtils.appendHex(sb, bytes, 0, bytes.length);

		return sb.toString();
	}
	public String constructShortFileTransferPropertyIndexListMessage() {
		StringBuilder sb = new StringBuilder();
		sb.append("C7");
		String msgId = Long.toHexString(messageId.getAndIncrement());
		padLeft(sb, msgId, 8);
		padLeft(sb, Long.toHexString(System.currentTimeMillis()- 2000), 8); // start time
		padLeft(sb, Integer.toHexString(-16), 2); //offset
		sb.append("00000000"); //event id
		sb.append("00"); //files remaining
		sb.append("0014"); //file type
		sb.append("00"); //file name
		String content = "0-13|23|25-26|50-67|70|71|79-87|102-108";
		byte[] bytes = content.getBytes(ProcessingConstants.US_ASCII_CHARSET);
		padLeft(sb, Integer.toHexString(bytes.length), 4); //file payload
		StringUtils.appendHex(sb, bytes, 0, bytes.length);

		return sb.toString();
	}
	public String constructStartFileTransferMessage(File file) throws IOException {
		long length = file.length();
		StringBuilder sb = new StringBuilder();
		sb.append("C8");
		String msgId = Long.toHexString(messageId.getAndIncrement());
		padLeft(sb, msgId, 8);
		padLeft(sb, Long.toHexString(System.currentTimeMillis()- 2000), 8); // start time
		padLeft(sb, Integer.toHexString(-16), 2); //offset
		sb.append("00000000"); //event id
		sb.append("00"); //files remaining
		padLeft(sb, Integer.toHexString((int)Math.ceil(length/1024.0)), 4); //blocks
		padLeft(sb, Long.toHexString(length), 8); //bytes
		sb.append("0009"); //file type
		//crc
		CRCType crcType = CRCType.CRC16Server;
		padLeft(sb, Integer.toHexString(crcType.getValue()), 2); // crc type
		FileInputStream in = new FileInputStream(file);
		try {
			byte[] crc = crcType.calculate(in);
			StringUtils.appendHex(sb, crc, 0, crc.length);
		} finally {
			in.close();
		}
		sb.append("00"); //file name

		return sb.toString();
	}
	public String constructStartFileTransferBadCRCMessage(File file) throws IOException {
		long length = file.length();
		StringBuilder sb = new StringBuilder();
		sb.append("C8");
		String msgId = Long.toHexString(messageId.getAndIncrement());
		padLeft(sb, msgId, 8);
		padLeft(sb, Long.toHexString(System.currentTimeMillis()- 2000), 8); // start time
		padLeft(sb, Integer.toHexString(-16), 2); //offset
		sb.append("00000000"); //event id
		sb.append("00"); //files remaining
		padLeft(sb, Integer.toHexString((int)Math.ceil(length/1024.0)), 4); //blocks
		padLeft(sb, Long.toHexString(length), 8); //bytes
		sb.append("0009"); //file type
		//crc
		CRCType crcType = CRCType.CRC16Server;
		padLeft(sb, Integer.toHexString(crcType.getValue()), 2); // crc type
		FileInputStream in = new FileInputStream(file);
		try {
			byte[] crc = crcType.calculate(in);
			crc[0] = (byte)~crc[0];
			StringUtils.appendHex(sb, crc, 0, crc.length);
		} finally {
			in.close();
		}
		sb.append("00"); //file name

		return sb.toString();
	}

	public void testGxFileTransfer(File file, FileType fileType) throws Exception {
		String fileName = file.getName();
		InputStream in;
		long length;
		if(fileName.endsWith(".hex")) {
			fileName = fileName.substring(0, fileName.length() - 4);
			in = new FromHexInputStream(new FileReader(file));
			length = file.length() / 2;
		} else {
			in = new FileInputStream(file);
			length = file.length();
		}
		try {
			MessageData_A4 dataA4 = new MessageData_A4();
			dataA4.setFileName(fileName);
			dataA4.setFileType(fileType);
			dataA4.setGroup((byte) 0);
			dataA4.setTotalBytes(length);
			byte[] bytes = new byte[250];
			dataA4.setTotalPackets((int) Math.ceil(length / (double) bytes.length));
			MessageData reply = sendMessage(dataA4);
			if(reply.getMessageType() != MessageType.FILE_XFER_START_ACK_3_0)
				throw new IOException("Unexpected reply " + reply);
			int len;
			MessageData_A6 dataA6 = new MessageData_A6();
			for(int i = 0; (len = in.read(bytes)) >= 0; i++) {
				if(len == bytes.length)
					dataA6.setContent(bytes);
				else {
					byte[] tmp = new byte[len];
					System.arraycopy(bytes, 0, tmp, 0, len);
					dataA6.setContent(tmp);
				}
				dataA6.setGroup((byte) 0);
				dataA6.setPacketNum(i);
				reply = sendMessage(dataA6);
				if(reply.getMessageType() != MessageType.FILE_XFER_ACK_3_0)
					throw new IOException("Unexpected reply " + reply);
			}
		} finally {
			in.close();
		}
	}

	public void testGxFileTransfer(String fileName, byte[] content, FileType fileType) throws Exception {
		int length = content.length;
		MessageData_A4 dataA4 = new MessageData_A4();
		dataA4.setFileName(fileName);
		dataA4.setFileType(fileType);
		dataA4.setGroup((byte) 0);
		dataA4.setTotalBytes(length);
		byte[] bytes = new byte[250];
		dataA4.setTotalPackets((int) Math.ceil(length / (double) bytes.length));
		MessageData reply = sendMessage(dataA4);
		if(reply.getMessageType() != MessageType.FILE_XFER_START_ACK_3_0)
			throw new IOException("Unexpected reply " + reply);
		MessageData_A6 dataA6 = new MessageData_A6();
		for(int i = 0; i * bytes.length < length; i++) {
			byte[] tmp;
			if(length >= (i + 1) * bytes.length)
				tmp = bytes;
			else {
				tmp = new byte[length - i * bytes.length];
			}
			System.arraycopy(content, 0, tmp, 0, tmp.length);
			dataA6.setContent(tmp);
			dataA6.setGroup((byte) 0);
			dataA6.setPacketNum(i);
			reply = sendMessage(dataA6);
			if(reply.getMessageType() != MessageType.FILE_XFER_ACK_3_0)
				throw new IOException("Unexpected reply " + reply);
		}
	}
	public String constructFileTransferMessage(byte[] bytes, int length, int packetNum) throws IOException {
		StringBuilder sb = new StringBuilder();
		sb.append("C9");
		String msgId = Long.toHexString(messageId.getAndIncrement());
		padLeft(sb, msgId, 8);
		padLeft(sb, Integer.toHexString(packetNum), 4); //packet #
		padLeft(sb, Integer.toHexString(length), 4); //# of bytes
		StringUtils.appendHex(sb, bytes, 0, length);
		return sb.toString();
	}
	public String constructFileTransferBadLengthMessage(byte[] bytes, int length, int packetNum) throws IOException {
		StringBuilder sb = new StringBuilder();
		sb.append("C9");
		String msgId = Long.toHexString(messageId.getAndIncrement());
		padLeft(sb, msgId, 8);
		padLeft(sb, Integer.toHexString(packetNum), 4); //packet #
		padLeft(sb, Integer.toHexString(length + 1), 4); //# of bytes
		StringUtils.appendHex(sb, bytes, 0, length);
		return sb.toString();
	}
	public static void testLocalBatch() throws Exception {
		String host = "127.0.0.1";
		int port = 443;
		String evNumber = "EV033475";
		String serialNumber = "G5059910";
		String encryptKey = "1773076481679196";
		String card = "4300123412341017=12100001";
		Date startDate = new Date(2008-1900, 0, 2);

		ClientEmulator ce = new ClientEmulator(host, port, evNumber, encryptKey.getBytes());
		ce.sendTranAndFillMessages((byte)1, evNumber, serialNumber, encryptKey, card, 1, startDate);
	}
	public void testFillSettlement(long eventId) throws Exception {
		MessageData_C5 md = new MessageData_C5();
		md.setBatchId(0);
		md.setCreationTime(Calendar.getInstance());
		md.setDirection(MessageDirection.CLIENT_TO_SERVER);
		md.setEventId(eventId);
		md.setEventType(EventType.FILL);
		//md.setMessageId(messageId.getAndIncrement());
		//md.setMessageNumber(0); //we will ignore this anyway
		md.setNewBatchId(1);
		md.setContentFormat(SettlementFormat.FORMAT_0);
		Format0Content sfd = (Format0Content)md.getContentFormatData();
		sfd.setCashItems(2);
		sfd.setCashAmount(new BigDecimal(22));
		sfd.setCashlessItems(3);
		sfd.setCashlessAmount(new BigDecimal(33));
		sendMessage(md);

	}
	public void testScheduledSettlement(long eventId) throws Exception {
		ReRixMessage response = sendMessage(constructScheduledSettlementMessage(eventId), 1);
		if(response != null)
			log.info("RESPONSE:\n" + new CAMessage().readResponseString(response));

	}
	public void testAuthFHMS(byte version) throws Exception {
		//testAuth(version, version == 3 ? 'C' : 'S', "4300123412341017=12100001");
		testAuth(version, version == 3 ? 'C' : 'S', "4300123412341017=12100001");
	}
	public void testAuth(byte version, char type, String card) throws Exception {
		Date startDate = new Date();
		String message;
		switch(version) {
			case 3:
				message = constructAuthV3Message((byte)msg++, startDate, type, 6, card, "");
				break;
			case 4:
				message = constructAuthV4Message(startDate, type, 11, card, null);
				break;
			default:
				throw new Exception("Version " + version + " Auth is not supported");
		}
		ReRixMessage response = sendMessage(message, 1);
		if(response != null)
			log.info("RESPONSE:\n" + new C2Message().readResponseString(response));
	}
	public void testCashSale(long transactionId) throws Exception {
		MessageData_C4 dataC4 = new MessageData_C4();
		dataC4.setBatchId(1);
		dataC4.setLineItemFormat((byte)0);
		dataC4.setReceiptResult(ReceiptResult.UNAVAILABLE);
		dataC4.setSaleAmount(new BigDecimal(275));
		dataC4.setSaleResult(SaleResult.SUCCESS);
		dataC4.setSaleSessionStart(Calendar.getInstance());
		dataC4.setSaleType(SaleType.ACTUAL);
		dataC4.setTransactionId(transactionId);
		Format0LineItem item = (Format0LineItem)dataC4.addLineItem();
		item.setComponentNumber(1);
		item.setDescription("BK");
		item.setDuration(2);
		item.setPrice(new BigDecimal(275));
		item.setQuantity(1);
		item.setSaleResult(SaleResult.SUCCESS);
		item.setItem(200);
		sendMessage(dataC4);
	}
	public void testCancelSale(long transactionId) throws Exception {
		testCancelSale(transactionId, SaleResult.CANCELLED_BY_USER);
	}

	public void testCancelSale(long transactionId, SaleResult saleResult) throws Exception {
		MessageData_C4 dataC4 = new MessageData_C4();
		dataC4.setBatchId(1);
		dataC4.setLineItemFormat((byte)0);
		dataC4.setReceiptResult(ReceiptResult.UNAVAILABLE);
		dataC4.setSaleAmount(BigDecimal.ZERO);
		dataC4.setSaleResult(saleResult);
		dataC4.setSaleSessionStart(Calendar.getInstance());
		dataC4.setSaleType(SaleType.ACTUAL);
		dataC4.setTransactionId(transactionId);

		sendMessage(dataC4);
	}
	
	public void testModemProperties() throws Exception {
		MessageData_C7 dataC7 = new MessageData_C7();
		dataC7.setContent("300=1\n301=1\n302=1\n310=1\n311=1\n312=1\n313=1\n314=1\n315=1".getBytes(ProcessingConstants.US_ASCII_CHARSET));
		Calendar cal = Calendar.getInstance();
		cal.setTimeZone(TimeZone.getTimeZone("MST"));
		dataC7.setCreationTime(cal);
		dataC7.setEventId(masterId++);
		dataC7.setFileName("MODEM-INFO");
		dataC7.setFileType(FileType.PROPERTY_LIST);
		sendMessage(dataC7);
	}
	
	public void testModemProperty1() throws Exception {
		MessageData_C7 dataC7 = new MessageData_C7();
		dataC7.setContent("300=15".getBytes(ProcessingConstants.US_ASCII_CHARSET));
		dataC7.setCreationTime(Calendar.getInstance());
		dataC7.setEventId(masterId++);
		dataC7.setFileName("MODEM-INFO-PARTIAL");
		dataC7.setFileType(FileType.PROPERTY_LIST);
		sendMessage(dataC7);
	}
	public MessageData testAuthPermission(EntryType entryType, String card) throws Exception {
		MessageData_AA data = new MessageData_AA();
		data.setAccountData(card);
		data.setEntryType(entryType);
		data.setTransactionId(masterId++);
		data.setValidationData(null);
		return sendMessage(data);
	}
	public MessageData testAuthV4(char type, String card, long amount) throws Exception {
		return testAuthV4(EntryType.getByValue(type), card, BigDecimal.valueOf(amount));
	}
	public MessageData testAuthV4(EntryType type, String card, BigDecimal amount) throws Exception {
		return testAuthV4(type, card, amount, masterId++);
	}
	public MessageData testAuthV4(EntryType type, String card, BigDecimal amount, long transactionId) throws Exception {
		MessageData_C2 dataC2 = new MessageData_C2();
		dataC2.setAmount(amount);
		dataC2.setCardReaderType(CardReaderType.GENERIC);
		dataC2.setEntryType(type);
		dataC2.setTransactionId(transactionId);
		((GenericTracksCardReader) dataC2.getCardReader()).setAccountData2(card);
		return sendMessage(dataC2);
	}
	public MessageData testAuthV4(char type, String card) throws Exception {
		return testAuthV4(type, card, 738);
	}
	public static void testAuthFHMSDev() throws Exception {
		String host = "usaapd1";
		int port = 444;
		String evNumber = "EV035194";
		byte[] encryptKey = "3022848002877358".getBytes();
		String card = "4300123412341017=12100001";
		Calendar cal = Calendar.getInstance();
		cal.set(2008, 9, 2, 10, 44, 10);
		Date startDate = cal.getTime();

		ClientEmulator ce = new ClientEmulator(host, port, evNumber, encryptKey);
		ce.startCommunication(3);
		ce.sendMessage(constructAuthV3Message((byte)0, startDate, 'C', 10, card, ""), 1);
		ce.stopCommunication();
	}
	public void testAuthBlackboard() throws Exception {
		testAuth((byte)4, 'S', "6329082000000000053"); //375019001001900 //6329082000000000055
		/*
		String host = "127.0.0.1";
		int port = 444;
		String evNumber = "EV035194";
		String encryptKey = "3022848002877358";
		String card = "6329082000000000053";//375019001001900 //6329082000000000055

		Calendar cal = Calendar.getInstance();
		cal.set(2008, 9, 2, 10, 38, 21);
		Date startDate = cal.getTime();

		ClientEmulator ce = new ClientEmulator(host, port, evNumber, encryptKey);
		ce.startCommunication(3);
		ce.sendMessage(constructAuthV3Message(startDate, 'S', 1, card, ""), 1);
		ce.stopCommunication();
		*/
	}
	/* For TESTING Aramark:
	 * URL:  http://rts.aramarkcampusit.com/relayTransactionservice/RelayService.asmx

POS Processor (Account name):  scanplus_encrypt_usatech

Encrypt Keys:    Private:   g9f9tr4j34t66lkjgovjo8rs

                          Public:     vgv8gfd9

Card #    Patron            Status      Account Balance

222222    Pink Panther     A               $50.00

333333    Builder Bob       A               $50.00

444444    Speed Racer      L               $50.00

555555    Thomas Train    A                 $0.00

	 */
	public void testAuthAramark() throws Exception {
		testAuth((byte)4, 'S', "444444"); //"222222");
	}

	public static void testAuthPaymentech() throws Exception {
		String host = "127.0.0.1"; //"usaapd1";
		int port = 444;
		String evNumber = "EV100052";
		byte[] encryptKey = "8382709761112244".getBytes();
		String card = "4300123412341017=12100001";
		Date startDate = new Date();

		ClientEmulator ce = new ClientEmulator(host, port, evNumber, encryptKey);
		ce.startCommunication(3);
		ce.sendMessage(constructAuthV3Message((byte)0, startDate, 'R', 1000, card, ""), 1);
		ce.stopCommunication();
	}

	public static void testAuthAquaFill() throws Exception {
		String host = "127.0.0.1";
		int port = 444;
		String evNumber = "EV035194";
		byte[] encryptKey = "3022848002877358".getBytes();
		//String card = "010992389"; //success
		String card = "310992389"; //partial
		Calendar cal = Calendar.getInstance();
		cal.set(2008, 9, 2, 10, 38, 39);
		Date startDate = cal.getTime();

		ClientEmulator ce = new ClientEmulator(host, port, evNumber, encryptKey);
		ce.startCommunication(3);
		ce.sendMessage(constructAuthV3Message((byte)0, startDate, 'S', 900, card, ""), 1);
		ce.stopCommunication();
	}
	public void testAuthInternal(byte version) throws Exception {
		//^6396211([0-9]{1})([0-9]{9})([0-9]{1})=([0-9]{4})([0-9]{5})(\??)$
		testAuth(version, 'S', "639621130020001130=010812345");
	}
	public static void testAuthInternalDev() throws Exception {
		String host = "usaapd1";
		int port = 444;
		String evNumber = "EV035194";
		byte[] encryptKey = "3022848002877358".getBytes();
		//String card = "010992389"; //success
		//^6396211([0-9]{1})([0-9]{9})([0-9]{1})=([0-9]{4})([0-9]{5})(\??)$
		String card = "639621130020001130=010812345";
		Calendar cal = Calendar.getInstance();
		cal.set(2008, 9, 2, 10, 44, 8);
		Date startDate = cal.getTime();

		ClientEmulator ce = new ClientEmulator(host, port, evNumber, encryptKey);
		ce.startCommunication(3);
		ce.sendMessage(constructAuthV3Message((byte)0, startDate, 'S', 900, card, ""), 1);
		ce.stopCommunication();
	}

	protected void sendTranAndFillMessages(byte protocolId, String evNumber, String serialNumber, String encryptKey, String card, int fills, Date startDate) throws Exception {
		float minFillDays = 1.0f;
		float maxFillDays = 4.0f;
		int minTransPerFill = 3;
		int maxTransPerFill = 10;
		/*
select --a.*, HC.HOST_COUNTER_PARAMETER, hc.HOST_COUNTER_VALUE,
'int ' || CASE WHEN HC.HOST_COUNTER_PARAMETER like 'CURRENCY_%' THEN 'cash'
WHEN HC.HOST_COUNTER_PARAMETER like 'CASHLESS_%' THEN 'credit'
WHEN HC.HOST_COUNTER_PARAMETER like 'PASSCARD_%' THEN 'other'
END || CASE WHEN HC.HOST_COUNTER_PARAMETER like '%_TRANSACTION' THEN 'Count'
WHEN HC.HOST_COUNTER_PARAMETER like '%_MONEY%' THEN 'Total'
END || ' = ' || hc.HOST_COUNTER_VALUE || ';'
FROM (select * from (
select e.event_id, DECODE(ed_v.EVENT_DETAIL_VALUE, '1', CE.EVENT_ID, '2', E.EVENT_ID) counter_event_id,
d.device_name, d.device_serial_cd, ed_v.EVENT_DETAIL_VALUE VERSION_VALUE,
NVL(ced_ts.EVENT_DETAIL_VALUE_TS, ed_ts.EVENT_DETAIL_VALUE_TS) COUNTER_EVENT_TS, pa.action_id
from device.device d
join device.host h on d.device_id = h.device_id
join device.event e on h.host_id = e.host_id
 JOIN DEVICE.EVENT_DETAIL ed_v ON E.EVENT_ID = ed_v.EVENT_ID AND ed_v.EVENT_DETAIL_TYPE_ID = 1 -- Event Type Version
JOIN DEVICE.EVENT_DETAIL ed_ts ON E.EVENT_ID = ed_ts.EVENT_ID AND ed_ts.EVENT_DETAIL_TYPE_ID = 2 -- Host Event Timestamp
left outer join (DEVICE.EVENT_NL_DEVICE_SESSION EES
			  JOIN DEVICE.EVENT_NL_DEVICE_SESSION CES ON EES.SESSION_ID = CES.SESSION_ID AND EES.EVENT_ID != CES.EVENT_ID
			  JOIN DEVICE.EVENT CE ON CES.EVENT_ID = CE.EVENT_ID
			   AND CE.EVENT_TYPE_ID = 1
              left outer JOIN DEVICE.EVENT_DETAIL ced_ts ON CE.EVENT_ID = ced_ts.EVENT_ID AND ced_ts.EVENT_DETAIL_TYPE_ID = 2) --Host Event Timestamp
on EES.EVENT_ID = E.EVENT_ID
 LEFT OUTER JOIN (PSS.TRAN T
		  JOIN PSS.CONSUMER_ACCT_PERMISSION CAP ON T.CONSUMER_ACCT_ID = CAP.CONSUMER_ACCT_ID
		  JOIN PSS.PERMISSION_ACTION PA ON CAP.PERMISSION_ACTION_ID = PA.PERMISSION_ACTION_ID
		  ) ON E.EVENT_GLOBAL_TRANS_CD = T.TRAN_GLOBAL_TRANS_CD
 WHERE d.device_name = 'EV035194'
 ORDER BY NVL(ced_ts.EVENT_DETAIL_VALUE_TS, ed_ts.EVENT_DETAIL_VALUE_TS) DESC)
 where rownum = 1) a
left outer JOIN (DEVICE.HOST_COUNTER_EVENT HCE
JOIN DEVICE.HOST_COUNTER HC ON HC.HOST_COUNTER_ID = HCE.HOST_COUNTER_ID AND (HC.HOST_COUNTER_PARAMETER LIKE '%_MONEY' OR HC.HOST_COUNTER_PARAMETER LIKE '%_TRANSACTION'))
 ON a.COUNTER_EVENT_ID = HCE.EVENT_ID;
 */
		int cashCount = 0;
		int cashTotal = 0;
		int creditCount = 0;
		int creditTotal = 0;
		int otherCount = 0;
		int otherTotal = 0;

		int byteCount = 0;

		int rollover = 1000000;

		int minAddMS = (int)(minFillDays * 24 * 60 * 60 * 1000 / (maxTransPerFill + 1));
		int maxAddMS = (int)(maxFillDays * 24 * 60 * 60 * 1000 / (minTransPerFill + 1));
		for(int i = 0; i < fills; i++) {
			startCommunication(protocolId);
			String message;
			/*cashCount = 0;
			cashTotal = 0;
			creditCount = 0;
			creditTotal = 0;
			otherCount = 0;
			otherTotal = 0;
			byteCount = 0;
			*/
			Date targetDate = new Date(startDate.getTime() + getRandom((int)(minFillDays * 24 * 60 * 60 * 1000), (int)(maxFillDays * 24 * 60 * 60 * 1000)));
			while(startDate.before(targetDate)) {
				startDate.setTime(startDate.getTime() + getRandom(minAddMS, maxAddMS));
				double d = Math.random();
				int cents = 100 + (25 * (int)(10 * Math.random()));
				if(d < 0.35) { //credit
					int[] items = new int[getRandom(1, 4)];
					for(int k = 0; k < items.length; k++) {
						items[k] = (int)(100 * Math.random());
					}
					cents = cents * items.length;
					message = constructLocalBatchMessage(startDate, 'C', cents, card, items);
					creditCount = (creditCount + items.length) % rollover;
					creditTotal = (creditTotal + cents) % rollover;
				} else if(d < 0.95) {
					int item = (int)(100 * Math.random());
					message = constructCashMessage(startDate, cents, item);
					cashCount = (cashCount + 1) % rollover;
					cashTotal = (cashTotal + cents) % rollover;
				} else {
					int[] items = new int[getRandom(1, 4)];
					for(int k = 0; k < items.length; k++) {
						items[k] = (int)(100 * Math.random());
					}
					cents = cents * items.length;
					message = constructLocalBatchMessage(startDate, 'S', cents, card, items);
					otherCount = (otherCount + items.length) % rollover;
					otherTotal = (otherTotal + cents) % rollover;
				}
				sendMessage(message, 1);
			}
			startDate.setTime(startDate.getTime() + getRandom(minAddMS, maxAddMS));
			if(Math.random() > 0.70) {
				message = constructFillV1Message(startDate, serialNumber);
				sendMessage(message, 1);
				message = constructCountersMessage(cashCount, cashTotal, creditCount, creditTotal, otherCount, otherTotal, byteCount);
				sendMessage(message, 1);
			} else {
				message = constructFillV2Message(startDate, cashCount, cashTotal, creditCount, creditTotal, otherCount, otherTotal, byteCount, card);
				sendMessage(message, 1);
			}
			stopCommunication();
		}
	}

	public static void sendFills(int fills, int protocolVersion) throws Exception {
		String host = "10.0.0.63";
		int port = 443;
		//String evNumber = "EV033586"; String serialNumber = "G5059952";	String encryptKey = "1160871936175643";
		//String evNumber = "EV033616"; String serialNumber = "M1004454";	String encryptKey = "1914699776573767";
		//String evNumber = "EV035194"; String serialNumber = "G5060281";	String encryptKey = "3022848002877358";
		String evNumber = "EV033712"; String serialNumber = "G5060002";	String encryptKey = "6267207686941245";

		String card = "4300123412341017=12100001";
		Date startDate = new Date(2008-1900, 0, 1);

		//EV033586,
		/*
		String host = "10.0.0.63";
		int port = 443;
		String evNumber = "EV035179";
		String serialNumber = "G5060278";
		String encryptKey = "1095270400165625";
		String card = "4300123412341017=12100001";
		/* /
		String host = "eccnet01.usatech.com"; //, 192.168.4.61";
		int port = 443;
		String evNumber = "EV083445";
		String serialNumber = "G5087253";
		String encryptKey = "1505132544113750";
		String card = "4300123412341017=12100001";
		*/
		ClientEmulator sft = new ClientEmulator(host, port, evNumber, encryptKey.getBytes());
		//sft.useTestClient = true;
		sft.sendTranAndFillMessages((byte)protocolVersion, evNumber, serialNumber, encryptKey, card, fills, startDate);
	}
	public static String constructLocalBatchMessage(Date startDate, char cardType, int cents, String card, int[] items) {
		StringBuilder sb = new StringBuilder();
		sb.append("A3");
		String time = Long.toHexString(startDate.getTime() / 1000);
		padLeft(sb, time, 8);
		padLeft(sb, time, 8);
		appendHex(sb, cardType, 2);
		appendHex(sb, cents, 8);
		appendHex(sb, card.length(), 2);
		sb.append(toHex(card.getBytes()));
		sb.append("4E");
		appendHex(sb, (items.length >> 2) + 1 /* byte length of items*/, 2);
		for(int i = 0; i < items.length; i++) {
			padLeft(sb, String.valueOf(items[i]), 2);
			appendHex(sb, cents/items.length /*unit price*/, 6);
		}
		return sb.toString();
	}

	public String constructInitV4Message_old(byte reasonCode, long protocolComplianceRevision, int propertyListVersion, byte deviceType, String deviceFirmwareVersion, String deviceSerialNum, String deviceInfo, String terminalInfo) {
		StringBuilder sb = new StringBuilder();
		sb.append("C0");
		String msgId = Long.toHexString(messageId.getAndIncrement());
		padLeft(sb, msgId, 8);
		appendHex(sb, reasonCode, 2);
		appendHex(sb, protocolComplianceRevision, 8);
		appendHex(sb, propertyListVersion, 4);
		appendHex(sb, deviceType, 2);

		appendHex(sb, deviceFirmwareVersion.length(), 2);
		sb.append(toHex(deviceFirmwareVersion.getBytes(ProcessingConstants.US_ASCII_CHARSET)));

		appendHex(sb, deviceSerialNum.length(), 2);
		sb.append(toHex(deviceSerialNum.getBytes(ProcessingConstants.US_ASCII_CHARSET)));

		appendHex(sb, deviceFirmwareVersion.length(), 2);
		sb.append(toHex(deviceFirmwareVersion.getBytes(ProcessingConstants.US_ASCII_CHARSET)));

		appendHex(sb, deviceInfo.length(), 2);
		sb.append(toHex(deviceInfo.getBytes(ProcessingConstants.US_ASCII_CHARSET)));

		appendHex(sb, terminalInfo.length(), 2);
		sb.append(toHex(terminalInfo.getBytes(ProcessingConstants.US_ASCII_CHARSET)));

		return sb.toString();
	}
	public MessageData constructInitV32Message(DeviceType deviceType, String deviceSerialNum, String deviceInfo, String terminalInfo) {
		MessageData_AD dataAD = new MessageData_AD();		
		dataAD.setDeviceInfo(deviceInfo);
		dataAD.setDeviceSerialNum(deviceSerialNum);
		dataAD.setDeviceType(deviceType);
		dataAD.setTerminalInfo(terminalInfo);
		dataAD.setMessageId(messageId.getAndIncrement());
		return dataAD;
	}
	public MessageData constructInitV4Message(InitializationReason reasonCode, long protocolComplianceRevision, int propertyListVersion, DeviceType deviceType, String deviceFirmwareVersion, String deviceSerialNum, String deviceInfo, String terminalInfo) {
		MessageData_C0 dataC0 = new MessageData_C0();
		dataC0.setDeviceFirmwareVersion(deviceFirmwareVersion);
		dataC0.setDeviceInfo(deviceInfo);
		dataC0.setDeviceSerialNum(deviceSerialNum);
		dataC0.setDeviceType(deviceType);
		dataC0.setPropertyListVersion(propertyListVersion);
		dataC0.setProtocolComplianceRevision(protocolComplianceRevision);
		dataC0.setReasonCode(reasonCode);
		dataC0.setTerminalInfo(terminalInfo);
		dataC0.setMessageId(messageId.getAndIncrement());
		return dataC0;
	}
	public static String constructAuthV3Message(byte messageNum, Date startDate, char cardType, int cents, String card, String pin) {
		StringBuilder sb = new StringBuilder();
		appendHex(sb, messageNum, 2);
		sb.append("A0");
		long masterId = startDate.getTime() / 1000;
		if(masterId >= masterId)
			masterId = masterId + 1;
		String time = Long.toHexString(masterId);
		padLeft(sb, time, 8);
		appendHex(sb, cardType, 2);
		appendHex(sb, cents, 8);
		appendHex(sb, pin.length(), 2);
		sb.append(toHex(pin.getBytes()));
		sb.append(toHex(card.getBytes()));
		return sb.toString();
	}
	public void testInitV32(String serialNumber) throws Exception {
		MessageData_8F data8F = (MessageData_8F)sendMessage(constructInitV32Message(DeviceType.GX,
				serialNumber,
				"Client Emulator 1.1 Device Info",
				"Client Emulator 1.1 Terminal Info"));
		if(data8F != null) {
			String deviceName = new String(data8F.getDeviceSerialNum(), data8F.getCharset());
			byte[] encryptionKey = data8F.getEncryptionKey();
			String encryptionKeyHex = StringUtils.toHex(encryptionKey);
			log.info("Got DeviceName: " + deviceName + "; Encryption Key: " + encryptionKeyHex);
			if(deviceName != null && deviceName.length() > 0 && encryptionKey != null && encryptionKey.length > 0) {
				this.evNumber = deviceName;
				this.encryptionKey = encryptionKey;
			}
		}
	}
	public void testAuthV3(char cardType, int cents, String card, String pin) throws Exception {
		MessageData_A0 data = new MessageData_A0();
		data.setAccountData(card);
		data.setAmount((long)cents);
		data.setCardType(CardType.getByValue(cardType));
		data.setPin(pin);
		data.setTransactionId(masterId++);
		sendMessage(data);
	}
	public void testAuthV32(long transactionId, int messageVersion, char cardType, long amount, String validationData, int cardReaderType, String accountData1, String accountData2, String accountData3, int additionalDataType, String additionalData) throws Exception {
		MessageData_AE data = new MessageData_AE();
		data.setMessageVersion(messageVersion);
		data.setTransactionId(transactionId);
		data.setCardType(CardType.getByValue(cardType));
		data.setAmount(amount);
		data.setValidationData(validationData);
		data.setCardReaderType(CardReaderType.getByValue((byte)cardReaderType));
		MessageData_AE.GenericCardReader cardReader = (MessageData_AE.GenericCardReader) data.getCardReader();
		cardReader.setAccountData1(accountData1);
		cardReader.setAccountData2(accountData2);
		cardReader.setAccountData3(accountData3);
		data.setAdditionalDataType(AEAdditionalDataType.getByValue((byte)additionalDataType));
		data.setAdditionalData(additionalData);
		sendMessage(data);
	}
	public void testBatchV2(long transactionId, int amount, TranDeviceResultType transactionResult) throws Exception {
		MessageData_2A data = new MessageData_2A();
		data.setTransactionId(transactionId);
		data.setSaleAmount(amount);
		data.setTransactionResult(transactionResult);
		data.setVendByteLength((byte)0);
		
		MessageData_2A.NetBatch0LineItem lineItem1 = (MessageData_2A.NetBatch0LineItem) data.addActualLineItem();
		lineItem1.setPositionNumber(0);
		lineItem1.setReportedPrice(3 * amount / 4);
		
		MessageData_2A.NetBatch0LineItem lineItem2 = (MessageData_2A.NetBatch0LineItem) data.addActualLineItem();
		lineItem2.setPositionNumber(1);
		lineItem2.setReportedPrice(amount - lineItem1.getReportedPrice());

		sendMessage(data);
	}
	public String constructAuthV4Message(Date startDate, char entryType, int cents, String card, String pin) {
		StringBuilder sb = new StringBuilder();
		sb.append("00C2");
		String msgId = Long.toHexString(messageId.getAndIncrement());
		padLeft(sb, msgId, 8);
		padLeft(sb, Long.toHexString(masterId++), 8);
		appendHex(sb, entryType, 2);
		String amt = String.valueOf(cents);
		appendHex(sb, amt.length(), 2);
		sb.append(toHex(amt.getBytes()));
		if(pin != null && pin.length() > 0) {
			appendHex(sb, pin.length() + 4, 2); // validation data
			sb.append(toHex("pin=".getBytes()));
			sb.append(toHex(pin.getBytes()));
		} else
			appendHex(sb, 0, 2); // validation data
		appendHex(sb, 0, 2); // card reader type
		appendHex(sb, 0, 2); // track 1
		appendHex(sb, card.length(), 2); // track 2
		sb.append(toHex(card.getBytes()));
		appendHex(sb, 0, 2); // track 3
		return sb.toString();
	}
	public static String constructCashMessage(Date startDate, int cents, int item) {
		StringBuilder sb = new StringBuilder();
		sb.append("96");
		String time = Long.toHexString(startDate.getTime() / 1000);
		padLeft(sb, time, 8);
		padLeft(sb, time, 8);
		appendHex(sb, cents, 6);
		appendHex(sb, item, 2);
		return sb.toString();
	}
	protected static int getRandom(int min, int max) {
		return min + (int)(Math.random() * (max-min));
	}

	public static String constructFillV2Message(Date startDate, int cashCount, int cashTotal,
			int creditCount, int creditTotal, int otherCount, int otherTotal, int byteCount,
			String card) {
		StringBuilder sb = new StringBuilder();
		sb.append("A8");
		padCounter(sb, cashCount);
		padCounter(sb, cashTotal);
		padCounter(sb, creditCount);
		padCounter(sb, creditTotal);
		padCounter(sb, otherCount);
		padCounter(sb, otherTotal);
		padCounter(sb, byteCount);
		padCounter(sb, 1/*attempted sessions*/);
		String time = Long.toHexString(startDate.getTime() / 1000);
		padLeft(sb, time, 8);
		padLeft(sb, time, 8);
		sb.append("46");
		appendHex(sb, card.length(), 2);
		sb.append(toHex(card.getBytes()));
		return  sb.toString();
	}

	public static String constructUpdateStatusRequestMessage(long messageId, short bitmap) {
		StringBuilder sb = new StringBuilder();
		sb.append("CA");
		padLeft(sb, Long.toHexString(messageId), 8);
		appendHex(sb, 0, 2);
		appendHex(sb, bitmap, 4);
		return sb.toString();
	}
	protected static SimpleDateFormat bcdDateFormat = new SimpleDateFormat("HHmmssMMddyyyy");
	public static String constructFillV1Message(Date startDate, String serialNumber) {
		StringBuilder sb = new StringBuilder();
		sb.append("93");
		String time = Long.toHexString(startDate.getTime() / 1000);
		padLeft(sb, time, 8);
		sb.append(bcdDateFormat.format(startDate));
		sb.append("49"); //"I"
		appendHex(sb, 0, 6);
		appendHex(sb, 0, 4);
		appendHex(sb, 14 + serialNumber.length(), 2);
		sb.append(toHex("Err.DEXFill-->".getBytes()));
		sb.append(toHex(serialNumber.getBytes()));
		sb.append("2000");
		return sb.toString();
	}
	public static String constructCountersMessage(int cashCount, int cashTotal,
			int creditCount, int creditTotal, int otherCount, int otherTotal, int byteCount) {
		StringBuilder sb = new StringBuilder();
		sb.append("86");
		padCounter(sb, cashCount);
		padCounter(sb, cashTotal);
		padCounter(sb, creditCount);
		padCounter(sb, creditTotal);
		padCounter(sb, otherCount);
		padCounter(sb, otherTotal);
		padCounter(sb, byteCount);
		padCounter(sb, 1/*attempted sessions*/);
		sb.append("00000000");
		return sb.toString();
	}
	protected static final char[] HEX_DIGITS = {
    	'0' , '1' , '2' , '3' , '4' , '5' ,
    	'6' , '7' , '8' , '9' , 'A' , 'B' ,
    	'C' , 'D' , 'E' , 'F'};

	public static StringBuilder appendHex(StringBuilder sb, int num, int length) {
		return padLeft(sb, Integer.toHexString(num), length);
	}
	public static StringBuilder appendHex(StringBuilder sb, long num, int length) {
		return padLeft(sb, Long.toHexString(num), length);
	}
	public static String toHex(byte[] bytes) {
    	if(bytes == null) return null;
    	char[] result = new char[bytes.length*2];
    	for(int i = 0; i < bytes.length; i++) {
    		result[i*2] = HEX_DIGITS[(bytes[i] >> 4) & 0x0f];
    		result[i*2+1] = HEX_DIGITS[bytes[i] & 0x0f];
    	}
    	return new String(result);
    }
	protected static StringBuilder padCounter(StringBuilder sb, int counter) {
		return padLeft(sb, String.valueOf(counter), 8);
	}
	protected static StringBuilder padLeft(StringBuilder sb, String s, int length) {
		if(s.length() > length)sb.append(s.substring(s.length() - length));
		else {
			if(s.length() < length) sb.append(counterPadding, 0, length - s.length());
			sb.append(s);
		}
		return sb;
	}

	public MessageData sendMessages(MessageData[] datas) throws Exception {
		for(MessageData data : datas)
			transmitMessage(data);
		return receiveReplyAdvanced();
	}

	public MessageData sendMessage(MessageData data) throws Exception {
		transmitMessage(data);
		return receiveReplyAdvanced();
	}
	
	public void transmitMessage(MessageData data) throws Exception {
		String evNumber;
		if (protocolVersion == 2)
			evNumber = new String(ByteArrayUtils.fromHex(this.evNumber));
        else
        	evNumber = this.evNumber;
		data.setDirection(MessageDirection.CLIENT_TO_SERVER);
		data.setMessageId(messageId.getAndIncrement());
		data.setMessageNumber((byte)msg++);
		ByteBuffer buffer = ByteBuffer.allocate(1024);
		data.writeData(buffer, false);
		buffer.flip();
		byte[] bytes = new byte[buffer.remaining()];
		buffer.get(bytes);
		String hex = StringUtils.toHex(bytes);
		log.info(format("{0} sending {1} on {2} to {3}", evNumber, data, socket.getLocalSocketAddress().toString(), socket.getRemoteSocketAddress().toString()));
		log.info("<- " + hex);
		transmit((byte)protocolVersion, evNumber, bytes, encryptionKey, crypts[protocolVersion], output, protocolVersion > 0);

		output.flush();
	}
	
	public MessageData receiveReply() {
		byte[] reply = null;
		switch(protocolVersion) {
			case 2:
			case 4:
				try {
					//reply = TransmissionProtocol.getProtocol(protocolVersion).receiveFromServer(input, evNumber, encryptionKey).getData();
				} catch(Exception e) {
					log.debug("Input Bytes: " + inputLogging.drainLog());
					log.warn("While reading response for device " + evNumber, e);
					return null;
				}
				break;
			default:
				try {
					/*int version = */input.read();
				} catch(Exception e) {
					log.warn("While waiting for response from server for device " + evNumber, e);
					return null;
				}
				try {
					reply = receive(protocolVersion == 6 || protocolVersion == 8 || protocolVersion == 0 ? evNumber : null, encryptionKey, crypts[protocolVersion], input, protocolVersion > 0);
				} catch(Exception e) {
					log.debug("Input Bytes: " + inputLogging.drainLog());
					log.warn("While reading response for device " + evNumber, e);
					return null;
				}
		}
		log.debug("Input Bytes: " + inputLogging.drainLog());
		log.info("-> " + StringUtils.toHex(reply));
		try {
			MessageData replyData = MessageDataFactory.readMessage(ByteBuffer.wrap(reply), MessageDirection.SERVER_TO_CLIENT);
			log.info(format("{0} received: {1} on {2} from {3}", evNumber, replyData, socket.getLocalSocketAddress().toString(), socket.getRemoteSocketAddress().toString()));
			switch(replyData.getMessageType()) {
				case GENERIC_RESPONSE_4_1:
					MessageData_CB replyCB = (MessageData_CB) replyData;
					if(replyCB.getServerActionCode() == GenericResponseServerActionCode.PROCESS_PROP_LIST) {
						Map<Integer, String> propertyValues = ((PropertyValueListAction) replyCB.getServerActionCodeData()).getPropertyValues();
						String hex = propertyValues.get(DeviceProperty.ENCRYPTION_KEY.getValue());
						if(hex != null && hex.length() > 0)
							this.encryptionKey = ByteArrayUtils.fromHex(hex);	
					}
					break;
				case SET_ID_NUMBER_AND_KEY:
					MessageData_8F reply8F = (MessageData_8F) replyData;
					this.encryptionKey = reply8F.getEncryptionKey();
					break;
			}
			return replyData;
		} catch(Exception e) {
			log.warn("While reading response for device " + evNumber, e);
			return null;
		}
	}

	public MessageData receiveReplyAdvanced() throws Exception {
		/*int version = */input.read();
		
		byte[] reply = receive(protocolVersion == 6 || protocolVersion == 8 || protocolVersion == 0 ? evNumber : null, encryptionKey, crypts[protocolVersion], input, protocolVersion > 0);
		
		log.debug("Input Bytes: " + inputLogging.drainLog());
		log.info("-> " + StringUtils.toHex(reply));
		
		MessageData replyData = MessageDataFactory.readMessage(ByteBuffer.wrap(reply), MessageDirection.SERVER_TO_CLIENT);
		log.info(format("{0} received: {1} on {2} from {3}", evNumber, replyData, socket.getLocalSocketAddress().toString(), socket.getRemoteSocketAddress().toString()));
		
		switch (replyData.getMessageType()) {
			case GENERIC_RESPONSE_4_1:
				MessageData_CB replyCB = (MessageData_CB)replyData;
				if (replyCB.getServerActionCode() == GenericResponseServerActionCode.PROCESS_PROP_LIST) {
					Map<Integer, String> propertyValues = ((PropertyValueListAction)replyCB.getServerActionCodeData()).getPropertyValues();
					String hex = propertyValues.get(DeviceProperty.ENCRYPTION_KEY.getValue());
					if (hex != null && hex.length() > 0)
						this.encryptionKey = ByteArrayUtils.fromHex(hex);
				}
				break;
			case SET_ID_NUMBER_AND_KEY:
				MessageData_8F reply8F = (MessageData_8F)replyData;
				this.encryptionKey = reply8F.getEncryptionKey();
				break;
		}
		return replyData;
	}

	public void sendRawBytes(byte[] data) throws IOException {
		output.write(data);
		output.flush();
	}
	public ReRixMessage sendMessage(String message, int maxReplies) throws Exception {
		return sendMessage(ByteArrayUtils.fromHex(message), maxReplies);
	}
	public ReRixMessage sendMessage(byte[] data, int maxReplies) throws Exception {
		return sendMessage(new ReRixMessage(data, evNumber), maxReplies);
	}
	public ReRixMessage sendMessage(ReRixMessage message, int maxReplies) throws Exception {
		if(protocolVersion == 4) {
		/*	message.setEVNumber(evNumber);
			protocol.transmitToServer(message, output, encryptionKey);
			output.flush();
			return protocol.receiveFromServer(input, evNumber, encryptionKey);*/
			return null;
		} else {
		String evNumber;
		if (protocolVersion == 2)
			evNumber = new String(ByteArrayUtils.fromHex(this.evNumber));
        else
        	evNumber = this.evNumber;

		log.info("<- " + message.getHexData());
		transmit((byte)protocolVersion, evNumber, message.getData(), encryptionKey, crypts[protocolVersion], output, protocolVersion > 0);

		output.flush();

        switch(protocolVersion) {
			case 2:
			case 4:
				break;
			default:
				try {
					/*int version = */input.read();
				} catch(Exception e) {
					log.warn("While waiting for response from server for device " + evNumber, e);
					return null;
				}
		}
        byte[] reply = null;
		for(int i = 0; i < maxReplies; i++) {
			try {
				reply = receive(protocolVersion == 6 || protocolVersion == 8 || protocolVersion == 0 ? evNumber : null, encryptionKey, crypts[protocolVersion], input, protocolVersion > 0);
				if(reply == null)
					break;
				log.debug("Input Bytes: " + inputLogging.drainLog());
				log.info("-> " + StringUtils.toHex(reply));
			} catch(Exception e) {
				log.debug("Input Bytes: " + inputLogging.drainLog());
				log.warn("While reading response for device " + evNumber, e);
				break;
			}
		}
		return new ReRixMessage(reply, evNumber);
		}
	}
	
	public void startCommunication(int protocolVersion) throws ClassNotFoundException, NetworkLayerException, UnknownHostException, IOException, SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		startCommunication(protocolVersion, 15000, 15000, 15000);
	}
	
	public void startCommunication(int protocolVersion, int connectTimeout, int readTimeout, int writeTimeout) throws ClassNotFoundException, NetworkLayerException, UnknownHostException, IOException, SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		this.protocolVersion = protocolVersion;
		if(useTestClient) 
			return;

		Class<?>[] parameterTypes = { Integer.TYPE, Integer.TYPE };
		Object[] constructorArgs = { new Integer(readTimeout), new Integer(writeTimeout) };
		String className = "com.usatech.networklayer.net.TransmissionProtocolV" + protocolVersion;
		try {
			Class<?> protocolClass = Class.forName(className);
			Constructor<?> constructor = protocolClass.getConstructor(parameterTypes);
			protocol = (TransmissionProtocol) constructor.newInstance(constructorArgs);
		} catch(InstantiationException e) {
			throw new NetworkLayerException("Failed to create TransmissionProtocol " + className, e);
		}
		socket = new Socket();
		socket.connect(new InetSocketAddress(host, port), connectTimeout);
		socket.setSoTimeout(readTimeout);
		inputLogging = new LoggingInputStream(socket.getInputStream());
		input = new DataInputStream(inputLogging);
		output = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
		msg = 0;
		log.info("Connected device '" + evNumber + "' to " + host + ":" + port + " from port " + socket.getLocalPort());
	}
	
	public boolean isConnected() {
		return socket != null && !socket.isClosed();
	}

	public int getLocalPort() {
		return socket != null ? socket.getLocalPort() : -1;
	}
	
	protected static final byte[] HANGUP_BYTES = {(byte)'+', (byte)'+', (byte)'+'};
	
	public void sendHangup() throws IOException {
		sendHangup(true);
	}

	public void sendHangup(boolean flush) throws IOException {
		output.write(HANGUP_BYTES);
		if(flush)
			output.flush();
	}
	
	public void stopCommunication() throws IOException {
		if(useTestClient) return;
		input.close();
		output.close();
		socket.close();
		log.info("Disconnected device '" + evNumber + "' from " + host + ":" + port + " from port " + socket.getLocalPort());
	}

	public String getEvNumber() {
		return evNumber;
	}

	public void setEvNumber(String evNumber) {
		this.evNumber = evNumber;
	}

	// tranmission / encryption

	protected int transmit(byte protocol, String deviceName, byte[] data, byte[] encryptionKey, Crypt crypt, DataOutputStream out, boolean sendLength) throws IOException, NetworkLayerException {
        switch(protocol) {
        	case 4:
	        	byte[] encrypted = encryptData(data, encryptionKey, crypt);        
	        	byte[] array = new byte[encrypted.length + deviceName.length()];
	            System.arraycopy(deviceName.getBytes(), 0, array, 0, deviceName.length());
	            System.arraycopy(encrypted, 0, array, 8, encrypted.length);
	
	            byte[] encoded = UUEncode.encode(array);
	            
	            if (log.isDebugEnabled())
	                log.debug("transmitToServer: " + deviceName + ": Encoded: " + StringUtils.toHex(encoded));
	
	            out.write(encoded);
	            out.write(TransmissionProtocolV4.PACKET_DELIMITER);
	            
	        	return encoded.length;
        	case 1:
        		int cnt = 0;
        		StringBuilder header = new StringBuilder();
        		out.write(protocol);
        		StringUtils.appendHex(header, protocol);
        		cnt++;
        		byte[] deviceNameBytes = deviceName.getBytes(ProcessingConstants.US_ASCII_CHARSET);
		        if(sendLength) {
		        	int length = data.length + Math.max(8, deviceNameBytes.length) + 1;
		        	out.write((byte)(length >> 8));
		        	out.write((byte)(length >> 0));
		        	StringUtils.appendHex(header, (short)length);   
		        	cnt += 2;
	        	}
		        for(int i = 0; i < 8 - deviceNameBytes.length; i++) {
		        	out.write(0);// this will pad with 0x00 if length < 8
		        	header.append("00");
		        }
		        out.write(deviceNameBytes);
		        StringUtils.appendHex(header, deviceNameBytes, 0, deviceNameBytes.length);       		
		        cnt += Math.max(8, deviceNameBytes.length);
		        out.write(data);
		        cnt += data.length;
		        int sum = ClientEmulator.calcCheckSumV1(data, ClientEmulator.calcCheckSumV1(deviceNameBytes, (byte)0));
		        out.write(sum);
		        cnt++;
		        if (log.isDebugEnabled())
		            log.debug("transmit " + deviceName + ": Header: " + header.toString());
		        return cnt;
        	default:
				encrypted = encryptData(data, encryptionKey, crypt);
		        array = new byte[encrypted.length + (sendLength ? 11 : 9)];
		
		        array[0] = protocol;
		
		        if(sendLength)
		        	ByteArrayUtils.writeUnsignedShort(array, 1, encrypted.length + 8);
		
		        deviceNameBytes = deviceName.getBytes();
		        System.arraycopy(deviceNameBytes, 0, array, (sendLength ? 3 : 1), Math.min(8, deviceNameBytes.length)); // this will pad with 0x00 if length < 8
		        System.arraycopy(encrypted, 0, array, (sendLength ? 11 : 9), encrypted.length);
		        if (log.isDebugEnabled())
		            log.debug("transmit " + deviceName + ": Header: " + StringUtils.toHex(array, 0, (sendLength ? 11 : 9)));
		
		        out.write(array);
		        return array.length;
        }
    }

	protected byte[] encryptData(byte[] data, byte[] encryptionKey, Crypt crypt) throws NetworkLayerException {
		if (log.isDebugEnabled())
			log.debug("Unencrypted: " + StringUtils.toHex(data));
		byte[] encrypted;
		if(crypt != null) {
			try {
				encrypted = crypt.encrypt(data, encryptionKey);
	        } catch(InvalidKeyException e) {
	                throw new NetworkLayerException("Failed to encrypt message (" + StringUtils.toHex(data) + ") : Invalid Key: " + StringUtils.toHex(encryptionKey), e);
	        } catch(CRCException e) {
	                throw new NetworkLayerException("Failed to encrypt message (" + StringUtils.toHex(data) + ") : CRC Check Failed!", e);
			} catch(CryptException e) {
	                throw new NetworkLayerException("Failed to encrypt message (" + StringUtils.toHex(data) + ") : Decryption Algorithm Failure!", e);
			} catch(Exception e) {
	                throw new NetworkLayerException("Caught unexpected exception encrypting message (" + StringUtils.toHex(data) + ") : " + e, e);
	        }
		} else {
			encrypted = data;
		}

		if (log.isDebugEnabled())
	        log.debug("Encrypted: " + StringUtils.toHex(encrypted) + " with key: " + StringUtils.toHex(encryptionKey));

	    return encrypted;
    }

	protected byte[] receive(String deviceName, byte[] encryptionKey, Crypt crypt, DataInputStream in, boolean readLength) throws IOException, NetworkLayerException {
		int expectedLength;
		if(readLength) {
			byte[] lengthArray = new byte[2];
		    try {
		        in.readFully(lengthArray);
		    } catch (EOFException e) {
		        throw new NetworkLayerException("End of stream encountered while reading length bytes.");
		    }
		    expectedLength = ByteArrayUtils.readUnsignedShort(lengthArray, 0);
		} else {
			expectedLength = 9;
		}
	    if(deviceName != null) {
	    	byte[] dn = new byte[8];
	    	in.readFully(dn);
	    	String readDeviceName=new String(dn);
	    	log.debug("Readback deviceName:"+readDeviceName);
	    	if(!readDeviceName.equals(deviceName))
	    		throw new NetworkLayerException("Device Name does not match!");
	    	expectedLength = expectedLength - 8;
	    }

	    byte[] encrypted = new byte[expectedLength];
	    try {
	        in.readFully(encrypted);
	    } catch (EOFException e) {
	        throw new NetworkLayerException("End of stream encountered while reading message bytes.");
	    }

	    return decryptData(encrypted, encryptionKey, crypt);
	}
	protected byte[] decryptData(byte[] data, byte[] encryptionKey, Crypt crypt) throws NetworkLayerException {
		if (log.isDebugEnabled())
			log.debug("Undecrypted: " + StringUtils.toHex(data));
		byte[] decrypted;
		if(crypt != null) {
			try {
				decrypted = crypt.decrypt(data, encryptionKey);
	        } catch(InvalidKeyException e) {
	                throw new NetworkLayerException("Failed to decrypt message (" + StringUtils.toHex(data) + ") : Invalid Key: " + StringUtils.toHex(encryptionKey), e);
	        } catch(CRCException e) {
	                throw new NetworkLayerException("Failed to decrypt message (" + StringUtils.toHex(data) + ") : CRC Check Failed!", e);
			} catch(CryptException e) {
	                throw new NetworkLayerException("Failed to decrypt message (" + StringUtils.toHex(data) + ") : Decryption Algorithm Failure!", e);
			} catch(Exception e) {
	                throw new NetworkLayerException("Caught unexpected exception decrypting message (" + StringUtils.toHex(data) + ") : " + e, e);
	        }
		} else {
			decrypted = data;
		}

		if (log.isDebugEnabled())
	        log.debug("Decrypted: " + StringUtils.toHex(decrypted));

	    return decrypted;
    }

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public long nextMasterId() {
		return ++masterId;
	}
	public long getMasterId() {
		return masterId;
	}
	public byte[] getEncryptionKey() {
		return encryptionKey;
	}
	public void setEncryptionKey(byte[] encryptionKey) {
		this.encryptionKey = encryptionKey;
	}

	public long getMessageId() {
		return messageId.get();
	}

	public void setMessageId(long messageId) {
		this.messageId.set(messageId);
	}

	public void setMasterId(long masterId) {
		this.masterId = masterId;
	}

	public String getTimeZoneGuid() {
		return timeZoneGuid;
	}

	public void setTimeZoneGuid(String timeZoneGuid) {
		this.timeZoneGuid = timeZoneGuid;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}
	
	public static byte calcCheckSumV1(byte[] data, byte sum) {
		for(int i = 0; i < data.length; i++) {
			sum += data[i];
		}
		return sum;
	}
}
