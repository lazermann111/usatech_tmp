package com.usatech.test;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.util.Calendar;

import simple.io.IOUtils;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
/**
 * Short File transfer C7-->CB response
 * @author yhe
 *
 */
public class C7Message extends V4Message {
	protected Calendar timeOfCreation;
	protected long eventId;
	protected int filesRemaining;
	protected int fileType;
	protected String fileName;
	protected String payLoad;
	public C7Message() {
		super();
		dataType=0xC7;
		timeOfCreation=Calendar.getInstance();
		eventId=System.currentTimeMillis()/1000;
		filesRemaining=1;
		fileType=0;//dex
		fileName="testC7.txt";
		payLoad="Test Client payload";
	}
	public int getFilesRemaining() {
		return filesRemaining;
	}
	public void setFilesRemaining(int filesRemaining) {
		this.filesRemaining = filesRemaining;
	}
	public int getFileType() {
		return fileType;
	}
	public void setFileType(int fileType) {
		this.fileType = fileType;
	}
	@Override
	public void getParams()throws Exception{
		super.getParams();
		File f;
		if(userInputTokens==null){
			f=CommandLineUtil.getFileTransfer();
		}else{
			f=CommandLineUtil.getFileTransferByPath(userInputTokens.nextToken());
		}
		fileName=f.getName();
		BufferedInputStream bi=new BufferedInputStream(new FileInputStream(f));
		payLoad=IOUtils.readFully(bi);
		CommandLineUtil.pw.println("Successfully loaded file fileName:"+fileName);
	}
	@Override
	public ReRixMessage createMessage() {
		int fileNameLen=fileName==null?0:fileName.length();
		int fileLen=payLoad==null?0:payLoad.length();
		ByteBuffer bb=ByteBuffer.allocate(21+fileNameLen+fileLen);
		writeMessageHeader(bb);
		ProcessingUtils.writeTimestamp(bb,timeOfCreation);
		ProcessingUtils.writeLongInt(bb,eventId);
		ProcessingUtils.writeByteInt(bb,filesRemaining);
		ProcessingUtils.writeShortInt(bb,fileType);
		ProcessingUtils.writeShortString(bb,fileName, ProcessingConstants.US_ASCII_CHARSET);
		ProcessingUtils.writeLongString(bb,payLoad, ProcessingConstants.US_ASCII_CHARSET);
		return writeMessageEnd(bb);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception{
		ClientConnectUtil conn=new ClientConnectUtil();
		conn.setProtocol(7);
		conn.startCommunication();
		C7Message sMsg=new C7Message();
		sMsg.getParams();
		ReRixMessage sentMsg=sMsg.createMessage();
		ReRixMessage responseMsg=conn.sendMessage(sentMsg, sMsg.encryptKey);
		sMsg.readResponse(responseMsg);
		conn.stopCommunication();

	}

}
