package com.usatech.test;

import java.nio.ByteBuffer;

import simple.text.StringUtils;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.MessageType;

public abstract class ESudsMessage extends LegacyMessage {
	protected int dataType2;
	public ESudsMessage() {
		super();
		dataType=0x9A;
		evNumber = "EV017714";//use a esuds device, device type 5 in device table
		encryptKey =StringUtils.toHex("2981435511666173");
	}
	
	@Override
	public void writeMessageHeader(ByteBuffer bb){
		ProcessingUtils.writeByteInt(bb, messageNumber);
		ProcessingUtils.writeByteInt(bb, dataType);
		ProcessingUtils.writeByteInt(bb, dataType2);
	}
	
	@Override
	public String getMessageTypeName(){
		byte[] dataTypes=new byte[2];
		dataTypes[0]=(byte)dataType;
		dataTypes[1]=(byte)dataType2;
		return MessageType.getMessageDescriptionSafely(dataTypes);
	}
}
