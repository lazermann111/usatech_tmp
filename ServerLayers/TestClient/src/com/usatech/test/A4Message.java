package com.usatech.test;

import java.nio.ByteBuffer;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;

/**
 * A4 File Transfer start v1.1 -->A5 File Transfer start ack v1.1
 * @author yhe
 *
 */
public class A4Message extends N7CMessage {

	public A4Message() {
		super();
		dataType=0xA4;
		MAX_PAYLOAD=1996;
	}

	@Override
	public ReRixMessage createMessage() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		ProcessingUtils.writeShortInt(bb, numberOfPacketsInFileTransfer);
		ProcessingUtils.writeLongInt(bb, totalNumberOfBytesInFileTransfer);
		ProcessingUtils.writeByteInt(bb, fileTransferGroupNumber);
		ProcessingUtils.writeByteInt(bb, fileType);
		ProcessingUtils.writeString(bb,fileName, ProcessingConstants.US_ASCII_CHARSET);
		return writeMessageEnd(bb);
	}
}
