package com.usatech.test;

import java.io.IOException;
import java.nio.ByteBuffer;

import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
/**
 * Initialization message C0-->CB response.
 * @author yhe
 *
 */
public class C0Message extends V4Message {
	protected int reasonCode;
	protected long protocolComplianceRevision;
	protected int propertyListVersion;
	protected int deviceType;
	protected String deviceFirmwareVersion="TestClient v0.0.3";
	protected String deviceSerialNum="";
	protected String deviceInfo="";
	protected String terminalInfo="";
	public C0Message() {
		super();
		dataType=0xC0;
		reasonCode=0;
		protocolComplianceRevision=4001008;
		deviceType=13;
		propertyListVersion=0;
	}

	@Override
	public ReRixMessage createMessage(){
		ByteBuffer bb=ByteBuffer.allocate(1037);
		writeMessageHeader(bb);
		ProcessingUtils.writeByteInt(bb, reasonCode);
		ProcessingUtils.writeLongInt(bb, protocolComplianceRevision);
		ProcessingUtils.writeShortInt(bb, propertyListVersion);
		ProcessingUtils.writeByteInt(bb, deviceType);
		ProcessingUtils.writeShortString(bb, deviceFirmwareVersion, ProcessingConstants.US_ASCII_CHARSET);
		ProcessingUtils.writeShortString(bb, deviceSerialNum, ProcessingConstants.US_ASCII_CHARSET);
		ProcessingUtils.writeShortString(bb, deviceInfo, ProcessingConstants.US_ASCII_CHARSET);
		ProcessingUtils.writeShortString(bb, terminalInfo, ProcessingConstants.US_ASCII_CHARSET);
		return writeMessageEnd(bb);
	}
	public long getProtocolComplianceRevision() {
		return protocolComplianceRevision;
	}
	public void setProtocolComplianceRevision(long protocolComplianceRevision) {
		this.protocolComplianceRevision = protocolComplianceRevision;
	}
	public int getPropertyListVersion() {
		return propertyListVersion;
	}
	public void setPropertyListVersion(int propertyListVersion) {
		this.propertyListVersion = propertyListVersion;
	}
	public int getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(int deviceType) {
		this.deviceType = deviceType;
	}
	public String getDeviceFirmwareVersion() {
		return deviceFirmwareVersion;
	}
	public void setDeviceFirmwareVersion(String deviceFirmwareVersion) {
		this.deviceFirmwareVersion = deviceFirmwareVersion;
	}
	public String getDeviceSerialNum() {
		return deviceSerialNum;
	}
	public void setDeviceSerialNum(String deviceSerialNum) {
		this.deviceSerialNum = deviceSerialNum;
	}
	public String getDeviceInfo() {
		return deviceInfo;
	}
	public void setDeviceInfo(String deviceInfo) {
		this.deviceInfo = deviceInfo;
	}
	public String getTerminalInfo() {
		return terminalInfo;
	}
	public void setTerminalInfo(String terminalInfo) {
		this.terminalInfo = terminalInfo;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception{
		ClientConnectUtil conn=new ClientConnectUtil();
		conn.setProtocol(7);
		conn.startCommunication();
		C0Message sMsg=new C0Message();
		sMsg.getParams();
		ReRixMessage sentMsg=sMsg.createMessage();
		ReRixMessage responseMsg=conn.sendMessage(sentMsg, sMsg.encryptKey);
		sMsg.readResponse(responseMsg);
		conn.stopCommunication();

	}
	public int getReasonCode() {
		return reasonCode;
	}
	public void setReasonCode(int reasonCode) {
		this.reasonCode = reasonCode;
	}
	/**
	 * @throws IOException
	 * @see com.usatech.test.BaseMessage#read(ByteBuffer)
	 */
	@Override
	protected void read(ByteBuffer data) throws IOException, InvalidByteValueException {
		super.read(data);
		setReasonCode(ProcessingUtils.readByteInt(data));
		setProtocolComplianceRevision(ProcessingUtils.readLongInt(data));
		setPropertyListVersion(ProcessingUtils.readShortInt(data));
		setDeviceType(ProcessingUtils.readByteInt(data));
		setDeviceFirmwareVersion(ProcessingUtils.readShortString(data, ProcessingConstants.US_ASCII_CHARSET));
		setDeviceSerialNum(ProcessingUtils.readShortString(data, ProcessingConstants.US_ASCII_CHARSET));
		setDeviceInfo(ProcessingUtils.readShortString(data, ProcessingConstants.US_ASCII_CHARSET));
		setTerminalInfo(ProcessingUtils.readShortString(data, ProcessingConstants.US_ASCII_CHARSET));
	}
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageTypeName())
			.append(": [messageNumber=").append(getMessageNumber())
			.append("; reasonCode=").append(getReasonCode())
			.append("; protocolComplianceRevision=").append(getProtocolComplianceRevision())
			.append("; propertyListVersion=").append(getPropertyListVersion())
			.append("; deviceType=").append(getDeviceType())
			.append("; deviceFirmwareVersion=").append(getDeviceFirmwareVersion())
			.append("; deviceSerialNum=").append(getDeviceSerialNum())
			.append("; deviceInfo=").append(getDeviceInfo())
			.append("; terminalInfo=").append(getTerminalInfo());
		sb.append("]");

		return sb.toString();
	}
}
