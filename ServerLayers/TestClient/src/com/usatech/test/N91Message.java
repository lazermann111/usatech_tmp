package com.usatech.test;

import java.nio.ByteBuffer;

import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
/**
 * 91 Generic NAK--> no response
 * @author yhe
 *
 */
public class N91Message extends BaseMessage {
	protected int nakType;

	public N91Message() {
		super();
		dataType=0x91;
		nakType=0;
	}

	@Override
	public ReRixMessage createMessage() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		ProcessingUtils.writeByteInt(bb, nakType);
		return writeMessageEnd(bb);
	}

	@Override
	public String readResponseString(ReRixMessage msg) throws InvalidByteValueException {
		return "No Response.\n";
	}

}
