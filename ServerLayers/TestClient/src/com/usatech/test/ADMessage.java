package com.usatech.test;

import java.nio.ByteBuffer;

import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
/**
 * AD initialization v3.1 -->8F
 *  Note: device name and serial number must be in db, otherwise no response.
 * After successful message, the encryption key for the device is changed in db.
 * @author yhe
 *
 */
public class ADMessage extends LegacyMessage {
	protected int deviceType;
	protected String uniqueDeviceSerialNumber;
	protected String deviceInformation;
	protected String terminalInformation;
	
	public ADMessage() {
		super();
		dataType=0xAD;
		deviceType=1;
		uniqueDeviceSerialNumber="G50602810";
		deviceInformation="G5";
		terminalInformation="G5";
	}

	@Override
	public ReRixMessage createMessage() throws Exception {
		deviceInformation+="\n";
		terminalInformation+="\n";
		int serialNumberLength=uniqueDeviceSerialNumber.length();
		int deviceInformationLength=deviceInformation.length();
		int terminalInformationLength=terminalInformation.length();
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		ProcessingUtils.writeByteInt(bb, deviceType);
		ProcessingUtils.writeByteInt(bb, serialNumberLength);
		bb.put(uniqueDeviceSerialNumber.getBytes());
		ProcessingUtils.writeShortInt(bb, deviceInformationLength);
		bb.put(deviceInformation.getBytes());
		ProcessingUtils.writeShortInt(bb, terminalInformationLength);
		bb.put(terminalInformation.getBytes());
		return writeMessageEnd(bb);
	}

	@Override
	public String readResponseString(ReRixMessage msg) throws InvalidByteValueException {
		return readResponseString8F(msg, uniqueDeviceSerialNumber.length());
	}

	public int getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(int deviceType) {
		this.deviceType = deviceType;
	}

	public String getUniqueDeviceSerialNumber() {
		return uniqueDeviceSerialNumber;
	}

	public void setUniqueDeviceSerialNumber(String uniqueDeviceSerialNumber) {
		this.uniqueDeviceSerialNumber = uniqueDeviceSerialNumber;
	}

	public String getDeviceInformation() {
		return deviceInformation;
	}

	public void setDeviceInformation(String deviceInformation) {
		this.deviceInformation = deviceInformation;
	}

	public String getTerminalInformation() {
		return terminalInformation;
	}

	public void setTerminalInformation(String terminalInformation) {
		this.terminalInformation = terminalInformation;
	}
	
}
