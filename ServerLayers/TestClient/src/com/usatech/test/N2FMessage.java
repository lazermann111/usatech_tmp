package com.usatech.test;

import java.nio.ByteBuffer;

import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
/**
 * 2F generic ack -->none
 * @author yhe
 *
 */
public class N2FMessage extends BaseMessage {
	protected int ackedMessageNumber;
	public N2FMessage() {
		super();
		dataType=0x2F;
		ackedMessageNumber=1;
	}

	@Override
	public ReRixMessage createMessage() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		ProcessingUtils.writeByteInt(bb, ackedMessageNumber);
		return writeMessageEnd(bb);
	}

	@Override
	public String readResponseString(ReRixMessage msg) throws InvalidByteValueException {
		return "No Response.\n";
	}

	public int getAckedMessageNumber() {
		return ackedMessageNumber;
	}

	public void setAckedMessageNumber(int ackedMessageNumber) {
		this.ackedMessageNumber = ackedMessageNumber;
	}
}
