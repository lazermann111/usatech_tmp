package com.usatech.test;

import java.math.BigDecimal;

import simple.io.Log;

import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.EntryType;

public class OTIDataBlob implements CardReaderDataBlob {

	private static Log log = Log.getLog();
	
	private byte [] dataBuffer;
	private EntryType entryType;
	private String filename;
	
	public OTIDataBlob(byte []dataBuffer, EntryType entryType, String filename) {
		this.dataBuffer = dataBuffer;
		this.entryType = entryType;
		this.filename = filename;
	}

	public byte [] getFileContent() {
		return getDataBuffer();
	}
	@Override
	public byte[] getDataBuffer() {
		return dataBuffer;
	}

	@Override
	public CardReaderType getCardReaderType() {
		return CardReaderType.OTI_EMV;
	}

	@Override
	public EntryType getEntryType() {
		return entryType;
	}
	
	@Override
	public String toStringId() {
		String dataLengthString = ", data length: " + ((getDataBuffer() != null) ? "" + getDataBuffer().length : "null");
		return getFilename() + " CardReaderType: " + getCardReaderType() + ", EntryType: " + getEntryType() + dataLengthString;
	}

	@Override
	public void dumpCompleteDataMsg(int msgNumber) {
		log.info("Msg # " + msgNumber + " type OTI_BEZEL dump NYI");
		
		int BUFFER_WIDTH = 16;
		byte asciiBuffer [] = new byte [BUFFER_WIDTH];
		int asciiBufferCounter = 0;
		
		int msgLength = getFileContent().length;
		
//		byte [] dumpBuffer = new byte [msgLength];
//		System.arraycopy(getHeaderBuffer(), 0, dumpBuffer, 0, getHeaderBuffer().length);
//		System.arraycopy(getDataBuffer(), 0, dumpBuffer, getHeaderBuffer().length, getDataBuffer().length);
//		System.arraycopy(getCrcBuffer(), 0, dumpBuffer, getHeaderBuffer().length + getDataBuffer().length, getCrcBuffer().length);
		byte [] dumpBuffer = getFileContent();
		
		System.out.println("\nComplete Reader Message #" + msgNumber + ": full message length: " + msgLength);
		System.out.println("============ BEGIN =====================");
		System.out.println("POS");
		boolean needRowCounter = true;
		for (int ii = 0; ii < dumpBuffer.length; ii++) {
			if (needRowCounter) {
				System.out.print(String.format("%03d  ", ii));
				needRowCounter = false;
			}
			int c = dumpBuffer[ii];
			asciiBuffer[asciiBufferCounter] = (byte)(c & 0xff);
			asciiBufferCounter++;
        	String value = String.format("%02X", (c & 0xff));
        	System.out.print(value + " ");
        	if ((asciiBufferCounter % BUFFER_WIDTH) == 0) {
        		CardReaderDataBlob.dumpBufferAsAscii(asciiBuffer, asciiBufferCounter);
				asciiBufferCounter = 0;
        		System.out.println("");
        		needRowCounter = true;
        	}
		}
		System.out.println("\n============ END ======================");
		
		
	}

	@Override
	public byte[] entireContents() {
		return getDataBuffer();
	}

	@Override
	public byte[] getRawData() {
		return getDataBuffer();
	}

	@Override
	public String getFilename() {
		return filename;
	}

	@Override
	public BigDecimal amountAuthorized() {
		return new BigDecimal("100");
	}
	
}
