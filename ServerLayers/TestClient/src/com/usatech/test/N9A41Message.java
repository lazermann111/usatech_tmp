package com.usatech.test;

import java.nio.ByteBuffer;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
/**
 * 9A41 Room Status With Starting Port Number -->2F
 * @author yhe
 *
 */
public class N9A41Message extends N9A47Message {
	protected int startingPortNumber;
	public N9A41Message() {
		super();
		dataType2=0x41;
		startingPortNumber=10;
	}
	@Override
	public ReRixMessage createMessage() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		bb.put((byte)startingPortNumber);
		for(int i=0; i<numberOfBlocks; i++){
			ProcessingUtils.writeByteInt(bb, washerDryerStatusByteBlockPort[i]);
		}
		return writeMessageEnd(bb);
	}
	public int getStartingPortNumber() {
		return startingPortNumber;
	}
	public void setStartingPortNumber(int startingPortNumber) {
		this.startingPortNumber = startingPortNumber;
	}

}
