package com.usatech.test;

import java.nio.ByteBuffer;

import simple.lang.InvalidByteValueException;

import com.usatech.networklayer.ReRixMessage;
/**
 * 75 Device Control Ack -->none
 * @author yhe
 *
 */
public class N75Message extends BaseMessage {
	public N75Message() {
		super();
		dataType=0x75;
	}

	@Override
	public ReRixMessage createMessage() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		return writeMessageEnd(bb);
	}

	@Override
	public String readResponseString(ReRixMessage msg) throws InvalidByteValueException {
		return "No Response.\n";
	}

}
