package com.usatech.test;

import java.nio.ByteBuffer;
import java.util.ArrayList;

import simple.lang.InvalidByteValueException;

import com.usatech.networklayer.ReRixMessage;
/**
 * 9A6A Washer/Dryer Labels --> 2F
 * @author yhe
 *
 */
public class N9A6AMessage extends ESudsMessage {
	protected int numberOfPortLabels;//not in spec but needed for input
	protected ArrayList<PortLabel> portLabels=new ArrayList<PortLabel>(5);
	public N9A6AMessage() {
		super();
		dataType2=0x6A;
		numberOfPortLabels=1;
	}
	@Override
	public void getParams() throws Exception{
		super.getParams();
		for(int i=0; i<numberOfPortLabels; i++){
			PortLabel pl=new PortLabel(userInputTokens);
			pl.getParams();
			portLabels.add(pl);
		}
	}
	@Override
	public ReRixMessage createMessage() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		for(PortLabel pl: portLabels){
			pl.writeMessage(bb);
		}
		return writeMessageEnd(bb);
	}

	@Override
	public String readResponseString(ReRixMessage msg) throws InvalidByteValueException {
		return readResponseString2F(msg);
	}
	public int getNumberOfPortLabels() {
		return numberOfPortLabels;
	}
	public void setNumberOfPortLabels(int numberOfPortLabels) {
		this.numberOfPortLabels = numberOfPortLabels;
	}
	public ArrayList<PortLabel> getPortLabels() {
		return portLabels;
	}
	public void setPortLabels(ArrayList<PortLabel> portLabels) {
		this.portLabels = portLabels;
	}

}
