package com.usatech.test;

import java.nio.ByteBuffer;
import java.util.StringTokenizer;

/**
 * Used by Esuds 9A6A message
 * @author yhe
 *
 */
public class PortLabel extends SubMessage {
	protected int portNumber;
	protected String portLabel;
	protected int topOrBottom;
	public PortLabel(StringTokenizer userInputTokens) {
		super(userInputTokens);
		portNumber=10;
		portLabel="abc";
		topOrBottom='B';
		
	}
	
	@Override
	public void getParams()throws Exception{
		super.getParams();
		if(portLabel.length()>3){
			throw new IllegalArgumentException("portLabel should not exceed 3 bytes.");
		}else if(portLabel.length()!=3){
			for(int i=0; i<3-portLabel.length(); i++){
				portLabel+=" ";
			}
		}
	}

	@Override
	public void writeMessage(ByteBuffer bb) {
		bb.put((byte)portNumber);
		bb.put(portLabel.getBytes());
		bb.put((byte)topOrBottom);
	}

	public int getPortNumber() {
		return portNumber;
	}

	public void setPortNumber(int portNumber) {
		this.portNumber = portNumber;
	}

	public String getPortLabel() {
		return portLabel;
	}

	public void setPortLabel(String portLabel) {
		this.portLabel = portLabel;
	}

	public int getTopOrBottom() {
		return topOrBottom;
	}

	public void setTopOrBottom(int topOrBottom) {
		this.topOrBottom = topOrBottom;
	}

}
