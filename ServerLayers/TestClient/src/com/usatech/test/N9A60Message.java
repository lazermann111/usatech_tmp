package com.usatech.test;
/**
 * 9A60 Intended Purchases Network Auth Batch -->71h
 * @author yhe
 *
 */
public class N9A60Message extends N9A5EMessage {

	public N9A60Message() {
		super();
		dataType2=0x60;
	}

}
