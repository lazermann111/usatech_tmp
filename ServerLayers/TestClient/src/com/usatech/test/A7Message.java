package com.usatech.test;

import java.nio.ByteBuffer;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;

public class A7Message extends A5Message {
	int packetNum;
	public A7Message() {
		super();
		dataType=0xA7;
	}
	@Override
	public ReRixMessage createMessage() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		ProcessingUtils.writeByteInt(bb, group);
		ProcessingUtils.writeShortInt(bb, packetNum);
		return writeMessageEnd(bb);
	}
	public int getPacketNum() {
		return packetNum;
	}
	public void setPacketNum(int packetNum) {
		this.packetNum = packetNum;
	}
}
