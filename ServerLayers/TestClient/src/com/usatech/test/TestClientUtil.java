package com.usatech.test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

import simple.io.ByteArrayUtils;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;
import simple.util.concurrent.LockSegmentFutureCache;

import com.usatech.layers.common.BasicDeviceInfo;
import com.usatech.layers.common.DeviceInfo;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.ActivationStatus;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.ServerActionCode;
import com.usatech.util.crypto.CRCException;
import com.usatech.util.crypto.Crypt;
import com.usatech.util.crypto.CryptException;
import com.usatech.util.crypto.CryptUtil;
/**
 * Methods to get BCD number and BCD date time for legacy device messages.
 * @author yhe
 *
 */
public class TestClientUtil {

	protected static final java.text.SimpleDateFormat bcdDateFormat = new java.text.SimpleDateFormat("HHmmssMMddyyyy");
	protected static Crypt           crypt           = CryptUtil.getCrypt("AES_CBC_CRC16");
	
	public static Calendar getTimeByUTCAndOffset(String utcTime, int utcOffset)throws ParseException{
		Calendar eventTs = Calendar.getInstance();
		eventTs.setTime(bcdDateFormat.parse(utcTime));
		eventTs.set(Calendar.DST_OFFSET, 0);
		eventTs.set(Calendar.ZONE_OFFSET, utcOffset);
		return eventTs;
	}
	
	public static Calendar getTimeByUTCAndOffset(long utcTime, int utcOffset)throws ParseException{
		Calendar eventTs = Calendar.getInstance();
		eventTs.setTimeInMillis(utcTime);
		eventTs.set(Calendar.DST_OFFSET, 0);
		eventTs.set(Calendar.ZONE_OFFSET, utcOffset);
		return eventTs;
	}
	
	public static byte[] getBCDByteArrayFromNumber(long num) throws IllegalArgumentException{
		String number=String.valueOf(num);
		int len=number.length();
		if(len<8){
			for(int i=0; i<8-len; i++){
				number="0"+number;
			}
		}else if(len>8){
			throw new IllegalArgumentException("Number:"+number+" is not valid.");
		}
		return ByteArrayUtils.fromHex(number); 
	}
	
	public static byte[] getBCDDateTime(Date aDate) throws IllegalArgumentException{
		String dateStr=bcdDateFormat.format(aDate);
		return ByteArrayUtils.fromHex(dateStr); 
	}
	
	/**
	 * @Todo
	 * current server implementation is wrong. once corrected, this method needs to change
	 * @param bb
	 * @return
	 */
	public static String getBCDTime(ByteBuffer bb){
		String bcdTime="";
		for(int i=0; i<7; i++){
			String byteHex=StringUtils.toHex(bb.get());
			int value=Integer.parseInt(byteHex, 16);
			if(value<10){
				bcdTime+= "0"+value;
			}else{
				bcdTime+=String.valueOf(value);
			}
		}
		return bcdTime;
	}
	
	public static long getBCDNumberFromByteBuffer(ByteBuffer bb){
		byte[] numberArray=new byte[4];
		bb.get(numberArray, 0, 4);
		return getBCDNumberFromByteArray(numberArray);
	}
	
	public static long getBCDNumberFromByteArray(byte[] bb){
		String bcdNum="";
		for(int i=0; i<4; i++){
			String byteHex=StringUtils.toHex(bb[i]);
			int value=Integer.parseInt(byteHex);
			bcdNum+=String.valueOf(value);
		}
		return Long.parseLong(bcdNum);
	}
	
	public static long getHexToDecimalFromByteBuffer(ByteBuffer bb){
		byte[] numberArray=new byte[4];
		bb.get(numberArray, 0, 4);
		String hexStr=StringUtils.toHex(numberArray);
		return hex2decimal(hexStr);
	}
	
	public static long hex2decimal(String s) {
        String digits = "0123456789ABCDEF";
        s = s.toUpperCase();
        long val = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            int d = digits.indexOf(c);
            val = 16*val + d;
        }
        return val;
    }
	
	protected static byte[] getEncryptedData(String cardData, String encryptKey) throws CryptException, CRCException, InvalidKeyException{
   	 byte[] data = cardData.getBytes();
   	 return  crypt.encrypt(data, ByteArrayUtils.fromHex(encryptKey));
	}
	
	public static int calcNumOfBlocks(int MAX_PAYLOAD, long totalNumOfBytesInFileTransfer){
		BigDecimal totalBytes=new BigDecimal(totalNumOfBytesInFileTransfer);
		return totalBytes.divide(new BigDecimal(MAX_PAYLOAD), RoundingMode.UP).intValue();
	}
	
	public static String decimalToHexLowerCase(long d) {
        String digits = "0123456789ABCDEF";
        if (d == 0) return "0";
        String hex = "";
        while (d > 0) {
            int digit = (int)d % 16;                // rightmost digit
            hex = digits.charAt(digit) + hex;  // string concatenation
            d = d / 16;
        }
        hex=StringUtils.pad(hex, '0', 2, Justification.RIGHT).toLowerCase();
        return hex;
    }
	
	public static void writeDecimalToHex(ByteBuffer buffer, long i) throws IllegalArgumentException, BufferOverflowException {
		//byte[] bcdArray=decimalToHex(i).getBytes();
		//System.out.println("length:"+bcdArray.length);
		//buffer.put(bcdArray);
		ProcessingUtils.writeLongInt(buffer, i);
	}
	
	public static long readHexToDecimal(ByteBuffer buffer) throws BufferUnderflowException{
		byte[] numberArray=new byte[4];
		buffer.get(numberArray, 0, 4);
		String hexStr=StringUtils.toHex(numberArray);
		return hexToDecimal(hexStr);
	}
	public static String decimalToHex(long d, int pad) {
        String digits = "0123456789ABCDEF";
        if (d == 0) return "0";
        String hex = "";
        while (d > 0) {
            int digit = (int)d % 16;                // rightmost digit
            hex = digits.charAt(digit) + hex;  // string concatenation
            d = d / 16;
        }
        hex=StringUtils.pad(hex, '0', pad, Justification.RIGHT).toLowerCase();
        return hex;
    }
	
	public static long hexToDecimal(String s) {
        String digits = "0123456789ABCDEF";
        s = s.toUpperCase();
        long val = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            int d = digits.indexOf(c);
            val = 16*val + d;
        }
        return val;
    }
	
	public static int decimalToHex(byte i){
		return Integer.parseInt(Integer.toHexString(i));
	}
	
	public static void writeTimestampBCD(ByteBuffer buffer, Date timestamp){
		//timestamp.set(Calendar.DST_OFFSET, 0);
		String dateStr=bcdDateFormat.format(timestamp);
		buffer.put(ByteArrayUtils.fromHex(dateStr)); 
	}
	public static Date readTimestampBCD(ByteBuffer buffer) throws BufferUnderflowException{
		byte[] bcdTime=new byte[7];
		buffer.get(bcdTime);
		Calendar calendar=Calendar.getInstance();
		int currentYear=calendar.get(Calendar.YEAR);
		calendar.clear();
		int hour=decimalToHex(bcdTime[0]);
		if(hour>24){
			throw new IllegalArgumentException("Time Validation Failed! Hour ="+hour);
		}
		int min=decimalToHex(bcdTime[1]);
		if(min>60){
			throw new IllegalArgumentException("Time Validation Failed! Hour ="+min);
		}
		int seconds=decimalToHex(bcdTime[2]);
		if(seconds>60){
			throw new IllegalArgumentException("Time Validation Failed! Hour ="+seconds);
		}
		int month=decimalToHex(bcdTime[3]);
		if(month>12||month==0){
			throw new IllegalArgumentException("Time Validation Failed! Hour ="+month);
		}
		int dayOfMonth=decimalToHex(bcdTime[4]);
		if(dayOfMonth>31||dayOfMonth==0){
			throw new IllegalArgumentException("Time Validation Failed! Hour ="+dayOfMonth);
		}
		int year=decimalToHex(bcdTime[5])*100+decimalToHex(bcdTime[6]);
		if(year>currentYear+1||year<currentYear-1){
			throw new IllegalArgumentException("Time Validation Failed! Hour ="+year);
		}
		calendar.set(year, month-1, dayOfMonth, hour, min, seconds);
		//calendar.set(Calendar.DST_OFFSET, 0);
		//calendar.set(Calendar.ZONE_OFFSET, calendar.getTimeZone().getOffset(calendar.getTimeInMillis()));
		return calendar.getTime();
	}
	
	public static Calendar getCalendarBySeconds(long timeSeconds, String timeZoneGuid){
		Calendar returnCalendar = Calendar.getInstance();
		returnCalendar.clear();
		returnCalendar.setTimeInMillis(timeSeconds*1000);
		TimeZone deviceTimeZone=TimeZone.getTimeZone(timeZoneGuid);
		returnCalendar.set(Calendar.DST_OFFSET, deviceTimeZone.getDSTSavings());
		returnCalendar.set(Calendar.ZONE_OFFSET, deviceTimeZone.getOffset(returnCalendar.getTimeInMillis()));
		return returnCalendar;
	}
	
	public static Calendar getCalendarByDate(Date date, String timeZoneGuid){
		return getCalendarBySeconds(date.getTime()/1000, timeZoneGuid);
	}
	public static interface ObjectFactory {
		public Object makeObject() ;
	}
	public static long calculateMemoryUsage(ObjectFactory factory, int n) {
	    Object[] handles = new Object[n];
	    for(int i = 0; i < n; i++)
	    	handles[i] = factory.makeObject();
	    long mem0 = Runtime.getRuntime().totalMemory() -
	      Runtime.getRuntime().freeMemory();
	    long mem1 = Runtime.getRuntime().totalMemory() -
	      Runtime.getRuntime().freeMemory();
	    for(int i = 0; i < n; i++)
	    	handles[i] = null;
	    System.gc(); System.gc(); System.gc(); System.gc();
	    System.gc(); System.gc(); System.gc(); System.gc();
	    System.gc(); System.gc(); System.gc(); System.gc();
	    System.gc(); System.gc(); System.gc(); System.gc();
	    mem0 = Runtime.getRuntime().totalMemory() -
	      Runtime.getRuntime().freeMemory();
	    for(int i = 0; i < n; i++)
	    	handles[i] = factory.makeObject();
	    System.gc(); System.gc(); System.gc(); System.gc();
	    System.gc(); System.gc(); System.gc(); System.gc();
	    System.gc(); System.gc(); System.gc(); System.gc();
	    System.gc(); System.gc(); System.gc(); System.gc();
	    mem1 = Runtime.getRuntime().totalMemory() -
	      Runtime.getRuntime().freeMemory();
	    return mem1 - mem0;
	}

	public static long calculateDIFutureCacheMemoryUsage(int n) throws ExecutionException {
	    LockSegmentFutureCache<String, DeviceInfo> cache = new LockSegmentFutureCache<String, DeviceInfo>(0, 1000, 0.75f, 32) {			
			@Override
			protected DeviceInfo createValue(String key, Object... additionalInfo) throws Exception {
				return generateDeviceInfo(key);
			}
		};
		for(int i = 0; i < n; i++)
	    	cache.getOrCreate(generateDeviceName());
	    long mem0 = Runtime.getRuntime().totalMemory() -
	      Runtime.getRuntime().freeMemory();
	    long mem1 = Runtime.getRuntime().totalMemory() -
	      Runtime.getRuntime().freeMemory();
	    cache.clear();
	    System.gc(); System.gc(); System.gc(); System.gc();
	    System.gc(); System.gc(); System.gc(); System.gc();
	    System.gc(); System.gc(); System.gc(); System.gc();
	    System.gc(); System.gc(); System.gc(); System.gc();
	    mem0 = Runtime.getRuntime().totalMemory() -
	      Runtime.getRuntime().freeMemory();
	    for(int i = 0; i < n; i++)
	    	cache.getOrCreate(generateDeviceName());
	    System.gc(); System.gc(); System.gc(); System.gc();
	    System.gc(); System.gc(); System.gc(); System.gc();
	    System.gc(); System.gc(); System.gc(); System.gc();
	    System.gc(); System.gc(); System.gc(); System.gc();
	    mem1 = Runtime.getRuntime().totalMemory() -
	      Runtime.getRuntime().freeMemory();
	    return mem1 - mem0;
	}
	public static void showDICacheMemoryUsage(int n) throws ExecutionException {
		long mem = calculateDIFutureCacheMemoryUsage(n);
		System.out.println("Device Info Future Cache produced " + n + " objects which took " + mem + " bytes");
	}
	
	public static void showMemoryUsage(ObjectFactory factory, int n) {
		long mem = calculateMemoryUsage(factory, n);
		System.out.println(factory.getClass().getName() + " produced " + factory.makeObject().getClass().getName() + " which took " + mem + " bytes for " + n + " objects");
	}
	protected static final Random random = new Random();
	protected static final AtomicInteger seq = new AtomicInteger(23437);
	protected static final DecimalFormat format = new DecimalFormat("000000");
	
	protected static String generateDeviceName() {
		return "TD" + format.format(seq.incrementAndGet());
	}
	protected static byte[] generateKey() {
		byte[] key = new byte[16];
		random.nextBytes(key);
		return key;
	}
	protected static BasicDeviceInfo generateDeviceInfo(String deviceName) {
		BasicDeviceInfo di = new BasicDeviceInfo(deviceName) {
			protected final Set<Object> listeners = Collections.newSetFromMap(new WeakHashMap<Object, Boolean>());
			protected final ReentrantLock lock = new ReentrantLock();
			
			@Override
			protected void unlock() {
				lock.unlock();
			}
			
			@Override
			protected void lock() {
				lock.lock();
			}
		};
		di.setActionCode(ServerActionCode.NO_ACTION);
		di.setActivationStatus(ActivationStatus.ACTIVATED);
		di.setDeviceCharset(ProcessingConstants.US_ASCII_CHARSET);
		di.setDeviceType(DeviceType.EDGE);
		di.setEncryptionKey(generateKey());
		di.setInitOnly(false);
		di.setLocale(Locale.getDefault().toString());
		di.setPreviousEncryptionKey(generateKey());
		di.setPropertyIndexesToRequest("300-301|1050|1081-1084");
		di.setTimeZoneGuid(TimeZone.getDefault().getID());
		return di;
	}
	public static void showDeviceInfoMemoryUsage() {
		showMemoryUsage(new ObjectFactory() {			
			@Override
			public Object makeObject() {
				return generateDeviceInfo(generateDeviceName());
			}
		}, 1000);
	}
	public static void main(String args[]) throws Exception{
		showDICacheMemoryUsage(1000);
		//showDeviceInfoMemoryUsage();
		/**
		 * 
		byte[] bcd=getBCDByteArrayFromNumber(1234567);
		System.out.println("bcd reserve:"+getBCDNumberFromByteArray(bcd));
		
		byte[] test=new byte[4];
		test[0]=0;
		test[1]=18;
		test[2]=-42;
		test[3]=-121;
		System.out.println("bcd :"+hex2decimal("0012D687"));
		*//*
		Calendar time=Calendar.getInstance();
		System.out.println("Calendar 1:"+time);
		byte[] bcdTime=getBCDDateTime(time.getTime());
		for(int i=0; i<bcdTime.length; i++){
			System.out.println(Integer.toHexString(bcdTime[i]));
		}
		Calendar timeNew=Calendar.getInstance();
		int year=decimalToHex(bcdTime[5])*100+decimalToHex(bcdTime[6]);
		int month=decimalToHex(bcdTime[3]);
		int dayOfMonth=decimalToHex(bcdTime[4]);
		int hour=decimalToHex(bcdTime[0]);
		int min=decimalToHex(bcdTime[1]);
		int seconds=decimalToHex(bcdTime[2]);
		System.out.println("month:"+month);
		timeNew.set(year, month-1, dayOfMonth, hour, min, seconds);
		System.out.println("Calendar 2:"+timeNew);
		bcdTime=getBCDDateTime(timeNew.getTime());
		for(int i=0; i<bcdTime.length; i++){
			System.out.println(Integer.toHexString(bcdTime[i]));
		}
		System.out.println("##############");
		ByteBuffer bb=ByteBuffer.allocate(7);
		writeTimestampBCD(bb, time.getTime());
		bb.flip();
		Calendar calendar=Calendar.getInstance();
		calendar.clear();
		calendar.setTime(readTimestampBCD(bb));
		System.out.println("Date is:"+calendar.getTime());
		bcdTime=getBCDDateTime(calendar.getTime());
		for(int i=0; i<bcdTime.length; i++){
			System.out.println(Integer.toHexString(bcdTime[i]));
		}
		
		System.out.println(getCalendarByDate(new Date(), "US/Mountain").getTime());
		*/
	}
	
	public static int generateMessageNumber(int messageNumber)
	{
		return messageNumber < 0 || messageNumber >= 255 ? 0 : messageNumber + 1;
	}
	
	public static long generateMessageId(long messageId)
	{
		long currentTimeSec = System.currentTimeMillis() / 1000;
		return messageId >= currentTimeSec ? messageId + 1 : currentTimeSec;
	}
	
	public static long getServerTimeZoneOffset(){
		return Calendar.getInstance().getTimeZone().getOffset(System.currentTimeMillis());
	}
	
	public static long getDeviceLocalTimeByOffsetForBCD(long timeZoneOffset){
		return System.currentTimeMillis()+getTimeZoneOffsetDiff(timeZoneOffset);
	}
	
	public static long getDeviceLocalTimeByOffset(long timeZoneOffset){
		return System.currentTimeMillis()+timeZoneOffset;
	}
	
	public static long getTimeZoneOffsetDiff(long deviceTimeZoneOffset){
		long serverOffset=getServerTimeZoneOffset();
		return deviceTimeZoneOffset-serverOffset;
	}
}
