package com.usatech.test;

import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.StringTokenizer;

import simple.bean.ConvertUtils;
import simple.io.Log;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
/**
 * C4 Sale -->CB response
 * @author yhe
 *
 */
public class C4Message extends V4Message {
	private static final Log log = Log.getLog();
	protected long transactionId=System.currentTimeMillis()/1000;
	protected long batchId;
	//0x41, 0x49, 0x43, A Actual purchase, I Intended purchase, C Cash
	protected int saleType;
	protected Calendar saleSessionStartTime;
	protected long saleSessionStartUTCTime;
	protected int saleSessionStartUTCOffset;
	protected int saleResult;
	protected BigDecimal saleAmount;
	protected int receiptResult;
	protected int lineItemVendBlockFormat;
	protected int numOfLineItemVendBlocks;
	protected ArrayList<LineItemVendBlock> lineItemVendBlocks=new ArrayList<LineItemVendBlock>(5);

	protected class LineItemVendBlock extends SubMessage{
		protected int componentNum;
		protected int itemPurchased;
		protected int saleResult;
		protected int quantity;
		protected BigDecimal price;
		protected String description;
		protected int duration;
		public LineItemVendBlock(StringTokenizer userInputTokens){
			super(userInputTokens);
			componentNum=0;
			itemPurchased=1;
			saleResult=0;
			quantity=2;
			price=new BigDecimal("100");
			description="test";
			duration=30;
		}
		
		public void writeMessage(ByteBuffer bb){
			ProcessingUtils.writeByteInt(bb,componentNum);
			ProcessingUtils.writeShortInt(bb, itemPurchased);
			ProcessingUtils.writeByteInt(bb,saleResult);
			ProcessingUtils.writeShortInt(bb, quantity);
			ProcessingUtils.writeStringAmount(bb, price);
			ProcessingUtils.writeShortString(bb, description, ProcessingConstants.US_ASCII_CHARSET);
			ProcessingUtils.writeShortInt(bb, duration);
		}
		public int getComponentNum() {
			return componentNum;
		}
		public void setComponentNum(int componentNum) {
			this.componentNum = componentNum;
		}
		public int getItemPurchased() {
			return itemPurchased;
		}
		public void setItemPurchased(int itemPurchased) {
			this.itemPurchased = itemPurchased;
		}
		public int getSaleResult() {
			return saleResult;
		}
		public void setSaleResult(int saleResult) {
			this.saleResult = saleResult;
		}
		public int getQuantity() {
			return quantity;
		}
		public void setQuantity(int quantity) {
			this.quantity = quantity;
		}
		public BigDecimal getPrice() {
			return price;
		}
		public void setPrice(BigDecimal price) {
			this.price = price;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public int getDuration() {
			return duration;
		}
		public void setDuration(int duration) {
			this.duration = duration;
		}
	}
	public C4Message(){
		super();
		dataType=0xC4;
		transactionId=0;
		batchId=0;
		saleType=0x43;//cash
		saleSessionStartUTCTime=System.currentTimeMillis();
		saleSessionStartUTCOffset=Calendar.getInstance().get(Calendar.ZONE_OFFSET);
		saleResult=0;
		saleAmount=new BigDecimal(10);
		receiptResult=0x50;//P receipt requested and sent successfully
		lineItemVendBlockFormat=0;
		numOfLineItemVendBlocks=1;
	}
	@Override
	public ReRixMessage createMessage() {
		ByteBuffer bb=ByteBuffer.allocate(280+numOfLineItemVendBlocks*520);
		writeMessageHeader(bb);
		ProcessingUtils.writeLongInt(bb,transactionId);
		ProcessingUtils.writeLongInt(bb,batchId);
		ProcessingUtils.writeByteInt(bb,saleType);
		ProcessingUtils.writeTimestamp(bb,saleSessionStartTime);
		ProcessingUtils.writeByteInt(bb,saleResult);
		ProcessingUtils.writeStringAmount(bb, saleAmount);
		ProcessingUtils.writeByteInt(bb, receiptResult);
		ProcessingUtils.writeByteInt(bb, lineItemVendBlockFormat);
		ProcessingUtils.writeByteInt(bb, numOfLineItemVendBlocks);
		for (LineItemVendBlock lineItem : lineItemVendBlocks){
			lineItem.writeMessage(bb);
		}
		return writeMessageEnd(bb);
	}

	@Override
	public void getParams() throws Exception{
		super.getParams();
		saleSessionStartTime=TestClientUtil.getTimeByUTCAndOffset(saleSessionStartUTCTime, saleSessionStartUTCOffset);
		for(int i=0;i<numOfLineItemVendBlocks; i++){
			LineItemVendBlock lineItem=new LineItemVendBlock(userInputTokens);
			lineItem.getParams();
			lineItemVendBlocks.add(lineItem);
		}
		if(previousMessage!=null){
			log.info("Use Previous Message transactionId:"+previousMessage.responseMap.get("transactionId"));
			transactionId=ConvertUtils.getLong(responseMap.get("transactionId"), transactionId);
		}
	}

	public long getBatchId() {
		return batchId;
	}
	public void setBatchId(long batchId) {
		this.batchId = batchId;
	}
	public int getSaleType() {
		return saleType;
	}
	public void setSaleType(int saleType) {
		this.saleType = saleType;
	}
	public int getSaleResult() {
		return saleResult;
	}
	public void setSaleResult(int saleResult) {
		this.saleResult = saleResult;
	}
	public BigDecimal getSaleAmount() {
		return saleAmount;
	}
	public void setSaleAmount(BigDecimal saleAmount) {
		this.saleAmount = saleAmount;
	}
	public int getReceiptResult() {
		return receiptResult;
	}
	public void setReceiptResult(int receiptResult) {
		this.receiptResult = receiptResult;
	}
	public int getLineItemVendBlockFormat() {
		return lineItemVendBlockFormat;
	}
	public void setLineItemVendBlockFormat(int lineItemVendBlockFormat) {
		this.lineItemVendBlockFormat = lineItemVendBlockFormat;
	}
	public int getNumOfLineItemVendBlocks() {
		return numOfLineItemVendBlocks;
	}
	public void setNumOfLineItemVendBlocks(int numOfLineItemVendBlocks) {
		this.numOfLineItemVendBlocks = numOfLineItemVendBlocks;
	}
	public ArrayList<LineItemVendBlock> getLineItemVendBlocks() {
		return lineItemVendBlocks;
	}
	public void setLineItemVendBlocks(ArrayList<LineItemVendBlock> lineItemVendBlocks) {
		this.lineItemVendBlocks = lineItemVendBlocks;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception{
		C4Message sMsg=new C4Message();
		ClientConnectUtil conn=new ClientConnectUtil();
		conn.setProtocol(7);
		conn.startCommunication();
		sMsg.getParams();
		ReRixMessage sentMsg=sMsg.createMessage();
		ReRixMessage responseMsg=conn.sendMessage(sentMsg,sMsg.encryptKey);
		sMsg.readResponse(responseMsg);
		conn.stopCommunication();
	}
	public long getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
	public long getSaleSessionStartUTCTime() {
		return saleSessionStartUTCTime;
	}
	public void setSaleSessionStartUTCTime(long saleSessionStartUTCTime) {
		this.saleSessionStartUTCTime = saleSessionStartUTCTime;
	}
	public int getSaleSessionStartUTCOffset() {
		return saleSessionStartUTCOffset;
	}
	public void setSaleSessionStartUTCOffset(int saleSessionStartUTCOffset) {
		this.saleSessionStartUTCOffset = saleSessionStartUTCOffset;
	}
}
