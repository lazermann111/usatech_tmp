package com.usatech.test;

import com.usatech.networklayer.ReRixMessage;

public class C9MessageManual extends C9Message {
	public C9MessageManual(){
		super();
	}
	public byte[] getBlockPayload() {
		return blockPayload;
	}

	public void setBlockPayload(byte[] blockPayload) {
		this.blockPayload = blockPayload;
	}
	
	public static void main(String[] args) throws Exception{
		ClientConnectUtil conn=new ClientConnectUtil();
		conn.setProtocol(7);
		conn.startCommunication();
		C8MessageManual sMsg=new C8MessageManual();
		sMsg.getParams();
		ReRixMessage sentMsg=sMsg.createMessage();
		ReRixMessage responseMsg=conn.sendMessage(sentMsg, sMsg.encryptKey);
		sMsg.readResponse(responseMsg);
		C9MessageManual sMsg2=new C9MessageManual();
		sMsg2.getParams();
		sentMsg=sMsg2.createMessage();
		responseMsg=conn.sendMessage(sentMsg, sMsg2.encryptKey);
		sMsg2.readResponse(responseMsg);
		conn.stopCommunication();

	}
}
