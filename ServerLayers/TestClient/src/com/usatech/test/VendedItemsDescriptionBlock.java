package com.usatech.test;

import java.nio.ByteBuffer;
import java.util.StringTokenizer;

import com.usatech.layers.common.ProcessingUtils;

public class VendedItemsDescriptionBlock extends SubMessage{
	public VendedItemsDescriptionBlock(StringTokenizer userInputTokens){
		super(userInputTokens);
	}
	protected int itemNumberVended=1;
	protected int amountOfItemVended=100;

	public void writeMessage(ByteBuffer bb){
		ProcessingUtils.writeByteInt(bb,itemNumberVended);
		ProcessingUtils.write3ByteInt(bb,amountOfItemVended);
	}
	public int getItemNumberVended() {
		return itemNumberVended;
	}
	public void setItemNumberVended(int itemNumberVended) {
		this.itemNumberVended = itemNumberVended;
	}
	public int getAmountOfItemVended() {
		return amountOfItemVended;
	}
	public void setAmountOfItemVended(int amountOfItemVended) {
		this.amountOfItemVended = amountOfItemVended;
	}
}