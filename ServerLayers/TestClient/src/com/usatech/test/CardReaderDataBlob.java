package com.usatech.test;

import java.io.IOException;
import java.math.BigDecimal;

import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.EntryType;

public interface CardReaderDataBlob {
	public byte [] getDataBuffer();
	public CardReaderType getCardReaderType();
	public EntryType getEntryType();
	public String toStringId(); 
	public void dumpCompleteDataMsg(int msgNumber);
	public byte [] entireContents();
	public byte [] getRawData();
	public BigDecimal amountAuthorized();
	public String getFilename();
	
	public static boolean isAscii(char c) {
		boolean result = (c >= 32 && c < 127);
		return result;
	}
	
	public static boolean isSpecialCharacter(char c) {
		boolean result = (c == '\n' || c == '\r' || c == '\t');
		return result;
	}

	public static void dumpBufferAsAscii(byte[] asciiBuffer, int asciiBufferCounter) {
		for (int ii = 0; ii < asciiBufferCounter; ii++) {
			char value = charAsAscii(asciiBuffer[ii]);
	    	System.out.print(value);
		}
	}
	public static char charAsAscii(byte value) {
		char result = 0;
		if (!isAscii((char)value) || isSpecialCharacter((char)value)) {
			result = '.';
		} else {
			result = (char)(value & 0x00ff);
		}
		return result;
	}

	public static CardReaderDataBlob getInstance(CardReaderType cardReaderType, byte [] fileContent, EntryType entryType, String filename) throws IOException {
		CardReaderDataBlob cardReaderData = null; 
		
		if (cardReaderType.equals(CardReaderType.OTI_EMV)) {
			cardReaderData = new OTIDataBlob(fileContent, entryType, filename);
		} else {
			cardReaderData = IdtechVendXDataBlob.parseVendXFromFile(fileContent, cardReaderType, entryType, filename);
		}
		return cardReaderData;
	}
}
