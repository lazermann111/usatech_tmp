package com.usatech.test;

import java.nio.ByteBuffer;
import java.util.ArrayList;

import simple.bean.ConvertUtils;
import simple.io.Log;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
/**
 * A2 --> 71h response
 * @author yhe
 *
 */
public class A2Message extends A2MessageBase {
	private static final Log log = Log.getLog();
	protected int numberOfVends;
	protected int vendByteLength;
	protected ArrayList<VendedItemsDescriptionBlock> vendedItemBlock=new ArrayList<VendedItemsDescriptionBlock>(5) ;
	
	public A2Message(){
		super();
		numberOfVends=1;
		vendByteLength=0;
	}

	@Override
	public void getParams() throws Exception{
		super.getParams();
		for(int i=0; i<numberOfVends; i++){
			VendedItemsDescriptionBlock vBlock=new VendedItemsDescriptionBlock(userInputTokens);
			vBlock.getParams();
			vendedItemBlock.add(vBlock);
		}
		if(previousMessage!=null){
			log.info("Use Previous Message transactionId:"+previousMessage.responseMap.get("transactionId"));
			transactionId=ConvertUtils.getLong(responseMap.get("transactionId"), transactionId);
		}
	}
	@Override
	public ReRixMessage createMessage() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		ProcessingUtils.writeLongInt(bb,transactionId);
		ProcessingUtils.writeLongInt(bb,amount);
		ProcessingUtils.writeByteInt(bb,transactionResult);
		int transactionDetail=(byte)numberOfVends|((byte)vendByteLength<<6);
		ProcessingUtils.writeByteInt(bb,transactionDetail);
		for(VendedItemsDescriptionBlock vBlock: vendedItemBlock){
			vBlock.writeMessage(bb);
		}
		return writeMessageEnd(bb);
	}
	public int getNumberOfVends() {
		return numberOfVends;
	}

	public void setNumberOfVends(int numberOfVends) {
		this.numberOfVends = numberOfVends;
	}

	public ArrayList<VendedItemsDescriptionBlock> getVendedItemBlock() {
		return vendedItemBlock;
	}

	public void setVendedItemBlock(ArrayList<VendedItemsDescriptionBlock> vendedItemBlock) {
		this.vendedItemBlock = vendedItemBlock;
	}
	
	public static void main(String args[]){
		int vendNumber=2;
		int vendByte=2;
		int finalByte=(byte)vendNumber|((byte)vendByte<<6);
		
		int first6=finalByte & 0x3f;
		int last2=finalByte>>6;
		System.out.println("ventNumber:"+first6);
		System.out.println("vendByte:"+last2);
		
	}
}
