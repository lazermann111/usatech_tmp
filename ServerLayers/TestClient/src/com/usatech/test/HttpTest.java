package com.usatech.test;

import java.awt.Window;
import java.io.Console;
import java.io.IOException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpState;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.auth.BasicScheme;
import org.apache.commons.httpclient.methods.PostMethod;

import simple.io.ConsoleInteraction;
import simple.io.GuiInteraction;
import simple.io.InOutInteraction;
import simple.io.Interaction;

public class HttpTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		testHttpToCa();
	}
	protected static void testHttpToCa() throws HttpException, IOException {
		Interaction interaction;
		Console console;
		if((console = System.console()) != null) {
			interaction = new ConsoleInteraction(console);
		} else if(!java.awt.GraphicsEnvironment.isHeadless()) {
			interaction = new GuiInteraction((Window) null);
		} else {
			interaction = new InOutInteraction(System.out, System.in);
		}
		
		//Sign entries
		HttpClient httpClient = new HttpClient();
		String url= "https://usasubca.usatech.com/certsrv/certfnsh.asp";
		HttpState state = new HttpState();
		String causername = System.getProperty("user.name");
		String capassword = new String(interaction.readPassword("Enter the password for user '%1$s' at '%2$s':", causername, url));
		state.setCredentials(new AuthScope(null, -1, null, "basic"), new UsernamePasswordCredentials(causername, capassword));
		//state.s
		PostMethod method = new PostMethod(url);
		method.addParameter("CertRequest", "JUNK");
		method.addParameter("TargetStoreFlags", "0");
		method.addParameter("SaveCert", "yes");
		method.addParameter("Mode", "newreq");
		method.addParameter("CertAttrib", "CertificateTemplate:" + "USATWebServer");
		method.getHostAuthState().setAuthScheme(new BasicScheme());
		
		int rc = httpClient.executeMethod(null, method, state);
		if(rc != 200)
			throw new IOException("Could not sign certificate. Got http code " + rc);
		
	}
}
