package com.usatech.test;

import java.nio.ByteBuffer;

import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.lang.InvalidByteValueException;
import simple.text.StringUtils;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
/**
 * AC auth v3.1 -->A1
 * @author yhe
 *
 */
public class ACMessage extends LegacyMessage {
	private static final Log log = Log.getLog();
	protected long transactionId=System.currentTimeMillis()/1000;
	protected int cardType;
	protected long amount;
	protected int cardReaderType;
	protected String cardData;
	protected int decryptedCardDataLength;
	protected int keyIdLength;
	protected String keyId;
	protected int encryptedCardDataLength;
	protected int pinLength;
	protected String pin;

	public ACMessage() {
		super();
		dataType=0xAC;
		cardType='C';
		amount=250;
		cardReaderType=1;
		keyId="1234";
		cardData="0123456789123456";
		pin="1234";
		
	}

	@Override
	public ReRixMessage createMessage() throws Exception {
		decryptedCardDataLength=cardData.length();
		byte[] encryptedCardData=TestClientUtil.getEncryptedData(cardData, encryptKey);
		encryptedCardDataLength=encryptedCardData.length;
		pinLength=pin.length();
		keyIdLength=keyId.length();
		log.info("encryptedCardDataLength:"+encryptedCardDataLength);
		log.info("encryptedCardData hex:"+StringUtils.toHex(encryptedCardData));
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		ProcessingUtils.writeLongInt(bb, transactionId);
		ProcessingUtils.writeByteInt(bb, cardType);
		ProcessingUtils.writeLongInt(bb, amount);
		ProcessingUtils.writeByteInt(bb, cardReaderType);
		ProcessingUtils.writeByteInt(bb, decryptedCardDataLength);
		ProcessingUtils.writeByteInt(bb, keyIdLength);
		bb.put(keyId.getBytes());
		ProcessingUtils.writeByteInt(bb, encryptedCardDataLength);
		bb.put(encryptedCardData);
		ProcessingUtils.writeByteInt(bb, pinLength);
		bb.put(pin.getBytes());
		return writeMessageEnd(bb);
	}

	@Override
	public String readResponseString(ReRixMessage msg) throws InvalidByteValueException {
		return readResponseStringA1(msg);
	}
	
	@Override
	public void verifyResponse(){
		long responseTransactionId=ConvertUtils.getLongSafely(responseMap.get("transactionId"), -1);
		if(transactionId!=responseTransactionId){
			log.error("Response transactionId:"+responseTransactionId+" does not match transactionId:"+transactionId);
		}
	}

	public int getCardType() {
		return cardType;
	}

	public void setCardType(int cardType) {
		this.cardType = cardType;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public int getCardReaderType() {
		return cardReaderType;
	}

	public void setCardReaderType(int cardReaderType) {
		this.cardReaderType = cardReaderType;
	}

	public String getCardData() {
		return cardData;
	}

	public void setCardData(String cardData) {
		this.cardData = cardData;
	}

	public String getKeyId() {
		return keyId;
	}

	public void setKeyId(String keyId) {
		this.keyId = keyId;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

}
