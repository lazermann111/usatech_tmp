package com.usatech.test;

import java.io.IOException;
import java.nio.ByteBuffer;

import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;

public class CBMessage extends V4Message {
	protected int resultCode;
	protected String responseMessage;
	protected int actionCode;
	protected boolean serverToClient;
	protected int reconnectTime;
	protected String propertyValueList;

	public CBMessage() {
		super();
		dataType = 0xCB;
		resultCode = 0;
		responseMessage = "";
		actionCode = 0;
		serverToClient = true;
	}

	@Override
	public ReRixMessage createMessage() {
		ByteBuffer bb=ByteBuffer.allocate(9);
		writeMessageHeader(bb);
		ProcessingUtils.writeByteInt(bb, resultCode);
		ProcessingUtils.writeShortString(bb, responseMessage, ProcessingConstants.US_ASCII_CHARSET);
		ProcessingUtils.writeByteInt(bb, actionCode);
		return writeMessageEnd(bb);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception{
		ClientConnectUtil conn=new ClientConnectUtil();
		conn.setProtocol(7);
		conn.startCommunication();
		CBMessage sMsg = new CBMessage();
		sMsg.getParams();
		ReRixMessage sentMsg = sMsg.createMessage();
		ReRixMessage responseMsg = conn.sendMessage(sentMsg, sMsg.encryptKey);
		sMsg.readResponse(responseMsg);
		conn.stopCommunication();
	}

	public int getResultCode() {
		return resultCode;
	}

	public void setResultCode(int resultCode) {
		this.resultCode = resultCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public int getActionCode() {
		return actionCode;
	}

	public void setActionCode(int actionCode) {
		this.actionCode = actionCode;
	}
	/**
	 * @throws IOException
	 * @see com.usatech.test.BaseMessage#read(ByteBuffer)
	 */
	@Override
	protected void read(ByteBuffer data) throws IOException, InvalidByteValueException {
		super.read(data);
		setResultCode(ProcessingUtils.readByteInt(data));
		setResponseMessage(ProcessingUtils.readShortString(data, ProcessingConstants.US_ASCII_CHARSET));
		setActionCode(ProcessingUtils.readByteInt(data));
		if(isServerToClient()) {
			switch(getActionCode()) {
				case 10:
					setReconnectTime(ProcessingUtils.readShortInt(data));
					break;
				case 14:
					setPropertyValueList(ProcessingUtils.readLongString(data, ProcessingConstants.US_ASCII_CHARSET));
					break;
			}
		}
	}
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageTypeName())
			.append(": [messageNumber=").append(getMessageNumber())
			.append("; resultCode=").append(getResultCode())
			.append("; responseMessage=").append(getResponseMessage())
			.append("; actionCode=").append(getActionCode());
		if(isServerToClient()) {
			switch(getActionCode()) {
				case 10:
					sb.append("; reconnectTime=").append(getReconnectTime());
					break;
				case 14:
					sb.append("; propertyValueList=").append(getPropertyValueList());
					break;
			}
		}
		sb.append("]");

		return sb.toString();
	}

	public boolean isServerToClient() {
		return serverToClient;
	}

	public void setServerToClient(boolean serverToClient) {
		this.serverToClient = serverToClient;
	}

	public int getReconnectTime() {
		return reconnectTime;
	}

	public void setReconnectTime(int reconnectTime) {
		this.reconnectTime = reconnectTime;
	}

	public String getPropertyValueList() {
		return propertyValueList;
	}

	public void setPropertyValueList(String propertyValuList) {
		this.propertyValueList = propertyValuList;
	}
}
