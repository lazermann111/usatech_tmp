package com.usatech.test;

import java.nio.ByteBuffer;

import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
/**
 * 2A --> 71h response
 * @author yhe
 *
 */
public abstract class N2AMessageBase extends LegacyMessage {
	private static final Log log = Log.getLog();
	protected long transactionId=System.currentTimeMillis()/1000;
	protected int amount;//US dollar in pennies 3 bytes
	protected int tax;// 2 bytes
	protected int transactionResult;

	public N2AMessageBase(){
		super();
		dataType=0x2A;
		amount=150;
		tax=50;
		transactionResult='S';
	}
	@Override
	public void getParams() throws Exception{
		super.getParams();
		if(previousMessage!=null){
			log.info("Use Previous Message transactionId:"+previousMessage.responseMap.get("transactionId"));
			transactionId=ConvertUtils.getLong(responseMap.get("transactionId"), transactionId);
		}
	}
	public void createMessage2A(ByteBuffer bb){
		ProcessingUtils.writeLongInt(bb,transactionId);
		ProcessingUtils.write3ByteInt(bb,amount);
		ProcessingUtils.writeShortInt(bb,tax);
		ProcessingUtils.writeByteInt(bb,transactionResult);
	}
	
	@Override
	public String readResponseString(ReRixMessage msg)throws InvalidByteValueException{
		return readResponseString71(msg);
	}

	public int getTransactionResult() {
		return transactionResult;
	}

	public void setTransactionResult(int transactionResult) {
		this.transactionResult = transactionResult;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getTax() {
		return tax;
	}

	public void setTax(int tax) {
		this.tax = tax;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
}
