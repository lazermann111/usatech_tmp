package com.usatech.test;

import java.nio.ByteBuffer;

import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.AuthResponseCode60;
import com.usatech.networklayer.ReRixMessage;
/**
 * 5E Auth request v2.0--> 60h response
 * @author yhe
 *
 */
public class N5EMessage extends BaseMessage {
	private static final Log log = Log.getLog();
	protected long transactionId=System.currentTimeMillis()/1000;
	protected int cardType;
	protected int amount;//US dollar in pennies
	protected String creditCardMagstripe;

	public N5EMessage(){
		super();
		dataType=0x5E;
		cardType='C';
		amount=150;
		creditCardMagstripe="0123456789123456";
	}

	@Override
	public ReRixMessage createMessage() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		ProcessingUtils.writeLongInt(bb,transactionId);
		ProcessingUtils.writeByteInt(bb,cardType);
		ProcessingUtils.write3ByteInt(bb,amount);
		ProcessingUtils.writeString(bb,creditCardMagstripe, ProcessingConstants.US_ASCII_CHARSET);
		return writeMessageEnd(bb);
	}

	@Override
	public String readResponseString(ReRixMessage msg)throws InvalidByteValueException{
		ByteBuffer bb=ByteBuffer.wrap(msg.getData());
		StringBuilder sb=readResponseHeader(bb);
		long transactionId=ProcessingUtils.readLongInt(bb);
		printResponse(sb, "TransactionId", transactionId);
		responseMap.put("transactionId", transactionId);
		int transactionResult=ProcessingUtils.readByteInt(bb);
		printResponse(sb, "TransactionResult",AuthResponseCode60.getByValue((byte)transactionResult));
		return sb.toString();
	}
	
	@Override
	public void verifyResponse(){
		long responseTransactionId=ConvertUtils.getLongSafely(responseMap.get("transactionId"), -1);
		if(transactionId!=responseTransactionId){
			log.error("Response transactionId:"+responseTransactionId+" does not match transactionId:"+transactionId);
		}
	}
	
	public int getCardType() {
		return cardType;
	}

	public void setCardType(int cardType) {
		this.cardType = cardType;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getCreditCardMagstripe() {
		return creditCardMagstripe;
	}

	public void setCreditCardMagstripe(String creditCardMagstripe) {
		this.creditCardMagstripe = creditCardMagstripe;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

}
