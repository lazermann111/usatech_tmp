package com.usatech.test;

import java.nio.ByteBuffer;

import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
/**
 * 99 Client Version --> no response
 * @author yhe
 *
 */
public class N99Message extends LegacyMessage {
	protected int clientVersionType;
	protected String versionString;// one user input ended with a line feed

	public N99Message() {
		super();
		dataType=0x99;
		clientVersionType=0;
		versionString="1";
	}
	
	@Override
	public ReRixMessage createMessage() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		ProcessingUtils.writeByteInt(bb, clientVersionType);
		ProcessingUtils.writeString(bb, versionString+"\n", ProcessingConstants.US_ASCII_CHARSET);
		return writeMessageEnd(bb);
	}

	@Override
	public String readResponseString(ReRixMessage msg) throws InvalidByteValueException {
		return "No Response.\n";
	}

	public int getClientVersionType() {
		return clientVersionType;
	}

	public void setClientVersionType(int clientVersionType) {
		this.clientVersionType = clientVersionType;
	}

	public String getVersionString() {
		return versionString;
	}

	public void setVersionString(String versionString) {
		this.versionString = versionString;
	}

}
