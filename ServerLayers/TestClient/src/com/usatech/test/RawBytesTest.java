/**
 *
 */
package com.usatech.test;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.net.SocketFactory;

import simple.io.ByteArrayUtils;
import simple.text.StringUtils;

/**
 * @author Brian S. Krug
 *
 */
public class RawBytesTest {

	/**
	 * @param args
	 * @throws IOException
	 * @throws UnknownHostException
	 */
	public static void main(String[] args) throws UnknownHostException, IOException {
		/*
		String rawBytesHex = "01000E4C442D424C4E4352000006000034";
		//String rawBytesHex = "01000C4C442D424C4E435200000533";
		String host = "localhost"; int port = 3268;
		 */
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String rawBytesHex = args[2];
		Socket socket = SocketFactory.getDefault().createSocket(host, port);
		System.out.print("--> ");
		System.out.println(rawBytesHex);
		byte[] send = ByteArrayUtils.fromHex(rawBytesHex);
		OutputStream out = socket.getOutputStream();
		out.write(send);
		out.flush();
		InputStream in = socket.getInputStream();
		byte[] receive = new byte[100];
		int r;
		System.out.print("<-- ");
		long start = System.currentTimeMillis();
		while((r=in.read(receive)) >= 0) {
			System.out.print(StringUtils.toHex(receive, 0, r));
			if(System.currentTimeMillis() - start > 30 * 1000)
				break;
		}
		System.out.println();
		System.out.println("DONE");
	}

}
