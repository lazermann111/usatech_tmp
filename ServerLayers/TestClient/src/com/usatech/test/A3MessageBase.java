package com.usatech.test;

import java.nio.ByteBuffer;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;

/**
 * A3 Local auth batch v2.2 --> 71h response
 * @author yhe
 *
 */
public abstract class A3MessageBase extends A2MessageBase {
	protected long dateAndTime=System.currentTimeMillis()/1000;
	protected int cardType;
	protected String creditCardMagstripe;
	public A3MessageBase(){
		super();
		dataType=0xA3;
		cardType='C';
		creditCardMagstripe="0123456789123456";
	}
	public void createMessageA3(ByteBuffer bb){
		ProcessingUtils.writeLongInt(bb,transactionId);
		ProcessingUtils.writeLongInt(bb,dateAndTime);
		ProcessingUtils.writeByteInt(bb,cardType);
		ProcessingUtils.writeLongInt(bb,amount);
		ProcessingUtils.writeShortString(bb,creditCardMagstripe, ProcessingConstants.US_ASCII_CHARSET);
		ProcessingUtils.writeByteInt(bb,transactionResult);
	}
	public int getCardType() {
		return cardType;
	}
	public void setCardType(int cardType) {
		this.cardType = cardType;
	}
	public String getCreditCardMagstripe() {
		return creditCardMagstripe;
	}
	public void setCreditCardMagstripe(String creditCardMagstripe) {
		this.creditCardMagstripe = creditCardMagstripe;
	}
	
}
