/**
 *
 */
package com.usatech.test;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import simple.app.AbstractCommandManager;
import simple.app.AppPropertiesConfiguration;
import simple.app.BasicCommandArgument;
import simple.app.Command;
import simple.app.CommandArgument;
import simple.app.CommandManager;
import simple.app.Commander;
import simple.app.InOutCommander;
import simple.app.Main;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.ByteArrayUtils;
import simple.io.Log;
import simple.io.logging.AbstractLog;
import simple.io.logging.SimpleBridge;
import simple.text.StringUtils;

import com.usatech.layers.common.constants.DeviceProperty;
import com.usatech.layers.common.constants.InitializationReason;
import com.usatech.networklayer.ReRixMessage;

/**
 * @author Brian S. Krug
 *
 */
public class TestClient extends Main {
	private static final Log log = Log.getLog();
	protected final CountDownLatch exitSignal = new CountDownLatch(1);
	protected File propertiesFile;
	protected Commander commander;
	protected int outputLevel = SimpleBridge.INFO;
	protected PropertiesConfiguration properties;
	protected int protocolVersion;
	protected String deviceName;
	protected byte[] encryptionKey;

	protected final CommandManager<TestClient> commandManager = new AbstractCommandManager<TestClient>() {
		@Override
		public TestClient getContext() {
			return TestClient.this;
		}

		@Override
		protected void registerDefaultCommands() {
			registerCommand(new Command<TestClient>() {
				public CommandArgument[] getCommandArguments() {
					return null;
				}
				/**
				 * @see simple.app.Command#executeCommand(java.lang.Object, java.io.PrintWriter, java.lang.String[])
				 */
				@Override
				public boolean executeCommand(TestClient commandArgument, PrintWriter out, Object[] arguments) {
					try {
						commandArgument.performCallIn(makeLog(out));
						return true;
					}
					catch (Exception e) {
						return false;
					}
				}
				/**
				 * @see simple.app.Command#getDescription()
				 */
				@Override
				public String getDescription() {
					return "Performs normal call-in to server";
				}
				/**
				 * @see simple.app.Command#getKey()
				 */
				@Override
				public String getKey() {
					return "callIn";
				}
			});
			registerCommand(new Command<TestClient>() {
				protected final CommandArgument[] commandArguments = new CommandArgument[] {
					new BasicCommandArgument("track-data", String.class, "Track data to use", false)
				};
				public CommandArgument[] getCommandArguments() {
					return commandArguments;
				}
				/**
				 * @see simple.app.Command#executeCommand(java.lang.Object, java.io.PrintWriter, java.lang.String[])
				 */
				@Override
				public boolean executeCommand(TestClient commandArgument, PrintWriter out, Object[] arguments) {
					String trackData;
					if(arguments == null || arguments.length == 0 || arguments[0] == null || (trackData=((String)arguments[0]).trim()).length() == 0) {
						out.println("Track Data not provided. Please include it as the first argument");
						return true;
					}
					commandArgument.swipeCard(trackData, makeLog(out));
					return true;
				}
				/**
				 * @see simple.app.Command#getDescription()
				 */
				@Override
				public String getDescription() {
					return "Sends card swipe track data to server for authorization";
				}
				/**
				 * @see simple.app.Command#getKey()
				 */
				@Override
				public String getKey() {
					return "swipe";
				}
			});
			registerCommand(new Command<TestClient>() {
				protected final CommandArgument[] commandArguments = new CommandArgument[] {
					new BasicCommandArgument("track-data", String.class, "Track data to use", false)
				};
				public CommandArgument[] getCommandArguments() {
					return commandArguments;
				}
					/**
				 * @see simple.app.Command#executeCommand(java.lang.Object, java.io.PrintWriter, java.lang.String[])
				 */
				@Override
				public boolean executeCommand(TestClient commandArgument, PrintWriter out, Object[] arguments) {
					String trackData;
					if(arguments == null || arguments.length == 0 || arguments[0] == null || (trackData=((String)arguments[0]).trim()).length() == 0) {
						out.println("Track Data not provided. Please include it as the first argument");
						return true;
					}
					commandArgument.tapRFID(trackData, makeLog(out));
					return true;
				}
				/**
				 * @see simple.app.Command#getDescription()
				 */
				@Override
				public String getDescription() {
					return "Sends RF ID track data to server for authorization";
				}
				/**
				 * @see simple.app.Command#getKey()
				 */
				@Override
				public String getKey() {
					return "tap";
				}
			});
			registerCommand(new Command<TestClient>() {
				protected final CommandArgument[] commandArguments = new CommandArgument[] {
					new BasicCommandArgument("cents", Integer.class, "Number of cents to use", false)
				};
				public CommandArgument[] getCommandArguments() {
					return commandArguments;
				}
					
				/**
				 * @see simple.app.Command#executeCommand(java.lang.Object, java.io.PrintWriter, java.lang.String[])
				 */
				@Override
				public boolean executeCommand(TestClient commandArgument, PrintWriter out, Object[] arguments) {
					if(arguments == null || arguments.length == 0 || arguments[0] == null) {
						out.println("Number of cents not provided. Please include it as the first argument");
						return true;
					}
					int cents;
					try {
						cents = ConvertUtils.getInt(arguments[0]);
					} catch(ConvertException e) {
						out.println("Could not convert '" + arguments[0] + "' to an integer");
						return true;
					}
					if(cents <= 0) {
						out.println("Cents must be positive, not " + cents);
						return true;
					}
					commandArgument.receiveCash(cents, makeLog(out));
					return true;
				}
				/**
				 * @see simple.app.Command#getDescription()
				 */
				@Override
				public String getDescription() {
					return "Puts cash money on the machine (in cents)";
				}
				/**
				 * @see simple.app.Command#getKey()
				 */
				@Override
				public String getKey() {
					return "cash";
				}
			});
			registerCommand(new Command<TestClient>() {
				protected final CommandArgument[] commandArguments = new CommandArgument[] {
					new BasicCommandArgument("productKey", String.class, "Product Key", false)
				};
				public CommandArgument[] getCommandArguments() {
					return commandArguments;
				}
				/**
				 * @see simple.app.Command#executeCommand(java.lang.Object, java.io.PrintWriter, java.lang.String[])
				 */
				@Override
				public boolean executeCommand(TestClient commandArgument, PrintWriter out, Object[] arguments) {
					String productKey;
					if(arguments == null || arguments.length == 0 || arguments[0] == null || (productKey=((String)arguments[0]).trim()).length() == 0) {
						out.println("Product Key not provided. Please include it as the first argument");
						return true;
					}
					commandArgument.vendProduct(productKey, makeLog(out));
					return true;
				}
				/**
				 * @see simple.app.Command#getDescription()
				 */
				@Override
				public String getDescription() {
					return "Vends a product";
				}
				/**
				 * @see simple.app.Command#getKey()
				 */
				@Override
				public String getKey() {
					return "vend";
				}
			});
			registerCommand(new Command<TestClient>() {
				public CommandArgument[] getCommandArguments() {
					return null;
				}
				/**
				 * @see simple.app.Command#executeCommand(java.lang.Object, java.io.PrintWriter, java.lang.String[])
				 */
				@Override
				public boolean executeCommand(TestClient commandArgument, PrintWriter out, Object[] arguments) {
					commandArgument.endTransaction(makeLog(out));
					return true;
				}
				/**
				 * @see simple.app.Command#getDescription()
				 */
				@Override
				public String getDescription() {
					return "End the current transaction";
				}
				/**
				 * @see simple.app.Command#getKey()
				 */
				@Override
				public String getKey() {
					return "end";
				}
			});
		}
	};
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new TestClient().run(args);
	}

	protected Log makeLog(final PrintWriter out) {
		return new AbstractLog() {
			@Override
			protected int getDebugLevel() {
				return SimpleBridge.DEBUG;
			}
			@Override
			protected int getErrorLevel() {
				return SimpleBridge.ERROR;
			}
			@Override
			protected int getFatalLevel() {
				return SimpleBridge.FATAL;
			}
			@Override
			protected int getInfoLevel() {
				return SimpleBridge.INFO;
			}
			@Override
			protected int getTraceLevel() {
				return SimpleBridge.TRACE;
			}
			@Override
			protected int getWarnLevel() {
				return SimpleBridge.WARN;
			}
			@Override
			protected int getEffectiveLevel() {
				return getOutputLevel();
			}

			@Override
			protected void logMessage(int level, Object msg, Throwable exception) {
				out.print(ConvertUtils.formatObject(new Date(), "DATE:HH:mm:ss:SSSS"));
				out.print(' ');
				out.print(SimpleBridge.getLevelName(level));
				out.print(": ");
				out.println(msg);
				if(exception != null)
					out.println(StringUtils.exceptionToString(exception));
			}
		};
	}

	/**
	 *
	 */
	public void performCallIn(Log out) throws Exception {
		out.info("Start Call In");
		// check if initialized
		String deviceName = getPropertyValue(DeviceProperty.DEVICE_GUID.getValue(), false);
		String encryptionKey = getPropertyValue(DeviceProperty.ENCRYPTION_KEY.getValue(), false);
		long messageId = ConvertUtils.getLongSafely(getPropertyValue(DeviceProperty.MESSAGE_ID.getValue(), false), System.currentTimeMillis() / 1000);
		byte messageNumber = 0;
		TwoWay comm;
		BaseMessage message = null;
		if(deviceName == null || deviceName.length() == 0 || encryptionKey == null || encryptionKey.length() == 0) {
			//use defaults
			deviceName = getDefaultPropertyValue(DeviceProperty.DEVICE_GUID.getValue());
			encryptionKey = getDefaultPropertyValue(DeviceProperty.ENCRYPTION_KEY.getValue());
			String deviceSerialCd = "EE100000123";//getPropertyValue(propertyIndex, useDefault)
			out.info("Initializing device '" + deviceSerialCd + "'");
			comm = new TwoWay(7, deviceName, ByteArrayUtils.fromHex(encryptionKey));
			//send init
			C0Message initMessage = new C0Message();
			//initMessage.setDeviceFirmwareVersion(deviceFirmwareVersion);
			//initMessage.setDeviceInfo(deviceInfo);
			initMessage.setDeviceSerialNum(deviceSerialCd);
			initMessage.setDeviceType(13);
			initMessage.setMessageId(messageId++);
			initMessage.setMessageNumber(messageNumber++ & 0xFF);
			initMessage.setPropertyListVersion(0);
			//initMessage.setProtocolComplianceRevision(protocolComplianceRevision);
			initMessage.setReasonCode(InitializationReason.NEW_DEVICE.getValue());
			//initMessage.setTerminalInfo(terminalInfo);
			message = initMessage;
		} else {
			comm = new TwoWay(7, deviceName, ByteArrayUtils.fromHex(encryptionKey));

		}
		String host = getPropertyValue(20, true);
		int port = ConvertUtils.getIntSafely(getPropertyValue(24, true), 0);
		comm.openSession(host, port);
		try {
			while(true) {
				ReRixMessage response = comm.sendMessage(message.createMessage());
				if(response == null) {
					out.info("Server dropped communication; Exiting session");
					break;
				}
				setPropertyValue(DeviceProperty.MESSAGE_ID.getValue(), messageId);

				// parse response
				switch(response.getData()[1]) {

				}
			}
		} finally {
			comm.closeSession();
		}
	}

	public void vendProduct(String productKey, Log out) {

	}

	public void swipeCard(String trackData, Log out) {

	}
	public void tapRFID(String trackData, Log out) {

	}
	public void receiveCash(int cents, Log out) {

	}
	public void endTransaction(Log out) {

	}
	@Override
	public void run(String[] args) {
		printInvocation(args);
		Map<String,Object> argMap;
		try {
			argMap = parseArguments(args);
		} catch(IOException e) {
			finishAndExit(e.getMessage(), 100, true, null);
			return;
		}
		String fileName = (String) argMap.get("properties-file");
		File file = locatePropertiesFile(fileName);
		if(file == null) {
			finishAndExit("Could not find properties file '" + fileName + "'", 110, true, null);
			return;
		}
		System.out.println("Using properties file '" + file.getAbsolutePath() + "'");
		try {
			setPropertiesFile(file);
		} catch(IOException e) {
			finishAndExit("Could not load properties file '" + fileName + "'", 120, true, e);
			return;
		}
		commander = new InOutCommander();
		commander.setPrompt(commandManager.getHelpPrompt());
		commander.start(commandManager);
		awaitExit();
	}

	protected void awaitExit() {
		log.debug("Awaiting Exit...");
        try {
			exitSignal.await();
		} catch(InterruptedException e) {
			log.warn("Thread interrupted; Exitting...", e);
		}
	}
	protected File locatePropertiesFile(String propertiesFile) {
		File file = new File(propertiesFile);
		if(file.isAbsolute()) {
			if(!file.canRead()) {
				finishAndExit(file.exists() ? "Cannot read properties file '" + file.getAbsolutePath() + "'" : "Properties file '" + file.getAbsolutePath() + "' does not exist", 105, true, null);
				return null;
			}
		} else {
			//looks first in user home, then in classpath, then in current dir
			String home = System.getProperty("user.home");
			if(home != null) {
				file = new File(home, propertiesFile);
				if(file.canRead())
					return file;
			}
			URL url = getClass().getClassLoader().getResource(propertiesFile);
			if(url != null && url.getProtocol().equals("file"))
				try {
					return new File(url.toURI());
				} catch(URISyntaxException e) {
					log.warn("While getting properties file", e);
				}
			file = new File(propertiesFile);
			if(file.canRead())
				return file;
		}
		return null;
	}
	@Override
	protected void registerDefaultCommandLineArguments() {
		registerCommandLineSwitch('p', "propertiesFile", true, true, "properties-file", "The properties file to use");
	}

	public File getPropertiesFile() {
		return propertiesFile;
	}

	public void setPropertiesFile(File propertiesFile) throws IOException {
		this.propertiesFile = propertiesFile;
		this.properties = loadConfig(propertiesFile);
	}

	public int getOutputLevel() {
		return outputLevel;
	}

	public void setOutputLevel(int outputLevel) {
		this.outputLevel = outputLevel;
	}

	protected PropertiesConfiguration loadConfig(File file) throws IOException {
		AppPropertiesConfiguration pc = new AppPropertiesConfiguration();
		try {
			pc.load(file);
		} catch(ConfigurationException e) {
			if(e.getCause() instanceof IOException)
				throw (IOException)e.getCause();
			else {
				IOException ioe = new IOException(e.getMessage());
				ioe.initCause(e);
				throw ioe;
			}
		}
		pc.setAutoSave(true);
		return pc;
	}
	/**
	 * @param value
	 * @return
	 */
	protected String getDefaultPropertyValue(int propertyIndex) {
		return properties.getString("default." + propertyIndex);
	}

	/**
	 * @param value
	 * @param b
	 * @return
	 */
	protected String getPropertyValue(int propertyIndex, boolean useDefault) {
		String value = properties.getString("instance." + propertyIndex);
		if(value == null && useDefault)
			value = getDefaultPropertyValue(propertyIndex);
		return value;
	}

	protected void setPropertyValue(int propertyIndex, Object value) {
		properties.setProperty("instance." + propertyIndex, value);
	}
}
