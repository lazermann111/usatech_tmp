package com.usatech.test;

import java.nio.ByteBuffer;

import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.AuthResponseCodeA1;
import com.usatech.layers.common.constants.BatchTransactionResult71;
import com.usatech.networklayer.ReRixMessage;

public abstract class LegacyMessage extends BaseMessage {
	
	public LegacyMessage() {
		super();
	}
	/**
	 * 71 response
	 * @param msg
	 * @return
	 * @throws InvalidByteValueException
	 */
	public String readResponseString71(ReRixMessage msg)throws InvalidByteValueException{
		ByteBuffer bb=ByteBuffer.wrap(msg.getData());
		StringBuilder sb=readResponseHeader(bb);
		printResponse(sb, "TransactionId", ProcessingUtils.readLongInt(bb));
		int transactionResult=ProcessingUtils.readByteInt(bb);
		printResponse(sb, "TransactionResult",BatchTransactionResult71.getByValue((byte)transactionResult));
		return sb.toString();
	}
	/**
	 * 2F response
	 * @param msg
	 * @return
	 * @throws InvalidByteValueException
	 */
	public String readResponseString2F(ReRixMessage msg)throws InvalidByteValueException{
		ByteBuffer bb=ByteBuffer.wrap(msg.getData());
		StringBuilder sb=readResponseHeader(bb);
		printResponse(sb, "AckedMessageNumber", ProcessingUtils.readByteInt(bb));
		return sb.toString();
	}
	/**
	 * 95 Cash Sale Detail Ack v2.0 spec wrong, returns hex
	 * @param msg
	 * @return
	 * @throws InvalidByteValueException
	 */
	public String readResponseString95(ReRixMessage msg)throws InvalidByteValueException{
		ByteBuffer bb=ByteBuffer.wrap(msg.getData());
		StringBuilder sb=readResponseHeader(bb);
		printResponse(sb, "CashSaleBatchId", TestClientUtil.getHexToDecimalFromByteBuffer(bb));
		return sb.toString();
	}
	
	public String readResponseStringA1(ReRixMessage msg)throws InvalidByteValueException{
		ByteBuffer bb=ByteBuffer.wrap(msg.getData());
		StringBuilder sb=readResponseHeader(bb);
		long transactionId=ProcessingUtils.readLongInt(bb);
		printResponse(sb, "TransactionId", transactionId);
		responseMap.put("transactionId", transactionId);
		int transactionResult=ProcessingUtils.readByteInt(bb);
		printResponse(sb, "TransactionResult",AuthResponseCodeA1.getByValue((byte)transactionResult));
		if(transactionResult==2){
			//only conditional approved will have this value
			printResponse(sb, "ApprovedAmount", ProcessingUtils.readLongInt(bb));
		}
		return sb.toString();
	}
	
	public String readResponseString8F(ReRixMessage msg, int uniqueDeviceSerialNumberLen)throws InvalidByteValueException{
		ByteBuffer bb=ByteBuffer.wrap(msg.getData());
		StringBuilder sb=readResponseHeader(bb);
		printResponse(sb, "EncryptionKey", new String(ProcessingUtils.readBytes(bb, 16)));
		printResponse(sb, "SerialNumberToSetInClient", new String(ProcessingUtils.readBytes(bb, uniqueDeviceSerialNumberLen)));
		return sb.toString();
	}
}
