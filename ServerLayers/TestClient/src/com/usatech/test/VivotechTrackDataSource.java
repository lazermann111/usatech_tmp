package com.usatech.test;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.constants.EntryType;

public enum VivotechTrackDataSource {
	VIVOTECH_MAGSTRIPE_SOURCE((byte)0x0c, EntryType.SWIPE),
	VIVOTECH_CONTACTLESS_SOURCE((byte)0x00, EntryType.CONTACTLESS),
	VIVOTECH_CONTACT_SOURCE((byte)0x01, EntryType.EMV_CONTACT),
	VIVOTECH_FALLBACK_MAGSTRIPE((byte)0x80, EntryType.SWIPE);
    
	private final byte value;
	private final EntryType entryType;

	private VivotechTrackDataSource(byte value, EntryType entryType) {
		this.value = value;
		this.entryType = entryType;
	}
	
    public byte getValue() {
        return value;
    }
    public EntryType getEntryType() {
    	return entryType;
    }
	
    protected final static EnumByteValueLookup<VivotechTrackDataSource> lookup = new EnumByteValueLookup<VivotechTrackDataSource>(VivotechTrackDataSource.class);
    public static VivotechTrackDataSource getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
	
}
