package com.usatech.test;

import java.util.Arrays;

import simple.io.ByteArrayUtils;
import simple.lang.InvalidValueException;
import simple.text.StringUtils;


public class EMVCAKeyComponents {
	private byte [] aid;
	private byte [] index;
	private byte [] modulus;
	private byte [] exponent;
	private byte [] checksum;
	
	public enum RID {
		AMEX("A000000025"),
		CHASE_NET("A000000003"),
		DISCOVER("A000000152"),
		DISCOVER_ZIP("A000000324"),
		DEBIT_ALLIANCE_NETWORK("A000000620"),
		INTERAC("A000000277"),
		JCB("A000000065"),
		MASTERCARD("A000000004"),
		VISA("A000000003"),
		VISA_DEBIT("A000000098")
		;
		
		private byte [] rid;
		RID(String rid) {
			this.rid = ByteArrayUtils.fromHex(rid);
		}
		public byte [] getRid() {
			return rid;
		}
		
		public static RID getByValue(byte []rid) throws InvalidValueException {
			for (RID tmpRid: RID.values()) {
				if (Arrays.equals(tmpRid.getRid(), rid)) {
					return tmpRid;
				}
			}
			throw new InvalidValueException(StringUtils.toHex(rid));
		}
	}
	
	public EMVCAKeyComponents(String aid, String index, String modulus, String exponent, String checksum) {
		this.aid = ByteArrayUtils.fromHex(aid);
		this.index = ByteArrayUtils.fromHex(index);
		this.modulus = ByteArrayUtils.fromHex(modulus);
		this.exponent = ByteArrayUtils.fromHex(exponent);
		this.checksum = ByteArrayUtils.fromHex(checksum);
	}

	public byte getHashAlgorithmIndicator() {
		return 0x01;
	}
	public byte getPublicKeyAlgorithmIndicator() {
		return 0x01;
	}
	
	public byte[] getAid() {
		return aid;
	}

	public byte [] getIndex() {
		return index;
	}

	public byte[] getModulus() {
		return modulus;
	}

	public byte[] getExponent() {
		return exponent;
	}

	public byte[] getChecksum() {
		return checksum;
	}
	
	public static final String FILENAME = "/Users/jscarpaci/Documents/USATech/emv/emv-parameter-download-samples/emv-param-dl-2016-01-07-10-13-01-162";

	@Override
	public String toString() {
		return diagString();
	}
	
	public String diagString() {
		String name = "";
		try {
			name = RID.getByValue(getAid()).toString();
		} catch (InvalidValueException e) {
			name = "Unknown: " + StringUtils.toHex(getAid());
		}
		String result = "Name: " + name + ", AID: " + StringUtils.toHex(getAid()) + ", Index: " + StringUtils.toHex(getIndex());
		return result;
	}
}

