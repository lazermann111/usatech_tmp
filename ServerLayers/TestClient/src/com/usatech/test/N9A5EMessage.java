package com.usatech.test;

import java.nio.ByteBuffer;
import java.util.ArrayList;

import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
/**
 * 9A5E Actual Purchase Network Auth Batch --> 71h
 * @author yhe
 *
 */
public class N9A5EMessage extends ESudsMessage {
	protected long transactionId=System.currentTimeMillis()/1000;
	protected int numberOfLineItems;
	protected ArrayList<ESudsLineItem> lineItem=new ArrayList<ESudsLineItem>(5);
	public N9A5EMessage() {
		super();
		dataType2=0x5E;
		numberOfLineItems=1;
	}

	@Override
	public void getParams()throws Exception{
		super.getParams();
		for(int i=0; i<numberOfLineItems; i++){
			ESudsLineItem lItem=new ESudsLineItem(userInputTokens);
			lItem.getParams();
			lineItem.add(lItem);
		}
	}
	
	@Override
	public ReRixMessage createMessage() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		ProcessingUtils.writeLongInt(bb, transactionId);
		ProcessingUtils.writeByteInt(bb, numberOfLineItems);
		for(ESudsLineItem lItem: lineItem){
			lItem.writeMessage(bb);
		}
		return writeMessageEnd(bb);
	}

	@Override
	public String readResponseString(ReRixMessage msg) throws InvalidByteValueException {
		return readResponseString71(msg);
	}
	
	public int getNumberOfLineItems() {
		return numberOfLineItems;
	}

	public void setNumberOfLineItems(int numberOfLineItems) {
		this.numberOfLineItems = numberOfLineItems;
	}

	public ArrayList<ESudsLineItem> getLineItem() {
		return lineItem;
	}

	public void setLineItem(ArrayList<ESudsLineItem> lineItem) {
		this.lineItem = lineItem;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

}
