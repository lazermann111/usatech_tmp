package com.usatech.test;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.InvalidKeyException;

import simple.io.ByteArrayUtils;
import simple.io.Log;
import simple.text.StringUtils;

import com.usatech.networklayer.NetworkLayerException;
import com.usatech.networklayer.ReRixMessage;
import com.usatech.util.crypto.CRCException;
import com.usatech.util.crypto.Crypt;
import com.usatech.util.crypto.CryptException;
import com.usatech.util.crypto.CryptUtil;
/**
 * Class to start stop connection and send message to Netlayer
 * @author yhe
 *
 */
public class TwoWay {
	private static final Log log = Log.getLog();
	protected Socket socket;
	protected DataInputStream input;
	protected DataOutputStream output;
	protected int protocolVersion;
	protected String deviceName;
	protected byte[] encryptionKey;

	protected static Crypt[] crypts = new Crypt[10];

	static {
		crypts[7] = CryptUtil.getCrypt("AES_CBC_CRC16");
	}

	public TwoWay(){
		this(7);
	}
	public TwoWay(int protocolVersion){
		this.protocolVersion = protocolVersion;
	}
	public TwoWay(int protocolVersion, String deviceName, byte[] encryptionKey){
		this.protocolVersion = protocolVersion;
		this.deviceName = deviceName;
		this.encryptionKey = encryptionKey;
	}
	public void openSession(String host, int port) throws UnknownHostException, IOException {
		closeSession();
		socket = new Socket(host, port);
		socket.setSoTimeout(10000);

		input = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
		output = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
	}
	public void closeSession() throws IOException {
		if(input != null) {
			input.close();
			input = null;
		}
		if(output != null) {
			output.close();
			output = null;
		}
		if(socket != null) {
			socket.close();
			socket = null;
		}

	}

	public ReRixMessage sendMessage(ReRixMessage msg) throws IOException, NetworkLayerException {
		return new ReRixMessage(sendMessage(msg.getData()), deviceName);
	}

	public byte[] sendMessage(byte[] data) throws IOException, NetworkLayerException  {
		log.info("<- " + StringUtils.toHex(data));
		transmit((byte)protocolVersion, deviceName, data, encryptionKey, crypts[protocolVersion], output);

		output.flush();
		int version;
        switch(protocolVersion) {
			case 2:
			case 4:
				version = protocolVersion;
				break;
			default:
				version = input.read();
		}
        byte[] reply = receive(deviceName, encryptionKey, crypts[version], input);
		if(reply == null)
			log.info("Server dropped communication");
		else
			log.info("-> " + StringUtils.toHex(reply));
		return reply;
	}
	protected int transmit(byte protocol, String deviceName, byte[] data, byte[] encryptionKey, Crypt crypt, DataOutputStream out) throws IOException, NetworkLayerException {
        byte[] encrypted = encryptData(data, encryptionKey, crypt);
        byte[] array = new byte[encrypted.length + 11];

        array[0] = protocol;

        ByteArrayUtils.writeUnsignedShort(array, 1, encrypted.length + 8);

        byte[] deviceNameBytes = deviceName.getBytes();
        System.arraycopy(deviceNameBytes, 0, array, 3, Math.min(8, deviceNameBytes.length)); // this will pad with 0x00 if length < 8
        System.arraycopy(encrypted, 0, array, 11, encrypted.length);

        if (log.isDebugEnabled())
            log.debug("transmit " + deviceName + ": Header: " + StringUtils.toHex(array, 0, 11));

        out.write(array);
        return array.length;
    }

	protected byte[] encryptData(byte[] data, byte[] encryptionKey, Crypt crypt) throws NetworkLayerException {
		if (log.isDebugEnabled())
			log.debug("Unencrypted: " + StringUtils.toHex(data));
		byte[] encrypted;
		if(crypt != null) {
			try {
				encrypted = crypt.encrypt(data, encryptionKey);
	        } catch(InvalidKeyException e) {
	                throw new NetworkLayerException("Failed to encrypt message (" + StringUtils.toHex(data) + ") : Invalid Key: " + StringUtils.toHex(encryptionKey), e);
	        } catch(CRCException e) {
	                throw new NetworkLayerException("Failed to encrypt message (" + StringUtils.toHex(data) + ") : CRC Check Failed!", e);
			} catch(CryptException e) {
	                throw new NetworkLayerException("Failed to encrypt message (" + StringUtils.toHex(data) + ") : Decryption Algorithm Failure!", e);
			} catch(Exception e) {
	                throw new NetworkLayerException("Caught unexpected exception encrypting message (" + StringUtils.toHex(data) + ") : " + e, e);
	        }
		} else {
			encrypted = data;
		}

		if (log.isDebugEnabled())
	        log.debug("Encrypted: " + StringUtils.toHex(encrypted));

	    return encrypted;
    }

	protected byte[] receive(String deviceName, byte[] encryptionKey, Crypt crypt, DataInputStream in) throws IOException, NetworkLayerException {
		byte[] lengthArray = new byte[2];
	    try {
	        in.readFully(lengthArray);
	    } catch (EOFException e) {
	        throw new NetworkLayerException("End of stream encountered while reading length bytes.");
	    }

	    int expectedLength = ByteArrayUtils.readUnsignedShort(lengthArray, 0);

	    byte[] encrypted = new byte[expectedLength];
	    try {
	        in.readFully(encrypted);
	    } catch (EOFException e) {
	        throw new NetworkLayerException("End of stream encountered while reading message bytes.");
	    }

	    return decryptData(encrypted, encryptionKey, crypt);
	}
	protected byte[] decryptData(byte[] data, byte[] encryptionKey, Crypt crypt) throws NetworkLayerException {
		if (log.isDebugEnabled())
			log.debug("Undecrypted: " + StringUtils.toHex(data));
		byte[] decrypted;
		if(crypt != null) {
			try {
				decrypted = crypt.decrypt(data, encryptionKey);
	        } catch(InvalidKeyException e) {
	                throw new NetworkLayerException("Failed to decrypt message (" + StringUtils.toHex(data) + ") : Invalid Key: " + StringUtils.toHex(encryptionKey), e);
	        } catch(CRCException e) {
	                throw new NetworkLayerException("Failed to decrypt message (" + StringUtils.toHex(data) + ") : CRC Check Failed!", e);
			} catch(CryptException e) {
	                throw new NetworkLayerException("Failed to decrypt message (" + StringUtils.toHex(data) + ") : Decryption Algorithm Failure!", e);
			} catch(Exception e) {
	                throw new NetworkLayerException("Caught unexpected exception decrypting message (" + StringUtils.toHex(data) + ") : " + e, e);
	        }
		} else {
			decrypted = data;
		}

		if (log.isDebugEnabled())
	        log.debug("Decrypted: " + StringUtils.toHex(decrypted));

	    return decrypted;
    }
	public int getProtocolVersion() {
		return protocolVersion;
	}
	public void setProtocolVersion(int protocolVersion) {
		this.protocolVersion = protocolVersion;
	}
	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	public byte[] getEncryptionKey() {
		return encryptionKey;
	}
	public void setEncryptionKey(byte[] encryptionKey) {
		this.encryptionKey = encryptionKey;
	}
}
