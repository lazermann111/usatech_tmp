package com.usatech.test;
import java.io.IOException;
import java.nio.ByteBuffer;

import simple.io.Log;
import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.GenericResponseResultCode;
import com.usatech.layers.common.constants.GenericResponseServerActionCode;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.networklayer.ReRixMessage;
/**
 * Base message.
 * @author yhe
 *
 */
public abstract class V4Message extends BaseMessage {
	private static final Log log = Log.getLog();
	protected long lastMessageId = 0;

	public V4Message(){
		super();
	}

	@Override
	public void writeMessageHeader(ByteBuffer bb){
		ProcessingUtils.writeByteInt(bb, messageNumber);
		ProcessingUtils.writeByteInt(bb, dataType);
		ProcessingUtils.writeLongInt(bb, messageId);
	}
	@Override
	public StringBuilder readResponseHeader(ByteBuffer bb)throws InvalidByteValueException{
		StringBuilder sb=new StringBuilder();
		printResponse(sb, "MessageNumber", ProcessingUtils.readByteInt(bb));
		byte dataTypeByte=(byte)ProcessingUtils.readByteInt(bb);
		printResponse(sb, "DataType", MessageType.getByValue(dataTypeByte));
		printResponse(sb, "MessageId", ProcessingUtils.readLongInt(bb));
		return sb;
	}

	@Override
	public String readResponseString(ReRixMessage msg)throws InvalidByteValueException{
		ByteBuffer bb=ByteBuffer.wrap(msg.getData());
		StringBuilder sb=readResponseHeader(bb);
		printResponse(sb, "ResultCode", GenericResponseResultCode.getByValue((byte)ProcessingUtils.readByteInt(bb)));
		printResponse(sb, "ResponseMessage", ProcessingUtils.readShortString(bb, ProcessingConstants.US_ASCII_CHARSET));
		int actionCode=ProcessingUtils.readByteInt(bb);
		printResponse(sb, "ActionCode", GenericResponseServerActionCode.getByValue((byte)actionCode));
		if(actionCode==10){
			printResponse(sb, "ReconnectTime", ProcessingUtils.readShortInt(bb));
		}else if(actionCode==14){
			printResponse(sb, "PropertyValueList", ProcessingUtils.readLongString(bb, ProcessingConstants.US_ASCII_CHARSET));
		}
		return sb.toString();
	}

	@Override
	public void readResponse(ReRixMessage msg)throws InvalidByteValueException{
		log.info(readResponseString(msg));
	}
	@Override
	public long getMessageId() {
		return messageId;
	}
	@Override
	public void setMessageId(long messageId) {
		this.messageId = messageId;
	}
	public long getLastMessageId() {
		return lastMessageId;
	}
	public void setLastMessageId(long lastMessageId) {
		this.lastMessageId = lastMessageId;
	}

	/**
	 * @throws IOException
	 * @see com.usatech.test.BaseMessage#read(ByteBuffer)
	 */
	@Override
	protected void read(ByteBuffer data) throws IOException, InvalidByteValueException {
		super.read(data);
		setMessageId(ProcessingUtils.readLongInt(data));
	}
}
