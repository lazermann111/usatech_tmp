package com.usatech.test;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.StringTokenizer;

import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.GenericResponseResultCode;
import com.usatech.layers.common.constants.GenericResponseServerActionCode;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.networklayer.ReRixMessage;
/**
 * Generic Request CA --> C8, C7, CB,
 * @author yhe
 *
 */
public class CAMessage extends V4Message {
	protected int requestType;
	protected SubMessage request;
	protected int tempRequestType=0;

	// update status request
	protected class GenericRequest0 extends SubMessage{
		protected int statusCodeBitmap;
		protected ArrayList<String> statusCode=new ArrayList<String>(5);
		public GenericRequest0(StringTokenizer userInputTokens){
			super(userInputTokens);
			statusCodeBitmap=0;
			statusCode.add("Sales or Settlement message pending");
			statusCode.add("Events pending");
			statusCode.add("Client outbound data pending");
			statusCode.add("Client inbound data pending");
			statusCode.add("Component ID pending");
		}
		@Override
		public void getParams() throws Exception{
			statusCodeBitmap=CommandLineUtil.getBitMapValue(statusCode, userInputTokens);
		}
		@Override
		public void writeMessage(ByteBuffer bb){
			ProcessingUtils.writeShortInt(bb,statusCodeBitmap);
		}
		public int getStatusCodeBitmap() {
			return statusCodeBitmap;
		}
		public void setStatusCodeBitmap(int statusCodeBitmap) {
			this.statusCodeBitmap = statusCodeBitmap;
		}
	}

	// file transfer request
	protected class GenericRequest1 extends SubMessage{
		protected int fileType;
		protected String fileName;
		public GenericRequest1(StringTokenizer userInputTokens){
			super(userInputTokens);
			fileType=0;
			fileName="testCA.txt";
		}

		@Override
		public void writeMessage(ByteBuffer bb){
			ProcessingUtils.writeShortInt(bb,fileType);
			ProcessingUtils.writeShortString(bb,fileName, ProcessingConstants.US_ASCII_CHARSET);
		}
		public int getFileType() {
			return fileType;
		}
		public void setFileType(int fileType) {
			this.fileType = fileType;
		}
		public String getFileName() {
			return fileName;
		}
		public void setFileName(String fileName) {
			this.fileName = fileName;
		}
	}
	//property list request
	protected class GenericRequest2 extends SubMessage{
		protected String propertyListRequest;
		public GenericRequest2(StringTokenizer userInputTokens){
			super(userInputTokens);
			propertyListRequest="Test client property list request";
		}
		@Override
		public void writeMessage(ByteBuffer bb){
			ProcessingUtils.writeLongString(bb,propertyListRequest, ProcessingConstants.US_ASCII_CHARSET);
		}
		public String getPropertyListRequest() {
			return propertyListRequest;
		}
		public void setPropertyListRequest(String propertyListRequest) {
			this.propertyListRequest = propertyListRequest;
		}
	}
	//property List
	protected class GenericRequest3 extends SubMessage{
		protected String propertyList;
		public GenericRequest3(StringTokenizer userInputTokens){
			super(userInputTokens);
			propertyList="Test client property list";
		}
		@Override
		public void writeMessage(ByteBuffer bb){
			ProcessingUtils.writeLongString(bb,propertyList, ProcessingConstants.US_ASCII_CHARSET);
		}
		public String getPropertyList() {
			return propertyList;
		}
		public void setPropertyList(String propertyList) {
			this.propertyList = propertyList;
		}
	}
	public CAMessage() {
		super();
		dataType=0xCA;
		requestType=tempRequestType;
		request=new GenericRequest0(userInputTokens);
	}

	@Override
	public void getParams() throws Exception{
		super.getParams();
		if(requestType==0){
			request=new GenericRequest0(userInputTokens);
		}else if (requestType==1){
			request=new GenericRequest1(userInputTokens);
		}else if(requestType==2){
			request=new GenericRequest2(userInputTokens);
		}else if (requestType==3){
			request=new GenericRequest3(userInputTokens);
		}
		if(request!=null){
			request.getParams();
		}

	}

	@Override
	public ReRixMessage createMessage() {
		int extraLen=0;
		if(requestType==0){
			extraLen=2;
		}else if(requestType==1){
			extraLen=258;
		}else if(requestType==2){
			String temp=((GenericRequest2)request).propertyListRequest;
			extraLen=temp==null?0:temp.length();
		}else if(requestType==3){
			String temp=((GenericRequest3)request).propertyList;
			extraLen=temp==null?0:temp.length();
		}
		ByteBuffer bb=ByteBuffer.allocate(10+extraLen);
		writeMessageHeader(bb);
		ProcessingUtils.writeByteInt(bb,requestType);
		if(request!=null){
			request.writeMessage(bb);
		}else{
			ProcessingUtils.writeShortString(bb,null, ProcessingConstants.US_ASCII_CHARSET);
		}
		return writeMessageEnd(bb);
	}

	@Override
	public String readResponseString(ReRixMessage msg)throws InvalidByteValueException{
		ByteBuffer bb=ByteBuffer.wrap(msg.getData());
		StringBuilder sb=new StringBuilder();
		printResponse(sb, "MessageNumber", ProcessingUtils.readByteInt(bb));
		int dataType=ProcessingUtils.readByteInt(bb);
		printResponse(sb, "DataType", MessageType.getByValue((byte)dataType));
		printResponse(sb, "MessageId", ProcessingUtils.readLongInt(bb));
		//CB
		if(dataType==0xCB){
			printResponse(sb, "ResultCode", GenericResponseResultCode.getByValue(bb.get()));
			printResponse(sb, "ResponseMessage", ProcessingUtils.readShortString(bb, ProcessingConstants.US_ASCII_CHARSET));
			GenericResponseServerActionCode actionCode = GenericResponseServerActionCode.getByValue(bb.get());
			printResponse(sb, "ActionCode", actionCode);
			switch(actionCode) {
				case DISCONNECT:
					printResponse(sb, "Reconnect Time", ProcessingUtils.readShortInt(bb));
					break;
				case PROCESS_PROP_LIST:
					printResponse(sb, "Property Value List", ProcessingUtils.readLongString(bb, ProcessingConstants.US_ASCII_CHARSET));
					break;
			}
		}else if(dataType==0xC8){
			//C8 file transfer start
			printResponse(sb, "TimeOfCreation", ProcessingUtils.readTimestamp(bb).getTime());
			printResponse(sb, "EventId", ProcessingUtils.readLongInt(bb));
			printResponse(sb, "FilesRemaining", ProcessingUtils.readByteInt(bb));
			printResponse(sb, "NumberOfBlocksInFileTransfer", ProcessingUtils.readShortInt(bb));
			printResponse(sb, "TotalNumberOfBytesInFileTransfer", ProcessingUtils.readLongInt(bb));
			printResponse(sb, "FileType", ProcessingUtils.readShortInt(bb));
			printResponse(sb, "CRCType", ProcessingUtils.readByteInt(bb));
			printResponse(sb, "CRC", ProcessingUtils.readShortInt(bb));
			printResponse(sb, "FileName", ProcessingUtils.readShortString(bb, ProcessingConstants.US_ASCII_CHARSET));
		}else if(dataType==0xC7){
			//short file transfer
			printResponse(sb, "TimeOfCreation", ProcessingUtils.readTimestamp(bb).getTime());
			printResponse(sb, "EventId", ProcessingUtils.readLongInt(bb));
			printResponse(sb, "FilesRemaining", ProcessingUtils.readByteInt(bb));
			printResponse(sb, "FileType", ProcessingUtils.readShortInt(bb));
			printResponse(sb, "FileName", ProcessingUtils.readShortString(bb, ProcessingConstants.US_ASCII_CHARSET));
			String payLoad=ProcessingUtils.readLongString(bb, ProcessingConstants.US_ASCII_CHARSET);
			int len=(payLoad==null?0:payLoad.length());
			printResponse(sb, "FileLength", len);
		}else if(dataType==0xCA){
			int requestType=ProcessingUtils.readByteInt(bb);
			printResponse(sb, "RequestType", requestType);
			if(requestType==0){
				printResponse(sb, "StatusCodeBitmap", ProcessingUtils.readShortInt(bb));
			}else if(requestType==1){
				printResponse(sb, "FileType", ProcessingUtils.readShortInt(bb));
				printResponse(sb, "FileName", ProcessingUtils.readShortString(bb, ProcessingConstants.US_ASCII_CHARSET));
			}else if(requestType==2){
				printResponse(sb, "PropertyListRequest", ProcessingUtils.readLongString(bb, ProcessingConstants.US_ASCII_CHARSET));
			}else if(requestType==4){
				printResponse(sb, "ExternalFileTransferRequest", ProcessingUtils.readByteInt(bb));
				printResponse(sb, "ServerIPAddress", ProcessingUtils.readLongInt(bb));
				printResponse(sb, "ServerPortNumber", ProcessingUtils.readShortInt(bb));
				printResponse(sb, "ServerLoginName", ProcessingUtils.readShortString(bb, ProcessingConstants.US_ASCII_CHARSET));
				printResponse(sb, "ServerPassword", ProcessingUtils.readShortString(bb, ProcessingConstants.US_ASCII_CHARSET));
				printResponse(sb, "FilePath/NameOnServer", ProcessingUtils.readShortString(bb, ProcessingConstants.US_ASCII_CHARSET));
				printResponse(sb, "FileFormat", ProcessingUtils.readByteInt(bb));
				printResponse(sb, "FileEncapsulation", ProcessingUtils.readByteInt(bb));
				int crcType=ProcessingUtils.readByteInt(bb);
				printResponse(sb, "CRCType", crcType);
				if(crcType==0x00){
					printResponse(sb, "CRCValue", ProcessingUtils.readShortInt(bb));
				}else{
					printResponse(sb, "CRCValue", ProcessingUtils.readLongInt(bb));
				}
				printResponse(sb, "EncryptionKey", ProcessingUtils.readShortString(bb, ProcessingConstants.US_ASCII_CHARSET));
				printResponse(sb, "TransferTimeWindow-StartTime", ProcessingUtils.readLongInt(bb));
				printResponse(sb, "TransferTimeWindow-EndTime", ProcessingUtils.readLongInt(bb));
				printResponse(sb, "NumberOfRetries", ProcessingUtils.readByteInt(bb));
				printResponse(sb, "RecommendedMinimumRetryInterval(Minutes)", ProcessingUtils.readByteInt(bb));
			}

		}else{
			printResponse(sb,"Unknown dataType", "");
		}

		return sb.toString();
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception{
		ClientConnectUtil conn=new ClientConnectUtil();
		conn.setProtocol(7);
		conn.startCommunication();
		CAMessage sMsg=new CAMessage();
		sMsg.getParams();
		ReRixMessage sentMsg=sMsg.createMessage();
		ReRixMessage responseMsg=conn.sendMessage(sentMsg, sMsg.encryptKey);
		sMsg.readResponse(responseMsg);
		conn.stopCommunication();

		//file transfer
		sMsg=new CAMessage();
		conn.startCommunication();
		sMsg.setTempRequestType(1);
		sMsg.getParams();
		sentMsg=sMsg.createMessage();
		responseMsg=conn.sendMessage(sentMsg, sMsg.encryptKey);
		sMsg.readResponse(responseMsg);
		conn.stopCommunication();

	}

	public void setTempRequestType(int tempRequestType) {
		this.tempRequestType = tempRequestType;
	}

	public int getRequestType() {
		return requestType;
	}

	public void setRequestType(int requestType) {
		this.requestType = requestType;
	}

	public SubMessage getRequest() {
		return request;
	}

	public void setRequest(SubMessage request) {
		this.request = request;
	}

	/**
	 * @throws IOException
	 * @see com.usatech.test.BaseMessage#read(ByteBuffer)
	 */
	@Override
	protected void read(ByteBuffer data) throws IOException, InvalidByteValueException {
		super.read(data);
		setRequestType(ProcessingUtils.readByteInt(data));
		SubMessage request = getRequest();
		if(request instanceof GenericRequest0) {
			GenericRequest0 gr0 = (GenericRequest0)request;
			gr0.setStatusCodeBitmap(ProcessingUtils.readShortInt(data));
			//TODO: use a Set of descriptions of these as in C3Message
		} else if(request instanceof GenericRequest1) {
			GenericRequest1 gr1 = (GenericRequest1)request;
			gr1.setFileType(ProcessingUtils.readShortInt(data));
			gr1.setFileName(ProcessingUtils.readShortString(data, ProcessingConstants.US_ASCII_CHARSET));
		} else if(request instanceof GenericRequest2) {
			GenericRequest2 gr2 = (GenericRequest2)request;
			gr2.setPropertyListRequest(ProcessingUtils.readLongString(data, ProcessingConstants.US_ASCII_CHARSET));
		} else if(request instanceof GenericRequest3) {
			GenericRequest3 gr3 = (GenericRequest3)request;
			gr3.setPropertyList(ProcessingUtils.readLongString(data, ProcessingConstants.US_ASCII_CHARSET));
		}
	}
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageTypeName())
			.append(": [messageNumber=").append(getMessageNumber())
			.append("; requestType=").append(getRequestType());
		SubMessage request = getRequest();
		if(request instanceof GenericRequest0) {
			GenericRequest0 gr0 = (GenericRequest0)request;
			sb.append(": [statusCodeBitmap=").append(gr0.getStatusCodeBitmap());
			//TODO: use a Set of descriptions of these as in C3Message
		} else if(request instanceof GenericRequest1) {
			GenericRequest1 gr1 = (GenericRequest1)request;
			sb.append(": [fileType=").append(gr1.getFileType())
			.append("; fileName=").append(gr1.getFileName());
		} else if(request instanceof GenericRequest2) {
			GenericRequest2 gr2 = (GenericRequest2)request;
			sb.append(": [propertyListRequest=").append(gr2.getPropertyListRequest());
		} else if(request instanceof GenericRequest3) {
			GenericRequest3 gr3 = (GenericRequest3)request;
			sb.append(": [propertyList=").append(gr3.getPropertyList());
		}
		sb.append("]]");

		return sb.toString();
	}
}
