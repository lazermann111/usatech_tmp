package com.usatech.test;

import com.usatech.networklayer.ReRixMessage;

public class C7MessageManual extends C7Message{
	public C7MessageManual(){
		super();
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getPayLoad() {
		return payLoad;
	}
	public void setPayLoad(String payLoad) {
		this.payLoad = payLoad;
	}
	
	public static void main(String[] args) throws Exception{
		ClientConnectUtil conn=new ClientConnectUtil();
		conn.setProtocol(7);
		conn.startCommunication();
		C7MessageManual sMsg=new C7MessageManual();
		sMsg.getParams();
		ReRixMessage sentMsg=sMsg.createMessage();
		ReRixMessage responseMsg=conn.sendMessage(sentMsg, sMsg.encryptKey);
		sMsg.readResponse(responseMsg);
		conn.stopCommunication();

	}

}
