package com.usatech.test;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

/*
 * Request a trial account to login https://www.twilio.com/login
 * userName: yhe@usatech.com
 * The From number needs to be acquired from twilio to be able to send sms, test using 14842716743
 * NOTE: The biggest concern before using it is twilio-7.4.0-jar-with-dependencies.jar is using open source lib that we also used in house. We need to find out whether their dependencies class does not conflict with ours.
 * 
 */
public class SMSTest {
	// Find your Account Sid and Token at twilio.com/user/account
  public static final String ACCOUNT_SID = "AC98df80014b643ff7b2d6f0a533cbc125";
  public static final String AUTH_TOKEN = "46a9a975c8fc7beb7dfda152a26bb694";
  
	public static void main(String[] args) {
		Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
    Message message = Message.creator(new PhoneNumber("+14849191742"),
        new PhoneNumber("+14842716743"), //this 
        "This is a test USAT sms using twilio").create();

    System.out.println(message.getSid());

	}

}
