/**
 *
 */
package com.usatech.test;

import static org.junit.Assert.assertTrue;

import java.beans.IntrospectionException;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.UndeclaredThrowableException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.Socket;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.JOptionPane;

import org.apache.axis2.AxisFault;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;

import com.caucho.hessian.client.HessianProxyFactory;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.ByteArrayUtils;
import simple.io.EncodingAppendable;
import simple.results.BeanException;
import simple.security.SecureHash;
import simple.security.SecurityUtils;
import simple.security.crypt.CryptUtils;
import simple.test.UnitTest;
import simple.text.RegexUtils;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;
import simple.util.AbstractLookupMap;
import simple.util.sheet.SheetUtils;
import simple.util.sheet.SheetUtils.RowValuesIterator;

import com.usatech.ec.ECAuthResponse;
import com.usatech.ec.ECDataSource;
import com.usatech.ec.ECProcessUpdatesResponse;
import com.usatech.ec.ECResponse;
import com.usatech.ec.ECServiceAPI;
import com.usatech.ec.EcStub;
import com.usatech.ec2.EC2AuthResponse;
import com.usatech.ec2.EC2CardId;
import com.usatech.ec2.EC2CardInfo;
import com.usatech.ec2.EC2DataHandler;
import com.usatech.ec2.EC2ProcessUpdatesResponse;
import com.usatech.ec2.EC2Response;
import com.usatech.ec2.EC2ServiceAPI;
import com.usatech.ec2.EC2TokenResponse;
import com.usatech.ec2.Ec2Stub;
import com.usatech.keymanager.service.test.KMSTests;
import com.usatech.layers.common.BCDInt;
import com.usatech.layers.common.HttpRequester;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.ABPermissionActionCode;
import com.usatech.layers.common.constants.AEAdditionalDataType;
import com.usatech.layers.common.constants.AuthPermissionActionCode;
import com.usatech.layers.common.constants.AuthResponseCode60;
import com.usatech.layers.common.constants.AuthResponseCodeA1;
import com.usatech.layers.common.constants.AuthResponseCodeAF;
import com.usatech.layers.common.constants.AuthResponseCodeC3;
import com.usatech.layers.common.constants.AuthResponseType;
import com.usatech.layers.common.constants.CRCType;
import com.usatech.layers.common.constants.CRCType.Calculator;
import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.CardType;
import com.usatech.layers.common.constants.ComponentType;
import com.usatech.layers.common.constants.CounterReasonCode;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.EBeaconAppOS;
import com.usatech.layers.common.constants.EBeaconAppType;
import com.usatech.layers.common.constants.ESudsRoomStatus;
import com.usatech.layers.common.constants.ESudsTopOrBottomType;
import com.usatech.layers.common.constants.EntryCapability;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.EventType;
import com.usatech.layers.common.constants.FileType;
import com.usatech.layers.common.constants.GenericRequestType;
import com.usatech.layers.common.constants.GenericResponseClientActionCode;
import com.usatech.layers.common.constants.GenericResponseResultCode;
import com.usatech.layers.common.constants.GenericResponseServerActionCode;
import com.usatech.layers.common.constants.GxBezelType;
import com.usatech.layers.common.constants.InitializationReason;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.PaymentActionType;
import com.usatech.layers.common.constants.PermissionResponseCodeAB;
import com.usatech.layers.common.constants.PosEnvironment;
import com.usatech.layers.common.constants.ReceiptResult;
import com.usatech.layers.common.constants.SaleResult;
import com.usatech.layers.common.constants.SaleType;
import com.usatech.layers.common.constants.SettlementFormat;
import com.usatech.layers.common.constants.TranDeviceResultType;
import com.usatech.layers.common.constants.WasherDryerType;
import com.usatech.layers.common.messagedata.AuthorizeMessageData;
import com.usatech.layers.common.messagedata.LineItem;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageDataFactory;
import com.usatech.layers.common.messagedata.MessageDataUtils;
import com.usatech.layers.common.messagedata.MessageData_0007;
import com.usatech.layers.common.messagedata.MessageData_2A;
import com.usatech.layers.common.messagedata.MessageData_2B;
import com.usatech.layers.common.messagedata.MessageData_2F;
import com.usatech.layers.common.messagedata.MessageData_5C;
import com.usatech.layers.common.messagedata.MessageData_60;
import com.usatech.layers.common.messagedata.MessageData_82;
import com.usatech.layers.common.messagedata.MessageData_86;
import com.usatech.layers.common.messagedata.MessageData_87;
import com.usatech.layers.common.messagedata.MessageData_8F;
import com.usatech.layers.common.messagedata.MessageData_90;
import com.usatech.layers.common.messagedata.MessageData_92;
import com.usatech.layers.common.messagedata.MessageData_93;
import com.usatech.layers.common.messagedata.MessageData_9A41;
import com.usatech.layers.common.messagedata.MessageData_9A5E;
import com.usatech.layers.common.messagedata.MessageData_9A60;
import com.usatech.layers.common.messagedata.MessageData_9A63;
import com.usatech.layers.common.messagedata.MessageData_9A6A;
import com.usatech.layers.common.messagedata.MessageData_A0;
import com.usatech.layers.common.messagedata.MessageData_A1;
import com.usatech.layers.common.messagedata.MessageData_A2;
import com.usatech.layers.common.messagedata.MessageData_A2.BEXTransactionDetail;
import com.usatech.layers.common.messagedata.MessageData_A4;
import com.usatech.layers.common.messagedata.MessageData_A6;
import com.usatech.layers.common.messagedata.MessageData_A8;
import com.usatech.layers.common.messagedata.MessageData_AB;
import com.usatech.layers.common.messagedata.MessageData_AC;
import com.usatech.layers.common.messagedata.MessageData_AD;
import com.usatech.layers.common.messagedata.MessageData_AE;
import com.usatech.layers.common.messagedata.MessageData_AE.GenericCardReader;
import com.usatech.layers.common.messagedata.MessageData_AF;
import com.usatech.layers.common.messagedata.MessageData_C1;
import com.usatech.layers.common.messagedata.MessageData_C1.BaseComponent;
import com.usatech.layers.common.messagedata.MessageData_C1.ComponentData;
import com.usatech.layers.common.messagedata.MessageData_C2;
import com.usatech.layers.common.messagedata.MessageData_C2.AppCardReader;
import com.usatech.layers.common.messagedata.MessageData_C2.EncryptingTracksCardReader;
import com.usatech.layers.common.messagedata.MessageData_C2.GenericTracksCardReader;
import com.usatech.layers.common.messagedata.MessageData_C3;
import com.usatech.layers.common.messagedata.MessageData_C3.AuthResponseTypeData;
import com.usatech.layers.common.messagedata.MessageData_C3.AuthorizationAuthResponse;
import com.usatech.layers.common.messagedata.MessageData_C3.AuthorizationV2AuthResponse;
import com.usatech.layers.common.messagedata.MessageData_C3.AuthorizationV2AuthResponse.MobileAppData;
import com.usatech.layers.common.messagedata.MessageData_C3.PermissionAuthResponse;
import com.usatech.layers.common.messagedata.MessageData_C4;
import com.usatech.layers.common.messagedata.MessageData_C4.Format0LineItem;
import com.usatech.layers.common.messagedata.MessageData_C5;
import com.usatech.layers.common.messagedata.MessageData_C5.Format0Content;
import com.usatech.layers.common.messagedata.MessageData_C6;
import com.usatech.layers.common.messagedata.MessageData_C7;
import com.usatech.layers.common.messagedata.MessageData_C8;
import com.usatech.layers.common.messagedata.MessageData_C9;
import com.usatech.layers.common.messagedata.MessageData_CA;
import com.usatech.layers.common.messagedata.MessageData_CA.UpdateStatusRequest;
import com.usatech.layers.common.messagedata.MessageData_CB;
import com.usatech.layers.common.messagedata.MessageData_CB.PropertyIndexListAction;
import com.usatech.layers.common.messagedata.MessageData_CB.PropertyValueListAction;
import com.usatech.layers.common.messagedata.MessageData_CC;
import com.usatech.layers.common.messagedata.MessageData_F1;
import com.usatech.layers.common.messagedata.MessageData_F1.StandardPayload;
import com.usatech.layers.common.messagedata.MessageData_Ping;
import com.usatech.layers.common.messagedata.MessageDirection;
import com.usatech.networklayer.test.EportConnectSOAPTest;
import com.usatech.tools.LuhnUtils;
import com.usatech.util.crypto.Crypt;
import com.usatech.util.crypto.CryptUtil;

/**
 * @author Brian S. Krug
 *
 */
public class ClientEmulatorControl extends UnitTest {
	private static Log log = LogFactory.getLog(ClientEmulator.class);
	// protected String host = "127.0.0.1";
	//protected String host = "intnet11";
	// protected String host = "10.0.0.67";
	protected String host = "10.0.0.68";
	// protected String host = "devnet11";
	// protected String host = "devnet01";
	// protected String host = "intnet11";
	//protected String host = "eccnet01";
	// protected String host = "eccnet11";
	// protected String host = "eccnet12";
	// protected String host = "192.168.4.63";
	//protected String host = "192.168.79.172";
	// protected String host = "usanet31.trooper.usatech.com";
	protected int offset = 0;
	protected int authPort = 14109 + offset;
	protected int salePort = 14108 + offset;
	protected int layer3Port = 14107 + offset;
	protected int unifedlayerPort = 443; // (offset > 0 ? 14143 + offset: 443);
	protected int ecPort = 9443;
	//protected int authPort = 14111;
	//protected int salePort = 444;
	protected final Map<String,Map<String,byte[]>> keys = new HashMap<String, Map<String,byte[]>>();
	static {
		System.setProperty("app.servicename", "ePortConnectHessianTest");
		System.setProperty("javax.net.ssl.trustStore", "../LayersCommon/conf/net/truststore.ts");
		System.setProperty("javax.net.ssl.trustStorePassword", "usatech");
	}
	public ClientEmulatorControl() {
		Map<String,byte[]> localKeys = new HashMap<String, byte[]>();
		localKeys.put("EV000000", ByteArrayUtils.fromHex("39323635303237373339323734343031"));
		localKeys.put("EV000001", ByteArrayUtils.fromHex("C34DFA94CBC0A54164ED24DACE23C265"));
		localKeys.put("EV100167", ByteArrayUtils.fromHex("39363034383331313331363433363231"));
		localKeys.put("TD999999", ByteArrayUtils.fromHex("2BC3F2FD63C917A210CB0212BEA96516"));
		localKeys.put("TD000021", ByteArrayUtils.fromHex("4F2C20384F832A10963DD63DD9F057EC"));
		localKeys.put("TD000023", ByteArrayUtils.fromHex("B7314205E38C584548C640F53014C844"));
		localKeys.put("TD000024", ByteArrayUtils.fromHex("5F81E99894F7ECD919A900660166048F"));
		localKeys.put("TD000042", ByteArrayUtils.fromHex("60DA1B0BE585266BB36FBAAC49A15ECC"));
		localKeys.put("TD000065", ByteArrayUtils.fromHex("B2C3BB41F2A68261F86AE96BED2222E3"));
		localKeys.put("TD000052", ByteArrayUtils.fromHex("AB028CCA58BA821A34E0AFAF05DE7306"));
		localKeys.put("TD000587", ByteArrayUtils.fromHex("60E6FB5883F3A0747C29988BBFAB4665"));
		localKeys.put("TD000593", ByteArrayUtils.fromHex("BD74D344A11663BA9E36D646C825D4F8"));
		localKeys.put("TD000594", ByteArrayUtils.fromHex("9499904D19534D940EB730DB8E6322E2"));
		localKeys.put("TD000595", ByteArrayUtils.fromHex("1E4B61A63A6E38480BE12789DDF7279A"));
		localKeys.put("EV033602", ByteArrayUtils.fromHex("52B103DDE6FADC616821637B962BEFBA"));
		localKeys.put("EV100134", ByteArrayUtils.fromHex("31333530333639323830373330373236"));
		localKeys.put("TD000597", ByteArrayUtils.fromHex("B2FAF31017D8B1C6E779089806F5DBB1"));
		localKeys.put("TD000580", ByteArrayUtils.fromHex("EF20B4694013EF2523497916A483906C"));
		localKeys.put("TD000581", ByteArrayUtils.fromHex("99F79B828E11EB76ED299236D5B4CA6F"));
		localKeys.put("TD000582", ByteArrayUtils.fromHex("7164689FFBCA7872DFA80C8617748F37"));
		localKeys.put("EV033414", ByteArrayUtils.fromHex("35363236353935373435393230363734"));
		localKeys.put("EV050271", ByteArrayUtils.fromHex("32303538393434353132313732343937"));
		localKeys.put("EV035433", ByteArrayUtils.fromHex("32303838353334303136323039343835"));
		localKeys.put("EV050278", ByteArrayUtils.fromHex("31323831303332313932373432373532"));
		localKeys.put("EV035194", ByteArrayUtils.fromHex("33303232383438303032383737333538"));
		localKeys.put("EV033406", ByteArrayUtils.fromHex("31303830383532343830323035383734"));
		localKeys.put("EV033409", ByteArrayUtils.fromHex("32313336373638353132353431363232"));
		localKeys.put("EV100027", ByteArrayUtils.fromHex("31313837313834363439363932373734"));
		localKeys.put("TD000159", ByteArrayUtils.fromHex("E314D232C9C79397E472B27784C45A33"));
		// localKeys.put("EV036214", ByteArrayUtils.fromHex("3DED24176D5AA19DDBFD4B6A87991E89"));
		localKeys.put("EV036214", ByteArrayUtils.fromHex("C34DFA94CBC0A54164ED24DACE23C265"));//
		localKeys.put("EV035425", ByteArrayUtils.fromHex("38363638313139303431393334333630"));
		localKeys.put("EV021026", ByteArrayUtils.fromHex("31383038353738303234313736343632"));
		localKeys.put("TD000473", ByteArrayUtils.fromHex("5CC314181C29D1E658D89BFBC165A99A"));
		localKeys.put("EV033483", ByteArrayUtils.fromHex("5ADAD0C45088F6316E2F709F57C8AF6A"));
		localKeys.put("EV100139", ByteArrayUtils.fromHex("0D32375C7E0B3261873E2C8D93B908DC"));
		localKeys.put("EV100166", ByteArrayUtils.fromHex("8A452E49E4F582CC57E9F5C03896E3B1"));
		localKeys.put("EV108402", ByteArrayUtils.fromHex("8A452E49E4F582CC57E9F5C03896E3B1"));
		localKeys.put("TD000022", ByteArrayUtils.fromHex("2DBC283C3D0DF48505E8B5D20677D462"));
		localKeys.put("EV100085", ByteArrayUtils.fromHex("32303739353535353834363730383539"));// FDMS
		localKeys.put("TD000188", ByteArrayUtils.fromHex("F62614C1DA1980980ED92247C9158553")); // EE100000017
		localKeys.put("TD000053", ByteArrayUtils.fromHex("59DF7EB5212B1DB9459B98D3F1E22516"));
		localKeys.put("TD000925", ByteArrayUtils.fromHex("EDE94E23D4AB6647BDC1C56B13B321B9"));
		localKeys.put("TD000908", ByteArrayUtils.fromHex("78DD3BDA05AFD15B62F4FA61DE866E5F"));
		localKeys.put("LD-BLNCR", new byte[0]);
		keys.put("127.0.0.1", localKeys);
		keys.put("localhost", localKeys);
		Map<String,byte[]> devKeys = new HashMap<String, byte[]>();
		devKeys.put("EV000001", ByteArrayUtils.fromHex("C34DFA94CBC0A54164ED24DACE23C265"));
		devKeys.put("EV035433", ByteArrayUtils.fromHex("32303838353334303136323039343835"));
		devKeys.put("TD000021", ByteArrayUtils.fromHex("C26F26A5FB906D00D48CB6A3BE67EDFE"));
		devKeys.put("TD000023", ByteArrayUtils.fromHex("AF8FB559A198E82B47F59CBFE89A9E81"));
		devKeys.put("TD000042", ByteArrayUtils.fromHex("7809D3437A71B6D8757D7D7BF45A9E82")); // EE100000008
		devKeys.put("TD000052", ByteArrayUtils.fromHex("3F3F6A3F3F3F3F403F3F3F153F3F3F33"));
		devKeys.put("TD000065", ByteArrayUtils.fromHex("FB0CC74DF62DF0A22087B184A65DFE46"));
		devKeys.put("TD999998", ByteArrayUtils.fromHex("0123456789ABCDEF0123456789ABCDEF"));
		devKeys.put("TD999999", ByteArrayUtils.fromHex("2BC3F2FD63C917A210CB0212BEA96516"));
		devKeys.put("TD000308", ByteArrayUtils.fromHex("C9D6D5AEA0919D06EFD3FFF9EFA988CD"));	
		devKeys.put("EV033602", ByteArrayUtils.fromHex("35363631333237333631303635363135"));	
		devKeys.put("EV033414", ByteArrayUtils.fromHex("5399246870E11AC04B140A4B418CF37C"));
		devKeys.put("EV035194", ByteArrayUtils.fromHex("33303232383438303032383737333538"));
		devKeys.put("EV100134", ByteArrayUtils.fromHex("31323237383436323132343733313736"));
		devKeys.put("EV100185", ByteArrayUtils.fromHex("59085F9FA193E099EB143877B8038F6D"));
		devKeys.put("EV033412", ByteArrayUtils.fromHex("E5F9FC2E83146D73796A0A7973354258"));
		devKeys.put("EV033406", ByteArrayUtils.fromHex("31303830383532343830323035383734"));
		devKeys.put("EV033409", ByteArrayUtils.fromHex("32313336373638353132353431363232"));
		devKeys.put("TD000587", ByteArrayUtils.fromHex("3C29269DC68A33D62582914206AB7342"));
		devKeys.put("EV100166", ByteArrayUtils.fromHex("33313838333236343031383634303733"));
		devKeys.put("EV100167", ByteArrayUtils.fromHex("36393633383535333637313132363232"));
		devKeys.put("TD000436", ByteArrayUtils.fromHex("F76C33B6AD19783518F7303ABA372B61")); // EE190000115
		devKeys.put("EV100212", ByteArrayUtils.fromHex("72A9B6EEA3B357F76F055A693DC99111"));
		devKeys.put("EV100139", ByteArrayUtils.fromHex("31303234343131303436313635303838"));
		devKeys.put("LD-BLNCR", new byte[0]);
		keys.put("devnet01", devKeys);
		keys.put("devnet11", devKeys);
		keys.put("devnet12", devKeys);
		keys.put("10.0.0.67", devKeys);
		Map<String,byte[]> intKeys = new HashMap<String, byte[]>();
		intKeys.put("EV000001", ByteArrayUtils.fromHex("C34DFA94CBC0A54164ED24DACE23C265"));
		intKeys.put("TD000021", ByteArrayUtils.fromHex("2757556473ADE7CE6F2E654C8B008BA4"));
		intKeys.put("TD000023", ByteArrayUtils.fromHex("B63F1F7F7E36D99DDF09A8DD8C5A8ADB"));
		intKeys.put("TD000024", ByteArrayUtils.fromHex("3F2D625314594975696E2C5B333F7861"));
		intKeys.put("TD000042", ByteArrayUtils.fromHex("3F6934CD4917EA4E09AAC63BB87CDA9A"));
		intKeys.put("TD000065", ByteArrayUtils.fromHex("5EF6E31B196B06E980AC73CFD30E6273"));
		intKeys.put("TD999998", ByteArrayUtils.fromHex("0123456789ABCDEF0123456789ABCDEF"));
		intKeys.put("EV100134", ByteArrayUtils.fromHex("84FCAA76EC9E6FA7631A6FED48D8363D"));
		intKeys.put("EV033602", ByteArrayUtils.fromHex("35363631333237333631303635363135"));	
		intKeys.put("EV033414", ByteArrayUtils.fromHex("31343731313135373238333235393334"));
		intKeys.put("EV033406", ByteArrayUtils.fromHex("31303830383532343830323035383734"));
		intKeys.put("EV033409", ByteArrayUtils.fromHex("32313336373638353132353431363232"));
		intKeys.put("EV100139", ByteArrayUtils.fromHex("E68239EB122B2D4D9B2AC0D340AFFD60"));
		intKeys.put("EV100212", ByteArrayUtils.fromHex("F1934EF4B8020F3E02B905C41BD2DDAF"));
		intKeys.put("TD001396", ByteArrayUtils.fromHex("3E0A41415614F2BC050DA64C021E486E"));
		intKeys.put("TD001759", ByteArrayUtils.fromHex("AD87B7FE6770E44AFF076B7D0B36CFA2"));
		intKeys.put("LD-BLNCR", new byte[0]);
		intKeys.put("EV100505", ByteArrayUtils.fromHex("36333335393736353031323338333732"));
		keys.put("intnet11", intKeys);
		keys.put("10.0.0.68", intKeys);
		Map<String,byte[]> eccKeys = new HashMap<String, byte[]>();
		eccKeys.put("TD000005", ByteArrayUtils.fromHex("6C14172873CCD4F0E450F1F9675081B7"));
		eccKeys.put("TD000021", ByteArrayUtils.fromHex("3D585F96F633D20A997A32CFD90A0099"));
		eccKeys.put("TD000023", ByteArrayUtils.fromHex("5E26F914565C4FF88626FD716924ADE2"));
		eccKeys.put("TD000024", ByteArrayUtils.fromHex("D4939321902D6E4CBC55AF8ABC1B0A52"));
		eccKeys.put("TD000042", ByteArrayUtils.fromHex("D86E4DA27F910758864A0BF61F2AF6EA"));
		eccKeys.put("TD999999", ByteArrayUtils.fromHex("0123456789ABCDEF0123456789ABCDEF"));
		eccKeys.put("TD000119", ByteArrayUtils.fromHex("4BBA69C2236997B487DD39B173E84BD6"));
		eccKeys.put("EV083765", ByteArrayUtils.fromHex("520ADAB819B11D29DB6C750F3FE041C6"));
		eccKeys.put("EV108402", ByteArrayUtils.fromHex("676BF0572F2E9467506B50878FAB93A2"));
		eccKeys.put("EV100167", ByteArrayUtils.fromHex("C7A76C4DCF07EBC477263E677A2A9F0C"));
		eccKeys.put("TD000899", ByteArrayUtils.fromHex("AD87B7FE6770E44AFF076B7D0B36CFA2"));
		eccKeys.put("LD-BLNCR", new byte[0]);
		keys.put("eccnet01", eccKeys);
		keys.put("eccnet11", eccKeys);
		keys.put("eccnet12", eccKeys);
		keys.put("192.168.4.63", eccKeys);
		Map<String,byte[]> prodKeys = new HashMap<String, byte[]>();
		prodKeys.put("TD999999", ByteArrayUtils.fromHex("0123456789ABCDEF0123456789ABCDEF"));
		prodKeys.put("LD-BLNCR", new byte[0]);
		prodKeys.put("TD001521", ByteArrayUtils.fromHex("71FD23D924FBFC890B3FDB971771EF95"));// EE100010001
		keys.put("192.168.79.172", prodKeys);	
		keys.put("usanet31.trooper.usatech.com", prodKeys);
	}
	@Test
	public void junit_V4Cryption() throws Exception {
		String deviceName = "EV033414";
		String messageHex = "00A04CD9777843000000C800343433333435383732383537313533393D2A2A2A2A2A2A2A2A2A2A2A2A2A2A2A2A2A2A2A2A";
		MessageData data = MessageDataFactory.readMessage(ByteBuffer.wrap(ByteArrayUtils.fromHex(messageHex)), MessageDirection.CLIENT_TO_SERVER, DeviceType.GX);
		ClientEmulator ce = new ClientEmulator(host, layer3Port, deviceName, keys.get(host).get(deviceName));
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.startCommunication(4);
		try {
			ce.sendMessage(data);
			ce.sendHangup();
			Thread.sleep(5000);			
		} finally {
			ce.stopCommunication();
		}
		
	}

	public final String EPORT_CONNECT_HESSIAN_URL = "https://" + host + ":" + ecPort + "/hessian/ec";
	public final String EPORT_CONNECT_2_HESSIAN_URL = "https://" + host + ":" + ecPort + "/hessian/ec2";
	public final String EPORT_CONNECT_SOAP_URL = "https://" + host + ":" + ecPort + "/soap/ec";
	public final String EPORT_CONNECT_2_SOAP_URL = "https://" + host + ":" + ecPort + "/soap/ec2";
	protected HessianProxyFactory hessianProxyFactory = null;
	protected final Map<String, ECServiceAPI> ePortConnects = new HashMap<String, ECServiceAPI>();
	protected final Map<String, EC2ServiceAPI> ePortConnect2s = new HashMap<String, EC2ServiceAPI>();
	protected final Map<String, EcStub> ePortConnectSoaps = new HashMap<String, EcStub>();
	protected final Map<String, Ec2Stub> ePortConnectSoap2s = new HashMap<String, Ec2Stub>();

	protected ECServiceAPI getECService() throws MalformedURLException {
		return getECService(null);
	}

	protected ECServiceAPI getECService(String env) throws MalformedURLException {
		if(hessianProxyFactory == null)
			hessianProxyFactory = new HessianProxyFactory();

		ECServiceAPI api = ePortConnects.get(env);
		if(api == null) {
			StringBuilder sb = new StringBuilder();
			sb.append("https://");
			if(StringUtils.isBlank(env))
				sb.append("localhost:");
			else
				sb.append("ec-").append(env).append(".usatech.com:");
			sb.append(ecPort).append("/hessian/ec");
			api = (ECServiceAPI) hessianProxyFactory.create(ECServiceAPI.class, sb.toString());
			ePortConnects.put(env, api);
		}
		return api;
	}

	protected EC2ServiceAPI getEC2Service() throws MalformedURLException {
		return getEC2Service(null);
	}

	protected EC2ServiceAPI getEC2Service(String env) throws MalformedURLException {
		if(hessianProxyFactory == null)
			hessianProxyFactory = new HessianProxyFactory();
		EC2ServiceAPI api = ePortConnect2s.get(env);
		if(api == null) {
			StringBuilder sb = new StringBuilder();
			sb.append("https://");
			if(StringUtils.isBlank(env))
				sb.append("localhost:");
			else if("usa".equalsIgnoreCase(env))
				sb.append("ec.usatech.com:");
			else
				sb.append("ec-").append(env).append(".usatech.com:");
			sb.append(ecPort).append("/hessian/ec2");
			api = (EC2ServiceAPI) hessianProxyFactory.create(EC2ServiceAPI.class, sb.toString());
			ePortConnect2s.put(env, api);
		}
		return api;
	}

	protected EcStub getECSoapService() throws AxisFault {
		return getECSoapService(null);
	}

	protected EcStub getECSoapService(String env) throws AxisFault {
		EcStub api = ePortConnectSoaps.get(env);
		if(api == null) {
			StringBuilder sb = new StringBuilder();
			sb.append("https://");
			if(StringUtils.isBlank(env))
				sb.append("localhost:");
			else
				sb.append("ec-").append(env).append(".usatech.com:");
			sb.append(ecPort).append("/soap/ec");
			api = new EcStub(sb.toString());
			ePortConnectSoaps.put(env, api);
		}
		return api;
	}

	protected Ec2Stub getEC2SoapService() throws AxisFault {
		return getEC2SoapService(null);
	}

	protected Ec2Stub getEC2SoapService(String env) throws AxisFault {
		Ec2Stub api = ePortConnectSoap2s.get(env);
		if(api == null) {
			StringBuilder sb = new StringBuilder();
			sb.append("https://");
			if(StringUtils.isBlank(env))
				sb.append("localhost:");
			else
				sb.append("ec-").append(env).append(".usatech.com:");
			sb.append(ecPort).append("/soap/ec2");
			api = new Ec2Stub(sb.toString());
			ePortConnectSoap2s.put(env, api);
		}
		return api;
	}
	protected static String getBasicResponseMessage(Object response) {
		StringBuilder sb = new StringBuilder();
		sb.append(response.getClass().getSimpleName()).append(' ');
		try {
			for(Map.Entry<String, ?> entry : ReflectionUtils.toPropertyMap(response).entrySet()) {
				sb.append(entry.getKey()).append(": ").append(entry.getValue()).append(", ");
			}
		} catch(IntrospectionException e) {
			log.error("While printing response", e);
		}

		return sb.substring(0, sb.length() - 2);
	}
	@Test
	public void junit_testHessianBadTranId() throws Exception {
		ECServiceAPI ec = getECService();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 150;
		String cardData = ";5454545454545454=10121015432112345601?"; // MC
		String cardType = "C";
		String tranResult = "S";
		String tranDetails = "A0|302|10|6|1|305|10|9|1";
		ECAuthResponse response = ec.chargeV3("testaccount6", "UbR!!q9hva!TGCssn#yK", EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, cardData, cardType, tranResult, tranDetails);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		if(response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED)
			log.info(message);
		else
			throw new Exception(message);
		response = ec.chargeV3("testaccount6", "UbR!!q9hva!TGCssn#yK", EportConnectSOAPTest.SERIAL_NUMBER, tranId - 1, amount, cardData, cardType, tranResult, tranDetails);
		if(response == null)
			throw new Exception("Received null response");
		message = getBasicResponseMessage(response);
		if(response.getReturnCode() == EportConnectSOAPTest.RES_FAILED)
			log.info(message);
		else
			throw new Exception(message);
	}

	@Test
	public void junit_testHessianBadTranIdv2() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 150;
		String cardData = ";5454545454545454=10121015432112345601?"; // MC
		String cardType = "C";
		String tranResult = "S";
		String tranDetails = "A0|302|10|6|1|305|10|9|1";
		EC2AuthResponse response = ec.chargePlain(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, cardData, cardType, tranResult, tranDetails, null);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		if(response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED)
			log.info(message);
		else
			throw new Exception(message);
		response = ec.chargePlain(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId - 1, amount, cardData, cardType, tranResult, tranDetails, null);
		if(response == null)
			throw new Exception("Received null response");
		message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_testHessianBadTranIdv2_int() throws Exception {
		EC2ServiceAPI ec = getEC2Service("int");
		String password = "96343409";
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 150;
		String cardData = ";5454545454545454=10121015432112345601?"; // MC
		String cardType = "C";
		// String tranResult = "S";
		// String tranDetails = "A0|302|10|6|1|305|10|9|1";
		EC2AuthResponse response = ec.authPlain(EportConnectSOAPTest.USERNAME, password, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, cardData, cardType, null);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		if(response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED)
			log.info(message);
		else
			throw new Exception(message);
		response = ec.authPlain(EportConnectSOAPTest.USERNAME, password, EportConnectSOAPTest.SERIAL_NUMBER, tranId - 1, amount, cardData, cardType, null);
		if(response == null)
			throw new Exception("Received null response");
		message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_testHessianCharge() throws Exception {
		ECServiceAPI ec = getECService();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 150;
		String cardData = ";5454545454545454=10121015432112345601?"; // MC
		String cardType = "C";
		String tranResult = "S";
		String tranDetails = "A0|302|10|6|1|305|10|9|1";
		ECAuthResponse response = ec.chargeV3(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, cardData, cardType, tranResult, tranDetails);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		if(response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED)
			log.info(message);
		else
			throw new Exception(message);
	}
	@Test
	public void junit_testHessianChargeCancel() throws Exception {
		ECServiceAPI ec = getECService();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 250;
		String cardData = ";5454545454545454=10121015432112345601?"; // MC
		String cardType = "C";
		String tranResult = "S";
		String tranDetails = "A0|302|20|6|1|305|10|13|1";
		ECAuthResponse response = ec.chargeV3(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, cardData, cardType, tranResult, tranDetails);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		if(response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED) {
			log.info(message);
			ECResponse batchResponse = ec.batchV3(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, 0, "C", "");
			if(batchResponse == null)
				throw new Exception("Received null response");
			message = getBasicResponseMessage(batchResponse);
			if(batchResponse.getReturnCode() == EportConnectSOAPTest.RES_OK)
				log.info(message);
			else
				throw new Exception(message);
		} else
			throw new Exception(message);
	}

	@Test
	public void junit_testHessianChargev2() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 150;
		String cardData = ";5454545454545454=10121015432112345601?"; // MC
		String entryType = "S";
		String tranResult = "S";
		String tranDetails = "A0|302|10|6|1|305|10|9|1";
		String attributes = null;
		EC2AuthResponse response = ec.chargePlain(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, cardData, entryType, tranResult, tranDetails, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		if(response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED)
			log.info(message);
		else
			throw new Exception(message);
	}

	@Test
	public void junit_testHessianDebitChargev2() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 150;
		String cardData = ";5108280000205847=1809879027?"; // LuhnUtils.fillInTrackData(LuhnUtils.generateCard("510828000", 16)); // MC - Debit
		String entryType = "S";
		String tranResult = "S";
		String tranDetails = "A0|302|10|6|1|305|10|9|1";
		String attributes = null;
		log.info("Authing with " + cardData);
		EC2AuthResponse response = ec.chargePlain(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, "K3MTB000006", tranId, amount, cardData, entryType, tranResult, tranDetails, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		if(response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED)
			log.info(message);
		else
			throw new Exception(message);
	}
	@Test
	public void junit_testSoapCashSale() throws Exception {
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 150;
		String tranResult = "S";
		String tranDetails = "A0|302|10|6|1|305|10|9|1";
		EcStub.BatchV3 request = new EcStub.BatchV3();
		request.setUsername(EportConnectSOAPTest.USERNAME);
		request.setPassword(EportConnectSOAPTest.PASSWORD);
		request.setSerialNumber(EportConnectSOAPTest.SERIAL_NUMBER);
		request.setTranId(tranId);
		request.setAmount(amount);
		request.setTranResult(tranResult);
		request.setTranDetails(tranDetails);

		EcStub ePortConnect = getECSoapService();
		EcStub.BatchV3Response responseMessage;
		try {
			responseMessage = ePortConnect.batchV3(request);
		} finally {
			ePortConnect._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");

		EcStub.ECResponse response = responseMessage.get_return();
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_testSoapProcessUpdates() throws Exception {
		EcStub.ProcessUpdates request = new EcStub.ProcessUpdates();
		request.setUsername(EportConnectSOAPTest.USERNAME);
		request.setPassword(EportConnectSOAPTest.PASSWORD);
		request.setSerialNumber(EportConnectSOAPTest.SERIAL_NUMBER);
		request.setUpdateStatus(0);

		EcStub ePortConnect = getECSoapService();
		EcStub.ProcessUpdatesResponse responseMessage;
		try {
			responseMessage = ePortConnect.processUpdates(request);
		} finally {
			ePortConnect._getServiceClient().cleanupTransport();
		}
		if(responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");

		EcStub.ECResponse response = responseMessage.get_return();
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_SoapFileUpload() throws Exception {
		Ec2Stub.UploadFile request = new Ec2Stub.UploadFile();

		request.setUsername(EportConnectSOAPTest.USERNAME);
		request.setPassword(EportConnectSOAPTest.PASSWORD);
		request.setSerialNumber(EportConnectSOAPTest.SERIAL_NUMBER);
		request.setFileType(FileType.CUSTOM_FILE_UPLOAD.getValue());
		request.setFileName("Test Custom File.txt");
		File file = new File("C:\\Users\\bkrug\\Downloads\\version.txt");
		EC2DataHandler dh = new EC2DataHandler(new ECDataSource(new FileInputStream(file)));
		request.setFileSize(file.length());
		request.setFileContent(dh);
		request.setAttributes("");

		Ec2Stub ePortConnect = getEC2SoapService();
		Ec2Stub.UploadFileResponse responseMessage;
		try {
			responseMessage = ePortConnect.uploadFile(request);
		} finally {
			ePortConnect._getServiceClient().cleanupTransport();
		}
		if(responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");

		String message = getBasicResponseMessage(responseMessage.get_return());
		log.info(message);
	}

	@Test
	public void junit_testSoapProcessUpdates2() throws Exception {
		Ec2Stub.ProcessUpdates request = new Ec2Stub.ProcessUpdates();
		request.setUsername(EportConnectSOAPTest.USERNAME);
		request.setPassword("BET+dkCsn2HAkU98+bxx");
		request.setSerialNumber(EportConnectSOAPTest.SERIAL_NUMBER);
		request.setUpdateStatus(0);
		request.setAppType("BSK Test");
		request.setAppVersion("1.1A");
		request.setAttributes("");

		Ec2Stub ePortConnect = getEC2SoapService();
		Ec2Stub.ProcessUpdatesResponse responseMessage;
		try {
			responseMessage = ePortConnect.processUpdates(request);
		} finally {
			ePortConnect._getServiceClient().cleanupTransport();
		}
		if(responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");

		Ec2Stub.EC2Response response = responseMessage.get_return();
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_testSoapProcessUpdates2_ecc() throws Exception {
		Ec2Stub.ProcessUpdates request = new Ec2Stub.ProcessUpdates();

		request.setUsername("testaccount3");
		request.setPassword("80233965");
		request.setSerialNumber("K3MTB000005");
		request.setUpdateStatus(0);
		request.setAppType("BSK Test");
		request.setAppVersion("1.1A");
		request.setAttributes("");

		Ec2Stub ePortConnect = getEC2SoapService("ecc");
		Ec2Stub.ProcessUpdatesResponse responseMessage;
		try {
			responseMessage = ePortConnect.processUpdates(request);
		} finally {
			ePortConnect._getServiceClient().cleanupTransport();
		}
		if(responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");

		Ec2Stub.EC2Response response = responseMessage.get_return();
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianNewPasswordTest() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		String deviceSerialCd = "K3BK000015";
		log.info("Sending process updates for " + deviceSerialCd);
		EC2ProcessUpdatesResponse response = ec.processUpdates("bkrug", "41540199", deviceSerialCd, 0, 2, "BSK Test", "1.1A", "");
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
		if(!StringUtils.isBlank(response.getNewPassword())) {
			log.info("Got new password. Use to it log in again");
			EC2ProcessUpdatesResponse response2 = ec.processUpdates("bkrug", response.getNewPassword(), deviceSerialCd, 0, 2, "BSK Test", "1.1B", "");
			if(response2 == null)
				throw new Exception("Received null response");
			message = getBasicResponseMessage(response2);
			log.info(message);
		}
	}

	@Test
	public void junit_HessianTmpPasscodeTest() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		String deviceSerialCd = "K3BK000015";
		log.info("Sending process updates for " + deviceSerialCd);
		// EC2ProcessUpdatesResponse response = ec.processUpdates("bkrug", "41227986", deviceSerialCd, 0, 2, "BSK Test", "1.1A", "");
		EC2ProcessUpdatesResponse response = ec.processUpdates("bkrug", "a8sAmCmeg8jjUfCuu!52", deviceSerialCd, 0, 2, "BSK Test", "1.1A", "");
		// EC2ProcessUpdatesResponse response = ec.processUpdates("testaccount1", "52927513", deviceSerialCd, 0, 2, "BSK Test", "1.1A", "");
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianAuthWithTmpPasswordTest() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		String deviceSerialCd = "K3BK000015";
		log.info("Sending auth for " + deviceSerialCd);
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 250;
		String track = ";5454545454545454=1505867921?";// ";4118960420454783=1809867921?"; // ";4118960420454742=1809867921?";// ";6011000040000000=20120000000000000000?";// Discover
		String entryType = "S";
		// String tranResult = "S";
		// String tranDetails = "A0|200|40|2|Ping pongs|200|85|2|Yo-yos";
		// String attributes = "requireCVVMatch=true\nrequireAVSMatch=true\nbillingPostalCode=19380\nsecurityCode=1987\nbillingAddress=101 Easy St\n";
		String attributes = "requireCVVMatch=true\nrequireAVSMatch=true\nsecurityCode=1987\nbillingPostalCode=19380\nbillingAddress=101 Easy St\n";
		// String attributes = null;
		EC2AuthResponse response = ec.authPlain("bkrug", "41540199", deviceSerialCd, tranId, amount, track, entryType, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
		if(response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED) {
			EC2Response batchResponse = ec.batch("bkrug", "41540199", deviceSerialCd, tranId, 0, "C", "", attributes);
			if(batchResponse == null)
				throw new Exception("Received null response");
			message = getBasicResponseMessage(batchResponse);
			log.info(message);
		}
	}

	@Test
	public void junit_HessianAuthWithNewPasswordTest() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		String deviceSerialCd = "K3BK000015";
		String username = "bkrug";
		String password = "a8sAmCmeg8jjUfCuu!52";
		log.info("Sending auth for " + deviceSerialCd);
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 250;
		long saleAmount = 200;
		String track = ";5454545454545454=1505867921?";// ";4118960420454783=1809867921?"; // ";4118960420454742=1809867921?";// ";6011000040000000=20120000000000000000?";// Discover
		String entryType = "S";
		// String tranResult = "S";
		// String tranDetails = "A0|200|40|2|Ping pongs|200|85|2|Yo-yos";
		// String attributes = "requireCVVMatch=true\nrequireAVSMatch=true\nbillingPostalCode=19380\nsecurityCode=1987\nbillingAddress=101 Easy St\n";
		// String attributes = "requireCVVMatch=true\nrequireAVSMatch=true\nsecurityCode=1987\nbillingPostalCode=19380\nbillingAddress=101 Easy St\n";
		// String attributes = "requireCVVMatch=true\nrequireAVSMatch=true\nsecurityCode=1987\nbillingPostalCode=19380\nbillingAddress=101 Easy St\nlatitude=23.0949\nlongitude=98.123\ngeocodeTime=" +
		// (System.currentTimeMillis() - 23 * 60 * 60 * 1000L) + "\n";
		// String attributes = "latitude=23.0949\nlongitude=98.123\ngeocodeTime=" + (System.currentTimeMillis() - 23 * 60 * 60 * 1000L) + "\n";
		String attributes = "latitude=42.28787\nlongitude=-71.064544\ngeocodeTime=" + (System.currentTimeMillis()) + "\n";
		// String attributes = null;
		EC2AuthResponse response = ec.authPlain(username, password, deviceSerialCd, tranId, amount, track, entryType, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
		if(response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED) {
			EC2Response batchResponse = ec.batch(username, password, deviceSerialCd, tranId, saleAmount, saleAmount == 0 ? "C" : "S", "", attributes);
			if(batchResponse == null)
				throw new Exception("Received null response");
			message = getBasicResponseMessage(batchResponse);
			log.info(message);
		}
	}
	
	@Test
	public void junit_TandemEMVCertification() throws Exception {
		/*
		String AMEX = ";371449635398431=20121015432112345678?";
		String AMEX_MANUAL = "371449635398431|2012||||";
		String CHASENET = ";4011361100000012=200810110000000?";
		String CHASENET_MANUAL = "4011361100000012|2008||||";
		String DINERS = ";36438999960016=20121015432112345601?";	
		String DISCOVER = ";6011000995500000=20121015432112345678?";
		String DISCOVER_MANUAL = "6011000995500000|2012||||";		
		String DISCOVER_CUP = ";6221261111113245=20121011000012312300?";	
		String JCB = ";3566002020140006=20121015432112345678?";			
		String MASTERCARD = ";5454545454545454=20121015432112345601?";
		String MASTERCARD_MANUAL = "5454545454545454|2012||||";
		String MASTERCARD_COM = ";5132850000000008=20121015432112345678?";
		String VISA_ENGLISH = ";4788250000028291=20121015432112345601?"; 
		String VISA_ENGLISH_MANUAL = "4788250000028291|2012|111||12345|"; 		
		String VISA_COMMERCIAL = ";4159280000000009=20121015432112345678?";		
		String VISA_ERROR = "4788999999999999|2012||||";
		 */
		long tranId = System.currentTimeMillis() / 1000;

		String serialCd = "K3EMVCERT00001"; // "K3BK000002";
		String username = "emvcert";
		String password = "NrjRQ*ZPtUyN@5uG3wx4"; //"97028009";
		String attributes = "";
		/*
		EC2ServiceAPI ec = getEC2Service("int");
		//get new password
		EC2ProcessUpdatesResponse response = ec.processUpdates(username, password, serialCd, 0, 2, "EMV Certification", "1.0", "");
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);*/
		
		/*hessianAuthAndSale("int", username, password, serialCd, tranId++, ";4788250000028291=20121015432112345601?", EntryType.SWIPE, 9500L, 0L, "Test", null, attributes, false);
		hessianAuthAndSale("int", username, password, serialCd, tranId++, ";5454545454545454=20121015432112345601?", EntryType.SWIPE, 9500L, 0L, "Test", null, attributes, false);
		hessianAuthAndSale("int", username, password, serialCd, tranId++, ";371449635398431=20121015432112345678?", EntryType.SWIPE, 9500L, 0L, "Test", null, attributes, false);
		hessianAuthAndSale("int", username, password, serialCd, tranId++, ";6011000995500000=20121015432112345678?", EntryType.SWIPE, 9500L, 0L, "Test", null, attributes, false);
		
		hessianAuthAndSale("int", username, password, serialCd, tranId++, ";4788250000028291=20121015432112345601?", EntryType.CONTACTLESS, 9500L, 0L, "Test", null, attributes, false);
		hessianAuthAndSale("int", username, password, serialCd, tranId++, ";5454545454545454=20121015432112345601?", EntryType.CONTACTLESS, 9500L, 0L, "Test", null, attributes, false);
		hessianAuthAndSale("int", username, password, serialCd, tranId++, ";371449635398431=20121015432112345678?", EntryType.CONTACTLESS, 9500L, 0L, "Test", null, attributes, false);
		hessianAuthAndSale("int", username, password, serialCd, tranId++, ";6011000995500000=20121015432112345678?", EntryType.CONTACTLESS, 9500L, 0L, "Test", null, attributes, false);*/
		
		hessianAuth("int", username, password, serialCd, tranId++, ";4788250000028291=20121015432112345601?", EntryType.SWIPE, 0L, attributes);
		hessianAuth("int", username, password, serialCd, tranId++, ";5454545454545454=20121015432112345601?", EntryType.SWIPE, 0L, attributes);
		hessianAuth("int", username, password, serialCd, tranId++, ";6011000995500000=20121015432112345678?", EntryType.SWIPE, 0L, attributes);
		
		hessianAuth("int", username, password, serialCd, tranId++, ";4788250000028291=20121015432112345601?", EntryType.CONTACTLESS, 0L, attributes);
		hessianAuth("int", username, password, serialCd, tranId++, ";5454545454545454=20121015432112345601?", EntryType.CONTACTLESS, 0L, attributes);
		hessianAuth("int", username, password, serialCd, tranId++, ";6011000995500000=20121015432112345678?", EntryType.CONTACTLESS, 0L, attributes);
		
		/*hessianAuthAndSale("int", username, password, serialCd, tranId++, ";5454545454545454=20121015432112345601?", EntryType.SWIPE, 200L, 100L, "Test", null, attributes, false);
		hessianAuthAndSale("int", username, password, serialCd, tranId++, ";5132850000000008=20121015432112345678?", EntryType.CONTACTLESS, 250L, 150L, "Test", null, attributes, false);
		hessianAuthAndSale("int", username, password, serialCd, tranId++, ";5405222222222226=20121015432112345678?", EntryType.SWIPE, 200L, 120L, "Test", null, attributes, false);*/
	}	
	
	@Test
	public void junit_NetConnectToTandemCertification() throws Exception {
		/* Approval codes for Chase test case document can be obtained from the database by running hte query:
		select a.auth_ts, t.tran_received_raw_acct_data, a.auth_amt, t.client_payment_type_cd, a.auth_authority_tran_cd from pss.auth a join pss.tran t on a.tran_id = t.tran_id 
		where a.auth_ts between to_date('09/26/2017 13:40', 'mm/dd/yyyy hh24:mi') and to_date('09/26/2017 13:45', 'mm/dd/yyyy hh24:mi') and t.device_name = 'WS000488'
		and a.auth_type_cd = 'N' order by a.auth_ts, a.auth_id
		*/
		long tranId = System.currentTimeMillis() / 1000;

		String serialCd = "K3EMVCERT00001"; // "K3BK000002";
		String username = "emvcert";
		String password = "NrjRQ*ZPtUyN@5uG3wx4"; //"97028009";
		String attributes = "";
		
		hessianAuthAndSale("int", username, password, serialCd, tranId++, ";4788250000028291=20121015432112345601?", EntryType.SWIPE, 200L, 0L, "Test", null, attributes, false);
		hessianAuthAndSale("int", username, password, serialCd, tranId++, ";5454545454545454=20121015432112345601?", EntryType.SWIPE, 200L, 0L, "Test", null, attributes, false);
		hessianAuthAndSale("int", username, password, serialCd, tranId++, ";371449635398431=20121015432112345678?", EntryType.SWIPE, 200L, 0L, "Test", null, attributes, false);
		hessianAuthAndSale("int", username, password, serialCd, tranId++, ";6011000995500000=20121015432112345678?", EntryType.SWIPE, 200L, 0L, "Test", null, attributes, false);
		
		hessianAuthAndSale("int", username, password, serialCd, tranId++, ";4788250000028291=20121015432112345601?", EntryType.CONTACTLESS, 200L, 0L, "Test", null, attributes, false);
		hessianAuthAndSale("int", username, password, serialCd, tranId++, ";5454545454545454=20121015432112345601?", EntryType.CONTACTLESS, 200L, 0L, "Test", null, attributes, false);
		hessianAuthAndSale("int", username, password, serialCd, tranId++, ";371449635398431=20121015432112345678?", EntryType.CONTACTLESS, 200L, 0L, "Test", null, attributes, false);
		hessianAuthAndSale("int", username, password, serialCd, tranId++, ";6011000995500000=20121015432112345678?", EntryType.CONTACTLESS, 200L, 0L, "Test", null, attributes, false);
		
		hessianAuthAndSale("int", username, password, serialCd, tranId++, ";4788250000028291=20121015432112345601?", EntryType.SWIPE, 300L, 200L, "Test", null, attributes, false);
		hessianAuthAndSale("int", username, password, serialCd, tranId++, ";5454545454545454=20121015432112345601?", EntryType.SWIPE, 300L, 200L, "Test", null, attributes, false);
		hessianAuthAndSale("int", username, password, serialCd, tranId++, ";371449635398431=20121015432112345678?", EntryType.SWIPE, 300L, 200L, "Test", null, attributes, false);
		hessianAuthAndSale("int", username, password, serialCd, tranId++, ";6011000995500000=20121015432112345678?", EntryType.SWIPE, 300L, 200L, "Test", null, attributes, false);
		
		hessianAuthAndSale("int", username, password, serialCd, tranId++, ";4788250000028291=20121015432112345601?", EntryType.CONTACTLESS, 300L, 200L, "Test", null, attributes, false);
		hessianAuthAndSale("int", username, password, serialCd, tranId++, ";5454545454545454=20121015432112345601?", EntryType.CONTACTLESS, 300L, 200L, "Test", null, attributes, false);
		hessianAuthAndSale("int", username, password, serialCd, tranId++, ";371449635398431=20121015432112345678?", EntryType.CONTACTLESS, 300L, 200L, "Test", null, attributes, false);
		hessianAuthAndSale("int", username, password, serialCd, tranId++, ";6011000995500000=20121015432112345678?", EntryType.CONTACTLESS, 300L, 200L, "Test", null, attributes, false);
		
		hessianAuth("int", username, password, serialCd, tranId++, ";4788250000028291=20121015432112345601?", EntryType.SWIPE, 0L, attributes);
		hessianAuth("int", username, password, serialCd, tranId++, ";5454545454545454=20121015432112345601?", EntryType.SWIPE, 0L, attributes);
		hessianAuth("int", username, password, serialCd, tranId++, ";6011000995500000=20121015432112345678?", EntryType.SWIPE, 0L, attributes);
		
		hessianAuth("int", username, password, serialCd, tranId++, ";4788250000028291=20121015432112345601?", EntryType.CONTACTLESS, 0L, attributes);
		hessianAuth("int", username, password, serialCd, tranId++, ";5454545454545454=20121015432112345601?", EntryType.CONTACTLESS, 0L, attributes);
		hessianAuth("int", username, password, serialCd, tranId++, ";6011000995500000=20121015432112345678?", EntryType.CONTACTLESS, 0L, attributes);
	}

	@Test
	public void junit_HessianTandemTest_dev() throws Exception {
		long tranId = System.currentTimeMillis() / 1000;

		String serialCd = "K3MTB000001"; // "K3BK000002";
		String username = "bkrug";
		String password = "k#mqDttpcjMCN4Ag#ZVN"; // "HswFWc2#+tpHCmHzqp*c"; // "33053795";
		String attributes = "longitude=-71.064544\nlatitude=42.28787\ngeocodeTime=" + (System.currentTimeMillis() - 23 * 60 * 60 * 1000L) + "\noemIntegrity=0\ngeocodeProvider=0\n";
		/*
		EC2ServiceAPI ec = getEC2Service("dev");
		EC2ProcessUpdatesResponse response = ec.processUpdates(username, password, serialCd, 0, 2, "BSK Test", "1.1E", "");
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
		// */
		// hessianAuthAndSale("dev", username, password, serialCd, tranId++, ";4118960420454783=1809867921?", EntryType.SWIPE, 500L, 0L, "Whole Lotta Nuttin'", null, null, false);
		// hessianAuthAndSale("dev", username, password, serialCd, tranId++, ";4118960420454783=1809867921?", EntryType.SWIPE, 500L, 50L, "Whole Lotta Nuttin'", null, attributes, false);
		hessianCharge("dev", username, password, serialCd, tranId++, "4055011111111111=2505867921", EntryType.SWIPE, 500L, null, null, attributes);
	}

	@Test
	public void junit_HessianTandemTest_ecc() throws Exception {
		long tranId = System.currentTimeMillis() / 1000;

		EC2ServiceAPI ec = getEC2Service("ecc");
		EC2Response response = ec.authEncrypted("ectest", "e3V2*pa*cn@w#RYSgvXn", "K3MTB000004", tranId, 300, 3, 39, "86505811E9C8C55D1FDD6B156C13D064CF00AAEEC7400B39A3BD46C5B0AFD0C60A59269A644ECB14", "62994996340010200007", "S", "geocodeTime=1436383842000\nlatitude=40.04282785\ngeocodeProvider=0\nlongitude=-75.53648855\noemIntegrity=0");

		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianTandemTest() throws Exception {
		long tranId = System.currentTimeMillis() / 1000;

		String serialCd = "K3BKUOOSP000001";// "K3BKAOMSP000001"; //"K3MTB000008"; //
		// String serialCd = "K3BKCOMSX000001";// "K3BKAOMSP000001"; //"K3MTB000008"; //
		String attributes = "longitude=-71.064544\nlatitude=42.28787\ngeocodeTime=" + (System.currentTimeMillis() - 23 * 60 * 60 * 1000L) + "\noemIntegrity=0\ngeocodeProvider=0\n";
		//
		// hessianAuthAndSale(null, null, null, "K3BKMOOOM000001", tranId, "5454545454545454|1512|||37602|", EntryType.MANUAL, 0L, 0L, null, null, null, false);
		// hessianAuthAndSale(null, null, null, serialCd, tranId++, ";4118960420454783=1809867921?", EntryType.SWIPE, 0L, 0L, null, null, null, false);
		// hessianAuthAndSale(null, null, null, serialCd, tranId++, ";4118960420454783=1809867921?", EntryType.SWIPE, 500L, 0L, null, null, null, false);
		// hessianAuth(null, null, null, serialCd, tranId++, ";4118960420454783=1809867921?", EntryType.CONTACTLESS, 500L, null);

		// hessianAuthAndSale(null, null, null, serialCd, tranId++, ";6011000040000006=20120000000000000000?", EntryType.SWIPE, 500L, 500L, null, null, null, false);
		// hessianSale(null, null, null, serialCd, 1432042153L, 300, null, null, null);
		// $32.64 & $152.64
		// hessianAuth(null, null, null, serialCd, tranId++, "5454545454545454=1505867921", EntryType.SWIPE, 300L, null); // Time out
		// hessianAuthAndSale(null, null, null, serialCd, tranId++, "5454545454545454=1505867921", EntryType.SWIPE, 15500L, 15500L, null, null, null, false);
		// hessianAuthAndSale(null, null, null, serialCd, tranId++, ";4118960420454783=1809867921?", EntryType.CONTACTLESS, 500L, 50L, null, null, attributes, false);
		// hessianAuthAndSale(null, null, null, serialCd, tranId++, "371449123458432=16121015432112345601", EntryType.SWIPE, 500L, 20L, null, null, null, false);
		hessianAuthAndSale(null, null, null, serialCd, tranId++, ";5454545454545454=2505867921?", EntryType.CONTACTLESS, 200L, 200L, null, null, null, false);
		// hessianAuthAndSale(null, null, null, serialCd, tranId++, ";4118960420454783=1809867921?", EntryType.SWIPE, 400L, 450L, null, null, null, true);
		// hessianCharge(null, null, null, serialCd, tranId++, ";4788250000028291=1809867921?", EntryType.SWIPE, 441L, null, null, attributes);
		// hessianAuthAndSale(null, null, null, serialCd, tranId++, "4055011111111111=2505867921", EntryType.SWIPE, 967L, 200L, null, null, attributes, false);
		// hessianAuthAndSale(null, null, null, serialCd, tranId++, "4055011111111111=2505867921", EntryType.SWIPE, 5000L, 200L, null, null, attributes, false);
		// hessianCharge(null, null, null, serialCd, tranId++, "4055011111111111=2505867921", EntryType.SWIPE, 5000L, null, null, attributes);
		// hessianCharge(null, null, null, serialCd, tranId++, "5431390000000003=20060000000000000000?3?", EntryType.SWIPE, 30L, null, null, attributes);

		/*
		EC2ServiceAPI ec = getEC2Service();
		String deviceSerialCd = "K3BKEOOOM000001";// "K3BK000015";
		String username = null; // "bkrug";
		String password = null; // "a8sAmCmeg8jjUfCuu!52";
		log.info("Sending auth for " + deviceSerialCd);
		long amount = 500;
		long saleAmount = 0;
		String[] tracks = { 
				//";5454545454545454=1505867921?", 
		// ";4118960420454783=1809867921?",
				//";4118960420454742=1809867921?", 
				//";6011000040000006=20120000000000000000?", 
		// "371449123458432=16121015432112345601",
		"5454545454545454|1505|111||76522|", // <card number>|<card expiration date>|<card security code>|<card holder name>|<card holder zip code>|<card holder address>
		};
		// String entryType = "C";
		String entryType = "N";
		// String entryType = "S";
		// String tranDetails = "A0|200|40|2|Ping pongs|200|85|2|Yo-yos";
		// String attributes = "requireCVVMatch=true\nrequireAVSMatch=true\nbillingPostalCode=19380\nsecurityCode=1987\nbillingAddress=101 Easy St\n";
		// String attributes = "requireCVVMatch=true\nrequireAVSMatch=true\nsecurityCode=1987\nbillingPostalCode=19380\nbillingAddress=101 Easy St\n";
		// String attributes = "requireCVVMatch=true\nrequireAVSMatch=true\nsecurityCode=1987\nbillingPostalCode=19380\nbillingAddress=101 Easy St\nlatitude=23.0949\nlongitude=98.123\ngeocodeTime=" +
		// (System.currentTimeMillis() - 23 * 60 * 60 * 1000L) + "\n";
		// String attributes = "latitude=23.0949\nlongitude=98.123\ngeocodeTime=" + (System.currentTimeMillis() - 23 * 60 * 60 * 1000L) + "\n";
		// String attributes = "longitude=-71.064544\nlatitude=42.28787\ngeocodeTime=" + (System.currentTimeMillis()) + "\n";
		// String attributes = "longitude=-67.18024\nlatitude=18.363285\ngeocodeTime=" + (System.currentTimeMillis()) + "\n";
		// String attributes = "longitude=-104.574559\nlatitude=49\ngeocodeTime=" + (System.currentTimeMillis()) + "\noemIntegrity=0\ngeocodeProvider=0\n";
		// String attributes = "longitude=-56.330949\nlatitude=46.792244\ngeocodeTime=" + (System.currentTimeMillis()) + "\noemIntegrity=0\ngeocodeProvider=0\n";
		String attributes = null;
		EC2AuthResponse[] responses = new EC2AuthResponse[tracks.length];
		for(int i = 0; i < 1; i++)
			for(int t = 0; t < tracks.length; t++) {
				int offset = i * tracks.length + t;
				responses[offset] = ec.authPlain(username, password, deviceSerialCd, tranId + offset, amount + i * 10, tracks[t], entryType, attributes);
				if(responses[offset] == null)
					log.warn("Received null response");
				else
					log.info(getBasicResponseMessage(responses[offset]));
			}

		for(int i = 0; i < 1; i++)
			for(int t = 0; t < tracks.length; t++) {
				int offset = i * tracks.length + t;
				if(responses[offset].getReturnCode() == EportConnectSOAPTest.RES_APPROVED) {
					long sa = saleAmount + i * 10;
					EC2Response batchResponse = ec.batch(username, password, deviceSerialCd, tranId + offset, sa, sa == 0 ? "C" : "S", "", attributes);
					if(batchResponse == null)
						log.warn("Received null response");
					else
						log.info(getBasicResponseMessage(batchResponse));
				}
			}
		*/
	}

	@Test
	public void junit_HessianTestWS() throws Exception {
		EC2ServiceAPI ec = getEC2Service("int");
		String serialNumber = "K3BK000001";
		String username = "bkrug";
		// String password = "11941958";
		String password = "a5XQC#rJzYCD22dQW^Ad";
		// EC2ProcessUpdatesResponse response = ec.processUpdates(username, password, serialNumber, 0, 2, "ePortMobileAndroid", "1.1A", "androidVersion=lollipop\nphoneNumber=888-111-2222\n");
		// EC2Response response = ec.uploadDeviceInfo(username, password, serialNumber, "androidVersion=lollipop\nphoneNumber=888-111-2222\n");
		long tranId = System.currentTimeMillis() / 1000;
		// String attributes = "longitude=-56.330949\nlatitude=46.792244\ngeocodeTime=" + (System.currentTimeMillis()) + "\noemIntegrity=0\ngeocodeProvider=0\n";
		String attributes = null;
		// EC2AuthResponse response = ec.authPlain(username, password, serialNumber, tranId++, 200, ";5454545454545454=10121015432112345601?", "S", attributes);
		EC2Response response = ec.batch(username, password, serialNumber, 1431627915, 150, "S", "A0|302|10|6|1|305|10|9|1", attributes);

		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianTestProdMobile() throws Exception {
		EC2ServiceAPI ec = getEC2Service("usa");
		String serialNumber = "K3BKCOMSX000001";
		String username = "testaccount1";
		// String password = "11941958";
		String password = "a5XQC#rJzYCD22dQW^Ad";
		EC2ProcessUpdatesResponse response = ec.processUpdates(username, password, serialNumber, 0, 2, "ClientEmulator", "1.0", "software=ClientEmulatorControl\ndeveloper=bkrug\n");
		// EC2Response response = ec.uploadDeviceInfo(username, password, serialNumber, "androidVersion=lollipop\nphoneNumber=888-111-2222\n");
		long tranId = System.currentTimeMillis() / 1000;
		// String attributes = "longitude=-56.330949\nlatitude=46.792244\ngeocodeTime=" + (System.currentTimeMillis()) + "\noemIntegrity=0\ngeocodeProvider=0\n";
		String attributes = null;
		// EC2AuthResponse response = ec.authPlain(username, password, serialNumber, tranId++, 200, ";5454545454545454=10121015432112345601?", "S", attributes);
		// EC2Response response = ec.batch(username, password, serialNumber, 1431627915, 150, "S", "A0|302|10|6|1|305|10|9|1", attributes);

		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianProcessUpdates2() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		EC2ProcessUpdatesResponse response = ec.processUpdates(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, 0, 2, "Crane", "0.1T", "");
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_testHessianProcessUpdates2_07() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		EC2ProcessUpdatesResponse response = ec.processUpdates("testaccount5", "YSa^+9P9vPQZExaYd94v", "K3MTB000007", 0, 2, "ePortMobileAndroid", "1.1A", "");
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
		if(!StringUtils.isBlank(response.getNewPassword())) {
			for(char ch = 'B'; ch < 'E'; ch++) {
				EC2ProcessUpdatesResponse response2 = ec.processUpdates("testaccount5", response.getNewPassword(), "K3MTB000007", 0, 2, "ePortMobileAndroid", "1.1" + ch, "");
				if(response2 == null)
					throw new Exception("Received null response");
				message = getBasicResponseMessage(response2);
				log.info(message);
			}
		}
	}

	@Test
	public void junit_testHessianProcessUpdates3_07() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		EC2ProcessUpdatesResponse response = ec.processUpdates("testaccount5", "3p3FgnQ@EPYMWu2c9eJD", "K3MTB000007", 0, 2, "BSK Test", "1.1E", "");
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_testHessianProcessUpdatesDev() throws Exception {
		EC2ServiceAPI ec = getEC2Service("dev");
		EC2ProcessUpdatesResponse response = ec.processUpdates(null, null, "K3MTB000007", 0, 2, "BSK Test", "1.1E", "");
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianProcessUpdates() throws Exception {
		ECServiceAPI ec = getECService();
		// ECProcessUpdatesResponse response = ec.processUpdates("bkrug", "2U^Erh7F#8PHt@8@emD+", "K3BK000016", 0);// "u@34V3^wtJxQbPx5U+su", "41540199"
		ECProcessUpdatesResponse response = ec.processUpdates("bkrug", "hvWr^4dYaAh4k5P*DzW3", "K3BKAOOMX000001", 0);// "u@34V3^wtJxQbPx5U+su", "41540199"

		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_testHessianProcessUpdates() throws Exception {
		ECServiceAPI ec = getECService();
		ECProcessUpdatesResponse response = ec.processUpdates(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, 0);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_testHessianUploadDeviceInfo2() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		EC2Response response = ec.uploadDeviceInfo(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, "Implementor=NA\nOther=All Kinds of Stuff\n");
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}
	@Test
	public void junit_HessianFileUpload() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		File file = new File("C:\\Users\\bkrug\\Downloads\\version.txt");
		int fileType = 26;
		EC2DataHandler dh = new EC2DataHandler(new ECDataSource(new FileInputStream(file)));
		/*
		byte[] fileBytes = new byte[(int) file.length()];
		InputStream in = new FileInputStream(file);
		try {
			in.read(fileBytes);
		} finally {
			in.close();
		}
		dh.setFileBytes(fileBytes);// */
		EC2Response response = ec.uploadFile(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, file.getName(), fileType, file.length(), dh, null);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_testHessianAuthSalev2() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 821;
		String cardData = ";5454545454545454=10121015432112345601?"; // MC
		String entryType = "S";
		String tranResult = "S";
		String tranDetails = "A0|302|10|6|1|305|10|9|1";
		String attributes = null;
		EC2AuthResponse response = ec.authPlain(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, cardData, entryType, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		if(response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED) {
			log.info(message);
			EC2Response batchResponse = ec.batch(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, tranResult, tranDetails, attributes);
			if(batchResponse == null)
				throw new Exception("Received null response");
			message = getBasicResponseMessage(batchResponse);
			if(batchResponse.getReturnCode() == EportConnectSOAPTest.RES_OK)
				log.info(message);
			else
				throw new Exception(message);
		} else
			throw new Exception(message);
	}

	@Test
	public void junit_testHessianAuthSalev2PrepaidEcc() throws Exception {
		String serial = "K3MTB000005";
		String username = "testaccount3";
		String password = "80233965";
		EC2ServiceAPI ec = getEC2Service("ecc");
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 64;
		String cardData = ";6396212001478860933=1403000082645?"; // Prepaid
		String entryType = "S";
		String tranResult = "S";
		String tranDetails = "A0|302|10|6|1|305|10|9|1";
		String attributes = null;
		EC2AuthResponse response = ec.authPlain(username, password, serial, tranId, amount, cardData, entryType, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		if(response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED) {
			log.info(message);
			EC2Response batchResponse = ec.batch(username, password, serial, tranId, amount, tranResult, tranDetails, attributes);
			if(batchResponse == null)
				throw new Exception("Received null response");
			message = getBasicResponseMessage(batchResponse);
			if(batchResponse.getReturnCode() == EportConnectSOAPTest.RES_OK)
				log.info(message);
			else
				throw new Exception(message);
		} else
			throw new Exception(message);
	}

	@Test
	public void junit_testHessianAuthSalev2Ecc() throws Exception {
		String serial = "K3MTB000005";
		String username = "testaccount3";
		String password = "80233965";
		EC2ServiceAPI ec = getEC2Service("ecc");
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 73;
		String cardData = ";5454545454545454=10121015432112345601?"; // MC
		String entryType = "S";
		String tranResult = "S";
		String tranDetails = "A0|302|10|6|1|305|10|9|1";
		String attributes = null;
		EC2AuthResponse response = ec.authPlain(username, password, serial, tranId, amount, cardData, entryType, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		if(response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED) {
			log.info(message);
			EC2Response batchResponse = ec.batch(username, password, serial, tranId, amount, tranResult, tranDetails, attributes);
			if(batchResponse == null)
				throw new Exception("Received null response");
			message = getBasicResponseMessage(batchResponse);
			if(batchResponse.getReturnCode() == EportConnectSOAPTest.RES_OK)
				log.info(message);
			else
				throw new Exception(message);
		} else
			throw new Exception(message);
	}

	@Test
	public void junit_testHessianChargev2Ecc() throws Exception {
		String serial = "K3MTB000005";
		String username = "testaccount3";
		String password = "80233965";
		EC2ServiceAPI ec = getEC2Service("ecc");
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 820;
		String cardData = ";5454545454545454=10121015432112345601?"; // MC
		String entryType = "S";
		String tranResult = "S";
		String tranDetails = "A0|302|10|6|1|305|10|9|1";
		String attributes = null;
		EC2AuthResponse response = ec.chargePlain(username, password, serial, tranId, amount, cardData, entryType, tranResult, tranDetails, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_testHessianAuthSaleEcc() throws Exception {
		String serial = "K3MTB000005";
		String username = "testaccount3";
		String password = "80233965";
		ECServiceAPI ec = getECService("ecc");
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 82;
		String cardData = ";5454545454545454=10121015432112345601?"; // MC
		String entryType = "S";
		String tranResult = "S";
		String tranDetails = "A0|302|10|6|1|305|10|9|1";
		ECAuthResponse response = ec.authV3(username, password, serial, tranId, amount, cardData, entryType);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		if(response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED) {
			log.info(message);
			ECResponse batchResponse = ec.batchV3(username, password, serial, tranId, amount, tranResult, tranDetails);
			if(batchResponse == null)
				throw new Exception("Received null response");
			message = getBasicResponseMessage(batchResponse);
			if(batchResponse.getReturnCode() == EportConnectSOAPTest.RES_OK)
				log.info(message);
			else
				throw new Exception(message);
		} else
			throw new Exception(message);
	}

	@Test
	public void junit_testHessianAuthSalePrepaidEcc() throws Exception {
		String serial = "K3MTB000005";
		String username = "testaccount3";
		String password = "83611725";
		ECServiceAPI ec = getECService("ecc");
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 820;
		String cardData = ";6396212001478860933=1403000082645?"; // Prepaid
		String entryType = "S";
		String tranResult = "S";
		String tranDetails = "A0|302|10|6|1|305|10|9|1";
		ECAuthResponse response = ec.authV3(username, password, serial, tranId, amount, cardData, entryType);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		if(response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED) {
			log.info(message);
			ECResponse batchResponse = ec.batchV3(username, password, serial, tranId, amount, tranResult, tranDetails);
			if(batchResponse == null)
				throw new Exception("Received null response");
			message = getBasicResponseMessage(batchResponse);
			if(batchResponse.getReturnCode() == EportConnectSOAPTest.RES_OK)
				log.info(message);
			else
				throw new Exception(message);
		} else
			throw new Exception(message);
	}

	@Test
	public void junit_testHessianAuthSale() throws Exception {
		ECServiceAPI ec = getECService();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 73;
		String cardData = ";5454545454545454=10121015432112345601?"; // MC
		String entryType = "S";
		String tranResult = "S";
		String tranDetails = "A0|302|10|6|1|305|10|9|1";
		ECAuthResponse response = ec.authV3(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, cardData, entryType);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		if(response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED) {
			log.info(message);
			ECResponse batchResponse = ec.batchV3(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, tranResult, tranDetails);
			if(batchResponse == null)
				throw new Exception("Received null response");
			message = getBasicResponseMessage(batchResponse);
			if(batchResponse.getReturnCode() == EportConnectSOAPTest.RES_OK)
				log.info(message);
			else
				throw new Exception(message);
		} else
			throw new Exception(message);
	}
	@Test
	public void junit_testHessianChargeCancelv2() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 250;
		String cardData = ";5454545454545454=10121015432112345601?"; // MC
		String entryType = "C";
		String tranResult = "S";
		String tranDetails = "A0|302|20|6|1|305|10|13|1";
		String attributes = null;
		EC2AuthResponse response = ec.chargePlain(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, cardData, entryType, tranResult, tranDetails, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		if(response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED) {
			log.info(message);
			EC2Response batchResponse = ec.batch(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, 0, "C", "", attributes);
			if(batchResponse == null)
				throw new Exception("Received null response");
			message = getBasicResponseMessage(batchResponse);
			if(batchResponse.getReturnCode() == EportConnectSOAPTest.RES_OK)
				log.info(message);
			else
				throw new Exception(message);
		} else
			throw new Exception(message);
	}

	@Test
	public void junit_HessianAuthUnknown() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 350;
		String cardData = "111123409321049=2005002816224"; // Unknown
		String entryType = "S";
		String attributes = null;
		EC2AuthResponse response = ec.authPlain(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, cardData, entryType, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianAuthDriverCard() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 0;
		String cardData = "639621150020000015=121256789";
		String entryType = "S";
		String attributes = null;
		EC2AuthResponse response = ec.authPlain(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, cardData, entryType, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianAuthRandomDev() throws Exception {
		EC2ServiceAPI ec = getEC2Service("dev");
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 350;
		String cardData = LuhnUtils.fillInTrackData(LuhnUtils.generateCard("51", 16)); //
		String entryType = "S";
		String attributes = null;
		for(int i = 0; i < 5; i++) {
			EC2AuthResponse response = ec.authPlain(EportConnectSOAPTest.USERNAME, "30325004", EportConnectSOAPTest.SERIAL_NUMBER, tranId + i, amount, cardData, entryType, attributes);
			if(response == null)
				throw new Exception("Received null response");
			String message = getBasicResponseMessage(response);
			log.info(message);
		}
	}

	@Test
	public void junit_HessianAuthBadCardTypeDev() throws Exception {
		ECServiceAPI ec = getECService("dev");
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 350;
		String cardData = LuhnUtils.fillInTrackData(LuhnUtils.generateCard("51", 16)); //
		String cardType = "M";
		String tranResult = "S";
		String tranDetails = "A0|200|90|2|Ping pongs|200|85|2|Yo-yos";
		ECAuthResponse response = ec.chargeV3(EportConnectSOAPTest.USERNAME, "30325004", EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, cardData, cardType, tranResult, tranDetails);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianAuthBadCardType() throws Exception {
		ECServiceAPI ec = getECService();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 350;
		String cardData = LuhnUtils.fillInTrackData(LuhnUtils.generateCard("51", 16)); //
		String cardType = "M";
		String tranResult = "S";
		String tranDetails = "A0|200|90|2|Ping pongs|200|85|2|Yo-yos";
		ECAuthResponse response = ec.chargeV3(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, cardData, cardType, tranResult, tranDetails);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianAuthBadTranResult() throws Exception {
		ECServiceAPI ec = getECService();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 350;
		String cardData = LuhnUtils.fillInTrackData(LuhnUtils.generateCard("51", 16)); //
		String cardType = "N";
		String tranResult = "?";
		String tranDetails = "A0|200|90|2|Ping pongs|200|85|2|Yo-yos";
		ECAuthResponse response = ec.chargeV3(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, cardData, cardType, tranResult, tranDetails);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianAuthBadTranId() throws Exception {
		ECServiceAPI ec = getECService();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 350;
		String cardData = LuhnUtils.fillInTrackData(LuhnUtils.generateCard("51", 16)); //
		String cardType = "N";
		String tranResult = "S";
		String tranDetails = "A0|200|90|2|Ping pongs|200|85|2|Yo-yos";
		ECAuthResponse response = ec.chargeV3(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, -1, amount, cardData, cardType, tranResult, tranDetails);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianAuthBadAmount() throws Exception {
		ECServiceAPI ec = getECService();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = -350;
		String cardData = LuhnUtils.fillInTrackData(LuhnUtils.generateCard("51", 16)); //
		String cardType = "N";
		String tranResult = "S";
		String tranDetails = "A0|200|90|2|Ping pongs|200|85|2|Yo-yos";
		ECAuthResponse response = ec.chargeV3(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, cardData, cardType, tranResult, tranDetails);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianAuthZeroAmount() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 0;
		String cardData = LuhnUtils.fillInTrackData(LuhnUtils.generateCard("51", 16)); //
		String entryType = "C";
		String attributes = "";
		EC2AuthResponse response = ec.authPlain(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, cardData, entryType, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianAuthBadCard() throws Exception {
		ECServiceAPI ec = getECService();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 350;
		String cardData = "4055011111111111|2006|222||99999|"; // Manual
		String cardType = "N";
		String tranResult = "S";
		String tranDetails = "";
		ECAuthResponse response = ec.chargeV3(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, cardData, cardType, tranResult, tranDetails);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianAuthBadCvv_dev() throws Exception {
		EC2ServiceAPI ec = getEC2Service("dev");
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 350;
		String cardData = "4055011111111111|2006|222||99999|"; // Manual
		String entryType = "N";
		String tranResult = "S";
		String tranDetails = "A0|200|350|1|Water Slide";
		String attributes = null;
		EC2AuthResponse response = ec.authPlain(EportConnectSOAPTest.USERNAME, "30325004", EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, cardData, entryType, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
		switch(response.getReturnCode()) {
			case ECResponse.RES_PARTIALLY_APPROVED:
				amount = response.getApprovedAmount();
			case ECResponse.RES_APPROVED:
				EC2Response batchResponse = ec.batch(EportConnectSOAPTest.USERNAME, "30325004", EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, tranResult, tranDetails, attributes);
				if(batchResponse == null)
					throw new Exception("Received null response");
				message = getBasicResponseMessage(batchResponse);
				log.info(message);
				break;
			case ECResponse.RES_CVV_MISMATCH:
			case ECResponse.RES_AVS_MISMATCH:
			case ECResponse.RES_CVV_AND_AVS_MISMATCH:
				batchResponse = ec.batch(EportConnectSOAPTest.USERNAME, "30325004", EportConnectSOAPTest.SERIAL_NUMBER, tranId, 0, "C", "", attributes);
				if(batchResponse == null)
					throw new Exception("Received null response");
				message = getBasicResponseMessage(batchResponse);
				log.info(message);
				break;
		}
	}

	@Test
	public void junit_HessianAuthBadAll() throws Exception {
		ECServiceAPI ec = getECService();
		long tranId = -1;
		long amount = -350;
		String cardData = "";
		String cardType = "M";
		String tranResult = "-";
		String tranDetails = "";
		ECAuthResponse response = ec.chargeV3(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, cardData, cardType, tranResult, tranDetails);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianChargeEc2BadAll() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = -31;
		long amount = -350;
		String entryType = "M";
		String tranResult = "Z";
		String tranDetails = "A0|200|90|2|Ping pongs|200|85|2|Yo-yos";
		int cardReaderType = 9;
		int decryptedCardDataLen = -4;
		String encryptedCardDataHex = "GHIJKL";
		String ksnHex = "ABCDEF12345G";
		String attributes = "";
		EC2AuthResponse response = ec.chargeEncrypted(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, cardReaderType, decryptedCardDataLen, encryptedCardDataHex, ksnHex, entryType, tranResult, tranDetails, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianChargeEc2BadEnc() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = -1;
		long amount = -350;
		String entryType = "M";
		String tranResult = "-";
		String tranDetails = "";
		int cardReaderType = 9;
		int decryptedCardDataLen = -4;
		String encryptedCardDataHex = "GHIJKL";
		String ksnHex = "ABCDEF12345";
		String attributes = "";
		EC2AuthResponse response = ec.chargeEncrypted(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, cardReaderType, decryptedCardDataLen, encryptedCardDataHex, ksnHex, entryType, tranResult, tranDetails, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianChargeRandomInt() throws Exception {
		EC2ServiceAPI ec = getEC2Service("int");
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 635;
		String cardData = LuhnUtils.fillInTrackData(LuhnUtils.generateCard("51", 16)); //
		String entryType = "S";
		String tranResult = "S";
		String tranDetails = "";
		// String tranDetails = "A0|200|90|2|Ping pongs|200|85|2|Yo-yos";
		// String tranDetails = "A0|403|14635|1";
		String attributes = null;
		for(int i = 0; i < 2; i++) {
			EC2AuthResponse response = ec.chargePlain(EportConnectSOAPTest.USERNAME, "18602545", EportConnectSOAPTest.SERIAL_NUMBER, tranId + i, amount, cardData, entryType, tranResult, tranDetails, attributes);
			if(response == null)
				throw new Exception("Received null response");
			String message = getBasicResponseMessage(response);
			log.info(message);
		}
	}

	@Test
	public void junit_HessianChargeRandomDev() throws Exception {
		EC2ServiceAPI ec = getEC2Service("dev");
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 200;
		String cardData = LuhnUtils.fillInTrackData(LuhnUtils.generateCard("42", 16)); //
		String entryType = "S";
		String tranResult = "S";
		String tranDetails = "";
		// String tranDetails = "A0|200|90|2|Ping pongs|200|85|2|Yo-yos";
		// String tranDetails = "A0|403|14635|1";
		String attributes = null;
		for(int i = 0; i < 3; i++) {
			EC2AuthResponse response = ec.chargePlain(EportConnectSOAPTest.USERNAME, "30325004", EportConnectSOAPTest.SERIAL_NUMBER, tranId + i, amount, cardData, entryType, tranResult, tranDetails, attributes);
			if(response == null)
				throw new Exception("Received null response");
			String message = getBasicResponseMessage(response);
			log.info(message);
		}
	}

	@Test
	public void junit_HessianChargeRandomWithZip_dev() throws Exception {
		EC2ServiceAPI ec = getEC2Service("dev");
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 250;
		String cardData = LuhnUtils.fillInTrackData(LuhnUtils.generateCard("41", 16)); //
		String entryType = "S";
		String tranResult = "S";
		String tranDetails = "";
		// String tranDetails = "A0|200|90|2|Ping pongs|200|85|2|Yo-yos";
		// String tranDetails = "A0|403|14635|1";
		String attributes = "billingPostalCode=85909";
		for(int i = 0; i < 2; i++) {
			EC2AuthResponse response = ec.chargePlain(EportConnectSOAPTest.USERNAME, "30325004", EportConnectSOAPTest.SERIAL_NUMBER, tranId + i, amount, cardData, entryType, tranResult, tranDetails, attributes);
			if(response == null)
				throw new Exception("Received null response");
			String message = getBasicResponseMessage(response);
			log.info(message);
		}
	}
	@Test
	public void junit_HessianChargePaymenttechWithZip_dev() throws Exception {
		EC2ServiceAPI ec = getEC2Service("dev");
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 350;
		String cardData = LuhnUtils.fillInTrackData(LuhnUtils.generateCard("41", 16)); //
		String entryType = "S";
		String tranResult = "S";
		String tranDetails = "";
		// String tranDetails = "A0|200|90|2|Ping pongs|200|85|2|Yo-yos";
		// String tranDetails = "A0|403|14635|1";
		String attributes = "billingPostalCode=C6A1B2";
		for(int i = 0; i < 2; i++) {
			EC2AuthResponse response = ec.chargePlain(EportConnectSOAPTest.USERNAME, "09936219", "K3MTB000003", tranId + i, amount, cardData, entryType, tranResult, tranDetails, attributes);
			if(response == null)
				throw new Exception("Received null response");
			String message = getBasicResponseMessage(response);
			log.info(message);
		}
	}

	@Test
	public void junit_HessianChargeSmallDev() throws Exception {
		EC2ServiceAPI ec = getEC2Service("dev");
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 5;
		String cardData = LuhnUtils.fillInTrackData(LuhnUtils.generateCard("41", 16)); //
		String entryType = "S";
		String tranResult = "S";
		StringBuilder sb = new StringBuilder();
		sb.append("A0|200|").append(amount).append("|1|Small Change");
		String tranDetails = sb.toString();
		String attributes = null;
		for(int i = 0; i < 4; i++) {
			EC2AuthResponse response = ec.chargePlain(EportConnectSOAPTest.USERNAME, "30325004", EportConnectSOAPTest.SERIAL_NUMBER, tranId + i, amount, cardData, entryType, tranResult, tranDetails, attributes);
			if(response == null)
				throw new Exception("Received null response");
			String message = getBasicResponseMessage(response);
			log.info(message);
		}
	}

	@Test
	public void junit_HessianChargeRandom() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 235;
		String entryType = "S";
		String tranResult = "S";
		// String tranDetails = "";
		// String tranDetails = "A0|200|90|2|Ping pongs|200|85|2|Yo-yos";
		String tranDetails = "A0|200|235|1|Gunk";
		String attributes = null;
		for(int i = 0; i < 1; i++) {
			String cardData = LuhnUtils.fillInTrackData(LuhnUtils.generateCard("40", 16)); //
			EC2AuthResponse response = ec.chargePlain(null, null, EportConnectSOAPTest.SERIAL_NUMBER, tranId + i, amount, cardData, entryType, tranResult, tranDetails, attributes);
			if(response == null)
				throw new Exception("Received null response");
			String message = getBasicResponseMessage(response);
			log.info(message);
		}
	}

	@Test
	public void junit_HessianChargeRandomVisaDebit() throws Exception {
		final EC2ServiceAPI ec = getEC2Service();
		final AtomicLong tranId = new AtomicLong(System.currentTimeMillis() / 1000);
		final long amount = 235;
		final String entryType = "C";
		final String tranResult = "S";
		// String tranDetails = "";
		// String tranDetails = "A0|200|90|2|Ping pongs|200|85|2|Yo-yos";
		final String tranDetails = "A0|200|235|1|Gunk";
		final String attributes = null;
		final String cardData = LuhnUtils.fillInTrackData(LuhnUtils.generateCard("400040000", 16)); //
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				EC2AuthResponse response = ec.chargePlain(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId.getAndIncrement(), amount, cardData, entryType, tranResult, tranDetails, attributes);
				if(response == null) {
					log.error("Received null response");
					return;
				}
				String message = getBasicResponseMessage(response);
				log.info(message);
			}
		};
		Thread[] threads = new Thread[10];
		for(int i = 0; i < threads.length; i++)
			threads[i] = new Thread(runnable);
		for(int i = 0; i < threads.length; i++)
			threads[i].start();
		for(int i = 0; i < threads.length; i++)
			threads[i].join();

	}

	@Test
	public void junit_HessianChargeApriva() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 235;
		String cardData = "9999901301=200112345";
		String entryType = "S";
		String tranResult = "S";
		// String tranDetails = "";
		// String tranDetails = "A0|200|90|2|Ping pongs|200|85|2|Yo-yos";
		String tranDetails = "A0|200|" + amount + "|1|Item #1";
		String attributes = null;
		EC2AuthResponse response = ec.chargePlain(EportConnectSOAPTest.USERNAME, "52927513", "K3TS1416329263307", tranId, amount, cardData, entryType, tranResult, tranDetails, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_kioskChargeCBORD() throws Exception {
		// K2MTB000023
		String card = "9999901501";// 9999901501=200112345
		String deviceName = "EV100139";
		ClientEmulator ce = new ClientEmulator(host, unifedlayerPort, deviceName, keys.get(host).get(deviceName));
		gx32AuthAndSale(ce, ce.getPort(), ce.getPort(), 7, EntryType.SWIPE, card, 0, 450, 345, true, null);
	}

	@Test
	public void junit_HessianChargeDiscoverDebit() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 450;
		String cardData = "6424130000000000=200112345";
		String entryType = "S";
		String tranResult = "S";
		// String tranDetails = "";
		// String tranDetails = "A0|200|90|2|Ping pongs|200|85|2|Yo-yos";
		String tranDetails = "A0|200|" + amount + "|1|Item #1";
		String attributes = null;
		EC2AuthResponse response = ec.chargePlain(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, cardData, entryType, tranResult, tranDetails, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianChargeAndCancel() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 200;
		String cardData = LuhnUtils.fillInTrackData(LuhnUtils.generateCard("51", 16)); //
		String entryType = "S";
		String tranResult = "S";
		// String tranDetails = "";
		// String tranDetails = "A0|200|90|2|Ping pongs|200|85|2|Yo-yos";
		String tranDetails = "A0|200|200|1|Gunk";
		String attributes = null;
		log.info("Sending charge #" + tranId + " with card " + cardData);
		EC2Response response = ec.chargePlain(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, cardData, entryType, tranResult, tranDetails, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
		JOptionPane.showConfirmDialog(null, "Shall I cancel the charge?");
		log.info("Canceling charge #" + tranId + " with card " + cardData);
		response = ec.batch(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, 0, "C", tranDetails, attributes);
		if(response == null)
			throw new Exception("Received null response");
		message = getBasicResponseMessage(response);
		log.info(message);

	}

	@Test
	public void junit_HessianChargeManualDev() throws Exception {
		EC2ServiceAPI ec = getEC2Service("dev");
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 200;
		String cardNumber = "5454545454545454"; // MessageResponseUtils.getCardNumber(LuhnUtils.fillInTrackData(LuhnUtils.generateCard("51", 16))); //
		// <card number>|<card expiration date>|<card security code>|<card holder name>|<card holder zip code>|<card holder address>
		StringBuilder sb = new StringBuilder();
		sb.append(cardNumber).append('|');
		sb.append("1505").append('|'); // expDate - YYMM
		sb.append("123").append('|');// cvv
		sb.append("").append('|'); // name
		sb.append("").append('|');// postal
		sb.append(""); // address
		String cardData = sb.toString();

		String entryType = "N";
		String tranResult = "S";
		// String tranDetails = "";
		// String tranDetails = "A0|200|90|2|Ping pongs|200|85|2|Yo-yos";
		String tranDetails = "A0|403|" + amount + "|1|Just Cuz";
		String attributes = null;
		for(int i = 0; i < 1; i++) {
			EC2AuthResponse response = ec.chargePlain(EportConnectSOAPTest.USERNAME, "30325004", EportConnectSOAPTest.SERIAL_NUMBER, tranId + i, amount, cardData, entryType, tranResult, tranDetails, attributes);
			if(response == null)
				throw new Exception("Received null response");
			String message = getBasicResponseMessage(response);
			log.info(message);
		}
	}

	@Test
	public void junit_HessianChargeManual() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 200;
		String cardNumber = "5454545454545454"; // MessageResponseUtils.getCardNumber(LuhnUtils.fillInTrackData(LuhnUtils.generateCard("51", 16))); //
		// <card number>|<card expiration date>|<card security code>|<card holder name>|<card holder zip code>|<card holder address>
		StringBuilder sb = new StringBuilder();
		sb.append(cardNumber).append('|');
		sb.append("1505").append('|'); // expDate - YYMM
		sb.append("1234").append('|');// cvv
		sb.append("").append('|'); // name
		sb.append("19350").append('|');// postal
		sb.append(""); // address
		String cardData = sb.toString();

		String entryType = "N";
		String tranResult = "S";
		// String tranDetails = "";
		// String tranDetails = "A0|200|90|2|Ping pongs|200|85|2|Yo-yos";
		String tranDetails = "A0|403|" + amount + "|1|Just Cuz";
		String attributes = null;
		for(int i = 0; i < 1; i++) {
			EC2AuthResponse response = ec.chargePlain(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId + i, amount, cardData, entryType, tranResult, tranDetails, attributes);
			if(response == null)
				throw new Exception("Received null response");
			String message = getBasicResponseMessage(response);
			log.info(message);
		}
	}

	@Test
	public void junit_HessianChargeManualMissingZip() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 200;
		String cardNumber = "5454545454545454"; // MessageResponseUtils.getCardNumber(LuhnUtils.fillInTrackData(LuhnUtils.generateCard("51", 16))); //
		// <card number>|<card expiration date>|<card security code>|<card holder name>|<card holder zip code>|<card holder address>
		StringBuilder sb = new StringBuilder();
		sb.append(cardNumber).append('|');
		sb.append("1505").append('|'); // expDate - YYMM
		sb.append("1987").append('|');// cvv
		sb.append("").append('|'); // name
		sb.append("").append('|');// postal
		sb.append("123 Main St"); // address
		String cardData = sb.toString();

		String entryType = "N";
		String tranResult = "S";
		// String tranDetails = "";
		// String tranDetails = "A0|200|90|2|Ping pongs|200|85|2|Yo-yos";
		String tranDetails = "A0|403|" + amount + "|1|Just Cuz";
		String attributes = null;
		for(int i = 0; i < 1; i++) {
			EC2AuthResponse response = ec.chargePlain(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId + i, amount, cardData, entryType, tranResult, tranDetails, attributes);
			if(response == null)
				throw new Exception("Received null response");
			String message = getBasicResponseMessage(response);
			log.info(message);
		}
	}

	@Test
	public void junit_CalcDiffTrackSameCRC() {
		String cardData = "6396212009972335492=2005002816224";
		int playdigits = 9;
		int calcCrc = InteractionUtils.getCRC16(cardData);
		byte[] cardArray = cardData.getBytes();
		int start = cardData.length() - playdigits;
		int end = cardData.length() - 1;
		for(int i = start; i <= end; i++)
			cardArray[i] = '0';
		int max = (int) Math.pow(10, (end - start + 1));
		int n;
		for(n = 1; n < max; n++) {
			String tryData = new String(cardArray);
			if(!tryData.equals(cardData)) {
				int tryCrc = InteractionUtils.getCRC16(tryData);
				if(tryCrc == calcCrc) {
					log.info("Found match: crc of " + tryData + " matches " + cardData + " (" + calcCrc + ")");
					return;
				}
			}
			String s = StringUtils.pad(Integer.toString(n), '0', end - start + 1, Justification.RIGHT);
			getBytesFromString(s, 0, s.length(), cardArray, start);

		}
	}

	@SuppressWarnings("deprecation")
	protected void getBytesFromString(String s, int srcBegin, int srcEnd, byte[] dst, int dstBegin) {
		s.getBytes(srcBegin, srcEnd, dst, dstBegin);
	}
	@Test
	public void junit_HessianAuthAndSalePrepaidLocalCAD() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 350;
		String cardData = "6396212009972335492=2005002816224"; // Good
		// String cardData = "6396212009972335492=2005000054591"; // Bad track - same crc
		String entryType = "S";
		String tranResult = "S";
		String tranDetails = "A0|200|90|2|Ping pongs|200|85|2|Yo-yos";
		String attributes = null;
		EC2AuthResponse response = ec.authPlain(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, cardData, entryType, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		if(response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED) {
			log.info(message);
			EC2Response batchResponse = ec.batch(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, tranResult, tranDetails, attributes);
			if(batchResponse == null)
				throw new Exception("Received null response");
			message = getBasicResponseMessage(batchResponse);
			if(batchResponse.getReturnCode() == EportConnectSOAPTest.RES_OK)
				log.info(message);
			else
				throw new Exception(message);
		} else
			throw new Exception(message);
	}

	@Test
	public void junit_HessianAuthAndSaleIsis() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 250;
		String cardData = ";4118960420454783=1809867921?"; // LuhnUtils.fillInTrackData(LuhnUtils.generateCard("41", 16)); // "4120082045889289=1809139965"; //"4197749956184052=1809227955";
															// //";4143464493714065=1809768709?"; //
		String entryType = "I";
		String tranResult = "S";
		String tranDetails = "A0|200|40|2|Ping pongs|200|85|2|Yo-yos";
		String attributes = null;
		log.info("Sending Plain Auth for card " + cardData);
		EC2AuthResponse response = ec.authPlain(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, cardData, entryType, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		if(response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED) {
			log.info(message);
			EC2Response batchResponse = ec.batch(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, tranResult, tranDetails, attributes);
			if(batchResponse == null)
				throw new Exception("Received null response");
			message = getBasicResponseMessage(batchResponse);
			if(batchResponse.getReturnCode() == EportConnectSOAPTest.RES_OK)
				log.info(message);
			else
				throw new Exception(message);
		} else
			throw new Exception(message);
	}

	@Test
	public void junit_HessianBatch() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 250;
		String tranResult = ""; // "TRAN_RESULT_CANCELED"; // "S";
		String tranDetails = "A0|200|40|2|Ping pongs|200|85|2|Yo-yos";
		String attributes = null;
		EC2Response batchResponse = ec.batch(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, tranResult, tranDetails, attributes);
		if(batchResponse == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(batchResponse);
		log.info(message);
	}

	@Test
	public void junit_HessianAuthAndCancelEncrypted() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 250;
		String maskedTrack = ";6011000040000000=20120000000000000000?";// Discover
		String encryptedCardDataHex = "F84D37ADF8BEE6EDB9185FA7792CC4923D130C2662CEAE4E845448B7A40B88CDB4A0320B4C74F729";
		String ksnHex = "9011040B005166000055";
		String entryType = "S";
		// String tranResult = "S";
		// String tranDetails = "A0|200|40|2|Ping pongs|200|85|2|Yo-yos";
		String attributes = "requireCVVMatch=true\nrequireAVSMatch=true\nbillingPostalCode=19380\nsecurityCode=1976\nbillingAddress=101 Easy St\n";
		// String attributes = null;
		EC2AuthResponse response = ec.authEncrypted(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, 1, maskedTrack.length(), encryptedCardDataHex, ksnHex, entryType, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		if(response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED) {
			log.info(message);
			EC2Response batchResponse = ec.batch(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, 0, "C", "", attributes);
			if(batchResponse == null)
				throw new Exception("Received null response");
			message = getBasicResponseMessage(batchResponse);
			if(batchResponse.getReturnCode() == EportConnectSOAPTest.RES_OK)
				log.info(message);
			else
				throw new Exception(message);
		} else
			throw new Exception(message);
	}

	@Test
	public void junit_HessianAuthAndCancelEncrypted_int() throws Exception {
		EC2ServiceAPI ec = getEC2Service("int");
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 250;
		String maskedTrack = ";6011000040000000=20120000000000000000?";// Discover
		String encryptedCardDataHex = "F84D37ADF8BEE6EDB9185FA7792CC4923D130C2662CEAE4E845448B7A40B88CDB4A0320B4C74F729";
		String ksnHex = "9011040B005166000055";
		String entryType = "S";
		// String tranResult = "S";
		// String tranDetails = "A0|200|40|2|Ping pongs|200|85|2|Yo-yos";
		String attributes = "requireCVVMatch=true\nrequireAVSMatch=true\nbillingPostalCode=19380\nsecurityCode=1976\nbillingAddress=101 Easy St\n";
		// String attributes = null;
		EC2AuthResponse response = ec.authEncrypted(EportConnectSOAPTest.USERNAME, "96343409", EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, 1, maskedTrack.length(), encryptedCardDataHex, ksnHex, entryType, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		if(response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED) {
			log.info(message);
			EC2Response batchResponse = ec.batch(EportConnectSOAPTest.USERNAME, "96343409", EportConnectSOAPTest.SERIAL_NUMBER, tranId, 0, "C", "", attributes);
			if(batchResponse == null)
				throw new Exception("Received null response");
			message = getBasicResponseMessage(batchResponse);
			if(batchResponse.getReturnCode() == EportConnectSOAPTest.RES_OK)
				log.info(message);
			else
				throw new Exception(message);
		} else
			throw new Exception(message);
	}

	@Test
	public void junit_HessianAuthAndCancel() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 250;
		String track = ";5454545454545454=1505867921?";// ";4118960420454783=1809867921?"; // ";4118960420454742=1809867921?";// ";6011000040000000=20120000000000000000?";// Discover
		String entryType = "S";
		String password = "81472067"; // EportConnectSOAPTest.PASSWORD;
		// String tranResult = "S";
		// String tranDetails = "A0|200|40|2|Ping pongs|200|85|2|Yo-yos";
		// String attributes = "requireCVVMatch=true\nrequireAVSMatch=true\nbillingPostalCode=19380\nsecurityCode=1987\nbillingAddress=101 Easy St\n";
		String attributes = "requireCVVMatch=true\nrequireAVSMatch=true\nsecurityCode=1987\nbillingPostalCode=19380\nbillingAddress=101 Easy St\n";
		// String attributes = null;
		EC2AuthResponse response = ec.authPlain(EportConnectSOAPTest.USERNAME, password, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, track, entryType, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		if(response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED) {
			log.info(message);
			EC2Response batchResponse = ec.batch(EportConnectSOAPTest.USERNAME, password, EportConnectSOAPTest.SERIAL_NUMBER, tranId, 0, "C", "", attributes);
			if(batchResponse == null)
				throw new Exception("Received null response");
			message = getBasicResponseMessage(batchResponse);
			if(batchResponse.getReturnCode() == EportConnectSOAPTest.RES_OK)
				log.info(message);
			else
				throw new Exception(message);
		} else
			throw new Exception(message);
	}

	@Test
	public void junit_HessianAuthBlacklisted_ecc() throws Exception {
		EC2ServiceAPI ec = getEC2Service("ecc");
		String serial = "K3MTB000005";
		String username = "testaccount3";
		String password = "80233965";
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 250;
		String track = "4055011111111111=10121015432112345678";
		String entryType = "S";
		String attributes = null;
		EC2AuthResponse response = ec.authPlain(username, password, serial, tranId, amount, track, entryType, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianAuthAndCancelManual() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		String cardNumber = "4141237418974952"; // MessageResponseUtils.getCardNumber(LuhnUtils.fillInTrackData("41*************2"));
		// <card number>|<card expiration date>|<card security code>|<card holder name>|<card holder zip code>|<card holder address>
		StringBuilder sb = new StringBuilder();
		sb.append(cardNumber).append('|');
		sb.append("1505").append('|'); // expDate - YYMM
		sb.append("1234").append('|');// cvv
		sb.append("").append('|'); // name
		sb.append("19350").append('|');// postal
		sb.append(""); // address
		String cardData = sb.toString();

		String entryType = "N";
		String attributes = "requireCVVMatch=true\nrequireAVSMatch=true\n";
		log.info("Sending auth and cancel with card '" + cardNumber + "'");
		EC2AuthResponse response = ec.authPlain(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, 1, cardData, entryType, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
		switch(response.getReturnCode()) {
			case ECResponse.RES_APPROVED:
			case ECResponse.RES_PARTIALLY_APPROVED:
			case ECResponse.RES_AVS_MISMATCH:
			case ECResponse.RES_CVV_AND_AVS_MISMATCH:
			case ECResponse.RES_CVV_MISMATCH:
				EC2Response batchResponse = ec.batch(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, 0, "C", "", attributes);
				if(batchResponse == null)
					throw new Exception("Received null response");
				message = getBasicResponseMessage(batchResponse);
				log.info(message);
		}
	}

	@Test
	public void junit_HessianTokenizeCard() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		String cardNumber = MessageResponseUtils.getCardNumber(LuhnUtils.fillInTrackData(LuhnUtils.generateCard("55", 16))); // "5454545454545454"; //"6396212009338376453"; //
		// String cardNumber = "6396212009338376453";
		// String cardNumber = "5454545454545454";
		log.info("Sending Tokenize with card '" + cardNumber + "'");
		String attributes = null;
		for(int i = 0; i < 1; i++) {
			EC2TokenResponse response = ec.tokenizePlain(EportConnectSOAPTest.USERNAME, "12837963", EportConnectSOAPTest.SERIAL_NUMBER, tranId + i, cardNumber, "2012", "111", null, "19355", null, attributes);
			if(response == null)
				throw new Exception("Received null response");
			String message = getBasicResponseMessage(response);
			log.info(message);
		}
	}

	@Test
	public void junit_HessianTokenizeRandomCard() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		String cardNumber = MessageResponseUtils.getCardNumber(LuhnUtils.fillInTrackData(LuhnUtils.generateCard("41", 16)));
		String attributes = null;
		for(int i = 0; i < 3; i++) {
			log.info("Sending Tokenize with card '" + cardNumber + "'; tranId=" + (tranId + i));
			EC2TokenResponse response = ec.tokenizePlain(null, null, EportConnectSOAPTest.SERIAL_NUMBER, tranId + i, cardNumber, "2012", "111", null, "19355", null, attributes);
			if(response == null)
				throw new Exception("Received null response");
			String message = getBasicResponseMessage(response);
			log.info(message);
		}
	}

	@Test
	public void junit_HessianTokenizeCard_dev() throws Exception {
		EC2ServiceAPI ec = getEC2Service("dev");
		long tranId = System.currentTimeMillis() / 1000;
		String cardNumber = MessageResponseUtils.getCardNumber(LuhnUtils.fillInTrackData(LuhnUtils.generateCard("41", 16))); // "5454545454545454"; //"6396212009338376453"; //
		log.info("Sending Tokenize with card '" + cardNumber + "'");
		String attributes = null;
		EC2TokenResponse response = ec.tokenizePlain(EportConnectSOAPTest.USERNAME, "30325004", EportConnectSOAPTest.SERIAL_NUMBER, tranId, cardNumber, "1612", "111", null, "19355", null, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianTokenizeMC() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		String cardNumber = "5454545454545454"; // MessageResponseUtils.getCardNumber(LuhnUtils.fillInTrackData(LuhnUtils.generateCard("41", 16))); // "5454545454545454"; //
		log.info("Sending Tokenize with card '" + cardNumber + "'");
		String attributes = null;
		EC2TokenResponse response = ec.tokenizePlain(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, cardNumber, "1505", "123", null, "19355", null, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianTokenizePrepaid() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		String cardNumber = "6396212009338376453";
		String securityCd = "9668";
		String postalCd = "19355";
		String expDate = "9912";
		log.info("Sending Tokenize with card '" + cardNumber + "'");
		String attributes = null;
		EC2TokenResponse response = ec.tokenizePlain(null, null, "V3-1-USD", tranId, cardNumber, expDate, securityCd, null, postalCd, null, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);// 10A797E5F5D96A78529A2D2583E73347
	}

	@Test
	public void junit_HessianRetokenizePrepaid() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		// String cardNumber = "6396212009338376453";
		long cardId = 126053; // 125725;
		String securityCd = "7805"; // "9668";
		String postalCd = "19355";
		// String expDate = "9912";
		log.info("Sending Retokenize for card " + cardId);
		String attributes = null;
		EC2TokenResponse response = ec.retokenize(null, null, "V3-1-USD", cardId, securityCd, null, postalCd, null, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}
	
	@Test
	public void junit_HessianTokenizePrepaid_devTest() throws Exception {
		EC2ServiceAPI ec = getEC2Service("dev");
		long tranId = System.currentTimeMillis() / 1000;
		String cardNumber = "6396212001958207019";
		String securityCd = "3602";
		String postalCd = "19355";
		String expDate = "2008";
		log.info("Sending Tokenize with card '" + cardNumber + "'");
		String attributes = null;
		EC2TokenResponse response = ec.tokenizePlain(null, null, "V3-1-USD", tranId, cardNumber, expDate, securityCd, null, postalCd, null, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);// 10A797E5F5D96A78529A2D2583E73347
	}
	
	@Test
	public void junit_HessianGetCardInfoPrepaidToken_devTest() throws Exception {
		//6396212001958207019=1809000201147, 3602, 125666
		EC2ServiceAPI ec = getEC2Service("dev");
		String cardId="125666";
		String token = "3837EE17A44C2D668697582F12202E40";
		StringBuilder sb = new StringBuilder();
		sb.append(cardId).append('|').append(token);
		String cardData = sb.toString();
		String entryType = "K";
		String attributes = null;
		EC2CardInfo response = ec.getCardInfoPlain(null,null, "V3-1-USD", cardData, entryType, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianPrepaidUntoken_devTest() throws Exception {
		//6396212001958207019=1809000201147, 3602, 125666
		EC2ServiceAPI ec = getEC2Service("dev");
		long cardId=125666;
		String token = "F390C974406F727B142EA674E3749427"; // MC
		String attributes = null;
		EC2Response response = ec.untokenize(null,null, "V3-1-USD", cardId, token, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}
	
	@Test
	public void junit_HessianChargeTokenPrepaid_devTest() throws Exception {
		EC2ServiceAPI ec = getEC2Service("dev");
		long tranId = System.currentTimeMillis() / 1000;
		long cardId = 125666;
		String token = "3837EE17A44C2D668697582F12202E40";
		long amount = 100;
		String entryType = "K";
		String tranResult = "S";
		// String tranDetails = "";
		// String tranDetails = "A0|200|90|2|Ping pongs|200|85|2|Yo-yos";
		String tranDetails = "A0|403|" + amount + "|1|Just Cuz";
		StringBuilder sb = new StringBuilder();
		sb.append(cardId).append('|').append(token);
		String cardData = sb.toString();

		log.info("Charging with token '" + cardData + "'");
		String attributes = null;
		EC2AuthResponse response = ec.chargePlain(null, null, "V3-1-USD", tranId, amount, cardData, entryType, tranResult, tranDetails, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}
	
	
	@Test
	public void junit_HessianRetokenizeOne() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		// String cardNumber = "4788250000028291";
		long cardId = 140420; // 125725;
		String securityCd = "111"; // "9668";
		String postalCd = "19355";
		String expDate = "2008";
		log.info("Sending Retokenize for card " + cardId);
		String attributes = null;
		EC2TokenResponse response = ec.retokenize(null, null, "V3-1-USD", cardId, securityCd, null, postalCd, null, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianAuthAndCancelToken_prod() throws Exception {
		EC2ServiceAPI ec = getEC2Service("usa");
		String deviceSerialCd = "K3MTB04D10C120611AA";
		String passcode = "15739647";
		long tranId = System.currentTimeMillis() / 1000;
		long cardId = 1227330941;
		String token = "080A9EDDDD3C72C3B6AEAE28819FA758";
		String entryType = "K";
		StringBuilder sb = new StringBuilder();
		sb.append(cardId).append('|').append(token);
		String cardData = sb.toString();
		EC2AuthResponse response = ec.authPlain(EportConnectSOAPTest.USERNAME, passcode, deviceSerialCd, tranId, 1, cardData, entryType, null);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
		switch(response.getReturnCode()) {
			case ECResponse.RES_APPROVED:
			case ECResponse.RES_PARTIALLY_APPROVED:
			case ECResponse.RES_AVS_MISMATCH:
			case ECResponse.RES_CVV_AND_AVS_MISMATCH:
			case ECResponse.RES_CVV_MISMATCH:
				EC2Response batchResponse = ec.batch(EportConnectSOAPTest.USERNAME, passcode, deviceSerialCd, tranId, 0, "C", "", null);
				if(batchResponse == null)
					throw new Exception("Received null response");
				message = getBasicResponseMessage(batchResponse);
				log.info(message);
		}
	}

	@Test
	public void junit_HessianTokenize_prod() throws Exception {
		EC2ServiceAPI ec = getEC2Service("usa");
		String deviceSerialCd = "K3MTB04D10C120611AA";
		String passcode = "15739647";
		long tranId = System.currentTimeMillis() / 1000;
		long cardId = 1227330941;
		String token = "080A9EDDDD3C72C3B6AEAE28819FA758";
		String entryType = "K";
		StringBuilder sb = new StringBuilder();
		sb.append(cardId).append('|').append(token);
		String cardData = sb.toString();
		EC2AuthResponse response = ec.authPlain(EportConnectSOAPTest.USERNAME, passcode, deviceSerialCd, tranId, 1, cardData, entryType, null);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
		switch(response.getReturnCode()) {
			case ECResponse.RES_APPROVED:
			case ECResponse.RES_PARTIALLY_APPROVED:
			case ECResponse.RES_AVS_MISMATCH:
			case ECResponse.RES_CVV_AND_AVS_MISMATCH:
			case ECResponse.RES_CVV_MISMATCH:
				EC2Response batchResponse = ec.batch(EportConnectSOAPTest.USERNAME, passcode, deviceSerialCd, tranId, 0, "C", "", null);
				if(batchResponse == null)
					throw new Exception("Received null response");
				message = getBasicResponseMessage(batchResponse);
				log.info(message);
		}
	}

	@Test
	public void junit_HessianChargeToken() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		// long cardId = 1001674850;
		// String token = "B82E8C4098B6F7D8BC6D040CD42E4B65";
		long cardId = 1000032341;
		String token = "205F377EE133C41EA9C1D77BF297EBCA";
		long amount = 200;
		String entryType = "K";
		String tranResult = "S";
		// String tranDetails = "";
		// String tranDetails = "A0|200|90|2|Ping pongs|200|85|2|Yo-yos";
		String tranDetails = "A0|403|" + amount + "|1|Just Cuz";
		StringBuilder sb = new StringBuilder();
		sb.append(cardId).append('|').append(token);
		String cardData = sb.toString();

		log.info("Charging with token '" + cardData + "'");
		String attributes = null;
		EC2AuthResponse response = ec.chargePlain(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, cardData, entryType, tranResult, tranDetails, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianChargeTokenVI() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		// long cardId = 1001674850;
		// String token = "B82E8C4098B6F7D8BC6D040CD42E4B65";
		long cardId = 1000032411;
		String token = "D5E09B0841AE795747976C4D89866236";
		long amount = 450;
		String entryType = "K";
		String tranResult = "S";
		// String tranDetails = "";
		// String tranDetails = "A0|200|90|2|Ping pongs|200|85|2|Yo-yos";
		String tranDetails = "A0|403|" + amount + "|1|Just Cuz";
		StringBuilder sb = new StringBuilder();
		sb.append(cardId).append('|').append(token);
		String cardData = sb.toString();

		log.info("Charging with token '" + cardData + "'");
		String attributes = null;
		EC2AuthResponse response = ec.chargePlain(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, cardData, entryType, tranResult, tranDetails, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianChargeTokenPrepaid() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		long cardId = 125725;
		String token = "A4D0A524BA9DB9F60E5C15D6641CF665";
		long amount = 100;
		String entryType = "K";
		String tranResult = "S";
		// String tranDetails = "";
		// String tranDetails = "A0|200|90|2|Ping pongs|200|85|2|Yo-yos";
		String tranDetails = "A0|403|" + amount + "|1|Just Cuz";
		StringBuilder sb = new StringBuilder();
		sb.append(cardId).append('|').append(token);
		String cardData = sb.toString();

		log.info("Charging with token '" + cardData + "'");
		String attributes = null;
		EC2AuthResponse response = ec.chargePlain(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, cardData, entryType, tranResult, tranDetails, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianTokenizeEncryptedCardMC() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		String maskedTrack = ";5473000010000014=25120000000000000000?";// MC
		CardReaderType cardReaderType = CardReaderType.MAGTEK_MAGNESAFE;
		String encryptedCardHex = "479C236ACCB11BFDC3F6C189B9BFE0852A1D3F102B2A5330F320B1E11F6A893644F22C438EA6D088";
		String ksnHex = "9011040B005166000054";
		log.info("Sending Tokenize with card '" + maskedTrack + "'");
		String attributes = null;
		EC2TokenResponse response = ec.tokenizeEncrypted(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, cardReaderType.getValue(), maskedTrack.length(), encryptedCardHex, ksnHex, "19355", null, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianChargeEncryptedToken() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		long cardId = 1000033781; // 1000000012;
		String token = "80A85B6AFDF8495DDA91AF5B3B6CC71B"; // "3D632DDBFA7B70638761B226C671D0B6";
		long amount = 200;
		String entryType = "K";
		String tranResult = "S";
		// String tranDetails = "";
		// String tranDetails = "A0|200|90|2|Ping pongs|200|85|2|Yo-yos";
		String tranDetails = "A0|403|" + amount + "|1|Great Stuff";
		StringBuilder sb = new StringBuilder();
		sb.append(cardId).append('|').append(token);
		String cardData = sb.toString();

		log.info("Charging with token '" + cardData + "'");
		String attributes = null;
		EC2AuthResponse response = ec.chargePlain(null, null, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, cardData, entryType, tranResult, tranDetails, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianTokenizeEncryptedCardMCwithZip() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		String maskedTrack = ";5473000010000014=25120000000000000000?";// MC
		CardReaderType cardReaderType = CardReaderType.MAGTEK_MAGNESAFE;
		String encryptedCardHex = "479C236ACCB11BFDC3F6C189B9BFE0852A1D3F102B2A5330F320B1E11F6A893644F22C438EA6D088";
		String ksnHex = "9011040B005166000054";
		log.info("Sending Tokenize with card '" + maskedTrack + "'");
		String attributes = "billingPostalCode=85909";
		EC2TokenResponse response = ec.tokenizeEncrypted(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, cardReaderType.getValue(), maskedTrack.length(), encryptedCardHex, ksnHex, "19355", null, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}
	@Test
	public void junit_HessianChargeIngenico() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		String maskedTrack = ";5473000010000014=25120000000000000000?";// MC
		CardReaderType cardReaderType = CardReaderType.INGENICO_MOBILE;
		String encryptedCardHex = "479C236ACCB11BFDC3F6C189B9BFE0852A1D3F102B2A5330F320B1E11F6A893644F22C438EA6D088";
		String ksnHex = "9011040B005166000054";
		long amount = 300;
		String entryType = "S";
		String tranResult = "S";
		String tranDetails = "A0|403|" + amount + "|1|Great Stuff";
		log.info("Sending Charge with card '" + maskedTrack + "'");
		String attributes = "billingPostalCode=85909";
		EC2AuthResponse response = ec.chargeEncrypted(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, cardReaderType.getValue(), maskedTrack.length(), encryptedCardHex, ksnHex, entryType, tranResult, tranDetails, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianChargeEncryptedCardMCwithZip() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		String maskedTrack = ";5473000010000014=25120000000000000000?";// MC
		CardReaderType cardReaderType = CardReaderType.MAGTEK_MAGNESAFE;
		String encryptedCardHex = "479C236ACCB11BFDC3F6C189B9BFE0852A1D3F102B2A5330F320B1E11F6A893644F22C438EA6D088";
		String ksnHex = "9011040B005166000054";
		long amount = 300;
		String entryType = "S";
		String tranResult = "S";
		String tranDetails = "A0|403|" + amount + "|1|Great Stuff";
		log.info("Sending Charge with card '" + maskedTrack + "'");
		String attributes = "billingPostalCode=85909";
		for(int i = 0; i < 15; i++) {
			EC2AuthResponse response = ec.chargeEncrypted(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId++, amount, cardReaderType.getValue(), maskedTrack.length(), encryptedCardHex, ksnHex, entryType, tranResult, tranDetails, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
		}
	}
	@Test
	public void junit_HessianChargeEncryptedCardMCwithZip_dev() throws Exception {
		EC2ServiceAPI ec = getEC2Service("dev");
		String serialCd = "K3MTB000001"; // "K3BK000002";
		String username = "bkrug";
		String password = "k#mqDttpcjMCN4Ag#ZVN"; // "HswFWc2#+tpHCmHzqp*c"; // "33053795";

		long tranId = System.currentTimeMillis() / 1000;
		String maskedTrack = ";5473000010000014=25120000000000000000?";// MC
		CardReaderType cardReaderType = CardReaderType.MAGTEK_MAGNESAFE;
		String encryptedCardHex = "479C236ACCB11BFDC3F6C189B9BFE0852A1D3F102B2A5330F320B1E11F6A893644F22C438EA6D088";
		String ksnHex = "9011040B005166000054";
		long amount = 300;
		String entryType = "S";
		String tranResult = "S";
		String tranDetails = "A0|403|" + amount + "|1|Great Stuff";
		log.info("Sending Charge with card '" + maskedTrack + "'");
		String attributes = "billingPostalCode=85909";
		for(int i = 0; i < 1; i++) {
			EC2AuthResponse response = ec.chargeEncrypted(username, password, serialCd, tranId++, amount, cardReaderType.getValue(), maskedTrack.length(), encryptedCardHex, ksnHex, entryType, tranResult, tranDetails, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
		}
	}
	@Test
	public void junit_HessianChargeEncryptedCardMC_ecc() throws Exception {
		String serial = "K3MTB000005";
		String username = "testaccount3";
		String password = "80233965";
		EC2ServiceAPI ec = getEC2Service("ecc");
		long tranId = System.currentTimeMillis() / 1000;
		String maskedTrack = ";5473000010000014=25120000000000000000?";// MC
		CardReaderType cardReaderType = CardReaderType.MAGTEK_MAGNESAFE;
		String encryptedCardHex = "479C236ACCB11BFDC3F6C189B9BFE0852A1D3F102B2A5330F320B1E11F6A893644F22C438EA6D088";
		String ksnHex = "9011040B005166000054";
		long amount = 300;
		String entryType = "S";
		String tranResult = "S";
		String tranDetails = "A0|403|" + amount + "|1|Great Stuff";
		log.info("Sending Charge with card '" + maskedTrack + "'");
		String attributes = "billingPostalCode=85909";
		for(int i = 0; i < 1; i++) {
			EC2AuthResponse response = ec.chargeEncrypted(username, password, serial, tranId, amount, cardReaderType.getValue(), maskedTrack.length(), encryptedCardHex, ksnHex, entryType, tranResult, tranDetails, attributes);
			if(response == null)
				throw new Exception("Received null response");
			String message = getBasicResponseMessage(response);
			log.info(message);
		}
	}

	@Test
	public void junit_HessianChargeEncryptedRandom() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 235;
		String entryType = "S";
		String tranResult = "S";
		// String tranDetails = "";
		// String tranDetails = "A0|200|90|2|Ping pongs|200|85|2|Yo-yos";
		String tranDetails = "A0|200|235|1|Gunk";
		String attributes = null;
		CardReaderType cardReaderType = CardReaderType.MAGTEK_MAGNESAFE;
		for(int i = 0; i < 1; i++) {
			String cardData = LuhnUtils.fillInTrackData(LuhnUtils.generateCard("40", 16));
			byte[] ksn = new byte[10];
			SecurityUtils.getSecureRandom().nextBytes(ksn);
			String ksnHex = StringUtils.toHex(ksn);
			byte[] encrypted = KMSTests.dukptEncrypt(ksnHex, cardReaderType, cardData);
			String encryptedCardHex = StringUtils.toHex(encrypted);
			EC2AuthResponse response = ec.chargeEncrypted(null, null, EportConnectSOAPTest.SERIAL_NUMBER, tranId + i, amount, cardReaderType.getValue(), cardData.length(), encryptedCardHex, ksnHex, entryType, tranResult, tranDetails, attributes);
			if(response == null)
				throw new Exception("Received null response");
			String message = getBasicResponseMessage(response);
			log.info(message);
		}
	}

	@Test
	public void junit_HessianChargeACS31_dev() throws Exception {
		EC2ServiceAPI ec = getEC2Service("dev");
		long tranId = System.currentTimeMillis() / 1000;
		String maskedCardData = ";478825******8291=********************?*";
		String encryptedCardDataHex = "4A41F780F51CB2B7F7D2F970A7BB87D22DCE5234644D4D333A1E5DCD96D19A768D23BF5E275BB6367A0F8349FAC31E40";
		String ksnHex = "00000000000000800020";
		int decryptedCardDataLength = maskedCardData.length();
		long amount = 200;
		String entryType = "S";
		String tranResult = "S";
		String tranDetails = "A0|403|" + amount + "|1|Great Stuff";

		log.info("Charging with encrypting reader for '" + maskedCardData + "'");
		String attributes = null;
		EC2AuthResponse response = ec.chargeEncrypted(EportConnectSOAPTest.USERNAME, "30325004", EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, CardReaderType.ACS_ACR31.getValue(), decryptedCardDataLength, encryptedCardDataHex, ksnHex, entryType, tranResult, tranDetails, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianChargeACS31_int() throws Exception {
		EC2ServiceAPI ec = getEC2Service("int");
		long tranId = System.currentTimeMillis() / 1000;
		String maskedCardData = ";478825******8291=********************?*";
		String encryptedCardDataHex = "4A41F780F51CB2B7F7D2F970A7BB87D22DCE5234644D4D333A1E5DCD96D19A768D23BF5E275BB6367A0F8349FAC31E40";
		String ksnHex = "00000000000000800020";
		int decryptedCardDataLength = maskedCardData.length();
		long amount = 200;
		String entryType = "S";
		String tranResult = "S";
		String tranDetails = "A0|403|" + amount + "|1|Great Stuff";

		log.info("Charging with encrypting reader for '" + maskedCardData + "'");
		String attributes = null;
		EC2AuthResponse response = ec.chargeEncrypted(EportConnectSOAPTest.USERNAME, "96343409", EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, CardReaderType.ACS_ACR31.getValue(), decryptedCardDataLength, encryptedCardDataHex, ksnHex, entryType, tranResult, tranDetails, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianUntokenizeMC() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long cardId = 1001674850;
		String token = "B82E8C4098B6F7D8BC6D040CD42E4B65";
		String attributes = null;
		EC2Response response = ec.untokenize(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, cardId, token, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianTokenizeEncryptedCardDiscover() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		String maskedTrack = ";6011000040000000=20120000000000000000?";// Discover
		CardReaderType cardReaderType = CardReaderType.MAGTEK_MAGNESAFE;
		String encryptedCardHex = "F84D37ADF8BEE6EDB9185FA7792CC4923D130C2662CEAE4E845448B7A40B88CDB4A0320B4C74F729";
		String ksnHex = "9011040B005166000055";
		log.info("Sending Tokenize with card '" + maskedTrack + "'");
		String attributes = null;
		EC2TokenResponse response = ec.tokenizeEncrypted(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, cardReaderType.getValue(), maskedTrack.length(), encryptedCardHex, ksnHex, "A3B 4C5", null, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianChargeEncryptedTokenDiscover() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		long cardId = 1001700750;
		String token = "FB06DAFC2AB532259A7BF6C91DC3BE24";
		long amount = 200;
		String entryType = "K";
		String tranResult = "S";
		// String tranDetails = "";
		// String tranDetails = "A0|200|90|2|Ping pongs|200|85|2|Yo-yos";
		String tranDetails = "A0|403|" + amount + "|1|Great Stuff";
		StringBuilder sb = new StringBuilder();
		sb.append(cardId).append('|').append(token);
		String cardData = sb.toString();

		log.info("Charging with token '" + cardData + "'");
		String attributes = null;
		EC2AuthResponse response = ec.chargePlain(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, cardData, entryType, tranResult, tranDetails, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_testHessianCashReplenish() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 150;
		long replenishCardId = 1001691820L;
		long replenishConsumerId = 44211;
		String attributes = null;
		EC2AuthResponse response = ec.replenishCash(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, replenishCardId, replenishConsumerId, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		if(response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED)
			log.info(message);
		else
			throw new Exception(message);
	}

	@Test
	public void junit_testHessianCashReplenish_dev() throws Exception {
		EC2ServiceAPI ec = getEC2Service("dev");
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 150;
		long replenishCardId = 125649L;
		long replenishConsumerId = 42756L;
		String attributes = null;
		EC2AuthResponse response = ec.replenishCash(EportConnectSOAPTest.USERNAME, "30325004", EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, replenishCardId, replenishConsumerId, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		if(response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED)
			log.info(message);
		else
			throw new Exception(message);
	}

	@Test
	public void junit_testHessianCashReplenishOperatorServiced() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 150;
		long replenishCardId = 125719L;
		long replenishConsumerId = 44202;
		String attributes = null;
		EC2AuthResponse response = ec.replenishCash(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, replenishCardId, replenishConsumerId, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		if(response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED)
			log.info(message);
		else
			throw new Exception(message);
	}

	@Test
	public void junit_testHessianCardReplenishOperatorServiced() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 140;
		String cardData = ";5454545454545454=10121015432112345601?"; // MC
		String entryType = "C";
		long replenishCardId = 125719L;
		long replenishConsumerId = 44202;
		String attributes = null;
		EC2AuthResponse response = ec.replenishPlain(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, cardData, entryType, replenishCardId, replenishConsumerId, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		if(response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED)
			log.info(message);
		else
			throw new Exception(message);
	}

	@Test
	public void junit_testHessianCardReplenish() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 240;
		String cardData = ";5454545454545454=10121015432112345601?"; // MC
		String entryType = "C";
		long replenishCardId = 1001691820L;
		long replenishConsumerId = 44211;
		String attributes = null;
		EC2AuthResponse response = ec.replenishPlain(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, cardData, entryType, replenishCardId, replenishConsumerId, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		if(response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED)
			log.info(message);
		else
			throw new Exception(message);
	}

	@Test
	public void junit_HessianGetCardIdPrepaidLocalCAD() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		String cardData = "6396212009972335492=2005002816224"; // MC
		String entryType = "S";
		String attributes = null;
		EC2CardId response = ec.getCardIdPlain(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, cardData, entryType, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianGetCardIdPrepaidLocalCADManual() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		String cardData = "6396212009972335492"; // MC
		String entryType = "M";
		String attributes = null;
		EC2CardId response = ec.getCardIdPlain(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, cardData, entryType, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianGetCardInfoPrepaidLocalCAD() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		String cardData = "6396212009972335492=2005002816224"; // MC
		String entryType = "S";
		String attributes = null;
		EC2CardInfo response = ec.getCardInfoPlain(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, cardData, entryType, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}
	@Test
	public void junit_HessianGetCardInfoPrepaidUnknown() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		String cardData = "6396212009972335483=2005002816224"; // MC
		String entryType = "S";
		String attributes = null;
		EC2CardInfo response = ec.getCardInfoPlain(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, cardData, entryType, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianGetCardInfoPrepaidLocalCADManual() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		String cardData = "6396212009972335492"; // MC
		String entryType = "N";
		String attributes = null;
		EC2CardInfo response = ec.getCardInfoPlain(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, cardData, entryType, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}
	@Test
	public void junit_HessianGetCardInfoEncryptedCredit() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		CardReaderType cardReaderType = CardReaderType.MAGTEK_MAGNESAFE;
		String cardData = LuhnUtils.fillInTrackData(LuhnUtils.generateCard("51", 16));
		byte[] ksn = new byte[10];
		new Random().nextBytes(ksn);
		String ksnHex = StringUtils.toHex(ksn);
		byte[] encrypted = KMSTests.dukptEncrypt(ksnHex, cardReaderType, cardData);
		String entryType = "S";
		String attributes = null;
		EC2CardInfo response = ec.getCardInfoEncrypted(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, cardReaderType.getValue(), cardData.length(), StringUtils.toHex(encrypted), ksnHex, entryType, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianGetCardInfoCredit() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		// String cardData = ";5454545454545454=10121015432112345601?"; // MC
		String cardData = LuhnUtils.fillInTrackData(LuhnUtils.generateCard("51", 16)); //
		String entryType = "S";
		String attributes = null;
		EC2CardInfo response = ec.getCardInfoPlain(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, cardData, entryType, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}
	@Test
	public void junit_HessianGetCardInfoUnknown() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		String cardData = "1111233131131=10121015432112345601?"; // MC
		String entryType = "S";
		String attributes = null;
		EC2CardInfo response = ec.getCardInfoPlain(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, cardData, entryType, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianGetCardIdRandom() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		String cardData = LuhnUtils.fillInTrackData(LuhnUtils.generateCard("51", 16)); //
		String entryType = "N";
		String attributes = null;
		log.info("Getting card id for " + cardData);
		EC2CardId response = ec.getCardIdPlain(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, cardData, entryType, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianGetCardIdRepeat() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		String cardData = "4137159626724480";
		String entryType = "M";
		String attributes = null;
		log.info("Getting card id for " + cardData);
		EC2CardId response = ec.getCardIdPlain(null, null, EportConnectSOAPTest.SERIAL_NUMBER, cardData, entryType, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianGetCardId_ecc() throws Exception {
		EC2ServiceAPI ec = getEC2Service("ecc");
		String serial = "K3MTB000005";
		String username = "testaccount3";
		String password = "80233965";
		String cardData = "4055011111111111";
		String entryType = "M";
		String attributes = null;
		log.info("Getting card id for " + cardData);
		EC2CardId response = ec.getCardIdPlain(username, password, serial, cardData, entryType, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianGetCardIdDriverCard() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		String cardData = "639621150060000012=1809305950";
		String entryType = "S";
		String attributes = null;
		log.info("Getting card id for " + cardData);
		EC2CardId response = ec.getCardIdPlain(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, cardData, entryType, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_HessianBadCredentials() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		String cardData = "5128172841526612=1809305950";
		String entryType = "M";
		String attributes = null;
		log.info("Getting card id for " + cardData);
		EC2CardId response = ec.getCardIdPlain(EportConnectSOAPTest.USERNAME, "NOT-THE-CORRECT-PASSWORD", EportConnectSOAPTest.SERIAL_NUMBER, cardData, entryType, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}
	@Test
	public void junit_testServerDecryption() throws Exception {
		byte[] key = ByteArrayUtils.fromHex("96844264AA958D7E6106F7BABBB77D03"); // 2FF246CDAD312AB5EEA0BD2043CB7500, 96844264AA958D7E6106F7BABBB77D03
		byte[] encrypted = ByteArrayUtils.fromHex("4D41B64012F9185478F1A88B89C72583B8DFC22EC0A2998A9A12BAC533D9D13D1851806C5974C6B62F69247811553C110A067A46ACC411DF55E63F3BAA087CCE");
		Crypt crypt = CryptUtil.getCrypt("RIJNDAEL_CRC16");// TEA_CRC16
		byte[] decrypted = crypt.decrypt(encrypted, key);
		log.info("Decrypted: " + StringUtils.toHex(decrypted));
	}
	@Test
	public void junit_esudsIntentedSale() throws Exception {
		String deviceName = "EV035433";
		sendIntendedSale(host, unifedlayerPort, deviceName, keys.get(host).get(deviceName), System.currentTimeMillis()/ 1000, 300);
	}
	@Test
	public void junit_esudsIntentedThenActualSale() throws Exception {
		String deviceName = "EV033412"; // "EV035433";
		int amount = 300;
		long tranId = System.currentTimeMillis() / 1000;
		String card = "375019001001" + "950"; // ^;?((37501)(9001001[0-9]{3})|63290820000000000[0-9]{2})(?:\?([\x00-\xFF])?)?$ ; //";5454545454545454=10121015432112345601?"; // MC

		if(sendESudsAuth(host, unifedlayerPort, deviceName, keys.get(host).get(deviceName), tranId, card, 'C', BigDecimal.valueOf(amount * 2))) {
			sendIntendedSale(host, unifedlayerPort, deviceName, keys.get(host).get(deviceName), tranId, amount);
			sendESudsSale(host, unifedlayerPort, deviceName, keys.get(host).get(deviceName), tranId, amount);
		}
	}

	@Test
	public void junit_esudsActualSale() throws Exception {
		String deviceName = "EV035433";
		sendESudsSale(host, unifedlayerPort, deviceName, keys.get(host).get(deviceName), System.currentTimeMillis()/ 1000, 300);
	}

	@Test
	public void junit_authAndSaleAramark() throws Exception {
		String card = "100001"; // Aramark -- this worked in local - 1/06/2014
		// String card = "1000019876543210=0123456789"; // Aramark --
		String deviceName = "EV100166";
		authAndSale(host, deviceName, card, BigDecimal.valueOf(10));
	}

	@Test
	public void junit_authAndSaleSprout() throws Exception {
		String card = "6277229030431058=49121010933500000788"; // "6277229030431041=49121010492800000292";
		String deviceName = "EV100166";
		authAndSale(host, deviceName, card, BigDecimal.valueOf(3));
	}
	@Test
	public void junit_authAndSaleSprout_dev() throws Exception {
		host = "10.0.0.67";
		String card = "6277229030431058=49121010933500000788"; // "6277229030431041=49121010492800000292";
		String deviceName = "EV100212";
		authAndSale(host, deviceName, card, BigDecimal.valueOf(3));
	}

	@Test
	public void junit_authEncrypted_int() throws Exception {
		host = "10.0.0.68";
		String card = ";4055011111111129=18121015432112345678?"; // "9999901501=200112345";
		String deviceName = "EV100212";
		String ksnHex = "62994996340010200003";
		String encryptedHex = "EEC84FCA289D24F402A86DEE82B11ACCCC9F86AF5F9C9009382F811FA4501C8517B18E3C4178A422"; // "68914723920AF8C816E3C2D5DF4E6CB5D1063C20DE3BA2EB";
		CardReaderType cardReaderType = CardReaderType.IDTECH_SECUREMAG;
		authAndSaleMagtek(host, authPort, deviceName, DeviceType.GX, cardReaderType, card.length(), ByteArrayUtils.fromHex(ksnHex), ByteArrayUtils.fromHex(encryptedHex), BigDecimal.valueOf(2));
	}

	@Test
	public void junit_authAndSaleBlackboard() throws Exception {
		String card = "375019001001880"; // Blackboard
		String deviceName = "EV100166";
		authAndSale(host, deviceName, card, BigDecimal.valueOf(10));
	}
	@Test
	public void junit_eSudsLabels() throws Exception {
		String deviceName = "EV035433";
		eSudsLabels(host, deviceName);
	}
	@Test
	public void junit_eSudsRoomLayout() throws Exception {
		String deviceName = "EV035433";
		eSudsRoomLayer(host, deviceName);
	}
	@Test
	public void junit_eSudsAuthAndSale() throws Exception {
		//String card = "375019001001880"; //Blackboard
		//String card = ";6396210011783541146=2012005899623?"; //Internal
		// String card = ";5454545454545454=10121015432112345601?"; //MC
		String card = "999999010=0044"; // eSuds passcard
		String deviceName = "EV035433";
		eSudsAuthAndSale(host, deviceName, card, 'C', BigDecimal.valueOf(1350), 50);
	}
	@Test
	public void junit_eSudsStatusStart() throws Exception {
		String deviceName = "EV035433";
		eSudsStatusChange(host, deviceName, ESudsRoomStatus.EQUIP_STATUS_IN_CYCLE_FIRST, 3);
	}
	@Test
	public void junit_eSudsStatusComplete() throws Exception {
		String deviceName = "EV035433";
		eSudsStatusChange(host, deviceName, ESudsRoomStatus.EQUIP_STATUS_IDLE_AVAILABLE, 3);
	}
	@Test
	public void junit_gxAuthAndSale_ecc() throws Exception {
		// String card = "375019001001880"; //Blackboard
		// String card = ";6396210011783541146=2012005899623?"; //Internal
		String card = ";5454545454545454=10121015432112345601?"; // MC
		String deviceName = "EV100167";
		gxAuthAndSale(host, deviceName, card, 'C', 0);
	}

	@Test
	public void junit_gxAuthAndSale() throws Exception {
		//String card = "375019001001880"; //Blackboard
		//String card = ";6396210011783541146=2012005899623?"; //Internal
		String card = ";5454545454545454=10121015432112345601?"; //MC
		String deviceName = "EV100167";
		gxAuthAndSale(host, deviceName, card, 'C', 0);
	}
	@Test
	public void junit_kioskAuthAndSaleDemo() throws Exception {
		// String card = ";5454545454545454=10121015432112345601?"; // MC
		String card = "371449123458431=10121015432112345601";
		String deviceName = "EV100139";
		ClientEmulator ce = new ClientEmulator(host, unifedlayerPort, deviceName, keys.get(host).get(deviceName));
		gx32AuthAndSale(ce, ce.getPort(), ce.getPort(), 7, card, 0, null);
	}
	@Test
	public void junit_kioskAuthAndSaleFDMS() throws Exception {
		// String card = ";5454545454545454=10121015432112345601?"; // MC
		String card = "6010561234567895=20121234567870713779";
		String deviceName = "EV100085";
		ClientEmulator ce = new ClientEmulator(host, unifedlayerPort, deviceName, keys.get(host).get(deviceName));
		gx32AuthAndSale(ce, ce.getPort(), ce.getPort(), 7, EntryType.SWIPE, card, 0, 600, 450, true, null);
	}

	@Test
	public void junit_gx32AuthErrorBin() throws Exception {
		// String card = ";5454545454545454=10121015432112345601?"; // MC
		String card = "8888888";
		String deviceName = "EV033483";
		ClientEmulator ce = new ClientEmulator(host, layer3Port, deviceName, keys.get(host).get(deviceName));
		gx32AuthAndSale(ce, ce.getPort(), ce.getPort(), 4, card, 0, null);
	}

	@Test
	public void junit_gxAuthAndSaleEsudsGeneric() throws Exception {
		String card = "4444"; // esuds generic
		String deviceName = "EV021026";
		gxAuthAndSale(host, deviceName, card, 'C', 0);
	}
	@Test
	public void junit_gxAuthAndSaleEsudsEtown() throws Exception {
		String card = "003314736=1100"; // esuds generic
		String deviceName = "EV021026";
		gxAuthAndSale(host, deviceName, card, 'C', 0);
	}

	@Test
	public void junit_gxAuthAndSaleLocal() throws Exception {
		// String card = ";6396210011783541146=2012005899623?"; //Internal
		String card = ";5454545454545454=10121015432112345601?"; // MC
		String deviceName = "EV035194";
		gxAuthAndSale(host, deviceName, card, 'C', 0);
	}

	@Test
	public void junit_gxAuthAndSaleIsis() throws Exception {
		// String card = "375019001001880"; //Blackboard
		// String card = ";6396210011783541146=2012005899623?"; //Internal
		// String card = ";5454545454545454=10121015432112345601?"; // MC
		// String card = ";4300123412341017=12100001?"; // Visa
		String card = ";4185963966837146=15040001?"; // Visa2
		String deviceName = "EV035194";
		gxAuthAndSale(host, deviceName, card, 'J', 0);
	}

	@Test
	public void junit_dukptAuthAndSale() throws Exception {
		// String card = "375019001001880"; //Blackboard
		// String card = ";6396210011783541146=2012005899623?"; //Internal
		String card = ";5454545454545454=10121015432112345601?"; // MC
		char cardType = 'C';
		String deviceName = "EV100185"; // "EV100185";
		int convenienceFee = 0;
		byte[] keySerialNum = ByteArrayUtils.fromHex("ABCDEF0123456789ABCDEF0123456789");
		byte[] encryptedData = ByteArrayUtils.fromHex("ABCDEF0123456789ABCDEF0123456789");

		ClientEmulator ce = new ClientEmulator(host, unifedlayerPort, deviceName, keys.get(host).get(deviceName));
		long tranId = ce.getMasterId();
		MessageData_AC data = new MessageData_AC();
		data.setAmount(500L);
		data.setCardReaderType(CardReaderType.MAGTEK_MAGNESAFE);
		data.setCardType(CardType.getByValue(cardType));
		MessageData_AC.EncryptingCardReaderImpl cardReader = (MessageData_AC.EncryptingCardReaderImpl) data.getCardReader();
		cardReader.setDecryptedLength(card.length());
		cardReader.setEncryptedData(encryptedData);
		cardReader.setKeySerialNum(keySerialNum);

		data.setTransactionId(tranId++);
		ce.setPort(unifedlayerPort);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.startCommunication(7);
		MessageData reply;
		try {
			reply = ce.sendMessage(data);
		} finally {
			ce.stopCommunication();
		}
		boolean doSale;
		switch(reply.getMessageType()) {
			case AUTH_RESPONSE_3_0:
				AuthResponseCodeA1 rc = ((MessageData_A1) reply).getResponseCode();
				switch(rc) {
					case APPROVED:
					case CONDITIONALLY_APPROVED:
						doSale = true;
						break;
					default:
						doSale = false;
						log.info("Cannot process sale because auth was rejected");
				}
				break;
			default:
				doSale = false;
				log.info("Cannot process sale because response was " + reply.getMessageType());
		}
		if(doSale) {
			MessageData_2A saleData = new MessageData_2A();
			saleData.setTransactionId(data.getTransactionId());
			saleData.setConvenienceFee(convenienceFee);
			saleData.setSaleAmount(333);
			saleData.setTransactionResult(TranDeviceResultType.SUCCESS);
			saleData.setVendByteLength((byte) 0);
			MessageData_2A.NetBatch0LineItem li = (MessageData_2A.NetBatch0LineItem) saleData.addActualLineItem();
			li.setPositionNumber(66);
			// li.setReportedPrice(121);
			li.setReportedPrice(0xFFFFFF);
			li = (MessageData_2A.NetBatch0LineItem) saleData.addActualLineItem();
			li.setPositionNumber(47);
			// li.setReportedPrice(188);
			li.setReportedPrice(0xFFFFFF);
			ce.setPort(salePort);
			log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
			ce.startCommunication(4);
			try {
				ce.sendMessage(saleData);
			} finally {
				ce.stopCommunication();
			}
		}

	}

	@Test
	public void junit_dukptAuthAndSaleLocal() throws Exception {
		// String card = "375019001001880"; //Blackboard
		// String card = ";6396210011783541146=2012005899623?"; //Internal
		String card = ";5454545454545454=10121015432112345601?"; // MC
		char cardType = 'C';
		String deviceName = "EV100027"; // "EV100185";
		int convenienceFee = 0;
		byte[] keySerialNum = ByteArrayUtils.fromHex("ABCDEF0123456789ABCDEF0123456789");
		byte[] encryptedData = ByteArrayUtils.fromHex("ABCDEF0123456789ABCDEF0123456789");

		ClientEmulator ce = new ClientEmulator(host, unifedlayerPort, deviceName, keys.get(host).get(deviceName));
		long tranId = ce.getMasterId();
		MessageData_AC data = new MessageData_AC();
		data.setAmount(500L);
		data.setCardReaderType(CardReaderType.MAGTEK_MAGNESAFE);
		data.setCardType(CardType.getByValue(cardType));
		MessageData_AC.EncryptingCardReaderImpl cardReader = (MessageData_AC.EncryptingCardReaderImpl) data.getCardReader();
		cardReader.setDecryptedLength(card.length());
		cardReader.setEncryptedData(encryptedData);
		cardReader.setKeySerialNum(keySerialNum);

		data.setTransactionId(tranId++);
		ce.setPort(unifedlayerPort);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.startCommunication(7);
		MessageData reply;
		try {
			reply = ce.sendMessage(data);
		} finally {
			ce.stopCommunication();
		}
		boolean doSale;
		switch(reply.getMessageType()) {
			case AUTH_RESPONSE_3_0:
				AuthResponseCodeA1 rc = ((MessageData_A1) reply).getResponseCode();
				switch(rc) {
					case APPROVED:
					case CONDITIONALLY_APPROVED:
						doSale = true;
						break;
					default:
						doSale = false;
						log.info("Cannot process sale because auth was rejected");
				}
				break;
			default:
				doSale = false;
				log.info("Cannot process sale because response was " + reply.getMessageType());
		}
		if(doSale) {
			MessageData_2A saleData = new MessageData_2A();
			saleData.setTransactionId(data.getTransactionId());
			saleData.setConvenienceFee(convenienceFee);
			saleData.setSaleAmount(333);
			saleData.setTransactionResult(TranDeviceResultType.SUCCESS);
			saleData.setVendByteLength((byte) 0);
			MessageData_2A.NetBatch0LineItem li = (MessageData_2A.NetBatch0LineItem) saleData.addActualLineItem();
			li.setPositionNumber(66);
			// li.setReportedPrice(121);
			li.setReportedPrice(0xFFFFFF);
			li = (MessageData_2A.NetBatch0LineItem) saleData.addActualLineItem();
			li.setPositionNumber(47);
			// li.setReportedPrice(188);
			li.setReportedPrice(0xFFFFFF);
			ce.setPort(salePort);
			log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
			ce.startCommunication(4);
			try {
				ce.sendMessage(saleData);
			} finally {
				ce.stopCommunication();
			}
		}

	}
	@Test
	public void junit_gxPasscardSale_conv_fee() throws Exception {
		String card = "999999999";
		String deviceName = "EV033414";
		ClientEmulator ce = new ClientEmulator(host, layer3Port, deviceName, keys.get(host).get(deviceName));
		MessageData_2B data2B = buildSale2B(ce.getMasterId(), SaleType.ACTUAL, 3, true, DeviceType.GX);
		data2B.setCreditCardMagstripe(card);
		data2B.setCardType(CardType.SPECIAL_SWIPE);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.startCommunication(4);
		MessageData reply;
		try {
			reply = ce.sendMessage(data2B);
		} finally {
			ce.stopCommunication();
		}
	}

	@Test
	public void junit_gxAuthAndSale_conv_fee() throws Exception {
		//String card = "375019001001880"; //Blackboard
		//String card = ";6396210011783541146=2012005899623?"; //Internal
		// String card = ";5454545454545454=10121015432112345601?"; //MC
		String card = "4055011111111111=10121015432112345678"; // VISA
		String deviceName = "EV033414";
		gxAuthAndSale(host, deviceName, card, 'C', 12);
	}
	@Test
	public void junit_authAndSaleInternal() throws Exception {
		String card = ";6396210011783541146=2012005899623?"; //Internal
		String deviceName = "TD000042";
		//String deviceName = "TD000023";
		authAndSale(host, deviceName, card, null);
	}
	@Test
	public void junit_authAndSaleTest() throws Exception {
		//String card = ";9912349876543210=2341?"; //Internal
		String card = ";002000007=0044?"; //Internal
		String deviceName = "TD000065";
		//String deviceName = "TD000023";
		authAndSale(host, deviceName, card, null);
	}
	@Test
	public void junit_authAndSaleBadTranId() throws Exception {
		String card = ";5454545454545454=10121015432112345601?"; // MC
		String deviceName = "TD000065";
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		ce.masterId = Integer.MAX_VALUE - 1;
		authAndSale(ce, authPort, salePort, card, EntryType.SWIPE, BigDecimal.valueOf(300), BigDecimal.valueOf(300), false);
	}

	@Test
	public void junit_authAndSaleDupTranId_int() throws Exception {
		host = "10.0.0.68";
		String card = ";5454545454545454=10121015432112345601?"; // MC
		String deviceName = "TD000021";
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		long tranId = ce.masterId;
		ce.startCommunication(7);
		try {
			ce.testAuthV4(EntryType.SWIPE, card, BigDecimal.valueOf(300));
		} finally {
			ce.stopCommunication();
		}
		ce.masterId = tranId - 10;
		ce.startCommunication(7);
		try {
			ce.testAuthV4(EntryType.SWIPE, card, BigDecimal.valueOf(200));
		} finally {
			ce.stopCommunication();
		}
	}
	@Test
	public void junit_authAndSaleIsis() throws Exception {
		String card = "6011000995500000=20121015432112345678"; // Isis
		String deviceName = "TD000473";
		authAndSale(host, deviceName, card, EntryType.ISIS, BigDecimal.valueOf(1000), BigDecimal.valueOf(150));
	}
	@Test
	public void junit_authAndSaleEBeacon() throws Exception {
		String card = "6011000995500000=20121015432112345678"; // Isis
		String deviceName = "TD000473";
		BigDecimal amount = BigDecimal.valueOf(1000);

		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));

		MessageData_C2 dataC2 = new MessageData_C2();
		dataC2.setAmount(amount);
		dataC2.setCardReaderType(CardReaderType.BLUETOOTH);
		dataC2.setEntryType(EntryType.BLUETOOTH);
		dataC2.setTransactionId(ce.getMasterId());
		AppCardReader acr = (AppCardReader) dataC2.getCardReader();
		long amid = ce.getMasterId() / 2;
		acr.setAppMessageId(amid);
		acr.setAppMessageFormatId(0xE1);
		MessageData_F1 dataF1 = new MessageData_F1();
		dataF1.setPayloadType((byte) 0);
		StandardPayload payload = (StandardPayload) dataF1.getPayload();
		payload.setAccountData(card);
		payload.setAppMessageId(amid);
		payload.setAppOS(EBeaconAppOS.IOS);
		payload.setAppType(EBeaconAppType.MORE_MOBILE_APP);
		payload.setAppVersion("BK Test 1.0A");
		payload.setEntryType(EntryType.SWIPE);
		byte[] appBytes = new byte[1100];
		Random random = new Random();
		long nonce = random.nextLong();
		ByteArrayUtils.writeLong(appBytes, 0, nonce);
		ByteBuffer appBuffer = ByteBuffer.wrap(appBytes, 6, appBytes.length - 16);
		dataF1.writeData(appBuffer, false);
		int length = appBuffer.position() - 6;
		ByteArrayUtils.writeUnsignedShort(appBytes, 4, length);
		Calculator calc = CRCType.CRC16Server.newCalculator();
		calc.update(appBytes, 6, length);
		byte[] calcCrc = calc.getValue();
		System.arraycopy(calcCrc, 0, appBytes, length + 6, calcCrc.length);
		int paddedLength = getPaddedLength(length + 8, 16);
		byte[] key = new byte[16];
		random.nextBytes(key);
		Key keySpec = new SecretKeySpec(key, "AES");
		Cipher cipher = Cipher.getInstance("AES/CBC/NOPADDING");
		cipher.init(Cipher.ENCRYPT_MODE, keySpec, new IvParameterSpec(new byte[16]));
		byte[] encryptedData = cipher.doFinal(appBytes, 0, paddedLength);
		byte[] encryptedKey = CryptUtils.encrypt("RSA/ECB/OAEPWITHSHA-512ANDMGF1PADDING", "eportconnect", key);
		byte[] appMessagePayload = new byte[2 + encryptedKey.length + encryptedData.length];
		ByteArrayUtils.writeUnsignedShort(appMessagePayload, 0, encryptedKey.length);
		System.arraycopy(encryptedKey, 0, appMessagePayload, 2, encryptedKey.length);
		System.arraycopy(encryptedData, 0, appMessagePayload, 2 + encryptedKey.length, encryptedData.length);
		acr.setAppMessagePayload(appMessagePayload);
		MessageData reply;
		ce.startCommunication(7);
		try {
			reply = ce.sendMessage(dataC2);
		} finally {
			ce.stopCommunication();
		}
		if(reply != null && reply.getMessageType() == MessageType.AUTH_RESPONSE_4_1) {
			MessageData_C3 replyC3 = (MessageData_C3) reply;
			if(replyC3.getAuthResponseTypeData() instanceof AuthorizationV2AuthResponse) {
				AuthorizationV2AuthResponse aar = (AuthorizationV2AuthResponse) replyC3.getAuthResponseTypeData();
				if(aar.getAdditionalData() instanceof MobileAppData) {
					MobileAppData mad = (MobileAppData) aar.getAdditionalData();
					log.info("Client Session Key = " + StringUtils.toHex(key) + " and Server Session Key = " + StringUtils.toHex(mad.getSessionKey()));
				}
			}
		}
	}

	@Test
	public void junit_RSACryptTest() throws Exception {
		System.setProperty("javax.net.ssl.keyStore", "../LayersCommon/conf/net/keystore.ks");
		System.setProperty("javax.net.ssl.keyStorePassword", "usatech");
		byte[] data = "1234567890123456".getBytes();
		byte[] encrypted = CryptUtils.encrypt("RSA/ECB/OAEPWITHSHA-512ANDMGF1PADDING", "eportconnect", data);
		log.info("Encrypted data is " + encrypted.length + " bytes");
		byte[] decrypted = CryptUtils.decrypt("RSA/ECB/OAEPWITHSHA-512ANDMGF1PADDING", "eportconnect", encrypted, 0, encrypted.length);
		log.info("Decrypted data is " + new String(decrypted));

	}
	protected int getPaddedLength(int length, int paddingSize) {
		return (length + ((paddingSize - (length % paddingSize)) % paddingSize));
	}
	@Test
	public void junit_authAndSaleManualBadZip() throws Exception {
		String card = "6011000995500083|2012|123||19355|"; // Manual
		// String card = "5454545454545454|1505|123||A9C 5R4|";
		String deviceName = "TD000473";
		authAndSale(host, deviceName, card, EntryType.MANUAL, BigDecimal.valueOf(200), BigDecimal.valueOf(150));
	}

	@Test
	public void junit_gxAuthAndSaleIsis2() throws Exception {
		String card = "6011000995500000=20121015432112345678"; // Isis
		String deviceName = "EV036214";
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		// TODO: AEAdditionalDataType.ISIS_SMARTTAP.getValue(), new String(ByteArrayUtils.fromHex("DF5310025A58313739353637353438333143460EDF21091234567890123456789000DF51051122334455"))); //Auth v3.2
		gx32AuthAndSale(ce, layer3Port, layer3Port, card, 5, new String(ByteArrayUtils.fromHex("DF5310025A58313739353637353438333143460EDF21091234567890123456789000DF51051122334455")));
	}
	@Test
	public void junit_gxAuthAndSaleIsis3() throws Exception {
		// String card = "5454545454545454=10121015432112345601"; // Isis
		// String card = "5147359999999991=13121543213961456789";
		String card = LuhnUtils.fillInTrackData(LuhnUtils.generateCard("51", 16));
		String deviceName = "EV036214";
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		// TODO: AEAdditionalDataType.ISIS_SMARTTAP.getValue(), new String(ByteArrayUtils.fromHex("DF5310025A58313739353637353438333143460EDF21091234567890123456789000DF51051122334455"))); //Auth v3.2
		gx32AuthAndSale(ce, layer3Port, layer3Port, card, 0, new String(ByteArrayUtils.fromHex("DF5310025A58313739353637353438333143460EDF21091234567890123456789000DF51051122334455")));
	}

	@Test
	public void junit_authAndSaleMC() throws Exception {
		String card = ";5454545454545454=10121015432112345601?"; //MC
		//String card = ";5454545454545454=11011015432112345601?"; //MC - exp date to cause socket timeout in simulation mode
		String deviceName = "TD000021";
		authAndSale(host, deviceName, card, BigDecimal.valueOf(300));
	}
	@Test
	public void junit_authAndSaleMC_ecc() throws Exception {
		host = "192.168.4.63";
		String card = ";5454545454545454=10121015432112345601?"; // MC
		// String card = ";5454545454545454=11011015432112345601?"; //MC - exp date to cause socket timeout in simulation mode
		String deviceName = "TD000021";
		authAndSale(host, deviceName, card, BigDecimal.valueOf(300));
	}

	@Test
	public void junit_authAndSaleBlank() throws Exception {
		String card = "";
		String deviceName = "TD000021";
		authAndSale(host, deviceName, card, BigDecimal.valueOf(300));
	}

	@Test
	public void junit_chargeMC() throws Exception {
		String card = ";5454545454545454=10121015432112345601?"; // MC
		// String card = ";5454545454545454=11011015432112345601?"; //MC - exp date to cause socket timeout in simulation mode
		String deviceName = "TD000021";
		charge(host, deviceName, card, BigDecimal.valueOf(300), false, 0);
	}

	@Test
	public void junit_chargeMCMixed() throws Exception {
		String card = ";5454545454545454=10121015432112345601?"; // MC
		// String card = ";5454545454545454=11011015432112345601?"; //MC - exp date to cause socket timeout in simulation mode
		String deviceName = "TD000023";
		charge(host, deviceName, card, BigDecimal.valueOf(300), false, 50);
	}

	@Test
	public void junit_chargeAndCancelMC() throws Exception {
		String card = ";5454545454545454=10121015432112345601?"; // MC
		// String card = ";5454545454545454=11011015432112345601?"; //MC - exp date to cause socket timeout in simulation mode
		String deviceName = "TD000021";
		charge(host, deviceName, card, BigDecimal.valueOf(250), true, 0);
	}

	@Test
	public void junit_authAndSalePaymentechCAD() throws Exception {
		String deviceName = "TD000053";
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		// authAndSale(ce, authPort, salePort, ";5454545454545454=10121015432112345601?", EntryType.SWIPE, BigDecimal.valueOf(500), BigDecimal.valueOf(300));
		authAndSale(ce, authPort, salePort, ";4300123412341017=12100001?", EntryType.SWIPE, BigDecimal.valueOf(500), BigDecimal.valueOf(300));
		// authAndSale(ce, authPort, salePort, "6011000995500000=10121015432112345678", EntryType.SWIPE, BigDecimal.valueOf(502), BigDecimal.valueOf(302));
		// authAndSale(ce, authPort, salePort, "371449635398431=10121015432112345678", EntryType.SWIPE, BigDecimal.valueOf(503), BigDecimal.valueOf(303));
		// authAndSale(ce, authPort, salePort, ";5454545454545454=10121015432112345601?", EntryType.CONTACTLESS, BigDecimal.valueOf(504), BigDecimal.valueOf(304));
		// authAndSale(ce, authPort, salePort, ";4300123412341017=12100001?", EntryType.CONTACTLESS, BigDecimal.valueOf(505), BigDecimal.valueOf(305));
		// authAndSale(ce, authPort, salePort, "6011000995500000=10121015432112345678", EntryType.CONTACTLESS, BigDecimal.valueOf(506), BigDecimal.valueOf(306));
		// authAndSale(ce, authPort, salePort, "371449635398431=10121015432112345678", EntryType.CONTACTLESS, BigDecimal.valueOf(507), BigDecimal.valueOf(307));
	}

	@Test
	public void junit_authAndSaleFHMS() throws Exception {
		String deviceName = "TD000052";
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		authAndSale(ce, authPort, salePort, ";5454545454545454=10121015432112345601?", EntryType.SWIPE, BigDecimal.valueOf(500), BigDecimal.valueOf(300));
		authAndSale(ce, authPort, salePort, ";4300123412341017=12100001?", EntryType.SWIPE, BigDecimal.valueOf(501), BigDecimal.valueOf(301));
		authAndSale(ce, authPort, salePort, "6011000995500000=10121015432112345678", EntryType.SWIPE, BigDecimal.valueOf(502), BigDecimal.valueOf(302));
		authAndSale(ce, authPort, salePort, "371449635398431=10121015432112345678", EntryType.SWIPE, BigDecimal.valueOf(503), BigDecimal.valueOf(303));
		authAndSale(ce, authPort, salePort, ";5454545454545454=10121015432112345601?", EntryType.CONTACTLESS, BigDecimal.valueOf(504), BigDecimal.valueOf(304));
		authAndSale(ce, authPort, salePort, ";4300123412341017=12100001?", EntryType.CONTACTLESS, BigDecimal.valueOf(505), BigDecimal.valueOf(305));
		authAndSale(ce, authPort, salePort, "6011000995500000=10121015432112345678", EntryType.CONTACTLESS, BigDecimal.valueOf(506), BigDecimal.valueOf(306));
		authAndSale(ce, authPort, salePort, "371449635398431=10121015432112345678", EntryType.CONTACTLESS, BigDecimal.valueOf(507), BigDecimal.valueOf(307));
		
	}
	@Test
	public void junit_authAndSaleAtriumTest() throws Exception {
		String deviceName = "EV100166";
		// String card = ";6362611234567891=491212000000?";
		// String card = ";6362614567891231=491212000000?";
		String card = ";6362617891234561=491212000000?";
		// authAndSale(ce, authPort, salePort, "6666777788889999", EntryType.SWIPE, BigDecimal.valueOf(500), BigDecimal.valueOf(300));
		authAndSale(host, deviceName, card, BigDecimal.ZERO);
	}
	@Test
	public void junit_authAndSaleAtriumTest_ecc() throws Exception {
		host = "192.168.4.63";
		String deviceName = "EV108402";
		// String card = ";6362611234567891=491212000000?";
		// String card = ";6362614567891231=491212000000?";
		String card = ";6362617891234561=491212000000?";
		// authAndSale(ce, authPort, salePort, "6666777788889999", EntryType.SWIPE, BigDecimal.valueOf(500), BigDecimal.valueOf(300));
		authAndSale(host, deviceName, card, BigDecimal.ZERO);
	}

	@Test
	public void junit_authAndSalePOSGateway() throws Exception {
		String deviceName = "TD000021";
		// String deviceName = "EV033414";
		// String card = ";6362611234567891=491212000000?";
		// String card = ";6362614567891231=491212000000?";
		String card = ";111111049234021=491212000000?";
		// authAndSale(ce, authPort, salePort, "6666777788889999", EntryType.SWIPE, BigDecimal.valueOf(500), BigDecimal.valueOf(300));
		authAndSale(host, deviceName, card, BigDecimal.valueOf(250));
	}

	@Test
	public void junit_authAndSaleEcc() throws Exception {
		host = "192.168.4.63";
		String deviceName = "TD000005";
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		authAndSale(ce, authPort, salePort, ";5454545454545454=10121015432112345601?", EntryType.SWIPE, BigDecimal.valueOf(500), BigDecimal.valueOf(300));
		authAndSale(ce, authPort, salePort, ";4300123412341017=12100001?", EntryType.SWIPE, BigDecimal.valueOf(501), BigDecimal.valueOf(301));
		authAndSale(ce, authPort, salePort, "6011000995500000=10121015432112345678", EntryType.SWIPE, BigDecimal.valueOf(502), BigDecimal.valueOf(302));
		authAndSale(ce, authPort, salePort, "371449635398431=10121015432112345678", EntryType.SWIPE, BigDecimal.valueOf(503), BigDecimal.valueOf(303));
		authAndSale(ce, authPort, salePort, ";5454545454545454=10121015432112345601?", EntryType.CONTACTLESS, BigDecimal.valueOf(504), BigDecimal.valueOf(304));
		authAndSale(ce, authPort, salePort, ";4300123412341017=12100001?", EntryType.CONTACTLESS, BigDecimal.valueOf(505), BigDecimal.valueOf(305));
		authAndSale(ce, authPort, salePort, "6011000995500000=10121015432112345678", EntryType.CONTACTLESS, BigDecimal.valueOf(506), BigDecimal.valueOf(306));
		authAndSale(ce, authPort, salePort, "371449635398431=10121015432112345678", EntryType.CONTACTLESS, BigDecimal.valueOf(507), BigDecimal.valueOf(307));

	}
	@Test
	public void junit_authAndSaleEccRandom() throws Exception {
		host = "192.168.4.63";
		String deviceName = "TD000005";
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		String cardData = LuhnUtils.fillInTrackData(LuhnUtils.generateCard("51", 16)); //
		authAndSale(ce, authPort, salePort, cardData, EntryType.SWIPE, BigDecimal.valueOf(500), BigDecimal.valueOf(300));
		authAndSale(ce, authPort, salePort, cardData, EntryType.SWIPE, BigDecimal.valueOf(500), BigDecimal.valueOf(300));
	}

	@Test
	public void junit_authAndSaleIntegration() throws Exception {
		String deviceName = "TD000065";
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		authAndSale(ce, authPort, salePort, ";5454545454545454=10121015432112345601?", EntryType.SWIPE, BigDecimal.valueOf(500), BigDecimal.valueOf(300));
		authAndSale(ce, authPort, salePort, ";4300123412341017=12100001?", EntryType.SWIPE, BigDecimal.valueOf(400), BigDecimal.valueOf(301));
		authAndSale(ce, authPort, salePort, "6011000995500000=10121015432112345678", EntryType.SWIPE, BigDecimal.valueOf(300), BigDecimal.valueOf(302));
		authAndSale(ce, authPort, salePort, "371449635398431=10121015432112345678", EntryType.SWIPE, BigDecimal.valueOf(600), BigDecimal.valueOf(303));
		authAndSale(ce, authPort, salePort, ";5454545454545454=10121015432112345601?", EntryType.CONTACTLESS, BigDecimal.valueOf(500), BigDecimal.valueOf(304));
		authAndSale(ce, authPort, salePort, ";4300123412341017=12100001?", EntryType.CONTACTLESS, BigDecimal.valueOf(400), BigDecimal.valueOf(305));
		authAndSale(ce, authPort, salePort, "6011000995500000=10121015432112345678", EntryType.CONTACTLESS, BigDecimal.valueOf(450), BigDecimal.valueOf(306));
		authAndSale(ce, authPort, salePort, "371449635398431=10121015432112345678", EntryType.CONTACTLESS, BigDecimal.valueOf(350), BigDecimal.valueOf(307));
		
	}
	@Test
	public void junit_authTestProduction() throws Exception {
		String deviceName = "TD001521";
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		String[] cards = new String[] {
				";5454545454545454=10121015432112345601?",
				";6396210011783541146=2012005899623?",
 "375019001001880" // (37501)(9001001[0-9]{3}
		};
		for(String card : cards) {
			log.debug("Testing card '" + card + "' at " + System.currentTimeMillis() + " (" + new Date() + "");
			ce.startCommunication(7);
			try {
				ce.testAuthV4(EntryType.SWIPE, card, BigDecimal.valueOf(500));
			} finally {
				ce.stopCommunication();
			}
		}
	}
	
	@Test
	public void junit_authAndSaleDev() throws Exception {
		host = "10.0.0.67";
		String deviceName = "TD000308";
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		authAndSale(ce, authPort, salePort, ";5454545454545454=10121015432112345601?", EntryType.SWIPE, BigDecimal.valueOf(500), BigDecimal.valueOf(300));
		authAndSale(ce, authPort, salePort, ";4300123412341017=12100001?", EntryType.SWIPE, BigDecimal.valueOf(501), BigDecimal.valueOf(301));
		authAndSale(ce, authPort, salePort, "6011000995500000=10121015432112345678", EntryType.SWIPE, BigDecimal.valueOf(502), BigDecimal.valueOf(0));
		authAndSale(ce, authPort, salePort, "371449635398431=10121015432112345678", EntryType.SWIPE, BigDecimal.valueOf(503), BigDecimal.valueOf(303));
		authAndSale(ce, authPort, salePort, ";5454545454545454=10121015432112345601?", EntryType.CONTACTLESS, BigDecimal.valueOf(504), BigDecimal.valueOf(304));
		authAndSale(ce, authPort, salePort, ";4300123412341017=12100001?", EntryType.CONTACTLESS, BigDecimal.valueOf(505), BigDecimal.valueOf(0));
		authAndSale(ce, authPort, salePort, "6011000995500000=10121015432112345678", EntryType.CONTACTLESS, BigDecimal.valueOf(506), BigDecimal.valueOf(306));
		authAndSale(ce, authPort, salePort, "371449635398431=10121015432112345678", EntryType.CONTACTLESS, BigDecimal.valueOf(507), BigDecimal.valueOf(307));
		
	}
	@Test
	public void junit_authAndSaleDevEsudsCard() throws Exception {
		String deviceName = "TD000042";
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		authAndSale(ce, authPort, salePort, "155123456712", EntryType.SWIPE, BigDecimal.valueOf(500), BigDecimal.valueOf(300));
	}
	@Test
	public void junit_authAndSalePrepaidDev() throws Exception {
		host = "10.0.0.67";
		String deviceName = "TD000042";
		// String deviceName = "TD000436";
		// String card = "6396212001783541145=1705004088564";
		String card = "6396212001081040709=9912007437948";
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		authAndSale(ce, authPort, salePort, card, EntryType.SWIPE, BigDecimal.valueOf(500), BigDecimal.valueOf(475));
	}
	@Test
	public void junit_authAndSalePrepaidEcc() throws Exception {
		String deviceName = "TD000005";
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		authAndSale(ce, authPort, salePort, "6396212001478860933=1403000082645", EntryType.SWIPE, BigDecimal.valueOf(500), BigDecimal.valueOf(200));
	}

	@Test
	public void junit_authAndSalePrepaidLocal() throws Exception {
		String deviceName = "TD000042";
		String card = "6396212009226328954=1509005008675";
		// String card = "6396212009338376453=9912002494655";
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		// authAndSale(ce, authPort, salePort, "6396212009892790701=9912003392621", EntryType.SWIPE, BigDecimal.valueOf(500), BigDecimal.valueOf(200));
		// authAndSale(ce, authPort, salePort, card, EntryType.SWIPE, BigDecimal.valueOf(82), BigDecimal.valueOf(82));
		// authAndSale(ce, authPort, salePort, card, EntryType.SWIPE, BigDecimal.valueOf(3000), BigDecimal.valueOf(3000));
		authAndSale(ce, authPort, salePort, "6396212009905315827=9912006912213", EntryType.SWIPE, BigDecimal.valueOf(5000), BigDecimal.valueOf(5000));
	}

	@Test
	public void junit_authAndSalePrepaidLocalCAD() throws Exception {
		String deviceName = "TD000023";// "TD000042"; //
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		authAndSale(ce, authPort, salePort, "6396212009972335492=2005002816224", EntryType.SWIPE, BigDecimal.valueOf(950), BigDecimal.valueOf(950));
	}

	@Test
	public void junit_authAndSalePrepaidLocalOperatorServiced() throws Exception {
		String deviceName = "TD000042";
		String card = "6396212009562758095=9912005159088";
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		authAndSale(ce, authPort, salePort, card, EntryType.SWIPE, BigDecimal.valueOf(3000), BigDecimal.valueOf(3000));
	}

	@Test
	public void junit_authAndSaleForFills1() throws Exception {
		String deviceName = "EV033414";
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		authAndSale(ce, authPort, salePort, ";5454545454545454=10121015432112345601?", EntryType.SWIPE, BigDecimal.valueOf(500), BigDecimal.valueOf(300));
		authAndSale(ce, authPort, salePort, ";4300123412341017=12100001?", EntryType.SWIPE, BigDecimal.valueOf(400), BigDecimal.valueOf(301));
		authAndSale(ce, authPort, salePort, "6011000995500000=10121015432112345678", EntryType.SWIPE, BigDecimal.valueOf(300), BigDecimal.valueOf(302));
		authAndSale(ce, authPort, salePort, "371449635398431=10121015432112345678", EntryType.SWIPE, BigDecimal.valueOf(600), BigDecimal.valueOf(303));
		authAndSale(ce, authPort, salePort, ";5454545454545454=10121015432112345601?", EntryType.CONTACTLESS, BigDecimal.valueOf(500), BigDecimal.valueOf(304));
		authAndSale(ce, authPort, salePort, ";4300123412341017=12100001?", EntryType.CONTACTLESS, BigDecimal.valueOf(400), BigDecimal.valueOf(305));
		authAndSale(ce, authPort, salePort, "6011000995500000=10121015432112345678", EntryType.CONTACTLESS, BigDecimal.valueOf(300), BigDecimal.valueOf(306));
		authAndSale(ce, authPort, salePort, "371449635398431=10121015432112345678", EntryType.CONTACTLESS, BigDecimal.valueOf(600), BigDecimal.valueOf(307));
		
	}
	@Test
	public void junit_authAndSaleForFills2() throws Exception {
		String deviceName = "EV035194";
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		authAndSale(ce, authPort, salePort, ";5454545454545454=10121015432112345601?", EntryType.SWIPE, BigDecimal.valueOf(500), BigDecimal.valueOf(300));
		authAndSale(ce, authPort, salePort, ";4300123412341017=12100001?", EntryType.SWIPE, BigDecimal.valueOf(400), BigDecimal.valueOf(301));
		authAndSale(ce, authPort, salePort, "6011000995500000=10121015432112345678", EntryType.SWIPE, BigDecimal.valueOf(300), BigDecimal.valueOf(302));
		authAndSale(ce, authPort, salePort, "371449635398431=10121015432112345678", EntryType.SWIPE, BigDecimal.valueOf(600), BigDecimal.valueOf(303));
		authAndSale(ce, authPort, salePort, ";5454545454545454=10121015432112345601?", EntryType.CONTACTLESS, BigDecimal.valueOf(500), BigDecimal.valueOf(304));
		authAndSale(ce, authPort, salePort, ";4300123412341017=12100001?", EntryType.CONTACTLESS, BigDecimal.valueOf(400), BigDecimal.valueOf(305));
		authAndSale(ce, authPort, salePort, "6011000995500000=10121015432112345678", EntryType.CONTACTLESS, BigDecimal.valueOf(300), BigDecimal.valueOf(306));
		authAndSale(ce, authPort, salePort, "371449635398431=10121015432112345678", EntryType.CONTACTLESS, BigDecimal.valueOf(600), BigDecimal.valueOf(307));
		// gxAuthAndSale(host, deviceName, card, 'C', 0);
	}

	@Test
	public void junit_authAndSaleForFills3() throws Exception {
		String deviceName = "TD000023";
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		authAndSale(ce, authPort, salePort, ";5454545454545454=10121015432112345601?", EntryType.SWIPE, BigDecimal.valueOf(500), BigDecimal.valueOf(300));
		authAndSale(ce, authPort, salePort, ";4300123412341017=12100001?", EntryType.SWIPE, BigDecimal.valueOf(501), BigDecimal.valueOf(301));
		authAndSale(ce, authPort, salePort, "6011000995500000=10121015432112345678", EntryType.SWIPE, BigDecimal.valueOf(502), BigDecimal.valueOf(302));
		authAndSale(ce, authPort, salePort, "371449635398431=10121015432112345678", EntryType.SWIPE, BigDecimal.valueOf(503), BigDecimal.valueOf(303));
		authAndSale(ce, authPort, salePort, ";5454545454545454=10121015432112345601?", EntryType.CONTACTLESS, BigDecimal.valueOf(504), BigDecimal.valueOf(304));
		authAndSale(ce, authPort, salePort, ";4300123412341017=12100001?", EntryType.CONTACTLESS, BigDecimal.valueOf(505), BigDecimal.valueOf(305));
		authAndSale(ce, authPort, salePort, "6011000995500000=10121015432112345678", EntryType.CONTACTLESS, BigDecimal.valueOf(506), BigDecimal.valueOf(306));
		authAndSale(ce, authPort, salePort, "371449635398431=10121015432112345678", EntryType.CONTACTLESS, BigDecimal.valueOf(507), BigDecimal.valueOf(307));
		
	}
	
	@Test
	public void junit_authAndSaleRandom() throws Exception {
		String deviceName = "TD000023";
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		String cardData = LuhnUtils.fillInTrackData(LuhnUtils.generateCard("41", 16)); //
		log.info("Sending sales on card " + cardData);
		for(int i = 0; i < 1; i++)
			authAndSale(ce, authPort, salePort, cardData, EntryType.SWIPE, BigDecimal.valueOf(300), BigDecimal.valueOf(300));
	}

	@Test
	public void junit_authAndSaleTandem() throws Exception {
		// host = "10.0.0.67";
		String deviceName = "TD000023";
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		String cardData = "4788250000028291=15121015432112345601?"; //
		String cardData2 = "4485536666666663=15121019206100000014?";
		log.info("Sending sales on card " + cardData);
		// authOnly(ce, authPort, cardData, EntryType.SWIPE, BigDecimal.valueOf(500)); // auth only

		// authAndSale(ce, authPort, salePort, cardData, EntryType.SWIPE, BigDecimal.valueOf(0), null); // balance check
		// authAndSale(ce, authPort, salePort, cardData, EntryType.SWIPE, BigDecimal.valueOf(500), BigDecimal.valueOf(0)); // cancel
		authAndSale(ce, authPort, salePort, cardData, EntryType.SWIPE, BigDecimal.valueOf(500), BigDecimal.valueOf(100)); // sale for less than auth
		// authAndSale(ce, authPort, salePort, cardData, EntryType.SWIPE, BigDecimal.valueOf(500), BigDecimal.valueOf(500)); // sale for same as auth
		// authAndSale(ce, authPort, salePort, cardData, EntryType.SWIPE, BigDecimal.valueOf(278), BigDecimal.valueOf(200)); // partial auth
		// authAndSale(ce, authPort, salePort, cardData, EntryType.SWIPE, BigDecimal.valueOf(278), BigDecimal.valueOf(500)); // partial auth, sale more than
		// authAndSale(ce, authPort, salePort, cardData, EntryType.SWIPE, BigDecimal.valueOf(278), BigDecimal.valueOf(250)); // partial auth, sale less than
		// authAndSale(ce, authPort, salePort, cardData, EntryType.SWIPE, BigDecimal.valueOf(150), BigDecimal.valueOf(500)); // full auth, sale more than
		// authAndSale(ce, authPort, salePort, cardData, EntryType.SWIPE, BigDecimal.valueOf(150), BigDecimal.valueOf(8000)); // partial auth, sale too high
		// authAndSale(ce, authPort, salePort, cardData, EntryType.SWIPE, BigDecimal.valueOf(965), BigDecimal.valueOf(200)); // partial auth
		// authAndSale(ce, authPort, salePort, cardData, EntryType.SWIPE, BigDecimal.valueOf(966), BigDecimal.valueOf(200)); // partial auth
		// authAndSale(ce, authPort, salePort, cardData, EntryType.SWIPE, BigDecimal.valueOf(300), BigDecimal.valueOf(500)); // auth timout
		// authAndSale(ce, authPort, salePort, cardData, EntryType.SWIPE, BigDecimal.valueOf(500), BigDecimal.valueOf(300)); // timout

		// authAndSale(ce, authPort, salePort, cardData2, EntryType.CONTACTLESS, BigDecimal.valueOf(400), BigDecimal.valueOf(100)); // sale for less than auth
		// authAndSale(ce, authPort, salePort, cardData2, EntryType.CONTACTLESS, BigDecimal.valueOf(400), BigDecimal.valueOf(400)); // sale for same as auth

		// authAndSale(ce, authPort, salePort, cardData, EntryType.CONTACTLESS, BigDecimal.valueOf(9850), BigDecimal.valueOf(9850)); // downgrade

		// authAndSale(ce, authPort, salePort, ";5454545454545454=1505867921?", EntryType.SWIPE, BigDecimal.valueOf(400), BigDecimal.valueOf(150)); // Mastercard sale less than auth
		// authAndSale(ce, authPort, salePort, ";5454545454545454=1505867921?", EntryType.SWIPE, BigDecimal.valueOf(400), BigDecimal.valueOf(450)); // Mastercard sale more than auth
		// authAndSale(ce, authPort, salePort, cardData, EntryType.SWIPE, BigDecimal.valueOf(293), BigDecimal.valueOf(293)); //
		// authAndSale(ce, authPort, salePort, cardData, EntryType.SWIPE, BigDecimal.valueOf(500), BigDecimal.valueOf(293)); //
		// authAndSale(ce, authPort, salePort, cardData, EntryType.SWIPE, BigDecimal.valueOf(500), BigDecimal.valueOf(225)); //

	}

	@Test
	public void junit_cancelStaleSaleTandem() throws Exception {
		String deviceName = "TD000023";
		long tranId = 1446038987L;
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		ce.setPort(salePort);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.startCommunication(7);
		try {
			ce.testCancelSale(tranId, SaleResult.CANCELLED_DEVICE_FAILURE);
		} finally {
			ce.stopCommunication();
		}
	}
	@Test
	public void processEmvParamDownloadUpdate() {
//		String card = ";371449635398431=10121015432112345678?:";
//		BigDecimal amount = new BigDecimal("1.00");
//		EntryType entryType = EntryType.SWIPE;
		
		String deviceName = "TD000925";
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
//		long tranId = ce.getMasterId();
		ce.setPort(salePort);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		MessageData reply = null;
		try {
			ce.startCommunication(7);
			MessageData_CA dataCA = new MessageData_CA();
			dataCA.setRequestType(GenericRequestType.UPDATE_STATUS_REQUEST);
			((UpdateStatusRequest) dataCA.getRequestTypeData()).setStatusCodeBitmap(0x01);
			reply = ce.sendMessage(dataCA);
			log.info("Reply: " + reply.toString());
		} catch (Exception e) {
			e.printStackTrace();
			log.info("Bad processEmvParamDownloadUpdate: " + reply);
		}
	}

	@Test
	public void junit_authAndCancel() throws Exception {
		// host = "10.0.0.67";
		String deviceName = "TD000023";
		String cardData = "4788250000028291=15121015432112345601?"; //
		// String cardData2 = "4485536666666663=15121019206100000014?";
		authAndCancel(deviceName, cardData);
	}

	@Test
	public void junit_authAndCancelPrepaid() throws Exception {
		// host = "10.0.0.67";
		String deviceName = "TD000042";
		String cardData = "6396212009226328954=1509005008675";
		authAndCancel(deviceName, cardData);
	}

	protected void authAndCancel(String deviceName, String cardData) throws Exception {
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		log.info("Sending cancel on card " + cardData);
		long tranId = ce.getMasterId();
		ce.setPort(authPort);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.startCommunication(7);
		MessageData reply;
		try {
			reply = ce.testAuthV4(EntryType.SWIPE, cardData, new BigDecimal(500));
		} finally {
			ce.stopCommunication();
		}
		if(reply == null)
			return;
		boolean doSale;
		switch(reply.getMessageType()) {
			case AUTH_RESPONSE_4_1:
				AuthResponseTypeData artd = ((MessageData_C3) reply).getAuthResponseTypeData();
				AuthResponseType art = ((MessageData_C3) reply).getAuthResponseType();
				switch(art) {
					case AUTHORIZATION:
						AuthResponseCodeC3 rc = ((AuthorizationAuthResponse) artd).getResponseCode();
						switch(rc) {
							case SUCCESS:
							case CONDITIONAL_SUCCESS:
								doSale = true;
								break;
							default:
								doSale = false;
								log.info("Cannot process sale because auth was rejected");
						}
						break;
					case AUTHORIZATION_V2:
						rc = ((AuthorizationV2AuthResponse) artd).getResponseCode();
						switch(rc) {
							case SUCCESS:
							case CONDITIONAL_SUCCESS:
								doSale = true;
								break;
							default:
								doSale = false;
								log.info("Cannot process sale because auth was rejected");
						}
						break;
					default:
						doSale = false;
						log.info("Cannot process sale because auth response was " + art);
				}
				break;
			default:
				doSale = false;
				log.info("Cannot process sale because response was " + reply.getMessageType());
		}
		if(doSale) {
			ce.setPort(salePort);
			log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
			ce.startCommunication(7);
			try {
				ce.testCancelSale(tranId, SaleResult.CANCELLED_DEVICE_FAILURE);
			} finally {
				ce.stopCommunication();
			}
		}
	}

	@Test
	public void junit_TandemReadiness() throws Exception {
		//String deviceName = "TD000023";
		String deviceName = "TD000021";
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		authAndSale(ce, authPort, salePort, "4788250000028291=15121015432112345601?", EntryType.SWIPE, BigDecimal.valueOf(326), null); // #1
		authAndSale(ce, authPort, salePort, "4485536666666663=15121019206100000014?", EntryType.SWIPE, BigDecimal.valueOf(2500), null); // #2
		authAndSale(ce, authPort, salePort, ";5454545454545454=1505867921?", EntryType.SWIPE, BigDecimal.valueOf(300), null); // #3
	}

	@Test
	public void junit_createRandomCard() {
		for(int i = 0; i < 10; i++) {
			String card = LuhnUtils.fillInTrackData(LuhnUtils.generateCard("41", 16));
			log.info("New card: " + MessageResponseUtils.getCardNumber(card));
		}

	}

	@Test
	public void junit_authAndSaleRandomNoAddress() throws Exception {
		String deviceName = "TD000188";
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		String cardData = LuhnUtils.fillInTrackData(LuhnUtils.generateCard("41", 16)); //
		authAndSale(ce, authPort, salePort, cardData, EntryType.SWIPE, BigDecimal.valueOf(600), BigDecimal.valueOf(300));
		authAndSale(ce, authPort, salePort, cardData, EntryType.SWIPE, BigDecimal.valueOf(500), BigDecimal.valueOf(200));
		authAndSale(ce, authPort, salePort, cardData, EntryType.SWIPE, BigDecimal.valueOf(400), BigDecimal.valueOf(100));
	}

	@Test
	public void junit_authAndSaleRepeat() throws Exception {
		String deviceName = "TD000023";
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		String cardData = ";5179819734719799=1809830959?";
		authAndSale(ce, authPort, salePort, cardData, EntryType.SWIPE, BigDecimal.valueOf(500), BigDecimal.valueOf(300));
	}

	@Test
	public void junit_authAndSaleMC2() throws Exception {
		String card = ";5454545454545454=10121015432112345601?"; //MC
		String deviceName = "TD000119";
		for(int i = 0; i < 5; i++) {
			authAndSale(host, deviceName, card, null);
			Thread.sleep(2000);
		}
	}
	@Test
	public void junit_authAndSaleMCDebit() throws Exception {
		String card = ";5108280000205847=1809879027?"; // MC - Debit
		String deviceName = "TD000023";
		authAndSale(host, deviceName, card, null);
	}
	@Test
	public void junit_authTooHigh() throws Exception {
		String deviceName = "TD000023";
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		ce.startCommunication(7);
		try {
			ce.testAuthV4(EntryType.SWIPE, ";5454545454545454=10121015432112345601?", BigDecimal.valueOf(20000));
		} finally {
			ce.stopCommunication();
		}
	}
	@Test
	public void junit_gxAuthTooHigh() throws Exception {
		String deviceName = "EV033483";
		ClientEmulator ce = new ClientEmulator(host, layer3Port, deviceName, keys.get(host).get(deviceName));
		long tranId = ce.getMasterId();
		MessageData_A0 data = new MessageData_A0();
		data.setAccountData(";5454545454545454=10121015432112345601?");
		data.setAmount(5000L);
		data.setCardType(CardType.CREDIT_SWIPE);
		data.setTransactionId(tranId++);
		ce.setPort(layer3Port);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.startCommunication(4);
		try {
			ce.sendMessage(data);
		} finally {
			ce.stopCommunication();
		}
	}

	@Test
	public void junit_gxAuthErrorBin() throws Exception {
		String deviceName = "EV033483";
		ClientEmulator ce = new ClientEmulator(host, layer3Port, deviceName, keys.get(host).get(deviceName));
		long tranId = ce.getMasterId();
		MessageData_A0 data = new MessageData_A0();
		data.setAccountData(";888888?");
		data.setAmount(5000L);
		data.setCardType(CardType.CREDIT_SWIPE);
		data.setTransactionId(tranId++);
		ce.setPort(layer3Port);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.startCommunication(4);
		try {
			ce.sendMessage(data);
		} finally {
			ce.stopCommunication();
		}
	}

	@Test
	public void junit_cashSale() throws Exception {
		String deviceName = "TD000022"; // "TD000436";
		cashSale(host, salePort, deviceName, keys.get(host).get(deviceName), null);
	}

	@Test
	public void junit_cashSaleEcc() throws Exception {
		String deviceName = "TD000899"; // "TD000436";
		cashSale(host, salePort, deviceName, keys.get(host).get(deviceName), null);
	}
	@Test
	public void junit_cashSaleDup() throws Exception {
		String deviceName = "TD000022"; // "TD000436";
		long tranId = 1452701330;
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.setPort(salePort);
		ce.startCommunication(7);
		try {
			ce.testSale(tranId, SaleType.CASH, 1, false);
		} finally {
			ce.stopCommunication();
		}
	}

	@Test
	public void junit_canceledSale() throws Exception {
		String deviceName = "TD000021";
		ClientEmulator ce = new ClientEmulator(host, salePort, deviceName, keys.get(host).get(deviceName));		
		MessageData_C4 md = new MessageData_C4();
		md.setBatchId(1);
		md.setDirection(MessageDirection.CLIENT_TO_SERVER);
		md.setMessageId(ce.messageId.incrementAndGet());
		//md.setMessageNumber(0); //we will ignore this anyway
		md.setReceiptResult(ReceiptResult.UNAVAILABLE);
		Calendar cal = Calendar.getInstance();
		//cal.setTimeInMillis(transactionId*1000);
		md.setSaleSessionStart(cal);
		md.setSaleType(SaleType.ACTUAL);
		md.setTransactionId(ce.messageId.incrementAndGet());
		md.setLineItemFormat((byte)0);
		md.setSaleAmount(BigDecimal.ZERO);
		md.setSaleResult(SaleResult.CANCELLED_MACHINE_FAILURE);			
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.setPort(salePort);
		ce.startCommunication(7);
		try {
			ce.sendMessage(md);
		} finally {
			ce.stopCommunication();
		}		
	}
	@Test
	public void junit_invalidMessageSale() throws Exception {
		String deviceName = "TD000021";
		ClientEmulator ce = new ClientEmulator(host, salePort, deviceName, keys.get(host).get(deviceName));
		MessageData_C4 md = new MessageData_C4() {
			public void writeData(ByteBuffer reply, boolean maskSensitiveData) throws IllegalStateException, BufferOverflowException {
				reply.put(getMessageNumber());
				reply.put(getMessageType().getValue());
				if(getMessageType().isMessageIdExpected())
					ProcessingUtils.writeLongInt(reply, getMessageId());
				ProcessingUtils.writeLongInt(reply, getTransactionId());
				ProcessingUtils.writeLongInt(reply, getBatchId());
				ProcessingUtils.writeByteInt(reply, '-');
				ProcessingUtils.writeTimestamp(reply, getSaleSessionStart());
				ProcessingUtils.writeByte(reply, getSaleResult().getValue());
				ProcessingUtils.writeStringAmount(reply, getSaleAmount());
				ProcessingUtils.writeByteInt(reply, getReceiptResult().getValue());
				ProcessingUtils.writeByte(reply, getLineItemFormat());
				ProcessingUtils.writeByteInt(reply, (byte) getLineItems().size());
				for(LineItem lineItem : getLineItems())
					lineItem.writeData(reply, maskSensitiveData);
			}
		};
		md.setBatchId(1);
		md.setDirection(MessageDirection.CLIENT_TO_SERVER);
		md.setMessageId(ce.messageId.incrementAndGet());
		// md.setMessageNumber(0); //we will ignore this anyway
		md.setReceiptResult(ReceiptResult.UNAVAILABLE);
		Calendar cal = Calendar.getInstance();
		// cal.setTimeInMillis(transactionId*1000);
		md.setSaleSessionStart(cal);
		md.setSaleType(SaleType.ACTUAL);
		md.setTransactionId(ce.messageId.incrementAndGet());
		md.setLineItemFormat((byte) 0);
		md.setSaleAmount(BigDecimal.ZERO);
		md.setSaleResult(SaleResult.CANCELLED_MACHINE_FAILURE);

		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.setPort(salePort);
		ce.startCommunication(7);
		try {
			ce.sendMessage(md);
		} finally {
			ce.stopCommunication();
		}
	}

	@Test
	public void junit_dexFile() throws Exception {
		String deviceName = "TD000022"; // "TD000021";
		String deviceSerialCd = "EE100000002"; // "EE100000001";
		sendDexFile(host, salePort, deviceName, deviceSerialCd, keys.get(host).get(deviceName), 1);
	}

	@Test
	public void junit_dexFile_dev() throws Exception {
		String deviceName = "TD000065"; // "TD000021";
		String deviceSerialCd = "EE100000015"; // "EE100000001";
		sendDexFile(host, salePort, deviceName, deviceSerialCd, keys.get(host).get(deviceName), 1);
	}

	@Test
	public void junit_dexFile2() throws Exception {
		String deviceName = "TD000021";
		String deviceSerialCd = "EE100000001";
		sendDexFile(host, salePort, deviceName, deviceSerialCd, keys.get(host).get(deviceName), 1);
	}

	@Test
	public void junit_spamDexFiles() throws Exception {
		String deviceName = "TD000021";
		String deviceSerialCd = "EE100000001";
		sendDexFile(host, salePort, deviceName, deviceSerialCd, keys.get(host).get(deviceName), 100);
	}

	protected void sendDexFile(String host, int salePort, String deviceName, String deviceSerialCd, byte[] encryptKey, int n) throws Exception {
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, encryptKey);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.setPort(salePort);
		ce.startCommunication(7);
		try {
			for(int i = 0; i < n; i++) {
				ce.testGenReqUSR(4);
				ce.testDexFileTransfer(deviceSerialCd);
				// ce.testDexFileTransfer("JUNKFORTEST");
			}
		} finally {
			ce.stopCommunication();
		}
	}

	@Test
	public void junit_edgeGDIFile() throws Exception {
		String deviceName = "TD000021";
		//String gdi = "A\nCFG=2\nSPEC=3002004\nREASON=0\nFW=BSK_Test_1\nBL=Bootloader = N/A\nDIAG=Diagnostic = N/A\nBEZEL=BT:B\rAV:2.5.1\rMN:54D908\rSN:100-0491BSK\rBV:Boot4.2.1\rHV:7.0.2\r\nMODEM=TYPE:208\rCGMM:GHI 467\rCCID:12345678901234567890\rCGSN:20\rCGMI:TelNotIt\rCGMR:1.2.3\r\n";
		String gdi = "A\nSPEC=3002004\nFW=BSK_Test_2\nBL=Bootloader = N/A\nDIAG=Diagnostic = N/A\nBEZEL=BT:B\rAV:2.5.2\rMN:54D907\rSN:100-0491BSK2\rBV:Boot4.2.2\rHV:7.0.4\r\nMODEM=TYPE:207\rCGMM:KLM 467\rCCID:12345678901234567891\rCGSN:777\rCGMI:TelNotIt2\rCGMR:3.5.6\r\n";
		//String gdi = "A\nSPEC=3002004\nFW=BSK_Test_3\nBL=Bootloader = N/A\nDIAG=Diagnostic = N/A\nC_00=TP:201\rMI:USAT\rMN:Edger\rAV:BSK_Test_4\rSN:EE100000001\rBV:BigOleBoot!\r\nC_01=TP:200\rMI:AVI\rMN:A90849\rSN:#123456\r\nC_02=TP:210\rCGMI:Telit\rCGMM:UE910-NAD\rCGMR:12.00.514\rCGSN:354676050014801\rCCID:1234567890123456789\rCSQ:3,40\rCIMI:6109890340\r\nC_03=TP:400\rMI:OTI\rMN:Saturn 6500-G6A\rSN:0006FD35\rAV:040110\rHV:1.0\r\n";
		// String gdi =
		// "A\nSPEC=3002004\nFW=BSK_Test_3\nBL=Bootloader = N/A\nDIAG=Diagnostic = N/A\nC_00=TP:201\rMI:USAT\rMN:Edger\rAV:BSK_Test_4\rSN:EE100000001\rBV:BigOleBoot!\r\nC_01=TP:200\rMI:AVI\rMN:A90849\rSN:#123456\r\nC_02=TP:206\rCGMI:Velit\rCGMM:VV00VV\rCGMR:N6.E3\rCGSN:354676050014801\rMEID:0004941\rCSQ:6,0\r\nC_03=TP:400\rMI:OTI\rMN:Saturn 6500-G6A\rSN:0006FD35\rAV:040110\rHV:1.0\r\nC_06=TP:100\rMI:AmpRF\rMN:2.0.0a\rSN:11-99\r\n";
		MessageData_CA dataCA = new MessageData_CA();
		dataCA.setRequestType(GenericRequestType.UPDATE_STATUS_REQUEST);
		((UpdateStatusRequest) dataCA.getRequestTypeData()).setStatusCodeBitmap(0x01);

		MessageData_C7 dataC7 = new MessageData_C7();
		dataC7.setContent(gdi.getBytes());
		Calendar cal = Calendar.getInstance();
		// cal.setTimeInMillis(4294967295000L);
		dataC7.setCreationTime(cal);
		dataC7.setFileType(FileType.GENERIC_DEVICE_INFORMATION);

		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.setPort(salePort);
		ce.startCommunication(7);
		try {
			ce.sendMessage(dataCA);
			ce.sendMessage(dataC7);
			// ce.sendMessage(dataC7);
			// ce.sendMessage(dataC7);
		} finally {
			ce.stopCommunication();
		}
	}

	@Test
	public void junit_badFile() throws Exception {
		String deviceName = "TD000021";
		ClientEmulator ce = new ClientEmulator(host, salePort, deviceName, keys.get(host).get(deviceName));
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.setPort(salePort);
		ce.startCommunication(7);
		try {
			ce.testGenReqUSR(4);
			ce.testLongFileTransferBadCRC(new File("D:/Documents/Test.txt"));
		} finally {
			ce.stopCommunication();
		}
	}
	@Test
	public void junit_extradata() throws Exception {
		String deviceName = "TD000021";
		ClientEmulator ce = new ClientEmulator(host, salePort, deviceName, keys.get(host).get(deviceName));
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.setPort(salePort);
		ce.startCommunication(7);
		try {
			ce.testGenReqUSR(4);
			ce.sendRawBytes(new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 });
			Thread.sleep(35000L);
		} finally {
			ce.stopCommunication();
		}
	}

	@Test
	public void junit_gxCfgFile() throws Exception {
		String deviceName = "EV035194";
		sendGxCfgFile(host, layer3Port, deviceName, keys.get(host).get(deviceName));
	}

	protected void sendGxCfgFile(String host, int salePort, String deviceName, byte[] encryptKey) throws Exception {
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, encryptKey);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.setPort(salePort);
		ce.startCommunication(4);
		try {
			ce.sendMessage(new MessageData_5C());
			MessageData_82 data82 = new MessageData_82();
			data82.setRequestNumber((byte) 1);
			ce.sendMessage(data82);
			ce.testGxFileTransfer(new File("C:\\Users\\bkrug\\Documents\\EV035194-CFG-N.hex"), FileType.CONFIGURATION_FILE);
			boolean done = false;
			while(!done) {
				MessageData_92 data92 = new MessageData_92();
				MessageData reply = ce.sendMessage(data92);
				switch(reply.getMessageType()) {
					case TERMINAL_UPDATE_STATUS:
						if(((MessageData_90) reply).getTerminalUpdateStatus() == 0) {
							done = true;
						} else {
							MessageData reply2 = ce.receiveReply();
							MessageData_2F data2F = new MessageData_2F();
							data2F.setAckedMessageNumber(reply2.getMessageNumber());
							ce.transmitMessage(data2F);
						}
						break;
					default:
						done = true;
				}
			}
		} finally {
			ce.stopCommunication();
		}
	}

	@Test
	public void junit_fillV3() throws Exception {
		String deviceName = "TD000023";
		String card = "639621150020000015=121256789";
		authAndFillV3(deviceName, card);
	}

	@Test
	public void junit_authFillAndSaleWithSameTranId() throws Exception {
		String deviceName = "TD000023";
		String fillCard = "639621150020000015=121256789";
		String chargeCard = ";5454545454545454=10121015432112345601?";
		ClientEmulator ce = new ClientEmulator(host, salePort, deviceName, keys.get(host).get(deviceName));
		long tranId = ce.getMasterId();
		ce.setPort(authPort);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.startCommunication(7);
		MessageData reply;
		try {
			reply = ce.testAuthV4(EntryType.SWIPE, fillCard, null, tranId);
		} finally {
			ce.stopCommunication();
		}
		boolean doFill;
		switch(reply.getMessageType()) {
			case AUTH_RESPONSE_4_1:
				AuthResponseTypeData artd = ((MessageData_C3) reply).getAuthResponseTypeData();
				AuthResponseType art = ((MessageData_C3) reply).getAuthResponseType();
				switch(art) {
					case PERMISSION:
						AuthPermissionActionCode ac = ((PermissionAuthResponse) artd).getActionCode();
						switch(ac) {
							case DRIVER_CARD:
								doFill = true;
								break;
							default:
								doFill = false;
								log.info("Cannot process sale because auth was rejected");
						}
						break;
					default:
						doFill = false;
						log.info("Cannot process fill because auth response was " + art);
				}
				break;
			default:
				doFill = false;
				log.info("Cannot process fill because response was " + reply.getMessageType());
		}
		if(doFill) {
			ce.startCommunication(7);
			try {
				reply = ce.testAuthV4(EntryType.SWIPE, chargeCard, BigDecimal.valueOf(500), tranId);
			} finally {
				ce.stopCommunication();
			}
			boolean doSale;
			switch(reply.getMessageType()) {
				case AUTH_RESPONSE_4_1:
					AuthResponseTypeData artd = ((MessageData_C3) reply).getAuthResponseTypeData();
					AuthResponseType art = ((MessageData_C3) reply).getAuthResponseType();
					switch(art) {
						case AUTHORIZATION:
						case AUTHORIZATION_V2:
							AuthResponseCodeC3 rc = ((AuthorizationAuthResponse) artd).getResponseCode();
							switch(rc) {
								case SUCCESS:
								case CONDITIONAL_SUCCESS:
									doSale = true;
									break;
								default:
									doSale = false;
									log.info("Cannot process sale because auth was rejected");
							}
							break;
						default:
							doSale = false;
							log.info("Cannot process sale because auth response was " + art);
					}
					break;
				default:
					doSale = false;
					log.info("Cannot process sale because response was " + reply.getMessageType());
			}
			if(doSale) {
				ce.setPort(salePort);
				log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
				ce.startCommunication(7);
				try {
					ce.testSingleSale(tranId, SaleType.ACTUAL, BigDecimal.valueOf(300), false);
				} finally {
					ce.stopCommunication();
				}
			}

			ce.setPort(salePort);
			log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
			ce.startCommunication(7);
			try {
				MessageData_C5 md = new MessageData_C5();
				md.setBatchId(0);
				md.setCreationTime(Calendar.getInstance());
				md.setEventId(tranId);
				md.setEventType(EventType.FILL);
				md.setNewBatchId(1);
				md.setContentFormat(SettlementFormat.FORMAT_0);
				Format0Content sfd = (Format0Content) md.getContentFormatData();
				sfd.setCashItems(2);
				sfd.setCashAmount(new BigDecimal(2200));
				sfd.setCashlessItems(6 * 6);
				sfd.setCashlessAmount(new BigDecimal(7.15 * 600));
				ce.sendMessage(md);
			} finally {
				ce.stopCommunication();
			}
		}
	}

	protected void authAndFillV3(String deviceName, String card) throws Exception {
		ClientEmulator ce = new ClientEmulator(host, salePort, deviceName, keys.get(host).get(deviceName));
		long tranId = ce.getMasterId();
		ce.setPort(authPort);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.startCommunication(7);
		MessageData reply;
		try {
			reply = ce.testAuthV4(EntryType.SWIPE, card, null);
		} finally {
			ce.stopCommunication();
		}
		boolean doFill;
		switch(reply.getMessageType()) {
			case AUTH_RESPONSE_4_1:
				AuthResponseTypeData artd = ((MessageData_C3)reply).getAuthResponseTypeData();
				AuthResponseType art = ((MessageData_C3)reply).getAuthResponseType();
				switch(art) {
					case PERMISSION:
						AuthPermissionActionCode ac = ((PermissionAuthResponse)artd).getActionCode();
						switch(ac) {
							case DRIVER_CARD:
								doFill = true;
								break;
							default:
								doFill = false;
								log.info("Cannot process sale because auth was rejected");
						}
						break;
					default:
						doFill = false;
						log.info("Cannot process fill because auth response was " + art);
				}
				break;
			default:
				doFill = false;
				log.info("Cannot process fill because response was " + reply.getMessageType());
		}
		if(doFill) {
			ce.setPort(salePort);
			log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
			ce.startCommunication(7);
			try {
				MessageData_C5 md = new MessageData_C5();
				md.setBatchId(0);
				md.setCreationTime(Calendar.getInstance());
				md.setEventId(tranId);
				md.setEventType(EventType.FILL);
				md.setNewBatchId(1);
				md.setContentFormat(SettlementFormat.FORMAT_0);
				Format0Content sfd = (Format0Content)md.getContentFormatData();
				sfd.setCashItems(2);
				sfd.setCashAmount(new BigDecimal(2200));
				sfd.setCashlessItems(6 * 6);
				sfd.setCashlessAmount(new BigDecimal(7.15 * 600));
				ce.sendMessage(md);
			} finally {
				ce.stopCommunication();
			}
		}
	}
	@Test
	public void junit_fillV2_button() throws Exception {
		authAndFillV2("EV033414", null, true, true);
	}
	@Test
	public void junit_fillV2_local() throws Exception {
		authAndFillV2("EV033414", "639621050020000015=121256789", true, false);
	}
	@Test
	public void junit_fillV2() throws Exception {
		authAndFillV2("EV035194", "639621150020000015=121256789", false, false);
	}

	@Test
	public void junit_redisplayFillV2() throws Exception {
		redisplayFillV2("EV035194", "639621150020000015=121256789");
	}

	@Test
	public void junit_fillV2_dev04() throws Exception {
		/*ca = 00158404589
		00174800665
		00128831226
		00182412571*/
		// authAndFillV2("EV050271", "639621151584045890=121256789", false, false);
		// authAndFillV2("EV050271", "63962115001288312260=121256789", false, false);
		// authAndFillV2("EV050271", "63962115001748006650=121256789", false, false);
		// authAndFillV2("EV050271", "63962115001824125710=121256789", false, false);
		authAndFillV2("EV035425", "639621150020000015=121256789", false, false);
	}
	
	@Test
	public void junit_fillV2_dev() throws Exception {
		host = "10.0.0.67";
		// authAndFillV2("TD000065", "639621150020000015=121256789", false, false);
		authAndFillV2("EV033414", null, true, true);
	}

	@Test
	public void junit_fillV3_dev04() throws Exception {
		authAndFillV3("TD000065", "639621150020002454=991252553");
	}

	@Test
	public void junit_fillV3_dev() throws Exception {
		host = "10.0.0.67";
		authAndFillV3("TD000065", "639621150020002454=991252553");
	}
	public void setCheckDigit(byte[] cardArray) {
		int checksum = addChecksum(cardArray, 0, cardArray.length - 1);
		checksum = (10 - (checksum % 10)) % 10;
		cardArray[cardArray.length - 1] = (byte) ('0' + checksum);
	}

	public int addChecksum(byte[] cardArray, int start, int end) {
		int len = cardArray.length;
		int checksum = 0;
		int tmp;

		for(int i = start + ((len + start) % 2); i < end; i += 2) {
			tmp = Character.getNumericValue(cardArray[i]) * 2;
			while(tmp > 9)
				tmp = (tmp % 10) + (tmp / 10);
			checksum += tmp;
		}
		for(int i = start + ((len + start + 1) % 2); i < end; i += 2) {
			tmp = Character.getNumericValue(cardArray[i]);
			checksum += tmp;
		}

		return checksum;
	}
	@Test
	public void junit_gxLocalBatch() throws Exception {
		String deviceName = "EV033414";
		ClientEmulator ce = new ClientEmulator(host, layer3Port, deviceName, keys.get(host).get(deviceName));
		MessageData_93 data = new MessageData_93();
		data.setCardType(CardType.INVENTORY_CARD);
		data.setCreditCardMagstripe("Err.DEXFill-->E4059902");
		long time = System.currentTimeMillis() / 1000;
		data.setSaleStartDate(time);
		data.setSaleAmount(0);
		data.setSaleTax(0);
		data.setTransactionId(time);
		data.setTransactionResult(TranDeviceResultType.SUCCESS);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.startCommunication(4);
		MessageData reply;
		try {
			reply = ce.sendMessage(data);
		} finally {
			ce.stopCommunication();
		}

	}

	@Test
	public void junit_gxLocalAuth() throws Exception {
		String deviceName = "EV033414";
		ClientEmulator ce = new ClientEmulator(host, layer3Port, deviceName, keys.get(host).get(deviceName));
		MessageData_2B data = new MessageData_2B();
		data.setCardType(CardType.CREDIT_SWIPE);
		data.setCreditCardMagstripe("123456782");
		long time = System.currentTimeMillis() / 1000;
		data.setSaleStartSeconds(time);
		data.setSaleAmount(222);
		data.setTransactionId(time);
		data.setTransactionResult(TranDeviceResultType.SUCCESS);
		data.setVendByteLength((byte) 1);
		MessageData_2B.NetBatch1LineItem li = (MessageData_2B.NetBatch1LineItem) data.addActualLineItem();
		li.setPositionNumber(1023);
		li.setReportedPrice(222);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.startCommunication(4);
		MessageData reply;
		try {
			reply = ce.sendMessage(data);
		} finally {
			ce.stopCommunication();
		}
	}

	@Test
	public void junit_gxGprsModemInfo() throws Exception {
		gxModemInfo(host, "EV033414");
	}
	
	@Test
	public void junit_gxCdmaModemInfo() throws Exception {
		gxCdmaModemInfo(host, "EV033414");
	}

	@Test
	public void junit_gxEthernetModemInfo() throws Exception {
		gxEthernetInfo(host, "EV033414");
	}

	protected void redisplayFillV2(String deviceName, String card) throws Exception {
		ClientEmulator ce = new ClientEmulator(host, salePort, deviceName, keys.get(host).get(deviceName));
		long tranId = ce.getMasterId();
		ce.setPort(layer3Port);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.startCommunication(4);
		try {
			MessageData_A8 md = new MessageData_A8();
			md.setCountersId(tranId);
			md.setDateAndTime(ConvertUtils.getLocalTime(System.currentTimeMillis(), "US/Mountain") / 1000);
			md.setFillCardMagstripe(card);
			md.setReasonCode(CounterReasonCode.REDISPLAY_CARD_SWIPE);
			md.setTotalAttemptedSession(makeBCDInt(13));
			md.setTotalBytesTransmitted(makeBCDInt(187654));
			md.setTotalCurrencyMoneyCounter(makeBCDInt(2300));
			md.setTotalCurrencyTransactionCounter(makeBCDInt(18));
			md.setTotalCashlessMoneyCounter(makeBCDInt(1575));
			md.setTotalCashlessTransactionCounter(makeBCDInt(11));
			md.setTotalPasscardMoneyCounter(makeBCDInt(100));
			md.setTotalPasscardTransactionCounter(makeBCDInt(1));
			ce.sendMessage(md);
		} finally {
			ce.stopCommunication();
		}
	}
	protected void authAndFillV2(String deviceName, String card, boolean local, boolean button) throws Exception {
		ClientEmulator ce = new ClientEmulator(host, salePort, deviceName, keys.get(host).get(deviceName));
		long tranId = ce.getMasterId();
		ce.setPort(layer3Port);
		boolean doFill;
		if(!local) {
			log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
			ce.startCommunication(4);
			MessageData reply;
			try {
				reply = ce.testAuthPermission(button ? EntryType.MANUAL : EntryType.SWIPE, card);
			} finally {
				ce.stopCommunication();
			}
			switch(reply.getMessageType()) {
				case PERMISSION_RESPONSE_1_0:
					PermissionResponseCodeAB rc = ((MessageData_AB)reply).getResponseCode();
					ABPermissionActionCode ac = ((MessageData_AB)reply).getActionCode();
					switch(rc) {
						case SUCCESS: case CONDITIONAL_SUCCESS:
							switch(ac) {
								case SHOW_CASHLESS_COUNTERS_REDISPLAY:
									doFill = true;
									break;
								default:
									doFill = false;
									log.info("Cannot process sale because action was " + ac);
									break;
								
							}
							break;
						default:
							doFill = false;
							log.info("Cannot process fill because auth was rejected");
					}
					break;
				default:
					doFill = false;
					log.info("Cannot process fill because response was " + reply.getMessageType());
			}
		} else {
			doFill = true;
		}
		
		if(doFill) {
			ce.setPort(layer3Port);
			log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
			ce.startCommunication(4);
			try {
				MessageData_A8 md = new MessageData_A8();
				md.setCountersId(tranId);
				md.setDateAndTime(ConvertUtils.getLocalTime(System.currentTimeMillis(), "US/Mountain") / 1000);
				md.setFillCardMagstripe(card);
				if(local) {
					if(button)
						md.setReasonCode(CounterReasonCode.FILL_MANUAL_LOCAL_NO_RESET_DISPLAYED);
					else
						md.setReasonCode(CounterReasonCode.FILL_SWIPE_LOCAL_NO_RESET_DISPLAYED);
				} else {
					if(button)
						md.setReasonCode(CounterReasonCode.FILL_MANUAL_NETWORK_NO_RESET_DISPLAYED);
					else
						md.setReasonCode(CounterReasonCode.FILL_SWIPE_NETWORK_NO_RESET_DISPLAYED);
				}
				md.setTotalAttemptedSession(makeBCDInt(13));
				md.setTotalBytesTransmitted(makeBCDInt(187654));
				md.setTotalCurrencyMoneyCounter(makeBCDInt(2300));
				md.setTotalCurrencyTransactionCounter(makeBCDInt(18));
				md.setTotalCashlessMoneyCounter(makeBCDInt(1575));
				md.setTotalCashlessTransactionCounter(makeBCDInt(11));
				md.setTotalPasscardMoneyCounter(makeBCDInt(100));
				md.setTotalPasscardTransactionCounter(makeBCDInt(1));
				ce.sendMessage(md);
			} finally {
				ce.stopCommunication();
			}
		}
	}
	protected static BCDInt makeBCDInt(int value) {
		return new BCDInt(value, String.valueOf(value));
	}
	@Test
	public void junit_fillV1() throws Exception {
		String deviceName = "EV033414";
		ClientEmulator ce = new ClientEmulator(host, salePort, deviceName, keys.get(host).get(deviceName));
		long tranId = ce.getMasterId();
		ce.setPort(layer3Port);
		long time = System.currentTimeMillis();
		log.debug("Starting communication at " + time + " (" + new Date() + "");
		
		int base = (int) ((time - 1293840000000L) / 30000);
		MessageData_86 data86 = new MessageData_86();
		data86.setTotalAttemptedSession(makeBCDInt(base / 1000));
		data86.setTotalBytesTransmitted(makeBCDInt(base / 2));
		data86.setTotalCurrencyMoneyCounter(makeBCDInt(base / 200));
		data86.setTotalCurrencyTransactionCounter(makeBCDInt(base / 21040));
		data86.setTotalCashlessMoneyCounter(makeBCDInt(base / 256));
		data86.setTotalCashlessTransactionCounter(makeBCDInt(base / 26201));
		data86.setTotalPasscardMoneyCounter(makeBCDInt(base / 3500));
		data86.setTotalPasscardTransactionCounter(makeBCDInt(base / 358921));
		
		MessageData_2B data2B = new MessageData_2B();
		data2B.setCardType(CardType.INVENTORY_CARD);
		data2B.setCreditCardMagstripe("Err.DEX Fill");
		data2B.setSaleAmount(0);
		data2B.setConvenienceFee(0);
		data2B.setSaleStartSeconds(ConvertUtils.getLocalTime(System.currentTimeMillis(), "US/Mountain") / 1000);
		data2B.setTransactionId(tranId);
		data2B.setTransactionResult(TranDeviceResultType.SUCCESS);
		
		ce.startCommunication(4);
		try {
			if(Math.random() < 0.5) {
				ce.sendMessage(data86);
				ce.sendMessage(data2B);			
			} else {
				ce.sendMessage(data2B);
				ce.sendMessage(data86);
			}		
		} finally {
			ce.stopCommunication();
		}
	}
	
	@Test
	public void junit_authAndCancelMC() throws Exception {
		String card = ";5454545454545454=10121015432112345601?"; //MC
		//String card = ";5454545454545454=11011015432112345601?"; //MC - exp date to cause socket timeout in simulation mode
		String deviceName = "TD000023";
		authAndSale(host, deviceName, card, BigDecimal.ZERO);
	}
	
	@Test
	public void junit_authAndSalePaymentTech_dev() throws Exception {
		host = "10.0.0.67";
		String card = ";5454545454545454=10121015432112345601?"; // MC
		// String card = ";5172965957958759=1809170125?";
		// String card = ";5454545454545454=11011015432112345601?"; //MC - exp date to cause socket timeout in simulation mode
		String deviceName = "EV033409";
		// for(int i = 0; i < 5; i++)
		authAndSale(host, deviceName, card, new BigDecimal(100));
	}

	@Test
	public void junit_authAndSaleInvalidReplenish() throws Exception {
		String card = ";5454545454545454=10121015432112345601?"; // MC
		// String card = ";5454545454545454=11011015432112345601?"; //MC - exp date to cause socket timeout in simulation mode
		String deviceName = "TD000023";
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));

		long tranId = ce.getMasterId();
		BigDecimal amount = new BigDecimal(500);
		ce.setPort(authPort);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.startCommunication(7);
		MessageData reply;
		try {
			reply = ce.testAuthV4(EntryType.SWIPE, card, amount);
		} finally {
			ce.stopCommunication();
		}
		boolean doSale;
		switch(reply.getMessageType()) {
			case AUTH_RESPONSE_4_1:
				AuthResponseTypeData artd = ((MessageData_C3) reply).getAuthResponseTypeData();
				AuthResponseType art = ((MessageData_C3) reply).getAuthResponseType();
				switch(art) {
					case AUTHORIZATION:
					case AUTHORIZATION_V2:
						AuthResponseCodeC3 rc = ((AuthorizationAuthResponse) artd).getResponseCode();
						switch(rc) {
							case SUCCESS:
							case CONDITIONAL_SUCCESS:
								doSale = true;
								break;
							default:
								doSale = false;
								log.info("Cannot process sale because auth was rejected");
						}
						break;
					default:
						doSale = false;
						log.info("Cannot process sale because auth response was " + art);
				}
				break;
			default:
				doSale = false;
				log.info("Cannot process sale because response was " + reply.getMessageType());
		}
		if(doSale) {
			ce.setPort(salePort);
			log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
			ce.startCommunication(7);
			try {
				MessageData_C4 md = new MessageData_C4();
				md.setBatchId(1);
				md.setDirection(MessageDirection.CLIENT_TO_SERVER);
				md.setMessageId(ce.messageId.incrementAndGet());
				// md.setMessageNumber(0); //we will ignore this anyway
				md.setReceiptResult(ReceiptResult.UNAVAILABLE);
				md.setSaleResult(SaleResult.SUCCESS);
				Calendar cal = Calendar.getInstance();
				cal.setTimeInMillis(tranId * 1000);
				md.setSaleSessionStart(cal);
				md.setSaleType(SaleType.ACTUAL);
				md.setTransactionId(tranId);
				md.setLineItemFormat((byte) 0);
				Format0LineItem li;
				li = (Format0LineItem) md.addLineItem();
				li.setComponentNumber(1);
				li.setItem(550);
				li.setDescription("B50D60202CAE9E2566E2950C964455DE0E6EA6D604C0C09DF316B326F5F7A1FC");
				li.setDuration(0);
				li.setPrice(amount);
				li.setQuantity(1);
				li.setSaleResult(SaleResult.SUCCESS);
				md.setSaleAmount(amount);
				ce.sendMessage(md);
			} finally {
				ce.stopCommunication();
			}
		}
	}

	@Test
	public void junit_authAndSalePaymentTech() throws Exception {
		String card = ";5454545454545454=10121015432112345601?"; //MC
		//String card = ";5454545454545454=11011015432112345601?"; //MC - exp date to cause socket timeout in simulation mode
		String deviceName = "TD000023";
		authAndSale(host, deviceName, card, null);
	}
	@Test
	public void junit_authAndSalePaymentTech_convfee() throws Exception {
		String card = ";5454545454545454=10121015432112345601?"; //MC
		//String card = ";5454545454545454=11011015432112345601?"; //MC - exp date to cause socket timeout in simulation mode
		String deviceName = "TD000023";
		authAndSale(host, deviceName, card, null, true);
	}
	@Test
	public void junit_authAndSaleECF() throws Exception {
		String card = "4055011111111111=10121015432112345678";
		// String card = ";5454545454545454=10121015432112345601?"; // MC
		// String card = ";4300123412341017=12100001?"; //Visa
		String deviceName = "TD000587";
		// authAndSale(host, deviceName, card, null);
		authAndSale(host, authPort, salePort, deviceName, keys.get(host).get(deviceName), card, EntryType.SWIPE, BigDecimal.valueOf(200), BigDecimal.valueOf(200), false);
		// authAndSale(host, authPort, salePort, deviceName, keys.get(host).get(deviceName), card, EntryType.SWIPE, BigDecimal.valueOf(355), BigDecimal.valueOf(125), false);
		// authAndSale(host, authPort, salePort, deviceName, keys.get(host).get(deviceName), card, EntryType.SWIPE, BigDecimal.valueOf(301), BigDecimal.valueOf(125), false);
		// authAndSale(host, authPort, salePort, deviceName, keys.get(host).get(deviceName), card, EntryType.SWIPE, BigDecimal.valueOf(304), BigDecimal.valueOf(125), false);
		// authAndSale(host, authPort, salePort, deviceName, keys.get(host).get(deviceName), card, EntryType.SWIPE, BigDecimal.valueOf(361), BigDecimal.valueOf(125), false);
		// authAndSale(host, authPort, salePort, deviceName, keys.get(host).get(deviceName), card, EntryType.SWIPE, BigDecimal.valueOf(380), BigDecimal.valueOf(125), false);
	}
	@Test
	public void junit_cashWithDupAuth() throws Exception {
		String card = ";4300123412341017=12100001?"; // Visa
		String deviceName = "TD000587";
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.setPort(salePort);
		ce.startCommunication(7);
		try {
			ce.testSingleSale(ce.getMasterId(), SaleType.CASH, new BigDecimal(125), false);
		} finally {
			ce.stopCommunication();
		}
		authAndSale(ce, authPort, salePort, card, EntryType.SWIPE, new BigDecimal(300), new BigDecimal(175), false);
	}

	@Test
	public void junit_cashWithDupAuth2() throws Exception {
		String card = ";4300123412341017=12100001?"; // Visa
		String deviceName = "TD000587";
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		long tranId = ce.getMasterId();
		authAndSale(ce, authPort, salePort, card, EntryType.SWIPE, new BigDecimal(300), new BigDecimal(175), false);
		ce.setPort(salePort);
		ce.startCommunication(7);
		try {
			ce.testSingleSale(tranId, SaleType.CASH, new BigDecimal(125), false);
		} finally {
			ce.stopCommunication();
		}
	}
	@Test
	public void junit_authAndSaleV32() throws Exception {
		String deviceName = "EV036214";
		long transactionId = System.currentTimeMillis() / 1000;
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.setPort(layer3Port);
		ce.startCommunication(4);
		try {
			ce.testAuthV32(transactionId, 0, CardType.CREDIT_ISIS.getValue(), 150, null, 0, null, "6011000995500000=20121015432112345678", null, AEAdditionalDataType.ISIS_SMARTTAP.getValue(), new String(ByteArrayUtils.fromHex("DF5310025A58313739353637353438333143460EDF21090116513583514525619000")));
		} finally {
			ce.stopCommunication();
		}
	}

	@Test
	public void junit_authAndSaleErrorBin() throws Exception {
		String card = ";88888888?"; //none
		String deviceName = "TD000023";
		authAndSale(host, deviceName, card, null);
	}
	@Test
	public void junit_authAndCancelECF() throws Exception {
		String card = ";4300123412341017=12100001?"; //Visa
		String deviceName = "TD000052";
		authAndSale(host, deviceName, card, BigDecimal.ZERO);
	}
	@Test
	public void junit_authFakeMagtek() throws Exception {
		String card = ";5454545454545454=10121015432112345601?"; //MC
		String deviceName = "TD000023";
		authFakeMagtek(host, authPort, deviceName, keys.get(host).get(deviceName), card, new BigDecimal("5.46"));
	}
	@Test
	public void junit_authAndSaleMagtekMC() throws Exception {
		String maskedTrack = ";5473000010000014=25120000000000000000?";// MC
		byte[] encryptedCard = ByteArrayUtils.fromHex("479C236ACCB11BFDC3F6C189B9BFE0852A1D3F102B2A5330F320B1E11F6A893644F22C438EA6D088");
		// String deviceName = "EV033406";
		String deviceName = "EV035194";
		byte[] ksn = ByteArrayUtils.fromHex("9011040B005166000054");
		authAndSaleMagtek(host, unifedlayerPort, deviceName, DeviceType.KIOSK, maskedTrack.length(), ksn, encryptedCard, new BigDecimal("1.20"));
	}

	@Test
	public void junit_authAndSaleMagtekDiscover() throws Exception {
		String maskedTrack = ";6011000040000000=20120000000000000000?";// Discover
		byte[] encryptedCard = ByteArrayUtils.fromHex("F84D37ADF8BEE6EDB9185FA7792CC4923D130C2662CEAE4E845448B7A40B88CDB4A0320B4C74F729");
		// String deviceName = "EV033409";
		String deviceName = "EV035194";
		byte[] ksn = ByteArrayUtils.fromHex("9011040B005166000055");
		authAndSaleMagtek(host, unifedlayerPort, deviceName, DeviceType.KIOSK, maskedTrack.length(), ksn, encryptedCard, new BigDecimal("10.00"));
	}

	@Test
	public void junit_authAndSaleMagtekCorrupt() throws Exception {
		String maskedTrack = ";6011000040000000=20120000000000000000?";// Discover
		byte[] encryptedCard = ByteArrayUtils.fromHex("F84D37ADF8BEE6EDB9185FA7792CC4923D130C2662CEAE4E845448B7A40B88CDB4A0320B4C74F729");
		String deviceName = "EV033409";
		byte[] ksn = ByteArrayUtils.fromHex("9011040B005166110055");
		authAndSaleMagtek(host, unifedlayerPort, deviceName, DeviceType.KIOSK, maskedTrack.length(), ksn, encryptedCard, new BigDecimal("1.20"));
	}

	@Test
	public void junit_authAndSaleOTI_DUKPT() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 200;		
		String attributes = null;
		String tranResult = "S";
		String tranDetails = "A0|200|" + amount + "|1|Item #1";

		// String ksnHex = "62994996340010200005";
		String ksnHex = "FFFFFFE0025B3F800002";
		Map<String, byte[]> encryptedCards = new LinkedHashMap<>();
		// From OTI

		// encryptedCards.put("4761730000000011=812201001230000000?",
		// ByteArrayUtils.fromHex("FC3EE934DF8167181EEADDCD473D6914F95119424D2C5F1133F01F256EF19712DF81690104DF816A0AFFFFFFE0025B3F800002DF816F0100DF700111DF690180"));
		encryptedCards.put("4761730000000011=812201001230000000?", ByteArrayUtils.fromHex("FC3EE934DF8167181EEADDCD473D6914F95119424D2C5F1133F01F256EF19712DF81690101DF816A0AFFFFFFE0025B3F800002DF816F0100DF700111DF690180"));
		/*
		encryptedCards.put(";4055011111111129=18121015432112345678?", ByteArrayUtils.fromHex("FC8178E98175DF81678140F49A105573C7B2C654D52695EAAB8FC725D86AC926CFBAD386415080A78B7A2B64156E25AE6A26D4A9EBA56077285C5B00F23F272BDDA9293ED2F3A787550AB1DF81690104DF816A0A62994996340010200005DF816F0100DF816A0A62994996340010200005DF8168021129DF690180"));
		encryptedCards.put(";5424180279791732=05121011000001234567?", ByteArrayUtils.fromHex("FC8178E98175DF81678140F49A105573C7B2C654D52695EAAB8FC721993995012E87882F7BB5A95742F32613416B890ABB7AF1193DAB55C044E240EBCEBEFE8A389A27A5E28E5363792F13DF81690104DF816A0A62994996340010200005DF816F0100DF816A0A62994996340010200005DF8168021732DF690180"));
		encryptedCards.put(";6011000995500000=20121015432112345678?", ByteArrayUtils.fromHex("FC8178E98175DF81678140F49A105573C7B2C654D52695EAAB8FC705369742F30691A8F70CF54D037B49159D28978E911A609F4020D379CC967033FB1431F5B1B666B559EA8DF77BE81DE3DF81690104DF816A0A62994996340010200005DF816F0100DF816A0A62994996340010200005DF8168020000DF690180"));
		encryptedCards.put(";371449635398431=20121015432112345678?", ByteArrayUtils.fromHex("FC8170E9816DDF8167813843B22A63F36EC1B238AE91036FADDCA38D70A16D88969FF2D40522038AC3FD473BEF8CA62AE36BB5C867F126FEED032819D6B3B2A8A4325FDF81690104DF816A0A62994996340010200005DF816F0100DF816A0A62994996340010200005DF8168028431DF690180"));
		encryptedCards.put(";6396212001298146869=9912008655564?", ByteArrayUtils.fromHex("FC8170E9816DDF81678138055301260747187D292C14EDD82443ED7B03AEFEAFE2A362B8D49537757DFB139E84C51C3B45854D359B064449B678B6BC401D120BEF0906DF81690104DF816A0A62994996340010200005DF816F0100DF816A0A62994996340010200005DF8168026869DF690180"));

		/*
		encryptedCards.put(";4055011111111129=18121015432112345678?", ByteArrayUtils.fromHex("FC8178E98175DF8167814071FBC6845FE1F92CEE23784F85A4CEE0D9FB696C6B8414EEB6F2B86A9C9BA0782B45540AB9A45733546AEF9CEF11610FDFFE43DA1A0708F30B07447766BAAFBCDF81690104DF816A0A62994996340010200005DF816F0100DF816A0A62994996340010200005DF8168021129DF690180"));		
		encryptedCards.put(";5424180279791732=05121011000001234567?", ByteArrayUtils.fromHex("FC8178E98175DF8167814071FBC6845FE1F92CEE23784F85A4CEE07E97A7256EE18331FBD7A8A3E069D1C79C3100FBF62674ED68DDDBB2187EAD7D66BD68BEB526308D905C4068E3D27BD0DF81690104DF816A0A62994996340010200005DF816F0100DF816A0A62994996340010200005DF8168021732DF690180"));
		encryptedCards.put(";6011000995500000=20121015432112345678?", ByteArrayUtils.fromHex("FC8178E975DF8167814071FBC6845FE1F92CEE23784F85A4CEE090E8ECFA305CDD5FDDE6B729EF2EF65D742BAE5259293665C9081F9EC027A7F949F1E73DC5D0BE49DEF899965748E3EADF81690104DF816A0A62994996340010200005DF816F0100DF816A0A62994996340010200005DF8168020000DF690180"));
		encryptedCards.put(";371449635398431=20121015432112345678?", ByteArrayUtils.fromHex("FC70E9816DDF81678138B8C1C1EC47369FF4E2CB428E20E9E24CDC714F9537133B3E8E14A6B747F20432312B0AB36167C8FB9CE96B24BB4086A6DDABEFBED7D3C745DF81690104DF816A0A62994996340010200005DF816F0100DF816A0A62994996340010200005DF8168028431DF690180"));
		encryptedCards.put(";6396212001298146869=9912008655564?", ByteArrayUtils.fromHex("FC8170E9816DDF816781382915829EDC85DFE6C541A3E1EDEFA3244FEFFC44DEBCE83AFA87E43E6D942A06A2323835B09E87B57D29FF22DEF7B0AA0AD7866287C015D4DF81690104DF816A0A62994996340010200005DF816F0100DF816A0A62994996340010200005DF8168026869DF690180"));
		// */
		for(Map.Entry<String, byte[]> entry : encryptedCards.entrySet()) {
			EC2AuthResponse response = ec.authEncrypted(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, ++tranId, amount, 6, entry.getKey().length(), StringUtils.toHex(entry.getValue()), ksnHex, String.valueOf(EntryType.SWIPE.getValue()), attributes);
			if(response == null)
				throw new Exception("Received null response");
			String message = getBasicResponseMessage(response);
			if(response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED) {
				log.info(message);
				EC2Response batchResponse = ec.batch(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, tranId, amount, tranResult, tranDetails, attributes);
				if(batchResponse == null)
					throw new Exception("Received null response");
				message = getBasicResponseMessage(batchResponse);
				if(batchResponse.getReturnCode() == EportConnectSOAPTest.RES_OK)
					log.info(message);
				else
					throw new Exception(message);
			} else
				throw new Exception(message);
		}
	}

	@Test
	public void junit_authAndSaleOTI_DUKPT_dev() throws Exception {
		EC2ServiceAPI ec = getEC2Service("dev");
		String serialCd = "K3MTB000001"; // "K3BK000002";
		String username = "bkrug";
		String password = "k#mqDttpcjMCN4Ag#ZVN"; // "HswFWc2#+tpHCmHzqp*c"; // "33053795";

		long tranId = System.currentTimeMillis() / 1000;
		long amount = 200;
		String attributes = null;
		String tranResult = "S";
		String tranDetails = "A0|200|" + amount + "|1|Item #1";

		String ksnHex = "62994996340010200005";
		Map<String, byte[]> encryptedCards = new LinkedHashMap<>();
		encryptedCards.put(";4055011111111129=18121015432112345678?", ByteArrayUtils.fromHex("FC8178E98175DF8167814071FBC6845FE1F92CEE23784F85A4CEE0D9FB696C6B8414EEB6F2B86A9C9BA0782B45540AB9A45733546AEF9CEF11610FDFFE43DA1A0708F30B07447766BAAFBCDF81690104DF816A0A62994996340010200005DF816F0100DF816A0A62994996340010200005DF8168021129DF690180"));
		/*
		encryptedCards.put(";5424180279791732=05121011000001234567?", ByteArrayUtils.fromHex("FC8178E98175DF8167814071FBC6845FE1F92CEE23784F85A4CEE07E97A7256EE18331FBD7A8A3E069D1C79C3100FBF62674ED68DDDBB2187EAD7D66BD68BEB526308D905C4068E3D27BD0DF81690104DF816A0A62994996340010200005DF816F0100DF816A0A62994996340010200005DF8168021732DF690180"));
		encryptedCards.put(";6011000995500000=20121015432112345678?", ByteArrayUtils.fromHex("FC8178E975DF8167814071FBC6845FE1F92CEE23784F85A4CEE090E8ECFA305CDD5FDDE6B729EF2EF65D742BAE5259293665C9081F9EC027A7F949F1E73DC5D0BE49DEF899965748E3EADF81690104DF816A0A62994996340010200005DF816F0100DF816A0A62994996340010200005DF8168020000DF690180"));
		encryptedCards.put(";371449635398431=20121015432112345678?", ByteArrayUtils.fromHex("FC70E9816DDF81678138B8C1C1EC47369FF4E2CB428E20E9E24CDC714F9537133B3E8E14A6B747F20432312B0AB36167C8FB9CE96B24BB4086A6DDABEFBED7D3C745DF81690104DF816A0A62994996340010200005DF816F0100DF816A0A62994996340010200005DF8168028431DF690180"));
		encryptedCards.put(";6396212001298146869=9912008655564?", ByteArrayUtils.fromHex("FC8170E9816DDF816781382915829EDC85DFE6C541A3E1EDEFA3244FEFFC44DEBCE83AFA87E43E6D942A06A2323835B09E87B57D29FF22DEF7B0AA0AD7866287C015D4DF81690104DF816A0A62994996340010200005DF816F0100DF816A0A62994996340010200005DF8168026869DF690180"));
		// */
		for(Map.Entry<String, byte[]> entry : encryptedCards.entrySet()) {
			EC2AuthResponse response = ec.authEncrypted(username, password, serialCd, ++tranId, amount, 6, entry.getKey().length(), StringUtils.toHex(entry.getValue()), ksnHex, String.valueOf(EntryType.SWIPE.getValue()), attributes);
			if(response == null)
				throw new Exception("Received null response");
			String message = getBasicResponseMessage(response);
			if(response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED) {
				log.info(message);
				EC2Response batchResponse = ec.batch(username, password, serialCd, tranId, amount, tranResult, tranDetails, attributes);
				if(batchResponse == null)
					throw new Exception("Received null response");
				message = getBasicResponseMessage(batchResponse);
				if(batchResponse.getReturnCode() == EportConnectSOAPTest.RES_OK)
					log.info(message);
				else
					throw new Exception(message);
			} else
				throw new Exception(message);
		}
	}
	
	// K3MT90EA17050D002900
	@Test
	public void junit_EmvTestTmpPasscode() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		String deviceSerialCd = "K3MT90EA17050D002900";
		String username = "jstest";
		String password = "PdpCRpxt29PYRNvvbt+G";	// PdpCRpxt29PYRNvvbt+G // 01232403
		log.info("Sending process updates for " + deviceSerialCd);
		// EC2ProcessUpdatesResponse response = ec.processUpdates("bkrug", "41540199", deviceSerialCd, 0, 2, "BSK Test", "1.1A", "");
		EC2ProcessUpdatesResponse response = ec.processUpdates(username, password, deviceSerialCd, 0, 2, "EMV Test", "1.1A", "");
		// EC2ProcessUpdatesResponse response = ec.processUpdates("testaccount1", "52927513", deviceSerialCd, 0, 2, "BSK Test", "1.1A", "");
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}
	@Test
	public void junit_authAndSaleIdtechVend() throws Exception {
		String [] filenameList = new String [] { 
			"IDTECH_VEND3AR-EMV_CONTACT-visa_advt_v601baseline_card_20160303_326.bin" 
//			"IDTECH_VENDX_ENHANCED_ENCRYPTED_MSR-SWIPE-Amex_NEOv1.00.046_TagFFFC0100_TestKey20160129.bin"				
		};

		boolean result = authAndSaleIdtechVend(filenameList);
		assertTrue(result);
	}
	public boolean authAndSaleIdtechVend(String [] filenameList) throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		String serialCd = "K3MT90EA17050D002900";
		String username = "jstest";
		String password = "PdpCRpxt29PYRNvvbt+G";	// PdpCRpxt29PYRNvvbt+G // 01232403

		long tranId = System.currentTimeMillis() / 1000;
		long amount = 200;
		String attributes = null;
		String tranResult = "S";
		String tranDetails = "A0|200|" + amount + "|1|Item #1";

//		String ksnHex = "62994996340010200005";
		String ksnHex = null;

		boolean result = false;
		
		for (String filename: filenameList) {
			CardReaderType cardReaderType = IdtechVendXDataBlob.cardReaderTypeFromFilename(filename);
			EntryType entryType = IdtechVendXDataBlob.entryTypeFromFilename(filename);
			byte [] fileContent = EMVTestMain.fileContentForFilename(filename);
			
			CardReaderDataBlob cardReaderData = CardReaderDataBlob.getInstance(cardReaderType, fileContent, entryType, filename);
			
			String key = "";
//			byte [] value = cardReaderData.getDataBuffer();
			byte [] value = cardReaderData.entireContents();
			
			EC2AuthResponse response = ec.authEncrypted(username, password, 
					serialCd, 
					++tranId, 
					amount, 
					cardReaderType.getValue(), 
					value.length, 
					StringUtils.toHex(value), 
					ksnHex,
					String.valueOf(entryType.getValue()), 
					attributes);
			String message = "";
			if(response == null) {
				message = "Received null response";
			} else {
				message = getBasicResponseMessage(response);
				result = response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED; 
				if(result) {
					log.info(message);
					EC2Response batchResponse = ec.batch(username, password, serialCd, tranId, amount, tranResult, tranDetails, attributes);
					if(batchResponse == null)
						throw new Exception("Received null response");
					message = getBasicResponseMessage(batchResponse);
					result = batchResponse.getReturnCode() == EportConnectSOAPTest.RES_OK; 
				}
			}
			log.info(message);
			if (!result) {
				break;
			}
		}
		return result;
	}


	@Test
	public void junit_authAndSaleOTI_DUKPT_ecc() throws Exception {
		EC2ServiceAPI ec = getEC2Service("ecc");
		String serialCd = "K3MTB000004";
		String username = "ectest";
		String password = "e3V2*pa*cn@w#RYSgvXn";

		long tranId = System.currentTimeMillis() / 1000;
		long amount = 200;
		String attributes = null;
		String tranResult = "S";
		String tranDetails = "A0|200|" + amount + "|1|Item #1";

		String ksnHex = "62994996340010200005";
		Map<String, byte[]> encryptedCards = new LinkedHashMap<>();
		encryptedCards.put(";4055011111111129=18121015432112345678?", ByteArrayUtils.fromHex("FC8178E98175DF8167814071FBC6845FE1F92CEE23784F85A4CEE0D9FB696C6B8414EEB6F2B86A9C9BA0782B45540AB9A45733546AEF9CEF11610FDFFE43DA1A0708F30B07447766BAAFBCDF81690104DF816A0A62994996340010200005DF816F0100DF816A0A62994996340010200005DF8168021129DF690180"));
		/*
		encryptedCards.put(";5424180279791732=05121011000001234567?", ByteArrayUtils.fromHex("FC8178E98175DF8167814071FBC6845FE1F92CEE23784F85A4CEE07E97A7256EE18331FBD7A8A3E069D1C79C3100FBF62674ED68DDDBB2187EAD7D66BD68BEB526308D905C4068E3D27BD0DF81690104DF816A0A62994996340010200005DF816F0100DF816A0A62994996340010200005DF8168021732DF690180"));
		encryptedCards.put(";6011000995500000=20121015432112345678?", ByteArrayUtils.fromHex("FC8178E975DF8167814071FBC6845FE1F92CEE23784F85A4CEE090E8ECFA305CDD5FDDE6B729EF2EF65D742BAE5259293665C9081F9EC027A7F949F1E73DC5D0BE49DEF899965748E3EADF81690104DF816A0A62994996340010200005DF816F0100DF816A0A62994996340010200005DF8168020000DF690180"));
		encryptedCards.put(";371449635398431=20121015432112345678?", ByteArrayUtils.fromHex("FC70E9816DDF81678138B8C1C1EC47369FF4E2CB428E20E9E24CDC714F9537133B3E8E14A6B747F20432312B0AB36167C8FB9CE96B24BB4086A6DDABEFBED7D3C745DF81690104DF816A0A62994996340010200005DF816F0100DF816A0A62994996340010200005DF8168028431DF690180"));
		encryptedCards.put(";6396212001298146869=9912008655564?", ByteArrayUtils.fromHex("FC8170E9816DDF816781382915829EDC85DFE6C541A3E1EDEFA3244FEFFC44DEBCE83AFA87E43E6D942A06A2323835B09E87B57D29FF22DEF7B0AA0AD7866287C015D4DF81690104DF816A0A62994996340010200005DF816F0100DF816A0A62994996340010200005DF8168026869DF690180"));
		// */
		for(Map.Entry<String, byte[]> entry : encryptedCards.entrySet()) {
			EC2AuthResponse response = ec.authEncrypted(username, password, serialCd, ++tranId, amount, 6, entry.getKey().length(), StringUtils.toHex(entry.getValue()), ksnHex, String.valueOf(EntryType.SWIPE.getValue()), attributes);
			if(response == null)
				throw new Exception("Received null response");
			String message = getBasicResponseMessage(response);
			if(response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED) {
				log.info(message);
				EC2Response batchResponse = ec.batch(username, password, serialCd, tranId, amount, tranResult, tranDetails, attributes);
				if(batchResponse == null)
					throw new Exception("Received null response");
				message = getBasicResponseMessage(batchResponse);
				if(batchResponse.getReturnCode() == EportConnectSOAPTest.RES_OK)
					log.info(message);
				else
					throw new Exception(message);
			} else
				throw new Exception(message);
		}
	}

	@Test
	public void junit_testNonExistentDevice() throws Exception {
		String deviceName = "TD999998";
		sendUSR(host, salePort, deviceName, keys.get(host).get(deviceName), 1);
	}
	@Test
	public void junit_sendUSR() throws Exception {
		String deviceName = "TD000021";
		sendUSR(host, salePort, deviceName, keys.get(host).get(deviceName), 1);
	}
	@Test
	public void junit_doEdgeSession() throws Exception {
		// String deviceName = "TD000065";
		// String deviceName = "TD000925"; // "TD000023";
		String deviceName = "TD000597";
		Calendar cal = Calendar.getInstance();
		/*
		// cal.add(Calendar.YEAR, 100);
		cal.setTimeInMillis(4294967295000L);
		MessageData_C7 futureLogFile = new MessageData_C7();
		futureLogFile.setContent("This is a nice log of what happened on the machine".getBytes(ProcessingConstants.US_ASCII_CHARSET));
		futureLogFile.setCreationTime(cal);
		futureLogFile.setFileName("My Future Short Log File");
		futureLogFile.setFilesRemaining(0);
		futureLogFile.setFileType(FileType.LOG_FILE);
		doEdgeSession(host, salePort, deviceName, keys.get(host).get(deviceName), futureLogFile);
		
		// doEdgeSession(host, salePort, deviceName, keys.get(host).get(deviceName));
		*/ // *
		MessageData_C7 shortPVLFile = new MessageData_C7();
		shortPVLFile.setContent("1203=From the Device\n52=ABCDEF0123456789ABCDEF0123456789\n1404=SEE ME PLEASE".getBytes(ProcessingConstants.US_ASCII_CHARSET));
		shortPVLFile.setCreationTime(cal);
		shortPVLFile.setFileName("My Short Property File");
		shortPVLFile.setFilesRemaining(2);
		shortPVLFile.setFileType(FileType.PROPERTY_LIST);
		doEdgeSession(host, salePort, deviceName, keys.get(host).get(deviceName), shortPVLFile);
		/*
		MessageData_C7 shortEXEFile = new MessageData_C7();
		byte[] bytes = new byte[200];
		new Random().nextBytes(bytes);
		shortEXEFile.setContent(bytes);
		shortEXEFile.setCreationTime(cal);
		shortEXEFile.setFileName("My Short Executable File");
		shortEXEFile.setFilesRemaining(1);
		shortEXEFile.setFileType(FileType.EXECUTABLE_FILE);

		MessageData_C7 shortLogFile = new MessageData_C7();
		shortLogFile.setContent("This is a nice log of what happened on the machine".getBytes(ProcessingConstants.US_ASCII_CHARSET));
		shortLogFile.setCreationTime(cal);
		shortLogFile.setFileName("My Short Log File");
		shortLogFile.setFilesRemaining(0);
		shortLogFile.setFileType(FileType.LOG_FILE);
		doEdgeSession(host, salePort, deviceName, keys.get(host).get(deviceName), shortPVLFile, shortEXEFile, shortLogFile);
		// */
		/*
		MessageData_C6 event1 = new MessageData_C6();
		event1.setComponentNumber(1);
		event1.setEventData("Is this a temporary message?");
		event1.setEventId(cal.getTimeInMillis() / 1000);
		event1.setEventTime(cal);
		event1.setEventType(EventType.DEVICE_ALERT);
		doEdgeSession(host, salePort, deviceName, keys.get(host).get(deviceName), event1);
		// */
		// doEdgeSession(host, salePort, deviceName, keys.get(host).get(deviceName));
	}

	@Test
	public void junit_doG9Session() throws Exception {
		// String deviceName = "TD000065";
		// String deviceName = "TD000925"; // "TD000023";
		String deviceName = "TD000908";
		Calendar cal = Calendar.getInstance();
		/*
		// cal.add(Calendar.YEAR, 100);
		cal.setTimeInMillis(4294967295000L);
		MessageData_C7 futureLogFile = new MessageData_C7();
		futureLogFile.setContent("This is a nice log of what happened on the machine".getBytes(ProcessingConstants.US_ASCII_CHARSET));
		futureLogFile.setCreationTime(cal);
		futureLogFile.setFileName("My Future Short Log File");
		futureLogFile.setFilesRemaining(0);
		futureLogFile.setFileType(FileType.LOG_FILE);
		doEdgeSession(host, salePort, deviceName, keys.get(host).get(deviceName), futureLogFile);
		
		// doEdgeSession(host, salePort, deviceName, keys.get(host).get(deviceName));
		*/ // *
		MessageData_C7 shortPVLFile = new MessageData_C7();
		shortPVLFile.setContent("1203=From the Device\n52=ABCDEF0123456789ABCDEF0123456789\n1404=SEE ME PLEASE".getBytes(ProcessingConstants.US_ASCII_CHARSET));
		shortPVLFile.setCreationTime(cal);
		shortPVLFile.setFileName("My Short Property File");
		shortPVLFile.setFilesRemaining(2);
		shortPVLFile.setFileType(FileType.PROPERTY_LIST);
		doEdgeSession(host, salePort, deviceName, keys.get(host).get(deviceName), shortPVLFile);
		/*
		MessageData_C7 shortEXEFile = new MessageData_C7();
		byte[] bytes = new byte[200];
		new Random().nextBytes(bytes);
		shortEXEFile.setContent(bytes);
		shortEXEFile.setCreationTime(cal);
		shortEXEFile.setFileName("My Short Executable File");
		shortEXEFile.setFilesRemaining(1);
		shortEXEFile.setFileType(FileType.EXECUTABLE_FILE);
		
		MessageData_C7 shortLogFile = new MessageData_C7();
		shortLogFile.setContent("This is a nice log of what happened on the machine".getBytes(ProcessingConstants.US_ASCII_CHARSET));
		shortLogFile.setCreationTime(cal);
		shortLogFile.setFileName("My Short Log File");
		shortLogFile.setFilesRemaining(0);
		shortLogFile.setFileType(FileType.LOG_FILE);
		doEdgeSession(host, salePort, deviceName, keys.get(host).get(deviceName), shortPVLFile, shortEXEFile, shortLogFile);
		// */
		/*
		MessageData_C6 event1 = new MessageData_C6();
		event1.setComponentNumber(1);
		event1.setEventData("Is this a temporary message?");
		event1.setEventId(cal.getTimeInMillis() / 1000);
		event1.setEventTime(cal);
		event1.setEventType(EventType.DEVICE_ALERT);
		doEdgeSession(host, salePort, deviceName, keys.get(host).get(deviceName), event1);
		// */
		// doEdgeSession(host, salePort, deviceName, keys.get(host).get(deviceName));
	}
	@Test
	public void junit_alertEvent() throws Exception {
		String deviceName = "TD000021";
		MessageData_C6 event1 = new MessageData_C6();
		event1.setComponentNumber(1);
		event1.setEventData("As first in session; Is this a temporary message?");
		Calendar cal = Calendar.getInstance();
		event1.setEventId(cal.getTimeInMillis() / 1000);
		event1.setEventTime(cal);
		event1.setEventType(EventType.DEVICE_ALERT);
		ClientEmulator ce = new ClientEmulator(host, salePort, deviceName, keys.get(host).get(deviceName));
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.startCommunication(7);
		try {
			// ce.sendMessage(usr);
			ce.sendMessage(event1);
		} finally {
			ce.stopCommunication();
		}
	}

	@Test
	public void junit_getLegacyPendingCommands() throws Exception {
		String deviceName = "EV100167";
		ClientEmulator ce = new ClientEmulator(host, layer3Port, deviceName, keys.get(host).get(deviceName));
		ce.startCommunication(4);
		boolean done = false;
		while(!done) {
			MessageData_92 data92 = new MessageData_92();
			MessageData reply = ce.sendMessage(data92);
			switch(reply.getMessageType()) {
				case TERMINAL_UPDATE_STATUS:
					if(((MessageData_90) reply).getTerminalUpdateStatus() == 0) {
						done = true;
					} else {
						MessageData reply2 = ce.receiveReply();
						switch(reply2.getMessageType()) {
							case GX_PEEK:
								MessageData_87 data87 = (MessageData_87) reply2;
								if(data87.getMemoryCode() == 68 && data87.getStartingAddress() == Long.parseLong("00802C00", 16)) { // 8399872
									// modem info
									byte[] modemInfo = ByteArrayUtils
											.fromHex("3C4D4F44454D3E0D4350494E3A52454144590D43474D493A54454C49540D4347534E3A3335373436323033303032363037350D43474D523A30372E30332E3230300D43474D4D3A47433836342D515541440D424E443A330D435245473A302C310D43475245473A302C310D43474154543A310D49503A3137322E32342E3138362E3133340D475052533A310D4353513A31322C300D2020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020CA");
									ce.testGxFileTransfer("Modem Info", modemInfo, FileType.GX_PEEK_RESPONSE);
									break;
								} else if(data87.getMemoryCode() == 66 && data87.getStartingAddress() == 0) {
									// config file
									ce.testGxFileTransfer(new File("C:\\Users\\bkrug\\Documents\\EV035194-CFG-N.hex"), FileType.CONFIGURATION_FILE);
									break;
								} else if(data87.getMemoryCode() == 68 && data87.getStartingAddress() == Long.parseLong("00807E90", 16)) {
									ce.testGxFileTransfer("Bootloader/Diagnostic Versions", "BL 1.0.0\nDIAG 3.2.1\n".getBytes(ProcessingConstants.US_ASCII_CHARSET), FileType.GX_PEEK_RESPONSE);
									break;
								} else if(data87.getMemoryCode() == 68 && data87.getStartingAddress() == Long.parseLong("00807EB2", 16)) {
									ce.testGxFileTransfer("Bezel Info", ("SN:7.06.005\rBT:" + GxBezelType.MEI_GEN_1.getValue() + "\rMN:Acme,Inc\rAV:4.44Y\r").getBytes(ProcessingConstants.US_ASCII_CHARSET), FileType.GX_PEEK_RESPONSE);
									break;
								} else if(data87.getMemoryCode() == 68 && data87.getStartingAddress() == Long.parseLong("00007F20", 16)) {
									ce.testGxFileTransfer("Firmware Version", "WonderWare-4.9.7".getBytes(ProcessingConstants.US_ASCII_CHARSET), FileType.GX_PEEK_RESPONSE);
									break;
								}
							default:
								MessageData_2F data2F = new MessageData_2F();
								data2F.setAckedMessageNumber(reply2.getMessageNumber());
								ce.transmitMessage(data2F); // TODO: send only - no receive
						}
					}
					break;
				default:
					done = true;
			}
		}
		ce.stopCommunication();
	}
	@Test
	public void junit_droppedDataTest() throws Exception {
		String deviceName = "EV083765";
		ClientEmulator ce = new ClientEmulator(host, layer3Port, deviceName, keys.get(host).get(deviceName));
		MessageData_92 data92 = new MessageData_92();
		int n = 100;
		for(int i = 0; i < n; i++) {
			ce.startCommunication(4);
			Thread.sleep(1200);
			ce.sendHangup(false);
			if(ce.sendMessage(data92) == null)
				log.info("Test " + i + " FAILED");
			else
				log.info("Test " + i + " succeeded");
			ce.stopCommunication();
		}
	}

	@Test
	public void junit_hangupTest() throws Exception {
		String deviceName = "EV083765";
		ClientEmulator ce = new ClientEmulator(host, layer3Port, deviceName, keys.get(host).get(deviceName));
		int n = 5;
		for(int i = 0; i < n; i++) {
			ce.startCommunication(4);
			Thread.sleep(1200);
			ce.sendHangup(true);
			Thread.sleep(500);
			ce.stopCommunication();
		}
	}

	@Test
	public void junit_sendLegacyPeek() throws Exception {
		String deviceName = "EV033602";
		ClientEmulator ce = new ClientEmulator(host, layer3Port, deviceName, keys.get(host).get(deviceName));
		ce.startCommunication(4);
		boolean done = false;
		while(!done) {
			MessageData_92 data92 = new MessageData_92();
			MessageData reply = ce.sendMessage(data92);
			switch(reply.getMessageType()) {
				case TERMINAL_UPDATE_STATUS:
					if(((MessageData_90) reply).getTerminalUpdateStatus() == 0) {
						done = true;
					} else {
						MessageData reply2 = ce.receiveReply();
						/*
						MessageData_2F data2F = new MessageData_2F();
						data2F.setAckedMessageNumber(reply2.getMessageNumber());
						ce.transmitMessage(data2F); // send only - no receive
						*/
						String fileName = "FAKE-PEEK";
						//byte[] content = "This is the content that we are sending. Not very big but just to test.".getBytes();
						byte[] content = "USA-Gx-FAKE-VERSION-1".getBytes();
						int packetSize = 200;
						MessageData_A4 dataA4 = new MessageData_A4();
						dataA4.setFileName(fileName);
						dataA4.setFileType(FileType.GX_PEEK_RESPONSE);
						dataA4.setTotalBytes(content.length);
						dataA4.setTotalPackets((int)Math.ceil(content.length / (double)packetSize));						
						MessageData replyStart = ce.sendMessage(dataA4); 
						if(replyStart.getMessageType() == MessageType.FILE_XFER_START_ACK_3_0) {
							if(content.length <= packetSize) {
								MessageData_A6 dataA6 = new MessageData_A6();
								dataA6.setContent(content);
								dataA6.setPacketNum(0);
								ce.sendMessage(dataA6); 
							} else {
								byte[] packet = new byte[packetSize];
								int packetNum = 0;
								for(int pos = 0; pos < content.length; pos += packetSize) {
									if(packetSize > content.length - pos)
										packet = new byte[content.length - pos];
									System.arraycopy(content, pos, packet, 0, packet.length);
									MessageData_A6 dataA6 = new MessageData_A6();
									dataA6.setContent(packet);
									dataA6.setPacketNum(packetNum++);
									MessageData replyXfr = ce.sendMessage(dataA6); 
									if(replyXfr.getMessageType() != MessageType.FILE_XFER_ACK_3_0) 
										break;									
								}
								
							}
						}
						
					}
					break;
				default:
					done = true;
			}
		}
		ce.stopCommunication();
	}
	@Test
	public void junit_sendQueueTimingCheck() throws Exception {
		String deviceName = "TD000021";
		sendQueueTimingCheck(host, authPort, deviceName, keys.get(host).get(deviceName), 1);
	}

	@Test
	public void junit_testTransmission() throws Exception {
		String host = "usanet31";
		int port = 443;
		//String hex = "01008F4C442D424C4E4352000007000013880519757361742E696E626F756E642E6D6573736167652E6175746823757361742E617574686F726974792E7265616C74696D652E696E736964652E746573740019757361742E696E626F756E642E6D6573736167652E6175746824757361742E617574686F726974792E7265616C74696D652E6F7574736964652E7465737443";
		String hex = "0100AE4C442D424C4E435200000700001388061E757361742E6B65796D616E616765722E646563727970742E6D616774656B19757361742E696E626F756E642E6D6573736167652E6175746823757361742E617574686F726974792E7265616C74696D652E696E736964652E746573740019757361742E696E626F756E642E6D6573736167652E6175746824757361742E617574686F726974792E7265616C74696D652E6F7574736964652E7465737441";
		Socket socket = new Socket(host, port);
		try {
			System.out.println("Connected to " + host + ':' + port);
			socket.setSoTimeout(15000);
			java.io.OutputStream out = new BufferedOutputStream(socket.getOutputStream());
			out.write(ByteArrayUtils.fromHex(hex));
			out.flush();
			System.out.println("Sent bytes: " + hex);
			InputStream in = socket.getInputStream();
			System.out.print("Receiving bytes: ");
			while(true) {
				int available = in.available();
				if(available > 1) {
					byte[] bytes = new byte[available];
					in.read(bytes);
					System.out.print(StringUtils.toHex(bytes));
				} else {
					int b = in.read();
					if(b < 0)
						break;
					System.out.print(StringUtils.toHex((byte) b));
				}
			}
		} finally {
			System.out.println();
			socket.close();
		}
		System.out.println("Closed connection");
	}
	
	@Test
	public void junit_printQueueTimingCheckBytes() throws Exception {
		final String deviceName = "LD-BLNCR";
		final String[] queues = { 
				"usat.keymanager.decrypt.magtek",
				"usat.inbound.message.auth", 
				"usat.authority.realtime.inside.test", 
				null, 
				"usat.inbound.message.auth", 
				"usat.authority.realtime.outside.test", 
		};
		log.info("Creating queue timing check");
		MessageData_0007 data = new MessageData_0007();
		for(String q : queues) {
			data.setTimeout(5000);
			data.addQueue().setQueueName(q);
		}
		data.setDirection(MessageDirection.CLIENT_TO_SERVER);
		data.setMessageId(0);
		data.setMessageNumber((byte) 0);
		ByteBuffer buffer = ByteBuffer.allocate(1024);
		buffer.put((byte) 1); // protocol
		buffer.position(3);
		byte[] deviceNameBytes = deviceName.getBytes(ProcessingConstants.US_ASCII_CHARSET);
		for(int i = 0; i < 8 - deviceNameBytes.length; i++) {
			buffer.put((byte) 0);// this will pad with 0x00 if length < 8
		}
		buffer.put(deviceNameBytes, 0, 8);
		data.writeData(buffer, false);
		int length = buffer.position() - 2;
		buffer.put(1, (byte) (length >> 8)); // first byte of length
		buffer.put(2, (byte) (length >> 0)); // second byte of length

		int sum = 0;
		for(int i = 3; i < buffer.position(); i++) {
			sum += buffer.get(i);
		}
		buffer.put((byte) sum);
		buffer.flip();
		String hex = StringUtils.toHex(buffer, 0, buffer.limit());
		log.info("Message Transmission: " + hex);
		final char[] HEX_DIGITS = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
		char[] result = new char[buffer.limit() * 4];
		for(int i = 0; i < buffer.limit(); i++) {
			result[i * 4 + 0] = '&';
			result[i * 4 + 1] = 'H';
			result[i * 4 + 2] = HEX_DIGITS[(buffer.get(i) >> 4) & 0x0f];
			result[i * 4 + 3] = HEX_DIGITS[buffer.get(i) & 0x0f];
		}
		log.info("IPSentry Transmission: " + new String(result));
	}

	@Test
	public void junit_continualProdQueueTimingCheck() throws Exception {
		// usat.inbound.message.auth,usat.authority.realtime.inside.test,,usat.inbound.message.auth,usat.authority.realtime.outside.test
		final String deviceName = "LD-BLNCR";
		final String[] authorities = {
				"aramark",
				"authority_iso8583_elavon",
				"authority_iso8583_fhms_paymentech",
				"blackboard",
				"internal",
				"posgateway_aquafill",
		};
		
		while(true) {
			for(int i = 0; i < 4; i++) {
				String host = "usanet2" + (i+1);
				log.info("Sending queue timing check to " + host);
				try {
					ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, null);
					for(String a : authorities) {
						MessageData_0007 data = new MessageData_0007();
						data.setTimeout(20000);
						data.addQueue().setQueueName("usat.inbound.message.auth");
						data.addQueue().setQueueName("usat.authority.realtime." + a);
						
						log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
						ce.startCommunication(1);
						try {
							ce.sendMessage(data);
						} finally {
							ce.stopCommunication();
						}
					}
				} catch(Exception e) {
					log.warn("Error while hitting " + host, e);
				}
			}
			try {
				Thread.sleep(1000*60*2);
			} catch(InterruptedException e) {
			}
		}
	}
	
	@Test
	public void junit_sendIPSentryCheck() throws Exception {
		final String deviceName = "LD-BLNCR";
		final String[] authorities = {
				"aramark",
				"authority_iso8583_elavon",
				"authority_iso8583_fhms_paymentech",
				"blackboard",
				"internal",
				"posgateway_aquafill",
		};
		MessageData_0007 data = new MessageData_0007();
		data.setTimeout(20000);
		
		for(int i = 0; i < authorities.length; i++) {
			if(i > 0)
				data.addQueue().setQueueName(null);
			data.addQueue().setQueueName("usat.inbound.message.auth");
			data.addQueue().setQueueName("usat.authority.realtime." + authorities[i]);
		}

		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, null);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.startCommunication(1);
		try {
			ce.sendMessage(data);
		} finally {
			ce.stopCommunication();
		}
	}
	@Test
	public void junit_sendLoadBalancerPing() throws Exception {
		String deviceName = "LD-BLNCR";
		sendPing(host, authPort, deviceName, keys.get(host).get(deviceName), 1);
		sendPing(host, salePort, deviceName, keys.get(host).get(deviceName), 1);
		sendPing(host, unifedlayerPort, deviceName, keys.get(host).get(deviceName), 1);
		sendPing(host, layer3Port, deviceName, keys.get(host).get(deviceName), 1);
	}
	@Test
	public void junit_continualLoadBalancerPinging() throws Exception {
		final String deviceName = "LD-BLNCR";
		final byte[] encryptionKey = keys.get(host).get(deviceName);
		while(true) {
			for(int i = 0; i < 1; i++) {
				sendPingSafely(host, authPort + i * 100, deviceName, encryptionKey, 1);
				sendPingSafely(host, salePort + i * 100, deviceName, encryptionKey, 1);
				sendPingSafely(host, i == 0 ? unifedlayerPort : 13700 + unifedlayerPort + i * 100, deviceName, encryptionKey, 1);
				sendPingSafely(host, layer3Port + i * 100, deviceName, encryptionKey, 1);
			}
			try {
				Thread.sleep(1000 * 2);
			} catch(InterruptedException e) {
			}
		}
	}
	protected void sendPingSafely(String host, int port, String deviceName, byte[] encryptionKey, int times) {
		try {
			sendPing(host, port, deviceName, encryptionKey, times);
		} catch(Exception e) {
			log.warn("Failed to ping " + host + ":" + port , e);
		}
	}
	
	@Test
	public void junit_elavonISO8583Certification() throws Exception {
		ClientEmulator ce1 = new ClientEmulator(host, authPort, "TD000593", keys.get(host).get("TD000593"));
		
		authAndSale(ce1, authPort, salePort, "4055011111111111=10121015432112345678", EntryType.SWIPE, new BigDecimal(100), new BigDecimal(200));
		authAndSale(ce1, authPort, salePort, "4055011111111111=10121015432112345678",  EntryType.SWIPE, new BigDecimal(100), new BigDecimal(70));
		authAndSale(ce1, authPort, salePort, "5454545454545454=10121015432112345601",  EntryType.SWIPE, new BigDecimal(100), new BigDecimal(0));
		authAndSale(ce1, authPort, salePort, "5454545454545454=10121015432112345601",  EntryType.SWIPE, new BigDecimal(100), new BigDecimal(150));
		authAndSale(ce1, authPort, salePort, "5454545454545454=10121015432112345601",  EntryType.SWIPE, new BigDecimal(100), new BigDecimal(200));
		authAndSale(ce1, authPort, salePort, "5454545454545454=10121015432112345601",  EntryType.SWIPE, new BigDecimal(100), new BigDecimal(135));
		authAndSale(ce1, authPort, salePort, "6011000995500000=10121015432112345678",  EntryType.SWIPE, new BigDecimal(100), new BigDecimal(250));
		authAndSale(ce1, authPort, salePort, "4055011111111111=10121015432112345678",  EntryType.CONTACTLESS, new BigDecimal(100), new BigDecimal(145));
		authAndSale(ce1, authPort, salePort, "371449635398431=10121015432112345678",  EntryType.SWIPE, new BigDecimal(100), new BigDecimal(0));
		authAndSale(ce1, authPort, salePort, "371449635398431=10121015432112345678",  EntryType.SWIPE, new BigDecimal(100), new BigDecimal(75));
		authAndSale(ce1, authPort, salePort, "4055011111111111=10121015432112345678",  EntryType.SWIPE, new BigDecimal(100), new BigDecimal(140));
		authAndSale(ce1, authPort, salePort, "4055011111111111=10121015432112345678",  EntryType.SWIPE, new BigDecimal(100), new BigDecimal(85));
		authAndSale(ce1, authPort, salePort, "4055011111111111=10121015432112345678",  EntryType.SWIPE, new BigDecimal(100), new BigDecimal(170));
		authAndSale(ce1, authPort, salePort, "6011000995500000=10121015432112345678",  EntryType.CONTACTLESS, new BigDecimal(100), new BigDecimal(220));
		authAndSale(ce1, authPort, salePort, "4055011111111111=10121015432112345678",  EntryType.SWIPE, new BigDecimal(100), new BigDecimal(0));		
		authAndSale(ce1, authPort, salePort, "5454545454545454=10121015432112345601",  EntryType.CONTACTLESS, new BigDecimal(100), new BigDecimal(350));
		authAndSale(ce1, authPort, salePort, "4055011111111111=10121015432112345678",  EntryType.CONTACTLESS, new BigDecimal(100), new BigDecimal(71));
		authAndSale(ce1, authPort, salePort, "6011000995500000=10121015432112345678",  EntryType.SWIPE, new BigDecimal(100), new BigDecimal(285));
		authAndSale(ce1, authPort, salePort, "371449635398431=10121015432112345678",  EntryType.SWIPE, new BigDecimal(100), new BigDecimal(393));

		//Multi-Vend
		ClientEmulator ce2 = new ClientEmulator(host, authPort, "TD000594", keys.get(host).get("TD000594"));
		authAndSale(ce2, authPort, salePort, "5454545454545454=10121015432112345601",  EntryType.SWIPE, new BigDecimal(1500), new BigDecimal(800));
		authAndSale(ce2, authPort, salePort, "5454545454545454=10121015432112345601",  EntryType.SWIPE, new BigDecimal(1500), new BigDecimal(750));
		authAndSale(ce2, authPort, salePort, "6011000995500000=10121015432112345678",  EntryType.SWIPE, new BigDecimal(1200), new BigDecimal(1000));
		authAndSale(ce2, authPort, salePort, "4055011111111111=10121015432112345678",  EntryType.CONTACTLESS, new BigDecimal(1200), new BigDecimal(500));
		authAndSale(ce2, authPort, salePort, "4055011111111111=10121015432112345678",  EntryType.SWIPE, new BigDecimal(1500), new BigDecimal(488));
		authAndSale(ce2, authPort, salePort, "5454545454545454=10121015432112345601",  EntryType.SWIPE, new BigDecimal(1500), new BigDecimal(0));
		authAndSale(ce2, authPort, salePort, "5454545454545454=10121015432112345601",  EntryType.SWIPE, new BigDecimal(1500), new BigDecimal(526));
		authAndSale(ce2, authPort, salePort, "371449635398431=10121015432112345678",   EntryType.SWIPE, new BigDecimal(1200), new BigDecimal(0));
		authAndSale(ce2, authPort, salePort, "4055011111111111=10121015432112345678",  EntryType.SWIPE, new BigDecimal(1200), new BigDecimal(585));
		authAndSale(ce2, authPort, salePort, "4055011111111111=10121015432112345678",  EntryType.CONTACTLESS, new BigDecimal(1200), new BigDecimal(340));
		authAndSale(ce2, authPort, salePort, "6011000995500000=10121015432112345678",  EntryType.SWIPE, new BigDecimal(1200), new BigDecimal(525));
		authAndSale(ce2, authPort, salePort, "4055011111111111=10121015432112345678",  EntryType.SWIPE, new BigDecimal(1500), new BigDecimal(0));
		authAndSale(ce2, authPort, salePort, "371449635398431=10121015432112345678",  EntryType.CONTACTLESS, new BigDecimal(1200), new BigDecimal(300));
		authAndSale(ce2, authPort, salePort, "4055011111111111=10121015432112345678",  EntryType.SWIPE, new BigDecimal(1500), new BigDecimal(500));
		authAndSale(ce2, authPort, salePort, "5454545454545454=10121015432112345601",  EntryType.SWIPE, new BigDecimal(1500), new BigDecimal(635));

		//High Dollar
		ClientEmulator ce3 = new ClientEmulator(host, authPort, "TD000595", keys.get(host).get("TD000595"));
		authAndSale(ce3, authPort, salePort, "4055011111111111=10121015432112345678",  EntryType.SWIPE, new BigDecimal(1500), new BigDecimal(1500));
		authAndSale(ce3, authPort, salePort, "4055011111111111=10121015432112345678",  EntryType.SWIPE, new BigDecimal(13500), new BigDecimal(13500));
		authAndSale(ce3, authPort, salePort, "5454545454545454=10121015432112345601",  EntryType.SWIPE, new BigDecimal(25000), new BigDecimal(25000));
		authAndSale(ce3, authPort, salePort, "5454545454545454=10121015432112345601",  EntryType.SWIPE, new BigDecimal(11000), new BigDecimal(0));
		authAndSale(ce3, authPort, salePort, "5454545454545454=10121015432112345601",  EntryType.SWIPE, new BigDecimal(6500), new BigDecimal(0));
		authAndSale(ce3, authPort, salePort, "6011000995500000=10121015432112345678",  EntryType.CONTACTLESS, new BigDecimal(21000), new BigDecimal(21000));
		authAndSale(ce3, authPort, salePort, "6011000995500000=10121015432112345678",  EntryType.SWIPE, new BigDecimal(9900), new BigDecimal(9900));
		authAndSale(ce3, authPort, salePort, "4055011111111111=10121015432112345678",  EntryType.SWIPE, new BigDecimal(7800), new BigDecimal(0));
		authAndSale(ce3, authPort, salePort, "371449635398431=10121015432112345678",  EntryType.SWIPE, new BigDecimal(2500), new BigDecimal(2500));
		authAndSale(ce3, authPort, salePort, "5454545454545454=10121015432112345601",  EntryType.SWIPE, new BigDecimal(4500), new BigDecimal(4500));
		authAndSale(ce3, authPort, salePort, "4055011111111111=10121015432112345678",  EntryType.CONTACTLESS, new BigDecimal(1500), new BigDecimal(1500));
		authAndSale(ce3, authPort, salePort, "5454545454545454=10121015432112345601",  EntryType.CONTACTLESS, new BigDecimal(5000), new BigDecimal(5000));
		authAndSale(ce3, authPort, salePort, "6011000995500000=10121015432112345678",  EntryType.SWIPE, new BigDecimal(9900), new BigDecimal(9900));
		authAndSale(ce3, authPort, salePort, "4055011111111111=10121015432112345678",  EntryType.SWIPE, new BigDecimal(22500), new BigDecimal(22500));
	}
	@Test
	public void junit_elavonISO8583Certification2() throws Exception {
		//Snack
		ClientEmulator ce1 = new ClientEmulator(host, authPort, "TD000593", keys.get(host).get("TD000593"));		
		authAndSale(ce1, authPort, salePort, "4055011111111111=10121015432112345678", EntryType.SWIPE, new BigDecimal(100), new BigDecimal(200)); // Test 1 
		authAndSale(ce1, authPort, salePort, "5454545454545454=10121015432112345601", EntryType.CONTACTLESS, new BigDecimal(100), new BigDecimal(70)); // Test 2
		authAndSale(ce1, authPort, salePort, "4055011111111111=10121015432112345678", EntryType.SWIPE, new BigDecimal(100), new BigDecimal(0)); // Test 3
		authAndSale(ce1, authPort, salePort, "5454545454545454=10121015432112345601", EntryType.SWIPE, new BigDecimal(100), new BigDecimal(150)); // Test 5
		authAndSale(ce1, authPort, salePort, "5454545454545454=10121015432112345601", EntryType.SWIPE, new BigDecimal(100), new BigDecimal(200)); // Test 6
		authAndSale(ce1, authPort, salePort, "5454545454545454=10121015432112345601", EntryType.SWIPE, new BigDecimal(100), new BigDecimal(135)); // Test 7
		authAndSale(ce1, authPort, salePort, "4055011111111111=10121015432112345678", EntryType.CONTACTLESS, new BigDecimal(100), new BigDecimal(145)); // Test 10
		authAndSale(ce1, authPort, salePort, "5454545454545454=10121015432112345601", EntryType.SWIPE, new BigDecimal(100), new BigDecimal(0)); // Test 11
		authAndSale(ce1, authPort, salePort, "371449635398431=10121015432112345678", EntryType.SWIPE, new BigDecimal(100), new BigDecimal(50)); // Test 13
		authAndSale(ce1, authPort, salePort, "6011000995500000=10121015432112345678", EntryType.SWIPE, new BigDecimal(100), new BigDecimal(79)); // Test 14
		
		//Multi-Vend
		ClientEmulator ce2 = new ClientEmulator(host, authPort, "TD000594", keys.get(host).get("TD000594"));
		authAndSale(ce2, authPort, salePort, "5454545454545454=10121015432112345601", EntryType.SWIPE, new BigDecimal(1500), new BigDecimal(800)); // Test 1 
		authAndSale(ce2, authPort, salePort, "6011000995500000=10121015432112345678", EntryType.SWIPE, new BigDecimal(1200), new BigDecimal(1000)); // Test 2
		authAndSale(ce2, authPort, salePort, "4055011111111111=10121015432112345678", EntryType.CONTACTLESS, new BigDecimal(1200), new BigDecimal(500)); // Test 3
		authAndSale(ce2, authPort, salePort, "5454545454545454=10121015432112345601", EntryType.SWIPE, new BigDecimal(1500), new BigDecimal(0)); // Test 4
		authAndSale(ce2, authPort, salePort, "371449635398431=10121015432112345678", EntryType.SWIPE, new BigDecimal(1500), new BigDecimal(526)); // Test 7
		authAndSale(ce2, authPort, salePort, "4055011111111111=10121015432112345678", EntryType.SWIPE, new BigDecimal(1200), new BigDecimal(0)); // Test 8
		authAndSale(ce2, authPort, salePort, "5454545454545454=10121015432112345601", EntryType.CONTACTLESS, new BigDecimal(1200), new BigDecimal(340)); // Test 10
		authAndSale(ce2, authPort, salePort, "6011000995500000=10121015432112345678", EntryType.SWIPE, new BigDecimal(1200), new BigDecimal(525)); // Test 12
		authAndSale(ce2, authPort, salePort, "4055011111111111=10121015432112345678", EntryType.SWIPE, new BigDecimal(1200), new BigDecimal(600)); // Test 13
		authAndSale(ce2, authPort, salePort, "4055011111111111=10121015432112345678", EntryType.SWIPE, new BigDecimal(1500), new BigDecimal(920)); // Test 14

		//High Dollar
		ClientEmulator ce3 = new ClientEmulator(host, authPort, "TD000595", keys.get(host).get("TD000595"));
		authAndSale(ce3, authPort, salePort, "4055011111111111=10121015432112345678", EntryType.SWIPE, new BigDecimal(1500), new BigDecimal(1500)); // Test 1 
		authAndSale(ce3, authPort, salePort, "371449635398431=10121015432112345678", EntryType.SWIPE, new BigDecimal(13500), new BigDecimal(13500)); // Test 2
		authAndSale(ce3, authPort, salePort, "5454545454545454=10121015432112345601", EntryType.SWIPE, new BigDecimal(11000), new BigDecimal(0)); // Test 4
		authAndSale(ce3, authPort, salePort, "4055011111111111=10121015432112345678", EntryType.CONTACTLESS, new BigDecimal(21000), new BigDecimal(21000)); // Test 6
		authAndSale(ce3, authPort, salePort, "4055011111111111=10121015432112345678", EntryType.SWIPE, new BigDecimal(7800), new BigDecimal(0)); // Test 7
		authAndSale(ce3, authPort, salePort, "5454545454545454=10121015432112345601", EntryType.CONTACTLESS, new BigDecimal(10000), new BigDecimal(10000)); // Test 10
		authAndSale(ce3, authPort, salePort, "6011000995500000=10121015432112345678", EntryType.SWIPE, new BigDecimal(8800), new BigDecimal(8800)); // Test 11
		authAndSale(ce3, authPort, salePort, "4055011111111111=10121015432112345678", EntryType.SWIPE, new BigDecimal(24000), new BigDecimal(24000)); // Test 12
	}
	
	@Test
	public void junit_elavonISO8583Certification4_AmEx() throws Exception {
		//Snack
		ClientEmulator ce1 = new ClientEmulator(host, authPort, "TD000580", keys.get(host).get("TD000580"));				
		authAndSale(ce1, authPort, salePort, "340000000000009=13121543213961456789?", EntryType.SWIPE, new BigDecimal(100), new BigDecimal(50)); // Test 13
	}
	@Test
	public void junit_elavonISO8583Certification4() throws Exception {
		/*
		//Snack
		ClientEmulator ce1 = new ClientEmulator(host, authPort, "TD000580", keys.get(host).get("TD000580"));				
		authAndSale(ce1, authPort, salePort, "4121630071281885=13121543213961456789?", EntryType.SWIPE, new BigDecimal(100), new BigDecimal(200)); // Test 1 
		authAndSale(ce1, authPort, salePort, "5415240007992183=13121543213961456789?", EntryType.CONTACTLESS, new BigDecimal(100), new BigDecimal(70)); // Test 2
		authAndSale(ce1, authPort, salePort, "4485666666666668=13121543213961456789?", EntryType.SWIPE, new BigDecimal(100), new BigDecimal(0)); // Test 3
		authAndSale(ce1, authPort, salePort, "5147359999999991=13121543213961456789?", EntryType.SWIPE, new BigDecimal(100), new BigDecimal(150)); // Test 5
		authAndSale(ce1, authPort, salePort, "5147359999999991=13121543213961456789?", EntryType.SWIPE, new BigDecimal(100), new BigDecimal(200)); // Test 6
		authAndSale(ce1, authPort, salePort, "5147359999999991=13121543213961456789?", EntryType.SWIPE, new BigDecimal(100), new BigDecimal(135)); // Test 7
		authAndSale(ce1, authPort, salePort, "4121630071281885=13121543213961456789?", EntryType.CONTACTLESS, new BigDecimal(100), new BigDecimal(145)); // Test 10
		authAndSale(ce1, authPort, salePort, "5415240007992183=13121543213961456789?", EntryType.SWIPE, new BigDecimal(100), new BigDecimal(0)); // Test 11
		authAndSale(ce1, authPort, salePort, "340000000000009=13121543213961456789?", EntryType.SWIPE, new BigDecimal(100), new BigDecimal(50)); // Test 13
		authAndSale(ce1, authPort, salePort, "6011013333333331=13121543213961456789?", EntryType.SWIPE, new BigDecimal(100), new BigDecimal(79)); // Test 14
		
		//Multi-Vend
		ClientEmulator ce2 = new ClientEmulator(host, authPort, "TD000581", keys.get(host).get("TD000581"));
		authAndSale(ce2, authPort, salePort, "5415240007992183=13121543213961456789?", EntryType.SWIPE, new BigDecimal(1500), new BigDecimal(800)); // Test 1 
		authAndSale(ce2, authPort, salePort, "6011013333333331=13121543213961456789?", EntryType.SWIPE, new BigDecimal(1200), new BigDecimal(1000)); // Test 2
		authAndSale(ce2, authPort, salePort, "4121630071281885=13121543213961456789?", EntryType.CONTACTLESS, new BigDecimal(1200), new BigDecimal(500)); // Test 3
		authAndSale(ce2, authPort, salePort, "5415240007992183=13121543213961456789?", EntryType.SWIPE, new BigDecimal(1500), new BigDecimal(0)); // Test 4
		authAndSale(ce2, authPort, salePort, "340000000000009=13121543213961456789?", EntryType.SWIPE, new BigDecimal(1500), new BigDecimal(526)); // Test 7
		authAndSale(ce2, authPort, salePort, "4121630071281885=13121543213961456789?", EntryType.SWIPE, new BigDecimal(1200), new BigDecimal(0)); // Test 8
		authAndSale(ce2, authPort, salePort, "5147359999999991=13121543213961456789?", EntryType.CONTACTLESS, new BigDecimal(1200), new BigDecimal(340)); // Test 10
		authAndSale(ce2, authPort, salePort, "6011013333333331=13121543213961456789?", EntryType.SWIPE, new BigDecimal(1200), new BigDecimal(525)); // Test 12
		authAndSale(ce2, authPort, salePort, "4121630071281885=13121543213961456789?", EntryType.SWIPE, new BigDecimal(1200), new BigDecimal(600)); // Test 13
		authAndSale(ce2, authPort, salePort, "4121630071281885=13121543213961456789?", EntryType.SWIPE, new BigDecimal(1500), new BigDecimal(920)); // Test 14
		*/
		//High Dollar
		ClientEmulator ce3 = new ClientEmulator(host, authPort, "TD000582", keys.get(host).get("TD000582"));
		authAndSale(ce3, authPort, salePort, "4121630071281885=13121543213961456789?", EntryType.SWIPE, new BigDecimal(1500), new BigDecimal(1500)); // Test 1 
		authAndSale(ce3, authPort, salePort, "340000000000009=13121543213961456789?", EntryType.SWIPE, new BigDecimal(13500), new BigDecimal(13500)); // Test 2
		authAndSale(ce3, authPort, salePort, "5147359999999991=13121543213961456789?", EntryType.SWIPE, new BigDecimal(11000), new BigDecimal(0)); // Test 4
		authAndSale(ce3, authPort, salePort, "4121630071281885=13121543213961456789?", EntryType.CONTACTLESS, new BigDecimal(21000), new BigDecimal(21000)); // Test 6
		authAndSale(ce3, authPort, salePort, "4121630071281885=13121543213961456789?", EntryType.SWIPE, new BigDecimal(7800), new BigDecimal(0)); // Test 7
		authAndSale(ce3, authPort, salePort, "5147359999999991=13121543213961456789?", EntryType.CONTACTLESS, new BigDecimal(10000), new BigDecimal(10000)); // Test 10
		authAndSale(ce3, authPort, salePort, "6011013333333331=13121543213961456789?", EntryType.SWIPE, new BigDecimal(8800), new BigDecimal(8800)); // Test 11
		authAndSale(ce3, authPort, salePort, "4121630071281885=13121543213961456789?", EntryType.SWIPE, new BigDecimal(24000), new BigDecimal(24000)); // Test 12
	}
	
	@Test
	public void junit_AuthAndSaleK3Test() throws Exception {
		String deviceSerialCd = "K3BKUOOMS000001";
		String card = "4121630071281885=13121543213961456789?";
		long amount = 200;
		long saleAmount = amount;
		EntryType entryType = EntryType.MANUAL;
		String[] manualExtras = new String[] {
				"111||19380|11 Main St",
				"222||19335|22 South St",
				"333||17102|33 West St",
				"444||80908|",				
		};
		String invoiceNumber = null;
		
		Pattern trackRegex = Pattern.compile("^;?([0-9]{15,19})=([0-9]{4})([0-9]{3})([0-9]*)(?:\\?([\\x00-\\xFF])?)?$");
		long tranId = System.currentTimeMillis() / 1000;
		int num = 1;
		String[] tracks;
		switch(entryType) {
			case MANUAL:
				// use variety of postal, cvv, address
				Matcher matcher = trackRegex.matcher(card);
				if(!matcher.matches())
					throw new IllegalArgumentException("Invalid track data '" + card + "'");

				String cardNumber = matcher.group(1);
				String expDate = matcher.group(2);
				// <card number>|<card expiration date>|<card security code>|<card holder name>|<card holder zip code>|<card holder address>
				tracks = new String[manualExtras.length];
				StringBuilder sb = new StringBuilder();
				for(int k = 0; k < manualExtras.length; k++) {
					sb.setLength(0);
					sb.append(cardNumber).append('|').append(expDate).append('|');
					if(cardNumber.startsWith("3"))
						sb.append("0");
					sb.append(manualExtras[k]);
					tracks[k] = sb.toString();
				}
				break;
			default:
				tracks = new String[] { card };
		}
		for(String track : tracks) {
			// do authsale
			hessianAuthAndSale(deviceSerialCd, tranId++, track, entryType, amount, saleAmount, "Test #" + num++, invoiceNumber, null);
		}
	}

	@Test
	public void junit_ElavonCertification2014Script() throws Exception {
		/* Must test a mix of:
		 * 1. Cards (MC, VI, DISC, AMEX)
		 * 2. Amounts (with various responses)
		 * 3. Entry Method (MANUAL, SWIPE, PROXIMITY)
		 * 4. Invoice Numbers or not (for MOTO, Ecommerce and Mobile)
		 * 5. Different zips/addresses/cvv (For MANUAL entry method)
		 * ---- Different Devices needed for factors below this line
		 * 6. POS Env (Unattended, Attended, MOTO, Ecommerce, Mobile)
		 * 7. Entry Capabilities (S, SP, SPE, M, MS, MSP, MSPE, SX, SPX, SPEX, MX, MSX, MSPX, MSPEX)
		 * 
		 * U - S, SP, MS, MSP
		 * A - S, SP, MS, MSP, SX, SPX, MX, MSX, MSPX
		 * M - M
		 * E - M
		 * C - MS, MSX
		 */
		int invoiceNumber = 37123991;
		Pattern trackRegex = Pattern.compile("^;?([0-9]{15,19})=([0-9]{4})([0-9]{3})([0-9]*)(?:\\?([\\x00-\\xFF])?)?$");
		Pattern cardRegex = Pattern.compile("[0-9]{15,19}");
		Pattern expDateRegex = Pattern.compile("([01]?\\d)/(\\d{2})");
		long tranId = System.currentTimeMillis() / 1000;
		AtomicInteger num = new AtomicInteger(1);
		String[] manualExtras = new String[] { "111||19380|11 Main St", "222||19335|22 South St", "333||17102|33 West St", "444||80908|", };// <card security code>|<card holder name>|<card holder zip code>|<card holder address>

		String file = "C:\\Users\\bkrug\\Documents\\Elavon Update 2014\\USAT IC Review Script 101714 Sales.csv";
		InputStream in = new FileInputStream(file);
		RowValuesIterator rvIter;
		try {
			rvIter = SheetUtils.getExcelRowValuesIterator(in);
		while(rvIter.hasNext()) {
			Map<String, Object> values = rvIter.next();
				String card = ConvertUtils.getString(values.get("ACCOUNT NUMBER"), false);
				BigDecimal authAmountMajor = ConvertUtils.convertRequired(BigDecimal.class, values.get("AUTH AMOUNT"));
				BigDecimal saleAmountMajor = ConvertUtils.convert(BigDecimal.class, values.get("SETTLEMENT AMOUNT"));
				Number testNum = ConvertUtils.convertSafely(Number.class, values.get("TEST #"), num);
			String entryMode = ConvertUtils.getString(values.get("POS ENTRY MODE"), true);
			String posType = ConvertUtils.getString(values.get("POS SYSTEM TYPE"), true);
			String posCap = ConvertUtils.getString(values.get("POS ENTRY CAPABILITY"), true);
				if(StringUtils.isBlank(card)) {
					String cardType = ConvertUtils.getString(values.get("CARD TYPE"), true);
					switch(cardType.toUpperCase()) {
						case "VISA":
							card = "4121630071281885=13121543213961456789?";
							break;
						case "M/C":
							card = "5147359999999991=13121543213961456789?";
							break;
						case "AMEX":
							card = "340000000000009=13121543213961456789?";
							break;
						case "DISC":
							card = "6011013333333331=13121543213961456789?";
							break;
						case "VISA-SHORT":
							card = "4055011111111111=1012101543211?";
							break;
						default:
							throw new Exception("Invalid card type '" + cardType + "'");
					}
				} else
					card = card.trim();

			EntryType entryType;
			switch(entryMode.toUpperCase()) {
					case "MAG STRIPE READ":
					case "MSR":
					entryType = EntryType.SWIPE;
					break;
					case "PROXIMITY READ":
					case "TAP":
					entryType = EntryType.CONTACTLESS;
					break;
					case "KEY ENTERED":
					case "KEYED CP":
					case "CNP":
					case "ECOMM":
					entryType = EntryType.MANUAL;
					break;
				default:
					throw new Exception("Invalid pos entry mode '" + entryMode + "'");
			}
			PosEnvironment posEnv;
			switch(posType.toUpperCase()) {
				case "UNATTENDED":
					posEnv = PosEnvironment.UNATTENDED;
					break;
				case "ATTENDED":
					posEnv = PosEnvironment.ATTENDED;
					break;
					case "MOBILE":
					case "ATTENDED - MOBILE":
					posEnv = PosEnvironment.MOBILE;
					break;
					case "MAIL ORDER / TELEPHONE ORDER":
					case "MOTO":
					posEnv = PosEnvironment.MOTO;
					break;
				case "ECOMMERCE":
					posEnv = PosEnvironment.ECOMMERCE;
					break;
				default:
					throw new Exception("Invalid pos system type '" + posType + "'");
			}
			String entryCaps;
			switch(posCap.toUpperCase()) {
				case "MAGNETICALLY SWIPE CAPABILITY":
					entryCaps = "OOMS";
					break;
				case "PROXIMITY READ CAPABILITY":
					entryCaps = "OMSP";
					break;
				case "MANUAL ENTRY ONLY":
					entryCaps = "OOOM";
					break;
				default:
					throw new Exception("Invalid pos capability '" + posCap + "'");
			}
				long authAmount = authAmountMajor.movePointRight(2).longValue();
			StringBuilder sb = new StringBuilder().append("K3BK").append(posEnv.getValue());
			StringUtils.appendPadded(sb, entryCaps, 'O', 4, Justification.RIGHT);
			sb.append("000001");
			String deviceSerialCd = sb.toString();
			String[] tracks;
			switch(entryType) {
				case MANUAL:
					// use variety of postal, cvv, address
					Matcher matcher = trackRegex.matcher(card);
						if(!matcher.matches()) {
							if(cardRegex.matcher(card).matches()) {
								String cvv = ConvertUtils.getString(values.get("CVV2/CVC2"), true);
								String expDate = ConvertUtils.getString(values.get("EXPIRY DATE"), false);
								tracks = new String[1];
								sb.setLength(0);
								sb.append(card).append('|');
								if(!StringUtils.isBlank(expDate)) {
									Matcher expDateMatcher = expDateRegex.matcher(expDate);
									if(!expDateMatcher.matches())
										throw new IllegalArgumentException("Invalid expiry date '" + expDate + "'");

									sb.append(expDateMatcher.group(2));
									if(expDateMatcher.group(1).length() == 1)
										sb.append('0');
									sb.append(expDateMatcher.group(1));
								}
								sb.append('|').append(cvv).append("||19355|");
								tracks[0] = sb.toString();
							} else
								throw new IllegalArgumentException("Invalid track data '" + card + "'");

						} else {
							String cardNumber = matcher.group(1);
							String expDate = matcher.group(2);
							// <card number>|<card expiration date>|<card security code>|<card holder name>|<card holder zip code>|<card holder address>
							tracks = new String[1];
							for(int k = 0; k < tracks.length; k++) {
								sb.setLength(0);
								sb.append(cardNumber).append('|').append(expDate).append('|');
								if(cardNumber.startsWith("3"))
									sb.append("0");
								sb.append(manualExtras[(k + num.get() - 1) % manualExtras.length]);
								tracks[k] = sb.toString();
							}
					}
					break;
				default:
					tracks = new String[] { card };
			}
			long saleAmount;
				if(saleAmountMajor != null)
					saleAmount = saleAmountMajor.movePointRight(2).longValue();
				else
					switch(posEnv) {
						case UNATTENDED:
							if(authAmount <= 5000) {
								saleAmount = Math.max(1, authAmount / 2);
								break;
							}
						default:
							saleAmount = authAmount;
					}
			for(String track : tracks) {
				// do authsale
					/*
						DecimalFormat amountFormat = new DecimalFormat("#,##0.00");
						log.info("Sending Auth for card " + track + " (" + entryType + ") on " + deviceSerialCd + " for $" + amountFormat.format(((double) authAmount) / 100));
						log.info("Sending Sale for card " + track + " (" + entryType + ") on " + deviceSerialCd + " for $" + amountFormat.format(((double) saleAmount) / 100));
					*/
					hessianAuthAndSale(deviceSerialCd, tranId++, track, entryType, authAmount, saleAmount, "Test #" + testNum, null, null);
					num.incrementAndGet();
				/*
				switch(posEnv) {
					case MOTO:
					case ECOMMERCE:
					case MOBILE:
						// do again with an invoice number
						hessianAuthAndSale(deviceSerialCd, tranId++, track, entryType, amount, saleAmount, "Test #" + num++, String.valueOf(invoiceNumber++), null);
						break;
				}*/
			}
		}
		} finally {
			in.close();
		}
	}

	@Test
	public void junit_TandemScript() throws Exception {
		/* Must test a mix of:
		 * 1. Cards (MC, VI, DISC, AMEX)
		 * 2. Amounts (with various responses)
		 * 3. Entry Method (MANUAL, SWIPE, PROXIMITY)
		 * 4. Invoice Numbers or not (for MOTO, Ecommerce and Mobile)
		 * 5. Different zips/addresses/cvv (For MANUAL entry method)
		 * ---- Different Devices needed for factors below this line
		 * 6. POS Env (Unattended, Attended, MOTO, Ecommerce, Mobile)
		 * 7. Entry Capabilities (S, SP, SPE, M, MS, MSP, MSPE, SX, SPX, SPEX, MX, MSX, MSPX, MSPEX)
		 * 
		 * U - S, SP, MS, MSP
		 * A - S, SP, MS, MSP, SX, SPX, MX, MSX, MSPX
		 * M - M
		 * E - M
		 * C - MS, MSX
		 */
		//String username = "bkrug";
		String username = "rgerwig";
		// String passcode = "41540199";
		int invoiceNumber = 37123991;
		long tranId = System.currentTimeMillis() / 1000;
		AtomicInteger num = new AtomicInteger(1);

		// String file = "D:\\Development\\Java Projects\\middleware\\ISO8583\\etc\\docs\\CPT PNS Cert Test.xlsx";
		//String file = "C:\\Users\\bkrug\\Documents\\Chase Paymentech\\CPT PNS Cert Tests.xlsx";
		String file = "D:\\Development\\workspace\\ISO8583\\etc\\docs\\CPT PNS Cert Tests.xlsx";
		
		InputStream in = new FileInputStream(file);
		RowValuesIterator rvIter;
		try {
			rvIter = SheetUtils.getExcelRowValuesIterator(in);
			while(rvIter.hasNext() && num.get() < 2) {
				Map<String, Object> values = rvIter.next();
				boolean skip = ConvertUtils.getBoolean(values.get("SKIP"), false);
				if(skip)
					continue;
				String track = ConvertUtils.getString(values.get("TRACK"), false);
				BigDecimal authAmountMajor = ConvertUtils.convertRequired(BigDecimal.class, values.get("AUTH AMOUNT"));
				BigDecimal saleAmountMajor = ConvertUtils.convert(BigDecimal.class, values.get("SALE AMOUNT"));
				Number testNum = ConvertUtils.convertSafely(Number.class, values.get("TEST #"), num.incrementAndGet());
				String entryTypeString = ConvertUtils.convertRequired(String.class, values.get("ENTRY TYPE"));
				EntryType entryType = ConvertUtils.convertRequired(EntryType.class, entryTypeString.trim().toUpperCase());
				PosEnvironment posEnv = ConvertUtils.convertRequired(PosEnvironment.class, values.get("POS TYPE"));
				String entryCaps = ConvertUtils.getString(values.get("ENTRY CAPABILITIES"), true);
				String attributes = ConvertUtils.getString(values.get("ATTRIBUTES"), false);
				if(!StringUtils.isBlank(attributes))
					attributes = RegexUtils.unescapeBackslash(attributes);
				StringBuilder sb = new StringBuilder(16);
				for(char ch : "MSPEX".toCharArray()) {
					if(entryCaps.indexOf(ch) >= 0)
						sb.append(ch);
				}
				entryCaps = sb.toString();
				long authAmount = authAmountMajor.movePointRight(2).setScale(0, BigDecimal.ROUND_HALF_UP).longValue();
				sb.setLength(0);
				sb.append("K3BK").append(posEnv.getValue());
				StringUtils.appendPadded(sb, entryCaps, 'O', 4, Justification.RIGHT);
				sb.append("000001");
				String deviceSerialCd = sb.toString();
				String password = null; // getECPassword(deviceSerialCd, username);
				if(saleAmountMajor != null)
					hessianAuthAndSale(null, username, password, deviceSerialCd, tranId++, track, entryType, authAmount, saleAmountMajor.movePointRight(2).setScale(0, BigDecimal.ROUND_HALF_UP).longValue(), "Test #" + testNum, posEnv == PosEnvironment.MOTO ? String.valueOf(invoiceNumber++) : null, attributes, false);
				else
					hessianAuth(null, username, password, deviceSerialCd, tranId++, track, entryType, authAmount, attributes);
			}
		} finally {
			in.close();
		}
	}

	protected final Properties passwordCache = new Properties();
	protected final File passwordCacheFile = new File(System.getProperty("user.dir", ".") + "/.testclient-secrets.properties");

	protected String getECPassword(String deviceSerialCd, String username) throws FileNotFoundException, IOException, SQLException, DataLayerException, NoSuchAlgorithmException, NoSuchProviderException, ConvertException, BeanException, ServiceException {
		if(passwordCache.isEmpty() && passwordCacheFile.length() > 0) {
			passwordCache.load(new FileReader(passwordCacheFile));
			setupDSF("ClientMain.properties");
		}
		String password = passwordCache.getProperty(deviceSerialCd);
		if(password != null)
			return password;
		String generatedPassword = SecurityUtils.getRandomPassword(20);
		byte[] generatedSalt = SecureHash.getSalt();
		byte[] generatedHash = SecureHash.getHash(generatedPassword, generatedSalt);
		Map<String, Object> params = new HashMap<>();
		params.put("deviceName", deviceSerialCd);
		DataLayerMgr.selectInto("GET_DEVICE_CREDENTIAL_INFO", params);
		password = passwordCache.getProperty(deviceSerialCd);
		if(password != null)
			return password;
		params.put("newUsername", username);
		params.put("newPasswordHash", generatedHash);
		params.put("newPasswordSalt", generatedSalt);
		// params.put("lastUpdateTime", lastUpdateTime);
		DataLayerMgr.executeUpdate("CHANGE_DEVICE_PASSWORD", params, true);
		if('Y' != ConvertUtils.getChar(params.get("updated"), 'N'))
			return getPassword(deviceSerialCd, username);
		passwordCache.setProperty(deviceSerialCd, generatedPassword);
		passwordCache.store(new FileWriter(passwordCacheFile), null);
		return generatedPassword;
	}
	@Test
	public void junit_AprivaTmpPasscode() throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		String deviceSerialCd = "K3TS1416329263307";
		String username = EportConnectSOAPTest.USERNAME;
		String password = "76579106";
		log.info("Sending process updates for " + deviceSerialCd);
		// EC2ProcessUpdatesResponse response = ec.processUpdates("bkrug", "41540199", deviceSerialCd, 0, 2, "BSK Test", "1.1A", "");
		EC2ProcessUpdatesResponse response = ec.processUpdates(username, password, deviceSerialCd, 0, 2, "Apriva Test", "1.1A", "");
		// EC2ProcessUpdatesResponse response = ec.processUpdates("testaccount1", "52927513", deviceSerialCd, 0, 2, "BSK Test", "1.1A", "");
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}
	@Test
	public void junit_AprivaTmpPasscode_ecc() throws Exception {
		EC2ServiceAPI ec = getEC2Service("ecc");
		String deviceSerialCd = "K3MTAE65120549001100";
		String username = "testaccount3";
		String password = "47357167";
		log.info("Sending process updates for " + deviceSerialCd);
		// EC2ProcessUpdatesResponse response = ec.processUpdates("bkrug", "41540199", deviceSerialCd, 0, 2, "BSK Test", "1.1A", "");
		EC2ProcessUpdatesResponse response = ec.processUpdates(username, password, deviceSerialCd, 0, 2, "Apriva Test", "1.1A", "");
		// EC2ProcessUpdatesResponse response = ec.processUpdates("testaccount1", "52927513", deviceSerialCd, 0, 2, "BSK Test", "1.1A", "");
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	@Test
	public void junit_AprivaCertification_2015() throws Exception {
		long tranId = System.currentTimeMillis() / 1000;
		String deviceSerialCd = "K3TS1416329263307";
		String username = EportConnectSOAPTest.USERNAME;
		String password = "GAhhXq!4huC^cUkrMU@J";
		String cardData = "9987771007141017"; // "9999901301=200112345";// 9999901501
		String encryptedCardDataHex = "D76EA16CC97B0AB1E683691655419FE1F955CC8DCFF832D3";
		String ksnHex = "62994996340010200003";
		int decryptedCardDataLen = cardData.length();
		// *
		aprivaAuthSale(tranId++, 100, 1, cardData, 302, username, password, deviceSerialCd);
		// aprivaAuthSale(tranId++, 400, 400, "9999901301", 302, username, password, deviceSerialCd);
		// aprivaChargeEncrypted(tranId++, 200, decryptedCardDataLen, encryptedCardDataHex, ksnHex, 303, username, password, deviceSerialCd);
		/*
		aprivaCharge(tranId++, 300, cardData, 304, username, password, deviceSerialCd);
		aprivaAuthSale(tranId++, 400, 400, cardData, 307, username, password, deviceSerialCd);
		aprivaAuthSale(tranId++, 500, 500, "8888888888", 307, username, password, deviceSerialCd);
		aprivaAuthSale(tranId++, 600, 600, "9999999999", 307, username, password, deviceSerialCd);
		aprivaAuthSale(tranId++, 700, 1000, cardData, 307, username, password, deviceSerialCd);
		aprivaAuthSale(tranId++, 800, 900, cardData, 310, username, password, deviceSerialCd);
		/* /
		for(int i = 1261; i < 1361; i++) {
			int rc = aprivaAuthSale(tranId++, i, 100, cardData, 101, username, password, deviceSerialCd);
			if(rc == ECResponse.RES_PARTIALLY_APPROVED || rc == ECResponse.RES_APPROVED)
				break;
		}*/
	}

	@Test
	public void junit_AprivaProdTest_ecc() throws Exception {
		long tranId = System.currentTimeMillis() / 1000;
		String deviceSerialCd = "K3MTAE65120549001100";
		String username = "testaccount3";
		String password = "2A2KUCbv!XDxaPeaAc86";
		String cardData = "9987771007141017"; // "9987771007141013";
		aprivaAuthSale("ecc", tranId++, 100, 3, cardData, 507, username, password, deviceSerialCd);
	}
	@Test
	public void junit_AprivaProdTest_usa() throws Exception {
		long tranId = System.currentTimeMillis() / 1000;
		String deviceSerialCd = "K3IDTT990000100";
		String username = "aroyce";
		String password = "82540605";
		String cardData = "9987771007141017"; // "9987771007141013";
		aprivaAuthSale("usa", tranId++, 100, 3, cardData, 507, username, password, deviceSerialCd);
	}

	@Test
	public void junit_AprivaBBCertification_2015() throws Exception {
		long tranId = System.currentTimeMillis() / 1000;
		String deviceSerialCd = "K3TS1416329263307";
		String username = EportConnectSOAPTest.USERNAME;
		String password = "52927513";
		String cardData = "9987771007141017"; // "9987771007141013";
		aprivaAuthSale(tranId++, 100, 9, cardData, 507, username, password, deviceSerialCd);
	}

	@Test
	public void junit_AprivaCBORDCertification_2015() throws Exception {
		long tranId = System.currentTimeMillis() / 1000;

		String cardData = "9999901501=200112345"; // "9999901101=200112345"; // "9999901501=200112345";// 9999901501 //"9999901501"
		String encryptedCardDataHex = "68914723920AF8C816E3C2D5DF4E6CB5D1063C20DE3BA2EB";
		String ksnHex = "62994996340010200003";
		int decryptedCardDataLen = cardData.length();
		String deviceName = "EV100139";
		host = "10.0.0.67";
		aprivaKioskAuthSale(deviceName, tranId++, 100, 2, cardData, 302);
		// aprivaKioskAuthSale(deviceName, tranId++, 400, 400, "9999901501", 302);
		// aprivaKioskAuthEncryptedSale(deviceName, tranId++, 200, decryptedCardDataLen, encryptedCardDataHex, ksnHex, 303);
		/*
		aprivaKioskAuthSale(deviceName, tranId++, 300, cardData, 304);
		aprivaKioskAuthSale(deviceName, tranId++, 400, 400, cardData, 307);
		aprivaKioskAuthSale(deviceName, tranId++, 500, 500, "8888888888", 307);
		aprivaKioskAuthSale(deviceName, tranId++, 600, 600, "9999999999", 307);
		aprivaKioskAuthSale(deviceName, tranId++, 700, 1000, cardData, 307);
		aprivaKioskAuthSale(deviceName, tranId++, 800, 900, cardData, 310);
		/* /
		for(int i = 1261; i < 1361; i++) {
			int rc = aprivaKioskAuthSale(deviceName, tranId++, i, 100, cardData, 101);
			if(rc == ECResponse.RES_PARTIALLY_APPROVED || rc == ECResponse.RES_APPROVED)
				break;
		}*/
	}

	protected void aprivaCharge(long tranId, long amount, String cardData, int n, String username, String password, String deviceSerialCd) throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		String entryType = "S";
		String tranResult = "S";
		// String tranDetails = "";
		// String tranDetails = "A0|200|90|2|Ping pongs|200|85|2|Yo-yos";
		String tranDetails = "A0|200|" + amount + "|1|Test Item #" + n;
		String attributes = null;
		EC2AuthResponse response = ec.chargePlain(username, password, deviceSerialCd, tranId, amount, cardData, entryType, tranResult, tranDetails, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	protected void aprivaChargeEncrypted(long tranId, long amount, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, int n, String username, String password, String deviceSerialCd) throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		String entryType = "S";
		String tranResult = "S";
		// String tranDetails = "";
		// String tranDetails = "A0|200|90|2|Ping pongs|200|85|2|Yo-yos";
		String tranDetails = "A0|200|" + amount + "|1|Test Item #" + n;
		String attributes = null;
		EC2AuthResponse response = ec.chargeEncrypted(username, password, deviceSerialCd, tranId, amount, CardReaderType.IDTECH_SECUREMAG.getValue(), decryptedCardDataLen, encryptedCardDataHex, ksnHex, entryType, tranResult, tranDetails, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
	}

	protected int aprivaAuthSale(long tranId, long authAmount, long saleAmount, String cardData, int n, String username, String password, String deviceSerialCd) throws Exception {
		return aprivaAuthSale(null, tranId, authAmount, saleAmount, cardData, n, username, password, deviceSerialCd);
	}

	protected int aprivaAuthSale(String env, long tranId, long authAmount, long saleAmount, String cardData, int n, String username, String password, String deviceSerialCd) throws Exception {
		return hessianAuthAndSale(env, username, password, deviceSerialCd, tranId, cardData, EntryType.SWIPE, authAmount, saleAmount, "Test Item #" + n, null, null, true);
	}

	protected int aprivaKioskAuthSale(String deviceName, long tranId, long authAmount, long saleAmount, String cardData, int n) throws Exception {
		ClientEmulator ce = new ClientEmulator(host, unifedlayerPort, deviceName, keys.get(host).get(deviceName));
		return kioskAuthAndSale(ce, tranId, unifedlayerPort, unifedlayerPort, EntryType.SWIPE, cardData, authAmount, saleAmount, null, "A0|200|" + saleAmount + "|1|Test Item #" + n);
	}

	@Test
	public void junit_ElavonCertification2014() throws Exception {
		/* Must test a mix of:
		 * 1. Cards (MC, VI, DISC, AMEX)
		 * 2. Amounts (with various responses)
		 * 3. Entry Method (MANUAL, SWIPE, PROXIMITY)
		 * 4. Invoice Numbers or not (for MOTO, Ecommerce and Mobile)
		 * 5. Different zips/addresses/cvv (For MANUAL entry method)
		 * ---- Different Devices needed for factors below this line
		 * 6. POS Env (Unattended, Attended, MOTO, Ecommerce, Mobile)
		 * 7. Entry Capabilities (S, SP, SPE, M, MS, MSP, MSPE, SX, SPX, SPEX, MX, MSX, MSPX, MSPEX)
		 * 
		 * U - S, SP, MS, MSP
		 * A - S, SP, MS, MSP, SX, SPX, MX, MSX, MSPX
		 * M - M
		 * E - M
		 * C - MS, MSX
		 */
		
		String[] cards = new String[] { "4121630071281885=13121543213961456789?", "5147359999999991=13121543213961456789?", "340000000000009=13121543213961456789?", "6011013333333331=13121543213961456789?", "4055011111111111=1012101543211?" };
		String[] manualExtras = new String[] {
				"111||19380|11 Main St",
				"222||19335|22 South St",
				"333||17102|33 West St",
				"444||80908|",				
		};// <card security code>|<card holder name>|<card holder zip code>|<card holder address>
		long[] amounts = new long[] {
				200,
				310,
				411,
				750,
			   8016,
			  90000,
				507,
				633,				
		};
		EntryType[] entryTypes = new EntryType[] { EntryType.SWIPE, EntryType.CONTACTLESS, EntryType.MANUAL };
		Map<PosEnvironment, String[]> posKinds = new LinkedHashMap<PosEnvironment, String[]>();
		posKinds.put(PosEnvironment.UNATTENDED, new String[] { "S", "SP", "MS", "MSP" });
		posKinds.put(PosEnvironment.ATTENDED, new String[] { "S", "SP", "MS", "MSP", "SX", "SPX", "MSX", "MSPX" });
		posKinds.put(PosEnvironment.MOTO, new String[] { "M" });
		posKinds.put(PosEnvironment.ECOMMERCE, new String[] { "M" });
		posKinds.put(PosEnvironment.MOBILE, new String[] { "MS", "MSX" });
		int invoiceNumber = 36123991;
		Pattern trackRegex = Pattern.compile("^;?([0-9]{15,19})=([0-9]{4})([0-9]{3})([0-9]*)(?:\\?([\\x00-\\xFF])?)?$");
		long tranId = System.currentTimeMillis() / 1000;
		int num = 1;
		for(String card : cards) {
			for(long amount : amounts) {
				for(EntryType entryType : entryTypes) {
					for(Map.Entry<PosEnvironment, String[]> posKind : posKinds.entrySet()) {
						for(String entryList : posKind.getValue()) {
							Set<EntryCapability> entryCapabilities = new LinkedHashSet<EntryCapability>(entryList.length(), 1.0f);
							for(int i = 0; i < entryList.length(); i++)
								entryCapabilities.add(EntryCapability.getByValue(entryList.charAt(i)));
							if(EntryCapability.isCapable(entryCapabilities, entryType)) {
								StringBuilder sb = new StringBuilder().append("K3BK").append(posKind.getKey().getValue());
								StringUtils.appendPadded(sb, entryList, 'O', 4, Justification.RIGHT);
								sb.append("000001");
								String deviceSerialCd = sb.toString();
								String[] tracks;
								switch(entryType) {
									case MANUAL:
										// use variety of postal, cvv, address
										Matcher matcher = trackRegex.matcher(card);
										if(!matcher.matches())
											throw new IllegalArgumentException("Invalid track data '" + card + "'");
												
										String cardNumber = matcher.group(1);
										String expDate = matcher.group(2);
										// <card number>|<card expiration date>|<card security code>|<card holder name>|<card holder zip code>|<card holder address>
										tracks = new String[manualExtras.length];
										for(int k = 0; k < manualExtras.length; k++) {
											sb.setLength(0);
											sb.append(cardNumber).append('|').append(expDate).append('|');
											if(cardNumber.startsWith("3"))
												sb.append("0");
											sb.append(manualExtras[k]);
											tracks[k] = sb.toString();
										}
										break;
									default:
										tracks = new String[] { card };
								}
								long saleAmount;
								switch(posKind.getKey()) {
									case UNATTENDED:
										saleAmount = Math.max(1, amount / 2);
										break;
									default:
										saleAmount = amount;
								}
								for(String track : tracks) {
									// do authsale
									hessianAuthAndSale(deviceSerialCd, tranId++, track, entryType, amount, saleAmount, "Test #" + num++, null, null);
									switch(posKind.getKey()) {
										case MOTO:
										case ECOMMERCE:
										case MOBILE:
											// do again with an invoice number
											hessianAuthAndSale(deviceSerialCd, tranId++, track, entryType, amount, saleAmount, "Test #" + num++, String.valueOf(invoiceNumber++), null);
											break;
									}
								}
							}
						}
					}
				}
			}
		}
	}

	protected int hessianAuthAndSale(String deviceSerialCd, long tranId, String track, EntryType entryType, long amount, long saleAmount, String itemDesc, String invoiceNumber, String attributes) throws Exception {
		return hessianAuthAndSale(EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, deviceSerialCd, tranId, track, entryType, amount, saleAmount, itemDesc, invoiceNumber, attributes, false);
	}

	protected int hessianAuthAndSale(String username, String password, String deviceSerialCd, long tranId, String track, EntryType entryType, long amount, long saleAmount, String itemDesc, String invoiceNumber, String attributes, boolean overridePartial) throws Exception {
		return hessianAuthAndSale(null, username, password, deviceSerialCd, tranId, track, entryType, amount, saleAmount, itemDesc, invoiceNumber, attributes, overridePartial);
	}

	protected int hessianAuthAndSale(String env, String username, String password, String deviceSerialCd, long tranId, String track, EntryType entryType, long amount, long saleAmount, String itemDesc, String invoiceNumber, String attributes, boolean overridePartial) throws Exception {
		EC2ServiceAPI ec = getEC2Service(env);
		DecimalFormat amountFormat = new DecimalFormat("#,##0.00");
		log.info("Sending Auth for card " + track + " (" + entryType + ") on " + deviceSerialCd + " for $" + amountFormat.format(((double) amount) / 100));
		EC2AuthResponse response = ec.authPlain(username, password, deviceSerialCd, tranId, amount, track, String.valueOf(entryType.getValue()), attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		switch(response.getReturnCode()) {
			case ECResponse.RES_PARTIALLY_APPROVED:
			case ECResponse.RES_APPROVED:
			case ECResponse.RES_AVS_MISMATCH:
			case ECResponse.RES_CVV_MISMATCH:
			case ECResponse.RES_CVV_AND_AVS_MISMATCH:
				if(!overridePartial && saleAmount > response.getApprovedAmount())
					saleAmount = response.getApprovedAmount();
				log.info(message);
				if(amount == 0)
					return response.getReturnCode();
				String tranResult;
				StringBuilder sb = new StringBuilder();
				if(saleAmount == 0 && StringUtils.isBlank(itemDesc)) {
					tranResult = "C";
				} else {
					tranResult = "S";
					sb.append("A0|200|").append(saleAmount).append("|1|");
					if(!StringUtils.isBlank(itemDesc))
						sb.append(itemDesc.replace('|', ' '));
					if(!StringUtils.isBlank(invoiceNumber))
						sb.append("|403||1|").append(invoiceNumber.replace('|', '/'));
				}
				String tranDetails = sb.toString();
				log.info("Sending Sale for card " + track + " (" + entryType + ") on " + deviceSerialCd + " for $" + amountFormat.format(((double) saleAmount) / 100));
				EC2Response batchResponse = ec.batch(username, password, deviceSerialCd, tranId, saleAmount, tranResult, tranDetails, attributes);
				if(batchResponse == null)
					throw new Exception("Received null response");
				message = getBasicResponseMessage(batchResponse);
				if(batchResponse.getReturnCode() == EportConnectSOAPTest.RES_OK)
					log.info(message);
				else
					log.warn("Sale Failed: " + message);
				return response.getReturnCode();
			default:
				log.warn("Auth Failed: " + message);
				return response.getReturnCode();
		}
	}

	protected int hessianSale(String env, String username, String password, String deviceSerialCd, long tranId, long saleAmount, String itemDesc, String invoiceNumber, String attributes) throws Exception {
		EC2ServiceAPI ec = getEC2Service(env);
		DecimalFormat amountFormat = new DecimalFormat("#,##0.00");
		log.info("Sending Sale for tran  " + tranId + " on " + deviceSerialCd + " for $" + amountFormat.format(((double) saleAmount) / 100));
		String tranResult = "S";
		StringBuilder sb = new StringBuilder();
		sb.append("A0|200|").append(saleAmount).append("|1|");
		if(!StringUtils.isBlank(itemDesc))
			sb.append(itemDesc.replace('|', ' '));
		if(!StringUtils.isBlank(invoiceNumber))
			sb.append("|403||1|").append(invoiceNumber.replace('|', '/'));
		String tranDetails = sb.toString();
		EC2Response batchResponse = ec.batch(username, password, deviceSerialCd, tranId, saleAmount, tranResult, tranDetails, attributes);
		if(batchResponse == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(batchResponse);
		if(batchResponse.getReturnCode() == EportConnectSOAPTest.RES_OK)
			log.info(message);
		else
			log.warn("Sale Failed: " + message);
		return batchResponse.getReturnCode();
	}

	protected int hessianAuth(String env, String username, String password, String deviceSerialCd, long tranId, String track, EntryType entryType, long amount, String attributes) throws Exception {
		// System.setProperty("jdk.tls.client.protocols", "SSLv3");
		// System.setProperty("jdk.tls.client.protocols", "TLSv1.1,TLSv1.2");
		// SSLContextImpl.DefaultSSLContext.getDefaultImpl()
		// SSLContext.getDefault().getDefaultSSLParameters().setProtocols(new String[] { "TLSv1.1" });
		EC2ServiceAPI ec = getEC2Service(env);
		DecimalFormat amountFormat = new DecimalFormat("#,##0.00");
		log.info("Sending Auth for card " + track + " (" + entryType + ") on " + deviceSerialCd + " for $" + amountFormat.format(((double) amount) / 100));
		EC2AuthResponse response = ec.authPlain(username, password, deviceSerialCd, tranId, amount, track, String.valueOf(entryType.getValue()), attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		switch(response.getReturnCode()) {
			case ECResponse.RES_PARTIALLY_APPROVED:
			case ECResponse.RES_APPROVED:
			case ECResponse.RES_AVS_MISMATCH:
			case ECResponse.RES_CVV_MISMATCH:
			case ECResponse.RES_CVV_AND_AVS_MISMATCH:
				log.info(message);
				return response.getReturnCode();
			default:
				log.warn("Auth Failed: " + message);
				return response.getReturnCode();
		}
	}

	protected int hessianCharge(String env, String username, String password, String deviceSerialCd, long tranId, String track, EntryType entryType, long amount, String itemDesc, String invoiceNumber, String attributes) throws Exception {
		EC2ServiceAPI ec = getEC2Service(env);
		DecimalFormat amountFormat = new DecimalFormat("#,##0.00");
		log.info("Sending Charge for card " + track + " (" + entryType + ") on " + deviceSerialCd + " for $" + amountFormat.format(((double) amount) / 100));
		String tranResult;
		StringBuilder sb = new StringBuilder();
		tranResult = "S";
		sb.append("A0|200|").append(amount).append("|1|");
		if(!StringUtils.isBlank(itemDesc))
			sb.append(itemDesc.replace('|', ' '));
		if(!StringUtils.isBlank(invoiceNumber))
			sb.append("|403||1|").append(invoiceNumber.replace('|', '/'));
		String tranDetails = sb.toString();
		EC2AuthResponse response = ec.chargePlain(username, password, deviceSerialCd, tranId, amount, track, String.valueOf(entryType.getValue()), tranResult, tranDetails, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
		return response.getReturnCode();
	}

	@Test
	public void junit_initEdgeDevice() throws Exception {
		ClientEmulator ce = new ClientEmulator(host, salePort, "TD999999", keys.get(host).get("TD999999"));
		MessageData_CB dataCB;
		ce.startCommunication(7);
		try {
			dataCB = (MessageData_CB)ce.sendMessage(ce.constructInitV4Message(InitializationReason.NEW_DEVICE, 4001005, 5, DeviceType.EDGE,
				"Client Emulator 1.3 firmware version",
				"EE100033333",
				"Client Emulator 1.3 Device Info",
				"Client Emulator 1.3 Terminal Info"));
		} finally {
			ce.stopCommunication();
		}
		if(dataCB != null && dataCB.getServerActionCode() == GenericResponseServerActionCode.PROCESS_PROP_LIST) {
			Map<Integer, String> propertyValues = ((PropertyValueListAction) dataCB.getServerActionCodeData()).getPropertyValues();
			String deviceName = propertyValues.get(51);
			String encryptionKeyHex = propertyValues.get(52);
			log.info("Got DeviceName: " + deviceName + "; Encryption Key: " + encryptionKeyHex);
		}
	}


	@Test
	public void junit_initEdgeDeviceEE100000015() throws Exception {
		host = "10.0.0.67";
		ClientEmulator ce = new ClientEmulator(host, salePort, "TD999999", keys.get(host).get("TD999999"));
		MessageData_CB dataCB;
		ce.startCommunication(7);
		try {
			dataCB = (MessageData_CB) ce.sendMessage(ce.constructInitV4Message(InitializationReason.APPLICATION_UPGRADE, 4001010, 17, DeviceType.EDGE,
				"Client Emulator 1.5 firmware version",
				"EE100000015",
				"Client Emulator 1.5 Device Info",
				"Client Emulator 1.5 Terminal Info"));
			if(dataCB != null && dataCB.getServerActionCode() == GenericResponseServerActionCode.PROCESS_PROP_LIST) {
				Map<Integer, String> propertyValues = ((PropertyValueListAction) dataCB.getServerActionCodeData()).getPropertyValues();
				String deviceName = propertyValues.get(51);
				String encryptionKeyHex = propertyValues.get(52);
				log.info("Got DeviceName: " + deviceName + "; Encryption Key: " + encryptionKeyHex);
				ce.setEvNumber(deviceName);
				ce.setEncryptionKey(ByteArrayUtils.fromHex(encryptionKeyHex));
			}

			if(dataCB != null && dataCB.getResultCode() == GenericResponseResultCode.OKAY) {
				MessageData_C1 dataC1 = new MessageData_C1();
				dataC1.setBaseComponentType(ComponentType.EPORT_EDGE_G9);
				dataC1.setDefaultCurrency(Currency.getInstance("USD"));
				BaseComponent bc = dataC1.getBaseComponent();
				bc.setLabel("BK Test Model");
				bc.setManufacturer("USA Tech");
				bc.setModel("Telemeter");
				bc.setRevision("3.03.0003BK");
				bc.setSerialNumber("EE100000015");
				ComponentData cd1 = dataC1.addComponent();
				cd1.setComponentNumber(1);
				cd1.setComponentType(ComponentType.VENDING_MACHINE);
				cd1.setManufacturer("VCS 90129812 VEC 9.1 '\r");

				ComponentData cd2 = dataC1.addComponent();
				cd2.setComponentNumber(2);
				cd2.setComponentType(ComponentType.VLTE_VERIZON_MODEM);
				cd2.setManufacturer("Telit");
				cd2.setModel("000000000000000");
				cd2.setLabel("354676050014801");
				cd2.setRevision("12.00.514");
				cd2.setSerialNumber("15773209171");

				ce.sendMessage(dataC1);
			}
		} finally {
			ce.stopCommunication();
		}
	}
	
	@Test
	public void junit_initEdgewithGDI() throws Exception {
		host = "10.0.0.67";
		ClientEmulator ce = new ClientEmulator(host, salePort, "TD999999", keys.get(host).get("TD999999"));
		MessageData_CB dataCB;
		ce.startCommunication(7);
		try {
			dataCB = (MessageData_CB) ce.sendMessage(ce.constructInitV4Message(InitializationReason.APPLICATION_UPGRADE, 4001010, 18, DeviceType.EDGE, "Client Emulator 1.5 firmware version", "EE100000015", "Client Emulator 1.5 Device Info", "Client Emulator 1.5 Terminal Info"));
			if(dataCB != null && dataCB.getServerActionCode() == GenericResponseServerActionCode.PROCESS_PROP_LIST) {
				Map<Integer, String> propertyValues = ((PropertyValueListAction) dataCB.getServerActionCodeData()).getPropertyValues();
				String deviceName = propertyValues.get(51);
				String encryptionKeyHex = propertyValues.get(52);
				log.info("Got DeviceName: " + deviceName + "; Encryption Key: " + encryptionKeyHex);
				ce.setEvNumber(deviceName);
				ce.setEncryptionKey(ByteArrayUtils.fromHex(encryptionKeyHex));
			}

			if(dataCB != null && dataCB.getResultCode() == GenericResponseResultCode.OKAY) {
				// *
				// String gdi =
				// "A\nSPEC=3002004\nFW=BSK_Test_3\nBL=Bootloader = N/A\nDIAG=Diagnostic = N/A\nC_00=TP:201\rMI:USAT\rMN:Edger\rAV:BSK_Test_4\rSN:EE100000001\rBV:BigOleBoot!\r\nC_01=TP:200\rMI:AVI\rMN:A90849\rSN:#123456\r\nC_02=TP:210\rCGMI:Telit\rCGMM:UE910-NAD\rCGMR:12.00.514\rCGSN:354676050014801\rCCID:1234567890123456789\rCSQ:3,40\rCIMI:6109890340\r\nC_03=TP:400\rMI:OTI\rMN:Saturn 6500-G6A\rSN:0006FD35\rAV:040110\rHV:1.0\r\n";
				String gdi = "A\nSPEC=3002004\nFW=BSK_Test_3\nBL=Bootloader = N/A\nDIAG=Diagnostic = N/A\nCY=USD\nC_00=TP:201\rMI:USAT\rMN:Edger\rAV:BSK_Test_4\rSN:EE100000001\rBV:BigOleBoot!\r\nC_01=TP:200\rMI:AVI\rMN:A90849\rSN:#123456\r\nC_02=TP:215\rCGMI:Velit\rCGMM:VV00VV\rCGMR:N6.E3\rCGSN:354676050014801\rMEID:0004941\rCSQ:6,0\r\nC_03=TP:400\rMI:OTI\rMN:Saturn 6500-G6A\rSN:0006FD35\rAV:040110\rHV:1.0\r\nC_06=TP:100\rMI:AmpRF\rMN:2.0.0a\rSN:11-99\r\n";

				MessageData_C7 dataC7 = new MessageData_C7();
				dataC7.setContent(gdi.getBytes());
				dataC7.setCreationTime(Calendar.getInstance());
				dataC7.setFileType(FileType.GENERIC_DEVICE_INFORMATION);
				ce.sendMessage(dataC7); // */
				/*
				MessageData_C1 dataC1 = new MessageData_C1();
				dataC1.setBaseComponentType(ComponentType.EPORT_EDGE_G9);
				dataC1.setDefaultCurrency(Currency.getInstance("USD"));
				BaseComponent bc = dataC1.getBaseComponent();
				bc.setLabel("BK Test Model");
				bc.setManufacturer("USA Tech");
				bc.setModel("Telemeter");
				bc.setRevision("3.03.0003BK");
				bc.setSerialNumber("EE100000015");
				ComponentData cd1 = dataC1.addComponent();
				cd1.setComponentNumber(1);
				cd1.setComponentType(ComponentType.VENDING_MACHINE);
				cd1.setManufacturer("VCS 90129812 VEC 9.1 '\r");

				ComponentData cd2 = dataC1.addComponent();
				cd2.setComponentNumber(2);
				cd2.setComponentType(ComponentType.CDMA_VERIZON_MODEM);
				cd2.setManufacturer("Telit");
				cd2.setModel("000000000000000");
				cd2.setLabel("354676050014801");
				cd2.setRevision("12.00.514");
				cd2.setSerialNumber("15773209171");
				ce.sendMessage(dataC1);
				// */
			}
		} finally {
			ce.stopCommunication();
		}
	}
	
	@Test
	public void junit_initG9withGDI() throws Exception {
		ClientEmulator ce = new ClientEmulator(host, salePort, "TD999999", keys.get(host).get("TD999999"));
		MessageData_CB dataCB;
		ce.startCommunication(7);
		try {
			dataCB = (MessageData_CB) ce.sendMessage(ce.constructInitV4Message(InitializationReason.APPLICATION_UPGRADE, 4001010, 18, DeviceType.EDGE, "Client Emulator 1.5 firmware version", "VJ200000003", "Client Emulator 1.5 Device Info", "Client Emulator 1.5 Terminal Info"));
			if(dataCB != null && dataCB.getServerActionCode() == GenericResponseServerActionCode.PROCESS_PROP_LIST) {
				Map<Integer, String> propertyValues = ((PropertyValueListAction) dataCB.getServerActionCodeData()).getPropertyValues();
				String deviceName = propertyValues.get(51);
				String encryptionKeyHex = propertyValues.get(52);
				log.info("Got DeviceName: " + deviceName + "; Encryption Key: " + encryptionKeyHex);
				ce.setEvNumber(deviceName);
				ce.setEncryptionKey(ByteArrayUtils.fromHex(encryptionKeyHex));
			}

			if(dataCB != null && dataCB.getResultCode() == GenericResponseResultCode.OKAY) {
				// *
				// String gdi =
				// "A\nSPEC=3002004\nFW=BSK_Test_3\nBL=Bootloader = N/A\nDIAG=Diagnostic =
				// N/A\nC_00=TP:201\rMI:USAT\rMN:Edger\rAV:BSK_Test_4\rSN:EE100000001\rBV:BigOleBoot!\r\nC_01=TP:200\rMI:AVI\rMN:A90849\rSN:#123456\r\nC_02=TP:210\rCGMI:Telit\rCGMM:UE910-NAD\rCGMR:12.00.514\rCGSN:354676050014801\rCCID:1234567890123456789\rCSQ:3,40\rCIMI:6109890340\r\nC_03=TP:400\rMI:OTI\rMN:Saturn
				// 6500-G6A\rSN:0006FD35\rAV:040110\rHV:1.0\r\n";
				String gdi = "A\nSPEC=3002004\nFW=BSK_Test_3\nBL=Bootloader = N/A\nDIAG=Diagnostic = N/A\nCY=USD\nC_00=TP:201\rMI:USAT\rMN:G9\rAV:BSK_Test_4\rSN:VJ200000003\rBV:BigOleBoot!\r\nC_01=TP:200\rMI:AVI\rMN:A90849\rSN:#123456\r\nC_02=TP:215\rCGMI:Velit\rCGMM:VV00VV\rCGMR:N6.E3\rCGSN:354676050014801\rMEID:0004941\rCSQ:6,0\r\nC_03=TP:400\rMI:MEI 4-in-1+\rMN:1\rSN:0006FD35\rAV:040110\rHV:1.0\r\nC_06=TP:100\rMI:AmpRF\rMN:2.0.0a\rSN:11-99\r\n";

				MessageData_C7 dataC7 = new MessageData_C7();
				dataC7.setContent(gdi.getBytes());
				dataC7.setCreationTime(Calendar.getInstance());
				dataC7.setFileType(FileType.GENERIC_DEVICE_INFORMATION);
				ce.sendMessage(dataC7); // */
				/*
				MessageData_C1 dataC1 = new MessageData_C1();
				dataC1.setBaseComponentType(ComponentType.EPORT_EDGE_G9);
				dataC1.setDefaultCurrency(Currency.getInstance("USD"));
				BaseComponent bc = dataC1.getBaseComponent();
				bc.setLabel("BK Test Model");
				bc.setManufacturer("USA Tech");
				bc.setModel("Telemeter");
				bc.setRevision("3.03.0003BK");
				bc.setSerialNumber("EE100000015");
				ComponentData cd1 = dataC1.addComponent();
				cd1.setComponentNumber(1);
				cd1.setComponentType(ComponentType.VENDING_MACHINE);
				cd1.setManufacturer("VCS 90129812 VEC 9.1 '\r");
				
				ComponentData cd2 = dataC1.addComponent();
				cd2.setComponentNumber(2);
				cd2.setComponentType(ComponentType.CDMA_VERIZON_MODEM);
				cd2.setManufacturer("Telit");
				cd2.setModel("000000000000000");
				cd2.setLabel("354676050014801");
				cd2.setRevision("12.00.514");
				cd2.setSerialNumber("15773209171");
				ce.sendMessage(dataC1);
				// */
			}
		} finally {
			ce.stopCommunication();
		}
	}

	@Test
	public void junit_activateEdgeDevice() throws Exception {
		String deviceName = "TD000023";
		ClientEmulator ce = new ClientEmulator(host, salePort, deviceName, keys.get(host).get(deviceName));		
		ce.startCommunication(7);
		try {
			ce.testGenReqAR();
		} finally {
			ce.stopCommunication();
		}
	}
	@Test
	public void junit_initKioskDevice() throws Exception {
		ClientEmulator ce = new ClientEmulator(host, unifedlayerPort, "EV000001", keys.get(host).get("EV000001"));		
		MessageData_8F reply;
		ce.startCommunication(7);
		try {
			ce.msg = 10;
			MessageData_AD data = new MessageData_AD();
			data.setDeviceInfo("Client Emulator 1.3 Device Info");
			data.setDeviceSerialNum("K3MTB000002");
			data.setDeviceType(DeviceType.KIOSK);
			data.setTerminalInfo("Client Emulator 1.3 Terminal Info");
			reply = (MessageData_8F)ce.sendMessage(data);
		} finally {
			ce.stopCommunication();
		}
		if(reply != null) {
			String deviceName = new String(reply.getDeviceSerialNum(), ProcessingConstants.US_ASCII_CHARSET);
			String encryptionKeyHex = new String(reply.getEncryptionKey(), ProcessingConstants.US_ASCII_CHARSET);;
			log.info("Got DeviceName: " + deviceName + "; Encryption Key: " + encryptionKeyHex);
		}
	}
	@Test
	public void junit_initG8ADevice() throws Exception {
		ClientEmulator ce = new ClientEmulator(host, layer3Port, "EV000001", keys.get(host).get("EV000001"));		
		MessageData_8F reply;
		ce.startCommunication(4);
		try {
			ce.msg = 10;
			MessageData_AD data = new MessageData_AD();
			//data.setDeviceInfo("A\nCFG=2\nSPEC=3002004\nREASON=0\nFW=USA-Gx1v6.0.1CT7\nBL=Bootloader = N/A\nDIAG=Diagnostic = N/A\nBEZEL=BT:V\rAV:EC5 GR 1.1.0\r\n");
			data.setDeviceInfo("A\nCFG=2\nSPEC=3002004\nREASON=0\nFW=USA-Gx1v6.0.1CT7-A\nBL=Bootloader = N/A\nDIAG=Diagnostic = N/A\nBEZEL=BT:B\rAV:2.5.1\rMN:54D908\rSN:100-0491BSK\rBV:Boot4.2.1\rHV:7.0.2\r\n");
			data.setDeviceSerialNum("G8200003");
			data.setDeviceType(DeviceType.GX);
			reply = (MessageData_8F)ce.sendMessage(data);
		} finally {
			ce.stopCommunication();
		}
		if(reply != null) {
			String deviceName = new String(reply.getDeviceSerialNum(), ProcessingConstants.US_ASCII_CHARSET);
			String encryptionKeyHex = new String(reply.getEncryptionKey(), ProcessingConstants.US_ASCII_CHARSET);;
			log.info("Got DeviceName: " + deviceName + "; Encryption Key: " + encryptionKeyHex);
		}
	}
	
	@Test
	public void junit_elavonISO8583Certification3() throws Exception {
		ClientEmulator ce1 = new ClientEmulator(host, authPort, "TD000597", keys.get(host).get("TD000597"));		
		
		authAndSale(ce1, authPort, salePort, "5454545454545454=10121015432112345601", EntryType.SWIPE, new BigDecimal(1500), new BigDecimal(800)); // Test 1 
		authAndSale(ce1, authPort, salePort, "6011000995500000=10121015432112345678", EntryType.SWIPE, new BigDecimal(1200), new BigDecimal(1000)); // Test 2
		authAndSale(ce1, authPort, salePort, "4055011111111111=10121015432112345678", EntryType.CONTACTLESS, new BigDecimal(1200), new BigDecimal(500)); // Test 3
		authAndSale(ce1, authPort, salePort, "5454545454545454=10121015432112345601", EntryType.SWIPE, new BigDecimal(1500), new BigDecimal(0)); // Test 4
		
		authAndSale(ce1, authPort, salePort, "371449635398431=10121015432112345678", EntryType.SWIPE, new BigDecimal(1500), new BigDecimal(526)); // Test 7
		authAndSale(ce1, authPort, salePort, "4055011111111111=10121015432112345678", EntryType.SWIPE, new BigDecimal(1200), new BigDecimal(0)); // Test 8
		authAndSale(ce1, authPort, salePort, "5454545454545454=10121015432112345601", EntryType.CONTACTLESS, new BigDecimal(1200), new BigDecimal(340)); // Test 10
	
		authAndSale(ce1, authPort, salePort, "6011000995500000=10121015432112345678", EntryType.SWIPE, new BigDecimal(1200), new BigDecimal(525)); // Test 12
		authAndSale(ce1, authPort, salePort, "4055011111111111=10121015432112345678", EntryType.SWIPE, new BigDecimal(1200), new BigDecimal(600)); // Test 13
		authAndSale(ce1, authPort, salePort, "4055011111111111=10121015432112345678", EntryType.SWIPE, new BigDecimal(1500), new BigDecimal(920)); // Test 14
	}

	@Test
	public void junit_elavonISO8583AuthAndSale() throws Exception {
		ClientEmulator ce1 = new ClientEmulator(host, authPort, "TD000597", keys.get(host).get("TD000597"));

		authAndSale(ce1, authPort, salePort, "5431390000000003=20060000000000000000?3?", EntryType.SWIPE, new BigDecimal(1500), new BigDecimal(800)); // Test 1
	}

	@Test
	public void junit_elavonISO8583AuthAndCancel() throws Exception {
		ClientEmulator ce1 = new ClientEmulator(host, authPort, "TD000597", keys.get(host).get("TD000597"));
		authAndSale(ce1, authPort, salePort, "5454545454545454=10121015432112345601", EntryType.SWIPE, new BigDecimal(1500), new BigDecimal(0)); // Test 1
	}

	@Test
	public void junit_initEdgeDeviceEE190000129_invalid_msg_id() throws Exception {
		ClientEmulator ce = new ClientEmulator(host, salePort, "TD999999", keys.get(host).get("TD999999"));
		ce.setMessageId(-1);
		MessageData_CB dataCB;
		ce.startCommunication(7);
		try {
			dataCB = (MessageData_CB) ce.sendMessage(ce.constructInitV4Message(InitializationReason.APPLICATION_UPGRADE, 4001010, 17, DeviceType.EDGE, "Client Emulator 1.5 firmware version", "EE190000129", "Client Emulator 1.5 Device Info", "Client Emulator 1.5 Terminal Info"));
			if(dataCB != null && dataCB.getServerActionCode() == GenericResponseServerActionCode.PROCESS_PROP_LIST) {
				Map<Integer, String> propertyValues = ((PropertyValueListAction) dataCB.getServerActionCodeData()).getPropertyValues();
				String deviceName = propertyValues.get(51);
				String encryptionKeyHex = propertyValues.get(52);
				log.info("Got DeviceName: " + deviceName + "; Encryption Key: " + encryptionKeyHex);
				ce.setEvNumber(deviceName);
				ce.setEncryptionKey(ByteArrayUtils.fromHex(encryptionKeyHex));
			}

			if(dataCB != null && dataCB.getResultCode() == GenericResponseResultCode.OKAY) {
				MessageData_C1 dataC1 = new MessageData_C1();
				dataC1.setBaseComponentType(ComponentType.EPORT_EDGE_G9);
				dataC1.setDefaultCurrency(Currency.getInstance("USD"));
				BaseComponent bc = dataC1.getBaseComponent();
				bc.setLabel("BK Test Model");
				bc.setManufacturer("USA Tech");
				bc.setModel("Telemeter");
				bc.setRevision("3.03.0003BK");
				bc.setSerialNumber("EE190000129");
				ComponentData cd1 = dataC1.addComponent();
				cd1.setComponentNumber(1);
				cd1.setComponentType(ComponentType.VENDING_MACHINE);
				cd1.setManufacturer("VCS 90129812 VEC 9.1 '\r");

				ComponentData cd2 = dataC1.addComponent();
				cd2.setComponentNumber(2);
				cd2.setComponentType(ComponentType.HSPA_ATT_MODEM);
				cd2.setManufacturer("Telit");
				cd2.setModel("000000000000000");
				cd2.setLabel("354676050014801");
				cd2.setRevision("12.00.514");
				cd2.setSerialNumber("15773209171");

				ComponentData cd3 = dataC1.addComponent();
				cd3.setComponentNumber(3);
				cd3.setComponentType(ComponentType.BEZEL);
				cd3.setManufacturer("Saturn 6500 EM");
				cd3.setModel("N/A");
				cd3.setLabel("123456");
				cd3.setRevision("1.0.1");
				cd3.setSerialNumber("456");

				ce.sendMessage(dataC1);
			}
		} finally {
			ce.stopCommunication();
		}
	}

	@Test
	public void junit_initEdgeDeviceEE190000129() throws Exception {
		ClientEmulator ce = new ClientEmulator(host, salePort, "TD999999", keys.get(host).get("TD999999"));
		MessageData_CB dataCB;
		ce.startCommunication(7);
		try {
			dataCB = (MessageData_CB) ce.sendMessage(ce.constructInitV4Message(InitializationReason.APPLICATION_UPGRADE, 4001010, 17, DeviceType.EDGE, "Client Emulator 1.5 firmware version", "EE190000129", "Client Emulator 1.5 Device Info", "Client Emulator 1.5 Terminal Info"));
			if(dataCB != null && dataCB.getServerActionCode() == GenericResponseServerActionCode.PROCESS_PROP_LIST) {
				Map<Integer, String> propertyValues = ((PropertyValueListAction) dataCB.getServerActionCodeData()).getPropertyValues();
				String deviceName = propertyValues.get(51);
				String encryptionKeyHex = propertyValues.get(52);
				log.info("Got DeviceName: " + deviceName + "; Encryption Key: " + encryptionKeyHex);
				ce.setEvNumber(deviceName);
				ce.setEncryptionKey(ByteArrayUtils.fromHex(encryptionKeyHex));
			}

			if(dataCB != null && dataCB.getResultCode() == GenericResponseResultCode.OKAY) {
				MessageData_C1 dataC1 = new MessageData_C1();
				dataC1.setBaseComponentType(ComponentType.EPORT_EDGE_G9);
				dataC1.setDefaultCurrency(Currency.getInstance("USD"));
				BaseComponent bc = dataC1.getBaseComponent();
				bc.setLabel("BK Test Model");
				bc.setManufacturer("USA Tech");
				bc.setModel("Telemeter");
				bc.setRevision("3.03.0003BK");
				bc.setSerialNumber("EE190000129");
				ComponentData cd1 = dataC1.addComponent();
				cd1.setComponentNumber(1);
				cd1.setComponentType(ComponentType.VENDING_MACHINE);
				cd1.setManufacturer("VCS 90129812 VEC 9.1 '\r");

				ComponentData cd2 = dataC1.addComponent();
				cd2.setComponentNumber(2);
				cd2.setComponentType(ComponentType.HSPA_ATT_MODEM);
				cd2.setManufacturer("Telit");
				cd2.setModel("000000000000000");
				cd2.setLabel("354676050014801");
				cd2.setRevision("12.00.514");
				cd2.setSerialNumber("15773209171");

				ComponentData cd3 = dataC1.addComponent();
				cd3.setComponentNumber(3);
				cd3.setComponentType(ComponentType.BEZEL);
				cd3.setManufacturer("Saturn 6500 EM");
				cd3.setModel("N/A");
				cd3.setLabel("123456");
				cd3.setRevision("1.0.1");
				cd3.setSerialNumber("456");

				ce.sendMessage(dataC1);
			}
		} finally {
			ce.stopCommunication();
		}
	}

	protected void authAndSale(String host, String deviceName, String card, BigDecimal saleAmount) throws Exception {
		authAndSale(host, authPort, salePort, deviceName, keys.get(host).get(deviceName), card, EntryType.SWIPE, saleAmount);
	}
	protected void authAndSale(String host, String deviceName, String card, BigDecimal saleAmount, boolean convFee) throws Exception {
		authAndSale(host, authPort, salePort, deviceName, keys.get(host).get(deviceName), card, EntryType.SWIPE, null, saleAmount, convFee);
	}
	protected void authAndSale(String host, String deviceName, String card, EntryType entryType, BigDecimal authAmount, BigDecimal saleAmount) throws Exception {
		authAndSale(host, authPort, salePort, deviceName, keys.get(host).get(deviceName), card, entryType, authAmount, saleAmount, false);
	}
	protected void authAndSale(String host, int authPort, int salePort, String deviceName, byte[] encryptKey, String card, EntryType entryType, BigDecimal saleAmount) throws Exception {
		authAndSale(host, authPort, salePort, deviceName, encryptKey, card, entryType, null, saleAmount, false);
	}
	protected void authAndSale(String host, int authPort, int salePort, String deviceName, byte[] encryptKey, String card, EntryType entryType, BigDecimal authAmount, BigDecimal saleAmount, boolean convFee) throws Exception {
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, encryptKey);
		authAndSale(ce, authPort, salePort, card, entryType, authAmount, saleAmount, convFee);
	}
	protected void authAndSale(ClientEmulator ce, int authPort, int salePort, String card, EntryType entryType, BigDecimal authAmount, BigDecimal saleAmount) throws Exception {
		authAndSale(ce, authPort, salePort, card, entryType, authAmount, saleAmount, false);
	}

	protected MessageData authOnly(ClientEmulator ce, int authPort, String card, EntryType entryType, BigDecimal authAmount) throws Exception {
		ce.setPort(authPort);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.startCommunication(7);
		MessageData reply;
		try {
			reply = ce.testAuthV4(entryType, card, authAmount);
		} finally {
			ce.stopCommunication();
		}
		return reply;
	}
	protected void authAndSale(ClientEmulator ce, int authPort, int salePort, String card, EntryType entryType, BigDecimal authAmount, BigDecimal saleAmount, boolean convFee) throws Exception {
		if(authAmount == null) {
			if(saleAmount == null || saleAmount.signum() == 0)
				authAmount = new BigDecimal(737);
			else
				authAmount = saleAmount;
		}
		long tranId = ce.getMasterId();
		ce.setPort(authPort);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.startCommunication(7);
		MessageData reply;
		try {
			reply = ce.testAuthV4(entryType, card, authAmount);
		} finally {
			ce.stopCommunication();
		}
		if(reply == null)
			return;
		boolean doSale;
		switch(reply.getMessageType()) {
			case AUTH_RESPONSE_4_1:
				AuthResponseTypeData artd = ((MessageData_C3)reply).getAuthResponseTypeData();
				AuthResponseType art = ((MessageData_C3)reply).getAuthResponseType();
				switch(art) {
					case AUTHORIZATION:
						AuthResponseCodeC3 rc = ((AuthorizationAuthResponse) artd).getResponseCode();
						switch(rc) {
							case SUCCESS:
							case CONDITIONAL_SUCCESS:
								doSale = true;
								break;
							default:
								doSale = false;
								log.info("Cannot process sale because auth was rejected");
						}
						break;
					case AUTHORIZATION_V2:
						rc = ((AuthorizationV2AuthResponse) artd).getResponseCode();
						switch(rc) {
							case SUCCESS: case CONDITIONAL_SUCCESS:
								doSale = true;
								break;
							default:
								doSale = false;
								log.info("Cannot process sale because auth was rejected");
						}
						break;
					default:
						doSale = false;
						log.info("Cannot process sale because auth response was " + art);
				}
				break;
			default:
				doSale = false;
				log.info("Cannot process sale because response was " + reply.getMessageType());
		}
		if(doSale && authAmount.signum() > 0) {
			ce.setPort(salePort);
			log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
			ce.startCommunication(7);
			try {
				if(saleAmount != null)
					ce.testSingleSale(tranId, SaleType.ACTUAL, saleAmount, convFee);
				else
					ce.testSale(tranId, SaleType.ACTUAL, 3, convFee);
			} finally {
				ce.stopCommunication();
			}
		}
	}

	protected void charge(String host, String deviceName, String card, BigDecimal chargeAmount, boolean cancel, long cashAmount) throws Exception {
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		charge(ce, authPort, card, EntryType.SWIPE, chargeAmount, 0, 3, cancel, cashAmount);
	}

	protected void charge(ClientEmulator ce, int authPort, String card, EntryType entryType, BigDecimal chargeAmount, long convFee, int items, boolean cancel, long cashAmount) throws Exception {
		if(chargeAmount == null) {
			chargeAmount = new BigDecimal(848);
		}
		String cashItemDesc = "001F";
		long tranId = ce.getMasterId();
		ce.setPort(authPort);
		MessageData_CC dataCC = new MessageData_CC();
		dataCC.setCardReaderType(CardReaderType.GENERIC);
		MessageData_CC.GenericCardReader gcr = (MessageData_CC.GenericCardReader) dataCC.getCardReader();
		gcr.setAccountData2(card);
		dataCC.setEntryType(entryType);
		dataCC.setReceiptResult(ReceiptResult.UNAVAILABLE);
		dataCC.setTransactionId(tranId);
		dataCC.setLineItemFormat((byte) 0);
		BigDecimal parts = BigDecimal.valueOf(items);
		MessageData_CC.Format0LineItem li;
		int n = 0;
		BigDecimal total = BigDecimal.ZERO;
		if(cashAmount > 0) {
			li = (MessageData_CC.Format0LineItem) dataCC.addLineItem();
			li.setComponentNumber(1);
			li.setItem(206);
			li.setDescription(cashItemDesc);
			li.setDuration(20);
			li.setPrice(BigDecimal.valueOf(cashAmount));
			li.setQuantity(1);
			li.setSaleResult(SaleResult.SUCCESS);
			total = total.add(BigDecimal.valueOf(cashAmount));
		}
		for(int i = 0; i < items; i++) {
			li = (MessageData_CC.Format0LineItem) dataCC.addLineItem();
			li.setComponentNumber(1);
			li.setItem(200);
			switch(i % 3) {
				case 0:
					li.setDescription("001F");
					li.setDuration(2);
					if(cashAmount > 0 && i == 0 && "001F".equals(cashItemDesc))
						li.setPrice(chargeAmount.divideToIntegralValue(parts).subtract(BigDecimal.valueOf(cashAmount)));
					else
						li.setPrice(chargeAmount.divideToIntegralValue(parts));
					li.setQuantity(1);
					li.setSaleResult(SaleResult.SUCCESS);
					break;
				case 1:
					li.setDescription("002B");
					li.setDuration(3);
					li.setPrice(chargeAmount.divideToIntegralValue(parts.multiply(BigDecimal.valueOf(2))));
					li.setQuantity(2);
					li.setSaleResult(SaleResult.SUCCESS);
					break;
				case 2:
					li.setDescription("ABCD");
					li.setDuration(7);
					li.setPrice(chargeAmount.divideToIntegralValue(parts.multiply(BigDecimal.valueOf(3))));
					li.setQuantity(3);
					li.setSaleResult(SaleResult.SUCCESS);
					break;
			}
			total = total.add(li.getPrice().multiply(BigDecimal.valueOf(li.getQuantity())));
			n += li.getQuantity();
		}
		if(convFee > 0 && items > 0) {
			li = (MessageData_CC.Format0LineItem) dataCC.addLineItem();
			li.setComponentNumber(1);
			li.setItem(203);
			li.setDuration(0);
			li.setPrice(new BigDecimal(10));
			li.setQuantity(n);
			li.setSaleResult(SaleResult.SUCCESS);
			total = total.add(li.getPrice().multiply(BigDecimal.valueOf(li.getQuantity())));
		}
		dataCC.setAmount(total);

		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.startCommunication(7);
		try {
			ce.sendMessage(dataCC);
		} finally {
			ce.stopCommunication();
		}

		if(cancel) {
			MessageData_C4 md = new MessageData_C4();
			md.setBatchId(1);
			md.setDirection(MessageDirection.CLIENT_TO_SERVER);
			md.setReceiptResult(ReceiptResult.UNAVAILABLE);
			Calendar cal = Calendar.getInstance();
			// cal.setTimeInMillis(transactionId*1000);
			md.setSaleSessionStart(cal);
			md.setSaleType(SaleType.ACTUAL);
			md.setTransactionId(tranId);
			md.setLineItemFormat((byte) 0);
			md.setSaleAmount(BigDecimal.ZERO);
			md.setSaleResult(SaleResult.CANCELLED_MACHINE_FAILURE);
			log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
			ce.setPort(salePort);
			ce.startCommunication(7);
			try {
				ce.sendMessage(md);
			} finally {
				ce.stopCommunication();
			}
		}
	}
	protected MessageData buildSaleC4(long transactionId, SaleType saleType, int items, boolean convFee) {
		MessageData_C4 md = new MessageData_C4();
		md.setBatchId(1);
		md.setDirection(MessageDirection.CLIENT_TO_SERVER);
		md.setReceiptResult(ReceiptResult.UNAVAILABLE);
		md.setSaleResult(SaleResult.SUCCESS);
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(transactionId * 1000);
		md.setSaleSessionStart(cal);
		md.setSaleType(saleType);
		md.setTransactionId(transactionId);
		LineItemAdder lia = new LineItemAdder_C4(md);
		BigDecimal total = addLineItems(lia, items, convFee);
		md.setSaleAmount(total);
		return md;
	}

	protected BigDecimal addLineItems(LineItemAdder lia, int items, boolean convFee) {
		BigDecimal total = BigDecimal.ZERO;
		int n = 0;
		for(int i = 0; i < items; i++) {
			String desc;
			BigDecimal price;
			int quantity;
			int duration;
			switch(i % 3) {
				case 0:
					desc = "#25";
					duration = 2;
					price = new BigDecimal(110);
					quantity = 1;
					break;
				case 1:
					desc = "#33";
					duration = 3;
					price = new BigDecimal(115);
					quantity = 2;
					break;
				case 2:
				default:
					desc = "#27";
					duration = 7;
					price = new BigDecimal(125);
					quantity = 3;
					break;
			}
			lia.addLineItem(1, 200, desc, duration, price, quantity);
			total = total.add(price.multiply(BigDecimal.valueOf(quantity)));
			n += quantity;
		}
		if(convFee) {
			BigDecimal price = new BigDecimal(7);
			lia.addLineItem(1, 203, null, 0, price, n);
			total = total.add(price.multiply(BigDecimal.valueOf(n)));
		}
		lia.complete();
		return total;
	}

	protected interface LineItemAdder {
		public void addLineItem(int component, int itemType, String description, int duration, BigDecimal price, int quantity);

		public void complete();
	}

	protected class LineItemAdder_C4 implements LineItemAdder {
		protected final MessageData_C4 dataC4;

		public LineItemAdder_C4(MessageData_C4 dataC4) {
			this.dataC4 = dataC4;
			dataC4.setLineItemFormat((byte) 0);
		}

		@Override
		public void addLineItem(int component, int itemType, String description, int duration, BigDecimal price, int quantity) {
			Format0LineItem li = (Format0LineItem) dataC4.addLineItem();
			li.setComponentNumber(component);
			li.setItem(itemType);
			li.setDescription(description);
			li.setDuration(duration);
			li.setPrice(price);
			li.setQuantity(quantity);
			li.setSaleResult(SaleResult.SUCCESS);
		}

		@Override
		public void complete() {
		}
	}

	protected class LineItemAdder_2A implements LineItemAdder {
		protected final MessageData_2A data2A;

		public LineItemAdder_2A(MessageData_2A data2A) {
			this.data2A = data2A;
			data2A.setVendByteLength((byte) 1);
		}

		@Override
		public void addLineItem(int component, int itemType, String description, int duration, BigDecimal price, int quantity) {
			int pn = 0;
			if(description == null)
				pn = 0;
			else if(description.matches("[0-9A-Fa-f]+"))
				pn = Integer.parseInt(description, 16);
			else if(description.length() < 2 || description.charAt(0) != '#' && (pn = ConvertUtils.getIntSafely(description.substring(1), -1)) < 0)
				pn = description.hashCode();
			for(int i = 0; i < quantity; i++) {
				MessageData_2A.NetBatch1LineItem li = (MessageData_2A.NetBatch1LineItem) data2A.addActualLineItem();
				li.setPositionNumber(pn);
				li.setReportedPrice(price.intValue());
			}
		}

		@Override
		public void complete() {
		}
	}

	protected class LineItemAdder_2B implements LineItemAdder {
		protected final MessageData_2B data2B;

		public LineItemAdder_2B(MessageData_2B data2B) {
			this.data2B = data2B;
			data2B.setVendByteLength((byte) 1);
		}

		@Override
		public void addLineItem(int component, int itemType, String description, int duration, BigDecimal price, int quantity) {
			int pn = 0;
			if(description == null)
				pn = 0;
			else if(description.matches("[0-9A-Fa-f]+"))
				pn = Integer.parseInt(description, 16);
			else if(description.length() < 2 || description.charAt(0) != '#' && (pn = ConvertUtils.getIntSafely(description.substring(1), -1)) < 0)
				pn = description.hashCode();
			for(int i = 0; i < quantity; i++) {
				MessageData_2B.NetBatch1LineItem li = (MessageData_2B.NetBatch1LineItem) data2B.addActualLineItem();
				li.setPositionNumber(pn);
				li.setReportedPrice(price.intValue());
			}
		}

		@Override
		public void complete() {
		}
	}
	protected class LineItemAdder_A2_BEX implements LineItemAdder {
		protected final MessageData_A2.BEXTransactionDetail detail;
		protected final StringBuilder sb = new StringBuilder("A0|");

		public LineItemAdder_A2_BEX(MessageData_A2.BEXTransactionDetail detail) {
			this.detail = detail;

		}

		@Override
		public void addLineItem(int component, int itemType, String description, int duration, BigDecimal price, int quantity) {
			sb.append(itemType).append('|').append(price).append('|').append(quantity).append('|');
			if(description != null)
				sb.append(description.replace('|', '_'));
			sb.append('|');
		}

		@Override
		public void complete() {
			detail.setPayload(sb.toString());
		}
	}

	protected class LineItemAdder_A2_Eport implements LineItemAdder {
		protected final MessageData_A2.EportTransactionDetail detail;
		protected final StringBuilder sb = new StringBuilder();

		public LineItemAdder_A2_Eport(MessageData_A2.EportTransactionDetail detail) {
			this.detail = detail;
			detail.setVendByteLength((byte) 2);
		}

		@Override
		public void addLineItem(int component, int itemType, String description, int duration, BigDecimal price, int quantity) {
			int pn = 0;
			if(description == null)
				pn = 0;
			else if(description.length() < 2 || description.charAt(0) != '#' && (pn = ConvertUtils.getIntSafely(description.substring(1), -1)) < 0)
				pn = description.hashCode();
			for(int i = 0; i < quantity; i++) {
				MessageData_A2.EportTransactionDetail.NetBatch2LineItem li = (MessageData_A2.EportTransactionDetail.NetBatch2LineItem) detail.addLineItem();
				li.setPositionNumber(pn);
				li.setReportedPrice(price.intValue());
			}
		}

		@Override
		public void complete() {
		}
	}

	protected class LineItemAdder_A2_MEI implements LineItemAdder {
		protected final MessageData_A2.MEITransactionDetail detail;
		protected final StringBuilder sb = new StringBuilder();

		public LineItemAdder_A2_MEI(MessageData_A2.MEITransactionDetail detail) {
			this.detail = detail;
			detail.setMeiVendByteLength((byte) 2);
		}

		@Override
		public void addLineItem(int component, int itemType, String description, int duration, BigDecimal price, int quantity) {
			int pn = 0;
			if(description == null)
				pn = 0;
			else if(description.length() < 2 || description.charAt(0) != '#' && (pn = ConvertUtils.getIntSafely(description.substring(1), -1)) < 0)
				pn = description.hashCode();
			for(int i = 0; i < quantity; i++) {
				MessageData_A2.MEITransactionDetail.NetBatch2LineItem li = (MessageData_A2.MEITransactionDetail.NetBatch2LineItem) detail.addLineItem();
				li.setPositionNumber(pn);
				li.setPrice(price.intValue());
			}
		}

		@Override
		public void complete() {
		}
	}
	protected MessageData buildSaleA2(long transactionId, SaleType saleType, int items, boolean convFee, DeviceType deviceType) {
		MessageData_A2 md = new MessageData_A2();
		md.setDeviceType(deviceType);
		md.setDirection(MessageDirection.CLIENT_TO_SERVER);
		md.setTransactionResult(TranDeviceResultType.SUCCESS);
		md.setTransactionId(transactionId);
		MessageData_A2.TransactionDetailData detail = md.getTransactionDetailData();
		LineItemAdder lia;
		if(detail instanceof MessageData_A2.BEXTransactionDetail) {
			lia = new LineItemAdder_A2_BEX((MessageData_A2.BEXTransactionDetail) detail);
		} else if(detail instanceof MessageData_A2.EportTransactionDetail) {
			lia = new LineItemAdder_A2_Eport((MessageData_A2.EportTransactionDetail) detail);
		} else if(detail instanceof MessageData_A2.MEITransactionDetail) {
			lia = new LineItemAdder_A2_MEI((MessageData_A2.MEITransactionDetail) detail);
		} else {
			throw new IllegalArgumentException("Detail is not of recognized type: " + detail.getClass().getName());
		}
		BigDecimal total = addLineItems(lia, items, convFee);
		md.setSaleAmount(total.longValue());
		return md;
	}

	protected MessageData buildSale2A(long transactionId, SaleType saleType, int items, boolean convFee, DeviceType deviceType) {
		MessageData_2A md = new MessageData_2A();
		md.setDirection(MessageDirection.CLIENT_TO_SERVER);
		md.setTransactionResult(TranDeviceResultType.SUCCESS);
		md.setTransactionId(transactionId);
		BigDecimal total = BigDecimal.ZERO;
		LineItemAdder lia = new LineItemAdder_2A(md);
		int n = 0;
		for(int i = 0; i < items; i++) {
			String desc;
			BigDecimal price;
			int quantity;
			int duration;
			switch(i % 3) {
				case 0:
					desc = "#25";
					duration = 2;
					price = new BigDecimal(110);
					quantity = 1;
					break;
				case 1:
					desc = "#33";
					duration = 3;
					price = new BigDecimal(115);
					quantity = 2;
					break;
				case 2:
				default:
					desc = "#27";
					duration = 7;
					price = new BigDecimal(125);
					quantity = 3;
					break;
			}
			lia.addLineItem(1, 200, desc, duration, price, quantity);
			total = total.add(price.multiply(BigDecimal.valueOf(quantity)));
			n += quantity;
		}
		if(convFee) {
			BigDecimal price = new BigDecimal(7);
			md.setConvenienceFee(price.multiply(BigDecimal.valueOf(n)).intValue());
			total = total.add(price.multiply(BigDecimal.valueOf(n)));
		}
		lia.complete();
		md.setSaleAmount(total.intValue());
		return md;
	}

	protected MessageData_2B buildSale2B(long transactionId, SaleType saleType, int items, boolean convFee, DeviceType deviceType) {
		MessageData_2B md = new MessageData_2B();
		md.setDirection(MessageDirection.CLIENT_TO_SERVER);
		md.setTransactionResult(TranDeviceResultType.SUCCESS);
		md.setTransactionId(transactionId);
		BigDecimal total = BigDecimal.ZERO;
		LineItemAdder lia = new LineItemAdder_2B(md);
		int n = 0;
		for(int i = 0; i < items; i++) {
			String desc;
			BigDecimal price;
			int quantity;
			int duration;
			switch(i % 3) {
				case 0:
					desc = "0025";
					duration = 2;
					price = new BigDecimal(110);
					quantity = 1;
					break;
				case 1:
					desc = "0033";
					duration = 3;
					price = new BigDecimal(115);
					quantity = 2;
					break;
				case 2:
				default:
					desc = "0027";
					duration = 7;
					price = new BigDecimal(125);
					quantity = 3;
					break;
			}
			lia.addLineItem(1, 200, desc, duration, price, quantity);
			total = total.add(price.multiply(BigDecimal.valueOf(quantity)));
			n += quantity;
		}
		if(convFee) {
			BigDecimal price = new BigDecimal(7);
			md.setConvenienceFee(price.intValue());
			total = total.add(price.multiply(BigDecimal.valueOf(n)));
		}
		lia.complete();
		md.setSaleAmount(total.intValue());
		return md;
	}
	protected void eSudsLabels(String host, String deviceName) throws Exception {
		MessageData_9A6A data = new MessageData_9A6A();
		{
			MessageData_9A6A.LabelData label1 = data.addLabel();
			label1.setPortLabel("A");
			label1.setPortNumber(1);
			label1.setTopOrBottom(ESudsTopOrBottomType.BOTTOM);
		}
		{
			MessageData_9A6A.LabelData label1 = data.addLabel();
			label1.setPortLabel("B");
			label1.setPortNumber(2);
			label1.setTopOrBottom(ESudsTopOrBottomType.BOTTOM);
		}
		{
			MessageData_9A6A.LabelData label1 = data.addLabel();
			label1.setPortLabel("C");
			label1.setPortNumber(3);
			label1.setTopOrBottom(ESudsTopOrBottomType.BOTTOM);
		}
		ClientEmulator ce = new ClientEmulator(host, unifedlayerPort, deviceName, keys.get(host).get(deviceName));
		ce.startCommunication(7);
		MessageData reply;
		try {
			reply = ce.sendMessage(data);
		} finally {
			ce.stopCommunication();
		}	
	}
	protected void eSudsRoomLayer(String host, String deviceName) throws Exception {
		MessageData_9A63 data = new MessageData_9A63();
		{
		MessageData_9A63.ComponentData comp1 = data.addComponent();
		comp1.setComponentNumber(1);
		comp1.setComponentType(ComponentType.GEN2_WASHER);
		//comp1.setLabel("A");
		comp1.setManufacturer("Maytag");
		comp1.setModelCode(7);
		comp1.setFirmwareVersion(1);
		comp1.setWasherDryerType(WasherDryerType.GEN2_WASHER);
		MessageData_9A63.ComponentData.SupportedCycleData cycle1 = comp1.addSupportedCycle();
		cycle1.setCyclePrice(150);
		cycle1.setCycleTime(4);
		cycle1.setCycleType(1);	
		}
		{
		MessageData_9A63.ComponentData comp2 = data.addComponent();
		comp2.setComponentNumber(2);
		comp2.setComponentType(ComponentType.GEN2_DRYER);
		//comp2.setLabel("B");
		comp2.setManufacturer("Maytag");
		comp2.setModelCode(8);
		comp2.setFirmwareVersion(1);
		comp2.setWasherDryerType(WasherDryerType.GEN2_DRYER);
		MessageData_9A63.ComponentData.SupportedCycleData cycle2 = comp2.addSupportedCycle();
		cycle2.setCyclePrice(110);
		cycle2.setCycleTime(5);
		cycle2.setCycleType(40);	
		}
		{
		MessageData_9A63.ComponentData comp3 = data.addComponent();
		comp3.setComponentNumber(3);
		comp3.setComponentType(ComponentType.GEN1_WASHER);
		comp3.setLabel("C");
		comp3.setManufacturer("Speed Queen");
		comp3.setModelCode(2);
		comp3.setFirmwareVersion(1);
		comp3.setWasherDryerType(WasherDryerType.GEN1_WASHER);
		MessageData_9A63.ComponentData.SupportedCycleData cycle3 = comp3.addSupportedCycle();
		cycle3.setCyclePrice(225);
		cycle3.setCycleTime(6);
		cycle3.setCycleType(1);	
		}
		ClientEmulator ce = new ClientEmulator(host, unifedlayerPort, deviceName, keys.get(host).get(deviceName));
		ce.startCommunication(7);
		MessageData reply;
		try {
			reply = ce.sendMessage(data);
		} finally {
			ce.stopCommunication();
		}
	}
	protected void eSudsStatusChange(String host, String deviceName, ESudsRoomStatus hostStatus, int component) throws Exception {
		ClientEmulator ce = new ClientEmulator(host, unifedlayerPort, deviceName, keys.get(host).get(deviceName));
		eSudsStatusChange(ce, unifedlayerPort, hostStatus, component);
	}
	protected void eSudsStatusChange(ClientEmulator ce, int port, ESudsRoomStatus hostStatus, int component) throws Exception {
		MessageData_9A41 data = new MessageData_9A41();
		data.setStartingPortNumber(component);
		MessageData_9A41.RoomStatuData stat = data.addRoomStatu();
		stat.setBottom(hostStatus);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.startCommunication(7);
		MessageData reply;
		try {
			reply = ce.sendMessage(data);
		} finally {
			ce.stopCommunication();
		}
		
	}
	protected void eSudsAuthAndSale(String host, String deviceName, String card, char cardType, BigDecimal saleAmount, int component) throws Exception {
		eSudsAuthAndSale(host, unifedlayerPort, unifedlayerPort, deviceName, keys.get(host).get(deviceName), card, 'C', null, saleAmount, component);
	}
	protected void eSudsAuthAndSale(String host, int authPort, int salePort, String deviceName, byte[] encryptKey, String card, char cardType, BigDecimal authAmount, BigDecimal saleAmount, int component) throws Exception {
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, encryptKey);
		eSudsAuthAndSale(ce, authPort, salePort, card, cardType, null, saleAmount, component);
	}

	protected boolean sendESudsAuth(String host, int authPort, String deviceName, byte[] encryptKey, long tranId, String card, char cardType, BigDecimal authAmount) throws Exception {
		MessageData_A0 data = new MessageData_A0();
		data.setAccountData(card);
		data.setAmount((long) authAmount.intValue());
		data.setCardType(CardType.getByValue(cardType));
		data.setTransactionId(tranId++);
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, encryptKey);
		ce.setPort(authPort);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.startCommunication(7);
		MessageData reply;
		try {
			reply = ce.sendMessage(data);
		} finally {
			ce.stopCommunication();
		}
		switch(reply.getMessageType()) {
			case AUTH_RESPONSE_3_0:
				AuthResponseCodeA1 rc = ((MessageData_A1) reply).getResponseCode();
				switch(rc) {
					case APPROVED:
					case CONDITIONALLY_APPROVED:
						return true;
					default:
						log.info("Cannot process sale because auth was rejected");
						return false;
				}
			default:
				log.info("Cannot process sale because response was " + reply.getMessageType());
				return false;
		}
	}

	protected void eSudsAuthAndSale(ClientEmulator ce, int authPort, int salePort, String card, char cardType, BigDecimal authAmount, BigDecimal saleAmount, int component) throws Exception {
		if(authAmount == null) {
			if(saleAmount == null || saleAmount.signum() == 0)
				authAmount = new BigDecimal(848);
			else
				authAmount = saleAmount;
		}
		long tranId = ce.getMasterId();
		MessageData_A0 data = new MessageData_A0();
		data.setAccountData(card);
		data.setAmount((long)authAmount.intValue());
		data.setCardType(CardType.getByValue(cardType));
		data.setTransactionId(tranId++);
		ce.setPort(authPort);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.startCommunication(7);
		MessageData reply;
		try {
			reply = ce.sendMessage(data);
		} finally {
			ce.stopCommunication();
		}
		boolean doSale;
		switch(reply.getMessageType()) {
			case AUTH_RESPONSE_3_0:
				AuthResponseCodeA1 rc = ((MessageData_A1)reply).getResponseCode();
				switch(rc) {
					case APPROVED: case CONDITIONALLY_APPROVED:
						doSale = true;
						break;
					default:
						doSale = false;
						log.info("Cannot process sale because auth was rejected");
				}
				break;
			default:
				doSale = false;
				log.info("Cannot process sale because response was " + reply.getMessageType());
		}
		if(doSale) {
			MessageData_9A5E saleData = new MessageData_9A5E();
			saleData.setTransactionId(data.getTransactionId());
			MessageData_9A5E.LineItemData li = saleData.addLineItem();
			li.setComponentNumber(component);
			li.setDescription("Test item #" + component);
			switch(component) {
				case 1: case 3: li.setItem(1); break;
				case 2: li.setItem(40); break;
			}
			
			li.setPrice(saleAmount.intValue());
			li.setQuantity(1);
			li.setTopOrBottom(ESudsTopOrBottomType.BOTTOM);
			ce.setPort(salePort);
			log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
			ce.startCommunication(7);
			try {
				ce.sendMessage(saleData);
			} finally {
				ce.stopCommunication();
			}
		}
	}
	
	protected void gxAuthAndSale(String host, String deviceName, String card, char cardType, int convenienceFee) throws Exception {
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		gxAuthAndSale(ce, layer3Port, layer3Port, card, cardType, convenienceFee);
	}

	protected int kioskAuthAndSale(ClientEmulator ce, long tranId, int authPort, int salePort, EntryType entryType, String card, long authAmount, long saleAmount, String additionalData, String tranDetails) throws Exception {
		MessageData_AE data = new MessageData_AE();
		data.setAdditionalData(additionalData);
		data.setAdditionalDataType(StringUtils.isBlank(additionalData) ? AEAdditionalDataType.NO_ADDITIONAL_DATA : AEAdditionalDataType.ISIS_SMARTTAP);
		data.setAmount(authAmount);
		data.setCardType(CardType.findAll(entryType, PaymentActionType.PURCHASE).get(0));
		data.setTransactionId(tranId++);
		data.setCardReaderType(CardReaderType.GENERIC);
		((GenericCardReader) data.getCardReader()).setAccountData2(card);
		ce.setPort(authPort);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.startCommunication(7);
		MessageData reply;
		try {
			reply = ce.sendMessage(data);
		} finally {
			ce.stopCommunication();
		}
		boolean doSale;
		int authrc = -1;
		switch(reply.getMessageType()) {
			case AUTH_RESPONSE_3_2:
				AuthResponseCodeAF rc = ((MessageData_AF) reply).getResponseCode();
				switch(rc) {
					case APPROVED:
					case CONDITIONALLY_APPROVED:
						doSale = true;
						break;
					default:
						doSale = false;
						log.info("Cannot process sale because auth was rejected");
				}
				authrc = rc.getValue();
				break;
			case AUTH_RESPONSE_3_0:
				AuthResponseCodeA1 rc30 = ((MessageData_A1) reply).getResponseCode();
				switch(rc30) {
					case APPROVED:
					case CONDITIONALLY_APPROVED:
						doSale = true;
						break;
					default:
						doSale = false;
						log.info("Cannot process sale because auth was rejected");
				}
				authrc = rc30.getValue();
				break;
			default:
				doSale = false;
				log.info("Cannot process sale because response was " + reply.getMessageType());
		}
		if(doSale) {
			MessageData_A2 saleData = new MessageData_A2();
			saleData.setTransactionId(data.getTransactionId());
			saleData.setDeviceType(DeviceType.KIOSK);
			saleData.setSaleAmount(saleAmount);
			saleData.setTransactionResult(TranDeviceResultType.SUCCESS);
			MessageData_A2.BEXTransactionDetail detail = (BEXTransactionDetail)saleData.getTransactionDetailData();
			detail.setPayload(tranDetails);
			ce.setPort(salePort);
			log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
			ce.startCommunication(7);
			try {
				ce.sendMessage(saleData);
			} finally {
				ce.stopCommunication();
			}
		}
		return authrc;
	}
	protected void gxAuthAndSale(ClientEmulator ce, int authPort, int salePort, String card, char cardType, int convenienceFee) throws Exception {
		long tranId = ce.getMasterId();
		MessageData_A0 data = new MessageData_A0();
		data.setAccountData(card);
		data.setAmount(122L);
		data.setCardType(CardType.getByValue(cardType));
		data.setTransactionId(tranId++);
		ce.setPort(authPort);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.startCommunication(4);
		MessageData reply;
		try {
			reply = ce.sendMessage(data);
		} finally {
			ce.stopCommunication();
		}
		boolean doSale;
		switch(reply.getMessageType()) {
			case AUTH_RESPONSE_3_0:
				AuthResponseCodeA1 rc = ((MessageData_A1)reply).getResponseCode();
				switch(rc) {
					case APPROVED: case CONDITIONALLY_APPROVED:
						doSale = true;
						break;
					default:
						doSale = false;
						log.info("Cannot process sale because auth was rejected");
				}
				break;
			default:
				doSale = false;
				log.info("Cannot process sale because response was " + reply.getMessageType());
		}
		if(doSale) {
			MessageData_2A saleData = new MessageData_2A();
			saleData.setTransactionId(data.getTransactionId());
			saleData.setConvenienceFee(convenienceFee);
			saleData.setSaleAmount(333);
			saleData.setTransactionResult(TranDeviceResultType.SUCCESS);
			saleData.setVendByteLength((byte)0);
			MessageData_2A.NetBatch0LineItem li = (MessageData_2A.NetBatch0LineItem) saleData.addActualLineItem();
			li.setPositionNumber(66);
			//li.setReportedPrice(121);
			li.setReportedPrice(0xFFFFFF);
			li = (MessageData_2A.NetBatch0LineItem) saleData.addActualLineItem();
			li.setPositionNumber(47);
			//li.setReportedPrice(188);
			li.setReportedPrice(0xFFFFFF);
			ce.setPort(salePort);
			log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
			ce.startCommunication(4);
			try {
				ce.sendMessage(saleData);
			} finally {
				ce.stopCommunication();
			}
		}
	}

	protected void gx32AuthAndSale(ClientEmulator ce, int authPort, int salePort, String card, int convenienceFee, String additionalData) throws Exception {
		gx32AuthAndSale(ce, authPort, salePort, 4, card, convenienceFee, additionalData);
	}

	protected void gx32AuthAndSale(ClientEmulator ce, int authPort, int salePort, int protocol, String card, int convenienceFee, String additionalData) throws Exception {
		gx32AuthAndSale(ce, authPort, salePort, protocol, EntryType.ISIS, card, convenienceFee, 500, 333, false, additionalData);
	}

	protected void gx32AuthAndSale(ClientEmulator ce, int authPort, int salePort, int protocol, EntryType entryType, String card, int convenienceFee, int authAmount, int saleAmount, boolean includePrices, String additionalData) throws Exception {
		long tranId = ce.getMasterId();
		MessageData_AE data = new MessageData_AE();
		data.setAdditionalData(additionalData);
		data.setAdditionalDataType(StringUtils.isBlank(additionalData) ? AEAdditionalDataType.NO_ADDITIONAL_DATA : AEAdditionalDataType.ISIS_SMARTTAP);
		data.setAmount((long) authAmount);
		data.setCardType(CardType.findAll(entryType, PaymentActionType.PURCHASE).get(0));
		data.setTransactionId(tranId++);
		data.setCardReaderType(CardReaderType.GENERIC);
		((GenericCardReader) data.getCardReader()).setAccountData2(card);
		ce.setPort(authPort);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.startCommunication(protocol);
		MessageData reply;
		try {
			reply = ce.sendMessage(data);
		} finally {
			ce.stopCommunication();
		}
		boolean doSale;
		switch(reply.getMessageType()) {
			case AUTH_RESPONSE_3_2:
				AuthResponseCodeAF rc = ((MessageData_AF) reply).getResponseCode();
				switch(rc) {
					case APPROVED:
					case CONDITIONALLY_APPROVED:
						doSale = true;
						break;
					default:
						doSale = false;
						log.info("Cannot process sale because auth was rejected");
				}
				break;
			case AUTH_RESPONSE_3_0:
				AuthResponseCodeA1 rc30 = ((MessageData_A1) reply).getResponseCode();
				switch(rc30) {
					case APPROVED:
					case CONDITIONALLY_APPROVED:
						doSale = true;
						break;
					default:
						doSale = false;
						log.info("Cannot process sale because auth was rejected");
				}
				break;
			default:
				doSale = false;
				log.info("Cannot process sale because response was " + reply.getMessageType());
		}
		if(doSale) {
			MessageData_2A saleData = new MessageData_2A();
			saleData.setTransactionId(data.getTransactionId());
			saleData.setConvenienceFee(convenienceFee);
			saleData.setSaleAmount(saleAmount);
			saleData.setTransactionResult(TranDeviceResultType.SUCCESS);
			saleData.setVendByteLength((byte) 0);
			MessageData_2A.NetBatch0LineItem li = (MessageData_2A.NetBatch0LineItem) saleData.addActualLineItem();
			li.setPositionNumber(66);
			int price1;
			int price2;

			if(includePrices) {
				price1 = (int) (saleAmount * 0.63);
				price2 = saleAmount - price1;
			} else {
				price1 = price2 = 0xFFFFFF;
			}
			li.setReportedPrice(price1);
			li = (MessageData_2A.NetBatch0LineItem) saleData.addActualLineItem();
			li.setPositionNumber(47);
			li.setReportedPrice(price2);
			ce.setPort(salePort);
			log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
			ce.startCommunication(protocol);
			try {
				ce.sendMessage(saleData);
			} finally {
				ce.stopCommunication();
			}
		}
	}
	protected void gxEthernetInfo(String host, String deviceName) throws Exception {
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		gxEthernetInfo(ce, layer3Port);
	}

	protected void gxEthernetInfo(ClientEmulator ce, int port) throws Exception {
		byte[] modemInfo1 = "dhcp-clientip-addressip-maskIPADDRNETMASKGATEWAYDNSDhcpserver:192.168.200.1\nVersionNo:3.10\nMacaddress:00:08:00:d2:71:10\nMTU:1500bytes\nRxBytes:413770bytes\nRxPackets:3048\nRxErrors:0\nRxdropped:0\nRxOverruns:0\nRxFrame:0\nRxCompressed:0\nTxBytes:1737bytes\nTxPackets:142\nTxErrors:1\nTxdropped:0\nTx :ARM/CNXT\n:ARM9TDMI\n:32BIT\n:LITTLE-ENDIAN\n:83.76\n:CNXTCX821XX\nSYSTEMUPTIM:12:07AMUP7MIN,LOADAVERAGE:0.00,0.02,\n !"
				.getBytes();
		gxSendFile(ce, port, FileType.GX_PEEK_RESPONSE, "Modem Info", modemInfo1);
	}
	protected void gxModemInfo(String host, String deviceName) throws Exception {
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		gxModemInfo(ce, layer3Port);
	}
	protected void gxModemInfo(ClientEmulator ce, int port) throws Exception {
		byte[] modemInfo1 = "<MODEM>\rCPIN:READY\rCGMI:TELIT\rCGSN:359551031422115\rCGMR:10.00.053\rCGMM:GC864-QUAD-V2\rCCID:89014104254002410697\rBND:3\rCGATT:1\rCREG:0,1\rCGREG:0,1\rIP:172.24.98.91\rGPRS:1\rCSQ:24,0\r>"
				.getBytes();
		byte[] modemInfo2 = ByteArrayUtils
				.fromHex("3C4D4F44454D3E0D4350494E3A52454144590D43474D493A54454C49540D4347534E3A4F4B0D43474D523A30372E30322E3230330D43474D4D3A47433836342D515541440D434349443A38393031343130343231323131323439343732300D424E443A330D43474154543A310D435245473A302C310D43475245473A302C310D49503A3137322E31362E3234342E35340D475052533A310D4353513A31332C300D2020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020CA");
		byte[] modemInfo3 = ByteArrayUtils
				.fromHex("3C4D4F44454D3E0D4350494E3A52454144590D43474D493A54454C49540D4347534E3A3335373436323033303032363037350D43474D523A30372E30332E3230300D43474D4D3A47433836342D515541440D424E443A330D435245473A302C310D43475245473A302C310D43474154543A310D49503A3137322E32342E3138362E3133340D475052533A310D4353513A31322C300D2020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020CA");
		byte[] modemInfo4 = "<MODEM>\rAUTOBND:0\rBND:3\rCMEERROR:10\rCPIN:READY\rCGMI:TELIT\rCGSN:359551031441214\rCGMR:10.00.053\rCGMM:GC864-QUAD-V2\rCCID:89014104254002652124\rCIMI:310410400265212\rSTIA:0,1,10,7FFFFFFF7F0D007F3E00001F23000\rPLMNMODE:1\rCREG:0,1\rCGREG:2,1,\"07E5\",\"3356\"\rGPRS:0\rCGATT:1\rIP:172.24.120.163\rCSQ:21,0\r                                                                                                                 "
				.getBytes();
		byte[] modemInfo5 = "<MODEM>\rAUTOBND:0\rBND:3\rCMEERROR:10\rCPIN:READY\rCGMI:TELIT\rCGSN:359551031441214\rCGMR:10.00.053\rCGMM:GC864-QUAD-V2\rCCID:89014104254002652124\rCIMI:234720400265212\rSTIA:0,1,10,7FFFFFFF7F0D007F3E00001F23000\rPLMNMODE:1\rCREG:0,1\rCGREG:2,1,\"07E5\",\"3356\"\rGPRS:0\rCGATT:1\rIP:172.24.120.163\rCSQ:21,0\r                                                                                                                 "
				.getBytes();
		gxSendFile(ce, port, FileType.GX_PEEK_RESPONSE, "Modem Info", modemInfo5);
    }
	protected void gxCdmaModemInfo(String host, String deviceName) throws Exception {
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		gxCdmaModemInfo(ce, layer3Port);
	}
	protected void gxCdmaModemInfo(ClientEmulator ce, int port) throws Exception {
		gxSendFile(ce, port, FileType.GX_PEEK_RESPONSE, "Modem Info", "<MODEM>\rMEID:A1092313,21381301H\rCGMI:TELIT\rCGMM:CC864-DUAL\rCGMR:09.01.023-B021\rCGSN:270113178514031337\rCIMI:310005703214440\rCNUM:,4844028773,129\rCREG:0,1\rSGACT:1,1\rCSQ:14,99\r>".getBytes());
    }
	
    protected void gxSendFile(ClientEmulator ce, int port, FileType fileType, String fileName, byte[] fileContent) throws Exception {
		ce.setPort(port);
		ce.startCommunication(4);
		try {
			gxSendFile(ce, fileType, fileName, fileContent);
		} finally {
			ce.stopCommunication();
		}
    }

	protected void gxSendFile(ClientEmulator ce, FileType fileType, String fileName, byte[] fileContent) throws Exception {
		MessageData_92 data92 = new MessageData_92();
		MessageData_A4 dataA4 = new MessageData_A4();
		dataA4.setFileType(fileType);
		dataA4.setFileName(fileName);
		dataA4.setGroup((byte) ce.getMessageNumber());
		dataA4.setTotalBytes(fileContent.length);
		final int packetSize = 200;
		final int packets = (int) Math.ceil(fileContent.length / (double) packetSize);
		byte[] packetContent = new byte[packetSize];
		dataA4.setTotalPackets(packets);
		MessageData_A6 dataA6 = new MessageData_A6();
		dataA6.setGroup(dataA4.getGroup());
		MessageData reply;
		reply = ce.sendMessage(data92);
		switch(reply.getMessageType()) {
			case TERMINAL_UPDATE_STATUS:
				if(((MessageData_90) reply).getTerminalUpdateStatus() == 0) {
					log.info("No updates received");
				} else {
					MessageData reply2 = ce.receiveReply();
					log.info("Got update: " + reply2);
					/*
					 * MessageData_2F data2F = new MessageData_2F(); data2F.setAckedMessageNumber(reply2.getMessageNumber()); ce.transmitMessage(data2F); //TODO: send only - no receive
					 */
				}
				break;
			default:
				log.info("Different message");
		}
		reply = ce.sendMessage(dataA4);
		if(reply.getMessageType() != MessageType.FILE_XFER_START_ACK_3_0)
			throw new Exception("File Transfer Start was not ACKed");
		for(int i = 0; i < packets; i++) {
			dataA6.setPacketNum(i);
			int remaining = fileContent.length - i * packetSize;
			if(remaining < packetSize) {
				packetContent = new byte[remaining];
			}
			System.arraycopy(fileContent, i * packetSize, packetContent, 0, packetContent.length);
			dataA6.setContent(packetContent);
			reply = ce.sendMessage(dataA6);
			if(reply.getMessageType() != MessageType.FILE_XFER_ACK_3_0)
				throw new Exception("File Transfer was not ACKed");
		}
	}
	protected void cashSale(String host, int salePort, String deviceName, byte[] encryptKey, BigDecimal amount) throws Exception {
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, encryptKey);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		ce.setPort(salePort);
		ce.startCommunication(7);
		try {
			if(amount != null)
				ce.testSingleSale(ce.getMasterId(), SaleType.CASH, amount, false);
			else
				ce.testSale(ce.getMasterId(), SaleType.CASH, 3, false);
		} finally {
			ce.stopCommunication();
		}
	}
	protected void authFakeMagtek(String host, int authPort, String deviceName, byte[] encryptKey, String card, BigDecimal amount) throws Exception {
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, encryptKey);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		long tranId = ce.getMasterId();
		MessageData_AC data = new MessageData_AC();
		data.setAmount(amount.longValue() * 100);
		data.setCardReaderType(CardReaderType.MAGTEK_MAGNESAFE);
		data.setCardType(CardType.CREDIT_SWIPE);
		data.setTransactionId(tranId);
		MessageData_AC.EncryptingCardReaderImpl mtcr = (MessageData_AC.EncryptingCardReaderImpl) data.getCardReader();
		mtcr.setDecryptedLength(37);
		mtcr.setKeySerialNum(new byte[] {1,2,3,4});
		mtcr.setEncryptedData(new byte[] {1,2,3,4});
		ce.startCommunication(7);
		try {
			ce.sendMessage(data);
		} finally {
			ce.stopCommunication();
		}
	}
	protected void authAndSaleMagtek(String host, int authPort, String deviceName, DeviceType deviceType, int decryptedLength, byte[] ksn, byte[] encryptedCard, BigDecimal amount) throws Exception {
		authAndSaleMagtek(host, authPort, deviceName, deviceType, CardReaderType.MAGTEK_MAGNESAFE, decryptedLength, ksn, encryptedCard, amount);
	}

	protected void authAndSaleMagtek(String host, int authPort, String deviceName, DeviceType deviceType, CardReaderType cardReaderType, int decryptedLength, byte[] ksn, byte[] encryptedCard, BigDecimal amount) throws Exception {
		MessageData_AC data = new MessageData_AC();
		data.setAmount(amount.longValue() * 100);
		data.setCardReaderType(cardReaderType);
		data.setCardType(CardType.CREDIT_SWIPE);
		MessageData_AC.EncryptingCardReaderImpl mtcr = (MessageData_AC.EncryptingCardReaderImpl) data.getCardReader();
		mtcr.setDecryptedLength(decryptedLength);
		mtcr.setKeySerialNum(ksn);
		mtcr.setEncryptedData(encryptedCard);
		authAndSale(host, 7, authPort, authPort, deviceName, data, 3, false, deviceType);
	}

	protected void authAndSale(String host, int protocol, int authPort, int salePort, String deviceName, AuthorizeMessageData auth, int saleItems, boolean convFee, DeviceType deviceType) throws Exception {
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		long tranId = ce.getMasterId();
		auth.setTransactionId(tranId);
		ce.startCommunication(protocol);
		MessageData reply;
		try {
			reply = ce.sendMessage(auth);
		} finally {
			ce.stopCommunication();
		}
		MessageData sale;
		switch(reply.getMessageType()) {
			case AUTH_RESPONSE_4_1:
				AuthResponseTypeData artd = ((MessageData_C3) reply).getAuthResponseTypeData();
				AuthResponseType art = ((MessageData_C3) reply).getAuthResponseType();
				switch(art) {
					case AUTHORIZATION:
						AuthResponseCodeC3 rc = ((AuthorizationAuthResponse) artd).getResponseCode();
						switch(rc) {
							case SUCCESS:
							case CONDITIONAL_SUCCESS:
								sale = buildSaleC4(tranId, SaleType.ACTUAL, saleItems, convFee);
								break;
							default:
								sale = null;
								log.info("Cannot process sale because auth was rejected");
						}
						break;
					default:
						sale = null;
						log.info("Cannot process sale because auth response was " + art);
				}
				break;
			case AUTH_RESPONSE_3_0:
				MessageData_A1 replyA1 = (MessageData_A1) reply;
				AuthResponseCodeA1 a1rc = replyA1.getResponseCode();
				switch(a1rc) {
					case APPROVED:
					case CONDITIONALLY_APPROVED:
						sale = buildSaleA2(tranId, SaleType.ACTUAL, saleItems, convFee, deviceType);
						break;
					default:
						sale = null;
						log.info("Cannot process sale because auth was rejected");
				}
				break;
			case AUTH_RESPONSE_2_0:
				MessageData_60 reply60 = (MessageData_60) reply;
				AuthResponseCode60 rc60 = reply60.getResponseCode();
				switch(rc60) {
					case PASS:
						sale = buildSale2A(tranId, SaleType.ACTUAL, saleItems, convFee, deviceType);
						break;
					default:
						sale = null;
						log.info("Cannot process sale because auth was rejected");
				}
				break;
			default:
				sale = null;
				log.info("Cannot process sale because response was " + reply.getMessageType());
		}
		if(sale != null) {
			ce.setPort(salePort);
			log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
			ce.startCommunication(protocol);
			try {
				ce.sendMessage(sale);
			} finally {
				ce.stopCommunication();
			}
		}
	}
	protected void sendIntendedSale(String host, int salePort, String deviceName, byte[] encryptKey, long tranId, int amount) throws Exception {
		ClientEmulator ce = new ClientEmulator(host, salePort, deviceName, encryptKey);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		MessageData_9A60 data = new MessageData_9A60();
		data.setTransactionId(tranId);
		MessageData_9A60.LineItemData li = data.addLineItem();
		li.setComponentNumber(1);
		li.setDescription("Test item #1");
		li.setItem(2);
		li.setPrice(amount);
		li.setQuantity(1);
		li.setTopOrBottom(ESudsTopOrBottomType.BOTTOM);
		ce.startCommunication(7);
		try {
			ce.sendMessage(data);
		} finally {
			ce.stopCommunication();
		}
	}
	protected void sendESudsSale(String host, int salePort, String deviceName, byte[] encryptKey, long tranId, int amount) throws Exception {
		ClientEmulator ce = new ClientEmulator(host, salePort, deviceName, encryptKey);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		MessageData_9A5E data = new MessageData_9A5E();
		data.setTransactionId(tranId);
		MessageData_9A5E.LineItemData li = data.addLineItem();
		li.setComponentNumber(1);
		li.setDescription("Test item #1");
		li.setItem(1);
		li.setPrice(amount);
		li.setQuantity(1);
		li.setTopOrBottom(ESudsTopOrBottomType.BOTTOM);
		ce.startCommunication(7);
		try {
			ce.sendMessage(data);
		} finally {
			ce.stopCommunication();
		}
	}
	protected void sendUSR(String host, int salePort, String deviceName, byte[] encryptKey, int times) throws Exception {
		ClientEmulator ce = new ClientEmulator(host, salePort, deviceName, encryptKey);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		MessageData_CA data = new MessageData_CA();
		data.setRequestType(GenericRequestType.UPDATE_STATUS_REQUEST);
		ce.startCommunication(7);
		try {
			for(int i = 0; i < times; i++)
				ce.sendMessage(data);
		} finally {
			ce.stopCommunication();
		}
	}

	protected void doEdgeSession(String host, int salePort, String deviceName, byte[] encryptKey, MessageData... uploads) throws Exception {
		ClientEmulator ce = new ClientEmulator(host, salePort, deviceName, encryptKey);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		MessageData data;
		MessageData_CA dataCA = new MessageData_CA();
		dataCA.setRequestType(GenericRequestType.UPDATE_STATUS_REQUEST);
		if(uploads != null && uploads.length > 0)
			((UpdateStatusRequest) dataCA.getRequestTypeData()).setStatusCodeBitmap(0x01);
		data = dataCA;
		MessageData_CB dataCB = new MessageData_CB();
		dataCB.setResultCode(GenericResponseResultCode.OKAY);		
		ce.startCommunication(7);
		int i = 0;
		try {
			while(true) {
				MessageData reply = ce.sendMessage(data);
				switch(reply.getMessageType()) {
					case GENERIC_RESPONSE_4_1:
						MessageData_CB replyCB = (MessageData_CB)reply;
						switch(replyCB.getServerActionCode()) {
							case NO_ACTION:
								if(uploads == null || uploads.length <= i)
									return;
								data = uploads[i++];
								if(uploads.length >= i)
									((UpdateStatusRequest) dataCA.getRequestTypeData()).setStatusCodeBitmap(0x00);
								break;
							case UPLOAD_PROPERTY_VALUE_LIST:
								data = createPropListReply(((PropertyIndexListAction) replyCB.getServerActionCodeData()).getPropertyIndexes());
								break;
							case SEND_UPDATE_STATUS_REQUEST:
							case SEND_UPDATE_STATUS_REQUEST_IMMEDIATE:
								data = dataCA;
								break;
							case PROCESS_PROP_LIST:
								Map<Integer, String> propertyValues = ((PropertyValueListAction) replyCB.getServerActionCodeData()).getPropertyValues();
								String newDeviceName = propertyValues.get(51);
								if(newDeviceName != null) {
									ce.setEvNumber(newDeviceName);
									log.info("Got new DeviceName: " + newDeviceName);
								}
								String encryptionKeyHex = propertyValues.get(52);
								if(encryptionKeyHex != null) {
									ce.setEncryptionKey(ByteArrayUtils.fromHex(encryptionKeyHex));
									log.info("Got new EncryptionKey: " + encryptionKeyHex);
								}
								break;
							default:
								dataCB.setClientActionCode(GenericResponseClientActionCode.UPDATE_STATUS_REQUEST);
								data = dataCB;
						}
						break;
					case GENERIC_REQUEST_4_1:
						MessageData_CA replyCA = (MessageData_CA)reply;
						switch(replyCA.getRequestType()) {
							case PROPERTY_LIST_REQUEST:
								data = createPropListReply(((MessageData_CA.PropertyListRequest)replyCA.getRequestTypeData()).getPropertyIndexes());
								break;
							default:
								dataCB.setClientActionCode(GenericResponseClientActionCode.UPDATE_STATUS_REQUEST);
								data = dataCB;
						}
						break;
					case FILE_XFER_START_4_1:
						MessageData_C8 replyC8 = (MessageData_C8) reply;
						log.info("Start File Transfer: " + replyC8.getFileType());
						dataCB.setClientActionCode(GenericResponseClientActionCode.NO_ACTION);
						data = dataCB;
						break;
					case FILE_XFER_4_1:
						MessageData_C9 replyC9 = (MessageData_C9) reply;
						log.info("Got File Transfer Data: " + new String(replyC9.getContent()));
						dataCB.setClientActionCode(GenericResponseClientActionCode.NO_ACTION);
						data = dataCB;
						break;
					case SHORT_FILE_XFER_4_1:
						MessageData_C7 replyC7 = (MessageData_C7) reply;
						log.info("Got File Transfer Data: " + new String(replyC7.getContent()));
						dataCB.setClientActionCode(GenericResponseClientActionCode.NO_ACTION);
						data = dataCB;
						break;
				}
			}
		} finally {
			ce.stopCommunication();
		}
	}

	protected MessageData createPropListReply(final Set<Integer> propList) {
		ByteBuffer buffer = ByteBuffer.allocate(1024);
		EncodingAppendable appendTo = new EncodingAppendable(ProcessingConstants.US_ASCII_CHARSET, buffer);
		try {
			MessageDataUtils.appendEfficientPropertyValueList(appendTo, new AbstractLookupMap<Integer, String>() {
				@Override
				public String get(Object key) {
					if(key instanceof Integer)
						return getPropertyValue((Integer)key);
					else
						return null;
				}
				@Override
				public Set<Integer> keySet() {
					return propList;
				}				
			}, false);
		} catch(IOException e) {
			throw new UndeclaredThrowableException(e);
		}
		buffer.flip();
		byte[] content = new byte[buffer.remaining()];
		buffer.get(content);
		MessageData_C7 dataC7 = new MessageData_C7();
		dataC7.setContent(content);
		dataC7.setCreationTime(Calendar.getInstance());
		dataC7.setFileType(FileType.PROPERTY_LIST);
		return dataC7;
	}
	protected String getPropertyValue(int propIndex) {
		switch(propIndex) {
			case 300:
				return "29";
			case 301:
				return "1";
		}
		return null;
	}
	protected void sendQueueTimingCheck(String host, int port, String deviceName, byte[] encryptKey, int times) throws Exception {
		ClientEmulator ce = new ClientEmulator(host, port, deviceName, encryptKey);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		MessageData_0007 data = new MessageData_0007();
		data.setTimeout(40000);
		String[] authorities = {
				"aramark",
				"authority_iso8583_elavon",
				"authority_iso8583_fhms_paymentech",
				"blackboard",
				"internal",
				"posgateway",
				"posgateway_aquafill",
		};
		String[] queues = new String[authorities.length * 3 - 1];
		for(int i = 0; i < authorities.length; i++) {
			queues[i*3] = "usat.inbound.message.auth";
			queues[i*3 + 1] = "usat.authority.realtime." + authorities[i];
			if(i + 1 < authorities.length)
				queues[i * 3 + 2] = null; // "netlayer-" + host + "-" + port + ".outbound";
		}
		for(String qn : queues)
			data.addQueue().setQueueName(qn);
		ce.startCommunication(1);
		try {
			for(int i = 0; i < times; i++)
				ce.sendMessage(data);
		} finally {
			ce.stopCommunication();
		}
	}
	protected void sendPing(String host, int salePort, String deviceName, byte[] encryptKey, int times) throws Exception {
		ClientEmulator ce = new ClientEmulator(host, salePort, deviceName, encryptKey);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		MessageData_Ping data = new MessageData_Ping();
		ce.startCommunication(0);
		try {
			for(int i = 0; i < times; i++) {
				ce.msg = (int)(256 * Math.random());
				ce.sendMessage(data);
			}
		} finally {
			ce.stopCommunication();
		}
	}
	
	@Test
	public void junit_HessianAuthAndSaleCoupon_ecc() throws Exception {
		EC2ServiceAPI ec = getEC2Service("ecc");
		long tranId = System.currentTimeMillis() / 1000;
		long amount = 250;
		HttpRequester requester = new HttpRequester();
		String type="INSTANT";
		String url = "https://api-qa.vsm2m.net/2.0/coupon/bulk/99?amount=1&type=" + type.toUpperCase();
		String coupon;
		try (BufferedReader result = new BufferedReader(requester.get(url, null, new HttpRequester.BasicAuthenticator("usatqcuser", "MmFyUpjOmXkz")))) {
			String header = result.readLine();
			assert "\"Coupon Code\"".equals(header) : "Unrecognized header response from coupon server: " + header;
			String line = result.readLine();
			assert line != null && line.startsWith("\"") && line.endsWith("\"") : "Unrecognized data from coupon server: " + line;
			coupon = line.substring(1, line.length() - 1);
		}
		log.info("Using coupon: " + coupon);
		String cardData = "+"+coupon; // LuhnUtils.fillInTrackData(LuhnUtils.generateCard("41", 16)); // "4120082045889289=1809139965"; //"4197749956184052=1809227955";
															// //";4143464493714065=1809768709?"; //
		String entryType = "N";
		String tranResult = "S";
		String tranDetails = "A0|200|40|2|Ping pongs|200|85|2|Yo-yos";
		String attributes = null;
		log.info("Sending Plain Auth for coupon "+cardData);
		
		String deviceSerialCd= "K3VS15107501";
		String username = "couponTest1";
		String password = "SUvuY@GwF7a*rT+QVHhH";
		EC2AuthResponse response = ec.authPlain(username,password, deviceSerialCd, tranId, amount, cardData, entryType, attributes);
		
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		if(response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED) {
			log.info(message);
			EC2Response batchResponse = ec.batch(username, password, deviceSerialCd, tranId, amount, tranResult, tranDetails, attributes);
			if(batchResponse == null)
				throw new Exception("Received null response");
			message = getBasicResponseMessage(batchResponse);
			if(batchResponse.getReturnCode() == EportConnectSOAPTest.RES_OK)
				log.info(message);
			else
				throw new Exception(message);
		} else
			throw new Exception(message);
	}
	
	@Test
	public void junit_HessianTokenizePrepaid_int() throws Exception {
		//use this method to do tokenize to get the token
		EC2ServiceAPI ec = getEC2Service("int");
		long tranId = System.currentTimeMillis() / 1000;
		//String cardNumber = "6396212009338376453";
		//String securityCd = "9668";
		//String cardNumber = "6396212009613834846";
		//String securityCd = "9596";
		//String cardNumber = "6396212009306917429";
		//String securityCd = "5794";
		String cardNumber = "6396212009867572373";
		String securityCd = "7810";
		String postalCd = "19355";
		String expDate = "2012";
		log.info("Sending Tokenize with card '" + cardNumber + "'");
		String attributes = null;
		EC2TokenResponse response = ec.tokenizePlain(null, null, "V3-1-USD", tranId, cardNumber, expDate, securityCd, null, postalCd, null, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);// 10A797E5F5D96A78529A2D2583E73347
	}
	
	@Test
	public void junit_authAndSaleVAS_int() throws Exception {
		//This is for the demo device in integration to do C2 with nfc payload
		String deviceName = "TD001396";
		BigDecimal amount = BigDecimal.valueOf(100);
		String unencryptedTagFlag="U|";//unencrypted, currently supported
		String encryptedTagFlag="E|";//encrypted, @TODO
		long cardId = 125855;
		String token = "8BD29F182DE29848E5A02FAE749DA545";//6396212006130497115=2012003596635, 9912, 125855 -- registered under yhe@usatech.com 12/01/2016 2012 exp
		StringBuilder sb = new StringBuilder();
		sb.append(unencryptedTagFlag).append(cardId).append('|').append(token);
		String cardData = sb.toString();

		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));

		MessageData_C2 dataC2 = new MessageData_C2();
		dataC2.setAmount(amount);
		dataC2.setCardReaderType(CardReaderType.GENERIC);//byte 0
		dataC2.setEntryType(EntryType.VAS);//'V'
		dataC2.setTransactionId(ce.getMasterId());
		dataC2.setValidationData(cardData);
		GenericTracksCardReader acr = (GenericTracksCardReader) dataC2.getCardReader();
		//acr.setAccountData1("6396212006130497115");
		//acr.setRawData("6396212009972335492".getBytes());
		MessageData reply;
		ce.startCommunication(7);
		try {
			reply = ce.sendMessage(dataC2);
		} finally {
			ce.stopCommunication();
		}
		if(reply != null && reply.getMessageType() == MessageType.AUTH_RESPONSE_4_1) {
			MessageData_C3 replyC3 = (MessageData_C3) reply;
			log.info("response:"+replyC3);
		}
	}
	
	@Test
	public void junit_authAndSaleVASEncryptedReader_int() throws Exception {
		//This is for the demo device in integration to do C2 with nfc payload
		String deviceName = "TD001396";
		BigDecimal amount = BigDecimal.valueOf(100);
		String unencryptedTagFlag="U|";//unencrypted, currently supported
		String encryptedTagFlag="E|";//encrypted, @TODO
		long cardId = 125855;
		String token = "8BD29F182DE29848E5A02FAE749DA545";//6396212006130497115=2012003596635, 9912, 125855 -- registered under yhe@usatech.com 12/01/2016 2012 exp
		StringBuilder sb = new StringBuilder();
		sb.append(unencryptedTagFlag).append(cardId).append('|').append(token);
		String cardData = sb.toString();

		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));

		MessageData_C2 dataC2 = new MessageData_C2();
		dataC2.setAmount(amount);
		dataC2.setCardReaderType(CardReaderType.IDTECH_VENDX_PARSED_3DES);//byte 0
		dataC2.setEntryType(EntryType.VAS);//'V'
		dataC2.setTransactionId(ce.getMasterId());
		dataC2.setValidationData(cardData);
		EncryptingTracksCardReader acr = (EncryptingTracksCardReader) dataC2.getCardReader();
		acr.setKeySerialNum("xyz".getBytes());//dummy
		acr.setEncryptedData1("6396212006130497115".getBytes());//dummy
		MessageData reply;
		ce.startCommunication(7);
		try {
			reply = ce.sendMessage(dataC2);
		} finally {
			ce.stopCommunication();
		}
		if(reply != null && reply.getMessageType() == MessageType.AUTH_RESPONSE_4_1) {
			MessageData_C3 replyC3 = (MessageData_C3) reply;
			log.info("response:"+replyC3);
		}
	}
	
	@Test
	public void junit_authAndSaleVASAlex_int() throws Exception {
		//This is for the demo device in integration to do C2 with nfc payload
		String deviceName = "TD001759";
		BigDecimal amount = BigDecimal.valueOf(100);
		String unencryptedTagFlag="U|";//unencrypted, currently supported
		String encryptedTagFlag="E|";//encrypted, @TODO
		long cardId = 125855;
		String token = "8BD29F182DE29848E5A02FAE749DA545";//6396212006130497115=2012003596635, 9912, 125855 -- registered under yhe@usatech.com 12/01/2016 2012 exp
		StringBuilder sb = new StringBuilder();
		sb.append(unencryptedTagFlag).append(cardId).append('|').append(token);
		String cardData = sb.toString();

		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));

		MessageData_C2 dataC2 = new MessageData_C2();
		dataC2.setAmount(amount);
		dataC2.setCardReaderType(CardReaderType.GENERIC);//byte 0
		dataC2.setEntryType(EntryType.VAS);//'V'
		dataC2.setTransactionId(ce.getMasterId());
		dataC2.setValidationData(cardData);
		GenericTracksCardReader acr = (GenericTracksCardReader) dataC2.getCardReader();
		//acr.setAccountData1("6396212006130497115");
		//acr.setRawData("6396212009972335492".getBytes());
		MessageData reply;
		ce.startCommunication(7);
		try {
			reply = ce.sendMessage(dataC2);
		} finally {
			ce.stopCommunication();
		}
		if(reply != null && reply.getMessageType() == MessageType.AUTH_RESPONSE_4_1) {
			MessageData_C3 replyC3 = (MessageData_C3) reply;
			log.info("response:"+replyC3);
		}
	}
	
	@Test
	public void junit_Tandem_ClassB_Cert_2017() throws Exception {
		
		String AMEX = ";371449635398431=20121015432112345678?";
		String AMEX_MANUAL = "371449635398431|2012||||";
		String CHASENET = ";4011361100000012=200810110000000?";
		String CHASENET_MANUAL = "4011361100000012|2008||||";
		String DINERS = ";36438999960016=20121015432112345601?";	
		String DISCOVER = ";6011000995500000=20121015432112345678?";
		String DISCOVER_MANUAL = "6011000995500000|2012||||";		
		String DISCOVER_CUP = ";6221261111113245=20121011000012312300?";	
		String JCB = ";3566002020140006=20121015432112345678?";			
		String MASTERCARD = ";5454545454545454=20121015432112345601?";
		String MASTERCARD_MANUAL = "5454545454545454|2012||||";
		String MASTERCARD_COM = ";5132850000000008=20121015432112345678?";
		String VISA_ENGLISH = ";4788250000028291=20121015432112345601?"; 
		String VISA_ENGLISH_MANUAL = "4788250000028291|2012|111||12345|"; 		
		String VISA_COMMERCIAL = ";4159280000000009=20121015432112345678?";		
		String VISA_ERROR = "4788999999999999|2012||||";
		
		String hessianDevice = "K3MTB000001";
		String hessianUserName = "testaccount3";
				
		long tranId = System.currentTimeMillis() / 1000;
		
		//String deviceName = "EV100505"; //manual keyed
		String deviceName = "TD000065"; //0021";
		ClientEmulator ce = new ClientEmulator(host, authPort, deviceName, keys.get(host).get(deviceName));

		//System.out.println(StringUtils.toHex("6335976501238372"));
		//MSR
		//Section A Sale Auth Reversal Void Token
		//#1 Chasenet
		////authAndSale(ce, authPort, salePort, CHASENET_MANUAL, EntryType.MANUAL, BigDecimal.valueOf(2500), BigDecimal.valueOf(2500)); 
		//#2 VISA RFID
		////authOnly(ce, authPort, VISA_ENGLISH, EntryType.CONTACTLESS, BigDecimal.valueOf(3500)); 
		
		//#3 auth and cancel visa TODO ?
		////charge(ce, authPort, VISA_ENGLISH, EntryType.SWIPE, BigDecimal.valueOf(9500),0,0,true,0); 
		
		//#4a and 4b visa sale and reverse
		////charge(ce, authPort, VISA_ENGLISH, EntryType.SWIPE, BigDecimal.valueOf(326),0,0,true,0); 

		//#5 We don't support tokens through CHASE so this one doesn't need to be run
		//#6 This is a sale using a token. Per instructions we have to run it without the token since we don't support tokens  
		////authAndSale(ce, authPort, salePort, VISA_ENGLISH_MANUAL, EntryType.MANUAL, BigDecimal.valueOf(500), BigDecimal.valueOf(500));
		//#7 Visa swipe sale
		////authAndSale(ce, authPort, salePort, VISA_ENGLISH, EntryType.SWIPE, BigDecimal.valueOf(327), BigDecimal.valueOf(327)); 
		
		//#8 visa swipe auth with different features
		//hessianAuth(null, hessianUserName, "", hessianDevice, tranId++, VISA_ENGLISH, EntryType.SWIPE, 0l, "AVS=33611\nCVD=111\n");
		////authOnly(ce, authPort, VISA_ENGLISH, EntryType.SWIPE, BigDecimal.valueOf(0));
		
		//#9 Visa manual sale
		////authAndSale(ce, authPort, salePort, VISA_ENGLISH_MANUAL, EntryType.MANUAL, BigDecimal.valueOf(963), BigDecimal.valueOf(963)); 		
		//#10 visa manual auth with features  
		//hessianAuth(null,hessianUserName, "", hessianDevice, tranId++, VISA_ENGLISH, EntryType.MANUAL, 767l, "AVS=12345\nCVD=111\n");
		////authOnly(ce, authPort, VISA_ENGLISH_MANUAL, EntryType.MANUAL, BigDecimal.valueOf(767)); 
		//#11 visa manual auth with token. We don't support tokens so we run it without the token  
		//hessianAuth(null,hessianUserName, "", hessianDevice, tranId++, VISA_ENGLISH, EntryType.MANUAL, 2100l, "AVS=12345\nCVD=111\n"); 
		////authOnly(ce, authPort, VISA_ENGLISH_MANUAL, EntryType.MANUAL, BigDecimal.valueOf(2100));	  
		//#12 Visa manual sale
		////authAndSale(ce, authPort, salePort, VISA_ENGLISH_MANUAL, EntryType.MANUAL, BigDecimal.valueOf(2700), BigDecimal.valueOf(2700)); 	
	  //#13 VISA SWIPE AUTH	
		////authOnly(ce, authPort, VISA_ENGLISH, EntryType.SWIPE, BigDecimal.valueOf(300)); 	//Times out socket read
		
		//#14 Don't support Balance Inquiry? 
		//#16 Don't support Balance Inquiry?
		//#17 Visa Commercial Swipe sale
		//hessianAuthAndSale(null,hessianUserName, "",  hessianDevice, tranId++, VISA_COMMERCIAL, EntryType.SWIPE, 1300l, 1300l,null, null,"Cust Ref#=7777777\nTax Flag=0\nTax Amt=0\nDestination Zip=22222\n",false);
		////authAndSale(ce, authPort, salePort, VISA_COMMERCIAL, EntryType.SWIPE, BigDecimal.valueOf(1300), BigDecimal.valueOf(1300)); 
		
		//18a b MC manual sale and reversal 
		////charge(ce, authPort, MASTERCARD_MANUAL, EntryType.MANUAL, BigDecimal.valueOf(3900),0,0,true,0); 
	  //#19a and 19b mc rfid sale and reverse
		////charge(ce, authPort, MASTERCARD, EntryType.CONTACTLESS, BigDecimal.valueOf(9600),0,0,true,0); 

		//#20 a b auth and reverse with token. We don't support tokens with CHASE per instructions run it without token
		////charge(ce, authPort, MASTERCARD_MANUAL, EntryType.MANUAL, BigDecimal.valueOf(3900),0,0,true,0); 
		//#21 MC swipe sale
		////authAndSale(ce, authPort, salePort, MASTERCARD, EntryType.SWIPE, BigDecimal.valueOf(965), BigDecimal.valueOf(965)); 	

		//#22 MC manual sale with token. We don't support tokens so run it without token
		////authAndSale(ce, authPort, salePort, MASTERCARD_MANUAL, EntryType.MANUAL, BigDecimal.valueOf(2600), BigDecimal.valueOf(2600)); 	
		//#23 mc manual auth with features
		//hessianAuth(null,hessianUserName, "",  hessianDevice, tranId++, MASTERCARD, EntryType.MANUAL, 864l, "AVS=76522\nCVD=222\n");
		////authOnly(ce, authPort, MASTERCARD_MANUAL, EntryType.MANUAL, BigDecimal.valueOf(864)); 		
		//#24 mc manual sale with features
		//hessianAuthAndSale(null,hessianUserName, "",  hessianDevice, tranId++, MASTERCARD, EntryType.MANUAL, 2800l, 2800l,null, null,"AVS=22222\nCVD=444\n",false);
		////authAndSale(ce, authPort, salePort, MASTERCARD_MANUAL, EntryType.MANUAL, BigDecimal.valueOf(2800), BigDecimal.valueOf(2800)); 		
		//#25 Token only request. We don't support tokens through CHASE so skip this one
		//#26 MC swipe sale
		////authAndSale(ce, authPort, salePort, MASTERCARD, EntryType.SWIPE, BigDecimal.valueOf(15500), BigDecimal.valueOf(15500)); //timeout			

		//#27 MC zero auth features
		//hessianAuth(null,hessianUserName, "",  hessianDevice, tranId++, MASTERCARD, EntryType.MANUAL, 0l, "AVS=37602\n");
		////authOnly(ce, authPort, MASTERCARD_MANUAL, EntryType.MANUAL, BigDecimal.valueOf(0));		
		//#28 Don't do Balance Inquiry?
		//#29 MC Commercial swipe sale
		//hessianAuthAndSale(null,hessianUserName, "",  hessianDevice, tranId++, MASTERCARD_COM, EntryType.SWIPE, 2700l, 2700l,null, null,"Cust Ref#=E5454R4857\nTax Flag=1\nTax Amt=700\nDestination Zip=22222\n",false);
		////authAndSale(ce, authPort, salePort, MASTERCARD_COM, EntryType.SWIPE, BigDecimal.valueOf(2700), BigDecimal.valueOf(2700)); 	  
	  
		//#30 MC swipe sale	
		////authAndSale(ce, authPort, salePort, MASTERCARD, EntryType.SWIPE, BigDecimal.valueOf(1101), BigDecimal.valueOf(1101)); 		

		//#31 MC Commercial swipe sale 
		//hessianAuthAndSale(null,hessianUserName, "",  hessianDevice, tranId++, MASTERCARD_COM, EntryType.SWIPE, 1500l, 1500l,null, null,"Cust Ref#=11112222333344445\nTax Flag=2\nTax Amt=0\nDestination Zip=55555\n",false);
		////authAndSale(ce, authPort, salePort, MASTERCARD_COM, EntryType.SWIPE, BigDecimal.valueOf(1500), BigDecimal.valueOf(1500)); 
		
		//#32 SAFETECH?
	  //#33 AMEX SWIPE AUTH
		////authOnly(ce, authPort, AMEX, EntryType.SWIPE, BigDecimal.valueOf(441));

	  //#34a and 35b amex swipe sale and reverse
		////charge(ce, authPort, AMEX, EntryType.SWIPE, BigDecimal.valueOf(767),0,0,true,0);	
		
		//#36 AMEX sale using token. We don't support token with CHASE so run it without the token
		//hessianAuthAndSale(null,hessianUserName, "",  hessianDevice, tranId++, AMEX, EntryType.MANUAL, 2300l, 2300l,null, null,"AVS=T0E1S2\nCVD=111\nn",false);
		////authAndSale(ce, authPort, salePort, AMEX_MANUAL, EntryType.MANUAL, BigDecimal.valueOf(2300), BigDecimal.valueOf(2300)); 			
		//#37 Don't do Balance Inquiry?
	  //#38 amex rfid sale
		////authAndSale(ce, authPort, salePort, AMEX, EntryType.CONTACTLESS, BigDecimal.valueOf(3600), BigDecimal.valueOf(3600)); 		
		
		//#39 Token only request. We don't support tokens through CHASE so skip it
	  //#40 DISCOVER SWIPE AUTH	
		////authOnly(ce, authPort, DISCOVER, EntryType.SWIPE, BigDecimal.valueOf(634)); 	
		
		//#41 Don't do balance inquiry
		//#42 discover auth with features
		//hessianAuth(null,hessianUserName, "",  hessianDevice, tranId++, DISCOVER, EntryType.MANUAL, 1200l, "AVS=76521\nCVD=Bypassed\n");
		////authOnly(ce, authPort, DISCOVER_MANUAL, EntryType.MANUAL, BigDecimal.valueOf(1200));		
	  //#43 Discover rfid sale
		////authAndSale(ce, authPort, salePort, DISCOVER, EntryType.CONTACTLESS, BigDecimal.valueOf(970), BigDecimal.valueOf(970)); 	

	  //#44 DISCOVER SWIPE AUTH
		////authOnly(ce, authPort, DISCOVER, EntryType.SWIPE, BigDecimal.valueOf(301));	//this one makes the socket timeout		
		
		//#45 TODO
		////authOnly(ce, authPort, DISCOVER_MANUAL, EntryType.MANUAL, BigDecimal.valueOf(1900));
		//#46 Token only request. We don't support tokens with CHASE so skip it 
	  //#47a and 47b discover swipe sale and reverse
		////charge(ce, authPort, DISCOVER, EntryType.SWIPE, BigDecimal.valueOf(634),0,0,true,0);			

		//#48 Token SAFETECH skip this one
		//#49 CUP manual sale
		//TODO WHAT is CID = 123?
		//authAndSale(ce, authPort, salePort, DISCOVER_CUP, EntryType.MANUAL, BigDecimal.valueOf(2300), BigDecimal.valueOf(2300)); 		
		//#50 DINERS CLUB SWIPE AUTH 
		////authOnly(ce, authPort, DINERS, EntryType.SWIPE, BigDecimal.valueOf(2400)); 		
	  
	  //#51 JCB swipe sale	
		////authAndSale(ce, authPort, salePort, JCB, EntryType.SWIPE, BigDecimal.valueOf(2500), BigDecimal.valueOf(2500)); 	
		
		//#52 JCB sale using token. per instructions run it without the token
		////authAndSale(ce, authPort, salePort, JCB, EntryType.SWIPE, BigDecimal.valueOf(8800), BigDecimal.valueOf(8800));
		
		//Section B Refunds and Returns
		//This does not apply to USA Tech EMV certification
		
		//Section C Prior Auth Force Sale
		//This does not apply to USA Tech EMV certification
		
		//Section D IIAS/FSA Card Data
		//This does not apply to USA Tech EMV certification			
		
		//SECTION E Error Conditions
	  //#1 Visa swipe sale
		////authAndSale(ce, authPort, salePort, VISA_ENGLISH, EntryType.SWIPE, BigDecimal.valueOf(1101), BigDecimal.valueOf(1101)); 
		//#2 and 3 do not apply
	  //#4 Visa manual auth
		////authOnly(ce, authPort, MASTERCARD_MANUAL, EntryType.MANUAL, BigDecimal.valueOf(2503)); 
	  //#5 Visa manual sale
		////authAndSale(ce, authPort, salePort, VISA_ERROR, EntryType.MANUAL, BigDecimal.valueOf(2500), BigDecimal.valueOf(2500)); 	
		
	}
}
