package com.usatech.test;

import java.nio.ByteBuffer;

import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
/**
 * 8E initialization v3.0 -->8F
 * Note: device name and serial number must be in db, otherwise no response.
 * After successful message, the encryption key for the device is changed in db.
 * @author yhe
 *
 */
public class N8EMessage extends LegacyMessage {
	protected int deviceType;
	protected String uniqueDeviceSerialNumber;
	
	public N8EMessage() {
		super();
		dataType=0x8E;
		deviceType=1;
		uniqueDeviceSerialNumber="G506028100";
	}

	@Override
	public ReRixMessage createMessage() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		ProcessingUtils.writeByteInt(bb, deviceType);
		ProcessingUtils.writeString(bb, uniqueDeviceSerialNumber, ProcessingConstants.US_ASCII_CHARSET);
		return writeMessageEnd(bb);
	}

	@Override
	public String readResponseString(ReRixMessage msg) throws InvalidByteValueException {
		return readResponseString8F(msg, uniqueDeviceSerialNumber.length());
	}

	public int getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(int deviceType) {
		this.deviceType = deviceType;
	}

	public String getUniqueDeviceSerialNumber() {
		return uniqueDeviceSerialNumber;
	}

	public void setUniqueDeviceSerialNumber(String uniqueDeviceSerialNumber) {
		this.uniqueDeviceSerialNumber = uniqueDeviceSerialNumber;
	}
	
}
