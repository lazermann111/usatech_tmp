package com.usatech.test;

import java.nio.ByteBuffer;

import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.networklayer.ReRixMessage;

public class AppLayerHealthCheck extends BaseMessage {
	protected int dataType2;
	protected int bitmap;
	public AppLayerHealthCheck() {
		super();
		dataType=0x00;
		dataType2=0x06;
	}

	@Override
	public ReRixMessage createMessage() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		ProcessingUtils.writeShortInt(bb, bitmap);
		return writeMessageEnd(bb);
	}
	
	@Override
	public void writeMessageHeader(ByteBuffer bb){
		ProcessingUtils.writeByteInt(bb, messageNumber);
		ProcessingUtils.writeByteInt(bb, dataType);
		ProcessingUtils.writeByteInt(bb, dataType2);
	}
	
	@Override
	public String getMessageTypeName(){
		byte[] dataTypes=new byte[2];
		dataTypes[0]=(byte)dataType;
		dataTypes[1]=(byte)dataType2;
		return MessageType.getMessageDescriptionSafely(dataTypes);
	}

	public int getBitmap() {
		return bitmap;
	}

	public void setBitmap(int bitmap) {
		this.bitmap = bitmap;
	}

	@Override
	public String readResponseString(ReRixMessage msg) throws InvalidByteValueException {
		// TODO Auto-generated method stub
		return null;
	}
}
