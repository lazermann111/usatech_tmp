package com.usatech.test;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.StringTokenizer;

import com.usatech.layers.common.ProcessingUtils;
/**
 * Used by 9A63
 * @author yhe
 *
 */
public class WasherDryerModelInfo extends SubMessage {
	protected int portNumber;
	protected String serialNumber;
	protected int modelCode;
	protected int factoryFirmwareVersion;
	protected int maxPrice;
	protected int washerDryerType;
	protected int numberOfSupportedCycles;
	protected ArrayList<SupportedCyclesByteBlock> supportedCyclesByteBlock=new ArrayList<SupportedCyclesByteBlock>(5); 
	
	protected class SupportedCyclesByteBlock extends SubMessage{
		protected int cycleType;
		protected int cyclePrice;//pennies
		protected int cycleTime;//minutes
		public SupportedCyclesByteBlock(StringTokenizer userInputTokens) {
			super(userInputTokens);
			cycleType=0;
			cyclePrice=150;
			cycleTime=20;
		}
		public int getCycleType() {
			return cycleType;
		}
		public void setCycleType(int cycleType) {
			this.cycleType = cycleType;
		}
		public int getCyclePrice() {
			return cyclePrice;
		}
		public void setCyclePrice(int cyclePrice) {
			this.cyclePrice = cyclePrice;
		}
		public int getCycleTime() {
			return cycleTime;
		}
		public void setCycleTime(int cycleTime) {
			this.cycleTime = cycleTime;
		}
		@Override
		public void writeMessage(ByteBuffer bb) {
			ProcessingUtils.writeByteInt(bb,cycleType);
			ProcessingUtils.writeShortInt(bb,cyclePrice);
			ProcessingUtils.writeByteInt(bb,cycleTime);
		}
	}
	public WasherDryerModelInfo(StringTokenizer userInputTokens) {
		super(userInputTokens);
		portNumber=10;
		serialNumber="1234567890";
		modelCode=10;
		factoryFirmwareVersion=1;
		maxPrice=500;
		washerDryerType='W';
		numberOfSupportedCycles=1;
	}
	
	@Override
	public void getParams()throws Exception{
		super.getParams();
		if(serialNumber.length()!=10){
			throw new IllegalArgumentException("Invalid serialNumber:"+serialNumber+". SerialNumber should be 10 bytes.");
		}
		for(int i=0; i<numberOfSupportedCycles; i++){
			SupportedCyclesByteBlock sb=new SupportedCyclesByteBlock(userInputTokens);
			sb.getParams();
			supportedCyclesByteBlock.add(sb);
		}
		
	}
	@Override
	public void writeMessage(ByteBuffer bb) {
		bb.put((byte)portNumber);
		bb.put(serialNumber.getBytes());
		ProcessingUtils.writeByteInt(bb,modelCode);
		ProcessingUtils.writeByteInt(bb,factoryFirmwareVersion);
		ProcessingUtils.writeShortInt(bb,maxPrice);
		ProcessingUtils.writeByteInt(bb,washerDryerType);
		ProcessingUtils.writeByteInt(bb,numberOfSupportedCycles);
		for(SupportedCyclesByteBlock sb: supportedCyclesByteBlock){
			sb.writeMessage(bb);
		}
	}

	public int getPortNumber() {
		return portNumber;
	}

	public void setPortNumber(int portNumber) {
		this.portNumber = portNumber;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public int getModelCode() {
		return modelCode;
	}

	public void setModelCode(int modelCode) {
		this.modelCode = modelCode;
	}

	public int getFactoryFirmwareVersion() {
		return factoryFirmwareVersion;
	}

	public void setFactoryFirmwareVersion(int factoryFirmwareVersion) {
		this.factoryFirmwareVersion = factoryFirmwareVersion;
	}

	public int getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(int maxPrice) {
		this.maxPrice = maxPrice;
	}

	public int getWasherDryerType() {
		return washerDryerType;
	}

	public void setWasherDryerType(int washerDryerType) {
		this.washerDryerType = washerDryerType;
	}

	public int getNumberOfSupportedCycles() {
		return numberOfSupportedCycles;
	}

	public void setNumberOfSupportedCycles(int numberOfSupportedCycles) {
		this.numberOfSupportedCycles = numberOfSupportedCycles;
	}

	public ArrayList<SupportedCyclesByteBlock> getSupportedCyclesByteBlock() {
		return supportedCyclesByteBlock;
	}

	public void setSupportedCyclesByteBlock(ArrayList<SupportedCyclesByteBlock> supportedCyclesByteBlock) {
		this.supportedCyclesByteBlock = supportedCyclesByteBlock;
	}

}
