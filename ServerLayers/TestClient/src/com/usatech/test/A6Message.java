package com.usatech.test;

import java.nio.ByteBuffer;

import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;

public class A6Message extends N7EMessage {

	public A6Message() {
		super();
		dataType=0xA6;
	}

	public ReRixMessage createMessage() throws Exception {
		int payLoadLen=packetPayload==null?0:packetPayload.length();
		ByteBuffer bb=ByteBuffer.allocate(5+payLoadLen);
		writeMessageHeader(bb);
		ProcessingUtils.writeByteInt(bb,fileTransferGroupNumber);
		ProcessingUtils.writeShortInt(bb,packetNumber);
		bb.put(packetPayload.getBytes());
		return writeMessageEnd(bb);
	}
	
	@Override
	public String readResponseString(ReRixMessage msg) throws InvalidByteValueException {
		ByteBuffer bb=ByteBuffer.wrap(msg.getData());
		StringBuilder sb=readResponseHeader(bb);
		printResponse(sb, "FileTransferGroupNumber", ProcessingUtils.readByteInt(bb));
		printResponse(sb, "PacketNumber", ProcessingUtils.readShortInt(bb));
		return sb.toString();
	}
}
