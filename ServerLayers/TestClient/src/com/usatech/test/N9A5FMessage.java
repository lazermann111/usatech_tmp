package com.usatech.test;

import java.nio.ByteBuffer;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;

/**
 * 9A5F eSuds Local Auth Batch -->71h
 * @author yhe
 *
 */
public class N9A5FMessage extends N9A5EMessage {
	protected Long dateAndTime=-1L;
	protected int transactionType;
	protected String creditCardMagstripe;
	public N9A5FMessage() {
		super();
		dataType2=0x5F;
		transactionType='C';
		creditCardMagstripe="";
	}
	
	@Override
	public ReRixMessage createMessage() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		ProcessingUtils.writeLongInt(bb, transactionId);
		ProcessingUtils.writeLongInt(bb, dateAndTime);
		ProcessingUtils.writeByteInt(bb, transactionType);
		ProcessingUtils.writeShortString(bb,creditCardMagstripe, ProcessingConstants.US_ASCII_CHARSET);
		ProcessingUtils.writeByteInt(bb, numberOfLineItems);
		for(ESudsLineItem lItem: lineItem){
			lItem.writeMessage(bb);
		}
		return writeMessageEnd(bb);
	}

	public int getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(int transactionType) {
		this.transactionType = transactionType;
	}

	public String getCreditCardMagstripe() {
		return creditCardMagstripe;
	}

	public void setCreditCardMagstripe(String creditCardMagstripe) {
		this.creditCardMagstripe = creditCardMagstripe;
	}

	public Long getDateAndTime() {
		return dateAndTime;
	}

	public void setDateAndTime(Long dateAndTime) {
		this.dateAndTime = dateAndTime;
	}

}
