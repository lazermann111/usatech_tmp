package com.usatech.test;

import java.nio.ByteBuffer;

import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
/**
 * 80 Kill file transfer -->81h
 * @author yhe
 *
 */
public class N80Message extends BaseMessage {
	protected int fileTransferGroupNumber;
	public N80Message() {
		super();
		dataType=0x80;
		fileTransferGroupNumber=1;
	}

	@Override
	public ReRixMessage createMessage() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		ProcessingUtils.writeByteInt(bb, fileTransferGroupNumber);
		return writeMessageEnd(bb);
	}

	@Override
	public String readResponseString(ReRixMessage msg) throws InvalidByteValueException {
		ByteBuffer bb=ByteBuffer.wrap(msg.getData());
		StringBuilder sb=readResponseHeader(bb);
		printResponse(sb, "FileTransferGroupNumber", ProcessingUtils.readByteInt(bb));
		return sb.toString();
	}

	public int getFileTransferGroupNumber() {
		return fileTransferGroupNumber;
	}

	public void setFileTransferGroupNumber(int fileTransferGroupNumber) {
		this.fileTransferGroupNumber = fileTransferGroupNumber;
	}
}
