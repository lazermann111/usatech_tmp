package com.usatech.test;

import java.nio.ByteBuffer;
import java.util.ArrayList;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
/**
 * A3 --> 71h response
 * @author yhe
 *
 */
public class A3Message extends A3MessageBase {
	protected int numberOfVends;
	protected int vendByteLength;
	protected ArrayList<VendedItemsDescriptionBlock> vendedItemBlock=new ArrayList<VendedItemsDescriptionBlock>(5) ;
	
	public A3Message(){
		super();
		numberOfVends=1;
		vendByteLength=0;
	}
	
	@Override
	public void getParams() throws Exception{
		super.getParams();
		for(int i=0; i<numberOfVends; i++){
			VendedItemsDescriptionBlock vBlock=new VendedItemsDescriptionBlock(userInputTokens);
			vBlock.getParams();
			vendedItemBlock.add(vBlock);
		}
	}
	@Override
	public ReRixMessage createMessage() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		createMessageA3(bb);
		int transactionDetail=(byte)numberOfVends|((byte)vendByteLength<<6);
		ProcessingUtils.writeByteInt(bb,transactionDetail);
		for(VendedItemsDescriptionBlock vBlock: vendedItemBlock){
			vBlock.writeMessage(bb);
		}
		return writeMessageEnd(bb);
	}
	public int getNumberOfVends() {
		return numberOfVends;
	}

	public void setNumberOfVends(int numberOfVends) {
		this.numberOfVends = numberOfVends;
	}

	public ArrayList<VendedItemsDescriptionBlock> getVendedItemBlock() {
		return vendedItemBlock;
	}

	public void setVendedItemBlock(ArrayList<VendedItemsDescriptionBlock> vendedItemBlock) {
		this.vendedItemBlock = vendedItemBlock;
	}
}
