package com.usatech.test;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;

import simple.io.Log;
import simple.io.TLVParser;
import simple.lang.InvalidByteValueException;
import simple.lang.InvalidValueException;
import simple.text.StringUtils;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.TLVTag;
import com.usatech.layers.common.util.Vend3ARParsing;
import com.usatech.layers.common.util.Vend3ARTransactionResponseData;
import com.usatech.layers.common.util.Vend3StatusCode;

public class IdtechVendXDataBlob implements CardReaderDataBlob{
	
	private static final Log log = Log.getLog();
	
	private static int HEADER_LENGTH = 14;
    private byte headerBuffer [] = new byte [HEADER_LENGTH];
    private int headerBufferCounter;

    private byte dataBuffer [] = null;
    private int dataBufferCounter;
    
    private static int CRC_BUFFER_LENGTH = 2;
    private byte crcBuffer [] = new byte [CRC_BUFFER_LENGTH];
    private int crcBufferCounter;
    private int headerDataLength;

    private static int COMMAND_OFFSET = 10;
    private static int STATUS_CODE_OFFSET = 11;
    private static int ERROR_CODE_OFFSET = 14;
    
    public static byte CMD_ACTIVATE_TRANSACTION = 0x02;
    public static byte CMD_GET_CONFIGURATION = 0x03;
    
	// this format is shown on page 137 in NEO Interface Developers Guide
	public static byte [] FAKE_ENCRYPTED_DATA_CONTACTLESS_FIELD_HEADING_DATA = { 
			0x01,																	// Attribution Contactless:bit0=1, EncryptionMode: 00 for TDES
			(byte)0xff, (byte)0xee, 0x12, 0x06, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06,	// KSN tlv
			(byte)0xff, (byte)0xee, 0x13, 0x00,										// Track 1 TLV (Magstripe Card)
			(byte)0xff, (byte)0xee, 0x14, 0x00,										// Track 2 TLV (Magstripe Card)
			0x01,																	// Clearing record present field
			(byte)0xe1, 0x01, 0x02													// Clearing record
	};
	public static byte [] FAKE_ENCRYPTED_DATA_CONTACT_FIELD_HEADING_DATA = { 
		0x00,																	// Attribution Contact:bit0=0, EncryptionMode: 00 for TDES
		(byte)0xff, (byte)0xee, 0x12, 0x06, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06,	// KSN tlv
		(byte)0xff, (byte)0xee, 0x13, 0x00,										// Track 1 TLV (Magstripe Card)
		(byte)0xff, (byte)0xee, 0x14, 0x00,										// Track 2 TLV (Magstripe Card)
		0x01,																	// Clearing record present field
		(byte)0xe1, 0x01, 0x02													// Clearing record
	};

    
    private static TLVParser TLV_PARSER = new TLVParser();
    private Map<byte [], byte[]>parseMap = null;
    
    private VendXEMVResponseData emvData;
    
    private EntryType entryType;
    private CardReaderType cardReaderType;
	private byte [] responseData;
	private String filename;
    
	public IdtechVendXDataBlob(CardReaderType cardReaderType, EntryType entryType, byte [] responseData) throws IOException {
		this(cardReaderType, entryType, responseData, null);
	}
    public IdtechVendXDataBlob(CardReaderType cardReaderType, EntryType entryType) throws IOException {
    	this(cardReaderType, entryType, null, null);
    }
    
    public IdtechVendXDataBlob(CardReaderType cardReaderType, EntryType entryType, byte [] responseData, String filename) throws IOException {
    	this.cardReaderType = cardReaderType;
    	this.entryType = entryType;
    	this.responseData = responseData;
    	this.filename = filename;
    	init();
    	if (responseData != null) {
	    	for (int ii = 0; ii < responseData.length; ii++) {
	    		boolean complete = append(responseData[ii]);
	    		if (complete && ii != responseData.length - 1) {
	    			String msg = "responseData counter is " + ii + ", but responseData.length is: " + responseData.length + ". Aborting as something is wrong.";
	    			System.err.println(msg);
	    			throw new IOException(msg);
	    		}
	    	}
    	}
    }
    
    private void init() {
    	headerBufferCounter = 0;
    	dataBufferCounter = 0;
    	crcBufferCounter = 0;
    }
    
    public Map<byte [], byte[]>parseMap() throws IOException {
    	if (parseMap == null) {
    		byte [] tlvData = getEMVData().getTlvData();
    		log.info("parseMap() tlvData: " + StringUtils.toHex(tlvData));
    		if (tlvData != null) {
    			parseMap = TLV_PARSER.parse(tlvData, 0, tlvData.length);
    		}
    	}
    	return parseMap;
    }
	
    
    /*
     * Vendi rev 51 EMV Contactless seems to parse to the same format as VendIII EMV Contactless... or at least close enough for testing
     */
    public static VendXEMVResponseData parseVendiEncryptedEMVContactless(byte []activateTxnResponse) throws IOException {
    	return parseIdtechVendIIIContactlessEMVData(activateTxnResponse);
    }
    
    /*
     * VIVOPay Advanced Reader Serial Interface Developers Guide 730-1002-07 Rev 1.0 page 11
     * My initial attempt from the VendIII
     */
    public static VendXEMVResponseData parseIdtechVendIIIContactlessEMVData(byte []activateTxnResponse) throws IOException {
    	VendXEMVResponseData emvData = new VendXEMVResponseData();
		emvData.setRawResponse(activateTxnResponse);
		if (activateTxnResponse != null) {
			int trackOneLength = 0;
			int trackTwoLength = 0;
			int trackThreeLength = 0;
			int TRACK1_LENGTH_LENGTH = 1;
			int TRACK2_LENGTH_LENGTH = 1;
			int TRACK3_LENGTH_LENGTH = 1;
			
			if (activateTxnResponse.length > 0) {
				trackOneLength = activateTxnResponse[0] & 0xff;
			}
			int track2LengthStart = TRACK1_LENGTH_LENGTH + trackOneLength;
			if (activateTxnResponse.length > track2LengthStart) {
				trackTwoLength = activateTxnResponse[track2LengthStart] & 0xff;
			}
			int track3LengthStart = track2LengthStart + trackTwoLength;
			if (activateTxnResponse.length > track3LengthStart) {
				trackThreeLength = activateTxnResponse[track3LengthStart] & 0xff;
			}
			
			int trackTwoOffset = TRACK1_LENGTH_LENGTH + trackOneLength + TRACK2_LENGTH_LENGTH;
			if ((trackTwoOffset + trackTwoLength) <= activateTxnResponse.length) {
				if (trackTwoLength > 0) {
					emvData.setTrackTwoData(new byte [trackTwoLength]);
					System.arraycopy(activateTxnResponse, trackTwoOffset, emvData.getTrackTwoData(), 0, trackTwoLength);
				}
			} else {
				throw new IOException("Invalid track 2 offset or length. Will exceed length of emvData  Track 2 offset " + trackTwoOffset + ". Track 2 length " + trackTwoLength + " emvData.length " + activateTxnResponse.length);
			}
			
			int tlvStartOffset = TRACK1_LENGTH_LENGTH + trackOneLength + TRACK2_LENGTH_LENGTH + trackTwoLength + TRACK3_LENGTH_LENGTH + trackThreeLength;
			int tlvLength = activateTxnResponse.length - tlvStartOffset;
			if (tlvLength > 0 && (tlvStartOffset + tlvLength) <= activateTxnResponse.length) {
		    	emvData.setTlvData(new byte[tlvLength]);
		    	System.arraycopy(activateTxnResponse, tlvStartOffset, emvData.getTlvData(), 0, tlvLength);
			} else {
//				throw new IOException("Invalid chip card tlv data offset or length. Will exceed length of emvData  tlv data offset " + tlvStartOffset + ". tlv length " + tlvLength + " emvData.length " + emvData.length);
			}
			
//			emvData = normalizeIdtechVendData(FAKE_ENCRYPTED_DATA_CONTACTLESS_FIELD_HEADING_DATA, activateTxnResponse.length, emvData);
		}
		return emvData;
	}

    /*
     * Status code 0x30 requires this parsing
     */
	public static VendXEMVResponseData parseIdtechVendIIIContactRequestOnlineAuthEmvData(byte [] activateTxnResponse) throws IOException {
		VendXEMVResponseData emvData = new VendXEMVResponseData();
		emvData.setRawResponse(activateTxnResponse);
		//byte [] resultData = null;
		if (activateTxnResponse != null) {
			emvData.setEntryType(EntryType.EMV_CONTACT);
			byte encipheredOnlinePin = activateTxnResponse[Vend3.DATA_START_OFFSET];
			int emvDataStart = (encipheredOnlinePin != 0) ? Vend3.DATA_START_OFFSET : Vend3.DATA_START_OFFSET + 1;
			int dataLength = activateTxnResponse.length - emvDataStart - Vend3.CRC_LENGTH;
			emvData.setTlvData(new byte [dataLength]);
			System.arraycopy(activateTxnResponse, emvDataStart, emvData.getTlvData(), 0, dataLength);
		}
		return emvData;
	}
	
	
    public int calculateDataLength() {
    	char msbLength = (char)(getHeaderBuffer()[HEADER_LENGTH - 2] & 0xff);
    	char lsbLength = (char)(getHeaderBuffer()[HEADER_LENGTH - 1] & 0xff);
    	int resultLength = (msbLength << 8) ^ lsbLength;
    	return resultLength;
    }
	
    public EntryType entryTypeFromMsg() throws IOException, InvalidValueException {
    	Map<byte[], byte[]>map = parseMap();
    	EntryType entryType = null;
    	if (map != null) {
	    	byte [] vivoTechGroupTagBytes = map.get(TLVTag.VIVOTECH_GROUP_TAG.getValue());
	    	if (vivoTechGroupTagBytes != null) {
		    	Map<byte [], byte[]> groupMap = TLV_PARSER.parse(vivoTechGroupTagBytes, 0, vivoTechGroupTagBytes.length);
		    	byte [] trackDataSourceList = groupMap.get(TLVTag.TRACK_DATA_SOURCE.getValue());
		    	if (trackDataSourceList != null && trackDataSourceList.length > 0) {
		    		entryType = Vend3CardReaderDataSource.entryTypeForValue(trackDataSourceList[0]);
		    	}
	    	}
    	}
    	return entryType;
    }

    public BigDecimal amountAuthorize() throws IOException {
		Map<byte[], byte[]> parseMap = parseMap();
		BigDecimal amount = null;
		if (parseMap != null) {
			byte[] dataForTag = parseMap.get(TLVTag.TRANSACTION_AMOUNT_AUTHORIZED.getValue());
			String dataAsString = bufferAsDecimalString(dataForTag);
			try {
				amount = new BigDecimal(dataAsString);
			} catch (NumberFormatException e) {
				e.printStackTrace();
				System.out.println("Amount to authorize not parsable.  Continuing with 1 for amount.");
				amount =  new BigDecimal("1");
			}
		}
    	return amount;
    }
    
	public boolean append(int inVal) throws IOException {
		return append((byte)(inVal & 0xff));
	}
	
	public boolean append(byte inVal) throws IOException {
    	boolean isMessageComplete = false;
    	if (getHeaderBufferCounter() < HEADER_LENGTH) {
    		getHeaderBuffer()[getHeaderBufferCounter()] = inVal;
    		incrementHeaderBufferCounter();
    	} else if (getHeaderBufferCounter() == HEADER_LENGTH) {
    		headerDataLength = calculateDataLength();
    		incrementHeaderBufferCounter(); 	// so it's done with the header buffer
    		dataBuffer = new byte [headerDataLength];
    		if (getDataBufferCounter() < headerDataLength) {
        		getDataBuffer()[getDataBufferCounter()] = inVal;
        		incrementDataBufferCounter();
    		} else if (getCrcBufferCounter() < CRC_BUFFER_LENGTH) {
        		getCrcBuffer()[getCrcBufferCounter()] = inVal;
        		incrementCrcBufferCounter();        			
    		} else {
    			isMessageComplete = true;
    		}
    	} else if (getDataBufferCounter() < headerDataLength) {
    		getDataBuffer()[getDataBufferCounter()] = inVal;
    		incrementDataBufferCounter();
    	} else if (getCrcBufferCounter() < CRC_BUFFER_LENGTH) {
    		getCrcBuffer()[getCrcBufferCounter()] = inVal;
    		incrementCrcBufferCounter();
    	} 
    	
    	if (getCrcBufferCounter() == CRC_BUFFER_LENGTH) {
    		isMessageComplete = true;
    	}
    	return isMessageComplete;
    }
	
	public boolean shouldAssembleEmvData(byte cmdByte, byte statusCode, byte errorCode) {
		boolean result = Vend3.isGoodEmvData(cmdByte, statusCode) || Vend3.isFailedWithAuthRequest(statusCode, errorCode);
		return result;
	}
	
	public boolean isGoodGetConfiguration(byte cmdByte, byte statusCode) {
		boolean result = (cmdByte == CMD_GET_CONFIGURATION && Vend3StatusCode.OK == statusCode);
		return result;
	}
	
    public int getHeaderDataLength() {
    	return headerDataLength;
    }
    
	public byte[] getHeaderBuffer() {
		return headerBuffer;
	}

	public int getHeaderBufferCounter() {
		return headerBufferCounter;
	}
	private void incrementHeaderBufferCounter() {
		headerBufferCounter++;
	}

	@Override
	public byte[] getDataBuffer() {
		return dataBuffer;
	}

	public int getDataBufferCounter() {
		return dataBufferCounter;
	}
	private void incrementDataBufferCounter() {
		dataBufferCounter++;
	}

	public byte[] getCrcBuffer() {
		return crcBuffer;
	}

	public int getCrcBufferCounter() {
		return crcBufferCounter;
	}
	private void incrementCrcBufferCounter() {
		crcBufferCounter++;
	}

	public VendXEMVResponseData getEMVData() {
    	return emvData;
    }
	public void setEMVData(VendXEMVResponseData emvData) {
		this.emvData = emvData;
	}
	
	@Override
	public String toStringId() {
		String dataLengthString = ", data length: " + ((getDataBuffer() != null) ? "" + getDataBuffer().length : "null");
		return getFilename() + " CardReaderType: " + getCardReaderType() + ", EntryType: " + getEntryType() + dataLengthString;
	}

	public byte getCmdByte() {
		byte cmdByte = (byte) 0xff;
		if (getHeaderBuffer() != null && getHeaderBuffer().length > COMMAND_OFFSET) {
			cmdByte = getHeaderBuffer()[COMMAND_OFFSET];
		}
		return cmdByte;
	}
	public Vend3.Command getCmd() throws InvalidByteValueException {
		return Vend3.Command.getByValue(getCmdByte());
	}

	public byte getStatusCodeByte() {
		byte statusCode = (byte) 0xff;
		if (getHeaderBuffer() != null && getHeaderBuffer().length > STATUS_CODE_OFFSET) {
			statusCode = getHeaderBuffer()[STATUS_CODE_OFFSET];
		}
		return statusCode;
	}
//	public Vend3.Vend3StatusCode getStatusCode() {
//		return Vend3.Vend3StatusCode.getByValue(getStatusCodeByte());
//	}
	
	public byte getErrorCodeByte() {
		byte errorCode = (byte) 0xff;
		if (getHeaderBuffer() != null && getHeaderBuffer().length > ERROR_CODE_OFFSET) {
			errorCode = getHeaderBuffer()[ERROR_CODE_OFFSET];
		}
		return errorCode;
	}
	
	/*********** diagnostics ***************/
	public void dumpData(int msgNumber) {
		int BUFFER_WIDTH = 16;
		byte asciiBuffer [] = new byte [BUFFER_WIDTH];
		int asciiBufferCounter = 0;
		
		System.out.println("\nMessage #" + msgNumber + ": There is a complete message with length: " + getHeaderDataLength());
		System.out.println("The data buffer length is: " + getDataBuffer().length);
		System.out.println("============ BEGIN =====================");
		System.out.println("POS");
		boolean needRowCounter = true;
		for (int ii = 0; ii < getDataBufferCounter(); ii++) {
			if (needRowCounter) {
				System.out.print(String.format("%03d  ", ii));
				needRowCounter = false;
			}
			int c = getDataBuffer()[ii];
			asciiBuffer[asciiBufferCounter] = (byte)(c & 0xff);
			asciiBufferCounter++;
        	String value = String.format("%02X", (c & 0xff));
        	System.out.print(value + " ");
        	if ((asciiBufferCounter % BUFFER_WIDTH) == 0) {
        		CardReaderDataBlob.dumpBufferAsAscii(asciiBuffer, asciiBufferCounter);
				asciiBufferCounter = 0;
        		System.out.println("");
        		needRowCounter = true;
        	}
		}

		System.out.println("\n============ END ======================");
	}
	
	@Override
	public void dumpCompleteDataMsg(int msgNumber) {
		int BUFFER_WIDTH = 16;
		byte asciiBuffer [] = new byte [BUFFER_WIDTH];
		int asciiBufferCounter = 0;
		
		int msgLength = getHeaderBuffer().length + getDataBuffer().length + getCrcBuffer().length;
		
		byte [] dumpBuffer = new byte [msgLength];
		System.arraycopy(getHeaderBuffer(), 0, dumpBuffer, 0, getHeaderBuffer().length);
		System.arraycopy(getDataBuffer(), 0, dumpBuffer, getHeaderBuffer().length, getDataBuffer().length);
		System.arraycopy(getCrcBuffer(), 0, dumpBuffer, getHeaderBuffer().length + getDataBuffer().length, getCrcBuffer().length);
		
		System.out.println("\nComplete Reader Message #" + msgNumber + ": full message length: " + msgLength);
		System.out.println("============ BEGIN =====================");
		System.out.println("POS");
		boolean needRowCounter = true;
		for (int ii = 0; ii < dumpBuffer.length; ii++) {
			if (needRowCounter) {
				System.out.print(String.format("%03d  ", ii));
				needRowCounter = false;
			}
			int c = dumpBuffer[ii];
			asciiBuffer[asciiBufferCounter] = (byte)(c & 0xff);
			asciiBufferCounter++;
        	String value = String.format("%02X", (c & 0xff));
        	System.out.print(value + " ");
        	if ((asciiBufferCounter % BUFFER_WIDTH) == 0) {
        		CardReaderDataBlob.dumpBufferAsAscii(asciiBuffer, asciiBufferCounter);
				asciiBufferCounter = 0;
        		System.out.println("");
        		needRowCounter = true;
        	}
		}
		System.out.println("\n============ END ======================");
	}

	public void dumpAllTags(int msgNumber) throws IOException {
		dumpAllTags(parseMap(), msgNumber);
	}
	
	public static void dumpAllTags(Map<byte [], byte []>parseMap, int msgNumber) throws IOException {
    	System.out.println("Msg #" + msgNumber + ". All tag fields...");
    	byte [] key = null;
	    	for (Map.Entry<byte [], byte []>entry: parseMap.entrySet()) {
	    		key = entry.getKey();
	    		if (key != null && key.length > 0 && key[0] != 0) {
	    	    	try {
			    		TLVTag tag = TLVTag.getByValue(key);
			    		System.out.println(tag);
			    		System.out.println("Key " + bufferAsHexStringWithSpaceSeparator(entry.getKey()) + " Value: " + bufferAsHexStringWithSpaceSeparator(entry.getValue()) + "\n");
			    		if (tag.equals(TLVTag.VIVOTECH_GROUP_TAG)) {
			    			Map<byte [], byte []>groupTag = TLV_PARSER.parse(entry.getValue(), 0, entry.getValue().length);
			    			byte [] trackDataSource = groupTag.get(TLVTag.TRACK_DATA_SOURCE.getValue());
			    			if (trackDataSource != null && trackDataSource.length > 0) {
			    				System.out.println("\tEntry Type Value: " + Vend3CardReaderDataSource.entryTypeForValue(trackDataSource[0]) + "\n");
			    			} else {
			    				System.out.println("Track data source not available.  Unable to determine EntryType");
			    			}
			    		}
	    	    	} catch (InvalidValueException e) {
	    	    		System.err.println("Value: " + bufferAsHexStringWithSpaceSeparator(key) + " not found in TLVTag.  Continuing to dump tags.");
	    	    		e.printStackTrace();
	    	    	}
	    		} else {
	    			System.out.println("Skipping key that was either null or zero bytes in length or array of [0]");
	    		}
	    	}
    	System.out.println("=================");
    }
    
    public void dumpTrackEquivalentData() throws IOException {
    	Map<byte[], byte[]> parseMap = parseMap();
    	if (parseMap != null) {
	    	byte [] track2Equivalent = parseMap.get(TLVTag.TRACK2_EQUIVALENT_DATA.getValue());
	    	if (track2Equivalent != null) {
		    	System.out.println("Track 2 Equivalent TLV: " + bufferAsHexStringWithSpaceSeparator(track2Equivalent));
	    	} else {
	    		System.out.println("Track 2 Equivalent TLV data not present.");
	    	}
    	} else {
    		System.out.println("Parse map not available.");
    	}
    	
    }

	public String hexByteListToHexString(byte [] buffer) {
		String result = new String(buffer, ProcessingConstants.ISO8859_1_CHARSET);
		return result;
	}
	
	public String bufferAsDecimalString(byte [] buffer) {
		StringBuffer sb = new StringBuffer("");
		if (buffer != null) {
			for (int ii= 0; ii < buffer.length; ii++) {
				String value = String.format("%02d", buffer[ii]); 
				sb.append(value);
			}
		}
		return sb.toString();
	}
	
	private static String bufferAsHexStringWithSpaceSeparator(byte []buffer) {
		StringBuffer sb = new StringBuffer("");
		if (buffer != null) {
			for (int ii= 0; ii < buffer.length; ii++) {
				sb.append(StringUtils.toHex(buffer[ii]));
				if ((ii + 1) < buffer.length) {
					sb.append(" ");
				}
			}
		}
		return sb.toString();
	}
	
//	public static boolean isAscii(char c) {
//		boolean result = (c >= 32 && c < 127);
//		return result;
//	}
//	
//	public static boolean isSpecialCharacter(char c) {
//		boolean result = (c == '\n' || c == '\r' || c == '\t');
//		return result;
//	}
//
//	public static void dumpBufferAsAscii(byte[] asciiBuffer, int asciiBufferCounter) {
//		for (int ii = 0; ii < asciiBufferCounter; ii++) {
//			char value = charAsAscii(asciiBuffer[ii]);
//	    	System.out.print(value);
//		}
//	}
//	public static char charAsAscii(byte value) {
//		char result = 0;
//		if (!isAscii((char)value) || isSpecialCharacter((char)value)) {
//			result = '.';
//		} else {
//			result = (char)(value & 0x00ff);
//		}
//		return result;
//	}
    public void dumpTrackData(int msgNumber) {
    	byte [] track2 = getEMVData().getTrackTwoData();
    	dumpTrack(track2, "Msg #" + msgNumber + ": Track 2");
    }
    private void dumpTrack(byte [] track, String msg) {
    	if (track != null && track.length > 0) {
    		System.out.println(msg + " " + bufferAsHexStringWithSpaceSeparator(track));
    	} else {
    		System.out.println(msg + " not found.");
    	}
    }
    
    private void dumpTagFromMap(TLVTag tag, Map<byte[], byte[]> parseMap) {
		byte[] dataForTag = parseMap.get(tag.getValue());
		String dataAsString = bufferAsHexStringWithSpaceSeparator(dataForTag);
		System.out.println(tag + ": " + dataAsString);
    }

    public void tandemFieldsDump() throws IOException {
    	Map<byte[], byte[]> parseMap = parseMap(); 
    	System.out.println("Known tandem fields...");
    	dumpTagFromMap(TLVTag.PAN_SEQUENCE_NUMBER, parseMap);
    	dumpTagFromMap(TLVTag.APPLICATION_IDENTIFIER, parseMap);
    	dumpTagFromMap(TLVTag.POS_ENTRY_MODE, parseMap);
    	System.out.println("======================");
	}

    public static IdtechVendXDataBlob parseVendXFromFile(byte [] fileContent, CardReaderType cardReaderType, EntryType entryType, String filename) throws IOException {
    	IdtechVendXDataBlob cardReaderData = new IdtechVendXDataBlob(cardReaderType, entryType, fileContent, filename);
    	if (cardReaderType.equals(CardReaderType.IDTECH_VEND3AR)) {
    		Vend3ARTransactionResponseData data = Vend3ARParsing.parseAnyARResponseData(fileContent);
    		VendXEMVResponseData emvData = new VendXEMVResponseData();
    		emvData.setEntryType(entryType);
    		emvData.setRawResponse(fileContent);
    		emvData.setTrackTwoData(data.getTrackTwoData());  //fix the setTrackTwoMagStripeData() method name
    		emvData.setTlvData(data.getTlvData());
    		cardReaderData.setEMVData(emvData);
    		
			VivotechTrackDataSource dataSource = Vend3.DEFAULT_VIVOTECH_TRACK_DATA_SOURCE;
			try {
				byte []trackDataSource = Vend3ARParsing.parseTrackDataSource(cardReaderData.getEMVData().getTlvData());
				dataSource = VivotechTrackDataSource.getByValue(trackDataSource[0]);
			} catch (InvalidByteValueException | IOException e) {
				System.out.println("Parsesd Vend3ARParsing.VivotechTrackDataSource not available");
			}
			cardReaderData.setCardReaderType(CardReaderType.IDTECH_VEND3AR);
			cardReaderData.setEntryType(dataSource.getEntryType());
			
    	} else if (cardReaderType.equals(CardReaderType.IDTECH_VENDX_ENCRYPTED_EMV)) {
    		VendXEMVResponseData emvData = IdtechVendXDataBlob.parseVendiEncryptedEMVContactless(cardReaderData.getDataBuffer());
    		cardReaderData.setEMVData(emvData);
    	}
    	
    	return cardReaderData; 
    }

    public static byte commandByteFromByteStream(byte []cmd) {
    	return cmd[COMMAND_OFFSET];
    }
    
    private static String componentSeparator = "-";
	public static String assembleFilenameFor(CardReaderType cardReaderType, EntryType entryType, String keyedFilename) {
		String result = cardReaderType.toString() + componentSeparator + entryType.toString() + componentSeparator + keyedFilename + ".bin";
		return result;
	}
	public static CardReaderType cardReaderTypeFromFilename(String filename) {
		String [] parts = filename.split(componentSeparator);
		return CardReaderType.valueOf(parts[0]);
	}
	public static EntryType entryTypeFromFilename(String filename) {
		String [] parts = filename.split(componentSeparator);
		return EntryType.valueOf(parts[1]);
	}
	public static String filenameFromFilename(String filename) {
		String [] parts = filename.split(componentSeparator);
		return parts[2];
	}

	@Override
	public CardReaderType getCardReaderType() {
		return cardReaderType;
	}

	public void setCardReaderType(CardReaderType cardReaderType) {
		this.cardReaderType = cardReaderType;
	}
	
	@Override
    public EntryType getEntryType() {
    	return entryType;
    }
    public void setEntryType(EntryType entryType) {
    	this.entryType = entryType;
    }
    @Override
	public String getFilename() {
		return filename;
	}
	public byte[] getResponseData() {
		return responseData;
	}
	
	@Override
	public byte[] entireContents() {
		int entireLength = getHeaderBuffer().length + getDataBuffer().length + getCrcBuffer().length;
		byte [] result = new byte [entireLength];
		System.arraycopy(getHeaderBuffer(), 0, result, 0, getHeaderBuffer().length);
		System.arraycopy(getDataBuffer(), 0, result, getHeaderBuffer().length, getDataBuffer().length);
		System.arraycopy(getCrcBuffer(), 0, result, getHeaderBuffer().length + getDataBuffer().length, getCrcBuffer().length);
		return result;
	}
	@Override
	public byte[] getRawData() {
		byte [] result = null;
		switch (getCardReaderType()) {
			case IDTECH_VENDX_ENCRYPTED_EMV:
			case IDTECH_VENDX_ENHANCED_ENCRYPTED_MSR:
				result = getDataBuffer();
				break;
			case IDTECH_VEND3AR:
			default:
				result = entireContents();
				break;
		}
	
		return result;
	}
        
	@Override
	public BigDecimal amountAuthorized() {
		BigDecimal result = new BigDecimal("100"); 
		try {
			Map<byte [], byte[]> map = TLV_PARSER.parse(getEMVData().getTlvData(), 0, getEMVData().getTlvData().length);
			byte [] amountAuthed = map.get(TLVTag.TRANSACTION_AMOUNT_AUTHORIZED.getValue());
			if (amountAuthed == null) {
				byte [] dataRecord = map.get(TLVTag.MASTERCARD_DATA_RECORD.getValue());
				if (dataRecord != null && dataRecord.length > 0) {
					Map<byte[], byte[]>mcMap = TLV_PARSER.parse(dataRecord, 0, dataRecord.length);
					amountAuthed = mcMap.get(TLVTag.TRANSACTION_AMOUNT_AUTHORIZED.getValue());
				}
			}
			if (amountAuthed != null) {
				String amount = StringUtils.toHex(amountAuthed);
				result = new BigDecimal(amount);
			} else {
				log.info("Defaulting Amount Authorized to " + result);
			}
		} catch (IOException e) {
			log.info("Unable to parse IdtechVendXDataBlob for amountAuthorized: ", e);
		}
		return result;
	}
}
