package com.usatech.test;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.util.StringTokenizer;

import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.AuthResponseCodeC3;
import com.usatech.layers.common.constants.AuthPermissionActionCode;
import com.usatech.layers.common.constants.PermissionResponseCodeC3;
import com.usatech.layers.common.constants.AuthResponseType;
import com.usatech.networklayer.ReRixMessage;
/**
 * Authorization request C2-->C3 response
 * @author yhe
 *
 */
public class C2Message extends V4Message {
	protected long transactionId=System.currentTimeMillis()/1000;;
	//0x42, 0x43, 0x4D, 0x53 B bar code, C contactless, M manually entered, S magnetic
	protected int entryType;
	protected BigDecimal amount;
	protected String validationData;
	protected int cardReaderType;
	protected SubMessage cardReader;

	public class CardReaderType0 extends SubMessage{
		protected String accountData1="";
		protected String accountData2="";
		protected String accountData3="";
		public CardReaderType0(StringTokenizer userInputTokens){
			super(userInputTokens);
		}
		@Override
		public void getParams()throws Exception{
			super.getParams();
		}
		@Override
		public void writeMessage(ByteBuffer bb){
			ProcessingUtils.writeShortString(bb,accountData1, ProcessingConstants.US_ASCII_CHARSET);
			ProcessingUtils.writeShortString(bb,accountData2, ProcessingConstants.US_ASCII_CHARSET);
			ProcessingUtils.writeShortString(bb,accountData3, ProcessingConstants.US_ASCII_CHARSET);
		}
		public String getAccountData1() {
			return accountData1;
		}
		public void setAccountData1(String accountData1) {
			this.accountData1 = accountData1;
		}
		public String getAccountData2() {
			return accountData2;
		}
		public void setAccountData2(String accountData2) {
			this.accountData2 = accountData2;
		}
		public String getAccountData3() {
			return accountData3;
		}
		public void setAccountData3(String accountData3) {
			this.accountData3 = accountData3;
		}

	}
	public class CardReaderType1 extends SubMessage{
		protected String keySerialNum="";
		protected int decryptedData1Len;
		protected int decryptedData2Len;
		protected int decryptedData3Len;
		protected String encryptedData1="";
		protected String encryptedData2="";
		protected String encryptedData3="";
		public CardReaderType1(StringTokenizer userInputTokens){
			super(userInputTokens);
		}

		@Override
		public void writeMessage(ByteBuffer bb){
			ProcessingUtils.writeShortString(bb,keySerialNum, ProcessingConstants.US_ASCII_CHARSET);
			ProcessingUtils.writeByteInt(bb,decryptedData1Len);
			ProcessingUtils.writeByteInt(bb,decryptedData2Len);
			ProcessingUtils.writeByteInt(bb,decryptedData3Len);
			ProcessingUtils.writeShortString(bb,encryptedData1, ProcessingConstants.US_ASCII_CHARSET);
			ProcessingUtils.writeShortString(bb,encryptedData2, ProcessingConstants.US_ASCII_CHARSET);
			ProcessingUtils.writeShortString(bb,encryptedData3, ProcessingConstants.US_ASCII_CHARSET);
		}
		public String getKeySerialNum() {
			return keySerialNum;
		}
		public void setKeySerialNum(String keySerialNum) {
			this.keySerialNum = keySerialNum;
		}
		public int getDecryptedData1Len() {
			return decryptedData1Len;
		}
		public void setDecryptedData1Len(int decryptedData1Len) {
			this.decryptedData1Len = decryptedData1Len;
		}
		public int getDecryptedData2Len() {
			return decryptedData2Len;
		}
		public void setDecryptedData2Len(int decryptedData2Len) {
			this.decryptedData2Len = decryptedData2Len;
		}
		public int getDecryptedData3Len() {
			return decryptedData3Len;
		}
		public void setDecryptedData3Len(int decryptedData3Len) {
			this.decryptedData3Len = decryptedData3Len;
		}
		public String getEncryptedData1() {
			return encryptedData1;
		}
		public void setEncryptedData1(String encryptedData1) {
			this.encryptedData1 = encryptedData1;
		}
		public String getEncryptedData2() {
			return encryptedData2;
		}
		public void setEncryptedData2(String encryptedData2) {
			this.encryptedData2 = encryptedData2;
		}
		public String getEncryptedData3() {
			return encryptedData3;
		}
		public void setEncryptedData3(String encryptedData3) {
			this.encryptedData3 = encryptedData3;
		}

	}
	public C2Message(){
		super();
		dataType=0xC2;
		amount=new BigDecimal("10");
		validationData="";
		entryType='S';
		cardReaderType=0;
		cardReader=new CardReaderType0(userInputTokens);
	}
	@Override
	public void getParams() throws Exception{
		super.getParams();
		if(cardReaderType==0){
			cardReader=new CardReaderType0(userInputTokens);
		}else{
			cardReader=new CardReaderType1(userInputTokens);
		}
		cardReader.getParams();
	}
	@Override
	public ReRixMessage createMessage() {
		ByteBuffer bb=ByteBuffer.allocate(1551);
		writeMessageHeader(bb);
		ProcessingUtils.writeLongInt(bb,transactionId);
		ProcessingUtils.writeByteInt(bb, entryType);
		ProcessingUtils.writeStringAmount(bb, amount);
		ProcessingUtils.writeShortString(bb, validationData, ProcessingConstants.US_ASCII_CHARSET);
		ProcessingUtils.writeByteInt(bb, cardReaderType);
		cardReader.writeMessage(bb);
		return writeMessageEnd(bb);
	}
	//C3
	@Override
	public String readResponseString(ReRixMessage msg)throws InvalidByteValueException{
		responseMap.put("transactionId", transactionId);//put for next sale message
		ByteBuffer bb=ByteBuffer.wrap(msg.getData());
		StringBuilder sb=readResponseHeader(bb);
		int responseType=ProcessingUtils.readByteInt(bb);
		printResponse(sb, "ResponseType", AuthResponseType.getByValue((byte)responseType));
		//Control
		if(responseType==0){
			int actionCode=ProcessingUtils.readByteInt(bb);
			if(actionCode!=1){
				printResponse(sb, "ActionCode", "Unknown action code from server:"+actionCode);
			}else{
				printResponse(sb, "ActionCode", "Perform session/call-in");
			}
			printResponse(sb, "Value", ProcessingUtils.readShortInt(bb));
		}else if (responseType==1){
			//Authorization
			printResponse(sb, "ResponseCode", AuthResponseCodeC3.getByValue((byte)ProcessingUtils.readByteInt(bb)));
			printResponse(sb, "BalanceAmount", ProcessingUtils.readStringAmount(bb));
			printResponse(sb, "ResultAmount", ProcessingUtils.readStringAmount(bb));
			printResponse(sb, "AuthorityAuthorizationCode", ProcessingUtils.readShortString(bb, ProcessingConstants.US_ASCII_CHARSET));
			printResponse(sb, "ResultMessage", ProcessingUtils.readShortString(bb, ProcessingConstants.US_ASCII_CHARSET));
		}else{
			//permission
			printResponse(sb, "ResponseCode", PermissionResponseCodeC3.getByValue((byte)ProcessingUtils.readByteInt(bb)));
			int actionCode=ProcessingUtils.readByteInt(bb);
			printResponse(sb, "ActionCode", AuthPermissionActionCode.getByValueSafely((byte)actionCode));
			if(actionCode==5){
				printResponse(sb, "ActionCodeParam", ProcessingUtils.readShortInt(bb));
			}

		}
		return sb.toString();
	}
	public void setEntryType(int entryType) {
		this.entryType = entryType;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getValidationData() {
		return validationData;
	}
	public void setValidationData(String validationData) {
		this.validationData = validationData;
	}
	public int getCardReaderType() {
		return cardReaderType;
	}
	public void setCardReaderType(int cardReaderType) {
		this.cardReaderType = cardReaderType;
	}
	public int getEntryType() {
		return entryType;
	}
	public SubMessage getCardReader() {
		return cardReader;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception{
		ClientConnectUtil conn=new ClientConnectUtil();
		conn.setProtocol(7);
		conn.startCommunication();
		C2Message sMsg=new C2Message();
		//sMsg.setTempAcct("639621130020001130=010812345");
		sMsg.getParams();
		//permission
		ReRixMessage sentMsg=sMsg.createMessage();
		ReRixMessage responseMsg=conn.sendMessage(sentMsg, sMsg.encryptKey);
		sMsg.readResponse(responseMsg);
		conn.stopCommunication();

		//auth
		sMsg=new C2Message();
		conn.startCommunication();
		//sMsg.setTempAcct("4300123412341017=12100001");
		sMsg.getParams();
		sentMsg=sMsg.createMessage();
		responseMsg=conn.sendMessage(sentMsg, sMsg.encryptKey);
		sMsg.readResponse(responseMsg);
		conn.stopCommunication();

		//control
		sMsg=new C2Message();
		conn.startCommunication();
		sMsg.setEntryType('Y');//invalid
		sMsg.getParams();
		sentMsg=sMsg.createMessage();
		responseMsg=conn.sendMessage(sentMsg, sMsg.encryptKey);
		sMsg.readResponse(responseMsg);
		conn.stopCommunication();

	}
	public long getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
	/**
	 * @see com.usatech.test.V4Message#read(java.nio.ByteBuffer)
	 */
	@Override
	protected void read(ByteBuffer data) throws IOException, InvalidByteValueException {
		super.read(data);
		setTransactionId(ProcessingUtils.readLongInt(data));
		setEntryType(ProcessingUtils.readByteInt(data));
		setAmount(ProcessingUtils.readStringAmount(data));
		setValidationData(ProcessingUtils.readShortString(data, ProcessingConstants.US_ASCII_CHARSET));
		setCardReaderType(ProcessingUtils.readByteInt(data));
		SubMessage crt = getCardReader();
		if(crt instanceof CardReaderType0) {
			CardReaderType0 crt0 = (CardReaderType0)crt;
			crt0.setAccountData1(ProcessingUtils.readShortString(data, ProcessingConstants.US_ASCII_CHARSET));
			crt0.setAccountData2(ProcessingUtils.readShortString(data, ProcessingConstants.US_ASCII_CHARSET));
			crt0.setAccountData3(ProcessingUtils.readShortString(data, ProcessingConstants.US_ASCII_CHARSET));
		} else if(crt instanceof CardReaderType1) {
			//TODO: implement
		}
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMessageTypeName())
			.append(": [messageNumber=").append(getMessageNumber())
			.append("; messageId=").append(getMessageId())
			.append("; transactionId=").append(getTransactionId())
			.append("; entryType=").append(getEntryType())
			.append("; amount=").append(getAmount())
			.append("; validationData=").append(getValidationData())
			.append("; cardReaderType=").append(getCardReaderType());
		SubMessage crt = getCardReader();
		if(crt instanceof CardReaderType0) {
			CardReaderType0 crt0 = (CardReaderType0)crt;
			sb.append(": [accountData1=").append(crt0.getAccountData1())
				.append("; accountData2=").append(crt0.getAccountData2())
				.append("; accountData3=").append(crt0.getAccountData3());
			} else if(crt instanceof CardReaderType1) {
			//TODO: implement
		}
		sb.append("]]");

		return sb.toString();
	}
}
