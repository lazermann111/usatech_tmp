package com.usatech.test;

import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.util.VendXEMVData;

public class VendXEMVResponseData extends VendXEMVData {

	private EntryType entryType;
	private byte [] rawResponse;

	public EntryType getEntryType() {
		return entryType;
	}
	public void setEntryType(EntryType entryType) {
		this.entryType = entryType;
	}
	public byte[] getRawResponse() {
		return rawResponse;
	}

	public void setRawResponse(byte[] rawResponse) {
		this.rawResponse = rawResponse;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(super.toString());
		sb.append(", Entry Type: ");
		sb.append(getEntryType());
		return sb.toString();
	}
}
