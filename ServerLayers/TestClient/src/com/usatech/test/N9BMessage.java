package com.usatech.test;

import java.nio.ByteBuffer;

import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
public class N9BMessage extends BaseMessage {
	String fileName;
	public N9BMessage() {
		super();
		dataType=0x9B;
		fileName="";
	}

	@Override
	public ReRixMessage createMessage() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		ProcessingUtils.writeString(bb, fileName, ProcessingConstants.US_ASCII_CHARSET);
		return writeMessageEnd(bb);
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Override
	public String readResponseString(ReRixMessage msg) throws InvalidByteValueException {
		// TODO Auto-generated method stub
		return null;
	}
}
