package com.usatech.test;

import java.nio.ByteBuffer;

import simple.lang.InvalidByteValueException;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.networklayer.ReRixMessage;
/**
 * B9 Sony info log V2.0 --> 2F
 * @author yhe
 *
 */
public class B9Message extends LegacyMessage {
	protected int infoType;
	protected String info;
	public B9Message() {
		super();
		dataType=0xB9;
		infoType='U';
		info="Test";
	}

	@Override
	public ReRixMessage createMessage() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(3+info.length());
		writeMessageHeader(bb);
		ProcessingUtils.writeByteInt(bb, infoType);
		bb.put(info.getBytes());
		return writeMessageEnd(bb);
	}

	@Override
	public String readResponseString(ReRixMessage msg) throws InvalidByteValueException {
		return readResponseString2F(msg);
	}

	public int getInfoType() {
		return infoType;
	}

	public void setInfoType(int infoType) {
		this.infoType = infoType;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

}
