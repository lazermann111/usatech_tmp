package com.usatech.test;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.io.Log;
import simple.lang.InvalidByteValueException;
import simple.lang.InvalidValueException;

import com.usatech.layers.common.constants.AuthResponseCodeC3;
import com.usatech.layers.common.constants.ReceiptResult;
import com.usatech.layers.common.constants.SaleResult;
import com.usatech.layers.common.constants.SaleType;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageData_C2;
import com.usatech.layers.common.messagedata.MessageData_C2.GenericRawCardReader;
import com.usatech.layers.common.messagedata.MessageData_C3;
import com.usatech.layers.common.messagedata.MessageData_C3.AuthResponseTypeData;
import com.usatech.layers.common.messagedata.MessageData_C4;
import com.usatech.layers.common.messagedata.MessageData_C4.Format0LineItem;
import com.usatech.layers.common.messagedata.MessageDirection;
import com.usatech.layers.common.util.Vend3StatusCode;
import com.usatech.test.EMVTestMain.Parms;


public class VendXTxnManager {
	private static final Log log = Log.getLog();
	private static final BigDecimal ZERO = new BigDecimal("0.00");
	
	private Vend3 reader;
	private byte lastReaderCommandSent;
	private long lastTransactionId;
	
	private Map<Long, AuthResponseCodeC3>transactionIdToAuthResponseMap = new HashMap<>();
	
	public VendXTxnManager(Vend3 reader) {
		this.reader = reader;
	}

	public Vend3 getReader() {
		return reader;
	}
	
	public void messageSent(byte [] cmd) {
		byte cmdByte = IdtechVendXDataBlob.commandByteFromByteStream(cmd);
		setLastReaderCommandSent(cmdByte);
	}
	public void messageReceived(IdtechVendXDataBlob responseMsg) {
//		log.info("VendXTxnManager: responseMsg RECEIVED!");
		try {
			Vend3.Command command = responseMsg.getCmd();
			switch (command) {
				case ACTIVATE_TRANSACTION:
					activateTransactionReceived(responseMsg);
					break;
				case GET_TRANSACTION_RESULT:
					log.info("GET_TRANSACTION_RESULT msg received.");
					break;
				default:
					log.info("VendXTxnManager unhandled message");
					break;
			}
		} catch (InvalidByteValueException | IOException | SQLException | DataLayerException | ConvertException | InvalidValueException e) {
			e.printStackTrace();
		}
	}
	
	private void activateTransactionReceived(IdtechVendXDataBlob responseMsg) throws IOException, SQLException, DataLayerException, ConvertException, InvalidValueException {
		//log.info("status code: " + responseMsg.getStatusCode());
		switch (responseMsg.getStatusCodeByte()) {
			case Vend3StatusCode.REQUEST_ONLINE_AUTH:
			case Vend3StatusCode.REQUEST_ONLINE_AUTH_CONTACT_EMV_ONLY: {
				//EMVTestMain.executeAuthorize(responseMsg);
				AuthResponseCodeC3 responseCode = executeAuthorize(responseMsg);
				if (responseCode != null) {
					switch (responseCode) {
						case SUCCESS:
						case CONDITIONAL_SUCCESS:
							switch (responseMsg.getStatusCodeByte()) {
								case Vend3StatusCode.REQUEST_ONLINE_AUTH_CONTACT_EMV_ONLY:
									getReader().sendContinueTxnForOnlineAuthorizationSuccess();
									break;
								case Vend3StatusCode.REQUEST_ONLINE_AUTH:
									log.info("REQUEST_ONLINE_AUTH response to Vend3");
									MessageData reply = executeActualSaleForLastTransaction(SaleResult.SUCCESS, responseMsg.amountAuthorize());
									// TODO - jms should check result of sale before calling it a success - 1/20/16
									log.info("Sale Response: " + reply);
									getReader().sendDisplayAuthorizationSuccess();
									break;
								default:
									log.info("Should not happend.");
									break;
							}
							break;
						default:
							getReader().sendDisplayAuthorizationFailed();
							executeActualSaleForLastTransaction(SaleResult.CANCELLED_BY_AUTH_FAILURE, ZERO);
							break;
					}
				} else {
					log.info("AuthResponseCodeC3 is not available.  Sending failure to reader. Check connection to server.");
					getReader().sendDisplayAuthorizationFailed();
					executeActualSaleForLastTransaction(SaleResult.CANCELLED_BY_AUTH_TIMEOUT, ZERO);
				}
			}
			break;
			case Vend3StatusCode.REQUEST_ONLINE_PIN:
			case Vend3StatusCode.REQUEST_SIGNATURE: {
//					Map<Long, AuthResponseCodeC3>map = getTransactionIdToAuthResponseMap();
//					AuthResponseCodeC3 responseCode = map.get(getLastTransactionId());
//					if (responseCode == null) {
//						responseCode = executeAuthorize(responseMsg);
//						switch (responseCode) {
//						case SUCCESS:
//						case CONDITIONAL_SUCCESS:
//							getReader().sendContinueTransactionForCardholderVerification();
//							MessageData reply = executeActualSaleForLastTransaction(SaleResult.SUCCESS, responseMsg.amountAuthorize());
//							// TODO - jms should check result of sale before calling it a success - 1/20/16
//							log.info("Sale Response: " + reply);
//							getReader().sendDisplayAuthorizationSuccess();
//							break;
//						default:
//							getReader().sendDisplayAuthorizationFailed();
//							executeActualSaleForLastTransaction(SaleResult.CANCELLED_BY_AUTH_FAILURE, ZERO);
//							break;
//						
//						}
//					} else {
//						getReader().sendContinueTransactionForCardholderVerification();
//					}
				getReader().sendContinueTransactionForCardholderVerification();
			}
			break;
			case Vend3StatusCode.OK:
				AuthResponseCodeC3 authResponse = executeAuthorize(responseMsg);
				if (authResponse != null) {
					switch (authResponse) {
						case SUCCESS:
						case CONDITIONAL_SUCCESS: {
							MessageData reply = executeActualSaleForLastTransaction(SaleResult.SUCCESS, responseMsg.amountAuthorize());
							// TODO - jms should check result of sale before calling it a success - 1/20/16
							log.info("activateTransactionReceived() - case OK Sale Resposne: " + reply);
							getReader().sendDisplayAuthorizationSuccess();
						}
						break;
						default: {
							log.info("activateTransactionReceived() - case OK sending auth failed");
							getReader().sendDisplayAuthorizationFailed();
							executeActualSaleForLastTransaction(SaleResult.CANCELLED_BY_AUTH_FAILURE, ZERO);
						}
						break;
					}
				}
			break;
			default:
				log.info("activateTransactionReceived() Unhandled Status Code: " + responseMsg.getStatusCodeByte());
			break;
		}
	}
	
	public void startTransaction() {
		getReader().sendActivateTransaction2("100");
	}

	public void stopTransaction() {
	}

	protected interface LineItemAdder {
		public void addLineItem(int component, int itemType, String description, int duration, BigDecimal price, int quantity);

		public void complete();
	}

	protected class LineItemAdder_C4 implements LineItemAdder {
		protected final MessageData_C4 dataC4;

		public LineItemAdder_C4(MessageData_C4 dataC4) {
			this.dataC4 = dataC4;
			dataC4.setLineItemFormat((byte) 0);
		}

		@Override
		public void addLineItem(int component, int itemType, String description, int duration, BigDecimal price, int quantity) {
			Format0LineItem li = (Format0LineItem) dataC4.addLineItem();
			li.setComponentNumber(component);
			li.setItem(itemType);
			li.setDescription(description);
			li.setDuration(duration);
			li.setPrice(price);
			li.setQuantity(quantity);
			li.setSaleResult(SaleResult.SUCCESS);
		}

		@Override
		public void complete() {
		}
	}

	protected MessageData buildSaleC4(long transactionId, SaleType saleType, List<BigDecimal>itemPrice, SaleResult saleResult, boolean convFee) {
		MessageData_C4 md = new MessageData_C4();
		md.setBatchId(1);
		md.setDirection(MessageDirection.CLIENT_TO_SERVER);
		md.setReceiptResult(ReceiptResult.UNAVAILABLE);
		md.setSaleResult(saleResult);
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(transactionId * 1000);
		md.setSaleSessionStart(cal);
		md.setSaleType(saleType);
		md.setTransactionId(transactionId);
		LineItemAdder lia = new LineItemAdder_C4(md);
		BigDecimal total = addLineItems(lia, itemPrice, convFee);
		md.setSaleAmount(total);
		return md;
	}

	protected BigDecimal addLineItems(LineItemAdder lia, List<BigDecimal>itemPrice, boolean convFee) {
		BigDecimal total = BigDecimal.ZERO;
		int n = 0;
		for(int i = 0; i < itemPrice.size(); i++) {
			String desc;
			BigDecimal price;
			int quantity;
			int duration;
			switch(i % 3) {
				case 0:
					desc = "#25";
					duration = 2;
					break;
				case 1:
					desc = "#33";
					duration = 3;
					break;
				case 2:
				default:
					desc = "#27";
					duration = 7;
					break;
			}
			quantity = 1;
			price = itemPrice.get(i);
			lia.addLineItem(1, 200, desc, duration, price, quantity);
			total = total.add(price.multiply(BigDecimal.valueOf(quantity)));
			n += quantity;
		}
		if(convFee) {
			BigDecimal price = new BigDecimal(7);
			lia.addLineItem(1, 203, null, 0, price, n);
			total = total.add(price.multiply(BigDecimal.valueOf(n)));
		}
		lia.complete();
		return total;
	}

	private MessageData executeActualSaleForLastTransaction(SaleResult saleResult, BigDecimal amount) throws SQLException, DataLayerException, ConvertException {
		MessageData reply = null;
		
		Parms parms = Parms.getParmsInstance();
		if (parms != null) {
			ClientEmulator ce;
			ce = new ClientEmulator(parms.getHost(), parms.getSalePort(), parms.getDeviceName(), parms.getEncryptKey());
			
			List<BigDecimal>itemPriceList = new ArrayList<>();
			if (amount != null) {
				itemPriceList.add(amount);
			}
			MessageData message = buildSaleC4(getLastTransactionId(), SaleType.ACTUAL, itemPriceList, saleResult, false);
				
			int protocol = 7;
			StringBuilder details = new StringBuilder();
			long startTime = 0;
			try {
				ce.setMessageNumber(0);
				ce.startCommunication(protocol);
				details.setLength(0);
				details.append("Sent:\n").append(message);
				startTime = System.currentTimeMillis();
				reply = ce.sendMessage(message);
				
				details.append("\nReceived:\n").append(reply);
				ce.stopCommunication();
			} catch(Exception e) {
				long endTime = System.currentTimeMillis();
				if(startTime == 0)
					startTime = endTime;
				details.append("\nERROR:\n").append(e.getMessage());
				log.warn("Error during session", e);
			} finally {
//				log.info("authorize details: " + details.toString());
			}
		}
		return reply;
	}

	public AuthResponseCodeC3 executeAuthorize(IdtechVendXDataBlob msg) throws IOException, SQLException, DataLayerException, ConvertException, InvalidValueException {
		AuthResponseCodeC3 responseCode = null;
		BigDecimal amount = msg.amountAuthorize();
		
		//byte [] emvData = (msg.getEntryType() == EntryType.EMV_CONTACTLESS) ? msg.getEMVData().getEmvData() : msg.getContactEmvData();
		byte [] emvData = msg.getEMVData().getRawResponse();
		
		Parms parms = Parms.getParmsInstance();
		if (parms != null) {
			ClientEmulator ce;
			MessageData_C2 message;
			ce = new ClientEmulator(parms.getHost(), parms.getPort(), parms.getDeviceName(), parms.getEncryptKey());
			message = new MessageData_C2();
			message.setAmount(amount);
			message.setCardReaderType(msg.getCardReaderType());

			GenericRawCardReader crt = (GenericRawCardReader)message.getCardReader();
			crt.setRawData(emvData);
			
			message.setEntryType(msg.getEntryType());
			long transactionId = new Date().getTime() / 1000;
			setLastTransactionId(transactionId);
			message.setTransactionId(transactionId);
				
			log.info("emvData length: " + emvData.length);

			int protocol = 7;
			
			StringBuilder details = new StringBuilder();
			long startTime = 0;
			try {
				ce.setMessageNumber(0);
				ce.startCommunication(protocol);
				details.setLength(0);
				details.append("Sent:\n").append(message);
				startTime = System.currentTimeMillis();
				MessageData reply = ce.sendMessage(message);
				
				MessageData_C3 replyC3 = (MessageData_C3)reply;
				AuthResponseTypeData artd = replyC3.getAuthResponseTypeData();
				MessageData_C3.AuthorizationV2AuthResponse response = (MessageData_C3.AuthorizationV2AuthResponse)artd;
				
//				log.info("MessageType: " + reply.getMessageType());
//				log.info("getAuthResponseType: " + replyC3.getAuthResponseType());
//				log.info("getAuthResponseTypeData: " + artd);
				
				responseCode = response.getResponseCode();
//				
				getTransactionIdToAuthResponseMap().put(transactionId, responseCode);
				
				log.info("responseCode: " + responseCode);
				
//				CommandCodeData ccd = replyC3.getCommandCodeData();
//				log.info("getCommandCodeData: " + ccd);
				
				details.append("\nReceived:\n").append(reply);
//				log.debug(details);
				ce.stopCommunication();
			} catch(Exception e) {
				long endTime = System.currentTimeMillis();
				if(startTime == 0)
					startTime = endTime;
				details.append("\nERROR:\n").append(e.getMessage());
				log.warn("Error during session", e);
			} finally {
//				log.info("authorize details: " + details.toString());
			}
		}
		return responseCode;
	}

	public byte getLastReaderCommandSent() {
		return lastReaderCommandSent;
	}

	public void setLastReaderCommandSent(byte lastCommandSent) {
		this.lastReaderCommandSent = lastCommandSent;
	}

	public long getLastTransactionId() {
		return lastTransactionId;
	}

	public void setLastTransactionId(long lastTransactionId) {
		this.lastTransactionId = lastTransactionId;
	}

	public Map<Long, AuthResponseCodeC3> getTransactionIdToAuthResponseMap() {
		return transactionIdToAuthResponseMap;
	}
	
}
