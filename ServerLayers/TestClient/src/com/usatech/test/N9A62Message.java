package com.usatech.test;

import java.nio.ByteBuffer;

import simple.lang.InvalidByteValueException;

import com.usatech.networklayer.ReRixMessage;
/**
 * 9A62 Washer/Dryer Diagnostics -->2F
 * @author yhe
 *
 */
public class N9A62Message extends ESudsMessage {
	protected int washerDryerPortNumber;
	protected String washerDryerDiagnosticsByteBlock;
	public N9A62Message() {
		super();
		dataType2=0x62;
		washerDryerPortNumber=10;
		washerDryerDiagnosticsByteBlock="";
	}

	@Override
	public ReRixMessage createMessage() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		bb.put((byte)washerDryerPortNumber);
		bb.put(washerDryerDiagnosticsByteBlock.getBytes());
		return writeMessageEnd(bb);
	}

	@Override
	public String readResponseString(ReRixMessage msg) throws InvalidByteValueException {
		return readResponseString2F(msg);
	}

	public int getWasherDryerPortNumber() {
		return washerDryerPortNumber;
	}

	public void setWasherDryerPortNumber(int washerDryerPortNumber) {
		this.washerDryerPortNumber = washerDryerPortNumber;
	}

	public String getWasherDryerDiagnosticsByteBlock() {
		return washerDryerDiagnosticsByteBlock;
	}

	public void setWasherDryerDiagnosticsByteBlock(String washerDryerDiagnosticsByteBlock) {
		this.washerDryerDiagnosticsByteBlock = washerDryerDiagnosticsByteBlock;
	}

}
