package com.usatech.test;

import java.nio.ByteBuffer;

import simple.lang.InvalidByteValueException;

import com.usatech.networklayer.ReRixMessage;
/**
 * 86 Gx Counters --> 2F
 * @author yhe
 *
 */
public class N86Message extends LegacyMessage {
	protected long totalCurrencyTransactionCounter;
	protected long totalCurrencyMoneyCounter;
	protected long totalCashlessTransactionCounter;
	protected long totalCashlessMoneyCounter;
	protected long totalPasscardTransactionCounter;
	protected long totalPasscardMoneyCounter;
	protected long totalBytesTransmitted;
	protected long totalAttemptedSession;
	protected long reservedCountersSpace;

	public N86Message() {
		super();
		dataType=0x86;
		totalCurrencyTransactionCounter=10012;
		totalCurrencyMoneyCounter=10034;
		totalCashlessTransactionCounter=10056;
		totalCashlessMoneyCounter=10078;
		totalPasscardTransactionCounter=10090;
		totalPasscardMoneyCounter=10000;
		totalBytesTransmitted=10000;
		totalAttemptedSession=10000;
		reservedCountersSpace=10000;
	}

	
	@Override
	public ReRixMessage createMessage() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		writeMessageHeader(bb);
		bb.put(TestClientUtil.getBCDByteArrayFromNumber(totalCurrencyTransactionCounter));
		bb.put(TestClientUtil.getBCDByteArrayFromNumber(totalCurrencyMoneyCounter));
		bb.put(TestClientUtil.getBCDByteArrayFromNumber(totalCashlessTransactionCounter));
		bb.put(TestClientUtil.getBCDByteArrayFromNumber(totalCashlessMoneyCounter));
		bb.put(TestClientUtil.getBCDByteArrayFromNumber(totalPasscardTransactionCounter));
		bb.put(TestClientUtil.getBCDByteArrayFromNumber(totalPasscardMoneyCounter));
		bb.put(TestClientUtil.getBCDByteArrayFromNumber(totalBytesTransmitted));
		bb.put(TestClientUtil.getBCDByteArrayFromNumber(totalAttemptedSession));
		bb.put(TestClientUtil.getBCDByteArrayFromNumber(reservedCountersSpace));
		return writeMessageEnd(bb);
	}

	@Override
	public String readResponseString(ReRixMessage msg) throws InvalidByteValueException {
		return readResponseString2F(msg);
	}

	public long getTotalCurrencyTransactionCounter() {
		return totalCurrencyTransactionCounter;
	}

	public void setTotalCurrencyTransactionCounter(long totalCurrencyTransactionCounter) {
		this.totalCurrencyTransactionCounter = totalCurrencyTransactionCounter;
	}

	public long getTotalCurrencyMoneyCounter() {
		return totalCurrencyMoneyCounter;
	}

	public void setTotalCurrencyMoneyCounter(long totalCurrencyMoneyCounter) {
		this.totalCurrencyMoneyCounter = totalCurrencyMoneyCounter;
	}

	public long getTotalCashlessTransactionCounter() {
		return totalCashlessTransactionCounter;
	}

	public void setTotalCashlessTransactionCounter(long totalCashlessTransactionCounter) {
		this.totalCashlessTransactionCounter = totalCashlessTransactionCounter;
	}

	public long getTotalCashlessMoneyCounter() {
		return totalCashlessMoneyCounter;
	}

	public void setTotalCashlessMoneyCounter(long totalCashlessMoneyCounter) {
		this.totalCashlessMoneyCounter = totalCashlessMoneyCounter;
	}

	public long getTotalPasscardTransactionCounter() {
		return totalPasscardTransactionCounter;
	}

	public void setTotalPasscardTransactionCounter(long totalPasscardTransactionCounter) {
		this.totalPasscardTransactionCounter = totalPasscardTransactionCounter;
	}

	public long getTotalPasscardMoneyCounter() {
		return totalPasscardMoneyCounter;
	}

	public void setTotalPasscardMoneyCounter(long totalPasscardMoneyCounter) {
		this.totalPasscardMoneyCounter = totalPasscardMoneyCounter;
	}

	public long getTotalBytesTransmitted() {
		return totalBytesTransmitted;
	}

	public void setTotalBytesTransmitted(long totalBytesTransmitted) {
		this.totalBytesTransmitted = totalBytesTransmitted;
	}

	public long getTotalAttemptedSession() {
		return totalAttemptedSession;
	}

	public void setTotalAttemptedSession(long totalAttemptedSession) {
		this.totalAttemptedSession = totalAttemptedSession;
	}

	public long getReservedCountersSpace() {
		return reservedCountersSpace;
	}

	public void setReservedCountersSpace(long reservedCountersSpace) {
		this.reservedCountersSpace = reservedCountersSpace;
	}

}
