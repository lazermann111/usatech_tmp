package com.usatech.test;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.util.Calendar;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.CRCType;
import com.usatech.networklayer.ReRixMessage;

/**
 * File transfer start C8-->CB response
 * @author yhe
 *
 */
public class C8Message extends V4Message {
	protected static final int MAX_PAYLOAD=1024;
	protected Calendar timeOfCreation;
	protected long eventId;
	protected int filesRemaining;
	protected int numOfBlocksInFileTransfer;
	protected long totalNumOfBytesInFileTransfer;
	protected int fileType;
	protected String fileName;
	protected BufferedInputStream input;
	protected BufferedInputStream inputCRC;

	public C8Message() {
		super();
		dataType=0xC8;
		timeOfCreation=Calendar.getInstance();
		eventId=System.currentTimeMillis()/1000;
		filesRemaining=1;
		numOfBlocksInFileTransfer=1;
		totalNumOfBytesInFileTransfer=10;
		fileType=0;//dex
		fileName="testC8.txt";
	}
	public BufferedInputStream retrieveInputStream(){
		return input;
	}
	
	@Override
	public void getParams()throws Exception{
		super.getParams();
		File f;
		if(userInputTokens==null){
			f=CommandLineUtil.getFileTransfer(-1, null);
		}else{
			f=CommandLineUtil.getFileTransfer(-1, userInputTokens.nextToken());
		}
		fileName=f.getName();
		totalNumOfBytesInFileTransfer=f.length();
		numOfBlocksInFileTransfer=TestClientUtil.calcNumOfBlocks(MAX_PAYLOAD,totalNumOfBytesInFileTransfer);
		input=new BufferedInputStream(new FileInputStream(f));
		inputCRC=new BufferedInputStream(new FileInputStream(f));
		CommandLineUtil.pw.println("Successfully loaded file fileName:"+fileName);
	}
	@Override
	public ReRixMessage createMessage() throws Exception{
		int fileNameLen=fileName==null?0:fileName.length();
		ByteBuffer bb=ByteBuffer.allocate(25+fileNameLen+5);
		writeMessageHeader(bb);
		ProcessingUtils.writeTimestamp(bb,timeOfCreation);
		ProcessingUtils.writeLongInt(bb,eventId);
		ProcessingUtils.writeByteInt(bb,filesRemaining);
		ProcessingUtils.writeShortInt(bb,numOfBlocksInFileTransfer);
		ProcessingUtils.writeLongInt(bb,totalNumOfBytesInFileTransfer);
		ProcessingUtils.writeShortInt(bb,fileType);
		//add crc
		CRCType crcType=CRCType.CRC16Server;
		bb.put(crcType.getValue());
		if(input==null||inputCRC==null){
			throw new IllegalArgumentException("Input for file content cannot be null.");
		}
		bb.put(crcType.calculate(inputCRC));
		ProcessingUtils.writeShortString(bb,fileName, ProcessingConstants.US_ASCII_CHARSET);
		return writeMessageEnd(bb);
	}
	public long getEventId() {
		return eventId;
	}

	public void setEventId(long eventId) {
		this.eventId = eventId;
	}

	public int getFilesRemaining() {
		return filesRemaining;
	}

	public void setFilesRemaining(int filesRemaining) {
		this.filesRemaining = filesRemaining;
	}



	public int getFileType() {
		return fileType;
	}

	public void setFileType(int fileType) {
		this.fileType = fileType;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception{
		ClientConnectUtil conn=new ClientConnectUtil();
		conn.setProtocol(7);
		conn.startCommunication();
		C8Message sMsg=new C8Message();
		sMsg.getParams();
		ReRixMessage sentMsg=sMsg.createMessage();
		ReRixMessage responseMsg=conn.sendMessage(sentMsg,sMsg.encryptKey);
		sMsg.readResponse(responseMsg);
		conn.stopCommunication();
	}
}
