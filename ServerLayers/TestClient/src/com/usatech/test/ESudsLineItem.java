package com.usatech.test;

import java.nio.ByteBuffer;
import java.util.StringTokenizer;

import com.usatech.layers.common.ProcessingUtils;
/**
 * used by 9A5E
 * @author yhe
 *
 */
public class ESudsLineItem extends SubMessage {
	protected int portNumberOfLineItem;
	protected int topOrBottomStartedOnLineItem;
	protected int itemPurchasedInLineItem;
	protected int quantity;
	protected int amountOfLineItem;
	public ESudsLineItem(StringTokenizer userInputTokens) {
		super(userInputTokens);
		portNumberOfLineItem=1;
		topOrBottomStartedOnLineItem='B';
		itemPurchasedInLineItem=1;//washer regular
		quantity=1;
		amountOfLineItem=150;
	}

	@Override
	public void writeMessage(ByteBuffer bb) {
		bb.put((byte)portNumberOfLineItem);
		ProcessingUtils.writeByteInt(bb,topOrBottomStartedOnLineItem);
		ProcessingUtils.writeByteInt(bb,itemPurchasedInLineItem);
		ProcessingUtils.writeByteInt(bb,quantity);
		ProcessingUtils.writeShortInt(bb,amountOfLineItem);
	}

	public int getPortNumberOfLineItem() {
		return portNumberOfLineItem;
	}

	public void setPortNumberOfLineItem(int portNumberOfLineItem) {
		this.portNumberOfLineItem = portNumberOfLineItem;
	}

	public int getTopOrBottomStartedOnLineItem() {
		return topOrBottomStartedOnLineItem;
	}

	public void setTopOrBottomStartedOnLineItem(int topOrBottomStartedOnLineItem) {
		this.topOrBottomStartedOnLineItem = topOrBottomStartedOnLineItem;
	}

	public int getItemPurchasedInLineItem() {
		return itemPurchasedInLineItem;
	}

	public void setItemPurchasedInLineItem(int itemPurchasedInLineItem) {
		this.itemPurchasedInLineItem = itemPurchasedInLineItem;
	}

	public int getAmountOfLineItem() {
		return amountOfLineItem;
	}

	public void setAmountOfLineItem(int amountOfLineItem) {
		this.amountOfLineItem = amountOfLineItem;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

}
