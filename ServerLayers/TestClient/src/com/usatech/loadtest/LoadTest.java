package com.usatech.loadtest;

import static simple.text.MessageFormat.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.configuration.Configuration;

import simple.app.BaseWithConfig;
import simple.app.MainWithConfig;
import simple.app.MainWithConfig.Action;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.bean.ReflectionUtils.BeanProperty;
import simple.io.ByteArrayUtils;
import simple.io.Log;
import simple.text.StringUtils;
import simple.text.ThreadSafeDateFormat;
import simple.util.ConcurrentTreeMap;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.constants.*;
import com.usatech.layers.common.messagedata.*;
import com.usatech.layers.common.messagedata.MessageData_C2.GenericTracksCardReader;
import com.usatech.layers.common.messagedata.MessageData_C4.Format0LineItem;
import com.usatech.test.ClientEmulator;

public class LoadTest extends MainWithConfig implements Action {
	private static final Log log = Log.getLog();
	
	private static ThreadSafeDateFormat DATE_FORMAT = new ThreadSafeDateFormat(new SimpleDateFormat("MM/dd/yyyy HH:mm:ss"));

	public static void main(String[] args) {
		new LoadTest().run(args);
	}

	private int numThreads = 1;
	private int runTimeSeconds = 1;
	private String host = "127.0.0.1";
	private String outputDir = ".";
	private int connectTimeout = 5000;
	private int readTimeout = 5000;
	private boolean stopAllOnError = false;
	private boolean stopThreadOnError = false;
	private boolean stopOnConstantErrors = false;
	private long contantErrorTime = 5000;
	private long startDelayFactor = 200;
	private long statsInterval = 5000;
	
	private AtomicInteger threadCounter = new AtomicInteger(0);
	
	private ConcurrentTreeMap<Integer, DeviceData> devices = new ConcurrentTreeMap<>();
	private ConcurrentTreeMap<Integer, byte[]> messages = new ConcurrentTreeMap<>();
	private ConcurrentTreeMap<Integer, String> origMessages = new ConcurrentTreeMap<>();
	private StatsTracker statsTracker;
	private ThreadPoolExecutor threadPool;
	private Object lockObject = new Object();
	private boolean prematureStop = false;
	
	private AtomicLong lastSuccess = new AtomicLong(System.currentTimeMillis());
	private AtomicLong maxMessageId = new AtomicLong();
	
	private PrintWriter out;

	@Override
	protected void registerDefaultActions() {
		registerAction("start", this);
	}
	
	@Override
	protected void registerDefaultCommandLineArguments() {
		super.registerDefaultCommandLineArguments();
	}
	
	@Override
	protected void finishAndExit(String message, int exitCode, boolean printUsage, Throwable exception) {
		if (out != null) {
			try {
				out.close();
			} catch (Exception e) {
			}
		}
		super.finishAndExit(message, exitCode, printUsage, exception);
	}

	@Override
	public void perform(Map<String, Object> argMap, Configuration config) {
		try {
			BaseWithConfig.configureProperties(this, config, argMap, false);
		} catch (Exception e) {
			finishAndExit(e.getMessage(), 200, true, e);
			return;
		}

		if (devices.isEmpty()) {
			finishAndExit("No devices loaded from properties!", 202, true, null);
			return;
		}
			
		if (messages.isEmpty()) {
			finishAndExit("No messages loaded from properties!", 204, true, null);
			return;
		}

		log.info("Loaded {0} devices", devices.size());
		log.info("Loaded {0} messages", messages.size());

		if (devices.size() < numThreads) {
			finishAndExit(format("Not enough devices loaded for number of threads: {0} < {1}", devices.size(), numThreads), 206, true, null);
			return;
		}

		if (!outputDir.endsWith("/")) {
			outputDir += "/";
		}
		
		File file = new File(outputDir + format("LoadTest_{0}_{1}_{2}.csv", host, numThreads, new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date())));

		try {
			out = new PrintWriter(new FileOutputStream(file));
		} catch (FileNotFoundException e) {
			finishAndExit(e.getMessage(), 300, true, null);
			return;
		}
		
		try {
			out.println("# LOAD TEST STARTING " + new Date());
			out.println("# host = " + InetAddress.getLocalHost().getHostName());
			out.println("# results = " + file.getAbsolutePath());
		} catch (Exception e) {
			finishAndExit(e.getMessage(), 305, true, null);
			return;
		}
		
		try {
			Collection<BeanProperty> props = ReflectionUtils.getBeanProperties(LoadTest.class);
			for(BeanProperty bp : props) {
				try {
					if (bp.getName().equals("devices") || bp.getName().equals("messages"))
						continue;
					out.printf("# %s = %s\n", bp.getName(), bp.getValue(this));
				} catch (Exception e) {
					e.printStackTrace(out);
				}
			}
			for(Entry<Integer, String> e : origMessages.entrySet()) {
				out.printf("# MESSAGE[%s] = %s\n", e.getKey(), e.getValue());
			}
		} catch (Throwable e) {
			finishAndExit(e.getMessage(), 310, true, null);
			return;
		}
	
		out.println("# ");
		
		statsTracker = new StatsTracker("SentMessages", out, statsInterval, TimeUnit.MILLISECONDS);

		long st = System.currentTimeMillis();
		
		List<Future<Boolean>> results = new ArrayList<Future<Boolean>>(); 
		log.info("Starting up {0} threads for {1} seconds", numThreads, runTimeSeconds);
		threadPool = (ThreadPoolExecutor)Executors.newFixedThreadPool(numThreads);
		devices.values().stream().limit(numThreads).forEach(d -> results.add(threadPool.submit(new Runner(d))));

		synchronized (lockObject) {
			try {
				lockObject.wait(TimeUnit.SECONDS.toMillis(runTimeSeconds));
			} catch (InterruptedException e) {
				log.debug(e);
			}
		}

		log.info("Signaling threads to finish after {0} seconds", (System.currentTimeMillis() - st)/1000);
		threadPool.shutdown();
		try {
			threadPool.awaitTermination(10, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			threadPool.shutdownNow();
		}
		
		try {
			statsTracker.shutdown(10, TimeUnit.SECONDS);
		} catch (Exception e) {
			throw new RuntimeException("StatsTracker has not shutdown", e);
		}
		
		// one last run to clear the last stats
		statsTracker.run();
		
		long successCount = results.stream().filter(f -> { try { return f.get(); } catch(Exception e) { return false; }}).count();
		
		if (successCount == numThreads)
			log.info("PASS: load test complete, all threads finished successfully!");
		else
			log.info("FAIL: load test complete, {0}, {1} of {2} threads encountered an error while processing and did not finish the load test!", prematureStop ? "stopped due to an error" : "continued despite an error", (numThreads-successCount), numThreads);
		
		long currentMax = maxMessageId.get();
		if (currentMax > 0) {
			log.info("Max messageId was {0}, so you must wait until at least {1} to run the again with the same set of devices.", currentMax, new Date(currentMax*1000));
		}
	}
	
	protected void signalStop() {
		synchronized (lockObject) {
			prematureStop = true;
			lockObject.notify();
		}
	}
	
	public void setDevices(int index, String deviceInput) {
		String[] values = StringUtils.split(deviceInput, ' ', '"');
		if (values.length != 3) {
			finishAndExit(format("devices[{0}] is invalid: expecting 3 values", index), 230, true, null);
			return;
		}
		
		try {
			DeviceData deviceData = new DeviceData(values[0], ByteArrayUtils.fromHex(values[1]), values[2]);
			devices.put(index, deviceData);
		} catch (Exception e) {
			finishAndExit(format("devices[{0}] is invalid: {1}", index, e.toString()), 232, true, null);
		}
	}
	
	public void setMessages(int index, String messageInput) {
		try {
			String[] values = StringUtils.split(messageInput, ' ', '"');
			ByteBuffer msgBuffer = ByteBuffer.allocate(10240);
			switch (values[0]) {
				case "CA":
					MessageData_CA dataCa = new MessageData_CA();
					dataCa.setRequestType(ConvertUtils.convert(GenericRequestType.class, values[1]));
					dataCa.writeData(msgBuffer, false);
					break;
				case "CB":
					MessageData_CB dataCb = new MessageData_CB();
					dataCb.setResultCode(ConvertUtils.convert(GenericResponseResultCode.class, values[1]));
					dataCb.setClientActionCode(ConvertUtils.convert(GenericResponseClientActionCode.class, values[2]));
					dataCb.writeData(msgBuffer, false);
					break;
				case "C2":
					MessageData_C2 dataC2 = new MessageData_C2();
					dataC2.setAmount(ConvertUtils.convert(BigDecimal.class, values[1]));
					dataC2.setCardReaderType(ConvertUtils.convert(CardReaderType.class, values[2]));
					dataC2.setEntryType(ConvertUtils.convert(EntryType.class, values[3]));
					//dataC2.setTransactionId(transactionId);
					((GenericTracksCardReader)dataC2.getCardReader()).setAccountData2(values[4]);
					dataC2.writeData(msgBuffer, false);
					break;
				case "C4":
					MessageData_C4 dataC4 = new MessageData_C4();
					dataC4.setBatchId(1);
					dataC4.setLineItemFormat((byte)0);
					dataC4.setReceiptResult(ConvertUtils.convert(ReceiptResult.class, values[1]));
					BigDecimal amount = ConvertUtils.convert(BigDecimal.class, values[2]);
					dataC4.setSaleAmount(amount);
					SaleResult saleResult = ConvertUtils.convert(SaleResult.class, values[3]);
					dataC4.setSaleResult(saleResult);
					dataC4.setSaleType(ConvertUtils.convert(SaleType.class, values[4]));
					dataC4.setSaleSessionStart(Calendar.getInstance());
	
					if (saleResult == SaleResult.SUCCESS) {
						Format0LineItem item = (Format0LineItem)dataC4.addLineItem();
						item.setComponentNumber(1);
						item.setDescription("TEST");
						item.setDuration(2);
						item.setPrice(amount);
						item.setQuantity(1);
						item.setSaleResult(saleResult);
						item.setItem(200);
					}
	
					dataC4.writeData(msgBuffer, false);
					break;
				case "C1":
					MessageData_C1 dataC1 = new MessageData_C1();
					dataC1.setBaseComponentType(ComponentType.EPORT_EDGE_G9);
					dataC1.setCharset(ProcessingConstants.US_ASCII_CHARSET);
					dataC1.setDefaultCurrency(Currency.getInstance("USD"));
					dataC1.getBaseComponent().setLabel("0");
					dataC1.getBaseComponent().setManufacturer("USA Technologies");
					dataC1.getBaseComponent().setModel("Client Emulator V1.1");
					dataC1.getBaseComponent().setRevision("1.1");
					dataC1.getBaseComponent().setSerialNumber("TEST");
	
					Component comp = dataC1.addComponent();
					((MessageData_C1.ComponentData)comp).setComponentNumber(1);
					comp.setComponentType(ComponentType.VENDING_MACHINE);
					comp.setLabel("TE0001");
					comp.setManufacturer("Dell");
					comp.setModel("Precision");
					comp = dataC1.addComponent();
	
					((MessageData_C1.ComponentData)comp).setComponentNumber(2);
					comp.setComponentType(ComponentType.GPRS_MODEM);
					comp.setLabel("310380137444000");
					comp.setManufacturer("EMULATION");
					comp.setModel("EMU-1");
					comp.setRevision("01.01.001");
					comp.setSerialNumber("89310380106030979589");
	
					dataC1.writeData(msgBuffer, false);
					break;
	
				case "PAUSE":
					long ms = ConvertUtils.getLong(values[1]);
					msgBuffer.put((byte)0xFF);
					msgBuffer.putLong(ms);
					break;
				// add more types here
				default:
					finishAndExit(format("messages[{0}] is invalid: unhandled message type {1}", index, values[0]), 240, true, null);
					return;
			}
			msgBuffer.flip();
			byte[] msgBytes = new byte[msgBuffer.remaining()];
			msgBuffer.get(msgBytes);
			messages.put(index, msgBytes);
			origMessages.put(index, messageInput);
		}
		catch(ArrayIndexOutOfBoundsException e) {
			finishAndExit(format("messages[{0}] is invalid: insufficient values", index), 242, true, null);
		}
		catch (Exception e) {
			finishAndExit(format("messages[{0}] is invalid: {1}", index, e.toString()), 244, true, null);
		}
	}
	
	public int getNumThreads() {
		return numThreads;
	}

	public void setNumThreads(int numThreads) {
		this.numThreads = numThreads;
	}

	public int getRunTimeSeconds() {
		return runTimeSeconds;
	}

	public void setrunTimeSeconds(int runTimeSeconds) {
		this.runTimeSeconds = runTimeSeconds;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getOutputDir() {
		return outputDir;
	}

	public void setOutputDir(String outputDir) {
		this.outputDir = outputDir;
	}
	
	public int getConnectTimeout() {
		return connectTimeout;
	}
	
	public void setConnectTimeout(int connectTimeout) {
		this.connectTimeout = connectTimeout;
	}
	
	public int getReadTimeout() {
		return readTimeout;
	}

	public void setReadTimeout(int readTimeout) {
		this.readTimeout = readTimeout;
	}
	
	public boolean isStopThreadOnError() {
		return stopThreadOnError;
	}

	public void setStopThreadOnError(boolean stopThreadOnError) {
		this.stopThreadOnError = stopThreadOnError;
	}

	public boolean isStopAllOnError() {
		return stopAllOnError;
	}

	public void setStopAllOnError(boolean stopAllOnError) {
		this.stopAllOnError = stopAllOnError;
	}

	public boolean isStopOnConstantErrors() {
		return stopOnConstantErrors;
	}

	public void setStopOnConstantErrors(boolean stopOnConstantErrors) {
		this.stopOnConstantErrors = stopOnConstantErrors;
	}

	public long getContantErrorTime() {
		return contantErrorTime;
	}

	public void setContantErrorTime(long contantErrorTime) {
		this.contantErrorTime = contantErrorTime;
	}

	public long getStartDelayFactor() {
		return startDelayFactor;
	}

	public void setStartDelayFactor(long startDelayFactor) {
		this.startDelayFactor = startDelayFactor;
	}
	
	public long getStatsInterval() {
		return statsInterval;
	}
	
	public void setStatsInterval(long statsInterval) {
		this.statsInterval = statsInterval;
	}

	protected class DeviceData {
		private final String deviceName;
		private final byte[] encryptionKey;
		private final String serialNumber;
		
		public DeviceData(String deviceName, byte[] encryptionKey, String serialNumber) {
			this.deviceName = deviceName;
			this.encryptionKey = encryptionKey;
			this.serialNumber = serialNumber;
		}

		public String getDeviceName() {
			return deviceName;
		}

		public byte[] getEncryptionKey() {
			return encryptionKey;
		}

		public String getSerialNumber() {
			return serialNumber;
		}
		
		public String toString() {
			return format("{0}/{1}", deviceName, serialNumber);
		}
	}

	protected class Runner implements Callable<Boolean> {
		protected DeviceData deviceData;
		protected int threadId;

		protected Runner(DeviceData deviceData) {
			this.deviceData = deviceData;
			threadId = threadCounter.getAndIncrement();
		}

		@Override
		public Boolean call() {
			if (startDelayFactor > 0) {
				try {
					TimeUnit.MILLISECONDS.sleep(startDelayFactor * threadId);
				} catch (InterruptedException e) {
					e.printStackTrace(System.out);
				}
			}

			log.info("{0} running", deviceData);

			long tranactionId = System.currentTimeMillis() / 1000;
			ClientEmulator ce = null;
			int loopCount = 0;
			int threadMessageCount = 0;
			int errorCount = 0;
			
			while (!threadPool.isShutdown()) {
				long startTime = System.currentTimeMillis();
				long lastMessageId = 0;
				try {
					for (byte[] msgBytes: messages.values()) {
						if (ByteArrayUtils.readUnsignedByte(msgBytes, 0) == 0xFF) {
							long ms = ByteArrayUtils.readLong(msgBytes, 1);
							Thread.sleep(ms);
						} else {
							int port = 14108;
							MessageData request = MessageDataFactory.readMessage(ByteBuffer.wrap(msgBytes), MessageDirection.CLIENT_TO_SERVER);
							Thread.yield();
							if (request instanceof MessageData_C2) {
								((MessageData_C2)request).setTransactionId(++tranactionId);
								port = 14109;
							} else if (request instanceof MessageData_C4) {
								if (((MessageData_C4)request).getSaleType() == SaleType.ACTUAL)
									((MessageData_C4)request).setTransactionId(tranactionId); // use last transaction id
								else((MessageData_C4)request).setTransactionId(++tranactionId); // use new transaction id
								((MessageData_C4)request).setSaleSessionStart(Calendar.getInstance());
							} else if (request instanceof MessageData_C1) {
								((MessageData_C1)request).getBaseComponent().setSerialNumber(deviceData.getSerialNumber());
							}
							
							if (ce == null || ce.getPort() != port || port == 14109) {
								if (ce != null && ce.isConnected())
									ce.stopCommunication();
								ce = new ClientEmulator(host, port, deviceData.getDeviceName(), deviceData.getEncryptionKey(), ce != null ? ce.getMessageId() : (System.currentTimeMillis()/1000));
								ce.startCommunication(8, connectTimeout, readTimeout, readTimeout);
								Thread.yield();
							}

							// messageId and messageNumber will be set by ClientEmulator
							// there may be other messages types that need dynamic modification here

							threadMessageCount++;
							MessageData response = ce.sendMessage(request);
							if (response != null) {
								log.debug("Sent id={0}, number={1}, type={2}, Received: id={3}, number={4}, type={5}", request.getMessageId(), request.getMessageNumber(), request.getMessageType(), response.getMessageId(), response.getMessageNumber(), response.getMessageType());
								lastMessageId = request.getMessageId();
							}
							
							lastSuccess.set(System.currentTimeMillis());
							if (prematureStop)
								return true;
						}
					}
					
					long endTime = System.currentTimeMillis();
					long et = (endTime - startTime);
					String details = format("PASS: device:{0}, loop:{1}, messages:{2}, et:{3}, totalMessages:{4}", deviceData, loopCount, messages.size(), et, threadMessageCount);
					statsTracker.addStats(true, messages.size(), et);
					log.debug(details);
				} catch (Exception e) {
					errorCount++;
					long endTime = System.currentTimeMillis();
					long et = (endTime - startTime);
					String details = format("FAIL: device:{0}, loop:{1}, messages:{2}, et:{3}, totalMessages:{4}, exception:{5}", deviceData, loopCount, messages.size(), et, threadMessageCount, e.toString());
					statsTracker.addStats(false, messages.size(), et);
					log.warn(details, e);
					if (stopAllOnError || (stopOnConstantErrors && ((System.currentTimeMillis() - lastSuccess.get()) > connectTimeout))) {
						signalStop();
						return false;
					} else if (stopThreadOnError) {
						return false;
					}
				} finally {
					while(true) {
						long currentMax = maxMessageId.get();
						if (lastMessageId > currentMax) {
							if(!maxMessageId.compareAndSet(currentMax, lastMessageId))
								continue;
						}
						break;
					}
					
				}
				
				loopCount++;
			}
			
			if (ce != null && ce.isConnected()) {
				try {
					ce.stopCommunication();
				} catch (Exception e) { 
					log.debug(e);
				}
			}
			
			log.info("{0} finished: {1} loops, {2} message", deviceData, loopCount, threadMessageCount);
			return errorCount == 0;
		}
	}
	
	public static class IntervalStats {
		private final String type;
		private final int loopNumber;
		private final long timestamp;
		
		private AtomicInteger taskCount = new AtomicInteger(0);
		private AtomicInteger failCount = new AtomicInteger(0);
		private AtomicInteger messageCount = new AtomicInteger(0);
		private AtomicLong totalET = new AtomicLong(0);
		
		public IntervalStats(String type, int loopNumber) {
			this.type = type;
			this.loopNumber = loopNumber;
			this.timestamp = System.currentTimeMillis();
		}
		
		public void addStats(boolean success, int messages, long et) {
			if (!success)
				failCount.incrementAndGet();
			taskCount.incrementAndGet();
			messageCount.addAndGet(messages);
			totalET.addAndGet(et);
			System.out.printf("loopNumber=%s, messageCount=%s, et=%s, total=%s\n", loopNumber, messageCount.get(), et, totalET);
		}
		
		public String getType() {
			return type;
		}
		
		public int getLoopNumber() {
			return loopNumber;
		}
		
		public long getTimestamp() {
			return timestamp;
		}
		
		public int getTaskCount() {
			return taskCount.get();
		}
		
		public int getFailCount() {
			return failCount.get();
		}
		
		public int getMessageCount() {
			return messageCount.get();
		}
		
		public long getTotalET() {
			return totalET.get();
		}
	}
	
	public static class StatsTracker implements Runnable {
		private String type;
		private PrintWriter out;

		private long startTime;
		private AtomicInteger loopCounter = new AtomicInteger(1);
		private AtomicReference<IntervalStats> statsRef;
		
		private ScheduledExecutorService executorService = Executors.newScheduledThreadPool(3);

		public StatsTracker(String type, PrintWriter out, long interval, TimeUnit unit) {
			this.type = type;
			this.out = out;
			out.printf("\"Task\",\"Timestamp\",\"Time Offset\",\"Loop Number\",\"Task Count\",\"Fail Count\",\"Message Count\",\"Total ET\"\n");
			statsRef = new AtomicReference<IntervalStats>(new IntervalStats(type, loopCounter.getAndIncrement()));
			this.startTime = System.currentTimeMillis();
			executorService.scheduleAtFixedRate(this, interval, interval, unit);
		}
		
		public String getType() {
			return type;
		}
		
		public void addStats(boolean success, int messages, long et) {
			statsRef.get().addStats(success, messages, et);
		}
		
		public void run() {
			IntervalStats oldStats = statsRef.getAndSet(new IntervalStats(type, loopCounter.getAndIncrement()));
			if (oldStats != null) {
				long offset = oldStats.getTimestamp() - startTime;
				out.printf("\"%s\",\"%s\",%s,%s,%s,%s,%s,%s\n", type, DATE_FORMAT.format(new Date(oldStats.getTimestamp())), offset, oldStats.getLoopNumber(), oldStats.getTaskCount(), oldStats.getFailCount(), oldStats.getMessageCount(), oldStats.getTotalET());
				out.flush();
			}
		}
		
		public void shutdown(long timeout, TimeUnit unit) throws InterruptedException {
			executorService.shutdown();
			executorService.awaitTermination(startTime, unit);
		}
	}
}
