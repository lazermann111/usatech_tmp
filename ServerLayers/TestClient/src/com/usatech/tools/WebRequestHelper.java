package com.usatech.tools;

import java.io.IOException;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.URI;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;

import simple.swt.UIUtils;

public class WebRequestHelper {
	protected String env = null;

	public String getDMSUrl() {
		if(env == null)
			return "http://localhost:8780/";
		else if("USA".equals(env))
			return "https://dms.usatech.com/";
		else
			return "https://dms-" + env.toLowerCase() + ".usatech.com/";
	}

	public String getEnv() {
		return env;
	}

	public void setEnv(String env) {
		this.env = env;
	}

	public HttpClient loginDMS() throws HttpException, IOException {
		HttpClient client = new HttpClient();
		PostMethod method = new PostMethod(getDMSUrl() + "logon.i");
		String username = System.getProperty("dmsUserName", System.getProperty("user.name").toUpperCase());
		method.addParameter("userName", username);
		method.addParameter("credential", UIUtils.promptForPassword("Enter the DMS password for " + username, "DMS Login", null));
		int rc = client.executeMethod(method);
		switch(rc) {
			case 200:
			case 302:
				return client;
		}
		throw new IOException("Couldn't login to DMS because response code is " + rc);
	}

	public static HttpMethod executeFollowRedirects(HttpClient client, PostMethod postMethod) throws HttpException, IOException, AssertionError {
		HttpMethod method = postMethod;
		while(true) {
			int rc = client.executeMethod(method);
			switch(rc) {
				case 200: // success
					return method;
				case 302: // re-direct
					Header locationHeader = method.getResponseHeader("location");
					if(locationHeader != null) {
						String location = locationHeader.getValue();
						HttpMethod getMethod = new GetMethod();
						getMethod.setURI(new URI(method.getURI(), location, false));
						method = getMethod;
					}
					break;
				default:
					assert false : "Response code is " + rc;
			}
		}
	}
}
