package com.usatech.tools;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;
import java.util.Map.Entry;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationUtils;
import org.apache.commons.configuration.FileSystem;
import org.junit.Assert;
import org.junit.Test;

import simple.app.Base;
import simple.app.BaseWithConfig;
import simple.app.MainWithConfig;
import simple.db.*;
import simple.db.config.ConfigLoader;
import simple.io.ConfigSource;
import simple.io.LoadingException;
import simple.io.Log;
import simple.text.StringUtils;

public class DataLayerTests {

	static {
		System.setProperty("app.servicename", "DataLayerTests");
		System.setProperty("app.instance", "1");
		System.setProperty("user.country", "US");
		System.setProperty("file.encoding", "ISO8859-1");
		
		System.setProperty("javax.net.ssl.trustStore", "../LayersCommon/conf/net/truststore.ts");
		System.setProperty("javax.net.ssl.trustStorePassword", "usatech");
		System.setProperty("javax.xml.parsers.SAXParserFactory", "com.sun.org.apache.xerces.internal.jaxp.SAXParserFactoryImpl");
		
		System.setProperty("log4j.configuration", "log4j.properties");
	}
	
	private static Log log = Log.getLog();
	
	// Specify the name of app config properties files to process.
	// The tester will extract data layer files from the app properties and filter out redundant references.
	private static String[] APP_CONFIGS = {
//		"DMS.properties",
		"AppLayerService.properties",
		"LoaderService.properties",
		"InsideAuthorityLayerService.properties",
		"OutsideAuthorityLayerService.properties",
////		"KeyManagerService.properties",
		"POSMLayerService.properties",
		"prepaid.properties",
		"ReportGeneratorService.properties",
		"ReportRequesterService.properties",
		"TransportService.properties",
////		"RDWLoader.properties",
		"usalive.properties"
	};
	
	private static List<String> EXCLUDE_CALL_IDS = Arrays.asList("SCAN_FOR_SERVICE_FEES");
	private static List<String> EXCLUDE_FILE_NAMES = Arrays.asList("DESIGN-DATA-LAYER.XML");
	
	private boolean testShared = true;

	public static void main(String args[]) throws Exception {
		new DataLayerTests().testCalls();
	}
	
	public static class CallResult implements Comparable<CallResult> {
		
		private String appConfigName;
		private String dataLayerFileName;
		private boolean loadedByPath;
		private String callId;
		private boolean pass;
		private Throwable t;
		
		private String identifier;
		
		public CallResult(String appConfigName, String dataLayerFileName, boolean loadedByPath, String callId) {
			this.appConfigName = appConfigName;
			this.dataLayerFileName = dataLayerFileName;
			this.loadedByPath = loadedByPath;
			this.callId = callId;
			this.pass = true;
			
			createIdentifier();
		}
		
		public CallResult(String appConfigName, String dataLayerFileName, boolean loadedByPath, String callId, Throwable t) {
			this(appConfigName, dataLayerFileName, loadedByPath, callId);
			this.pass = false;
			this.t = t;
		}
		
		private void createIdentifier() {
			if (appConfigName.indexOf('.') > 0)
				appConfigName = appConfigName.substring(0, appConfigName.indexOf('.'));
			
				
			if (dataLayerFileName.indexOf(':') > 0)
				dataLayerFileName = dataLayerFileName.substring(dataLayerFileName.indexOf(':')+1);
			
			// kludge because these 2 run from the same directory and load datasource files by path, but don't have the same database permissions
			// there's probably a better way to do this
			if (dataLayerFileName.equals("rgr-data-layer.xml")) {
				appConfigName = "ReportGeneratorService";
				loadedByPath = true;
			}
			if (dataLayerFileName.equals("applayer-data-layer.xml")) {
				appConfigName = "AppLayerService";
				loadedByPath = true;
			}
			
			identifier = String.format("%s:%s:%s", loadedByPath ? appConfigName: "shared", dataLayerFileName, callId);
		}
		
		public void setFailure(Throwable t) {
			this.pass = false;
			this.t = t;
		}
		
		public String getAppConfigName() {
			return appConfigName;
		}
		
		public String getDataLayerFileName() {
			return dataLayerFileName;
		}
		
		public String getCallId() {
			return callId;
		}
		
		public boolean isPass() {
			return pass;
		}
		
		public Throwable getThrowable() {
			return t;
		}
		
		public String getIdentifier() {
			return identifier;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((identifier == null) ? 0 : identifier.hashCode());
			result = prime * result + (pass ? 1231 : 1237);
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			CallResult other = (CallResult)obj;
			if (identifier == null) {
				if (other.identifier != null)
					return false;
			} else if (!identifier.equals(other.identifier))
				return false;
			if (pass != other.pass)
				return false;
			return true;
		}

		@Override
		public String toString() {
			//return String.format("CallResult[identifier=%s, pass=%s, t=%s]", identifier, pass, t);
			return String.format("%s: %s", identifier, formatMessage(t));
		}

		@Override
		public int compareTo(CallResult o) {
			return identifier.compareTo(o.identifier);
		}
	}
	
	private Map<String, CallResult> callResults = new LinkedHashMap<>();
	private Map<String, Set<IndexedCall>> callIndex = new HashMap<String, Set<IndexedCall>>();
	
	private static class IndexedCall {
		public String source;
		public String callId;
		public String sql;
		
		public IndexedCall(String source, String callId, String sql) {
			this.source = source;
			this.callId = callId;
			this.sql = sql;
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((sql == null) ? 0 : sql.hashCode());
			return result;
		}
		
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			IndexedCall other = (IndexedCall)obj;
			if (sql == null) {
				if (other.sql != null)
					return false;
			} else if (!sql.equals(other.sql))
				return false;
			return true;
		}
		
		@Override
		public String toString() {
			return source;
		}
		
	}

	private void addResult(CallResult result) {
		CallResult oldResult = callResults.get(result.getIdentifier());
		// TODO: if another error, record both errors - the second could be more signifigant
		if (oldResult == null || !oldResult.isPass() && result.isPass()) {
			if (oldResult != null)
				System.out.printf("%s Overwriting old ERROR result with success!\n", result.getIdentifier());
			callResults.put(result.getIdentifier(), result);
		}
	}
	
	private void addToIndex(CallResult result, String sql) {
		String source = result.getDataLayerFileName();
		if (source.indexOf('/') > 0)
			source = source.substring(source.lastIndexOf('/')+1);
		
		String callId = result.getCallId();
		
		Set<IndexedCall> calls = callIndex.get(callId);
		if (calls == null) {
			calls = new HashSet<IndexedCall>();
			callIndex.put(callId, calls);
		}
		
		calls.add(new IndexedCall(source, callId, sql));
	}

	
	private boolean hasAlreadyPassed(CallResult result) {
		CallResult oldResult = callResults.get(result.getIdentifier());
		return (oldResult != null && oldResult.isPass());
	}
	
	@Test
	public void testCalls() throws Exception {
		try {
			for(String appConfigName : APP_CONFIGS) {
				URL appConfigUrl = ConfigurationUtils.locate(FileSystem.getDefaultFileSystem(), null, appConfigName);
				
				String resoureBase = appConfigUrl.toString().substring(0, appConfigUrl.toString().lastIndexOf('/')+1);
				Configuration appConfig = MainWithConfig.loadConfig(appConfigName, null, null);
				
				DataSourceFactory dsf = null;
				Configuration dataSourceConfig = appConfig.subset("simple.db.datasource");
				if(dataSourceConfig != null && !dataSourceConfig.isEmpty()) {
					dsf = BaseWithConfig.configure(DataSourceFactory.class, dataSourceConfig, null, true, LocalDataSourceFactory.class);
					Thread.sleep(10000); // wait a few seconds for dynamic connections to load (we could set dynamicLoading = false)
				}
				
				String[] dataLayerFileNames = StringUtils.split(appConfig.getString(Base.DATA_LAYER_FILES_PROPERTY), StringUtils.STANDARD_DELIMS, false);
				if(dataLayerFileNames == null || dataLayerFileNames.length == 0)
					continue;
				
				for(String dataLayerFileName : dataLayerFileNames) {
					dataLayerFileName = dataLayerFileName.trim();
					if (EXCLUDE_FILE_NAMES.contains(dataLayerFileName.toUpperCase())) {
						continue;
					}
					
					
					DataLayer dataLayer = new DataLayer();
					dataLayer.setDataSourceFactory(dsf);
					
					// during loading the of the XML, type lookup/conversion of an ARRAY type needs a database connection 
					// which it gets from Call.getConnectionHolder which use the globalDataLayer, so DataLayerMgr.setDataSourceFactory 
					// must be set before the config is loaded
					DataLayerMgr.setDataSourceFactory(dsf);
					
					boolean loadedByPath = false;
					String dataLayerFilePath = resoureBase + dataLayerFileName;
					
					try {
						try {
							ConfigLoader.loadConfig(ConfigSource.createConfigSource(dataLayerFilePath), dataLayer);
							loadedByPath = true;
						} catch (LoadingException e) {
							if (!testShared) {
								System.out.printf("Skipping shared %s\n", dataLayerFileName);
								continue;
							}
							ConfigLoader.loadConfig(ConfigSource.createConfigSource(dataLayerFileName), dataLayer);
							loadedByPath = false;
						}
					} catch (Throwable e) {
						System.out.printf("Failed to load %s\n", dataLayerFileName);
						//e.printStackTrace(System.out);
						addResult(new CallResult(appConfigName, dataLayerFileName, false, "ALL", e));
						continue;
					}
					
					System.out.printf("Testing calls in file %s\n", dataLayerFileName);

					CallSource callSource = dataLayer.getCallSource();
					for(String callId : callSource.getCallIds()) {
						if (EXCLUDE_CALL_IDS.contains(callId.toUpperCase())) {
							System.out.printf("Skipping excluded call '%s'\n", callId);
							continue;
						}
						
						CallResult callResult = new CallResult(appConfigName, dataLayerFileName, loadedByPath, callId);
						if (hasAlreadyPassed(callResult)) {
							System.out.printf("%s has already passed tests\n", callResult.getIdentifier());
							continue;
						}
						
						Call call = callSource.getCall(callId);
						Argument[] args = call.getArguments();
						
						addToIndex(callResult, call.getSql());

						Map<String, Object> in = new HashMap<String, Object>();
						Map<String, Object> out = new HashMap<String, Object>();
						Cursor results = null;
						
						for(Argument arg : args) {
							if (arg instanceof Cursor) {
								results = (Cursor) arg;
							} else {
								if (arg.isIn()) {
									Class<?> type = arg.getJavaType();
									Object val = getDummyValue(type);
									if (val == null) {
										System.out.printf("Unhandled parameter type '%s' for '%s' in call '%s'\n", arg.getPropertyName(), call.getLabel());
									}
									
									in.put(arg.getPropertyName(), val);
								} else {
									out.put(arg.getPropertyName(), null);
								}
							}
						}
						
						System.out.printf("  Testing %s...", callId);
						
						Connection connection = null;
						try {
							connection = dataLayer.getConnectionForCall(callId, false);
							call.executeCall(connection, in, out, null);
							System.out.printf("OK\n");
						} catch (SQLException e) {
							String sqlState = e.getSQLState();
							if (sqlState == null) {
								callResult.setFailure(e);
							} else {
								switch(sqlState) {
									case "23000": // integrity constraint violation
									case "72000": // usually custom stored proc error
									case "02000": // no data found
									case "22008": // invalid date conversation
									case "22012": // divide by zero
										System.out.printf("OK: SQLState=%s, Error=%s\n", e.getSQLState(), formatMessage(e));
										break;
									case "65000": // PL/SQL error
									case "42000": // syntax error or access rule violation
										System.out.printf("ERROR: SQLState=%s, Error=%s\n", e.getSQLState(), formatMessage(e));
										callResult.setFailure(e);
										break;
									// TODO: handle postgresql errors?
									default: // unhandled 
										System.out.printf("UNHANDLED: SQLState=%s\n%s\n", e.getSQLState(), e.getMessage());
										callResult.setFailure(e);
								}
							}
							//e.printStackTrace();
						} catch(Exception e) {
							System.out.printf("ERROR (%s)\n", formatMessage(e));
							callResult.setFailure(e);
						} finally {
							if (connection != null) {
								try {
									connection.rollback();
								} catch (Exception e) {
									e.printStackTrace();
								}
								connection.close();
							}
						}
						
						addResult(callResult);
					}
				}
				
				dsf.close();
			}
			
			for(Entry<String,Set<IndexedCall>> entry : callIndex.entrySet()) {
				if (entry.getValue().size() > 1) {
					System.out.printf("Warning: callId %s detected in more than one file: %s!\n", entry.getKey(), entry.getValue());
				}
			}
			
			long errorCount = callResults.values().stream().filter(cr -> !cr.isPass()).count();
			if (errorCount == 0) {
				System.out.printf("PASS\n");
			}

			System.out.printf("Detected %s errors!\n", errorCount);
			callResults.values().stream().filter(cr -> !cr.isPass()).sorted().forEach(cr -> System.out.println(cr));
			
		} catch (Throwable e) {
			e.printStackTrace();
		}
		
		Assert.assertTrue(false);
	}
	
	public static String formatMessage(Throwable e) {
		StringBuilder msg = new StringBuilder();
		Throwable current = e;
		boolean first = true;
		while(current != null) {
			Scanner scanner = new Scanner(current.getMessage());
			while(scanner.hasNextLine()) {
				String line = scanner.nextLine();
				if (!first)
					msg.append(", ");
				msg.append(line);
				first = false;
			}
			scanner.close();
			current = current.getCause();
		}
		
		return msg.toString();
		
	}
	
	private static Object getDummyValue(Class<?> type) {
		if (type == null) {
			//System.out.printf("No type for parameter %s for call %s\n", arg.getPropertyName(), call.getLabel());
			return null;
		} else if (type.isAssignableFrom(Number.class))
			return 1;
		else if (type.isAssignableFrom(BigInteger.class))
			return BigInteger.valueOf(1);
		else if (type.isAssignableFrom(BigDecimal.class))
			return BigDecimal.valueOf(1);
		else if (type.isAssignableFrom(String.class))
			return "1";
		else if (type.isAssignableFrom(Date.class))
			return new Date();
		else if (type.getName().equals("byte") || type.getName().equals("int") || type.getName().equals("long") || type.getName().equals("float") || type.getName().equals("double"))
			return 1;
		else if (type.getName().equals("boolean"))
			return false;
		else if (type.isArray()) {
			return new Object[] {getDummyValue(type.getComponentType())};
		}
		else {
			System.out.printf("No dummy value handling for type '%s'\n", type.getName());
			return null;
		}
		
	}

}
