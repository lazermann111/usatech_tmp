package com.usatech.tools;

import java.beans.IntrospectionException;
import java.net.MalformedURLException;
import java.text.Format;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.axis2.AxisFault;

import com.caucho.hessian.client.HessianProxyFactory;
import com.usatech.ec.ECServiceAPI;
import com.usatech.ec.EcStub;
import com.usatech.ec2.EC2AuthResponse;
import com.usatech.ec2.EC2Response;
import com.usatech.ec2.EC2ServiceAPI;
import com.usatech.ec2.Ec2Stub;
import com.usatech.layers.common.constants.AEAdditionalDataType;
import com.usatech.layers.common.constants.AuthResponseCodeA1;
import com.usatech.layers.common.constants.AuthResponseCodeAF;
import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.CardType;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.PaymentActionType;
import com.usatech.layers.common.constants.SaleResult71;
import com.usatech.layers.common.constants.TranDeviceResultType;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageData_2A;
import com.usatech.layers.common.messagedata.MessageData_71;
import com.usatech.layers.common.messagedata.MessageData_A0;
import com.usatech.layers.common.messagedata.MessageData_A1;
import com.usatech.layers.common.messagedata.MessageData_AE;
import com.usatech.layers.common.messagedata.MessageData_AF;
import com.usatech.test.ClientEmulator;

import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.lang.Initializer;
import simple.text.StringUtils;

public class ClientEmulatorHelper {
	private static final Log log = Log.getLog();
	protected static final Format EXPIRATION_DATE_FORMAT = ConvertUtils.getFormat("DATE", "yyMM");
	protected static final Initializer<Exception> dataLayerInit = new Initializer<Exception>() {
		@Override
		protected void doInitialize() throws Exception {
			Setup.setupDataSource_USADEV04();
			Setup.setupDataLayerFile("test-data-layer.xml");
		}

		@Override
		protected void doReset() {
		}
	};
	protected final AtomicLong masterId = new AtomicLong();
	protected String env = null;
	protected long nextMasterId() {
		long next = System.currentTimeMillis() / 1000;
		while(true) {
			long current = masterId.get();
			if(next <= current)
				next = current + 1;
			if(masterId.compareAndSet(current, next))
				return next;
		}
	}

	public static void initDataLayer() throws InterruptedException, Exception {
		dataLayerInit.initialize();
	}
	public String formatExpirationDate(int addMonths) {
		Calendar cal = Calendar.getInstance();
		if(addMonths != 0)
			cal.add(Calendar.MONTH, addMonths);
		return EXPIRATION_DATE_FORMAT.format(cal.getTime());
	}

	public void hessianChargeManual(String serialNumber, String cardNumber, String expirationDate, String cvv, String postal, long amount, int expectedResultCode) throws Exception {
		// <card number>|<card expiration date>|<card security code>|<card holder name>|<card holder zip code>|<card holder address>
		StringBuilder sb = new StringBuilder();
		sb.append(cardNumber).append('|');
		sb.append(expirationDate).append('|'); // expDate - YYMM
		sb.append(cvv).append('|');// cvv
		sb.append("").append('|'); // name
		sb.append(postal).append('|');// postal
		sb.append(""); // address
		String cardData = sb.toString();
		EC2AuthResponse response = hessianCharge(serialNumber, EntryType.MANUAL, cardData, amount, "requireCVVMatch=false\nrequireAVSMatch=false\n");
		if(response.getReturnCode() != expectedResultCode)
			throw new Exception("Return code " + response.getReturnCode() + " did not matched expected " + expectedResultCode);
	}

	public void hessianChargeSwipe(String serialNumber, String cardData, long amount, int expectedResultCode) throws Exception {
		EC2AuthResponse response = hessianCharge(serialNumber, EntryType.SWIPE, cardData, amount, null);
		if(response.getReturnCode() != expectedResultCode)
			throw new Exception("Return code " + response.getReturnCode() + " did not matched expected " + expectedResultCode);
	}

	public void hessianAuthSaleSwipe(String serialNumber, String cardData, long authAmount, long saleAmount, int authExpectedResultCode, int saleExpectedResultCode) throws Exception {
		EC2AuthResponse authResponse = hessianAuth(serialNumber, EntryType.SWIPE, cardData, authAmount, null);
		if(authResponse.getReturnCode() != authExpectedResultCode)
			throw new Exception("Auth Return code " + authResponse.getReturnCode() + " did not matched expected " + authExpectedResultCode);
		EC2Response saleResponse = hessianSale(serialNumber, saleAmount, null);
		if(saleResponse.getReturnCode() != saleExpectedResultCode)
			throw new Exception("Sale Return code " + saleResponse.getReturnCode() + " did not matched expected " + saleExpectedResultCode);
	}

	public EC2AuthResponse hessianCharge(String serialNumber, EntryType entryType, String cardData, long amount, String attributes) throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = nextMasterId();
		String tranResult = "S";
		String tranDetails = "A0|200|" + amount + "|1|Test Item";
		EC2AuthResponse response = ec.chargePlain(null, null, serialNumber, tranId, amount, cardData, String.valueOf(entryType.getValue()), tranResult, tranDetails, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
		return response;
	}

	public EC2AuthResponse hessianAuth(String serialNumber, EntryType entryType, String cardData, long amount, String attributes) throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = nextMasterId();
		EC2AuthResponse response = ec.authPlain(null, null, serialNumber, tranId, amount, cardData, String.valueOf(entryType.getValue()), attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
		return response;
	}

	public EC2Response hessianSale(String serialNumber, long amount, String attributes) throws Exception {
		EC2ServiceAPI ec = getEC2Service();
		long tranId = masterId.get();
		String tranResult = "S";
		String tranDetails = "A0|200|" + amount + "|1|Test Item";
		EC2Response response = ec.batch(null, null, serialNumber, tranId, amount, tranResult, tranDetails, attributes);
		if(response == null)
			throw new Exception("Received null response");
		String message = getBasicResponseMessage(response);
		log.info(message);
		return response;
	}
	protected HessianProxyFactory hessianProxyFactory = null;
	protected final Map<String, ECServiceAPI> ePortConnects = new HashMap<String, ECServiceAPI>();
	protected final Map<String, EC2ServiceAPI> ePortConnect2s = new HashMap<String, EC2ServiceAPI>();
	protected final Map<String, EcStub> ePortConnectSoaps = new HashMap<String, EcStub>();
	protected final Map<String, Ec2Stub> ePortConnectSoap2s = new HashMap<String, Ec2Stub>();
	protected int authPort = 14109;
	protected int salePort = 14108;
	protected int layer3Port = 14107;
	protected int unifedlayerPort = 14143;
	protected int ecPort = 9443;

	protected ECServiceAPI getECService() throws MalformedURLException {
		return getECService(env);
	}

	protected ECServiceAPI getECService(String env) throws MalformedURLException {
		if(hessianProxyFactory == null)
			hessianProxyFactory = new HessianProxyFactory();

		ECServiceAPI api = ePortConnects.get(env);
		if(api == null) {
			StringBuilder sb = new StringBuilder();
			sb.append("https://");
			if(StringUtils.isBlank(env))
				sb.append("localhost:");
			else
				sb.append("ec-").append(env).append(".usatech.com:");
			sb.append(ecPort).append("/hessian/ec");
			api = (ECServiceAPI) hessianProxyFactory.create(ECServiceAPI.class, sb.toString());
			ePortConnects.put(env, api);
		}
		return api;
	}

	protected EC2ServiceAPI getEC2Service() throws MalformedURLException {
		return getEC2Service(env);
	}

	protected EC2ServiceAPI getEC2Service(String env) throws MalformedURLException {
		if(hessianProxyFactory == null)
			hessianProxyFactory = new HessianProxyFactory();
		EC2ServiceAPI api = ePortConnect2s.get(env);
		if(api == null) {
			StringBuilder sb = new StringBuilder();
			sb.append("https://");
			if(StringUtils.isBlank(env))
				sb.append("localhost:");
			else if("usa".equalsIgnoreCase(env))
				sb.append("ec.usatech.com:");
			else
				sb.append("ec-").append(env).append(".usatech.com:");
			sb.append(ecPort).append("/hessian/ec2");
			api = (EC2ServiceAPI) hessianProxyFactory.create(EC2ServiceAPI.class, sb.toString());
			ePortConnect2s.put(env, api);
		}
		return api;
	}

	protected EcStub getECSoapService() throws AxisFault {
		return getECSoapService(null);
	}

	protected EcStub getECSoapService(String env) throws AxisFault {
		EcStub api = ePortConnectSoaps.get(env);
		if(api == null) {
			StringBuilder sb = new StringBuilder();
			sb.append("https://");
			if(StringUtils.isBlank(env))
				sb.append("localhost:");
			else
				sb.append("ec-").append(env).append(".usatech.com:");
			sb.append(ecPort).append("/soap/ec");
			api = new EcStub(sb.toString());
			ePortConnectSoaps.put(env, api);
		}
		return api;
	}

	protected Ec2Stub getEC2SoapService() throws AxisFault {
		return getEC2SoapService(env);
	}

	protected Ec2Stub getEC2SoapService(String env) throws AxisFault {
		Ec2Stub api = ePortConnectSoap2s.get(env);
		if(api == null) {
			StringBuilder sb = new StringBuilder();
			sb.append("https://");
			if(StringUtils.isBlank(env))
				sb.append("localhost:");
			else
				sb.append("ec-").append(env).append(".usatech.com:");
			sb.append(ecPort).append("/soap/ec2");
			api = new Ec2Stub(sb.toString());
			ePortConnectSoap2s.put(env, api);
		}
		return api;
	}

	protected String getBasicResponseMessage(Object response) {
		StringBuilder sb = new StringBuilder();
		sb.append(response.getClass().getSimpleName()).append(' ');
		try {
			for(Map.Entry<String, ?> entry : ReflectionUtils.toPropertyMap(response).entrySet()) {
				sb.append(entry.getKey()).append(": ").append(entry.getValue()).append(", ");
			}
		} catch(IntrospectionException e) {
			log.error("While printing response", e);
		}

		return sb.substring(0, sb.length() - 2);
	}

	protected ClientEmulator getClientEmulator(String serialCd) throws Exception {
		// get device info
		dataLayerInit.initialize();
		Map<String, Object> params = new HashMap<>();
		params.put("deviceSerialCd", serialCd);
		DataLayerMgr.selectInto("GET_DEVICE_INFO", params);
		String deviceName = ConvertUtils.getString(params.get("deviceName"), true);
		byte[] encryptionKey = ConvertUtils.convert(byte[].class, params.get("encryptionKey"));
		DeviceType deviceType = ConvertUtils.convert(DeviceType.class, params.get("deviceType"));
		long masterId = ConvertUtils.getLong(params.get("masterId"), 0L);
		String timezone = ConvertUtils.getString(params.get("timezone"), false);
		long time = System.currentTimeMillis() / 1000;
		if(masterId < time)
			masterId = time;
		int port;
		switch(deviceType) {
			case G4:
			case GX:
				port = 14107;
				break;
			case ESUDS:
			case MEI:
				port = 14143;
				break;
			case EDGE:
				port = 14108;
				break;
			default:
				port = 14143;
				break;
		}
		ClientEmulator ce = new ClientEmulator("localhost", port, deviceName, encryptionKey);
		ce.setMasterId(masterId);
		ce.setTimeZoneGuid(timezone);
		return ce;
	}

	public void gx32AuthAndSale(String serialCd, String cardData, EntryType entryType, long authAmount, int saleAmount, int convenienceFee, AuthResponseCodeAF expectedAuthResponse, Long expectedApprovedAmount) throws Exception {
		ClientEmulator ce = getClientEmulator(serialCd);
		long tranId = ce.nextMasterId();
		MessageData_AE data = new MessageData_AE();
		data.setAmount(authAmount);
		data.setCardType(CardType.findAll(entryType, PaymentActionType.PURCHASE).get(0));
		data.setTransactionId(tranId);
		data.setMessageVersion(0);
		data.setValidationData(null);
		data.setCardReaderType(CardReaderType.GENERIC);
		MessageData_AE.GenericCardReader cardReader = (MessageData_AE.GenericCardReader) data.getCardReader();
		cardReader.setAccountData2(cardData);
		data.setAdditionalDataType(AEAdditionalDataType.NO_ADDITIONAL_DATA);
		data.setAdditionalData(null);

		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		int protocol;
		switch(ce.getPort()) {
			case 14107:
				protocol = 4;
				break;
			default:
				protocol = 7;
		}
		ce.startCommunication(protocol);
		MessageData reply;
		try {
			reply = ce.sendMessage(data);
		} finally {
			ce.stopCommunication();
		}
		boolean doSale;
		switch(reply.getMessageType()) {
			case AUTH_RESPONSE_3_2:
				MessageData_AF replyAF = (MessageData_AF) reply;
				switch(replyAF.getResponseCode()) {
					case APPROVED:
					case CONDITIONALLY_APPROVED:
						doSale = true;
						break;
					default:
						doSale = false;
						log.info("Cannot process sale because auth was rejected");
				}
				if(expectedAuthResponse != null && replyAF.getResponseCode() != expectedAuthResponse)
					throw new Exception("Return code " + replyAF.getResponseCode() + " did not matched expected " + expectedAuthResponse);
				if(!ConvertUtils.areEqual(expectedApprovedAmount, replyAF.getApprovedAmount()))
					throw new Exception("Approved Amount " + replyAF.getApprovedAmount() + " did not matched expected " + expectedApprovedAmount);
				break;
			default:
				doSale = false;
				throw new Exception("Unrecognized authorization response: " + reply.getMessageType());
		}
		if(doSale) {
			MessageData_2A saleData = new MessageData_2A(); // XXX: is this the right message to send for a device that does Auth v3.2?
			saleData.setTransactionId(data.getTransactionId());
			saleData.setConvenienceFee(convenienceFee);
			saleData.setSaleAmount(saleAmount);
			saleData.setTransactionResult(saleAmount > 0 ? TranDeviceResultType.SUCCESS : TranDeviceResultType.CANCELLED);
			saleData.setVendByteLength((byte) 0);
			if(saleAmount > 0) {
				MessageData_2A.NetBatch0LineItem li = (MessageData_2A.NetBatch0LineItem) saleData.addActualLineItem();
				li.setPositionNumber(66);
				int price1;
				int price2;
				price1 = (int) (saleAmount * 0.63);
				price2 = saleAmount - price1;
				li.setReportedPrice(price1);
				li = (MessageData_2A.NetBatch0LineItem) saleData.addActualLineItem();
				li.setPositionNumber(47);
				li.setReportedPrice(price2);
			}
			log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
			ce.startCommunication(protocol);
			try {
				reply = ce.sendMessage(saleData);
			} finally {
				ce.stopCommunication();
			}
			switch(reply.getMessageType()) {
				case BATCH_ACK_2_0:
					MessageData_71 reply71 = (MessageData_71) reply;
					if(reply71.getSaleResult() != SaleResult71.PASS)
						throw new Exception("Sale '" + ce.getEvNumber() + "-" + reply71.getTransactionId() + "'was not accepted");
					break;
				default:
					throw new Exception("Unrecognized sale response: " + reply.getMessageType());
			}
		}
	}

	public void gxAuthAndSale(String serialCd, String cardData, EntryType entryType, long authAmount, int saleAmount, int convenienceFee, boolean includePrices, AuthResponseCodeA1 expectedAuthResponse, Long expectedApprovedAmount) throws Exception {
		ClientEmulator ce = getClientEmulator(serialCd);
		long tranId = ce.nextMasterId();
		MessageData_A0 data = new MessageData_A0();
		data.setAccountData(cardData);
		data.setAmount(authAmount);
		data.setCardType(CardType.findAll(entryType, PaymentActionType.PURCHASE).get(0));
		data.setTransactionId(tranId);
		log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
		int protocol;
		switch(ce.getPort()) {
			case 14107:
				protocol = 4;
				break;
			default:
				protocol = 7;
		}
		ce.startCommunication(protocol);
		MessageData reply;
		try {
			reply = ce.sendMessage(data);
		} finally {
			ce.stopCommunication();
		}
		boolean doSale;
		switch(reply.getMessageType()) {
			case AUTH_RESPONSE_3_0:
				MessageData_A1 replyA1 = (MessageData_A1) reply;
				switch(replyA1.getResponseCode()) {
					case APPROVED:
					case CONDITIONALLY_APPROVED:
						doSale = true;
						break;
					default:
						doSale = false;
						log.info("Cannot process sale because auth was rejected");
				}
				if(expectedAuthResponse != null && replyA1.getResponseCode() != expectedAuthResponse)
					throw new Exception("Return code " + replyA1.getResponseCode() + " did not matched expected " + expectedAuthResponse);
				if(!ConvertUtils.areEqual(expectedApprovedAmount, replyA1.getApprovedAmount()))
					throw new Exception("Approved Amount " + replyA1.getApprovedAmount() + " did not matched expected " + expectedApprovedAmount);
				break;
			default:
				doSale = false;
				throw new Exception("Unrecognized authorization response: " + reply.getMessageType());
		}
		if(doSale) {
			MessageData_2A saleData = new MessageData_2A();
			saleData.setTransactionId(data.getTransactionId());
			saleData.setConvenienceFee(convenienceFee);
			saleData.setSaleAmount(saleAmount);
			saleData.setTransactionResult(saleAmount > 0 ? TranDeviceResultType.SUCCESS : TranDeviceResultType.CANCELLED);
			saleData.setVendByteLength((byte) 0);
			if(saleAmount > 0) {
				MessageData_2A.NetBatch0LineItem li = (MessageData_2A.NetBatch0LineItem) saleData.addActualLineItem();
				li.setPositionNumber(66);
				int price1;
				int price2;

				if(includePrices) {
					price1 = (int) (saleAmount * 0.63);
					price2 = saleAmount - price1;
				} else {
					price1 = price2 = 0xFFFFFF;
				}
				li.setReportedPrice(price1);
				li = (MessageData_2A.NetBatch0LineItem) saleData.addActualLineItem();
				li.setPositionNumber(47);
				li.setReportedPrice(price2);
			}
			log.debug("Starting communication at " + System.currentTimeMillis() + " (" + new Date() + "");
			ce.startCommunication(protocol);
			try {
				reply = ce.sendMessage(saleData);
			} finally {
				ce.stopCommunication();
			}
			switch(reply.getMessageType()) {
				case BATCH_ACK_2_0:
					MessageData_71 reply71 = (MessageData_71) reply;
					if(reply71.getSaleResult() != SaleResult71.PASS)
						throw new Exception("Sale '" + ce.getEvNumber() + "-" + reply71.getTransactionId() + "'was not accepted");
					break;
				default:
					throw new Exception("Unrecognized sale response: " + reply.getMessageType());
			}
		}
	}

	public String getEnv() {
		return env;
	}

	public void setEnv(String env) {
		this.env = env;
		if(env == null)
			unifedlayerPort = 14143;
		else
			unifedlayerPort = 443;
	}
}
