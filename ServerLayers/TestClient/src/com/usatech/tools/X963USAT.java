package com.usatech.tools;


import java.nio.ByteBuffer;



import de.flexiprovider.api.KeyDerivation;
import de.flexiprovider.api.MessageDigest;
import de.flexiprovider.api.exceptions.DigestException;
import de.flexiprovider.api.exceptions.InvalidAlgorithmParameterException;
import de.flexiprovider.api.exceptions.InvalidKeyException;
import de.flexiprovider.api.parameters.AlgorithmParameterSpec;
import de.flexiprovider.core.md.SHA256;

/**
 * X963 is a key derivation function defined in ANSI X9.63.
 * <p>
 * X963 can be used as follows:
 * 
 * <pre>
 * KeyDerivation kdf = Registry.getKeyDerivation(&quot;X963&quot;);
 * kdf.init(secretKey.toByteArray());
 * kdf.setSharedInfo(byte[] sharedInfo);
 * byte[] derivedKey = kdf.doFinal(int keyDataLen);
 * </pre>
 * 
 * @author Jochen Hechler
 * @author Marcus St�gbauer
 * @author Martin D�ring
 * 
 * NOTE: the only thing that is changed from the original is the md to be SHA256
 * @TODO check license GPL
 */
public class X963USAT extends KeyDerivation {

    // the hash function
    private MessageDigest md;

    // the secret key
    private byte[] z;

    // a shared info string
    private byte[] sharedInfo;

    /**
     * Constructor. Set the message digest.
     */
    public X963USAT() {
    	md = new SHA256();
    }
    

    public MessageDigest getMd() {
			return md;
		}

		public void setMd(MessageDigest md) {
			this.md = md;
		}



		public X963USAT(byte[] z, byte[] sharedInfo, MessageDigest digest) {
			super();
			this.z = z;
			this.sharedInfo = sharedInfo;
			this.md = digest;
		}


    /**
     * Derive X9.63 KDF key
     *
     * @param k    Key len in BITs
     * @return     Derived key as bytes
     */
    public byte[] deriveKey(int k) {
        int el = this.md.getDigestLength() * Byte.SIZE;
        int j = k / el;
        byte[] result = new byte[k / Byte.SIZE];
        Integer counter = 1;
        try {
            for(int i = 1; i <= j - 1; i++) {
                this.md.update(this.z);
                this.md.update(ByteBuffer.allocate(Integer.SIZE / Byte.SIZE).putInt(counter).array());
                this.md.update(this.sharedInfo);
                this.md.digest(result, (i - 1) * (el / Byte.SIZE), el / Byte.SIZE);
                counter++;
            }
        } catch (DigestException var8) {
            throw new RuntimeException("internal error", var8);
        }
        int l = k - (el * j);
        this.md.update(this.z);
        this.md.update(ByteBuffer.allocate(Integer.SIZE / Byte.SIZE).putInt(counter).array());
        this.md.update(this.sharedInfo);
        try {
            this.md.digest(result, l * (el / Byte.SIZE), el / Byte.SIZE);
        } catch (DigestException e) {
            throw new RuntimeException("internal error", e);
        }
        return result;
    }


		@Override
		public void init(byte[] secret, AlgorithmParameterSpec params) throws InvalidKeyException, InvalidAlgorithmParameterException {
			// TODO Auto-generated method stub
			
		}


}

