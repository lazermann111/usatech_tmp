package com.usatech.tools;

import java.io.IOException;

import com.usatech.tools.deploy.UsatSettingUpdateFileTask;

import simple.db.BasicDataSourceFactory;
import simple.db.DataLayerMgr;
import simple.db.config.ConfigException;
import simple.db.config.ConfigLoader;
import simple.io.LoadingException;

public class Setup {

	public static void setupDataLayerFile(String resourcePath) throws ConfigException, IOException, LoadingException {
		ConfigLoader.loadConfig(resourcePath);
	}

	public static void setupDataSource_USADEV04() {
		setupDataSource("OPER", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=ECCDB013.USATECH.COM)(PORT=1523)))(CONNECT_DATA=(SERVICE_NAME=USAODEV04)))", "LOADER_2", "LOADER_1", false);
	}

	public static void setupDataSource_USADEV03() {
		setupDataSource("OPER", "oracle.jdbc.driver.OracleDriver", UsatSettingUpdateFileTask.OPER_DEV, "LOADER_2", "LOADER_1", false);
	}

	public static void setupDataSource_USADEV02() {
		setupDataSource("OPER", "oracle.jdbc.driver.OracleDriver", UsatSettingUpdateFileTask.OPER_INT, "LOADER_2", "LOADER_1", false);
	}

	public static void setupDataSource(String name, String driver, String url, String username, String password, boolean autoCommit) {
		BasicDataSourceFactory bdsf = new BasicDataSourceFactory();
		bdsf.addDataSource(name, driver, url, username, password, autoCommit);
		DataLayerMgr.setDataSourceFactory(bdsf);
	}
}
