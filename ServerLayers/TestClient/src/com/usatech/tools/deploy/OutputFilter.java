package com.usatech.tools.deploy;

public interface OutputFilter {
	public String filter(String argument);
}
