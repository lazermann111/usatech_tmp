/**
 * 
 */
package com.usatech.tools.deploy;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Date;

import simple.bean.ConvertUtils;
import simple.io.HeapBufferStream;
import simple.io.Interaction;
import simple.security.SecurityUtils;
import simple.security.crypt.CryptUtils;
import simple.text.StringUtils;


public class RenewJKSCertTask implements DeployTask {
	protected final Date expiration;
	protected final String keystorePath;
	protected final PasswordCache cache;
	protected final int keystorePermissions;
	protected final HostSpecificValue aliasValue;
	protected final HostSpecificValue commonNameValue;
	protected final String certificateAuthority;
	protected final String templateName;
	protected final String webUser;
	protected static final String defaultSelfsignTemplate="USATWebServer";
	protected boolean replaceDeprecatedSigAlg = false;
	protected final Collection<Host> similarHosts;

	public RenewJKSCertTask(Date expiration, String keystorePath, PasswordCache cache, int keystorePermissions) {
		this.expiration = expiration;
		this.keystorePath = keystorePath;
		this.cache = cache;
		this.keystorePermissions = keystorePermissions;
		this.aliasValue = new HostSpecificValue() {
			@Override
			public boolean isOverriding() {
				return true;
			}

			@Override
			public String getValue(Host host, Interaction interaction) {
				return host.getSimpleName();
			}
		};
		this.commonNameValue = new HostSpecificValue() {
			@Override
			public boolean isOverriding() {
				return true;
			}

			@Override
			public String getValue(Host host, Interaction interaction) {
				return host.getFullName();
			}
		};
		this.certificateAuthority = "usasubca.usatech.com";
		this.templateName = "USATWebServer";
		this.webUser = System.getProperty("user.name");
		this.similarHosts = null;
	}

	public RenewJKSCertTask(Date expiration, String keystorePath, PasswordCache cache, int keystorePermissions, HostSpecificValue aliasValue, HostSpecificValue commonNameValue, String certificateAuthority, String templateName, String webUser) {
		this(expiration, keystorePath, cache, keystorePermissions, aliasValue, commonNameValue, certificateAuthority, templateName, webUser, null);
	}

	public RenewJKSCertTask(Date expiration, String keystorePath, PasswordCache cache, int keystorePermissions, HostSpecificValue aliasValue, HostSpecificValue commonNameValue, String certificateAuthority, String templateName, String webUser, Collection<Host> similarHosts) {
		this.expiration = expiration;
		this.keystorePath = keystorePath;
		this.cache = cache;
		this.keystorePermissions = keystorePermissions;
		this.aliasValue = aliasValue;
		this.commonNameValue = commonNameValue;
		this.certificateAuthority = certificateAuthority;
		this.templateName = templateName;
		this.webUser = webUser;
		this.similarHosts = similarHosts;
	}

	@Override
	public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
		//get the alias		
		String commonName = commonNameValue.getValue(deployTaskInfo.host, deployTaskInfo.interaction);
		String alias = aliasValue.getValue(deployTaskInfo.host, deployTaskInfo.interaction);
		
		//Get the keystore stream
		HeapBufferStream bufferStream = new HeapBufferStream(true);
		InputStream in;
		if(UploadTask.checkFileUsingCmd(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, keystorePath)) {
			deployTaskInfo.interaction.printf("Retrieving contents of keystore at '%1$s'", keystorePath);
			UploadTask.readFileUsingTmp(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, keystorePath, bufferStream.getOutputStream(), null);
			in = bufferStream.getInputStream();
			in.mark(bufferStream.size());
		} else {
			in = null;
		}
		//Decode with password
		try {
			KeyStore keyStore = SecurityUtils.getKeyStore(in, "JKS", null, null);
			//check if expired
			Certificate[] certs = keyStore.getCertificateChain(alias);
			boolean expired = false;
			if(certs == null)
				expired = true;
			else
				for(Certificate cert : certs) {
					if(cert instanceof X509Certificate) {
						X509Certificate x509 = (X509Certificate) cert;
						Date expiredDate = x509.getNotAfter();
						String sigAlg = x509.getSigAlgName();
						if(expiration.after(expiredDate) || (replaceDeprecatedSigAlg && sigAlg != null && sigAlg.startsWith("SHA1with"))) {
							expired = true;
							break;
						}
					}
				}
			if(expired) {
				char[] password = DeployUtils.getPassword(cache, deployTaskInfo.host, "keystore", "the keystore at '" + keystorePath + "'", deployTaskInfo.interaction);
				// Check other servers
				if(similarHosts != null) {
					for(Host similarHost : similarHosts) {
						if(!similarHost.equals(deployTaskInfo.host)) {
							HeapBufferStream similarBufferStream = new HeapBufferStream(false);
							if(UploadTask.checkFileUsingCmd(similarHost, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, keystorePath)) {
								deployTaskInfo.interaction.printf("Retrieving contents of keystore at '%1$s'", keystorePath);
								UploadTask.readFileUsingTmp(similarHost, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, keystorePath, similarBufferStream.getOutputStream(), null);
								InputStream similarIn = similarBufferStream.getInputStream();
								try {
									KeyStore similarKeyStore = SecurityUtils.getKeyStore(similarIn, "JKS", null, password);
									Certificate[] similarCerts = similarKeyStore.getCertificateChain(alias);
									if(similarCerts != null) {
										boolean similarExpired = false;
										for(Certificate cert : similarCerts) {
											if(cert instanceof X509Certificate) {
												X509Certificate x509 = (X509Certificate) cert;
												Date expiredDate = x509.getNotAfter();
												String sigAlg = x509.getSigAlgName();
												if(expiration.after(expiredDate) || (replaceDeprecatedSigAlg && sigAlg != null && sigAlg.startsWith("SHA1with"))) {
													similarExpired = true;
													break;
												}
											}
										}
										if(!similarExpired) {
											// We can use it here
											Key privateKey = similarKeyStore.getKey(alias, password);
											keyStore.setKeyEntry(alias, privateKey, password, similarCerts);
											deployTaskInfo.interaction.printf("Certificate imported from " + similarHost + ". Saving the keystore");
											similarBufferStream.clear();
											keyStore.store(similarBufferStream.getOutputStream(), password);
											UploadTask.writeFileUsingTmp(deployTaskInfo, similarBufferStream.getInputStream(), keystorePermissions, "root", "usat", null, keystorePath);
											return "Renewed certificate and private key";
										}
									}
								} finally {
									similarIn.close();
								}
							}
						}
					}
				}
				
				// Create new entry
				String dName="CN=" + commonName + ", O=USA Technologies\\, Inc, L=Malvern, S=PA, C=US";
				if(templateName!=null){
					StringBuilder csr = new StringBuilder();
					KeyPair pair = CryptUtils.generateCSR("RSA", 2048, dName, csr);
					InputStream certStream = DeployUtils.signCertificate(certificateAuthority, templateName, webUser, deployTaskInfo.interaction, deployTaskInfo.passwordCache, csr.toString());
					try {
						deployTaskInfo.interaction.printf("Certificate created and signed. Swapping out the old with the new one");
						if(in != null)
							in.reset();
						keyStore.load(in, password);

						CryptUtils.importCertificateChain(keyStore, alias, pair, certStream, password);
						deployTaskInfo.interaction.printf("Certificate imported. Saving the keystore");
					} finally {
						certStream.close();
					}
				}else{
					String tempAlias=alias+".temp";
					Certificate csrCert = keyStore.getCertificate(tempAlias);
					if(csrCert!=null){
						StringBuilder previousCSR=new StringBuilder();
						KeyPair pair=CryptUtils.extractCSR(keyStore, tempAlias, password, dName, previousCSR);
						boolean reuse = ConvertUtils.getBooleanSafely(deployTaskInfo.interaction.readLine("Re-use previous CSR ?"), true);
						if(reuse) {
							String csrResultInputFilePath=deployTaskInfo.interaction.readLine("Sign the previous CSR,  then enter the result file path:\n%1s", previousCSR);
							if(StringUtils.isBlank(csrResultInputFilePath))
								return "No CSR result file path entered. Rerun when the cert is ready.";
							InputStream certStream = new FileInputStream(csrResultInputFilePath);
							try {
								deployTaskInfo.interaction.printf("Certificate created and signed. Swapping out the old with the new one");
								if(in != null)
									in.reset();
								keyStore.load(in, password);

								CryptUtils.importCertificateChain(keyStore, alias, pair, certStream, password);
								deployTaskInfo.interaction.printf("Certificate imported. Saving the keystore");
								keyStore.deleteEntry(tempAlias);
								deployTaskInfo.interaction.printf("Previous temp csr cert deleted");
							} finally {
								certStream.close();
							}
						} else {
							StringBuilder newCSR = new StringBuilder();
							KeyPair newPair = CryptUtils.generateCSR("RSA", 2048, dName, newCSR);
							InputStream certStream = DeployUtils.signCertificate(certificateAuthority, defaultSelfsignTemplate, webUser, deployTaskInfo.interaction, deployTaskInfo.passwordCache, newCSR.toString());
							try {
								deployTaskInfo.interaction.printf("Saving newly generated CSR selfsigned cert.");
								if(in != null)
									in.reset();
								keyStore.load(in, password);

								CryptUtils.importCertificateChain(keyStore, tempAlias, newPair, certStream, password);
								deployTaskInfo.interaction.printf("Newly generated CSR saved in the keystore");
								deployTaskInfo.interaction.printf(newCSR.toString());
							} finally {
								certStream.close();
							}
						}
					}else{
						boolean generateCSR = ConvertUtils.getBooleanSafely(deployTaskInfo.interaction.readLine("Generate a csr? Y for generate CSR"), true);
						if(generateCSR){
							StringBuilder csr = new StringBuilder();
							KeyPair pair = CryptUtils.generateCSR("RSA", 2048, dName, csr);
							InputStream certStream = DeployUtils.signCertificate(certificateAuthority, defaultSelfsignTemplate, webUser, deployTaskInfo.interaction, deployTaskInfo.passwordCache, csr.toString());
							try {
								deployTaskInfo.interaction.printf("Saving newly generated CSR.");
								if(in != null)
									in.reset();
								keyStore.load(in, password);

								CryptUtils.importCertificateChain(keyStore, tempAlias, pair, certStream, password);
								deployTaskInfo.interaction.printf("Newly generated CSR selfsigned cert saved in the keystore");
								deployTaskInfo.interaction.printf(csr.toString());
							} finally {
								certStream.close();
							}
						}
					}
				}
				bufferStream.clear();
				keyStore.store(bufferStream.getOutputStream(), password);
				UploadTask.writeFileUsingTmp(deployTaskInfo, bufferStream.getInputStream(), keystorePermissions, "root", "usat", null, keystorePath);
				
				if(similarHosts != null) {
					for(Host similarHost : similarHosts) {
						if(!similarHost.equals(deployTaskInfo.host)) {
							copyKeyStoreEntry(keyStore, alias, password, similarHost, deployTaskInfo);
						}
					}
				}

				return "Renewed certificate and private key";
			}
		} catch(KeyStoreException e) {
			throw new IOException(e);
		} catch(NoSuchProviderException e) {
			throw new IOException(e);
		} catch(NoSuchAlgorithmException e) {
			throw new IOException(e);
		} catch(CertificateException e) {
			throw new IOException(e);
		} catch(InvalidKeyException e) {
			throw new IOException(e);
		} catch(SignatureException e) {
			throw new IOException(e);
		} catch (UnrecoverableKeyException e) {
			throw new IOException(e);
		} finally {
			in.close();
		}
		return "Certificate is up to date";
	}

	protected void copyKeyStoreEntry(KeyStore keyStore, String alias, char[] password, Host similarHost, DeployTaskInfo deployTaskInfo) throws UnrecoverableKeyException, KeyStoreException, NoSuchProviderException, NoSuchAlgorithmException, CertificateException, IOException, InterruptedException {
		HeapBufferStream similarBufferStream = new HeapBufferStream(false);
		if(UploadTask.checkFileUsingCmd(similarHost, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, keystorePath)) {
			deployTaskInfo.interaction.printf("Retrieving contents of keystore at '%1$s' on '%2$s", keystorePath, similarHost.getSimpleName());
			UploadTask.readFileUsingTmp(similarHost, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, keystorePath, similarBufferStream.getOutputStream(), null);
			InputStream similarIn = similarBufferStream.getInputStream();
			try {
				KeyStore similarKeyStore = SecurityUtils.getKeyStore(similarIn, "JKS", null, password);
				Key privateKey = keyStore.getKey(alias, password);
				Certificate[] certs = keyStore.getCertificateChain(alias);
				similarKeyStore.setKeyEntry(alias, privateKey, password, certs);
				deployTaskInfo.interaction.printf("Copied Certificate to " + similarHost + ". Saving the keystore");
				similarBufferStream.clear();
				similarKeyStore.store(similarBufferStream.getOutputStream(), password);
				UploadTask.writeFileUsingTmp(similarHost, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, similarBufferStream.getInputStream(), keystorePermissions, "root", "usat", null, keystorePath);
			} finally {
				similarIn.close();
			}
		}
	}

	/**
	 * @throws IOException
	 */
	protected void onNewEntry(String alias, Certificate[] certificateChain, PrivateKey privateKey, DeployTaskInfo deployTaskInfo) throws IOException {
		// hook for subclasses
	}

	@Override
	public String getDescription(DeployTaskInfo deployTaskInfo) {
		StringBuilder sb = new StringBuilder();
		sb.append("Renewing certificate and private key '").append(aliasValue.getValue(deployTaskInfo.host, deployTaskInfo.interaction)).append("' in keystore ").append(keystorePath);
		return sb.toString();
	}

	public boolean isReplaceDeprecatedSigAlg() {
		return replaceDeprecatedSigAlg;
	}

	public void setReplaceDeprecatedSigAlg(boolean replaceDeprecatedSigAlg) {
		this.replaceDeprecatedSigAlg = replaceDeprecatedSigAlg;
	}
}