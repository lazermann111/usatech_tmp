package com.usatech.tools.deploy;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import simple.app.Processor;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.lang.SystemUtils;
import simple.text.StringUtils;

import com.sshtools.j2ssh.SshClient;
import com.sshtools.j2ssh.session.SessionChannelClient;

public class CvsPullTask extends DefaultPullTask {
	protected final String cvsRootDir;
	protected final String filePath;
	protected final String revision;
	protected static final Host CVS_HOST = new HostImpl("LOCAL", "cvs.usatech.com");
	protected static final char NEWLINE = '\n';
	protected final InputStream resultInputStream = new InputStream() {
		@Override
		public int available() throws IOException {
			long remaining = fileSize - pos;
			if(remaining > Integer.MAX_VALUE)
				return Integer.MAX_VALUE;
			return (int) remaining;
		}

		@Override
		public void close() throws IOException {
			try {
				if(fileInputStream != null)
					fileInputStream.close();
			} finally {
				fileSize = 0;
				pos = 0;
				fileInputStream = null;
				if(sshSession != null)
					try {
						sshSession.close();
					} finally {
						sshSession = null;
					}
			}
		}

		protected void readAfterContent() throws IOException {
			String line;
			StringBuilder message = new StringBuilder();
			while((line = readLine(fileInputStream)) != null) {
				if("ok".equals(line)) {
					pos++;
					return;
				} else if(line.startsWith("error")) {
					throw new IOException("Could not pull file " + getFilePath() + " (" + getRevision() + ") from CVS because: " + message.toString());
				} else if(line.startsWith("M ") || line.startsWith("E ")) {
					message.append(StringUtils.substringAfter(line, " "));
				}
			}
		}
		@Override
		public int read(byte[] b, int off, int len) throws IOException {
			if(fileInputStream == null)
				throw new IOException("Task has not run yet");
			if(pos < fileSize) {
				int n = fileInputStream.read(b, off, Math.min(available(), len));
				if(n < 0) {
					if(pos != fileSize)
						throw new IOException("InputStream content does not match expected byte count");
					readAfterContent();
				} else
					pos += n;
				return n;
			}
			if(pos == fileSize) {
				readAfterContent();
			}
			return -1;
		}

		@Override
		public long skip(long n) throws IOException {
			if(fileInputStream == null)
				throw new IOException("Task has not run yet");
			return fileInputStream.skip(Math.min(fileSize - pos, n));
		}
		@Override
		public int read() throws IOException {
			if(fileInputStream == null)
				throw new IOException("Task has not run yet");
			if(pos < fileSize) {
				int n = fileInputStream.read();
				if(n < 0) {
					if(pos != fileSize)
						throw new IOException("InputStream content does not match expected byte count");
					readAfterContent();
				} else
					pos++;
				return n;
			}
			if(pos == fileSize) {
				readAfterContent();
			}
			return -1;
		}
	};
	protected InputStream fileInputStream;
	protected SessionChannelClient sshSession;
	protected long fileSize;
	protected long pos;

	public CvsPullTask(String filePath) {
		this(filePath, "HEAD");
	}

	public CvsPullTask(String filePath, String revision) {
		this("/usr/local/cvsroot/NetworkServices", filePath, revision);
	}
	public CvsPullTask(String cvsRootDir, String filePath, String revision) {
		this.cvsRootDir = cvsRootDir;
		this.filePath = filePath;
		this.revision = revision;
	}

	@Override
	public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
		SshClient ssh = deployTaskInfo.sshClientCache.getSshClient(CVS_HOST, deployTaskInfo.user);
		SessionChannelClient sshSession = ssh.openSessionChannel();
		boolean okay = false;
		try {
			if(!sshSession.executeCommand("cvs server"))
				throw new IOException("Could not start cvs server on remote host");
			InputStream in = new DataInputStream(sshSession.getInputStream());
			OutputStream out = new DataOutputStream(sshSession.getOutputStream());
			Writer writer = new OutputStreamWriter(out);
			writer.append("Root ").append(getCvsRootDir()).append(NEWLINE);
			writer.append("Argument -r").append(getRevision()).append(NEWLINE);
			writer.append("Argument ").append(getFilePath()).append(NEWLINE);
			writer.append("export").append(NEWLINE);
			writer.flush();

			StringBuilder message = new StringBuilder();
			String line;
			while((line = readLine(in)) != null) {
				if(line.startsWith("Updated ")) {
					readLine(in);
					readLine(in);
					readLine(in);
					long fileSize;
					try {
						fileSize = ConvertUtils.getLong(readLine(in));
					} catch(ConvertException e) {
						throw new IOException("Could not parse file size", e);
					}
					deployTaskInfo.interaction.printf("Pulling file %1$s (%2$s) from CVS of %3$d bytes", getFilePath(), getRevision(), fileSize);
					this.fileInputStream = in;
					this.fileSize = fileSize;
					this.pos = 0;
					okay = true;
					break;
				} else if("ok".equals(line)) {
					// it shouldn't really get here
					throw new IOException("ok was read but not expected");
				} else if(line.startsWith("error")) {
					throw new IOException("Could not pull file " + getFilePath() + " (" + getRevision() + ") from CVS because: " + message.toString());
				} else if(line.startsWith("M ") || line.startsWith("E ")) {
					message.append(StringUtils.substringAfter(line, " "));
				}
			}
		} finally {
			if(!okay)
				sshSession.close();
			else
				this.sshSession = sshSession;
		}
		return "Pulled '" + getFilePath() + "' (" + getRevision() + ") from CVS repository";
		
	}

	protected static String readLine(InputStream in) throws IOException {
		StringBuilder line = new StringBuilder(512);
		while(true) {
			int b = in.read();
			if(b == -1) {
				if(line.length() == 0)
					return null;
				return line.toString();
			}
			char ch = (char) b;
			if(ch == NEWLINE)
				return line.toString();
			line.append(ch);
		}
	}

	public String getCvsRootDir() {
		return cvsRootDir;
	}

	public String getFilePath() {
		return filePath;
	}

	public String getRevision() {
		return revision;
	}

	public InputStream getResultInputStream() {
		return resultInputStream;
	}

	@Override
	public InputStream process(DeployTaskInfo argument) throws ServiceException {
		try {
			perform(argument);
		} catch(IOException e) {
			throw new ServiceException(e);
		} catch(InterruptedException e) {
			throw new ServiceException(e);
		}
		return getResultInputStream();
	}

	@Override
	public Class<InputStream> getReturnType() {
		return InputStream.class;
	}

	@Override
	public Class<DeployTaskInfo> getArgumentType() {
		return DeployTaskInfo.class;
	}

	@Override
	public String getDescription(DeployTaskInfo deployTaskInfo) {
		StringBuilder sb = new StringBuilder();
		sb.append("Pulling '").append(getFilePath()).append("' (").append(getRevision()).append(") from CVS repository");
		return sb.toString();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("CVS Resource '").append(getFilePath()).append("' (").append(getRevision()).append(')');
		return sb.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof CvsPullTask))
			return false;
		if(this == obj)
			return true;
		CvsPullTask cpt = (CvsPullTask) obj;
		return ConvertUtils.areEqual(cpt.cvsRootDir, cvsRootDir) && ConvertUtils.areEqual(cpt.filePath, filePath) && ConvertUtils.areEqual(cpt.revision, revision);
	}

	@Override
	public int hashCode() {
		return SystemUtils.addHashCodes(0, cvsRootDir, filePath, revision);
	}
}
