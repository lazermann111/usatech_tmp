package com.usatech.tools.deploy;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingWorker;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.apache.commons.configuration.Configuration;

import com.sshtools.j2ssh.session.PseudoTerminal;
import com.sshtools.j2ssh.transport.HostKeyVerification;
import com.sshtools.j2ssh.transport.InvalidHostFileException;

import simple.app.BaseWithConfig;
import simple.io.GuiInteraction;
import simple.io.Interaction;
import simple.io.Log;
import simple.io.resource.sftp.SftpUtils;
import simple.swt.SortedTrackableListModel;
import simple.swt.ToggleListSelectionModel;

public class DeployMgr extends BaseWithConfig {
	private static final Log log = Log.getLog();
	protected static final String DEPLOY_MGR_JAR = DeployMgr.class.getClassLoader().getResource("").getPath() + "../lib/deploymgr.jar";
	public static final DateFormat fileDateFormat = new SimpleDateFormat("yyyyMMdd-HHmm");
	protected HostKeyVerification hostKeyVerification;
	protected String user;
	
	protected static final FileFilter changeSetFileFilter = new FileFilter() {
		@Override
		public boolean accept(File pathname) {
			return pathname.isDirectory() || pathname.getName().endsWith("ChangeSet.class");
		}		
	};
	protected static final PseudoTerminal deployerTerminal = new PseudoTerminal() {		
		@Override
		public int getWidth() {
			return 2000;
		}		
		@Override
		public String getTerm() {
			return null;
		}		
		@Override
		public int getRows() {
			return 2000;
		}		
		@Override
		public int getHeight() {
			return 2000;
		}		
		@Override
		public String getEncodedTerminalModes() {
			return "";
		}		
		@Override
		public int getColumns() {
			return 2000;
		}
	};

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new DeployMgr().run(args);
	}

	@Override
	protected void registerDefaultCommandLineArguments() {
		super.registerDefaultCommandLineArguments();
		registerCommandLineSwitch('d', "searchPath", true, true, "searchPath", "The path in which to look for ChangeSets");
	}
	@Override
	protected void execute(Map<String, Object> argMap, Configuration config) {
		String knownHostsFile = config.getString("knownhosts.file");
		try {
			hostKeyVerification = SftpUtils.createHostKeyVerification(knownHostsFile);
		} catch(InvalidHostFileException e) {
			finishAndExit("Could not load known hosts file", 201, e);
			return;
		}
		user = System.getProperty("remoteUserName", System.getProperty("user.name"));
		final CountDownLatch exit = new CountDownLatch(1);
		final JFrame frame = new JFrame("USAT Deployer");
		frame.setSize(600, 400);
		frame.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				exit.countDown();
			}
		});
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		frame.getContentPane().add(mainPanel);
		final JButton deployButton = new JButton("Deploy");
		JPanel buttonPanel = new JPanel(new FlowLayout());
		buttonPanel.add(deployButton);
		mainPanel.add(buttonPanel, BorderLayout.SOUTH);
		JPanel interactionPanel = new JPanel();
		interactionPanel.setLayout(new BorderLayout());
		JPanel changeSetPanel = new JPanel();
		JPanel hostPanel = new JPanel();
		SortedTrackableListModel<Host> hostModel = new SortedTrackableListModel<Host>(false, new Comparator<Host>() {
			public int compare(Host o1, Host o2) {
				return o1.getSimpleName().compareToIgnoreCase(o2.getSimpleName());
			}
		});
		//Add hosts
		try {
			addHosts(hostModel);
		} catch(UnknownHostException e) {
			finishAndExit("Unknown Host", 204, e);
			return;
		}
		
		final JList hostList = new JList(hostModel);
		hostList.setSelectionModel(new ToggleListSelectionModel());
		hostList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		hostList.setEnabled(false);
		hostList.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				Object[] selected = hostList.getSelectedValues();
				if(selected == null || selected.length == 0) 
					deployButton.setEnabled(false);
				else {
					deployButton.setEnabled(true);
				}
			}
		});
		hostPanel.setLayout(new BorderLayout());
		hostPanel.add(new JScrollPane(hostList), BorderLayout.CENTER);		
		hostPanel.add(new JLabel("Hosts:"), BorderLayout.NORTH);		

		SortedTrackableListModel<ChangeSet> changeSetModel = new SortedTrackableListModel<ChangeSet>(false, new Comparator<ChangeSet>() {
			public int compare(ChangeSet o1, ChangeSet o2) {
				return o1.getName().compareToIgnoreCase(o2.getName());
			}
		});
		//Add changesets
		String searchPath = (String)argMap.get("searchPath");
		if(searchPath != null) {
			File searchDir = new File(searchPath);
			if(searchDir.isAbsolute())
				addChangeSets(new File(searchPath), changeSetFileFilter, null, changeSetModel);
			else {
				String packageName = searchPath.replaceAll("[/\\\\]", ".");
				List<URL> urls;
				try {
					urls = Collections.list(getClass().getClassLoader().getResources(""));
				} catch (IOException e) {
					finishAndExit("Error loading change sets", 205, e);
					return;
				}
				for(URL url : urls) {
					if("file".equals(url.getProtocol())) {
						String file = url.getFile();
						if(file != null && file.endsWith("/")) {
							// its a directory - search for classes
							try {
								addChangeSets(new File(new File(url.toURI()), searchPath), changeSetFileFilter, packageName, changeSetModel);
							} catch(URISyntaxException e) {
								log.warn("Could not read directory '" + file + "'", e);
								continue;
							}
						} else {
							// Its a jar file - search for classes
							JarFile jarFile;
							try {
								jarFile = new JarFile(new File(url.toURI()));
							} catch(IOException e) {
								log.warn("Could not read jar file '" + file + "'", e);
								continue;
							} catch(URISyntaxException e) {
								log.warn("Could not read jar file '" + file + "'", e);
								continue;
							}
							addChangeSetsFromJar(jarFile, changeSetModel, packageName);							
						}
					}
				}
			}
		} else {
			List<URL> urls;
			try {
				urls = Collections.list(getClass().getClassLoader().getResources(""));
			} catch (IOException e) {
				finishAndExit("Error loading change sets", 205, e);
				return;
			}
			for(URL url : urls) {
				if("file".equals(url.getProtocol())) {
					String file = url.getFile();
                    if(file != null && file.endsWith("/")) {
                    	//its a directory - search for classes
                    	try {
                    		addChangeSets(new File(url.toURI()), changeSetFileFilter, null, changeSetModel);
                    	} catch(Exception e) {
							log.warn("Could not read directory '" + file + "'", e);
							continue;
						}
                    } else {
                    	//Its a jar file - search for classes
                    	JarFile jarFile;
						try {
							jarFile = new JarFile(new File(url.toURI()));
						} catch(IOException e) {
							log.warn("Could not read jar file '" + file + "'", e);
							continue;
						} catch(URISyntaxException e) {
							log.warn("Could not read jar file '" + file + "'", e);
							continue;
						}
						addChangeSetsFromJar(jarFile, changeSetModel, null);
                    }
				}
			}
		}
		
		try {
			File file = new File(DEPLOY_MGR_JAR);
			if (file.exists()) {
				JarFile jarFile = new JarFile(file);			
				addChangeSetsFromJar(jarFile, changeSetModel, null);
			}
		} catch(Exception e) {
			log.error("Error reading jar file '" + DEPLOY_MGR_JAR + "'", e);
		}	
		
		final JList changeSetList = new JList(changeSetModel);
		changeSetList.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				ChangeSet changeSet = (ChangeSet)changeSetList.getSelectedValue();
				if(changeSet == null) 
					hostList.setEnabled(false);
				else {
					hostList.setEnabled(true);
					//TODO: disable those hosts not applicable
				}
			}
		});
		changeSetPanel.setLayout(new BorderLayout());
		changeSetPanel.add(new JScrollPane(changeSetList), BorderLayout.CENTER);
		changeSetPanel.add(new JLabel("Change Sets:"), BorderLayout.NORTH);		

		final Interaction interaction = new GuiInteraction(interactionPanel);		
		mainPanel.add(new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true, new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true, changeSetPanel, hostPanel), interactionPanel), BorderLayout.CENTER);
		final PasswordCache passwordCache = new MapPasswordCache();
		final SshClientCache sshClientCache = new DefaultSshClientCache(interaction, hostKeyVerification, passwordCache);
		deployButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deployButton.setEnabled(false);
				final ChangeSet changeSet = (ChangeSet)changeSetList.getSelectedValue();
				final Object[] hostsSelected = hostList.getSelectedValues();
				(new SwingWorker<Void, Void>() {
					@Override
					protected Void doInBackground() throws Exception {
						executeDeploy(frame, changeSet, hostsSelected, interaction, user, passwordCache, sshClientCache);
						return null;
					}
					@Override
					protected void done() {
						deployButton.setEnabled(true);
					}
				}).execute();				
			}
		});
		frame.setVisible(true);
		try {
			exit.await();
		} catch(InterruptedException e) {
			//ignore
		}
		sshClientCache.shutdown();
	}

	protected void addHosts(Collection<Host> hosts) throws UnknownHostException {
		hosts.addAll(new USATRegistry().getHosts());
	}

	protected void addHosts2(Collection<Host> hosts) {
		// hostModel.add(new HostImpl("local", new String[] { "mst", "msr", "kls" }, "devmst05.usatech.com"));
		hosts.add(new HostImpl("local", new String[] { "mst", "msr", "kls", "rdw" }, "devdbs11.usatech.com"));
		for(String env : new String[] { "dev", "int", "ecc" }) {
			for(String[] types : new String[][] { { "apr", "app" }, { "net", "web" } }) {
				/*
				for(String ord : new String[] { "01" }) {
					String[] oldTypes = new String[types.length];
					for(int i = 0; i < types.length; i++)
						oldTypes[i] = types[i] + "_old";
					hostModel.add(new HostImpl(env, oldTypes, env + types[0] + ord + ".usatech.com"));
				}
				//*/
				for(String ord : new String[] { "11", "12" })
					hosts.add(new HostImpl(env, types, env + types[0] + ord + ".usatech.com"));
			}
			for(String[] types : new String[][] { { "mst", "msr" } }) {
				/*	
				for(String ord : new String[] { "01", "02", "03", "04" }) {
					hostModel.add(new HostImpl(env, type + "_old", env + type + ord + ".usatech.com"));
				}
				*/
				for(String ord : new String[] { "11", "12" })
					hosts.add(new HostImpl(env, types, env + types[0] + ord + ".usatech.com"));
			}
			// *
			for(String type : new String[] { "rpt" }) {
				for(String ord : new String[] { "01" })
					hosts.add(new HostImpl(env, type, env + type + ord + ".usatech.com"));
			}
			// */
			for(String ord : new String[] { "01" })
				hosts.add(new HostImpl(env, "app_old", env + "apr" + ord + ".usatech.com"));
			for(String ord : new String[] { "01", "02", "03", "04" })
				hosts.add(new HostImpl(env, "msr_old", env + "msr" + ord + ".usatech.com"));
		}
		{
			String env = "dev";
			for(String[] types : new String[][] { { "mst", "msr" } }) {
				for(String ord : new String[] { "13", "14" })
					hosts.add(new HostImpl(env, types, env + types[0] + ord + ".usatech.com"));
			}
			hosts.add(new HostImpl(env, "kls", "usadev01.usatech.com"));
		}
		{
			// @Todo add ecc 11, 12 and usa 31,32,33,34
			for(String env : new String[] { "dev", "int" }) {
				for(String[] types : new String[][] { { "kls" } }) {
					for(String ord : new String[] { "11", "12" })
						hosts.add(new HostImpl(env, types, env + types[0] + ord + ".usatech.com"));
				}
			}
			for(String env : new String[] { "ecc" }) {
				for(String[] types : new String[][] { { "kls" } }) {
					for(String ord : new String[] { "11", "12", "13", "14" })
						hosts.add(new HostImpl(env, types, env + types[0] + ord + ".usatech.com"));
				}
			}
		}

		for(String env : new String[] { "usa" }) {
			for(String type : new String[] { "apr", "net", "mst", "kls" }) {
				/*
				for(String ord : new String[] {"21", "22", "23", "24", "25"})
					hostModel.add(new HostImpl(env, type + "_old", env + type + ord + ".trooper.usatech.com"));
				*/
				for(String ord : new String[] { "31", "32", "33", "34" })
					hosts.add(new HostImpl(env, type, env + type + ord + ".trooper.usatech.com"));
			}
			for(String type : new String[] { "app", "web", "rpt", "xfr" }) {
				for(String ord : new String[] { "21", "22" })
					hosts.add(new HostImpl(env, type + "_old", env + type + ord + ".trooper.usatech.com"));
			}
			for(String type : new String[] { "msr" }) {
				for(String ord : new String[] { "21", "22", "23", "24" })
					hosts.add(new HostImpl(env, type + "_old", env + type + ord + ".trooper.usatech.com"));
			}
			for(String type : new String[] { "app", "web", "msr" }) {
				for(String ord : new String[] { "31", "32", "33", "34" })
					hosts.add(new HostImpl(env, type, env + type + ord + ".trooper.usatech.com"));
			}
		}

	}
	protected void addChangeSets(File dir, FileFilter fileFilter, String packageName, Collection<ChangeSet> changeSets) {
		log.info("Looking for change sets in " + dir);
		File[] files = dir.listFiles(fileFilter);
		if(files != null)
			for(File file : files) {
				if(file.isDirectory())
					addChangeSets(file, fileFilter, (packageName == null || packageName.length() == 0 ? "" : packageName + ".") + file.getName(), changeSets);
				else {
					String className = (packageName == null || packageName.length() == 0 ? "" : packageName + ".") + file.getName().substring(0, file.getName().length() - 6).replace('$', '.');
					addChangeSet(className, changeSets);
				}
			}	
	}
	
	protected boolean addChangeSet(String className, Collection<ChangeSet> changeSets) {
		log.info("Adding change set " + className);
		Class<?> type;
		try {
			type = Class.forName(className);
		} catch(ClassNotFoundException e) {
			log.error("Error loading class " + className, e);
			return false;
		}
		if(ChangeSet.class.isAssignableFrom(type) && !type.isInterface() && !type.isMemberClass() && !Modifier.isAbstract(type.getModifiers()) && Modifier.isPublic(type.getModifiers())) {
			try {
				changeSets.add(type.asSubclass(ChangeSet.class).newInstance());
				return true;
			} catch(InstantiationException e) {
				log.debug("Could not instantiate instance of class " + type.getName(), e);
			} catch(IllegalAccessException e) {
				log.debug("Could not instantiate instance of class " + type.getName(), e);
			}
		}
		return false;
	}
	
	protected void addChangeSetsFromJar(JarFile jarFile, SortedTrackableListModel<ChangeSet> changeSetModel, String packageName) {
		log.info("Loading change sets from " + jarFile.getName());
		for(Enumeration<JarEntry> entries = jarFile.entries(); entries.hasMoreElements();) {
			JarEntry entry = entries.nextElement();
			String name = entry.getName();
			if((packageName == null || name.startsWith(packageName.replace('.', '/'))) && name.endsWith("ChangeSet.class"))
				addChangeSet(name.substring(0, name.length() - 6).replace('/', '.').replace('$', '.'), changeSetModel);
		}
	}

	protected void executeDeploy(JFrame frame, ChangeSet changeSet, Object[] hostsSelected, Interaction interaction, String user, PasswordCache passwordCache, SshClientCache sshClientCache) {
		if(changeSet == null) {
			JOptionPane.showMessageDialog(frame, "No Change Set is selected. Please select the Change Set to Deploy", "Change Set Not Selected", JOptionPane.WARNING_MESSAGE);
			return;
		}
		StringBuilder sb = new StringBuilder();
		sb.append("Deploy change '").append(changeSet.getName()).append("' on ");
		if(hostsSelected == null || hostsSelected.length == 0) {
			JOptionPane.showMessageDialog(frame, "No Hosts were selected. Please select at least one Host on which to Deploy", "Hosts Not Selected", JOptionPane.WARNING_MESSAGE);
			return;
		}
		List<Host> hosts = new ArrayList<Host>();	
		for(Object o : hostsSelected) {
			Host host = (Host)o;
			if(changeSet.isApplicable(host))
				hosts.add(host);
		}
		if(hosts.isEmpty()) {
			JOptionPane.showMessageDialog(frame, "None of the selected Hosts is applicable for this Change Set. Please select different Hosts", "Hosts Not Applicable", JOptionPane.WARNING_MESSAGE);
			return;
		}
		for(int i = 0; i < hosts.size(); i++) {
			if(i != 0) {
				if(i == hosts.size() - 1) 
					sb.append(" and ");
				else
					sb.append(", ");
			}
			sb.append(hosts.get(i).getSimpleName());
		}
		if(JOptionPane.showConfirmDialog(frame, sb.toString(), "Confirm Deploy", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
			deploy(changeSet, hosts, interaction, user, passwordCache, sshClientCache);
		}
	}
	
	protected void deployOnHost(ChangeSet changeSet, Interaction interaction, String user, PasswordCache passwordCache, SshClientCache sshClientCache, Host host) {
		int sectionId = interaction.startSection(changeSet.getName() + " on " + host.getSimpleName());
		// Connect to the host
		/*
		try {
			sshClientCache.getSshClient(host, user);
		} catch(IOException e) {
			log.error("Could not connect to " + host.getSimpleName(), e);
			interaction.printf("Could not connect to %1$s because: %2$s", host.getSimpleName(), e.getMessage());
			//TODO: question continuing or aborting
			return;
		}*/
		int taskSectionId = 0;
		try {
			long start = System.currentTimeMillis();
			log.info("Updating " + host.getSimpleName() + "...");
			if (changeSet instanceof InteractiveChangeSet)
				((InteractiveChangeSet) changeSet).setInteraction(interaction);
			DeployTask[] tasks = changeSet.getTasks(host, passwordCache);
			DeployTaskInfo info = new DeployTaskInfo(interaction, host, user, passwordCache, sshClientCache);
			for(DeployTask task : tasks) {
				taskSectionId = interaction.startSection(task.getDescription(info));
				String result = task.perform(info);
				interaction.endSection(taskSectionId, true, result);
			}
			interaction.endSection(sectionId, true, "DONE in " + (System.currentTimeMillis() - start) + " ms");
			interaction.printf("Successfully deployed %1$s to %2$s", changeSet.getName(), host.getSimpleName());
		} catch(IOException e) {
			log.warn("Could not execute tasks", e);
			if(taskSectionId != 0)
				interaction.endSection(taskSectionId, false, "Task ERROR: " + e.getMessage());
			interaction.endSection(sectionId, false, "ERROR: " + e.getMessage());
			interaction.printf("Error while deploying %1$s to %2$s because: %3$s", changeSet.getName(), host.getSimpleName(), e.getMessage());
		} catch(InterruptedException e) {
			log.warn("Interrupted while executing tasks", e);
			if(taskSectionId != 0)
				interaction.endSection(taskSectionId, false, "Task ERROR: " + e.getMessage());
			interaction.endSection(sectionId, false, "ERROR: " + e.getMessage());
			interaction.printf("Interrupted while deploying %1$s to %2$s", changeSet.getName(), host.getSimpleName());
		} catch(RuntimeException e) {
			log.warn("Could not execute tasks", e);
			if(taskSectionId != 0)
				interaction.endSection(taskSectionId, false, "Task ERROR: " + e.getMessage());
			interaction.endSection(sectionId, false, "ERROR: " + e.getMessage());
			interaction.printf("Error while deploying %1$s to %2$s because: %3$s", changeSet.getName(), host.getSimpleName(), e.getMessage());
		} catch(Error e) {
			log.warn("Could not execute tasks", e);
			if(taskSectionId != 0)
				interaction.endSection(taskSectionId, false, "Task ERROR: " + e.getMessage());
			interaction.endSection(sectionId, false, "ERROR: " + e.getMessage());
			interaction.printf("Error while deploying %1$s to %2$s because: %3$s", changeSet.getName(), host.getSimpleName(), e.getMessage());
		}
	}

	protected void deploy(ChangeSet changeSet, List<Host> hosts, Interaction interaction, String user, PasswordCache passwordCache, SshClientCache sshClientCache) {
		interaction.clear();
		interaction.printf("Starting deployment of %1$s at %2$tD %2$tT", changeSet.getName(), new Date());	
		// List<Host> standbyHosts=new ArrayList<Host>();
		for(Host host : hosts) {
			/*
								if(changeSet instanceof AbstractLinuxChangeSet){
								boolean registered = ((AbstractLinuxChangeSet) changeSet).isAppRegistered(USATRegistry.POSTGRES_KLS);
								if(host.isServerType("KLS") && registered && PgDeployMap.standbyMap.get(host.getSimpleName()) != null) {
								standbyHosts.add(PgDeployMap.standbyMap.get(host.getSimpleName()));
								}
								}*/
			deployOnHost(changeSet, interaction, user, passwordCache, sshClientCache, host);
		}
		/*
		for(Host host:standbyHosts){
			deployOnHost(changeSet, standbyHosts, interaction, user, passwordCache, sshClientCache, host);
		}*/
		interaction.printf("Completed deployment of %1$s at %2$tD %2$tT", changeSet.getName(), new Date());		
	}
/*
	protected OutputStream writePrivilegedFile(Interaction interaction, SshClient ssh, SftpClient sftp, final String path) throws IOException, InterruptedException {
		return new ByteArrayOutputStream() {
			@Override
			public void close() throws IOException {
				log.info("Wrote " + size() + " bytes to privileged file at '" + path + "'");
				super.close();
			}
		};
	}
	protected InputStream readPrivilegedFile2(Interaction interaction, SshClient ssh, SftpClient sftp, String path) throws IOException, InterruptedException {
		final StringBuilder buffer = new StringBuilder();
		final ReentrantLock lock = new ReentrantLock();
		final Condition signal = lock.newCondition();
		final SessionChannelClient session = ssh.openSessionChannel();
    	try {
    		session.addEventListener(new ChannelEventAdapter() {
				public void onDataReceived(Channel channel, byte[] data) {
					String s = new String(data);
		        	lock.lock();
					try {
						buffer.append(s);
		        		int pos = StringUtils.lastIndexOf(buffer, new char[] {10,13});
			        	String line;
			        	if(pos < 0)
			        		line = buffer.toString();
			        	else
			        		line = buffer.substring(pos+1);
		        		if(sessionPromptPattern.matcher(line).matches()) {
		        			log.debug("Received data:\n" + buffer.toString());
							buffer.setLength(0);
							signal.signal();
						}
					} finally {
						lock.unlock();
					}
				}
			});
			if(!session.requestPseudoTerminal(deployerTerminal))
	            throw new IOException("The server failed to allocate a pseudo terminal");
			if (!session.startShell())
	            throw new IOException("The server failed to start a shell");
			lock.lockInterruptibly();
			try {
				signal.await();
			} finally {
				lock.unlock();
			}
			PrintWriter writer = new PrintWriter(session.getOutputStream());
			writer.write("sudo su\n");
			writer.flush();
			lock.lockInterruptibly();
			try {
				signal.await();
			} finally {
				lock.unlock();
			}
			SftpSubsystemClient sftpSub = new SftpSubsystemClient();
			ssh.openChannel(sftpSub);
			if(!sftpSub.initialize())
	            throw new SshException("The SFTP Subsystem could not be initialized");
			//This still doesn't work.
			final long size = sftpSub.getAttributes(path).getSize().longValue();
			SftpFile file = sftpSub.openFile(path, SftpSubsystemClient.OPEN_READ);
			return new SftpFileInputStream(file);
    	} finally {
    		session.close();
    	}
	}
	public static InputStream readPrivilegedFile(Interaction interaction, final SshClient ssh, SftpClient sftp, String path) throws IOException, InterruptedException {
		final StringBuilder buffer = new StringBuilder();
		final ReentrantLock lock = new ReentrantLock();
		final Condition signal = lock.newCondition();
		boolean okay = false;
		final SessionChannelClient session = ssh.openSessionChannel();
    	try {
    		final AtomicLong remaining = new AtomicLong(-1);
			final BufferStream bufferStream = new BufferStream() {
				@Override
				protected boolean done() {
					lock.lock();
					try {
						long l = remaining.get();
						log.debug("Remaining = " + l + " with pos = " + pos + "; count = " + count);
						if(l == 0 && pos >= count)
							return true;
						while(l < 0 || pos >= count) {
							signal.await();
							l = remaining.get();
							log.debug("Remaining = " + l + " (loop) with pos = " + pos + "; count = " + count);
						}
					} catch(InterruptedException e) {
						//do nothing
					} finally {
						lock.unlock();
					}
					
					return false;
				}
			};
			final boolean useSSHEOL = true;
			OutputStream capturingOutputStream = new FilterOutputStream(bufferStream.getOutputStream()) {
				protected final byte[] ONE = new byte[1];
				protected byte[] eol;
				protected int eolPartial = 0;
				protected int stage = 1;// 1 = wait for prompt; 2 = command echo; 3 = size; 4 = data
				@Override
				public void write(int b) throws IOException {
					ONE[0] = (byte)b;
					write(ONE, 0, 1);
				}
				@Override
				public void write(byte[] b, int off, int len) throws IOException {
					switch(stage) {
						case 1:
							String s = new String(b, off, len);
				        	lock.lock();
							try {
								buffer.append(s);
				        		int pos = StringUtils.lastIndexOf(buffer, new char[] {10,13});
					        	String line;
					        	if(pos < 0)
					        		line = buffer.toString();
					        	else
					        		line = buffer.substring(pos+1);
				        		if(sessionPromptPattern.matcher(line).matches()) {
									buffer.setLength(0);
									signal.signal();
									stage++;
								}
							} finally {
								lock.unlock();
							}
							break;
						case 2:
							if(useSSHEOL) {
								if(eol == null)
									eol = ssh.getRemoteEOLString().getBytes();
								int pos = ByteArrayUtils.indexOf(b, off, len, eol, eolPartial, eol.length);
								if(pos == -1) {
									eolPartial = 0;
								} else if(pos < 0) { //potential partial
									eolPartial = -pos - 1;
								} else { // we found the eol
									stage++;
									len = len + off - pos - eol.length;
									off = pos + eol.length;		
									eolPartial = 0;
								}
							} else if(eol == null || eol.length == 0) {
								OUTER: for(int i = off; i < off + len; i++) {
									byte ch = b[i];
									switch(ch) {
										case 10: case 13:
											if(i+1 < off+len) {
												stage++;
												switch(b[i+1]) {
													case 10: case 13:
														if(b[i+1] != ch) {
															eol = new byte[]{b[i], b[i+1]};
															len = off + len - i - 2;
															off = i + 2;
															break OUTER;
														}
														//fall-thru
													default:
														eol = new byte[]{b[i]};
														len = off + len - i - 1;
														off = i + 1;
														break OUTER;							
												}
											} else {
												//special case when next byte may be part of eol
												eol = new byte[]{b[i]};
											}
									}
								}
							} else if(len > 0){
								stage++;
								switch(b[off]) {
									case 10: case 13:
										if(b[off] != eol[0]) {
											eol = new byte[]{eol[0], b[off]};
											off++;
											len--;	
										}
								}
							}
							if(stage != 3)
								break;
						case 3:
							int pos = ByteArrayUtils.indexOf(b, off, len, eol, eolPartial, eol.length);
							if(pos == -1) {
								if(eolPartial > 0) {
									buffer.append(new String(eol, 0, eolPartial));
									eolPartial = 0;
								}
								buffer.append(new String(b, off, len));
							} else if(pos < 0) { //potential partial
								if(eolPartial > 0 && len > eol.length - eolPartial - 1) {
									buffer.append(new String(eol, 0, eolPartial));
								}
								eolPartial = -pos - 1;
								buffer.append(new String(b, off, len + pos + 1));
							} else { // we found the eol
								stage++;
								buffer.append(new String(b, off, pos - off));
								try {
									remaining.set(ConvertUtils.getLong(buffer.toString()));
								} catch(ConvertException e) {
									throw new IOException(e);
								}
								buffer.setLength(0);
								len = len + off - pos - eol.length;
								off = pos + eol.length;
								lock.lock();
								try {
									signal.signal();
								} finally {
									lock.unlock();
								}
							}
							if(stage != 4)
								break;
						case 4:
							if(len > 0) {
								if(remaining.get() < len)
									len = (int)remaining.get();
								lock.lock();
								try {
									out.write(b, off, len);
									log.debug("Remaining = " + remaining.get() + " after writing " + len + " bytes with count = " +  bufferStream.size());
									signal.signal();
								} finally {
									lock.unlock();
								}
								long l = remaining.addAndGet(-len); 
								if(l <= 0)
									close();
								log.debug("Remaining = " + l + " after writing " + len + " bytes");
							}
					}
				}

				@Override
				public void close() throws IOException {
					super.close();
					session.close();
				}
			};
			session.bindOutputStream(capturingOutputStream);			
			if(!session.requestPseudoTerminal(deployerTerminal))
	            throw new IOException("The server failed to allocate a pseudo terminal");
			if (!session.startShell())
	            throw new IOException("The server failed to start a shell");
			lock.lockInterruptibly();
			try {
				signal.await();
			} finally {
				lock.unlock();
			}
			PrintWriter writer = new PrintWriter(session.getOutputStream());
			writer.write("sudo ls -go ");
			writer.write(path);
			writer.write(" | cut -c15-22; sudo cat ");
			writer.write(path);
			writer.write('\n');
			writer.flush();
			
			okay = true;
			return bufferStream.getInputStream();
    	} finally {
    		if(!okay)
    			session.close();
    	}
		
	}
	
	public static InputStream signCertificate(Interaction interaction, String csrContent) throws HttpException, IOException {
		//Sign entries
		HttpClient httpClient = new HttpClient();
		String url= "https://usasubca.usatech.com/certsrv/certfnsh.asp";
		HttpState state = new HttpState();
		String causername = System.getProperty("user.name");
		String capassword = new String(interaction.readPassword("Enter the password for user '%1$s' at '%2$s':", causername, url));
		state.setCredentials(new AuthScope(null, -1, null, "basic"), new UsernamePasswordCredentials(causername, capassword));
		
		PostMethod method = new PostMethod(url);
		method.addParameter("CertRequest", csrContent);
		method.addParameter("TargetStoreFlags", "0");
		method.addParameter("SaveCert", "yes");
		method.addParameter("Mode", "newreq");
		method.addParameter("CertAttrib", "CertificateTemplate:" + "USATWebServer");
		method.getHostAuthState().setAuthScheme(new BasicScheme());
		
		int rc = httpClient.executeMethod(null, method, state);
		if(rc != 200)
			throw new IOException("Could not sign certificate. Got http code " + rc);
		int requestId = 0;
		BufferedReader reader = new BufferedReader(new InputStreamReader(method.getResponseBodyAsStream()));
		Pattern pattern = Pattern.compile("\\?ReqID=(\\d+)\\&");
		String line;
		while((line=reader.readLine()) != null) {
			Matcher matcher = pattern.matcher(line);
			if(matcher.find()) {
				try {
					requestId = ConvertUtils.getInt(matcher.group(1));
				} catch(ConvertException e) {
					log.warn("Could not convert requestId", e);
				}
				break;
			}
		}
		if(requestId == 0)
			throw new IOException("Could not find requestId in response");
		url = "https://usasubca.usatech.com/certsrv/certnew.p7b?ReqID=" + requestId + "&Enc=b64";
		GetMethod getMethod = new GetMethod(url);
		getMethod.getHostAuthState().setAuthScheme(new BasicScheme());
		
		rc = httpClient.executeMethod(null, getMethod, state);
		if(rc != 200)
			throw new IOException("Could not get certificate. Got http code " + rc);
		Header contentTypeHeader = getMethod.getResponseHeader("Content-Type");
		if(contentTypeHeader != null && contentTypeHeader.getValue() != null && contentTypeHeader.getValue().startsWith("text/html")) {
			throw new IOException("Did not get DER-encoded certificate chain. Instead got:\n" + getMethod.getResponseBodyAsString());
		}
		return getMethod.getResponseBodyAsStream();
	}
	//protected static final Pattern SERVER_HOSTNAME_PATTERN = Pattern.compile("((?:\\d{1,3}\\.){3}\\d{1,3})|(?:([^.]+)(\\..*)?)");
	//TODO: I wish there was a better way to figure out the FQDN
	protected String getServerHostname(String host) throws UnknownHostException {
		String hostname = InetAddress.getByName(host).getCanonicalHostName();
		int pos = hostname.indexOf('.');
		if(pos > 0)
			return hostname;
		if(hostname.startsWith("usa"))
			return hostname + ".trooper.usatech.com";
		else
			return hostname + ".usatech.com";
	}

	protected File getFileFromClasspath(String resourceName) throws URISyntaxException {
		URL url = getClass().getClassLoader().getResource(resourceName);
		return new File(url.toURI());
	}	*/
}
