package com.usatech.tools.deploy;

import java.io.IOException;

import simple.io.HeapBufferStream;

import com.sshtools.j2ssh.FileTransferProgress;
/**
 * This task will ssh-keygen for the srcHost srcUser and put the public key entry to the destHost destUser .ssh/authorized_keys file
 * This is for srcHost srcUser to access destHost using destUser via ssh or rsync
 * @author yhe
 *
 */
public class RsaSshSetupTask implements DeployTask {
	
	public Host srcHost;
	public Host destHost;
	public String srcUser;
	public String destUser;
	public int srcUid;
	public int destUid;
	

	public RsaSshSetupTask(Host srcHost, Host destHost, String srcUser,
			int srcUid, String destUser, int destUid) {
		super();
		this.srcHost = srcHost;
		this.destHost = destHost;
		this.srcUser = srcUser;
		this.destUser = destUser;
		this.srcUid = srcUid;
		this.destUid = destUid;
	}

	@Override
	public String perform(DeployTaskInfo deployTaskInfo) throws IOException,
			InterruptedException {
		String[] ret = ExecuteTask.executeCommands(srcHost, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache.getSshClient(srcHost, deployTaskInfo.user),
				"sudo su", 
				"if [ `egrep -c '^"+srcUser+":' /etc/passwd` -eq 0 ]; then groupadd -g "+srcUid+" "+srcUser+"; useradd -g "+srcUid+" "+srcUser+"; fi",
				"if [ -e /home/" + srcUser + "/.ssh/id_rsa ]; then echo 'Y'; else echo 'N'; fi" );
		if("N".equals(ret[2].trim())) {
			deployTaskInfo.interaction.getWriter().format("Generating new rsa key for use with rsync");
			ExecuteTask.executeCommands(srcHost, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache.getSshClient(srcHost, deployTaskInfo.user), 
					"sudo su - " + srcUser + " 'ssh-keygen -t rsa -b 2048 -f /home/" + srcUser + "/.ssh/id_rsa -N \"\"'");
			HeapBufferStream buffer = new HeapBufferStream(false);
			String srcFilePath = "/home/" + srcUser + "/.ssh/id_rsa.pub";
			FileTransferProgress progress = UploadTask.makeFileTransferProgress(deployTaskInfo.interaction, false, srcFilePath);
			UploadTask.readFileUsingTmp(srcHost, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, srcFilePath, buffer.getOutputStream(), progress);
			srcFilePath = "/home/" + destUser + "/.ssh/authorized_keys";
			ExecuteTask.executeCommands(destHost, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache.getSshClient(destHost, deployTaskInfo.user),
					"sudo su",
					"if [ `egrep -c '^"+destUser+":' /etc/passwd` -eq 0 ]; then groupadd -g "+destUid+" "+destUser+"; useradd -g "+destUid+" "+destUser+"; fi",
					DeployUtils.getMakeDir("/home/" + destUser + "/.ssh", destUser, destUser, 0700),
					"if [ -r " + srcFilePath + " ]; then mv " + srcFilePath + " " + srcFilePath + "." + System.currentTimeMillis() + "; fi");			
			progress = UploadTask.makeFileTransferProgress(deployTaskInfo.interaction, true, "");
			UploadTask.writeFileUsingTmp(destHost, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, buffer.getInputStream(), 0600, "destUser", "destUser", progress, srcFilePath);
			return "Created new rsa key on " + srcHost.getSimpleName() + " for use with rsync and put it on " + destHost.getSimpleName();
		}
		
		return "Rsa key already exists";
	}

	@Override
	public String getDescription(DeployTaskInfo deployTaskInfo) {
		return "Generate ssh key on srcHost and setup authorized_keys file on destHost"; 
	}

}
