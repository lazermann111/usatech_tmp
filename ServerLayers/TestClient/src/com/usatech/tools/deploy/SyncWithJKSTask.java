package com.usatech.tools.deploy;

import java.security.KeyStore;
import java.security.KeyStore.Entry;
import java.security.KeyStoreException;
import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.Set;

import simple.security.crypt.CryptUtils.CopyEntryFilter;
import simple.util.IteratorEnumeration;

public class SyncWithJKSTask extends ImportJKSEntriesTask {
	protected final String prefix;
	protected final Set<String> selectedAliases;
	protected static class PrefixedCopyEntryFilter implements CopyEntryFilter {
		protected final String prefix;

		public PrefixedCopyEntryFilter(String prefix) {
			this.prefix = prefix;
		}
		@Override
		public String getTargetAlias(String sourceAlias, Entry sourceEntry) {
			return prefix + sourceAlias;
		}
	}

	public SyncWithJKSTask(String sourceKeystorePath, String targetKeystorePath, PasswordCache cache, int targetKeystorePermissions, String prefix) {
		this(sourceKeystorePath, targetKeystorePath, cache, targetKeystorePermissions, prefix, null);
	}

	public SyncWithJKSTask(String sourceKeystorePath, String targetKeystorePath, PasswordCache cache, int targetKeystorePermissions, String prefix, Set<String> selectedAliases) {
		super(sourceKeystorePath, targetKeystorePath, cache, targetKeystorePermissions, new PrefixedCopyEntryFilter(prefix));
		this.prefix = prefix;
		this.selectedAliases = selectedAliases;
	}

	protected Enumeration<String> getAliases(KeyStore sourceKeystore, KeyStore targetKeystore) throws KeyStoreException {
		Set<String> aliases = new LinkedHashSet<String>();
		if(selectedAliases == null) {
			for(Enumeration<String> aliasEnum = sourceKeystore.aliases(); aliasEnum.hasMoreElements();)
				aliases.add(aliasEnum.nextElement());
			for(Enumeration<String> aliasEnum = targetKeystore.aliases(); aliasEnum.hasMoreElements();) {
				String alias = aliasEnum.nextElement();
				if(alias.startsWith(prefix))
					aliases.add(alias.substring(prefix.length()));
			}
		} else {
			for(String selectedAlias : selectedAliases) {
				if(sourceKeystore.containsAlias(selectedAlias) || targetKeystore.containsAlias(prefix + selectedAlias))
					aliases.add(selectedAlias);
			}
		}
		return new IteratorEnumeration<String>(aliases.iterator());
	}

	protected boolean isRemoveEntries() {
		return true;
	}
}
