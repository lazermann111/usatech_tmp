/**
 * 
 */
package com.usatech.tools.deploy;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrSubstitutor;

import simple.app.Processor;
import simple.app.ServiceException;

/** 
 * This task is capable of a number of SSL operations operating on a key.
 * 
 * That number is currently 3.
 * 
 * <code><pre>
 * 1) openssl.exe pkcs8 -topk8 -inform PEM -outform DER -in d:\development\cce_fill_rsa_private -passin pass:test -out d:\development\cce_fill_rsa_clearprivate -nocrypt
 * 2) openssl.exe pkcs8 -topk8 -inform PEM -outform PEM -in d:\development\cce_fill_rsa_private -passin pass:test -out d:\development\cce_fill_rsa_clearprivate -nocrypt
 * 3) openssl.exe req -new -x509 -key d:\Development\cce_fill_rsa_clearprivate -keyform DER -out D:/development/cce_fill_rsa_cert -days 365 -batch -config batch.cfg
 * </pre></code>
 * 
 * The batch.cfg (in resources/) file specifies the DN:
 * <pre>
 * RANDFILE               = $ENV::HOME/.rnd

[ req ]
distinguished_name     = req_distinguished_name
attributes             = req_attributes
prompt                 = no
output_password        = 

[ req_attributes ]

[ req_distinguished_name ]
C                      = US
ST                     = PA
L                      = Malvern
O                      = USA Technologies
OU                     = Network Services
emailAddress           = SoftwareDevelopmentTeam@usatech.com
CN                     = CCEFILL

 * </pre>
 */
public class OpenSSLTask implements DeployTask {
	
	private static final String OPENSSL_CMD = "openssl";
	
	public enum Operation { 
			
		CONVERT_PEM_TO_CLEARTEXT_DER(
				null,
				"Decrypt a PEM RSA into a DER file",
				"${openssl} ${tool} ${op} -inform ${inform} -outform ${outform} -in ${in} -passin ${passin} -out ${out}",
				"openssl", OPENSSL_CMD,
				"tool", "pkcs8",
				"op", "-topk8",
				"inform", "PEM",
				"outform", "DER",
				"in", "${inputFile1}",
				"out", "${derFile1}", 
				"passin", null,
				"-nocrypt"),
				
		CONVERT_PEM_TO_CLEARTEXT_PEM(
				CONVERT_PEM_TO_CLEARTEXT_DER, /* This operation also results in a DER file for your convenience. */
				"Decrypt a PEM RSA into another PEM file (use for creating X509 certificates)",
				"${openssl} ${tool} ${op} -inform ${inform} -outform ${outform} -in ${in} -passin ${passin} -out ${out}",
				"openssl", OPENSSL_CMD,
				"tool", "pkcs8",
				"op", "-topk8",
				"inform", "PEM",
				"outform", "PEM",
				"in", "${inputFile1}",
				"out", "${intermediateFile1}", 
				"passin", null,
				"-nocrypt"),
				
		NEW_X509_CERTIFICATE_REQUEST_WITH_PRECONVERT (
				CONVERT_PEM_TO_CLEARTEXT_PEM,
				"Create an X509 Certificate",
				"${openssl} ${tool} ${op} -key ${key} ${keyform} -out ${out} ${expiry} ${batch}",
				"openssl", OPENSSL_CMD,
				"tool", "req",
				"op","-new -x509",
				"keyform", "-keyform ${keyformDefault}",
				"keyformDefault", "PEM",
				"expiry", "-days ${expiryDaysDefault}",
				"expiryDaysDefault", "365",
				"batch", "-batch -config ${batchFile}",
				"batchFile", null, 
				"key", "${intermediateFile1}",
				"out", "${resultFile1}"
				);

		private String operationTemplate; 
		private HashMap<String, String> toolPresets = null;
		private String title;
		private Operation prerequisite;

		private Operation(Operation prereq, String title, String operationTemplate, String... operationKeyPairs) {
			this.prerequisite = prereq;
			this.title = title;
			StringBuffer buf = new StringBuffer(operationTemplate);
			int i = 0;
			while(i+2 <= operationKeyPairs.length)
			{
				if(toolPresets == null) { toolPresets = new HashMap<String, String>(operationKeyPairs.length/2); }
				toolPresets.put(operationKeyPairs[i], operationKeyPairs[i+1]);
				i+=2;
			}
			buf.append(" ");
			if(i + 1 == operationKeyPairs.length)
			{
				buf.append(operationKeyPairs[i]);
			}
			this.operationTemplate = buf.toString();
		}		
		
		public HashMap<String, String> getToolPresets() {
			return toolPresets;
		}

		public String getOperationTemplate() {
			return operationTemplate;
		}

		public String getTitle() {
			return title;
		}

		public Operation getPrerequisite() {
			return prerequisite;
		}
	}
	
	private String description;
	private Operation operation;
	private String passwordKey;
	private File outDerFile;
	private File outPubFile;
	private Processor<DeployTaskInfo, InputStream> certBatchConfigInput;
	private String privateKeyHostFrom;
	private String privateKeyPath;
	
	public OpenSSLTask(Operation operation, File certBatchConfig, String passwordKey, String description, File outPubFile, File outDerFile, String privateKeyHostFrom, String privateKeyPath) {
		this(operation, new FileContent(certBatchConfig), passwordKey, description, outPubFile, outDerFile, privateKeyHostFrom, privateKeyPath);
	}

	public OpenSSLTask(Operation operation, Processor<DeployTaskInfo, InputStream> certBatchConfigInput, String passwordKey, String description, File outPubFile, File outDerFile, String privateKeyHostFrom, String privateKeyPath) {
		this.description = description;
		this.operation = operation;
		this.passwordKey = passwordKey;
		this.outDerFile = outDerFile;
		this.outPubFile = outPubFile;
		this.certBatchConfigInput = certBatchConfigInput;
		this.privateKeyHostFrom=privateKeyHostFrom;
		this.privateKeyPath=privateKeyPath;
	}
	@Override
	public String perform(DeployTaskInfo taskInfo) throws IOException, InterruptedException {
		taskInfo.interaction.printf("Using remote server to convert RSA key described as '%s' on server %s", description, taskInfo.host.getFullName());
				
		taskInfo.interaction.printf("Uploading input files to the server.");

		String tmpInputFile1 = UploadTask.getTmpFilePath(taskInfo.user);
		String tmpBatchCfgPath = UploadTask.getTmpFilePath(taskInfo.user);
		String tmpIntermediateFile1 = UploadTask.getTmpFilePath(taskInfo.user);
		String tmpResultFile1 = UploadTask.getTmpFilePath(taskInfo.user);
		String tmpDerFile1 = UploadTask.getTmpFilePath(taskInfo.user);
		
		InputStream batchCfgFile;
		try {
			batchCfgFile = certBatchConfigInput.process(taskInfo);
		} catch(ServiceException e) {
			throw new IOException(e);
		}
		UploadTask.writeFile(taskInfo, batchCfgFile, 0700, taskInfo.user, "usat", null, tmpBatchCfgPath);
		
		Host hostFrom = new HostImpl("", privateKeyHostFrom);
		File tmpFile = File.createTempFile("cce-fill-rsa-copy-", ".tmp");
		try {
			UploadTask.readFile(hostFrom, taskInfo.passwordCache, taskInfo.interaction, taskInfo.sshClientCache, taskInfo.user, privateKeyPath, new FileOutputStream(tmpFile), null);
			UploadTask.writeFile(taskInfo, new FileInputStream(tmpFile), 0700, taskInfo.user, "usat", null, tmpInputFile1);
		} finally {
			tmpFile.delete();
		}
					
		//get the password for the private key
		char[] keyPassword = DeployUtils.getPassword(taskInfo, passwordKey, "Private key described as '"+description);
		
		// we will be passing this password to the server for the conversion of the private key.
		Map<String, String> parameterKeys = new HashMap<String, String>();
					
		parameterKeys.put("passin", "pass:" + new String(keyPassword).replaceAll("([()&\\\\'\";])", "\\\\$1"));
		parameterKeys.put("inputFile1", tmpInputFile1);
		parameterKeys.put("intermediateFile1", tmpIntermediateFile1);
		parameterKeys.put("resultFile1", tmpResultFile1);
		parameterKeys.put("derFile1", tmpDerFile1);
		parameterKeys.put("batchFile", tmpBatchCfgPath);
		
		boolean success;
		if(executeOperation(taskInfo, operation, parameterKeys))
		{
			taskInfo.interaction.printf("Successfully created PKCS8 public/private key/cert pair on remote system using open SSL. Downloading.");
			
			FileOutputStream fos = new FileOutputStream(outDerFile);
			UploadTask.readFileUsingTmp(taskInfo, tmpDerFile1, fos, null);
			fos.close();

			fos = new FileOutputStream(outPubFile);				
			UploadTask.readFileUsingTmp(taskInfo, tmpResultFile1, fos, null);
			fos.close();
			success = true;
		} else
			success = false;

		ExecuteTask.executeCommands(taskInfo, 
				"rm " + tmpInputFile1, 
				"rm " + tmpBatchCfgPath, 
				"rm " + tmpIntermediateFile1,
				"rm " + tmpResultFile1, 
				"rm " + tmpDerFile1);	
		return success ? "Successfully performed " + operation : "Failed to perform " + operation;
	}

	private boolean executeOperation(DeployTaskInfo taskInfo, Operation thisOp, Map<String, String> parameterKeys) throws IOException, InterruptedException {
		
		if(thisOp.getPrerequisite() != null)
		{
			if(!executeOperation(taskInfo, thisOp.getPrerequisite(), parameterKeys)) 
			{
				taskInfo.interaction.printf("Failed to perform operation: %1$s", thisOp.name());
				return false;
			}
		}
		
		// supports overriding and chaining prereq operations by doing a multiple pass replace.
		String opCommand = thisOp.getOperationTemplate();
		int start;
		int left = StringUtils.countMatches(opCommand,  "${");
		do 
		{
			start = left;
			
			opCommand = StrSubstitutor.replace(thisOp.getOperationTemplate(), thisOp.getToolPresets());
			opCommand = StrSubstitutor.replace(opCommand, parameterKeys);
			
			left = StringUtils.countMatches(opCommand,  "${");
		}
		while(left < start && left > 0);
		

		String[] results = ExecuteTask.executeCommands(taskInfo, opCommand + "; echo $?");
		
		return results != null && results.length == 1 && results[0] != null && results[0].charAt(0) == '0';
	}

	@Override
	public String getDescription(DeployTaskInfo deployTaskInfo) {
		StringBuilder sb = new StringBuilder();
		sb.append("Performing ").append(operation).append(" for '").append(description).append("'");
		return sb.toString();
	}
}
