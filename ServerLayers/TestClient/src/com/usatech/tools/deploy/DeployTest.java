package com.usatech.tools.deploy;

import java.awt.BorderLayout;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;

import simple.bean.ConvertUtils;
import simple.io.GuiInteraction;
import simple.io.InOutInteraction;
import simple.io.Interaction;
import simple.io.resource.sftp.SftpUtils;
import simple.lang.Holder;
import simple.lang.SystemUtils;
import simple.security.SecurityUtils;
import simple.security.crypt.CryptUtils;
import simple.text.StringUtils;
import simple.util.CaseInsensitiveComparator;

import com.sshtools.j2ssh.SshClient;
import com.sshtools.j2ssh.session.SessionChannelClient;

public class DeployTest {

	/**
	 * @param args
	 * @throws UnknownHostException
	 */
	public static void main(String[] args) throws Exception {
		// testRoutingIPTables();
		// getKMSAppletSigningCert();
		// testCVSPull("Simple1.5/build_base.xml");
		// printConnectionInfoCsv();
		// renewKeyInKeyStore();
		printAppInfoWiki();
		/*
		Collection<String> hostNames;
		try {
			hostNames = getHostNames(IPTablesUpdateTask.normalizeIp("usaapp31"));
		} catch(UnknownHostException e) {
			System.out.println("Unknown host");
			e.printStackTrace();
			return;
		}
		System.out.println(hostNames);
		*/
	}

	protected static void testCVSPull(String fileName) throws IOException {
		String user = System.getProperty("remoteUserName", System.getProperty("user.name"));
		final Interaction interaction = new InOutInteraction();
		final PasswordCache passwordCache = new MapPasswordCache();
		final SshClientCache sshClientCache = new DefaultSshClientCache(interaction, SftpUtils.createHostKeyVerification(System.getProperty("user.home") + "/knownhosts.lst"), passwordCache);
		SshClient ssh = sshClientCache.getSshClient(new HostImpl("LOCAL", "cvs.usatech.com"), user);
		SessionChannelClient sshSession = ssh.openSessionChannel();
		boolean ok = sshSession.executeCommand("cvs server");
		System.out.println("Execute command result: " + ok);
		InputStream instream = new DataInputStream(sshSession.getInputStream());
		OutputStream outstream = new DataOutputStream(sshSession.getOutputStream());
		String[] parts = StringUtils.split(fileName, "/\\".toCharArray(), true);
		String dir = StringUtils.join(parts, "/", 0, parts.length - 1);
		String file = parts[parts.length - 1];
		writeLine(outstream, "Root /usr/local/cvsroot/NetworkServices");
		writeLine(outstream, "Argument -rHEAD");
		writeLine(outstream, "Argument " + fileName);

		// writeLine(outstream, "Directory " + dir);
		// writeLine(outstream, "Valid-responses " + "E M ok error Valid-requests " + "Created Merged Updated Update-existing " + "Removed Remove-entry New-entry " +
		// "Checked-in Checksum Copy-file Notified " + "Clear-sticky Set-sticky " + "Clear-static-directory Set-static-directory " + "");
		// writeLine(outstream, "Argument " + fileName);

		// writeLine(outstream, "Entry /" + file + "////THEAD");
		// writeLine(outstream, "Lost " + file);
		writeLine(outstream, "export");
		String line;
		while((line = readLine(instream)) != null) {
			System.out.println("Response Line: " + line);
			String resp = StringUtils.substringBefore(line, "");
			String args = StringUtils.substringAfter(line, "");
			if(line.startsWith("Updated ")) {
				String newEntries = readLine(instream);
				String version = readLine(instream);
				String mode = readLine(instream);
				String filesize = readLine(instream);
				System.out.println("Update directory: " + args + " with new entries " + newEntries + " [" + version + "] (" + mode + ") of size " + filesize);
				int size = Integer.parseInt(filesize);
				long skipped = instream.skip(size);
				System.out.println("Skipped " + skipped + " bytes of content");
			}
			if("ok".equals(line) || line.startsWith("error"))
				break;
		}
	}

	protected static void writeLine(OutputStream outstream, String line) throws IOException {
		// outstream.write((line + "\012").getBytes());
		outstream.write((line + "\n").getBytes());
		outstream.flush();
	}
	protected static String readLine(InputStream instream) {
		char ch;
		StringBuilder line = new StringBuilder(512); // REVIEW Better number? Avg?
		try {
			for(;;) {
				int inByte = instream.read();
				if(inByte == -1) {
					if(line.length() == 0)
						line = null;
					break;
				}

				ch = (char) inByte;
				if(ch == '\n' /*'\012'*/) {
					break;
				}

				line.append(ch);
			}
		} catch(IOException ex) {
			line = null;
		}

		return (line != null ? line.toString() : null);
	}
	protected static void getKMSAppletSigningCert() throws IOException, KeyStoreException, NoSuchProviderException, NoSuchAlgorithmException, CertificateException, InvalidKeyException, UnrecoverableKeyException, SignatureException {
		File jksFile = new File("D:\\Development\\Java Projects\\LayersCommon\\conf\\kls\\keystore.ks");
		String alias = InetAddress.getLocalHost().getCanonicalHostName();
		char[] password = "usatech".toCharArray();
		String commonName = StringUtils.substringBefore(alias, ".");
		KeyStore keyStore = SecurityUtils.getKeyStore(new FileInputStream(jksFile), "JKS", null, password);

		final JFrame frame = new JFrame("Key Manager Service Reloader");
		frame.setSize(600, 400);
		frame.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		final JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		frame.getContentPane().add(mainPanel);
		final JPanel interactionPanel = new JPanel();
		interactionPanel.setLayout(new BorderLayout());
		mainPanel.add(interactionPanel, BorderLayout.CENTER);
		final Interaction interaction = new GuiInteraction(interactionPanel);
		frame.setVisible(true);
		StringBuilder csr = new StringBuilder();
		KeyPair pair = CryptUtils.extractCSR(keyStore, alias, password, "CN=" + commonName + ", O=USA Technologies\\, Inc, L=Malvern, S=PA, C=US", csr);
		InputStream certStream = DeployUtils.signCertificate("usasubca.usatech.com", "USATCodeSigning", System.getProperty("user.name"), interaction, new MapPasswordCache(), csr.toString());
		CryptUtils.importCertificateChain(keyStore, alias + "-applet", pair, certStream, password);
		keyStore.store(new FileOutputStream(jksFile), password);
	}
	protected static void testRoutingIPTables() throws IOException {
		String env = "DEV";
		final PrintWriter pw = new PrintWriter(System.out);
		IPTablesUpdateTask routingIPTTask = new IPTablesUpdateTask();
		Map<String, String> subHostPrefixMap = new HashMap<String, String>();
		subHostPrefixMap.put("usatech", "www");
		subHostPrefixMap.put("esuds", "esudsweb");
		for(App app : new App[] { USATRegistry.USALIVE_APP, USATRegistry.ESUDS_APP, USATRegistry.USATECH_WEBSITE_APP }) {
			String subHostPrefix = subHostPrefixMap.get(app.getName());
			if(subHostPrefix == null) {
				subHostPrefix = app.getName();
			}
			String subhostname = subHostPrefix + ("USA".equalsIgnoreCase(env) ? ".usatech.com" : "-" + env.toLowerCase() + ".usatech.com");
			String subhostip = InetAddress.getByName(subhostname).getHostAddress();
			routingIPTTask.prerouteTcp(null, null, subhostip, null, null, null);
		}
		pw.print("# ");
		pw.print(env);
		pw.print("web1");
		pw.println("x Rules: -----------------------------");
		pw.println(routingIPTTask.printAddCommands());
		pw.flush();
	}

	protected static void printAppInfoWiki() throws UnknownHostException {
		USATRegistry reg = new USATRegistry();
		Map<String,String> output = new TreeMap<String,String>();
		for(App app : USATRegistry.getPossibleApps()) {
			StringBuilder sb = new StringBuilder();
			sb.append("|[").append(app.getName()).append('|').append(app.getAppDesc().replaceAll("\\s+|Service$", "")).append("]|").append(app.getAppDesc()).append('|');
			if(app.getServerType() == null)
				sb.append("any|");
			else
				sb.append(app.getServerType()).append('|');
			if("postgres".equalsIgnoreCase(app.getName())) {
				sb.append(" | | | | ");
			} else {
				int basePort = 7000 + 100 * 7 + app.getPortOffset();
				sb.append(basePort + 1).append('|').append(basePort + 2).append('|').append(basePort + 7).append('|');
				if("netlayer".equalsIgnoreCase(app.getName()))
					sb.append(" |").append(9443);
				else if("keymgr".equalsIgnoreCase(app.getName()))
					sb.append(basePort + 80).append('|').append(basePort + 43);
				else if("httpd".equalsIgnoreCase(app.getName()))
					sb.append(80).append('|').append(443);
				else if(app.getPortOffset() > 100)
					sb.append(basePort + 80).append("| ");
				else
					sb.append(" | ");
			}
			output.put(app.getServerType() + '-' + app.getName().toUpperCase(), sb.toString());
		}
		final PrintWriter pw = new PrintWriter(System.out);
		pw.println("!!Applications");
		pw.println("||Name||Description||Server Type||Production JMX Registry Port||Production JMX Server Port||Production QueueLayer Port||Production HTTP Port||Production HTTPS Port");
		for(String v : output.values())
			pw.println(v);
		pw.println("!!Servers");
		pw.println("||Name||Environment||Server Type(s)");
		output.clear();
		for(Host host : reg.getHosts()) {
			StringBuilder sb = new StringBuilder();
			sb.append('|').append(host.getSimpleName()).append('|').append(host.getServerEnv()).append('|');
			for(String st : host.getServerTypes())
				sb.append(st).append(", ");
			output.put(host.getServerEnv() + '-' + host.getSimpleName().toUpperCase(), sb.substring(0, sb.length() - 2));
		}
		for(String v : output.values())
			pw.println(v);

		pw.flush();
	}
	protected static void testIPTables() throws IOException {
		String env = "ECC";
		boolean printTests = false;

		final PrintWriter pw = new PrintWriter(System.out);
		IPTablesUpdateTask iptTask;
		if(printTests) {
			iptTask = new IPTablesUpdateTask() {
				@Override
				public void allowTcpOut(String remoteHost, int port) throws IOException {
					for(String ip : normalizeIp(remoteHost)) {
						pw.print("nc -v ");
						pw.print(ip);
						pw.print(' ');
						pw.print(port);
						pw.println();
					}
					super.allowTcpOut(remoteHost, port);
				}

				@Override
				public void allowTcpOutWithPassive(String remoteHost, int port) throws IOException {
					for(String ip : normalizeIp(remoteHost)) {
						pw.print("nc -v ");
						pw.print(ip);
						pw.print(' ');
						pw.print(port);
						pw.println();
					}
					super.allowTcpOutWithPassive(remoteHost, port);
				}

				@Override
				public void allowTcpOutWithRelated(String remoteHost, int port, String[] relatedHosts, int relatedPort) throws IOException {
					for(String ip : normalizeIp(remoteHost)) {
						pw.print("nc -v ");
						pw.print(ip);
						pw.print(' ');
						pw.print(port);
						pw.println();
					}
					super.allowTcpOutWithRelated(remoteHost, port, relatedHosts, relatedPort);
				}
			};
		} else {
			iptTask = new IPTablesUpdateTask();
		}
		BasicServerSet serverSetMain = new BasicServerSet();
		serverSetMain.registerApp(USATRegistry.APP_LAYER);
		serverSetMain.registerApp(USATRegistry.INAUTH_LAYER);
		serverSetMain.registerApp(USATRegistry.POSM_LAYER);
		serverSetMain.registerApp(USATRegistry.NET_LAYER);
		serverSetMain.registerApp(USATRegistry.OUTAUTH_LAYER);
		serverSetMain.registerApp(USATRegistry.POSTGRES_MST);

		char seventh = (env.equalsIgnoreCase("USA") ? '3' : '1');
		int num = (env.equalsIgnoreCase("USA") ? 4 : 2);
		serverSetMain.registerServerSet(env, "MST", seventh, num, 4 / num);
		serverSetMain.registerServerSet(env, "NET", seventh, num, 4 / num);
		serverSetMain.registerServerSet(env, "APR", seventh, num, 4 / num);

		BasicServerSet serverSetDms = new BasicServerSet();
		serverSetDms.registerApp(USATRegistry.HTTPD_NET);
		serverSetDms.registerApp(USATRegistry.DMS_APP);
		serverSetDms.registerServerSet(env, "NET", seventh, num, 1);
		serverSetDms.registerServerSet(env, "APR", seventh, num, 1);
		serverSetDms.registerServerSet(env, "MST", seventh, num, 4 / num);

		for(String serverType : new String[] { "NET", "APR", "MST" }) {
			Host host = serverSetMain.getServers(serverType).get(0).host;
			if(printTests) {
				pw.print("# ");
				pw.print(env);
				pw.print(serverType);
				pw.print(seventh);
				pw.println("x Tests: -----------------------------");
			}
			UsatIPTablesUpdateTask.updateForUsat(iptTask, host, serverSetMain);
			UsatIPTablesUpdateTask.updateForUsat(iptTask, host, serverSetDms);
			pw.println();
			pw.print("# ");
			pw.print(env);
			pw.print(serverType);
			pw.print(seventh);
			pw.println("x Rules: -----------------------------");
			pw.println(iptTask.printAddCommands());
			iptTask.clearRules();
		}
		pw.flush();
	}

	protected static final Pattern ipAddressPattern = Pattern.compile("(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\s*(?:/(\\d{1,2}))?");
	protected static Collection<String> getHostNames(String[] ips) {
		Set<String> names = new LinkedHashSet<String>();
		byte[] addr = new byte[4];
		for(String ip : ips) {
			if(StringUtils.isBlank(ip))
				continue;
			Matcher matcher = ipAddressPattern.matcher(ip);
			if(matcher.matches()) {
				String wild = matcher.group(5);
				if(StringUtils.isBlank(wild) || "32".equals(wild)) {
					for(int i = 0; i < addr.length; i++) {
						int b = Integer.parseInt(matcher.group(i + 1));
						addr[i] = (byte) b;
					}
					try {
						names.add(InetAddress.getByAddress(addr).getCanonicalHostName());
					} catch(UnknownHostException e) {
						e.printStackTrace();
					}
				} else
					names.add(ip);
			} else
				names.add(ip);
		}

		return names;
	}

	protected static class ConnInfo {
		public final String to;
		public final String from;
		public final int port;
		public final Set<String> notes = new TreeSet<String>(new CaseInsensitiveComparator<String>());

		public ConnInfo(String from, String to, int port, String... notes) {
			super();
			this.to = to;
			this.from = from;
			this.port = port;
			if(notes != null && notes.length > 0)
				for(String note : notes)
					this.notes.add(note);
		}

		@Override
		public int hashCode() {
			return SystemUtils.addHashCodes(port, to, from);
		}

		@Override
		public boolean equals(Object obj) {
			if(obj instanceof ConnInfo) {
				ConnInfo ci = (ConnInfo) obj;
				return ci.port == port && ConvertUtils.areEqual(ci.to, to) && ConvertUtils.areEqual(ci.from, from);
			}
			return super.equals(obj);
		}

		@Override
		public String toString() {
			return new StringBuilder(from).append(" --> ").append(to).append(" on ").append(port).toString();
		}

		public void writeToCsv(PrintWriter pw) {
			StringUtils.writeCSVLine(new Object[] { from, to, port, StringUtils.join(notes, "; ") }, pw);
		}
	}

	protected static void mergeConnInfo(Map<ConnInfo, ConnInfo> connInfos, String from, String to, int port, String... notes) {
		ConnInfo ci = new ConnInfo(from, to, port, notes);
		ConnInfo old = connInfos.put(ci, ci);
		if(old != null)
			ci.notes.addAll(old.notes);
	}
	protected static void printConnectionInfoCsv() throws IOException {
		String env = "USA";
		File file = new File("C:\\Users\\bkrug\\Documents\\open_ports_PROD.csv");
		final PrintWriter pw = new PrintWriter(file);
		StringUtils.writeCSVLine(new String[] { "FROM", "TO", "PORT", "NOTES" }, pw);
		final Holder<Host> currentHost = new Holder<Host>();
		final Map<ConnInfo, ConnInfo> connInfos = new LinkedHashMap<ConnInfo, ConnInfo>();
		USATRegistry registry = new USATRegistry();
		BasicServerSet serverSetMain = registry.getServerSet(env);
		final Map<String, String> knownHosts = new HashMap<String, String>();
		for(Host host : serverSetMain.getHosts())
			knownHosts.put(host.getSimpleName(), host.getFullName());

		IPTablesUpdateTask iptTask = new IPTablesUpdateTask() {
			@Override
			public void allowTcpOut(String remoteHost, int port) throws IOException {
				for(String hn : findHostNames(remoteHost)) {
					mergeConnInfo(connInfos, currentHost.getValue().getFullName(), hn, port);
				}
			}

			@Override
			public void allowTcpOutWithPassive(String remoteHost, int port) throws IOException {
				for(String hn : findHostNames(remoteHost)) {
					mergeConnInfo(connInfos, currentHost.getValue().getFullName(), hn, port, "ALLOW PASSIVE");
				}
			}

			@Override
			public void allowTcpOutWithRelated(String remoteHost, int port, String[] relatedHosts, int relatedPort) throws IOException {
				for(String hn : findHostNames(remoteHost)) {
					mergeConnInfo(connInfos, currentHost.getValue().getFullName(), hn, port, "ALLOW RELATED ON PORT " + relatedPort + " TO " + StringUtils.join(relatedHosts, ", "));
				}
			}

			@Override
			public void allowTcpIn(String remoteHost, int port) throws IOException {
				for(String hn : findHostNames(remoteHost)) {
					mergeConnInfo(connInfos, hn, currentHost.getValue().getFullName(), port);
				}
			}

			protected Collection<String> findHostNames(String remoteHost) {
				String fqdn = knownHosts.get(remoteHost);
				if(fqdn != null)
					return Collections.singleton(fqdn);
				try {
					return getHostNames(normalizeIp(remoteHost));
				} catch(UnknownHostException e) {
					return Collections.singleton(remoteHost);
				}
			}
		};

		serverSetMain.registerApp(USATRegistry.APP_LAYER);
		serverSetMain.registerApp(USATRegistry.LOADER);
		serverSetMain.registerApp(USATRegistry.INAUTH_LAYER);
		serverSetMain.registerApp(USATRegistry.POSM_LAYER);
		serverSetMain.registerApp(USATRegistry.NET_LAYER);
		serverSetMain.registerApp(USATRegistry.OUTAUTH_LAYER);
		serverSetMain.registerApp(USATRegistry.KEYMGR);
		serverSetMain.registerApp(USATRegistry.DMS_APP);
		serverSetMain.registerApp(USATRegistry.ENERGYMISERS_WEBSITE_APP);
		serverSetMain.registerApp(USATRegistry.ESUDS_APP);
		serverSetMain.registerApp(USATRegistry.HOTCHOICE_APP);
		serverSetMain.registerApp(USATRegistry.HTTPD_NET);
		serverSetMain.registerSubApps(USATRegistry.HTTPD_NET, USATRegistry.DMS_APP);
		serverSetMain.registerApp(USATRegistry.HTTPD_WEB);
		serverSetMain.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.USALIVE_APP, USATRegistry.HOTCHOICE_APP, USATRegistry.VERIZON_APP, USATRegistry.PREPAID_APP, USATRegistry.ENERGYMISERS_WEBSITE_APP, USATRegistry.ESUDS_APP, USATRegistry.USATECH_WEBSITE_APP);
		serverSetMain.registerApp(USATRegistry.PREPAID_APP);
		serverSetMain.registerApp(USATRegistry.RPTGEN_LAYER);
		serverSetMain.registerApp(USATRegistry.RPTREQ_LAYER);
		serverSetMain.registerApp(USATRegistry.TRANSPORT_LAYER);
		serverSetMain.registerApp(USATRegistry.USALIVE_APP);
		serverSetMain.registerApp(USATRegistry.USATECH_WEBSITE_APP);
		serverSetMain.registerApp(USATRegistry.VERIZON_APP);

		serverSetMain.registerApp(USATRegistry.POSTGRES_MST);
		serverSetMain.registerApp(USATRegistry.POSTGRES_KLS);
		serverSetMain.registerApp(USATRegistry.POSTGRES_MSR);
		serverSetMain.registerApp(USATRegistry.POSTGRES_PGS);
		/*
				char seventh = (env.equalsIgnoreCase("USA") ? '3' : '1');
				int num = (env.equalsIgnoreCase("USA") ? 4 : 2);
				serverSetMain.registerServerSet(env, "MST", seventh, num, 4 / num);
				serverSetMain.registerServerSet(env, "NET", seventh, num, 4 / num);
				serverSetMain.registerServerSet(env, "APR", seventh, num, 4 / num);

				BasicServerSet serverSetDms = new BasicServerSet();
				serverSetDms.registerApp(USATRegistry.HTTPD_NET);
				serverSetDms.registerApp(USATRegistry.DMS_APP);
				serverSetDms.registerServerSet(env, "NET", seventh, num, 1);
				serverSetDms.registerServerSet(env, "APR", seventh, num, 1);
				serverSetDms.registerServerSet(env, "MST", seventh, num, 4 / num);
		*/
		int hostCount = 0;
		for(Host host : serverSetMain.getHosts()) {
			currentHost.setValue(host);
			UsatIPTablesUpdateTask.updateForUsat(iptTask, host, serverSetMain);
			hostCount++;
		}
		for(ConnInfo ci : connInfos.keySet())
			ci.writeToCsv(pw);
		pw.flush();
		System.out.println("Wrote connection info for " + hostCount + " hosts");
	}

	protected static void renewKeyInKeyStore() throws IOException, GeneralSecurityException {
		String keystorePath = "D:\\Development\\Java Projects\\LayersCommon\\conf\\apr\\keystore.ks";
		String alias = "mykey";
		Host host = new HostImpl("LOCAL", "localhost");
		KeyStore keyStore = SecurityUtils.getKeyStore(new FileInputStream(keystorePath), "JKS", null, null);
		StringBuilder csr = new StringBuilder();
		KeyPair pair = CryptUtils.generateCSR("RSA", 2048, "CN=" + host.getSimpleName() + ", O=USA Technologies\\, Inc, L=Malvern, S=PA, C=US", csr);
		String webUser = System.getProperty("user.name");
		Interaction interaction = new InOutInteraction();
		PasswordCache passwordCache = new MapPasswordCache();
		InputStream certStream = DeployUtils.signCertificate("usasubca.usatech.com", "USATWebServer", webUser, interaction, passwordCache, csr.toString());
		interaction.printf("Certificate created and signed. Swapping out the old with the new one");
		char[] password = "usatech".toCharArray(); // DeployUtils.getPassword(passwordCache, host, "keystore", "the keystore at '" + keystorePath + "'", interaction);
		CryptUtils.importCertificateChain(keyStore, alias, pair, certStream, password);
		interaction.printf("Certificate imported. Saving the keystore");
		keyStore.store(new FileOutputStream(keystorePath), password);
	}
}
