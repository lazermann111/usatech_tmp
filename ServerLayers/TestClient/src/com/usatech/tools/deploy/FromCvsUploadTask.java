package com.usatech.tools.deploy;

import java.io.InputStream;

import simple.app.Processor;
import simple.app.ServiceException;
import simple.io.IOUtils;
import simple.io.UnixEolInputStream;

public class FromCvsUploadTask extends UploadTask {
	protected static class UnixEolContentGenerator implements Processor<DeployTaskInfo, InputStream> {
		protected final Processor<DeployTaskInfo, InputStream> delegate;

		public UnixEolContentGenerator(Processor<DeployTaskInfo, InputStream> delegate) {
			this.delegate = delegate;
		}

		@Override
		public InputStream process(DeployTaskInfo argument) throws ServiceException {
			return new UnixEolInputStream(delegate.process(argument));
		}

		@Override
		public Class<InputStream> getReturnType() {
			return InputStream.class;
		}

		@Override
		public Class<DeployTaskInfo> getArgumentType() {
			return DeployTaskInfo.class;
		}
	}

	public FromCvsUploadTask(String sourcePath, String sourceRevision, String destFilePath, int permissions) {
		this(sourcePath, sourceRevision, destFilePath, permissions, null, null);
	}

	public FromCvsUploadTask(String sourcePath, String sourceRevision, String destFilePath, int permissions, String owner, String group) {
		this(sourcePath, sourceRevision, destFilePath, permissions, owner, group, false);
	}
	public FromCvsUploadTask(String sourcePath, String sourceRevision, String destFilePath, int permissions, String owner, String group, boolean unixEol) {
		this(new GitPullTask(sourcePath, sourceRevision), destFilePath, permissions, owner, group, true, unixEol);
	}

	public FromCvsUploadTask(String sourcePath, String sourceRevision, String destFilePath, int permissions, String owner, String group, boolean overwriteExisting, boolean unixEol) {
		this(new GitPullTask(sourcePath, sourceRevision), destFilePath, permissions, owner, group, overwriteExisting, unixEol);
	}

	public FromCvsUploadTask(String sourceRoot, String sourcePath, String sourceRevision, String destFilePath, int permissions, String owner, String group, boolean overwriteExisting, boolean unixEol) {
		this(new GitPullTask(sourceRoot, sourcePath, sourceRevision), destFilePath, permissions, owner, group, overwriteExisting, unixEol);
	}

	protected FromCvsUploadTask(DefaultPullTask vcsPullTask, String destFilePath, int permissions, String owner, String group, boolean overwriteExisting, boolean unixEol) {
		super(unixEol ? new UnixEolContentGenerator(vcsPullTask) : vcsPullTask, permissions, owner, group, IOUtils.getFileName(destFilePath), overwriteExisting, destFilePath);
	}
}
