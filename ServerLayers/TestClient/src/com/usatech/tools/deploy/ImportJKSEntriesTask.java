/**
 * 
 */
package com.usatech.tools.deploy;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.util.Enumeration;

import simple.app.Processor;
import simple.app.ServiceException;
import simple.io.HeapBufferStream;
import simple.security.SecurityUtils;
import simple.security.crypt.CryptUtils;


public class ImportJKSEntriesTask implements DeployTask {
	protected final Processor<DeployTaskInfo, InputStream> sourceKeystoreGenerator;
	protected final String sourceKeystoreDesc;
	protected final String sourceKeystoreKey;
	protected final String targetKeystorePath;
	protected final PasswordCache cache;
	protected final CryptUtils.CopyEntryFilter filter;
	protected final int targetKeystorePermissions;
	protected char[] sourceKeystorePassword;

	public ImportJKSEntriesTask(final File sourceKeystoreFile, char[] sourceKeystorePassword, String targetKeystorePath, PasswordCache cache, int targetKeystorePermissions, CryptUtils.CopyEntryFilter filter) {
		this(new Processor<DeployTaskInfo, InputStream>() {
			@Override
			public InputStream process(DeployTaskInfo deployTaskInfo) throws ServiceException {
				try {
					return new FileInputStream(sourceKeystoreFile);
				} catch(IOException e) {
					throw new ServiceException(e);
				}
			}

			@Override
			public Class<DeployTaskInfo> getArgumentType() {
				return DeployTaskInfo.class;
			}

			@Override
			public Class<InputStream> getReturnType() {
				return InputStream.class;
			}

			@Override
			public String toString() {
				return sourceKeystoreFile.getName();
			}
		}, sourceKeystorePassword, sourceKeystoreFile.getAbsolutePath(), targetKeystorePath, cache, targetKeystorePermissions, filter);
	}

	public ImportJKSEntriesTask(Processor<DeployTaskInfo, InputStream> sourceKeystoreGenerator, char[] sourceKeystorePassword, String sourceKeystoreKey, String targetKeystorePath, PasswordCache cache, int targetKeystorePermissions, CryptUtils.CopyEntryFilter filter) {
		this.sourceKeystoreGenerator = sourceKeystoreGenerator;
		this.targetKeystorePath = targetKeystorePath;
		this.cache = cache;
		this.targetKeystorePermissions = targetKeystorePermissions;
		this.filter = filter;
		this.sourceKeystorePassword = sourceKeystorePassword;
		this.sourceKeystoreKey = sourceKeystoreKey;
		StringBuilder sb = new StringBuilder().append("the ");
		if(targetKeystorePath.endsWith(".ks"))
			sb.append("keystore");
		else if(targetKeystorePath.endsWith(".ts"))
			sb.append("truststore");
		else
			sb.append("jks");
		sb.append(" at '").append(sourceKeystoreGenerator).append("'");
		this.sourceKeystoreDesc = sb.toString();
	}

	public ImportJKSEntriesTask(final String sourceKeystorePath, String targetKeystorePath, PasswordCache cache, int targetKeystorePermissions, CryptUtils.CopyEntryFilter filter) {
		this(new Processor<DeployTaskInfo, InputStream>() {
			@Override
			public InputStream process(DeployTaskInfo deployTaskInfo) throws ServiceException {
				try {
					HeapBufferStream bufferStream = new HeapBufferStream();
					InputStream in;
					if(UploadTask.checkFileUsingCmd(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, sourceKeystorePath)) {
						deployTaskInfo.interaction.printf("Retrieving contents of truststore at '%1$s'", sourceKeystorePath);
						UploadTask.readFileUsingTmp(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, sourceKeystorePath, bufferStream.getOutputStream(), null);
						in = bufferStream.getInputStream();
					} else {
						in = null;
					}
					return in;
				} catch(IOException e) {
					throw new ServiceException(e);
				} catch(InterruptedException e) {
					throw new ServiceException(e);
				}
			}

			@Override
			public Class<DeployTaskInfo> getArgumentType() {
				return DeployTaskInfo.class;
			}

			@Override
			public Class<InputStream> getReturnType() {
				return InputStream.class;
			}

			@Override
			public String toString() {
				return sourceKeystorePath;
			}
		}, null, sourceKeystorePath, targetKeystorePath, cache, targetKeystorePermissions, filter);
	}
	@Override
	public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
		//get the alias		
		String type;
		if(targetKeystorePath.endsWith(".ks"))
			type = "keystore";
		else if(targetKeystorePath.endsWith(".ts"))
			type = "truststore";
		else
			type = "jks";
		char[] targetKeystorePassword = DeployUtils.getPassword(cache, deployTaskInfo.host, targetKeystorePath, "the " + type + " at '" + targetKeystorePath + "'", deployTaskInfo.interaction);
		if(sourceKeystorePassword == null) {
			sourceKeystorePassword = DeployUtils.getPassword(cache, deployTaskInfo.host, sourceKeystoreKey, sourceKeystoreDesc, deployTaskInfo.interaction);
			if(sourceKeystorePassword.length == 0)
				sourceKeystorePassword = null;
		}
		//Get the truststore stream
		HeapBufferStream bufferStream = new HeapBufferStream();
		InputStream in;
		if(UploadTask.checkFileUsingCmd(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, targetKeystorePath)) {
			deployTaskInfo.interaction.printf("Retrieving contents of truststore at '%1$s'", targetKeystorePath);
			UploadTask.readFileUsingTmp(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, targetKeystorePath, bufferStream.getOutputStream(), null);
			in = bufferStream.getInputStream();
		} else {
			in = null;
		}
		//Decode with password
		try {
			KeyStore targetKeystore = SecurityUtils.getKeyStore(in, "JKS", null, targetKeystorePassword);
			KeyStore sourceKeystore = SecurityUtils.getKeyStore(sourceKeystoreGenerator.process(deployTaskInfo), "JKS", null, sourceKeystorePassword);
			int cnt = CryptUtils.copyKeystoreEntries(getAliases(sourceKeystore, targetKeystore), sourceKeystore, sourceKeystorePassword, targetKeystore, targetKeystorePassword, filter, isRemoveEntries());
			if(cnt > 0) {
				deployTaskInfo.interaction.printf("Copied  (" + cnt + ") entries into keystore '" + targetKeystorePath + "'");
				bufferStream.clear();
				targetKeystore.store(bufferStream.getOutputStream(), targetKeystorePassword);
				UploadTask.writeFileUsingTmp(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, bufferStream.getInputStream(), targetKeystorePermissions, "root", "usat", null, targetKeystorePath);
				return "Copied " + cnt + " entries";
			}
		} catch(KeyStoreException e) {
			throw new IOException(e);
		} catch(NoSuchProviderException e) {
			throw new IOException(e);
		} catch(NoSuchAlgorithmException e) {
			throw new IOException(e);
		} catch(CertificateException e) {
			throw new IOException(e);
		} catch(UnrecoverableEntryException e) {
			throw new IOException(e);
		} catch(ServiceException e) {
			if(e.getCause() instanceof IOException)
				throw (IOException) e.getCause();
			if(e.getCause() instanceof InterruptedException)
				throw (InterruptedException) e.getCause();
			throw new IOException(e);
		}
		return "Entries already exist";
	}

	protected Enumeration<String> getAliases(KeyStore sourceKeystore, KeyStore targetKeystore) throws KeyStoreException {
		return sourceKeystore.aliases();
	}

	protected boolean isRemoveEntries() {
		return false;
	}

	@Override
	public String getDescription(DeployTaskInfo deployTaskInfo) {
		StringBuilder sb = new StringBuilder();
		sb.append("Copying keystore entries from '").append(sourceKeystoreDesc).append("' into keystore ").append(targetKeystorePath);
		return sb.toString();
	}
}