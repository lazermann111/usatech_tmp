package com.usatech.tools.deploy;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MapPasswordCache implements PasswordCache {
	protected class CacheKey {
		public final String hostEnv;
		public final String hostType;
		public final String key;
		public CacheKey(String hostEnv, String hostType, String key) {
			this.hostEnv = hostEnv;
			this.hostType = hostType;
			this.key = key;
		}
		@Override
		public int hashCode() {
			return key.hashCode() + 31 * hostEnv.hashCode() + 31 * 31 * hostType.hashCode();
		}
		@Override
		public boolean equals(Object obj) {
			if(!(obj instanceof CacheKey))
				return false;
			CacheKey ck = (CacheKey)obj;
			return key.equals(ck.key) && hostEnv.equals(ck.hostEnv) && hostType.equals(ck.hostType);
		}
	}

	protected class WebCacheKey {
		public final String url;
		public final String user;

		public WebCacheKey(String url, String user) {
			this.url = url;
			this.user = user;
		}

		@Override
		public int hashCode() {
			return url.hashCode() + 31 * user.hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			if(!(obj instanceof WebCacheKey))
				return false;
			WebCacheKey ck = (WebCacheKey) obj;
			return url.equals(ck.url) && user.equals(ck.user);
		}
	}
	protected final Map<CacheKey, char[]> map = new HashMap<CacheKey, char[]>();
	protected final Map<WebCacheKey, char[]> webmap = new HashMap<WebCacheKey, char[]>();
	@Override
	public char[] getPassword(Host host, String key) {
		return map.get(new CacheKey(host.getServerEnv(), getServerType(host), key));
	}

	@Override
	public void setPassword(Host host, String key, char[] password) {
		map.put(new CacheKey(host.getServerEnv(), getServerType(host), key), password);
	}

	protected String getServerType(Host host) {
		Set<String> serverTypes = host.getServerTypes();
		if(serverTypes.isEmpty())
			return host.getSimpleName();
		return serverTypes.iterator().next();
	}
	@Override
	public char[] getWebPassword(String url, String user) {
		return webmap.get(new WebCacheKey(url, user));
	}

	@Override
	public void setWebPassword(String url, String user, char[] password) {
		webmap.put(new WebCacheKey(url, user), password);
	}
}
