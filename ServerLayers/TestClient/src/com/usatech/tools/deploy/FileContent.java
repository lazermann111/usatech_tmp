package com.usatech.tools.deploy;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import simple.app.Processor;
import simple.app.ServiceException;

public class FileContent implements Processor<DeployTaskInfo, InputStream> {
	protected final File file;

	public FileContent(File file) {
		this.file = file;
	}

	@Override
	public Class<DeployTaskInfo> getArgumentType() {
		return DeployTaskInfo.class;
	}

	@Override
	public Class<InputStream> getReturnType() {
		return InputStream.class;
	}

	@Override
	public InputStream process(DeployTaskInfo argument) throws ServiceException {
		try {
			return new FileInputStream(file);
		} catch(FileNotFoundException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public String toString() {
		return "file:" + file.getAbsolutePath();
	}
}

