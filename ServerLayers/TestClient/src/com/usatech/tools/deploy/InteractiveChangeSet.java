package com.usatech.tools.deploy;

import simple.io.Interaction;

public class InteractiveChangeSet {
	protected Interaction interaction;

	public Interaction getInteraction() {
		return interaction;
	}

	public void setInteraction(Interaction interaction) {
		this.interaction = interaction;
	}	
}
