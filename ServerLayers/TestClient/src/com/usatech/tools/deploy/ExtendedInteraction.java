package com.usatech.tools.deploy;

import java.io.File;
import java.util.concurrent.Future;

import simple.io.Interaction;

public interface ExtendedInteraction extends Interaction {
	public String readLineDefault(String fmt, String defaultValue, Object... args);

	public int[] readChoice(String fmt, String[] options, boolean multiple, int[] defaultChoices, Object... args);

	public File readFile(String caption, String baseDir, String fmt, Object... args);

	public Future<String> getLineFutureDefault(String fmt, String defaultValue, Object... args);

	public Future<File> getFileFuture(String caption, String baseDir, String fmt, Object... args);

	public Future<int[]> getChoiceFuture(String fmt, String[] options, boolean multiple, int[] defaultChoices, Object... args);

}
