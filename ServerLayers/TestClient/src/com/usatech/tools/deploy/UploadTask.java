/**
 * 
 */
package com.usatech.tools.deploy;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import simple.app.Processor;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.event.ProgressEvent;
import simple.event.ProgressListener;
import simple.io.IOUtils;
import simple.io.Interaction;
import simple.text.StringUtils;

import com.sshtools.j2ssh.FileTransferProgress;
import com.sshtools.j2ssh.SftpClient;
import com.sshtools.j2ssh.sftp.FileAttributes;

public class UploadTask implements DeployTask {
	//private static final Log log = Log.getLog();
	protected final InputStream content;
	protected final Processor<DeployTaskInfo, InputStream> contentGenerator;
	protected final int permissions;
	protected final String owner;
	protected final String group;
	protected final Set<String> remoteFilePaths = new LinkedHashSet<String>();
	protected final String contentPath;
	protected final boolean overwriteExisting;
	protected boolean privileged = true;
	
	public UploadTask(File file, int permissions) throws IOException {
		this(file, permissions, null, null);
	}

	public UploadTask(File file, int permissions, String owner, String group) throws IOException {
		this(new FileInputStream(file), permissions, owner, group, file.getCanonicalPath(), true, file.getName());
	}

	public UploadTask(File file, String remoteDir, int permissions, String owner, String group) throws IOException {
		this(new FileInputStream(file), permissions, owner, group, file.getCanonicalPath(), true, toUnixDirectory(remoteDir) + file.getName());
	}

	public UploadTask(File file, String remoteDir, int permissions, String owner, String group, boolean overwriteExisting) throws IOException {
		this(new FileInputStream(file), permissions, owner, group, file.getCanonicalPath(), overwriteExisting, toUnixDirectory(remoteDir) + file.getName());
	}

	public UploadTask(final URL url, String remoteDir, int permissions, String owner, String group, boolean overwriteExisting) {
		this(new Processor<DeployTaskInfo, InputStream>() {
			@Override
			public InputStream process(DeployTaskInfo deployTaskInfo) throws ServiceException {
				try {
					return url.openStream();
				} catch(IOException e) {
					throw new ServiceException(e);
				}
			}

			@Override
			public Class<DeployTaskInfo> getArgumentType() {
				return DeployTaskInfo.class;
			}

			@Override
			public Class<InputStream> getReturnType() {
				return InputStream.class;
			}
		}, permissions, owner, group, url.toString(), overwriteExisting, toUnixDirectory(remoteDir) + StringUtils.substringAfterLast(url.getPath(), "/"));
	}

	public UploadTask(DefaultPullTask vcsPullTask, int permissions, String owner, String group) {
		this(vcsPullTask, permissions, owner, group, null);

	}

	public UploadTask(DefaultPullTask vcsPullTask, int permissions, String owner, String group, String directory) {
		this(vcsPullTask, permissions, owner, group, directory, true);
	}

	public UploadTask(DefaultPullTask vcsPullTask, int permissions, String owner, String group, String directory, boolean overwriteExisting) {
		this(vcsPullTask, permissions, owner, group, IOUtils.getFileName(vcsPullTask.getFilePath()), directory, overwriteExisting);
	}

	protected UploadTask(DefaultPullTask vcsPullTask, int permissions, String owner, String group, String fileName, String directory, boolean overwriteExisting) {
		this(vcsPullTask, permissions, owner, group, fileName, overwriteExisting, IOUtils.getFullPath(directory, fileName));
	}

	public UploadTask(InputStream content, int permissions, String owner, String group, String contentPath, boolean overwriteExisting, String... remoteFilePaths) {
		this.content = content;
		this.contentGenerator = null;
		this.permissions = permissions;
		this.contentPath = contentPath;
		this.owner = owner;
		this.group = group;
		this.overwriteExisting = overwriteExisting;
		addRemoteFilePaths(remoteFilePaths);
	}

	public UploadTask(Processor<DeployTaskInfo, InputStream> contentGenerator, int permissions, String owner, String group, String contentPath, boolean overwriteExisting, String... remoteFilePaths) {
		this.content = null;
		this.contentGenerator = contentGenerator;
		this.permissions = permissions;
		this.contentPath = contentPath;
		this.owner = owner;
		this.group = group;
		this.overwriteExisting = overwriteExisting;
		addRemoteFilePaths(remoteFilePaths);
	}
	@Override
	public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
		return perform(deployTaskInfo, deployTaskInfo.host, deployTaskInfo.user);
	}

	public String perform(DeployTaskInfo deployTaskInfo, Host host, String user) throws IOException, InterruptedException {
		String[] filePaths;
		if(!overwriteExisting) {
			List<String> notExisting = null;
			for(String remoteFilePath : remoteFilePaths) {
				boolean exists;
				if(privileged)
					exists = checkFileUsingCmd(host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, user, remoteFilePath);
				else
					exists = checkFile(host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, user, remoteFilePath);
				if(exists) {
					deployTaskInfo.interaction.printf("File '%1$s' already exists. Not overwriting", remoteFilePath);
				} else {
					if(notExisting == null)
						notExisting = new ArrayList<String>();
					notExisting.add(remoteFilePath);
				}
			}
			if(notExisting == null || notExisting.isEmpty())
				return "Files already exist";
			filePaths = notExisting.toArray(new String[notExisting.size()]);
		} else
			filePaths = remoteFilePaths.toArray(new String[remoteFilePaths.size()]);
		FileTransferProgress progress = makeFileTransferProgress(deployTaskInfo.interaction, true, contentPath, filePaths);
		InputStream in;
		if(content != null)
			in = content;
		else
			try {
				in = contentGenerator.process(deployTaskInfo);
			} catch(Exception e) {
				throw new IOException("Could not generate content for '" + contentPath + "'", e);
			}
		in = decorate(in);
		if(privileged)
			writeFileUsingTmp(host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, user, in, permissions, owner, group, progress, filePaths);
		else
			writeFile(new DeployTaskInfo(deployTaskInfo.interaction, host, user, deployTaskInfo.passwordCache, deployTaskInfo.sshClientCache), in, permissions, owner, group, progress, filePaths);
		return "Uploaded " + filePaths.length + " files";
	}	

	// Hook for sub-classes
	protected InputStream decorate(InputStream in) {
		return in;
	}

	public static void writeFile(DeployTaskInfo taskInfo, InputStream content, Integer permissions, String owner, String group, FileTransferProgress progress, String... filePaths) throws IOException, InterruptedException {
		for(String filePath : filePaths) {
			try {
				taskInfo.getSftp().put(content, filePath, progress);
			} catch(IOException e) {
				if(e.getMessage().equals("Permission denied")) {
					taskInfo.interaction.printf("Permission denied on writing file '%1$s'; writing to tmp location and trying again", filePath);
					writeFileUsingTmp(taskInfo, content, permissions, owner, group, progress, filePaths);
					return;
				}
				throw new IOException("Could not write to " + filePaths + " because: " + e.getMessage(), e);
			}
			if(permissions != null)
				try {
					taskInfo.getSftp().chmod(permissions, filePath);
				} catch(IOException e) {
					if(e.getMessage().equals("Permission denied")) {
						taskInfo.interaction.printf("Permission denied while changing permissions on file '%1$s'; changing permissions with sudo", filePath);
						ExecuteTask.executeCommands(taskInfo, "sudo chmod " + Integer.toOctalString(permissions) + " \"" + filePath + '"');
					} else {
						throw new IOException("Could not update permissions on " + filePaths + " because: " + e.getMessage(), e);
					}
				}
			if(owner != null || group != null) {
				String cmd;
				if(owner != null) {
					cmd = "sudo chown " + owner + (group != null ? ':' + group : "") + " \"" + filePath + '"';
				} else {
					cmd = "sudo chgrp " + group + " \"" + filePath + '"';
				}
				ExecuteTask.executeCommands(taskInfo, cmd);
			}
		}
	}

	/**
	 * 
	 */
	public static void writeFileUsingTmp(Host host, PasswordCache passwordCache, Interaction interaction, SshClientCache sshClientCache, String sshUser, InputStream content, Integer permissions, String owner, String group, FileTransferProgress progress, String... filePaths) throws IOException, InterruptedException {
		writeFileUsingTmp(new DeployTaskInfo(interaction, host, sshUser, passwordCache, sshClientCache), content, permissions, owner, group,
				progress, filePaths);
	}

	public static void writeFileUsingTmp(DeployTaskInfo taskInfo, InputStream content, Integer permissions, String owner, String group, FileTransferProgress progress, String... filePaths) throws IOException, InterruptedException {
		String tmpFileName = getTmpFilePath(taskInfo.user);
		taskInfo.getSftp().put(content, tmpFileName, progress);
		List<String> commands = new ArrayList<String>();
		commands.add("sudo su");
		if(permissions != null)
			commands.add("chmod " + Integer.toOctalString(permissions) + " " + tmpFileName);
		if(owner != null || group != null) {
			if(owner != null) {
				commands.add("chown " + owner + (group != null ? ':' + group : "") + " " + tmpFileName);
			} else {
				commands.add("chgrp " + group + " " + tmpFileName);
			}
		}
		for(String filePath : filePaths)
			commands.add("/bin/cp -p " + tmpFileName + " \"" + filePath + '"');
		commands.add("/bin/rm " + tmpFileName);
		ExecuteTask.executeCommands(taskInfo, commands);
	}

	public static boolean readFile(Host host, PasswordCache passwordCache, Interaction interaction, SshClientCache sshClientCache, String sshUser, String filePath, OutputStream content, FileTransferProgress progress) throws IOException, InterruptedException {
		SftpClient sftp = sshClientCache.getSftpClient(host, sshUser);
		try {
			sftp.get(filePath, content, progress);
		} catch(IOException e) {
			if(e.getMessage().equals("Permission denied")) {
				interaction.printf("Permission denied on writing file '%1$s'; writing to tmp location and trying again", filePath);
				String tmpFileName = getTmpFilePath(sshUser);
				ExecuteTask.executeCommands(host, passwordCache, interaction, sshClientCache.getSshClient(host, sshUser), "sudo su", "/bin/cp -p \"" + filePath + "\" " + tmpFileName, "chown `logname` " + tmpFileName, "chmod 0600 " + tmpFileName);
				sftp.get(tmpFileName, content, progress);
				sftp.rm(tmpFileName);
			} else if(e.getMessage().equals("No such file")) {
				return false;
			} else {
				throw new IOException("Could not read from " + filePath + " because: " + e.getMessage(), e);
			}
		}
		return true;
	}

	public static boolean readFileUsingTmp(Host host, PasswordCache passwordCache, Interaction interaction, SshClientCache sshClientCache, String sshUser, String filePath, OutputStream content, FileTransferProgress progress) throws IOException, InterruptedException {
		String tmpFileName = getTmpFilePath(sshUser);
		ExecuteTask.executeCommands(host, passwordCache, interaction, sshClientCache.getSshClient(host, sshUser), "sudo su", "/bin/cp -p \"" + filePath + "\" " + tmpFileName, "chown `logname` " + tmpFileName, "chmod 0600 " + tmpFileName);
		SftpClient sftp = sshClientCache.getSftpClient(host, sshUser);
		try {
			sftp.get(tmpFileName, content, progress);
		} catch(IOException e) {
			if(e.getMessage().equals("No such file")) {
				return false;
			}
			throw new IOException("Could not read from " + filePath + " because: " + e.getMessage(), e);
		}
		sftp.rm(tmpFileName);
		return true;
	}

	public static InputStream readFileUsingTmp(Host host, PasswordCache passwordCache, Interaction interaction, SshClientCache sshClientCache, String sshUser, String filePath) throws IOException, InterruptedException {
		final String tmpFileName = getTmpFilePath(sshUser);
		ExecuteTask.executeCommands(host, passwordCache, interaction, sshClientCache.getSshClient(host, sshUser), "sudo su", "/bin/cp -p \"" + filePath + "\" " + tmpFileName, "chown `logname` " + tmpFileName, "chmod 0600 " + tmpFileName);
		final SftpClient sftp = sshClientCache.getSftpClient(host, sshUser);
		try {
			return new FilterInputStream(sftp.getContents(tmpFileName)) {
				@Override
				public void close() throws IOException {
					try {
						super.close();
					} finally {
						sftp.rm(tmpFileName);
					}
				}
			};
		} catch(IOException e) {
			if(e.getMessage().equals("No such file")) {
				return null;
			}
			throw new IOException("Could not read from " + filePath + " because: " + e.getMessage(), e);
		}
	}

	public static boolean checkFile(Host host, PasswordCache passwordCache, Interaction interaction, SshClientCache sshClientCache, String sshUser, String filePath) throws IOException, InterruptedException {
		SftpClient sftp = sshClientCache.getSftpClient(host, sshUser);
		try {
			FileAttributes attr = sftp.stat(filePath);
			return attr.getSize().longValue() > 0;
		} catch(IOException e) {
			if(e.getMessage().equals("Permission denied")) {
				interaction.printf("Permission denied on checking file '%1$s'; writing to tmp location and trying again", filePath);
				String tmpFileName = getTmpFilePath(sshUser);
				ExecuteTask.executeCommands(host, passwordCache, interaction, sshClientCache.getSshClient(host, sshUser), "sudo su", "/bin/cp -p \"" + filePath + "\" " + tmpFileName, "chown `logname` " + tmpFileName, "chmod 0600 " + tmpFileName);
				FileAttributes attr = sftp.stat(filePath);
				sftp.rm(tmpFileName);
				return attr.getSize().longValue() > 0;
			} else if(e.getMessage().equals("No such file")) {
				return false;
			} else {
				throw new IOException("Could not read from " + filePath + " because: " + e.getMessage(), e);
			}
		}
	}

	public static boolean checkFileUsingCmd(Host host, PasswordCache passwordCache, Interaction interaction, SshClientCache sshClientCache, String sshUser, String filePath) throws IOException, InterruptedException {
		String[] results = ExecuteTask.executeCommands(host, passwordCache, interaction, sshClientCache.getSshClient(host, sshUser), "sudo su", "if [ -s \"" + filePath + "\" ]; then  ls -s \"" + filePath + "\" | sed 's/^ *\\([0-9][0-9]*\\) .*/\\1/g'; else echo '0'; fi");
		int size;
		try {
			size = ConvertUtils.getInt(results[1].trim().split("[\n\r]+", 2)[0]);
		} catch(ConvertException e) {
			throw new IOException("Could not get size of " + filePath + " because: " + e.getMessage(), e);
		}
		return size > 0;
	}

	protected static String toUnixDirectory(String dir) {
		if(dir == null || dir.trim().length() == 0)
			return "";
		dir = dir.replace('\\', '/');
		if(!dir.endsWith("/"))
			return dir + "/";
		return dir;
	}

	public static FileTransferProgress makeFileTransferProgress(Interaction interaction, boolean upload, String srcFilePath, String... targetFilePaths) {
		StringBuilder sb = new StringBuilder();
		if(upload)
			sb.append("Uploading file: ");
		else
			sb.append("Downloading file: ");
		sb.append(srcFilePath);
		if(targetFilePaths != null && targetFilePaths.length > 0) {
			sb.append(" to ");
			for(int i = 0; i < targetFilePaths.length; i++) {
				if(i > 0)
					sb.append(" and ");
				sb.append(targetFilePaths[i]);
			}
			sb.append('.');
		}
		final ProgressListener listener = interaction.createProgressMeter();
		final ProgressEvent event = new ProgressEvent(0, null, sb.toString());
		FileTransferProgress progress = new FileTransferProgress() {
			public void completed() {
				listener.progressStart(event);
			}

			public boolean isCancelled() {
				return false;
			}

			public void progressed(long bytesSoFar) {
				event.setStatusProgress((int) bytesSoFar);
				listener.progressUpdate(event);
			}

			public void started(long bytesTotal, String remoteFile) {
				listener.progressStart(event);
			}
		};
		return progress;
	}
	/** 
	 * All tmp files paths returned are seeded from one initially random value per run 
	 */
	private static double uniqueFileIndex = Math.random();
	
	public static String getTmpFilePath(String sshUser) {
		return "/home/" + sshUser + '/' + StringUtils.toHex(System.currentTimeMillis()) + '-' + StringUtils.toHex(Double.doubleToLongBits(++uniqueFileIndex)) + ".wrk";
	}

	public static void readFileUsingTmp(DeployTaskInfo taskInfo,
			String filePath, OutputStream content, FileTransferProgress progress) throws IOException, InterruptedException {
		UploadTask.readFile(taskInfo.host, taskInfo.passwordCache, taskInfo.interaction, taskInfo.sshClientCache, taskInfo.user, filePath, content, progress);
	}

	@Override
	public String getDescription(DeployTaskInfo deployTaskInfo) {
		StringBuilder sb = new StringBuilder();
		sb.append("Uploading file '").append(contentPath).append("' to ");
		boolean first = true;
		for(String path : remoteFilePaths) {
			if(first)
				first = false;
			else
				sb.append(" and ");
			sb.append(path);
		}
		return sb.toString();
	}

	public String[] getRemoteFilePaths() {
		return remoteFilePaths.toArray(new String[remoteFilePaths.size()]);
	}

	public void setRemoteFilePaths(String... remoteFilePaths) {
		this.remoteFilePaths.clear();
		addRemoteFilePaths(remoteFilePaths);
	}

	public void addRemoteFilePaths(String... remoteFilePaths) {
		if(remoteFilePaths != null)
			for(String path : remoteFilePaths)
				this.remoteFilePaths.add(path);
	}

	public void removeRemoteFilePaths(String... remoteFilePaths) {
		if(remoteFilePaths != null)
			for(String path : remoteFilePaths)
				this.remoteFilePaths.remove(path);
	}
}