package com.usatech.tools.deploy;

import java.util.Iterator;
import java.util.List;

import simple.io.Interaction;
import simple.text.StringUtils;
import simple.util.CollectionUtils;

public class AppSettingUpdateFileTask extends PropertiesUpdateFileTask {
	protected static final int POSM_ADMIN_PORT_OFFSET = 90;

	public AppSettingUpdateFileTask(final App app, final Integer ordinal, final int instanceNum, final int instanceCount, final PasswordCache cache, final ServerSet serverSet) {
		this(app, ordinal, instanceNum, instanceCount, cache, serverSet, true);
	}

	public AppSettingUpdateFileTask(final App app, final Integer ordinal, final int instanceNum, final int instanceCount, final PasswordCache cache, final ServerSet serverSet, final boolean unsplit) {
		super(false, 0640, app.getUserName(), app.getUserName());
		final int appIndex;
		appIndex = app.getPortOffset();
		String specificFolderPath = "/opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal) + "/specific";
		setFilePath(specificFolderPath+"/USAT_app_settings.properties");
		
		registerValue("javax.net.ssl.serverKeyAlias", new HostSpecificValue() {
			public String getValue(Host host, Interaction interaction) {
				return host.getSimpleName();
			}
			public boolean isOverriding() {
				return true;
			}
		});
		registerValue("javax.net.ssl.keyStore", new DefaultHostSpecificValue("../conf/keystore.ks"));
		registerValue("javax.net.ssl.keyStorePassword", new GetPasswordHostSpecificValue(cache, "keystore", "Key Store"));
		registerValue("javax.net.ssl.trustStore", new DefaultHostSpecificValue("../conf/truststore.ts"));
		registerValue("javax.net.ssl.trustStorePassword", new GetPasswordHostSpecificValue(cache, "truststore", "Trust Store"));
		
		int basePort = 7000 + 100 * (ordinal == null ? 7 : ordinal);
		registerValue("jmx.remote.x.rmiRegistryPort", new DefaultHostSpecificValue(String.valueOf(basePort + appIndex + 1)));
		registerValue("jmx.remote.x.rmiServerPort", new DefaultHostSpecificValue(String.valueOf(basePort + appIndex + 2)));
		registerValue("manager-port", new DefaultHostSpecificValue(String.valueOf(basePort + appIndex)));

		registerValue("jmx.remote.x.login.config", new DefaultHostSpecificValue("JMXControl"));
		registerValue("jmx.remote.x.access.file", new DefaultHostSpecificValue("../conf/jmx.access"));
		
		registerValue("control-address-regex", new DefaultHostSpecificValue("^10\\.0\\.0\\.75|10\\.0\\.0\\.31|192\\.168\\.69\\.41|10\\.20\\.0\\.41$"));
		AliasMapHostSpecificValue clientKeyAliasMap = new AliasMapHostSpecificValue();
		registerValue("javax.net.ssl.clientKeyAliasMap", clientKeyAliasMap);
		clientKeyAliasMap.registerAlias(636, "-");
		clientKeyAliasMap.registerAlias(5432, "-");
		clientKeyAliasMap.registerAlias(15432, "-");
		clientKeyAliasMap.registerAlias(5431, "-", "INT");
		clientKeyAliasMap.registerAlias(5431, "-", "ECC");
		if("applayer".equalsIgnoreCase(app.getName())) {
			registerValue("USAT.database.MQ.password.APP_LAYER", new ServerEnvHostSpecificValue("APP_LAYER_1", "APP_LAYER_1", new GetPasswordHostSpecificValue(cache, "database.MQ.APP_LAYER", "APP_LAYER for MQ"), new GetPasswordHostSpecificValue(cache, "database.MQ.APP_LAYER", "APP_LAYER for MQ")));
			registerValue("USAT.database.FILE_REPO.password.APP_LAYER", new ServerEnvHostSpecificValue("APP_LAYER_1","APP_LAYER_1", new GetPasswordHostSpecificValue(cache, "database.FILE_REPO.APP_LAYER", "APP_LAYER for FILE_REPO"), new GetPasswordHostSpecificValue(cache, "database.FILE_REPO.APP_LAYER", "APP_LAYER for FILE_REPO")));
			int i = 0;
			Server server = null;
			Iterator<Server> iter = serverSet.getServers("MST").iterator();
			for(; i < instanceNum;) {
				server = iter.next();
				i += server.instances.length;
			}
			int port;
			if(server.instances.length == 1)
				port = 5432;
			else
				port = 5432 + instanceNum - i;
			StringBuilder sb = new StringBuilder();
			sb.append("jdbc:postgresql://").append(server.host.getSimpleName()).append(':').append(port).append("/app_").append(instanceNum).append("?ssl=true&connectTimeout=5&tcpKeepAlive=true");
			registerValue("USAT.database.APP.url", new DefaultHostSpecificValue(sb.toString()));
			registerValue("USAT.database.APP.password.APP_LAYER", new ServerEnvHostSpecificValue("APP_LAYER_1", "APP_LAYER_1", new GetPasswordHostSpecificValue(cache, "database.MQ.APP_LAYER", "APP_LAYER for MQ"), new GetPasswordHostSpecificValue(cache, "database.MQ.APP_LAYER", "APP_LAYER for MQ")));
			registerValue("USAT.database.OPER.password.APP_LAYER", new ServerEnvHostSpecificValue("APP_LAYER_1","APP_LAYER_1", new GetPasswordHostSpecificValue(cache, "database.OPER.APP_LAYER", "APP_LAYER for OPER"), new GetPasswordHostSpecificValue(cache, "database.OPER.APP_LAYER", "APP_LAYER for OPER")));
			// This is the value when a new Kiosk device is initialized, it will send email to this address
			registerValue("USAT.initEmailTo", new ServerEnvHostSpecificValue("SoftwareDevelopmentTeam@usatech.com", "SoftwareDevelopmentTeam@usatech.com", "QualityAssurance@usatech.com", "KioskAlerts@usatech.com"));
			registerValue("USAT.applayer.inactivityThreshhold", new ServerEnvHostSpecificValue("600000000", "600000000", "600000","600000"));
			registerValue("STATIC.com.usatech.applayer.AppLayerUtils.validDeviceTimeDays", new DefaultHostSpecificValue("365"));
			registerValue("STATIC.com.usatech.applayer.LegacyUtils.validDeviceTimeDays", new DefaultHostSpecificValue("365"));
			if(ordinal != null) 
				registerValue("STATIC.java.lang.System.property(derby.drda.portNumber)", new DefaultHostSpecificValue(String.valueOf(15027 + 100 * ordinal)));
			//@Todo use a common email distribution
			registerValue("USAT.binRangeApproveEmailTo", new ServerEnvHostSpecificValue("SoftwareDevelopmentTeam@usatech.com", "SoftwareDevelopmentTeam@usatech.com", "SoftwareDevelopmentTeam@usatech.com", "ApplicationSupportTeam@usatech.com"));
			registerValue("USAT.environment.suffix", new ServerEnvHostSpecificValue("-DEV", "-INT", "-ECC", ""));
			// Set list of keyreceivers other than the same instance
			sb.setLength(0);
			i = 1;
			for(Server netserver : serverSet.getServers("NET")) {
				for(int k = 0; k < netserver.instances.length; k++) {
					if(i != instanceNum)
						sb.append(",netlayer-").append(i).append(".keyreceiver");
					i++;
				}
			}
			registerValue("USAT.netlayer.keyreceiver.others", new DefaultHostSpecificValue(sb.substring(1)));
		} else if("loader".equalsIgnoreCase(app.getName())) {
			registerValue("USAT.database.MSR_MQ.password.LOADER", new ServerEnvHostSpecificValue("LOADER_1", "LOADER_1", new GetPasswordHostSpecificValue(cache, "database.MSR_MQ.LOADER", "LOADER for MSR_MQ"), new GetPasswordHostSpecificValue(cache, "database.MSR_MQ.LOADER", "LOADER for MSR_MQ")));
			registerValue("USAT.database.MQ.password.LOADER", new ServerEnvHostSpecificValue("LOADER_1", "LOADER_1", new GetPasswordHostSpecificValue(cache, "database.MQ.LOADER", "LOADER for MQ"), new GetPasswordHostSpecificValue(cache, "database.MQ.LOADER", "LOADER for MQ")));
			registerValue("USAT.database.FILE_REPO.password.LOADER", new ServerEnvHostSpecificValue("LOADER_1", "LOADER_1", new GetPasswordHostSpecificValue(cache, "database.FILE_REPO.LOADER", "LOADER for FILE_REPO"), new GetPasswordHostSpecificValue(cache, "database.FILE_REPO.LOADER", "LOADER for FILE_REPO")));
			registerValue("USAT.database.OPER.password.LOADER", new ServerEnvHostSpecificValue("LOADER_1", "LOADER_1", new GetPasswordHostSpecificValue(cache, "database.OPER.LOADER", "LOADER for OPER"), new GetPasswordHostSpecificValue(cache, "database.OPER.LOADER", "LOADER for OPER")));
			registerValue("USAT.database.REPORT.password.LOADER", new ServerEnvHostSpecificValue("LOADER_1", "LOADER_1", new GetPasswordHostSpecificValue(cache, "database.REPORT.LOADER", "LOADER for REPORT"), new GetPasswordHostSpecificValue(cache, "database.REPORT.LOADER", "LOADER for REPORT")));
			registerValue("USAT.database.MAIN.password.LOADER", new ServerEnvHostSpecificValue("LOADER_1", "LOADER_1", new GetPasswordHostSpecificValue(cache, "database.MAIN.LOADER", "LOADER for MAIN"), new GetPasswordHostSpecificValue(cache, "database.MAIN.LOADER", "LOADER for MAIN")));
			registerValue("USAT.applayer.inactivityThreshhold", new ServerEnvHostSpecificValue("600000000", "600000000", "600000","600000"));
			registerValue("STATIC.com.usatech.applayer.AppLayerUtils.validDeviceTimeDays", new DefaultHostSpecificValue("365"));
			registerValue("STATIC.com.usatech.applayer.LegacyUtils.validDeviceTimeDays", new DefaultHostSpecificValue("365"));
			
			String vzm2pLoyaltyQueueKey = "usat.authority.vzm2p.loyalty";
			registerValue("STATIC.com.usatech.applayer.TranImportTask.vzm2pLoyaltyQueueKey", new ServerEnvHostSpecificValue(vzm2pLoyaltyQueueKey, vzm2pLoyaltyQueueKey, vzm2pLoyaltyQueueKey, (String) null));
			
			registerValue("STATIC.com.usatech.applayer.TranReportTask.rdwLoadEnabled", new ServerEnvHostSpecificValue("false", "false", "false", "false"));
		} else if("inauthlayer".equalsIgnoreCase(app.getName())) {
			registerValue("USAT.database.MQ.password.AUTH_LAYER", new ServerEnvHostSpecificValue("AUTH_LAYER_1","AUTH_LAYER_1", new GetPasswordHostSpecificValue(cache, "database.MQ.AUTH_LAYER", "AUTH_LAYER for MQ"), new GetPasswordHostSpecificValue(cache, "database.MQ.AUTH_LAYER", "AUTH_LAYER for MQ")));
			registerValue("USAT.database.FILE_REPO.password.AUTH_LAYER", new ServerEnvHostSpecificValue("AUTH_LAYER_1","AUTH_LAYER_1", new GetPasswordHostSpecificValue(cache, "database.FILE_REPO.AUTH_LAYER", "AUTH_LAYER for FILE_REPO"), new GetPasswordHostSpecificValue(cache, "database.FILE_REPO.AUTH_LAYER", "AUTH_LAYER for FILE_REPO")));
			registerValue("USAT.authoritylayer.interchange.hsqltxnmanager.url", new DefaultHostSpecificValue("jdbc:hsqldb:file:conf/hsql/iso8583;user=sa;password="));
			registerValue("USAT.authoritylayer.interchange.fhms.url", new ServerEnvHostSpecificValue("206.253.184.212:7085","206.253.184.212:7085","206.253.184.212:7085","206.253.184.210:7085"));
			registerValue("USAT.authoritylayer.interchange.fhms.url2", new ServerEnvHostSpecificValue("206.253.184.211:7085","206.253.184.211:7085","206.253.184.211:7085","206.253.180.210:7085"));
			registerValue("USAT.authoritylayer.interchange.fhms.simulationMode", new ServerEnvHostSpecificValue("true","true","false","false"));
			registerValue("USAT.authoritylayer.interchange.elavon.url", new ServerEnvHostSpecificValue("198.203.191.112:8100","198.203.191.112:8100","198.203.191.112:8100", "198.203.192.217:8100")); //nettrans1.novainfo.net and nettrans2.novainfo.net
			registerValue("USAT.authoritylayer.interchange.elavon.url2", new ServerEnvHostSpecificValue("198.203.191.84:8100", "", "","198.203.191.104:8100"));
			registerValue("USAT.database.OPER.password.IN_AUTH_LAYER", new ServerEnvHostSpecificValue("IN_AUTH_LAYER_1","IN_AUTH_LAYER_1", new GetPasswordHostSpecificValue(cache, "database.OPER.IN_AUTH_LAYER", "IN_AUTH_LAYER for OPER"), new GetPasswordHostSpecificValue(cache, "database.OPER.IN_AUTH_LAYER", "IN_AUTH_LAYER for OPER")));
			registerValue("USAT.authlayer.inactivityThreshhold", new ServerEnvHostSpecificValue("1200000000", "1200000000", "1200000", "1200000"));
			registerValue("com.usatech.authoritylayer.PrepaidAuthorityTask.registrationUrl", new ServerEnvHostSpecificValue("getmore-dev.usatech.com", "getmore-int.usatech.com", "getmore-ecc.usatech.com", "getmore.usatech.com"));
			registerValue("USAT.authoritylayer.tandem.urls", new ServerEnvHostSpecificValue("206.253.180.250:12000", "206.253.180.250:12000", "206.253.180.250:12000", "206.253.180.20:15110,206.253.184.20:15110"));
			registerValue("USAT.authoritylayer.tns.urls", new ServerEnvHostSpecificValue("10.1.215.70:8502", "10.1.215.70:8502", "10.1.215.70:8502", "10.30.215.65:5010,10.40.215.65:5010"));
			registerValue("com.usatech.authoritylayer.TandemGatewayTask.authPartiallyReversed.whenLess", new DefaultHostSpecificValue("true"));
			/*registerValue("com.usatech.authoritylayer.TandemGatewayTask.authPartiallyReversed.whenLessByMerchant(000000311007)", new DefaultHostSpecificValue("true"));
			registerValue("com.usatech.authoritylayer.TandemGatewayTask.authPartiallyReversed.whenLessByMerchant(000390048102)", new DefaultHostSpecificValue("true"));
			registerValue("com.usatech.authoritylayer.TandemGatewayTask.authPartiallyReversed.whenLessByMerchant(920000000000)", new DefaultHostSpecificValue("true"));
			registerValue("com.usatech.authoritylayer.TandemGatewayTask.authPartiallyReversed.whenLessByMerchant(612000000000)", new DefaultHostSpecificValue("true"));
			registerValue("com.usatech.authoritylayer.TandemGatewayTask.authPartiallyReversed.whenLessByMerchant(641000000000)", new DefaultHostSpecificValue("true"));
			registerValue("com.usatech.authoritylayer.TandemGatewayTask.authPartiallyReversed.whenLessByMerchant(645200000000)", new DefaultHostSpecificValue("true"));
			registerValue("com.usatech.authoritylayer.TandemGatewayTask.authPartiallyReversed.whenLessByMerchant(677500000000)", new DefaultHostSpecificValue("true"));
			registerValue("com.usatech.authoritylayer.TandemGatewayTask.authPartiallyReversed.whenLessByMerchant(032258065000)", new DefaultHostSpecificValue("true"));*/
		} else if("outauthlayer".equalsIgnoreCase(app.getName())) {
			registerValue("USAT.database.MQ.password.AUTH_LAYER", new ServerEnvHostSpecificValue("AUTH_LAYER_1","AUTH_LAYER_1", new GetPasswordHostSpecificValue(cache, "database.MQ.AUTH_LAYER", "AUTH_LAYER for MQ"), new GetPasswordHostSpecificValue(cache, "database.MQ.AUTH_LAYER", "AUTH_LAYER for MQ")));
			registerValue("USAT.database.FILE_REPO.password.AUTH_LAYER", new ServerEnvHostSpecificValue("AUTH_LAYER_1","AUTH_LAYER_1", new GetPasswordHostSpecificValue(cache, "database.FILE_REPO.AUTH_LAYER", "AUTH_LAYER for FILE_REPO"), new GetPasswordHostSpecificValue(cache, "database.FILE_REPO.AUTH_LAYER", "AUTH_LAYER for FILE_REPO")));
			registerValue("USAT.authoritylayer.interchange.hsqltxnmanager.url", new DefaultHostSpecificValue("jdbc:hsqldb:file:conf/hsql/iso8583;user=sa;password="));
			registerValue("USAT.authoritylayer.interchange.paymentech.simulationMode", new ServerEnvHostSpecificValue("false","false","false","false"));
			registerValue("USAT.authoritylayer.interchange.paymentech.url", new ServerEnvHostSpecificValue("https://netconnectkavar1.paymentech.net/NetConnect/controller","https://netconnectkavar1.paymentech.net/NetConnect/controller","https://netconnectkavar1.paymentech.net/NetConnect/controller","https://netconnectka1.paymentech.net/NetConnect/controller"));
			registerValue("USAT.authoritylayer.interchange.paymentech.userid", new ServerEnvHostSpecificValue("USATECHINC","USATECHINC","USATECHINC","USATCO06"));
			registerValue("USAT.authoritylayer.interchange.paymentech.password", new ServerEnvHostSpecificValue("PA55WORD", "PA55WORD", "PA55WORD", new GetPasswordHostSpecificValue(cache, "gateway.paymentech", "Paymentech Gateway")));
			registerValue("USAT.authoritylayer.blackboard.vendorNumber", new ServerEnvHostSpecificValue("213","213","213","2548"));
			registerValue("USAT.authoritylayer.interchange.firstdata.url", new ServerEnvHostSpecificValue("https://stg.dw.us.fdcnet.biz/vs/services/vxnservice","https://stg.dw.us.fdcnet.biz/vs/services/vxnservice","https://stg.dw.us.fdcnet.biz/vs/services/vxnservice",""));
			registerValue("USAT.authoritylayer.interchange.firstdata.did", new ServerEnvHostSpecificValue("00010122117335237765","00010122117335237765","00010122117335237765",""));
			registerValue("USAT.authoritylayer.interchange.firstdata.tppid", new ServerEnvHostSpecificValue("TST001", "TST001", "TST001", ""));
			
			registerValue("USAT.authoritylayer.elavon.fileIdOffset", ordinal == null ? new HostSpecificValue() {
				public boolean isOverriding() {
					return true;
				}			
				public String getValue(Host host, Interaction interaction) {
					return host.getSimpleName().substring(7,8);
				}
			} : new DefaultHostSpecificValue(ordinal.toString()));
			registerValue("USAT.authoritylayer.elavon.destinationId", new ServerEnvHostSpecificValue("9495", "9495", "9495", "8009"));
			registerValue("USAT.authoritylayer.elavon.fileExtension", new ServerEnvHostSpecificValue("uat","uat","uat","edc"));
			registerValue("USAT.authoritylayer.elavon.host", new ServerEnvHostSpecificValue("10.0.0.110", "10.0.0.110", "198.203.191.201", "sftp.novainfo.net"));
			registerValue("USAT.authoritylayer.elavon.port", new ServerEnvHostSpecificValue("21", "21", "990", "990"));
			registerValue("USAT.authoritylayer.elavon.username", new ServerEnvHostSpecificValue("anonymous", "anonymous", "usatech", "usatech"));
			registerValue("USAT.authoritylayer.elavon.password", new ServerEnvHostSpecificValue("password@anonymous.com", "password@anonymous.com", new GetPasswordHostSpecificValue(cache, "gateway.elavon.ftp", "Elavon FTP usatech user"), new GetPasswordHostSpecificValue(
					cache, "gateway.elavon.ftp", "Elavon FTP usatech user")));
			registerValue("USAT.authoritylayer.elavon.encryptType", new ServerEnvHostSpecificValue("SSL_REQUIRE", "SSL_REQUIRE", "SSL_IMPLICIT", "SSL_IMPLICIT"));
			registerValue("USAT.authoritylayer.elavon.settlementRemoteDir", new ServerEnvHostSpecificValue("INBOX-DEV", "INBOX-INT", "INBOX", "INBOX"));
			registerValue("USAT.authoritylayer.elavon.feedbackRemoteDir", new ServerEnvHostSpecificValue("OUTBOX-DEV", "OUTBOX-INT", "OUTBOX", "OUTBOX"));
			registerValue("USAT.authoritylayer.elavon.feedbackProcessedDir", new DefaultHostSpecificValue("work/processed"));
			registerValue("USAT.authoritylayer.elavon.merchantCity", new DefaultHostSpecificValue("Malvern"));
			registerValue("USAT.authoritylayer.elavon.merchantState", new DefaultHostSpecificValue("PA"));
			registerValue("USAT.authoritylayer.elavon.merchantZip", new DefaultHostSpecificValue("19355"));
			registerValue("com.usatech.authoritylayer.ElavonClearingFileTask.partialAuthReversalTerminalPattern", new ServerEnvHostSpecificValue(".*", ".*", ".*", (String) null));
			registerValue("com.usatech.authoritylayer.ElavonClearingFileTask.addressAddendumTerminalPattern", new ServerEnvHostSpecificValue(".*", ".*", ".*", ".*"));

			registerValue("USAT.authoritylayer.interchange.heartland.url", new ServerEnvHostSpecificValue("https://posgateway.cert.secureexchange.net/Hps.Exchange.PosGateway.UAT/PosGatewayService.asmx","https://posgateway.cert.secureexchange.net/Hps.Exchange.PosGateway.UAT/PosGatewayService.asmx","https://posgateway.cert.secureexchange.net/Hps.Exchange.PosGateway.UAT/PosGatewayService.asmx","https://posgateway.secureexchange.net/Hps.Exchange.PosGateway/PosGatewayService.asmx"));
			registerValue("USAT.authoritylayer.interchange.heartland.licenseid", new ServerEnvHostSpecificValue("20432","20432","20432","20432"));
			registerValue("USAT.authoritylayer.interchange.heartland.userid", new ServerEnvHostSpecificValue("17030134A","17030134A","17030134A","17030134A"));
			registerValue("USAT.authoritylayer.interchange.heartland.password", new GetPasswordHostSpecificValue(cache, "gateway.heartland", "Heartland processing"));
			registerValue("USAT.authoritylayer.interchange.comsgate.url", new ServerEnvHostSpecificValue("https://webtest.comstarinteractive-do-not-use.com/comsgate.asp", "https://www.comstarinteractive-do-not-use.com/chargeanywheremanager/m2p/comsgate.asp", "https://www.comstarinteractive-do-not-use.com/chargeanywheremanager/m2p/comsgate.asp", "https://www.comstarinteractive-do-not-use.com/chargeanywheremanager/m2p/comsgate.asp"));
			registerValue("USAT.authoritylayer.interchange.comsgate.identification", new ServerEnvHostSpecificValue("WT.27680.0001", "WT.66995.0006", "WT.66995.0006", "WT.66995.0006"));
			registerValue("USAT.authoritylayer.interchange.comsgate.connectionTimeoutMs", new DefaultHostSpecificValue("5000"));
			registerValue("USAT.authoritylayer.interchange.comsgate.socketTimeoutMs", new DefaultHostSpecificValue("15000"));
			registerValue("USAT.authlayer.inactivityThreshhold", new ServerEnvHostSpecificValue("1200000000", "1200000000", "1200000", "1200000"));
			registerValue("USAT.authoritylayer.debitBinRange.host", new ServerEnvHostSpecificValue("10.0.0.110", "10.0.0.110", /*"198.203.191.201"*/"sftp.novainfo.net", "sftp.novainfo.net"));
			registerValue("USAT.authoritylayer.debitBinRange.port", new ServerEnvHostSpecificValue("21", "21", "990", "990"));
			registerValue("USAT.authoritylayer.debitBinRange.username", new ServerEnvHostSpecificValue("anonymous", "anonymous", "usatbin", "usatbin"));
			registerValue("USAT.authoritylayer.debitBinRange.password", new ServerEnvHostSpecificValue("dev", "int", new GetPasswordHostSpecificValue(cache, "gateway.elavon.ftp", "Elavon FTP usatbin user"), new GetPasswordHostSpecificValue(cache, "gateway.elavon.ftp", "Elavon FTP usatbin user")));
			registerValue("USAT.authoritylayer.debitBinRange.encryptType", new ServerEnvHostSpecificValue("SSL_REQUIRE", "SSL_REQUIRE", "SSL_IMPLICIT", "SSL_IMPLICIT"));
			registerValue("USAT.authoritylayer.debitBinRange.remoteDir", new ServerEnvHostSpecificValue("BIN_RANGE", "BIN_RANGE", "/Distribution/User/binuser/outbox", "/Distribution/User/binuser/outbox"));
			// registerValue("USAT.authoritylayer.debitBinRange.host", new DefaultHostSpecificValue("sftp.novainfo.net"));
			// registerValue("USAT.authoritylayer.debitBinRange.port", new DefaultHostSpecificValue("990"));
			// registerValue("USAT.authoritylayer.debitBinRange.username", new DefaultHostSpecificValue("usatbin"));
			// registerValue("USAT.authoritylayer.debitBinRange.password", new GetPasswordHostSpecificValue(cache, "gateway.elavon.ftp", "Elavon FTP usatbin user"));
			// registerValue("USAT.authoritylayer.debitBinRange.encryptType", new DefaultHostSpecificValue("SSL_IMPLICIT"));
			// registerValue("USAT.authoritylayer.debitBinRange.remoteDir", new DefaultHostSpecificValue("/Distribution/User/binuser/outbox"));
			
			String vzm2pTestLoyaltyUrl = "http://204.51.112.180:8080/active-bpel/services/soap12/M2P_USAT_Payment_Processing";
			registerValue("STATIC.com.usatech.authoritylayer.VZM2PTask.vzm2pLoyaltyUrl", new ServerEnvHostSpecificValue(vzm2pTestLoyaltyUrl, vzm2pTestLoyaltyUrl, vzm2pTestLoyaltyUrl, (String) null));
			
			String noopHostnameVerifier="org.apache.http.conn.ssl.NoopHostnameVerifier";
			registerValue("javax.net.ssl.HostnameVerifier.class", new ServerEnvHostSpecificValue(noopHostnameVerifier, noopHostnameVerifier, noopHostnameVerifier,(String)null));
			String noopHostnameVerifierInstance="\"javax.net.ssl.HostnameVerifier\"";
			registerValue("STATIC.com.usatech.layers.common.sprout.SproutUtils.hostNameVerifier", new ServerEnvHostSpecificValue(noopHostnameVerifierInstance, noopHostnameVerifierInstance, noopHostnameVerifierInstance,(String)null));			
			String testSproutAuthorizationToken = "cbf35bb7eb1fe7eacfbad3ffe313eebadc00d35a615ce372306bd8a6a0fab6df";
			registerValue("STATIC.com.usatech.authoritylayer.SproutGatewayTask.sproutAuthorizationToken", new ServerEnvHostSpecificValue(testSproutAuthorizationToken, testSproutAuthorizationToken, testSproutAuthorizationToken, new GetPasswordHostSpecificValue(cache, "Sprout prod auth token", "Sprout prod auth token")));

			String aprivaHost_test = "aibapp53.aprivaeng.com"; // "aibapp19.aprivaeng.com";
			int aprivaPort_test = 11098;
			String aprivaHost_prod = "api.apriva.com";
			int aprivaPort_prod = 11098;
			registerValue("USAT.authoritylayer.apriva.host", new ServerEnvHostSpecificValue(aprivaHost_test, aprivaHost_test, aprivaHost_test, aprivaHost_prod));
			registerValue("USAT.authoritylayer.apriva.port", new ServerEnvHostSpecificValue(String.valueOf(aprivaPort_test), String.valueOf(aprivaPort_test), String.valueOf(aprivaPort_test), String.valueOf(aprivaPort_prod)));
			clientKeyAliasMap.registerAlias(aprivaHost_test, aprivaPort_test, "apriva-client");
			clientKeyAliasMap.registerAlias(aprivaHost_prod, aprivaPort_prod, "apriva-client-prod", "ECC");
			clientKeyAliasMap.registerAlias(aprivaHost_prod, aprivaPort_prod, "apriva-client-prod", "USA");
			
			registerValue("USAT.authoritylayer.coupon.host", new ServerEnvHostSpecificValue("api-integration-ext.vsm2m.net", "api-integration-ext.vsm2m.net", "api-integration-ext.vsm2m.net", "api-prime.vsm2m.net")); 
			registerValue("USAT.authoritylayer.coupon.port", new DefaultHostSpecificValue(String.valueOf(443)));
			registerValue("USAT.authoritylayer.coupon.username", new ServerEnvHostSpecificValue("usatqcuser", "usatqcuser", "usatqcuser", "usatqcuser"));
			registerValue("USAT.authoritylayer.coupon.password", new ServerEnvHostSpecificValue("MmFyUpjOmXkz","MmFyUpjOmXkz", "MmFyUpjOmXkz", new GetPasswordHostSpecificValue(cache, "coupon.vsm2m", "Password for Coupon vsm2m")));

			registerValue("USAT.authoritylayer.ugryd.instanceCount", new ServerEnvHostSpecificValue("4", "4", "4", "4"));
			registerValue("USAT.authoritylayer.ugryd.url", new ServerEnvHostSpecificValue("http://nm.uat.ugryd.com/services.php", "http://nm.uat.ugryd.com/services.php", "http://nm.uat.ugryd.com/services.php", "https://nm.ugryd.com/services.php"));
			registerValue("USAT.authoritylayer.ugryd.username", new ServerEnvHostSpecificValue("usatech", "usatech", "usatech", "usat"));
			registerValue("USAT.authoritylayer.ugryd.password", new ServerEnvHostSpecificValue(
					new GetPasswordHostSpecificValue(cache, "ugryd.password", "CBORD UGryd TEST"),
					new GetPasswordHostSpecificValue(cache, "ugryd.password", "CBORD UGryd TEST"),
					new GetPasswordHostSpecificValue(cache, "ugryd.password", "CBORD UGryd TEST"),
					new GetPasswordHostSpecificValue(cache, "ugryd.password", "CBORD UGryd PROD")));
			registerValue("USAT.authoritylayer.ugryd.useDeviceSerialCdForTerminalCd", new ServerEnvHostSpecificValue("false", "false", "false", "true"));
			
			registerValue("USAT.authoritylayer.USConnectApollo.url", new ServerEnvHostSpecificValue(
					"http://dev.apollo.usconnect.blueworldinc.com", 
					"http://dev.apollo.usconnect.blueworldinc.com", 
					"http://dev.apollo.usconnect.blueworldinc.com", 
					"https://hercules.usconnectme.com"));
			registerValue("USAT.authoritylayer.USConnectApollo.token", new ServerEnvHostSpecificValue(
					new GetPasswordHostSpecificValue(cache, "usconnect.token", "USConnect Apollo DEV"),
					new GetPasswordHostSpecificValue(cache, "usconnect.token", "USConnect Apollo DEV"),
					new GetPasswordHostSpecificValue(cache, "usconnect.token", "USConnect Apollo DEV"),
					new GetPasswordHostSpecificValue(cache, "usconnect.token", "USConnect Apollo PROD")));
			registerValue("USAT.authoritylayer.USConnectApollo.minimumBalanceAmount", new DefaultHostSpecificValue("0"));
			
		} else if("posmlayer".equalsIgnoreCase(app.getName())) {
			registerValue("USAT.database.MQ.password.POSM_LAYER", new ServerEnvHostSpecificValue("POSM_LAYER_1", "POSM_LAYER_1", new GetPasswordHostSpecificValue(cache, "database.MQ.POSM_LAYER", "POSM_LAYER for MQ"), new GetPasswordHostSpecificValue(cache, "database.MQ.POSM_LAYER", "POSM_LAYER for MQ")));
			registerValue("USAT.database.FILE_REPO.password.POSM_LAYER", new ServerEnvHostSpecificValue("POSM_LAYER_1","POSM_LAYER_1", new GetPasswordHostSpecificValue(cache, "database.FILE_REPO.POSM_LAYER", "POSM_LAYER for FILE_REPO"), new GetPasswordHostSpecificValue(cache, "database.FILE_REPO.POSM_LAYER", "POSM_LAYER for FILE_REPO")));
			registerValue("USAT.database.OPER.password.POSM_LAYER", new ServerEnvHostSpecificValue("POSM_LAYER_1","POSM_LAYER_1", new GetPasswordHostSpecificValue(cache, "database.OPER.POSM_LAYER", "POSM_LAYER for OPER"), new GetPasswordHostSpecificValue(cache, "database.OPER.POSM_LAYER", "POSM_LAYER for OPER")));
			registerValue("USAT.database.OPER.password.POSM_ADMIN_USER", new ServerEnvHostSpecificValue("ZODT8FER","ZODT8FER", new GetPasswordHostSpecificValue(cache, "database.OPER.POSM_ADMIN_USER", "POSM_ADMIN_USER for OPER"), new GetPasswordHostSpecificValue(cache, "database.OPER.POSM_ADMIN_USER", "POSM_ADMIN_USER for OPER")));
			registerValue("USAT.database.REPORT.password.POSM_LAYER", new ServerEnvHostSpecificValue("POSM_LAYER_1","POSM_LAYER_1", new GetPasswordHostSpecificValue(cache, "database.REPORT.POSM_LAYER", "POSM_LAYER for REPORT"), new GetPasswordHostSpecificValue(cache, "database.REPORT.POSM_LAYER", "POSM_LAYER for REPORT")));
			registerValue("USAT.database.MAIN.password.POSM_LAYER", new ServerEnvHostSpecificValue("POSM_LAYER_1", "POSM_LAYER_1", new GetPasswordHostSpecificValue(cache, "database.MAIN.POSM_LAYER", "POSM_LAYER for MAIN"), new GetPasswordHostSpecificValue(cache, "database.MAIN.POSM_LAYER", "POSM_LAYER for MAIN")));

			registerValue("USAT.posmlayer.inactivityThreshhold", new ServerEnvHostSpecificValue("1200000000", "1200000000", "1200000", "1200000"));
			registerValue("USAT.posmlayer.retryIntervalMillis", new ServerEnvHostSpecificValue("900000","900000","900000","3600000"));
			registerValue("USAT.posmlayer.retryAttempts", new DefaultHostSpecificValue("144"));
			registerValue("USAT.simplemq.direct.posmadmin.port", new DefaultHostSpecificValue(String.valueOf(getDirectPort(POSM_ADMIN_PORT_OFFSET, ordinal))));
			registerValue("USAT.simplemq.directPollerUrl.posmadmin_p", new DefaultHostSpecificValue("localhost:${USAT.simplemq.direct.posmadmin.port}"));

			registerValue("simple.app.Service.POSMQuartzScheduledTask.job(eSudsDiagnostic).jobDataMap(customer.ids)", new ServerEnvHostSpecificValue("35", "35", "35", "36,3"));
			registerValue("simple.app.Service.POSMQuartzScheduledTask.job(eSudsDiagnostic).jobDataMap(customer.36.email.addresses)", new ServerEnvHostSpecificValue(null, null, null, "service@caldwellandgregory.com,wjensen@caldwellandgregory.com"));
			registerValue("simple.app.Service.POSMQuartzScheduledTask.job(eSudsDiagnostic).jobDataMap(customer.3.email.addresses)", new ServerEnvHostSpecificValue(null, null, null, "dfd@asi-maytag.com"));
			registerValue("simple.app.Service.POSMQuartzScheduledTask.job(eSudsDiagnostic).jobDataMap(customer.35.email.addresses)", new ServerEnvHostSpecificValue("SoftwareDevelopmentTeam@usatech.com", "QualityAssurance@usatech.com", "QualityAssurance@usatech.com", (String) null));
			
			registerValue("USAT.environment.suffix", new ServerEnvHostSpecificValue("-DEV", "-INT", "-ECC", ""));
			registerValue("USAT.riskAlert.email", new ServerEnvHostSpecificValue("SoftwareDevelopmentTeam@usatech.com", "QualityAssurance@usatech.com", "QualityAssurance@usatech.com", "RiskManagement@usatech.com"));
			registerValue("USAT.dfr.alert.email", new ServerEnvHostSpecificValue("SoftwareDevelopmentTeam@usatech.com", "SoftwareDevelopmentTeam@usatech.com", "SoftwareDevelopmentTeam@usatech.com", "ApplicationSupportTeam@usatech.com,RiskManagement@usatech.com"));

			registerValue("USAT.authoritylayer.tandem.dfr.host", new ServerEnvHostSpecificValue("206.253.180.47", "206.253.180.47", "206.253.180.47", "206.253.184.37"));
			registerValue("USAT.authoritylayer.tandem.dfr.username", new ServerEnvHostSpecificValue("usatsdfr", "usatsdfr", "usatsdfr", "usapddfr"));
			registerValue("USAT.authoritylayer.tandem.dfr.password", new ServerEnvHostSpecificValue("pgpyvsdr", "pgpyvsdr", "pgpyvsdr", new GetPasswordHostSpecificValue(cache, "gateway.tandem.dfr.sftp", "Password for Paymentech DFR SFTP")));
			registerValue("USAT.authoritylayer.tandem.dfr.filePathPrefix", new ServerEnvHostSpecificValue("/test/data/253145/", "/test/data/253145/", "/test/data/253145/", "/prod/data/253145/"));
			registerValue("USAT.authoritylayer.tandem.binrange.host", new ServerEnvHostSpecificValue("206.253.180.47", "206.253.180.47", "206.253.180.47", "206.253.184.37"));
			registerValue("USAT.authoritylayer.tandem.binrange.username", new ServerEnvHostSpecificValue("usatsbin", "usatsbin", "usatsbin", "usapdbin"));
			registerValue("USAT.authoritylayer.tandem.binrange.password", new ServerEnvHostSpecificValue("6fdc5tjg", "6fdc5tjg", "6fdc5tjg", new GetPasswordHostSpecificValue(cache, "gateway.tandem.binrange.sftp", "Password for Paymentech Bin Range SFTP")));
			registerValue("USAT.authoritylayer.tandem.binrange.filePathFormat", new ServerEnvHostSpecificValue("/test/data/253144/GBINP.V005.{0,DATE,MMddyyyy}.P", "/test/data/253144/GBINP.V005.{0,DATE,MMddyyyy}.P", "/test/data/253144/GBINP.V005.{0,DATE,MMddyyyy}.P", "/prod/data/253144/GBINP.V005.{0,DATE,MMddyyyy}.P"));

			registerValue("USAT.authoritylayer.tandem.sftp.host", new ServerEnvHostSpecificValue("206.253.180.47", "206.253.180.47", "206.253.180.47", "206.253.184.37"));
			registerValue("USAT.authoritylayer.tandem.sftp.port", new DefaultHostSpecificValue("22"));
			registerValue("USAT.authoritylayer.tandem.companyId", new DefaultHostSpecificValue("248258"));
			registerValue("USAT.authoritylayer.tandem.companyName", new DefaultHostSpecificValue("USA Technologies, Inc."));
			
			registerValue("USAT.authoritylayer.tandem.submerchants.pid", new ServerEnvHostSpecificValue("251341", "251341", "251341", "251341"));
			registerValue("USAT.authoritylayer.tandem.submerchants.pidPassword", new ServerEnvHostSpecificValue(
					new GetPasswordHostSpecificValue(cache, "tandem.submerchants.pidPassword", "tandem.submerchants.pidPassword"),
					new GetPasswordHostSpecificValue(cache, "tandem.submerchants.pidPassword", "tandem.submerchants.pidPassword"),
					new GetPasswordHostSpecificValue(cache, "tandem.submerchants.pidPassword", "tandem.submerchants.pidPassword"),
					new GetPasswordHostSpecificValue(cache, "tandem.submerchants.pidPassword", "tandem.submerchants.pidPassword")));
			registerValue("USAT.authoritylayer.tandem.submerchants.sid", new ServerEnvHostSpecificValue("251341", "251341", "251341", "251341"));
			registerValue("USAT.authoritylayer.tandem.submerchants.sidPassword", new ServerEnvHostSpecificValue(
					new GetPasswordHostSpecificValue(cache, "tandem.submerchants.sidPassword", "tandem.submerchants.sidPassword"),
					new GetPasswordHostSpecificValue(cache, "tandem.submerchants.sidPassword", "tandem.submerchants.sidPassword"),
					new GetPasswordHostSpecificValue(cache, "tandem.submerchants.sidPassword", "tandem.submerchants.sidPassword"),
					new GetPasswordHostSpecificValue(cache, "tandem.submerchants.sidPassword", "tandem.submerchants.sidPassword")));
			registerValue("USAT.authoritylayer.tandem.submerchants.host", new DefaultHostSpecificValue("${USAT.authoritylayer.tandem.sftp.host}"));
			registerValue("USAT.authoritylayer.tandem.submerchants.port", new DefaultHostSpecificValue("${USAT.authoritylayer.tandem.sftp.port}"));
			registerValue("USAT.authoritylayer.tandem.submerchants.username", new ServerEnvHostSpecificValue("usamtcht", "usamtcht", "usamtcht", "usamtchp"));
			registerValue("USAT.authoritylayer.tandem.submerchants.password", new ServerEnvHostSpecificValue("aixrn25j", "aixrn25j", "aixrn25j", new GetPasswordHostSpecificValue(cache, "tandem.sftp.submerchants", "Chase Paymentech SubMerchants Upload SFTP", true)));
			registerValue("USAT.authoritylayer.tandem.submerchants.upload.directory", new ServerEnvHostSpecificValue("/test/data/${USAT.authoritylayer.tandem.submerchants.pid}", "/test/data/${USAT.authoritylayer.tandem.submerchants.pid}", "/test/data/${USAT.authoritylayer.tandem.submerchants.pid}", "/home/${USAT.authoritylayer.tandem.submerchants.pid}"));
			registerValue("USAT.authoritylayer.tandem.submerchants.retryIntervalMs", new DefaultHostSpecificValue("62208000000"));
			registerValue("USAT.authoritylayer.tandem.submerchants.includeValidationFailures", new DefaultHostSpecificValue("true"));
			
			registerValue("USAT.authoritylayer.tandem.fanf.pid", new ServerEnvHostSpecificValue("253889", "253889", "253889", "253889"));
			registerValue("USAT.authoritylayer.tandem.fanf.pidPassword", new ServerEnvHostSpecificValue(
					new GetPasswordHostSpecificValue(cache, "tandem.fanf.pidPassword", "tandem.fanf.pidPassword"),
					new GetPasswordHostSpecificValue(cache, "tandem.fanf.pidPassword", "tandem.fanf.pidPassword"),
					new GetPasswordHostSpecificValue(cache, "tandem.fanf.pidPassword", "tandem.fanf.pidPassword"),
					new GetPasswordHostSpecificValue(cache, "tandem.fanf.pidPassword", "tandem.fanf.pidPassword")));
			registerValue("USAT.authoritylayer.tandem.fanf.sid", new ServerEnvHostSpecificValue("253889", "253889", "253889", "253889"));
			registerValue("USAT.authoritylayer.tandem.fanf.sidPassword", new ServerEnvHostSpecificValue(
					new GetPasswordHostSpecificValue(cache, "tandem.fanf.sidPassword", "tandem.fanf.sidPassword"),
					new GetPasswordHostSpecificValue(cache, "tandem.fanf.sidPassword", "tandem.fanf.sidPassword"),
					new GetPasswordHostSpecificValue(cache, "tandem.fanf.sidPassword", "tandem.fanf.sidPassword"),
					new GetPasswordHostSpecificValue(cache, "tandem.fanf.sidPassword", "tandem.fanf.sidPassword")));
			registerValue("USAT.authoritylayer.tandem.fanf.host", new DefaultHostSpecificValue("${USAT.authoritylayer.tandem.sftp.host}"));
			registerValue("USAT.authoritylayer.tandem.fanf.port", new DefaultHostSpecificValue("${USAT.authoritylayer.tandem.sftp.port}"));
			registerValue("USAT.authoritylayer.tandem.fanf.username", new ServerEnvHostSpecificValue("usatfanf", "usatfanf", "usatfanf", "usapfanf"));
			registerValue("USAT.authoritylayer.tandem.fanf.password", new ServerEnvHostSpecificValue("27m5oa7d", "27m5oa7d", "27m5oa7d", new GetPasswordHostSpecificValue(cache, "tandem.sftp.fanf", "Chase Paymentech FANF Upload SFTP", true)));
			registerValue("USAT.authoritylayer.tandem.fanf.upload.directory", new ServerEnvHostSpecificValue("/test/data/${USAT.authoritylayer.tandem.fanf.pid}", "/test/data/${USAT.authoritylayer.tandem.fanf.pid}", "/test/data/${USAT.authoritylayer.tandem.fanf.pid}", "/home/${USAT.authoritylayer.tandem.fanf.pid}"));
			String noopHostnameVerifier="org.apache.http.conn.ssl.NoopHostnameVerifier";
			registerValue("javax.net.ssl.HostnameVerifier.class", new ServerEnvHostSpecificValue(noopHostnameVerifier, noopHostnameVerifier, noopHostnameVerifier,(String)null));
			String noopHostnameVerifierInstance="\"javax.net.ssl.HostnameVerifier\"";
			registerValue("STATIC.com.usatech.layers.common.sprout.SproutUtils.hostNameVerifier", new ServerEnvHostSpecificValue(noopHostnameVerifierInstance, noopHostnameVerifierInstance, noopHostnameVerifierInstance,(String)null));			
			String testSproutUrl = "https://account-cert.bytevampire.com";
			registerValue("STATIC.com.usatech.layers.common.sprout.SproutUtils.sproutUrl", new ServerEnvHostSpecificValue(testSproutUrl, testSproutUrl, testSproutUrl, "https://api.bytevampire.com"));
			String testSproutAuthorizationToken = "cbf35bb7eb1fe7eacfbad3ffe313eebadc00d35a615ce372306bd8a6a0fab6df";
			registerValue("STATIC.com.usatech.layers.common.sprout.SproutUtils.sproutAuthorizationToken", new ServerEnvHostSpecificValue(testSproutAuthorizationToken, testSproutAuthorizationToken, testSproutAuthorizationToken, new GetPasswordHostSpecificValue(cache, "Sprout prod auth token", "Sprout prod auth token")));

		} else if("netlayer".equalsIgnoreCase(app.getName())) {
			int offset = (ordinal == null ? 1 : ordinal) * 100;
			registerValue("USAT.netlayer.port.unified", new DefaultHostSpecificValue(String.valueOf(14043 + offset)));
			registerValue("USAT.netlayer.port.layer3", new DefaultHostSpecificValue(String.valueOf(14007 + offset)));
			registerValue("USAT.netlayer.port.batch", new DefaultHostSpecificValue(String.valueOf(14008 + offset)));
			registerValue("USAT.netlayer.port.auth", new DefaultHostSpecificValue(String.valueOf(14009 + offset)));
			registerValue("USAT.netlayer.port.test", new DefaultHostSpecificValue(String.valueOf(ordinal == null || ordinal == 1 ? 3268 : 14068 + ordinal * 100)));
			registerValue("USAT.netlayer.deviceNameRegex", new DefaultHostSpecificValue("^((EV|TD)[0-9]{6}|LD-BLNCR)$"));
			registerValue("USAT.netlayer.deviceLimitedActivityRegex", new DefaultHostSpecificValue("^(INVALID|LD-BLNCR)$"));
			registerValue("USAT.database.MQ.password.NET_LAYER", new ServerEnvHostSpecificValue("NET_LAYER_1","NET_LAYER_1", new GetPasswordHostSpecificValue(cache, "database.MQ.NET_LAYER", "NET_LAYER for MQ"), new GetPasswordHostSpecificValue(cache, "database.MQ.NET_LAYER", "NET_LAYER for MQ")));
			registerValue("USAT.database.FILE_REPO.password.NET_LAYER", new ServerEnvHostSpecificValue("NET_LAYER_1","NET_LAYER_1", new GetPasswordHostSpecificValue(cache, "database.FILE_REPO.NET_LAYER", "NET_LAYER for FILE_REPO"), new GetPasswordHostSpecificValue(cache, "database.FILE_REPO.NET_LAYER", "NET_LAYER for FILE_REPO")));
			registerValue("USAT.netlayer.inactivityThreshhold", new ServerEnvHostSpecificValue("600000000", "600000000", "600000","600000"));
			registerValue("jetty.port", new DefaultHostSpecificValue(String.valueOf(ordinal == null || ordinal == 1 ? 9443 : 19043 + ordinal * 100)));
			registerValue("PublicWebServiceContext.tempDirectory", new DefaultHostSpecificValue("/opt/USAT/tmp/netlayer_jetty_${USAT.jetty.port}"));
			registerValue("PublicSslContextFactory.includeProtocols", new DefaultHostSpecificValue("TLSv1.1,TLSv1.2"));
			registerValue("STATIC.com.usatech.eportconnect.ECSessionManager.tmpPasswordTimeLimit", new ServerEnvHostSpecificValue("0", "0", "0", String.valueOf(24 * 60 * 60 * 1000L)));
		} else if("dms".equalsIgnoreCase(app.getName())) {
			registerValue("jetty.port", new DefaultHostSpecificValue(String.valueOf(7080 + app.getPortOffset() + (ordinal == null ? 700 : ordinal * 100))));
			registerValue("USAT.web.sessionTimeout", new ServerEnvHostSpecificValue("28800", "28800", "28800", "28800"));
			registerValue("USAT.database.OPER.password.DMS_USER", new ServerEnvHostSpecificValue("DMS_USER_1", "DMS_USER_1", new GetPasswordHostSpecificValue(cache, "database.OPER.DMS_USER", "DMS_USER for OPER"), new GetPasswordHostSpecificValue(cache, "database.OPER.DMS_USER", "DMS_USER for OPER")));
			registerValue("USAT.database.REPORT.password.DMS_USER", new ServerEnvHostSpecificValue("DMS_USER_1", "DMS_USER_1", new GetPasswordHostSpecificValue(cache, "database.REPORT.DMS_USER", "DMS_USER for REPORT"), new GetPasswordHostSpecificValue(cache, "database.REPORT.DMS_USER", "DMS_USER for REPORT")));
			registerValue("USAT.database.MAIN.password.DMS_USER", new ServerEnvHostSpecificValue("DMS_1", "DMS_1", new GetPasswordHostSpecificValue(cache, "database.MAIN.DMS_USER", "DMS_USER for MAIN"), new GetPasswordHostSpecificValue(cache, "database.MAIN.DMS_USER", "DMS_USER for MAIN")));
			registerValue("USAT.database.MQ.password.DMS", new ServerEnvHostSpecificValue("DMS_1", "DMS_1", new GetPasswordHostSpecificValue(cache, "database.MQ.DMS", "DMS for MQ"), new GetPasswordHostSpecificValue(cache, "database.MQ.DMS", "DMS for MQ")));
			registerValue("USAT.database.FILE_REPO.password.DMS", new ServerEnvHostSpecificValue("DMS_1", "DMS_1", new GetPasswordHostSpecificValue(cache, "database.FILE_REPO.DMS", "DMS for FILE_REPO"), new GetPasswordHostSpecificValue(cache, "database.FILE_REPO.DMS", "DMS for FILE_REPO")));
			registerValue("USAT.environment.suffix", new ServerEnvHostSpecificValue("-DEV", "-INT", "-ECC", ""));
			registerValue("USAT.loadbalancer.ip.pattern", new ServerEnvHostSpecificValue("10\\.0\\.0\\.64", "10\\.0\\.0\\.64", "192\\.168\\.4\\.65", "192\\.168\\.79\\.18[34]"));
			MappedHostSpecificValue eportConnectUrl = new MappedHostSpecificValue();
			eportConnectUrl.registerValue("DEV", "APR", "https://ec-dev.usatech.com:9443/hessian/ec2");
			eportConnectUrl.registerValue("INT", "APR", "https://ec-int.usatech.com:9443/hessian/ec2");
			eportConnectUrl.registerValue("ECC", "APR", "https://ec-ecc.usatech.com:9443/hessian/ec2");
			eportConnectUrl.registerValue("USA", "APR", "https://ec.usatech.com:9443/hessian/ec2");
			registerValue("STATIC.com.usatech.ec2.EC2ClientUtils.eportConnectUrl", eportConnectUrl);
			registerValue("jetty.handler.WebAppContext.tempDirectory", new DefaultHostSpecificValue("/opt/USAT/tmp/dms_jetty_${USAT.jetty.port}"));
			
			registerValue("USAT.database.LOGS.password.LOGS_USER",
					new ServerEnvHostSpecificValue("LOGS_USER_1", "LOGS_USER_1",
							new GetPasswordHostSpecificValue(cache, "database.LOGS.LOGS_USER", "LOGS_USER for LOGS"),
							new GetPasswordHostSpecificValue(cache, "database.LOGS.LOGS_USER", "LOGS_USER for LOGS")));

		} else if("rptreq".equalsIgnoreCase(app.getName())) {
			registerValue("USAT.database.REPORT.password.RPT_REQ_APP", new ServerEnvHostSpecificValue(StringUtils.reverse("RPT_REQ_APP_1"), StringUtils.reverse("RPT_REQ_APP_1"), new GetPasswordHostSpecificValue(cache, "database.REPORT.RPT_REQ_APP", "RPT_REQ_APP for REPORT"), new GetPasswordHostSpecificValue(cache, "database.REPORT.RPT_REQ_APP", "RPT_REQ_APP for REPORT")));
			registerValue("USAT.database.MSR_MQ.password.RPTREQ", new ServerEnvHostSpecificValue("RPTREQ_1", "RPTREQ_1", new GetPasswordHostSpecificValue(cache, "database.MSR_MQ.RPTREQ", "RPTREQ for MSR_MQ"), new GetPasswordHostSpecificValue(cache, "database.MSR_MQ.RPTREQ", "RPTREQ for MSR_MQ")));
		} else if("rptgen".equalsIgnoreCase(app.getName())) {
			MappedHostSpecificValue eportConnectUrl = new MappedHostSpecificValue();
			eportConnectUrl.registerValue("DEV", "APP", "https://ec-dev.usatech.com:9443/hessian/ec2");
			eportConnectUrl.registerValue("INT", "APP", "https://ec-int.usatech.com:9443/hessian/ec2");
			eportConnectUrl.registerValue("ECC", "APP", "https://ec-ecc.usatech.com:9443/hessian/ec2");
			eportConnectUrl.registerValue("USA", "APP", "https://ec.usatech.com:9443/hessian/ec2");
			registerValue("STATIC.com.usatech.ec2.EC2ClientUtils.eportConnectUrl", eportConnectUrl);
			registerValue("USAT.database.MSR_MQ.password.RPTGEN", new ServerEnvHostSpecificValue("RPTGEN_1", "RPTGEN_1", new GetPasswordHostSpecificValue(cache, "database.MSR_MQ.RPTGEN", "RPTGEN for MSR_MQ"), new GetPasswordHostSpecificValue(cache, "database.MSR_MQ.RPTGEN", "RPTGEN for MSR_MQ")));
			registerValue("USAT.database.MSR_FILE_REPO.password.RPTGEN", new ServerEnvHostSpecificValue("RPTGEN_1", "RPTGEN_1", new GetPasswordHostSpecificValue(cache, "database.MSR_FILE_REPO.RPTGEN", "RPTGEN for MSR_FILE_REPO"), new GetPasswordHostSpecificValue(cache, "database.MSR_FILE_REPO.RPTGEN", "RPTGEN for MSR_FILE_REPO")));
			registerValue("USAT.database.REPORT.password.RPT_GEN_APP", new ServerEnvHostSpecificValue(StringUtils.reverse("RPT_GEN_APP_1"), StringUtils.reverse("RPT_GEN_APP_1"), new GetPasswordHostSpecificValue(cache, "database.REPORT.RPT_GEN_APP", "RPT_GEN_APP for REPORT"), new GetPasswordHostSpecificValue(cache, "database.REPORT.RPT_GEN_APP", "RPT_GEN_APP for REPORT")));
			registerValue("USAT.database.MAIN.password.RPTGEN", new ServerEnvHostSpecificValue("RPTGEN_1", "RPTGEN_1", new GetPasswordHostSpecificValue(cache, "database.MAIN.RPTGEN", "RPTGEN for MAIN"), new GetPasswordHostSpecificValue(cache, "database.MAIN.RPTGEN", "RPTGEN for MAIN")));
			registerValue("USAT.environment.suffix", new ServerEnvHostSpecificValue("-DEV", "-INT", "-ECC", ""));
			registerValue("USAT.rptgen.inactivityThreshhold", new ServerEnvHostSpecificValue("1200000", "1200000", "1200000", "1200000"));
			registerValue("STATIC.simple.falcon.engine.standard.DirectXHTMLGenerator.timingLogged", new ServerEnvHostSpecificValue("true", "true", "true", (String) null));
			registerValue("USAT.initEmailTo", new ServerEnvHostSpecificValue("SoftwareDevelopmentTeam@usatech.com", "SoftwareDevelopmentTeam@usatech.com", "QualityAssurance@usatech.com", "KioskAlerts@usatech.com"));
			registerValue("USAT.marketingEmail", new ServerEnvHostSpecificValue("SoftwareDevelopmentTeam@usatech.com", "QualityAssurance@usatech.com", "QualityAssurance@usatech.com", "MoreEmailBlastApprovers@usatech.com"));
		} else if("rdwloader".equalsIgnoreCase(app.getName())) {
			registerValue("USAT.database.MSR_MQ.password.RPTGEN", new ServerEnvHostSpecificValue("RPTGEN_1", "RPTGEN_1", new GetPasswordHostSpecificValue(cache, "database.MSR_MQ.RPTGEN", "RPTGEN for MSR_MQ"), new GetPasswordHostSpecificValue(cache, "database.MSR_MQ.RPTGEN", "RPTGEN for MSR_MQ")));
			registerValue("USAT.database.OPER.password.RDW_LOADER", new ServerEnvHostSpecificValue("RDW_LOADER", "RDW_LOADER", new GetPasswordHostSpecificValue(cache, "database.OPER.RDWLOADER", "RDW_LOADER for OPER"), new GetPasswordHostSpecificValue(cache, "database.OPER.RDWLOADER", "RDW_LOADER for OPER")));
			registerValue("USAT.database.MAIN.password.RDWLOADER", new ServerEnvHostSpecificValue("RDWLOADER_1", "RDWLOADER_1", new GetPasswordHostSpecificValue(cache, "database.MAIN.RDWLOADER", "RDWLOADER for MAIN"), new GetPasswordHostSpecificValue(cache, "database.MAIN.RDWLOADER", "RDWLOADER for MAIN")));
			registerValue("USAT.rdwloader.inactivityThreshhold", new ServerEnvHostSpecificValue("1200000", "1200000", "1200000", "1200000"));
		} else if("transport".equalsIgnoreCase(app.getName())) {
			registerValue("USAT.database.MSR_MQ.password.TRANSPORT", new ServerEnvHostSpecificValue("TRANSPORT_1", "TRANSPORT_1", new GetPasswordHostSpecificValue(cache, "database.MSR_MQ.TRANSPORT", "TRANSPORT for MSR_MQ"), new GetPasswordHostSpecificValue(cache, "database.MSR_MQ.TRANSPORT", "TRANSPORT for MSR_MQ")));
			registerValue("USAT.database.MSR_FILE_REPO.password.TRANSPORT", new ServerEnvHostSpecificValue("TRANSPORT_1", "TRANSPORT_1", new GetPasswordHostSpecificValue(cache, "database.MSR_FILE_REPO.TRANSPORT", "TRANSPORT for MSR_FILE_REPO"), new GetPasswordHostSpecificValue(cache, "database.MSR_FILE_REPO.TRANSPORT", "TRANSPORT for MSR_FILE_REPO")));
			registerValue("USAT.knownhosts.file", new DefaultHostSpecificValue(specificFolderPath+"/known_hosts"));
			registerValue("jmx.remote.x.rmiRegistryHost", new DefaultHostSpecificValue("localhost")); // NOTE: I don't know why this is needed, but transporter fails to connect to the fqdn of its host (for usaweb32, usaweb33, and usaweb34)
			registerValue("USAT.environment.suffix", new ServerEnvHostSpecificValue("-DEV", "-INT", "-ECC", ""));
			registerValue("USAT.transport.inactivityThreshhold", new ServerEnvHostSpecificValue("7200000", "7200000", "7200000", "7200000"));
			registerValue("USAT.transport.eseye.username",
					new ServerEnvHostSpecificValue(
							new GetPasswordHostSpecificValue(cache, "transport.eseye.username", "EsEye API login"),
							new GetPasswordHostSpecificValue(cache, "transport.eseye.username", "EsEye API login"),
							new GetPasswordHostSpecificValue(cache, "transport.eseye.username", "EsEye API login"),
							new GetPasswordHostSpecificValue(cache, "transport.eseye.username", "EsEye API login"))
						);
			registerValue("USAT.transport.eseye.password",
					new ServerEnvHostSpecificValue(
							new GetPasswordHostSpecificValue(cache, "transport.eseye.password", "EsEye API password"),
							new GetPasswordHostSpecificValue(cache, "transport.eseye.password", "EsEye API password"),
							new GetPasswordHostSpecificValue(cache, "transport.eseye.password", "EsEye API password"),
							new GetPasswordHostSpecificValue(cache, "transport.eseye.password", "EsEye API password"))
						);
			registerValue("USAT.transport.eseye.location.password",
					new ServerEnvHostSpecificValue(
							new GetPasswordHostSpecificValue(cache, "transport.eseye.location.password", "EsEye Location API password"),
							new GetPasswordHostSpecificValue(cache, "transport.eseye.location.password", "EsEye Location API password"),
							new GetPasswordHostSpecificValue(cache, "transport.eseye.location.password", "EsEye Location API password"),
							new GetPasswordHostSpecificValue(cache, "transport.eseye.location.password", "EsEye Location API password"))
						);
			registerValue("USAT.transport.eseye.esEyePortfolioId",
					new ServerEnvHostSpecificValue(
							new GetPasswordHostSpecificValue(cache, "transport.eseye.esEyePortfolioId", "EsEye PortfolioID"),
							new GetPasswordHostSpecificValue(cache, "transport.eseye.esEyePortfolioId", "EsEye PortfolioID"),
							new GetPasswordHostSpecificValue(cache, "transport.eseye.esEyePortfolioId", "EsEye PortfolioID"),
							new GetPasswordHostSpecificValue(cache, "transport.eseye.esEyePortfolioId", "EsEye PortfolioID"))
			);
			registerValue("USAT.transport.verizon.username",
					new ServerEnvHostSpecificValue(
							new GetPasswordHostSpecificValue(cache, "transport.verizon.username", "Verizon API login"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.username", "Verizon API login"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.username", "Verizon API login"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.username", "Verizon API login"))
			);
			registerValue("USAT.transport.verizon.password",
					new ServerEnvHostSpecificValue(
							new GetPasswordHostSpecificValue(cache, "transport.verizon.password", "Verizon API password"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.password", "Verizon API password"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.password", "Verizon API password"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.password", "Verizon API password"))
			);
			registerValue("USAT.transport.verizon.appKey",
					new ServerEnvHostSpecificValue(
							new GetPasswordHostSpecificValue(cache, "transport.verizon.appKey", "Verizon API AppKey"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.appKey", "Verizon API AppKey"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.appKey", "Verizon API AppKey"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.appKey", "Verizon API AppKey"))
			);
			registerValue("USAT.transport.verizon.appSecret",
					new ServerEnvHostSpecificValue(
							new GetPasswordHostSpecificValue(cache, "transport.verizon.appSecret", "Verizon API AppSecret"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.appSecret", "Verizon API AppSecret"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.appSecret", "Verizon API AppSecret"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.appSecret", "Verizon API AppSecret"))
			);
			registerValue("USAT.transport.verizon.billingAccountName",
					new ServerEnvHostSpecificValue(
							new GetPasswordHostSpecificValue(cache, "transport.verizon.billingAccountName", "Verizon billing account name"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.billingAccountName", "Verizon billing account name"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.billingAccountName", "Verizon billing account name"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.billingAccountName", "Verizon billing account name"))
			);
			registerValue("USAT.transport.verizon.callbackUrl",
					new ServerEnvHostSpecificValue(
							new GetPasswordHostSpecificValue(cache, "transport.verizon.callbackUrl", "Verizon modem status callback URL"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.callbackUrl", "Verizon modem status callback URL"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.callbackUrl", "Verizon modem status callback URL"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.callbackUrl", "Verizon modem status callback URL"))
			);
			registerValue("USAT.transport.verizon.defaultServicePlan",
					new ServerEnvHostSpecificValue(
							new GetPasswordHostSpecificValue(cache, "transport.verizon.defaultServicePlan", "Default service plan for Verizon modem activation"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.defaultServicePlan", "Default service plan for Verizon modem activation"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.defaultServicePlan", "Default service plan for Verizon modem activation"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.defaultServicePlan", "Default service plan for Verizon modem activation"))
			);
			registerValue("USAT.transport.verizon.CDMAIPPool",
					new ServerEnvHostSpecificValue(
							new GetPasswordHostSpecificValue(cache, "transport.verizon.CDMAIPPool", "Verizon IP pool for CDMA devices"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.CDMAIPPool", "Verizon IP pool for CDMA devices"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.CDMAIPPool", "Verizon IP pool for CDMA devices"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.CDMAIPPool", "Verizon IP pool for CDMA devices"))
			);
			registerValue("USAT.transport.verizon.LTEIPPool",
					new ServerEnvHostSpecificValue(
							new GetPasswordHostSpecificValue(cache, "transport.verizon.LTEIPPool", "Verizon IP pool for LTE devices"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.LTEIPPool", "Verizon IP pool for LTE devices"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.LTEIPPool", "Verizon IP pool for LTE devices"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.LTEIPPool", "Verizon IP pool for LTE devices"))
			);
			registerValue("USAT.transport.verizon.location.username",
					new ServerEnvHostSpecificValue(
							new GetPasswordHostSpecificValue(cache, "transport.verizon.location.username", "Verizon API login for LBS"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.location.username", "Verizon API login for LBS"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.location.username", "Verizon API login for LBS"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.location.username", "Verizon API login for LBS"))
			);
			registerValue("USAT.transport.verizon.location.password",
					new ServerEnvHostSpecificValue(
							new GetPasswordHostSpecificValue(cache, "transport.verizon.location.password", "Verizon API password for LBS"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.location.password", "Verizon API password for LBS"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.location.password", "Verizon API password for LBS"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.location.password", "Verizon API password for LBS"))
			);
			registerValue("USAT.transport.verizon.location.appKey",
					new ServerEnvHostSpecificValue(
							new GetPasswordHostSpecificValue(cache, "transport.verizon.location.appKey", "Verizon API AppKey for LBS"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.location.appKey", "Verizon API AppKey for LBS"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.location.appKey", "Verizon API AppKey for LBS"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.location.appKey", "Verizon API AppKey for LBS"))
			);
			registerValue("USAT.transport.verizon.location.appSecret",
					new ServerEnvHostSpecificValue(
							new GetPasswordHostSpecificValue(cache, "transport.verizon.location.appSecret", "Verizon API AppSecret for LBS"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.location.appSecret", "Verizon API AppSecret for LBS"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.location.appSecret", "Verizon API AppSecret for LBS"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.location.appSecret", "Verizon API AppSecret for LBS"))
			);
			registerValue("USAT.transport.verizon.location.billingAccountName",
					new ServerEnvHostSpecificValue(
							new GetPasswordHostSpecificValue(cache, "transport.verizon.location.billingAccountName", "Verizon billing account name for LBS"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.location.billingAccountName", "Verizon billing account name for LBS"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.location.billingAccountName", "Verizon billing account name for LBS"),
							new GetPasswordHostSpecificValue(cache, "transport.verizon.location.billingAccountName", "Verizon billing account name for LBS"))
			);
			registerValue("USAT.transport.urbanAirShip.apiKey",
					new ServerEnvHostSpecificValue(
							new GetPasswordHostSpecificValue(cache, "transport.urbanAirShip.apiKey", "Urban AirShip API key"),
							new GetPasswordHostSpecificValue(cache, "transport.urbanAirShip.apiKey", "Urban AirShip API key"),
							new GetPasswordHostSpecificValue(cache, "transport.urbanAirShip.apiKey", "Urban AirShip API key"),
							new GetPasswordHostSpecificValue(cache, "transport.urbanAirShip.apiKey", "Urban AirShip API key"))
			);
			String urbanairshipStaging="https://staging-wallet-api.urbanairship.com/v1";
			String urbanairshipProd="https://wallet-api.urbanairship.com/v1";
			registerValue("USAT.passapi.url",new ServerEnvHostSpecificValue(urbanairshipStaging,urbanairshipStaging,urbanairshipStaging,urbanairshipProd));
			registerValue("USAT.passapi.username",
					new ServerEnvHostSpecificValue(
							"yhe@usatech.com",
							"yhe@usatech.com",
							"yhe@usatech.com",
							new GetPasswordHostSpecificValue(cache, "USAT.passapi.username", "Urbanairship pass api username"))
						);
			registerValue("USAT.passapi.password",
					new ServerEnvHostSpecificValue(
							new GetPasswordHostSpecificValue(cache, "USAT.passapi.password", "Urbanairship pass api password"),
							new GetPasswordHostSpecificValue(cache, "USAT.passapi.password", "Urbanairship pass api password"),
							new GetPasswordHostSpecificValue(cache, "USAT.passapi.password", "Urbanairship pass api password"),
							new GetPasswordHostSpecificValue(cache, "USAT.passapi.password", "Urbanairship pass api password"))
						);
		} else if("esuds".equalsIgnoreCase(app.getName())) {
			registerValue("USAT.website.esuds.jetty.port", new DefaultHostSpecificValue(String.valueOf(7080 + app.getPortOffset() + (ordinal == null ? 700 : ordinal * 100))));
			registerValue("USAT.database.OPER.password.ESUDS_OPERATOR_REPORT_USER", new ServerEnvHostSpecificValue("ESUDS_OPERATOR_REPORT_USER", "ESUDS_OPERATOR_REPORT_USER", new GetPasswordHostSpecificValue(cache, "database.OPER.ESUDS_OPERATOR_REPORT_USER", "ESUDS_OPERATOR_REPORT_USER for OPER"), new GetPasswordHostSpecificValue(cache, "database.OPER.ESUDS_OPERATOR_REPORT_USER", "ESUDS_OPERATOR_REPORT_USER for OPER")));
			registerValue("USAT.database.OPER.password.SECURITY_ADMIN", new ServerEnvHostSpecificValue("SECURITY_ADMIN", "SECURITY_ADMIN", new GetPasswordHostSpecificValue(cache, "database.OPER.SECURITY_ADMIN", "SECURITY_ADMIN for OPER"), new GetPasswordHostSpecificValue(cache, "database.OPER.SECURITY_ADMIN", "SECURITY_ADMIN for OPER")));
			registerValue("USAT.database.REPORT.password.USALIVE_APP", new ServerEnvHostSpecificValue(StringUtils.reverse("USALIVE_APP_1"), StringUtils.reverse("USALIVE_APP_1"), new GetPasswordHostSpecificValue(cache, "database.REPORT.USALIVE_APP", "USALIVE_APP for REPORT"), new GetPasswordHostSpecificValue(cache, "database.REPORT.USALIVE_APP", "USALIVE_APP for REPORT")));
			registerValue("USAT.database.REPORT.password.FOLIO_CONF", new ServerEnvHostSpecificValue("FOLIO_CONF", "FOLIO_CONF", new GetPasswordHostSpecificValue(cache, "database.REPORT.FOLIO_CONF", "FOLIO_CONF for REPORT"), new GetPasswordHostSpecificValue(cache, "database.REPORT.FOLIO_CONF", "FOLIO_CONF for REPORT")));
		} else if("usalive".equalsIgnoreCase(app.getName())) {
			registerValue("USAT.database.MSR_MQ.password.USALIVE", new ServerEnvHostSpecificValue("USALIVE_1", "USALIVE_1", new GetPasswordHostSpecificValue(cache, "database.MSR_MQ.USALIVE", "USALIVE for MSR_MQ"), new GetPasswordHostSpecificValue(cache, "database.MSR_MQ.USALIVE", "USALIVE for MSR_MQ")));
			registerValue("USAT.database.MSR_FILE_REPO.password.USALIVE", new ServerEnvHostSpecificValue("USALIVE_1", "USALIVE_1", new GetPasswordHostSpecificValue(cache, "database.MSR_FILE_REPO.USALIVE", "USALIVE for MSR_FILE_REPO"), new GetPasswordHostSpecificValue(cache, "database.MSR_FILE_REPO.USALIVE", "USALIVE for MSR_FILE_REPO")));
			registerValue("USAT.database.REPORT.password.USALIVE_APP", new ServerEnvHostSpecificValue(StringUtils.reverse("USALIVE_APP_1"), StringUtils.reverse("USALIVE_APP_1"), new GetPasswordHostSpecificValue(cache, "database.REPORT.USALIVE_APP", "USALIVE_APP for REPORT"), new GetPasswordHostSpecificValue(cache, "database.REPORT.USALIVE_APP", "USALIVE_APP for REPORT")));
			registerValue("USAT.database.MAIN.password.USALIVE", new ServerEnvHostSpecificValue("USALIVE_1", "USALIVE_1", new GetPasswordHostSpecificValue(cache, "database.MAIN.USALIVE", "USALIVE for MAIN"), new GetPasswordHostSpecificValue(cache, "database.MAIN.USALIVE", "USALIVE for MAIN")));
			registerValue("USAT.environment.suffix", new ServerEnvHostSpecificValue("-DEV", "-INT", "-ECC", ""));
			registerValue("USAT.jetty.port", new DefaultHostSpecificValue(String.valueOf(7080 + app.getPortOffset() + (ordinal == null ? 700 : ordinal * 100))));
			registerValue("USAT.jetty.home", new DefaultHostSpecificValue("."));
			registerValue("USAT.web.sessionTimeout", new ServerEnvHostSpecificValue("28800", "28800", "28800", "28800"));
			registerValue("USAT.loadbalancer.ip.pattern", new ServerEnvHostSpecificValue("10\\.0\\.0\\.64", "10\\.0\\.0\\.64", "192\\.168\\.4\\.65", "192\\.168\\.79\\.18[34]"));
			registerValue("USAT.email.convenienceFeeNotifications", new ServerEnvHostSpecificValue("SoftwareDevelopmentTeam@usatech.com", "QualityAssurance@usatech.com", "QualityAssurance@usatech.com", "ConvenienceFeeNotifications@usatech.com"));
			registerValue("USAT.email.orderNotifications", new ServerEnvHostSpecificValue("SoftwareDevelopmentTeam@usatech.com", "QualityAssurance@usatech.com", "QualityAssurance@usatech.com", "orderpayments@usatech.com"));
			MappedHostSpecificValue eportConnectUrl = new MappedHostSpecificValue();
			eportConnectUrl.registerValue("DEV", "APP", "https://ec-dev.usatech.com:9443/hessian/ec2");
			eportConnectUrl.registerValue("INT", "APP", "https://ec-int.usatech.com:9443/hessian/ec2");
			eportConnectUrl.registerValue("ECC", "APP", "https://ec-ecc.usatech.com:9443/hessian/ec2");
			eportConnectUrl.registerValue("USA", "APP", "https://ec.usatech.com:9443/hessian/ec2");
			registerValue("STATIC.com.usatech.ec2.EC2ClientUtils.eportConnectUrl", eportConnectUrl);
			registerValue("STATIC.com.usatech.usalive.report.TestTransportStep.testVDISoap", new DefaultHostSpecificValue("true"));
			registerValue("STATIC.com.usatech.layers.common.util.BackendPayorUtils.jspPageDirectory", new DefaultHostSpecificValue("/"));
			registerValue("STATIC.com.usatech.usalive.pepsi.PepsiRequestHandler.serviceEnabled", new ServerEnvHostSpecificValue("true", "true", "false", "false"));
			/*
			if(instanceNum==2){
				MappedHostSpecificValue rdwConfigFile = new MappedHostSpecificValue();
				rdwConfigFile.registerValue("INT", "APP", "com/usatech/usalive/report/BuildReportPillars-RDW.properties");
				rdwConfigFile.registerValue("ECC", "APP", "com/usatech/usalive/report/BuildReportPillars-RDW.properties");
				registerValue("STATIC.com.usatech.usalive.report.AbstractBuildActivityFolioStep.configFile", rdwConfigFile);
			}
			*/
			registerValue("STATIC.com.usatech.usalive.web.RMAUtils.rmaFromName", new ServerEnvHostSpecificValue("TEST DEV USA Technologies Inc", "TEST INT USA Technologies Inc", "TEST ECC USA Technologies Inc", "USA Technologies Inc"));
			registerValue("STATIC.com.usatech.usalive.web.RMAUtils.rmaToCustomerServiceEmail", new ServerEnvHostSpecificValue("SoftwareDevelopmentTeam@usatech.com", "QualityAssurance@usatech.com", "QualityAssurance@usatech.com", "RMA@usatech.com"));
			registerValue("jetty.handler.WebAppContext.tempDirectory", new DefaultHostSpecificValue("/opt/USAT/tmp/usalive_jetty_${USAT.jetty.port}"));
		} else if("prepaid".equalsIgnoreCase(app.getName())) {
			clientKeyAliasMap.registerAlias(9443, "-");
			registerValue("USAT.jetty.port", new DefaultHostSpecificValue(String.valueOf(7080 + app.getPortOffset() + (ordinal == null ? 700 : ordinal * 100))));
			registerValue("USAT.jetty.home", new DefaultHostSpecificValue("."));
			registerValue("USAT.web.sessionTimeout", new ServerEnvHostSpecificValue("28800", "28800", "28800", "28800"));
			registerValue("PrepaidWebAppContext.baseResource", new DefaultHostSpecificValue("${USAT.jetty.home}/web"));	
			registerValue("USAT.database.REPORT.password.PREPAID_APP", new ServerEnvHostSpecificValue("PREPAID_APP_1", "PREPAID_APP_1", new GetPasswordHostSpecificValue(cache, "database.REPORT.PREPAID_APP", "PREPAID_APP for REPORT"), new GetPasswordHostSpecificValue(cache, "database.REPORT.PREPAID_APP", "PREPAID_APP for REPORT")));
			MappedHostSpecificValue eportConnectUrl = new MappedHostSpecificValue();
			eportConnectUrl.registerValue("DEV", "APP", "https://ec-dev.usatech.com:9443/hessian/ec2");
			eportConnectUrl.registerValue("INT", "APP", "https://ec-int.usatech.com:9443/hessian/ec2");
			eportConnectUrl.registerValue("ECC", "APP", "https://ec-ecc.usatech.com:9443/hessian/ec2");
			eportConnectUrl.registerValue("USA", "APP", "https://ec.usatech.com:9443/hessian/ec2");
			registerValue("STATIC.com.usatech.ec2.EC2ClientUtils.eportConnectUrl", eportConnectUrl);
			registerValue("STATIC.com.usatech.prepaid.web.PrepaidUtils.usaliveRootUrl", new ServerEnvHostSpecificValue("https://usalive-dev.usatech.com/", "https://usalive-int.usatech.com/", "https://usalive-ecc.usatech.com/", "https://usalive.usatech.com/"));
			registerValue("PrepaidWebAppContext.tempDirectory", new DefaultHostSpecificValue("/opt/USAT/tmp/prepaid_jetty_${USAT.jetty.port}"));
			String noopHostnameVerifier="org.apache.http.conn.ssl.NoopHostnameVerifier";
			registerValue("javax.net.ssl.HostnameVerifier.class", new ServerEnvHostSpecificValue(noopHostnameVerifier, noopHostnameVerifier, noopHostnameVerifier,(String)null));
			String noopHostnameVerifierInstance="\"javax.net.ssl.HostnameVerifier\"";
			registerValue("STATIC.com.usatech.layers.common.sprout.SproutUtils.hostNameVerifier", new ServerEnvHostSpecificValue(noopHostnameVerifierInstance, noopHostnameVerifierInstance, noopHostnameVerifierInstance,(String)null));			
			String testSproutUrl = "https://account-cert.bytevampire.com";
			registerValue("STATIC.com.usatech.layers.common.sprout.SproutUtils.sproutUrl", new ServerEnvHostSpecificValue(testSproutUrl, testSproutUrl, testSproutUrl, "https://api.bytevampire.com"));
			String testSproutAuthorizationToken = "cbf35bb7eb1fe7eacfbad3ffe313eebadc00d35a615ce372306bd8a6a0fab6df";
			registerValue("STATIC.com.usatech.layers.common.sprout.SproutUtils.sproutAuthorizationToken", new ServerEnvHostSpecificValue(testSproutAuthorizationToken, testSproutAuthorizationToken, testSproutAuthorizationToken, new GetPasswordHostSpecificValue(cache, "Sprout prod auth token", "Sprout prod auth token")));
			registerValue("USAT.database.MSR_MQ.password.RPTREQ", new ServerEnvHostSpecificValue("RPTREQ_1", "RPTREQ_1", new GetPasswordHostSpecificValue(cache, "database.MSR_MQ.RPTREQ", "RPTREQ for MSR_MQ"), new GetPasswordHostSpecificValue(cache, "database.MSR_MQ.RPTREQ", "RPTREQ for MSR_MQ")));
			registerValue("USAT.prepaid.passDemoUrl", new ServerEnvHostSpecificValue("", "https://wallet-api.urbanairship.com/v1/pass/dynamic/3fdc21ea-027f-43bd-a96a-948d126cccef", "", ""));
		}else if("keymgr".equalsIgnoreCase(app.getName())) {
			registerValue("USAT.database.MQ.password.KEYMGR", new ServerEnvHostSpecificValue("KEYMGR_1", "KEYMGR_1", new GetPasswordHostSpecificValue(cache, "database.MQ.KEYMGR", "KEYMGR for MQ"), new GetPasswordHostSpecificValue(cache, "database.MQ.KEYMGR", "KEYMGR for MQ")));
			registerValue("USAT.database.FILE_REPO.password.KEYMGR", new ServerEnvHostSpecificValue("KEYMGR_1", "KEYMGR_1", new GetPasswordHostSpecificValue(cache, "database.FILE_REPO.KEYMGR", "KEYMGR for FILE_REPO"), new GetPasswordHostSpecificValue(cache, "database.FILE_REPO.KEYMGR", "KEYMGR for FILE_REPO")));
			registerValue("USAT.database.KM.password.KEYMGR", new ServerEnvHostSpecificValue("KEYMGR", "KEYMGR", new GetPasswordHostSpecificValue(cache, "database.KM.KEYMGR", "KEYMGR for KM"), new GetPasswordHostSpecificValue(cache, "database.KM.KEYMGR", "KEYMGR for KM")));
			registerValue("USAT.database.KM.url", new DefaultHostSpecificValue("jdbc:postgresql://localhost:5432/km?ssl=true&connectTimeout=5&tcpKeepAlive=true"));
			StringBuilder otherInstances = new StringBuilder();
			otherInstances.append(1);
			for(int i = 2; i <= instanceCount; i++)
				otherInstances.append(',').append(i);
			registerValue("USAT.keymgr.otherInstances", new DefaultHostSpecificValue(otherInstances.toString()));
			registerValue("USAT.jetty.port", new DefaultHostSpecificValue(String.valueOf(7080 + app.getPortOffset() + (ordinal == null ? 700 : ordinal * 100))));
			registerValue("USAT.jetty.sslport", new DefaultHostSpecificValue(String.valueOf(7043 + app.getPortOffset() + (ordinal == null ? 700 : ordinal * 100))));
			registerValue("USAT.jetty.redirect.sslport", new DefaultHostSpecificValue(String.valueOf(ordinal == null ? 443 : 7043 + app.getPortOffset() + ordinal * 100)));
			registerValue("USAT.jetty.home", new DefaultHostSpecificValue("."));
			registerValue("USAT.web.sessionTimeout", new ServerEnvHostSpecificValue("3600", "3600", "3600", "3600"));
			registerValue("USAT.keymanager.webTrustStore", new DefaultHostSpecificValue("../conf/webtruststore.ts"));
			registerValue("USAT.keymanager.webTrustStorePassword", new GetPasswordHostSpecificValue(cache, "truststore", "Trust Store"));			
			registerValue("USAT.keyManager.emvEnabled", new ServerEnvHostSpecificValue("true", "true", "true", "false"));

			registerValue("USAT.keyManager.cardReaderTypeKeyId[1]", new ServerEnvHostSpecificValue("1221661629429", "1221661629429", "1221661629429", "1222874644089"));
			registerValue("USAT.keyManager.cardReaderTypeKeyId[2]", new ServerEnvHostSpecificValue("1221661629429", "1221661629429", "1221661629429", "1222874644089"));
			registerValue("USAT.keyManager.cardReaderTypeKeyId[3]", new DefaultHostSpecificValue("3"));
			registerValue("USAT.keyManager.cardReaderTypeKeyId[4]", new DefaultHostSpecificValue("4"));
			registerValue("USAT.keyManager.cardReaderTypeKeyId[5]", new DefaultHostSpecificValue("5"));
			registerValue("USAT.keyManager.cardReaderTypeKeyId[6]", new DefaultHostSpecificValue("6"));
			registerValue("USAT.keyManager.cardReaderTypeKeyId[8]", new DefaultHostSpecificValue("8"));
			registerValue("USAT.keyManager.cardReaderTypeKeyId[9]", new DefaultHostSpecificValue("9"));
			registerValue("USAT.keyManager.cardReaderTypeKeyId[10]", new DefaultHostSpecificValue("9"));
			registerValue("USAT.keyManager.cardReaderTypeKeyId[11]", new DefaultHostSpecificValue("9"));
			registerValue("USAT.keyManager.cardReaderTypeKeyId[12]", new DefaultHostSpecificValue("9"));
			registerValue("USAT.keyManager.cardReaderTypeKeyId[13]", new DefaultHostSpecificValue("13"));
			registerValue("USAT.keyManager.cardReaderTypeKeyId[14]", new DefaultHostSpecificValue("9"));
			registerValue("USAT.keyManager.cardReaderTypeKeyId[15]", new DefaultHostSpecificValue("9"));
			registerValue("USAT.keyManager.cardReaderTypeKeyId[16]", new DefaultHostSpecificValue("9"));
			registerValue("USAT.keyManager.cardReaderTypeKeyId[17]", new DefaultHostSpecificValue("17"));
			registerValue("USAT.keyManager.cardReaderTypeKeyId[18]", new DefaultHostSpecificValue("9"));

			registerValue("USAT.ssss.threshold", new ServerEnvHostSpecificValue("3", "3", "3", "3"));
			registerValue("USAT.ssss.shares", new ServerEnvHostSpecificValue("7", "7", "7", "7"));
			registerValue("USAT.keymanager.admin.email", new ServerEnvHostSpecificValue("SoftwareDevelopmentTeam@usatech.com", "SoftwareDevelopmentTeam@usatech.com", "KeyManagerSAs@usatech.com", "KeyManagerSAs@usatech.com"));
			registerValue("jetty.handler.WebAppContext.tempDirectory", new DefaultHostSpecificValue("/opt/USAT/tmp/keymgr_jetty_${USAT.jetty.port}"));
			registerValue("jetty.connector.SslNioConnector.sslContextFactory.includeProtocols", new DefaultHostSpecificValue("TLSv1.2"));
		} else if(USATRegistry.APP_MONITOR.equals(app)) {
			registerValue("USAT.database.OPER.password.MONITOR", new ServerEnvHostSpecificValue("MONITOR", "MONITOR", new GetPasswordHostSpecificValue(cache, "database.OPER.MONITOR", "MONITOR for OPER"), new GetPasswordHostSpecificValue(cache, "database.OPER.MONITOR", "MONITOR for OPER")));
		} else if ("logLayer".equalsIgnoreCase(app.getName())) {
			
			registerValue("USAT.database.LOGS.password.LOGS_USER",
					new ServerEnvHostSpecificValue("LOGS_USER_1", "LOGS_USER_1",
							new GetPasswordHostSpecificValue(cache, "database.LOGS.LOGS_USER", "LOGS_USER for LOGS"),
							new GetPasswordHostSpecificValue(cache, "database.LOGS.LOGS_USER", "LOGS_USER for LOGS")));

			if ((app.getAppDesc().contains("DEV"))) {
				registerValue("USAT.database.LOGS.url",
						new DefaultHostSpecificValue(UsatSettingUpdateFileTask.LOGS_DEV));
				registerValue("USAT.logsLayer.agentHosts",
						new DefaultHostSpecificValue("devapr11:9501,devnet11:9501,devkls11:9501,devapr12:9501,devnet12:9501,devkls12:9501"));
			}
			if ((app.getAppDesc().contains("INT"))) {
				registerValue("USAT.database.LOGS.url",
						new DefaultHostSpecificValue(UsatSettingUpdateFileTask.LOGS_INT));
				registerValue("USAT.logsLayer.agentHosts",
						new DefaultHostSpecificValue("intapr11:9501,intnet11:9501,intkls11:9501,intapr12:9501,intnet12:9501,intkls12:9501"));
			}
			if ((app.getAppDesc().contains("ECC"))) {
				registerValue("USAT.database.LOGS.url",
						new DefaultHostSpecificValue(UsatSettingUpdateFileTask.LOGS_ECC));
				registerValue("USAT.logsLayer.agentHosts",
						new DefaultHostSpecificValue("eccapr11:9501,eccnet11:9501,ecckls11:9501,eccapr12:9501,eccnet12:9501,ecckls12:9501"));
			}
			if ((app.getAppDesc().contains("USA"))) {
				registerValue("USAT.database.LOGS.url",
						new DefaultHostSpecificValue(UsatSettingUpdateFileTask.LOGS_PROD));
				registerValue("USAT.logsLayer.agentHosts",
						new DefaultHostSpecificValue("usanet31:9501,usanet32:9501,usanet33:9501,usanet34:9501,"
								+ "usaapr31:9501,usaapr32:9501,usaapr33:9501,usaapr34:9501,"
								+ "usakls31:9501,usakls32:9501,usakls33:9501,usakls34:9501,"
								+ "usaweb31:9501,usaweb32:9501,usaweb33:9501,usaweb34:9501,"
								+ "usaapp31:9501,usaapp32:9501,usaapp33:9501,usaapp34:9501 "));
			}
			registerValue("USAT.logsLayer.inputLogsFolder", new DefaultHostSpecificValue("inputlogs"));
			registerValue("USAT.logsLayer.copyFolder", new DefaultHostSpecificValue("inputlogs/copies"));
			registerValue("USAT.logsLayer.hoursInPartition", new ServerEnvHostSpecificValue("24","24","24", "6"));
		} else if (USATRegistry.LOGS_AGENT_APR_APP.equals(app)) {
			registerValue("USAT.logsAgent.port", new DefaultHostSpecificValue("9501"));
			registerValue("USAT.logsAgent.appLogsFolder", new ServerEnvHostSpecificValue(
					"dms,usalive,applayer1,applayer2,inauthlayer1,inauthlayer2,loader1,loader2,posmlayer1,posmlayer2,prepaid,rptgen1,rptgen2,rptreq1,rptreq2",
					"dms,usalive,applayer1,applayer2,inauthlayer1,inauthlayer2,loader1,loader2,posmlayer1,posmlayer2,prepaid,rptgen1,rptgen2,rptreq1,rptreq2",
					"dms,usalive,applayer1,applayer2,inauthlayer1,inauthlayer2,loader1,loader2,posmlayer1,posmlayer2,prepaid,rptgen1,rptgen2,rptreq1,rptreq2",
					"dms,applayer,inauthlayer,loader,posmlayer"));
		} else if (USATRegistry.LOGS_AGENT_APP.equals(app)) {
			registerValue("USAT.logsAgent.port", new DefaultHostSpecificValue("9501"));
			registerValue("USAT.logsAgent.appLogsFolder", new DefaultHostSpecificValue("usalive,prepaid,rptgen,rptreq"));
		} else if (USATRegistry.LOGS_AGENT_WEB.equals(app)) {
			registerValue("USAT.logsAgent.port", new DefaultHostSpecificValue("9501"));
			registerValue("USAT.logsAgent.appLogsFolder", new DefaultHostSpecificValue("transport"));	
		} else if (USATRegistry.LOGS_AGENT_NET_APP.equals(app)) {
			registerValue("USAT.logsAgent.port", new DefaultHostSpecificValue("9501"));
			registerValue("USAT.logsAgent.appLogsFolder", new ServerEnvHostSpecificValue(
					"netlayer1,netlayer2,outauthlayer1,outauthlayer2,transport1,transport2",
					"netlayer1,netlayer2,outauthlayer1,outauthlayer2,transport1,transport2",
					"netlayer1,netlayer2,outauthlayer1,outauthlayer2,transport1,transport2",
					"netlayer,outauthlayer"));
		} else if (USATRegistry.LOGS_AGENT_KLS_APP.equals(app)) {
			registerValue("USAT.logsAgent.port", new DefaultHostSpecificValue("9501"));
			registerValue("USAT.logsAgent.appLogsFolder", new DefaultHostSpecificValue("keymgr"));
		} else if (USATRegistry.ORAEXPORT.equals(app)) {
			registerValue("USAT.database.Export.username",
					new ServerEnvHostSpecificValue("system", "system",
							new GetPasswordHostSpecificValue(cache, "database.Export.username", "Export username"),
							new GetPasswordHostSpecificValue(cache, "database.Export.username", "Export username")));
			registerValue("USAT.database.Export.password",
					new ServerEnvHostSpecificValue("usatmanager", "usatmanager",
							new GetPasswordHostSpecificValue(cache, "database.Export.password", "Export password"),
							new GetPasswordHostSpecificValue(cache, "database.Export.password", "Export password")));
			registerValue("USAT.database.Import.username",
					new ServerEnvHostSpecificValue("admin_1", "admin_1",
							new GetPasswordHostSpecificValue(cache, "database.Import.username", "Import username"),
							new GetPasswordHostSpecificValue(cache, "database.Import.username", "Import username")));
			registerValue("USAT.database.Import.password",
					new ServerEnvHostSpecificValue("ADMIN_1", "ADMIN_1",
							new GetPasswordHostSpecificValue(cache, "database.Import.password", "Import password"),
							new GetPasswordHostSpecificValue(cache, "database.Import.password", "Import password")));
			registerValue("USAT.Export.copyFolder", new DefaultHostSpecificValue("/copies"));
		}
		if("logAgent".equalsIgnoreCase(app.getName())){
			registerValue("USAT.logsAgent.daysAgoLogs",new ServerEnvHostSpecificValue("1", "1", "1", "1"));
		}
		
		int mqCount = 0;
		registerValue("USAT.app.instance", new DefaultHostSpecificValue(String.valueOf(instanceNum)));
		String mqPrefix = null;
		if(serverSet.getServers("MST") != null && CollectionUtils.isIn(app.getName(), "netlayer", "outauthlayer", "keymgr", "applayer", "inauthlayer", "posmlayer", "loader", "dms")) {
			// tran processing queue
			mqPrefix="MQ_";
			for(Server server : serverSet.getServers("MST"))
				mqCount += server.instances.length;
			registerValue("USAT.simplemq.primaryDataSource", new DefaultHostSpecificValue(mqPrefix + String.valueOf(((instanceNum - 1) % mqCount) + 1)));
			if(USATRegistry.KEYMGR.equals(app))
				registerValue("USAT.simplemq.secondaryDataSources", new DefaultHostSpecificValue(StringUtils.generateReverseList(mqPrefix, null, ",", instanceNum - 1, mqCount - 1, 1, mqCount), true /*unsplit*/));
			else
				registerValue("USAT.simplemq.secondaryDataSources", new DefaultHostSpecificValue(StringUtils.generateList(mqPrefix, null, ",", instanceNum + 1, mqCount - 1, 1, mqCount), unsplit));
			registerValue("USAT.simplemq.direct." + app.getName() + ".port", new DefaultHostSpecificValue(String.valueOf(getDirectPort(app, ordinal))));
		}
		mqCount = 0;
		if(serverSet.getServers("MSR") != null && CollectionUtils.isIn(app.getName(), "transport", "usalive", "rptgen", "rptreq", "loader", "rdwloader", "prepaid")) {
			// report processing queue
			for(Server server : serverSet.getServers("MSR"))
				mqCount += server.instances.length;
			if("loader".equals(app.getName()))
				mqPrefix = "MSR_MQ_";
			else
				mqPrefix = "MQ_";
			registerValue("USAT.simplemq.msrPrimaryDataSource", new DefaultHostSpecificValue(mqPrefix + String.valueOf(((instanceNum - 1) % mqCount) + 1)));
			registerValue("USAT.simplemq.msrSecondaryDataSources", new DefaultHostSpecificValue(StringUtils.generateList(mqPrefix, null, ",", instanceNum + 1, mqCount - 1, 1, mqCount), unsplit));
			registerValue("USAT.simplemq.direct." + app.getName() + ".port", new DefaultHostSpecificValue(String.valueOf(getDirectPort(app, ordinal))));
			//@Todo Ying check whether USAT.simplemq.msrClusterName should always be MSR
			registerValue("USAT.simplemq.msrClusterName", new DefaultHostSpecificValue("MSR"));
		}
		
		StringBuilder sb = new StringBuilder();
		for(App remoteApp : app.getConsumeQueueLayerFrom()) {
			List<Server> servers = serverSet.getServers(remoteApp.getServerType());
			final int total = remoteApp.getInstanceCount(servers);
			if(total == 0)
				continue;
			int i = 0;
			int p = 0;
			final int start;
			if(instanceCount == total)
				start = instanceNum;
			else if(total > instanceCount && (total % instanceCount) == 0)
				start = (instanceNum - 1) * (total / instanceCount) + 1;
			else if(total < instanceCount && (instanceCount % total) == 0)
				start = (int) Math.ceil((double) instanceNum * total / instanceCount);
			else
				start = instanceNum;
			for(Server server : servers) {
				for(String inst : remoteApp.filterInstances(server.instances)) {
					if(++p >= start) {
						sb.setLength(0);
						sb.append("USAT.simplemq.directPollerUrl.").append(remoteApp.getName().toLowerCase()).append('_');
						if(i == 0)
							sb.append('p');
						else
							sb.append('s').append(i);
						Integer ord = (inst == null ? null : Integer.parseInt(inst));
						final int port = getDirectPort(remoteApp, ord);
						String propertyName = sb.toString();
						sb.setLength(0);
						sb.append(server.host.getFullName()).append(':').append(port);
						registerValue(propertyName, new DefaultHostSpecificValue(sb.toString(), i == 0 || unsplit));
						i++;
					}
				}
			}
			p = 0;
			OUTER: for(Server server : servers) {
				for(String inst : remoteApp.filterInstances(server.instances)) {
					if(++p >= start)
						break OUTER;
					sb.setLength(0);
					sb.append("USAT.simplemq.directPollerUrl.").append(remoteApp.getName().toLowerCase()).append('_');
					if(i == 0)
						sb.append('p');
					else
						sb.append('s').append(i);
					Integer ord = (inst == null ? null : Integer.parseInt(inst));
					final int port = getDirectPort(remoteApp, ord);
					String propertyName = sb.toString();
					sb.setLength(0);
					sb.append(server.host.getFullName()).append(':').append(port);
					registerValue(propertyName, new DefaultHostSpecificValue(sb.toString(), i == 0 || unsplit));
					i++;
				}
			}
			for(; i < 4; i++) {
				sb.setLength(0);
				sb.append("USAT.simplemq.directPollerUrl.").append(remoteApp.getName().toLowerCase()).append('_');
				if(i == 0)
					sb.append('p');
				else
					sb.append('s').append(i);
				String propertyName = sb.toString();
				registerValue(propertyName, new DefaultHostSpecificValue("", i == 0 || unsplit));
			}
		}

		registerValue("include", new DefaultHostSpecificValue("USAT_environment_settings.properties"));	
	}

	protected int getDirectPort(App app, Integer ordinal) {
		return getDirectPort(app.getPortOffset(), ordinal);
	}

	protected int getDirectPort(int appPortOffset, Integer ordinal) {
		return 7007 + appPortOffset + (ordinal == null ? 7 : ordinal) * 100;
	}
}
