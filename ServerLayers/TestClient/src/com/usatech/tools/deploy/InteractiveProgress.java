package com.usatech.tools.deploy;

import simple.event.ProgressEvent;
import simple.event.ProgressListener;
import simple.io.Interaction;

import com.sshtools.j2ssh.FileTransferProgress;

public class InteractiveProgress implements FileTransferProgress {
	protected final ProgressListener listener;
	protected final ProgressEvent event = new ProgressEvent(0, null, "Progress");

	public InteractiveProgress(Interaction interaction) {
		listener = interaction.createProgressMeter();
	}

	public void completed() {
		listener.progressStart(event);
	}

	public boolean isCancelled() {
		return false;
	}

	public void progressed(long bytesSoFar) {
		event.setStatusProgress((int) bytesSoFar);
		listener.progressUpdate(event);
	}

	public void started(long bytesTotal, String remoteFile) {
		listener.progressStart(event);
	}

	public Float getStatusPercent() {
		return event.getStatusPercent();
	}

	public void setStatusPercent(Float statusPercent) {
		event.setStatusPercent(statusPercent);
	}

	public String getStatusDescription() {
		return event.getStatusDescription();
	}

	public void setStatusDescription(String statusDescription) {
		event.setStatusDescription(statusDescription);
	}
}
