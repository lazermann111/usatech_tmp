package com.usatech.tools.deploy;

public class SingleServerTypeHostSpecificValue extends MappedHostSpecificValue {
	public SingleServerTypeHostSpecificValue(String serverType, String value) {
		registerValue(null, serverType,value);	
	}
	public SingleServerTypeHostSpecificValue(String serverType, HostSpecificValue value) {
		registerValue(null, serverType,value);	
	}
}
