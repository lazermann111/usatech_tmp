package com.usatech.tools.deploy;

import java.io.File;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import simple.util.CaseInsensitiveHashMap;
import simple.util.CompositeCollection;

public class USATRegistry implements Registry {
	protected final static Set<App> possibleApps = new HashSet<App>();
	protected final static Set<App> possibleAppsUnmod = Collections.unmodifiableSet(possibleApps);
	public static final File baseDir = new File(System.getProperty("rootSourceDir", "D:\\Development\\Java Projects"));
	public static final File buildsDir = new File(baseDir, "ApplicationBuilds");
	public static final App APP_LAYER = new USATApp("applayer", "APR", "ApplicationBuilds/AppLayer", "applayer-", 50, 910, "App Layer", "AppLayerService", "app_layer");
	public static final App DMS_APP = new USATApp("dms", "APR", "ApplicationBuilds/DMS", "dms-", 1000, 950, "DMS", "DMS", "dms");
	public static final App LOGS_AGENT_NET_APP = new USATApp("logAgent", "NET", "ApplicationBuilds/LogLayer", "logagent-", 2200, 962, "Log Agent", "LogAgentService", "log_agent");
	public static final App LOGS_AGENT_APR_APP = new USATApp("logAgent", "APR", "ApplicationBuilds/LogLayer", "logagent-", 2200, 962, "Log Agent", "LogAgentService", "log_agent");
	public static final App LOGS_AGENT_KLS_APP = new USATApp("logAgent", "KLS", "ApplicationBuilds/LogLayer", "logagent-", 2200, 962, "Log Agent", "LogAgentService", "log_agent");
	public static final App LOGS_AGENT_APP = new USATApp("logAgent", "APP", "ApplicationBuilds/LogLayer", "logagent-", 2200, 962, "Log Agent", "LogAgentService", "log_agent");
	public static final App LOGS_AGENT_WEB = new USATApp("logAgent", "WEB", "ApplicationBuilds/LogLayer", "logagent-", 2200, 962, "Log Agent", "LogAgentService", "log_agent");
	public static final App ENERGYMISERS_WEBSITE_APP = new USATApp("energymisers", "WEB", "ApplicationBuilds/energymisers", "energymisers-", 1400, 954, "energymisers", "energymisers", "energymisers");
	public static final App ESUDS_APP = new USATApp("esuds", "APP", "ApplicationBuilds/eSudsWebsite", "esuds-", 1200, 952, "eSuds", "eSuds", "esuds");
	public static final App HOTCHOICE_APP = new USATApp("hotchoice", "WEB", "ApplicationBuilds/USALive", "hotchoice-", 1100, 951, "USALive", "hotchoice", "usalive");
	public static final App HTTPD_NET = new USATApp("httpd", "NET", "server_app_config/WEB/Httpd/linux", "httpd-", 0, 1 /*bin*/, "httpd", "httpd", null, "bin");
	public static final App HTTPD_WEB = new USATApp("httpd", "WEB", "server_app_config/WEB/Httpd/linux", "httpd-", 0, 1 /*bin*/, "httpd", "httpd", null, "bin");
	public static final App HTTPD_MON = new USATApp("httpd", "MON", "server_app_config/WEB/Httpd/linux", "httpd-", 0, 1 /*bin*/, "httpd", "httpd", null, "bin");
	public static final App INAUTH_LAYER = new USATApp("inauthlayer", "APR", "ApplicationBuilds/AuthorityLayer", "authoritylayer-inside-", 70, 903, "Inside Authority Layer", "InsideAuthorityLayerService", "auth_layer");
	public static final App KEYMGR = new USATApp("keymgr", "KLS", "ApplicationBuilds/KeyManager", "keymgr-", 900, 954, "Key Manager Service", "KeyManagerService", "keymgr");
	public static final App LOADER = new USATApp("loader", "APR", "ApplicationBuilds/AppLayer", "loader-", 60, 909, "Loader", "LoaderService", "loader", "loader");
	public static final App RDW_LOADER = new USATApp("rdwloader", "APP", "ApplicationBuilds/ReportGenerator", "rdwloader-", 90, 912, "RDW Loader", "RDWLoaderService", "rdwloader", "rdwloader");
	public static final App NET_LAYER = new USATApp("netlayer", "NET", "ApplicationBuilds/NetLayer", "netlayer-", 40, 902, "Network Layer", "NetLayerService", "net_layer");
	public static final App ORAEXPORT = new USATApp("oraexport", "LOGS", "ApplicationBuilds/Oraexport", "oraexport-", 2600, 962, "Oracle Export Service", "OraExportService", "oraexport");
	public static final App OUTAUTH_LAYER = new USATApp("outauthlayer", "NET", "ApplicationBuilds/AuthorityLayer", "authoritylayer-outside-", 80, 903, "Outside Authority Layer", "OutsideAuthorityLayerService", "auth_layer");
	public static final App POSM_LAYER = new USATApp("posmlayer", "APR", "ApplicationBuilds/POSMLayer", "posmlayer-", 0, 918, "POSM Layer", "POSMLayerService", "posm_layer");
	public static final App POSTGRES_MON = new USATApp("postgres", "MON", null, null, 5432, 90, "PostgreSQL", "postgresql", "postgres");
	public static final App POSTGRES_MST = new USATApp("postgres", "MST", null, null, 5432, 90, "PostgreSQL", "postgresql", "postgres");
	public static final App POSTGRES_KLS = new USATApp("postgres", "KLS", null, null, 5432, 90, "PostgreSQL", "postgresql", "postgres");
	public static final App POSTGRES_MSR = new USATApp("postgres", "MSR", null, null, 5432, 90, "PostgreSQL", "postgresql", "postgres");
	public static final App POSTGRES_PGS = new USATApp("postgres", "PGS", null, null, 5432, 90, "PostgreSQL", "postgresql", "postgres");
	public static final App POSTGRES_LOG = new USATApp("postgres", "LOGS", null, null, 5432, 90, "PostgreSQL", "postgresql", "postgres");
	public static final App RPTGEN_LAYER = new USATApp("rptgen", "APP", "ApplicationBuilds/ReportGenerator", "report-generator-", 10, 921, "Report Generator", "ReportGeneratorService", "rptgen", "rptgen");
	public static final App RPTREQ_LAYER = new USATApp("rptreq", "APP", "ApplicationBuilds/ReportGenerator", "report-requester-", 30, 923, "Report Requester", "ReportRequesterService", "rptreq", "rptreq");
	public static final Map<String,String> subHostPrefixMap=new HashMap<String,String>();
	public static final App TRANSPORT_LAYER = new USATApp("transport", "WEB", "ApplicationBuilds/ReportGenerator", "transporter-", 20, 922, "Report Transporter", "TransportService", "transport", "transport");
	public static final App USALIVE_APP = new USATApp("usalive", "APP", "ApplicationBuilds/USALive", "usalive-", 1100, 951, "USALive", "USALive", "usalive");
	public static final App PREPAID_APP = new USATApp("prepaid", "APP", "ApplicationBuilds/Prepaid", "prepaid-", 800, 955, "Prepaid", "Prepaid", "prepaid");
	public static final App GETMORE_APP = new USATApp("getmore", "WEB", null, null, 0, 0, "Prepaid", null, null);
	public static final App USATECH_WEBSITE_APP = new USATApp("usatech", "WEB", "ApplicationBuilds/usatech", "usatech-", 1300, 953, "usatech", "usatech", "usatech");
	public static final App VERIZON_APP = new USATApp("verizon", "WEB", "ApplicationBuilds/USALive", "verizon-", 1100, 951, "USALive", "verizon", "usalive");
	public static final App APP_MONITOR = new USATApp("appmonitor", "MON", "ApplicationBuilds/Tools", "appmonitor-", 0, 930, "App Monitor", "AppMonitor", "monitor", "monitor");
	public static final App JSTATD = new USATApp("jstatd", null, "ApplicationBuilds/Tools", "jstatd-", 0, 0, "jstatd", "jstatd", null, "root");
	
	public static final App LOGS_LAYER_DEV = new USATLogApp("logLayer", "LOGS", "ApplicationBuilds/LogLayer", "loglayer-", 2100, 961, "Log Layer DEV", "LogLayerService", "log_layer");
	public static final App LOGS_LAYER_INT = new USATLogApp("logLayer", "LOGS", "ApplicationBuilds/LogLayer", "loglayer-", 2400, 963, "Log Layer INT", "LogLayerService", "log_layer");
	public static final App LOGS_LAYER_ECC = new USATLogApp("logLayer", "LOGS", "ApplicationBuilds/LogLayer", "loglayer-", 2500, 964, "Log Layer ECC", "LogLayerService", "log_layer");
	public static final App LOGS_LAYER_USA = new USATLogApp("logLayer", "LOGS", "ApplicationBuilds/LogLayer", "loglayer-", 2500, 964, "Log Layer USA", "LogLayerService", "log_layer");
	

	protected static class USATApp extends App {
		public USATApp(String name, String serverType, String appBuildDir, String appBuildPrefix, int portOffset, int appUid, String appDesc, String serviceName, String dbUserName, String userName, String... produceQueueLayerTo) {
			super(name, serverType, appBuildDir, appBuildPrefix, portOffset, appUid, appDesc, serviceName, dbUserName, userName, produceQueueLayerTo);
			possibleApps.add(this);
		}

		public USATApp(String name, String serverType, String appBuildDir, String appBuildPrefix, int portOffset, int appUid, String appDesc, String serviceName, String dbUserName) {
			super(name, serverType, appBuildDir, appBuildPrefix, portOffset, appUid, appDesc, serviceName, dbUserName);
			possibleApps.add(this);
		}

	}
	
	protected static class USATLogApp extends App {
		public USATLogApp(String name, String serverType, String appBuildDir, String appBuildPrefix, int portOffset, int appUid, String appDesc, String serviceName, String dbUserName, String userName, String... produceQueueLayerTo) {
			super(name, serverType, appBuildDir, appBuildPrefix, portOffset, appUid, appDesc, serviceName, dbUserName, userName, produceQueueLayerTo);
			possibleApps.add(this);
		}

		public USATLogApp(String name, String serverType, String appBuildDir, String appBuildPrefix, int portOffset, int appUid, String appDesc, String serviceName, String dbUserName) {
			super(name, serverType, appBuildDir, appBuildPrefix, portOffset, appUid, appDesc, serviceName, dbUserName);
			possibleApps.add(this);
		}
		
		@Override
		public int hashCode() {
			return appDesc  == null ? 0 : appDesc.hashCode();
			
		}

	}

	protected final Map<App, String> apps = new HashMap<App, String>();
	protected final Map<String, String> appVersionMap = new CaseInsensitiveHashMap<String>();
	protected final Map<String, BasicServerSet> serverSets = new CaseInsensitiveHashMap<BasicServerSet>();
	public static String USALIVE_CONTEXT_PATH = "";
	
	public static final int REPLICA_LOCAL_USER_UID=956;

	static {
		APP_LAYER.registerConsumeQueueLayerFrom(NET_LAYER, KEYMGR);
		APP_LAYER.registerProduceQueueLayerTo("APR", "NET", "KLS");
		LOADER.registerConsumeQueueLayerFrom(NET_LAYER, KEYMGR);
		INAUTH_LAYER.registerConsumeQueueLayerFrom(APP_LAYER, POSM_LAYER, KEYMGR, INAUTH_LAYER, OUTAUTH_LAYER);
		INAUTH_LAYER.registerProduceQueueLayerTo("APR", "NET", "KLS");
		POSM_LAYER.registerConsumeQueueLayerFrom(POSM_LAYER, INAUTH_LAYER, OUTAUTH_LAYER, LOADER, DMS_APP);
		POSM_LAYER.registerProduceQueueLayerTo("APR", "NET", "KLS");
		NET_LAYER.registerConsumeQueueLayerFrom(OUTAUTH_LAYER, APP_LAYER, INAUTH_LAYER, KEYMGR);
		NET_LAYER.registerProduceQueueLayerTo("APR", "KLS");
		OUTAUTH_LAYER.registerConsumeQueueLayerFrom(APP_LAYER, POSM_LAYER, KEYMGR, INAUTH_LAYER, OUTAUTH_LAYER);
		OUTAUTH_LAYER.registerProduceQueueLayerTo("APR", "NET", "KLS");
		KEYMGR.registerConsumeQueueLayerFrom(NET_LAYER, POSM_LAYER, KEYMGR, DMS_APP, INAUTH_LAYER, OUTAUTH_LAYER, APP_LAYER, PREPAID_APP);
		KEYMGR.registerProduceQueueLayerTo("APR", "NET", "KLS");
	
		USALIVE_APP.registerConsumeQueueLayerFrom(RPTGEN_LAYER, TRANSPORT_LAYER);
		USALIVE_APP.registerProduceQueueLayerTo("APP", "WEB");
		RPTGEN_LAYER.registerConsumeQueueLayerFrom(USALIVE_APP, RPTGEN_LAYER, RPTREQ_LAYER, TRANSPORT_LAYER);
		RPTGEN_LAYER.registerProduceQueueLayerTo("APP", "WEB");
		RPTREQ_LAYER.registerConsumeQueueLayerFrom();
		RPTREQ_LAYER.registerProduceQueueLayerTo("APP", "WEB");
		TRANSPORT_LAYER.registerConsumeQueueLayerFrom(RPTGEN_LAYER, RPTREQ_LAYER, USALIVE_APP, PREPAID_APP);
		TRANSPORT_LAYER.registerProduceQueueLayerTo("APP");
	
		DMS_APP.registerConsumeQueueLayerFrom(POSM_LAYER);
		DMS_APP.registerProduceQueueLayerTo("KLS", "APR"); //add produce to posm
		
		PREPAID_APP.registerConsumeQueueLayerFrom(KEYMGR, TRANSPORT_LAYER);
		PREPAID_APP.registerProduceQueueLayerTo("KLS", "WEB");

		// HTTPD_NET.registerSubApps(DMS_APP);
		// HTTPD_WEB.registerSubApps(PREPAID_APP, USALIVE_APP, ESUDS_APP, USATECH_WEBSITE_APP, ENERGYMISERS_WEBSITE_APP, HOTCHOICE_APP, VERIZON_APP, GETMORE_APP);
		
		HTTPD_NET.setVersionPeriodReplacement(".");
		HTTPD_WEB.setVersionPeriodReplacement(".");
	
		subHostPrefixMap.put("usatech", "www");
		subHostPrefixMap.put("esuds", "esudsweb");
		// subHostPrefixMap.put("prepaid", "getmore");

		LOGS_AGENT_NET_APP.setMaxInstances(1);
		LOGS_AGENT_APR_APP.setMaxInstances(1);
		LOGS_AGENT_KLS_APP.setMaxInstances(1);
		LOGS_AGENT_APP.setMaxInstances(1);
		LOGS_AGENT_WEB.setMaxInstances(1);
		LOGS_LAYER_DEV.setMaxInstances(3);
		
		HTTPD_NET.setMaxInstances(1);
		HTTPD_WEB.setMaxInstances(1);
		DMS_APP.setMaxInstances(1);
		USALIVE_APP.setMaxInstances(1);
		ESUDS_APP.setMaxInstances(1);
		USATECH_WEBSITE_APP.setMaxInstances(1);
		ENERGYMISERS_WEBSITE_APP.setMaxInstances(1);
		HOTCHOICE_APP.setMaxInstances(1);
		VERIZON_APP.setMaxInstances(1);
		PREPAID_APP.setMaxInstances(1);
		JSTATD.setMaxInstances(1);
		JSTATD.setStandardJavaApp(false);
		RDW_LOADER.setMaxInstances(1);
	}

	public USATRegistry() throws UnknownHostException {
		registerServers();
	}

	/* (non-Javadoc)
	 * @see com.usatech.tools.deploy.Registry#getRegisteredApps()
	 */
	@Override
	public Set<App> getRegisteredApps() {
		return apps.keySet();
	}

	/* (non-Javadoc)
	 * @see com.usatech.tools.deploy.Registry#registerApp(com.usatech.tools.deploy.App)
	 */
	@Override
	public void registerApp(App app) {
		registerApp(app, null);
	}

	@Override
	public void registerSubApps(App app, App... subApps) {
		for(BasicServerSet serverSet : serverSets.values())
			serverSet.registerSubApps(app, subApps);
	}

	@Override
	public void unregisterSubApps(App app, App... subApps) {
		for(BasicServerSet serverSet : serverSets.values())
			serverSet.unregisterSubApps(app, subApps);
	}

	@Override
	public void clearSubApps(App app) {
		for(BasicServerSet serverSet : serverSets.values())
			serverSet.clearSubApps(app);
	}

	/* (non-Javadoc)
	 * @see com.usatech.tools.deploy.Registry#registerApp(com.usatech.tools.deploy.App, java.lang.String)
	 */
	@Override
	public void registerApp(App app, String version) {
		boolean exists = apps.containsKey(app);
		apps.put(app, version);
		if(!exists) {
			for(BasicServerSet serverSet : serverSets.values())
				serverSet.registerApp(app);
		}
	}

	protected void registerServers() throws UnknownHostException {
		// registerServerSet("LOCAL", "MST", "devmst05");
		// registerServerSet("LOCAL", "KLS", "devmst05", "bk1", "bk2");
		registerServerSet("LOCAL", "PGS", "devdbs11");
		registerServerSet("LOCAL", "PGS", "devdbs12");
		registerServerSet("LOCAL", "MST", "devdbs11", "dk1", "dk2", "js1", "js2", "pc1", "pc2");
		registerServerSet("LOCAL", "KLS", "devdbs11", "dk1", "dk2", "yh1", "yh2", "js1", "js2", "pc1", "pc2");
		registerServerSet("LOCAL", "ODB", "devdb012");

		registerServerSet("LOCAL", "MST", "devtool11", "bk1", "bk2", "dk1", "dk2", "dl1", "dl2", "ev1", "ev2", "yh1", "yh2");
		registerServerSet("LOCAL", "KLS", "devtool11", "bk1", "bk2", "dk1", "dk2", "dl1", "dl2", "ev1", "ev2");
		registerServerSet("DEV", "MON", "devtool12");
		// registerServerSet("DEV", "MST", "devtool12");

		registerServerSet("DEV", "PGS", '1', 2, 1);
		registerServerSet("INT", "PGS", '1', 2, 1);
		registerServerSet("ECC", "PGS", '1', 2, 1);
		registerServerSet("USA", "PGS", '3', 2, 1);

		registerServerSet("DEV", "MST", '1', 4, 1);
		registerServerSet("INT", "MST", '1', 2, 2);
		registerServerSet("ECC", "MST", '1', 2, 2);
		registerServerSet("USA", "MST", '3', 4, 1);
	
		registerServerSet("DEV", "NET", '1', 2, 2);
		registerServerSet("INT", "NET", '1', 2, 2);
		registerServerSet("ECC", "NET", '1', 2, 2);
		registerServerSet("USA", "NET", '3', 4, 1);
	
		registerServerSet("DEV", "APR", '1', 2, 2);
		registerServerSet("INT", "APR", '1', 2, 2);
		registerServerSet("ECC", "APR", '1', 2, 2);
		registerServerSet("USA", "APR", '3', 4, 1);

		registerServerSet("DEV", "LOGS", "ecclog11");
		registerServerSet("INT", "LOGS", "ecclog11");
		registerServerSet("ECC", "LOGS", "ecclog11");
		registerServerSet("USA", "LOGS", "usalog31");
		
		registerServerSet("DEV", "MSR", '1', 4, 1);
		registerServerSet("INT", "MSR", '1', 2, 2);
		registerServerSet("ECC", "MSR", '1', 2, 2);
		registerServerSet("USA", "MSR", '3', 4, 1);
	
		registerServerSet("DEV", "WEB", '1', 2, 2);
		registerServerSet("INT", "WEB", '1', 2, 2);
		registerServerSet("ECC", "WEB", '1', 2, 2);
		registerServerSet("USA", "WEB", '3', 4, 1);
	
		registerServerSet("DEV", "APP", '1', 2, 2);
		registerServerSet("INT", "APP", '1', 2, 2);
		registerServerSet("ECC", "APP", '1', 2, 2);
		registerServerSet("USA", "APP", '3', 4, 1);
		
		registerServerSet("DEV", "KLS", '1', 2, 1);
		registerServerSet("INT", "KLS", '1', 2, 1);
		registerServerSet("ECC", "KLS", '1', 4, 1);
		registerServerSet("USA", "KLS", '3', 4, 1);

		registerServerSet("DEV", "MON", '1', 1, 1);

		registerServerSet("DEV", "ODB", "eccdb013");
		registerServerSet("INT", "ODB", "eccdb014");
		registerServerSet("ECC", "ODB", "eccscan01");
		registerServerSet("USA", "ODB", "oradbscan03.trooper.usatech.com");
	}

	/* (non-Javadoc)
	 * @see com.usatech.tools.deploy.Registry#getServerSet(java.lang.String)
	 */
	@Override
	public BasicServerSet getServerSet(String serverEnv) {
		BasicServerSet serverSet = serverSets.get(serverEnv);
		if(serverSet == null) {
			serverSet = new BasicServerSet();
			for(App app : apps.keySet())
				serverSet.registerApp(app);
			serverSets.put(serverEnv, serverSet);
		}
		return serverSet;
	}

	public void registerServerSet(String serverEnv, String serverType, char seventh, int numServers, int numInstances) throws UnknownHostException {
		getServerSet(serverEnv).registerServerSet(serverEnv, serverType, seventh, numServers, numInstances);
	}

	public void registerServerSet(String serverEnv, String serverType, String hostName, String... instances) throws UnknownHostException {
		getServerSet(serverEnv).registerServerSet(serverEnv, serverType, hostName, instances);
	}

	public void registerFakeServerSet(String serverEnv, String serverType, String hostName) throws UnknownHostException {
		getServerSet(serverEnv).registerServerSet(serverEnv, serverType, hostName, true);
	}

	/* (non-Javadoc)
	 * @see com.usatech.tools.deploy.Registry#unregisterApp(com.usatech.tools.deploy.App)
	 */
	@Override
	public void unregisterApp(App app) {
		if(apps.keySet().remove(app)) {
			for(BasicServerSet serverSet : serverSets.values())
				serverSet.unregisterApp(app);
		}
	}

	/* (non-Javadoc)
	 * @see com.usatech.tools.deploy.Registry#updateAppVersions()
	 */
	@Override
	public void updateAppVersions() {
		for(Map.Entry<App, String> entry : apps.entrySet())
			entry.getKey().setVersion(entry.getValue());
	}

	public Collection<Host> getHosts() {
		CompositeCollection<Host> allHosts = new CompositeCollection<Host>();
		for(BasicServerSet serverSet : serverSets.values())
			allHosts.merge(serverSet.getHosts());
		return allHosts;
	}

	public static Set<App> getPossibleApps() {
		return possibleAppsUnmod;
	}
}
