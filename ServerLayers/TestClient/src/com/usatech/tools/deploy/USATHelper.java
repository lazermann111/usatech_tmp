package com.usatech.tools.deploy;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.Writer;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import com.usatech.tools.deploy.PgDeployMap.PGDATADir;

import simple.app.Processor;
import simple.io.Base64;
import simple.io.BufferStream;
import simple.io.ConcatenatingInputStream;
import simple.io.EncodingInputStream;
import simple.io.HeapBufferStream;
import simple.io.ReplacementsLineFilteringReader;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;

public class USATHelper {

	/**
	 * @throws IOException
	 */
	public static void addOdbcLinkTasks(Host host, PasswordCache cache, List<DeployTask> tasks, String workDir)
			throws IOException {
		List<DeployTask> subTasks = new ArrayList<DeployTask>();
		Processor<DeployTaskInfo, InputStream> iniFile = new ContentProducer() {
			protected void writeContent(PrintWriter pw, DeployTaskInfo taskInfo) {
				pw.println("[Oracle_DB]");
				pw.println("Description=Oracle ODBC");
				pw.println("Driver=Oracle");
				pw.println("Application Attributes=T");
				pw.println("Attributes=W");
				pw.println("BatchAutocommitMode=IfAllSuccessful");
				pw.println("CloseCursor=F");
				pw.println("DisableDPM=F");
				pw.println("DisableMTS=T");
				pw.println("EXECSchemaOpt=");
				pw.println("EXECSyntax=T");
				pw.println("Failover=T");
				pw.println("FailoverDelay=10");
				pw.println("FailoverRetryCount=10");
				pw.println("FetchBufferSize=64000");
				pw.println("ForceWCHAR=F");
				pw.println("Lobs=T");
				pw.println("Longs=T");
				pw.println("MetadataIdDefault=F");
				pw.println("QueryTimeout=T");
				pw.println("ResultSets=T");
				pw.println("ServerName=PDB");
				pw.println("SQLGetData extensions=F");
				pw.println("Translation DLL=");
				pw.println("Translation Option=0");
				pw.println("UserID=POSTGRES_MAIN");
				pw.print("Password=");
				String env = taskInfo.host.getServerEnv();
				if ("LOCAL".equalsIgnoreCase(env))
					pw.println("POSTGRES_MAIN");
				else if ("DEV".equalsIgnoreCase(env))
					pw.println("POSTGRES_MAIN");
				else if ("INT".equalsIgnoreCase(env))
					pw.println("POSTGRES_MAIN");
				else
					pw.println(
							DeployUtils.getPassword(taskInfo, "database.OPER.POSTGRES_MAIN", "POSTGRES_MAIN for OPER"));
			}
		};
		Processor<DeployTaskInfo, InputStream> tnsnamesFile = new ContentProducer() {
			protected void writeContent(PrintWriter pw, DeployTaskInfo taskInfo) {
				pw.print("PDB=");
				String env = taskInfo.host.getServerEnv();
				if ("LOCAL".equalsIgnoreCase(env))
					pw.println("ServerName=//devdb012.usatech.com:1521/USADEV04.WORLD");
				else if ("DEV".equalsIgnoreCase(env))
					pw.println(UsatSettingUpdateFileTask.OPER_DEV.substring("jdbc:oracle:thin:@".length()));
				else if ("INT".equalsIgnoreCase(env))
					pw.println(UsatSettingUpdateFileTask.OPER_INT.substring("jdbc:oracle:thin:@".length()));
				else if ("ECC".equalsIgnoreCase(env))
					pw.println(UsatSettingUpdateFileTask.OPER_ECC.substring("jdbc:oracle:thin:@".length()));
				else if ("USA".equalsIgnoreCase(env))
					pw.println(UsatSettingUpdateFileTask.OPER_PROD_DEFAULT.substring("jdbc:oracle:thin:@".length()));

			}
		};
		String unixODBCVersion = "2.3.1";
		subTasks.add(new FromCvsUploadTask("server_app_config/oracle/unixODBC-" + unixODBCVersion + ".tar.gz", "HEAD",
				workDir + "/unixODBC-" + unixODBCVersion + ".tar.gz", 0644, null, null, false));
		subTasks.add(new FromCvsUploadTask("server_app_config/oracle/ODBC-Link-1.0.4.tar.gz", "HEAD",
				workDir + "/ODBC-Link-1.0.4.tar.gz", 0644, null, null, false));
		subTasks.add(new FromCvsUploadTask(
				"server_app_config/oracle/oracle-instantclient11.2-basic-11.2.0.3.0-1.x86_64.rpm", "HEAD",
				workDir + "/oracle-instantclient11.2-basic-11.2.0.3.0-1.x86_64.rpm", 0644, null, null, false));
		subTasks.add(new FromCvsUploadTask(
				"server_app_config/oracle/oracle-instantclient11.2-odbc-11.2.0.3.0-1.x86_64.rpm", "HEAD",
				workDir + "/oracle-instantclient11.2-odbc-11.2.0.3.0-1.x86_64.rpm", 0644, null, null, false));
		subTasks.add(new FromCvsUploadTask("server_app_config/PGS/odbcinst.ini", "HEAD", workDir + "/odbcinst.ini",
				0644, null, null, true));
		subTasks.add(new UploadTask(iniFile, 0644, null, null, "odbc.ini", true, workDir + "/odbc.ini"));
		subTasks.add(new UploadTask(tnsnamesFile, 0644, null, null, "tnsnames.ora", true, workDir + "/tnsnames.ora"));
		subTasks.add(new ExecuteTask("sudo su", "cd " + workDir,
				"if [ -d unixODBC-2.3.1 ]; then rm -rf unixODBC-2.3.1;fi",
				"if [ -f unixODBC-2.3.1.tar ]; then rm -f unixODBC-2.3.1.tar;fi",
				"PG_SUFFIX=`/usr/pgsql-latest/bin/pg_config --version | cut -d ' ' -f 2 | cut -d '.' -f1-2 --output-delimiter=`",
				// "yum -v -y --disablerepo=* --enablerepo=pgdg$PG_SUFFIX install
				// postgresql$PG_SUFFIX-devel | grep -v '#'",
				"gunzip unixODBC-" + unixODBCVersion + ".tar.gz", "tar xvf unixODBC-" + unixODBCVersion + ".tar",
				"cd unixODBC-" + unixODBCVersion, "./configure --prefix=/usr/unixODBC", "make", "make install", "cd ..",
				"tar -zxvf ODBC-Link-1.0.4.tar.gz", "cd ODBC-Link-1.0.4",
				"sed -i 's|^#include \"utils/date\\.h\"$|&\\n#include \"utils/timestamp.h\"|' odbclink.c",
				"export PATH=/usr/pgsql-latest/bin/:$PATH",
				"make USE_PGXS=1 PG_CPPFLAGS='-I/usr/unixODBC/include' SHLIB_LINK='-L/usr/unixODBC/lib -lodbc'",
				"make USE_PGXS=1 PG_CPPFLAGS='-I/usr/unixODBC/include' SHLIB_LINK='-L/usr/unixODBC/lib -lodbc' install",
				"cd ..", "su - postgres",
				"if [ ! -r ~/.bash_profile ]; then echo 'export LD_LIBRARY_PATH=/usr/pgsql-latest/lib/:/usr/unixODBC/lib/:/usr/lib/oracle/11.2/client64/lib/' > ~/.bash_profile; chown postgres:postgres ~/.bash_profile; chmod 0744 ~/.bash_profile; "
						+ "elif [ `grep -c 'LD_LIBRARY_PATH=/usr/pgsql-latest/lib:/usr/unixODBC/lib/:/usr/lib/oracle/11.2/client64/lib/' ~/.bash_profile` -eq 0 ]; then echo 'export LD_LIBRARY_PATH=/usr/pgsql-latest/lib/:/usr/unixODBC/lib/:/usr/lib/oracle/11.2/client64/lib/' >> ~/.bash_profile; fi",
				"if [ ! -x ~/.bash_profile ]; then chmod u+x ~/.bash_profile; fi",
				"if [ `grep -c 'export ORACLE_HOME=' ~/.bash_profile` -eq 0 ]; then echo 'export ORACLE_HOME=/usr/lib/oracle/11.2/client64' >> ~/.bash_profile; fi",
				"if [ `grep -c 'export ORACLE_SID=' ~/.bash_profile` -eq 0 ]; then echo 'export ORACLE_SID=orcl' >> ~/.bash_profile; fi",
				"exit", "mkdir -p /usr/lib/oracle/11.2/client64/network/admin",
				"mkdir -p /usr/lib/oracle/11.2/client64/log", "chmod -R 0755 /usr/lib/oracle/11.2/client64/network",
				"chmod 0777 /usr/lib/oracle/11.2/client64/log",
				"echo 'NAMES.DIRECTORY_PATH=(TNSNAMES, EZCONNECT)' > /usr/lib/oracle/11.2/client64/network/admin/sqlnet.ora",
				"/bin/cp tnsnames.ora /usr/lib/oracle/11.2/client64/network/admin/tnsnames.ora",
				"chmod 0644 /usr/lib/oracle/11.2/client64/network/admin/*",
				"sed -i 's|^export LD_LIBRARY_PATH=/usr/pgsql-latest/lib/$|export LD_LIBRARY_PATH=/usr/pgsql-latest/lib/:/usr/unixODBC/lib/:/usr/lib/oracle/11.2/client64/lib/\\nexport ORACLE_HOME=/usr/lib/oracle/11.2/client64\\nexport ORACLE_SID=orcl|' /opt/USAT/postgres/bin/postgresql.sh",
				"if [ `rpm -qa|grep oracle-instantclient11.2-basic-11.2.0.3.0-1.x86_64|wc -l` -eq 0 ]; then rpm -i oracle-instantclient11.2-basic-11.2.0.3.0-1.x86_64.rpm;fi",
				"if [ `rpm -qa|grep oracle-instantclient11.2-odbc-11.2.0.3.0-1.x86_64|wc -l` -eq 0 ]; then rpm -i oracle-instantclient11.2-odbc-11.2.0.3.0-1.x86_64.rpm;fi",
				"/bin/cp odbc*.ini /usr/unixODBC/etc/",
				"if [ ! -h /usr/unixODBC/lib/libodbcinst.so.1 ]; then ln -s /usr/unixODBC/lib/libodbcinst.so /usr/unixODBC/lib/libodbcinst.so.1;fi"
		// "su - postgres -c '/opt/USAT/postgres/bin/postgresql.sh stop &&
		// /opt/USAT/postgres/bin/postgresql.sh start &'",
		// "sleep 15",
		// "su - postgres -c '/usr/pgsql-latest/bin/psql main -c \"DROP SCHEMA IF EXISTS
		// odbclink CASCADE;\"'",
		// "su - postgres -c '/usr/pgsql-latest/bin/psql main -f
		// /usr/pgsql-latest/share/contrib/odbclink.sql'"
		));
		tasks.add(new OptionalTask(
				"sudo su -c 'if [ -r /usr/unixODBC/etc/odbc.ini ] && [ -r /usr/lib/oracle/11.2/client64/network/admin/tnsnames.ora ] && [ -r /usr/pgsql-latest/share/contrib/odbclink.sql ]; then echo \"Y\"; else echo \"N\"; fi'",
				Pattern.compile("^N\\s*"), false, subTasks.toArray(new DeployTask[subTasks.size()])));
	}

	public static String[] getJmxUsers(Host host) {
		// Setting all environments identical now, but leaving separate returns if that
		// needs to be changed - pcowan, 11/14/16
		String[] usersWithJMX = {"ahachikyan", "aroyce", "dkouznetsov", "esamodelkova", "fwang", "jbradley", "mnjanje", "kchigurupati", "szalesskaya", "tfroschle", "tcattani" };
		if ("USA".equalsIgnoreCase(host.getServerEnv())) {
			return usersWithJMX;
		} else if ("ECC".equalsIgnoreCase(host.getServerEnv())) {
			return usersWithJMX;
		} else if ("INT".equalsIgnoreCase(host.getServerEnv())) {
			return usersWithJMX;
		} else if ("DEV".equalsIgnoreCase(host.getServerEnv())) {
			return usersWithJMX;
		}
		return null;
	}

	protected static void uploadPgMonitConf(Host host, List<DeployTask> tasks) throws IOException {
		String userName = "postgres";
		String serviceName = "postgresql";
		String[] pgNames = getPgNames(host);
		for (int i = 0; i < pgNames.length; i++) {
			String pgName = pgNames[i];
			BufferStream bufferStream = new HeapBufferStream();
			PrintStream writer = new PrintStream(bufferStream.getOutputStream());
			tasks.add(new ExecuteTask("sudo su",
					DeployUtils.getMakeDir("/opt/USAT/" + pgName, "postgres", "postgres", 0755),
					DeployUtils.getMakeDir("/opt/USAT/" + pgName + "/bin", "postgres", "postgres", 0755)));
			boolean klsPrimary;
			boolean klsStandby;
			int port;
			if (host.isServerType("KLS")) {
				char hostLast = host.getSimpleName().charAt(host.getSimpleName().length() - 1);
				char nameLast = pgName.charAt(pgName.length() - 1);
				if (hostLast != nameLast) {
					port = 15432;
					klsPrimary = false;
					klsStandby = true;
				} else {
					port = 5432;
					klsPrimary = true;
					klsStandby = false;
				}
			} else {
				klsPrimary = false;
				klsStandby = false;
				port = 5432 - pgNames.length + i + 1;
			}
			String binFileName = "/opt/USAT/" + pgName + "/bin/" + serviceName + ".sh";
			writer.print("check process ");
			writer.print(pgName);
			writer.print(" with pidfile ");
			writer.print("/opt/USAT/" + pgName + "/data/postmaster.pid");
			writer.println();
			writer.print("    start program = \"");
			writer.print(binFileName);
			writer.print(" start\" as uid ");
			writer.print(userName);
			writer.print(" with timeout 90 seconds");
			writer.println();
			writer.print("    stop  program = \"");
			writer.print(binFileName);
			writer.print(" stop\" as uid ");
			writer.print(userName);
			writer.println();
			writer.print("    if failed host localhost port ");
			writer.print(port);
			writer.println(" with timeout 15 seconds for 4 cycles then restart");
			writer.println("    if 10 restarts within 10 cycles then timeout");
			writer.print("    depends on ");
			writer.print(pgName);
			writer.println("_bin");
			writer.println();
			writer.print("check file ");
			writer.print(pgName);
			writer.print("_bin with path /usr/pgsql-latest/bin/postgres");
			writer.println();
			writer.println("    alert noone@usatech.com { invalid }");
			writer.println("    if changed checksum then alert");
			writer.println("    if failed permission 755 then alert");
			writer.println("    if failed uid root then alert");
			writer.println("    if failed gid root then alert");

			BufferStream binBufferStream = new HeapBufferStream();
			PrintStream binWriter = new PrintStream(binBufferStream.getOutputStream());
			binWriter.println("#!/bin/bash");
			binWriter.println("export LD_LIBRARY_PATH=/usr/pgsql-latest/lib/");
			binWriter.println("case $1 in");
			binWriter.print("    start) ");
			binWriter.print("/usr/pgsql-latest/bin/postgres -p ");
			binWriter.print(port);
			binWriter.print(" -D ");
			binWriter.print("/opt/USAT/" + pgName + "/data > ");
			binWriter.print("/opt/USAT/" + pgName + "/logs/server.log 2>&1");
			if (klsStandby) {
				tasks.add(new FromCvsUploadTask("server_app_config/KLS/check_standby_lag.sh", "HEAD",
						"/opt/USAT/monit/bin/check_standby_lag.sh", 0755, "root", "root", true));
				String checkFileName = "/opt/USAT/" + pgName + "/bin/check_standby_lag.sh";
				BufferStream checkBufferStream = new HeapBufferStream();
				PrintStream checkWriter = new PrintStream(checkBufferStream.getOutputStream());
				checkWriter.println("#!/bin/bash");
				checkWriter.print("/opt/USAT/monit/bin/check_standby_lag.sh ");
				checkWriter.println("/opt/USAT/" + pgName + "/data");
				checkWriter.flush();
				tasks.add(new UnixUploadTask(checkBufferStream.getInputStream(), checkFileName, 0755, "root", "root",
						true, checkFileName));
				writer.println("check program " + pgName + "_status with path \"/opt/USAT/" + pgName
						+ "/bin/check_standby_lag.sh\"");
				writer.println("    if status != 0 then alert");
				writer.println("    depends on " + pgName);
			} else if (klsPrimary) {
				binWriter.print(
						" & \n		sleep 15;\n		/usr/pgsql-latest/bin/psql -U postgres -p 5432 -c \"CREATE TABLE temp_test( test integer ); insert into temp_test values(1); DROP TABLE temp_test;\"");
			}
			binWriter.print(";;");
			binWriter.println();
			binWriter.print("    stop) ");
			binWriter.print("/usr/pgsql-latest/bin/pg_ctl -o '-p ");
			binWriter.print(port);
			binWriter.print("' -D ");
			binWriter.print("/opt/USAT/" + pgName + "/data stop -m fast;;");
			binWriter.println("esac");
			binWriter.flush();
			tasks.add(new UnixUploadTask(binBufferStream.getInputStream(), binFileName, 0755, userName, userName, true,
					binFileName));
			writer.flush();
			if (bufferStream.getLength() > 0) {
				String fileName = "/opt/USAT/monit/conf.d/" + pgName + ".conf";
				tasks.add(new UnixUploadTask(bufferStream.getInputStream(), fileName, 0644, "root", "root", true,
						fileName));
			}
		}
	}

	public static void uploadAppMonitConf(Host host, List<DeployTask> tasks, App app, Integer ordinal)
			throws IOException {
		BufferStream bufferStream = new HeapBufferStream();
		PrintStream writer = new PrintStream(bufferStream.getOutputStream());
		String appSpecificDir = app.getName() + (ordinal == null ? "" : ordinal);
		if ("postgres".equals(app.getName())) {
			int port;
			if (ordinal != null)
				port = 5430 + ordinal;
			else
				port = 5432;
			String appDir;
			StringBuilder standbyCheck = new StringBuilder("");
			StringBuilder klsForceCheckpointOnStart = new StringBuilder("");
			PgDeployMap.PGDATADir pgDataDir;
			if (USATRegistry.POSTGRES_KLS.equals(app)
					&& (pgDataDir = PgDeployMap.hostPGDATADirMap.get(host.getSimpleName())) != null) {
				if (host.isStandby()) {
					appDir = pgDataDir.getStandbyDir();
					appSpecificDir = appDir.substring(appDir.lastIndexOf("/") + 1);
					port = 15432;
					tasks.add(new ExecuteTask("sudo su", DeployUtils.getMakeDir(appDir, "postgres", "postgres", 0755),
							DeployUtils.getMakeDir(appDir + "/bin", "postgres", "postgres", 0755)));
					tasks.add(new FromCvsUploadTask("server_app_config/KLS/check_standby_lag.sh", "HEAD",
							"/opt/USAT/monit/bin/check_standby_lag.sh", 0755, "root", "root", true));
					String checkFileName = appDir + "/bin/check_standby_lag.sh";
					BufferStream checkBufferStream = new HeapBufferStream();
					PrintStream checkWriter = new PrintStream(checkBufferStream.getOutputStream());
					checkWriter.println("#!/bin/bash");
					checkWriter.print("/opt/USAT/monit/bin/check_standby_lag.sh ");
					checkWriter.println(appDir + "/data");
					checkWriter.flush();
					tasks.add(new UnixUploadTask(checkBufferStream.getInputStream(), checkFileName, 0755, "root",
							"root", true, checkFileName));
					standbyCheck.append("check program " + appSpecificDir + "_status with path \"" + appDir
							+ "/bin/check_standby_lag.sh\"" + "\n");
					standbyCheck.append("    if status != 0 then alert" + "\n");
					standbyCheck.append("    depends on " + appSpecificDir + "\n");
				} else {
					appDir = pgDataDir.getPrimaryDir();
					appSpecificDir = appDir.substring(appDir.lastIndexOf("/") + 1);
					port = 5432;
					tasks.add(new ExecuteTask("sudo su", DeployUtils.getMakeDir(appDir, "postgres", "postgres", 0755),
							DeployUtils.getMakeDir(appDir + "/bin", "postgres", "postgres", 0755)));
					klsForceCheckpointOnStart.append(
							" & \n		sleep 15;\n		/usr/pgsql-latest/bin/psql -U postgres -p 5432 -c \"CREATE TABLE temp_test( test integer ); insert into temp_test values(1); DROP TABLE temp_test;\"");
				}
			} else {
				appSpecificDir = app.getName() + (ordinal == null ? "" : ordinal);
				appDir = "/opt/USAT/" + appSpecificDir;
				tasks.add(new ExecuteTask("sudo su", DeployUtils.getMakeDir(appDir, "postgres", "postgres", 0755),
						DeployUtils.getMakeDir(appDir + "/bin", "postgres", "postgres", 0755)));
			}

			String binFileName = appDir + "/bin/" + app.getServiceName() + ".sh";
			BufferStream binBufferStream = new HeapBufferStream();
			PrintStream binWriter = new PrintStream(binBufferStream.getOutputStream());
			binWriter.println("#!/bin/bash");
			binWriter.println("export LD_LIBRARY_PATH=/usr/pgsql-latest/lib/");
			binWriter.println("case $1 in");
			binWriter.print("    start) ");
			binWriter.print("/usr/pgsql-latest/bin/postgres -p ");
			binWriter.print(port);
			binWriter.print(" -D ");
			binWriter.print(appDir + "/data > ");
			binWriter.print(appDir + "/logs/server.log 2>&1");
			binWriter.print(klsForceCheckpointOnStart.toString());
			binWriter.print(";;");
			binWriter.println();
			binWriter.print("    stop) ");
			binWriter.print("/usr/pgsql-latest/bin/pg_ctl -o '-p ");
			binWriter.print(port);
			binWriter.print("' -D ");
			binWriter.print(appDir + "/data stop -m fast;;");
			binWriter.println("esac");
			binWriter.flush();
			tasks.add(new UnixUploadTask(binBufferStream.getInputStream(), binFileName, 0755, app.getUserName(),
					app.getUserName(), true, binFileName));

			writer.print("check process ");
			writer.print(appSpecificDir);
			writer.print(" with pidfile ");
			writer.print(appDir + "/data/postmaster.pid");
			writer.println();
			writer.print("    start program = \"");
			writer.print(binFileName);
			writer.print(" start\" as uid ");
			writer.print(app.getUserName());
			writer.print(" with timeout 90 seconds");
			writer.println();
			writer.print("    stop  program = \"");
			writer.print(binFileName);
			writer.print(" stop\" as uid ");
			writer.print(app.getUserName());
			writer.println();
			writer.print("    if failed host localhost port ");
			writer.print(port);
			writer.println(" with timeout 15 seconds for 4 cycles then restart");
			writer.println("    if 10 restarts within 10 cycles then timeout");
			writer.print("    depends on ");
			writer.print(appSpecificDir);
			writer.println("_bin");
			writer.println();
			writer.print("check file ");
			writer.print(appSpecificDir);
			writer.print("_bin with path /usr/pgsql-latest/bin/postgres");
			writer.println();
			writer.println("    alert noone@usatech.com { invalid }");
			writer.println("    if changed checksum then alert");
			writer.println("    if failed permission 755 then alert");
			writer.println("    if failed uid root then alert");
			writer.println("    if failed gid root then alert");
			writer.println(standbyCheck.toString());
		} else if ("httpd".equals(app.getName())) {
			int port = 80 + 100 * (ordinal == null ? 0 : ordinal - 1);
			writer.print("check process ");
			writer.print(app.getName());
			if (ordinal != null)
				writer.print(ordinal);
			writer.print(" with pidfile /opt/USAT/");
			writer.print(app.getName());
			if (ordinal != null)
				writer.print(ordinal);
			writer.print("/logs/");
			writer.print(app.getServiceName());
			writer.print(".pid");
			writer.println();
			writer.print("    start program = \"/usr/local/apache2/bin/httpd -f /opt/USAT/");
			writer.print(app.getName());
			if (ordinal != null)
				writer.print(ordinal);
			writer.print("/conf/httpd.conf -k start\" with timeout 90 seconds");
			writer.println();
			writer.print("    stop  program = \"/usr/local/apache2/bin/httpd -f /opt/USAT/");
			writer.print(app.getName());
			if (ordinal != null)
				writer.print(ordinal);
			writer.print("/conf/httpd.conf -k stop\"");
			writer.println();
			writer.print("    if failed host localhost port ");
			writer.print(port);
			writer.println(" protocol HTTP request \"/server-status\" with timeout 15 seconds then restart");
			writer.println("    if 10 restarts within 10 cycles then timeout");
			writer.print("    if failed host localhost port ");
			writer.print(port);
			writer.println(" protocol apache-status dnslimit > 25% or loglimit > 80% for 4 cycles then alert");
			writer.print("    depends on ");
			writer.print(app.getName());
			if (ordinal != null)
				writer.print(ordinal);
			writer.println("_bin");
			writer.println();
			writer.print("check file ");
			writer.print(app.getName());
			if (ordinal != null)
				writer.print(ordinal);
			writer.println("_bin with path /usr/local/apache2/bin/httpd");
			writer.println("    alert noone@usatech.com { invalid }");
			writer.println("    if changed checksum then alert");
			writer.println("    if failed permission 755 then alert");
			writer.println("    if failed uid root then alert");
			writer.println("    if failed gid root then alert");
		} else if (app.isStandardJavaApp()) {
			int appPort = 7000 + app.getPortOffset() + (ordinal == null ? 700 : ordinal * 100);
			// check status
			String checkFileName = "/opt/USAT/" + appSpecificDir + "/bin/check_app_status.sh";
			BufferStream checkBufferStream = new HeapBufferStream();
			PrintStream checkWriter = new PrintStream(checkBufferStream.getOutputStream());
			checkWriter.println("#!/bin/bash");
			checkWriter.print("/opt/USAT/monit/bin/check_app_status.sh ");
			checkWriter.println(appPort);
			checkWriter.flush();
			tasks.add(new UnixUploadTask(checkBufferStream.getInputStream(), checkFileName, 0755, "root", "root", true,
					checkFileName));
			// check memory
			String checkMemFileName = "/opt/USAT/" + appSpecificDir + "/bin/check_memory_percent.sh";
			BufferStream checkMemBufferStream = new HeapBufferStream();
			PrintStream checkMemWriter = new PrintStream(checkMemBufferStream.getOutputStream());
			checkMemWriter.println("#!/bin/bash");
			checkMemWriter.println("port=" + appPort);
			checkMemWriter.println("percent=`/opt/USAT/monit/bin/check_memory_percent.sh $port`");
			checkMemWriter.println("if ! [[ \"$percent\" =~ ^[0-9]+$ ]] ; then");
			checkMemWriter.println("  echo \"[`date`] app port $port: Max Memory is not a number\" >&2; exit 1");
			checkMemWriter.println("fi");
			checkMemWriter.println("if [ $percent -ge 95 ]; then");
			checkMemWriter.println(
					"  echo \"[`date`] app port $port: Used Memory percentage ($percent %) is greater than the threashhold.\" >&2; exit 2");
			checkMemWriter.println("fi");
			checkMemWriter.flush();
			tasks.add(new UnixUploadTask(checkMemBufferStream.getInputStream(), checkMemFileName, 0755, "root", "root",
					true, checkMemFileName));
			writer = new PrintStream(bufferStream.getOutputStream());
			writer.print("check process ");
			writer.print(appSpecificDir);
			writer.print(" with pidfile /opt/USAT/");
			writer.print(appSpecificDir);
			writer.print("/logs/");
			writer.print(app.getServiceName());
			writer.println(".pid");
			writer.print("    start program = \"/opt/USAT/");
			writer.print(appSpecificDir);
			writer.print("/bin/");
			writer.print(app.getServiceName());
			writer.print(".sh start\"");
			StringBuilder extraDependency = new StringBuilder();
			StringBuilder extraDependencyCheck = new StringBuilder();
			if ("keymgr".equals(app.getName())) {
				String appAbsoluteDir = "/opt/USAT/" + appSpecificDir;
				extraDependency.append("    depends on keymgr_reloader_bin");
				extraDependencyCheck.append("check file keymgr_reloader_bin with path ");
				extraDependencyCheck.append(appAbsoluteDir);
				extraDependencyCheck.append("/bin/reloader.sh\n");
				extraDependencyCheck.append("    alert noone@usatech.com { invalid }\n");
				extraDependencyCheck.append("    if changed checksum then alert\n");
				extraDependencyCheck.append("    if failed permission 754 then alert\n");
				extraDependencyCheck.append("    if failed uid ");
				extraDependencyCheck.append(app.getUserName());
				extraDependencyCheck.append(" then alert\n");
			}
			writer.println(" with timeout 90 seconds");
			writer.print("    stop  program = \"/opt/USAT/");
			writer.print(appSpecificDir);
			writer.print("/bin/");
			writer.print(app.getServiceName());
			writer.print(".sh stop 30000\"");
			writer.println(" with timeout 90 seconds");
			writer.print("    if failed host localhost port ");
			writer.print(appPort);
			writer.println(" with timeout 15 seconds for 4 cycles then restart");
			writer.println("    if 10 restarts within 10 cycles then timeout");
			if (USATRegistry.USALIVE_APP.equals(app)) {
				writer.print("    if failed url http://localhost:");
				writer.print(7080 + app.getPortOffset() + (ordinal == null ? 700 : ordinal * 100));
				writer.print(USATRegistry.USALIVE_CONTEXT_PATH);
				writer.println(
						"/ping_report_engine.i?timeout=5000 content == 'Reporting Engine responded in [0-9]{1,} ms' with timeout 5 seconds for 40 cycles then restart");
			}

			writer.print("    depends on ");
			writer.print(appSpecificDir);
			writer.println("_bin");
			writer.println(extraDependency.toString());
			writer.println();
			writer.print("check file ");
			writer.print(appSpecificDir);
			writer.print("_bin with path /opt/USAT/");
			writer.print(appSpecificDir);
			writer.print("/bin/");
			writer.print(app.getServiceName());
			writer.println(".sh");
			writer.println("    alert noone@usatech.com { invalid }");
			writer.println("    if changed checksum then alert");
			writer.println("    if failed permission 754 then alert");
			writer.print("    if failed uid ");
			writer.print(app.getUserName());
			writer.println(" then alert");
			writer.println();
			writer.print("check program ");
			writer.print(appSpecificDir);
			writer.print("_status with path \"");
			writer.print(checkFileName);
			writer.println("\" every 20 cycles");// 5 minutes
			writer.print(
					"    if status > 0 then exec \"/bin/bash -c '/opt/USAT/monit/bin/kill_owned_by_user.sh `cat /opt/USAT/");
			writer.print(appSpecificDir);
			writer.print("/logs/");
			writer.print(app.getServiceName());
			writer.print(".pid` ");
			writer.print(app.getUserName());
			writer.print(" /opt/USAT/");
			writer.print(appSpecificDir);
			writer.print("/bin/");
			writer.print(app.getServiceName());
			writer.print(".sh restart'\"");
			writer.println();
			writer.print("    depends on ");
			writer.print(appSpecificDir);
			writer.println();
			// check memory usage
			writer.println();
			writer.print("check program ");
			writer.print(appSpecificDir);
			writer.print("_mem_status with path \"");
			writer.print("/opt/USAT/");
			writer.print(appSpecificDir);
			writer.println("/bin/check_memory_percent.sh\" every 4 cycles");// check every minute, if failed 5 times for
																			// 5 checks, kill the process
			writer.print(
					"    if status = 1 5 times within 20 cycles then exec \"/bin/bash -c '/opt/USAT/monit/bin/kill_owned_by_user.sh `cat /opt/USAT/");
			writer.print(appSpecificDir);
			writer.print("/logs/");
			writer.print(app.getServiceName());
			writer.print(".pid` ");
			writer.print(app.getUserName());
			writer.print(" /opt/USAT/");
			writer.print(appSpecificDir);
			writer.print("/bin/");
			writer.print(app.getServiceName());
			writer.print(".sh restart'\"");
			writer.println();
			writer.print(
					"    if status = 2 5 times within 20 cycles then exec \"/bin/bash -c '/opt/USAT/monit/bin/kill_owned_by_user.sh `cat /opt/USAT/");
			writer.print(appSpecificDir);
			writer.print("/logs/");
			writer.print(app.getServiceName());
			writer.print(".pid` ");
			writer.print(app.getUserName());
			writer.print(" MEM=/opt/USAT/");
			writer.print(appSpecificDir);
			writer.print("/logs/");
			writer.print(app.getServiceName());
			writer.print("_`date +%Y%m%d_%H%M%S`.dmp");
			writer.print(" /opt/USAT/");
			writer.print(appSpecificDir);
			writer.print("/bin/");
			writer.print(app.getServiceName());
			writer.print(".sh restart'\"");
			writer.println();
			writer.print("    depends on ");
			writer.print(appSpecificDir);
			writer.print("_status");
			writer.println();
			// check inactivity
			if (app.isUsingQueueLayer()) {
				String checkInActiveFileName = "/opt/USAT/" + appSpecificDir + "/bin/check_inactivity.sh";
				BufferStream checkInActiveBufferStream = new HeapBufferStream();
				PrintStream checkInActiveWriter = new PrintStream(checkInActiveBufferStream.getOutputStream());
				checkInActiveWriter.println("#!/bin/bash");
				checkInActiveWriter.print("/opt/USAT/monit/bin/check_inactivity.sh ");
				checkInActiveWriter.print(appPort);
				checkInActiveWriter.println(" 300");
				checkInActiveWriter.flush();
				tasks.add(new UnixUploadTask(checkInActiveBufferStream.getInputStream(), checkInActiveFileName, 0755,
						"root", "root", true, checkInActiveFileName));
				writer.println();
				writer.print("check program ");
				writer.print(appSpecificDir);
				writer.print("_inactivity with path \"");
				writer.print(checkInActiveFileName);
				writer.println("\" every 4 cycles");// check every minute
				writer.print("    if status = 2 then alert");
				writer.println();
			}

			writer.println(extraDependencyCheck.toString());
			writer.println();
		} else {
			tasks.add(new FromCvsUploadTask("server_app_config/common/monit/" + appSpecificDir + ".conf", "HEAD",
					"/opt/USAT/monit/conf.d/" + appSpecificDir + ".conf", 0644, "root", "root", true));
			return;
		}
		writer.flush();
		if (bufferStream.getLength() > 0) {
			String fileName = "/opt/USAT/monit/conf.d/" + appSpecificDir + ".conf";
			tasks.add(
					new UnixUploadTask(bufferStream.getInputStream(), fileName, 0644, "root", "root", true, fileName));
		}
	}

	/**
	 * Adds tasks to update postgresql.conf, pg_hba.conf, pg_ident.conf, server.crt,
	 * and root.crt. Does not handle freezing cluster or restarting postgres
	 * 
	 * @param tasks
	 * @param host
	 * @param registry
	 * @throws IOException
	 */
	public static void addPostgresConfTasks(List<DeployTask> tasks, final Host host, Registry registry)
			throws IOException {
		String serverType = host.getServerTypes().iterator().next();
		if (host.isServerType("KLS")) {
			PGDATADir pgdd = PgDeployMap.hostPGDATADirMap.get(host.getSimpleName());
			if ("LOCAL".equalsIgnoreCase(host.getServerEnv())) {
				addPostgresConfTasks(tasks, host, "KLS", "postgres", pgdd.getPrimaryDir(), "postgresql_primary.conf",
						5432, registry.getServerSet(host.getServerEnv()));
			} else {
				addPostgresConfTasks(tasks, host, "KLS", "postgres", pgdd.getPrimaryDir(), "postgresql_primary.conf",
						5432, registry.getServerSet(host.getServerEnv()));
				addPostgresConfTasks(tasks, host, "KLS", "postgres", pgdd.getStandbyDir(), "postgresql_standby.conf",
						15432, registry.getServerSet(host.getServerEnv()));
				addPostgresReplicaConfTask(tasks, host, "postgres", pgdd.getStandbyDir());
			}
		} else if (host.isServerType("PGS")) {
			List<DeployTask> subTasks = new ArrayList<>();
			addPostgresConfTasks(subTasks, host, "PGS", "postgres", "/opt/USAT/postgres", "postgresql_primary.conf", 0,
					registry.getServerSet(host.getServerEnv()));
			tasks.add(new OptionalTask(
					"sudo clustat | sed -n 's/^ service:\\([A-Za-z0-9._][A-Za-z0-9._]*\\)  *" + host.getSimpleName()
							+ "-priv .*$/\1/gp'",
					Pattern.compile(".*^primary$.*", Pattern.MULTILINE), false,
					subTasks.toArray(new DeployTask[subTasks.size()])));
			subTasks.clear();
			addPostgresConfTasks(tasks, host, "PGS", "postgres", "/opt/USAT/postgres_stdby", "postgresql_standby.conf",
					0, registry.getServerSet(host.getServerEnv()));
			tasks.add(new OptionalTask(
					"sudo clustat | sed -n 's/^ service:\\([A-Za-z0-9._][A-Za-z0-9._]*\\)  *" + host.getSimpleName()
							+ "-priv .*$/\1/gp'",
					Pattern.compile(".*^stdby$.*", Pattern.MULTILINE), false,
					subTasks.toArray(new DeployTask[subTasks.size()])));
		} else {
			Server server = registry.getServerSet(host.getServerEnv()).getServer(host, serverType);
			if (server.instances == null || server.instances.length <= 1)
				addPostgresConfTasks(tasks, host, serverType, "postgres", "/opt/USAT/postgres", "postgresql.conf", 5432,
						registry.getServerSet(host.getServerEnv()));
			else
				for (int i = 0; i < server.instances.length; i++)
					addPostgresConfTasks(tasks, host, serverType, "postgres",
							"/opt/USAT/postgres" + server.instances[i], "postgresql.conf",
							5430 + Integer.parseInt(server.instances[i]), registry.getServerSet(host.getServerEnv()));
		}
	}

	public static void addPostgresConfTasks(List<DeployTask> tasks, App app, String[] pgDirs, String[] postgresqlConfs,
			final Host host, final int port, final ServerSet serverSet) throws IOException {
		for (int i = 0; i < pgDirs.length; i++) {
			addPostgresConfTasks(tasks, host, app.getServerType(), app.getUserName(), pgDirs[i], postgresqlConfs[i],
					port, serverSet);
			if (i > 0)
				addPostgresReplicaConfTask(tasks, host, app.getUserName(), pgDirs[i]);
		}
	}

	public static InputStream create_pg_hba(Host host, ServerSet serverSet) throws IOException {
		BufferStream buffer = new HeapBufferStream();
		try (Writer writer = new OutputStreamWriter(buffer.getOutputStream())) {
			write_pg_hba_line(writer, "#TYPE", "DATABASE", "USER", "CIDR-ADDRESS", "METHOD");
			write_pg_hba_line(writer, "local", "all", "postgres", null, "peer map=local");
			if (host.isServerType("KLS")) {
				write_pg_hba_line(writer, "hostssl", "km", "keymgr", "samehost", "md5");
				for (Host h : serverSet.getHosts("KLS"))
					write_pg_hba_line(writer, "hostssl", "replication", "replica",
							InetAddress.getByName(h.getFullName()).getHostAddress() + "/32", "cert");
			} else {
				write_pg_hba_line(writer, "host", "all", "all", "samehost", "md5");
				write_pg_hba_line(writer, "hostssl", "all", "postgres", "all", "reject");

				// all app servers
				Set<Host> hosts = new LinkedHashSet<>();
				if (host.isServerType("MST")) {
					hosts.addAll(serverSet.getHosts("MST"));
					hosts.addAll(serverSet.getHosts("APR"));
					hosts.addAll(serverSet.getHosts("NET"));
					hosts.addAll(serverSet.getHosts("KLS"));
				}
				if (host.isServerType("MSR")) {
					hosts.addAll(serverSet.getHosts("MSR"));
					hosts.addAll(serverSet.getHosts("APP"));
					hosts.addAll(serverSet.getHosts("WEB"));
				}

				if (host.isServerType("PGS")) {
					hosts.addAll(serverSet.getHosts("APR"));
					hosts.addAll(serverSet.getHosts("APP"));
					for (Host h : serverSet.getHosts("PGS"))
						write_pg_hba_line(writer, "hostssl", "replication", "replica",
								InetAddress.getByName(h.getFullName()).getHostAddress() + "/32", "cert");
					if ("DEV".equalsIgnoreCase(host.getServerEnv()))
						write_pg_hba_line(writer, "hostssl", "replication", "replica",
								InetAddress.getByName("devpgs10").getHostAddress() + "/32", "cert");
					else if ("INT".equalsIgnoreCase(host.getServerEnv()))
						write_pg_hba_line(writer, "hostssl", "replication", "replica",
								InetAddress.getByName("intpgs10").getHostAddress() + "/32", "cert");
					else if ("ECC".equalsIgnoreCase(host.getServerEnv()))
						write_pg_hba_line(writer, "hostssl", "replication", "replica",
								InetAddress.getByName("eccpgs10").getHostAddress() + "/32", "cert");
					else if ("USA".equalsIgnoreCase(host.getServerEnv()))
						write_pg_hba_line(writer, "hostssl", "replication", "replica",
								InetAddress.getByName("usapgs30").getHostAddress() + "/32", "cert");
				}

				for (Host h : hosts)
					write_pg_hba_line(writer, "hostssl", "all", "all",
							InetAddress.getByName(h.getFullName()).getHostAddress() + "/32", "md5");

				// Now developers
				for (String s : new String[] { "10.0.0.70", "10.0.0.104", "10.0.0.106", "10.0.0.110" })
					write_pg_hba_line(writer, "hostssl", "all", "admin_1", s + "/32", "md5");

				// And read only
				write_pg_hba_line(writer, "hostssl", "all", "monitor", "10.0.0.0/23", "md5");
				write_pg_hba_line(writer, "hostssl", "all", "monitor", "10.20.0.0/24", "md5");
			}
			writer.flush();
		}
		return buffer.getInputStream();
	}

	protected static void write_pg_hba_line(Writer writer, String type, String database, String user, String host,
			String method) throws IOException {
		write_pg_hba_col(writer, type, 12);
		write_pg_hba_col(writer, database, 12);
		write_pg_hba_col(writer, user, 12);
		write_pg_hba_col(writer, host, 20);
		writer.append(method).append('\n');
	}

	protected static void write_pg_hba_col(Writer writer, String value, int length) throws IOException {
		if (value == null)
			StringUtils.appendPadded(writer, "", ' ', length, Justification.LEFT);
		else if (value.length() >= length)
			writer.append(value).append(' ');
		else
			StringUtils.appendPadded(writer, value, ' ', length, Justification.LEFT);
	}

	public static void addPgHbaConfTasks(List<DeployTask> tasks, Host host, ServerSet serverSet) throws IOException {
		for (String pgName : getPgNames(host))
			tasks.add(new UploadTask(create_pg_hba(host, serverSet), 0644, "postgres", "postgres", "pg_hba.conf", true,
					"/opt/USAT/" + pgName + "/data/pg_hba.conf"));
	}

	protected static void addPostgresConfTasks(List<DeployTask> tasks, Host host, String serverType, String username,
			String pgDir, String postgresqlConf, int port, ServerSet serverSet) throws IOException {
		tasks.add(new UploadTask(create_pg_hba(host, serverSet), 0644, username, username, "pg_hba.conf", true,
				pgDir + "/data/pg_hba.conf"));
		/*
		 * tasks.add(new FromCvsUploadTask("server_app_config/" +
		 * serverType.toUpperCase() + "/pg_hba.conf", "HEAD", pgDir +
		 * "/data/pg_hba.conf", 0644, username, username, true) {
		 * 
		 * @Override protected InputStream decorate(InputStream in) { if(hbaReplacements
		 * != null) in = new EncodingInputStream(new ReplacementsLineFilteringReader(new
		 * BufferedReader(new InputStreamReader(in)), "\n", hbaReplacements)); return
		 * in; } });
		 */
		tasks.add(new FromCvsUploadTask("server_app_config/" + serverType.toUpperCase() + "/" + postgresqlConf, "HEAD",
				pgDir + "/data/postgresql.conf", 0644, username, username, true) {
			@Override
			protected InputStream decorate(InputStream in) {
				if (port > 0)
					in = new ConcatenatingInputStream(in, new ByteArrayInputStream(("\nport = " + port).getBytes()));
				if (host.isServerType("MST") && "ECC".equalsIgnoreCase(host.getServerEnv())) {
					in = new EncodingInputStream(
							new ReplacementsLineFilteringReader(new BufferedReader(new InputStreamReader(in)), "\n",
									Collections.singletonMap(Pattern.compile("^shared_buffers\\s*=\\s*8192MB"),
											"shared_buffers = 4096MB")));
				}
				return in;
			}
		});
		tasks.add(new FromCvsUploadTask("server_app_config/" + serverType.toUpperCase() + "/pg_ident.conf", "HEAD",
				pgDir + "/data/pg_ident.conf", 0644, username, username));
		tasks.add(new FromCvsUploadTask("ServerLayers/LayersCommon/conf/usat_root.crt", "HEAD",
				pgDir + "/data/root.crt", 0644, username, username, false, false));
		tasks.add(new RenewFileCertTask(new Date(System.currentTimeMillis() + 180 * 24 * 60 * 60 * 1000L),
				pgDir + "/data/server.crt", pgDir + "/data/root.crt", pgDir + "/data/server.key", 0644, 0600, username,
				username));
	}

	protected static void addPostgresReplicaConfTask(List<DeployTask> tasks, Host host, String username, String pgDir)
			throws IOException {
		tasks.add(new RenewFileCertTask(new Date(System.currentTimeMillis() + 180 * 24 * 60 * 60 * 1000L),
				pgDir + "/data/client.crt", pgDir + "/data/client.key", 0644, 0600, "postgres", "postgres", null,
				"replica", "USATAuthenticatedSession", "replica"));
		String primaryHost = PgDeployMap.primaryMap.get(host.getSimpleName()).getSimpleName();
		BufferStream recoveryBufferStream = new HeapBufferStream();
		PrintWriter recoveryWriter = new PrintWriter(recoveryBufferStream.getOutputStream());
		recoveryWriter.println("standby_mode = 'on'");
		recoveryWriter.println("primary_conninfo = 'host=" + primaryHost
				+ " port=5432 user=replica sslmode=require sslcert=" + pgDir + "/data/client.crt sslkey=" + pgDir
				+ "/data/client.key sslrootcert=" + pgDir + "/data/root.crt'");
		recoveryWriter.flush();
		tasks.add(new UnixUploadTask(recoveryBufferStream.getInputStream(), pgDir + "/data", "recovery.conf", 0644,
				username, username, true));
	}

	/**
	 * @throws IOException
	 */
	public static void addMonitInstallTasks(List<DeployTask> tasks, Host host, String workDir) throws IOException {
		tasks.add(new OptionalTask("if [ ! -x /usr/monit-latest/bin/monit ]; then echo 'Y'; fi",
				Pattern.compile("^Y\\s*"), false,
				new FromCvsUploadTask("server_app_config/common/monit/monit-5.3.1-linux-x64.tar.gz", "HEAD",
						workDir + "/monit-5.3.1-linux-x64.tar.gz", 0644),
				new ExecuteTask("sudo su",
						"gunzip -c " + workDir + "/monit-5.3.1-linux-x64.tar.gz | tar -xv --no-same-owner -C /usr",
						"ln -s /usr/monit-5.3.1 /usr/monit-latest", "ln -s /usr/monit-latest/bin/monit /bin/monit",
						DeployUtils.getMakeDir("/opt/USAT/monit", "root", "root", 0755),
						DeployUtils.getMakeDir("/opt/USAT/monit/conf.d", "root", "root", 0755),
						DeployUtils.getMakeDir("/opt/USAT/monit/ssl", "root", "root", 0700),
						DeployUtils.getMakeDir("/opt/USAT/monit/logs", "root", "root", 0755),
						DeployUtils.getMakeDir("/opt/USAT/monit/bin", "root", "root", 0755),
						"/bin/cp /etc/pam.d/system-auth-ac /etc/pam.d/monit")));
		tasks.add(new FromCvsUploadTask("server_app_config/common/monit/check_unmonitored.sh", "HEAD",
				"/opt/USAT/monit/bin/check_unmonitored.sh", 0755, "root", "root", true));
		tasks.add(new FromCvsUploadTask("server_app_config/common/monit/check_app_status.sh", "HEAD",
				"/opt/USAT/monit/bin/check_app_status.sh", 0755, "root", "root", true));
		tasks.add(new FromCvsUploadTask("server_app_config/common/monit/check_openfiles.sh", "HEAD",
				"/opt/USAT/monit/bin/check_openfiles.sh", 0755, "root", "root", true));
		tasks.add(new FromCvsUploadTask("server_app_config/common/monit/kill_owned_by_user.sh", "HEAD",
				"/opt/USAT/monit/bin/kill_owned_by_user.sh", 0755, "root", "root", true));
		tasks.add(new FromCvsUploadTask("server_app_config/common/monit/check_memory_percent.sh", "HEAD",
				"/opt/USAT/monit/bin/check_memory_percent.sh", 0755, "root", "root", true));
		tasks.add(new FromCvsUploadTask("server_app_config/common/monit/check_inactivity.sh", "HEAD",
				"/opt/USAT/monit/bin/check_inactivity.sh", 0755, "root", "root", true));
		tasks.add(
				new UnixUploadTask(new EncodingInputStream(new StringReader("include /opt/USAT/monit/conf.d/*.conf\n")),
						"/etc/monitrc", 0700, "root", "root", true, "/etc/monitrc"));
		byte[] randomBytes = new byte[32];
		AbstractLinuxChangeSet.RANDOM.nextBytes(randomBytes);
		final Map<Pattern, String> replacements = new HashMap<Pattern, String>();
		replacements.put(Pattern.compile("\\<ENV_SUFFIX\\>"),
				("USA".equalsIgnoreCase(host.getServerEnv()) ? "" : "-" + host.getServerEnv().toLowerCase()));
		replacements.put(Pattern.compile("\\<PASSWORD\\>"), Base64.encodeBytes(randomBytes, true).replace('/', '_'));
		replacements.put(Pattern.compile("\\<EMAIL\\>"), "SoftwareDevelopmentTeam@usatech.com");
		if ("DEV".equalsIgnoreCase(host.getServerEnv()) || "INT".equalsIgnoreCase(host.getServerEnv()))
			replacements.put(Pattern.compile("\\<ALERT_FILTER\\>"), "on { instance }");
		else
			replacements.put(Pattern.compile("\\<ALERT_FILTER\\>"), "not on { action }");
		tasks.add(new FromCvsUploadTask("server_app_config/common/monit/base.conf", "HEAD",
				"/opt/USAT/monit/conf.d/base.conf.NEW", 0600, "root", "root", true) {
			@Override
			protected InputStream decorate(InputStream in) {
				return new EncodingInputStream(new ReplacementsLineFilteringReader(
						new BufferedReader(new InputStreamReader(in)), "\n", replacements));
			}
		});
		tasks.add(new FromCvsUploadTask("server_app_config/common/monit/system.conf", "HEAD",
				"/opt/USAT/monit/conf.d/system.conf", 0644, "root", "root", true));
		// cronUpdateTask.registerJob(Pattern.compile(".*" + appDir +
		// "/archivelogs\\.sh.*"), new int[] { 8 + (ordinal == null ? 1 : ordinal) * 8
		// }, null, null, null, null, appDir +
		// "/archivelogs.sh >> " + appDir + "/archivelogs.log 2>&1", "# Rename and gzip
		// rotated log files");
		tasks.add(new RenewFileCertTask(new Date(System.currentTimeMillis() + 180 * 24 * 60 * 60 * 1000L),
				"/opt/USAT/monit/ssl/monit.pem", "/opt/USAT/monit/ssl/monit.pem", 0700, 0700, "root", "root"));
		DeployTask monitConfUploadTask;
		/*
		 * Set<String> shutdownProcessList = new LinkedHashSet<String>();
		 * if(host.isServerType("MST") || host.isServerType("MSR"))
		 * shutdownProcessList.add("postgres"); if(host.isServerType("KLS")) { PGDATADir
		 * pgDirs = PgDeployMap.hostPGDATADirMap.get(host.getSimpleName());
		 * shutdownProcessList.add(pgDirs.getPrimaryDir().substring(pgDirs.getPrimaryDir
		 * ().lastIndexOf("/") + 1));
		 * shutdownProcessList.add(pgDirs.getStandbyDir().substring(pgDirs.getStandbyDir
		 * ().lastIndexOf("/") + 1)); } if(!shutdownProcessList.isEmpty()) { final
		 * HeapBufferStream monitConfExtra = new HeapBufferStream(); Writer writer = new
		 * OutputStreamWriter(monitConfExtra.getOutputStream()); try { writer.
		 * append("\npre-stop script\n    cp ~root/.monit.state ~root/.monit.state.save\n    if [ -n \"$UPSTART_STOP_EVENTS\" ]; then"
		 * ); for(String process : shutdownProcessList)
		 * writer.append("\n        /usr/monit-latest/bin/monit stop ").append(process);
		 * writer.
		 * append("\n    fi\nend script\npost-stop exec cp ~root/.monit.state.save ~root/.monit.state\n"
		 * ); writer.flush(); } finally { writer.close(); } monitConfUploadTask = new
		 * FromCvsUploadTask("server_app_config/common/monit/init-USAT-monit.conf",
		 * "HEAD", "USAT-monit.conf", 0640, "root", "root", true) {
		 * 
		 * @Override protected InputStream decorate(InputStream in) { return new
		 * ConcatenatingInputStream(in, monitConfExtra.getInputStream()); } }; } else
		 */
		monitConfUploadTask = new FromCvsUploadTask("server_app_config/common/monit/init-USAT-monit.conf", "HEAD",
				"USAT-monit.conf", 0640, "root", "root", true);
		tasks.add(monitConfUploadTask);
	}

	public static void addMonitRestartTasks(List<DeployTask> tasks, String workDir) {
		tasks.add(new ExecuteTask("sudo su",
				"if [ -f /opt/USAT/monit/conf.d/base.conf.NEW ]; then /bin/mv /opt/USAT/monit/conf.d/base.conf.NEW /opt/USAT/monit/conf.d/base.conf; fi",
				"if [ ! -f /etc/init/USAT/monit.conf ]; then /bin/cp USAT-monit.conf /etc/init/USAT/monit.conf; initctl start USAT/monit; elif diff -b "
						+ workDir + "/USAT-monit.conf /etc/init/USAT/monit.conf; then /bin/rm " + workDir
						+ "/USAT-monit.conf; initctl restart USAT/monit; else initctl stop USAT/monit; /bin/mv "
						+ workDir + "/USAT-monit.conf /etc/init/USAT/monit.conf; initctl start USAT/monit; fi",
				"/usr/monit-latest/bin/monit summary"));
	}

	public static void installUsaliveStaticFiles(Host host, List<DeployTask> tasks, String version, String workDir) {
		App app = USATRegistry.HTTPD_WEB;
		if (host.isServerType("WEB")) {// add web static tasks
			List<String> subcommands = new ArrayList<String>();
			subcommands.add("sudo su");
			USATRegistry.USALIVE_APP.setVersion(version);
			USATRegistry.HOTCHOICE_APP.setVersion(version);
			USATRegistry.VERIZON_APP.setVersion(version);
			for (App subApp : new App[] { USATRegistry.USALIVE_APP, USATRegistry.HOTCHOICE_APP,
					USATRegistry.VERIZON_APP }) {
				// for(App subApp:new App[]{USALIVE_APP}){
				DefaultPullTask subInstallFileTask = subApp.getRetrieveInstallFileTask();
				tasks.add(subInstallFileTask);
				tasks.add(new UploadTask(subInstallFileTask.getResultInputStream(), 0644, null, null,
						subApp.getInstallFileName(), true, workDir + '/' + subApp.getInstallFileName()));
				String subAppDir = "/opt/USAT/httpd/" + subApp.getName();
				subcommands.add(DeployUtils.getMakeDir(subAppDir, app.getUserName(), app.getUserName(), null));
				subcommands.add("/bin/rm -r " + subAppDir + "/*");
				Set<String> staticWebFiles = new HashSet<String>(Arrays.asList(
						new String[] { "web/*.pdf", "web/*.ico", "web/*.htm", "web/*.html", "web/*.css", "web/*.gif",
								"web/*.png", "web/*.jpg", "web/*.js", "web/*.swf", "web/*.xls", "web/*.csv" })); // TODO:
																													// may
																													// need
																													// to
																													// change
																													// this
																													// for
																													// dms
																													// /
																													// usalive
																													// /
																													// esuds
				StringBuilder sb = new StringBuilder();
				if (USATRegistry.USALIVE_APP.equals(subApp)) {
					subcommands.add(DeployUtils.getMakeDir(subAppDir + USATRegistry.USALIVE_CONTEXT_PATH,
							app.getUserName(), app.getUserName(), null));
					sb.append("gunzip -c ").append(workDir).append('/').append(subApp.getInstallFileName())
							.append(" | tar -C ").append(subAppDir).append(USATRegistry.USALIVE_CONTEXT_PATH)
							.append(" -xvp --strip-components=1");
					for (String staticWebFile : staticWebFiles)
						sb.append(' ').append(staticWebFile);
					subcommands.add(sb.toString());
					if (!USATRegistry.USALIVE_CONTEXT_PATH.trim().isEmpty())
						subcommands.add("/bin/cp --preserve " + subAppDir + USATRegistry.USALIVE_CONTEXT_PATH
								+ "/favicon.ico " + subAppDir);
					subcommands.add(new StringBuilder("gunzip -c ").append(subApp.getInstallFileName())
							.append(" | tar -C ").append(subAppDir).append(" -xvp version.txt").toString());
				} else if (USATRegistry.HOTCHOICE_APP.equals(subApp) || USATRegistry.VERIZON_APP.equals(subApp)) {
					subcommands.add(DeployUtils.getMakeDir(subAppDir + USATRegistry.USALIVE_CONTEXT_PATH,
							app.getUserName(), app.getUserName(), null));
					sb.append("gunzip -c ").append(workDir).append('/').append(subApp.getInstallFileName())
							.append(" | tar -C ").append(subAppDir).append(USATRegistry.USALIVE_CONTEXT_PATH)
							.append(" -xvp");
					subcommands.add(sb.toString());
					if (!USATRegistry.USALIVE_CONTEXT_PATH.trim().isEmpty())
						subcommands.add("/bin/cp --preserve " + subAppDir + USATRegistry.USALIVE_CONTEXT_PATH
								+ "/favicon.ico " + subAppDir);
				}
				subcommands.add("chown -R " + app.getUserName() + ':' + app.getUserName() + " " + subAppDir);
				subcommands.add("chmod -R u+rwX,go+rX,go-w " + subAppDir + USATRegistry.USALIVE_CONTEXT_PATH);
			}
			subcommands.add("/usr/local/apache2/bin/httpd -f /opt/USAT/httpd/conf/httpd.conf -k graceful");
			tasks.add(new ExecuteTask(subcommands.toArray(new String[subcommands.size()])));
		}
	}

	public static void downloadJavaFromCVS(Host host, PasswordCache cache, List<DeployTask> tasks,
			final int majorVersion, final int minorVersion, String workDir) {
		downloadJavaFromCVS(host, cache, tasks, majorVersion, minorVersion, -1, workDir);
	}

	public static void downloadJavaFromCVS(Host host, PasswordCache cache, List<DeployTask> tasks,
			final int majorVersion, final int minorVersion, final int securityVersion, String workDir) {
		if (majorVersion < 9) {
			final String fileName = "jdk-" + majorVersion + "u" + minorVersion + "-linux-x64.tar.gz";
			final String versionDir = "server_app_config/java/jdk" + majorVersion;
			StringBuilder sb = new StringBuilder();
			sb.append("/usr/java/jdk1.").append(majorVersion).append(".0_");
			if (minorVersion < 10) {
				StringUtils.appendPadded(sb, String.valueOf(minorVersion), '0', 2, Justification.RIGHT);
			} else {
				sb.append(minorVersion);
			}
			tasks.add(new FromCvsUploadTask(versionDir + '/' + fileName, "HEAD", workDir + "/" + fileName, 0755));
		} else {
			final String fileName = "jdk-" + majorVersion + "." + minorVersion + "." + securityVersion + "_linux-x64_bin.tar.gz";
			final String versionDir = "server_app_config/java/jdk-" + majorVersion;
			tasks.add(new FromCvsUploadTask(versionDir + '/' + fileName, "HEAD", workDir + "/" + fileName, 0755));
		}
	}

	public static void installJava(Host host, PasswordCache cache, List<DeployTask> tasks, final int majorVersion,
			final int minorVersion, String fileName, String workDir) {
		installJava(host, cache, tasks, majorVersion, minorVersion, -1, fileName, workDir);
	}

	public static void installJava(Host host, PasswordCache cache, List<DeployTask> tasks, final int majorVersion,
			final int minorVersion, final int securityVersion, String fileName, String workDir) {
		if (majorVersion < 9) {
			// clean up old Java versions
			tasks.add(new ExecuteTask("sudo su", "ls -1d /usr/java/jdk* | grep -v \"`ls -d -o /usr/jdk/latest | sed 's/.* -> //g'`\" | xargs -r /bin/rm -r"));
			StringBuilder sb = new StringBuilder();
			sb.append("/usr/java/jdk1.").append(majorVersion).append(".0_");
			if (minorVersion < 10) {
				StringUtils.appendPadded(sb, String.valueOf(minorVersion), '0', 2, Justification.RIGHT);
			} else {
				sb.append(minorVersion);
			}
			final String remoteDir = sb.toString();
			List<DeployTask> subtasks = new ArrayList<DeployTask>();
			subtasks.add(new ExecuteTask("sudo su", "cd /usr/java", "tar -xzf " + workDir + "/" + fileName,
					"chown -R root:root " + remoteDir, "unlink /usr/jdk/latest",
					"ln -s " + remoteDir + " /usr/jdk/latest", "/bin/rm -f " + workDir + "/" + fileName));
			subtasks.add(new ExecuteTask("sudo su",
					"cp " + workDir + "/local_policy.jar " + remoteDir + "/jre/lib/security",
					"cp " + workDir + "/US_export_policy.jar " + remoteDir + "/jre/lib/security"));
			if (host.isServerType("NET") || host.isServerType("WEB"))
				subtasks.add(new SyncWithJKSTask(remoteDir + "/jre/lib/security/cacerts",
						"/opt/USAT/conf/truststore.ts", cache, 0640, "__jdk__"));
			else if (host.isServerType("APP"))
				subtasks.add(
						new SyncWithJKSTask(remoteDir + "/jre/lib/security/cacerts", "/opt/USAT/conf/truststore.ts",
								cache, 0640, "__jdk__", Collections.singleton("addtrustexternalca")));
			tasks.add(new OptionalTask("if [ ! -x " + remoteDir + " ]; then echo 'Y'; fi", Pattern.compile("^Y\\s*"),
					false, subtasks.toArray(new DeployTask[subtasks.size()])));
		} else {
			// clean up old Java versions
			tasks.add(new ExecuteTask("sudo su", "ls -1d /usr/java/jdk* | grep -v \"`ls -d -o /usr/jdk/latest | sed 's/.* -> //g'`\" | xargs -r /bin/rm -r"));
			StringBuilder sb = new StringBuilder();
			sb.append("/usr/java/jdk-").append(majorVersion).append(".");
			sb.append(minorVersion);
			sb.append(".").append(securityVersion);
			final String remoteDir = sb.toString();
			List<DeployTask> subtasks = new ArrayList<DeployTask>();
			subtasks.add(new ExecuteTask("sudo su", "cd /usr/java", "tar -xzf " + workDir + "/" + fileName,
					"chown -R root:root " + remoteDir, "unlink /usr/jdk/latest",
					"ln -s " + remoteDir + " /usr/jdk/latest", "/bin/rm -f " + workDir + "/" + fileName));
			subtasks.add(new ExecuteTask("sudo su", 
					"cp " + workDir + "/local_policy.jar " + remoteDir + "/lib/security",
					"cp " + workDir + "/US_export_policy.jar " + remoteDir + "/lib/security"));
			if (host.isServerType("NET") || host.isServerType("WEB"))
				subtasks.add(new SyncWithJKSTask(remoteDir + "/lib/security/cacerts",
						"/opt/USAT/conf/truststore.ts", cache, 0640, "__jdk__"));
			else if (host.isServerType("APP"))
				subtasks.add(
						new SyncWithJKSTask(remoteDir + "/lib/security/cacerts", "/opt/USAT/conf/truststore.ts",
								cache, 0640, "__jdk__", Collections.singleton("addtrustexternalca")));
			tasks.add(new OptionalTask("if [ ! -x " + remoteDir + " ]; then echo 'Y'; fi", Pattern.compile("^Y\\s*"),
					false, subtasks.toArray(new DeployTask[subtasks.size()])));
		}
	}

	public static void installPostGIS(Host host, PasswordCache cache, List<DeployTask> tasks) {
		String env = host.getServerEnv().toUpperCase();
		tasks.add(new OptionalTask("sudo yum list installed postgis2_* || echo 'YES'",
				Pattern.compile(".*YES\\s*", Pattern.DOTALL), false,
				new ExecuteTask("sudo su", "pgVersion=`/usr/pgsql-latest/bin/pg_config --version | cut -d ' ' -f 2`",
						"pgVersionMajor=`cut -d '.' -f1 <<< $pgVersion`",
						"pgVersionMinor=`cut -d '.' -f2 <<< $pgVersion`",
						"if [ $pgVersionMinor -eq 2 ]; then pgVersionRel=7; else pgVersionRel=1; fi",
						"rpm -Uh --force http://yum.postgresql.org/$pgVersionMajor.$pgVersionMinor/redhat/rhel-6-x86_64/pgdg-redhat$pgVersionMajor$pgVersionMinor-$pgVersionMajor.$pgVersionMinor-$pgVersionRel.noarch.rpm; echo $? | egrep '0|1'",
						"rpm -Uh http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm; echo $? | egrep '0|1'",
						"rpm -Uh http://elgis.argeo.org/repos/6/elgis-release-6-6_0.noarch.rpm; echo $? | egrep '0|1'",
						"yum -y -q --enablerepo=pgdg$pgVersionMajor$pgVersionMinor upgrade postgresql$pgVersionMajor$pgVersionMinor",
						"yum -y -q --enablerepo=elgis,epel,pgdg$pgVersionMajor$pgVersionMinor install postgis2_$pgVersionMajor$pgVersionMinor",
						(env.equals("INT") || env.equals("ECC") ? "monit restart postgres1; monit restart postgres2"
								: "monit restart postgres"),
						"try=1; while [[ $try -lt 60 ]] && [[ `su - postgres -c '/usr/pgsql-latest/bin/pg_ctl status -D /opt/USAT/postgres/data' > /dev/null; echo $?` -eq 3 ]]; do sleep 1; ((try++)); done")));

	}

	public static String[] getPgNames(Host host) {
		if (host.isServerType("KLS") && !"LOCAL".equalsIgnoreCase(host.getServerEnv())) {
			int ord = host.getSimpleName().charAt(host.getSimpleName().length() - 1) - '0';
			int limit = ("DEV".equalsIgnoreCase(host.getServerEnv()) || "INT".equalsIgnoreCase(host.getServerEnv()) ? 2
					: 4);
			return new String[] { "postgres_" + ord, "postgres_" + ((ord % limit) + 1) };
		} else if ((host.isServerType("MST") || host.isServerType("MSR"))
				&& ("INT".equalsIgnoreCase(host.getServerEnv()) || "ECC".equalsIgnoreCase(host.getServerEnv()))) {
			return new String[] { "postgres1", "postgres2" };
		} else if (host.isServerType("PGS")) {
			return new String[] { "postgres", "postgres_stdby" };
		}
		return new String[] { "postgres" };
	}

	public static DeployTask createPgRestartTask(Host host) {
		List<String> commands = new ArrayList<>();
		commands.add("sudo su");
		addPgRestartCommands(host, commands);
		return new ExecuteTask(commands.toArray(new String[commands.size()]));
	}

	public static void addPgRestartCommands(Host host, List<String> commands) {
		if (host.isServerType("PGS")) {
			commands.add("for srvc in $(clustat | grep '" + host.getSimpleName()
					+ "-priv .*started' | sed -ne 's/ *service:\\([^ ]*\\) .*/\\1/p'); do clusvcadm -R $srvc; done");
		} else {
			String[] pgNames = getPgNames(host);
			for (String pgName : pgNames)
				commands.add("/usr/monit-latest/bin/monit restart " + pgName);
			for (String pgName : pgNames)
				commands.add("while [ `/usr/monit-latest/bin/monit summary | egrep -c \"Process '" + pgName
						+ "'.* (Initializing|pending)\"` -gt 0 ]; do sleep 1; done");
		}
	}

	public static void addPgStopCommands(Host host, List<String> commands) {
		if (host.isServerType("PGS")) {
			commands.add("for srvc in $(clustat | grep '" + host.getSimpleName()
					+ "-priv .*started' | sed -ne 's/ *service:\\([^ ]*\\) .*/\\1/p'); do clusvcadm -s $srvc; done");
		} else {
			String[] pgNames = getPgNames(host);
			for (String pgName : pgNames)
				commands.add("/usr/monit-latest/bin/monit stop " + pgName);
			for (String pgName : pgNames)
				commands.add("while [ `/usr/monit-latest/bin/monit summary | grep -c \"Process '" + pgName
						+ "'.* Running\"` -gt 0 ]; do sleep 1; done");
		}
	}

	public static DeployTask createUpgradePgMinorTask(Host host, String version) {
		List<String> commands = new ArrayList<>();
		commands.add("sudo su");
		addUpgradePgMinorCommands(host, commands, version);
		return new ExecuteTask(commands.toArray(new String[commands.size()]));
	}

	public static void addUpgradePgMinorCommands(Host host, List<String> commands, String version) {
		String[] parts = StringUtils.split(version, '.', 3);
		if (parts.length != 3)
			throw new IllegalArgumentException("Invalid version '" + version + "'");
		int rel;
		switch (parts[0] + '.' + parts[1]) {
		case "9.2":
			rel = 7;
			break;
		default:
			rel = 1;
			break;
		}
		commands.add("rpm -Uh --force http://yum.postgresql.org/" + parts[0] + '.' + parts[1]
				+ "/redhat/rhel-6-x86_64/pgdg-redhat" + parts[0] + parts[1] + '-' + parts[0] + '.' + parts[1] + '-'
				+ rel + ".noarch.rpm; echo $? | egrep '0|1'");
		commands.add("yum -y -q --enablerepo=pgdg" + parts[0] + parts[1] + " upgrade postgresql" + parts[0] + parts[1]);
	}

	public static void addInstallPgMajorBinariesTasks(Host host, String version, String workDir, List<DeployTask> tasks)
			throws IOException {
		String[] parts = StringUtils.split(version, '.', 3);
		if (parts.length != 3)
			throw new IllegalArgumentException("Invalid version '" + version + "'");
		String dash;
		if ("5".equals(parts[1]))
			dash = "2";
		else
			dash = "1";
		tasks.add(new ExecuteTask("sudo su",
				"rpm -Uh --force http://yum.postgresql.org/" + parts[0] + '.' + parts[1]
						+ "/redhat/rhel-6-x86_64/pgdg-redhat" + parts[0] + parts[1] + '-' + parts[0] + '.' + parts[1]
						+ '-' + dash + ".noarch.rpm; echo $? | egrep '0|1'",
				"yum -y -q --enablerepo=pgdg" + parts[0] + parts[1] + " install postgresql" + parts[0] + parts[1]
						+ "-server postgresql" + parts[0] + parts[1] + "-contrib",
				"unlink /usr/pgsql-latest", "ln -s /usr/pgsql-" + parts[0] + '.' + parts[1] + " /usr/pgsql-latest"));
		if (host.isServerType("PGS")) {
			USATHelper.addOdbcLinkTasks(host, null, tasks, workDir);
			tasks.add(new OptionalTask("if [ ! -x /usr/pgsql-latest/lib/pg_repack.so ]; then echo 'Y'; fi",
					Pattern.compile("^Y\\s*"), false,
					new FromCvsUploadTask("server_app_config/Postgresql/pg_repack/pg_repack-1.3.3.zip", "HEAD",
							"pg_repack-1.3.3.zip", 0644),
					new ExecuteTask("sudo su",
							// "yum -y install readline*",
							"if [ -d /home/`logname`/pg_repack-1.3.3 ]; then /bin/rm -r /home/`logname`/pg_repack-1.3.3; fi",
							"cd /home/`logname`", "unzip pg_repack-1.3.3.zip", "cd pg_repack-1.3.3",
							"export PATH=$PATH:/usr/pgsql-latest/bin", "make", "make install")));
		}
	}

	protected static void addStopPostgresCommands(Host host, String pgName, List<String> subcommands) {
		if (host.isServerType("PGS") && !"LOCAL".equalsIgnoreCase(host.getServerEnv())) {
			subcommands.add("su - postgres");
			subcommands.add("if [ -f /opt/USAT/" + pgName + "/data/PG_VERSION -a -f /opt/USAT/" + pgName
					+ "/data/postmaster.pid ]; then /usr/pgsql-latest/bin/pg_ctl -w -D /opt/USAT/" + pgName
					+ "/data stop -m fast; fi");
			subcommands.add("exit");
		} else {
			subcommands.add("/usr/monit-latest/bin/monit stop " + pgName
					+ "; while [ `/usr/monit-latest/bin/monit summary | grep -c \"Process '" + pgName
					+ "'.* Running\"` -ne 0 ]; do sleep 1; done;");
		}
	}

	protected static void addStartPostgresCommands(Host host, String pgName, List<String> subcommands) {
		if (host.isServerType("PGS") && !"LOCAL".equalsIgnoreCase(host.getServerEnv())) {
			subcommands.add("su - postgres");
			subcommands.add("/usr/pgsql-latest/bin/pg_ctl -w -D /opt/USAT/" + pgName + "/data start");
			subcommands.add("exit");
		} else {
			subcommands.add("/usr/monit-latest/bin/monit start " + pgName
					+ "; while [ `/usr/monit-latest/bin/monit summary | grep -c \"Process '" + pgName
					+ "'.* Running\"` -eq 0 ]; do sleep 1; done;");
		}
	}

	public static void addUpgradePgDataTasks(Host host, PasswordCache cache, List<DeployTask> tasks, String workDir,
			int port, String pgName) {
		String newConfPath = workDir + "/postgresql.conf";
		List<String> subcommands = new ArrayList<>();
		addUpgradePgDataCommands(host, pgName, true, newConfPath, subcommands, null);
		tasks.add(new OptionalTask(
				"if [ `/usr/pgsql-latest/bin/pg_config --version | cut -d ' ' -f 2 | cut -d '.' -f1-2` = `sudo cat /opt/USAT/"
						+ pgName + "/data/PG_VERSION` ]; then echo 'Y'; else echo 'N'; fi",
				Pattern.compile("N"), false,
				new FromCvsUploadTask(
						"server_app_config/" + host.getServerTypes().iterator().next() + "/postgresql.conf", "HEAD",
						newConfPath, 0644, "postgres", "postgres", true),
				new ExecuteTask(subcommands.toArray(new String[subcommands.size()]))));
	}

	public static void addUpgradePgDataCommands(Host host, String pgName, boolean shutdown, String newConfPath,
			List<String> subcommands, String notifyUser) {
		subcommands.add("sudo su");
		if (shutdown)
			addStopPostgresCommands(host, pgName, subcommands);
		subcommands.add("if [ -d /opt/USAT/" + pgName + "/old_data -a \\( -h /opt/USAT/" + pgName
				+ "/data -o -d /opt/USAT/" + pgName + "/data \\) ] && [ `ls -1 /opt/USAT/" + pgName
				+ "/data/PG_VERSION | wc -l` -gt 0 ]; then /bin/mv /opt/USAT/" + pgName + "/old_data /opt/USAT/"
				+ pgName + "/old_data_`date +%Y%m%d_%k%M%S`/; fi");
		subcommands.add("if [ -d /opt/USAT/" + pgName + "/data ] && [ `ls -1 /opt/USAT/" + pgName
				+ "/data | wc -l` -gt 0 ]; then mkdir /opt/USAT/" + pgName + "/old_data; /bin/mv /opt/USAT/" + pgName
				+ "/data/* /opt/USAT/" + pgName + "/old_data/; fi");
		subcommands.add("if [ -d /opt/USAT/" + pgName + "/new_data ] && [ \"`ls -ld /opt/USAT/" + pgName
				+ "/new_data | cut -d' ' -f3`\" != \"postgres\" ] ; then chown -R postgres:postgres /opt/USAT/" + pgName
				+ "/new_data; fi");
		subcommands.add("if [ `ls -1 /opt/USAT/" + pgName + "/new_data | wc -l` -gt 0 ]; then /bin/rm -r /opt/USAT/"
				+ pgName + "/new_data/*; fi");
		subcommands.add("chown postgres:postgres /opt/USAT/" + pgName + "/old_data");
		subcommands.add("chmod -R 700 /opt/USAT/" + pgName + "/old_data");
		subcommands.add("sudo su - postgres");
		subcommands.add("/usr/pgsql-latest/bin/initdb -D /opt/USAT/" + pgName + "/new_data");
		subcommands.add("/bin/cp /opt/USAT/" + pgName + "/old_data/*.conf /opt/USAT/" + pgName + "/new_data");
		subcommands.add("/bin/cp /opt/USAT/" + pgName + "/old_data/*.crt /opt/USAT/" + pgName + "/new_data");
		subcommands.add("/bin/cp /opt/USAT/" + pgName + "/old_data/*.key /opt/USAT/" + pgName + "/new_data");
		subcommands.add("/bin/cp \"" + newConfPath + "\" /opt/USAT/" + pgName + "/new_data/postgresql.conf");
		boolean hasRepack = host.isServerType("PGS");
		if (hasRepack) {
			subcommands.add("/usr/pgsql-`cat /opt/USAT/" + pgName
					+ "/old_data/PG_VERSION`/bin/pg_ctl -w -o '-p 50432 -c unix_socket_directories=/var/run/postgresql' -D /opt/USAT/"
					+ pgName + "/old_data start > /opt/USAT/" + pgName + "/logs/server.log 2>&1");
			subcommands.add("/usr/pgsql-`cat /opt/USAT/" + pgName
					+ "/old_data/PG_VERSION`/bin/psql main -p 50432 -c 'DROP EXTENSION IF EXISTS pg_repack CASCADE;'");
			subcommands.add("/usr/pgsql-`cat /opt/USAT/" + pgName
					+ "/old_data/PG_VERSION`/bin/pg_ctl -w -o '-p 50432' -D /opt/USAT/" + pgName
					+ "/old_data stop -m fast");
		}
		if (notifyUser != null) {
			subcommands.add("echo \"[`date`] pg_upgrade start\"| mail -s \"pg_upgrade start\" \"" + notifyUser + "\"");
		}
		subcommands.add("/usr/pgsql-latest/bin/pg_upgrade --link --jobs 8 -b /usr/pgsql-`cat /opt/USAT/" + pgName
				+ "/old_data/PG_VERSION`/bin -B /usr/pgsql-latest/bin -d /opt/USAT/" + pgName
				+ "/old_data -D /opt/USAT/" + pgName + "/new_data");
		subcommands.add("/usr/pgsql-latest/bin/pg_ctl -w -o '-p 50432' -D /opt/USAT/" + pgName
				+ "/new_data start > /opt/USAT/" + pgName + "/logs/server.log 2>&1");
		if (notifyUser != null) {
			subcommands.add(
					"echo \"[`date`] pg_upgrade done. start vacuumdb\"| mail -s \"pg_upgrade done. start vacuumdb\" \""
							+ notifyUser + "\"");
		}
		subcommands.add("vacuumdb -a -z -p 50432");
		if (notifyUser != null) {
			subcommands.add("echo \"[`date`] vacuumdb done\"| mail -s \"vacuumdb done\" \"" + notifyUser + "\"");
		}
		if (hasRepack)
			subcommands.add("/usr/pgsql-latest/bin/psql main -p 50432 -c 'CREATE EXTENSION pg_repack;'");
		subcommands.add("/usr/pgsql-latest/bin/pg_ctl -o '-p 50432' -D /opt/USAT/" + pgName + "/new_data stop -m fast");
		subcommands.add("exit");
		subcommands.add("if [ `ls -1 /opt/USAT/" + pgName + "/data | wc -l` -gt 0 ]; then /bin/rm -r /opt/USAT/"
				+ pgName + "/data/*; fi");
		subcommands.add("/bin/mv /opt/USAT/" + pgName + "/new_data/* /opt/USAT/" + pgName + "/data");

		if (shutdown)
			addStartPostgresCommands(host, pgName, subcommands);
	}
}
