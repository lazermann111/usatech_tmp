package com.usatech.tools.deploy;

import java.io.IOException;
import java.util.List;

import com.sshtools.j2ssh.SftpClient;
import com.sshtools.j2ssh.SshClient;
import com.sshtools.j2ssh.connection.Channel;
import com.sshtools.j2ssh.connection.ConnectionProtocol;
import com.sshtools.j2ssh.io.ByteArrayWriter;

public class ExtendedSshClient extends SshClient {
	public ConnectionProtocol getConnection() {
		return connection;
	}

	public SftpClient openSftpClient() throws IOException {
		SftpClient sftp = super.openSftpClient();
		boolean okay = false;
		try {
			List<?> list = getActiveChannels();
			Channel sftpChannel = (Channel) list.get(list.size() - 1);
			ByteArrayWriter baw = new ByteArrayWriter();
			baw.writeString("sudo su");
			connection.sendChannelRequest(sftpChannel, "exec", true, baw.toByteArray());
			okay = true;
			return sftp;
		} finally {
			if(!okay)
				sftp.quit();
		}
	}
}