/**
 * 
 */
package com.usatech.tools.deploy;

import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;

import simple.bean.ConvertUtils;
import simple.io.HeapBufferStream;
import simple.io.Interaction;
import simple.io.Log;
import simple.security.SecurityUtils;
import simple.security.crypt.CryptUtils;
import simple.util.CollectionUtils;

import com.sshtools.j2ssh.SftpClient;
import com.sshtools.j2ssh.SshClient;

public class RotateMessageChainEncryptionKeyTask implements DeployTask {
	private static final Log log = Log.getLog();
	protected static final DateFormat aliasSuffixFormat = (DateFormat) ConvertUtils.getFormat("DATE", "yyyyMMdd");
	protected final Date rotateDate;
	protected final String keystorePath;
	protected final String truststorePath;
	protected final int keystorePermissions;
	protected final int truststorePermissions;
	protected final ServerSet serverSet;
	protected boolean copyFromPeer;
	protected final Date cleanKeystoreDate;

	public RotateMessageChainEncryptionKeyTask(Date rotateDate, String keystorePath, String truststorePath, int keystorePermissions, int truststorePermissions, ServerSet serverSet, boolean copyFromPeer, Date cleanKeystoreDate) {
		this.rotateDate = rotateDate;
		this.keystorePath = keystorePath;
		this.truststorePath = truststorePath;
		this.keystorePermissions = keystorePermissions;
		this.truststorePermissions = truststorePermissions;
		this.serverSet = serverSet;
		this.copyFromPeer = copyFromPeer;
		this.cleanKeystoreDate = cleanKeystoreDate;
	}

	@Override
	public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
		Map<String, App> apps = new HashMap<String, App>();
		Map<String, SortedMap<String, Date>> appKeyMap = new HashMap<String, SortedMap<String, Date>>();
		Map<Host, DeployUtils.KeyStoreInfo> keyStoreHosts = new HashMap<Host, DeployUtils.KeyStoreInfo>();
		Set<Host> trustStoreHosts = new HashSet<Host>();
		Map<String, App> trustApps = new HashMap<String, App>();
		for(String serverType : deployTaskInfo.host.getServerTypes()) {
			List<Server> servers = serverSet.getServers(serverType);
			if(servers != null && !servers.isEmpty()) {
				for(Server server : servers) {
					if(!server.isOnHost(deployTaskInfo.host))
						keyStoreHosts.put(server.host, null);
					else if(server.apps != null) {
						for(App app : server.apps) {
							if(app.isUsingQueueLayer()) {
								apps.put(app.getName(), app);
								appKeyMap.put(app.getName(), new TreeMap<String, Date>());
							}
						}
					}
					Set<String> trustStoreServerTypes;
					if("APR".equalsIgnoreCase(serverType)) {
						trustStoreServerTypes = CollectionUtils.asSet("NET", "APR", "KLS");
					} else if("NET".equalsIgnoreCase(serverType)) {
						trustStoreServerTypes = CollectionUtils.asSet("APR", "NET", "KLS");
					} else if("APP".equalsIgnoreCase(serverType)) {
						trustStoreServerTypes = CollectionUtils.asSet("WEB", "APP", "KLS");
					} else if("WEB".equalsIgnoreCase(serverType)) {
						trustStoreServerTypes = CollectionUtils.asSet("APP", "WEB", "KLS");
					} else if("KLS".equalsIgnoreCase(serverType)) {
						trustStoreServerTypes = CollectionUtils.asSet("NET", "KLS", "APR");
					} else {
						trustStoreServerTypes = Collections.emptySet();
					}
					for(Server other : serverSet.getServers(trustStoreServerTypes)) {
						trustStoreHosts.add(other.host);
						if(other.apps!=null){
							if(server.apps!=null){
								for(App currentServerApp:server.apps){
									for(App a : other.apps){
										if(currentServerApp.getConsumeQueueLayerFrom().contains(a) || a.getConsumeQueueLayerFrom().contains(currentServerApp)) {
											trustApps.put(a.getName(), a);
										}
									}
								}
							}
						}
					}
				}
			}
		}
		if(apps.isEmpty())
			return "Messge Chain Certificates are up to date"; // only do this on first host
		
		String aliasSuffix = aliasSuffixFormat.format(new Date());
		char[] tmpKeyStorePassword = "SECRET".toCharArray();
		//Decode with password
		try {
			DeployUtils.KeyStoreInfo mainKsi = DeployUtils.createKeyStoreInfo(deployTaskInfo.sshClientCache, deployTaskInfo.host, keystorePath, "keystore", deployTaskInfo.interaction, deployTaskInfo.user, deployTaskInfo.passwordCache);
			DeployUtils.KeyStoreInfo trustKsi = DeployUtils.createKeyStoreInfo(deployTaskInfo.sshClientCache, deployTaskInfo.host, truststorePath, "truststore", deployTaskInfo.interaction, deployTaskInfo.user, deployTaskInfo.passwordCache);
			KeyStore tmpKeyStore = null;
			boolean updateTrustStore = false;
			int deletedCnt = 0;
			for(String appName : apps.keySet()) {
				// Clean up expired entries in keystore
				SortedSet<String> aliases = mainKsi.getAliases(appName);
				if(aliases.size() > 1) {
					Date newDate = new Date(System.currentTimeMillis() - 30 * 24 * 60 * 60 * 1000L);
					for(String altAlias : aliases.headSet(aliases.last())) {
						X509Certificate cert = (X509Certificate) mainKsi.getReadableKeyStore().getCertificate(altAlias);
						if(cert == null || !cleanKeystoreDate.after(cert.getNotAfter()) || cert.getNotBefore().after(newDate))
							break; // a good one so stop
						deletedCnt++;
						mainKsi.getWritableKeyStore().deleteEntry(altAlias);
					}
				}

				// Create new entry if necessary
				String prevAliasSuffix = mainKsi.getLastAliasSuffix(appName);
				if(prevAliasSuffix == null)
					prevAliasSuffix = "";
				else {
					X509Certificate cert = mainKsi.getLastCertificate(appName);
					if(cert != null && !rotateDate.after(cert.getNotAfter())) {
						X509Certificate trustCert = trustKsi.getLastCertificate(appName);
						if(trustCert == null || !cert.equals(trustCert)) {
							trustKsi.getWritableKeyStore().setCertificateEntry(mainKsi.getLastAlias(appName), cert);
							updateTrustStore = true;
						}
						continue;
					}
				}
				if(copyFromPeer) {
					//get from peer
					String lastPeerAliasSuffix = "";
					X509Certificate lastPeerCert = null;
					DeployUtils.KeyStoreInfo lastKsi = null;
					for(Map.Entry<Host, DeployUtils.KeyStoreInfo> keyStoreHostsEntry : keyStoreHosts.entrySet()) {
						DeployUtils.KeyStoreInfo ksi = keyStoreHostsEntry.getValue();
						if(ksi == null) {
							ksi = DeployUtils.createKeyStoreInfo(keyStoreHostsEntry.getKey(), keystorePath, "keystore", deployTaskInfo.interaction, deployTaskInfo.user, deployTaskInfo.sshClientCache, deployTaskInfo.passwordCache);
							keyStoreHostsEntry.setValue(ksi);
						}
						String peerAliasSuffix = ksi.getLastAliasSuffix(appName);
						if(peerAliasSuffix != null && lastPeerAliasSuffix.compareTo(peerAliasSuffix) < 0) {
							lastPeerAliasSuffix = peerAliasSuffix;
							lastPeerCert = ksi.getLastCertificate(appName);
							lastKsi = ksi;
						}
					}
					if(lastKsi != null && prevAliasSuffix.compareTo(lastPeerAliasSuffix) < 0) {
						prevAliasSuffix = lastPeerAliasSuffix;
						if(!rotateDate.after(lastPeerCert.getNotAfter())) {
							// found a good cert! copy to tmpKeyStore and continue;
							if(tmpKeyStore == null)
								tmpKeyStore = SecurityUtils.getKeyStore(null, "JKS", null, tmpKeyStorePassword);
							if(CryptUtils.copyKeystoreEntry("mce." + appName + ".data." + lastPeerAliasSuffix, lastKsi.getWritableKeyStore(), lastKsi.getPassword(), tmpKeyStore, tmpKeyStorePassword, null))
								continue;
						}
					}
				}
				// Calculate alias
				String alias;
				int c = prevAliasSuffix.compareTo(aliasSuffix);
				if(c < 0)
					alias = "mce." + appName + ".data." + aliasSuffix;
				else if(c > 0) { // this is not good
					int p = prevAliasSuffix.indexOf('-');
					if(p >= 0) {
						alias = "mce." + appName + ".data." + prevAliasSuffix.substring(0, p) + '-' + calcNext(prevAliasSuffix.substring(p + 1));
					} else {
						alias = "mce." + appName + ".data." + prevAliasSuffix + "-a";
					}
				} else { // this just means it was run more than once in a day
					alias = "mce." + appName + ".data." + aliasSuffix + "-a";
				}
				if(tmpKeyStore == null)
					tmpKeyStore = SecurityUtils.getKeyStore(null, "JKS", null, tmpKeyStorePassword);
				// Create pair
				App app = apps.get(appName);
				StringBuilder csr = new StringBuilder();
				KeyPair pair = CryptUtils.generateCSR("RSA", 2048, "CN=" + app.getServiceName() + ", O=USA Technologies\\, Inc, L=Malvern, S=PA, C=US", csr);
				InputStream certStream = DeployUtils.signCertificate(deployTaskInfo.interaction, deployTaskInfo.passwordCache, csr.toString());
				CryptUtils.importCertificateChain(tmpKeyStore, alias, pair, certStream, tmpKeyStorePassword);
			}
			int ksCertCnt = 0;
			int tsCertCnt = 0;
			if(tmpKeyStore != null || deletedCnt > 0) {
				int added;
				if(tmpKeyStore != null) {
					deployTaskInfo.interaction.readLine("New Certificates were created (" + tmpKeyStore.size() + "). Press any key to add these to the keystores and truststores");
					// first do our own
					added = CryptUtils.copyKeystoreEntries(tmpKeyStore, tmpKeyStorePassword, mainKsi.getWritableKeyStore(), mainKsi.getPassword(), null);
				} else
					added = 0;
				HeapBufferStream bufferStream = new HeapBufferStream();
				if(added > 0 || deletedCnt > 0) {
					bufferStream.clear();
					mainKsi.getWritableKeyStore().store(bufferStream.getOutputStream(), mainKsi.getPassword());
					UploadTask.writeFileUsingTmp(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, bufferStream.getInputStream(), keystorePermissions, "root", "usat", null, keystorePath);
					ksCertCnt += added;
				}
				if(tmpKeyStore != null) {
					// next do any other keystores
					for(Map.Entry<Host, DeployUtils.KeyStoreInfo> entry : keyStoreHosts.entrySet()) {
						DeployUtils.KeyStoreInfo ksi = entry.getValue();
						if(ksi != null && ksi.getWritableKeyStore() != null) {
							int cnt = CryptUtils.copyKeystoreEntries(tmpKeyStore, tmpKeyStorePassword, ksi.getWritableKeyStore(), ksi.getPassword(), null);
							if(cnt > 0) {
								// save it on source system
								bufferStream.clear();
								ksi.getWritableKeyStore().store(bufferStream.getOutputStream(), ksi.getPassword());
								UploadTask.writeFileUsingTmp(entry.getKey(), deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, bufferStream.getInputStream(), keystorePermissions, "root", "usat", null, keystorePath);
								ksCertCnt += cnt;
							}
						} else
							ksCertCnt += copyKeystoreEntriesToHost(tmpKeyStore, tmpKeyStorePassword, entry.getKey(), keystorePath, deployTaskInfo.interaction, deployTaskInfo.user, deployTaskInfo.sshClientCache, deployTaskInfo.passwordCache, null);
					}
					// next do any truststores
					for(Host host : trustStoreHosts) {
						if(!host.equals(deployTaskInfo.host))
							tsCertCnt += copyKeystoreEntriesToHost(tmpKeyStore, tmpKeyStorePassword, host, truststorePath, deployTaskInfo.interaction, deployTaskInfo.user, deployTaskInfo.sshClientCache, deployTaskInfo.passwordCache, CryptUtils.CERTIFICATES_ONLY_ENTRY_FILTER);
					}
				}
			}
			// make sure my truststore has all I need
			if(copyFromPeer) {
				tmpKeyStore = null;
				for(App trustApp : trustApps.values()) {
					String prevAliasSuffix = trustKsi.getLastAliasSuffix(trustApp.getName());
					if(prevAliasSuffix == null)
						prevAliasSuffix = "";
					else {
						X509Certificate cert = trustKsi.getLastCertificate(trustApp.getName());
						if(cert != null && !rotateDate.after(cert.getNotAfter()))
							continue;
					}
					// get from source
					String lastPeerAliasSuffix = "";
					X509Certificate lastPeerCert = null;
					DeployUtils.KeyStoreInfo lastKsi = null;
					for(Server trustServer : serverSet.getServers(trustApp.getServerType())) {
						if(!trustServer.isOnHost(deployTaskInfo.host)) {
							DeployUtils.KeyStoreInfo ksi = keyStoreHosts.get(trustServer.host);
							if(ksi == null) {
								ksi = DeployUtils.createKeyStoreInfo(trustServer.host, truststorePath, "truststore", deployTaskInfo.interaction, deployTaskInfo.user, deployTaskInfo.sshClientCache, deployTaskInfo.passwordCache);
								keyStoreHosts.put(trustServer.host, ksi);
							}
							String peerAliasSuffix = ksi.getLastAliasSuffix(trustApp.getName());
							if(peerAliasSuffix != null && lastPeerAliasSuffix.compareTo(peerAliasSuffix) < 0) {
								lastPeerAliasSuffix = peerAliasSuffix;
								lastPeerCert = ksi.getLastCertificate(trustApp.getName());
								lastKsi = ksi;
							}
						}
					}
					if(lastKsi != null && prevAliasSuffix.compareTo(lastPeerAliasSuffix) < 0) {
						prevAliasSuffix = lastPeerAliasSuffix;
						if(!rotateDate.after(lastPeerCert.getNotAfter())) {
							// found a good cert! copy to tmpKeyStore and continue;
							if(tmpKeyStore == null)
								tmpKeyStore = SecurityUtils.getKeyStore(null, "JKS", null, tmpKeyStorePassword);
							CryptUtils.copyKeystoreEntry("mce." + trustApp.getName() + ".data." + lastPeerAliasSuffix, lastKsi.getWritableKeyStore(), lastKsi.getPassword(), tmpKeyStore, tmpKeyStorePassword, CryptUtils.CERTIFICATES_ONLY_ENTRY_FILTER);
						}
					}
				}
				// Clean up expired entries in truststore
				Date cleanDate = new Date();
				for(String appName : trustKsi.getAppNames()) {
					SortedSet<String> aliases = trustKsi.getAliases(appName);
					if(aliases.size() > 1) {
						for(String alias : aliases.headSet(aliases.last())) {
							X509Certificate cert = (X509Certificate) trustKsi.getReadableKeyStore().getCertificate(alias);
							if(cert == null || !cleanDate.after(cert.getNotAfter()))
								break; // a good one so stop
							trustKsi.getWritableKeyStore().deleteEntry(alias);
							updateTrustStore = true;
						}
					}
				}

				if(updateTrustStore || tmpKeyStore != null) {
					int n;
					if(tmpKeyStore != null) {
						deployTaskInfo.interaction.readLine("New Certificates were copied (" + tmpKeyStore.size() + "). Press any key to add these to the truststore");
						// first do our own
						n = CryptUtils.copyKeystoreEntries(tmpKeyStore, tmpKeyStorePassword, trustKsi.getWritableKeyStore(), trustKsi.getPassword(), CryptUtils.CERTIFICATES_ONLY_ENTRY_FILTER);
					} else {
						n = 0;
					}
					if(updateTrustStore || n > 0) {
						HeapBufferStream bufferStream = new HeapBufferStream();
						trustKsi.getWritableKeyStore().store(bufferStream.getOutputStream(), trustKsi.getPassword());
						UploadTask.writeFileUsingTmp(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, bufferStream.getInputStream(), truststorePermissions, "root", "usat", null, truststorePath);
						tsCertCnt += n;
					}
				}
			}
			return ksCertCnt == 0 && tsCertCnt == 0 ? "Messge Chain Certificates are up to date" : "Updated " + ksCertCnt + " keystore entries and " + tsCertCnt + " truststore entries";
		} catch(GeneralSecurityException e) {
			throw new IOException(e);
		}
	}

	protected String calcNext(String previous) {
		char ch = previous.charAt(previous.length() - 1);
		if(ch < 'z') {
			if(ch >= 'a')
				return previous.substring(0, previous.length() - 1) + (char) (ch + 1);
			return previous.substring(0, previous.length() - 1) + 'a';
		}
		return previous + 'a';
	}


	protected int copyKeystoreEntriesToHost(KeyStore source, char[] sourcePassword, Host targetHost, String targetKeystorePath, Interaction interaction, String user, SshClientCache sshClientCache, PasswordCache passwordCache, CryptUtils.CopyEntryFilter filter) throws IOException, KeyStoreException, NoSuchAlgorithmException, UnrecoverableEntryException, NoSuchProviderException,
			CertificateException, InterruptedException {
		// start ssh and sftp
		SshClient ssh = sshClientCache.getSshClient(targetHost, user);
		try {
			log.info("Updating keystore '" + targetKeystorePath + "' on " + targetHost.getSimpleName() + "...");
			SftpClient sftp = ssh.openSftpClient();
			try {
				char[] password = DeployUtils.getPassword(passwordCache, targetHost, "keystore", "the keystore at '" + targetKeystorePath + "'", interaction);

				// Get the keystore stream
				HeapBufferStream bufferStream = new HeapBufferStream();
				InputStream in;
				if(UploadTask.checkFileUsingCmd(targetHost, passwordCache, interaction, sshClientCache, user, targetKeystorePath)) {
					interaction.printf("Retrieving contents of keystore at '%1$s'", targetKeystorePath);
					UploadTask.readFileUsingTmp(targetHost, passwordCache, interaction, sshClientCache, user, targetKeystorePath, bufferStream.getOutputStream(), null);
					in = bufferStream.getInputStream();
				} else {
					in = null;
				}
				// Decode with password
				KeyStore targetKeyStore = SecurityUtils.getKeyStore(in, "JKS", null, password);
				int result = CryptUtils.copyKeystoreEntries(source, sourcePassword, targetKeyStore, password, filter);
				if(result > 0) {
					// save keystore
					bufferStream.clear();
					targetKeyStore.store(bufferStream.getOutputStream(), password);
					UploadTask.writeFileUsingTmp(targetHost, passwordCache, interaction, sshClientCache, user, bufferStream.getInputStream(), keystorePermissions, "root", "usat", null, targetKeystorePath);
				}
				return result;
			} finally {
				sftp.quit();
			}
		} catch(IOException e) {
			interaction.printf("Error while updating keystore '%1$s' on %2$s because: %3$s", targetKeystorePath, targetHost.getSimpleName(), e.getMessage());
			throw new IOException("Could not update keystore '" + targetKeystorePath + "' on " + targetHost.getSimpleName(), e);
		} catch(InterruptedException e) {
			interaction.printf("Interrupted while updating keystore '%1$s' on %2$s", targetKeystorePath, targetHost.getSimpleName());
			throw e;
		}
	}

	@Override
	public String getDescription(DeployTaskInfo deployTaskInfo) {
		StringBuilder sb = new StringBuilder();
		sb.append("Rotating message chain certificate and private key in keystore '").append(keystorePath).append("'");
		return sb.toString();
	}
}