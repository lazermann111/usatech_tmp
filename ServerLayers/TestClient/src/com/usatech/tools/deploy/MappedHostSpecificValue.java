package com.usatech.tools.deploy;

import java.util.ArrayList;
import java.util.List;

import simple.io.Interaction;

public class MappedHostSpecificValue implements HostSpecificValue {
	protected static class MappedEntry {
		public final String serverEnv;
		public final String serverType;
		public final HostSpecificValue value;
		public MappedEntry(String serverEnv, String serverType, HostSpecificValue value) {
			this.serverEnv = serverEnv;
			this.serverType = serverType;
			this.value = value;
		}
	}
	protected final List<MappedEntry> values = new ArrayList<MappedEntry>();
	public void registerValue(String serverEnv, String serverType, String value) {
		registerValue(serverEnv, serverType, new DefaultHostSpecificValue(value));
	}

	public void registerValue(String serverEnv, String[] serverTypes, String value) {
		for(String serverType : serverTypes)
			registerValue(serverEnv, serverType, value);
	}
	public void registerValue(String serverEnv, String serverType, HostSpecificValue value) {
		values.add(new MappedEntry(serverEnv, serverType, value));
	}

	public void registerValue(String serverEnv, String[] serverTypes, HostSpecificValue value) {
		for(String serverType : serverTypes)
			registerValue(serverEnv, serverType, value);
	}

	@Override
	public String getValue(Host host, Interaction interaction) {
		for(MappedEntry entry : values) {
			if((entry.serverEnv == null || entry.serverEnv.equalsIgnoreCase(host.getServerEnv())) && (entry.serverType == null || host.isServerType(entry.serverType)))
				return entry.value == null ? null : entry.value.getValue(host, interaction);
		}
		return null;
	}

	@Override
	public boolean isOverriding() {
		for(MappedEntry entry : values) {
			if(entry.value != null && !entry.value.isOverriding())
				return false;
		}
		return true;
	}

}
