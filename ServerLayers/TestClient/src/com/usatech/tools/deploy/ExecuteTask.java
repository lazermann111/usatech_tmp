/**
 * 
 */
package com.usatech.tools.deploy;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CoderResult;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import simple.app.Processor;
import simple.app.ServiceException;
import simple.io.Interaction;
import simple.io.Log;
import simple.io.PassThroughWriter;
import simple.text.StringUtils;
import simple.util.CollectionUtils;
import simple.util.concurrent.TimedPipe;

import com.sshtools.j2ssh.SftpClient;
import com.sshtools.j2ssh.SshClient;
import com.sshtools.j2ssh.connection.Channel;
import com.sshtools.j2ssh.connection.ChannelEventListener;
import com.sshtools.j2ssh.session.SessionChannelClient;

public class ExecuteTask implements DeployTask {
	private static final Log log = Log.getLog();
	protected static final char[] newlines = "\r\n".toCharArray();
	protected static final String CHECK_SUCCESS_CMD = "echo \"[$?]\"";
	protected static final Pattern CHECK_SUCCESS_PATTERN = Pattern.compile("\\s*\\[(\\d+)\\]\\s*");
	protected static final BitSet DEFAULT_COMMAND_SUCCESS = new BitSet();
	static {
		DEFAULT_COMMAND_SUCCESS.set(0);
	}

	protected static class SessionInteraction_NEW extends SessionInteraction {
		protected final AtomicReference<Future<String>> inputFutureRef = new AtomicReference<Future<String>>();
		protected final TimedPipe pipe;
		protected final ByteBuffer inBuffer = ByteBuffer.allocate(1024);
		protected final CharBuffer outBuffer = CharBuffer.allocate(1024 * 1024);
		protected final CharsetDecoder decoder = Charset.defaultCharset().newDecoder();
		protected int limit = 0;

		public SessionInteraction_NEW(Host host, PasswordCache passwordCache, SessionChannelClient session, Writer echoLogger, OutputFilter outputFilter) throws IOException {
			super(host, passwordCache, session, echoLogger, outputFilter);
			this.pipe = new TimedPipe();
		}

		public void onDataReceived(Channel channel, byte[] data) {
			try {
				pipe.write(ByteBuffer.wrap(data), 0);
			} catch(IOException e) {
				log.error("Could not write to TimedPipe", e);
			}
			Future<String> inputFuture = inputFutureRef.get();
			if(inputFuture != null)
				inputFuture.cancel(true);
		}

		protected void handlePassword(Interaction interaction, Processor<String, String> interactiveInput, String prompt, boolean remember) throws ServiceException, IOException {
			char[] pass;
			if(passwordCache != null && remember) {
				pass = passwordCache.getPassword(host, prompt);
				if(pass == null) {
					pass = interaction.readPassword("");
					passwordCache.setPassword(host, prompt, pass);
				}
			} else if(interactiveInput != null) {
				String input = interactiveInput.process(prompt);
				pass = (input != null ? input.toCharArray() : null);
			} else {
				pass = interaction.readPassword("");
			}
			// Use rawWriter so password is not echoed.
			if(pass != null && pass.length > 0) {
				rawWriter.write(pass);
				switch(pass[pass.length - 1]) {
					case '\n':
					case '\r':
						break;
					default:
						rawWriter.write('\n');
				}
			} else {
				rawWriter.write('\n');
			}
			rawWriter.flush();
		}

		public String sendCommand(String cmd, long idleTime, Interaction interaction, Processor<String, String> interactiveInput) throws IOException, InterruptedException, ServiceException {
			if(initialized.compareAndSet(false, true)) {
				if(!session.requestPseudoTerminal(DeployMgr.deployerTerminal))
					throw new IOException("The server failed to allocate a pseudo terminal");
				if(!session.startShell())
					throw new IOException("The server failed to start a shell");
				sendCommand(null, 30000, interaction, null);
			}
			if(cmd != null) {
				cmd = cmd.trim();
				writer.write(cmd);
				writer.write('\n');
				writer.flush();
			}
			int last = 0;
			int offset = 0;
			String line = null;
			OUTER: while(true) {
				int r = pipe.read(inBuffer, idleTime);
				if(r == 0) {
					String input;
					if(interactiveInput != null) {
						input = interactiveInput.process(line);
						if(input == null || input.length() == 0)
							input = awaitInput(interaction);
					} else
						input = awaitInput(interaction);
					if(input != null && input.length() > 0) {
						writer.write(input);
						switch(input.charAt(input.length() - 1)) {
							case '\n':
							case '\r':
								break;
							default:
								writer.write('\n');
						}
						writer.flush();
					}
					continue;
				}
				int prev = outBuffer.position();
				while(true) { // decode loop
					inBuffer.flip();
					CoderResult cr = decoder.decode(inBuffer, outBuffer, false);
					inBuffer.compact();
					if(outBuffer.position() > prev) {
						if(cmd != null && outBuffer.position() >= cmd.length() + 1 && prev < cmd.length() + 3) {
							int w = 0;
							for(; w < outBuffer.position(); w++)
								if(!Character.isWhitespace(outBuffer.get(w)))
									break;
							if(outBuffer.position() >= cmd.length() + w + 1) {
								int m = regionsMatch(outBuffer, w, cmd);
								if(m == 0)
									log.info("Could not find echoed command '" + cmd + "'");
								else if(m > 0) {
									char ch = outBuffer.get(m);
									switch(ch) {
										case '\n':
										case '\r':
											if(echoLogger != null)
												try {
													echoLogger.write(ch);
												} catch(IOException e) {
													// ignore
												}
											m++;
											if(m >= outBuffer.position())
												continue OUTER;
											char ch2 = outBuffer.get(m);
											if(ch2 != ch && (ch2 == '\n' || ch2 == '\r')) {
												m++;
												if(echoLogger != null)
													try {
														echoLogger.write(ch2);
													} catch(IOException e) {
														// ignore
													}
											}
											last = m;
											offset = last;
											prev = last;
											cmd = null;
											if(last >= outBuffer.position())
												continue OUTER;
											break;
										default:
											log.info("Could not find echoed command '" + cmd + "'");
									}
								}
							}
						}
						
						if(echoLogger != null)
							try {
								String l = new String(outBuffer.array(), prev, outBuffer.position() - prev);
								if(outputFilter != null)
									l = outputFilter.filter(l);
								echoLogger.write(l);
							} catch(IOException e) {
								// ignore
							}
					}
					if(cr.isUnderflow())
						break;
					if(cr.isOverflow()) {
						if(last == 0) {
							if(echoLogger != null)
								try {
									echoLogger.write(outBuffer.array(), 0, outBuffer.capacity() / 2);
									echoLogger.flush();
								} catch(IOException e) {
									// ignore
								}
							// trash half the buffer then continue
							outBuffer.position(outBuffer.capacity() / 2);
						}

						outBuffer.limit(outBuffer.position());
						outBuffer.position(last);
						outBuffer.compact();
						last = 0;
						offset = 0;
						prev = 0;
					}
					if(cr.isError() || cr.isMalformed() || cr.isUnmappable())
						decoder.reset();
				}

				int oldLast = last;
				last = outBuffer.position();
				// ignore trailing newlines
				for(; last > prev; last--) {
					char ch = outBuffer.get(last - 1);
					switch(ch) {
						case '\n':
						case '\r':
							continue;
					}
					break;
				}
				// find the last line
				LAST: for(; last > prev; last--) {
					char ch = outBuffer.get(last - 1);
					switch(ch) {
						case '\n':
						case '\r':
							break LAST;
					}
				}
				if(last > 0 && last <= prev) {
					char ch = outBuffer.get(last - 1);
					switch(ch) {
						case '\n':
						case '\r':
							// all good - we found the newline at the end of the previously read data
							break;
						default:
							last = oldLast;
					}
				}
				line = new String(outBuffer.array(), last, outBuffer.position() - last);
				// log.info("Read " + r + " bytes; CharBuffer at " + outBuffer.position() + "; prev=" + prev + "; last=" + last + "; line='" + line + "'");
				StringBuilder promptBuilder = new StringBuilder();
				if(isPasswordPrompt(line, promptBuilder)) {
					prepareBuffer();
					handlePassword(interaction, interactiveInput, promptBuilder.toString(), true);
					last = 0;
				} else if(isPasscodePrompt(line, promptBuilder)) {
					prepareBuffer();
					handlePassword(interaction, interactiveInput, promptBuilder.toString(), false);
					last = 0;
				} else if(isPrompt(line)) {
					for(; last > 0; last--) {
						char ch = outBuffer.get(last - 1);
						switch(ch) {
							case '\n':
							case '\r':
								continue;
						}
						break;
					}
					String result = offset >= last ? "" : new String(outBuffer.array(), offset, last - offset);
					prepareBuffer();
					return result;
				}
			}
		}

		// -1 = need more input, 0 = no, 1 = yes
		protected int regionsMatch(CharBuffer cs1, int start1, CharSequence cs2) {
			int start2 = 0;
			int i = 0;
			for(; i + start2 < cs2.length() && i + start1 < cs1.position(); i++) {
				char ch1 = cs1.get(i + start1);
				char ch2 = cs2.charAt(i + start2);
				if(ch1 == '\r')
					ch1 = '\n';
				if(ch2 == '\r')
					ch2 = '\n';
				if(ch1 != ch2)
					return 0;
				if(ch1 == '\n') {
					while(i + start1 + 1 < cs1.position()) {
						switch(cs1.get(i + start1 + 1)) {
							case '\n':
							case '\r':
								start1++;
								continue;
						}
						break;
					}
					while(i + start2 + 1 < cs2.length()) {
						switch(cs2.charAt(i + start2 + 1)) {
							case '\n':
							case '\r':
								start2++;
								continue;
						}
						break;
					}
				}
			}
			if(i + start2 < cs2.length())
				return -1;
			return i + start1;
		}

		protected void prepareBuffer() {
			outBuffer.clear();
			decoder.reset();
			if(echoLogger != null)
				try {
					echoLogger.flush();
				} catch(IOException e) {
					// ignore
				}
		}

		public void close() {
			pipe.close();
		}

		protected String awaitInput(Interaction interaction) {
			Future<String> inputFuture = interaction.getLineFuture("");
			inputFutureRef.set(inputFuture);
			try {
				return inputFuture.get();
			} catch(InterruptedException e) {
				return null;
			} catch(ExecutionException e) {
				return null;
			} catch(CancellationException e) {
				return null;
			} finally {
				inputFutureRef.set(null);
			}
		}
	}

	protected static class SessionInteraction_OLD extends SessionInteraction {
		protected Future<String> inputFuture = null;
		protected boolean password;
		protected boolean remember;
		protected String command;
		protected String prompt;
		protected StringBuilder buffer = new StringBuilder();
		protected final ReentrantLock lock = new ReentrantLock();
		protected final Condition signal = lock.newCondition();

		public SessionInteraction_OLD(Host host, PasswordCache passwordCache, SessionChannelClient session, Writer echoLogger, OutputFilter outputFilter) {
			super(host, passwordCache, session, echoLogger, outputFilter);
		}

		public void onDataReceived(Channel channel, byte[] data) {
	    	String s = new String(data);
	    	lock.lock();
			try {
				lastReceived = System.currentTimeMillis();
				if(buffer == null) {
					log.warn("After session prompt was found, received data from Channel " + channel.getName() + ":\n" + s);
					return;
				}
				if(outputFilter != null)
					s = outputFilter.filter(s);
				buffer.append(s);
				int pos = StringUtils.lastIndexOf(buffer, newlines);
	        	String line;
	        	if(pos < 0)
	        		line = buffer.toString();
				else {
	        		line = buffer.substring(pos+1);
					if(pos > 0 && CollectionUtils.iterativeSearch(newlines, buffer.charAt(pos - 1)) >= 0)
						pos--;
					if(command != null && pos >= command.length()) {
						// XXX: do we need to account for possibility that newlines precede echoed command?
						if(StringUtils.regionsMatch(buffer, 0, command, 0, command.length())) {
							int end = command.length();
							for(int i = 0; i < 2 && CollectionUtils.iterativeSearch(newlines, buffer.charAt(end)) >= 0; i++)
								end++;
							buffer.delete(0, end);
							pos -= end;
						}
						command = null;
					}
				}
				if(echoLogger != null)
					try {
						echoLogger.write(s);
					} catch(IOException e) {
						// ignore
					}
				prompt = null;
				StringBuilder promptBuilder = new StringBuilder();
				if(isPasswordPrompt(line, promptBuilder)) {
					password = true;
					remember = true;
					prompt = promptBuilder.toString();
					prepareForInput(pos);
				} else if(isPasscodePrompt(line, promptBuilder)) {
					password = true;
					remember = false;
					prompt = promptBuilder.toString();
					prepareForInput(pos);
				} else if(isPrompt(line)) {
					password = false;
					remember = false;
					prepareForInput(pos);
				}
			} finally {
				lock.unlock();
			}
	        log.debug("<-- Received from Channel " + channel.getName() + ":\n" +  s);
	    }

		protected void prepareForInput(int pos) {
			if(pos < 0)
				buffer.setLength(0);
			else
				buffer.setLength(pos);
			buffer = null;
			if(echoLogger != null)
				try {
					echoLogger.flush();
				} catch(IOException e) {
					// ignore
				}
			if(inputFuture != null)
				inputFuture.cancel(true);
			signal.signal();
		}

		public String sendCommand(String cmd, long idleTime, Interaction interaction, Processor<String, String> interactiveInput) throws IOException, InterruptedException, ServiceException {
			if(initialized.compareAndSet(false, true)) {
				if(!session.requestPseudoTerminal(DeployMgr.deployerTerminal))
					throw new IOException("The server failed to allocate a pseudo terminal");
				lock.lockInterruptibly();
				try {
					if(!session.startShell())
						throw new IOException("The server failed to start a shell");
					signal.await();
				} finally {
					lock.unlock();
				}
			}
			StringBuilder myBuffer = new StringBuilder();
			lock.lockInterruptibly();
			try {
				buffer = myBuffer;
				command = cmd;
			} finally {
				lock.unlock();
			}
			//writer.flush() and onDataReceived() may deadlock on each other if we lock on lock for the whole of this method
			long now = System.currentTimeMillis();
			lastReceived = now;
			long time = idleTime;
			writer.write(cmd);
	    	writer.write('\n');
	    	writer.flush();
	    	String line;
	    	while(true) {
	    		lock.lockInterruptibly();
	    		try {
					if(buffer != null) {
						if(time > 0)
							signal.await(time, TimeUnit.MILLISECONDS);
						else
							signal.await();
					}
					if(buffer == null) {
						String result = myBuffer.toString();
						if(password) {
							char[] pass;
							if(passwordCache != null && remember) {
								pass = passwordCache.getPassword(host, prompt);
								if(pass == null) {
									pass = interaction.readPassword("");
									passwordCache.setPassword(host, prompt, pass);
								}
							} else if(interactiveInput != null) {
								String input = interactiveInput.process(prompt);
								pass = (input != null ? input.toCharArray() : null);
							} else {
								pass = interaction.readPassword("");
							}
							// Use rawWriter so password is not echoed.
							if(pass != null && pass.length > 0) {
								rawWriter.write(pass);
								switch(pass[pass.length - 1]) {
									case '\n':
									case '\r':
										break;
									default:
										rawWriter.write('\n');
								}
							} else {
								rawWriter.write('\n');
							}
							rawWriter.flush();
							myBuffer.setLength(0);
							buffer = myBuffer;
							continue;
						}
						return result;
					}
					now = System.currentTimeMillis();
					time = (lastReceived + idleTime - now);
					if(time > 0)
		        		continue;
					int lastPos = myBuffer.length();
		        	int pos;
		        	do {
		        		pos = StringUtils.lastIndexOf(myBuffer, new char[] {10,13}, --lastPos);
		        	} while(pos == lastPos) ;
		        	if(pos < 0)
		        		line = myBuffer.toString();
		        	else
		        		line = myBuffer.substring(pos+1);
		        	if(echoLogger != null)
						try {
							echoLogger.flush();
						} catch(IOException e) {
							//ignore
						}
	    		} finally {
					lock.unlock();
				}
	    		String input;
				if(interactiveInput != null) {
					input = interactiveInput.process(line);
					if(input == null || input.length() == 0)
						input = awaitInput(interaction);
				} else
					input = awaitInput(interaction);
	        	if(input != null && input.length() > 0) {			        		
	        		writer.write(input);
	        		switch(input.charAt(input.length() - 1)) {
	        			case '\n':case '\r':
	        				break;
	        			default:
	        				writer.write('\n');
	        		}
	            	writer.flush();
	        	}
	        	time = idleTime;
	    	} 
		}

		protected String awaitInput(Interaction interaction) {
			try {
				lock.lockInterruptibly();
			} catch(InterruptedException e) {
				return null;
			}
			try {
				inputFuture = interaction.getLineFuture("");
			} finally {
				lock.unlock();
			}
			try {
				return inputFuture.get();
			} catch(InterruptedException e) {
				return null;
			} catch(ExecutionException e) {
				return null;
			} finally {
				lock.lock();
				try {
					inputFuture = null;
				} finally {
					lock.unlock();
				}
			}
		}
	}

	protected static abstract class SessionInteraction implements ChannelEventListener {
		protected Pattern promptPattern = Pattern.compile("(?:[\\w.-]*|\\[\\w+@\\w+ [^\\]]+\\])[$#] ");
		protected Pattern passwordPromptPattern = Pattern.compile("(\\[sudo\\] password for \\w+: )|(.*[Pp]assword:\\s*)|(.*[Pp]assphrase(\\s+\\w+)*:\\s*)");
		protected Pattern passcodePromptPattern = Pattern.compile("(.*PASSCODE(\\s+\\w+)*:\\s*)");
		protected final Writer writer;
		protected final Writer rawWriter;
		protected final Writer echoLogger;
		protected final OutputFilter outputFilter;
		protected long lastReceived;
		protected final Host host;
		protected final PasswordCache passwordCache;
		protected final AtomicBoolean initialized = new AtomicBoolean();
		protected final SessionChannelClient session;

		public SessionInteraction(Host host, PasswordCache passwordCache, SessionChannelClient session, Writer echoLogger, OutputFilter outputFilter) {
			this.host = host;
			this.passwordCache = passwordCache;
			if(echoLogger != null) {
				this.echoLogger = echoLogger;
				this.rawWriter = new OutputStreamWriter(session.getOutputStream());
				this.writer = new PassThroughWriter(rawWriter, echoLogger);
			} else {
				this.echoLogger = null;
				this.writer = new OutputStreamWriter(session.getOutputStream());
				this.rawWriter = this.writer;
			}
			this.outputFilter = outputFilter;
			session.addEventListener(this);
			this.session = session;
		}

		public abstract String sendCommand(String cmd, long idleTime, Interaction interaction, Processor<String, String> interactiveInput) throws IOException, InterruptedException, ServiceException;

		public void close() {

		}
		public void onChannelOpen(Channel channel) {
			log.debug("+++ Channel " + channel.getName() + " opened +++");
		}

		public void onChannelClose(Channel channel) {
			log.debug("--- Channel " + channel.getName() + " closed ---");
		}

		public void onChannelEOF(Channel channel) {
			log.debug("--- Channel " + channel.getName() + " ended ---");
		}

		public void onDataSent(Channel channel, byte[] data) {
			// Do nothing
		}

		protected boolean isPrompt(String line) {
			return promptPattern.matcher(line).matches();
		}

		protected boolean isPasswordPrompt(String line, StringBuilder prompt) {
			Matcher matcher = passwordPromptPattern.matcher(line);
			if(!matcher.matches())
				return false;
			for(int i = 1; i <= matcher.groupCount(); i++) {
				String g = matcher.group(i);
				if(g != null)
					prompt.append(g);
			}
			return true;
		}

		protected boolean isPasscodePrompt(String line, StringBuilder prompt) {
			Matcher matcher = passcodePromptPattern.matcher(line);
			if(!matcher.matches())
				return false;
			for(int i = 1; i <= matcher.groupCount(); i++) {
				String g = matcher.group(i);
				if(g != null)
					prompt.append(g);
			}
			return true;
		}
	}

	protected final List<String> commands = new ArrayList<String>();
	protected final Processor<String, String> interactiveInput;
	protected final long idleTime;
	protected final OutputFilter outputFilter;
	protected String description;
	protected final BitSet commandSuccess = (BitSet) DEFAULT_COMMAND_SUCCESS.clone();

	public ExecuteTask(String... commands) {
		this(2000, null, commands);
	}
	public ExecuteTask(long idleTime, Processor<String, String> interactiveInput, String... commands) {
		this(idleTime, interactiveInput, null, commands);
	}

	public ExecuteTask(OutputFilter outputFilter, String... commands) {
		this(2000, null, outputFilter, commands);
	}

	public ExecuteTask(long idleTime, Processor<String, String> interactiveInput, OutputFilter outputFilter, String... commands) {
		this.idleTime = idleTime;
		this.interactiveInput = interactiveInput;
		this.outputFilter = outputFilter;
		addCommands(commands);
	}
	@Override
	public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
		executeCommands(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.getSsh(), idleTime, getInteractiveInput(deployTaskInfo), outputFilter, commandSuccess, getCommands());
		return "Executed " + commands.size() + " commands";
	}	

	public static Collection<String> executeCommands(Host host, PasswordCache passwordCache, Interaction interaction, SshClient ssh, Collection<String> commands) throws IOException, InterruptedException {
		return Arrays.asList(executeCommands(host, passwordCache, interaction, ssh, commands.toArray(new String[commands.size()])));
	}

	public static Collection<String> executeCommands(DeployTaskInfo taskInfo, Collection<String> commands) throws IOException, InterruptedException {
		return Arrays.asList(executeCommands(taskInfo.host, taskInfo.passwordCache, taskInfo.interaction, taskInfo.getSsh(), commands.toArray(new String[commands.size()])));
	}

	public static String[] executeCommands(DeployTaskInfo taskInfo, String... commands) throws IOException, InterruptedException {
		return executeCommands(taskInfo.host, taskInfo.passwordCache, taskInfo.interaction, taskInfo.getSsh(), commands);
	}
	public static String[] executeCommands(Host host, PasswordCache passwordCache, Interaction interaction, SshClient ssh, String... commands) throws IOException, InterruptedException {
		return executeCommands(host, passwordCache, interaction, ssh, DEFAULT_COMMAND_SUCCESS, commands);
	}

	public static String[] executeCommands(Host host, PasswordCache passwordCache, Interaction interaction, SshClient ssh, BitSet commandSuccess, String... commands) throws IOException, InterruptedException {
		SessionChannelClient ssc = ssh.openSessionChannel();
    	try {
			String[] result = new String[commands.length];
			SessionInteraction si = createSessionInteraction(host, passwordCache, ssc, interaction.getWriter(), null);
			try {
				for(int i = 0; i < commands.length; i++) {
					result[i] = executeCommand(interaction, si, commands[i]);
					if(!StringUtils.isBlank(CHECK_SUCCESS_CMD)) {
						String check = executeCommand(interaction, si, CHECK_SUCCESS_CMD);
						Matcher matcher = CHECK_SUCCESS_PATTERN.matcher(check);
						if(!matcher.matches())
							throw new IOException("Check Success Command returned unexpected result: '" + check + "'");
						int resultCode = Integer.parseInt(matcher.group(1));
						if(!commandSuccess.get(resultCode & 0xFF))
							throw new IOException("Command '" + commands[i] + "' exitted with status " + resultCode + ": " + result[i]);
					}
				}
			} finally {
				si.close();
			}
			return result;
		} catch(ServiceException e) {
			throw new IOException(e);
		} finally {
    		ssc.close();
    	}
	}

	protected static SessionInteraction createSessionInteraction(Host host, PasswordCache passwordCache, SessionChannelClient session, Writer echoLogger, OutputFilter outputFilter) throws IOException {
		SessionInteraction si = new SessionInteraction_NEW(host, passwordCache, session, echoLogger, outputFilter);
		return si;
	}
	// This does not work
	// *
	public static SftpClient getPrivilegedSftp(Host host, PasswordCache passwordCache, Interaction interaction, SshClient ssh) throws IOException, InterruptedException {
		final SessionChannelClient ssc = ssh.openSessionChannel();
		String result = null;
		try {
			SessionInteraction si = createSessionInteraction(host, passwordCache, ssc, interaction.getWriter(), null);
			try {
				result = executeCommand(interaction, si, "sudo su");
			} finally {
				si.close();
			}
			return new SftpClient(ssh) {
				@Override
				public void quit() throws IOException {
					try {
						super.quit();
					} finally {
						ssc.close();
					}
				}
			};
		} catch(ServiceException e) {
			throw new IOException(e);
		} finally {
			if(result == null)
				ssc.close();
		}
	}

	// */
	public static Collection<String> executeCommands(Host host, PasswordCache passwordCache, Interaction interaction, SshClient ssh, long idleTime, Processor<String, String> interactiveInput, Collection<String> commands) throws IOException, InterruptedException {
		return Arrays.asList(executeCommands(host, passwordCache, interaction, ssh, idleTime, interactiveInput, commands.toArray(new String[commands.size()])));
	}

	public static String[] executeCommands(Host host, PasswordCache passwordCache, Interaction interaction, SshClient ssh, long idleTime, Processor<String, String> interactiveInput, String... commands) throws IOException, InterruptedException {
		return executeCommands(host, passwordCache, interaction, ssh, idleTime, interactiveInput, null, commands);
	}

	public static String[] executeCommands(Host host, PasswordCache passwordCache, Interaction interaction, SshClient ssh, long idleTime, Processor<String, String> interactiveInput, OutputFilter outputFilter, String... commands) throws IOException, InterruptedException {
		return executeCommands(host, passwordCache, interaction, ssh, idleTime, interactiveInput, outputFilter, DEFAULT_COMMAND_SUCCESS, commands);
	}

	public static String[] executeCommands(Host host, PasswordCache passwordCache, Interaction interaction, SshClient ssh, long idleTime, Processor<String, String> interactiveInput, OutputFilter outputFilter, BitSet commandSuccess, String... commands) throws IOException, InterruptedException {
		SessionChannelClient ssc = ssh.openSessionChannel();
    	try {
			String[] result = new String[commands.length];
			SessionInteraction si = createSessionInteraction(host, passwordCache, ssc, interaction.getWriter(), outputFilter);
			try {
				for(int i = 0; i < commands.length; i++) {
					result[i] = executeCommand(interaction, si, idleTime, interactiveInput, commands[i]);
					if(!StringUtils.isBlank(CHECK_SUCCESS_CMD)) {
						String check = executeCommand(interaction, si, idleTime, null, CHECK_SUCCESS_CMD);
						Matcher matcher = CHECK_SUCCESS_PATTERN.matcher(check);
						if(!matcher.matches())
							throw new IOException("Check Success Command returned unexpected result: '" + check + "'");
						int resultCode = Integer.parseInt(matcher.group(1));
						if(!commandSuccess.get(resultCode & 0xFF))
							throw new IOException("Command '" + commands[i] + "' exitted with status " + resultCode + ": " + result[i]);
					}
				}
			} finally {
				si.close();
			}
			return result;
    	} catch(ServiceException e) {
			throw new IOException(e);
		} finally {
    		ssc.close();
    	}
	}

	protected static String executeCommand(Interaction interaction, ExecuteTask.SessionInteraction si, long idleTime, Processor<String, String> interactiveInput, String cmd) throws IOException, InterruptedException, ServiceException {
		//interaction.printf("==>  %1$s", cmd);
		String reply = si.sendCommand(cmd, idleTime, interaction, interactiveInput);
		log.info("Sent:\n\t" + cmd +"\nGot reply:\n\t" + reply);
		//interaction.printf("<==  %1$s", reply);
		return reply;
	}

	protected static String executeCommand(Interaction interaction, ExecuteTask.SessionInteraction si, String cmd) throws IOException, InterruptedException, ServiceException {
		//interaction.printf("==>  %1$s", cmd);
		String reply = si.sendCommand(cmd, 0, interaction, null);
		log.info("Sent:\n\t" + cmd +"\nGot reply:\n\t" + reply);
		// interaction.printf("<==  %1$s", reply);
		return reply;
	}

	protected Processor<String, String> getInteractiveInput(DeployTaskInfo deployTaskInfo) {
		return interactiveInput;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String getDescription(DeployTaskInfo deployTaskInfo) {
		if(!StringUtils.isBlank(getDescription()))
			return getDescription();
		StringBuilder sb = new StringBuilder();
		sb.append("Executing ").append(commands.size()).append(" commands");
		return sb.toString();
	}

	public boolean getCommandSuccess(int resultCode) {
		return commandSuccess.get(resultCode);
	}

	public void setCommandSuccess(int resultCode, boolean isSuccess) {
		if(isSuccess)
			commandSuccess.set(resultCode);
		else
			commandSuccess.clear(resultCode);
	}

	public void setCommandSuccessful(int... resultCodes) {
		for(int resultCode : resultCodes)
			commandSuccess.set(resultCode);
	}

	public String[] getCommands() {
		return commands.toArray(new String[commands.size()]);
	}

	public void setCommands(String... commands) {
		this.commands.clear();
		addCommands(commands);
	}

	public void addCommands(String... commands) {
		if(commands != null)
			for(String command : commands)
				this.commands.add(command);
	}

	public void removeCommands(String... commands) {
		if(commands != null)
			for(String command : commands)
				this.commands.remove(command);
	}
}