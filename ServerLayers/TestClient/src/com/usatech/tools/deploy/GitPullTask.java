package com.usatech.tools.deploy;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.lang.SystemUtils;
import simple.text.StringUtils;

public class GitPullTask extends DefaultPullTask {
	protected final String gitRootDir;
	protected final String filePath;
	protected final String revision;
	protected static final Host GIT_HOST = new HostImpl("LOCAL", "git.usatech.com");
	protected static final char NEWLINE = '\n';
	protected static final String DEV_REP    = "/opt/USAT/git/NetworkServices.git";
	protected static final String DEPLOY_REP = "/opt/USAT/git/NetworkBuilds.git";
	protected final InputStream resultInputStream = new InputStream() {
		@Override
		public int available() throws IOException {
			long remaining = fileSize - pos;
			if (remaining > Integer.MAX_VALUE)
				return Integer.MAX_VALUE;
			return (int) remaining;
		}

		@Override
		public void close() throws IOException {
			try {
				if (fileInputStream != null)
					fileInputStream.close();
			} finally {
				fileSize = 0;
				pos = 0;
				fileInputStream = null;
				if (sshChannel != null)
					try {
						sshChannel.disconnect();
					} finally {
						sshChannel = null;
					}
			}
		}

		protected void readAfterContent() throws IOException {
			String line;
			StringBuilder message = new StringBuilder();
			while ((line = readLine(fileInputStream)) != null) {
				if ("ok".equals(line)) {
					pos++;
					return;
				} else if (line.startsWith("error")) {
					throw new IOException("Could not pull file " + getFilePath() + " (" + getRevision()
							+ ") from Git because: " + message.toString());
				} else if (line.startsWith("M ") || line.startsWith("E ")) {
					message.append(StringUtils.substringAfter(line, " "));
				}
			}
		}

		@Override
		public int read(byte[] b, int off, int len) throws IOException {
			if (fileInputStream == null)
				throw new IOException("Task has not run yet");
			if (pos < fileSize) {
				int n = fileInputStream.read(b, off, Math.min(available(), len));
				if (n < 0) {
					if (pos != fileSize)
						throw new IOException("InputStream content does not match expected byte count");
					readAfterContent();
				} else
					pos += n;
				return n;
			}
			if (pos == fileSize) {
				readAfterContent();
			}
			return -1;
		}

		@Override
		public long skip(long n) throws IOException {
			if (fileInputStream == null)
				throw new IOException("Task has not run yet");
			return fileInputStream.skip(Math.min(fileSize - pos, n));
		}

		@Override
		public int read() throws IOException {
			if (fileInputStream == null)
				throw new IOException("Task has not run yet");
			if (pos < fileSize) {
				int n = fileInputStream.read();
				if (n < 0) {
					if (pos != fileSize)
						throw new IOException("InputStream content does not match expected byte count");
					readAfterContent();
				} else
					pos++;
				return n;
			}
			if (pos == fileSize) {
				readAfterContent();
			}
			return -1;
		}
	};
	protected InputStream fileInputStream;
	protected Channel sshChannel;
	protected long fileSize;
	protected long pos;

	public GitPullTask(String filePath) {
		this(filePath, "HEAD");
	}

	public GitPullTask(String filePath, String revision) {
		this(getNameRepo(filePath), filePath, revision);
	}

	private static String getNameRepo(String path) {
		if (path.startsWith("ApplicationBuilds") || path.startsWith("server_app_config"))
			return DEPLOY_REP;
		else					
			return DEV_REP;
	}

	public GitPullTask(String gitRootDir, String filePath, String revision) {
		this.gitRootDir = gitRootDir;
		this.filePath = filePath;
		this.revision = revision;
	}

	@Override
	public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
		Session sshSession = null;
		Channel channel = null;
		try {
			sshSession = deployTaskInfo.sshClientCache.getJSchClient(GIT_HOST, deployTaskInfo.user);
			fileSize = getFileSize(sshSession);
		} catch (JSchException e) {
			throw new IOException(e.getMessage());
		}

		boolean okay = false;
		try {
			String command = "cd " + getGitRootDir() + " && exec git archive " + getRevision() + " " + getFilePath()
					+ " | tar -xO " + getFilePath();

			
			try {
				channel = sshSession.openChannel("exec");
			} catch (JSchException e) {
				throw new IOException(e.getMessage());
			}
			((ChannelExec) channel).setCommand(command);

			channel.setInputStream(null);

			InputStream in = new DataInputStream(channel.getInputStream());

			try {
				channel.connect();
				while (channel.getExitStatus() == -1 || in.available() > 0) {
					if (in.available() > 0) {
						deployTaskInfo.interaction.printf("Pulling file %1$s (%2$s) from Git of %3$d bytes",
								getFilePath(), getRevision(), fileSize);
						this.fileInputStream = in;
						this.pos = 0;
						okay = true;
						return "Pulled '" + getFilePath() + "' (" + getRevision() + ") from Git repository";
					}
					try {
						Thread.sleep(1000);
					} catch (Exception e) {
						System.out.println(e);
					}
				}

			} catch (JSchException e) {
				throw new IOException(e.getMessage());
			}
			throw new IOException("End of stream not expected");

		} finally {
			if (!okay)
				channel.disconnect();
			else
				this.sshChannel = channel;
		}

	}

	private long getFileSize(Session sshSession) throws IOException, JSchException {
		String cmdResult = executeCmd(sshSession,
				"cd " + gitRootDir + " && exec git rev-list -1 " + getRevision() + " -- " + getFilePath()
						+ "  | sed -e 's/$/:" + StringUtils.escape(getFilePath(), "/".toCharArray(), '\\')
						+ "/g' | git cat-file --batch-check");

		long fileSize;
		try {
			if(cmdResult.trim().isEmpty()) 
				throw new IOException("Could not find file " + getFilePath() + " (" + getRevision()	+ ")");
			fileSize = ConvertUtils.getLong(cmdResult.split("\\s")[2]);
		} catch (ConvertException e) {
			throw new IOException("Could not parse file size", e);
		}
		return fileSize;
	}

	private String executeCmd(Session sshSession, String cmd) throws JSchException, IOException {
		StringBuilder message = new StringBuilder();

		Channel channel = sshSession.openChannel("exec");
		((ChannelExec) channel).setCommand(cmd);

		channel.setInputStream(null);

		((ChannelExec) channel).setErrStream(System.err);

		InputStream in = new DataInputStream(channel.getInputStream());
		channel.connect();

		byte[] tmp = new byte[1024];
		while (true) {
			while (in.available() > 0) {
				int i = in.read(tmp, 0, 1024);
				if (i < 0)
					break;
				message.append(new String(tmp, 0, i));
			}
			if (channel.isClosed()) {
				if (in.available() > 0)
					continue;
				break;
			}
			try {
				Thread.sleep(1000);
			} catch (Exception ee) {
			}
		}
		channel.disconnect();

		return message.toString().trim();
	}

	protected static String readLine(InputStream in) throws IOException {
		StringBuilder line = new StringBuilder(512);
		while (true) {
			int b = in.read();
			if (b == -1) {
				if (line.length() == 0)
					return null;
				return line.toString();
			}
			char ch = (char) b;
			if (ch == NEWLINE)
				return line.toString();
			line.append(ch);
		}
	}

	public String getGitRootDir() {
		return gitRootDir;
	}

	public String getFilePath() {
		return filePath;
	}

	public String getRevision() {
		return revision;
	}

	public InputStream getResultInputStream() {
		return resultInputStream;
	}

	@Override
	public InputStream process(DeployTaskInfo argument) throws ServiceException {
		try {
			perform(argument);
		} catch (IOException e) {
			throw new ServiceException(e);
		} catch (InterruptedException e) {
			throw new ServiceException(e);
		}
		return getResultInputStream();
	}

	@Override
	public Class<InputStream> getReturnType() {
		return InputStream.class;
	}

	@Override
	public Class<DeployTaskInfo> getArgumentType() {
		return DeployTaskInfo.class;
	}

	@Override
	public String getDescription(DeployTaskInfo deployTaskInfo) {
		StringBuilder sb = new StringBuilder();
		sb.append("Pulling '").append(getFilePath()).append("' (").append(getRevision())
				.append(") from Git repository");
		return sb.toString();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Git Resource '").append(getFilePath()).append("' (").append(getRevision()).append(')');
		return sb.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof GitPullTask))
			return false;
		if (this == obj)
			return true;
		GitPullTask cpt = (GitPullTask) obj;
		return ConvertUtils.areEqual(cpt.gitRootDir, gitRootDir) && ConvertUtils.areEqual(cpt.filePath, filePath)
				&& ConvertUtils.areEqual(cpt.revision, revision);
	}

	@Override
	public int hashCode() {
		return SystemUtils.addHashCodes(0, gitRootDir, filePath, revision);
	}
}
