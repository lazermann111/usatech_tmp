/**
 * 
 */
package com.usatech.tools.deploy;

import java.io.IOException;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;

import simple.io.HeapBufferStream;
import simple.security.SecurityUtils;


public class HttpsWithJKSCertTask implements DeployTask {
	protected final String[] urls;
	protected final String keystorePath;
	protected final PasswordCache cache;
	protected final String alias;

	public HttpsWithJKSCertTask(String[] urls, String alias, String keystorePath, PasswordCache cache) {
		this.urls = urls;
		this.alias = alias;
		this.keystorePath = keystorePath;
		this.cache = cache;
	}

	@Override
	public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
		//get the alias		
		char[] password = DeployUtils.getPassword(cache, deployTaskInfo.host, "keystore", "the keystore at '" + keystorePath + "'", deployTaskInfo.interaction);
		
		//Get the keystore stream
		HeapBufferStream bufferStream = new HeapBufferStream();
		InputStream in;
		if(UploadTask.checkFileUsingCmd(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, keystorePath)) {
			deployTaskInfo.interaction.printf("Retrieving contents of keystore at '%1$s'", keystorePath);
			UploadTask.readFileUsingTmp(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, keystorePath, bufferStream.getOutputStream(), null);
			in = bufferStream.getInputStream();
		} else {
			in = null;
		}
		//Decode with password
		boolean[] ret;
		try {
			KeyStore keyStore = SecurityUtils.getKeyStore(in, "JKS", null, password);
			//check if expired
			Certificate[] certs = keyStore.getCertificateChain(alias);
			if(certs == null)
				throw new IOException("Certificate '" + alias + "' not found in keystore at '" + keystorePath);
			Key key = keyStore.getKey(alias, password);
			ret = DeployUtils.attemptHttps(certs, key instanceof PrivateKey ? (PrivateKey) key : null, deployTaskInfo.interaction, urls);
		} catch(Exception e) {
			throw new IOException(e);
		}
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		for(int i = 0; i < urls.length; i++) {
			if(ret[i]) {
				if(first) {
					sb.append("Successfully hit ");
					first = false;
				} else
					sb.append(", ");
				sb.append('\'').append(urls[i]).append('\'');
			}
		}
		first = true;
		for(int i = 0; i < urls.length; i++) {
			if(ret[i]) {
				if(first) {
					if(sb.length() > 0)
						sb.append(" and ");
					sb.append("Failed to hit ");
					first = false;
				} else
					sb.append(", ");
				sb.append('\'').append(urls[i]).append('\'');
			}
		}
		sb.append(" with certificate '").append(alias).append("' from ").append(keystorePath);
		return sb.toString();
	}

	@Override
	public String getDescription(DeployTaskInfo deployTaskInfo) {
		StringBuilder sb = new StringBuilder();
		sb.append("Hitting ");
		for(int i = 0; i < urls.length; i++) {
			if(i > 0)
				sb.append(", ");
			sb.append('\'').append(urls[i]).append('\'');
		}
		sb.append(" with certificate '").append(alias).append("' from ").append(keystorePath);
		return sb.toString();
	}
}