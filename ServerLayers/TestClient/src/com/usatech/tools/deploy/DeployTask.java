/**
 * 
 */
package com.usatech.tools.deploy;

import java.io.IOException;



public interface DeployTask {
	public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException;

	public String getDescription(DeployTaskInfo deployTaskInfo);
}