/**
 * 
 */
package com.usatech.tools.deploy;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.LinkedHashMap;
import java.util.Map;

import simple.app.Processor;
import simple.app.ServiceException;
import simple.io.HeapBufferStream;
import simple.security.SecurityUtils;
import simple.security.crypt.CryptUtils;


public class ImportJKSCertTask implements DeployTask {
	protected final Map<String, Processor<DeployTaskInfo, InputStream>> certInputMap = new LinkedHashMap<>();
	protected final String truststorePath;
	protected final PasswordCache cache;
	protected final boolean replace;
	protected final int truststorePermissions;

	public ImportJKSCertTask(String alias, File certFile, String truststorePath, PasswordCache cache, int truststorePermissions, boolean replace) {
		this(truststorePath, cache, truststorePermissions, replace);
		addCertificate(alias, certFile);
	}

	public ImportJKSCertTask(String alias, Processor<DeployTaskInfo, InputStream> certInput, String truststorePath, PasswordCache cache, int truststorePermissions, boolean replace) {
		this(truststorePath, cache, truststorePermissions, replace);
		addCertificate(alias, certInput);
	}

	public ImportJKSCertTask(String truststorePath, PasswordCache cache, int truststorePermissions, boolean replace) {
		this.truststorePath = truststorePath;
		this.cache = cache;
		this.replace = replace;
		this.truststorePermissions = truststorePermissions;
	}

	public void addCertificate(String alias, File certFile) {
		addCertificate(alias, new FileContent(certFile));
	}

	public void addCertificate(String alias, Processor<DeployTaskInfo, InputStream> certInput) {
		certInputMap.put(alias, certInput);
	}

	@Override
	public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
		//Get the truststore stream
		HeapBufferStream bufferStream = new HeapBufferStream(true);
		InputStream in;
		if(UploadTask.checkFileUsingCmd(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, truststorePath)) {
			deployTaskInfo.interaction.printf("Retrieving contents of truststore at '%1$s'", truststorePath);
			UploadTask.readFileUsingTmp(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, truststorePath, bufferStream.getOutputStream(), null);
			in = bufferStream.getInputStream();
			in.mark(bufferStream.size());
		} else {
			in = null;
		}
		//Decode with password
		try {
			KeyStore keyStore = SecurityUtils.getKeyStore(in, "JKS", null, null);
			// check if exists
			int changed = 0;
			char[] password = null;
			for(Map.Entry<String, Processor<DeployTaskInfo, InputStream>> entry : certInputMap.entrySet()) {
				Certificate cert = keyStore.getCertificate(entry.getKey());
				if(replace || cert == null) {
					if(changed == 0) {
						password = DeployUtils.getPassword(cache, deployTaskInfo.host, "truststore", "the truststore at '" + truststorePath + "'", deployTaskInfo.interaction);
						if(in != null)
							in.reset();
						keyStore.load(in, password);
						changed++;
					}
					CryptUtils.importTrustedCertificate(keyStore, entry.getKey(), entry.getValue().process(deployTaskInfo));
					deployTaskInfo.interaction.printf("Added certificate '" + entry.getKey() + " to the truststore");
				}
			}
			if(changed > 0) {
				deployTaskInfo.interaction.printf("Saving the truststore");
				bufferStream.clear();
				keyStore.store(bufferStream.getOutputStream(), password);
				UploadTask.writeFileUsingTmp(deployTaskInfo, bufferStream.getInputStream(), truststorePermissions, "root", "usat", null, truststorePath);
				return "Successfully imported " + changed + " certificate(s). " + (certInputMap.size() - changed) + " already exist";
			}
		} catch(KeyStoreException e) {
			throw new IOException(e);
		} catch(NoSuchProviderException e) {
			throw new IOException(e);
		} catch(NoSuchAlgorithmException e) {
			throw new IOException(e);
		} catch(CertificateException e) {
			throw new IOException(e);
		} catch(ServiceException e) {
			throw new IOException(e);
		}
		return certInputMap.size() + " certificate(s) already exist";
	}

	@Override
	public String getDescription(DeployTaskInfo deployTaskInfo) {
		StringBuilder sb = new StringBuilder();
		if(certInputMap.size() == 1) {
			Map.Entry<String, Processor<DeployTaskInfo, InputStream>> entry = certInputMap.entrySet().iterator().next();
			sb.append("Importing certificate '").append(entry.getValue()).append("' as '").append(entry.getKey()).append("'");
		} else {
			sb.append("Importing certificates '");
			for(Map.Entry<String, Processor<DeployTaskInfo, InputStream>> entry : certInputMap.entrySet()) {
				sb.append(entry.getValue()).append("' as '").append(entry.getKey()).append("', ");
			}
			sb.setLength(sb.length() - 2);
		}

		sb.append(" into keystore ").append(truststorePath);
		return sb.toString();
	}
}