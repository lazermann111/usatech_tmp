package com.usatech.tools.deploy;

import java.io.IOException;

import simple.bean.ConvertUtils;
import simple.io.Interaction;
import simple.lang.SystemUtils;
import simple.util.concurrent.Cache;
import simple.util.concurrent.LockSegmentCache;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.sshtools.j2ssh.SftpClient;
import com.sshtools.j2ssh.SshClient;
import com.sshtools.j2ssh.transport.HostKeyVerification;

public class DefaultSshClientCache implements SshClientCache {
	
	protected static enum SshType {
		SSHTOOL, JSCH
	}
	
	protected static class CacheKey {
		public final String hostFullName;
		public final String user;
		

		public CacheKey(Host host, String user) {
			this(host.getFullName(), user);
		}

		public CacheKey(String hostFullName, String user) {
			this.hostFullName = hostFullName;
			this.user = user;
		}
		@Override
		public boolean equals(Object obj) {
			if(!(obj instanceof CacheKey))
				return false;
			CacheKey ck = (CacheKey) obj;
			return ConvertUtils.areEqual(ck.hostFullName, hostFullName) && ConvertUtils.areEqual(ck.user, user);
		}

		@Override
		public int hashCode() {
			return SystemUtils.addHashCodes(0, hostFullName, user);
		}

		@Override
		public String toString() {
			return user + '@' + hostFullName;
		}
	}

	protected static class SshClientPair {
		public final SshClient ssh;
		public SftpClient sftp;
		public Session jsch;

		public SshClientPair(SshClient ssh, Session jsch) {
			this.ssh = ssh;
			this.jsch = jsch;
		}
	}
	protected final Interaction interaction;
	protected final HostKeyVerification hostKeyVerification;
	protected final PasswordCache passwordCache;
	protected final Cache<CacheKey, SshClientPair, IOException> cache = new LockSegmentCache<CacheKey, SshClientPair, IOException>(100, 0.75f, 16) {
		@Override
		protected SshClientPair createValue(CacheKey key, Object... additionalInfo) throws IOException {
			return new SshClientPair(new ExtendedSshClient(), new ExtendedJSch(key).getSession());
		}
	};

	public DefaultSshClientCache(Interaction interaction, HostKeyVerification hostKeyVerification, PasswordCache passwordCache) {
		super();
		this.interaction = interaction;
		this.hostKeyVerification = hostKeyVerification;
		this.passwordCache = passwordCache;
	}

	protected SshClientPair getSshClientPair(Host host, String user, SshType sshType) throws IOException {
		SshClientPair pair = cache.getOrCreate(new CacheKey(host, user));
		if (sshType == SshType.SSHTOOL) { 
			synchronized(pair.ssh) {
				if(DeployUtils.connectAndAuthenticate(pair.ssh, host, user, interaction, hostKeyVerification, passwordCache))
					pair.sftp = null;
			}
		} else
			synchronized(pair.jsch) {
				DeployUtils.connectAndAuthenticate(pair.jsch, host, user, interaction, passwordCache);
			}	
		return pair;
	}
	protected SshClientPair getSshClientPair(Host host, String user) throws IOException {
		return getSshClientPair(host, user, SshType.SSHTOOL);
	}
	
	@Override
	public SshClient getSshClient(Host host, String user) throws IOException {
		return getSshClientPair(host, user).ssh;
	}
	
	@Override
	public SftpClient getSftpClient(Host host, String user) throws IOException {
		SshClientPair pair = getSshClientPair(host, user);
		synchronized(pair.ssh) {
			if(pair.sftp == null || pair.sftp.isClosed())
				pair.sftp = pair.ssh.openSftpClient();
		}
		return pair.sftp;
	}

	@Override
	public void shutdown() {
		for(SshClientPair pair : cache.values()) {
			synchronized(pair.ssh) {
				pair.ssh.disconnect();
			}
		synchronized(pair.jsch) {
			pair.ssh.disconnect();
		}
		}
	}

	@Override
	public Session getJSchClient(Host host, String user) throws IOException, JSchException {
		return getSshClientPair(host, user, SshType.JSCH).jsch;
	}
}
