package com.usatech.tools.deploy;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import simple.io.IOUtils;
import simple.text.StringUtils;

public abstract class MultiLinuxPatchChangeSet extends AbstractLinuxChangeSet {
	protected final Map<String, Map<DefaultPullTask, UploadTask>> uploads = new HashMap<String, Map<DefaultPullTask, UploadTask>>();
	protected final Map<App, Map<ResourceType, Set<File>>> resources = new LinkedHashMap<App, Map<ResourceType, Set<File>>>();
	protected final Set<App> restarts = new LinkedHashSet<App>();
	protected String patchDir = "patch";

	protected static enum ResourceType {
		DIRECTORY, FILE, CLASS, LIB
	};

	public MultiLinuxPatchChangeSet() throws UnknownHostException {
		super();
	}

	@Override
	protected final void registerApps() {
	}

	@Override
	protected boolean shouldAddHostBasedTask(DeployTaskInfo deployTaskInfo) {
		return true;
	}

	@Override
	protected Set<App> filterApps(Set<App> apps, DeployTaskInfo deployTaskInfo, String serverType) {
		return apps;
	}
	/**
	 * Register a non-class resource (such as a data-layer.xml file or jsp file)
	 * 
	 * @param cvsFilePath
	 *            eg. usalive/web/css/usalive-app-style.css
	 * @param appSubDirPath
	 *            eg. target machine's app subpath such as 'web/css' so the file will go to usalive/web/css
	 * @param cvsRevision
	 */
	public final void registerResource(String cvsFilePath, String appSubDirPath, String cvsRevision, boolean restartRequired, App... apps) {
		addResource(cvsFilePath, appSubDirPath, cvsRevision, restartRequired, ResourceType.FILE, apps);
	}

	/**
	 * Register a new jar as part of the classpath of the apps
	 * 
	 * @param cvsLibPath
	 * @param cvsRevision
	 * @param apps
	 */
	public final void registerLib(String cvsLibPath, String cvsRevision, App... apps) {
		addResource(cvsLibPath, null, cvsRevision, true, ResourceType.LIB, apps);
	}

	/**
	 * Register a class to be compiled and patched into the apps
	 * 
	 * @param cvsSourceDirectory
	 * @param clazz
	 * @param cvsRevision
	 * @param apps
	 */
	public final void registerSource(String cvsSourceDirectory, final Class<?> clazz, String cvsRevision, App... apps) {
		final String simpleName = clazz.getSimpleName();
		final String packageDir = clazz.getPackage().getName().replace('.', '/');
		final String fullPackageDir = IOUtils.getFullPath("classes", packageDir);
		addResource(IOUtils.getFullPath(cvsSourceDirectory, packageDir + "/" + simpleName + ".java"), fullPackageDir, cvsRevision, true, ResourceType.CLASS, apps);
	}

	public final void registerAppRestart(App... apps) {
		if(apps == null || apps.length == 0)
			throw new IllegalArgumentException("No apps were provided. Please provide at least one app to which this applies");
		for(App app : apps) {
			registerApp(app);
			resources.putIfAbsent(app, new EnumMap<ResourceType, Set<File>>(ResourceType.class));
			restarts.add(app);
		}
	}
	protected final void addResource(String vcsFilePath, String appSubDirPath, String vcsRevision, boolean restartRequired, ResourceType type, App... apps) {
		// if("HEAD".equalsIgnoreCase(cvsRevision))
		// throw new IllegalArgumentException("HEAD revision not allowed for patches; please tag or branch the file");
		if(apps == null || apps.length == 0)
			throw new IllegalArgumentException("No apps were provided. Please provide at least one app to which this applies");
		if(type == ResourceType.DIRECTORY)
			throw new IllegalArgumentException("Invalid resource type use  FILE, CLASS, or LIB");
		String fileName = IOUtils.getFileName(vcsFilePath);
		File file;
		if(appSubDirPath != null && (appSubDirPath = appSubDirPath.trim()).length() > 0)
			file = new File(appSubDirPath, fileName);
		else
			file = new File(fileName);
		for(App app : apps) {
			Map<ResourceType, Set<File>> resourceMap = resources.get(app);
			if(resourceMap == null) {
				resourceMap = new EnumMap<ResourceType, Set<File>>(ResourceType.class);
				resources.put(app, resourceMap);
			}
			if(file.getParentFile() != null) {
				Set<File> dirs = resourceMap.get(ResourceType.DIRECTORY);
				if(dirs == null) {
					dirs = new LinkedHashSet<File>();
					resourceMap.put(ResourceType.DIRECTORY, dirs);
				}
				dirs.add(file.getParentFile());
			}
			Set<File> files = resourceMap.get(type);
			if(files == null) {
				files = new LinkedHashSet<File>();
				resourceMap.put(type, files);
			}
			files.add(file);
		}
		DefaultPullTask vcsPullTask = new GitPullTask(vcsFilePath, vcsRevision);
		for(App app : apps) {
			registerApp(app);
			if(restartRequired)
				restarts.add(app);
			Map<DefaultPullTask, UploadTask> localFiles = uploads.get(app.getServerType());
			if(localFiles == null) {
				localFiles = new LinkedHashMap<DefaultPullTask, UploadTask>();
				uploads.put(app.getServerType(), localFiles);
			}
			UploadTask uploadTask = localFiles.get(vcsPullTask);
			if(uploadTask == null) {
				uploadTask = new UploadTask(vcsPullTask, 0644, null, null, IOUtils.getFileName(vcsFilePath), true, new String[0]);
				localFiles.put(vcsPullTask, uploadTask);
			}
			uploadTask.addRemoteFilePaths(IOUtils.getFullPath(patchDir, file.getPath().replace(File.separatorChar, '/')));
		}
	}

	@Override
	protected final void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		ExecuteTask executeTask = new ExecuteTask();
		tasks.add(executeTask);
		for(String serverType : host.getServerTypes()) {
			Map<DefaultPullTask, UploadTask> uts = uploads.get(serverType);
			if(uts != null)
				for(UploadTask ut : uts.values()) {
					for(String path : ut.getRemoteFilePaths()) {
						String dir = IOUtils.getDirectory(path);
						if(!StringUtils.isBlank(dir))
							executeTask.addCommands("mkdir -p " + dir);
					}
					tasks.add(ut);
				}
		}
		addPreHostTasks(host, tasks, cache);
	}

	/**
	 * @throws IOException
	 */
	protected void addPreHostTasks(Host host, List<DeployTask> tasks, PasswordCache cache) throws IOException {

	}

	@Override
	protected void addPostHostTasks(List<DeployTask> tasks) {
		if(!StringUtils.isBlank(patchDir))
			tasks.add(new ExecuteTask("/bin/rm -rf " + patchDir));
	}

	@Override
	protected final void addTasks(Host host, App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		Map<ResourceType, Set<File>> resourceMap = resources.get(app);
		if(resourceMap == null)
			return;
		if(app.isStandardJavaApp() || app.getName().equals("httpd")) {
			String appDir = "/opt/USAT/" + app.getName() + (instance == null ? "" : instance);
			if(commands.isEmpty())
				commands.add("sudo su");
			Set<File> dirs = resourceMap.get(ResourceType.DIRECTORY);
			if(dirs != null)
				for(File dir : dirs)
					commands.add("mkdir -p " + IOUtils.getFullPath(appDir, dir.getPath().replace(File.separatorChar, '/')));
			Set<File> files = resourceMap.get(ResourceType.FILE);
			if(files != null)
				for(File f : files)
					commands.add("/bin/cp -p /home/`logname`/" + getPatchDir() + '/' + f.getPath().replace(File.separatorChar, '/') + " " + IOUtils.getFullPath(appDir, f.getParent().replace(File.separatorChar, '/')));
			Set<File> libs = resourceMap.get(ResourceType.LIB);
			String libDir = appDir + "/lib/";
			if(libs != null)
				for(File lib : libs) {
					commands.add("/bin/cp -p /home/`logname`/" + getPatchDir() + '/' + lib.getPath().replace(File.separatorChar, '/') + " " + libDir + lib.getPath());
					commands.add("chown " + app.getUserName() + ":" + app.getUserName() + " " + libDir + lib.getPath());
					commands.add("chmod 644 " + libDir + lib.getPath().replace(File.separatorChar, '/'));
					String scriptFile = appDir + "/bin/" + app.getServiceName() + ".sh";
					commands.add("if ! grep -c 'CLASSPATH=\".*:lib/" + lib.getPath().replace(File.separatorChar, '/') + "' " + scriptFile + "; then /bin/cp " + scriptFile + " " + scriptFile + ".ORIG && sed 's#\\(CLASSPATH=\"[^\"]*\\)\"#\\1:lib/" + lib.getPath() + "\"#' " + scriptFile + ".ORIG > " + scriptFile + "; fi");
				}

			if(dirs != null)
				for(File dir : dirs) {
					File base = dir;
					while(base.getParentFile() != null)
						base = base.getParentFile();
					String fullDir = IOUtils.getFullPath(appDir, base.getPath());
					commands.add("chown -R " + app.getUserName() + ":" + app.getUserName() + " " + fullDir);
					commands.add("chmod -R 755 " + fullDir);
				}
			Set<File> srcs = resourceMap.get(ResourceType.CLASS);
			if(srcs != null) {
				commands.add("cd " + appDir);
				StringBuilder sb = new StringBuilder();
				sb.append("/usr/jdk/latest/bin/javac -g -cp $(grep -m 1 \"CLASSPATH=\" ").append(appDir);
				sb.append("/bin/").append(app.getServiceName()).append(".sh | cut -d= -f2- | sed 's/^\"\\(.*\\)\"$/\\1/'):/usr/jdk/latest/jre/lib/rt.jar -d ").append(appDir).append("/classes");
				for(File f : srcs) {
					sb.append(" /home/`logname`/").append(getPatchDir()).append('/').append(f.getPath().replace(File.separatorChar, '/'));
				}
				commands.add(sb.toString());
				commands.add("chown -R " + app.getUserName() + ':' + app.getUserName() + ' ' + appDir + "/classes/*");
			}
		}
		Set<App> subApps = registry.getServerSet(host.getServerEnv()).getSubApps(app);
		if(subApps != null && !subApps.isEmpty()) {
			for(App subApp : subApps) {
				String appDir = "/opt/USAT/" + app.getName() + (instance == null ? "" : instance) + "/" + subApp.getName();
				if(commands.isEmpty())
					commands.add("sudo su");
				Set<File> dirs = resourceMap.get(ResourceType.DIRECTORY);
				if(dirs != null)
					for(File dir : dirs) {
						String dirPath = getDir(dir.getPath().replace(File.separatorChar, '/'), app);
						if(dirPath != null)
							commands.add("mkdir -p " + IOUtils.getFullPath(appDir, dirPath));
					}

				Set<File> files = resourceMap.get(ResourceType.FILE);
				if(files != null)
					for(File f : files) {
						String dir = getDir(f.getParent().replace(File.separatorChar, '/'), app);
						if(dir != null)
							commands.add("/bin/cp -p /home/`logname`/" + getPatchDir() + '/' + f.getPath().replace(File.separatorChar, '/') + " " + IOUtils.getFullPath(appDir, dir));
					}
				if(dirs != null)
					for(File dir : dirs) {
						String dirPath = getDir(dir.getPath().replace(File.separatorChar, '/'), app);
						if(dirPath != null) {
							String fullDir = IOUtils.getFullPath(appDir, dirPath /*.substring(0, dirPath.indexOf('/'))*/);
							commands.add("chown -R " + app.getUserName() + ":" + app.getUserName() + " " + fullDir);
							commands.add("chmod -R 755 " + fullDir);
						}
					}
			}
		}
		addRestartAppCommands(host, app, instance, instanceNum, instanceCount, tasks, commands, cache);
	}

	protected String getDir(String dir, App app) {
		if(app.getName().equals("httpd")) {
			if(dir.startsWith("web/"))
				return dir.substring(4);
			if(dir.equals("web"))
				return "";
			return null;
		}
		return dir;
	}
	/**
	 * @throws IOException
	 */
	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(restarts.contains(app)) {
			if(enableAppIfDisabled(host, app, instance == null ? null : Integer.parseInt(instance)))
				commands.add("/usr/monit-latest/bin/monit restart " + app.getName() + (instance == null ? "" : instance));
			else
				commands.add("/opt/USAT/" + app.getName() + (instance == null ? "" : instance) + "/bin/" + app.getServiceName() + ".sh stop 10000");
		}
	}

	public String getPatchDir() {
		return patchDir;
	}

	public void setPatchDir(String patchDir) {
		this.patchDir = patchDir;
	}
}
