/**
 * 
 */
package com.usatech.tools.deploy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import simple.bean.ConvertUtils;
import simple.text.StringUtils;

public class App {
	protected final String name;
	protected final String serverType;
	protected String version;
	protected final String appBuildDir;
	protected final String appBuildPrefix;
	protected final int portOffset;
	protected final int appUid;
	protected final String appDesc;
	protected final String serviceName;
	protected final String dbUserName;
	protected final String userName;
	protected final Set<String> produceQueueLayerTo = new HashSet<String>();
	protected final Set<App> consumeQueueLayerFrom = new HashSet<App>();
	protected final Set<String> produceQueueLayerToUnmod = Collections.unmodifiableSet(produceQueueLayerTo);
	protected final Set<App> consumeQueueLayerFromUnmod = Collections.unmodifiableSet(consumeQueueLayerFrom);
	protected int maxInstances = 0;
	protected String versionPeriodReplacement = "_";
	protected DefaultPullTask retrieveInstallFileTask;
	protected String installFileName;
	protected boolean standardJavaApp;
	
	public App(String name, String serverType, String appBuildDir, String appBuildPrefix, int portOffset, int appUid, String appDesc, String serviceName, String dbUserName) {
		this(name, serverType, appBuildDir, appBuildPrefix, portOffset, appUid, appDesc, serviceName, dbUserName, name);
	}

	public App(String name, String serverType, String appBuildDir, String appBuildPrefix, int portOffset, int appUid, String appDesc, String serviceName, String dbUserName, String userName, String... produceQueueLayerTo) {
		this.name = name;
		this.serverType = serverType;
		this.appBuildDir = appBuildDir;
		this.appBuildPrefix = appBuildPrefix;
		this.portOffset = portOffset;
		this.appUid = appUid;
		this.appDesc = appDesc;
		this.serviceName = serviceName;
		this.dbUserName = dbUserName;
		this.userName = userName;
		if(produceQueueLayerTo != null && produceQueueLayerTo.length > 0)
			this.produceQueueLayerTo.addAll(Arrays.asList(produceQueueLayerTo));
		if(appBuildDir != null && appBuildPrefix != null && !name.equals("httpd"))
			standardJavaApp = true;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
		updateInstallFileName();
	}
	public String getName() {
		return name;
	}
	public String getServerType() {
		return serverType;
	}

	protected void updateInstallFileName() {
		if(StringUtils.isBlank(version) || StringUtils.isBlank(appBuildPrefix))
			this.installFileName = null;
		else
			this.installFileName = appBuildPrefix + version.replaceAll("\\.", versionPeriodReplacement) + ".tar.gz";
	}
	/*
	public File getInstallFile() {
		if(version != null && version.trim().length() > 0)
			return new File(appBuildDir, appBuildPrefix + version.replaceAll("\\W", versionPeriodReplacement) + ".tar.gz");
		return null;
	}
	 */
	public DefaultPullTask getRetrieveInstallFileTask() {
		if(installFileName != null) {
			if(retrieveInstallFileTask == null)
				retrieveInstallFileTask = new GitPullTask(appBuildDir + "/" + installFileName);
			return retrieveInstallFileTask;
		}
		return null;
	}

	public String getInstallFileName() {
		return installFileName;
	}
	@Override
	public int hashCode() {
		return name == null ? 0 : name.hashCode();
	}
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof App))
			return false;
		App other = (App) obj;
		return ConvertUtils.areEqual(name, other.name) && ConvertUtils.areEqual(serverType, other.serverType);
	}
	public int getPortOffset() {
		return portOffset;
	}
	public int getAppUid() {
		return appUid;
	}
	public String getAppDesc() {
		return appDesc;
	}
	public String getServiceName() {
		return serviceName;
	}
	public String getDbUserName() {
		return dbUserName;
	}

	public boolean isUsingQueueLayer() {
		return !produceQueueLayerTo.isEmpty() || !consumeQueueLayerFrom.isEmpty();
	}

	public String getUserName() {
		return userName;
	}

	public void registerConsumeQueueLayerFrom(App... remoteApps) {
		if(remoteApps != null)
			for(App remoteApp : remoteApps)
				if(remoteApp != null)
					consumeQueueLayerFrom.add(remoteApp);
	}

	public void registerProduceQueueLayerTo(String... remoteServerTypes) {
		if(remoteServerTypes != null)
			for(String remoteServerType : remoteServerTypes)
				if(remoteServerType != null)
					produceQueueLayerTo.add(remoteServerType);
	}

	public Set<String> getProduceQueueLayerTo() {
		return produceQueueLayerToUnmod;
	}

	public Set<App> getConsumeQueueLayerFrom() {
		return consumeQueueLayerFromUnmod;
	}

	public int getMaxInstances() {
		return maxInstances;
	}

	public void setMaxInstances(int maxInstances) {
		this.maxInstances = maxInstances;
	}

	public String[] filterInstances(String[] instances) {
		if(instances == null || maxInstances < 1 || maxInstances >= instances.length)
			return instances;
		if(maxInstances == 1)
			return new String[1];
		String[] filtered = new String[maxInstances];
		System.arraycopy(instances, 0, filtered, 0, filtered.length);
		return filtered;
	}

	public int getInstanceCount(List<Server> servers) {
		if(servers == null)
			return 0;
		int total = 0;
		for(Server server : servers) {
			String[] instances = filterInstances(server.instances);
			if(instances != null)
				total += instances.length;
		}
		return total;
	}

	public List<String> getInstances(List<Server> servers) {
		if(servers == null)
			return Collections.emptyList();
		List<String> list = new ArrayList<String>();
		for(Server server : servers) {
			String[] instances = filterInstances(server.instances);
			if(instances != null)
				list.addAll(Arrays.asList(instances));
		}
		return list;
	}

	public String getVersionPeriodReplacement() {
		return versionPeriodReplacement;
	}

	public void setVersionPeriodReplacement(String versionPeriodReplacement) {
		this.versionPeriodReplacement = versionPeriodReplacement;
		updateInstallFileName();
	}

	public boolean isStandardJavaApp() {
		return standardJavaApp;
	}

	public void setStandardJavaApp(boolean standardJavaApp) {
		this.standardJavaApp = standardJavaApp;
	}
}