package com.usatech.tools.deploy;

import java.util.HashMap;
import java.util.Map;

import simple.io.Interaction;

public class UsatSettingUpdateFileTask extends PropertiesUpdateFileTask {
	public final static String LOGS_DEV  = "jdbc:postgresql://ecclog11:5432/devmain?ssl=true&connectTimeout=5&tcpKeepAlive=true";
	public final static String LOGS_INT  = "jdbc:postgresql://ecclog11:5432/intmain?ssl=true&connectTimeout=5&tcpKeepAlive=true";
	public final static String LOGS_ECC  = "jdbc:postgresql://ecclog11:5432/eccmain?ssl=true&connectTimeout=5&tcpKeepAlive=true";
	public final static String LOGS_PROD = "jdbc:postgresql://usalog31:5432/main?ssl=true&connectTimeout=5&tcpKeepAlive=true";
	
	public final static String OPER_DEV="jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=eccdb013.usatech.com)(PORT=1522)))(CONNECT_DATA=(SERVICE_NAME=usadev03)))";
	public final static String OPER_INT="jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=eccdb014.usatech.com)(PORT=1524)))(CONNECT_DATA=(SERVICE_NAME=usadev02)))";
	public final static String OPER_ECC="jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=eccscan03.usatech.com)(PORT=1525))(ADDRESS=(PROTOCOL=TCP)(HOST=eccscan01.usatech.com)(PORT=1525)))(CONNECT_DATA=(SERVICE_NAME=usaecc_db.world)))";
	
	public final static String OPER_PROD_DEFAULT = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.61)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.62)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.91)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.92)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)))";
	public final static String REPORT_PROD_DEFAULT = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.61)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.62)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.91)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.92)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)(INSTANCE_NAME=USAPDB1)))";
	public final static Map<String, String> OPER_PROD = new HashMap<String, String>();
	public final static Map<String, String> REPORT_PROD = new HashMap<String, String>();
	
	static {
		OPER_PROD.put("usaapr31", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.61)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.62)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.91)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.92)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)))");
		OPER_PROD.put("usaapr32", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.61)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.62)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.91)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.92)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)))");
		OPER_PROD.put("usaapr33", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.61)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.62)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.91)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.92)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)))");
		OPER_PROD.put("usaapr34", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.61)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.62)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.91)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.92)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)))");
		
		REPORT_PROD.put("usaapr31", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.61)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.62)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.91)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.92)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)(INSTANCE_NAME=USAPDB1)))");
		REPORT_PROD.put("usaapr32", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.61)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.62)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.91)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.92)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)(INSTANCE_NAME=USAPDB1)))");
		REPORT_PROD.put("usaapr33", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.61)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.62)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.91)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.92)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)(INSTANCE_NAME=USAPDB1)))");
		REPORT_PROD.put("usaapr34", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.61)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.62)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.91)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.92)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)(INSTANCE_NAME=USAPDB1)))");
		
		OPER_PROD.put("usaapp31", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.61)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.62)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.91)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.92)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)))");
		OPER_PROD.put("usaapp32", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.61)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.62)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.91)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.92)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)))");
		OPER_PROD.put("usaapp33", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.61)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.62)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.91)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.92)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)))");
		OPER_PROD.put("usaapp34", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.61)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.62)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.91)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.92)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)))");
		
		REPORT_PROD.put("usaapp31", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.61)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.62)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.91)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.92)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)(INSTANCE_NAME=USAPDB2)))");
		REPORT_PROD.put("usaapp32", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.61)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.62)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.91)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.92)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)(INSTANCE_NAME=USAPDB2)))");
		REPORT_PROD.put("usaapp33", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.61)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.62)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.91)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.92)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)(INSTANCE_NAME=USAPDB2)))");
		REPORT_PROD.put("usaapp34", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.61)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.62)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.91)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.92)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)(INSTANCE_NAME=USAPDB1)))");
	}
	
	public UsatSettingUpdateFileTask() {
		this(null);
	}

	public UsatSettingUpdateFileTask(ServerSet serverSet) {
		super(false, 0640, "root", "usat");
		setFilePath("/opt/USAT/conf/USAT_environment_settings.properties");
		// Debug Settings - debug level values: OFF, DEBUG
		// SECURITY ALERT: debug level must be OFF in non-development environments!
		registerValue("USAT.debug.level", new ServerEnvHostSpecificValue("OFF", "OFF", "OFF", "OFF"));
		
		// RDW settings
		if(serverSet != null && serverSet.getServers("APR") != null) {
			String[] aprServerTypes = new String[] { "APR" };
			MappedHostSpecificValue rdwEnabled = new MappedHostSpecificValue();
			rdwEnabled.registerValue("DEV",aprServerTypes,"true");
			rdwEnabled.registerValue("INT",aprServerTypes,"true");
			rdwEnabled.registerValue("ECC",aprServerTypes,"true");
			rdwEnabled.registerValue("USA",aprServerTypes,"false");
			registerValue("USAT.rdw.enabled", rdwEnabled);
		}

		// Other settings
		registerValue("java.beans.PropertyEditorManager.searchPath", new DefaultHostSpecificValue("simple.bean.editors"));
		
		// Database Settings
		String[] appAprServerTypes = new String[] { "APR", "APP", "MON" };
		MappedHostSpecificValue operUrl = new MappedHostSpecificValue();
		operUrl.registerValue("DEV", appAprServerTypes, OPER_DEV);
		operUrl.registerValue("INT", appAprServerTypes, OPER_INT);
		operUrl.registerValue("ECC", appAprServerTypes, OPER_ECC);
		operUrl.registerValue("USA", appAprServerTypes, new HostSpecificValue() {
			public boolean isOverriding() {
				return false;
			}

			public String getValue(Host host, Interaction interaction) {
				String value = OPER_PROD.get(host.getSimpleName());
				if (value == null)
					value = OPER_PROD_DEFAULT;
				return value;
			}
		});
		registerValue("USAT.database.OPER.url", operUrl);

		MappedHostSpecificValue rptUrl = new MappedHostSpecificValue();
		rptUrl.registerValue("DEV", appAprServerTypes, OPER_DEV);
		rptUrl.registerValue("INT", appAprServerTypes, OPER_INT);
		rptUrl.registerValue("ECC", appAprServerTypes, OPER_ECC);
		rptUrl.registerValue("USA", appAprServerTypes, new HostSpecificValue() {
			public boolean isOverriding() {
				return false;
			}

			public String getValue(Host host, Interaction interaction) {			
				String value = REPORT_PROD.get(host.getSimpleName());
				if (value == null)
					value = REPORT_PROD_DEFAULT;
				return value;
			}
		});

		registerValue("USAT.database.REPORT.url", rptUrl);
		HostSpecificValue mainUrl = new HostSpecificValue() {
			@Override
			public String getValue(Host host, Interaction interaction) {
				StringBuilder sb = new StringBuilder();
				sb.append("jdbc:postgresql://").append(host.getServerEnv().toLowerCase()).append("pgs");
				if("USA".equalsIgnoreCase(host.getServerEnv()))
					sb.append('3');
				else
					sb.append('1');
				sb.append("0:5432/main?ssl=true&connectTimeout=5&tcpKeepAlive=true");
				return sb.toString();
			}

			@Override
			public boolean isOverriding() {
				return true;
			}
		};
		registerValue("USAT.database.MAIN.url", mainUrl);
		registerValue("USAT.database.MAIN_READONLY.failoverUrls", mainUrl);
		registerValue("USAT.database.MAIN_READONLY.url", new HostSpecificValue() {
			@Override
			public String getValue(Host host, Interaction interaction) {
				StringBuilder sb = new StringBuilder();
				sb.append("jdbc:postgresql://").append(host.getServerEnv().toLowerCase()).append("pgs1");
				if("USA".equalsIgnoreCase(host.getServerEnv()))
					sb.append('3');
				else
					sb.append('1');
				sb.append("0:15432/main?ssl=true&connectTimeout=5&tcpKeepAlive=true");
				return sb.toString();
			}

			@Override
			public boolean isOverriding() {
				return true;
			}
		});

		/*
		MappedHostSpecificValue kmUrl = new MappedHostSpecificValue();
		kmUrl.registerValue("DEV", "APR", "https://usadev01.usatech.com/KeyManager/soapservice/KeyManager");
		kmUrl.registerValue("INT", "APR", "https://ecckls01.usatech.com/KeyManager/soapservice/KeyManager");
		kmUrl.registerValue("ECC", "APR", "https://ecckls01.usatech.com/KeyManager/soapservice/KeyManager");
		kmUrl.registerValue("USA", "APR", "https://usakls21.trooper.usatech.com/KeyManager/soapservice/KeyManager");
		registerValue("USAT.keyManager.url", kmUrl);
		
		MappedHostSpecificValue km2Url = new MappedHostSpecificValue();
		km2Url.registerValue("DEV", "APR", "https://usadev01.usatech.com/KeyManager/soapservice/KeyManager");
		km2Url.registerValue("INT", "APR", "https://ecckls01.usatech.com/KeyManager/soapservice/KeyManager");
		km2Url.registerValue("ECC", "APR", "https://ecckls01.usatech.com/KeyManager/soapservice/KeyManager");
		km2Url.registerValue("USA", "APR", "https://usakls22.trooper.usatech.com/KeyManager/soapservice/KeyManager");
		registerValue("USAT.keyManager.url2", km2Url);
		*/
		registerValue("USAT.keyManager.cardReaderTypeKeyId[1]", new ServerEnvHostSpecificValue("1221661629429", "1221661629429", "1221661629429","1222874644089"));
		registerValue("USAT.keyManager.cardReaderTypeKeyId[2]", new ServerEnvHostSpecificValue("1221661629429", "1221661629429", "1221661629429","1222874644089"));
		 
		if(serverSet == null)
			registerValue("USAT.fileRepository.hostPrefix.layers", new ServerEnvHostSpecificValue("devmst1", "intmst1", "eccmst1", "usamst3"));
		else {
			StringBuilder sb = new StringBuilder();
			if(serverSet.getServers("MST")!=null){
				// when it has MST, setup processing MQ and FILE_REPO
				for(String db : new String[] { "MQ", "FILE_REPO" }) {
					int k = 1;
					for(Server server : serverSet.getServers("MST")) {
						for(String instance : server.instances) {
							Integer ordinal = (instance == null ? null : Integer.parseInt(instance));
							sb.setLength(0);
							String key = sb.append("USAT.database.").append(db).append('_').append(k++).append(".url").toString();
							sb.setLength(0);
							sb.append("jdbc:postgresql://").append(server.host.getSimpleName()).append(':');
							if(ordinal == null)
								sb.append(5432);
							else
								sb.append(5430 + ordinal);
							sb.append('/').append(db.toLowerCase().replace("_", "")).append("?ssl=true&connectTimeout=5&tcpKeepAlive=true");
							registerValue(key, new DefaultHostSpecificValue(sb.toString()));
						}
					}
				}
			}
			if (serverSet.getServers("MSR")!=null){
				//when it has MSR, setup processing MSR_MQ and MSR_FILE_REPO
				String msrMQPrefix="MSR_";
				for(String db : new String[] { "MQ", "FILE_REPO" }) {
					int k = 1;
					for(Server server : serverSet.getServers("MSR")) {
						for(String instance : server.instances) {
							Integer ordinal = (instance == null ? null : Integer.parseInt(instance));
							sb.setLength(0);
							String key = sb.append("USAT.database.").append(msrMQPrefix).append(db).append('_').append(k++).append(".url").toString();
							sb.setLength(0);
							sb.append("jdbc:postgresql://").append(server.host.getSimpleName()).append(':');
							if(ordinal == null)
								sb.append(5432);
							else
								sb.append(5430 + ordinal);
							sb.append('/').append(db.toLowerCase().replace("_", "")).append("?ssl=true&connectTimeout=5&tcpKeepAlive=true");
							registerValue(key, new DefaultHostSpecificValue(sb.toString()));
						}
					}
				}
			}
		}

		registerValue("STATIC.com.usatech.layers.common.InteractionUtils.tranImportQueueKey", new SingleServerTypeHostSpecificValue("APR", "usat.import.tran"));
		registerValue("STATIC.com.usatech.layers.common.InteractionUtils.checkTerminalQueueQos.timeToLive", new SingleServerTypeHostSpecificValue("APR", "2000"));
		registerValue("USAT.service.maxCancelAttempts", new DefaultHostSpecificValue("10"));
		registerValue("USAT.service.pausePerLoopMillis", new DefaultHostSpecificValue("-1"));
		registerValue("USAT.service.pausePerLoopIterations", new DefaultHostSpecificValue("1"));
		
		// USAT.usalive.baseUrl
		MappedHostSpecificValue usaliveBaseUrl = new MappedHostSpecificValue();
		usaliveBaseUrl.registerValue("DEV", "APP", "https://usalive-dev.usatech.com" + USATRegistry.USALIVE_CONTEXT_PATH + "/");
		usaliveBaseUrl.registerValue("DEV", "WEB", "https://usalive-dev.usatech.com" + USATRegistry.USALIVE_CONTEXT_PATH + "/");
		usaliveBaseUrl.registerValue("INT", "APP", "https://usalive-int.usatech.com" + USATRegistry.USALIVE_CONTEXT_PATH + "/");
		usaliveBaseUrl.registerValue("INT", "WEB", "https://usalive-int.usatech.com" + USATRegistry.USALIVE_CONTEXT_PATH + "/");
		usaliveBaseUrl.registerValue("ECC", "APP", "https://usalive-ecc.usatech.com" + USATRegistry.USALIVE_CONTEXT_PATH + "/");
		usaliveBaseUrl.registerValue("ECC", "WEB", "https://usalive-ecc.usatech.com" + USATRegistry.USALIVE_CONTEXT_PATH + "/");
		usaliveBaseUrl.registerValue("USA", "APP", "https://usalive.usatech.com" + USATRegistry.USALIVE_CONTEXT_PATH + "/");
		usaliveBaseUrl.registerValue("USA", "WEB", "https://usalive.usatech.com" + USATRegistry.USALIVE_CONTEXT_PATH + "/");
		registerValue("USAT.usalive.baseUrl", usaliveBaseUrl);

		// USAT.prepaid.baseUrl
		MappedHostSpecificValue prepaidBaseUrl = new MappedHostSpecificValue();
		prepaidBaseUrl.registerValue("DEV", "APP", "https://getmore-dev.usatech.com/");
		prepaidBaseUrl.registerValue("INT", "APP", "https://getmore-int.usatech.com/");
		prepaidBaseUrl.registerValue("ECC", "APP", "https://getmore-ecc.usatech.com/");
		prepaidBaseUrl.registerValue("USA", "APP", "https://getmore.usatech.com/");
		registerValue("USAT.prepaid.baseUrl", prepaidBaseUrl);
		
		//for WebHelper.checkEmail
		registerValue("STATIC.com.usatech.layers.common.util.WebHelper.emailVerification", new DefaultHostSpecificValue("MX_RECORD"));
		
		// used by both APR and APP
		registerValue("STATIC.com.usatech.layers.common.InteractionUtils.checkTerminalQueueKey", new DefaultHostSpecificValue("usat.posm.terminal.check"));
		
		//09132016 added for dialect
		registerValue("USAT.database.datasource.REPORT.validationQuery", new DefaultHostSpecificValue("{call DBMS_SESSION.MODIFY_PACKAGE_STATE(DBMS_SESSION.REINITIALIZE)}"));
		registerValue("USAT.database.datasource.REPORT.driverClassName", new DefaultHostSpecificValue("oracle.jdbc.driver.OracleDriver"));
		registerValue("USAT.database.datasource.OPER.validationQuery", new DefaultHostSpecificValue("{call DBMS_SESSION.MODIFY_PACKAGE_STATE(DBMS_SESSION.REINITIALIZE)}"));
		registerValue("USAT.database.datasource.OPER.driverClassName", new DefaultHostSpecificValue("oracle.jdbc.driver.OracleDriver"));
		registerValue("USAT.database.dialect", new DefaultHostSpecificValue("ora"));
		registerValue("USAT.quartz.jobStore.driverDelegateClass", new DefaultHostSpecificValue("org.quartz.impl.jdbcjobstore.oracle.OracleDelegate"));
		registerValue("simple.mq.Supervisor.directProducer.reprioritizeInterval", new DefaultHostSpecificValue("10000"));
		
		// for logs history 
		String[] logsLayerServerTypes = new String[] { "APP","APR" };
		MappedHostSpecificValue logsUrl = new MappedHostSpecificValue();
		logsUrl.registerValue("DEV", logsLayerServerTypes, LOGS_DEV);
		logsUrl.registerValue("INT", logsLayerServerTypes, LOGS_INT);
		logsUrl.registerValue("ECC", logsLayerServerTypes, LOGS_ECC);
		logsUrl.registerValue("USA", logsLayerServerTypes, LOGS_PROD);
		registerValue("USAT.database.LOGS.url", logsUrl);
		
		// for OraExport
		String[] OraExportServerTypes = new String[] { "LOGS" };
	    MappedHostSpecificValue ExpUrl = new MappedHostSpecificValue();
		ExpUrl.registerValue("DEV", OraExportServerTypes, OPER_DEV);
		ExpUrl.registerValue("INT", OraExportServerTypes, OPER_INT);
		ExpUrl.registerValue("ECC", OraExportServerTypes, OPER_ECC);
		registerValue("USAT.database.Export.url", ExpUrl);
		
	}

}
