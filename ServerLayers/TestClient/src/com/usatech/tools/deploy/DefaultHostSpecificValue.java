package com.usatech.tools.deploy;

import simple.io.Interaction;

public class DefaultHostSpecificValue implements HostSpecificValue {
	protected final String value;
	protected final boolean overriding;
	
	public DefaultHostSpecificValue(String value) {
		this(value, true);
	}

	public DefaultHostSpecificValue(String value, boolean overriding) {
		this.value = value;
		this.overriding = overriding;
	}
	@Override
	public String getValue(Host host, Interaction interaction) {
		return value;
	}

	@Override
	public boolean isOverriding() {
		return overriding;
	}
}
