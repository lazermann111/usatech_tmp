package com.usatech.tools.deploy;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import simple.app.Processor;
import simple.app.ServiceException;
import simple.io.BufferStream;
import simple.io.HeapBufferStream;
import simple.text.StringUtils;

public class PostgresSetupTask implements DeployTask {
	protected final Map<String, RoleAttr> roles = new HashMap<String, RoleAttr>();
	protected final Map<String, Set<String>> users = new HashMap<String, Set<String>>();
	protected final Map<String, Set<String>> tablespaces = new HashMap<String, Set<String>>();
	protected final Map<String, DbInfo> databases = new HashMap<String, DbInfo>();
	// protected final Map<SchemaObject, String> functions = new LinkedHashMap<SchemaObject, String>();
	// protected final Map<DbAndSchema, Processor<DeployTaskInfo, InputStream>> setups = new HashMap<DbAndSchema, Processor<DeployTaskInfo, InputStream>>();
	protected final String appDir;
	protected final String serviceName;
	protected final int port;
	protected boolean checkDefaultPrivileges = true;

	protected static class DbInfo {
		public String tablespace;
		protected final Map<String, SchemaInfo> schemas = new LinkedHashMap<String, SchemaInfo>();

		public SchemaInfo getOrCreateSchemaInfo(String schema) {
			schema = schema.trim().toLowerCase();
			SchemaInfo si = schemas.get(schema);
			if(si == null) {
				si = new SchemaInfo();
				schemas.put(schema, si);
			}
			return si;
		}
	}

	protected static class SchemaInfo {
		public final Map<String, String> functions = new LinkedHashMap<String, String>();
		public Processor<DeployTaskInfo, InputStream> setup;
	}

	protected static class RoleAttr {
		public final Set<String> options = new LinkedHashSet<String>();
		public final Set<String> subroles = new HashSet<String>();
	}

	public PostgresSetupTask(App app, Integer ordinal, int port) {
		this("/opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal), app.getName() + (ordinal == null ? "" : ordinal), port);
	}

	public PostgresSetupTask(String appDir, String serviceName, int port) {
		this.appDir = appDir;
		this.serviceName = serviceName;
		this.port = port;
	}

	protected DbInfo getOrCreateDbInfo(String database) {
		database = database.trim();
		DbInfo dbi = databases.get(database);
		if(dbi == null) {
			dbi = new DbInfo();
			databases.put(database, dbi);
		}
		return dbi;
	}

	public void registerFunction(String database, String schema, String name, String script) {
		getOrCreateDbInfo(database).getOrCreateSchemaInfo(schema).functions.put(name.trim().toLowerCase(), script);
	}

	public void registerSetup(String database, String schema, final File setupFile) {
		registerSetup(database, schema, new FileContent(setupFile));
	}

	public void registerSetup(String database, String schema, final File setupFile, String tablespace, String... grantCreate) {
		registerDatabase(database, tablespace, grantCreate);
		registerSetup(database, schema, new FileContent(setupFile));
	}

	public void registerSetup(String database, String schema, Processor<DeployTaskInfo, InputStream> setupFile) {
		getOrCreateDbInfo(database).getOrCreateSchemaInfo(schema).setup = setupFile;
	}
	
	public void registerSetup(String database, String schema, Processor<DeployTaskInfo, InputStream> setupFile, String tablespace, String... grantCreate) {
		registerDatabase(database, tablespace, grantCreate);
		registerSetup(database, schema, setupFile);
	}

	public void registerDatabase(String database, String tablespace, String... grantCreate) {
		tablespace = tablespace.trim();
		getOrCreateDbInfo(database).tablespace = tablespace;
		registerTablespace(tablespace, grantCreate);
	}

	public void registerTablespace(String tablespace, String... grantCreate) {
		tablespace = tablespace.trim();
		Set<String> grantSet = tablespaces.get(tablespace);
		if(grantSet == null) {
			grantSet = new HashSet<String>();
			tablespaces.put(tablespace, grantSet);
		}
		if(grantCreate != null)
			for(String grant : grantCreate)
				if(!StringUtils.isBlank(grant)) {
					grantSet.add(grant.trim());
					registerRole(grant, null);
				}
	}

	public void registerRole(String role, String[] options, String... subroles) {
		role = role.trim();
		RoleAttr roleAttr = roles.get(role);
		if(roleAttr == null) {
			roleAttr = new RoleAttr();
			roles.put(role, roleAttr);
		}
		if(options != null)
			for(String option : options)
				if(!StringUtils.isBlank(option))
					roleAttr.options.add(option.trim());
		if(subroles != null)
			for(String subrole : subroles)
				if(!StringUtils.isBlank(subrole)) {
					roleAttr.subroles.add(subrole.trim());
					registerRole(subrole.trim(), null);
				}
	}

	public void registerUser(String user, String... roles) {
		user = user.trim();
		Set<String> roleSet = users.get(user);
		if(roleSet == null) {
			roleSet = new HashSet<String>();
			users.put(user, roleSet);
		}
		if(roles != null)
			for(String role : roles)
				if(!StringUtils.isBlank(role)) {
					role = role.trim();
					roleSet.add(role);
					registerRole(role, null);
				}
	}

	@Override
	public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
		List<String> commands = new ArrayList<String>();
		commands.add("sudo su");
		if(!deployTaskInfo.host.isServerType("PGS")){
			commands.add("/usr/monit-latest/bin/monit start " + serviceName + "; while [ `/usr/monit-latest/bin/monit summary | grep -c \"Process '" + serviceName + "'.* pending\"` -gt 0 ]; do sleep 1; done");
		}
		for(String tablespace : tablespaces.keySet()) {
			commands.add("if [ ! -d " + appDir + "/tblspace/" + tablespace + " ]; then mkdir " + appDir + "/tblspace/" + tablespace + "; chown -R postgres:postgres " + appDir + "/tblspace/" + tablespace + "; fi");
		}
		if(checkDefaultPrivileges)
			commands.add("su - postgres -c '/usr/pgsql-latest/bin/psql -p " + port + " -t -c \"\\ddp\" -o \"|cat\"'");
		commands.add("su - postgres -c '/usr/pgsql-latest/bin/psql -p " + port + " -t -c \"select datname from pg_database;\" -o \"|cat\"'");
		commands.add("su - postgres -c '/usr/pgsql-latest/bin/psql -p " + port + " -t -c \"select spcname from pg_tablespace;\" -o \"|cat\"'");
		commands.add("su - postgres -c '/usr/pgsql-latest/bin/psql -p " + port + " -t -c \"select u.rolname, r.rolname from pg_authid u left join pg_auth_members m on m.member = u.oid left join pg_authid r on m.roleid = r.oid;\" -o \"|cat\"'");

		String[] ret = ExecuteTask.executeCommands(deployTaskInfo, commands.toArray(new String[commands.size()]));
		Map<String, Map<String, Set<String>>> existingDatabases = new HashMap<String, Map<String, Set<String>>>();
		BufferedReader reader;
		String line;
		String dblist = ret[ret.length - 3];
		if(dblist.startsWith("psql: "))
			throw new IOException("Error connecting to postgres database: " + dblist.substring(6));
		reader = new BufferedReader(new StringReader(dblist));
		while((line = reader.readLine()) != null) {
			existingDatabases.put(line.trim(), null);
		}
		boolean updatePrivs;
		if(checkDefaultPrivileges) {
			String privs = ret[ret.length - 4];
			if(privs.startsWith("psql: "))
				throw new IOException("Error connecting to postgres database: " + privs.substring(6));
			if(StringUtils.isBlank(privs.trim()) || privs.contains(" | function | public=X"))
				updatePrivs = true;
			else
				updatePrivs = false;
		} else
			updatePrivs = false;
		Set<String> existingTblsps = new HashSet<String>();
		String tbsplist = ret[ret.length - 2];
		if(tbsplist.startsWith("psql: "))
			throw new IOException("Error connecting to postgres database: " + tbsplist.substring(6));
		reader = new BufferedReader(new StringReader(tbsplist));
		while((line = reader.readLine()) != null) {
			existingTblsps.add(line.trim());
		}
		Map<String, Set<String>> existingUsers = new HashMap<String, Set<String>>();
		String userlist = ret[ret.length - 1];
		if(userlist.startsWith("psql: "))
			throw new IOException("Error connecting to postgres database: " + userlist.substring(6));

		reader = new BufferedReader(new StringReader(userlist));
		while((line = reader.readLine()) != null) {
			String[] parts = StringUtils.split(line, '|', 1);
			String user = parts[0].trim();
			String role = parts[1].trim();
			Set<String> roles = existingUsers.get(user);
			if(roles == null) {
				roles = new HashSet<String>();
				existingUsers.put(user, roles);
			}
			if(!StringUtils.isBlank(role))
				roles.add(role);
		}
		BufferStream bufferStream = new HeapBufferStream();
		PrintWriter writer = new PrintWriter(bufferStream.getOutputStream());
		// writer.println("DO $$");
		// writer.println("BEGIN");
		boolean update = false;
		if(updatePrivs) {
			update = true;
			writer.println("ALTER DEFAULT PRIVILEGES REVOKE EXECUTE ON FUNCTIONS FROM PUBLIC;");
		}
		for(Map.Entry<String, RoleAttr> entry : roles.entrySet()) {
			String role = entry.getKey();
			RoleAttr roleAttr = entry.getValue();
			if(!existingUsers.containsKey(role) && !users.containsKey(role)) {
				update = true;
				// writer.print("IF NOT EXISTS(SELECT USENAME FROM PG_USER WHERE USENAME='");
				// writer.print(role);
				// writer.println("') THEN");
				writer.print("CREATE ROLE ");
				writer.print(role);
				if(!roleAttr.options.isEmpty()) {
					writer.print(" WITH");
					for(String option : roleAttr.options) {
						writer.print(' ');
						writer.print(option);
					}
				}
				writer.println(';');
				// writer.println("END IF;");
			}
			// ???: SHould we revoke also ?
			Set<String> existingRoles = existingUsers.get(role);
			for(String subrole : roleAttr.subroles) {
				if(existingRoles == null || !existingRoles.contains(subrole)) {
					update = true;
					writer.print("GRANT ");
					writer.print(subrole);
					writer.print(" TO ");
					writer.print(role);
					writer.println(';');
				}
			}
		}
		for(Map.Entry<String, Set<String>> entry : users.entrySet()) {
			String user = entry.getKey();
			Set<String> roles = entry.getValue();
			if(!existingUsers.containsKey(user)) {
				update = true;
				// writer.print("IF NOT EXISTS(SELECT USENAME FROM PG_USER WHERE USENAME='");
				// writer.print(user);
				// writer.println("') THEN");
				writer.print("CREATE USER ");
				writer.print(user);
				writer.print(" PASSWORD '");
				if(deployTaskInfo.host.getServerEnv().equalsIgnoreCase("LOCAL") || deployTaskInfo.host.getServerEnv().equalsIgnoreCase("DEV") || deployTaskInfo.host.getServerEnv().equalsIgnoreCase("INT")) {
					if(user.matches("^dms_\\d+$"))
						writer.print("DMS_USER_1");
					else
						writer.print(user.replaceAll("_\\d+$", "_1").toUpperCase());
				} else {
					String baseUser = user.replaceAll("_\\d+$", "");
					String serverType = deployTaskInfo.host.getServerTypes().iterator().next();
					writer.print(DeployUtils.getPassword(deployTaskInfo.passwordCache, deployTaskInfo.host, "database." + serverType + "." + baseUser, baseUser + " for " + serverType, deployTaskInfo.interaction));
				}
				writer.println("';");
				// writer.println("END IF;");
			}
			if(roles != null) {
				// ???: SHould we revoke also ?
				Set<String> existingRoles = existingUsers.get(user);
				for(String role : roles) {
					if(existingRoles == null || !existingRoles.contains(role)) {
						update = true;
						writer.print("GRANT ");
						writer.print(role);
						writer.print(" TO ");
						writer.print(user);
						writer.println(';');
					}
				}
			}
		}
		commands.clear();
		int tbspCnt = 0;
		for(Map.Entry<String, Set<String>> entry : tablespaces.entrySet()) {
			String tablespace = entry.getKey();
			Set<String> grants = entry.getValue();
			if(!existingTblsps.contains(tablespace)) {
				if(commands.isEmpty())
					commands.add("sudo su - postgres");
				tbspCnt++;
				commands.add("mkdir -p '" + appDir + "/tblspace/" + tablespace + "'");
				commands.add("/usr/pgsql-latest/bin/psql -p " + port + " -c \"CREATE TABLESPACE " + tablespace + " LOCATION '" + appDir + "/tblspace/" + tablespace + "';\"");
				if(grants != null) {
					update = true;
					for(String grant : grants) {
						writer.print("GRANT CREATE ON TABLESPACE ");
						writer.print(tablespace);
						writer.print(" TO ");
						writer.print(grant);
						writer.println(';');
					}
				}
			}
		}
		int dbCnt = 0;
		for(Map.Entry<String, DbInfo> entry : databases.entrySet()) {
			String database = entry.getKey();
			String tablespace = entry.getValue().tablespace;
			if(!existingDatabases.containsKey(database)) {
				if(commands.isEmpty())
					commands.add("sudo su - postgres");
				dbCnt++;
				commands.add("/usr/pgsql-latest/bin/psql -p " + port + " -c \"CREATE DATABASE " + database + " TABLESPACE " + tablespace + ";\"");
			}
		}
		// writer.println("END");
		// writer.println("$$");
		writer.flush();
		if(!commands.isEmpty()) {
			deployTaskInfo.interaction.printf("Setting up Postgres Cluster tablespaces and databases");
			ExecuteTask.executeCommands(deployTaskInfo, commands);
			commands.clear();
		}

		// -----------
		commands.add("sudo su");
		for(Map.Entry<String, DbInfo> entry : databases.entrySet()) {
			StringBuilder sb = new StringBuilder();
			for(Map.Entry<String, SchemaInfo> schemaEntry : entry.getValue().schemas.entrySet()) {
				if(!schemaEntry.getValue().functions.isEmpty()) {
					if(sb.length() == 0) {
						sb.append("su - postgres -c '/usr/pgsql-latest/bin/psql ").append(entry.getKey()).append(" -p ").append(port).append(" -t -c \"select nspname, proname, pronargs from pg_namespace n left outer join pg_proc p on p.pronamespace = n.oid and (");
					} else {
						sb.append(" or ");
					}
					sb.append("(nspname = '\"'\"'").append(schemaEntry.getKey()).append("'\"'\"' and proname in(");
					for(String fn : schemaEntry.getValue().functions.keySet())
						sb.append("'\"'\"'").append(fn).append("'\"'\"',");
					sb.setLength(sb.length() - 1);
					sb.append("))");
				}
			}
			if(sb.length() == 0)
				sb.append("su - postgres -c '/usr/pgsql-latest/bin/psql ").append(entry.getKey()).append(" -p ").append(port).append(" -t -c \"select nspname from pg_namespace");
			else
				sb.append(')');
			sb.append(";\" -o \"|cat\"'");
			commands.add(sb.toString());
		}
		ret = ExecuteTask.executeCommands(deployTaskInfo, commands.toArray(new String[commands.size()]));
		int i = 1;
		for(String database : databases.keySet()) {
			if(existingDatabases.containsKey(database)) {
				String r = ret[i];
				if(r.startsWith("psql: "))
					throw new IOException("Error connecting to postgres database: " + r.substring(6));
				Map<String, Set<String>> schemas = new HashMap<String, Set<String>>();
				existingDatabases.put(database, schemas);
				reader = new BufferedReader(new StringReader(r));
				while((line = reader.readLine()) != null) {
					String[] parts = StringUtils.split(line, '|', 2);
					String schema = parts[0].trim();
					Set<String> fns = schemas.get(schema);
					if(fns == null) {
						fns = new HashSet<String>();
						schemas.put(schema, fns);
					}
					if(parts.length > 1) {
						String name = parts[1].trim();
						// String cnt = parts[2].trim();
						if(!name.isEmpty())
							fns.add(name);
					}
				}
			}
			i++;
		}

		// -----------
		if(update) {
			deployTaskInfo.interaction.printf("Setting up Postgres Cluster");
			String filename = "postgres_setup.sql";
			String filepath = "/home/" + getUserName() + "/" + filename;
			UploadTask.writeFileUsingTmp(deployTaskInfo, bufferStream.getInputStream(), 0640, getUserName(), getUserName(), null, filepath);
			ExecuteTask.executeCommands(deployTaskInfo, "sudo su - postgres -c '/usr/pgsql-latest/bin/psql -p " + port + " -f " + filepath + "'", "sudo /bin/rm " + filepath);
		} else
			deployTaskInfo.interaction.printf("Postgres Cluster is update to date");
		int schemaCnt = 0;
		for(Map.Entry<String, DbInfo> entry : databases.entrySet()) {
			String database = entry.getKey();
			Map<String, Set<String>> existingSchemas = existingDatabases.get(database);
			for(Map.Entry<String, SchemaInfo> schemaEntry : entry.getValue().schemas.entrySet()) {
				String schema = schemaEntry.getKey();
				SchemaInfo si = schemaEntry.getValue();
				if(si.setup != null && (existingSchemas == null || !existingSchemas.containsKey(schema))) {
					deployTaskInfo.interaction.printf("Setting up Postgres schema '" + schema + "' in database '" + database + "'");
					String filepath = "/home/" + getUserName() + "/" + schema + "_setup.sql";
					InputStream in;
					try {
						in = si.setup.process(deployTaskInfo);
					} catch(ServiceException e) {
						throw new IOException(e);
					}
					try {
						UploadTask.writeFileUsingTmp(deployTaskInfo, in, 0640, getUserName(), getUserName(), null, filepath);
					} finally {
						in.close();
					}
					ExecuteTask.executeCommands(deployTaskInfo, "sudo su - postgres -c '/usr/pgsql-latest/bin/psql " + database + " -p " + port + " -f " + filepath + "'", "sudo /bin/rm " + filepath);
					schemaCnt++;
				} else
					deployTaskInfo.interaction.printf("Postgres schema '" + schema + "' in database '" + database + "' is already setup");
				if(!si.functions.isEmpty()) {
					Set<String> existingFunctions = existingSchemas.get(schema);
					bufferStream.clear();
					for(Map.Entry<String, String> fnEntry : si.functions.entrySet()) {
						if(existingFunctions == null || !existingFunctions.contains(fnEntry.getKey())) {
							writer.write(fnEntry.getValue());
							writer.write('\n');
						}
					}
					if(bufferStream.getLength() > 0) {
						deployTaskInfo.interaction.printf("Adding functions to schema '" + schema + "' on database '" + database + "'");
						String filepath = "/home/" + getUserName() + "/" + schema + "_functions.sql";
						UploadTask.writeFileUsingTmp(deployTaskInfo, bufferStream.getInputStream(), 0640, getUserName(), getUserName(), null, filepath);
						ExecuteTask.executeCommands(deployTaskInfo, "sudo su - postgres -c '/usr/pgsql-latest/bin/psql " + database + " -p " + port + " -f " + filepath + "'", "sudo /bin/rm " + filepath);
					}
				}
			}
		}
		if(!update && tbspCnt == 0 && dbCnt == 0 && schemaCnt == 0)
			return "Postgres Cluster is update to date";
		StringBuilder sb = new StringBuilder();
		if(update)
			sb.append("Created users or granted roles");
		if(tbspCnt > 0) {
			if(sb.length() > 0)
				sb.append(" and ");
			sb.append("Created ").append(tbspCnt).append(" tablespaces");
		}
		if(dbCnt > 0) {
			if(sb.length() > 0)
				sb.append(" and ");
			sb.append("Created ").append(dbCnt).append(" databases");
		}
		if(schemaCnt > 0) {
			if(sb.length() > 0)
				sb.append(" and ");
			sb.append("Created ").append(schemaCnt).append(" schemas and their objects");
		}

		return sb.toString();
	}

	protected String getUserName() {
		return "postgres";
	}

	@Override
	public String getDescription(DeployTaskInfo deployTaskInfo) {
		StringBuilder sb = new StringBuilder();
		sb.append("Setting up Postgres Cluster");
		return sb.toString();
	}

	public boolean isCheckDefaultPrivileges() {
		return checkDefaultPrivileges;
	}

	public void setCheckDefaultPrivileges(boolean checkDefaultPrivileges) {
		this.checkDefaultPrivileges = checkDefaultPrivileges;
	}
}
