package com.usatech.tools.deploy;

import java.util.HashMap;
import java.util.Map;

import simple.io.Interaction;
import simple.util.CaseInsensitiveHashMap;

public class AliasMapHostSpecificValue implements HostSpecificValue {
	protected final Map<String, Map<String, String>> aliasMaps = new CaseInsensitiveHashMap<Map<String, String>>();

	public void registerAlias(int port, String alias) {
		registerAlias(null, port, alias, null);
	}

	public void registerAlias(String destHost, int port, String alias) {
		registerAlias(destHost, port, alias, null);
	}

	public void registerAlias(int port, String alias, String serverEnv) {
		registerAlias(null, port, alias, serverEnv);
	}

	public void registerAlias(String destHost, int port, String alias, String serverEnv) {
		Map<String, String> aliasMap = aliasMaps.get(serverEnv);
		if(aliasMap == null) {
			aliasMap = new HashMap<String, String>();
			aliasMaps.put(serverEnv, aliasMap);
		}
		aliasMap.put((destHost == null || (destHost = destHost.trim()).length() == 0 ? ".*" : destHost) + ':' + port, alias == null || (alias = alias.trim()).length() == 0 ? "-" : alias);
	}
	@Override
	public String getValue(Host host, Interaction interaction) {
		Map<String, String> allAliasMap = aliasMaps.get(null);
		Map<String, String> specificAliasMap = aliasMaps.get(host.getServerEnv());
		StringBuilder sb = new StringBuilder();
		sb.append('{');
		if(specificAliasMap != null)
			for(Map.Entry<String, String> entry : specificAliasMap.entrySet()) {
				if(sb.length() > 1)
					sb.append(',');
				sb.append(entry.getKey()).append('=').append(entry.getValue());
			}
		if(allAliasMap != null)
			for(Map.Entry<String, String> entry : allAliasMap.entrySet()) {
				if(specificAliasMap != null && specificAliasMap.containsKey(entry.getKey()))
					continue;
				if(sb.length() > 1)
					sb.append(',');
				sb.append(entry.getKey()).append('=').append(entry.getValue());
			}
		sb.append('}');

		return sb.toString();
	}

	@Override
	public boolean isOverriding() {
		return true;
	}
}
