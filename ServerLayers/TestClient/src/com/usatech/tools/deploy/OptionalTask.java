package com.usatech.tools.deploy;

import java.io.IOException;
import java.util.regex.Pattern;

public class OptionalTask implements DeployTask {
	protected final DeployTask[] falseDelegates;
	protected final DeployTask[] trueDelegates;
	protected final String command;
	protected final Pattern resultPattern;
	protected final boolean negate;

	public OptionalTask(String command, Pattern resultPattern, boolean negate, DeployTask... delegates) {
		this.trueDelegates = delegates;
		this.falseDelegates = null;
		this.command = command;
		this.resultPattern = resultPattern;
		this.negate = negate;
	}

	public OptionalTask(String command, Pattern resultPattern, DeployTask trueDelegate, DeployTask falseDelegate) {
		this.trueDelegates = new DeployTask[] { trueDelegate };
		this.falseDelegates = new DeployTask[] { falseDelegate };
		this.command = command;
		this.resultPattern = resultPattern;
		this.negate = false;
	}

	public OptionalTask(String command, Pattern resultPattern, DeployTask[] trueDelegates, DeployTask[] falseDelegates) {
		this.trueDelegates = trueDelegates;
		this.falseDelegates = falseDelegates;
		this.command = command;
		this.resultPattern = resultPattern;
		this.negate = false;
	}
	@Override
	public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
		String[] results = ExecuteTask.executeCommands(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.getSsh(), command);
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		if(resultPattern.matcher(results[0]).matches() != negate) {
			sb.append("Executed ").append(trueDelegates.length).append(" sub-tasks: ");
			for(DeployTask delegate : trueDelegates) {
				if(first)
					first = false;
				else
					sb.append("; ");
				sb.append(delegate.perform(deployTaskInfo));
			}
		} else if(falseDelegates != null) {
			sb.append("Executed ").append(falseDelegates.length).append(" sub-tasks: ");
			for(DeployTask delegate : falseDelegates) {
				if(first)
					first = false;
				else
					sb.append("; ");
				sb.append(delegate.perform(deployTaskInfo));
			}
		}
		return sb.toString();
	}

	@Override
	public String getDescription(DeployTaskInfo deployTaskInfo) {
		StringBuilder sb = new StringBuilder();
		sb.append("Checking '").append(command).append("' for '").append(resultPattern.pattern()).append("' and performing ");
		if(trueDelegates != null && trueDelegates.length > 0) {
			sb.append('[');
			for(int i = 0; i < trueDelegates.length; i++) {
				if(i > 0)
					sb.append(", ");
				sb.append(trueDelegates[i].getDescription(deployTaskInfo));
			}
			sb.append(" if ").append(String.valueOf(!negate).toUpperCase());
		}
		if(falseDelegates != null && falseDelegates.length > 0) {
			if(trueDelegates != null && trueDelegates.length > 0)
				sb.append(" and performing ");
			sb.append('[');
			for(int i = 0; i < falseDelegates.length; i++) {
				if(i > 0)
					sb.append(", ");
				sb.append(falseDelegates[i].getDescription(deployTaskInfo));
			}
			sb.append(" if ").append(String.valueOf(negate).toUpperCase());
		}
		return sb.toString();
	}
}
