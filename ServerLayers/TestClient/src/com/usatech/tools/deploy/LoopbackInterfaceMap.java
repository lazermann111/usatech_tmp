package com.usatech.tools.deploy;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;

public class LoopbackInterfaceMap {
	
	public static HashMap<String,String> DEV_LOOPBACK_MAP=new HashMap<String, String>();
	public static HashMap<String,String> INT_LOOPBACK_MAP=new HashMap<String, String>();
	public static HashMap<String,String> ECC_LOOPBACK_MAP=new HashMap<String, String>();
	public static HashMap<String,String> USA_LOOPBACK_MAP=new HashMap<String, String>();
	
	public static HashMap<String,String> ECC_REAL_IP_MAP=new HashMap<String, String>();
	public static HashMap<String,String> USA_REAL_IP_MAP=new HashMap<String, String>();
	
	
	
	static{
		DEV_LOOPBACK_MAP.put("10.0.0.22", "1");
		DEV_LOOPBACK_MAP.put("10.0.0.24", "2");
		DEV_LOOPBACK_MAP.put("10.0.0.29", "3");
		DEV_LOOPBACK_MAP.put("10.0.0.32", "4");
		DEV_LOOPBACK_MAP.put("10.0.0.36", "5");
		DEV_LOOPBACK_MAP.put("10.0.0.67", "6");
		DEV_LOOPBACK_MAP.put("10.0.0.229", "7");//prepaid-dev.usatech.com
		
		INT_LOOPBACK_MAP.put("10.0.0.30", "1");
		INT_LOOPBACK_MAP.put("10.0.0.241", "2");
		INT_LOOPBACK_MAP.put("10.0.0.242", "3");
		INT_LOOPBACK_MAP.put("10.0.0.243", "4");
		INT_LOOPBACK_MAP.put("10.0.0.244", "5");
		INT_LOOPBACK_MAP.put("10.0.0.68", "6");
		INT_LOOPBACK_MAP.put("10.0.0.230", "7");//prepaid-int.usatech.com
		
		ECC_LOOPBACK_MAP.put("192.168.4.100", "1");//usalive-ecc
		ECC_LOOPBACK_MAP.put("192.168.4.101", "2");//esuds-ecc
		ECC_LOOPBACK_MAP.put("192.168.4.102", "3");//hotchoice-ecc
		ECC_LOOPBACK_MAP.put("192.168.4.103", "4");//www-ecc.usatech.com
		ECC_LOOPBACK_MAP.put("192.168.4.104", "5");//verizon-ecc
		ECC_LOOPBACK_MAP.put("192.168.4.63", "6");//dms-ecc.usatech.com
		ECC_LOOPBACK_MAP.put("192.168.4.105", "7");// prepaid-ecc.usatech.com
		
		USA_LOOPBACK_MAP.put("192.168.79.194", "1");//usalive.usatech.com
		USA_LOOPBACK_MAP.put("192.168.79.197", "2");//www.usatech.com
		USA_LOOPBACK_MAP.put("192.168.79.198", "3");//www.esuds.net
		USA_LOOPBACK_MAP.put("192.168.79.199", "4");//hotchoice.usatech.com
		USA_LOOPBACK_MAP.put("192.168.79.200", "5");//verizon.usatech.com
		USA_LOOPBACK_MAP.put("192.168.79.163", "1");//dms.usatech.com
		USA_LOOPBACK_MAP.put("192.168.79.205", "6");//prepaid.usatech.com
		USA_LOOPBACK_MAP.put("192.168.79.206", "7");// getmore.usatech.com
		
		ECC_REAL_IP_MAP.put("usalive-ecc.usatech.com", "192.168.4.100");//usalive-ecc
		ECC_REAL_IP_MAP.put("esuds-ecc.usatech.com", "192.168.4.101");//esuds-ecc
		ECC_REAL_IP_MAP.put("esudsweb-ecc.usatech.com", "192.168.4.101");//esudsweb-ecc.usatech.com
		ECC_REAL_IP_MAP.put("hotchoice-ecc.usatech.com", "192.168.4.102");//hotchoice-ecc
		ECC_REAL_IP_MAP.put("www-ecc.usatech.com", "192.168.4.103");//www-ecc.usatech.com
		ECC_REAL_IP_MAP.put("verizon-ecc.usatech.com", "192.168.4.104");//verizon-ecc
		ECC_REAL_IP_MAP.put("dms-ecc.usatech.com", "192.168.4.63");//dms-ecc.usatech.com
		ECC_REAL_IP_MAP.put("prepaid-ecc.usatech.com", "192.168.4.105");// prepaid-ecc.usatech.com
		ECC_REAL_IP_MAP.put("getmore-ecc.usatech.com", "192.168.4.105");// getmore-ecc.usatech.com
		
		USA_REAL_IP_MAP.put("usalive.usatech.com", "192.168.79.194");//usalive.usatech.com
		USA_REAL_IP_MAP.put("www.usatech.com", "192.168.79.197");//www.usatech.com
		USA_REAL_IP_MAP.put("esudsweb.usatech.com", "192.168.79.198");//esudsweb.usatech.com
		USA_REAL_IP_MAP.put("www.esuds.net", "192.168.79.198");//www.esuds.net
		USA_REAL_IP_MAP.put("hotchoice.usatech.com", "192.168.79.199");//hotchoice.usatech.com
		USA_REAL_IP_MAP.put("verizon.usatech.com", "192.168.79.200");//verizon.usatech.com
		USA_REAL_IP_MAP.put("dms.usatech.com", "192.168.79.163");//dms.usatech.com
		USA_REAL_IP_MAP.put("prepaid.usatech.com", "192.168.79.205");//prepaid.usatech.com
		USA_REAL_IP_MAP.put("getmore.usatech.com", "192.168.79.206");// getmore.usatech.com
	}
	
	public static HashMap<String,String> getMapByEnv(String env){
		if(env.equalsIgnoreCase("DEV")){
			return DEV_LOOPBACK_MAP;
		}else if(env.equalsIgnoreCase("INT")){
			return INT_LOOPBACK_MAP;
		}else if(env.equalsIgnoreCase("ECC")){
			return ECC_LOOPBACK_MAP;
		}else if(env.equalsIgnoreCase("USA")){
			return USA_LOOPBACK_MAP;
		}else{
			throw new IllegalArgumentException("Unknow environment.");
		}
	}
	
	public static String getHostRealIp(String env, String hostname) throws UnknownHostException{
		if(env.equalsIgnoreCase("DEV")||env.equalsIgnoreCase("INT")){
			return InetAddress.getByName(hostname).getHostAddress();
		}else if(env.equalsIgnoreCase("ECC")){
			return ECC_REAL_IP_MAP.get(hostname.toLowerCase());
		}else if(env.equalsIgnoreCase("USA")){
			return USA_REAL_IP_MAP.get(hostname.toLowerCase());
		}else{
			throw new IllegalArgumentException("Unknow env="+env+" hostname="+hostname);
		}
	}

}
