/**
 * 
 */
package com.usatech.tools.deploy;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import simple.app.Processor;
import simple.bean.ConvertUtils;
import simple.io.IOUtils;
import simple.text.StringUtils;


public class UploadAndRunPostgresSQLTask extends UploadTask {
	protected final String[] databases;
	protected final Boolean shutdown;
	protected final Integer ordinal;

	public UploadAndRunPostgresSQLTask(String cvsFilePath, String cvsRevision, Boolean shutdown, String... databases) {
		this(cvsFilePath, cvsRevision, shutdown, null, databases);
	}

	public UploadAndRunPostgresSQLTask(String cvsFilePath, String cvsRevision, Boolean shutdown, Integer ordinal, String... databases) {
		this(new GitPullTask(cvsFilePath, cvsRevision), shutdown, ordinal, databases);
	}

	public UploadAndRunPostgresSQLTask(DefaultPullTask vcsPullTask, Boolean shutdown, String... databases) {
		this(vcsPullTask, shutdown, null, databases);
	}

	public UploadAndRunPostgresSQLTask(DefaultPullTask vcsPullTask, Boolean shutdown, Integer ordinal, String... databases) {
		super(vcsPullTask, 0644, "postgres", "postgres", IOUtils.getFileName(vcsPullTask.getFilePath()), true, "/home/postgres/" + IOUtils.getFileName(vcsPullTask.getFilePath()));
		this.databases = databases;
		this.shutdown = shutdown;
		this.privileged = false;
		this.ordinal = ordinal;
	}

	public UploadAndRunPostgresSQLTask(File file, Boolean shutdown, String... databases) throws IOException {
		this(file, shutdown, null, databases);
	}

	public UploadAndRunPostgresSQLTask(String sql, Boolean shutdown, String... databases) {
		this(sql, shutdown, null, databases);
	}

	public UploadAndRunPostgresSQLTask(File file, Boolean shutdown, Integer ordinal, String... databases) throws IOException {
		super(file, "/home/postgres/", 0644, "postgres", "postgres");
		this.databases = databases;
		this.shutdown = shutdown;
		this.privileged = false;
		this.ordinal = ordinal;
	}

	public UploadAndRunPostgresSQLTask(String sql, Boolean shutdown, Integer ordinal, String... databases) {
		super(new ByteArrayInputStream(sql.getBytes()), 0644, "postgres", "postgres", "Inline postgres script", true, "/tmp/tmp_postgres_script_" + StringUtils.toHex(Double.doubleToLongBits(Math.random())) + ".sql");
		this.databases = databases;
		this.shutdown = shutdown;
		this.privileged = false;
		this.ordinal = ordinal;
	}

	public UploadAndRunPostgresSQLTask(Processor<DeployTaskInfo, InputStream> sqlGenerator, Boolean shutdown, Integer ordinal, String... databases) {
		super(sqlGenerator, 0644, "postgres", "postgres", "Inline postgres script", true, "/tmp/tmp_postgres_script_" + StringUtils.toHex(Double.doubleToLongBits(Math.random())) + ".sql");
		this.databases = databases;
		this.shutdown = shutdown;
		this.privileged = false;
		this.ordinal = ordinal;
	}

	@Override
	public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
		return perform(deployTaskInfo, deployTaskInfo.host, deployTaskInfo.user);
	}

	public String perform(DeployTaskInfo deployTaskInfo, Host host, String user) throws IOException, InterruptedException {
		String ret = super.perform(deployTaskInfo, host, user);
		String postgres_bin;
		String postgres_data;
		String postgres_logs;
		String stopCommand;
		String startCommand;

		char penult = host.getSimpleName().charAt(host.getSimpleName().length() - 2);
		if(penult == '1' || penult == '3') {
			// linux
			postgres_bin = "/usr/pgsql-latest/bin";
			postgres_data = "/opt/USAT/postgres" + (ordinal == null ? "" : ordinal.toString()) + "/data";
			postgres_logs = "/opt/USAT/postgres" + (ordinal == null ? "" : ordinal.toString()) + "/logs";
			stopCommand = "/usr/monit-latest/bin/monit stop postgres" + (ordinal == null ? "" : ordinal.toString() + "; su - postgres -c '" + postgres_bin + "/pg_ctl -D " + postgres_data + " stop -m fast'");
			startCommand = "/usr/monit-latest/bin/monit start postgres" + (ordinal == null ? "" : ordinal.toString());
		} else {
			// solaris
			postgres_bin = "/opt/USAT/postgres/latest/bin/64";
			postgres_data = "/opt/USAT/postgres/latest/data";
			postgres_logs = "/opt/USAT/postgres/latest/data/logs";
			stopCommand = "svcadm -v disable -st postgresql";
			startCommand = "svcadm -v enable -s postgresql";
		}
		boolean shutdownValue;
		if(shutdown == null) {
			String answer = deployTaskInfo.interaction.readLine("Should postgres database on " + host.getSimpleName() + " be shutdown to run the script '" + contentPath + "'?");
			shutdownValue = ConvertUtils.getBooleanSafely(answer, false);
		} else
			shutdownValue = shutdown;
		List<String> commands = new ArrayList<String>();
		commands.add("sudo su");
		int port;
		if(shutdownValue) {
			port = 5435;
			commands.add(stopCommand);
			commands.add("su - postgres -c '" + postgres_bin + "/pg_ctl -w -D " + postgres_data + " -l " + postgres_logs + "/server.log start -o \"-p " + port + "\"'");
		} else if(ordinal != null) {
			port = 5430 + ordinal;
		} else {
			port = 5432;
		}
		for(String remotePath : remoteFilePaths) {
			if(!remotePath.startsWith("/") && !remotePath.startsWith("\\"))
				remotePath = deployTaskInfo.sshClientCache.getSftpClient(host, user).pwd() + "/" + remotePath;
			for(String database : databases) {
				commands.add(buildExecuteSQLCommand(postgres_bin, database, port, remotePath));
			}
		}
		if(shutdownValue) {
			commands.add("su - postgres -c '" + postgres_bin + "/pg_ctl -D " + postgres_data + " stop -m fast'");
			commands.add(startCommand);
		}
		ExecuteTask.executeCommands(host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache.getSshClient(host, user), commands);
		return ret + " and executed script";
	}	

	protected String buildExecuteSQLCommand(String postgres_bin, String database, int port, String filePath) {
		return "su - postgres -c '" + postgres_bin + "/psql " + database + " -p " + port + " -f " + filePath + "'";
	}

	@Override
	public String getDescription(DeployTaskInfo deployTaskInfo) {
		StringBuilder sb = new StringBuilder(super.getDescription(deployTaskInfo));
		sb.append(" and executing it on postgres databases ").append(Arrays.toString(databases));
		return sb.toString();
	}
}