package com.usatech.tools.deploy;

import java.io.IOException;
import java.util.BitSet;
import java.util.Map;
import java.util.regex.Pattern;

import simple.util.SimpleLinkedHashMap;

public class ChooseTask implements DeployTask {
	protected final Map<Pattern, DeployTask[]> choices = new SimpleLinkedHashMap<Pattern, DeployTask[]>() {
		private static final long serialVersionUID = 6819762912404501463L;
		protected boolean keyEquals(Pattern key1, Pattern key2) {
			return key1.pattern().equals(key2.pattern()) && key1.flags() == key2.flags();
		}
		@Override
		protected int hash(Pattern x) {
			return x.pattern().hashCode();
		}
	};
	protected final String command;
	protected final boolean negate;
	protected final BitSet commandSuccess;
	protected static final BitSet DEFAULT_COMMAND_SUCCESS = new BitSet();

	static {
		DEFAULT_COMMAND_SUCCESS.set(0);
	}

	public ChooseTask(String command, boolean negate, BitSet commandSuccess) {
		this.command = command;
		this.negate = negate;
		this.commandSuccess = (commandSuccess == null ? DEFAULT_COMMAND_SUCCESS : commandSuccess);
	}

	public ChooseTask(String command, boolean negate) {
		this(command, negate, null);
	}

	public ChooseTask(String command) {
		this(command, false);
	}

	@Override
	public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
		String[] results = ExecuteTask.executeCommands(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.getSsh(), commandSuccess, command);
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		for(Map.Entry<Pattern, DeployTask[]> entry : choices.entrySet()) {
			if(entry.getKey().matcher(results[0]).matches() != negate) {
				sb.append("Executed ").append(entry.getValue().length).append(" sub-tasks: ");
				for(DeployTask delegate : entry.getValue()) {
					if(first)
						first = false;
					else
						sb.append("; ");
					sb.append(delegate.perform(deployTaskInfo));
				}
				return sb.toString();
			}
		}
		return "Result '" + results[0] + "' did not match any choices";
	}

	public void registerChoice(Pattern match, DeployTask... delegates) {
		choices.put(match, delegates);
	}

	public void deregisterChoice(Pattern match) {
		choices.remove(match);
	}
	@Override
	public String getDescription(DeployTaskInfo deployTaskInfo) {
		StringBuilder sb = new StringBuilder();
		sb.append("Checking '").append(command).append("' for ").append(choices.size()).append("choices");
		return sb.toString();
	}
}
