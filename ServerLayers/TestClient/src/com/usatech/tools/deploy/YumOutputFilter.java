package com.usatech.tools.deploy;


public class YumOutputFilter implements OutputFilter {

	@Override
	public String filter(String argument) {
		int p = argument.indexOf("###");
		if(p < 0)
			return argument;
		int s = 0;
		StringBuilder sb = new StringBuilder();
		do {
			p++;
			sb.append(argument, s, p);
			sb.append("..");
			p += 2;
			for(; p < argument.length() && argument.charAt(p) == '#'; p++)
				;
			sb.append("#");
			s = p;
			p = argument.indexOf("###", s);
		} while(p > 0);
		sb.append(argument, s, argument.length());
		return sb.toString();
	}
}
