package com.usatech.tools.deploy;

import java.io.InputStream;

import simple.app.Processor;

public abstract class DefaultPullTask implements DeployTask, Processor<DeployTaskInfo, InputStream> {

	public abstract InputStream getResultInputStream();

	public abstract String getFilePath(); 
	
}
