package com.usatech.tools.deploy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import simple.text.StringUtils;
import simple.util.CollectionUtils;

public class UsatIPTablesUpdateTask extends IPTablesUpdateTask {
	protected final ServerSet serverSet;

	public UsatIPTablesUpdateTask(ServerSet serverSet) {
		super();
		this.serverSet = serverSet;
	}

	@Override
	public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
		this.rules.clear();
		updateForUsat(this, deployTaskInfo.host, serverSet);
		return super.perform(deployTaskInfo);
	}

	public static void updateForUsat(IPTablesUpdateTask iptTask, Host host, ServerSet serverSet) throws IOException {
		iptTask.allowTcpOutAny();
		iptTask.allowUdpOutAny();
		List<Server> servers;
		List<String> ipsentryHosts = new ArrayList<String>();
		ipsentryHosts.add("10.0.0.31");
		ipsentryHosts.add("192.168.69.41");
		ipsentryHosts.add("10.20.0.41");

		List<String> mgmtHosts = new ArrayList<String>();
		mgmtHosts.addAll(ipsentryHosts);
		mgmtHosts.add("10.0.0.0/23"); // from all local
		mgmtHosts.add("10.20.0.50/32");
		mgmtHosts.add("192.168.69.70");

		List<Server> monitorServers = serverSet.getServers("MON");
		if(monitorServers != null)
			for(Server ms : monitorServers)
				mgmtHosts.add(ms.ip);

		for(String serverType : host.getServerTypes()) {
			Server server = serverSet.getServer(host, serverType);
			if(server != null) {
				Set<App> apps = serverSet.getApps(serverType);
				if(apps != null) {
					// to ldap server(s) for authentication for jmx, etc
					if("DEV".equalsIgnoreCase(host.getServerEnv())) {
						iptTask.allowTcpOut("devldp01.usatech.com", 636);
						iptTask.allowTcpOut("devldp02.usatech.com", 636);
					} else if("INT".equalsIgnoreCase(host.getServerEnv())) {
						iptTask.allowTcpOut("devldp01.usatech.com", 636);
						iptTask.allowTcpOut("devldp02.usatech.com", 636);
					} else if("ECC".equalsIgnoreCase(host.getServerEnv())) {
						iptTask.allowTcpOut("devldp01.usatech.com", 636);
						iptTask.allowTcpOut("devldp02.usatech.com", 636);
					} else if("USA".equalsIgnoreCase(host.getServerEnv())) {
						//iptTask.allowTcpOut("usadc022.usatech.com", 636);
						iptTask.allowTcpOut("usadc001.usatech.com", 636);
						iptTask.allowTcpOut("usadc002.usatech.com", 636);
						iptTask.allowTcpOut("usaldp21.trooper.usatech.com", 636);
						iptTask.allowTcpOut("usaldp22.trooper.usatech.com", 636);
					}

					// for monit & for jstatd
					for(String mgmtHost : mgmtHosts) {
						iptTask.allowTcpIn(mgmtHost, 2812);
						iptTask.allowTcpIn(mgmtHost, 7001);
						iptTask.allowTcpIn(mgmtHost, 7002);
						iptTask.allowTcpIn(mgmtHost, 7003);
					}
					for(App app : apps) {
						// to/from other apps
						allowProduceQueueLayer(server, iptTask, serverSet, app, app.getProduceQueueLayerTo());
						allowConsumeQueueLayer(iptTask, serverSet, app.getConsumeQueueLayerFrom());

						for(String instance : app.filterInstances(server.instances)) {
							if("postgres".equals(app.getName()))
								continue;
							Integer ordinal = (instance == null ? null : Integer.parseInt(instance));
							if(!"httpd".equals(app.getName())) {
								// management from ipsentry
								for(String ipsentryHost : ipsentryHosts)
									iptTask.allowTcpIn(ipsentryHost, 7000 + app.getPortOffset() + (ordinal == null ? 700 : ordinal * 100));

								// jmx from terminal server / developer machines
								for(String mgmtHost : mgmtHosts) {
									iptTask.allowTcpIn(mgmtHost, 7001 + app.getPortOffset() + (ordinal == null ? 700 : ordinal * 100));
									iptTask.allowTcpIn(mgmtHost, 7002 + app.getPortOffset() + (ordinal == null ? 700 : ordinal * 100));
								}
							}
							if("netlayer".equalsIgnoreCase(app.getName())) {
								// from devices
								iptTask.allowTcpIn(null, 14007 + (ordinal == null ? 100 : ordinal * 100));
								iptTask.allowTcpIn(null, 14008 + (ordinal == null ? 100 : ordinal * 100));
								iptTask.allowTcpIn(null, 14009 + (ordinal == null ? 100 : ordinal * 100));
								iptTask.allowTcpIn(null, 14043 + (ordinal == null ? 100 : ordinal * 100));
								if(ordinal == null || ordinal == 1)
									iptTask.allowTcpIn(null, 443);
								iptTask.allowTcpIn(null, (ordinal == null || ordinal == 1 ? 3268 : 14068 + ordinal * 100));
								iptTask.allowTcpIn(null, (ordinal == null || ordinal == 1 ? 9443 : 19043 + ordinal * 100));
							} else if("applayer".equalsIgnoreCase(app.getName())) {
								// to AppLayerDB
								for(String mgmtHost : mgmtHosts)
									iptTask.allowTcpIn(mgmtHost, (ordinal == null ? 1527 : 15027 + ordinal * 100));
							} else if("dms".equalsIgnoreCase(app.getName())) {
								int port = 7080 + app.getPortOffset() + (ordinal == null ? 700 : ordinal * 100);
								for(Server remote : serverSet.getServers("NET"))
									iptTask.allowTcpIn(remote.ip, port); // for web requests
								for(String mgmtHost : mgmtHosts)
									iptTask.allowTcpIn(mgmtHost, port);
							} else if("usalive".equalsIgnoreCase(app.getName())||"prepaid".equalsIgnoreCase(app.getName())) {
								int port = 7080 + app.getPortOffset() + (ordinal == null ? 700 : ordinal * 100);
								for(Server remote : serverSet.getServers("WEB"))
									iptTask.allowTcpIn(remote.ip, port); // for web requests
								for(String mgmtHost : mgmtHosts)
									iptTask.allowTcpIn(mgmtHost, port);
							} else if("esuds".equalsIgnoreCase(app.getName())) {
								int port = 7080 + app.getPortOffset() + (ordinal == null ? 700 : ordinal * 100);
								for(Server remote : serverSet.getServers("WEB"))
									iptTask.allowTcpIn(remote.ip, port); // for web requests
								for(String mgmtHost : mgmtHosts)
									iptTask.allowTcpIn(mgmtHost, port);
							} else if("keymgr".equalsIgnoreCase(app.getName())) {
								int port = 7080 + app.getPortOffset() + (ordinal == null ? 700 : ordinal * 100);
								int sslport = 7043 + app.getPortOffset() + (ordinal == null ? 700 : ordinal * 100);
								for(String mgmtHost : mgmtHosts) {
									iptTask.allowTcpIn(mgmtHost, port);
									iptTask.allowTcpIn(mgmtHost, sslport);
									if(ordinal == null || ordinal == 1) {
										iptTask.allowTcpIn(mgmtHost, 80);
										iptTask.allowTcpIn(mgmtHost, 443);
									}
								}								
								if ("INT".equalsIgnoreCase(host.getServerEnv())) {
									// ViVOtech's source IPs for their Key Manager integration testing
									iptTask.allowTcpIn("12.208.161.0/24", 443);
									iptTask.allowTcpIn("12.208.161.0/24", 8643);
								}
							} else if("httpd".equals(app.getName())) {
								// for httpd
								iptTask.allowTcpIn("10.0.0.0/16", 80);
								iptTask.allowTcpIn("10.20.0.0/24", 80);
								iptTask.allowTcpIn("192.168.0.0/16", 80);
								iptTask.allowTcpIn("10.0.0.0/16", 8443);
								iptTask.allowTcpIn("10.20.0.0/24", 8443);
								iptTask.allowTcpIn("192.168.0.0/16", 8443);
								if(host.isServerType("NET")||host.isServerType("WEB")) {
									Set<App> subAppSet = serverSet.getSubApps(app);
									if(subAppSet!=null){
										for(App subApp:subAppSet){
											if(!StringUtils.isBlank(subApp.getServerType()))
												for(Server remote : serverSet.getServers(subApp.getServerType())) {
													iptTask.allowTcpOut(remote.ip, 7080 + subApp.portOffset + (ordinal == null ? 700 : ordinal * 100));
												}
										}
									}
								}
								if(host.isServerType("WEB") && ("USA".equalsIgnoreCase(host.getServerEnv()) || "ECC".equalsIgnoreCase(host.getServerEnv()))) {
									iptTask.allowTcpIn(null, 80);
									iptTask.allowTcpIn(null, 443);
								}
							}
						}
					}
				}
			}
			if(host.isServerType("APR") || host.isServerType("NET") || host.isServerType("APP") || host.isServerType("WEB") || host.isServerType("KLS")) {
				// to mq dbs
				servers = serverSet.getServers(CollectionUtils.asSet("MST", "MSR"));
				if(servers != null) {
					for(Server remote : servers) {
						if("INT".equalsIgnoreCase(host.getServerEnv()) || "ECC".equalsIgnoreCase(host.getServerEnv()))
							iptTask.allowTcpOut(remote.ip, 5431);
						iptTask.allowTcpOut(remote.ip, 5432);
					}
				}
			}
			if(host.isServerType("APR")) {
				// to oracle and kls and other resources
				if("DEV".equalsIgnoreCase(host.getServerEnv())) {
					iptTask.allowTcpOut("devpgs10", 5432); // postgresql
					iptTask.allowTcpOut("devpgs110", 15432);
					iptTask.allowTcpOut("devdb011", 1521); // oracle
					iptTask.allowTcpOut("206.253.184.212", 7085); // fhms
					iptTask.allowTcpOut("206.253.184.211", 7085); // fhms
					iptTask.allowTcpOut("198.203.191.84", 8100); // elavon
					iptTask.allowTcpOut("198.203.191.112", 8100); // elavon
					iptTask.allowTcpOut("206.253.180.250", 12000); // tandem
					iptTask.allowTcpOut("206.253.180.47", 22); // cpt files
				} else if("INT".equalsIgnoreCase(host.getServerEnv())) {
					iptTask.allowTcpOut("intpgs10", 5432); // postgresql
					iptTask.allowTcpOut("intpgs110", 15432); // postgresql
					iptTask.allowTcpOut("intdb011", 1521); // oracle
					iptTask.allowTcpOut("206.253.184.212", 7085); // fhms
					iptTask.allowTcpOut("206.253.184.211", 7085); // fhms
					iptTask.allowTcpOut("198.203.191.84", 8100); // elavon
					iptTask.allowTcpOut("198.203.191.112", 8100); // elavon
					iptTask.allowTcpOut("206.253.180.250", 12000); // tandem
					iptTask.allowTcpOut("206.253.180.47", 22); // cpt files
				} else if("ECC".equalsIgnoreCase(host.getServerEnv())) {
					iptTask.allowTcpOut("eccpgs10", 5432); // postgresql
					iptTask.allowTcpOut("eccpgs110", 15432); // postgresql
					iptTask.allowTcpOutWithRelated("eccscan01", 1525, new String[] { "eccdb011-vip", "eccdb012-vip" }, 1521); // oracle
					iptTask.allowTcpOut("usastt01", 1521); // oracle // TODO: this needs to change to the new servers
					iptTask.allowTcpOut("206.253.184.212", 7085); // fhms
					iptTask.allowTcpOut("206.253.184.211", 7085); // fhms
					iptTask.allowTcpOut("198.203.191.84", 8100); // elavon
					iptTask.allowTcpOut("198.203.191.112", 8100); // elavon
					iptTask.allowTcpOut("206.253.180.250", 12000); // tandem
					iptTask.allowTcpOut("206.253.180.47", 22); // cpt files
				} else if("USA".equalsIgnoreCase(host.getServerEnv())) {
					iptTask.allowTcpOut("usapgs30.trooper.usatech.com", 5432); // postgresql
					iptTask.allowTcpOut("usapgs130.trooper.usatech.com", 15432); // postgresql
					/*
					iptTask.allowTcpOutWithRelated("usadbp21-vip.trooper.usatech.com", 1510, 1521); // oracle
					iptTask.allowTcpOutWithRelated("usadbp22-vip.trooper.usatech.com", 1510, 1521); // oracle
					iptTask.allowTcpOutWithRelated("usadbp23-vip.trooper.usatech.com", 1510, 1521); // oracle
					iptTask.allowTcpOutWithRelated("usardb23-vip.trooper.usatech.com", 1530, 1521); // oracle
					iptTask.allowTcpOutWithRelated("usardb24-vip.trooper.usatech.com", 1530, 1521); // oracle
					iptTask.allowTcpOutWithRelated("usabkp21-vip.trooper.usatech.com", 1530, 1521); // oracle
					*/
					iptTask.allowTcpOutWithRelated("oradbscan01.trooper.usatech.com", 1535, 
							new String[] { "usadb031-vip.trooper.usatech.com", "usadb032-vip.trooper.usatech.com", "usadb033-vip.trooper.usatech.com", "usadb034-vip.trooper.usatech.com" }, 1521); // oracle
					/*
					iptTask.allowTcpOut("usakls31.trooper.usatech.com", 443); // kls
					iptTask.allowTcpOut("usakls32.trooper.usatech.com", 443); // kls
					iptTask.allowTcpOut("usakls33.trooper.usatech.com", 443); // kls
					iptTask.allowTcpOut("usakls34.trooper.usatech.com", 443); // kls
					*/
					iptTask.allowTcpOut("206.253.184.210", 7085); // fhms
					iptTask.allowTcpOut("206.253.180.210", 7085); // fhms
					iptTask.allowTcpOut("198.203.191.104", 8100); // elavon
					iptTask.allowTcpOut("198.203.192.217", 8100); // elavon
					iptTask.allowTcpOut("206.253.184.20", 15100); // tandem
					iptTask.allowTcpOut("206.253.180.20", 15100); // tandem
					iptTask.allowTcpOut("206.253.184.37", 22); // cpt files
				}
			}
			if(host.isServerType("NET")) {
				// to external resources
				if("DEV".equalsIgnoreCase(host.getServerEnv())) {
					iptTask.allowTcpOut("netconnectkavar1.paymentech.net", 443); // paymentech // 65.124.118.187
					iptTask.allowTcpOut("208.72.254.252", 443); // firstdata // // stg.dw.us.fdcnet.biz
					iptTask.allowTcpOutWithPassive("10.0.0.110", 21); // elavon ftp
					iptTask.allowTcpOut("posgateway.cert.secureexchange.net", 443); // heartland // 12.130.236.230
					iptTask.allowTcpIn("10.0.0.0/23", 990); // ftps server to imitate Elavon
					iptTask.allowTcpOut("aibapp19.aprivaeng.com", 11098); // Apriva Test
				} else if("INT".equalsIgnoreCase(host.getServerEnv())) {
					iptTask.allowTcpOut("netconnectkavar1.paymentech.net", 443); // paymentech
					iptTask.allowTcpOut("208.72.254.252", 443); // firstdata // // stg.dw.us.fdcnet.biz
					iptTask.allowTcpOutWithPassive("10.0.0.110", 21); // elavon ftp
					iptTask.allowTcpOut("posgateway.cert.secureexchange.net", 443); // heartland
					iptTask.allowTcpIn("10.0.0.0/23", 990); // ftps server to imitate Elavon
					iptTask.allowTcpOut("aibapp19.aprivaeng.com", 11098); // Apriva Test
				} else if("ECC".equalsIgnoreCase(host.getServerEnv())) {
					iptTask.allowTcpOut("netconnectkavar1.paymentech.net", 443); // paymentech
					iptTask.allowTcpOut("208.72.254.252", 443); // firstdata // // stg.dw.us.fdcnet.biz
					iptTask.allowTcpOutWithPassive("198.203.191.201", 990); // elavon ftp
					iptTask.allowTcpOut("posgateway.cert.secureexchange.net", 443); // heartland
					iptTask.allowTcpOut("aibapp19.aprivaeng.com", 11098); // Apriva Test
					iptTask.allowTcpOut("api.apriva.com", 11098); // Apriva Prod
				} else if("USA".equalsIgnoreCase(host.getServerEnv())) {
					iptTask.allowTcpOut("netconnectka1.paymentech.net", 443); // paymentech
					// iptTask.allowTcpOut(getServerIP("stg.dw.us.fdcnet.biz"), 443); // firstdata
					iptTask.allowTcpOutWithPassive("sftp.novainfo.net", 990); // elavon ftp
					iptTask.allowTcpOut("posgateway.secureexchange.net", 443); // heartland
					iptTask.allowTcpOut("api.apriva.com", 11098); // Apriva Prod
				}
				// to database configured external resources
				iptTask.allowTcpOut(null, 443); // POSGateway-AquaFill, Aramark (potentially)
				iptTask.allowTcpOut(null, 80); // Aramark
				iptTask.allowTcpOut(null, 9003); // Blackboard
				iptTask.allowTcpOut(null, 9100); // POSGateway
			}
			if(host.isServerType("MST") || host.isServerType("MSR")) {
				int[] ports;
				if("INT".equalsIgnoreCase(host.getServerEnv()) || "ECC".equalsIgnoreCase(host.getServerEnv()))
					ports = new int[] { 5431, 5432 };
				else
					ports = new int[] { 5432 };

				for(int port : ports) {
					// from mgmt machines
					for(String mgmtHost : mgmtHosts)
						iptTask.allowTcpIn(mgmtHost, port);

					// from the apps
					if(host.isServerType("MST")) {
						servers = serverSet.getServers(CollectionUtils.asSet("APR", "NET", "KLS"));
						if(servers != null) {
							for(Server remote : servers)
								iptTask.allowTcpIn(remote.ip, port);
						}
					}

					if(host.isServerType("MSR")) {
						servers = serverSet.getServers(CollectionUtils.asSet("APP", "WEB", "APR"));
						if(servers != null) {
							for(Server remote : servers)
								iptTask.allowTcpIn(remote.ip, port);
						}
					}
				}
				// to postgres yum
				iptTask.allowTcpOut("yum.postgresql.org", 80);
			}
			if(host.isServerType("APP")) {
				// to oracle and other resources
				if("DEV".equalsIgnoreCase(host.getServerEnv())) {
					iptTask.allowTcpOut("devpgs10", 5432); // postgresql
					iptTask.allowTcpOut("devpgs110", 15432); // postgresql
					iptTask.allowTcpOut("devdb011", 1521); // oracle
					iptTask.allowTcpOut("10.0.0.67", 9443); // eport-connect
				} else if("INT".equalsIgnoreCase(host.getServerEnv())) {
					iptTask.allowTcpOut("intpgs10", 5432); // postgresql
					iptTask.allowTcpOut("intpgs110", 15432); // postgresql
					iptTask.allowTcpOut("intdb011", 1521); // oracle
					iptTask.allowTcpOut("10.0.0.68", 9443); // eport-connect
				} else if("ECC".equalsIgnoreCase(host.getServerEnv())) {
					iptTask.allowTcpOut("eccpgs10", 5432); // postgresql
					iptTask.allowTcpOut("eccpgs110", 15432); // postgresql
					iptTask.allowTcpOutWithRelated("eccscan01", 1525, new String[] { "eccdb011-vip", "eccdb012-vip" }, 1521); // oracle
					iptTask.allowTcpOut("192.168.4.63", 9443); // eport-connect
				} else if("USA".equalsIgnoreCase(host.getServerEnv())) {
					iptTask.allowTcpOut("usapgs30.trooper.usatech.com", 5432); // postgresql
					iptTask.allowTcpOut("usapgs130.trooper.usatech.com", 15432); // postgresql
					/*
					iptTask.allowTcpOutWithRelated("usardb23-vip.trooper.usatech.com", 1530, 1521); // oracle
					iptTask.allowTcpOutWithRelated("usardb24-vip.trooper.usatech.com", 1530, 1521); // oracle
					iptTask.allowTcpOutWithRelated("usabkp21-vip.trooper.usatech.com", 1530, 1521); // oracle
					*/
					iptTask.allowTcpOutWithRelated("oradbscan01.trooper.usatech.com", 1535, 
							new String[] { "usadb031-vip.trooper.usatech.com", "usadb032-vip.trooper.usatech.com", "usadb033-vip.trooper.usatech.com", "usadb034-vip.trooper.usatech.com" }, 1521); // oracle				
					iptTask.allowTcpOut("192.168.79.163", 9443); // eport-connect
				}
			}
			
			if(host.isServerType("KLS")) {
				Host standby = PgDeployMap.standbyMap.get(host.getSimpleName());
				if(standby != null)
					iptTask.allowTcpIn(standby.getFullName(), 5432);
				Host primary = PgDeployMap.primaryMap.get(host.getSimpleName());
				if(primary != null)
					iptTask.allowTcpOut(primary.getFullName(), 15432);
				int[] ports = new int[] { 5432, 15432 };
				for(int port : ports) {
					// from ipsentry
					for(String ipsentryHost : ipsentryHosts)
						iptTask.allowTcpIn(ipsentryHost, port);
				}
			}

			if(host.isServerType("PGS")) {
				servers = serverSet.getServers(CollectionUtils.asSet("APR", "APP"));
				if(servers != null) {
					for(Server remote : servers){
						iptTask.allowTcpIn(remote.ip, 5432);
						iptTask.allowTcpIn(remote.ip, 15432);
				}
				}
				int[] ports = new int[] { 5432, 15432};
				for(int port : ports) {
					for(String mgmtHost : mgmtHosts)
						iptTask.allowTcpIn(mgmtHost, port);
					for(String ipsentryHost : ipsentryHosts)
						iptTask.allowTcpIn(ipsentryHost, port);
				}
				iptTask.allowTcpIn("10.0.0.0/23", 5432); // from all local
			}
		}
	}

	public static void allowConsumeQueueLayer(IPTablesUpdateTask iptTask, ServerSet serverSet, Set<App> remoteApps) throws IOException {
		if(remoteApps == null || remoteApps.isEmpty())
			return;
		Set<String> serverTypes = new HashSet<String>();
		for(App remoteApp : remoteApps)
			serverTypes.add(remoteApp.getServerType());
		List<Server> servers = serverSet.getServers(serverTypes);
		if(servers != null) {
			for(Server remote : servers) {
				for(App remoteApp : remoteApps) {
					for(String instance : remoteApp.filterInstances(remote.instances)) {
						Integer ord = (instance == null ? null : Integer.parseInt(instance));
						if(remote.serverType.equalsIgnoreCase(remoteApp.getServerType()))
							iptTask.allowTcpOut(remote.ip, 7007 + remoteApp.getPortOffset() + (ord == null ? 700 : ord * 100));
					}
				}
			}
		}
	}

	public static void allowProduceQueueLayer(Server currentServer, IPTablesUpdateTask iptTask, ServerSet serverSet, App app, Set<String> remoteServerTypes) throws IOException {
		if(remoteServerTypes == null || remoteServerTypes.isEmpty())
			return;
		List<Server> servers = serverSet.getServers(remoteServerTypes);
		if(servers != null) {
			for(Server remote : servers) {
				for(String instance : app.filterInstances(currentServer.instances)) {
					Integer ord = (instance == null ? null : Integer.parseInt(instance));
					iptTask.allowTcpIn(remote.ip, 7007 + app.getPortOffset() + (ord == null ? 700 : ord * 100));
				}
			}
		}
	}
}
