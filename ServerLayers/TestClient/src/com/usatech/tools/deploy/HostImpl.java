package com.usatech.tools.deploy;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

public class HostImpl implements Host {
	protected final String serverEnv;
	protected final Set<String> serverTypes;
	protected final Set<String> unmodServerTypes;
	protected final String fullName;
	protected final String simpleName;
	protected boolean standby;

	public HostImpl(String serverEnv, String fullName) {
		this.serverEnv = serverEnv;
		this.serverTypes = new LinkedHashSet<String>();
		this.unmodServerTypes = Collections.unmodifiableSet(serverTypes);
		this.fullName = fullName;
		this.simpleName = getSimpleName(fullName);
	}

	public HostImpl(String serverEnv, String serverType, String fullName) {
		this(serverEnv, fullName);
		addServerType(serverType);
	}
	public HostImpl(String serverEnv, String serverType, String fullName, boolean standby) {
		this(serverEnv, serverType, fullName);
		this.standby=standby;
	}
	public HostImpl(String serverEnv, String[] serverTypes, String fullName) {
		this(serverEnv, fullName);
		for(String serverType : serverTypes)
			addServerType(serverType);
	}

	public static String getSimpleName(String fullName) {
		int pos = fullName.indexOf('.');
		if(pos > 0)
			return fullName.substring(0, pos);
		return fullName;
	}
	public String getServerEnv() {
		return serverEnv;
	}
	public boolean isServerType(String serverType) {
		return serverTypes.contains(serverType.toUpperCase());
	}
	public String getFullName() {
		return fullName;
	}
	public String getSimpleName() {
		return simpleName;
	}
	@Override
	public String toString() {
		return getSimpleName();
	}
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Host))
			return false;
		return getFullName().equalsIgnoreCase(((Host)obj).getFullName());
	}
	@Override
	public int hashCode() {
		return getFullName().hashCode();
	}
	public Set<String> getServerTypes() {
		return unmodServerTypes;
	}

	public boolean addServerType(String serverType) {
		return serverTypes.add(serverType.toUpperCase());
	}
	
	public void setStandby(boolean standby) {
		this.standby = standby;
	}

	public boolean isStandby(){
		return standby;
	}
}
