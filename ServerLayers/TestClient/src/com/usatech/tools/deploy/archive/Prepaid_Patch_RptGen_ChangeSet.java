package com.usatech.tools.deploy.archive;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import simple.io.ConfigSource;
import simple.servlet.AbstractInputForm;
import simple.servlet.Action;
import simple.servlet.ActionNotFoundException;
import simple.servlet.CalendarHelper;
import simple.servlet.Catch;
import simple.servlet.ContainerServletUser;
import simple.servlet.Dispatcher;
import simple.servlet.InputFile;
import simple.servlet.InputForm;
import simple.servlet.Interceptor;
import simple.servlet.InvalidSessionTokenException;
import simple.servlet.JspRequestDispatcherFactory;
import simple.servlet.NoSessionTokenException;
import simple.servlet.NotAuthorizedException;
import simple.servlet.NotLoggedOnException;
import simple.servlet.RequestDispatcherFactory;
import simple.servlet.RequestInfo;
import simple.servlet.RequestInfoUtils;
import simple.servlet.RequestUtils;
import simple.servlet.ServletUser;
import simple.servlet.SimpleAction;
import simple.servlet.SimpleInputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.Step;
import simple.servlet.StepConfigException;
import simple.servlet.XsrfProtectionLevel;
import simple.util.CollectionUtils;

import com.usatech.ccs.CampaignBlastTask;
import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;

public class Prepaid_Patch_RptGen_ChangeSet extends AbstractLinuxPatchChangeSet {
	public Prepaid_Patch_RptGen_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ReportGenerator/src", CampaignBlastTask.class, "REL_prepaid_1_1_0");
		registerResource("ReportGenerator/src/rg-campaign-data-layer.xml", "classes", "REL_prepaid_1_1_0");
		registerResource("ReportGenerator/src/com/usatech/campaign/promo-template.jsp", "classes/com/usatech/campaign", "REL_prepaid_1_1_0");

		registerSource("Simple1.5/src", Interceptor.class, "REL_prepaid_1_1_0");
		registerSource("Simple1.5/src", RequestDispatcherFactory.class, "REL_prepaid_1_1_0");
		registerSource("Simple1.5/src", JspRequestDispatcherFactory.class, "REL_prepaid_1_1_0");
		registerSource("Simple1.5/src", RequestUtils.class, "REL_prepaid_1_1_0");
		registerSource("Simple1.5/src", InputForm.class, "REL_prepaid_1_1_0");
		registerSource("Simple1.5/src", CalendarHelper.class, "REL_prepaid_1_1_0");
		registerSource("Simple1.5/src", InputFile.class, "REL_prepaid_1_1_0");
		registerSource("Simple1.5/src", RequestInfo.class, "REL_prepaid_1_1_0");
		registerSource("Simple1.5/src", ServletUser.class, "REL_prepaid_1_1_0");
		registerSource("Simple1.5/src", XsrfProtectionLevel.class, "REL_prepaid_1_1_0");
		registerSource("Simple1.5/src", SimpleServlet.class, "REL_prepaid_1_1_0");

		registerSource("Simple1.5/src", AbstractInputForm.class, "REL_prepaid_1_1_0");
		registerSource("Simple1.5/src", Action.class, "REL_prepaid_1_1_0");
		registerSource("Simple1.5/src", ActionNotFoundException.class, "REL_prepaid_1_1_0");
		registerSource("Simple1.5/src", Catch.class, "REL_prepaid_1_1_0");
		registerSource("Simple1.5/src", ContainerServletUser.class, "REL_prepaid_1_1_0");
		registerSource("Simple1.5/src", Dispatcher.class, "REL_prepaid_1_1_0");
		registerSource("Simple1.5/src", InvalidSessionTokenException.class, "REL_prepaid_1_1_0");
		registerSource("Simple1.5/src", NoSessionTokenException.class, "REL_prepaid_1_1_0");
		registerSource("Simple1.5/src", NotAuthorizedException.class, "REL_prepaid_1_1_0");
		registerSource("Simple1.5/src", NotLoggedOnException.class, "REL_prepaid_1_1_0");
		registerSource("Simple1.5/src", RequestInfoUtils.class, "REL_prepaid_1_1_0");
		registerSource("Simple1.5/src", SimpleAction.class, "REL_prepaid_1_1_0");
		registerSource("Simple1.5/src", SimpleInputForm.class, "REL_prepaid_1_1_0");
		registerSource("Simple1.5/src", Step.class, "REL_prepaid_1_1_0");
		registerSource("Simple1.5/src", StepConfigException.class, "REL_prepaid_1_1_0");

		registerSource("Simple1.5/src", CollectionUtils.class, "REL_prepaid_1_1_0");
		registerSource("Simple1.5/src", ConfigSource.class, "REL_prepaid_1_1_0");

		registerLib("ThirdPartyJavaLibraries/lib/javax.el-2.2.0.v201108011116.jar", "HEAD");
		registerLib("ThirdPartyJavaLibraries/lib/org.apache.jasper.glassfish-2.2.2.v201112011158.jar", "HEAD");
	}

	@Override
	protected void addTasks(Host host, App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		String appDir = "/opt/USAT/" + app.getName() + (instance == null ? "" : instance);
		tasks.add(new ExecuteTask("sudo ln -s /opt/USAT/prepaid/classes/version.txt " + appDir + "/classes/prepaid-version.txt", "sudo chmod 0755 /opt/USAT/prepaid"));
		super.addTasks(host, app, ordinal, instance, instanceNum, instanceCount, tasks, commands, cache);
	}

	protected void registerApps() {
		registerApp(USATRegistry.RPTGEN_LAYER);
	}

	@Override
	public String getName() {
		return "Prepaid - RptGen - Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
