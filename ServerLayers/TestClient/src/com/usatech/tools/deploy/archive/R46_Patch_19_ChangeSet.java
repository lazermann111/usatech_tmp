package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R46_Patch_19_ChangeSet extends MultiLinuxPatchChangeSet {
	public R46_Patch_19_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/AuthorityLayer/src/InsideAuthorityLayerService.properties", "classes", "REL_authoritylayer_1_32_19", true, USATRegistry.INAUTH_LAYER);
	}

	@Override
	public String getName() {
		return "R46 Patch 19";
	}
}
