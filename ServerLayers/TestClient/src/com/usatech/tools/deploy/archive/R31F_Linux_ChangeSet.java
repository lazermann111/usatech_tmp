package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.APP_LAYER;
import static com.usatech.tools.deploy.USATRegistry.LOADER;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.AppSettingUpdateFileTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;

public class R31F_Linux_ChangeSet extends AbstractLinuxChangeSet {
	public R31F_Linux_ChangeSet() throws UnknownHostException {
		super();
	}

	@Override
	protected void registerApps() {
		registerApp(LOADER, "1.17.0");
		registerApp(APP_LAYER, "1.17.0");
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
	}
	
	@Override
	protected void addTasks(Host host, final App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(!"postgres".equalsIgnoreCase(app.getName())) {
			addPrepareAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache);
			tasks.add(new AppSettingUpdateFileTask(app, ordinal, instanceNum, instanceCount, cache, registry.getServerSet(host.getServerEnv()), false));
			addInstallAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache, commands);
		}
	}

	protected void addBeforeRestartCommands(Host host, App app, Integer ordinal, int instanceNum, List<String> commands, List<DeployTask> tasks) {
		StringBuilder sb = new StringBuilder();
		if(USATRegistry.APP_LAYER.equals(app)) {
			sb.append("/bin/rm -r /opt/USAT/").append(app.getName());
			if(ordinal != null)
				sb.append(ordinal);
			sb.append("/db/AppLayerDBOld");
			commands.add(sb.toString());
			sb.setLength(0);
			sb.append("/bin/mv /opt/USAT/").append(app.getName());
			if(ordinal != null)
				sb.append(ordinal);
			sb.append("/db/AppLayerDB /opt/USAT/").append(app.getName());
			if(ordinal != null)
				sb.append(ordinal);
			sb.append("/db/AppLayerDBOld");
			commands.add(sb.toString());
		}
	}

	@Override
	public String getName() {
		return "Edge Server - R31F Patch - Apps";
	}
}
