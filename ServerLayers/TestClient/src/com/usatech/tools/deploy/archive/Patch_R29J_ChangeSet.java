package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.APP_LAYER;
import static com.usatech.tools.deploy.USATRegistry.INAUTH_LAYER;
import static com.usatech.tools.deploy.USATRegistry.NET_LAYER;
import static com.usatech.tools.deploy.USATRegistry.OUTAUTH_LAYER;
import static com.usatech.tools.deploy.USATRegistry.POSM_LAYER;

import java.io.File;
import java.net.UnknownHostException;

import simple.io.logging.JDK14toSimpleHandler;

import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;

public class Patch_R29J_ChangeSet extends AbstractLinuxPatchChangeSet {
	protected String baseDir;

	public Patch_R29J_ChangeSet() throws UnknownHostException {
		super();
		baseDir = System.getProperty("rootSourceDir");
		File simpleBinDir = new File(baseDir + "/Simple1.5/bin");
		registerClass(simpleBinDir, JDK14toSimpleHandler.class);
		//File posmlayerBinDir = new File(baseDir + "/POSMLayer/bin");
		//registerClass(posmlayerBinDir, POSMUtils.class);
		//registerResource(posmlayerBinDir, "posm-data-layer.xml");
		// File layersCommonBinDir = new File(baseDir + "/LayersCommon/bin");
		// registerClass(layersCommonBinDir, AppLayerUtils.class);
		// File layersCommonBinDir = new File(baseDir + "/LayersCommon/bin");
		// registerClass(layersCommonBinDir, LoadDataAttrEnum.class);
	}
	protected void registerApps() {
		registerApp(APP_LAYER);
		registerApp(INAUTH_LAYER);
		registerApp(POSM_LAYER);
		registerApp(NET_LAYER);
		registerApp(OUTAUTH_LAYER);
	}
	@Override
	public String getName() {
		return "Edge Server - R29J Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
