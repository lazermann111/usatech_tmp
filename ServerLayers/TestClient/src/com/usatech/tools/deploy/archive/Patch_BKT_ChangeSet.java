package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.authoritylayer.InternalAuthorityTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class Patch_BKT_ChangeSet extends MultiLinuxPatchChangeSet {

	public Patch_BKT_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/AuthorityLayer/src/authority-data-layer.xml", "classes", "HEAD", false, USATRegistry.INAUTH_LAYER);
		registerSource("ServerLayers/AuthorityLayer/src", InternalAuthorityTask.class, "HEAD", USATRegistry.INAUTH_LAYER);
	}

	@Override
	public String getName() {
		return "BK Test";
	}

}
