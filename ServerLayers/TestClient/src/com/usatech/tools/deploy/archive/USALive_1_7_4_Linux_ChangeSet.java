package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.HTTPD_WEB;
import static com.usatech.tools.deploy.USATRegistry.RPTGEN_LAYER;
import static com.usatech.tools.deploy.USATRegistry.RPTREQ_LAYER;
import static com.usatech.tools.deploy.USATRegistry.TRANSPORT_LAYER;
import static com.usatech.tools.deploy.USATRegistry.USALIVE_APP;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.AppSettingUpdateFileTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.UsatSettingUpdateFileTask;

public class USALive_1_7_4_Linux_ChangeSet extends AbstractLinuxChangeSet {
	public USALive_1_7_4_Linux_ChangeSet() throws UnknownHostException {
		super();
	}

	@Override
	protected void registerApps() {
		registerApp(RPTGEN_LAYER, "3.6.4");
		registerApp(TRANSPORT_LAYER, "3.6.4");
		registerApp(RPTREQ_LAYER, "3.6.4");
		// registerApp(POSTGRES_MSR);
		registerApp(USALIVE_APP, "1.7.4");
		// registerApp(ESUDS_APP, "1.2.2");
		// USATECH_WEBSITE_APP.setVersion("1.2.0");
		// ENERGYMISERS_WEBSITE_APP.setVersion("1.1.0");
		registerApp(HTTPD_WEB, "2.2.22");
		// registerApp(HTTPD_NET, "2.2.22");
	}
		
	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("WEB") || host.isServerType("APP");
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareHostTasks(host, tasks, cache);
		if(host.isServerType("WEB") || host.isServerType("APP")) {
			tasks.add(new UsatSettingUpdateFileTask(registry.getServerSet(host.getServerEnv())));
		}
	}
	
	@Override
	protected void addTasks(Host host, App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache);
		if(app.getInstallFileName() != null) {
			tasks.add(new AppSettingUpdateFileTask(app, ordinal, instanceNum, instanceCount, cache, registry.getServerSet(host.getServerEnv())));
		}
		addInstallAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache, commands);
	}

	@Override
	public String getName() {
		return "USALive 1.7.4 Install";
	}
}
