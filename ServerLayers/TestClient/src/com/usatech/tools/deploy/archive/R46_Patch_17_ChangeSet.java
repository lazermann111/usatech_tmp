package com.usatech.tools.deploy.archive;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.EnumMap;
import java.util.List;
import java.util.Set;

import com.usatech.dms.action.DeviceActions;
import com.usatech.iso8583.interchange.tandem.TandemMsg;
import com.usatech.posm.schedule.FanfUploadJob;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;

public class R46_Patch_17_ChangeSet extends MultiLinuxPatchChangeSet {
	public R46_Patch_17_ChangeSet() throws UnknownHostException {
		super();
		registerSource("middleware/ISO8583/src", TandemMsg.class, "REL_authoritylayer_1_32_16", USATRegistry.INAUTH_LAYER);
		registerSource("DMS/src", DeviceActions.class, "REL_DMS_1_8_17", USATRegistry.DMS_APP);
		registerResource("ServerLayers/POSMLayer/conf/posm-data-layer.xml", "classes", "REL_posmlayer_1_23_17", false, USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/POSMLayer/src/main", FanfUploadJob.class, "REL_posmlayer_1_23_17", USATRegistry.POSM_LAYER);
		registerApp(USATRegistry.LOADER);
		resources.putIfAbsent(USATRegistry.LOADER, new EnumMap<ResourceType, Set<File>>(ResourceType.class));
	}


	@Override
	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(USATRegistry.LOADER.equals(app))
			commands.add("sudo sed -i 's/<call id=\"UPLOAD_FILE\" dataSource=\"OPER\" sql=\"/<call id=\"UPLOAD_FILE\" dataSource=\"OPER\" queryTimeout=\"600\" sql=\"/g' /opt/USAT/" + app.getName() + (instance == null ? "" : instance) + "/classes/loader-data-layer.xml");
		super.addRestartAppCommands(host, app, instance, instanceNum, instanceCount, tasks, commands, cache);
	}

	@Override
	public String getName() {
		return "R46 Patch 17";
	}
}
