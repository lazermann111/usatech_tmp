package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.APP_LAYER;
import static com.usatech.tools.deploy.USATRegistry.INAUTH_LAYER;
import static com.usatech.tools.deploy.USATRegistry.KEYMGR;
import static com.usatech.tools.deploy.USATRegistry.LOADER;
import static com.usatech.tools.deploy.USATRegistry.NET_LAYER;
import static com.usatech.tools.deploy.USATRegistry.OUTAUTH_LAYER;
import static com.usatech.tools.deploy.USATRegistry.POSM_LAYER;
import static com.usatech.tools.deploy.USATRegistry.POSTGRES_MST;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.AppSettingUpdateFileTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.PostgresSetupTask;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.tools.deploy.UsatIPTablesUpdateTask;
import com.usatech.tools.deploy.UsatSettingUpdateFileTask;

public class R30_Linux_ChangeSet extends AbstractLinuxChangeSet {
	public R30_Linux_ChangeSet() throws UnknownHostException {
		super();
	}

	@Override
	protected void registerApps() {
		registerApp(LOADER, "1.16.0");
		registerApp(APP_LAYER, "1.16.0");
		registerApp(INAUTH_LAYER, "1.16.0");
		registerApp(POSM_LAYER, "1.7.0");
		registerApp(NET_LAYER, "1.16.0");
		registerApp(OUTAUTH_LAYER, "1.16.0");
		registerApp(KEYMGR, "2.0.0");
		registerApp(POSTGRES_MST, null);
		registerApp(USATRegistry.POSTGRES_MSR, null);
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(host.isServerType("NET") || host.isServerType("APR") || host.isServerType("KLS")) {
			addPrepareHostTasks(host, tasks, cache);
			UsatSettingUpdateFileTask usatSettingTask = new UsatSettingUpdateFileTask(registry.getServerSet(host.getServerEnv()));
			tasks.add(usatSettingTask);
		}
	}
	
	@Override
	protected void addTasks(Host host, final App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(host.isServerType("MST")) {
			tasks.add(new UsatIPTablesUpdateTask(registry.getServerSet(host.getServerEnv())));
			PostgresSetupTask pust = new PostgresSetupTask(app, ordinal, 5432);
			tasks.add(pust);
			for(int i = 1; i <= 4; i++)
				pust.registerUser(USATRegistry.KEYMGR.getDbUserName() + '_' + i, "use_mq", "write_file_repo");
			tasks.add(new ExecuteTask("su - postgres -c '/usr/pgsql-latest/bin/psql postgres -f /home/postgres/postgres_user_setup.sql'", "sudo /bin/rm /home/postgres/postgres_user_setup.sql"));
		} else if(host.isServerType("MSR")) {
			tasks.add(new UsatIPTablesUpdateTask(registry.getServerSet(host.getServerEnv())));
			PostgresSetupTask pust = new PostgresSetupTask(app, ordinal, 5432);
			tasks.add(pust);
			for(int i = 1; i <= 4; i++)
				pust.registerUser(USATRegistry.LOADER.getDbUserName() + '_' + i, "use_mq", "write_file_repo");
			tasks.add(new ExecuteTask("su - postgres -c '/usr/pgsql-latest/bin/psql postgres -f /home/postgres/postgres_user_setup.sql'", "sudo /bin/rm /home/postgres/postgres_user_setup.sql"));
		} else if(!"postgres".equalsIgnoreCase(app.getName())) {
			addPrepareAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache);
			tasks.add(new AppSettingUpdateFileTask(app, ordinal, instanceNum, instanceCount, cache, registry.getServerSet(host.getServerEnv()), false));
			addInstallAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache, commands);
		}
	}

	protected void addBeforeRestartCommands(Host host, App app, Integer ordinal, int instanceNum, List<String> commands, List<DeployTask> tasks) {
		/*
		if(app.getName().equals("applayer")) {
			commands.add("/bin/rm -r /opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal) + "/db/AppLayerDBOld");
			commands.add("/bin/mv /opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal) + "/db/AppLayerDB /opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal) + "/db/AppLayerDBOld");
		}
		//*/
	}

	@Override
	public String getName() {
		return "Edge Server - R30 Install - Apps";
	}
}
