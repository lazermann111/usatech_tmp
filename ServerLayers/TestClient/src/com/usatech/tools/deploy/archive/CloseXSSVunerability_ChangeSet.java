package com.usatech.tools.deploy.archive;

import java.io.File;
import java.net.UnknownHostException;

import simple.servlet.XSSFilterRequestWrapper;
import simple.text.StringUtils;

import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.USATRegistry;

public class CloseXSSVunerability_ChangeSet extends AbstractLinuxPatchChangeSet {
	protected String baseDir;

	public CloseXSSVunerability_ChangeSet() throws UnknownHostException {
		super();
		baseDir = System.getProperty("rootSourceDir");
		File simpleBinDir = new File(baseDir + "/Simple1.5/bin");
		registerClass(simpleBinDir, StringUtils.class);
		registerClass(simpleBinDir, XSSFilterRequestWrapper.class);

	}
	protected void registerApps() {
		registerApp(USATRegistry.DMS_APP);
		registerApp(USATRegistry.ESUDS_APP);
		registerApp(USATRegistry.RPTGEN_LAYER);
		registerApp(USATRegistry.USALIVE_APP);
	}

	@Override
	public String getName() {
		return "Close XSS Vunerability";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
