package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.keymanager.service.DUKPTDecryptTask;
import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.USATRegistry;

public class Patch_KeyMgr_ChangeSet extends AbstractLinuxPatchChangeSet {
	protected String baseDir;

	public Patch_KeyMgr_ChangeSet() throws UnknownHostException {
		super();
		baseDir = System.getProperty("rootSourceDir");
		// /*
		// File simpleBinDir = new File(baseDir + "/Simple1.5/bin");
		// registerClass(simpleBinDir, ReflectionUtils.class);
		// registerClass(simpleBinDir, DecompressingFormat.class);
		/*
		registerClass(simpleBinDir, DirectPeerProducer.class);
		registerClass(simpleBinDir, ByteChannelByteInput.class);
		registerClass(simpleBinDir, ByteChannelByteOutput.class);
		registerClass(simpleBinDir, DirectPoller.class);
		registerClass(simpleBinDir, CopyingJMXByteChannel.class);
		registerClass(simpleBinDir, NIOServer.class);
		// */

		// File falconBinDir = new File(baseDir + "/Falcon/src");
		// registerClass(falconBinDir, StandardExecuteEngine.class);
		// registerClass(falconBinDir, FilterItem.class);
		// registerClass(falconBinDir, DirectoryResourceResolver.class);
		// registerClass(falconBinDir, ReportRunner.class);
		//File posmlayerBinDir = new File(baseDir + "/POSMLayer/bin");
		//registerClass(posmlayerBinDir, POSMUtils.class);
		//registerResource(posmlayerBinDir, "posm-data-layer.xml");
		// File layersCommonBinDir = new File(baseDir + "/LayersCommon/bin");
		// registerClass(layersCommonBinDir, AppLayerUtils.class);
		// File layersCommonBinDir = new File(baseDir + "/LayersCommon/bin");
		// registerClass(layersCommonBinDir, LoadDataAttrEnum.class);
		// File usaliveCRWebDir = new File(baseDir + "/usalive/customerreporting_web");
		// registerResource(usaliveCRWebDir, "login.jsp");
		// File keyMgrBinDir = new File(baseDir + "/KeyManager/output");
		// registerClass(keyMgrBinDir, SSSS.class);
		// registerResource("KeyManagerService/src/KeyManagerService.properties", "classes/", "HEAD");
		// registerSource("KeyManagerService/src", MassRetrieveTask.class, "HEAD");
		// registerSource("KeyManagerService/src", StoreTask.class, "HEAD");
		// registerSource("KeyManagerService/src", DUKPTDecryptTask.class, "HEAD");
		// registerSource("KeyManagerService/src", KeyManagerEngine.class, "HEAD");
		// *
		// Update BDK Change:
		// registerSource("KeyManager/src-common", KEKStore.class, "HEAD");
		// registerSource("KeyManager/src-common", MasterKeyLoader.class, "HEAD");
		// registerResource("KeyManager/admin/genbdk.jsp", "web/admin/", "HEAD");
		// registerResource("KeyManager/admin/index.jsp", "web/admin/", "HEAD");
		// */
		// Log DUKPT decrypt non-match:
		registerSource("KeyManagerService/src", DUKPTDecryptTask.class, "REL_keymgr_2_10_1");
	}
	protected void registerApps() {
		registerApp(USATRegistry.KEYMGR);
		/*
		App app = new App("keymgr", "KLS", "ApplicationBuilds/KeyManager", "keymgr-", 900, 954, "Key Manager Service", "KeyManagerService", "keymgr") {
			public CvsPullTask getRetrieveInstallFileTask() {
				if(installFileName != null) {
					if(retrieveInstallFileTask == null)
						retrieveInstallFileTask = new CvsPullTask(appBuildDir + "/" + installFileName, "Java8");
					return retrieveInstallFileTask;
				}
				return null;
			}
		};
		registerApp(app, "2.10.0");
		*/
	}

	@Override
	public String getName() {
		return "KeyMgr Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}

	/*
	@Override
	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
	// do nothing
	}
	//*/
	/*
	@Override
	protected void addTasks(Host host, final App app, Integer ordinal, String instance, final int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addInstallAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache, commands);
	} // */
}
