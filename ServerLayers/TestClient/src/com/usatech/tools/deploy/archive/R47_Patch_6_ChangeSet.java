package com.usatech.tools.deploy.archive;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.List;

import simple.db.WatchedCallableStatement;
import simple.mq.peer.DbPoller;
import simple.mq.peer.PeerSupervisor;

import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.PropertiesUpdateFileTask;
import com.usatech.tools.deploy.RotateMessageChainEncryptionKeyTask;
import com.usatech.tools.deploy.ServerEnvHostSpecificValue;
import com.usatech.tools.deploy.USATRegistry;

public class R47_Patch_6_ChangeSet extends MultiLinuxPatchChangeSet {
	public R47_Patch_6_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/POSMLayer/conf/POSMLayerService.properties", "classes", "REL_posmlayer_1_24_6", true, USATRegistry.POSM_LAYER);
		registerResource("ServerLayers/POSMLayer/conf/posm-data-layer.xml", "classes", "REL_posmlayer_1_24_6", false, USATRegistry.POSM_LAYER);
		registerSource("Simple1.5/src", PeerSupervisor.class, "REL_keymgr_2_15_4", USATRegistry.NET_LAYER, USATRegistry.OUTAUTH_LAYER, USATRegistry.APP_LAYER, USATRegistry.LOADER, USATRegistry.POSM_LAYER, USATRegistry.INAUTH_LAYER, USATRegistry.KEYMGR, USATRegistry.TRANSPORT_LAYER, USATRegistry.USALIVE_APP, USATRegistry.RPTGEN_LAYER, USATRegistry.RPTREQ_LAYER, USATRegistry.RDW_LOADER);
		registerSource("Simple1.5/src", DbPoller.class, "REL_keymgr_2_15_4", USATRegistry.NET_LAYER, USATRegistry.OUTAUTH_LAYER, USATRegistry.APP_LAYER, USATRegistry.LOADER, USATRegistry.POSM_LAYER, USATRegistry.INAUTH_LAYER, USATRegistry.KEYMGR, USATRegistry.TRANSPORT_LAYER, USATRegistry.USALIVE_APP, USATRegistry.RPTGEN_LAYER, USATRegistry.RPTREQ_LAYER, USATRegistry.RDW_LOADER);
		registerSource("Simple1.5/src", WatchedCallableStatement.class, "REL_keymgr_2_15_4", USATRegistry.NET_LAYER, USATRegistry.OUTAUTH_LAYER, USATRegistry.APP_LAYER, USATRegistry.LOADER, USATRegistry.POSM_LAYER, USATRegistry.INAUTH_LAYER, USATRegistry.KEYMGR, USATRegistry.TRANSPORT_LAYER, USATRegistry.USALIVE_APP, USATRegistry.RPTGEN_LAYER, USATRegistry.RPTREQ_LAYER, USATRegistry.RDW_LOADER);
	}
	
	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(app.equals(USATRegistry.POSM_LAYER)) {
			PropertiesUpdateFileTask puft = new PropertiesUpdateFileTask(true, 0640, app.getUserName(), app.getUserName());
			puft.setFilePath("/opt/USAT/" + app.getName() + (instance == null ? "" : instance) + "/specific/USAT_app_settings.properties");
			puft.registerValue("USAT.riskAlert.email", new ServerEnvHostSpecificValue("SoftwareDevelopmentTeam@usatech.com", "QualityAssurance@usatech.com", "QualityAssurance@usatech.com", "RiskManagement@usatech.com"));
			tasks.add(puft);
		}
		super.addRestartAppCommands(host, app, instance, instanceNum, instanceCount, tasks, commands, cache);
	}

	@Override
	protected void addPreHostTasks(Host host, List<DeployTask> tasks, PasswordCache cache) throws IOException {
		tasks.add(new RotateMessageChainEncryptionKeyTask(new Date(System.currentTimeMillis() + 180 * 24 * 60 * 60 * 1000L), "/opt/USAT/conf/keystore.ks", "/opt/USAT/conf/truststore.ts", 0640, 0640, registry.getServerSet(host.getServerEnv()), true, new Date(System.currentTimeMillis() - 365 * 24 * 60 * 60 * 1000L)));
		super.addPreHostTasks(host, tasks, cache);
	}

	@Override
	public String getName() {
		return "R47 Patch 6";
	}
}
