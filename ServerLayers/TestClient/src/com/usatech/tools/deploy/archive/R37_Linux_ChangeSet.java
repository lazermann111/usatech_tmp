package com.usatech.tools.deploy.archive;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.AppSettingUpdateFileTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.RunSqlTask;
import com.usatech.tools.deploy.Server;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.tools.deploy.UploadAndRunPostgresSQLTask;
import com.usatech.tools.deploy.UsatSettingUpdateFileTask;

public class R37_Linux_ChangeSet extends AbstractLinuxChangeSet {
	public R37_Linux_ChangeSet() throws UnknownHostException {
		super();
	}

	@Override
	protected void registerApps() {
		// *
		String pgVersion = null; // "9.3.2"
		registerApp(USATRegistry.POSTGRES_PGS, pgVersion);
		registerApp(USATRegistry.POSTGRES_MST, pgVersion);
		registerApp(USATRegistry.POSTGRES_MSR, pgVersion);
		registerApp(USATRegistry.POSTGRES_KLS, pgVersion);
		registerApp(USATRegistry.LOADER, "1.23.0");
		registerApp(USATRegistry.APP_LAYER, "1.23.0");
		registerApp(USATRegistry.INAUTH_LAYER, "1.23.0");
		registerApp(USATRegistry.NET_LAYER, "1.23.0");
		registerApp(USATRegistry.OUTAUTH_LAYER, "1.23.0");
		registerApp(USATRegistry.KEYMGR, "2.7.0"); // */
		registerApp(USATRegistry.DMS_APP, "1.0.12"); // *
		registerApp(USATRegistry.HTTPD_NET, null);
		registry.registerSubApps(USATRegistry.HTTPD_NET, USATRegistry.DMS_APP);
		registerApp(USATRegistry.RPTGEN_LAYER, "3.12.0");
		registerApp(USATRegistry.RPTREQ_LAYER, "3.12.0");
		registerApp(USATRegistry.TRANSPORT_LAYER, "3.12.0");
		registerApp(USATRegistry.USALIVE_APP, "1.13.0");
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.USALIVE_APP);
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.VERIZON_APP);
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.HOTCHOICE_APP);
		registerApp(USATRegistry.PREPAID_APP, "1.3.0");
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.GETMORE_APP); // The APP server needs PREPAID_APP, but the WEB server also needs the GETMORE_APP as subapp
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.PREPAID_APP);
		registerApp(USATRegistry.HTTPD_WEB, null); // */
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareHostTasks(host, tasks, cache);
		tasks.add(new UsatSettingUpdateFileTask(registry.getServerSet(host.getServerEnv())));
		tasks.add(new ExecuteTask("sudo find /opt/USAT/ -name '*.dmp' -delete"));
	}
	
	@Override
	protected void addTasks(Host host, final App app, Integer ordinal, String instance, final int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache);
		if(app.getInstallFileName() != null)
			tasks.add(new AppSettingUpdateFileTask(app, ordinal, instanceNum, instanceCount, cache, registry.getServerSet(host.getServerEnv()), false));
		addInstallAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache, commands);
		if(USATRegistry.POSTGRES_KLS.equals(app)) {
			tasks.add(new UploadAndRunPostgresSQLTask("KeyManagerService/src/km_account_setup.sql", "HEAD", false, ordinal, "km"));
		}
		if(USATRegistry.POSTGRES_MST.equals(app) || USATRegistry.POSTGRES_MSR.equals(app)) {
			tasks.add(new UploadAndRunPostgresSQLTask("Simple1.5/src/simple/mq/peer/mq_peer_postgres_setup_8.sql", "HEAD", false, ordinal, "mq"));
		}
		if(USATRegistry.APP_LAYER.equals(app)) {
			int i = 0;
			Server server = null;
			Iterator<Server> iter = registry.getServerSet(host.getServerEnv()).getServers("MST").iterator();
			for(; i < instanceNum;) {
				server = iter.next();
				i += server.instances.length;
			}
			final Host mstHost = server.host;
			int index = instanceNum - i + server.instances.length - 1;
			String mstInstance = server.instances[index];
			Integer mstOrdinal = (mstInstance == null ? null : Integer.parseInt(mstInstance));
			/*
			tasks.add(new UploadAndRunPostgresSQLTask("DatabaseScripts/edge_implementation/setup_app_db_v3.sql", "HEAD", false, mstOrdinal, "app_" + instanceNum) {
				@Override
				public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
					return perform(deployTaskInfo, mstHost, deployTaskInfo.user);
				}
			});
			 */
			// Add entries to DATA_SYNC table
			String sql = "INSERT INTO ENGINE.DATA_SYNC_" + instanceNum
					+ "(DATA_SYNC_ID, DATA_SYNC_TYPE_CD, OBJECT_CD, OPERATION_CD, ITEM_ID, ITEM_CD, CREATED_BY, CREATED_UTC_TS) SELECT ENGINE.SEQ_DATA_SYNC_ID.NEXTVAL, 'CONSUMER_ACCT', 'PSS.CONSUMER_ACCT', 'U', CONSUMER_ACCT_ID, TO_CHAR(CONSUMER_ACCT_ID), USER, SYS_EXTRACT_UTC(SYSTIMESTAMP) FROM PSS.CONSUMER_ACCT WHERE CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y' AND VZM2P_LOYALTY_ENABLED = 'Y'";
			tasks.add(new RunSqlTask(sql));
		}
	}

	@Override
	public String getName() {
		return "Edge Server - R37 Install - Apps";
	}
}
