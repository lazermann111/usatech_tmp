package com.usatech.tools.deploy.archive;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.usatech.tools.deploy.AbstractSolarisChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.UploadAndRunPostgresSQLTask;

public class R29_Postgres_ChangeSet extends AbstractSolarisChangeSet {
	protected File mqSetup4File = new File(System.getProperty("rootSourceDir", "D:\\Development\\Java Projects") + "\\Simple1.5\\src\\simple\\mq\\peer\\mq_peer_postgres_setup_4.sql");
	public R29_Postgres_ChangeSet() {
		super();
				
	}
	protected void registerApps() {
		registerApp(POSTGRES);
	}
	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) {
	}
	@Override
	protected void addTasks(Host host, App app, Integer ordinal, List<DeployTask> tasks, List<String> commands, final PasswordCache cache) throws IOException {
		if("postgres".equalsIgnoreCase(app.getName()) && (ordinal == null || ordinal == 1)) {
			String[] databases;
			if("LOCAL".equalsIgnoreCase(host.getServerEnv())) {
				databases = new String[] { "bkmq1", "bkmq2", "dkmq1", "dkmq2", "phmq1", "phmq2" };
			} else {
				databases = new String[] { "mq" };
			}
			for(String database : databases)
				tasks.add(new UploadAndRunPostgresSQLTask(mqSetup4File, null, database));
		}
	}
	
	@Override
	public String getName() {
		return "Edge Server - R29 Install - Postgres";
	}
}
