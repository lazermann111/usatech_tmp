package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.APP_LAYER;
import static com.usatech.tools.deploy.USATRegistry.baseDir;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DefaultHostSpecificValue;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.IPTablesUpdateTask;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.PropertiesUpdateFileTask;
import com.usatech.tools.deploy.UploadTask;

public class Enable_SNMP_ChangeSet extends AbstractLinuxChangeSet {
	protected static final File usatJmxAgentJar = new File(baseDir, "USATJMXAgent/dist/usat-jmx-agent-1.0.6.jar");
	public Enable_SNMP_ChangeSet() throws UnknownHostException {
		super();
	}

	@Override
	protected void registerApps() {
		registerApp(APP_LAYER);
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
	}
	
	@Override
	protected void addTasks(Host host, App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		final String appDir = "/opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal);
		tasks.add(new UploadTask(usatJmxAgentJar, appDir + "/lib", 0644, app.getUserName(), app.getUserName()));
		PropertiesUpdateFileTask settingsFile = new PropertiesUpdateFileTask(true, 0640, app.getUserName(), app.getUserName());
		settingsFile.setFilePath(appDir + "/specific/USAT_app_settings.properties");
		int port = 7000 + 100 * (ordinal == null ? 7 : ordinal) + app.getPortOffset() + 3;
		int trap = 7000 + 100 * (ordinal == null ? 7 : ordinal) + app.getPortOffset() + 4;
		settingsFile.registerValue("com.sun.management.snmp.port", new DefaultHostSpecificValue(String.valueOf(port)));
		settingsFile.registerValue("com.sun.management.snmp.trap", new DefaultHostSpecificValue(String.valueOf(trap)));
		tasks.add(settingsFile);
		IPTablesUpdateTask iptTask = new IPTablesUpdateTask();
		iptTask.allowTcpIn("solarwinds.usatech.com", port);
		iptTask.allowTcpIn("solarwinds.usatech.com", trap);
		iptTask.allowTcpIn("10.0.0.64/26", port);
		iptTask.allowTcpIn("10.0.0.64/26", trap);
		tasks.add(iptTask);
		commands.add("sudo su");
		commands.add("initctl restart USAT/" + app.getName() + (ordinal == null ? "" : ordinal.toString()));
	}

	@Override
	public String getName() {
		return "Enable SNMP Agent";
	}
}
