package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.layers.common.constants.ComponentType;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R46_Patch_5_ChangeSet extends MultiLinuxPatchChangeSet {
	public R46_Patch_5_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/LayersCommon/src/", ComponentType.class, "REL_netlayer_1_32_5", USATRegistry.NET_LAYER, USATRegistry.APP_LAYER);
	}

	@Override
	public String getName() {
		return "R46 Patch 5";
	}
}
