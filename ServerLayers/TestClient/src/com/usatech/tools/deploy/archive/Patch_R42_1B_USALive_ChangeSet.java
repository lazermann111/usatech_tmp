package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.usalive.servlet.LoginStep;

public class Patch_R42_1B_USALive_ChangeSet extends AbstractLinuxPatchChangeSet {
	public Patch_R42_1B_USALive_ChangeSet() throws UnknownHostException {
		super();		
		registerSource("usalive/src",  LoginStep.class, "REL_R42_1B");
		registerResource("usalive/xsl/other-data-layer.xml", "classes/", "REL_R42_1B");
		registerResource("usalive/web/offer_signups.html.jsp", "web/", "REL_R42_1B");
		registerResource("usalive/web/public/fall_special_order.html.jsp", "web/public/", "REL_R42_1B");
	}
	protected void registerApps() {
		registerApp(USATRegistry.USALIVE_APP);
	}
	@Override
	public String getName() {
		return "Edge Server - R42.1B USALive Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
