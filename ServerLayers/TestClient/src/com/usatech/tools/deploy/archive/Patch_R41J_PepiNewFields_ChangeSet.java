package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.applayer.AuthorizeTask;
import com.usatech.authoritylayer.InternalAuthorityTask;
import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.USATRegistry;

public class Patch_R41J_PepiNewFields_ChangeSet extends AbstractLinuxPatchChangeSet {

	public Patch_R41J_PepiNewFields_ChangeSet() throws UnknownHostException {
		super();		
		registerSource("ServerLayers/AuthorityLayer/src",  InternalAuthorityTask.class, "BRN_PROD");
		registerResource("ServerLayers/AuthorityLayer/src/authority-data-layer.xml", "classes/", "BRN_PROD");
		registerSource("ServerLayers/AppLayer/src",  AuthorizeTask.class, "BRN_PROD");
	}
	protected void registerApps() {
		registerApp(USATRegistry.INAUTH_LAYER);
		registerApp(USATRegistry.APP_LAYER);
	}
	@Override
	public String getName() {
		return "Edge Server - R41J Pepi New fields Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
