package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.transport.KnownHostsVerifier;
import com.usatech.transport.SecureFTPGanymedTransporter;

public class R46_Patch_1_ChangeSet extends MultiLinuxPatchChangeSet {
	public R46_Patch_1_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ReportGenerator/src/", KnownHostsVerifier.class, "BRN_R45", USATRegistry.TRANSPORT_LAYER);
		registerSource("ReportGenerator/src/", SecureFTPGanymedTransporter.class, "BRN_R45", USATRegistry.TRANSPORT_LAYER);
	}

	@Override
	public String getName() {
		return "R46 Patch 1";
	}
}
