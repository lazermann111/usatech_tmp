package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

/*
 * This changeset fixes issues in R42
 */
public class Patch_R42_2_ChangeSet extends MultiLinuxPatchChangeSet {

	public Patch_R42_2_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ReportGenerator/src/com/usatech/report/rgg-data-layer.xml", "classes/com/usatech/report/", "REL_report_generator_3_16_2", false, USATRegistry.RPTGEN_LAYER);
	}

	@Override
	public String getName() {
		return "Edge Server - R42.2 Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
