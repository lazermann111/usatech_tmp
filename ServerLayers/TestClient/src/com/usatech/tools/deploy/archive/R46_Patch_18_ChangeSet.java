package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.dms.device.DeviceInitStep;
import com.usatech.layers.common.device.DevicesConstants;
import com.usatech.layers.common.util.DeviceUtils;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R46_Patch_18_ChangeSet extends MultiLinuxPatchChangeSet {
	public R46_Patch_18_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/LayersCommon/src", DeviceUtils.class, "REL_DMS_1_8_18", USATRegistry.DMS_APP);
		registerSource("ServerLayers/LayersCommon/src", DevicesConstants.class, "REL_DMS_1_8_18", USATRegistry.DMS_APP);
		registerSource("DMS/src", DeviceInitStep.class, "REL_DMS_1_8_18", USATRegistry.DMS_APP);
	}

	@Override
	public String getName() {
		return "R46 Patch 18";
	}
}
