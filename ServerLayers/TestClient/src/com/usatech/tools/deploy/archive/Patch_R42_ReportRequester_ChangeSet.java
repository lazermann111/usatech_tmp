package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.report.ReportRequestFactory;
import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.USATRegistry;

/*
 * This changeset is fix issues in R42
 */
public class Patch_R42_ReportRequester_ChangeSet extends AbstractLinuxPatchChangeSet {
	protected String baseDir;

	public Patch_R42_ReportRequester_ChangeSet() throws UnknownHostException {
		super();
		// registerSource("ReportGenerator/src", GenerateCreditXMLTask.class, "REL_report_generator_3_16_1");
		// registerSource("ReportGenerator/src", GenerateEFTXMLTask.class, "REL_report_generator_3_16_1");
		registerSource("ReportGenerator/src", ReportRequestFactory.class, "REL_report_generator_3_16_1");
		// registerSource("ReportGenerator/src", CreditData.class, "REL_report_generator_3_16_1");
		// registerSource("ReportGenerator/src", EFTData.class, "REL_report_generator_3_16_1");
		// registerResource("ReportGenerator/src/com/usatech/report/rgg-data-layer.xml", "classes/com/usatech/report/", "REL_report_generator_3_16_1");
	}
	protected void registerApps() {
		registerApp(USATRegistry.RPTREQ_LAYER);
	}
	@Override
	public String getName() {
		return "Edge Server - R42.1 - Report Requester Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
