package com.usatech.tools.deploy.archive;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.List;

import simple.app.Processor;
import simple.app.ServiceException;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.AppSettingUpdateFileTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.DeployTaskInfo;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.tools.deploy.UsatSettingUpdateFileTask;

public class R42_Linux_ChangeSet extends AbstractLinuxChangeSet {
	protected static final Processor<DeployTaskInfo, InputStream> EMPTY_RESOURCE = new Processor<DeployTaskInfo, InputStream>() {
		public InputStream process(DeployTaskInfo argument) throws ServiceException {
			return new ByteArrayInputStream(new byte[0]);
		}

		public Class<InputStream> getReturnType() {
			return InputStream.class;
		}

		public Class<DeployTaskInfo> getArgumentType() {
			return DeployTaskInfo.class;
		}
	};

	public R42_Linux_ChangeSet() throws UnknownHostException {
		super();
	}

	@Override
	public boolean isApplicable(Host host) {
		return super.isApplicable(host);
	}

	@Override
	protected void registerApps() {
		registerApp(USATRegistry.LOADER, "1.28.0");
		registerApp(USATRegistry.POSM_LAYER, "1.19.0");
		registerApp(USATRegistry.RPTGEN_LAYER, "3.16.0");
		registerApp(USATRegistry.RPTREQ_LAYER, "3.16.0");
		registerApp(USATRegistry.USALIVE_APP, "1.18.0");
		registerApp(USATRegistry.TRANSPORT_LAYER, "3.16.0");
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
			addPrepareHostTasks(host, tasks, cache);
			tasks.add(new UsatSettingUpdateFileTask(registry.getServerSet(host.getServerEnv())));
			tasks.add(new ExecuteTask("sudo find /opt/USAT/ -name '*.dmp' -delete"));
	}
	
	@Override
	protected void addTasks(Host host, final App app, Integer ordinal, String instance, final int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache);
		if(app.getInstallFileName() != null)
			tasks.add(new AppSettingUpdateFileTask(app, ordinal, instanceNum, instanceCount, cache, registry.getServerSet(host.getServerEnv()), false));
		addInstallAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache, commands);
	}

	@Override
	public String getName() {
		return "Edge Server - R42 Install - Apps";
	}
}
