package com.usatech.tools.deploy.archive;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.posm.tasks.RiskCheckTask;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DefaultHostSpecificValue;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.GetPasswordHostSpecificValue;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.PropertiesUpdateFileTask;
import com.usatech.tools.deploy.ServerEnvHostSpecificValue;
import com.usatech.tools.deploy.USATRegistry;

public class R47_Patch_1_ChangeSet extends MultiLinuxPatchChangeSet {
	public R47_Patch_1_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ReportGenerator/src/rgr-data-layer.xml", "classes/", "REL_report_generator_3_21_1", true, USATRegistry.RPTREQ_LAYER);
		registerResource("ServerLayers/POSMLayer/conf/posm-data-layer.xml", "classes", "REL_posmlayer_1_24_1", false, USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/POSMLayer/src/main", RiskCheckTask.class, "REL_posmlayer_1_24_1", USATRegistry.POSM_LAYER);
		registerResource("ServerLayers/AppLayer/src/loader-data-layer.xml", "classes", "REL_applayer_1_33_1", false, USATRegistry.LOADER);
		registerResource("ServerLayers/POSMLayer/conf/POSMLayerService.properties", "classes", "REL_posmlayer_1_24_1", true, USATRegistry.POSM_LAYER);
		registerResource("DMS/resources/datalayers/terminal-data-layer.xml", "classes", "REL_DMS_1_9_1", false, USATRegistry.DMS_APP);
	}
	
	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(app.equals(USATRegistry.POSM_LAYER)) {
			PropertiesUpdateFileTask puft = new PropertiesUpdateFileTask(true, 0640, app.getUserName(), app.getUserName());
			puft.setFilePath("/opt/USAT/" + app.getName() + (instance == null ? "" : instance) + "/specific/USAT_app_settings.properties");
			puft.registerValue("USAT.authoritylayer.tandem.fanf.host", new DefaultHostSpecificValue("${USAT.authoritylayer.tandem.sftp.host}"));
			puft.registerValue("USAT.authoritylayer.tandem.fanf.username", new ServerEnvHostSpecificValue("usatfanf", "usatfanf", "usatfanf", "usapfanf"));
			puft.registerValue("USAT.authoritylayer.tandem.fanf.password", new ServerEnvHostSpecificValue("27m5oa7d", "27m5oa7d", "27m5oa7d", new GetPasswordHostSpecificValue(cache, "tandem.sftp.fanf", "Chase Paymentech FANF Upload SFTP", true)));
			puft.registerValue("USAT.authoritylayer.tandem.fanf.upload.directory", new ServerEnvHostSpecificValue("/test/data/${USAT.authoritylayer.tandem.fanf.pid}", "/test/data/${USAT.authoritylayer.tandem.fanf.pid}", "/test/data/${USAT.authoritylayer.tandem.fanf.pid}", "/home/${USAT.authoritylayer.tandem.fanf.pid}"));
			puft.registerValue("USAT.environment.suffix", new ServerEnvHostSpecificValue("-DEV", "-INT", "-ECC", ""));

			tasks.add(puft);
		}
		super.addRestartAppCommands(host, app, instance, instanceNum, instanceCount, tasks, commands, cache);
	}


	@Override
	public String getName() {
		return "R47 Patch 1";
	}
}
