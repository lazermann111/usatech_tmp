package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R45_Patch_8_ChangeSet extends MultiLinuxPatchChangeSet {
	public R45_Patch_8_ChangeSet() throws UnknownHostException {
		super();
		registerResource("usalive/customerreporting_web/terminal_details.jsp", "web", "BRN_R45", false, USATRegistry.USALIVE_APP);

	}

	@Override
	public String getName() {
		return "R45 Patch 8";
	}
}
