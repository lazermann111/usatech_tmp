package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import simple.falcon.engine.standard.DirectXHTMLGenerator;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R46_Patch_4_ChangeSet extends MultiLinuxPatchChangeSet {
	public R46_Patch_4_ChangeSet() throws UnknownHostException {
		super();
		registerSource("Falcon/src/", DirectXHTMLGenerator.class, "BRN_R46", USATRegistry.RPTGEN_LAYER, USATRegistry.USALIVE_APP);
	}

	@Override
	public String getName() {
		return "R46 Patch 4";
	}
}
