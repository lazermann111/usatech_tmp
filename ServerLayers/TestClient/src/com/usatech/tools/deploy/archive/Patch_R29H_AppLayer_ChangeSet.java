package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.APP_LAYER;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;

public class Patch_R29H_AppLayer_ChangeSet extends AbstractLinuxPatchChangeSet {
	protected String baseDir;

	public Patch_R29H_AppLayer_ChangeSet() throws UnknownHostException {
		super();
		baseDir = System.getProperty("rootSourceDir");
		// File simpleBinDir = new File(baseDir + "/Simple1.5/bin");
		// registerClass(simpleBinDir, SingleStashResource.class);
		//File posmlayerBinDir = new File(baseDir + "/POSMLayer/bin");
		//registerClass(posmlayerBinDir, POSMUtils.class);
		//registerResource(posmlayerBinDir, "posm-data-layer.xml");
		File applayerBinDir = new File(baseDir + "/AppLayer/bin");
		registerResource(applayerBinDir, "loader-data-layer.xml");
		// registerClass(applayerBinDir, AppLayerUtils.class);
		// registerClass(applayerBinDir, TranImportTask.class);
		// File layersCommonBinDir = new File(baseDir + "/LayersCommon/bin");
		// registerClass(layersCommonBinDir, LoadDataAttrEnum.class);
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		super.addTasks(host, tasks, commands, cache);
		/*
		if(host.isServerType("APR")) {
			tasks.add(new DeployTask() {
				
				@Override
				public void perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
					final String keystorePath = "/opt/USAT/conf/keystore.ks";	
					final String alias = "mce.applayer.data.20090917";
					// Get the keystore stream
					char[] targetPassword = DeployUtils.getPassword(deployTaskInfo.passwordCache, deployTaskInfo.host, "keystore", "the " + "keystore" + " at '" + keystorePath + "'", deployTaskInfo.interaction);
					HeapBufferStream bufferStream = new HeapBufferStream();
					InputStream in;
					if(UploadTask.checkFileUsingCmd(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.ssh, deployTaskInfo.sftp, deployTaskInfo.user, keystorePath)) {
						deployTaskInfo.interaction.printf("Retrieving contents of keystore at '%1$s'", keystorePath);
						UploadTask.readFileUsingTmp(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.ssh, deployTaskInfo.sftp, deployTaskInfo.user, keystorePath, bufferStream.getOutputStream(), null);
						in = bufferStream.getInputStream();
					} else {
						in = null;
					}

					// Decode with password
					try {
						KeyStore targetKeyStore = SecurityUtils.getKeyStore(in, "JKS", null, targetPassword);

						if(targetKeyStore.isKeyEntry(alias)) {
							deployTaskInfo.interaction.printf("Keystore at '%1$s' on %2$s already contains alias '%3$s'", keystorePath, deployTaskInfo.host.getSimpleName(), alias);
							return;
						}
						String sourceHostName;
						if("USA".equalsIgnoreCase(deployTaskInfo.host.getServerEnv()))
							sourceHostName = "usaapr21.trooper.usatech.com";
						else
							sourceHostName = deployTaskInfo.host.getServerEnv().toLowerCase() + "apr01.usatech.com";
						Host sourceHost = new HostImpl(deployTaskInfo.host.getServerEnv(), "APR_OLD", sourceHostName);
						SshClient sourceSsh = deployTaskInfo.sshClientCache.getSshClient(sourceHost, deployTaskInfo.user);
						char[] sourcePassword = DeployUtils.getPassword(deployTaskInfo.passwordCache, sourceHost, "keystore", "the " + "keystore" + " at '" + keystorePath + "'", deployTaskInfo.interaction);
						SftpClient sourceSftp = sourceSsh.openSftpClient();
						try {

							// Get the keystore stream
							bufferStream.clear();
							if(UploadTask.checkFileUsingCmd(sourceHost, deployTaskInfo.passwordCache, deployTaskInfo.interaction, sourceSsh, sourceSftp, deployTaskInfo.user, keystorePath)) {
								deployTaskInfo.interaction.printf("Retrieving contents of keystore at '%1$s'", keystorePath);
								UploadTask.readFileUsingTmp(sourceHost, deployTaskInfo.passwordCache, deployTaskInfo.interaction, sourceSsh, sourceSftp, deployTaskInfo.user, keystorePath, bufferStream.getOutputStream(), null);
								in = bufferStream.getInputStream();
							} else {
								in = null;
							}
						} finally {
							sourceSftp.quit();
						}

						// Decode with password
						KeyStore sourceKeyStore = SecurityUtils.getKeyStore(in, "JKS", null, sourcePassword);
						if(!sourceKeyStore.isKeyEntry(alias)) {
							deployTaskInfo.interaction.printf("Keystore at '%1$s' on %2$s does not contains alias '%3$s'; not copying", keystorePath, sourceHost.getSimpleName(), alias);
							return;
						}
						if(CryptUtils.copyKeystoreEntry(alias, sourceKeyStore, sourcePassword, targetKeyStore, targetPassword, null)) {
							deployTaskInfo.interaction.printf("Copied key for alias '%3$s' into keystore at '%1$s' on %2$s", keystorePath, deployTaskInfo.host.getSimpleName(), alias);
							bufferStream.clear();
							targetKeyStore.store(bufferStream.getOutputStream(), targetPassword);
							UploadTask.writeFileUsingTmp(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.ssh, deployTaskInfo.sftp, deployTaskInfo.user, bufferStream.getInputStream(), null, null, null, null, keystorePath);
							deployTaskInfo.interaction.printf("Saved new keystore at '%1$s' on %2$s", keystorePath, deployTaskInfo.host.getSimpleName());
						}
					} catch(KeyStoreException e) {
						throw new IOException(e);
					} catch(NoSuchAlgorithmException e) {
						throw new IOException(e);
					} catch(UnrecoverableEntryException e) {
						throw new IOException(e);
					} catch(NoSuchProviderException e) {
						throw new IOException(e);
					} catch(CertificateException e) {
						throw new IOException(e);
					}
				}
			});
		}
		// */
	}
	protected void registerApps() {
		registerApp(APP_LAYER);
	}
	@Override
	public String getName() {
		return "Edge Server - R29H AppLayer Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}

	@Override
	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		// do nothing
	}
}
