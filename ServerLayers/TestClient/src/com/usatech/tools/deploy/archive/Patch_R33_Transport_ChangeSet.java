package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.TRANSPORT_LAYER;

import java.io.File;
import java.net.UnknownHostException;


import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
import com.usatech.transport.EmailTransporter;
import com.usatech.transport.TransportTask;

public class Patch_R33_Transport_ChangeSet extends AbstractLinuxPatchChangeSet {
	protected String baseDir;

	public Patch_R33_Transport_ChangeSet() throws UnknownHostException {
		super();
		baseDir = System.getProperty("rootSourceDir");
		File simpleBinDir = new File(baseDir + "/ReportGenerator/bin");
		registerClass(simpleBinDir, TransportTask.class);
		registerClass(simpleBinDir, EmailTransporter.class);
		registerResource("ReportGenerator/src/TransportService.properties", "classes/", "HEAD");
	}
	protected void registerApps() {
		registerApp(TRANSPORT_LAYER);
	}
	@Override
	public String getName() {
		return "Edge Server - R33 - Transport Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
