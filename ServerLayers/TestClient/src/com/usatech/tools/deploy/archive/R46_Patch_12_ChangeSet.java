package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R46_Patch_12_ChangeSet extends MultiLinuxPatchChangeSet {
	public R46_Patch_12_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/NetLayer/src/NetLayerService.properties", "classes", "BRN_R46", false, USATRegistry.NET_LAYER);
	}

	@Override
	public String getName() {
		return "R46 Patch 12";
	}
}
