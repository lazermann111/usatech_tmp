package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R45_Patch_1_ChangeSet extends MultiLinuxPatchChangeSet {
	public R45_Patch_1_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ReportGenerator/src/com/usatech/report/rgr-data-layer.xml", "classes/com/usatech/report/", "BRN_R45", false, USATRegistry.RPTREQ_LAYER);

	}

	@Override
	public String getName() {
		return "R45 Patch 1 report requester";
	}
}
