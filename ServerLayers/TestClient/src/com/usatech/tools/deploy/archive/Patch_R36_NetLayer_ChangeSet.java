package com.usatech.tools.deploy.archive;


import java.net.UnknownHostException;

import com.usatech.eportconnect.ECSessionManager;
import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.USATRegistry;

public class Patch_R36_NetLayer_ChangeSet extends AbstractLinuxPatchChangeSet {

	public Patch_R36_NetLayer_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/NetLayer/src/NetLayerService.properties", "classes/", "BRN_PROD");
		registerSource("ServerLayers/NetLayer/src", ECSessionManager.class, "BRN_PROD");
	}
	protected void registerApps() {
		registerApp(USATRegistry.NET_LAYER);
	}
	@Override
	public String getName() {
		//return "Patch Edge Server - R36 - Usalive IE 11 Patch";
		return "Patch Edge Server - R36 - NetLayer Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
	
	@Override
	public boolean isApplicable(Host host) {
		return true;
	}
}
