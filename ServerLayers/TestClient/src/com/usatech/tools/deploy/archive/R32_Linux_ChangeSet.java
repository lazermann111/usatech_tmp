package com.usatech.tools.deploy.archive;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.AppSettingUpdateFileTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.tools.deploy.UsatSettingUpdateFileTask;

public class R32_Linux_ChangeSet extends AbstractLinuxChangeSet {
	public R32_Linux_ChangeSet() throws UnknownHostException {
		super();
	}

	@Override
	protected void registerApps() {
		registerApp(USATRegistry.LOADER, "1.18.0");
		registerApp(USATRegistry.APP_LAYER, "1.18.0");
		registerApp(USATRegistry.INAUTH_LAYER, "1.18.0");
		registerApp(USATRegistry.POSM_LAYER, "1.9.0");
		registerApp(USATRegistry.NET_LAYER, "1.18.0");
		registerApp(USATRegistry.OUTAUTH_LAYER, "1.18.0");
		registerApp(USATRegistry.KEYMGR, "2.2.0");
		registerApp(USATRegistry.POSTGRES_KLS);
		registerApp(USATRegistry.POSTGRES_MST);
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareHostTasks(host, tasks, cache);
		if(host.isServerType("NET") || host.isServerType("APR"))
			tasks.add(new UsatSettingUpdateFileTask(registry.getServerSet(host.getServerEnv())));
	}
	
	@Override
	protected void addTasks(Host host, final App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache);
		if(app.getInstallFileName() != null)
			tasks.add(new AppSettingUpdateFileTask(app, ordinal, instanceNum, instanceCount, cache, registry.getServerSet(host.getServerEnv()), false));
		addInstallAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache, commands);
		tasks.add(new ExecuteTask("sudo su", "if [ ! -x /bin/monit ]; then ln -sf /usr/monit-latest/bin/monit /bin/monit; fi"));
	}

	protected void addBeforeRestartCommands(Host host, App app, Integer ordinal, int instanceNum, List<String> commands, List<DeployTask> tasks) {
		StringBuilder sb = new StringBuilder();
		if(USATRegistry.APP_LAYER.equals(app)) {
			sb.append("/bin/rm -r /opt/USAT/").append(app.getName());
			if(ordinal != null)
				sb.append(ordinal);
			sb.append("/db/AppLayerDBOld");
			commands.add(sb.toString());
			sb.setLength(0);
			sb.append("/bin/mv /opt/USAT/").append(app.getName());
			if(ordinal != null)
				sb.append(ordinal);
			sb.append("/db/AppLayerDB /opt/USAT/").append(app.getName());
			if(ordinal != null)
				sb.append(ordinal);
			sb.append("/db/AppLayerDBOld");
			commands.add(sb.toString());
			sb.setLength(0);
			sb.append("OLD_DATE=`date +%Y%m%d-%H%M%S` && /bin/cp /opt/USAT/").append(app.getName());
			if(ordinal != null)
				sb.append(ordinal);
			sb.append("/db/derby.properties /opt/USAT/").append(app.getName());
			if(ordinal != null)
				sb.append(ordinal);
			sb.append("/db/derby.properties.$OLD_DATE && sed 's/derby\\.user\\.APP_LAYER=.*/derby.user.APP_LAYER='`grep USAT.database.EMBED.password.APP_LAYER /opt/USAT/").append(app.getName());
			if(ordinal != null)
				sb.append(ordinal);
			sb.append("/specific/USAT_app_settings.properties | cut -d= -f2`'/\ns/derby\\.user\\.APP_CLUSTER=.*/derby.user.APP_CLUSTER='`grep USAT.database.EMBED.password.APP_CLUSTER /opt/USAT/").append(app.getName());
			if(ordinal != null)
				sb.append(ordinal);
			sb.append("/specific/USAT_app_settings.properties | cut -d= -f2`'/' /opt/USAT/").append(app.getName());
			if(ordinal != null)
				sb.append(ordinal);
			sb.append("/db/derby.properties.$OLD_DATE > /opt/USAT/").append(app.getName());
			if(ordinal != null)
				sb.append(ordinal);
			sb.append("/db/derby.properties");
			commands.add(sb.toString());
		}
	}

	@Override
	public String getName() {
		return "Edge Server - R32 Install - Apps";
	}
}
