package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R45_Patch_17_ChangeSet extends MultiLinuxPatchChangeSet {
	public R45_Patch_17_ChangeSet() throws UnknownHostException {
		super();
		registerResource("DMS/webroot/WEB-INF/web.xml", "web/WEB-INF", "REL_DMS_1_7_17", true, USATRegistry.DMS_APP);
	}

	@Override
	public String getName() {
		return "R45 Patch 17";
	}

	@Override
	protected void addPostHostTasks(List<DeployTask> tasks) {
		super.addPostAppsTasks(tasks);
		tasks.add(new ExecuteTask("sudo su", "sed -i 's/credential=[^,]*[^*][^,]*,/credential=***,/g' /opt/USAT/dms/logs/DMS.log*", "for logfile in /opt/USAT/dms/logs/DMS.*.gz; do gunzip -c $logfile | sed 's/credential=[^,]*[^*][^,]*,/credential=***,/g' | gzip -c > $logfile.SAFE && /bin/mv $logfile.SAFE $logfile; done"));
	}
}
