package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import simple.util.concurrent.ConcurrentQueueBuffer;

import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.USATRegistry;

public class Patch_EdgeServer_ChangeSet extends AbstractLinuxPatchChangeSet {
	protected String baseDir;

	public Patch_EdgeServer_ChangeSet() throws UnknownHostException {
		super();
		baseDir = System.getProperty("rootSourceDir");
		// File simpleBinDir = new File(baseDir + "/Simple1.5/bin");
		// registerClass(simpleBinDir, AbstractSegmentedFutureCache.class);
		// registerClass(simpleBinDir, AbstractBufferInterpretter.class);
		// File posmlayerBinDir = new File(baseDir + "/POSMLayer/bin");
		//registerClass(posmlayerBinDir, POSMUtils.class);
		// registerClass(posmlayerBinDir, POSMSettlementFeedbackTask.class);
		// registerResource(posmlayerBinDir, "posm-data-layer.xml");
		// registerClass(posmlayerBinDir, POSMCheckTask.class);
		// registerClass(posmlayerBinDir, POSMNextTask.class);
		// File layersCommonBinDir = new File(baseDir + "/LayersCommon/bin");
		// registerClass(layersCommonBinDir, AppLayerUtils.class);
		// File layersCommonBinDir = new File(baseDir + "/LayersCommon/bin");
		// registerClass(layersCommonBinDir, LoadDataAttrEnum.class);
		// registerClass(simpleBinDir, AbstractServiceManager.class);
		// registerClass(simpleBinDir, Base.class);
		// registerClass(simpleBinDir, BaseWithConfig.class);
		// registerClass(simpleBinDir, ServiceManager.class);
		// registerClass(simpleBinDir, ServiceManagerWithConfig.class);
		// File authlayerBinDir = new File(baseDir + "/AuthorityLayer/bin");
		// registerResource(authlayerBinDir, "OutsideAuthorityLayerService.properties");
		// File applayerBinDir = new File(baseDir + "/AppLayer/bin");
		// File netlayerBinDir = new File(baseDir + "/NetLayer/bin");

		// registerClass(netlayerBinDir, ReplyOnlyProcessor_FileTransferAck.class);
		// registerClass(applayerBinDir, AuthorizeTask.class);
		// registerClass(layersCommonBinDir, AlertingUtils.class);
		// registerClass(layersCommonBinDir, AppLayerUtils.class);

		// File keymgrBinDir = new File(baseDir + "/KeyManagerService/bin");
		// registerClass(keymgrBinDir, MassDecryptTask.class);

		// registerClass(applayerBinDir, AppLayerDeviceInfoManager.class);
		// registerResource(applayerBinDir, "AppLayerService.properties");

		// registerClass(simpleBinDir, simple.io.resource.AbstractDbResourceFolder.class);
		// registerClass(applayerBinDir, InboundFileImportTask.class);
		// File netlayerBinDir = new File(baseDir + "/NetLayer/bin");
		// registerClass(netlayerBinDir, Layer3Transmission2.class);
		// registerClass(applayerBinDir, DataSyncTask.class);
		// registerClass(authlayerBinDir, InternalAuthorityTask.class);
		// registerClass(applayerBinDir, InboundFileTransferTask.class);

		// registerClass(simpleBinDir, AbstractServiceManager.class);
		// registerClass(simpleBinDir, SystemUtils.class);
		// registerSource("ServerLayers/NetLayer/src", ECRequestHandler.class, "BRN_R33");
		// registerSource("ServerLayers/NetLayer/src", ECSessionManager.class, "BRN_R33");

		// registerResource(posmlayerBinDir, "posm-data-layer.xml", "classes");
		registerSource("Simple1.5/src", ConcurrentQueueBuffer.class, "HEAD");

	}
	protected void registerApps() {
		registerApp(USATRegistry.LOADER);
		registerApp(USATRegistry.APP_LAYER);
		registerApp(USATRegistry.INAUTH_LAYER);
		registerApp(USATRegistry.POSM_LAYER);
		registerApp(USATRegistry.DMS_APP);
		
		registerApp(USATRegistry.NET_LAYER);
		registerApp(USATRegistry.OUTAUTH_LAYER);
		
		registerApp(USATRegistry.KEYMGR);
		
		registerApp(USATRegistry.RPTGEN_LAYER);
		registerApp(USATRegistry.RPTREQ_LAYER);
		registerApp(USATRegistry.USALIVE_APP);
		registerApp(USATRegistry.PREPAID_APP);
		
		registerApp(USATRegistry.TRANSPORT_LAYER);
	}
	@Override
	public String getName() {
		return "Edge Server - Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}

	/*
	@Override
	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		commands.add("if [ `grep -c 'log4j.logger.simple.io.server.AbstractBufferInterpretter=DEBUG' /opt/USAT/" + app.getName() + (instance == null ? "" : instance) + "/specific/log4j.properties` -eq 0 ]; then echo 'log4j.logger.simple.io.server.AbstractBufferInterpretter=DEBUG' >> /opt/USAT/" + app.getName() + (instance == null ? "" : instance) + "/specific/log4j.properties; fi");
		super.addRestartAppCommands(host, app, instance, instanceNum, instanceCount, tasks, commands, cache);
	}
	// */
}
