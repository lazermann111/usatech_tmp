package com.usatech.tools.deploy.archive;

import java.io.File;

import com.usatech.tools.deploy.AbstractSolarisPatchChangeSet;

import simple.mq.peer.DirectPeerProducer;

public class R28A_ChangeSet extends AbstractSolarisPatchChangeSet {
	public R28A_ChangeSet() {
		super();
		appMap.remove("MST");
		registerClass(new File(System.getProperty("rootSourceDir", "D:\\Development\\Java Projects") + "\\Simple1.5\\bin"), DirectPeerProducer.class);
	}
	@Override
	public String getName() {
		return "Edge Server - R28A Patch";
	}
}
