package com.usatech.tools.deploy.archive;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;

public class Patch_Prepaid_ChangeSet extends AbstractLinuxPatchChangeSet {
	public Patch_Prepaid_ChangeSet() throws UnknownHostException {
		super();
		// registerResource("Prepaid/web/signup.html.jsp", "web", "REL_prepaid_1_1_1");
		// registerResource("Prepaid/src/prepaid-data-layer.xml", "classes", "REL_prepaid_1_1_2");
		// registerResource("Prepaid/web/index.html.jsp", "web", "HEAD");
		// registerResource("Prepaid/web/vendor/index.html.jsp", "web/vendor", "HEAD");
		registerResource("Prepaid/web/update_password_by_passcode.html.jsp", "web", "HEAD");
	}
	protected void registerApps() {
		registerApp(USATRegistry.PREPAID_APP);
	}

	@Override
	public String getName() {
		return "Prepaid Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}

	@Override
	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		// no restart needed
	}
}
