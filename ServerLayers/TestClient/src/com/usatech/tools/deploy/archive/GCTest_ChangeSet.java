package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.APP_LAYER;
import static com.usatech.tools.deploy.USATRegistry.INAUTH_LAYER;
import static com.usatech.tools.deploy.USATRegistry.NET_LAYER;
import static com.usatech.tools.deploy.USATRegistry.OUTAUTH_LAYER;
import static com.usatech.tools.deploy.USATRegistry.POSM_LAYER;
import static com.usatech.tools.deploy.USATRegistry.baseDir;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.UploadTask;

public class GCTest_ChangeSet implements ChangeSet {
	protected static final File setenvFile = new File(baseDir, "LayersCommon/conf/setenv-gctest.sh");
	@Override
	public String getName() {
		return "GC Test Setup";
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		App[] apps = new App[] { APP_LAYER, NET_LAYER, INAUTH_LAYER, POSM_LAYER, OUTAUTH_LAYER };
		List<DeployTask> tasks = new ArrayList<DeployTask>(apps.length + 1);
		List<String> commands = new ArrayList<String>(apps.length);
		commands.add("sudo su");
		for(App app : apps) {
			if(host.isServerType(app.getServerType())) {
				tasks.add(new UploadTask(new FileInputStream(setenvFile), 0750, app.getName(), app.getName(), setenvFile.getCanonicalPath(), true, "/opt/USAT/" + app.getName() + "1/bin/setenv.sh"));
				commands.add("initctl restart USAT/" + app.getName() + "1");
			}
		}
		tasks.add(new ExecuteTask(commands.toArray(new String[commands.size()])));
		return tasks.toArray(new DeployTask[tasks.size()]);
	}

	@Override
	public boolean isApplicable(Host host) {
		return true;
	}
	@Override
	public String toString() {
		return getName();
	}
}
