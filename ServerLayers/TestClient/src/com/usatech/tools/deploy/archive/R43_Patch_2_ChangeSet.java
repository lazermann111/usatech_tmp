package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import simple.text.MinimalCDataCharset;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.transport.soap.vdi.VDIDexUploadInput;

public class R43_Patch_2_ChangeSet extends MultiLinuxPatchChangeSet {
	public R43_Patch_2_ChangeSet() throws UnknownHostException {
		super();
		registerSource("Simple1.5/src", MinimalCDataCharset.class, "REL_report_generator_3_17_3", USATRegistry.TRANSPORT_LAYER);
		registerSource("ReportGenerator/src", VDIDexUploadInput.class, "REL_report_generator_3_17_3", USATRegistry.TRANSPORT_LAYER);
	}

	@Override
	public String getName() {
		return "R43 Patch #2";
	}
}
