package com.usatech.tools.deploy.archive;

import java.io.File;

import com.usatech.tools.deploy.AbstractSolarisPatchChangeSet;

import simple.db.specific.AbstractSpecific;
import simple.db.specific.OracleSpecific;

public class PatchUSALiveForOracle11_ChangeSet extends AbstractSolarisPatchChangeSet {
	public PatchUSALiveForOracle11_ChangeSet() {
		super();
		File simpleSrcDir = new File(baseDir + "/Simple1.5/src");
		registerSource(simpleSrcDir, AbstractSpecific.class);
		registerSource(simpleSrcDir, OracleSpecific.class);
		// File simpleBinDir = new File(baseDir + "/Simple1.5/bin");
		// registerClass(simpleBinDir, OracleSpecific.class);
		// registerClass(simpleBinDir, StringUtils.class);
	}
	protected void registerApps() {
		registerApp(USALIVE_APP);
		registerApp(RPT_GEN);
	}
	@Override
	protected void registerOrds() {
		Integer[] ords = new Integer[]{null};
		ordMap.put("DEV", ords);
		ordMap.put("INT", ords);
		ordMap.put("ECC", ords);
		ordMap.put("USA", new Integer[]{null});
	}
	@Override
	public String getName() {
		return "Patch USALive for Oracle 11g";
	}
}
