package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.prepaid.web.PrepaidUtils;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R45_Patch_6_ChangeSet extends MultiLinuxPatchChangeSet {
	public R45_Patch_6_ChangeSet() throws UnknownHostException {
		super();
		registerSource("Prepaid/src", PrepaidUtils.class, "BRN_R45", USATRegistry.PREPAID_APP);
	}

	@Override
	public String getName() {
		return "R45 Patch #6";
	}
}
