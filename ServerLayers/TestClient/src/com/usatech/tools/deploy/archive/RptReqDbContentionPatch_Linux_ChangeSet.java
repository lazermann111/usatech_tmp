package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.RPTREQ_LAYER;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;

public class RptReqDbContentionPatch_Linux_ChangeSet extends AbstractLinuxChangeSet {
	public RptReqDbContentionPatch_Linux_ChangeSet() throws UnknownHostException {
		super();
	}
	
	private static final String RG_APP_VERSION = "3.7.1";
	
	@Override
	protected void registerApps() {
		registerApp(RPTREQ_LAYER, RG_APP_VERSION);
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareHostTasks(host, tasks, cache);
	}
	
	@Override
	protected void addTasks(Host host, App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache);
		addInstallAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache, commands);
	}

	@Override
	public String getName() {
		return "RptReq Db Contention Patch Install";
	}
}
