package com.usatech.tools.deploy.archive;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;

public class PreR33_Linux_ChangeSet extends AbstractLinuxChangeSet {
	public PreR33_Linux_ChangeSet() throws UnknownHostException {
		super();
	}

	@Override
	protected void registerApps() {
		registerApp(USATRegistry.NET_LAYER);
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
	}
	
	@Override
	protected void addTasks(Host host, final App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		StringBuilder sb = new StringBuilder();
		sb.append("if [ `grep -c 'simple.security.EncryptionInfoMapping.encryptionInfo(usat.keymanager.store).cipherName=RSA' /opt/USAT/netlayer");
		if(instance != null)
			sb.append(instance);
		sb.append("/classes/NetLayerService.properties` -eq 0 ]; then\necho '")
			.append("simple.security.EncryptionInfoMapping.encryptionInfo(usat.keymanager.store).cipherName=RSA\n")
			.append("simple.security.EncryptionInfoMapping.encryptionInfo(usat.keymanager.store).secondaryCipherName=AES\n")
			.append("simple.security.EncryptionInfoMapping.encryptionInfo(usat.keymanager.store).keyAliasPrefix=mce.keymgr.data.\n")
			.append("simple.security.EncryptionInfoMapping.encryptionInfo(usat.keymanager.store).keySize=256\n")
			.append("' >> /opt/USAT/netlayer");
		if(instance != null)
			sb.append(instance);
		sb.append("/classes/NetLayerService.properties");
		if(instance == null || "1".equals(instance)) {
			sb.append("; monit restart netlayer");
			if(instance != null)
				sb.append(instance);
		}
		sb.append("; fi");
		tasks.add(new ExecuteTask("sudo su", sb.toString()));
	}

	@Override
	public String getName() {
		return "Edge Server - Pre R33 Install - NetLayer";
	}
}
