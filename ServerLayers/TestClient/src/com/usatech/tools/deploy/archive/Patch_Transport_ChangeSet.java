package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.USATRegistry;

public class Patch_Transport_ChangeSet extends AbstractLinuxPatchChangeSet {
	public Patch_Transport_ChangeSet() throws UnknownHostException {
		super();
		// registerLib("ThirdPartyJavaLibraries/lib/ftpsbean-1.4.5-usat-6.jar", "HEAD");
		// registerSource("ReportGenerator/src", FTPTransporter.class, "REL_report_generator_3_9_3");
		registerResource("ReportGenerator/src/TransportService.properties", "classes", "REL_report_generator_3_15_1");
		// registerSource("ReportGenerator/src", EmailTransporter.class, "HEAD");
	}


	@Override
	public boolean isApplicable(Host host) {
		return true;
	}
	protected void registerApps() {
		registerApp(USATRegistry.TRANSPORT_LAYER);
	}
	@Override
	public String getName() {
		return "Transport Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
	/*
	@Override
	protected void addTasks(Host host, App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		String appDir = "/opt/USAT/" + app.getName() + (instance == null ? "" : instance);
		String scriptFile = appDir + "/bin/" + app.getServiceName() + ".sh";
		if(commands.isEmpty())
			commands.add("sudo su");
		commands.add("if grep -c 'CLASSPATH=\".*:lib/ftpsbean-1.4.5-usat-5.jar' " + scriptFile + "; then /bin/cp " + scriptFile + " " + scriptFile + ".ORIG && sed 's#:lib/ftpsbean-1.4.5-usat-5.jar##g' " + scriptFile + ".ORIG > " + scriptFile + "; fi");
		super.addTasks(host, app, ordinal, instance, instanceNum, instanceCount, tasks, commands, cache);
	}
	*/
	/*
	@Override
	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
	}
	// */
}
