package com.usatech.tools.deploy.archive;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.CvsPullTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.tools.deploy.UploadTask;
import com.usatech.usalive.servlet.GeneratePasscodeStep;

public class Patch_R31L_ChangeSet extends AbstractLinuxPatchChangeSet {
	protected String USALive_TAG="REL_USALive_1_9_0";
	
	
	public Patch_R31L_ChangeSet() throws UnknownHostException {
		super();
		registerResource("usalive/xsl/device-actions.xml", "classes", USALive_TAG);
		registerResource("usalive/xsl/device-data-layer.xml", "classes", USALive_TAG);
		registerSource("usalive/src", GeneratePasscodeStep.class, USALive_TAG);
		registerResource("usalive/customerreporting_web/terminal_details.jsp", "web", USALive_TAG);
		registerResource("usalive/web/css/usalive-app-style.css", "web/css", USALive_TAG);
		
		
	}
	protected void registerApps() {
		registerApp(USATRegistry.USALIVE_APP);
		registerApp(USATRegistry.HTTPD_WEB);
	}

	@Override
	public String getName() {
		return "Edge Server - R31L Patch USALive - Apps";
	}
	
	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		super.addTasks(host, tasks, commands, cache);
		if(host.isServerType("WEB")) {
			ExecuteTask backUpTask=new ExecuteTask("sudo su",
					"cp /opt/USAT/httpd/usalive/CustomerReporting/css/usalive-app-style.css /opt/USAT/httpd/usalive/CustomerReporting/css/usalive-app-style.css.bak",
					"cp /opt/USAT/httpd/hotchoice/CustomerReporting/css/usalive-app-style.css /opt/USAT/httpd/hotchoice/CustomerReporting/css/usalive-app-style.css.bak",
					"cp /opt/USAT/httpd/verizon/CustomerReporting/css/usalive-app-style.css /opt/USAT/httpd/verizon/CustomerReporting/css/usalive-app-style.css.bak"
				);
			tasks.add(backUpTask);
		}
		
		if(host.isServerType("APP")) {
			ExecuteTask backUpTask=new ExecuteTask("sudo su",
					"cp /opt/USAT/usalive/classes/device-actions.xml /opt/USAT/usalive/classes/device-actions.xml.bak",
					"cp /opt/USAT/usalive/classes/device-data-layer.xml /opt/USAT/usalive/classes/device-data-layer.xml.bak",
					"cp /opt/USAT/usalive/web/terminal_details.jsp /opt/USAT/usalive/web/terminal_details.jsp.bak"
				);
			tasks.add(backUpTask);
		}
		
	}
	
	@Override
	protected void addTasks(Host host, App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(app.equals(USATRegistry.HTTPD_WEB)) {
			tasks.add(new UploadTask(new CvsPullTask("usalive/web/css/usalive-app-style.css", USALive_TAG), 0644, app.getUserName(), app.getUserName(), "usalive-app-style.css", true, 
					"/opt/USAT/" + app.getName() + (instance == null ? "" : instance) + "/usalive/CustomerReporting/css/usalive-app-style.css",
					"/opt/USAT/" + app.getName() + (instance == null ? "" : instance) + "/hotchoice/CustomerReporting/css/usalive-app-style.css",
					"/opt/USAT/" + app.getName() + (instance == null ? "" : instance) + "/verizon/CustomerReporting/css/usalive-app-style.css"));
		} else
			super.addTasks(host, app, ordinal, instance, instanceNum, instanceCount, tasks, commands, cache);
		
	}

}
