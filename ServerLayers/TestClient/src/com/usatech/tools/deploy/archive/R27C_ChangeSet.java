package com.usatech.tools.deploy.archive;

import java.io.File;

import com.usatech.tools.deploy.AbstractSolarisPatchChangeSet;

import simple.db.WatchedCallableStatement;

public class R27C_ChangeSet extends AbstractSolarisPatchChangeSet {
	public R27C_ChangeSet() {
		super();
		appMap.remove("MST");
		registerClass(new File(System.getProperty("rootSourceDir", "D:\\Development\\Java Projects") + "\\Simple1.5\\bin"), WatchedCallableStatement.class);
	}
	@Override
	public String getName() {
		return "Edge Server - R27C Patch";
	}
}
