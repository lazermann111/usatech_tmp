package com.usatech.tools.deploy.archive;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import simple.falcon.engine.standard.DirectXHTMLGenerator;

import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.CvsPullTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.tools.deploy.UploadTask;

public class USALive_1_8_0_PatchJ_ChangeSet extends AbstractLinuxPatchChangeSet {
	public USALive_1_8_0_PatchJ_ChangeSet() throws UnknownHostException {
		super();
		registerSource("Falcon/src", DirectXHTMLGenerator.class, "REL_USALive_1_8_0_J");
		// registerSource("Simple1.5/src", PeerSupervisor2.class, "REL_USALive_1_8_0_J");
		registerResource("usalive/web/css/usalive-app-style.css", "web/css", "REL_USALive_1_8_0_J");
	}

	protected void registerApps() {
		registerApp(USATRegistry.RPTGEN_LAYER);
		registerApp(USATRegistry.USALIVE_APP);
		registerApp(USATRegistry.HTTPD_WEB);
	}
	@Override
	public String getName() {
		return "Patch J USALive 1.8.0";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}

	@Override
	protected void addTasks(Host host, App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(app.equals(USATRegistry.HTTPD_WEB)) {
			tasks.add(new UploadTask(new CvsPullTask("usalive/web/css/usalive-app-style.css", "REL_USALive_1_8_0_J"), 0644, app.getUserName(), app.getUserName(), "usalive-app-style.css", true, 
					"/opt/USAT/" + app.getName() + (instance == null ? "" : instance) + "/usalive/CustomerReporting/css/usalive-app-style.css",
					"/opt/USAT/" + app.getName() + (instance == null ? "" : instance) + "/hotchoice/CustomerReporting/css/usalive-app-style.css",
					"/opt/USAT/" + app.getName() + (instance == null ? "" : instance) + "/verizon/CustomerReporting/css/usalive-app-style.css"));
		} else
			super.addTasks(host, app, ordinal, instance, instanceNum, instanceCount, tasks, commands, cache);
		/*
		if(app.equals(USATRegistry.RPTGEN_LAYER)) {
			PropertiesUpdateFileTask puft = new PropertiesUpdateFileTask(true, null, null, null);
			puft.setFilePath("/opt/USAT/" + app.getName() + (instance == null ? "" : instance) + "/classes/ReportGeneratorService.properties");
			puft.registerValue("com.usatech.report.ReportRequestFactory.transportMaxRetryAllowed", new DefaultHostSpecificValue("100"));
			tasks.add(puft);
		}
		*/
	}
}
