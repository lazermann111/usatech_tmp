package com.usatech.tools.deploy.archive;

import java.io.File;

import com.usatech.applayer.TranImportTask;
import com.usatech.tools.deploy.AbstractSolarisPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;

public class R28D_ChangeSet extends AbstractSolarisPatchChangeSet {
	String baseDir;
	public R28D_ChangeSet() {
		super();
		baseDir = System.getProperty("rootSourceDir");
		File applayerBinDir = new File(baseDir + "/AppLayer/bin");
		registerClass(applayerBinDir, TranImportTask.class);
	}
	protected void registerApps() {
		registerApp(APP_LAYER);
	}
	@Override
	public String getName() {
		return "Edge Server - R28D Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
