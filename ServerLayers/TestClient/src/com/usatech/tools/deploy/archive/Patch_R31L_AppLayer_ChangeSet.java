package com.usatech.tools.deploy.archive;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.layers.common.LegacyUpdateStatusProcessor;
import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;

public class Patch_R31L_AppLayer_ChangeSet extends AbstractLinuxPatchChangeSet {
	
	protected String AppLayer_TAG="REL_applayer_1_18_0";
	
	
	public Patch_R31L_AppLayer_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/AppLayer/src/applayer-data-layer.xml", "classes", "REL_EportMobilePasscode");
		registerSource("ServerLayers/LayersCommon/src", LegacyUpdateStatusProcessor.class, AppLayer_TAG);
		
		
	}
	protected void registerApps() {
		registerApp(USATRegistry.APP_LAYER);
	}

	@Override
	public String getName() {
		return "Edge Server - R31L Patch AppLayer - Apps";
	}
	
	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		super.addTasks(host, tasks, commands, cache);
		
		if(host.isServerType("APR")) {
			String applayerDir=null;
			if("USA".equalsIgnoreCase(host.getServerEnv())){
				applayerDir="/opt/USAT/applayer/";
				
			}else{
				applayerDir="/opt/USAT/applayer1/";
			}
			ExecuteTask backUpTask=new ExecuteTask("sudo su",
					"cp "+applayerDir+"classes/applayer-data-layer.xml"+" "+applayerDir+"classes/applayer-data-layer.xml.bak"
				);
			tasks.add(backUpTask);
		}
	}

}
