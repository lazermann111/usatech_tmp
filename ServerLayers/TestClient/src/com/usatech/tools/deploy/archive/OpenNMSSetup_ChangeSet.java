package com.usatech.tools.deploy.archive;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.IPTablesUpdateTask;
import com.usatech.tools.deploy.OptionalTask;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.UploadTask;

public class OpenNMSSetup_ChangeSet implements ChangeSet {

	@Override
	public String getName() {
		return "Open NMS Install";
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("MST");
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		List<DeployTask> tasks = new ArrayList<DeployTask>();
		List<String> commands = new ArrayList<String>();
		commands.add("sudo su");
		commands.add("rpm -Uvh http://yum.opennms.org/repofiles/opennms-repo-stable-rhel6.noarch.rpm");
		commands.add("yum -y install yum-tsflags");
		// "/opt/opennms /var/opennms /var/log/opennms"
		commands.add("yum -y --tsflags='justdb' install opennms | grep -v '#'");
		commands.add("yum -y install jicmp6 | grep -v '#'");
		commands.add("yum -y install jicmp | grep -v '#'");
		commands.add("OPEN_NMS_VERSION=`yum -v info opennms | grep 'Version     :' | cut -c15-`");
		commands.add("rpm -iv --relocate /opt/opennms=/usr/opennms-$OPEN_NMS_VERSION --relocate /var/opennms=/opt/USAT/opennms/conf --relocate /var/log/opennms=/opt/USAT/opennms/logs `find /var/cache/yum/ -name opennms-core-$OPEN_NMS_VERSION-*.rpm`");
		commands.add("rpm -iv --relocate /opt/opennms=/usr/opennms-$OPEN_NMS_VERSION --relocate /var/opennms=/opt/USAT/opennms/conf --relocate /var/log/opennms=/opt/USAT/opennms/logs `find /var/cache/yum/ -name opennms-webapp-jetty-$OPEN_NMS_VERSION-*.rpm`");
		commands.add("rpm -iv --relocate /opt/opennms=/usr/opennms-$OPEN_NMS_VERSION --relocate /var/opennms=/opt/USAT/opennms/conf --relocate /var/log/opennms=/opt/USAT/opennms/logs `find /var/cache/yum/ -name opennms-$OPEN_NMS_VERSION-*.rpm`");
		commands.add("ln -s /usr/opennms-$OPEN_NMS_VERSION /usr/opennms-latest");
		commands.add("find /usr/opennms-latest/ | xargs grep -l /opt/opennms/logs | xargs sed -i 's#/opt/opennms/logs#/opt/USAT/opennms/logs#g'");
		commands.add("find /usr/opennms-latest/ | xargs grep -l /opt/opennms/share | xargs sed -i 's#/opt/opennms/share#/opt/USAT/opennms/conf#g'");
		commands.add("find /usr/opennms-latest/ | xargs grep -l /opt/opennms | xargs sed -i 's#/opt/opennms#/usr/opennms-latest#g'");
		commands.add("/usr/opennms-latest/bin/runjava -S /usr/jdk/latest/bin/java");
		commands.add("export ADDITIONAL_MANAGER_OPTIONS='-Dinstall.dir=/usr/opennms-latest -Dinstall.etc.dir=/usr/opennms-latest/etc'");
		// commands.add("/usr/opennms-latest/bin/install -disQ -P " + USERPASSWORD + " -a admin_1 -A " + ADMINPASSWORD);
		// TODO: update opennms source to allow creation of opennms database and objects in a better way
		
		commands.add("yum -v --disablerepo=* --enablerepo=pgdg92 install postgresql92-devel");
		commands.add("tar xzf iplike-2.0.3.tar.gz");
		commands.add("cd iplike-2.0.3");
		commands.add("./configure --with-pgsql=/usr/pgsql-latest/bin/pg_config");
		commands.add("make");
		commands.add("make install");
		commands.add("/usr/sbin/install_iplike.sh -c /usr/pgsql-latest/bin/psql -x '' -s postgres -l /usr/pgsql-latest/lib");

		tasks.add(new OptionalTask("if [ -x /usr/opennms-latest/bin/opennms ]; then echo 'YES'; else echo 'NO'; fi", Pattern.compile("YES"), true,
				new UploadTask(new URL("http://downloads.sourceforge.net/project/opennms/IPLIKE/stable-2.0/iplike-2.0.3.tar.gz?r=http%3A%2F%2Fsourceforge.net%2Fprojects%2Fopennms%2Ffiles%2FIPLIKE%2Fstable-2.0%2F&ts=1352746594&use_mirror=iweb"), null, 0644, "root", "root", false),
				new UploadTask(new URL("http://ftp.postgresql.org/pub/source/v9.2.1/postgresql-9.2.1.tar.gz"), null, 0644, "root", "root", false),
				new ExecuteTask(commands.toArray(new String[commands.size()]))));

		commands.clear();
		
		// TODO: create opennms program in monit
		IPTablesUpdateTask iptTask = new IPTablesUpdateTask();
		iptTask.allowTcpIn("10.0.0.110", 8980);
		tasks.add(iptTask);
		return tasks.toArray(new DeployTask[tasks.size()]);
	}

	@Override
	public String toString() {
		return getName();
	}
}
