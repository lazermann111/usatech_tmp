package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.APP_LAYER;
import static com.usatech.tools.deploy.USATRegistry.INAUTH_LAYER;
import static com.usatech.tools.deploy.USATRegistry.NET_LAYER;
import static com.usatech.tools.deploy.USATRegistry.OUTAUTH_LAYER;
import static com.usatech.tools.deploy.USATRegistry.POSM_LAYER;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.AppSettingUpdateFileTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.UsatSettingUpdateFileTask;

public class R29_Linux_ChangeSet extends AbstractLinuxChangeSet {
	public R29_Linux_ChangeSet() throws UnknownHostException {
		super();
		APP_LAYER.setVersion("1.15.0");
		POSM_LAYER.setVersion("1.6.0");
		INAUTH_LAYER.setVersion("1.15.0");
		NET_LAYER.setVersion("1.15.0");
		OUTAUTH_LAYER.setVersion("1.15.0");
	}

	@Override
	protected void registerApps() {
		registerApp(APP_LAYER);
		registerApp(INAUTH_LAYER);
		registerApp(POSM_LAYER);
		registerApp(NET_LAYER);
		registerApp(OUTAUTH_LAYER);
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(host.isServerType("NET") || host.isServerType("APR")) {
			addPrepareHostTasks(host, tasks, cache);
			UsatSettingUpdateFileTask usatSettingTask = new UsatSettingUpdateFileTask(registry.getServerSet(host.getServerEnv()));
			/*
			usatSettingTask.registerValue("USAT.fileRepository.hostPrefix.layers", new HostSpecificValue() {
				@Override
				public boolean isOverriding() {
					return true;
				}
				
				@Override
				public String getValue(Host host, Interaction interaction) {
					StringBuilder sb = new StringBuilder();
					sb.append(host.getServerEnv().toLowerCase()).append("mst");
					if(ConvertUtils.getBooleanSafely(interaction.readLine("Use new MST servers? (Y/N)"), false))
						sb.append("1");
					else
						sb.append("0");
					return sb.toString();
				}
			});*/
			tasks.add(usatSettingTask);
		}
	}
	
	@Override
	protected void addTasks(Host host, App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache);
		tasks.add(new AppSettingUpdateFileTask(app, ordinal, instanceNum, instanceCount, cache, registry.getServerSet(host.getServerEnv())));
		addInstallAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache, commands);
	}

	protected void addBeforeRestartCommands(Host host, App app, Integer ordinal, int instanceNum, List<String> commands, List<DeployTask> tasks) {
		/*
		if(app.getName().equals("applayer")) {
			commands.add("/bin/rm -r /opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal) + "/db/AppLayerDBOld");
			commands.add("/bin/mv /opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal) + "/db/AppLayerDB /opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal) + "/db/AppLayerDBOld");
		}
		//*/
	}
	@Override
	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return super.enableAppIfDisabled(host, app, ordinal) && !app.getName().equals("applayer");
	}

	@Override
	public String getName() {
		return "Edge Server - R29 Install - Apps";
	}
}
