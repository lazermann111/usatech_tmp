package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.POSM_LAYER;

import java.io.File;
import java.net.UnknownHostException;

import com.usatech.posm.tasks.POSMSettleUpdateTask;
import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;

public class Patch_R29D_ChangeSet extends AbstractLinuxPatchChangeSet {
	protected String baseDir;

	public Patch_R29D_ChangeSet() throws UnknownHostException {
		super();
		baseDir = System.getProperty("rootSourceDir");
		// File simpleBinDir = new File(baseDir + "/Simple1.5/bin");
		// registerClass(simpleBinDir, SingleStashResource.class);
		File posmlayerBinDir = new File(baseDir + "/POSMLayer/bin");
		registerClass(posmlayerBinDir, POSMSettleUpdateTask.class);
		// registerClass(posmlayerBinDir, POSMUtils.class);
		// registerResource(posmlayerBinDir, "posm-data-layer.xml");
		// File applayerBinDir = new File(baseDir + "/AppLayer/bin");
		// registerClass(applayerBinDir, InboundFileTransferTask.class);
		// File layersCommonBinDir = new File(baseDir + "/LayersCommon/bin");
		// registerClass(layersCommonBinDir, LoadDataAttrEnum.class);
	}
	protected void registerApps() {
		registerApp(POSM_LAYER);
	}
	@Override
	public String getName() {
		return "Edge Server - R29D Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
