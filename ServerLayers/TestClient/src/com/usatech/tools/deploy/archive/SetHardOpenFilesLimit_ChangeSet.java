package com.usatech.tools.deploy.archive;

import java.io.IOException;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;


public class SetHardOpenFilesLimit_ChangeSet implements ChangeSet {

	@Override
	public String getName() {
		return "Set Hard Open Files Limit";
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("APR") || host.isServerType("NET");
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		return new DeployTask[] {
			new ExecuteTask("sudo su", 
					"if [ `tail -1 /etc/security/limits.conf | grep -c '\\S'` -gt 0 ]; then echo '' >> /etc/security/limits.conf; fi", 
					"/bin/cp /etc/security/limits.conf /etc/security/limits.conf.bak", 
					"sed 's/@usat *soft *nofile *20000/@usat        -       nofile          20000/g' /etc/security/limits.conf.bak > /etc/security/limits.conf")
		};
	}

	@Override
	public String toString() {
		return getName();
	}
}
