package com.usatech.tools.deploy.archive;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.posm.schedule.SubmerchantsUploadJob;
import com.usatech.tools.deploy.*;
import com.usatech.report.build.BuildActivityDetailExtFolio;

public class R47_Patch_5_ChangeSet extends MultiLinuxPatchChangeSet {
	public R47_Patch_5_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/POSMLayer/src/main", SubmerchantsUploadJob.class, "REL_posmlayer_1_24_5", USATRegistry.POSM_LAYER);
		registerSource("usalive/src", BuildActivityDetailExtFolio.class, "REL_USALive_1_23_5", USATRegistry.USALIVE_APP);
		registerResource("usalive/src/com/usatech/usalive/report/BuildReportPillars.properties", "classes/com/usatech/usalive/report", "REL_USALive_1_23_5", false, USATRegistry.USALIVE_APP);
	}
	
	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(app.equals(USATRegistry.POSM_LAYER)) {
			PropertiesUpdateFileTask puft = new PropertiesUpdateFileTask(true, 0640, app.getUserName(), app.getUserName());
			puft.setFilePath("/opt/USAT/" + app.getName() + (instance == null ? "" : instance) + "/specific/USAT_app_settings.properties");
			
			puft.registerValue("USAT.authoritylayer.tandem.submerchants.pid", new ServerEnvHostSpecificValue("251341", "251341", "251341", "251341"));
			puft.registerValue("USAT.authoritylayer.tandem.submerchants.pidPassword", new ServerEnvHostSpecificValue("USAMATCH", "USAMATCH", "USAMATCH", "USAMATCH"));
			puft.registerValue("USAT.authoritylayer.tandem.submerchants.sid", new ServerEnvHostSpecificValue("251341", "251341", "251341", "251341"));
			puft.registerValue("USAT.authoritylayer.tandem.submerchants.sidPassword", new ServerEnvHostSpecificValue("TPPDATA", "TPPDATA", "TPPDATA", "TPPDATA"));
			puft.registerValue("USAT.authoritylayer.tandem.submerchants.host", new DefaultHostSpecificValue("${USAT.authoritylayer.tandem.sftp.host}"));
			puft.registerValue("USAT.authoritylayer.tandem.submerchants.port", new DefaultHostSpecificValue("${USAT.authoritylayer.tandem.sftp.port}"));
			puft.registerValue("USAT.authoritylayer.tandem.submerchants.username", new ServerEnvHostSpecificValue("usamtcht", "usamtcht", "usamtcht", "usamtchp"));
			puft.registerValue("USAT.authoritylayer.tandem.submerchants.password", new ServerEnvHostSpecificValue("aixrn25j", "aixrn25j", "aixrn25j", new GetPasswordHostSpecificValue(cache, "tandem.sftp.submerchants", "Chase Paymentech SubMerchants Upload SFTP", true)));
			puft.registerValue("USAT.authoritylayer.tandem.submerchants.upload.directory", new ServerEnvHostSpecificValue("/test/data/${USAT.authoritylayer.tandem.submerchants.pid}", "/test/data/${USAT.authoritylayer.tandem.submerchants.pid}", "/test/data/${USAT.authoritylayer.tandem.submerchants.pid}", "/home/${USAT.authoritylayer.tandem.submerchants.pid}"));

			tasks.add(puft);
		}
		super.addRestartAppCommands(host, app, instance, instanceNum, instanceCount, tasks, commands, cache);
	}

	@Override
	public String getName() {
		return "R47 Patch 5";
	}
}
