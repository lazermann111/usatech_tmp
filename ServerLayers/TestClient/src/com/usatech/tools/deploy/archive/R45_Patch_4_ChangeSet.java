package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.report.GenerateFolioReportTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R45_Patch_4_ChangeSet extends MultiLinuxPatchChangeSet {
	public R45_Patch_4_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ReportGenerator/src", GenerateFolioReportTask.class, "REL_report_generator_3_19_2", USATRegistry.RPTGEN_LAYER);
	}

	@Override
	public String getName() {
		return "R45 Patch 4";
	}
}
