package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.APP_LAYER;
import static com.usatech.tools.deploy.USATRegistry.INAUTH_LAYER;
import static com.usatech.tools.deploy.USATRegistry.KEYMGR;
import static com.usatech.tools.deploy.USATRegistry.LOADER;
import static com.usatech.tools.deploy.USATRegistry.NET_LAYER;
import static com.usatech.tools.deploy.USATRegistry.OUTAUTH_LAYER;
import static com.usatech.tools.deploy.USATRegistry.POSM_LAYER;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.AppSettingUpdateFileTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.tools.deploy.UnixUploadTask;
import com.usatech.tools.deploy.UsatSettingUpdateFileTask;

public class R31_Linux_ChangeSet extends AbstractLinuxChangeSet {
	public R31_Linux_ChangeSet() throws UnknownHostException {
		super();
	}

	@Override
	protected void registerApps() {
		registerApp(LOADER, "1.17.0");
		registerApp(APP_LAYER, "1.17.0");
		registerApp(INAUTH_LAYER, "1.17.0");
		registerApp(POSM_LAYER, "1.8.0");
		registerApp(NET_LAYER, "1.17.0");
		registerApp(OUTAUTH_LAYER, "1.17.0");
		registerApp(KEYMGR, "2.1.0");
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(host.isServerType("NET") || host.isServerType("APR") || host.isServerType("KLS")) {
			addPrepareHostTasks(host, tasks, cache);
			UsatSettingUpdateFileTask usatSettingTask = new UsatSettingUpdateFileTask(registry.getServerSet(host.getServerEnv()));
			tasks.add(usatSettingTask);
		}
		if(host.isServerType("KLS")) {
			String content = "INSERT INTO ROLE VALUES('Allow client to access interface via SOAP','SOAP_PROCESSING',7)\nINSERT INTO ROLE VALUES('Allow client to access interface via Hessian','HESSIAN_PROCESSING',8)\nINSERT INTO ROLE VALUES('Allow client to store data','STORE_DATA',9)\nINSERT INTO ROLE VALUES('Allow client to retrieve data','RETRIEVE_DATA',10)";
			tasks.add(new UnixUploadTask(new ByteArrayInputStream(content.getBytes()), "km_add_data_R31.sql", 0640, null, null, true, "km_add_data_R31.sql"));
		}
	}
	
	@Override
	protected void addTasks(Host host, final App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(!"postgres".equalsIgnoreCase(app.getName())) {
			addPrepareAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache);
			tasks.add(new AppSettingUpdateFileTask(app, ordinal, instanceNum, instanceCount, cache, registry.getServerSet(host.getServerEnv()), false));
			addInstallAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache, commands);
		}
		tasks.add(new ExecuteTask("sudo su", "ln -sf /usr/monit-latest/bin/monit /bin/monit"));
	}

	protected void addBeforeRestartCommands(Host host, App app, Integer ordinal, int instanceNum, List<String> commands, List<DeployTask> tasks) {
		StringBuilder sb = new StringBuilder();
		if(USATRegistry.APP_LAYER.equals(app)) {
			sb.append("/bin/rm -r /opt/USAT/").append(app.getName());
			if(ordinal != null)
				sb.append(ordinal);
			sb.append("/db/AppLayerDBOld");
			commands.add(sb.toString());
			sb.setLength(0);
			sb.append("/bin/mv /opt/USAT/").append(app.getName());
			if(ordinal != null)
				sb.append(ordinal);
			sb.append("/db/AppLayerDB /opt/USAT/").append(app.getName());
			if(ordinal != null)
				sb.append(ordinal);
			sb.append("/db/AppLayerDBOld");
			commands.add(sb.toString());
			sb.setLength(0);
			sb.append("OLD_DATE=`date +%Y%m%d-%H%M%S` && /bin/cp /opt/USAT/").append(app.getName());
			if(ordinal != null)
				sb.append(ordinal);
			sb.append("/db/derby.properties /opt/USAT/").append(app.getName());
			if(ordinal != null)
				sb.append(ordinal);
			sb.append("/db/derby.properties.$OLD_DATE && sed 's/derby\\.user\\.APP_LAYER=.*/derby.user.APP_LAYER='`grep USAT.database.EMBED.password.APP_LAYER /opt/USAT/").append(app.getName());
			if(ordinal != null)
				sb.append(ordinal);
			sb.append("/specific/USAT_app_settings.properties | cut -d= -f2`'/\ns/derby\\.user\\.APP_CLUSTER=.*/derby.user.APP_CLUSTER='`grep USAT.database.EMBED.password.APP_CLUSTER /opt/USAT/").append(app.getName());
			if(ordinal != null)
				sb.append(ordinal);
			sb.append("/specific/USAT_app_settings.properties | cut -d= -f2`'/' /opt/USAT/").append(app.getName());
			if(ordinal != null)
				sb.append(ordinal);
			sb.append("/db/derby.properties.$OLD_DATE > /opt/USAT/").append(app.getName());
			if(ordinal != null)
				sb.append(ordinal);
			sb.append("/db/derby.properties");
			commands.add(sb.toString());
		} else if(USATRegistry.KEYMGR.equals(app)) {
			commands.add("if [ `grep -c 'SOAP_PROCESSING' /opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal) + "/keydb/keymanager.script` -eq 0 ]; then /bin/cp -n /opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal) + "/keydb/keymanager.script /opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal)
					+ "/keydb/keymanager.script.`date +%Y%m%d-%H%M%S` && cat /home/`logname`/km_add_data_R31.sql >> /opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal) + "/keydb/keymanager.script; fi");
		}
	}

	@Override
	public String getName() {
		return "Edge Server - R31 Install - Apps";
	}
}
