package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.report.GenerateFolioReportTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R45_Patch_10_ChangeSet extends MultiLinuxPatchChangeSet {
	public R45_Patch_10_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ReportGenerator/src", GenerateFolioReportTask.class, "REL_report_generator_3_19_10", USATRegistry.RPTGEN_LAYER);
		registerResource("usalive/xsl/ccs-data-layer.xml", "classes", "REL_USALive_1_21_10", false, USATRegistry.USALIVE_APP);
	}

	@Override
	public String getName() {
		return "R45 Patch 10";
	}
}
