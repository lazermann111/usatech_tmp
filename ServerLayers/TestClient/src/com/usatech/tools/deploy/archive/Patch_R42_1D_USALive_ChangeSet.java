package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.usalive.web.OfferUtils;

public class Patch_R42_1D_USALive_ChangeSet extends AbstractLinuxPatchChangeSet {
	public Patch_R42_1D_USALive_ChangeSet() throws UnknownHostException {
		super();
		
		registerResource("usalive/xsl/other-data-layer.xml", "classes/", "REL_R42_1D");
		registerResource("usalive/xsl/usalive-data-layer.xml", "classes/", "REL_R42_1D");
		
		registerSource("usalive/src", OfferUtils.class, "REL_R42_1D");
		
		registerResource("usalive/web/offer_signups.html.jsp", "web/", "REL_R42_1D");
		
		registerResource("usalive/web/eport_mobile_signup.html.jsp", "web/", "REL_R42_1D");
		registerResource("usalive/web/eport_online_eport_mobile_offer.html.jsp", "web/", "REL_R42_1D");
		registerResource("usalive/web/eport_online_eport_mobile_signup.html.jsp", "web/", "REL_R42_1D");
	}
	protected void registerApps() {
		registerApp(USATRegistry.USALIVE_APP);
	}
	@Override
	public String getName() {
		return "Edge Server - R42.1D USALive Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
