package com.usatech.tools.deploy.archive;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.usatech.report.build.*;
import com.usatech.tools.deploy.AbstractSolarisPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.report.build.BuildActivityDetailExtFolio;
import com.usatech.report.build.BuildActivityDetailFolio;
import com.usatech.report.build.BuildActivityGraphFolio;

public class USALive_161_C_ChangeSet extends AbstractSolarisPatchChangeSet {
	public USALive_161_C_ChangeSet() {
		super();
		File usaliveBinDir = new File(baseDir, "usalive\\bin");
		registerClass(usaliveBinDir, AbstractBuildActivityFolio.class);
		registerClass(usaliveBinDir, BuildActivityDetailExtFolio.class);
		registerClass(usaliveBinDir, BuildActivityDetailFolio.class);
		registerClass(usaliveBinDir, BuildActivityExtFolio.class);
		registerClass(usaliveBinDir, BuildActivityFolio.class);
		
		registerClass(usaliveBinDir, BuildActivityGraphFolio.class);
		registerClass(usaliveBinDir, BuildActivitySummaryFolio.class);
	}
	@Override
	protected void addTasks(Host host, App app, Integer ordinal, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		commands.add("sudo su");
		commands.add("cp /home/dkouznet/files/usalive/activity_parameters.xsl.old /opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal.toString()) + "/classes/templates/report/activity_parameters.xsl");
		commands.add("cp /home/dkouznet/files/usalive/parameters_base.xsl.old /opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal.toString()) + "/classes/templates/report/parameters_base.xsl");

		super.addTasks(host, app, ordinal, tasks, commands, cache);
	}
	protected void registerApps() {
		registerApp(USALIVE_APP);
	}
	@Override
	protected void registerOrds() {
		Integer[] ords = new Integer[]{null};
		ordMap.put("DEV", ords);
		ordMap.put("INT", ords);
		ordMap.put("ECC", ords);
		ordMap.put("USA", new Integer[]{null});
	}
	@Override
	public String getName() {
		return "USALive 1.6.1 C Patch";
	}
}
