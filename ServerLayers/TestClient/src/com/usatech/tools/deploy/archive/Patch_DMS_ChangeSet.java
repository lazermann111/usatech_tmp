package com.usatech.tools.deploy.archive;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.dms.device.ReorderStep;
import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;

public class Patch_DMS_ChangeSet extends AbstractLinuxPatchChangeSet {
	protected String baseDir;
	
	public Patch_DMS_ChangeSet() throws UnknownHostException {
		super();
		baseDir = System.getProperty("rootSourceDir");
		/*
		baseDir = System.getProperty("rootSourceDir");
		File dmsBinDir = new File(baseDir + "/DMS/bin");
		registerClass(dmsBinDir, BulkConfigWizard2Step.class);
		*/
		// registerResource("DMS/resources/datalayers/device-data-layer.xml", "classes", "REL_DMS_1_2_0");
		// registerResource("DMS/resources/jsp/devices/configuration/bulkConfigWizard1.jsp", "web/jsp/devices/configuration", "REL_DMS_1_0_9L");
		registerSource(new File(baseDir, "DMS/src"), ReorderStep.class);
	}

	@Override
	public boolean isApplicable(Host host) {
		return true;
	}
	protected void registerApps() {
		registerApp(USATRegistry.DMS_APP);
	}
	@Override
	public String getName() {
		return "DMS Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}

	@Override
	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		super.addRestartAppCommands(host, app, instance, instanceNum, instanceCount, tasks, commands, cache);
	}
}
