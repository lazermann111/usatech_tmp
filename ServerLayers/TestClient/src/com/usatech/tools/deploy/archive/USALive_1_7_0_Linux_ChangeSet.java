package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.ENERGYMISERS_WEBSITE_APP;
import static com.usatech.tools.deploy.USATRegistry.ESUDS_APP;
import static com.usatech.tools.deploy.USATRegistry.HOTCHOICE_APP;
import static com.usatech.tools.deploy.USATRegistry.HTTPD_WEB;
import static com.usatech.tools.deploy.USATRegistry.POSTGRES_MSR;
import static com.usatech.tools.deploy.USATRegistry.RPTGEN_LAYER;
import static com.usatech.tools.deploy.USATRegistry.RPTREQ_LAYER;
import static com.usatech.tools.deploy.USATRegistry.TRANSPORT_LAYER;
import static com.usatech.tools.deploy.USATRegistry.USALIVE_APP;
import static com.usatech.tools.deploy.USATRegistry.USATECH_WEBSITE_APP;
import static com.usatech.tools.deploy.USATRegistry.VERIZON_APP;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.AppSettingUpdateFileTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.UsatSettingUpdateFileTask;

public class USALive_1_7_0_Linux_ChangeSet extends AbstractLinuxChangeSet {
	public USALive_1_7_0_Linux_ChangeSet() throws UnknownHostException {
		super();
		RPTREQ_LAYER.setVersion("3.6.0");
		RPTGEN_LAYER.setVersion("3.6.0");
		TRANSPORT_LAYER.setVersion("3.6.0");
		USALIVE_APP.setVersion("1.7.0");
		ESUDS_APP.setVersion("1.2.1");
		USATECH_WEBSITE_APP.setVersion("1.2.0");
		ENERGYMISERS_WEBSITE_APP.setVersion("1.1.0");
		HOTCHOICE_APP.setVersion("1.7.0");
		VERIZON_APP.setVersion("1.7.0");
		HTTPD_WEB.setVersion("2.2.22");
	}

	@Override
	protected void registerApps() {
		registerApp(RPTGEN_LAYER);
		registerApp(TRANSPORT_LAYER);
		registerApp(RPTREQ_LAYER);
		registerApp(POSTGRES_MSR);
		registerApp(USALIVE_APP);
		registerApp(ESUDS_APP);
		registerApp(HTTPD_WEB);
	}
		
	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("APP")||host.isServerType("MSR")||host.isServerType("WEB");
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareHostTasks(host, tasks, cache);
		if(host.isServerType("WEB") || host.isServerType("APP")) {
			tasks.add(new UsatSettingUpdateFileTask(registry.getServerSet(host.getServerEnv())));
		}
	}
	
	@Override
	protected void addTasks(Host host, App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache);
		if(app.getInstallFileName() != null) {
			tasks.add(new AppSettingUpdateFileTask(app, ordinal, instanceNum, instanceCount, cache, registry.getServerSet(host.getServerEnv())));
		}
		addInstallAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache, commands);
	}

	protected void addBeforeRestartCommands(Host host, App app, Integer ordinal, int instanceNum, List<String> commands, List<DeployTask> tasks) {
	}

	@Override
	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return !RPTREQ_LAYER.getName().equalsIgnoreCase(app.getName()) && super.enableAppIfDisabled(host, app, ordinal);
	}
	@Override
	public String getName() {
		return "USALive 1.7.0 / Report Generator 3.6.0 Install";
	}
}
