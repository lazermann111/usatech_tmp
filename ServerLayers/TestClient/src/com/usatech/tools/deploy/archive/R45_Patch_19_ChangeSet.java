package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.authoritylayer.AprivaAuthorityTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R45_Patch_19_ChangeSet extends MultiLinuxPatchChangeSet {
	public R45_Patch_19_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/AuthorityLayer/src", AprivaAuthorityTask.class, "REL_authoritylayer_1_31_19", USATRegistry.OUTAUTH_LAYER);
	}

	@Override
	public String getName() {
		return "R45 Patch 19";
	}
}
