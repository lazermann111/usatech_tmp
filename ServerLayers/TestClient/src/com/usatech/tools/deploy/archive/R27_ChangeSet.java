package com.usatech.tools.deploy.archive;

import java.io.File;
import java.io.IOException;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.UploadAndRunPostgresSQLTask;
import com.usatech.tools.deploy.UploadTask;

public class R27_ChangeSet implements ChangeSet {

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		if(host.isServerType("MST")) {
			if("LOCAL".equalsIgnoreCase(host.getServerEnv())) {
				String[] databases = {"bkmq1", "bkmq2", "dkmq1", "dkmq2" };
				DeployTask[] tasks = new DeployTask[databases.length*2];
				for(int i = 0; i < databases.length; i++) {
					tasks[i*2] = new UploadAndRunPostgresSQLTask(new File("D:\\Development\\Database Projects\\DatabaseScripts\\edge_implementation\\R27.MQ.1.sql"), false, databases[i]);
					tasks[i*2+1] = new ExecuteTask("sudo su", "su - postgres", "bash",
							"/opt/USAT/postgres/latest/bin/64/psql " + databases[i] + " -c \"SELECT 'ALTER TABLE MQ.'|| T.TABLENAME || ' OWNER TO use_mq; ' FROM MQ.QUEUE Q JOIN PG_TABLES T ON T.SCHEMANAME = 'mq' AND UPPER(T.TABLENAME) IN(Q.QUEUE_TABLE, 'I'||Q.QUEUE_TABLE) WHERE T.TABLEOWNER != 'use_mq' ORDER BY q.queue_table;\" -t -o /home/postgres/change_q_owner.sql",
							"/opt/USAT/postgres/latest/bin/64/psql " + databases[i] + " -f /home/postgres/change_q_owner.sql");					
				}
				return tasks;
			} else
				return new DeployTask[] {
					new UploadAndRunPostgresSQLTask(new File("D:\\Development\\Database Projects\\DatabaseScripts\\edge_implementation\\R27.MQ.1.sql"), false, "mq"),	
					new ExecuteTask("sudo su", "su - postgres", "bash",
						"/opt/USAT/postgres/latest/bin/64/psql mq -c \"SELECT 'ALTER TABLE MQ.'|| T.TABLENAME || ' OWNER TO use_mq; ' FROM MQ.QUEUE Q JOIN PG_TABLES T ON T.SCHEMANAME = 'mq' AND UPPER(T.TABLENAME) IN(Q.QUEUE_TABLE, 'I'||Q.QUEUE_TABLE) WHERE T.TABLEOWNER != 'use_mq' ORDER BY q.queue_table;\" -t -o /home/postgres/change_q_owner.sql",
						"/opt/USAT/postgres/latest/bin/64/psql mq -f /home/postgres/change_q_owner.sql"),	
				};			
		} else if(host.isServerType("APR") || host.isServerType("NET")) {
			return new DeployTask[] {
					new UploadTask(new File(System.getProperty("rootSourceDir", "D:\\Development\\Java Projects") + "\\ApplicationBuilds\\Scripts\\edge-r27-install.sh"), 0744),
			};
		}
		return null;
	}

	@Override
	public String getName() {
		return "Edge Server - R27 Install";
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("MST") || host.isServerType("APR") || host.isServerType("NET");
	}
	@Override
	public String toString() {
		return getName();
	}
}
