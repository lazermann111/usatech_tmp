package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.transport.SendEmailTask;

public class Prepaid_Patch_Loader_ChangeSet extends AbstractLinuxPatchChangeSet {
	public Prepaid_Patch_Loader_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/AppLayer/src", SendEmailTask.class, "REL_prepaid_1_1_0");
	}
	protected void registerApps() {
		registerApp(USATRegistry.LOADER);
	}
	@Override
	public String getName() {
		return "Prepaid - Loader - Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
