package com.usatech.tools.deploy.archive;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;

public class Patch_POSMLayer_ChangeSet extends AbstractLinuxPatchChangeSet {

	public Patch_POSMLayer_ChangeSet() throws UnknownHostException {
		super();
		// registerSource("ServerLayers/POSMLayer/src/main", UnprocessedAccountCheck.class, "HEAD");
		// registerResource("ServerLayers/POSMLayer/conf/POSMLayerService.properties", "classes/", "HEAD");
		registerResource("ServerLayers/POSMLayer/conf/posm-data-layer.xml", "classes/", "HEAD");
	}


	@Override
	public boolean isApplicable(Host host) {
		return true;
	}
	protected void registerApps() {
		registerApp(USATRegistry.POSM_LAYER);
	}
	@Override
	public String getName() {
		return "POSMLayer Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}

	// *
	@Override
	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
	}
	// */
}
