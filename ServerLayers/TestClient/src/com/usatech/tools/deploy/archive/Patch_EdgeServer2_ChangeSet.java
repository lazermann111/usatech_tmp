package com.usatech.tools.deploy.archive;

import java.io.File;
import java.net.UnknownHostException;

import com.usatech.applayer.SessionControlTask;
import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.USATRegistry;

public class Patch_EdgeServer2_ChangeSet extends AbstractLinuxPatchChangeSet {
	protected String baseDir;

	public Patch_EdgeServer2_ChangeSet() throws UnknownHostException {
		super();
		baseDir = System.getProperty("rootSourceDir");
		// File simpleBinDir = new File(baseDir + "/Simple1.5/bin");
		// registerClass(simpleBinDir, JDK14toSimpleHandler.class);
		// File posmlayerBinDir = new File(baseDir + "/POSMLayer/bin");
		//registerClass(posmlayerBinDir, POSMUtils.class);
		// registerClass(posmlayerBinDir, POSMSettlementFeedbackTask.class);
		// registerResource(posmlayerBinDir, "posm-data-layer.xml");
		// registerClass(posmlayerBinDir, POSMCheckTask.class);
		// registerClass(posmlayerBinDir, POSMNextTask.class);
		// File layersCommonBinDir = new File(baseDir + "/LayersCommon/bin");
		// registerClass(layersCommonBinDir, AppLayerUtils.class);
		// File layersCommonBinDir = new File(baseDir + "/LayersCommon/bin");
		// registerClass(layersCommonBinDir, LoadDataAttrEnum.class);
		// registerClass(simpleBinDir, AbstractServiceManager.class);
		// registerClass(simpleBinDir, Base.class);
		// registerClass(simpleBinDir, BaseWithConfig.class);
		// registerClass(simpleBinDir, ServiceManager.class);
		// registerClass(simpleBinDir, ServiceManagerWithConfig.class);
		// File authlayerBinDir = new File(baseDir + "/AuthorityLayer/bin");
		// File authlayerBinSrc = new File(baseDir + "/AuthorityLayer/src");
		// registerResource("ServerLayers/AuthorityLayer/src/authority-data-layer.xml", "classes/", "HEAD");
		// registerResource(authlayerBinDir, "OutsideAuthorityLayerService.properties");
		File applayerBinDir = new File(baseDir + "/AppLayer/bin");
		// registerClass(applayerBinDir, EmbeddedDbService.class);
		// registerClass(applayerBinDir, AuthorizeTask.class);
		// registerClass(layersCommonBinDir, AlertingUtils.class);
		// registerClass(layersCommonBinDir, AppLayerUtils.class);

		// File keymgrBinDir = new File(baseDir + "/KeyManagerService/bin");
		// registerClass(keymgrBinDir, MassDecryptTask.class);
		// registerClass(keymgrBinDir, MassRetrieveTask.class);
		// File msgBinDir = new File(baseDir + "/USATMessaging/bin");
		// registerClass(msgBinDir, MessageChainService.class);
		// registerClass(msgBinDir, MessageChainV11.class);

		// registerClass(msgBinDir, ManualQueueLayerProducer.class);

		// registerClass(simpleBinDir, Email.class);

		// registerClass(simpleBinDir, TimedByteChannel.class);
		// registerClass(simpleBinDir, DirectPoller.class);
		// registerClass(simpleBinDir, DirectPollerConfig.class);

		// registerClass(simpleBinDir, ByteChannelByteInput.class);
		// registerClass(simpleBinDir, ByteChannelByteOutput.class);

		// registerClass(simpleBinDir, NIOServer.class);
		// registerClass(simpleBinDir, DirectPeerProducer.class);
		// registerClass(simpleBinDir, TrackingJMXByteChannel.class);

		// registerResource(posmlayerBinDir, "POSMLayerService.properties");
		// registerClass(simpleBinDir, AppMonitor.class);
		/*
		File usaliveBinDir = new File(baseDir + "/usalive/bin");
		registerClass(usaliveBinDir, AbstractBuildActivityFolioStep.class);
		registerClass(usaliveBinDir, BuildActivityFolio.class);
		registerClass(usaliveBinDir, BuildActivityExtFolio.class);
		registerClass(usaliveBinDir, BuildActivityGraphFolio.class);
		registerClass(usaliveBinDir, BuildActivitySummaryFolio.class);
		registerClass(usaliveBinDir, BuildActivityDetailExtFolio.class);
		registerClass(usaliveBinDir, BuildActivityDetailFolio.class);
		*/
		registerClass(applayerBinDir, SessionControlTask.class);
		registerResource("ServerLayers/AppLayer/src/loader-data-layer.xml", "classes/", "HEAD");
	}

	/*
		@Override
		protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
			PropertiesUpdateFileTask sysctlUpdateTask = new PropertiesUpdateFileTask(true, null, null, null);
			sysctlUpdateTask.setFilePath("/etc/sysctl.conf");
			sysctlUpdateTask.registerValue("net.ipv4.tcp_keepalive_time", new DefaultHostSpecificValue(String.valueOf(5)));
			sysctlUpdateTask.registerValue("net.ipv4.tcp_keepalive_intvl", new DefaultHostSpecificValue(String.valueOf(1)));
			sysctlUpdateTask.registerValue("net.ipv4.tcp_keepalive_probes", new DefaultHostSpecificValue(String.valueOf(5)));

			tasks.add(sysctlUpdateTask);
			commands.add("sudo sysctl -e -p");
			super.addTasks(host, tasks, commands, cache);
		}
	*/
	@Override
	public boolean isApplicable(Host host) {
		return true;
	}
	protected void registerApps() {
		// registerApp(USATRegistry.LOADER);
		// registerApp(USATRegistry.APP_LAYER);
		// registerApp(USATRegistry.INAUTH_LAYER);
		// registerApp(USATRegistry.POSM_LAYER);
		// registerApp(USATRegistry.NET_LAYER);
		// registerApp(USATRegistry.OUTAUTH_LAYER);
		// registerApp(USATRegistry.KEYMGR);
		// registerApp(USATRegistry.APP_MONITOR);
		registerApp(USATRegistry.USALIVE_APP);

	}
	@Override
	public String getName() {
		return "Edge Server - Patch 2";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}

	/*
	@Override
	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
	}
	// */
}
