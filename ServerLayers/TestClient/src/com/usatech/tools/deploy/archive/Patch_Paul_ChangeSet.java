package com.usatech.tools.deploy.archive;

import java.io.File;
import java.net.UnknownHostException;

import simple.mq.peer.DirectPeerProducer;

import com.usatech.posm.utils.POSMUtils;
import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.USATRegistry;

public class Patch_Paul_ChangeSet extends AbstractLinuxPatchChangeSet {
	protected String baseDir;

	public Patch_Paul_ChangeSet() throws UnknownHostException {
		super();
		baseDir = System.getProperty("rootSourceDir");
		File posmlayerBinDir = new File(baseDir + "/AppLayer/bin");
		registerClass(posmlayerBinDir, DirectPeerProducer.class);
	}
	protected void registerApps() {
		registerApp(USATRegistry.APP_LAYER);
	}
	@Override
	public String getName() {
		return "Edge Server - Paul Test";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
