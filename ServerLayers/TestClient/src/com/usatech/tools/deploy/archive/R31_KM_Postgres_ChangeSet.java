package com.usatech.tools.deploy.archive;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.tools.deploy.UploadAndRunPostgresSQLTask;

public class R31_KM_Postgres_ChangeSet extends AbstractLinuxChangeSet {
	public R31_KM_Postgres_ChangeSet() throws UnknownHostException {
		super();
				
	}
	protected void registerApps() {
		registerApp(USATRegistry.POSTGRES_KLS);
	}

	@Override
	protected void addTasks(Host host, App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if("postgres".equalsIgnoreCase(app.getName()) && (instance == null || instanceNum == 1)) {
			String[] databases;
			if("LOCAL".equalsIgnoreCase(host.getServerEnv())) {
				databases = new String[] { "bkkm1", "bkkm2", "dkkm1", "dkkm2", "yhkm1", "yhkm2" };
			} else {
				databases = new String[] { "km" };
			}
			for(String database : databases)
				tasks.add(new UploadAndRunPostgresSQLTask("DatabaseScripts/releases/REL_Edge_Update_R31/KM_Postgres_Update_R31.sql", "HEAD", null, database));
		}
	}
	
	@Override
	public String getName() {
		return "Edge Server - R31 KM Postgres Update";
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
	}
}
