package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.RPTREQ_LAYER;

import java.io.File;
import java.net.UnknownHostException;


import com.usatech.report.RequestRetryTransportTask;
import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
/*
 * This changeset is try to distribute the retry message. change cron to fire every 5 min instead of 1 hour
 */
public class Patch_R33_ReportRequester_ChangeSet extends AbstractLinuxPatchChangeSet {
	protected String baseDir;

	public Patch_R33_ReportRequester_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ReportGenerator/src/com/usatech/report/rgr-data-layer.xml", "classes/com/usatech/report/", "HEAD");
		registerResource("ReportGenerator/src/ReportRequesterService.properties", "classes/", "HEAD");
		baseDir = System.getProperty("rootSourceDir");
		File simpleBinDir = new File(baseDir + "/ReportGenerator/bin");
		registerClass(simpleBinDir, RequestRetryTransportTask.class);
	}
	protected void registerApps() {
		registerApp(RPTREQ_LAYER);
	}
	@Override
	public String getName() {
		return "Edge Server - R33 - Improve Transport Retry Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
