package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.posm.schedule.dfr.DFRReport;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R46_Patch_13_ChangeSet extends MultiLinuxPatchChangeSet {
	public R46_Patch_13_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/POSMLayer/src/main", DFRReport.class, "BRN_R46", USATRegistry.POSM_LAYER);
	}

	@Override
	public String getName() {
		return "R46 Patch 13";
	}
}
