package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R45_Patch_16_ChangeSet extends MultiLinuxPatchChangeSet {
	public R45_Patch_16_ChangeSet() throws UnknownHostException {
		super();
		registerResource("usalive/web/rma_create_device.html.jsp", "web", "BRN_R45", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/rma_create_parts.html.jsp", "web", "BRN_R45", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/xsl/rma-data-layer.xml", "classes", "BRN_R45", false, USATRegistry.USALIVE_APP);

	}

	@Override
	public String getName() {
		return "R45 Patch 16 for USALive RMA";
	}
}
