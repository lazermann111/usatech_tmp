package com.usatech.tools.deploy.archive;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;

public class Patch_Usatech_ChangeSet extends AbstractLinuxPatchChangeSet {
	public Patch_Usatech_ChangeSet() throws UnknownHostException {
		super();
		File usatechDir = new File(USATRegistry.baseDir, "USATech Website");
		// registerSource("Falcon/src", DirectXHTMLGenerator.class, "REL_USALive_1_10_1");
		// registerResource("usalive/xsl/report-data-layer.xml", "classes", "REL_USALive_1_10_1");
		// registerResource("usalive/xsl/templates/device/terminal-customers.xsl", "classes/templates/device", "REL_USALive_1_10_1");
		// registerResource("usalive/web/css/usalive-app-style.css", "web/css", "REL_USALive_1_10_2");
		registerResource(usatechDir, "eport-mobile/index.php", "usatech");
		registerResource(usatechDir, "imgs/eportgo-header.png", "usatech");
		registerResource(usatechDir, "imgs/eportgo-button.gif", "usatech");
		registerResource(usatechDir, "imgs/eportgo-newsite.jpg", "usatech");
		registerResource(usatechDir, "imgs/eportmobile-header.gif", "usatech");
		registerResource(usatechDir, "imgs/eportmobile-button.gif", "usatech");
		registerResource(usatechDir, "imgs/eportmobile-newsite.jpg", "usatech");

	}


	@Override
	public boolean isApplicable(Host host) {
		return true;
	}
	protected void registerApps() {
		registerApp(USATRegistry.HTTPD_WEB);
	}
	@Override
	public String getName() {
		return "USATech Website Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}

	// *
	@Override
	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
	}
	// */
}
