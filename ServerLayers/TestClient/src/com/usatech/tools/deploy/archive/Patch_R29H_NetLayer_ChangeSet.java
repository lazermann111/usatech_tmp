package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.NET_LAYER;

import java.io.File;
import java.net.UnknownHostException;

import simple.io.resource.SingleStashResource;

import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;

public class Patch_R29H_NetLayer_ChangeSet extends AbstractLinuxPatchChangeSet {
	protected String baseDir;

	public Patch_R29H_NetLayer_ChangeSet() throws UnknownHostException {
		super();
		baseDir = System.getProperty("rootSourceDir");
		File simpleBinDir = new File(baseDir + "/Simple1.5/bin");
		registerClass(simpleBinDir, SingleStashResource.class);
		//File posmlayerBinDir = new File(baseDir + "/POSMLayer/bin");
		//registerClass(posmlayerBinDir, POSMUtils.class);
		//registerResource(posmlayerBinDir, "posm-data-layer.xml");
		// File applayerBinDir = new File(baseDir + "/AppLayer/bin");
		// registerResource(applayerBinDir, "loader-data-layer.xml");
		// registerClass(applayerBinDir, AppLayerUtils.class);
		// registerClass(applayerBinDir, TranImportTask.class);
		// File layersCommonBinDir = new File(baseDir + "/LayersCommon/bin");
		// registerClass(layersCommonBinDir, LoadDataAttrEnum.class);
	}
	protected void registerApps() {
		registerApp(NET_LAYER);
	}
	@Override
	public String getName() {
		return "Edge Server - R29H NetLayer Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
