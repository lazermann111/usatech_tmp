package com.usatech.tools.deploy.archive;

import java.io.File;
import java.net.UnknownHostException;

import com.usatech.networklayer.processors.ReplyOnlyProcessor_FileTransfer;
import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.USATRegistry;

public class R31H_Linux_ChangeSet extends AbstractLinuxPatchChangeSet {
	protected String baseDir;
	public R31H_Linux_ChangeSet() throws UnknownHostException {
		super();
		baseDir = System.getProperty("rootSourceDir");
		File netlayerBinDir = new File(baseDir + "/NetLayer/bin");
		registerClass(netlayerBinDir, ReplyOnlyProcessor_FileTransfer.class);
	}

	protected void registerApps() {
		registerApp(USATRegistry.NET_LAYER);
	}
	@Override
	public String getName() {
		return "Edge Server - R31H Patch - Apps";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
