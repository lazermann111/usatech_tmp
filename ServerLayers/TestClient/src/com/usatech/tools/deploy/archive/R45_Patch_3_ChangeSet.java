package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.posm.tasks.ReplenishPrepaidTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R45_Patch_3_ChangeSet extends MultiLinuxPatchChangeSet {
	public R45_Patch_3_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/POSMLayer/src/main", ReplenishPrepaidTask.class, "BRN_R45", USATRegistry.POSM_LAYER);
	}

	@Override
	public String getName() {
		return "R45 Patch #3";
	}
}
