package com.usatech.tools.deploy.archive;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import simple.falcon.xml.ReportAdapter;

import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;

public class Patch_USALive17_ChangeSet extends AbstractLinuxPatchChangeSet {
	protected String baseDir;

	public Patch_USALive17_ChangeSet() throws UnknownHostException {
		super();
		baseDir = System.getProperty("rootSourceDir");
		// /*
		// File simpleBinDir = new File(baseDir + "/Simple1.5/bin");
		// registerClass(simpleBinDir, ReflectionUtils.class);
		// registerClass(simpleBinDir, DecompressingFormat.class);
		/*
		registerClass(simpleBinDir, DirectPeerProducer.class);
		registerClass(simpleBinDir, ByteChannelByteInput.class);
		registerClass(simpleBinDir, ByteChannelByteOutput.class);
		registerClass(simpleBinDir, DirectPoller.class);
		registerClass(simpleBinDir, CopyingJMXByteChannel.class);
		registerClass(simpleBinDir, NIOServer.class);
		// */

		File falconBinDir = new File(baseDir + "/Falcon/src");
		registerClass(falconBinDir, ReportAdapter.class);
		// registerClass(falconBinDir, FOGenerator.class);

		// registerClass(falconBinDir, StandardExecuteEngine.class);
		// registerClass(falconBinDir, FilterItem.class);
		// registerClass(falconBinDir, DirectoryResourceResolver.class);
		// registerClass(falconBinDir, ReportRunner.class);
		//File posmlayerBinDir = new File(baseDir + "/POSMLayer/bin");
		//registerClass(posmlayerBinDir, POSMUtils.class);
		//registerResource(posmlayerBinDir, "posm-data-layer.xml");
		// File layersCommonBinDir = new File(baseDir + "/LayersCommon/bin");
		// registerClass(layersCommonBinDir, AppLayerUtils.class);
		// File layersCommonBinDir = new File(baseDir + "/LayersCommon/bin");
		// registerClass(layersCommonBinDir, LoadDataAttrEnum.class);
		// File usaliveCRWebDir = new File(baseDir + "/usalive/customerreporting_web");
		// registerResource(usaliveCRWebDir, "login.jsp");
		// File usaliveBinDir = new File(baseDir + "/usalive/bin");
		// registerResource(usaliveBinDir, "selection-data-layer.xml");
		// registerResource(usaliveBinDir, "report-data-layer.xml");
		// registerLib(new File(baseDir, "/ThirdPartyJavaLibraries/lib/poi-ooxml-schemas-3.7-20101029.jar"));
		// registerLib(new File(baseDir, "/ThirdPartyJavaLibraries/lib/dom4j-1.6.1.jar"));
		// registerLib(new File(baseDir, "/ThirdPartyJavaLibraries/lib/xmlbeans-2.3.0.jar"));

		// registerResource(simpleBinDir, "simple/xml/serializer/XHTMLTagFlags.properties");
		// registerClass(simpleBinDir, StringUtils.class);
		// registerClass(simpleBinDir, XSSFilterRequestWrapper.class);

		// File usaliveXslDir = new File(baseDir + "/usalive/xsl");
		// registerResource(usaliveXslDir, "templates/general/app-base.xsl");
		// registerResource(new File(baseDir + "/ReportGenerator/src"), "ReportRequesterService.properties");
	}
	protected void registerApps() {
		// registerApp(USALIVE_APP);
		// registerApp(USATRegistry.DMS_APP);
		// registerApp(USATRegistry.ESUDS_APP);
		registerApp(USATRegistry.RPTGEN_LAYER);
		// registerApp(USATRegistry.RPTREQ_LAYER);
		// registerApp(TRANSPORT_LAYER);
		registerApp(USATRegistry.USALIVE_APP);
		// USALIVE_APP.setVersion("1.7.0");
	}

	@Override
	public String getName() {
		return "USALive 1.7 / RG 3.6 Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}

	protected void addTasks(Host host, App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		/*if(commands.isEmpty())
			commands.add("sudo su");
		String propFile = "/opt/USAT/" + app.getName() + (instance == null ? "" : instance) + "/classes/" + app.getServiceName() + ".properties";
		commands.add("if ! grep -c 'simple.mq.Supervisor.defaultRedeliveryDetection=STRICT' " + propFile + "; then echo '\nsimple.mq.Supervisor.defaultRedeliveryDetection=STRICT' >> " + propFile + "; fi");
		// Integer ordinal = (instance == null ? null : Integer.parseInt(instance));
		// tasks.add(new AppSettingUpdateFileTask(app, ordinal, instanceNum, instanceCount, cache, registry.getServerSet(host.getServerEnv())));
		*/
		super.addTasks(host, app, ordinal, instance, instanceNum, instanceCount, tasks, commands, cache);
		// addInstallAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks);
		/*
		String log4jFile = "/opt/USAT/" + app.getName() + (instance == null ? "" : instance) + "/specific/log4j.properties";
		if(commands.isEmpty())
			commands.add("sudo su");
		commands.add("if ! grep -c 'log4j.logger.simple.io.ByteChannelByteOutput' " + log4jFile + "; then echo 'log4j.logger.simple.io.ByteChannelByteOutput=DEBUG' >> " + log4jFile + "; fi");
		//*/
	}

	/*
		@Override
		protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
			if(host.isServerType("WEB") || host.isServerType("APP")) {
				tasks.add(new UsatSettingUpdateFileTask(registry.getServerSet(host.getServerEnv())));
			}
		}

	// */
	/*
	@Override
	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
	// do nothing
	}
	//*/
}
