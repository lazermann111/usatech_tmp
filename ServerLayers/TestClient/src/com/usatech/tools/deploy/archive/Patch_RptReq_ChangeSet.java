package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.report.RequestRetryTransportTask;
import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.USATRegistry;

public class Patch_RptReq_ChangeSet extends AbstractLinuxPatchChangeSet {

	public Patch_RptReq_ChangeSet() throws UnknownHostException {
		super();
		// registerResource("ReportGenerator/src/com/usatech/report/rgr-data-layer.xml", "classes/com/usatech/report", "HEAD");
		registerSource("ReportGenerator/src", RequestRetryTransportTask.class, "HEAD");
	}

	@Override
	public boolean isApplicable(Host host) {
		return true;
	}
	protected void registerApps() {
		registerApp(USATRegistry.RPTREQ_LAYER);
	}
	@Override
	public String getName() {
		return "RptReq Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}

	/*
	@Override
	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
	}
	// */
}
