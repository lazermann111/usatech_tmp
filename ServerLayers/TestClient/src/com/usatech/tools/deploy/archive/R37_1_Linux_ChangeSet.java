package com.usatech.tools.deploy.archive;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;

public class R37_1_Linux_ChangeSet extends AbstractLinuxChangeSet {
	public R37_1_Linux_ChangeSet() throws UnknownHostException {
		super();
	}

	@Override
	protected void registerApps() {
		// *
		registerApp(USATRegistry.DMS_APP, "1.0.12");
		registerApp(USATRegistry.LOADER, "1.23.1");
		registerApp(USATRegistry.APP_LAYER, "1.23.1");
		registerApp(USATRegistry.NET_LAYER, "1.23.1");
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		// addPrepareHostTasks(host, tasks, cache);
		// tasks.add(new UsatSettingUpdateFileTask(registry.getServerSet(host.getServerEnv())));
		// tasks.add(new ExecuteTask("sudo find /opt/USAT/ -name '*.dmp' -delete"));
	}
	
	@Override
	protected void addTasks(Host host, final App app, Integer ordinal, String instance, final int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		// addPrepareAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache);
		addInstallAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache, commands);
	}

	@Override
	public String getName() {
		return "Edge Server - R37.1 Install - Apps";
	}
}
