package com.usatech.tools.deploy.archive;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;

public class R47_Patch_11_ChangeSet extends MultiLinuxPatchChangeSet {
	public R47_Patch_11_ChangeSet() throws UnknownHostException {
		super();
		registerResource("usalive/xsl/rma-data-layer.xml", "classes", "REL_USALive_1_24_0", false, USATRegistry.USALIVE_APP);
	}
	
	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		super.addRestartAppCommands(host, app, instance, instanceNum, instanceCount, tasks, commands, cache);
	}

	@Override
	public String getName() {
		return "R47 Patch 11";
	}
}
