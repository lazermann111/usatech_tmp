package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.authoritylayer.SproutGatewayTask;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.sprout.SproutPurchaseItem;
import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.USATRegistry;

public class Patch_R41F_OutsideAuthorityLayer_ChangeSet extends AbstractLinuxPatchChangeSet {

	public Patch_R41F_OutsideAuthorityLayer_ChangeSet() throws UnknownHostException {
		super();		
		registerSource("ServerLayers/LayersCommon/src",  AuthorityAttrEnum.class, "REL_R41F");
		registerSource("ServerLayers/AuthorityLayer/src",  SproutPurchaseItem.class, "REL_R41F");
		registerSource("ServerLayers/AuthorityLayer/src",  SproutGatewayTask.class, "REL_R41F");
	}
	protected void registerApps() {
		registerApp(USATRegistry.OUTAUTH_LAYER);
	}
	@Override
	public String getName() {
		return "Edge Server - R41F OutsideAuthorityLayer Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
