package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.NET_LAYER;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DefaultHostSpecificValue;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.PropertiesUpdateFileTask;

public class Patch_R29F_ChangeSet extends AbstractLinuxChangeSet {
	public Patch_R29F_ChangeSet() throws UnknownHostException {
		super();
	}

	@Override
	protected void registerApps() {
		registerApp(NET_LAYER);
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
	}
	
	@Override
	protected void addTasks(Host host, App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		final String appDir = "/opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal);
		PropertiesUpdateFileTask settingsFile = new PropertiesUpdateFileTask(true, 0640, app.getUserName(), app.getUserName());
		settingsFile.setFilePath(appDir + "/classes/" + app.getServiceName() + ".properties");
		settingsFile.registerValue("com.usatech.networklayer.processors.PING_AUTH.loadBalancerQueueNames",
				new DefaultHostSpecificValue("usat.inbound.message.auth,usat.authority.realtime.authority_iso8583_elavon,,usat.inbound.message.auth,usat.authority.realtime.authority_iso8583_fhms_paymentech,,usat.inbound.message.auth,usat.authority.realtime.blackboard,,usat.inbound.message.auth,usat.authority.realtime.aramark,,usat.inbound.message.auth,usat.authority.realtime.authority_iso8583_heartland,,usat.inbound.message.auth,usat.authority.realtime.posgateway_aquafill"));
		settingsFile.registerValue("com.usatech.networklayer.processors.PING_AUTH.loadBalancerQueueNames[17]", new DefaultHostSpecificValue(null));
		tasks.add(settingsFile);
		commands.add("sudo su");
		commands.add("initctl restart USAT/" + app.getName() + (ordinal == null ? "" : ordinal.toString()));
	}

	@Override
	public String getName() {
		return "Edge Server - R29F Patch";
	}
}
