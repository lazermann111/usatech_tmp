package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.POSM_LAYER;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DefaultHostSpecificValue;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.PropertiesUpdateFileTask;

public class Patch_R29E_ChangeSet extends AbstractLinuxChangeSet {
	public Patch_R29E_ChangeSet() throws UnknownHostException {
		super();
	}

	@Override
	protected void registerApps() {
		registerApp(POSM_LAYER);
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
	}
	
	@Override
	protected void addTasks(Host host, App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		final String appDir = "/opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal);
		PropertiesUpdateFileTask settingsFile = new PropertiesUpdateFileTask(true, 0640, app.getUserName(), app.getUserName());
		settingsFile.setFilePath(appDir + "/classes/POSMLayerService.properties");
		settingsFile.registerValue("simple.db.datasource.REPORT.minIdle", new DefaultHostSpecificValue(String.valueOf(15)));
		settingsFile.registerValue("simple.db.datasource.OPER.minIdle", new DefaultHostSpecificValue(String.valueOf(20)));
		settingsFile.registerValue("simple.db.datasource.OPER.connectionProperties", new DefaultHostSpecificValue("oracle.net.CONNECT_TIMEOUT=15000;oracle.net.READ_TIMEOUT=20000"));
		settingsFile.registerValue("simple.db.datasource.REPORT.connectionProperties", new DefaultHostSpecificValue("oracle.net.CONNECT_TIMEOUT=15000;oracle.net.READ_TIMEOUT=20000"));
		tasks.add(settingsFile);
		commands.add("sudo su");
		commands.add("initctl restart USAT/" + app.getName() + (ordinal == null ? "" : ordinal.toString()));
	}

	@Override
	public String getName() {
		return "Edge Server - R29E Patch";
	}
}
