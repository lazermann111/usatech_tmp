package com.usatech.tools.deploy.archive;


import java.net.UnknownHostException;


import com.usatech.report.ReportRequestFactory;
import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.USATRegistry;

public class Patch_R36_ReportRequester_ChangeSet extends AbstractLinuxPatchChangeSet {

	public Patch_R36_ReportRequester_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ReportGenerator/src", ReportRequestFactory.class, "BRN_PROD");
		registerResource("ReportGenerator/src/com/usatech/report/rgr-data-layer.xml", "classes/com/usatech/report/", "HEAD");
	}
	protected void registerApps() {
		registerApp(USATRegistry.RPTREQ_LAYER);
	}
	@Override
	public String getName() {
		return "Patch Edge Server - R36 - Report Requester Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
	
	@Override
	public boolean isApplicable(Host host) {
		return true;
	}
}
