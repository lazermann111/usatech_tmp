package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.transport.AbstractHttpTransporter;

public class R46_Patch_8_ChangeSet extends MultiLinuxPatchChangeSet {
	public R46_Patch_8_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ReportGenerator/src/", AbstractHttpTransporter.class, "BRN_R46", USATRegistry.TRANSPORT_LAYER);
		registerResource("Prepaid/src/prepaid-data-layer.xml", "classes", "BRN_R46", false, USATRegistry.PREPAID_APP);
	}

	@Override
	public String getName() {
		return "R46 Patch 8";
	}
}
