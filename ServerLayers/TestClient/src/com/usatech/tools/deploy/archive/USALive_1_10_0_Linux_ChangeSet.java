package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.HTTPD_WEB;
import static com.usatech.tools.deploy.USATRegistry.RPTREQ_LAYER;
import static com.usatech.tools.deploy.USATRegistry.USALIVE_APP;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.AppSettingUpdateFileTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.UsatSettingUpdateFileTask;

public class USALive_1_10_0_Linux_ChangeSet extends AbstractLinuxChangeSet {
	
	public USALive_1_10_0_Linux_ChangeSet() throws UnknownHostException {
		super();
	}
	
	private static final String USALIVE_APP_VERSION="1.10.0";
	private static final String RG_APP_VERSION = "3.8.0";
	
	@Override
	protected void registerApps() {
		registerApp(RPTREQ_LAYER, RG_APP_VERSION);
		registerApp(HTTPD_WEB, "2.2.23-usat-build");
		registerApp(USALIVE_APP, USALIVE_APP_VERSION);
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareHostTasks(host, tasks, cache);
		if(host.isServerType("WEB") || host.isServerType("APP")) {
			tasks.add(new UsatSettingUpdateFileTask(registry.getServerSet(host.getServerEnv())));
		}
	}
	
	@Override
	protected void addTasks(Host host, App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if("httpd".equals(app.getName())) {
			if(ordinal != null) {
				if(ordinal == 1)
					ordinal = null;
				else
					return;
			}
		}
		addPrepareAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache);
		if(app.getInstallFileName() != null && !"httpd".equals(app.getName())) {
			tasks.add(new AppSettingUpdateFileTask(app, ordinal, instanceNum, instanceCount, cache, registry.getServerSet(host.getServerEnv())));
		}
		addInstallAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache, commands);
	}

	@Override
	public String getName() {
		return "USALive 1.10.0 Install";
	}
}
