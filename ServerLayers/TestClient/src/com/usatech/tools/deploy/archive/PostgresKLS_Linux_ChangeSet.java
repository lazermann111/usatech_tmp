package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.POSTGRES_KLS;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.AppSettingUpdateFileTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.UsatSettingUpdateFileTask;

public class PostgresKLS_Linux_ChangeSet extends AbstractLinuxChangeSet {
	public PostgresKLS_Linux_ChangeSet() throws UnknownHostException {
		super();
		//KEYMGR.setVersion("2.0.0");
	}

	@Override
	protected void registerApps() {
		//registerApp(KEYMGR);
		registerApp(POSTGRES_KLS);
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("KLS");
	}
	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(host.isServerType("KLS")) {
			addPrepareHostTasks(host, tasks, cache);
			UsatSettingUpdateFileTask usatSettingTask = new UsatSettingUpdateFileTask(registry.getServerSet(host.getServerEnv()));
			tasks.add(usatSettingTask);
		}
	}
	
	@Override
	protected void addTasks(Host host, App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache);
		tasks.add(new AppSettingUpdateFileTask(app, ordinal, instanceNum, instanceCount, cache, registry.getServerSet(host.getServerEnv())));
		addInstallAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache, commands);
	}

	protected void addBeforeRestartCommands(Host host, App app, Integer ordinal, int instanceNum, List<String> commands, List<DeployTask> tasks) {
	}
	@Override
	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return super.enableAppIfDisabled(host, app, ordinal) && !app.getName().equals("applayer");
	}

	@Override
	public String getName() {
		return "PostgresKLS Install - Apps";
	}
}
