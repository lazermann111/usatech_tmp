package com.usatech.tools.deploy.archive;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oracle.jdbc.driver.OracleDriver;
import simple.db.Argument;
import simple.db.Call;
import simple.db.DataLayerException;
import simple.db.DbUtils;
import simple.db.Parameter;
import simple.sql.SQLType;
import simple.util.StaticMap;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.AppSettingUpdateFileTask;
import com.usatech.tools.deploy.CvsPullTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.DeployTaskInfo;
import com.usatech.tools.deploy.DeployUtils;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATHelper;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.tools.deploy.UploadAndRunPostgresSQLTask;
import com.usatech.tools.deploy.UsatSettingUpdateFileTask;

public class R46_Linux_ChangeSet extends AbstractLinuxChangeSet {
	public R46_Linux_ChangeSet() throws UnknownHostException {
		super();
	}

	@Override
	protected void registerApps() {
		registerApp(USATRegistry.KEYMGR, "2.14.0");
		registerApp(USATRegistry.LOADER, "1.32.0");
		registerApp(USATRegistry.APP_LAYER, "1.32.0");
		registerApp(USATRegistry.INAUTH_LAYER, "1.32.0");
		registerApp(USATRegistry.POSM_LAYER, "1.23.0");
		registerApp(USATRegistry.NET_LAYER, "1.32.0");
		registerApp(USATRegistry.OUTAUTH_LAYER, "1.32.0");
		registerApp(USATRegistry.DMS_APP, "1.8.0");
		registerApp(USATRegistry.HTTPD_NET, null);
		registry.registerSubApps(USATRegistry.HTTPD_NET, USATRegistry.DMS_APP);
		// registerApp(USATRegistry.RDW_LOADER, "3.18.0");
		registerApp(USATRegistry.RPTGEN_LAYER, "3.20.0");
		registerApp(USATRegistry.RPTREQ_LAYER, "3.20.0");
		registerApp(USATRegistry.USALIVE_APP, "1.22.0");
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.USALIVE_APP);
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.VERIZON_APP);
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.HOTCHOICE_APP);
		registerApp(USATRegistry.PREPAID_APP, "1.10.0");
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.GETMORE_APP); // The APP server needs PREPAID_APP, but the WEB server also needs the GETMORE_APP as subapp
		// registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.PREPAID_APP);
		registerApp(USATRegistry.HTTPD_WEB, null);
		registerApp(USATRegistry.TRANSPORT_LAYER, "3.20.0");
		registerApp(USATRegistry.POSTGRES_KLS);
		registerApp(USATRegistry.POSTGRES_MST);
		// registerApp(USATRegistry.POSTGRES_MSR);
		// registerApp(USATRegistry.POSTGRES_PGS);
	}

	@Override
	public boolean isApplicable(Host host) {
		return super.isApplicable(host) || host.isServerType("ODB") || host.isServerType("PGS") || host.isServerType("MST");
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(host.isServerType("ODB")) {
			final CvsPullTask source = new CvsPullTask("ServerLayers/TestClient/resources/usatcanadafile.csv", "HEAD");
			tasks.add(source);
			tasks.add(new DeployTask() {
				@Override
				public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
					int n;
					try {
						n = loadCABanks(source, deployTaskInfo);
					} catch(SQLException | DataLayerException e) {
						throw new IOException(e);
					}
					return "Loaded " + n + " Canadian Banks into database";
				}

				@Override
				public String getDescription(DeployTaskInfo deployTaskInfo) {
					return "Load Canadian Banks into database";
				}
			});
		} else if(host.isServerType("PGS")) {
			tasks.add(new UploadAndRunPostgresSQLTask("DatabaseScripts/releases/REL_Edge_Update_R46/R46.PGS.1.sql", "HEAD", Boolean.FALSE, "main"));
		} else if(host.isServerType("MST")) {
			USATHelper.installPostGIS(host, cache, tasks);
			String[] instances = registry.getServerSet(host.getServerEnv()).getServer(host, "MST").instances;
			int count = instances == null ? 1 : instances.length;
			int n = (host.getSimpleName().charAt(host.getSimpleName().length() - 1) - '0') - 1;
			for(int i = 0; i < count; i++) {
				String appdb = "app_" + String.valueOf(n * count + i + 1);
				Integer ordinal;
				if("LOCAL".equalsIgnoreCase(host.getServerEnv())) {
					ordinal = null;
				} else {
					ordinal = (instances == null || instances[0] == null ? null : i + 1);
				}
				tasks.add(new UploadAndRunPostgresSQLTask("CREATE EXTENSION IF NOT EXISTS postgis; DO $$ BEGIN PERFORM * FROM PG_ATTRIBUTE A JOIN PG_CLASS C ON C.OID = A.ATTRELID JOIN PG_NAMESPACE N ON N.OID = C.RELNAMESPACE WHERE UPPER(A.ATTNAME) = 'MERCHANT_CATEGORY_CODE' AND UPPER(C.RELNAME) = 'DEVICE_INFO' AND UPPER(N.NSPNAME) =  'APP_CLUSTER'; IF NOT FOUND THEN ALTER TABLE APP_CLUSTER.DEVICE_INFO ADD MERCHANT_CATEGORY_CODE SMALLINT; END IF; END; $$;", false, ordinal, appdb));
				tasks.add(new UploadAndRunPostgresSQLTask("DatabaseScripts/releases/REL_Edge_Update_R46/INSERT_GEOCODE_COUNTRY.sql", "HEAD", false, ordinal, appdb));
			}
		} else {
			// USATHelper.installJava(host, cache, tasks, 8, 25, getWorkDir());
			addPrepareHostTasks(host, tasks, cache);
			tasks.add(new UsatSettingUpdateFileTask(registry.getServerSet(host.getServerEnv())));
			tasks.add(new ExecuteTask("sudo find /opt/USAT/ -name '*.dmp' -delete"));
			if(host.isServerType("WEB")) {
				String[] subapps = new String[] { "esuds", "getmore", "hotchoice", "prepaid", "usalive", "verizon" };
				String[] cmds = new String[subapps.length + 1];
				cmds[0] = "sudo su";
				for(int i = 0; i < subapps.length; i++)
					cmds[i + 1] = "if [ -r /opt/USAT/httpd/ssl/comodo.ca-bundle] && [`grep -cF -e '-----BEGIN CERTIFICATE-----' /opt/USAT/httpd/ssl/" + subapps[i] + ".crt` -eq 1 ]; then cat /opt/USAT/httpd/ssl/comodo.ca-bundle >> /opt/USAT/httpd/ssl/" + subapps[i] + ".crt; fi";
				tasks.add(new ExecuteTask(cmds));
			}
		}
	}
	
	@Override
	protected void addTasks(Host host, final App app, Integer ordinal, String instance, final int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache);
		if(app.getInstallFileName() != null)
			tasks.add(new AppSettingUpdateFileTask(app, ordinal, instanceNum, instanceCount, cache, registry.getServerSet(host.getServerEnv()), false));
		if(USATRegistry.KEYMGR.equals(app))
			tasks.add(new UploadAndRunPostgresSQLTask("KeyManagerService/src/km_account_setup_2.sql", "HEAD", false, ordinal, "km"));
		addInstallAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache, commands);
	}

	@Override
	public String getName() {
		return "Edge Server - R46 Install - Apps";
	}
	
	protected Connection getConnection(DeployTaskInfo deployTaskInfo) throws IOException {
		String operUrl;
		if("LOCAL".equals(deployTaskInfo.host.getServerEnv()))
			operUrl = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb012.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=USADEV04.WORLD)))";
		else if("DEV".equals(deployTaskInfo.host.getServerEnv()))
			operUrl = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb011.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=usadev03.world)))";
		else if("INT".equals(deployTaskInfo.host.getServerEnv()))
			operUrl = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=intdb011.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=usadev02.world)))";
		else if("ECC".equals(deployTaskInfo.host.getServerEnv()))
			operUrl = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=eccscan01.usatech.com)(PORT=1525))(ADDRESS=(PROTOCOL=TCP)(HOST=eccscan02.usatech.com)(PORT=1525)))(CONNECT_DATA=(SERVICE_NAME=usaecc_db.world)))";
		else if("USA".equals(deployTaskInfo.host.getServerEnv()))
			operUrl = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.120)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.121)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.122)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.220)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.221)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.222)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)))";
		else
			throw new IOException("Server Type '" + deployTaskInfo.host.getServerEnv() + "' is not recognized");

		new OracleDriver();
		char[] password = DeployUtils.getPassword(deployTaskInfo, "SYSTEM@OPER", "SYSTEM user on OPER database");
		Connection conn;
		try {
			conn = DriverManager.getConnection(operUrl, "SYSTEM", new String(password));
		} catch(SQLException e) {
			deployTaskInfo.passwordCache.setPassword(deployTaskInfo.host, "SYSTEM@OPER", null);
			throw new IOException(e);
		}
		return conn;
	}
	
	protected static final Call INSERT_CA_BANK_CALL = new Call();
	static {
		INSERT_CA_BANK_CALL.setSql("INSERT INTO CORP.ROUTING_NUM(ROUTING_NUM, BANK_COUNTRY_CD, LAST_CHANGE_DATE, BANK_NAME, BANK_ADDRESS, BANK_CITY, BANK_STATE_CD, BANK_POSTAL, FILE_DATE) VALUES(LPAD(?, 9 , 0),'CA',SYSDATE,?,?,?,?,REPLACE(?, ' ', ''),?)");
		INSERT_CA_BANK_CALL.setArguments(new Argument[] {
				new Parameter(null, new SQLType(Types.NUMERIC, null, null, null), false, false, true),
				new	Parameter("routingNum", new SQLType(Types.VARCHAR,  null, 20, null), false, true, false),
				new	Parameter("bankName", new SQLType(Types.VARCHAR,  null, 50, null), false, true, false),
				new	Parameter("bankAddress", new SQLType(Types.VARCHAR,  null, 255, null), false, true, false),
				new	Parameter("bankCity", new SQLType(Types.VARCHAR,  null, 50, null), false, true, false),
				new	Parameter("bankStateCd", new SQLType(Types.VARCHAR,  null, 2, null), false, true, false),
				new	Parameter("bankPostal", new SQLType(Types.VARCHAR,  null, 20, null), false, true, false),
				new	Parameter("fileTime", new SQLType(Types.TIMESTAMP), false, true, false),   
		});		
	}
	protected static final Pattern CA_BANK_PARSER = Pattern.compile("([^,]+),([0-9]+),(.+), ([^,]+), ([A-Z]{2}) ([A-Z0-9]{3} [A-Z0-9]{3})(?: \\([^)]+\\))?,+\\s*");
	protected static final Pattern CA_BANK_LENIENT_PARSER = Pattern.compile("((?:\"[^\"]+\")|[^,]+),([0-9]+),\"?(?:(.*),)?\\s*([^,]*), ?([A-Z]{2})(?: +([A-Z0-9]{3} ?[A-Z0-9]{0,3}))?(?: ?\\([^)]+\\))?,*\"?\\s*");

	protected int loadCABanks(CvsPullTask source, DeployTaskInfo deployTaskInfo) throws IOException, SQLException, DataLayerException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(source.getResultInputStream()));
		try {
			String line;
			int n = 0;
			int success = 0;
			boolean okay = false;
			Connection conn = getConnection(deployTaskInfo);
			try {
				while((line = reader.readLine()) != null) {
					Matcher matcher = CA_BANK_PARSER.matcher(line);
					n++;
					if(!matcher.matches()) {
						matcher = CA_BANK_LENIENT_PARSER.matcher(line);
						if(!matcher.matches()) {
							deployTaskInfo.interaction.getWriter().println("ERROR: Line " + n + " does not match expected. Skipping");
							continue;
						}
					}
					String[] keys = new String[] { "bankName", "routingNum", "bankAddress", "bankCity", "bankStateCd", "bankPostal" };
					String[] values = new String[keys.length];
					for(int i = 0; i < matcher.groupCount() && i < keys.length; i++) {
						values[i] = matcher.group(i + 1);
						if(values[i] != null && values[i].startsWith("\"") && values[i].endsWith("\""))
							values[i] = values[i].substring(1, values[i].length() - 1);
					}
					Map<String, String> params = StaticMap.create(keys, values);
					deployTaskInfo.interaction.getWriter().println("Parsed line " + n + " as: " + params);
					INSERT_CA_BANK_CALL.executeCall(conn, params, null);
					conn.commit();
					success++;
				}
				okay = true;
			} finally {
				DbUtils.commitOrRollback(conn, okay, true);
			}
			return success;
		} finally {
			reader.close();
		}
	}
}
