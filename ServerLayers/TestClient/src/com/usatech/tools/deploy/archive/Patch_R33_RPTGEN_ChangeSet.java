package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.RPTGEN_LAYER;

import java.io.File;
import java.net.UnknownHostException;


import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
import com.usatech.transport.KnownHostsVerifier;
import com.usatech.transport.TransportReason;

public class Patch_R33_RPTGEN_ChangeSet extends AbstractLinuxPatchChangeSet {
	protected String baseDir;

	public Patch_R33_RPTGEN_ChangeSet() throws UnknownHostException {
		super();
		baseDir = System.getProperty("rootSourceDir");
		File simpleBinDir = new File(baseDir + "/ReportGenerator/bin");
		registerClass(simpleBinDir, TransportReason.class);
		registerClass(simpleBinDir, KnownHostsVerifier.class);
	}
	protected void registerApps() {
		registerApp(RPTGEN_LAYER);
	}
	@Override
	public String getName() {
		return "Edge Server - R33 - RptGen Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
