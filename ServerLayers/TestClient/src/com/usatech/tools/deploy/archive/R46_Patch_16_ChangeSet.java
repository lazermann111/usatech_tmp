package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.authoritylayer.TandemGatewayTask;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.posm.schedule.SubmerchantsUploadJob;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R46_Patch_16_ChangeSet extends MultiLinuxPatchChangeSet {
	public R46_Patch_16_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/POSMLayer/conf/posm-data-layer.xml", "classes", "REL_posmlayer_1_23_16", false, USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/AuthorityLayer/src", TandemGatewayTask.class, "REL_authoritylayer_1_32_16", USATRegistry.INAUTH_LAYER);
		registerSource("ServerLayers/LayersCommon/src", AuthorityAttrEnum.class, "REL_authoritylayer_1_32_16", USATRegistry.INAUTH_LAYER);
		registerSource("ServerLayers/POSMLayer/src/main", SubmerchantsUploadJob.class, "REL_posmlayer_1_23_16", USATRegistry.POSM_LAYER);
		registerResource("ServerLayers/AppLayer/src/AppLayerService.properties", "classes", "REL_applayer_1_32_16", true, USATRegistry.APP_LAYER);
	}

	@Override
	public String getName() {
		return "R46 Patch 16";
	}
}
