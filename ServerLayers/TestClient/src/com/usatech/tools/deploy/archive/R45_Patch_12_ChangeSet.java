package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R45_Patch_12_ChangeSet extends MultiLinuxPatchChangeSet {
	public R45_Patch_12_ChangeSet() throws UnknownHostException {
		super();
		registerResource("DMS/resources/jsp/dealer/licenseDetails.jsp", "web/jsp/dealer", "REL_DMS_1_7_1", false,USATRegistry.DMS_APP);
	}

	@Override
	public String getName() {
		return "R45 Patch 12 For new fees";
	}
}
