package com.usatech.tools.deploy.archive;

import java.util.Arrays;
import java.util.List;

import com.usatech.tools.deploy.AbstractSolarisChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.HostSpecificValue;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.PropertiesUpdateFileTask;

import simple.io.Interaction;
import simple.text.StringUtils;
import simple.util.CollectionUtils;

public class R28_5_ChangeSet extends AbstractSolarisChangeSet {
	protected final int[] instances = new int[] {1,2};
	public R28_5_ChangeSet() {
		super();
		Integer[] ords = new Integer[instances.length];
		for(int i = 0; i < instances.length; i++)
			ords[i] = instances[i];
		ordMap.put("DEV", ords);
		ordMap.put("INT", ords);
		ordMap.put("ECC", ords);		
	}
	@Override
	public boolean isApplicable(Host host) {
		boolean hasServerType = false;
		for(String serverType : host.getServerTypes()) {
			if(appMap.containsKey(serverType)) {
				hasServerType = true;
				break;
			}
		}
		if(!hasServerType)
			return false;
		Integer[] ords = ordMap.get(host.getServerEnv());
		if(ords == null || ords.length == 0)
			return false;
		if(ords[0] == null && ords.length == 1)
			return CollectionUtils.iterativeSearch(instances, Integer.parseInt(host.getSimpleName().substring(7))) >= 0;
		return true;
	}
	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) {
		commands.add("sudo su");
	}

	@Override
	protected void addTasks(Host host, App app, final Integer ordinal, List<DeployTask> tasks, List<String> commands, PasswordCache cache) {
		commands.add("svcadm -v restart " + app.getName() + (ordinal == null ? "" : ordinal.toString()));
		String copyPath = app.getName() + (ordinal == null ? "" : ordinal.toString()) + "_USAT_app_settings.properties";
		String filePath = "/opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal.toString()) + "/specific/USAT_app_settings.properties";
		final int instance = (ordinal == null ? Integer.parseInt(host.getSimpleName().substring(7)) : ordinal);
		tasks.add(new ExecuteTask("sudo su", "cp " + filePath + " ~/" + copyPath, "chmod 666 ~/" + copyPath));
		
		PropertiesUpdateFileTask appUpdateTask = new PropertiesUpdateFileTask(true, 0640, app.getName(), "usat");
		appUpdateTask.setFilePath(copyPath);
		appUpdateTask.registerValue("USAT.simplemq.secondaryDataSources", new HostSpecificValue() {
			public String getValue(Host host, Interaction interaction) {
				return StringUtils.generateList("MQ_", null, ",", instance + 1, instanceCount - 1, 1, instanceCount);
			}
			public boolean isOverriding() {
				return true;
			}
		});
		
		final int[] offsets = new int[instanceCount];
		if(ordinal == null) {
			Arrays.fill(offsets, 7707);
		} else {
			for(int i = 0; i < offsets.length; i++) {
				offsets[i] = 17007 + ((1 + ((ordinal + i - 1) % offsets.length)) * 100);
			}
		}
		StringBuilder sb = new StringBuilder();
		for(App a : apps) {
			if(!"postgres".equalsIgnoreCase(a.getName()))
				for(int i = 1; i < offsets.length; i++) {
					final String entryServerType = a.getServerType().toLowerCase();
					final String entryApp = a.getName().toLowerCase();
					sb.setLength(0);
					sb.append("USAT.simplemq.directPollerUrl.").append(entryApp).append('_');
					sb.append('s').append(i);
					final int port = offsets[i] + a.getPortOffset();
					final int sindex = i;
					appUpdateTask.registerValue(sb.toString(), new HostSpecificValue() {
						public String getValue(Host host, Interaction interaction) {
							StringBuilder sb = new StringBuilder();
							sb.append(host.getSimpleName(), 0, 3).append(entryServerType);
							if(ordinal == null) {
								int sord = Integer.parseInt(host.getSimpleName().substring(7));
								sord = 1 + ((sord + sindex - 1) % offsets.length);
								sb.append(host.getSimpleName(), 6, 7).append(sord).append(":${USAT.simplemq.direct.").append(entryApp).append(".port}");
							} else
								sb.append("01:").append(port);
							
							return sb.toString();
						}
						public boolean isOverriding() {
							return true;
						}
					});						
				}
		}
		tasks.add(appUpdateTask);
		tasks.add(new ExecuteTask("sudo su", "chmod 640 ~/" + copyPath, "chown " + app.getName() + ":" + app.getName() + " ~/" + copyPath, "mv ~/" + copyPath + " " + filePath));	
	}

	@Override
	public String getName() {
		return "Edge Server - R28 Install (Step 5)";
	}
}
