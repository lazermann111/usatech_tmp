package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R45_Patch_14_ChangeSet extends MultiLinuxPatchChangeSet {
	public R45_Patch_14_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ReportGenerator/src/ReportRequesterService.properties", "classes/", "REL_report_generator_3_19_1", true,USATRegistry.RPTREQ_LAYER);
		registerResource("ReportGenerator/src/com/usatech/report/rgr-data-layer.xml", "classes/com/usatech/report/", "REL_report_generator_3_19_1", true, USATRegistry.RPTREQ_LAYER);
		registerResource("ReportGenerator/src/com/usatech/report/rgr-data-layer.xml", "classes/com/usatech/report/", "REL_report_generator_3_19_1", true, USATRegistry.RPTGEN_LAYER);
	}

	@Override
	public String getName() {
		return "R45 Patch 14 For report requester and generator";
	}
}
