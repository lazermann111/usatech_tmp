package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R43_Patch_4_ChangeSet extends MultiLinuxPatchChangeSet {
	public R43_Patch_4_ChangeSet() throws UnknownHostException {
		super();
		registerResource("usalive/xsl/rma-data-layer.xml", "classes", "BRN_R44", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/rma_rental_for_credit_display.jsp", "web", "BRN_R44", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/rma_parts_display.jsp", "web", "BRN_R44", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/rma_all_device_display.jsp", "web", "BRN_R44", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/rma_replacement.jsp", "web", "BRN_R44", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/rma_rental_for_credit.html.jsp", "web", "BRN_R44", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/rma_create_parts.html.jsp", "web", "BRN_R44", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/rma_device.html.jsp", "web", "BRN_R44", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/rma_parts.html.jsp", "web", "BRN_R44", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/rma_create_device.html.jsp", "web", "BRN_R44", false, USATRegistry.USALIVE_APP);
	}

	@Override
	public String getName() {
		return "R43 Patch #4";
	}
}
