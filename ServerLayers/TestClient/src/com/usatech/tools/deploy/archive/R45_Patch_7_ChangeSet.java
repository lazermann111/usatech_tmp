package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R45_Patch_7_ChangeSet extends MultiLinuxPatchChangeSet {
	public R45_Patch_7_ChangeSet() throws UnknownHostException {
		super();
		registerResource("Prepaid/src/prepaid-data-layer.xml", "classes", "REL_prepaid_1_9_2", false, USATRegistry.PREPAID_APP);

	}

	@Override
	public String getName() {
		return "R45 Patch 7";
	}
}
