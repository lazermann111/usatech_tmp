package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.posm.tasks.POSMNextTask;
import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.USATRegistry;

public class Patch_R41F_POSMLayer_ChangeSet extends AbstractLinuxPatchChangeSet {

	public Patch_R41F_POSMLayer_ChangeSet() throws UnknownHostException {
		super();		
		registerResource("ServerLayers/LayersCommon/src/common-data-layer.xml", "classes", "REL_R41F");
		registerResource("ServerLayers/POSMLayer/conf/posm-data-layer.xml", "classes", "REL_R41F");
		registerSource("ServerLayers/LayersCommon/src",  AuthorityAttrEnum.class, "REL_R41F");
		registerSource("ServerLayers/POSMLayer/src/main",  POSMNextTask.class, "REL_R41F");
	}
	protected void registerApps() {
		registerApp(USATRegistry.POSM_LAYER);
	}
	@Override
	public String getName() {
		return "Edge Server - R41F POSMLayer Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
