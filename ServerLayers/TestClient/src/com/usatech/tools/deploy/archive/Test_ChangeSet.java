package com.usatech.tools.deploy.archive;

import java.io.File;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.UsatSettingUpdateFileTask;
public class Test_ChangeSet implements ChangeSet {
	@Override
	public String getName() {
		return "TEST";
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) {
		UsatSettingUpdateFileTask usat = new UsatSettingUpdateFileTask();
		usat.setFilePath("USAT_environment_settings.properties");
		int ordinal = 3;
		String appName;
		if(host.isServerType("APR"))
			appName = "inauthlayer";
		else if(host.isServerType("NET"))
			appName = "outauthlayer";
		else
			appName = "XXX";
		File buildsDir = new File(System.getProperty("rootSourceDir", "D:\\Development\\Java Projects") + "\\ApplicationBuilds");
		/*
		AppSettingUpdateFileTask app = new AppSettingUpdateFileTask(appName, ordinal, 3, Arrays.asList(new App[] {
		APP_LAYER, INAUTH_LAYER, POSM_LAYER, NET_LAYER, OUTAUTH_LAYER, POSTGRESQL_MST
		}), 5, cache, null);
		app.setFilePath("USAT_app_settings.properties");
		*/
		return new DeployTask[] {
				new ExecuteTask("sudo su", "cp /opt/USAT/" + appName + ordinal + "/classes/USAT_environment_settings.properties ~", "cp /opt/USAT/" + appName + ordinal + "/classes/USAT_environment_settings.properties ~/USAT_app_settings.properties", "chown bkrug:usat ~/USAT_*_settings.properties"),
				usat,
		// app,
		};
	}

	@Override
	public boolean isApplicable(Host host) {
		return true;
	}
	@Override
	public String toString() {
		return getName();
	}
}
