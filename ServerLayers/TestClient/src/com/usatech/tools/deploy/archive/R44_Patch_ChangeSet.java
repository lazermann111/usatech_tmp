package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.applayer.AuthorizeTask;
import com.usatech.applayer.UpdateAuthorizationTask;
import com.usatech.keymanager.service.UpdateAccountTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R44_Patch_ChangeSet extends MultiLinuxPatchChangeSet {
	public R44_Patch_ChangeSet() throws UnknownHostException {
		super();
		// registerResource("Prepaid/src/prepaid-data-layer.xml", "classes", "REL_prepaid_1_8_0", false, USATRegistry.PREPAID_APP);
		// registerSource("Simple1.5/src", ConsumedPeerMessage.class, "REL_report_generator_3_18_1", USATRegistry.RPTGEN_LAYER);
		registerSource("ServerLayers/AppLayer/src", UpdateAuthorizationTask.class, "REL_applayer_1_30_1", USATRegistry.LOADER);
		registerSource("KeyManagerService/src", UpdateAccountTask.class, "REL_keymgr_2_12_1", USATRegistry.KEYMGR);
		registerResource("ServerLayers/AppLayer/src/applayer-data-layer.xml", "classes", "REL_applayer_1_30_1", false, USATRegistry.APP_LAYER);
		registerSource("ServerLayers/AppLayer/src", AuthorizeTask.class, "REL_applayer_1_30_1", USATRegistry.APP_LAYER);
		registerResource("usalive/xsl/usalive-data-layer.xml", "classes", "REL_USALive_1_20_1", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/customerreporting_web/home_stat.jsp", "web", "REL_USALive_1_20_1", false, USATRegistry.USALIVE_APP);
	}

	@Override
	public String getName() {
		return "R44 Patch";
	}
}
