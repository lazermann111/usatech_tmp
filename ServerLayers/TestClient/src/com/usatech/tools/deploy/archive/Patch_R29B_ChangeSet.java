package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.POSM_LAYER;

import java.io.File;
import java.net.UnknownHostException;

import com.usatech.posm.utils.POSMUtils;
import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;

public class Patch_R29B_ChangeSet extends AbstractLinuxPatchChangeSet {
	protected String baseDir;

	public Patch_R29B_ChangeSet() throws UnknownHostException {
		super();
		baseDir = System.getProperty("rootSourceDir");
		File posmlayerBinDir = new File(baseDir + "/POSMLayer/bin");
		registerClass(posmlayerBinDir, POSMUtils.class);
		registerResource(posmlayerBinDir, "posm-data-layer.xml");
		// File applayerBinDir = new File(baseDir + "/AppLayer/bin");
		// registerClass(applayerBinDir, MessageProcessor_82.class);
		// File layersCommonBinDir = new File(baseDir + "/LayersCommon/bin");
		// registerClass(layersCommonBinDir, LoadDataAttrEnum.class);
	}
	protected void registerApps() {
		registerApp(POSM_LAYER);
	}
	@Override
	public String getName() {
		return "Edge Server - R29B Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
