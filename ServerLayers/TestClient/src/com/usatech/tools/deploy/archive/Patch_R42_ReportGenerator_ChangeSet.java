package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.report.GenerateCreditXMLTask;
import com.usatech.report.GenerateEFTXMLTask;
import com.usatech.report.ReportRequestFactory;
import com.usatech.report.rest.CreditData;
import com.usatech.report.rest.EFTData;
import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.USATRegistry;

/*
 * This changeset is fix issues in R42
 */
public class Patch_R42_ReportGenerator_ChangeSet extends AbstractLinuxPatchChangeSet {
	protected String baseDir;

	public Patch_R42_ReportGenerator_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ReportGenerator/src", GenerateCreditXMLTask.class, "REL_report_generator_3_16_1");
		registerSource("ReportGenerator/src", GenerateEFTXMLTask.class, "REL_report_generator_3_16_1");
		registerSource("ReportGenerator/src", ReportRequestFactory.class, "REL_report_generator_3_16_1");
		registerSource("ReportGenerator/src", CreditData.class, "REL_report_generator_3_16_1");
		registerSource("ReportGenerator/src", EFTData.class, "REL_report_generator_3_16_1");
		registerResource("ReportGenerator/src/com/usatech/report/rgg-data-layer.xml", "classes/com/usatech/report/", "REL_report_generator_3_16_1");
	}
	protected void registerApps() {
		registerApp(USATRegistry.RPTGEN_LAYER);
	}
	@Override
	public String getName() {
		return "Edge Server - R42.1 - Report Generator Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
