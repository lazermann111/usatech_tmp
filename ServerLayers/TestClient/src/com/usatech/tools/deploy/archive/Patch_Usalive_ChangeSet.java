package com.usatech.tools.deploy.archive;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;

public class Patch_Usalive_ChangeSet extends AbstractLinuxPatchChangeSet {
	public Patch_Usalive_ChangeSet() throws UnknownHostException {
		super();
		// registerSource("Falcon/src", DirectXHTMLGenerator.class, "REL_USALive_1_10_1");
		// registerResource("usalive/xsl/report-data-layer.xml", "classes", "REL_USALive_1_10_1");
		// registerResource("usalive/xsl/templates/device/terminal-customers.xsl", "classes/templates/device", "REL_USALive_1_10_1");
		// registerResource("usalive/web/css/usalive-app-style.css", "web/css", "REL_USALive_1_10_2");
		// registerSource("usalive/src", AbstractBuildActivityFolioStep.class, "REL_USALive_1_11_1");
		// registerSource("usalive/src", AbstractBuildActivityFolioStep.class, "HEAD");
		// registerResource("usalive/xsl/templates/refund/create-refund-set.xsl", "classes/templates/refund", "HEAD");
		// registerResource("usalive/xsl/templates/refund/create-refund.xsl", "classes/templates/refund", "HEAD");
		// registerResource("usalive/web/scripts/refund-search.js", "web/scripts", "HEAD");
		// registerSource("Falcon/src", FilterItem.class, "HEAD");
		// registerResource("usalive/xsl/user-data-layer.xml", "classes", "HEAD");
		registerResource("usalive/xsl/other-data-layer.xml", "classes", "BRN_R41");
		registerResource("usalive/web/payor_config.html.jsp", "web", "HEAD");
	}


	@Override
	public boolean isApplicable(Host host) {
		return true;
	}
	protected void registerApps() {
		// registerApp(USATRegistry.RPTGEN_LAYER);
		registerApp(USATRegistry.USALIVE_APP);
		// registerApp(USATRegistry.HTTPD_WEB);
		// registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.USALIVE_APP);
	}
	@Override
	public String getName() {
		return "USALive Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}

	@Override
	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		// super.addRestartAppCommands(host, app, instance, instanceNum, instanceCount, tasks, commands, cache);
	}
}
