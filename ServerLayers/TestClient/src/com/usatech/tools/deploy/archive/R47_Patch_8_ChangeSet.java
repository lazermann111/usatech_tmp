package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.authoritylayer.InternalAuthorityTask;
import com.usatech.posm.tasks.POSMSaleUpdateTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R47_Patch_8_ChangeSet extends MultiLinuxPatchChangeSet {
	public R47_Patch_8_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/POSMLayer/src/main", POSMSaleUpdateTask.class, "REL_posmlayer_1_24_8", USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/AuthorityLayer/src", InternalAuthorityTask.class, "REL_authoritylayer_1_33_8", USATRegistry.INAUTH_LAYER);
	}
	

	@Override
	public String getName() {
		return "R47 Patch 8";
	}
}
