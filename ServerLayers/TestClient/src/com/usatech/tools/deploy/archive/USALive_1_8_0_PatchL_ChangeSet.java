package com.usatech.tools.deploy.archive;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.CvsPullTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.UploadTask;

public class USALive_1_8_0_PatchL_ChangeSet implements ChangeSet {
	public USALive_1_8_0_PatchL_ChangeSet() throws UnknownHostException {
		super();
	}

	@Override
	public String getName() {
		return "Patch L USALive 1.8.0";
	}
	
	@Override
	public String toString() {
		return getName();
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("WEB");
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache)
			throws IOException {
		ArrayList<DeployTask> tasks=new ArrayList<DeployTask>();
		//-rw-r--r--. 1 bin bin 38832 Sep 26 11:15 usalive-app-style.css
		tasks.add(new UploadTask(new CvsPullTask("usalive/web/css/usalive-app-style.css", "REL_USALive_1_8_0_J"), 0644, "bin", "bin", "usalive-app-style.css", true, 
				"/opt/USAT/httpd/usalive/CustomerReporting/css/usalive-app-style.css",
				"/opt/USAT/httpd/hotchoice/CustomerReporting/css/usalive-app-style.css",
				"/opt/USAT/httpd/verizon/CustomerReporting/css/usalive-app-style.css"));
		// monit restart httpd
		tasks.add(new ExecuteTask("sudo su",
				"/usr/monit-latest/bin/monit restart httpd"
			));
		
		DeployTask[] deployTasks=new DeployTask[tasks.size()];
		for(int i=0; i<tasks.size();i++){
			deployTasks[i]=tasks.get(i);
		}
		return deployTasks;
	}
}
