package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.applayer.AuthorizeTask;
import com.usatech.prepaid.PSRequestHandler;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R45_Patch_11_ChangeSet extends MultiLinuxPatchChangeSet {
	public R45_Patch_11_ChangeSet() throws UnknownHostException {
		super();
		registerSource("Prepaid/src", PSRequestHandler.class, "BRN_R45", USATRegistry.PREPAID_APP);
		registerSource("ServerLayers/AppLayer/src", AuthorizeTask.class, "REL_applayer_1_31_3", USATRegistry.APP_LAYER);
	}

	@Override
	public String getName() {
		return "R45 Patch 11";
	}
}
