package com.usatech.tools.deploy.archive;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import simple.io.BufferStream;
import simple.io.HeapBufferStream;

import com.sshtools.j2ssh.SftpClient;
import com.usatech.tools.deploy.AbstractSolarisChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.DeployTaskInfo;
import com.usatech.tools.deploy.DeployUtils;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.UploadTask;


public class R28_1_ChangeSet extends AbstractSolarisChangeSet {
	protected File mqSetupFile = new File(System.getProperty("rootSourceDir", "D:\\Development\\Java Projects") + "\\Simple1.5\\src\\simple\\mq\\peer\\mq_peer_postgres_setup_2.sql");
	public R28_1_ChangeSet() {
		super();
				
	}
	protected void registerApps() {
		registerApp(POSTGRES);
	}
	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) {
	}
	@Override
	protected void addTasks(Host host, App app, Integer ordinal, List<DeployTask> tasks, List<String> commands, final PasswordCache cache) throws IOException {
		if("postgres".equalsIgnoreCase(app.getName()) && (ordinal == null || ordinal == 1)) {
			String[] databases;
			if("LOCAL".equalsIgnoreCase(host.getServerEnv()))
				databases = new String[]{ "bkmq1", "bkmq2", "dkmq1", "dkmq2" };
			else
				databases = new String[]{"mq"};
			final String userFileName = "add_fifth_user.sql";
			if(tasks.isEmpty()) {
				tasks.add(new UploadTask(mqSetupFile, 0644));
				commands.add("sudo su");
				commands.add("mv '" + mqSetupFile.getName() + "' /home/postgres");
				if(!"LOCAL".equalsIgnoreCase(host.getServerEnv())) {
					tasks.add(new DeployTask() {					
						@Override
						public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
							BufferStream bufferStream = new HeapBufferStream();
							PrintWriter pw = new PrintWriter(bufferStream.getOutputStream());
							for(App a : new App[] {APP_LAYER, NET_LAYER, INAUTH_LAYER, OUTAUTH_LAYER, POSM_LAYER}) {
								pw.print("CREATE USER ");
								pw.print(a.getDbUserName());
								pw.print("_5 PASSWORD '");
								if("USA".equalsIgnoreCase(deployTaskInfo.host.getServerEnv()) || "ECC".equalsIgnoreCase(deployTaskInfo.host.getServerEnv())) {
									char[] pwd = DeployUtils.getPassword(cache, deployTaskInfo.host, "database.MQ." + a.getDbUserName().toUpperCase(), a.getDbUserName().toUpperCase() + " for MQ", deployTaskInfo.interaction);
									if(pwd != null) {
										pw.print(pwd);
									} else {
										pw.print(a.getDbUserName().toUpperCase());
										pw.print("_1");
									}
								} else {
									pw.print(a.getDbUserName().toUpperCase());
									pw.print("_1");
								}
								pw.println("';");
								pw.print("GRANT use_mq TO ");
								pw.print(a.getDbUserName());
								pw.println("_5;");
								pw.print("GRANT write_file_repo TO ");
								pw.print(a.getDbUserName());
								pw.println("_5;");
							}
							pw.flush();
							SftpClient sftp = deployTaskInfo.sshClientCache.getSftpClient(deployTaskInfo.host, deployTaskInfo.user);
							sftp.put(bufferStream.getInputStream(), userFileName);
							sftp.chmod(0644, userFileName);
							return "Complete";
						}

						@Override
						public String getDescription(DeployTaskInfo deployTaskInfo) {
							return "Create postgres db users";
						}
					});	
					commands.add("mv ~/'" + userFileName + "' /home/postgres");
					File postgresConfFile = new File(buildsDir.getParentFile(), "server_app_config/MST/postgresql.conf");			
					tasks.add(new UploadTask(postgresConfFile, 0600));
					commands.add("chown postgres:postgres ~/postgresql.conf");				
					commands.add("mv ~/postgresql.conf /opt/USAT/postgres/latest/data");				
				}
			}
			commands.add("svcadm -v disable -st postgresql");
			commands.add("su - postgres -c '/opt/USAT/postgres/latest/bin/64/pg_ctl -w -D /opt/USAT/postgres/latest/data -l /opt/USAT/postgres/latest/data/logs/server.log start -o \"-p 5435\"'");
			for(String database : databases) {
				commands.add("su - postgres -c '/opt/USAT/postgres/latest/bin/64/psql " + database + " -p 5435 -f /home/postgres/" + mqSetupFile.getName() + "'");
			}
			if(!"LOCAL".equalsIgnoreCase(host.getServerEnv()))
				commands.add("su - postgres -c '/opt/USAT/postgres/latest/bin/64/psql postgres -p 5435 -f /home/postgres/" + userFileName + "'");			
			commands.add("su - postgres -c '/opt/USAT/postgres/latest/bin/64/pg_ctl -D /opt/USAT/postgres/latest/data stop -m fast'");
			commands.add("svcadm -v enable -s postgresql");
		}
	}
	
	@Override
	public String getName() {
		return "Edge Server - R28 Install (Step 1)";
	}
}
