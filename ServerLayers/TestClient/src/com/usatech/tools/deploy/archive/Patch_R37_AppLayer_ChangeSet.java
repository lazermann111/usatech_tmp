package com.usatech.tools.deploy.archive;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;

public class Patch_R37_AppLayer_ChangeSet extends AbstractLinuxPatchChangeSet {
	public Patch_R37_AppLayer_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/AppLayer/src/applayer-data-layer.xml", "classes", "REL_applayer_1_23_1");
	}
	protected void registerApps() {
		registerApp(USATRegistry.APP_LAYER);
		registerApp(USATRegistry.LOADER);
	}

	@Override
	public String getName() {
		return "Edge Server - R37 Patch AppLayer";
	}
	
	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
	@Override
	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
	}
}
