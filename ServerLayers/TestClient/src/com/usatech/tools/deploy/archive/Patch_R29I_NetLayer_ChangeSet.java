package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.NET_LAYER;

import java.io.File;
import java.net.UnknownHostException;

import com.usatech.layers.common.constants.GenericResponseServerActionCode;
import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;

public class Patch_R29I_NetLayer_ChangeSet extends AbstractLinuxPatchChangeSet {
	protected String baseDir;

	public Patch_R29I_NetLayer_ChangeSet() throws UnknownHostException {
		super();
		baseDir = System.getProperty("rootSourceDir");
		File layersCommonBinDir = new File(baseDir + "/LayersCommon/bin");
		registerClass(layersCommonBinDir, GenericResponseServerActionCode.class);
	}
	protected void registerApps() {
		registerApp(NET_LAYER);
	}
	@Override
	public String getName() {
		return "Edge Server - R29I NetLayer Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
