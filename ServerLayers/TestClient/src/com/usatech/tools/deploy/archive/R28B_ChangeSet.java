package com.usatech.tools.deploy.archive;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import com.usatech.applayer.InboundFileTransferTask;

import com.usatech.layers.common.LegacyUtils;
import com.usatech.tools.deploy.AbstractSolarisPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.UploadAndRunPostgresSQLTask;

public class R28B_ChangeSet extends AbstractSolarisPatchChangeSet {
	protected final File mqSetup3File;
	public R28B_ChangeSet() {
		super();
		String baseDir = System.getProperty("rootSourceDir", "D:\\Development\\Java Projects");
		this.mqSetup3File = new File(baseDir + "\\Simple1.5\\src\\simple\\mq\\peer\\mq_peer_postgres_setup_3.sql");
		File applayerBinDir = new File(baseDir + "\\AppLayer\\bin");
		File applayerSrcDir = new File(baseDir + "\\AppLayer\\src");
		registerResource(applayerSrcDir, "legacy-data-layer.xml");
		registerResource(applayerSrcDir, "loader-data-layer.xml");
		registerResource(applayerSrcDir, "AppLayerService.properties");
		registerClass(applayerBinDir, InboundFileTransferTask.class);
		File layersCommonBinDir = new File(baseDir + "\\LayersCommon\\bin");
		registerClass(layersCommonBinDir, LegacyUtils.class);
	}
	protected void registerApps() {
		registerApp(APP_LAYER);
		Set<App> emptyApps = Collections.emptySet();
		appMap.put("MST", emptyApps);
	}
	@Override
	public String getName() {
		return "Edge Server - R28B Patch";
	}
	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(host.isServerType("MST")) {
			String[] databases;
			if("LOCAL".equalsIgnoreCase(host.getServerEnv())) {
				databases = new String[] {"bkmq1", "bkmq2", "dkmq1", "dkmq2"};
			} else {
				databases = new String[] {"mq"};
			}
			for(String database : databases)
				tasks.add(new UploadAndRunPostgresSQLTask(mqSetup3File, false, database));
		}
	}

}
