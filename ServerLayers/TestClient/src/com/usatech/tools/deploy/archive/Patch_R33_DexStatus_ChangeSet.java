package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.USALIVE_APP;

import java.io.File;
import java.net.UnknownHostException;


import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
import com.usatech.usalive.report.TestTransportStep;

public class Patch_R33_DexStatus_ChangeSet extends AbstractLinuxPatchChangeSet {
	protected String baseDir;

	public Patch_R33_DexStatus_ChangeSet() throws UnknownHostException {
		super();
		baseDir = System.getProperty("rootSourceDir");
		registerResource("ReportGenerator/src/com/usatech/report/build/BuildReportPillars.properties", "classes/com/usatech/report/build", "HEAD");
	}
	protected void registerApps() {
		registerApp(USALIVE_APP);
	}
	@Override
	public String getName() {
		return "Edge Server - R33 - DexStatus Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
