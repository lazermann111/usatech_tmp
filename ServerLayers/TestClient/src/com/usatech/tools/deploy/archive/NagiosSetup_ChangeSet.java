package com.usatech.tools.deploy.archive;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.IPTablesUpdateTask;
import com.usatech.tools.deploy.OptionalTask;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.UploadTask;

public class NagiosSetup_ChangeSet implements ChangeSet {
	protected final String coreVersion = "3.4.1";
	protected final String pluginVersion = "1.4.16";

	@Override
	public String getName() {
		return "Nagios " + coreVersion + " with plugins " + pluginVersion + " Install";
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("MST");
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		List<DeployTask> tasks = new ArrayList<DeployTask>();
		List<String> commands = new ArrayList<String>();
		commands.add("sudo su");
		commands.add("yum -y install httpd php | grep -v '#'");
		commands.add("yum -y install gcc glibc glibc-common | grep -v '#'");
		commands.add("yum -y install gd gd-devel | grep -v '#'");
		commands.add("grep -c 'nagios:' /etc/group || groupadd -g 1001 nagios");
		commands.add("grep -c 'nagcmd:' /etc/group || groupadd -g 1000 nagcmd");
		commands.add("if [ `egrep -c '^nagios:' /etc/passwd` -eq 0 ]; then useradd -c 'Nagios' -g 1000 -G nagcmd -m -u 1001 nagios; fi");
		commands.add("if [ `egrep -c '^nagcmd:x:1000:(\\w\\w*,)*apache' /etc/group` -eq 0 ]; then usermod -G nagcmd apache; fi");

		tasks.add(new ExecuteTask(commands.toArray(new String[commands.size()])));
		
		commands.clear();
		commands.add("sudo su");
		commands.add("tar xzf nagios-" + coreVersion + ".tar.gz");
		commands.add("cd nagios");
		commands.add("./configure --with-command-group=nagcmd --sysconfdir=/opt/USAT/nagios/conf --datadir=/opt/USAT/nagios/web --prefix=/usr/nagios-" + coreVersion);
		commands.add("make all");
		commands.add("make install");
		// commands.add("make install-init");
		commands.add("make install-config");
		commands.add("make install-commandmode");
		commands.add("ln -s /usr/nagios-" + coreVersion + " /usr/nagios-latest");
		commands.add("make install-webconf");
		commands.add("mkdir /opt/USAT/nagios/bin/");
		commands.add("/bin/cp daemon-init /opt/USAT/nagios/bin/nagios.sh");
		commands.add("chown -R nagios:nagios /opt/USAT/nagios/bin/");
		commands.add("chmod u+x /opt/USAT/nagios/bin/nagios.sh");
		// TODO: Update /opt/USAT/nagios/conf/objects/contacts.cfg (replace email address)
		commands.add("chcon -R -t httpd_sys_content_t /usr/nagios-" + coreVersion + "/sbin/");
		commands.add("chcon -R -t httpd_sys_content_t /opt/USAT/nagios/web/");
		// commands.add("htpasswd -c /opt/USAT/nagios/conf/htpasswd.users nagiosadmin");
		// TODO: update /etc/httpd/conf.d/nagios.conf (add "SSLRequireSSL")
		// commands.add("service httpd restart");
		tasks.add(new OptionalTask("sudo /usr/nagios-latest/bin/nagios --help", Pattern.compile(".*^Nagios Core " + coreVersion.replaceAll("([^\\w\\s])", "\\\\$1") + "$.*", Pattern.DOTALL | Pattern.MULTILINE), true, new UploadTask(new URL("http://prdownloads.sourceforge.net/sourceforge/nagios/nagios-" + coreVersion + ".tar.gz"), null, 0644, "root", "root", false), new ExecuteTask(commands
				.toArray(new String[commands.size()]))));

		commands.clear();
		commands.add("sudo su");
		commands.add("tar xzf nagios-plugins-" + pluginVersion + ".tar.gz");
		commands.add("cd nagios-plugins-" + pluginVersion);
		commands.add("./configure --with-nagios-user=nagios --with-nagios-group=nagios --sysconfdir=/opt/USAT/nagios/conf --datadir=/opt/USAT/nagios/web --prefix=/usr/nagios-" + coreVersion);
		commands.add("make");
		commands.add("make install");

		tasks.add(new OptionalTask("if [ `sudo du -s /usr/nagios-" + coreVersion + "/libexec/ | cut -f1` -gt 0 ]; then echo 'Y'; else echo 'N'; fi", Pattern.compile("^Y$"), true,
				new UploadTask(new URL("http://prdownloads.sourceforge.net/sourceforge/nagiosplug/nagios-plugins-" + pluginVersion + ".tar.gz"), null, 0644, "root", "root", false),
				new ExecuteTask(commands.toArray(new String[commands.size()]))));


		// TODO: create nagios program in monit
		IPTablesUpdateTask iptTask = new IPTablesUpdateTask();
		iptTask.allowTcpIn("10.0.0.110", 80);
		iptTask.allowTcpIn("10.0.0.110", 443);
		tasks.add(iptTask);
		return tasks.toArray(new DeployTask[tasks.size()]);
	}

	@Override
	public String toString() {
		return getName();
	}
}
