package com.usatech.tools.deploy.archive;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;

import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.LoopbackInterfaceMap;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;

public class RollbackNETHttpdVirtualHostChangeSet implements ChangeSet {
	
	public static final File baseDir = new File(System.getProperty("rootSourceDir", "D:\\Development\\Java Projects"));
	protected static final File buildsDir = new File(baseDir, "ApplicationBuilds");
	protected static final File serverAppConfigDir = new File(baseDir, "server_app_config");

	@Override
	public String getName() {
		return "Rollback NET Server Httpd virtual host using loopback interface Task";
	}
	
	@Override
	public String toString() {
		return getName();
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("NET");
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache)
			throws IOException {
		ArrayList<DeployTask> tasks=new ArrayList<DeployTask>();
		// 1. rollback to sysctl.conf and /sbin/sysctl -p
		ExecuteTask addToSysctlConf=new ExecuteTask("sudo su",
				"cp /etc/sysctl.conf.netbak /etc/sysctl.conf",
				"/sbin/sysctl -e -p"
			);
		
		tasks.add(addToSysctlConf);
		
		HashMap<String,String> loopbackMap=LoopbackInterfaceMap.getMapByEnv(host.getServerEnv());
		//2. remove loopback interface
		for(App subApp : new App[] { USATRegistry.DMS_APP }) {
			String subHostPrefix=subApp.getName();
			String subhostname = subHostPrefix + ("USA".equalsIgnoreCase(host.getServerEnv()) ? ".usatech.com" : "-" + host.getServerEnv().toLowerCase() + ".usatech.com");
			String subhostip = InetAddress.getByName(subhostname).getHostAddress();
			String deviceName="lo:"+loopbackMap.get(subhostip);
			tasks.add(new ExecuteTask("sudo su",
					"ifdown "+deviceName,
					"rm -rf /etc/sysconfig/network-scripts/ifcfg-"+deviceName)
					);
		}
		
		//3. rollback conf/httpd and conf.d folder files
		ExecuteTask httpdConfBak=new ExecuteTask("sudo su",
				"cp /opt/USAT/httpd/conf/httpd.conf.netbak /opt/USAT/httpd/conf/httpd.conf",
				"cp /opt/USAT/httpd/conf.d.netbak/* /opt/USAT/httpd/conf.d"
			);
		tasks.add(httpdConfBak);
		
		ExecuteTask iptablesRemoveRollback=new ExecuteTask("sudo su",
				"mv /etc/sysconfig/iptables.netbak /etc/sysconfig/iptables",
				"/etc/init.d/iptables restart"
			);
		tasks.add(iptablesRemoveRollback);
		
		// monit restart httpd
		tasks.add(new ExecuteTask("sudo su",
				"/usr/monit-latest/bin/monit restart httpd"
			));
		
		DeployTask[] deployTasks=new DeployTask[tasks.size()];
		for(int i=0; i<tasks.size();i++){
			deployTasks[i]=tasks.get(i);
		}
		return deployTasks;
	}

}
