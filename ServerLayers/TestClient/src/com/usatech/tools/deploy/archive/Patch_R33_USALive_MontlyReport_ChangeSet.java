package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.USALIVE_APP;

import java.io.File;
import java.net.UnknownHostException;


import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
import com.usatech.usalive.report.AsyncRunBasicReportStep;

public class Patch_R33_USALive_MontlyReport_ChangeSet extends AbstractLinuxPatchChangeSet {
	protected String baseDir;

	public Patch_R33_USALive_MontlyReport_ChangeSet() throws UnknownHostException {
		super();
		baseDir = System.getProperty("rootSourceDir");
		File usaliveBinDir = new File(baseDir + "/usalive/bin");
		registerClass(usaliveBinDir, AsyncRunBasicReportStep.class);
		registerResource("usalive/xsl/report-actions.xml", "classes/", "HEAD");
		registerResource("usalive/customerreporting_web/monthly_cash_credit.jsp", "web/", "HEAD");
		
	}
	protected void registerApps() {
		registerApp(USALIVE_APP);
	}
	@Override
	public String getName() {
		return "Edge Server - R33 - Usalive Monthly Report Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
