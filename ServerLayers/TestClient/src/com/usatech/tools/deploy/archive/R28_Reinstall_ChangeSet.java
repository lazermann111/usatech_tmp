package com.usatech.tools.deploy.archive;

import java.io.IOException;
import java.util.List;

import com.usatech.tools.deploy.AbstractSolarisChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;

public class R28_Reinstall_ChangeSet extends AbstractSolarisChangeSet {
	public R28_Reinstall_ChangeSet() {
		super();
		APP_LAYER.setVersion("1.14.0");
		POSM_LAYER.setVersion("1.5.0");
		INAUTH_LAYER.setVersion("1.14.0");
		NET_LAYER.setVersion("1.14.0");
		OUTAUTH_LAYER.setVersion("1.14.0");			
	}
	protected void registerApps() {
		registerApp(APP_LAYER);
		registerApp(INAUTH_LAYER);
		registerApp(POSM_LAYER);
		registerApp(NET_LAYER);
		registerApp(OUTAUTH_LAYER);
	}
	
	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
	}
	
	@Override
	protected void addTasks(Host host, App app, Integer ordinal, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addInstallAppTasks(host, app, ordinal, tasks, commands, cache);
	}
	
	@Override
	public String getName() {
		return "Edge Server - R28 Re-Install";
	}
	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
