package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import simple.falcon.engine.standard.DirectXHTMLGenerator;

import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.USATRegistry;

public class Patch_Rptgen_ChangeSet extends AbstractLinuxPatchChangeSet {

	public Patch_Rptgen_ChangeSet() throws UnknownHostException {
		super();
		registerSource("Falcon/src", DirectXHTMLGenerator.class, "HEAD");
		// registerSource("ReportGenerator/src", RecordEmailSentTask.class, "HEAD");
	}

	@Override
	public boolean isApplicable(Host host) {
		return true;
	}
	protected void registerApps() {
		registerApp(USATRegistry.RPTGEN_LAYER);
		registerApp(USATRegistry.USALIVE_APP);
	}
	@Override
	public String getName() {
		return "RptGen Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}

	/*
	@Override
	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
	}
	// */
}
