package com.usatech.tools.deploy.archive;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.AppSettingUpdateFileTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.tools.deploy.UploadAndRunPostgresSQLTask;
import com.usatech.tools.deploy.UsatSettingUpdateFileTask;

public class R33_Linux_ChangeSet extends AbstractLinuxChangeSet {
	public R33_Linux_ChangeSet() throws UnknownHostException {
		super();
	}

	@Override
	protected void registerApps() {
		registerApp(USATRegistry.POSTGRES_PGS, "9.2.3");
		registerApp(USATRegistry.POSTGRES_KLS, "9.2.3");
		registerApp(USATRegistry.POSTGRES_MST, "9.2.3");
		registerApp(USATRegistry.POSTGRES_MSR, "9.2.3");
		registerApp(USATRegistry.LOADER, "1.19.0");
		registerApp(USATRegistry.APP_LAYER, "1.19.0");
		registerApp(USATRegistry.INAUTH_LAYER, "1.19.0");
		registerApp(USATRegistry.POSM_LAYER, "1.10.0");
		registerApp(USATRegistry.NET_LAYER, "1.19.0");
		registerApp(USATRegistry.OUTAUTH_LAYER, "1.19.0");
		registerApp(USATRegistry.KEYMGR, "2.3.0");
		registerApp(USATRegistry.DMS_APP, "1.0.8");
		registerApp(USATRegistry.HTTPD_NET, "2.2.23-usat-build");
		registry.registerSubApps(USATRegistry.HTTPD_NET, USATRegistry.DMS_APP);
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareHostTasks(host, tasks, cache);
		if(host.isServerType("NET") || host.isServerType("APR"))
			tasks.add(new UsatSettingUpdateFileTask(registry.getServerSet(host.getServerEnv())));
		tasks.add(new ExecuteTask("sudo find /opt/USAT/ -name *.dmp -delete"));
	}
	
	@Override
	protected void addTasks(Host host, final App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache);
		if(USATRegistry.POSTGRES_KLS.equals(app)) {
			tasks.add(new UploadAndRunPostgresSQLTask("DatabaseScripts/releases/REL_Edge_Update_R33/KeyMgrUpdate.sql", "HEAD", false, ordinal, "km"));
		}
		if(app.getInstallFileName() != null)
			tasks.add(new AppSettingUpdateFileTask(app, ordinal, instanceNum, instanceCount, cache, registry.getServerSet(host.getServerEnv()), false));
		addInstallAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache, commands);
		if(USATRegistry.POSTGRES_PGS.equals(app)) {
			tasks.add(new UploadAndRunPostgresSQLTask("DatabaseScripts/postgresql/device_session_and_message.sql", "HEAD", false, ordinal, "main"));
		}
		if(USATRegistry.POSTGRES_MST.equals(app)) {
			tasks.add(new UploadAndRunPostgresSQLTask("DatabaseScripts/postgresql/app_cluster.consumer_acct.sql", "HEAD", false, (Integer) null, "app_" + instanceNum));
		}
	}

	@Override
	public String getName() {
		return "Edge Server - R33 Install - Apps";
	}
}
