package com.usatech.tools.deploy.archive;

import java.io.IOException;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.FromCvsUploadTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;

public class UpdateCheckStatusScript_ChangeSet implements ChangeSet {

	@Override
	public String getName() {
		return "Update CheckStatus Script";
	}

	@Override
	public boolean isApplicable(Host host) {
		return true;
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		return new DeployTask[] { 
				new FromCvsUploadTask("server_app_config/common/monit/check_app_status.sh", "HEAD", "/opt/USAT/monit/bin/check_app_status.sh", 0755, "root", "root", true)
		};
	}

	@Override
	public String toString() {
		return getName();
	}
}
