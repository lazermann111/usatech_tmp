package com.usatech.tools.deploy.archive;

import java.io.File;

import com.usatech.tools.deploy.AbstractSolarisPatchChangeSet;

import simple.mq.peer.PeerSupervisor;

public class R27F_ChangeSet extends AbstractSolarisPatchChangeSet {
	public R27F_ChangeSet() {
		super();
		appMap.remove("MST");
		registerClass(new File(System.getProperty("rootSourceDir", "D:\\Development\\Java Projects") + "\\Simple1.5\\bin"), PeerSupervisor.class);
	}
	@Override
	public String getName() {
		return "Edge Server - R27F Patch";
	}
}
