package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import simple.bean.ConvertUtils;
import simple.falcon.engine.standard.DirectXHTMLGenerator;
import simple.text.StringUtils;

import com.usatech.dms.terminal.AffectedTransListStep;
import com.usatech.report.GenerateFolioReportTask;
import com.usatech.report.ReportRequest;
import com.usatech.report.ReportRequestFactory;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R45_Patch_2_ChangeSet extends MultiLinuxPatchChangeSet {
	public R45_Patch_2_ChangeSet() throws UnknownHostException {
		super();
		// registerResource("Prepaid/src/prepaid-data-layer.xml", "classes", "REL_prepaid_1_8_0", false, USATRegistry.PREPAID_APP);
		// registerSource("Simple1.5/src", ConsumedPeerMessage.class, "REL_report_generator_3_18_1", USATRegistry.RPTGEN_LAYER);
		// registerSource("ServerLayers/AppLayer/src", UpdateAuthorizationTask.class, "REL_applayer_1_30_1", USATRegistry.LOADER);
		// registerSource("KeyManagerService/src", UpdateAccountTask.class, "REL_keymgr_2_12_1", USATRegistry.KEYMGR);
		// registerResource("ServerLayers/AppLayer/src/loader-data-layer.xml", "classes", "REL_applayer_1_31_1", false, USATRegistry.LOADER);
		// registerSource("ServerLayers/AppLayer/src", AuthorizeTask.class, "REL_applayer_1_30_1", USATRegistry.APP_LAYER);
		// registerResource("usalive/xsl/usalive-data-layer.xml", "classes", "REL_USALive_1_20_1", false, USATRegistry.USALIVE_APP);
		// registerResource("usalive/customerreporting_web/home_stat.jsp", "web", "REL_USALive_1_20_1", false, USATRegistry.USALIVE_APP);
		// registerResource("usalive/xsl/report-actions.xml", "classes", "REL_USALive_1_19_0", false, USATRegistry.USALIVE_APP);
		// registerResource("usalive/xsl/other-data-layer.xml", "classes", "REL_USALive_1_21_0", false, USATRegistry.USALIVE_APP);
		registerSource("Simple1.5/src", StringUtils.class, "REL_report_generator_3_19_1", USATRegistry.RPTGEN_LAYER, USATRegistry.USALIVE_APP, USATRegistry.PREPAID_APP, USATRegistry.DMS_APP);
		registerSource("Simple1.5/src", ConvertUtils.class, "REL_report_generator_3_19_1", USATRegistry.RPTGEN_LAYER, USATRegistry.USALIVE_APP, USATRegistry.PREPAID_APP);
		registerSource("Falcon/src", DirectXHTMLGenerator.class, "REL_report_generator_3_19_1", USATRegistry.RPTGEN_LAYER, USATRegistry.USALIVE_APP);
		registerSource("ReportGenerator/src", GenerateFolioReportTask.class, "REL_report_generator_3_19_1", USATRegistry.RPTGEN_LAYER);
		registerSource("ReportGenerator/src", ReportRequest.class, "REL_report_generator_3_19_1", USATRegistry.RPTGEN_LAYER, USATRegistry.USALIVE_APP);
		registerSource("ReportGenerator/src", ReportRequestFactory.class, "REL_report_generator_3_19_1", USATRegistry.RPTGEN_LAYER, USATRegistry.USALIVE_APP);
		registerSource("DMS/src", AffectedTransListStep.class, "REL_DMS_1_7_1", USATRegistry.DMS_APP);
		registerResource("DMS/resources/jsp/terminals/dateSelection.jsp", "web/jsp/terminals", "REL_DMS_1_7_1", false, USATRegistry.DMS_APP);
	}

	@Override
	public String getName() {
		return "R45 Patch 2";
	}
}
