package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.applayer.MessageProcessor_CA;
import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.USATRegistry;

public class Patch_AppLayer_ChangeSet extends AbstractLinuxPatchChangeSet {
	protected String baseDir;

	public Patch_AppLayer_ChangeSet() throws UnknownHostException {
		super();
		baseDir = System.getProperty("rootSourceDir");
		// File simpleBinDir = new File(baseDir + "/Simple1.5/bin");
		// File msgBinDir = new File(baseDir + "/USATMessaging/bin");
		// File applayerBinDir = new File(baseDir + "/AppLayer/bin");
		// registerClass(applayerBinDir, MessageProcessor_0206.class);
		// registerResource("ServerLayers/AppLayer/src/loader-data-layer.xml", "classes/", "HEAD");
		// registerSource("USATMessaging/src", BridgeTask.class, "REL_applayer_1_21_1");
		// registerSource("ServerLayers/LayersCommon/src", WS2CardInfoResponseMessageData.class, "HEAD");
		// registerSource("ServerLayers/LayersCommon/src", WS2RequestMessageData.class, "HEAD");
		// registerSource("ServerLayers/LayersCommon/src", WSResponseMessageData.class, "HEAD");
		// registerSource("ServerLayers/LayersCommon/src", WS2ResponseMessageData.class, "HEAD");
		// registerSource("ServerLayers/AppLayer/src", AuthorizeTask.class, "HEAD");
		// registerSource("ServerLayers/AppLayer/src", MessageProcessor_021A.class, "HEAD");

		// registerSource("ServerLayers/LayersCommon/src", EC2DataHandler.class, "HEAD");
		// registerSource("ServerLayers/LayersCommon/src", EC2ByteArrayDataSource.class, "HEAD");
		// registerSource("ServerLayers/LayersCommon/src", EC2DataHandler.class, "HEAD");
		// registerSource("ServerLayers/LayersCommon/src", EC2ByteArrayDataSource.class, "HEAD");
		// registerLib("ThirdPartyJavaLibraries/lib/axiom-api-1.2.13.jar", "HEAD");

		registerSource("ServerLayers/AppLayer/src", MessageProcessor_CA.class, "REL_applayer_1_28_1");
	}


	@Override
	public boolean isApplicable(Host host) {
		return true;
	}
	protected void registerApps() {
		registerApp(USATRegistry.APP_LAYER);
	}
	@Override
	public String getName() {
		return "AppLayer Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}

	/*
	@Override
	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
	}
	// */
}
