package com.usatech.tools.deploy.archive;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.dms.terminal.RiskAlertsStep;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DefaultHostSpecificValue;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.GetPasswordHostSpecificValue;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.PropertiesUpdateFileTask;
import com.usatech.tools.deploy.USATRegistry;

public class R47_Patch_3_ChangeSet extends MultiLinuxPatchChangeSet {
	public R47_Patch_3_ChangeSet() throws UnknownHostException {
		super();
		registerSource("DMS/src", RiskAlertsStep.class, "REL_DMS_1_9_3", USATRegistry.DMS_APP);
		registerResource("ServerLayers/POSMLayer/conf/posm-data-layer.xml", "classes", "REL_posmlayer_1_24_3", false, USATRegistry.POSM_LAYER);
		registerResource("ServerLayers/POSMLayer/conf/POSMLayerService.properties", "classes", "REL_posmlayer_1_24_3", true, USATRegistry.POSM_LAYER);
		registerResource("usalive/xsl/device-data-layer.xml", "classes", "REL_USALive_1_23_3", false, USATRegistry.USALIVE_APP);
	}
	
	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(app.equals(USATRegistry.POSM_LAYER)) {
			PropertiesUpdateFileTask puft = new PropertiesUpdateFileTask(true, 0640, app.getUserName(), app.getUserName());
			puft.setFilePath("/opt/USAT/" + app.getName() + (instance == null ? "" : instance) + "/specific/USAT_app_settings.properties");
			puft.registerValue("USAT.authoritylayer.tandem.submerchants.host", new DefaultHostSpecificValue("localhost"));
			puft.registerValue("USAT.authoritylayer.tandem.submerchants.username", new DefaultHostSpecificValue("chaseftp"));
			puft.registerValue("USAT.authoritylayer.tandem.submerchants.password", new GetPasswordHostSpecificValue(cache, "tandem.sftp.submerchants", "Chase Paymentech SubMerchants Upload SFTP", true));
			puft.registerValue("USAT.authoritylayer.tandem.submerchants.upload.directory", new DefaultHostSpecificValue("mbox"));

			tasks.add(puft);
		}
		super.addRestartAppCommands(host, app, instance, instanceNum, instanceCount, tasks, commands, cache);
	}

	@Override
	public String getName() {
		return "R47 Patch 3";
	}
}
