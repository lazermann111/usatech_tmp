package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.authoritylayer.TandemGatewayTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R46_Patch_6_ChangeSet extends MultiLinuxPatchChangeSet {
	public R46_Patch_6_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/AuthorityLayer/src/", TandemGatewayTask.class, "REL_authoritylayer_1_32_6", USATRegistry.INAUTH_LAYER);
	}

	@Override
	public String getName() {
		return "R46 Patch 6";
	}
}
