package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.HOTCHOICE_APP;
import static com.usatech.tools.deploy.USATRegistry.USALIVE_APP;
import static com.usatech.tools.deploy.USATRegistry.VERIZON_APP;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.CvsPullTask;
import com.usatech.tools.deploy.DefaultPullTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.UploadTask;

public class FixUSALiveBrandedSites_ChangeSet implements ChangeSet {

	@Override
	public String getName() {
		return "Fix USALive Branded Sites";
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("WEB");
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		List<DeployTask> tasks = new ArrayList<DeployTask>();
		List<String> commands = new ArrayList<String>();
		App[] apps = new App[] {
 HOTCHOICE_APP, VERIZON_APP
		};
		commands.add("sudo su");
		for(App app : apps) {
			app.setVersion("1.7.4");
			DefaultPullTask installFileTask = app.getRetrieveInstallFileTask();
			tasks.add(installFileTask);
			tasks.add(new UploadTask(installFileTask.getResultInputStream(), 0644, null, null, app.getInstallFileName(), true, app.getInstallFileName()));
			String appDir = "/opt/USAT/httpd/" + app.getName();
			commands.add("/bin/rm -r " + appDir + "/*");
			Set<String> staticWebFiles = new HashSet<String>(Arrays.asList(new String[] {
					"web/*.pdf",
					"web/*.ico",
					"web/*.htm",
					"web/*.html",
					"web/*.css",
					"web/*.gif",
					"web/*.png",
					"web/*.jpg",
					"web/*.js",
					"web/*.swf",
					"web/*.xls",
					"web/*.csv"
			})); //TODO: may need to change this for dms / usalive / esuds
			StringBuilder sb = new StringBuilder();
			if(USALIVE_APP.equals(app)) {
				commands.add(getMakeDir(appDir+"/CustomerReporting", app.getUserName(), app.getUserName(), null));
				sb.append("gunzip -c ").append(app.getInstallFileName()).append(" | tar -C ").append(appDir).append("/CustomerReporting").append(" -xv --strip-components=1");
				for(String staticWebFile : staticWebFiles)
					sb.append(' ').append(staticWebFile);
				commands.add(sb.toString());
				commands.add("/bin/cp --preserve " + appDir + "/CustomerReporting/favicon.ico " + appDir);
				commands.add(new StringBuilder("gunzip -c ").append(app.getInstallFileName()).append(" | tar -C ").append(appDir).append(" -xv version.txt").toString());
			} else if(HOTCHOICE_APP.equals(app) || VERIZON_APP.equals(app)) {
				commands.add(getMakeDir(appDir+"/CustomerReporting", app.getUserName(), app.getUserName(), null));
				sb.append("gunzip -c ").append(app.getInstallFileName()).append(" | tar -C ").append(appDir).append("/CustomerReporting -xv");
				commands.add(sb.toString());
				commands.add("/bin/cp --preserve " + appDir + "/CustomerReporting/favicon.ico " + appDir);
			} else {
				sb.append("gunzip -c ").append(app.getInstallFileName()).append(" | tar -C ").append(appDir).append(" -xv --strip-components=1");
				for(String staticWebFile : staticWebFiles)
					sb.append(' ').append(staticWebFile);
				commands.add(sb.toString());
				commands.add(new StringBuilder("gunzip -c ").append(app.getInstallFileName()).append(" | tar -C ").append(appDir).append(" -xv version.txt").toString());
			}
			commands.add("chown -R " + app.getUserName() + ':' + app.getUserName() + " " + appDir);
		}
		
		if(!commands.isEmpty()) {
			// commands.add("kill -s SIGHUP `cat /opt/USAT/httpd/logs/httpd.pid`");
			tasks.add(new ExecuteTask(commands.toArray(new String[commands.size()])));
		}
		return tasks.toArray(new DeployTask[tasks.size()]);
	}

	protected String getMakeDir(String dir, String owner, String group, Integer permissions) {
		StringBuilder sb = new StringBuilder();
		sb.append("test -d ").append(dir).append(" || (mkdir ");
		if(permissions != null && permissions > -1)
			sb.append("-m ").append(Integer.toOctalString(permissions)).append(' ');
		sb.append(dir).append(" && chown ").append(owner).append(':').append(group).append(' ').append(dir).append(')');
		return sb.toString();
	}

	@Override
	public String toString() {
		return getName();
	}
}
