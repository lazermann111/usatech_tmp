package com.usatech.tools.deploy.archive;

import java.util.List;

import com.usatech.tools.deploy.AbstractSolarisChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DefaultHostSpecificValue;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.HostSpecificValue;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.PropertiesUpdateFileTask;

import simple.io.Interaction;
import simple.text.StringUtils;
import simple.util.CollectionUtils;

public class R28_2_ChangeSet extends AbstractSolarisChangeSet {
	protected final int[] instances = new int[] {3,4};
	public R28_2_ChangeSet() {
		super();
		Integer[] ords = new Integer[instances.length];
		for(int i = 0; i < instances.length; i++)
			ords[i] = instances[i];
		ordMap.put("DEV", ords);
		ordMap.put("INT", ords);
		ordMap.put("ECC", ords);		
	}
	@Override
	public boolean isApplicable(Host host) {
		boolean hasServerType = false;
		for(String serverType : host.getServerTypes()) {
			if(appMap.containsKey(serverType)) {
				hasServerType = true;
				break;
			}
		}
		if(!hasServerType)
			return false;
		Integer[] ords = ordMap.get(host.getServerEnv());
		if(ords == null || ords.length == 0)
			return false;
		if(ords[0] == null && ords.length == 1)
			return CollectionUtils.iterativeSearch(instances, Integer.parseInt(host.getSimpleName().substring(7))) >= 0;
		return true;
	}
	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) {
		commands.add("sudo su");
	}

	@Override
	protected void addTasks(Host host, App app, Integer ordinal, List<DeployTask> tasks, List<String> commands, PasswordCache cache) {
		commands.add("svcadm -v restart " + app.getName() + (ordinal == null ? "" : ordinal.toString()));
		String filePath;
		String copyPath;
		final int instance;
		if(ordinal != null) {
			copyPath = app.getName() + ordinal + "_USAT_environment_settings.properties";
			filePath = "/opt/USAT/" + app.getName() + ordinal + "/classes/USAT_environment_settings.properties";
			instance = ordinal;
		} else if("netlayer".equalsIgnoreCase(app.getName()) || "applayer".equalsIgnoreCase(app.getName())) {
			copyPath = "USAT_environment_settings.properties";
			filePath = "/opt/USAT/conf/USAT_environment_settings.properties";
			instance = Integer.parseInt(host.getSimpleName().substring(7));
		} else
			return;
		tasks.add(new ExecuteTask("sudo su", "cp " + filePath + " " + copyPath, "chmod 666 " + copyPath));
		
		PropertiesUpdateFileTask usatUpdateTask = new PropertiesUpdateFileTask(true, 0640, "root", "usat");
		usatUpdateTask.setFilePath(copyPath);
		usatUpdateTask.registerValue("USAT.simplemq.secondaryDataSources", new HostSpecificValue() {
			public String getValue(Host host, Interaction interaction) {
				return StringUtils.generateList("MQ_", null, ",", instance + 1, 1, 3, 4);
			}
			public boolean isOverriding() {
				return true;
			}
		});
		int[] cs;
		switch(instance) {
			//case 1: cs = new int[]{2,3,4}; break;
			//case 2: cs = new int[]{1,2,3}; break;
			case 3: cs = new int[]{2,3}; break;
			case 4: cs = new int[]{1,2}; break;
			default: cs = new int[0];
		}
		for(int c : cs) {
			for(App a : apps) {
				if(!"postgres".equalsIgnoreCase(a.getName()))
					usatUpdateTask.registerValue("USAT.simplemq.directPollerUrl." + a.getName().toLowerCase() + "_s" + c, new DefaultHostSpecificValue(""));
			}
		}
		tasks.add(usatUpdateTask);
		tasks.add(new ExecuteTask("sudo su", "chmod 640 " + copyPath, "mv " + copyPath + " " + filePath, "chown root:usat " + filePath));	
	}

	@Override
	public String getName() {
		return "Edge Server - R28 Install (Step 2)";
	}
}
