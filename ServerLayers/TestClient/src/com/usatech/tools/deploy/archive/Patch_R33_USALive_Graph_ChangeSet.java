package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.USALIVE_APP;

import java.io.File;
import java.net.UnknownHostException;


import com.usatech.report.build.BuildAnnualCashlessFolio;
import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;

public class Patch_R33_USALive_Graph_ChangeSet extends AbstractLinuxPatchChangeSet {
	protected String baseDir;

	public Patch_R33_USALive_Graph_ChangeSet() throws UnknownHostException {
		super();
		baseDir = System.getProperty("rootSourceDir");
		File simpleBinDir = new File(baseDir + "/usalive/bin");
		registerClass(simpleBinDir, BuildAnnualCashlessFolio.class);
		registerResource("usalive/xsl/selection-actions.xml", "classes/", "HEAD");
		registerResource("usalive/xsl/templates/report/activity_total_cashless_parameters.xsl", "classes/templates/report/", "HEAD");
		registerResource("usalive/customerreporting_web/home_stat.jsp", "web/", "HEAD");
		registerResource("ReportGenerator/src/com/usatech/report/build/BuildReportPillars.properties", "classes/com/usatech/report/build", "HEAD");
		registerResource("usalive/xsl/report-actions.xml", "classes/", "HEAD");
	}
	protected void registerApps() {
		registerApp(USALIVE_APP);
	}
	@Override
	public String getName() {
		return "Edge Server - R33 - Usalive Graph Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
