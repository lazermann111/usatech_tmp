package com.usatech.tools.deploy.archive;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.LoopbackInterfaceMap;
import com.usatech.tools.deploy.PasswordCache;

public class RollbackHttpdVirtualHostChangeSet implements ChangeSet {
	
	public static final File baseDir = new File(System.getProperty("rootSourceDir", "D:\\Development\\Java Projects"));
	protected static final File buildsDir = new File(baseDir, "ApplicationBuilds");
	protected static final File serverAppConfigDir = new File(baseDir, "server_app_config");
	public static final App HTTPD_WEB = new App("httpd", "WEB", "server_app_config/WEB/Httpd/linux", "httpd-", 0, 1 /*bin*/, "httpd", "httpd", null, "bin");
	public static final App USALIVE_APP = new App("usalive", "APP", "ApplicationBuilds/USALive", "usalive-", 1100, 951, "USALive", "USALive", "usalive");
	public static final App ESUDS_APP = new App("esuds", "APP", "ApplicationBuilds/eSudsWebsite", "esuds-", 1200, 952, "eSuds", "eSuds", "esuds");
	public static final App USATECH_WEBSITE_APP = new App("usatech", "WEB", "ApplicationBuilds/usatech", "usatech-", 1300, 953, "usatech", "usatech", "usatech");
	public static final App ENERGYMISERS_WEBSITE_APP = new App("energymisers", "WEB", "ApplicationBuilds/energymisers", "energymisers-", 1400, 954, "energymisers", "energymisers", "energymisers");
	public static final App HOTCHOICE_APP = new App("hotchoice", "WEB", "ApplicationBuilds/USALive", "hotchoice-", 1100, 951, "USALive", "hotchoice", "usalive");
	public static final App VERIZON_APP = new App("verizon", "WEB", "ApplicationBuilds/USALive", "verizon-", 1100, 951, "USALive", "verizon", "usalive");
	public static final Map<String,String> subHostPrefixMap=new HashMap<String,String>();
	
	static{
		//do energymisers seperately because it shares ip address with usatech website
		subHostPrefixMap.put("usatech", "www");
		subHostPrefixMap.put("esuds", "esudsweb");
	}
	

	@Override
	public String getName() {
		return "Rollback Httpd virtual host using loopback interface Task";
	}
	
	@Override
	public String toString() {
		return getName();
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("WEB");
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache)
			throws IOException {
		ArrayList<DeployTask> tasks=new ArrayList<DeployTask>();
		// 1. rollback to sysctl.conf and /sbin/sysctl -p
		ExecuteTask addToSysctlConf=new ExecuteTask("sudo su",
				"cp /etc/sysctl.conf.bak /etc/sysctl.conf",
				"/sbin/sysctl -e -p"
			);
		
		tasks.add(addToSysctlConf);
		
		HashMap<String,String> loopbackMap=LoopbackInterfaceMap.getMapByEnv(host.getServerEnv());
		//2. remove loopback interface
		for(App subApp : new App[] { USALIVE_APP, ESUDS_APP, USATECH_WEBSITE_APP, HOTCHOICE_APP, VERIZON_APP }) {
			String subHostPrefix=subHostPrefixMap.get(subApp.getName());
			if(subHostPrefix==null){
				subHostPrefix=subApp.getName();
			}
			String subhostname = subHostPrefix + ("USA".equalsIgnoreCase(host.getServerEnv()) ? ".usatech.com" : "-" + host.getServerEnv().toLowerCase() + ".usatech.com");
			String subhostip = InetAddress.getByName(subhostname).getHostAddress();
			String deviceName="lo:"+loopbackMap.get(subhostip);
			tasks.add(new ExecuteTask("sudo su",
					"ifdown "+deviceName,
					"rm -rf /etc/sysconfig/network-scripts/ifcfg-"+deviceName)
					);
		}
		
		//3. rollback conf/httpd and conf.d folder files
		ExecuteTask httpdConfBak=new ExecuteTask("sudo su",
				"cp /opt/USAT/httpd/conf/httpd.conf.bak /opt/USAT/httpd/conf/httpd.conf",
				"cp /opt/USAT/httpd/conf.d.bak/* /opt/USAT/httpd/conf.d"
			);
		tasks.add(httpdConfBak);
		
		ExecuteTask iptablesRemoveRollback=new ExecuteTask("sudo su",
				"mv /etc/sysconfig/iptables.bak /etc/sysconfig/iptables",
				"/etc/init.d/iptables restart"
			);
		tasks.add(iptablesRemoveRollback);
		
		// monit restart httpd
		tasks.add(new ExecuteTask("sudo su",
				"/usr/monit-latest/bin/monit restart httpd"
			));
		
		DeployTask[] deployTasks=new DeployTask[tasks.size()];
		for(int i=0; i<tasks.size();i++){
			deployTasks[i]=tasks.get(i);
		}
		return deployTasks;
	}

}
