package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.baseDir;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import simple.app.Processor;
import simple.app.ServiceException;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.AppSettingUpdateFileTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.DeployTaskInfo;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.OptionalTask;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.SyncWithJKSTask;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.tools.deploy.UploadAndRunPostgresSQLTask;
import com.usatech.tools.deploy.UploadTask;
import com.usatech.tools.deploy.UsatSettingUpdateFileTask;

public class R43_Linux_ChangeSet extends AbstractLinuxChangeSet {
	protected static final Processor<DeployTaskInfo, InputStream> EMPTY_RESOURCE = new Processor<DeployTaskInfo, InputStream>() {
		public InputStream process(DeployTaskInfo argument) throws ServiceException {
			return new ByteArrayInputStream(new byte[0]);
		}

		public Class<InputStream> getReturnType() {
			return InputStream.class;
		}

		public Class<DeployTaskInfo> getArgumentType() {
			return DeployTaskInfo.class;
		}
	};

	public R43_Linux_ChangeSet() throws UnknownHostException {
		super();
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("PGS") || host.isServerType("MST") || host.isServerType("MSR") || super.isApplicable(host);
	}

	@Override
	protected void registerApps() {
		registerApp(USATRegistry.KEYMGR, "2.11.0");
		registerApp(USATRegistry.LOADER, "1.29.0");
		registerApp(USATRegistry.APP_LAYER, "1.29.0");
		registerApp(USATRegistry.INAUTH_LAYER, "1.29.0");
		registerApp(USATRegistry.POSM_LAYER, "1.20.0");
		registerApp(USATRegistry.NET_LAYER, "1.29.0");
		registerApp(USATRegistry.OUTAUTH_LAYER, "1.29.0");
		registerApp(USATRegistry.DMS_APP, "1.5.0");
		registerApp(USATRegistry.HTTPD_NET, null);
		registry.registerSubApps(USATRegistry.HTTPD_NET, USATRegistry.DMS_APP);
		registerApp(USATRegistry.RDW_LOADER, "3.17.0");
		registerApp(USATRegistry.RPTGEN_LAYER, "3.17.0");
		registerApp(USATRegistry.RPTREQ_LAYER, "3.17.0");
		registerApp(USATRegistry.USALIVE_APP, "1.19.0");
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.USALIVE_APP);
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.VERIZON_APP);
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.HOTCHOICE_APP);
		registerApp(USATRegistry.PREPAID_APP, "1.7.0");
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.GETMORE_APP); // The APP server needs PREPAID_APP, but the WEB server also needs the GETMORE_APP as subapp
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.PREPAID_APP);
		registerApp(USATRegistry.HTTPD_WEB, null);
		registerApp(USATRegistry.TRANSPORT_LAYER, "3.17.0");
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(host.isServerType("MST")) {
			String[] instances = registry.getServerSet(host.getServerEnv()).getServer(host, "MST").instances;
			int count = instances == null ? 1 : instances.length;
			int n = (host.getSimpleName().charAt(host.getSimpleName().length() - 1) - '0') - 1;
			for(int i = 0; i < count; i++) {
				String mqdb;
				String appdb = "app_" + String.valueOf(n * count + i + 1);
				Integer ordinal;
				if("LOCAL".equalsIgnoreCase(host.getServerEnv())) {
					mqdb = instances[i].substring(0, 2) + "mq" + instances[i].substring(2);
					ordinal = null;
				} else {
					mqdb = "mq";
					ordinal = (instances == null || instances[0] == null ? null : i + 1);
				}
				tasks.add(new UploadAndRunPostgresSQLTask("DatabaseScripts/releases/REL_Edge_Update_R43/ElavonUpdates.APP.1.sql", "HEAD", Boolean.FALSE, ordinal, appdb));
				tasks.add(new UploadAndRunPostgresSQLTask("DatabaseScripts/releases/REL_Edge_Update_R43/R43.MQ.1.sql", "HEAD", Boolean.FALSE, ordinal, mqdb));
			}
		} else if(host.isServerType("MSR")) {
			tasks.add(new UploadAndRunPostgresSQLTask("DatabaseScripts/releases/REL_Edge_Update_R43/R43.MQ.1.sql", "HEAD", Boolean.FALSE, "mq"));
		} else if(host.isServerType("PGS")) {
			tasks.add(new UploadAndRunPostgresSQLTask("DatabaseScripts/releases/REL_Edge_Update_R43/R43.MAIN.1.sql", "HEAD", Boolean.FALSE, "main"));
		} else {
			tasks.add(new ExecuteTask("sudo su", "ls -1d /usr/java/jdk1.6.0_* | head -n -1 | xargs -r /bin/rm -r")); // clean up old javas
			final int majorVersion = 8;
			final int minorVersion = 20;
			final String fileName = "jdk-" + majorVersion + "u" + minorVersion + "-linux-x64.gz";
			final File versionDir = new File(baseDir, "server_app_config/java/jdk" + majorVersion);
			StringBuilder sb = new StringBuilder();
			sb.append("/usr/java/jdk1.").append(majorVersion).append(".0_");
			StringUtils.appendPadded(sb, String.valueOf(minorVersion), '0', 2, Justification.RIGHT);
			final String remoteDir = sb.toString();
			List<DeployTask> subtasks = new ArrayList<DeployTask>();
			subtasks.add(new UploadTask(new File(versionDir, fileName), 0755));
			subtasks.add(new ExecuteTask("sudo su", "cd /usr/java", "tar -xzf /home/`logname`/" + fileName, "chown -R root:root " + remoteDir, "unlink /usr/jdk/latest", "ln -s " + remoteDir + " /usr/jdk/latest", "/bin/rm -f /home/`logname`/" + fileName));
			subtasks.add(new UploadTask(new File(versionDir, "local_policy.jar"), remoteDir + "/jre/lib/security", 0644, "root", "root"));
			subtasks.add(new UploadTask(new File(versionDir, "US_export_policy.jar"), remoteDir + "/jre/lib/security", 0644, "root", "root"));
			if(host.isServerType("NET") || host.isServerType("WEB"))
				subtasks.add(new SyncWithJKSTask(remoteDir + "/jre/lib/security/cacerts", "/opt/USAT/conf/truststore.ts", cache, 0640, "__jdk__"));
			else if(host.isServerType("APP"))
				subtasks.add(new SyncWithJKSTask(remoteDir + "/jre/lib/security/cacerts", "/opt/USAT/conf/truststore.ts", cache, 0640, "__jdk__", Collections.singleton("addtrustexternalca")));
			tasks.add(new OptionalTask("if [ ! -x " + remoteDir + " ]; then echo 'Y'; fi", Pattern.compile("^Y\\s*"), false, subtasks.toArray(new DeployTask[subtasks.size()])));

			if(host.isServerType("KLS"))
				tasks.add(new UploadAndRunPostgresSQLTask("DatabaseScripts/releases/REL_Edge_Update_R43/R43.KM.1.sql", "HEAD", Boolean.FALSE, "km"));
			addPrepareHostTasks(host, tasks, cache);
			tasks.add(new UsatSettingUpdateFileTask(registry.getServerSet(host.getServerEnv())));
			tasks.add(new ExecuteTask("sudo find /opt/USAT/ -name '*.dmp' -delete"));
		}
	}
	
	@Override
	protected void addTasks(Host host, final App app, Integer ordinal, String instance, final int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(USATRegistry.KEYMGR.equals(app) && host.isServerType("MST"))
			return;
		addPrepareAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache);
		if(app.getInstallFileName() != null)
			tasks.add(new AppSettingUpdateFileTask(app, ordinal, instanceNum, instanceCount, cache, registry.getServerSet(host.getServerEnv()), false));
		addInstallAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache, commands);
	}

	@Override
	public String getName() {
		return "Edge Server - R43 Install - Apps";
	}
}
