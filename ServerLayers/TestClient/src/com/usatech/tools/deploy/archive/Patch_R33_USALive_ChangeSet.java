package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.USALIVE_APP;

import java.io.File;
import java.net.UnknownHostException;


import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
import com.usatech.usalive.report.TestTransportStep;

public class Patch_R33_USALive_ChangeSet extends AbstractLinuxPatchChangeSet {
	protected String baseDir;

	public Patch_R33_USALive_ChangeSet() throws UnknownHostException {
		super();
		baseDir = System.getProperty("rootSourceDir");
		File simpleBinDir = new File(baseDir + "/usalive/bin");
		registerClass(simpleBinDir, TestTransportStep.class);
		//registerResource("usalive/xsl/payment-actions.xml", "classes", "HEAD");
	}
	protected void registerApps() {
		registerApp(USALIVE_APP);
	}
	@Override
	public String getName() {
		return "Edge Server - R33 - Usalive Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
