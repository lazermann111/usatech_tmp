package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.APP_LAYER;
import static com.usatech.tools.deploy.USATRegistry.INAUTH_LAYER;
import static com.usatech.tools.deploy.USATRegistry.POSM_LAYER;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DefaultHostSpecificValue;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.PropertiesUpdateFileTask;

public class SwitchOverToUSAPDB_ChangeSet implements ChangeSet {
	protected static final File baseDir = new File(System.getProperty("rootSourceDir", "D:\\Development\\Java Projects"));
	
	public SwitchOverToUSAPDB_ChangeSet() {
	}
	@Override
	public String getName() {
		return "Switch over to USAPDB";
	}

	@Override
	public boolean isApplicable(Host host) {
		return "USA".equalsIgnoreCase(host.getServerEnv()) && (host.isServerType("APP_OLD") || host.isServerType("RPT_OLD") || host.isServerType("APR"));
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		List<String> commands = new ArrayList<String>();
		commands.add("sudo su");
		List<DeployTask> tasks = new ArrayList<DeployTask>();
		PropertiesUpdateFileTask usatEnvFileTask = new PropertiesUpdateFileTask(true, 0640, "root", "usat");
		usatEnvFileTask.setFilePath("/opt/USAT/conf/USAT_environment_settings.properties");
		usatEnvFileTask.registerValue("USAT.database.REPORT.url", new DefaultHostSpecificValue("jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=oradbscan01.trooper.usatech.com)(PORT=1535))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=usapdb.world)(FAILOVER_MODE=(TYPE=select)(METHOD=basic))))"));
		tasks.add(usatEnvFileTask);
		if(host.isServerType("APP_OLD")) {
			commands.add("if [ $(grep -c \" *dsn *=> *\'USARDB'\" /opt/jobs/CCE_Fill_Reconciliation/OOCGI/Query/Config.pm) -gt 0 ]; then cp /opt/jobs/CCE_Fill_Reconciliation/OOCGI/Query/Config.pm /opt/jobs/CCE_Fill_Reconciliation/OOCGI/Query/Config.pm.BAK; sed \"s/\\( *dsn *=> *\\)'USARDB'/\\1'USAPDB'/\" /opt/jobs/CCE_Fill_Reconciliation/OOCGI/Query/Config.pm.BAK > /opt/jobs/CCE_Fill_Reconciliation/OOCGI/Query/Config.pm; fi");
			commands.add("svcadm -v restart usalive");
			commands.add("svcadm -v restart esuds");
		}
		if(host.isServerType("RPT_OLD")) {
			commands.add("svcadm -v restart rptgen");
		}
		if(host.isServerType("APR")) {
			for(App app : new App[] { APP_LAYER, INAUTH_LAYER, POSM_LAYER }) {
				PropertiesUpdateFileTask usatAppFileTask = new PropertiesUpdateFileTask(true, 0640, app.getUserName(), app.getUserName());
				usatAppFileTask.setFilePath("/opt/USAT/" + app.getName() + "/specific/USAT_app_settings.properties");
				StringBuilder sb = new StringBuilder();
				sb.append("{.*:5432=-,.*:636=-");
				if(app == APP_LAYER)
					sb.append(",.*:443=applayer.keymanager");
				else if(app == POSM_LAYER)
					sb.append(",.*:443=posmlayer.keymanager");
				sb.append("}");
				usatAppFileTask.registerValue("javax.net.ssl.clientKeyAliasMap", new DefaultHostSpecificValue(sb.toString()));
				tasks.add(usatAppFileTask);

			}

			commands.add("initctl restart USAT/applayer");
			commands.add("initctl restart USAT/posmlayer");
			commands.add("/usr/monit-latest/bin/monit -v restart dms");
		}
		if(commands.size() > 1)
			tasks.add(new ExecuteTask(commands.toArray(new String[commands.size()])));
		/*
		commands.add("mkdir -p tools/classes/simple/test");
		commands.add("mv JDBCConnectionTest.class tools/classes/simple/test");
		Set<String> dbUsers = new HashSet<String>();
		String oracleLib = null;
		if(host.isServerType("APP_OLD")) {
			dbUsers.add("USALIVE_APP");
			dbUsers.add("FOLIO_CONF");
			dbUsers.add("usat_custom");
			oracleLib = "/opt/USAT/usalive/lib/ojdbc6_g-11.2.0.1.0.jar";
		}
		if(host.isServerType("RPT_OLD")) {
			dbUsers.add("RPT_GEN_APP");
			dbUsers.add("FOLIO_CONF");	
			oracleLib = "/opt/USAT/rptgen/lib/ojdbc6_g-11.2.0.1.0.jar";
		}
		if(host.isServerType("APR")) {
			dbUsers.add("APP_LAYER_" + host.getSimpleName().charAt(7));			
			dbUsers.add("POSM_LAYER_" + host.getSimpleName().charAt(7));
			dbUsers.add("DMS_USER_" + host.getSimpleName().charAt(7));
			oracleLib = "/opt/USAT/applayer/lib/ojdbc6_g-11.2.0.3.jar";
			IPTablesUpdateTask iptTask = new IPTablesUpdateTask("INPUT", "OUTPUT", null);
			iptTask.allowTcpOutWithRelated("oradbscan01.trooper.usatech.com", 1535, new String[] { "usadb031-vip.trooper.usatech.com", "usadb032-vip.trooper.usatech.com", "usadb033-vip.trooper.usatech.com", "usadb034-vip.trooper.usatech.com" }, 1521); // oracle

			tasks.add(iptTask);
		}
		for(String dbUser : dbUsers)
			commands.add("/usr/jdk/latest/bin/java -cp tools/classes:" + oracleLib + " simple.test.JDBCConnectionTest oracle.jdbc.driver.OracleDriver 'jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=oradbscan01.trooper.usatech.com)(PORT=1535))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=usapdb.world)(FAILOVER_MODE=(TYPE=select)(METHOD=basic))))' " + dbUser);
		tasks.add(new UploadTask(new File(baseDir, "Simple1.5/bin/simple/test/JDBCConnectionTest.class"), 0644));
		tasks.add(new ExecuteTask(commands.toArray(new String[commands.size()])) {
				@Override
				protected Processor<String, String> getInteractiveInput(final DeployTaskInfo deployTaskInfo) {
					return new Processor<String, String>() {
						@Override
						public Class<String> getArgumentType() {
							return String.class;
						}
		
						@Override
						public Class<String> getReturnType() {
							return String.class;
						}
		
						@Override
						public String process(String argument) throws ServiceException {
							return new String(DeployUtils.getPassword(deployTaskInfo, argument, argument));
						}
					};
				}
		});*/
		return tasks.toArray(new DeployTask[tasks.size()]);
	}

	@Override
	public String toString() {
		return getName();
	}
}
