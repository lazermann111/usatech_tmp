package com.usatech.tools.deploy.archive;

import java.io.File;

import com.usatech.posm.tasks.POSMSettlementFeedbackTask;
import com.usatech.posm.tasks.POSMSettlementUpdateTask;
import com.usatech.tools.deploy.AbstractSolarisPatchChangeSet;

public class R27D_ChangeSet extends AbstractSolarisPatchChangeSet {
	public R27D_ChangeSet() {
		super();
		registerClass(new File(System.getProperty("rootSourceDir", "D:\\Development\\Java Projects") + "\\POSMLayer\\bin"), POSMSettlementFeedbackTask.class);
		registerClass(new File(System.getProperty("rootSourceDir", "D:\\Development\\Java Projects") + "\\POSMLayer\\bin"), POSMSettlementUpdateTask.class);
	}
	@Override
	protected void registerApps() {
		registerApp(POSM_LAYER);
	}
	@Override
	public String getName() {
		return "Edge Server - R27D Patch";
	}
}
