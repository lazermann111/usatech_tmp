package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.PREPAID_APP;

import java.io.File;
import java.net.UnknownHostException;


import com.usatech.prepaid.PSRequestHandler;
import com.usatech.prepaid.PrepaidHessianServlet;
import com.usatech.prepaid.web.PrepaidUtils;
import com.usatech.ps.GetPostalInfo;
import com.usatech.ps.PSRequest;
import com.usatech.ps.PrepaidServiceAPI;
import com.usatech.ps.PsSkeleton;
import com.usatech.ps.PsStub;
import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;

public class Patch_R34_Prepaid_ChangeSet extends AbstractLinuxPatchChangeSet {
	protected String baseDir;

	public Patch_R34_Prepaid_ChangeSet() throws UnknownHostException {
		super();
		baseDir = System.getProperty("rootSourceDir");
		File dir = new File(baseDir + "/Prepaid/bin");
		registerClass(dir, PrepaidServiceAPI.class);
		registerClass(dir, GetPostalInfo.class);
		registerClass(dir, PSRequest.class);
		registerClass(dir, PsSkeleton.class);
		registerClass(dir, PsStub.class);
		registerClass(dir, PSRequestHandler.class);
		registerClass(dir, PrepaidHessianServlet.class);
		registerClass(dir, PrepaidUtils.class);
		registerResource("Prepaid/web/WEB-INF/services/ps/META-INF/ps.wsdl", "web/WEB-INF/services/ps/META-INF", "HEAD");
	}
	protected void registerApps() {
		registerApp(PREPAID_APP);
	}
	@Override
	public String getName() {
		return "Edge Server - R34 - Prepaid Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
