package com.usatech.tools.deploy.archive;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.AppSettingUpdateFileTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.FromCvsUploadTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.tools.deploy.UploadAndRunPostgresSQLTask;
import com.usatech.tools.deploy.UsatSettingUpdateFileTask;

public class R34_Linux_ChangeSet extends AbstractLinuxChangeSet {
	public R34_Linux_ChangeSet() throws UnknownHostException {
		super();
	}

	@Override
	protected void registerApps() {
		registerApp(USATRegistry.POSTGRES_PGS, "9.2.4");
		registerApp(USATRegistry.POSTGRES_KLS, "9.2.4");
		registerApp(USATRegistry.POSTGRES_MST, "9.2.4");
		registerApp(USATRegistry.POSTGRES_MSR, "9.2.4");
		registerApp(USATRegistry.LOADER, "1.20.0");
		registerApp(USATRegistry.APP_LAYER, "1.20.0");
		registerApp(USATRegistry.INAUTH_LAYER, "1.20.0");
		registerApp(USATRegistry.POSM_LAYER, "1.11.0");
		registerApp(USATRegistry.NET_LAYER, "1.20.0");
		registerApp(USATRegistry.OUTAUTH_LAYER, "1.20.0");
		registerApp(USATRegistry.KEYMGR, "2.4.0");
		registerApp(USATRegistry.DMS_APP, "1.0.9");
		registerApp(USATRegistry.HTTPD_NET, null);
		registry.registerSubApps(USATRegistry.HTTPD_NET, USATRegistry.DMS_APP);
		registerApp(USATRegistry.RPTGEN_LAYER, "3.9.0");
		registerApp(USATRegistry.RPTREQ_LAYER, "3.9.0");
		registerApp(USATRegistry.TRANSPORT_LAYER, "3.9.0");
		registerApp(USATRegistry.USALIVE_APP, "1.10.0");
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.USALIVE_APP);
		registerApp(USATRegistry.HTTPD_WEB, null);
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareHostTasks(host, tasks, cache);
		if(host.isServerType("NET") || host.isServerType("APR"))
			tasks.add(new UsatSettingUpdateFileTask(registry.getServerSet(host.getServerEnv())));
		if(host.isServerType("APP"))
			tasks.add(new FromCvsUploadTask("eSudsWebsite/conf/esuds-data-layer.xml", "HEAD", "/opt/USAT/esuds/classes/esuds-data-layer.xml", 0644, "esuds", "esuds", true));
		tasks.add(new ExecuteTask("sudo find /opt/USAT/ -name *.dmp -delete"));
	}
	
	@Override
	protected void addTasks(Host host, final App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache);
		if(app.getInstallFileName() != null)
			tasks.add(new AppSettingUpdateFileTask(app, ordinal, instanceNum, instanceCount, cache, registry.getServerSet(host.getServerEnv()), false));
		addInstallAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache, commands);
		if(USATRegistry.POSTGRES_MST.equals(app) || USATRegistry.POSTGRES_MSR.equals(app)) {
			tasks.add(new UploadAndRunPostgresSQLTask("Simple1.5/src/simple/mq/peer/mq_peer_postgres_setup_5.sql", "HEAD", false, ordinal, "mq"));
			if(USATRegistry.POSTGRES_MST.equals(app))
				tasks.add(new UploadAndRunPostgresSQLTask("DatabaseScripts/releases/REL_Edge_Update_R34/R34.APP.1.sql", "HEAD", false, (Integer) null, "app_" + instanceNum));
		} else if(USATRegistry.POSTGRES_PGS.equals(app)) {
			tasks.add(new UploadAndRunPostgresSQLTask("DatabaseScripts/releases/REL_Edge_Update_R34/R34.MAIN.1.sql", "HEAD", false, ordinal, "main"));
			tasks.add(new UploadAndRunPostgresSQLTask("DatabaseScripts/postgresql/device_session_and_message.sql", "HEAD", false, ordinal, "main"));
		}			
	}

	@Override
	public String getName() {
		return "Edge Server - R34 Install - Apps";
	}
}
