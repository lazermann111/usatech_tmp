package com.usatech.tools.deploy.archive;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import simple.event.LogTimingTaskListener;
import simple.event.NadaTaskListener;
import simple.falcon.engine.standard.DirectXHTMLGenerator;

import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;

public class Patch_Sodexo_Report_ChangeSet extends AbstractLinuxPatchChangeSet {
	public Patch_Sodexo_Report_ChangeSet() throws UnknownHostException {
		super();
		registerSource("Simple1.5/src", LogTimingTaskListener.class, "REL_SODEXO_REPORT");
		registerSource("Simple1.5/src", NadaTaskListener.class, "REL_SODEXO_REPORT");
		registerSource("Falcon/src", DirectXHTMLGenerator.class, "REL_SODEXO_REPORT");
		
	}
	protected void registerApps() {
		registerApp(USATRegistry.USALIVE_APP);
		registerApp(USATRegistry.RPTGEN_LAYER);
	}

	@Override
	public String getName() {
		return "Sodexo Report Falcon Patch";
	}
	
	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
	@Override
	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
	}
}
