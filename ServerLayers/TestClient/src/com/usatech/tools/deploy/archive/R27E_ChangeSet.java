package com.usatech.tools.deploy.archive;

import java.io.File;
import java.io.IOException;

import com.usatech.authoritylayer.POSGatewayTask;
import com.usatech.tools.deploy.AbstractSolarisPatchChangeSet;

public class R27E_ChangeSet extends AbstractSolarisPatchChangeSet {
	public R27E_ChangeSet() throws IOException {
		super();
		registerClass(new File(System.getProperty("rootSourceDir", "D:\\Development\\Java Projects") + "\\AuthorityLayer\\bin").getCanonicalFile(), POSGatewayTask.class);
	}
	@Override
	protected void registerApps() {
		registerApp(OUTAUTH_LAYER);
	}
	@Override
	public String getName() {
		return "Edge Server - R27E Patch";
	}
}
