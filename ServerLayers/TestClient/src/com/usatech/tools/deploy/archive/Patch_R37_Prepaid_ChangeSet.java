package com.usatech.tools.deploy.archive;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.ps.PsStub;
import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;

public class Patch_R37_Prepaid_ChangeSet extends AbstractLinuxPatchChangeSet {
	public Patch_R37_Prepaid_ChangeSet() throws UnknownHostException {
		super();
		registerSource("Prepaid/src", com.usatech.ps.PSPrepaidActivity.class, "REL_prepaid_1_3_0");
		registerSource("Prepaid/src", PsStub.class, "REL_prepaid_1_3_0");
		registerSource("Prepaid/src", com.usatech.ps.xsd.PSPrepaidActivity.class, "REL_prepaid_1_3_0");
		registerResource("Prepaid/web/WEB-INF/services/ps/META-INF/ps.wsdl", "web/WEB-INF/services/ps/META-INF/", "REL_prepaid_1_3_0");
	}
	protected void registerApps() {
		registerApp(USATRegistry.PREPAID_APP);
	}

	@Override
	public String getName() {
		return "Edge Server - R37 Patch Prepaid WebService";
	}
	
	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
	@Override
	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
	}
}
