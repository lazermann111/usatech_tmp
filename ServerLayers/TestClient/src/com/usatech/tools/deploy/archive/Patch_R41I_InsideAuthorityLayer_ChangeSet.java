package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.authoritylayer.InternalAuthorityTask;
import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.USATRegistry;

public class Patch_R41I_InsideAuthorityLayer_ChangeSet extends AbstractLinuxPatchChangeSet {

	public Patch_R41I_InsideAuthorityLayer_ChangeSet() throws UnknownHostException {
		super();		
		registerSource("ServerLayers/AuthorityLayer/src",  InternalAuthorityTask.class, "REL_authoritylayer_1_27_0");
	}
	protected void registerApps() {
		registerApp(USATRegistry.INAUTH_LAYER);
	}
	@Override
	public String getName() {
		return "Edge Server - R41I InsideAuthorityLayer Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
