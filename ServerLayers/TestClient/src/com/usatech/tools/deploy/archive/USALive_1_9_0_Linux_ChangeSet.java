package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.HOTCHOICE_APP;
import static com.usatech.tools.deploy.USATRegistry.RPTGEN_LAYER;
import static com.usatech.tools.deploy.USATRegistry.RPTREQ_LAYER;
import static com.usatech.tools.deploy.USATRegistry.TRANSPORT_LAYER;
import static com.usatech.tools.deploy.USATRegistry.USALIVE_APP;
import static com.usatech.tools.deploy.USATRegistry.VERIZON_APP;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.AppSettingUpdateFileTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.UsatSettingUpdateFileTask;

public class USALive_1_9_0_Linux_ChangeSet extends AbstractLinuxChangeSet {
	public static final File baseDir = new File(System.getProperty("rootSourceDir", "D:\\Development\\Java Projects"));
	protected static final File serverAppConfigDir = new File(baseDir, "server_app_config");
	public static final App HTTPD_WEB = new App("httpd", "WEB", "server_app_config/WEB/Httpd/linux", "httpd-", 0, 1 /*bin*/, "httpd", "httpd", null, "bin");
	
	
	public USALive_1_9_0_Linux_ChangeSet() throws UnknownHostException {
		super();
	}
	
	private static final String USALIVE_APP_VERSION="1.9.0";
	private static final String RG_APP_VERSION = "3.8.0";
	
	@Override
	protected void registerApps() {
		registerApp(RPTGEN_LAYER, RG_APP_VERSION);
		registerApp(TRANSPORT_LAYER, RG_APP_VERSION);
		registerApp(RPTREQ_LAYER, RG_APP_VERSION);
		registerApp(USALIVE_APP, USALIVE_APP_VERSION);
		HOTCHOICE_APP.setVersion(USALIVE_APP_VERSION);
		VERIZON_APP.setVersion(USALIVE_APP_VERSION);
		registry.registerSubApps(HTTPD_WEB, USALIVE_APP, HOTCHOICE_APP, VERIZON_APP);
		HTTPD_WEB.setVersionPeriodReplacement(".");
		HTTPD_WEB.setMaxInstances(1);
		registerApp(HTTPD_WEB, "2.2.23-usat-build");
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareHostTasks(host, tasks, cache);
		if(host.isServerType("WEB") || host.isServerType("APP")) {
			tasks.add(new UsatSettingUpdateFileTask(registry.getServerSet(host.getServerEnv())));
		}
	}
	
	@Override
	protected void addTasks(Host host, App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if("httpd".equals(app.getName())) {
			if(ordinal != null) {
				if(ordinal == 1)
					ordinal = null;
				else
					return;
			}
		}
		addPrepareAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache);
		if(app.getInstallFileName() != null && !"httpd".equals(app.getName())) {
			tasks.add(new AppSettingUpdateFileTask(app, ordinal, instanceNum, instanceCount, cache, registry.getServerSet(host.getServerEnv())));
		}
		addInstallAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache, commands);
	}

	@Override
	public String getName() {
		return "USALive 1.9.0 Install";
	}
}
