package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.posm.tasks.POSMSaleUpdateTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R47_Patch_7_ChangeSet extends MultiLinuxPatchChangeSet {
	public R47_Patch_7_ChangeSet() throws UnknownHostException {
		super();
		registerResource("usalive/xsl/other-data-layer.xml", "classes", "REL_USALive_1_23_7", false, USATRegistry.USALIVE_APP);
		registerSource("ServerLayers/POSMLayer/src/main", POSMSaleUpdateTask.class, "REL_posmlayer_1_24_7", USATRegistry.POSM_LAYER);
	}
	

	@Override
	public String getName() {
		return "R47 Patch 7";
	}
}
