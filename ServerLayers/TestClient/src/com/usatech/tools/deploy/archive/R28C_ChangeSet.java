package com.usatech.tools.deploy.archive;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.usatech.applayer.AuthorizeTask;
import com.usatech.applayer.EmbeddedDbService;
import com.usatech.tools.deploy.AbstractSolarisPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.UploadTask;

public class R28C_ChangeSet extends AbstractSolarisPatchChangeSet {
	String baseDir;
	public R28C_ChangeSet() {
		super();
		baseDir = System.getProperty("rootSourceDir");
		File applayerBinDir = new File(baseDir + "/AppLayer/bin");
		registerClass(applayerBinDir, EmbeddedDbService.class);
		registerClass(applayerBinDir, AuthorizeTask.class);
	}
	protected void registerApps() {
		registerApp(APP_LAYER);
	}
	@Override
	public String getName() {
		return "Edge Server - R28C Patch";
	}
	@Override
	protected void addTasks(Host host, App app, Integer ordinal, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {		
		super.addTasks(host, app, ordinal, tasks, commands, cache);
		String remoteBaseDir = "/opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal);
		tasks.add(new UploadTask(new File(baseDir + "/AppLayer/db", "setup_embedded_db.sql"), remoteBaseDir + "/db", 0644, "applayer", "applayer"));
		tasks.add(new UploadTask(new File(baseDir + "/AppLayer/db", "ind_db_ardef.txt"), remoteBaseDir + "/db", 0644, "applayer", "applayer"));		
	}
	
	protected void addBeforeRestartCommands(Host host, App app, Integer ordinal, List<String> commands) {
        if(app.getName().equals("applayer")) {
        	commands.add("rm -r /opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal) + "/db/AppLayerDBOld");
        	commands.add("mv /opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal) + "/db/AppLayerDB /opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal) + "/db/AppLayerDBOld");
        }
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
