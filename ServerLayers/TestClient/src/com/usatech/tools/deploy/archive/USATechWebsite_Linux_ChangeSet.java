package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.HTTPD_WEB;
import static com.usatech.tools.deploy.USATRegistry.USATECH_WEBSITE_APP;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;

public class USATechWebsite_Linux_ChangeSet extends AbstractLinuxChangeSet {
	public USATechWebsite_Linux_ChangeSet() throws UnknownHostException {
		super();
		USATECH_WEBSITE_APP.setVersion("1.0.0");
	}

	@Override
	protected void registerApps() {
		registerApp(HTTPD_WEB);
		registerApp(USATECH_WEBSITE_APP);
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("WEB");
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareHostTasks(host, tasks, cache);
	}
	
	@Override
	protected void addTasks(Host host, App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if("httpd".equals(app.getName()) || "esuds".equals(app.getName())||"usatech".equals(app.getName())) {
			if(ordinal != null) {
				if(ordinal == 1)
					ordinal = null;
				else
					return;
			}
		}
		addPrepareAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache);
	}

	protected void addBeforeRestartCommands(Host host, App app, Integer ordinal, int instanceNum, List<String> commands, List<DeployTask> tasks) {
	}
	@Override
	public String getName() {
		return "USATech website Install - Linux";
	}
}
