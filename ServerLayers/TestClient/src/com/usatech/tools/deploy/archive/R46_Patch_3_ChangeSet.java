package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R46_Patch_3_ChangeSet extends MultiLinuxPatchChangeSet {
	public R46_Patch_3_ChangeSet() throws UnknownHostException {
		super();
		registerResource("Prepaid/web/signup_question.html.jsp", "web", "BRN_R46", false, USATRegistry.PREPAID_APP);
		registerResource("usalive/xsl/consumer-data-layer.xml", "classes", "BRN_R46", false, USATRegistry.USALIVE_APP);
	}

	@Override
	public String getName() {
		return "R46 Patch 3";
	}
}
