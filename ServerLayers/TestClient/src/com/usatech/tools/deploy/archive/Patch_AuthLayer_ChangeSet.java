package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.authoritylayer.InterchangeGatewayTask;
import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.USATRegistry;

public class Patch_AuthLayer_ChangeSet extends AbstractLinuxPatchChangeSet {
	protected String baseDir;

	public Patch_AuthLayer_ChangeSet() throws UnknownHostException {
		super();
		baseDir = System.getProperty("rootSourceDir");
		// File simpleBinDir = new File(baseDir + "/Simple1.5/bin");
		// File msgBinDir = new File(baseDir + "/USATMessaging/bin");
		// File applayerBinDir = new File(baseDir + "/AppLayer/bin");
		// registerClass(applayerBinDir, SessionControlTask.class);
		// registerResource("ServerLayers/AuthorityLayer/src/authority-data-layer.xml", "classes/", "HEAD");
		// registerSource("ServerLayers/AuthorityLayer/src", InternalAuthorityTask.class, "HEAD");
		// registerResource("ServerLayers/AuthorityLayer/src/InsideAuthorityLayerService.properties", "classes/", "HEAD");
		registerSource("ServerLayers/AuthorityLayer/src", InterchangeGatewayTask.class, "HEAD");
	}


	@Override
	public boolean isApplicable(Host host) {
		return true;
	}
	protected void registerApps() {
		registerApp(USATRegistry.INAUTH_LAYER);
	}
	@Override
	public String getName() {
		return "InAuthLayer Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}

	/*
	@Override
	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
	}
	// */
}
