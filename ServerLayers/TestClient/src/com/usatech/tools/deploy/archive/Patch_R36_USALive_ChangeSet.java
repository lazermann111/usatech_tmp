package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.USALIVE_APP;

import java.net.UnknownHostException;


import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;

public class Patch_R36_USALive_ChangeSet extends AbstractLinuxPatchChangeSet {

	public Patch_R36_USALive_ChangeSet() throws UnknownHostException {
		super();
		//registerResource("usalive/xsl/templates/user/user-admin.xsl", "classes/templates/user/", "REL_USALive_1_12_0");
		registerResource("usalive/xsl/selection-actions.xml", "classes/", "REL_USALive_1_12_0");
		registerResource("usalive/xsl/templates/selection/select-terminal-customers.xsl", "classes/templates/selection/", "REL_USALive_1_12_0");
	}
	protected void registerApps() {
		registerApp(USALIVE_APP);
	}
	@Override
	public String getName() {
		//return "Patch Edge Server - R36 - Usalive IE 11 Patch";
		return "Patch Edge Server - R36 - Usalive Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
	
	@Override
	public boolean isApplicable(Host host) {
		return true;
	}
}
