package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.applayer.AppLayerDeviceInfoManager;
import com.usatech.layers.common.messagedata.MessageData_CB;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R43_Patch_ChangeSet extends MultiLinuxPatchChangeSet {
	public R43_Patch_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/AppLayer/src", AppLayerDeviceInfoManager.class, "BRN_R43", USATRegistry.APP_LAYER);
		registerResource("ServerLayers/AppLayer/src/legacy-data-layer.xml", "classes", "BRN_R43", false, USATRegistry.LOADER);
		registerSource("ServerLayers/LayersCommon/src", MessageData_CB.class, "BRN_R43", USATRegistry.NET_LAYER, USATRegistry.DMS_APP, USATRegistry.LOADER, USATRegistry.APP_LAYER);

	}

	@Override
	public String getName() {
		return "R43 Patch";
	}
}
