package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.RPTGEN_LAYER;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
/*
 * This changeset is to allow email blast to send if not campaign not started.
 */
public class Patch_R34_ReportGenerator_ChangeSet extends AbstractLinuxPatchChangeSet {
	protected String baseDir;

	public Patch_R34_ReportGenerator_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ReportGenerator/src/rg-campaign-data-layer.xml", "classes/", "HEAD"); // previous is 1.6
	}
	protected void registerApps() {
		registerApp(RPTGEN_LAYER);
	}
	@Override
	public String getName() {
		return "Edge Server - R34 - Report Generator email blast Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
