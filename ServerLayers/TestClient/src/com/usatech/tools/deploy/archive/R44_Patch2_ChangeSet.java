package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.applayer.TranImportTask;
import com.usatech.applayer.TranReportTask;
import com.usatech.ccs.ConsumerCommunicationTask;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.constants.ConsumerCommAttrEnum;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R44_Patch2_ChangeSet extends MultiLinuxPatchChangeSet {
	public R44_Patch2_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/LayersCommon/src", InteractionUtils.class, "BRN_R44", USATRegistry.LOADER);
		registerSource("ServerLayers/LayersCommon/src", ConsumerCommAttrEnum.class, "BRN_R44", USATRegistry.LOADER);
		registerSource("ServerLayers/AppLayer/src", TranImportTask.class, "BRN_R44", USATRegistry.LOADER);
		registerSource("ServerLayers/AppLayer/src", TranReportTask.class, "BRN_R44", USATRegistry.LOADER);
		registerResource("ServerLayers/AppLayer/src/loader-data-layer.xml", "classes", "BRN_R44", false, USATRegistry.LOADER);
		registerSource("ServerLayers/LayersCommon/src", ConsumerCommAttrEnum.class, "BRN_R44", USATRegistry.RPTGEN_LAYER);
		registerSource("ReportGenerator/src", ConsumerCommunicationTask.class, "BRN_R44", USATRegistry.RPTGEN_LAYER);
	}

	@Override
	public String getName() {
		return "R44 Patch 2";
	}
}
