package com.usatech.tools.deploy.archive;

import static com.usatech.tools.deploy.USATRegistry.POSTGRES_MSR;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.activemq.util.ByteArrayInputStream;

import simple.app.Processor;
import simple.app.ServiceException;
import simple.io.HeapBufferStream;

import com.sshtools.j2ssh.SshClient;
import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.DeployTaskInfo;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.OptionalTask;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.Server;
import com.usatech.tools.deploy.ServerSet;
import com.usatech.tools.deploy.UploadAndRunPostgresSQLTask;
import com.usatech.tools.deploy.UploadTask;

public class CopyOldFileRepoFilesToNew_ChangeSet extends AbstractLinuxChangeSet {
	protected String oldResourceExportSql = "SELECT LO_EXPORT(RESOURCE_CONTENT, '/home/postgres/tmp_file_repo_' || RESOURCE_CONTENT || '.dat') FROM FILE_REPO.RESOURCE;\n"
											+ "COPY FILE_REPO.RESOURCE TO '/home/postgres/tmp_file_repo_resources.dat' BINARY;\n";

	public CopyOldFileRepoFilesToNew_ChangeSet() throws UnknownHostException {
		super();
		registry.registerServerSet("ECC", "MSR_OLD", '0', 4, 1);
		registry.registerServerSet("USA", "MSR_OLD", '2', 4, 1);
	}


	@Override
	protected void registerApps() {
		registerApp(POSTGRES_MSR);
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
	}
	
	@Override
	protected void addTasks(Host host, App app, final Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		ServerSet serverSet = registry.getServerSet(host.getServerEnv());
		final Server oldServer = serverSet.getServers("MSR_OLD").get(instanceNum - 1);
		// connect to old and stream copy of file_repo tables and large objects to local file
		tasks.add(new OptionalTask("test -r postgres/file_repo_copy.tar.gz; echo $?", Pattern.compile("0"), new DeployTask[] {
			new ExecuteTask("sudo su", "mkdir /opt/USAT/postgres/tmp", "cd /opt/USAT/postgres/tmp", "gunzip -c /home/`logname`/postgres/file_repo_copy.tar.gz | tar xf -"),
		}, new DeployTask[] {
			new ExecuteTask("mkdir postgres" + (ordinal == null ? "" : "_" + ordinal)),
			new DeployTask() {
				@Override
					public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
					SshClient ssh = deployTaskInfo.sshClientCache.getSshClient(oldServer.host, deployTaskInfo.user);
					ExecuteTask.executeCommands(oldServer.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, ssh, 2000, null,
							"sudo su", 
							"mkdir /home/postgres",
							"chown postgres /home/postgres");				
					UploadTask.writeFileUsingTmp(oldServer.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, new ByteArrayInputStream(oldResourceExportSql.getBytes()), 0644, "postgres", "postgres", null, "/home/postgres/export_resources.sql");
					ExecuteTask.executeCommands(oldServer.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, ssh, 2000, null,
							"sudo su", 
							"su - postgres -c '/opt/USAT/postgres/8.4-community/bin/64/psql filerepo -f /home/postgres/export_resources.sql'",
							"scp /home/postgres/tmp_file_repo_*.dat " + deployTaskInfo.user + "@" + deployTaskInfo.host.getFullName() + ":/home/"+ deployTaskInfo.user +"/postgres" + (ordinal == null ? "" : "_" + ordinal) + "/");
						return "Complete";
				}

					@Override
					public String getDescription(DeployTaskInfo deployTaskInfo) {
						return "Make Postgres filerepo db";
					}
			}
		}));
		tasks.add(new UploadAndRunPostgresSQLTask(new Processor<DeployTaskInfo, InputStream>() {
			@Override
			public InputStream process(DeployTaskInfo argument) throws ServiceException {
				HeapBufferStream bufferStream = new HeapBufferStream();
				PrintWriter writer = new PrintWriter(bufferStream.getOutputStream());
				writer.println("DO $$");
				writer.println("DECLARE");
				writer.println("lr_rec RECORD;");
				writer.println("BEGIN");
				writer.println("CREATE TEMPORARY TABLE TMP_IMPORT_RESOURCE(LIKE FILE_REPO.RESOURCE);");
				writer.print("COPY TMP_IMPORT_RESOURCE FROM '/opt/USAT/postgres/tmp/tmp_file_repo_resources.dat' (FORMAT 'binary');");
				writer.print("UPDATE TMP_IMPORT_RESOURCE SET RESOURCE_CONTENT = LO_IMPORT('/opt/USAT/postgres/tmp/tmp_file_repo_' || RESOURCE_CONTENT || '.dat');");
				writer.println("FOR lr_rec IN SELECT RESOURCE_CONTENT FROM TMP_IMPORT_RESOURCE LOOP");
				writer.println("EXECUTE 'GRANT SELECT ON LARGE OBJECT ' || lr_rec.RESOURCE_CONTENT || ' TO read_file_repo';");
				writer.println("EXECUTE 'GRANT ALL ON LARGE OBJECT ' || lr_rec.RESOURCE_CONTENT || ' TO write_file_repo';");
				writer.println("END LOOP;");
				writer.println("INSERT INTO FILE_REPO.RESOURCE(RESOURCE_CD, RESOURCE_NAME, RESOURCE_CONTENT, RESOURCE_CONTENT_TYPE, RESOURCE_CONTENT_COMPRESSION, RESOURCE_CONTENT_LENGTH, CREATED_UTC_TS, CREATED_BY, UPDATED_UTC_TS, UPDATED_BY) ");
				writer.println("SELECT RESOURCE_CD, RESOURCE_NAME, RESOURCE_CONTENT, RESOURCE_CONTENT_TYPE, RESOURCE_CONTENT_COMPRESSION, RESOURCE_CONTENT_LENGTH, CREATED_UTC_TS, CREATED_BY, UPDATED_UTC_TS, UPDATED_BY FROM TMP_IMPORT_RESOURCE;");
				writer.println("END");
				writer.println("$$");
				writer.flush();
				return bufferStream.getInputStream();
			}

			@Override
			public Class<InputStream> getReturnType() {
				return InputStream.class;
			}

			@Override
			public Class<DeployTaskInfo> getArgumentType() {
				return DeployTaskInfo.class;
			}
		}, false, ordinal, "filerepo"));
	}

	protected void addBeforeRestartCommands(Host host, App app, Integer ordinal, int instanceNum, List<String> commands, List<DeployTask> tasks) {
	}

	@Override
	public String getName() {
		return "Copy Old File Repo Files to New";
	}
}
