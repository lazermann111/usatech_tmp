package com.usatech.tools.deploy.archive;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import simple.falcon.engine.standard.FilterGroupAwareParameter;
import simple.falcon.engine.standard.StandardDesignEngine;
import simple.falcon.engine.standard.StandardDesignEngine2;
import simple.falcon.engine.standard.StandardDesignEngine3;
import simple.falcon.engine.standard.StandardExecuteEngine;
import simple.falcon.engine.standard.StandardFilterItem;
import simple.falcon.engine.standard.StandardPillar;

import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;

public class R46_Patch_2_ChangeSet extends MultiLinuxPatchChangeSet {
	public R46_Patch_2_ChangeSet() throws UnknownHostException {
		super();
		registerSource("Falcon/src/", StandardFilterItem.class, "REL_report_generator_3_20_2", USATRegistry.RPTGEN_LAYER, USATRegistry.USALIVE_APP);
		registerSource("Falcon/src/", StandardDesignEngine.class, "REL_report_generator_3_20_2", USATRegistry.RPTGEN_LAYER, USATRegistry.USALIVE_APP);
		registerSource("Falcon/src/", StandardDesignEngine2.class, "REL_report_generator_3_20_2", USATRegistry.RPTGEN_LAYER, USATRegistry.USALIVE_APP);
		registerSource("Falcon/src/", StandardDesignEngine3.class, "REL_report_generator_3_20_2", USATRegistry.RPTGEN_LAYER, USATRegistry.USALIVE_APP);
		registerSource("Falcon/src/", StandardExecuteEngine.class, "REL_report_generator_3_20_2", USATRegistry.RPTGEN_LAYER, USATRegistry.USALIVE_APP);
		registerSource("Falcon/src/", FilterGroupAwareParameter.class, "REL_report_generator_3_20_2", USATRegistry.RPTGEN_LAYER, USATRegistry.USALIVE_APP);
		registerSource("Falcon/src/", StandardPillar.class, "REL_report_generator_3_20_2", USATRegistry.RPTGEN_LAYER, USATRegistry.USALIVE_APP);
		registerLib("ThirdPartyJavaLibraries/lib/connectbot-ssh2-1.8.4.1.jar", "HEAD", USATRegistry.TRANSPORT_LAYER);
	}

	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		/*
		if(app == USATRegistry.RPTGEN_LAYER)
			tasks.add(new ExecuteTask("sudo su", "if [ `grep -c 'log4j.logger.com.usatech.report.GenerateFolioReportTask=DEBUG' /opt/USAT/" + app.getName() + (instance == null ? "" : instance) + "/specific/log4j.properties` -eq 0 ]; then echo 'log4j.logger.com.usatech.report.GenerateFolioReportTask=DEBUG' >> /opt/USAT/" + app.getName() + (instance == null ? "" : instance)
					+ "/specific/log4j.properties; fi"));
		 */
		/*
		1.	Add ganymed-ssh2-build251beta1-usat1.jar to /opt/USAT/transport/lib
		2.	Save the /opt/USAT/transport/bin/TransportService.sh to TransportService.sh.org and modified the /opt/USAT/transport/bin/TransportService.sh to add ganymed to the classpath
		3.	R46_Patch_1_ChangeSet.java that modified 2 java files
			registerSource("ReportGenerator/src/", KnownHostsVerifier.class, "BRN_R45", USATRegistry.TRANSPORT_LAYER);
			registerSource("ReportGenerator/src/", SecureFTPGanymedTransporter.class, "BRN_R45", USATRegistry.TRANSPORT_LAYER);
			 */
		if(app == USATRegistry.TRANSPORT_LAYER) {
			String appDir = "/opt/USAT/" + app.getName() + (instance == null ? "" : instance);
			tasks.add(new ExecuteTask("sudo su",
				"if [ -r " + appDir + "/lib/ganymed-ssh2-build251beta1-usat1.jar ]; then /bin/mv -f " + appDir + "/lib/ganymed-ssh2-build251beta1-usat1.jar .; fi",
				"if [ -r " + appDir + "/bin/TransportService.sh.org ]; then /bin/mv -f " + appDir + "/bin/TransportService.sh.org " + appDir + "/bin/TransportService.sh; fi",
				"if [ -r " + appDir + "/classes/com/usatech/transport ]; then /bin/rm -r " + appDir + "/classes/com/usatech/transport; fi"			
				));
		}
		super.addRestartAppCommands(host, app, instance, instanceNum, instanceCount, tasks, commands, cache);
		
	}
	@Override
	public String getName() {
		return "R46 Patch 2";
	}
}
