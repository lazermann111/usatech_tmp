package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.USATRegistry;

public class Patch_R42_1C_USALive_ChangeSet extends AbstractLinuxPatchChangeSet {
	public Patch_R42_1C_USALive_ChangeSet() throws UnknownHostException {
		super();		
		registerResource("usalive/xsl/other-data-layer.xml", "classes/", "REL_R42_1C");
		
		registerResource("usalive/web/USAT_ePort-Mobile_Addendum.pdf", "web/", "REL_R42_1C");
		registerResource("usalive/web/USAT_ePort-Online_Addendum.pdf", "web/", "REL_R42_1C");
		
		registerResource("usalive/web/public/eport_mobile_signup.html.jsp", "web/public/", "REL_R42_1C");
		registerResource("usalive/web/public/eport_online_eport_mobile_signup.html.jsp", "web/public/", "REL_R42_1C");
		registerResource("usalive/web/public/eport_online_signup.html.jsp", "web/public/", "REL_R42_1C");
		registerResource("usalive/web/public/fall_special_order.html.jsp", "web/public/", "REL_R42_1C");
		
		registerResource("usalive/web/offer_signups.html.jsp", "web/", "REL_R42_1C");
	}
	protected void registerApps() {
		registerApp(USATRegistry.USALIVE_APP);
	}
	@Override
	public String getName() {
		return "Edge Server - R42.1C USALive Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
