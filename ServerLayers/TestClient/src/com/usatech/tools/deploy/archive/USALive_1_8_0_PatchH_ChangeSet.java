package com.usatech.tools.deploy.archive;

import java.io.File;
import java.net.UnknownHostException;

import simple.falcon.engine.standard.DirectXHTMLGenerator;

import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.USATRegistry;

public class USALive_1_8_0_PatchH_ChangeSet extends AbstractLinuxPatchChangeSet {
	protected String baseDir;
	public USALive_1_8_0_PatchH_ChangeSet() throws UnknownHostException {
		super();
		baseDir = System.getProperty("rootSourceDir");
		File falconBinDir = new File(baseDir + "/Falcon/src");
		registerClass(falconBinDir, DirectXHTMLGenerator.class);
	}

	protected void registerApps() {
		registerApp(USATRegistry.RPTGEN_LAYER);
	}
	@Override
	public String getName() {
		return "Patch H USALive 1.8.0";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
