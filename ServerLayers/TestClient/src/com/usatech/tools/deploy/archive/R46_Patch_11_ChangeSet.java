package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R46_Patch_11_ChangeSet extends MultiLinuxPatchChangeSet {
	public R46_Patch_11_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/POSMLayer/conf/POSMLayerService.properties", "classes", "REL_posmlayer_1_23_11", true, USATRegistry.POSM_LAYER);
		registerResource("ServerLayers/POSMLayer/conf/posm-data-layer.xml", "classes", "REL_posmlayer_1_23_11", false, USATRegistry.POSM_LAYER);
		registerResource("ServerLayers/AppLayer/src/loader-data-layer.xml", "classes", "REL_applayer_1_32_11", false, USATRegistry.LOADER);
	}

	@Override
	public String getName() {
		return "R46 Patch 11";
	}
}