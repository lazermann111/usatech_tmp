package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.USATRegistry;

public class Patch_R42_1D2_USALive_ChangeSet extends AbstractLinuxPatchChangeSet {
	public Patch_R42_1D2_USALive_ChangeSet() throws UnknownHostException {
		super();
		
		registerResource("usalive/web/public/eport_mobile_signup.html.jsp", "web/public/", "REL_R42_1D");
		registerResource("usalive/web/public/eport_online_eport_mobile_signup.html.jsp", "web/public/", "REL_R42_1D");
		registerResource("usalive/web/public/eport_online_signup.html.jsp", "web/public/", "REL_R42_1D");
		registerResource("usalive/web/public/fall_special_order.html.jsp", "web/public/", "REL_R42_1D");
	}
	protected void registerApps() {
		registerApp(USATRegistry.USALIVE_APP);
	}
	@Override
	public String getName() {
		return "Edge Server - R42.1D2 USALive Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
