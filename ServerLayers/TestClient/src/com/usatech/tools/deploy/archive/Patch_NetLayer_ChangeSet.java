package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.eportconnect.ECRequestHandler;
import com.usatech.layers.common.messagedata.WS2CardInfoResponseMessageData;
import com.usatech.layers.common.messagedata.WS2RequestMessageData;
import com.usatech.layers.common.messagedata.WS2ResponseMessageData;
import com.usatech.layers.common.messagedata.WSResponseMessageData;
import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.USATRegistry;

public class Patch_NetLayer_ChangeSet extends AbstractLinuxPatchChangeSet {
	protected String baseDir;

	public Patch_NetLayer_ChangeSet() throws UnknownHostException {
		super();
		baseDir = System.getProperty("rootSourceDir");
		// File simpleBinDir = new File(baseDir + "/Simple1.5/bin");
		// File msgBinDir = new File(baseDir + "/USATMessaging/bin");
		// File applayerBinDir = new File(baseDir + "/AppLayer/bin");
		// registerClass(applayerBinDir, SessionControlTask.class);
		// registerResource("ServerLayers/NetLayer/src/NetLayerService.properties", "classes/", "HEAD");
		// registerSource("ServerLayers/LayersCommon/src", MessageData_0302.class, "HEAD");
		// registerSource("ServerLayers/LayersCommon/src", MessageData_030B.class, "HEAD");
		// registerSource("ServerLayers/LayersCommon/src", MessageData_030D.class, "HEAD");
		// registerSource("ServerLayers/LayersCommon/src", MessageData_030E.class, "HEAD");
		
		registerSource("ServerLayers/LayersCommon/src", WS2CardInfoResponseMessageData.class, "HEAD");
		registerSource("ServerLayers/LayersCommon/src", WS2RequestMessageData.class, "HEAD");
		registerSource("ServerLayers/LayersCommon/src", WSResponseMessageData.class, "HEAD");
		registerSource("ServerLayers/LayersCommon/src", WS2ResponseMessageData.class, "HEAD");
		registerSource("ServerLayers/NetLayer/src", ECRequestHandler.class, "HEAD");

	}


	@Override
	public boolean isApplicable(Host host) {
		return true;
	}
	protected void registerApps() {
		registerApp(USATRegistry.NET_LAYER);
	}
	@Override
	public String getName() {
		return "NetLayer Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}

	/*
	@Override
	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
	}
	// */
}
