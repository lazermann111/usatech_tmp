package com.usatech.tools.deploy.archive;

import java.io.IOException;
import java.util.List;

import com.usatech.tools.deploy.AbstractSolarisChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.AppSettingUpdateFileTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;

public class FixInactivity_ChangeSet extends AbstractSolarisChangeSet {
	public FixInactivity_ChangeSet() {
		super();
	}
	protected void registerApps() {
		registerApp(APP_LAYER);
		registerApp(INAUTH_LAYER);
		registerApp(POSM_LAYER);
		registerApp(NET_LAYER);
		registerApp(OUTAUTH_LAYER);
	}
	
	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
	}
	
	@Override
	protected void addTasks(Host host, App app, Integer ordinal, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		final int num = (ordinal == null ? Integer.parseInt(host.getSimpleName().substring(7)) : ordinal);
		AppSettingUpdateFileTask appUpdateTask = new AppSettingUpdateFileTask(app, ordinal, num, instanceCount, cache, null);
		tasks.add(appUpdateTask);
	}
	
	@Override
	public String getName() {
		return "Edge Server - Fix Inactivity";
	}
	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
