package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import simple.falcon.engine.standard.CSVGenerator;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R47_Patch_10_ChangeSet extends MultiLinuxPatchChangeSet {
	public R47_Patch_10_ChangeSet() throws UnknownHostException {
		super();
		registerSource("Falcon/src", CSVGenerator.class, "REL_report_generator_3_21_10", USATRegistry.RPTGEN_LAYER, USATRegistry.USALIVE_APP);
	}
	

	@Override
	public String getName() {
		return "R47 Patch 10";
	}
}
