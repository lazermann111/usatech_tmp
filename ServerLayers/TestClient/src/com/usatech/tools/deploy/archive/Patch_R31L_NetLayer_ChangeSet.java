package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.eportconnect.ECSessionManager;
import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.USATRegistry;

public class Patch_R31L_NetLayer_ChangeSet extends AbstractLinuxPatchChangeSet {
	
	
	protected String NetLayer_TAG="REL_EportMobilePasscode";
	
	public Patch_R31L_NetLayer_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/NetLayer/src", ECSessionManager.class, NetLayer_TAG);
		
	}
	protected void registerApps() {
		registerApp(USATRegistry.NET_LAYER);
	}

	@Override
	public String getName() {
		return "Edge Server - R31L Patch NetLayer - Apps";
	}
	
	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}

}
