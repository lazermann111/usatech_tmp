package com.usatech.tools.deploy.archive;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import simple.app.Processor;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.DeployTaskInfo;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.HostImpl;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.UploadTask;

public class CopyDLQToNew_ChangeSet implements ChangeSet {
	public CopyDLQToNew_ChangeSet() {
		super();
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		if(host.isServerType("MST")) { // new
			String[] cmds;
			if("USA".equalsIgnoreCase(host.getServerEnv())) {
				cmds = new String[3];
			} else if("ECC".equalsIgnoreCase(host.getServerEnv()) || "INT".equalsIgnoreCase(host.getServerEnv())) {
				cmds = new String[4];
			} else if("DEV".equalsIgnoreCase(host.getServerEnv())) {
				cmds = new String[3];
			} else {
				throw new IOException("Invalid host");
			}
			cmds[0] = "sudo su";
			cmds[1] = "runuser - postgres";
			cmds[2] = "/usr/pgsql-latest/bin/psql mq -p 5432 -q -c \"SELECT DLQ.QUEUE_TABLE, DLQ.MESSAGE_ID, MQ.RESURRECT_MESSAGE(DLQ.MESSAGE_ID) FROM MQ.DLQ JOIN MQ.QUEUE Q ON DLQ.QUEUE_TABLE =  Q.QUEUE_TABLE WHERE Q.DATA_SOURCE_NAME IS NULL AND Q.QUEUE_NAME != 'usat.posm.terminal.next';\"";
			if(cmds.length > 3)
				cmds[3] = "/usr/pgsql-latest/bin/psql mq -p 5431 -q -c \"SELECT DLQ.QUEUE_TABLE, DLQ.MESSAGE_ID, MQ.RESURRECT_MESSAGE(DLQ.MESSAGE_ID) FROM MQ.DLQ JOIN MQ.QUEUE Q ON DLQ.QUEUE_TABLE =  Q.QUEUE_TABLE WHERE Q.DATA_SOURCE_NAME IS NULL AND Q.QUEUE_NAME != 'usat.posm.terminal.next';\"";

			return new DeployTask[] {
					new ExecuteTask(cmds)
			};
		} else if(host.isServerType("MST_OLD")) { // old
			final Host targetHost;
			final int pgPort;
			if("USA".equalsIgnoreCase(host.getServerEnv())) {
				char seventh = host.getSimpleName().charAt(7);
				if(seventh == '5')
					seventh = '4';
				targetHost = new HostImpl(host.getServerEnv(), "MST", "usamst3" + seventh + ".trooper.usatech.com");
				pgPort = 5432;
			} else if("ECC".equalsIgnoreCase(host.getServerEnv()) || "INT".equalsIgnoreCase(host.getServerEnv())) {
				int n = 1 + (host.getSimpleName().charAt(7) - '0');
				targetHost = new HostImpl(host.getServerEnv(), "MST", host.getServerEnv().toLowerCase() + "mst1" + (n / 2) + ".usatech.com");
				pgPort = 5431 + (n % 2);
			} else if("DEV".equalsIgnoreCase(host.getServerEnv())) {
				targetHost = new HostImpl(host.getServerEnv(), "MST", "devmst1" + host.getSimpleName().charAt(7) + ".usatech.com");
				pgPort = 5432;
			} else {
				throw new IOException("Invalid host");
			}
			final String sourcePath = "/home/postgres/mq_dlq_r29.dat";
			return new DeployTask[] {
					new ExecuteTask("sudo su", "su - postgres", "/opt/USAT/postgres/latest/bin/64/psql mq -c \"COPY MQ.DLQ TO '" + sourcePath + "' (FORMAT 'binary');\""),
					new DeployTask() {
						@Override
				public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
							copyFileFromOldToNew(deployTaskInfo, sourcePath, targetHost, "/home/" + deployTaskInfo.user + "/mq_dlq_r29_" + pgPort + ".dat", pgPort);
					return "Complete";
						}

						@Override
						public String getDescription(DeployTaskInfo deployTaskInfo) {
							return "Copy DLQ to New MQ database";
						}
					}
			};
		}
		return null;
	}

	protected void copyFileFromOldToNew(DeployTaskInfo deployTaskInfo, String sourcePath, Host targetHost, String targetPath, int pgPort) throws IOException, InterruptedException {
		File tmpFile = File.createTempFile("deploy_mgr_copy-", ".tmp");
		try {
			UploadTask.readFileUsingTmp(deployTaskInfo, sourcePath, new FileOutputStream(tmpFile), null);
			deployTaskInfo.interaction.printf("Copy from '" + sourcePath + "' on " + deployTaskInfo.host.getSimpleName() + " to '" + targetPath + "' on " + targetHost.getSimpleName() + " ...");
			UploadTask.writeFileUsingTmp(targetHost, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, new FileInputStream(tmpFile), 0644, "postgres", "postgres", null, targetPath);
			Processor<String, String> interactiveInput = null;
			ExecuteTask.executeCommands(targetHost, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache.getSshClient(targetHost, deployTaskInfo.user), 5000L, interactiveInput, "sudo su", "runuser - postgres", "/usr/pgsql-latest/bin/psql mq -p " + pgPort + " -c \"COPY MQ.DLQ FROM '" + targetPath + "' (FORMAT 'binary');\"", "/usr/pgsql-latest/bin/psql mq -p "
					+ pgPort + " -c \"SELECT DLQ.QUEUE_TABLE, DLQ.MESSAGE_ID, MQ.RESURRECT_MESSAGE(DLQ.MESSAGE_ID) FROM MQ.DLQ JOIN MQ.QUEUE Q ON DLQ.QUEUE_TABLE =  Q.QUEUE_TABLE WHERE Q.DATA_SOURCE_NAME IS NOT NULL AND Q.QUEUE_NAME != 'usat.posm.terminal.next';\"");
		} finally {
			tmpFile.delete();
		}
	}
	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("MST") || host.isServerType("MST_OLD");
	}
	
	@Override
	public String toString() {
		return getName();
	}
	@Override
	public String getName() {
		return "Copy DLQ messages to New MST";
	}
}
