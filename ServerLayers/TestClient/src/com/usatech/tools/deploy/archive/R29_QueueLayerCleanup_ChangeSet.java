package com.usatech.tools.deploy.archive;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.usatech.tools.deploy.AbstractSolarisChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.UploadAndRunPostgresSQLTask;

public class R29_QueueLayerCleanup_ChangeSet extends AbstractSolarisChangeSet {
	protected File cleanupFile = new File(System.getProperty("rootSourceDir", "D:\\Development\\Java Projects") + "\\..\\Database Projects\\DatabaseScripts\\QueueLayer\\CleanupOldKeyReceiverQueues.sql");
	public R29_QueueLayerCleanup_ChangeSet() {
		super();
				
	}
	protected void registerApps() {
		registerApp(POSTGRES);
	}
	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) {
	}
	@Override
	protected void addTasks(Host host, App app, Integer ordinal, List<DeployTask> tasks, List<String> commands, final PasswordCache cache) throws IOException {
		if("postgres".equalsIgnoreCase(app.getName()) && (ordinal == null || ordinal == 1)) {
			String[] databases;
			if("LOCAL".equalsIgnoreCase(host.getServerEnv())) {
				databases = new String[] { "bkmq1", "bkmq2", "dkmq1", "dkmq2", "phmq1", "phmq2" };
			} else {
				databases = new String[] { "mq" };
			}
			tasks.add(new UploadAndRunPostgresSQLTask(cleanupFile, false, databases) {
				@Override
				protected String buildExecuteSQLCommand(String postgres_bin, String database, int port, String filePath) {
					int limit = 250;
					return "RESULT=" + limit + "; while [ $RESULT = " + limit + " ]; do RESULT=`" +
							super.buildExecuteSQLCommand(postgres_bin, database, port, filePath) + " | cut -d\" \" -f2`; echo \"Deleted $RESULT rows\"; done";
				}
			});
		}
	}
	
	@Override
	public String getName() {
		return "Edge Server - R29 Install - QueueLayer Cleanup";
	}
}
