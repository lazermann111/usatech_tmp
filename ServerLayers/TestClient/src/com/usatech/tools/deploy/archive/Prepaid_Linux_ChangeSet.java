package com.usatech.tools.deploy.archive;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.AppSettingUpdateFileTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.LoopbackInterfaceMap;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.tools.deploy.UsatSettingUpdateFileTask;

public class Prepaid_Linux_ChangeSet extends AbstractLinuxChangeSet {
	private static final String PREPAID_APP_VERSION = "1.1.3";
	
	public Prepaid_Linux_ChangeSet() throws UnknownHostException {
		super();
	}
	
	@Override
	protected void registerApps() {
		registerApp(USATRegistry.HTTPD_WEB, "2.2.23-usat-build");
		registerApp(USATRegistry.PREPAID_APP, PREPAID_APP_VERSION);
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.PREPAID_APP, USATRegistry.GETMORE_APP);
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareHostTasks(host, tasks, cache);
		if(host.isServerType("WEB") || host.isServerType("APP")) {
			tasks.add(new UsatSettingUpdateFileTask(registry.getServerSet(host.getServerEnv())));
		}
		// add loopback interface
		if(host.isServerType("WEB")){
			HashMap<String, String> loopbackMap = LoopbackInterfaceMap.getMapByEnv(host.getServerEnv());
			String subHostPrefix = "getmore";
			String subhostname = subHostPrefix + ("USA".equalsIgnoreCase(host.getServerEnv()) ? ".usatech.com" : "-" + host.getServerEnv().toLowerCase() + ".usatech.com");
			String subhostip = LoopbackInterfaceMap.getHostRealIp(host.getServerEnv(), subhostname);
			String deviceNameSuffix = loopbackMap.get(subhostip);
			if(deviceNameSuffix == null)
				throw new IOException("DeviceNameSuffix is null for " + subhostip);
			String deviceName = "lo:" + deviceNameSuffix;
			tasks.add(new ExecuteTask("sudo su",
					"test -f /etc/sysconfig/network-scripts/ifcfg-"+deviceName+" || echo 'DEVICE="+deviceName+"\nIPADDR="+subhostip+"\nNETMASK=0.0.0.0\nONBOOT=yes' >> /etc/sysconfig/network-scripts/ifcfg-"+deviceName,
					"ifup "+deviceName)
					);
		}
	}
	
	@Override
	protected void addTasks(Host host, App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if("httpd".equals(app.getName())) {
			if(ordinal != null) {
				if(ordinal == 1)
					ordinal = null;
				else
					return;
			}
		}
		addPrepareAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache);
		if(app.getInstallFileName() != null && !"httpd".equals(app.getName())) {
			tasks.add(new AppSettingUpdateFileTask(app, ordinal, instanceNum, instanceCount, cache, registry.getServerSet(host.getServerEnv())));
		}
		addInstallAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache, commands);
	}

	@Override
	public String getName() {
		return "Prepaid " + PREPAID_APP_VERSION + " Install";
	}
}
