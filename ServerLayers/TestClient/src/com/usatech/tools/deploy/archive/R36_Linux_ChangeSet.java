package com.usatech.tools.deploy.archive;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.AppSettingUpdateFileTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.tools.deploy.UploadAndRunPostgresSQLTask;
import com.usatech.tools.deploy.UsatSettingUpdateFileTask;

public class R36_Linux_ChangeSet extends AbstractLinuxChangeSet {
	public R36_Linux_ChangeSet() throws UnknownHostException {
		super();
	}

	@Override
	protected void registerApps() {
		registerApp(USATRegistry.POSTGRES_PGS, "9.2.4");
		registerApp(USATRegistry.POSTGRES_MST, "9.2.4");
		registerApp(USATRegistry.POSTGRES_MSR, "9.2.4");
		registerApp(USATRegistry.LOADER, "1.22.0");
		registerApp(USATRegistry.APP_LAYER, "1.22.0");
		registerApp(USATRegistry.INAUTH_LAYER, "1.22.0");
		registerApp(USATRegistry.POSM_LAYER, "1.13.0");
		registerApp(USATRegistry.NET_LAYER, "1.22.0");
		registerApp(USATRegistry.OUTAUTH_LAYER, "1.22.0");
		registerApp(USATRegistry.KEYMGR, "2.6.0");
		registerApp(USATRegistry.DMS_APP, "1.0.11");
		registerApp(USATRegistry.HTTPD_NET, null);
		registry.registerSubApps(USATRegistry.HTTPD_NET, USATRegistry.DMS_APP);
		registerApp(USATRegistry.RPTGEN_LAYER, "3.11.0");
		registerApp(USATRegistry.RPTREQ_LAYER, "3.11.0");
		registerApp(USATRegistry.TRANSPORT_LAYER, "3.11.0");
		registerApp(USATRegistry.USALIVE_APP, "1.12.0");
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.USALIVE_APP);
		registerApp(USATRegistry.PREPAID_APP, "1.2.0");
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.GETMORE_APP); // The APP server needs PREPAID_APP, but the WEB server also needs the GETMORE_APP as subapp
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.PREPAID_APP);
		registerApp(USATRegistry.HTTPD_WEB, null);
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareHostTasks(host, tasks, cache);
		tasks.add(new UsatSettingUpdateFileTask(registry.getServerSet(host.getServerEnv())));
		tasks.add(new ExecuteTask("sudo find /opt/USAT/ -name '*.dmp' -delete"));
	}
	
	@Override
	protected void addTasks(Host host, final App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache);
		if(app.getInstallFileName() != null)
			tasks.add(new AppSettingUpdateFileTask(app, ordinal, instanceNum, instanceCount, cache, registry.getServerSet(host.getServerEnv()), false));
		addInstallAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache, commands);
		if(USATRegistry.POSTGRES_MST.equals(app) || USATRegistry.POSTGRES_MSR.equals(app))
			tasks.add(new UploadAndRunPostgresSQLTask("Simple1.5/src/simple/mq/peer/mq_peer_postgres_setup_7.sql", "HEAD", false, ordinal, "mq"));
	}

	@Override
	public String getName() {
		return "Edge Server - R36 Install - Apps";
	}
}
