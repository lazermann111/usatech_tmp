package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R45_Patch_13_ChangeSet extends MultiLinuxPatchChangeSet {
	public R45_Patch_13_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ReportGenerator/src/ReportRequesterService.properties", "classes/", "REL_report_generator_3_19_1", true,USATRegistry.RPTREQ_LAYER);
	}

	@Override
	public String getName() {
		return "R45 Patch 13 For report requester";
	}
}
