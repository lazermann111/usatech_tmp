package com.usatech.tools.deploy.archive;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.usatech.tools.deploy.AbstractSolarisChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.AppSettingUpdateFileTask;
import com.usatech.tools.deploy.DefaultHostSpecificValue;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.HostSpecificValue;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.UsatSettingUpdateFileTask;

import simple.io.Interaction;
import simple.text.StringUtils;
import simple.util.CollectionUtils;

public abstract class AbstractR28_ChangeSet extends AbstractSolarisChangeSet {
	protected final int[] instances;
	protected final String name;
	public AbstractR28_ChangeSet(boolean first) {
		super();
		APP_LAYER.setVersion("1.14.0");
		POSM_LAYER.setVersion("1.5.0");
		INAUTH_LAYER.setVersion("1.14.0");
		NET_LAYER.setVersion("1.14.0");
		OUTAUTH_LAYER.setVersion("1.14.0");	
		if(first) {
			instances = new int[] {1,2};
			name = "Edge Server - R28 Install (Step 3)";
		} else {
			instances = new int[] {3,4,5};
			name = "Edge Server - R28 Install (Step 4)";
		}
		Integer[] ords = new Integer[instances.length];
		for(int i = 0; i < instances.length; i++)
			ords[i] = instances[i];
		ordMap.put("DEV", ords);
		ordMap.put("INT", ords);
		ordMap.put("ECC", ords);		
	}
	@Override
	public boolean isApplicable(Host host) {
		boolean hasServerType = false;
		for(String serverType : host.getServerTypes()) {
			if(appMap.containsKey(serverType)) {
				hasServerType = true;
				break;
			}
		}
		if(!hasServerType)
			return false;
		Integer[] ords = ordMap.get(host.getServerEnv());
		if(ords == null || ords.length == 0)
			return false;
		if(ords[0] == null && ords.length == 1)
			return CollectionUtils.iterativeSearch(instances, Integer.parseInt(host.getSimpleName().substring(7))) >= 0;
		return true;
	}
	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(host.isServerType("NET") || host.isServerType("APR")) {
			addPrepareHostTasks(host, tasks, commands, cache);
			String filePath = "USAT_environment_settings.properties";
			tasks.add(new ExecuteTask("sudo su", "cp /opt/USAT/conf/USAT_environment_settings.properties /opt/USAT/conf/USAT_environment_settings.properties." + new SimpleDateFormat("yyyyMMdd-HHmm").format(new Date()), "cp /opt/USAT/conf/USAT_environment_settings.properties ~/" + filePath, "chmod 666 " + filePath));
			UsatSettingUpdateFileTask usatUpdateTask = new UsatSettingUpdateFileTask();
			usatUpdateTask.setFilePath(filePath);
			tasks.add(usatUpdateTask);
			tasks.add(new ExecuteTask("sudo su", "mv " + filePath + " /opt/USAT/conf/USAT_environment_settings.properties", "chmod 640 /opt/USAT/conf/USAT_environment_settings.properties", "chown usat:usat /opt/USAT/conf/USAT_environment_settings.properties"));	
		}
	}
	
	@Override
	protected void addTasks(Host host, App app, Integer ordinal, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareAppTasks(host, app, ordinal, tasks, commands, cache);
		String filePath = app.getName() + (ordinal == null ? "" : ordinal.toString()) + "_USAT_app_settings.properties";
		tasks.add(new ExecuteTask("sudo su", "cp /opt/USAT/conf/USAT_environment_settings.properties " + filePath, "chmod 666 " + filePath));
		final int num = (ordinal == null ? Integer.parseInt(host.getSimpleName().substring(7)) : ordinal);
		AppSettingUpdateFileTask appUpdateTask = new AppSettingUpdateFileTask(app, ordinal, num, instanceCount, cache, null);
		appUpdateTask.setFilePath(filePath);
		if(num < 3) {
			appUpdateTask.registerValue("USAT.simplemq.secondaryDataSources", new HostSpecificValue() {
				public String getValue(Host host, Interaction interaction) {
					return StringUtils.generateList("MQ_", null, ",", num + 1, 1, 1, 2);
				}
				public boolean isOverriding() {
					return true;
				}
			});
			int[] cs;
			switch(num) {
				case 1: cs = new int[]{2,3,4}; break;
				case 2: cs = new int[]{1,2,3}; break;
				//case 3: cs = new int[]{3,4}; break;
				//case 4: cs = new int[]{2,3}; break;
				default: cs = new int[0];
			}
			for(int c : cs) {
				for(App a : apps) {
					if(!"postgres".equalsIgnoreCase(a.getName()))
						appUpdateTask.registerValue("USAT.simplemq.directPollerUrl." + a.getName().toLowerCase() + "_s" + c, new DefaultHostSpecificValue(""));
				}
			}		
		} 
		tasks.add(appUpdateTask);
		addInstallAppTasks(host, app, ordinal, tasks, commands, cache);
	}

	@Override
	protected void addBeforeStopCommands(Host host, App app, Integer ordinal, List<String> commands) {
		String appName = app.getName() + (ordinal == null ? "" : ordinal.toString());
		commands.add("mv ~/" + appName + "_USAT_app_settings.properties /opt/USAT/" + appName + "/specific/USAT_app_settings.properties");
		commands.add("chmod 640 /opt/USAT/" + appName + "/specific/USAT_app_settings.properties");
		commands.add("chown " + app.getName() + ":" + app.getName() + " /opt/USAT/" + appName + "/specific/USAT_app_settings.properties");
	}
	
	@Override
	protected boolean overrideExisting(Host host, App app, Integer ordinal, String filePath) {
		return "setenv.sh".equals(filePath) || 
				"jmx.access".equals(filePath) || 
				super.overrideExisting(host, app, ordinal, filePath);
	}
	@Override
	public String getName() {
		return name;
	}
}
