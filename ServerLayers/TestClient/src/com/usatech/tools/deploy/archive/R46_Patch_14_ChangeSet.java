package com.usatech.tools.deploy.archive;

import java.net.UnknownHostException;

import com.usatech.posm.schedule.FanfUploadJob;
import com.usatech.posm.schedule.SubmerchantsUploadJob;
import com.usatech.posm.schedule.dfr.DFRDatasetHandler;
import com.usatech.posm.schedule.dfr.DFRReport;
import com.usatech.posm.schedule.dfr.DFRReportDef;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R46_Patch_14_ChangeSet extends MultiLinuxPatchChangeSet {
	public R46_Patch_14_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/POSMLayer/conf/POSMLayerService.properties", "classes", "REL_posmlayer_1_23_14", true, USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/POSMLayer/src/main", DFRDatasetHandler.class, "REL_posmlayer_1_23_14", USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/POSMLayer/src/main", DFRReport.class, "REL_posmlayer_1_23_14", USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/POSMLayer/src/main", DFRReportDef.class, "REL_posmlayer_1_23_14", USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/POSMLayer/src/main", SubmerchantsUploadJob.class, "REL_posmlayer_1_23_14", USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/POSMLayer/src/main", FanfUploadJob.class, "REL_posmlayer_1_23_14", USATRegistry.POSM_LAYER);
	}

	@Override
	public String getName() {
		return "R46 Patch 14";
	}
}
