package com.usatech.tools.deploy.archive;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.AppSettingUpdateFileTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.DeployTaskInfo;
import com.usatech.tools.deploy.DeployUtils;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.Server;
import com.usatech.tools.deploy.USATHelper;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.tools.deploy.UploadAndRunPostgresSQLTask;
import com.usatech.tools.deploy.UsatSettingUpdateFileTask;

import oracle.jdbc.driver.OracleDriver;
import simple.app.Processor;
import simple.app.ServiceException;

public class R44_Linux_ChangeSet extends AbstractLinuxChangeSet {
	public R44_Linux_ChangeSet() throws UnknownHostException {
		super();
	}

	@Override
	protected void registerApps() {
		registerApp(USATRegistry.KEYMGR, "2.12.0");
		registerApp(USATRegistry.LOADER, "1.30.0");
		registerApp(USATRegistry.APP_LAYER, "1.30.0");
		registerApp(USATRegistry.INAUTH_LAYER, "1.30.0");
		registerApp(USATRegistry.POSM_LAYER, "1.21.0");
		registerApp(USATRegistry.NET_LAYER, "1.30.0");
		registerApp(USATRegistry.OUTAUTH_LAYER, "1.30.0");
		registerApp(USATRegistry.DMS_APP, "1.6.0");
		registerApp(USATRegistry.HTTPD_NET, null);
		registry.registerSubApps(USATRegistry.HTTPD_NET, USATRegistry.DMS_APP);
		// registerApp(USATRegistry.RDW_LOADER, "3.18.0");
		registerApp(USATRegistry.RPTGEN_LAYER, "3.18.0");
		registerApp(USATRegistry.RPTREQ_LAYER, "3.18.0");
		registerApp(USATRegistry.USALIVE_APP, "1.20.0");
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.USALIVE_APP);
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.VERIZON_APP);
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.HOTCHOICE_APP);
		registerApp(USATRegistry.PREPAID_APP, "1.8.0");
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.GETMORE_APP); // The APP server needs PREPAID_APP, but the WEB server also needs the GETMORE_APP as subapp
		// registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.PREPAID_APP);
		registerApp(USATRegistry.HTTPD_WEB, null);
		registerApp(USATRegistry.TRANSPORT_LAYER, "3.18.0");
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("MST") || super.isApplicable(host);
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(host.isServerType("MST")) {
			String[] instances = registry.getServerSet(host.getServerEnv()).getServer(host, "MST").instances;
			Map<Integer, String[]> dbInst = new HashMap<>();
			if("LOCAL".equalsIgnoreCase(host.getServerEnv()) && instances != null) {
				Set<String> dbNames = new LinkedHashSet<>();
				for(int i = 0; i < instances.length; i++) {
					if(instances[i] == null || instances[i].length() < 2)
						continue;
					switch(instances[i].substring(0, 2)) {
						case "dk":
							dbNames.add("app_1");
							break;
						case "bk":
							dbNames.add("app_2");
							break;
						case "yh":
							dbNames.add("app_3");
							break;
						case "js":
							dbNames.add("app_4");
							break;
						case "pc":
							dbNames.add("app_5");
							break;
						case "ml":
							dbNames.add("app_6");
							break;
					}
				}
				dbInst.put(null, dbNames.toArray(new String[dbNames.size()]));
			} else {
				int count = instances == null ? 1 : instances.length;
				int n = (host.getSimpleName().charAt(host.getSimpleName().length() - 1) - '0') - 1;
				for(int i = 0; i < count; i++) {
					String appdb = "app_" + String.valueOf(n * count + i + 1);
					Integer ordinal = (instances == null || instances[0] == null ? null : i + 1);
					dbInst.put(ordinal, new String[] { appdb });
				}
			}
			for(Map.Entry<Integer, String[]> entry : dbInst.entrySet()) {
				tasks.add(new UploadAndRunPostgresSQLTask("DatabaseScripts/releases/REL_Edge_Update_R44/R44.APP.sql", "HEAD", Boolean.FALSE, entry.getKey(), entry.getValue()));
				tasks.add(new UploadAndRunPostgresSQLTask(new Processor<DeployTaskInfo, InputStream>() {
					@Override
					public InputStream process(DeployTaskInfo deployTaskInfo) throws ServiceException {
						String operUrl;
						if("LOCAL".equals(deployTaskInfo.host.getServerEnv()))
							operUrl = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb012.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=USADEV04.WORLD)))";
						else if("DEV".equals(deployTaskInfo.host.getServerEnv()))
							operUrl = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb011.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=usadev03.world)))";
						else if("INT".equals(deployTaskInfo.host.getServerEnv()))
							operUrl = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=intdb011.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=usadev02.world)))";
						else if("ECC".equals(deployTaskInfo.host.getServerEnv()))
							operUrl = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=eccscan01.usatech.com)(PORT=1525))(ADDRESS=(PROTOCOL=TCP)(HOST=eccscan02.usatech.com)(PORT=1525)))(CONNECT_DATA=(SERVICE_NAME=usaecc_db.world)))";
						else if("USA".equals(deployTaskInfo.host.getServerEnv()))
							operUrl = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.120)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.121)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.122)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.220)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.221)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.222)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)))";
						else
							throw new ServiceException("Server Type '" + deployTaskInfo.host.getServerEnv() + "' is not recognized");

						new OracleDriver();
						char[] password = DeployUtils.getPassword(deployTaskInfo, "SYSTEM@OPER", "SYSTEM user on OPER database");
						final Connection conn;
						try {
							conn = DriverManager.getConnection(operUrl, "SYSTEM", new String(password));
						} catch(SQLException e) {
							deployTaskInfo.passwordCache.setPassword(deployTaskInfo.host, "SYSTEM@OPER", null);
							throw new ServiceException(e);
						}
						final ResultSet rs;
						boolean okay = false;
						try {
							PreparedStatement ps = conn
									.prepareStatement("SELECT 'UPDATE APP_CLUSTER.DEVICE_INFO SET DOING_BUSINESS_AS = ''' || REPLACE(DOING_BUSINESS_AS, '''', '''''') || ''', ADDRESS = ''' || REPLACE(ADDRESS, '''', '''''') || ''', CITY = ''' || REPLACE(CITY, '''', '''''') || ''', STATE_CD = ''' || REPLACE(STATE_CD, '''', '''''') || ''', POSTAL = ''' || REPLACE(POSTAL, '''', '''''') || ''', COUNTRY_CD = ''' || REPLACE(COUNTRY_CD, '''', '''''') || ''', CUSTOMER_SERVICE_PHONE = ''' || REPLACE(CUSTOMER_SERVICE_PHONE, '''', '''''') || ''', CUSTOMER_SERVICE_EMAIL = ''' || REPLACE(CUSTOMER_SERVICE_EMAIL, '''', '''''') || ''', CUSTOMER_ID = ' || CASE WHEN CUSTOMER_ID IS NULL THEN 'NULL' ELSE TO_CHAR(CUSTOMER_ID) END || ' WHERE DEVICE_NAME = ''' || REPLACE(DEVICE_NAME, '''', '''''') || ''';' || CHR(10) FROM APP_LAYER.VW_DEVICE_INFO WHERE DOING_BUSINESS_AS IS NOT NULL OR ADDRESS IS NOT NULL OR CITY IS NOT NULL OR STATE_CD IS NOT NULL OR POSTAL IS NOT NULL OR COUNTRY_CD IS NOT NULL OR CUSTOMER_ID IS NOT NULL ORDER BY DEVICE_NAME");
							rs = ps.executeQuery();
							okay = true;
						} catch(SQLException e) {
							throw new ServiceException(e);
						} finally {
							if(!okay)
								try {
									conn.close();
								} catch(SQLException e) {
									// ignore
								}
						}
						try {
							File file = File.createTempFile("update_device_details_", ".sql");
							file.deleteOnExit();
							Writer out = new FileWriter(file);
							try {
								while(rs.next()) {
									out.write(rs.getString(1));
								}
								out.flush();
							} finally {
								out.close();
							}
							deployTaskInfo.interaction.getWriter().println("Wrote " + rs.getRow() + " rows to temp file");
							deployTaskInfo.interaction.getWriter().flush();
							return new FileInputStream(file);
						} catch(IOException | SQLException e) {
							throw new ServiceException(e);
						}
					}

					@Override
					public Class<InputStream> getReturnType() {
						return InputStream.class;
					}

					@Override
					public Class<DeployTaskInfo> getArgumentType() {
						return DeployTaskInfo.class;
					}
				}, Boolean.FALSE, entry.getKey(), entry.getValue()));
			}
		} else {
			USATHelper.installJava(host, cache, tasks, 8, 25, "jdk-8u25-linux-x64.gz", getWorkDir());
			if(!host.getServerEnv().equalsIgnoreCase("USA") && host.isServerType("WEB"))
				tasks.add(new ExecuteTask("sudo /bin/rm -f /opt/USAT/httpd/conf.d/prepaid.conf"));
			addPrepareHostTasks(host, tasks, cache);
			tasks.add(new UsatSettingUpdateFileTask(registry.getServerSet(host.getServerEnv())));
			tasks.add(new ExecuteTask("sudo find /opt/USAT/ -name '*.dmp' -delete"));
		}

	}
	
	@Override
	protected void addTasks(Host host, final App app, Integer ordinal, String instance, final int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache);
		if(app.getInstallFileName() != null)
			tasks.add(new AppSettingUpdateFileTask(app, ordinal, instanceNum, instanceCount, cache, registry.getServerSet(host.getServerEnv()), false));
		addInstallAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache, commands);
	}

	@Override
	protected void addBeforeRestartCommands(Host host, App app, Integer ordinal, final int instanceNum, List<String> commands, List<DeployTask> tasks) {
		if(USATRegistry.APP_LAYER.equals(app)) {
			int i = 0;
			Server server = null;
			Iterator<Server> iter = registry.getServerSet(host.getServerEnv()).getServers("MST").iterator();
			for(; i < instanceNum;) {
				server = iter.next();
				i += server.instances.length;
			}
			final int port;
			if(server.instances.length == 1 || "LOCAL".equalsIgnoreCase(host.getServerEnv()))
				port = 5432;
			else
				port = 5430 + instanceNum - i + server.instances.length;
			if(!commands.isEmpty())
				tasks.add(new ExecuteTask(commands.toArray(new String[commands.size()])));
			commands.clear();
			commands.add("sudo su");
			final Host mstHost = server.host;
			final StringBuilder sb = new StringBuilder();
			sb.append("su - postgres -c '/usr/pgsql-latest/bin/psql app_").append(instanceNum).append(" -p ").append(port).append(" -c \"TRUNCATE TABLE APP_CLUSTER.PAYMENT_SUBTYPE_DETAIL;\"'");
			tasks.add(new DeployTask() {
				@Override
				public String getDescription(DeployTaskInfo deployTaskInfo) {
					return "Truncating PAYMENT_SUBTYPE_DETAIL";
				}

				@Override
				public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
					ExecuteTask.executeCommands(mstHost, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache.getSshClient(mstHost, deployTaskInfo.user), "sudo su", sb.toString());
					return "Done";
				}
			});
		}
		super.addBeforeRestartCommands(host, app, ordinal, instanceNum, commands, tasks);
	}

	@Override
	public String getName() {
		return "Edge Server - R44 Install - Apps";
	}
}
