package com.usatech.tools.deploy.archive;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import simple.io.EncodingInputStream;
import simple.io.ReplacementsLineFilteringReader;

import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.LoopbackInterfaceMap;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.tools.deploy.UnixUploadTask;

public class  HttpdVirtualHostNETChangeSet implements ChangeSet {
	
	public static final File baseDir = new File(System.getProperty("rootSourceDir", "D:\\Development\\Java Projects"));
	protected static final File buildsDir = new File(baseDir, "ApplicationBuilds");
	protected static final File serverAppConfigDir = new File(baseDir, "server_app_config");

	@Override
	public String getName() {
		return "NET Server Httpd virtual host using loopback interface Task";
	}
	
	@Override
	public String toString() {
		return getName();
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("NET");
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache)
			throws IOException {
		String interfaceStr;
		if("ECC".equalsIgnoreCase(host.getServerEnv())||"USA".equalsIgnoreCase(host.getServerEnv())) {
			interfaceStr="bond0";
		}else{
			interfaceStr="eth0";
		}
		ArrayList<DeployTask> tasks=new ArrayList<DeployTask>();
		// 1. add to sysctl.conf and /sbin/sysctl -p
		ExecuteTask addToSysctlConf=new ExecuteTask("sudo su",
				"cp /etc/sysctl.conf /etc/sysctl.conf.netbak",
				"grep -c 'net.ipv4.conf.all.arp_ignore=1' /etc/sysctl.conf || echo 'net.ipv4.conf.all.arp_ignore=1' >> /etc/sysctl.conf",
				"grep -c 'net.ipv4.conf."+interfaceStr+".arp_ignore=1' /etc/sysctl.conf || echo 'net.ipv4.conf."+interfaceStr+".arp_ignore=1' >> /etc/sysctl.conf",
				"grep -c 'net.ipv4.conf.all.arp_announce=2' /etc/sysctl.conf || echo 'net.ipv4.conf.all.arp_announce=2' >> /etc/sysctl.conf",
				"grep -c 'net.ipv4.conf."+interfaceStr+".arp_announce=2' /etc/sysctl.conf || echo 'net.ipv4.conf."+interfaceStr+".arp_announce=2' >> /etc/sysctl.conf",
				"/sbin/sysctl -e -p"
			);
		
		tasks.add(addToSysctlConf);
		
		StringBuilder iptablesSedStr=new StringBuilder();
		if(!"USA".equalsIgnoreCase(host.getServerEnv())){
			String realIp= InetAddress.getByName(host.getSimpleName() + "-vip.usatech.com").getHostAddress();
			iptablesSedStr.append("/-A PREROUTING -d "+realIp+"/d;");
		}
		HashMap<String,String> loopbackMap=LoopbackInterfaceMap.getMapByEnv(host.getServerEnv());
		
		//2. add loopback interface
		for(App subApp : new App[] { USATRegistry.DMS_APP }) {
			String subHostPrefix=subApp.getName();
			String subhostname = subHostPrefix + ("USA".equalsIgnoreCase(host.getServerEnv()) ? ".usatech.com" : "-" + host.getServerEnv().toLowerCase() + ".usatech.com");
			String subhostip = InetAddress.getByName(subhostname).getHostAddress();
			String deviceName="lo:"+loopbackMap.get(subhostip);
			tasks.add(new ExecuteTask("sudo su",
					"test -d /etc/sysconfig/network-scripts/ifcfg-"+deviceName+" || echo 'DEVICE="+deviceName+"\nIPADDR="+subhostip+"\nNETMASK=0.0.0.0\nONBOOT=yes' >> /etc/sysconfig/network-scripts/ifcfg-"+deviceName,
					"ifup "+deviceName)
					);
			iptablesSedStr.append("/-A PREROUTING -d "+subhostip+"\\/32 -p tcp -j REDIRECT/d;");
			iptablesSedStr.append("/-A PREROUTING -d "+subhostip+"\\/32 -p udp -j REDIRECT/d;");
		}
		
		//3. update conf/httpd and conf.d folder files
		ExecuteTask httpdConfBak=new ExecuteTask("sudo su",
				"cp /opt/USAT/httpd/conf/httpd.conf /opt/USAT/httpd/conf/httpd.conf.netbak",
				"cp -pr /opt/USAT/httpd/conf.d /opt/USAT/httpd/conf.d.netbak"
			);
		tasks.add(httpdConfBak);
		tasks.add(new UnixUploadTask(new File(serverAppConfigDir, "WEB/Httpd/conf/httpd.conf"), "/opt/USAT/httpd/conf", 0644, USATRegistry.HTTPD_NET.getUserName(), USATRegistry.HTTPD_NET.getUserName(), true));
		
		String appServer;
		String last2 = host.getSimpleName().substring(6);
		String instanceNum = host.getSimpleName().substring(7);
		if("USA".equalsIgnoreCase(host.getServerEnv())) {
			appServer = "app";
		} else {
			appServer = "apr";
		}

		String lb_ip_escaped;
		if("DEV".equalsIgnoreCase(host.getServerEnv()))
			lb_ip_escaped = "10\\\\.0\\\\.0\\\\.64";
		else if("INT".equalsIgnoreCase(host.getServerEnv()))
			lb_ip_escaped = "10\\\\.0\\\\.0\\\\.64";
		else if("ECC".equalsIgnoreCase(host.getServerEnv()))
			lb_ip_escaped = "192\\\\.168\\\\.4\\\\.65";
		else if("USA".equalsIgnoreCase(host.getServerEnv()))
			lb_ip_escaped = "192\\\\.168\\\\.79\\\\.(?:18[34]|19[56])";
		else
			lb_ip_escaped = "";
		
		for(App subApp : new App[] { USATRegistry.DMS_APP }) {
			String subHostPrefix=subApp.getName();
			String subhostname = subHostPrefix + ("USA".equalsIgnoreCase(host.getServerEnv()) ? ".usatech.com" : "-" + host.getServerEnv().toLowerCase() + ".usatech.com");
			String subhostip = InetAddress.getByName(subhostname).getHostAddress();
			Map<Pattern, String> replacements = new HashMap<Pattern, String>();
			replacements.put(Pattern.compile("<APP_SERVER_NAME>"), host.getServerEnv().toLowerCase() + appServer + last2);
			replacements.put(Pattern.compile("<WORKER_NAME>"), "worker_" + instanceNum);
			replacements.put(Pattern.compile("<WEB_SERVER_NAME>"), subhostname);
			replacements.put(Pattern.compile("<WEB_SERVER_IP>"), subhostip);
			replacements.put(Pattern.compile("<WEB_SERVER_IP_ESCAPED>"), subhostip.replace(".", "\\\\."));
			replacements.put(Pattern.compile("<LOADBALANCER_IP_ESCAPED>"), lb_ip_escaped);
			File file = new File(serverAppConfigDir, "WEB/Httpd/conf.d/" + subApp.getName() + ".conf");
			tasks.add(new UnixUploadTask(new EncodingInputStream(new ReplacementsLineFilteringReader(new BufferedReader(new FileReader(file)), "\n", replacements)), "/opt/USAT/httpd/conf.d/" + file.getName(), 0644, USATRegistry.HTTPD_NET.getUserName(), USATRegistry.HTTPD_NET.getUserName(), true, file.getAbsolutePath()));
		}
		
		iptablesSedStr.deleteCharAt(iptablesSedStr.length()-1);
		//4. remove iptables rules
		ExecuteTask iptablesRemove=new ExecuteTask("sudo su",
				"cp /etc/sysconfig/iptables /etc/sysconfig/iptables.netbak",
				"sed '"+iptablesSedStr+"' /etc/sysconfig/iptables >/etc/sysconfig/iptables.new",
				"mv /etc/sysconfig/iptables.new /etc/sysconfig/iptables",
				"/etc/init.d/iptables restart"
			);
		tasks.add(iptablesRemove);
		// monit restart httpd
		tasks.add(new ExecuteTask("sudo su",
				"/usr/monit-latest/bin/monit restart httpd"
			));
		DeployTask[] deployTasks=new DeployTask[tasks.size()];
		for(int i=0; i<tasks.size();i++){
			deployTasks[i]=tasks.get(i);
		}
		return deployTasks;
	}

}
