package com.usatech.tools.deploy.archive;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.DeployTaskInfo;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.IPFUpdateFileTask;
import com.usatech.tools.deploy.PasswordCache;



public class OpenMSTPorts_ChangeSet implements ChangeSet {

	@Override
	public String getName() {
		return "Open MST ports";
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("MST") && "USA".equalsIgnoreCase(host.getServerEnv());
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		IPFUpdateFileTask ipfTask = new IPFUpdateFileTask(true);
		ipfTask.setFilePath("/etc/ipf/ipf.conf");
		for(int i = 1; i <= 5; i++) {
			ipfTask.allowTcp("192.168.77.7" + i, 5432);
			ipfTask.allowTcp("192.168.79.17" + i, 5432);
		}
		String backup = "/etc/ipf/ipf.conf." + new SimpleDateFormat("yyyyMMdd-HHmmss").format(new Date());
		return new DeployTask[] {
			new ExecuteTask("sudo su", "cp /etc/ipf/ipf.conf " + backup),
			ipfTask,
			new DeployTask() {				
				@Override
			public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
						deployTaskInfo.interaction.readLine("IPF was updated. Press enter to reload firewall");
				return "Complete";
				}

			@Override
			public String getDescription(DeployTaskInfo deployTaskInfo) {
				return "Copy IPF config";
			}
			},
			new ExecuteTask("sudo su", "diff -b /etc/ipf/ipf.conf " + backup + " || /usr/sbin/ipf -Fa -f /etc/ipf/ipf.conf")
		};
	}

	@Override
	public String toString() {
		return getName();
	}
}
