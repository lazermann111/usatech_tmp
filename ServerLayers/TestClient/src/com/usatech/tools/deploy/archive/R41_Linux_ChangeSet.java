package com.usatech.tools.deploy.archive;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.List;

import simple.app.Processor;
import simple.app.ServiceException;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.AppSettingUpdateFileTask;
import com.usatech.tools.deploy.CvsPullTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.DeployTaskInfo;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.tools.deploy.UploadCoteTask;
import com.usatech.tools.deploy.UsatSettingUpdateFileTask;

public class R41_Linux_ChangeSet extends AbstractLinuxChangeSet {
	protected static final Processor<DeployTaskInfo, InputStream> EMPTY_RESOURCE = new Processor<DeployTaskInfo, InputStream>() {
		public InputStream process(DeployTaskInfo argument) throws ServiceException {
			return new ByteArrayInputStream(new byte[0]);
		}

		public Class<InputStream> getReturnType() {
			return InputStream.class;
		}

		public Class<DeployTaskInfo> getArgumentType() {
			return DeployTaskInfo.class;
		}
	};

	public R41_Linux_ChangeSet() throws UnknownHostException {
		super();
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("ODB") || super.isApplicable(host);
	}

	@Override
	protected void registerApps() {
		registerApp(USATRegistry.KEYMGR, "2.10.0");
		registerApp(USATRegistry.LOADER, "1.27.0");
		registerApp(USATRegistry.APP_LAYER, "1.27.0");
		registerApp(USATRegistry.INAUTH_LAYER, "1.27.0");
		registerApp(USATRegistry.POSM_LAYER, "1.18.0");
		registerApp(USATRegistry.NET_LAYER, "1.27.0");
		registerApp(USATRegistry.OUTAUTH_LAYER, "1.27.0");
		registerApp(USATRegistry.DMS_APP, "1.4.0");
		registerApp(USATRegistry.HTTPD_NET, null);
		registry.registerSubApps(USATRegistry.HTTPD_NET, USATRegistry.DMS_APP);
		registerApp(USATRegistry.RPTGEN_LAYER, "3.15.0");
		registerApp(USATRegistry.RPTREQ_LAYER, "3.15.0");
		registerApp(USATRegistry.USALIVE_APP, "1.17.0");
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.USALIVE_APP);
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.VERIZON_APP);
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.HOTCHOICE_APP);
		registerApp(USATRegistry.PREPAID_APP, "1.6.0");
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.GETMORE_APP); // The APP server needs PREPAID_APP, but the WEB server also needs the GETMORE_APP as subapp
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.PREPAID_APP);
		registerApp(USATRegistry.HTTPD_WEB, null);
		registerApp(USATRegistry.TRANSPORT_LAYER, "3.15.0");
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(host.isServerType("ODB")) {
			String subdomain;
			if("USA".equalsIgnoreCase(host.getServerEnv()))
				subdomain = "getmore.usatech.com";
			else if("LOCAL".equalsIgnoreCase(host.getServerEnv()))
				subdomain = "localhost"; // java.net.Inet4Address.getLocalHost().getCanonicalHostName();
			else
				subdomain = "getmore-" + host.getServerEnv().toLowerCase() + ".usatech.com";
			tasks.add(new UploadCoteTask("top_logo", "image/png", subdomain, new CvsPullTask("Prepaid/web/images/icon-logo_more-white.png", "HEAD")));
			tasks.add(new UploadCoteTask("top_logo_welcome", "image/png", subdomain, new CvsPullTask("Prepaid/web/images/icon-logo_more.png", "HEAD")));
			tasks.add(new UploadCoteTask("extra_style", "text/css", subdomain, EMPTY_RESOURCE));
			tasks.add(new UploadCoteTask("home_image", "image/png", subdomain, new CvsPullTask("Prepaid/web/images/home-logo_more.png", "HEAD")));
			tasks.add(new UploadCoteTask("cash_icon", "image/png", subdomain, new CvsPullTask("Prepaid/web/images/icon-cash60x50.png", "HEAD")));
			tasks.add(new UploadCoteTask("sale_icon", "image/png", subdomain, new CvsPullTask("Prepaid/web/images/icon-sale60x50.png", "HEAD")));
			tasks.add(new UploadCoteTask("new_icon", "image/png", subdomain, new CvsPullTask("Prepaid/web/images/icon-new60x50.png", "HEAD")));
			tasks.add(new UploadCoteTask("email_logo", "image/png", subdomain, new CvsPullTask("Prepaid/web/images/more-logo315x40.png", "HEAD")));
			tasks.add(new UploadCoteTask("top_logo", "image/png", subdomain + "/pepi", new CvsPullTask("Prepaid/web/images/icon-logo_pepi-white.png", "HEAD")));
			tasks.add(new UploadCoteTask("top_logo_welcome", "image/png", subdomain + "/pepi", new CvsPullTask("Prepaid/web/images/icon-logo_pepi.png", "HEAD")));
			tasks.add(new UploadCoteTask("extra_style", "text/css", subdomain + "/pepi", new CvsPullTask("Prepaid/web/css/pepi.css", "HEAD")));
			tasks.add(new UploadCoteTask("bottom_logo", "image/png", subdomain + "/pepi", new CvsPullTask("Prepaid/web/images/logo-pepi-footer200x80.png", "HEAD")));
			tasks.add(new UploadCoteTask("home_image", "image/png", subdomain + "/pepi", new CvsPullTask("Prepaid/web/images/home-logo_simplymore-simplybetter.png", "HEAD")));
			tasks.add(new UploadCoteTask("cash_icon", "image/png", subdomain + "/pepi", new CvsPullTask("Prepaid/web/images/icon-cash60x50_pepi.png", "HEAD")));
			tasks.add(new UploadCoteTask("sale_icon", "image/png", subdomain + "/pepi", new CvsPullTask("Prepaid/web/images/icon-sale60x50_pepi.png", "HEAD")));
			tasks.add(new UploadCoteTask("new_icon", "image/png", subdomain + "/pepi", new CvsPullTask("Prepaid/web/images/icon-new60x50_pepi.png", "HEAD")));
			tasks.add(new UploadCoteTask("new_user_image", "image/png", subdomain + "/pepi", new CvsPullTask("Prepaid/web/images/logo-pepi-signup150x60.png", "HEAD")));
			tasks.add(new UploadCoteTask("email_logo", "image/png", subdomain + "/pepi", new CvsPullTask("Prepaid/web/images/more-logo315x40_pepi.png", "HEAD")));
			tasks.add(new UploadCoteTask("email_logo", "image/png", subdomain + "/pepi", new CvsPullTask("Prepaid/web/images/more-logo315x40_pepi.png", "HEAD")));
		} else {
			addPrepareHostTasks(host, tasks, cache);
			tasks.add(new UsatSettingUpdateFileTask(registry.getServerSet(host.getServerEnv())));
			tasks.add(new ExecuteTask("sudo find /opt/USAT/ -name '*.dmp' -delete"));
		}
	}
	
	@Override
	protected void addTasks(Host host, final App app, Integer ordinal, String instance, final int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache);
		if(app.getInstallFileName() != null)
			tasks.add(new AppSettingUpdateFileTask(app, ordinal, instanceNum, instanceCount, cache, registry.getServerSet(host.getServerEnv()), false));
		addInstallAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache, commands);
	}

	@Override
	public String getName() {
		return "Edge Server - R41 Install - Apps";
	}
}
