package com.usatech.tools.deploy;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Types;

import oracle.jdbc.driver.OracleDriver;
import simple.app.Processor;
import simple.app.ServiceException;
import simple.db.Argument;
import simple.db.Call;
import simple.db.Parameter;
import simple.db.ParameterException;
import simple.sql.SQLType;

public class UploadCoteTask implements DeployTask {
	protected String coteFileName;
	protected String coteContentType;
	protected String subdomain;
	protected Processor<DeployTaskInfo, InputStream> resource;
	protected static final Call UPSERT_CALL = new Call();
	static {
		UPSERT_CALL.setSql("{? = call WEB_CONTENT.UPSERT_COTE(?,?,?,?)}");
		UPSERT_CALL.setArguments(new Argument[] {
			new	Parameter("result", new SQLType(Types.NUMERIC), false, false, true),
			new	Parameter("coteId", new SQLType(Types.NUMERIC), false, false, true),
			new	Parameter("coteFileName", new SQLType(Types.VARCHAR,  null, 100, null), true, true, false),
			new	Parameter("coteContentType", new SQLType(Types.VARCHAR,  null, 100, null), true, true, false),
			new	Parameter("subdomainUrl", new SQLType(Types.VARCHAR,  null, 100, null), false, true, false),
			new	Parameter("coteContent", new SQLType(Types.BLOB), true, true, false),
		});		
	}

	public UploadCoteTask() {
	}
	
	public UploadCoteTask(String coteFileName, String coteContentType, String subdomain, Processor<DeployTaskInfo, InputStream> resource) {
		this();
		this.coteFileName = coteFileName;
		this.coteContentType = coteContentType;
		this.subdomain = subdomain;
		this.resource = resource;
	}

	@Override
	public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
		InputStream in;
		try {
			in = resource.process(deployTaskInfo);
		} catch(ServiceException e) {
			throw new IOException(e);
		}
		try {
			Connection conn = getConnection(deployTaskInfo);
			try {
				UPSERT_CALL.executeCall(conn, new Object[] { coteFileName, coteContentType, subdomain, in }, null);
				conn.commit();
			} finally {
				conn.close();
			}
		} catch(ParameterException e) {
			throw new IOException(e);
		} catch(SQLException e) {
			throw new IOException(e);
		} finally {
			in.close();
		}
		return "Uploaded resource " + resource + " to cote " + coteFileName + " for " + subdomain;
	}

	@SuppressWarnings("unused")
	protected Connection getConnection(DeployTaskInfo deployTaskInfo) throws IOException {
		String operUrl;
		if("LOCAL".equals(deployTaskInfo.host.getServerEnv()))
			operUrl = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=ECCDB013.USATECH.COM)(PORT=1523)))(CONNECT_DATA=(SERVICE_NAME=USAODEV04)))";
		else if("DEV".equals(deployTaskInfo.host.getServerEnv()))
			operUrl = UsatSettingUpdateFileTask.OPER_DEV;
		else if("INT".equals(deployTaskInfo.host.getServerEnv()))
			operUrl = UsatSettingUpdateFileTask.OPER_INT;
		else if("ECC".equals(deployTaskInfo.host.getServerEnv()))
			operUrl = UsatSettingUpdateFileTask.OPER_ECC;
		else if("USA".equals(deployTaskInfo.host.getServerEnv()))
			operUrl = UsatSettingUpdateFileTask.OPER_PROD_DEFAULT;
		else
			throw new IOException("Server Type '" + deployTaskInfo.host.getServerEnv() + "' is not recognized");

		new OracleDriver();
		char[] password = DeployUtils.getPassword(deployTaskInfo, "SYSTEM@OPER", "SYSTEM user on OPER database");
		Connection conn;
		try {
			conn = DriverManager.getConnection(operUrl, "SYSTEM", new String(password));
		} catch(SQLException e) {
			deployTaskInfo.passwordCache.setPassword(deployTaskInfo.host, "SYSTEM@OPER", null);
			throw new IOException(e);
		}
		return conn;
	}
	@Override
	public String getDescription(DeployTaskInfo deployTaskInfo) {
		return "Uploading resource " + resource + " to cote " + coteFileName + " for " + subdomain;
	}

	public String getCoteFileName() {
		return coteFileName;
	}

	public void setCoteFileName(String coteFileName) {
		this.coteFileName = coteFileName;
	}

	public String getCoteContentType() {
		return coteContentType;
	}

	public void setCoteContentType(String coteContentType) {
		this.coteContentType = coteContentType;
	}

	public String getSubdomain() {
		return subdomain;
	}

	public void setSubdomain(String subdomain) {
		this.subdomain = subdomain;
	}

	public Processor<DeployTaskInfo, InputStream> getResource() {
		return resource;
	}

	public void setResource(Processor<DeployTaskInfo, InputStream> resource) {
		this.resource = resource;
	}
}
