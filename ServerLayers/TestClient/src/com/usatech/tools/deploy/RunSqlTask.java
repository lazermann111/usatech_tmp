package com.usatech.tools.deploy;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class RunSqlTask implements DeployTask {
	protected String driver = "oracle.jdbc.driver.OracleDriver";
	protected String sql;
	protected String userName = "SYSTEM";
	protected String dbName = "OPER";

	public RunSqlTask() {
	}

	public RunSqlTask(String sql) {
		this.sql = sql;
	}
	protected String getDbUrl(DeployTaskInfo deployTaskInfo) throws IOException {
		if("DEV".equals(deployTaskInfo.host.getServerEnv()))
			return "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb011.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=usadev03.world)))";
		else if("INT".equals(deployTaskInfo.host.getServerEnv()))
			return "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=intdb011.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=usadev02.world)))";
		else if("ECC".equals(deployTaskInfo.host.getServerEnv()))
			return "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=eccscan01.usatech.com)(PORT=1525))(ADDRESS=(PROTOCOL=TCP)(HOST=eccscan02.usatech.com)(PORT=1525)))(CONNECT_DATA=(SERVICE_NAME=usaecc_db.world)))";
		else if("USA".equals(deployTaskInfo.host.getServerEnv()))
			return "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.120)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.121)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.122)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.220)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.221)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.222)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)))";
		else
			throw new IOException("Server Type '" + deployTaskInfo.host.getServerEnv() + "' is not recognized");
	}
	@Override
	public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
		String operUrl = getDbUrl(deployTaskInfo);
		try {
			Class.forName(getDriver());
		} catch(ClassNotFoundException e) {
			throw new IOException("Driver '" + getDriver() + "' not found in classpath");
		}
		char[] password = DeployUtils.getPassword(deployTaskInfo, getConnectionKey(), getConnectionDesc());
		Connection conn;
		try {
			conn = DriverManager.getConnection(operUrl, getUserName(), new String(password));
		} catch(SQLException e) {
			deployTaskInfo.passwordCache.setPassword(deployTaskInfo.host, getConnectionKey(), null);
			throw new IOException(e);
		}
		int rows;
		try {
			conn.setAutoCommit(true);
			PreparedStatement ps = conn.prepareStatement(getSql());
			try {
				rows = ps.executeUpdate();
			} finally {
				ps.close();
			}
		} catch(SQLException e) {
			throw new IOException(e);
		} finally {
			try {
				conn.close();
			} catch(SQLException e) {
			}
		}
		return "Updated " + rows + " rows with:\n" + getSql();
	}

	@Override
	public String getDescription(DeployTaskInfo deployTaskInfo) {
		return "Execute SQL on " + getConnectionDesc();
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public String getConnectionKey() {
		return getUserName() + '@' + getDbName();
	}

	public String getConnectionDesc() {
		return getUserName() + " user on " + getDbName() + " database";
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}
}
