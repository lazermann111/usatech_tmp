package com.usatech.tools.deploy;

import java.io.IOException;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.sshtools.j2ssh.SftpClient;
import com.sshtools.j2ssh.SshClient;

import simple.io.Interaction;

public class DeployTaskInfo {
	public final Interaction interaction;
	public final Host host;
	public final String user;
	public final PasswordCache passwordCache;
	public final SshClientCache sshClientCache;
	protected SftpClient sftp;

	public DeployTaskInfo(Interaction interaction, Host host, String user, PasswordCache passwordCache, SshClientCache sshClientCache) {
		this.interaction = interaction;
		this.host = host;
		this.user = user;
		this.passwordCache = passwordCache;
		this.sshClientCache = sshClientCache;
	}

	public SftpClient getSftp() throws IOException {
		return sshClientCache.getSftpClient(host, user);
	}

	public SshClient getSsh() throws IOException {
		return sshClientCache.getSshClient(host, user);
	}
	
	public Session getJsch() throws IOException, JSchException {
		return sshClientCache.getJSchClient(host, user);
	}
}
