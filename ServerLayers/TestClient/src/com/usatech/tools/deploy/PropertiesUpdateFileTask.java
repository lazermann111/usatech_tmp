package com.usatech.tools.deploy;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Map;

import simple.io.EOLType;
import simple.io.EnhancedBufferedReader;
import simple.text.StringUtils;

public class PropertiesUpdateFileTask extends AbstractUpdateFileTask {
	protected static final Object COMMENT_MARKER = new Object();
	protected static final Object LITERAL_MARKER = new Object();
	protected EOLType eolType = null;
	protected EOLType existingEolType = null;
	
	public PropertiesUpdateFileTask(boolean keepExisting, Integer permissions, String owner, String group) {
		super(keepExisting, permissions, owner, group);
	}

	@Override
	protected void readFile(InputStream in, Map<String, Object> values) throws IOException {
		EnhancedBufferedReader reader = new EnhancedBufferedReader(new InputStreamReader(in));
		StringBuilder contValue = null;
		for(String line; (line=reader.readLine()) != null; ) {
			EOLType currentEolType = EOLType.getEOLType(reader.readEOL());
			if(currentEolType != null && currentEolType != EOLType.EOF && currentEolType != EOLType.NONE) {
				if(existingEolType == null)
					existingEolType = currentEolType;
				else if(existingEolType != currentEolType)
					existingEolType = EOLType.EOF;
			}
			//remove leading spaces
			int offset = 0;
			while(offset < line.length() && Character.isWhitespace(line.charAt(offset)))
				offset++;
			if(offset >= line.length()) {
				contValue = null;
			} else if(contValue != null) {
				int end = line.length() - 1;
				while(end >= offset && Character.isWhitespace(line.charAt(end)))
					end--;
				if(end >= offset && line.charAt(end) == '\\' && (end == offset || line.charAt(end-1) != '\\')) {
					contValue.append(line, offset, end);
					appendUnescaped(contValue, line, offset, end, false);
				} else {
					appendUnescaped(contValue, line, offset, line.length(), false);
					contValue = null;
				}
			} else {
				char ch = line.charAt(offset);
				if(ch == '#' || ch == '!') {
					String value;
					//*XXX: Should this be the second one?
					value = line.substring(offset);
					/*/
					StringBuilder sb = new StringBuilder();
					appendUnescaped(sb, line, offset, line.length(), false);
					value = sb.toString();
					// */
					values.put(value, COMMENT_MARKER);
				} else {
					//find the separator
					StringBuilder key = new StringBuilder();
					int start = appendUnescaped(key, line, offset, line.length(), true);
					if(++start > line.length())
						values.put(line, LITERAL_MARKER);
					else {
						int end = line.length() - 1;
						while(end >= start && Character.isWhitespace(line.charAt(end)))
							end--;
						while(Character.isWhitespace(key.charAt(key.length() - 1)))
							key.setLength(key.length() - 1);
						if(end >= start && line.charAt(end) == '\\' && (end == start || line.charAt(end-1) != '\\')) {
							contValue = new StringBuilder();
							values.put(key.toString(), contValue);
							appendUnescaped(contValue, line, start, end, false);
						} else {
							StringBuilder sb = new StringBuilder();
							appendUnescaped(sb, line, start, line.length(), false);
							values.put(key.toString(), sb.toString());						
						}
					}
				}				
			}
		}
	}

	@Override
	protected void writeFile(OutputStream out, Map<String, Object> values) throws IOException {
		String eol;
		if(eolType != null && eolType != EOLType.EOF && eolType != EOLType.NONE)
			eol = eolType.getEol();
		else if(existingEolType != null && existingEolType != EOLType.EOF && existingEolType != EOLType.NONE)
			eol = existingEolType.getEol();
		else
			eol = EOLType.LF.getEol();
		StringBuilder line = new StringBuilder();
		for(Map.Entry<String, Object> entry : values.entrySet()) {
			line.setLength(0);
			if(entry.getValue() != null) {
				if(entry.getValue() == COMMENT_MARKER) {
					String s = entry.getKey();
					char ch = s.trim().charAt(0);
					if(ch != '#' && ch != '!') 
						line.append('#');
					line.append(s);
				} else if(entry.getValue() == LITERAL_MARKER) {
					line.append(entry.getKey());
				} else if(entry.getKey() != null) {
					appendEscaped(line, entry.getKey(), true);
					appendSeparator(line);
					appendEscaped(line, entry.getValue().toString(), false);
				}
				line.append(eol);
				out.write(line.toString().getBytes());
			}
		}
		out.flush();
	}
	protected StringBuilder appendSeparator(StringBuilder line) {
		return line.append('=');
	}
	protected StringBuilder appendEscaped(StringBuilder line, CharSequence text, boolean full) {
		return appendEscaped(line, text, 0, text.length(), full);
	}
	protected StringBuilder appendEscaped(StringBuilder line, CharSequence text, int start, int end, boolean full) {
		for(int i = start; i < end; i++) {
			char ch = text.charAt(i);
			switch(ch) {
				case '=': case ':': case ' ':
					if(!full) {
						line.append(ch);
						break;
					}
					//fall-through
				case '\\': case '#': case '!':
					line.append('\\').append(ch);
					break;
				case '\t': 
					line.append("\\t"); 
					break;
				case '\n': 
					line.append("\\n"); 
					break;
				case '\f': 
					line.append("\\f"); 
					break;
				case '\r': 
					line.append("\\r"); 
					break;
                default:
                	if(ch >= ' ' && ch <= '~')
                		line.append(ch);
                	else {
                		line.append("\\u");
                		StringUtils.appendHex(line, (short)ch);
                	}
			}
		}
		return line;
	}

	protected int appendUnescaped(StringBuilder key, CharSequence line, int start, int end, boolean stopOnSeparator) {
		LOOP: for(; start < end; start++) {
			char ch = line.charAt(start);
			switch(ch) {
				case '\\': // escaped char
					if(++start < end) {
						ch = line.charAt(start);
						switch(ch) {
							case 'u':
								if(start + 4 < end) {
									start++;
				                    // Read the xxxx
				                    int value = 0;
				                    for(int i = 0; i < 4; i++) {
				                    	ch = line.charAt(start++);												
				                        switch (ch) {
				                          case '0': case '1': case '2': case '3': case '4':
				                          case '5': case '6': case '7': case '8': case '9':
				                             value = (value << 4) + ch - '0';
				                             break;
				                          case 'a': case 'b': case 'c':
				                          case 'd': case 'e': case 'f':
				                             value = (value << 4) + 10 + ch - 'a';
				                             break;
				                          case 'A': case 'B': case 'C':
				                          case 'D': case 'E': case 'F':
				                             value = (value << 4) + 10 + ch - 'A';
				                             break;
				                          default:
				                              throw new IllegalArgumentException("Malformed \\uxxxx encoding.");
				                        }
				                     }
				                     key.append((char)value);
								} else {
									key.append(line, start - 1, end);
									start = end;
								}
								break;
							case 't': 
								key.append('\t'); 
								break;
							case 'n': 
								key.append('\n'); 
								break;
							case 'f': 
								key.append('\f'); 
								break;
							case 'r': 
								key.append('\r'); 
								break;
		                    default:
		                    	key.append(ch);
		                }
					}
					break;
				case '=': case ':':
					if(stopOnSeparator)
						break LOOP;
					//fall-thru
				default:
					key.append(ch);
			}	
		}
		return start;
	}
}
