package com.usatech.tools.deploy;

import simple.io.Interaction;

public class GetPasswordHostSpecificValue implements HostSpecificValue {
	protected final String key;
	protected final String desc;
	protected final PasswordCache cache;
	protected final boolean overriding;
	/**
	 * when boolean overriding is false, if the setting exists, the GetPasswordHostSpecificValue will not prompt but will use the existing setting value
	 * when boolean overriding is true, if the setting exists, the GetPasswordHostSpecificValue will always prompt for the setting value
	 * if the setting does not exist, it will always prompt for the value
	 * @param cache
	 * @param key
	 * @param desc
	 */
	public GetPasswordHostSpecificValue(PasswordCache cache, String key, String desc) {
		this(cache, key, desc, false);
	}
	/**
	 * when boolean overriding is false, if the setting exists, the GetPasswordHostSpecificValue will not prompt but will use the existing setting value
	 * when boolean overriding is true, if the setting exists, the GetPasswordHostSpecificValue will always prompt for the setting value
	 * if the setting does not exist, it will always prompt for the value
	 * @param cache
	 * @param key
	 * @param desc
	 */
	public GetPasswordHostSpecificValue(PasswordCache cache, String key, String desc, boolean overriding) {
		this.cache = cache;
		this.key = key;
		this.desc = desc;
		this.overriding = overriding;
	}

	@Override
	public String getValue(Host host, Interaction interaction) {
		char[] pwd = DeployUtils.getPassword(cache, host, key, desc, interaction);
		return (pwd == null ? null : new String(pwd));
	}

	@Override
	public boolean isOverriding() {
		return overriding;
	}
}
