package com.usatech.tools.deploy;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import simple.io.BufferStream;
import simple.io.HeapBufferStream;
import simple.io.Interaction;
import simple.util.CollectionUtils;


public abstract class AbstractUpdateFileTask implements DeployTask {	
	protected final Map<String,HostSpecificValue> valueMap = new LinkedHashMap<String, HostSpecificValue>();
	protected final boolean keepExisting;
	protected boolean loadExisting = false;
	protected String filePath;
	protected final Integer permissions;
	protected final String owner;
	protected final String group;

	protected AbstractUpdateFileTask(boolean keepExisting, Integer permissions, String owner, String group) {
		this.keepExisting = keepExisting;
		this.permissions = permissions;
		this.owner = owner;
		this.group = group;
	}

	public void registerValue(String propertyName, HostSpecificValue value) {
		valueMap.put(propertyName, value);
		if(!loadExisting && value != null && !value.isOverriding())
			loadExisting = true;
	}
	protected Map<String,Object> getValues(Map<String,Object> existingValues, Interaction interaction, Host host) {
		final Map<String,Object> values = new LinkedHashMap<String, Object>(valueMap.size());
		if(keepExisting)
			values.putAll(existingValues);
		for(Map.Entry<String, HostSpecificValue> entry : valueMap.entrySet()) {
			if(entry.getValue().isOverriding() || !existingValues.containsKey(entry.getKey()))
				values.put(entry.getKey(), entry.getValue().getValue(host,interaction));
			else if(!keepExisting)
				values.put(entry.getKey(), existingValues.get(entry.getKey()));				
		}
		return values;
	}
	@Override
	public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
		final Map<String,Object> existingValues;
		final BufferStream bufferStream = new HeapBufferStream();	
		if((loadExisting || keepExisting) && UploadTask.readFileUsingTmp(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, getFilePath(), bufferStream.getOutputStream(), null)) {
			existingValues = new LinkedHashMap<String, Object>(valueMap.size());
			readFile(bufferStream.getInputStream(), existingValues);
		} else
			existingValues = Collections.emptyMap();
		final Map<String, Object> values = getValues(existingValues, deployTaskInfo.interaction, deployTaskInfo.host);
		// Use BufferStream for now
		bufferStream.clear();
		writeFile(bufferStream.getOutputStream(), values);
		UploadTask.writeFileUsingTmp(deployTaskInfo, bufferStream.getInputStream(), permissions, owner, group, null, getFilePath());
		deployTaskInfo.interaction.printf("Updated file '%1$s' on %2$s", getFilePath(), deployTaskInfo.host.getSimpleName());
		return "Updated " + CollectionUtils.getDiffCount(existingValues, values) + " settings on file '" + getFilePath() + "'";
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	protected abstract void readFile(InputStream in, Map<String, Object> values) throws IOException ;
	protected abstract void writeFile(OutputStream out, Map<String, Object> values) throws IOException ;
	
	public boolean isKeepExisting() {
		return keepExisting;
	}
	
	@Override
	public String getDescription(DeployTaskInfo deployTaskInfo) {
		return "Updating '" + getFilePath() + "' with " + valueMap.size() + " properties on " + deployTaskInfo.host.getSimpleName();
	}

}
