package com.usatech.tools.deploy;

import java.net.UnknownHostException;
import java.util.Set;

public interface Registry {

	public Set<App> getRegisteredApps();

	public void registerApp(App app);

	public void registerApp(App app, String version);

	public void registerSubApps(App app, App... subApps);

	public void unregisterSubApps(App app, App... subApps);

	public void clearSubApps(App app);

	public BasicServerSet getServerSet(String serverEnv);

	public void unregisterApp(App app);

	public void updateAppVersions();

	public void registerServerSet(String serverEnv, String serverType, char seventh, int numServers, int numInstances) throws UnknownHostException;

	public void registerServerSet(String serverEnv, String serverType, String hostName, String... instances) throws UnknownHostException;

}