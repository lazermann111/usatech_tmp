package com.usatech.tools.deploy;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import simple.bean.ConvertUtils;
import simple.io.Interaction;
import simple.text.StringUtils;

public class AppInstanceSplitterTask extends PropertiesUpdateFileTask {
	protected static final Pattern pollerConfigPattern = Pattern.compile("USAT\\.simplemq\\.directPollerUrl\\.\\w+_s(\\d)");
	// protected static final Pattern pollerConfigPattern = Pattern.compile("simple\\.mq\\.Supervisor\\.directPollerConfig\\(\\w+(\\d)\\)\\..*");
	protected static final Pattern queueLayerDataSourcePattern = Pattern.compile("USAT\\.simplemq\\.secondaryDataSources");
	protected final int instanceNum;
	protected final int instanceCount;

	public AppInstanceSplitterTask(App app, Integer ordinal, int instanceNum, int instanceCount) {
		super(true, null, null, null);
		this.instanceNum = instanceNum;
		this.instanceCount = instanceCount;
		setFilePath("/opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal.toString()) + "/specific/USAT_app_settings.properties");
	}

	@Override
	protected Map<String, Object> getValues(Map<String, Object> existingValues, Interaction interaction, Host host) {
		final Map<String, Object> values = new LinkedHashMap<String, Object>(valueMap.size());
		for(Map.Entry<String, Object> entry : existingValues.entrySet()) {
			Matcher matcher = pollerConfigPattern.matcher(entry.getKey());
			if(matcher.matches()) {
				final int instance = 1 + ConvertUtils.getIntSafely(matcher.group(1), -1);
				int maxIgnore = instanceCount - ((instanceNum - 1) % (instanceCount / 2));
				int minIgnore = 1 + maxIgnore - (instanceCount / 2);
				if(instance > 0 && instance >= minIgnore && instance <= maxIgnore) {
					// values.put(entry.getKey() + '=' + entry.getValue(), COMMENT_MARKER);
					values.put(entry.getKey(), "");
					continue;
				}
			} else if(queueLayerDataSourcePattern.matcher(entry.getKey()).matches()) {
				int start = 1 + ((instanceNum - 1) / (instanceCount / 2)) * (instanceCount / 2);
				int end = start + (instanceCount / 2) - 1;
				// values.put(entry.getKey() + '=' + entry.getValue(), COMMENT_MARKER);
				values.put(entry.getKey(), StringUtils.generateList("MQ_", null, ",", instanceNum + 1, (instanceCount / 2) - 1, start, end));
				continue;
			}
			values.put(entry.getKey(), entry.getValue());
		}
		return values;
	}
}
