/**
 * 
 */
package com.usatech.tools.deploy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.text.DateFormatSymbols;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.HeapBufferStream;
import simple.lang.SystemUtils;
import simple.text.StringUtils;
import simple.util.CollectionUtils;


public class CronUpdateTask implements DeployTask {
	protected static final Pattern cronPattern = Pattern.compile("(\\*(?:/\\d+)?|\\d+(?:-\\d+(?:/\\d+)?)?(?:,\\d+(?:-\\d+)?)*)\\s+(\\*(?:/\\d+)?|\\d+(?:-\\d+(?:/\\d+)?)?(?:,\\d+(?:-\\d+)?)*)\\s+(\\*(?:/\\d+)?|\\d+(?:-\\d+(?:/\\d+)?)?(?:,\\d+(?:-\\d+)?)*)\\s+(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec|\\*(?:/\\d+)?|\\d+(?:-\\d+(?:/\\d+)?)?(?:,\\d+(?:-\\d+)?)*)\\s+(sun|mon|tue|wed|thu|fri|sat|\\*(?:/\\d+)?|\\d+(?:-\\d+(?:/\\d+)?)?(?:,\\d+(?:-\\d+)?)*)\\s+(\\S.*)", Pattern.CASE_INSENSITIVE);
	protected static final String[] MONTHS;
	protected static final String[] DAYS_OF_WEEK;
	protected static final BitSet GET_CRON_COMMAND_SUCCESS = new BitSet();
	static {
		DateFormatSymbols dfs = DateFormatSymbols.getInstance();
		MONTHS = dfs.getShortMonths();
		for(int i = 0; i < MONTHS.length; i++)
			MONTHS[i] = MONTHS[i].toUpperCase();
		DAYS_OF_WEEK = dfs.getShortWeekdays();
		for(int i = 0; i < DAYS_OF_WEEK.length; i++)
			DAYS_OF_WEEK[i] = DAYS_OF_WEEK[i].toUpperCase();
		GET_CRON_COMMAND_SUCCESS.set(0);
		GET_CRON_COMMAND_SUCCESS.set(1);
	}
	protected static class Job {
		public int[] minutes;
		public int[] hours;
		public int[] daysOfMonth;
		public int[] months;
		public int[] daysOfWeek;
		public String command;
		public String comments;

		public Job() {

		}

		public Job(int[] minutes, int[] hours, int[] daysOfMonth, int[] months, int[] daysOfWeek, String command, String comments) {
			super();
			this.minutes = minutes;
			this.hours = hours;
			this.daysOfMonth = daysOfMonth;
			this.months = months;
			this.daysOfWeek = daysOfWeek;
			this.command = command;
			this.comments = comments;
		}

		public void writeJob(Writer writer) throws IOException {
			if(comments != null) {
				BufferedReader commentReader = new BufferedReader(new StringReader(comments));
				String commentLine;
				while((commentLine = commentReader.readLine()) != null) {
					if(commentLine.charAt(0) != '#')
						writer.write('#');
					writer.write(commentLine);
					writer.write('\n');
				}
			}
			writeCommandLine(writer);
		}

		protected void writeCommandLine(Writer writer) throws IOException {
			writeField(writer, minutes);
			writer.write(' ');
			writeField(writer, hours);
			writer.write(' ');
			writeField(writer, daysOfMonth);
			writer.write(' ');
			writeField(writer, months);
			writer.write(' ');
			writeField(writer, daysOfWeek);
			writer.write(' ');
			BufferedReader commandReader = new BufferedReader(new StringReader(command));
			String commandLine = commandReader.readLine();
			writer.write(commandLine);
			boolean contChar = commandLine.trim().endsWith("\\");
			while((commandLine = commandReader.readLine()) != null) {
				if(!contChar)
					writer.write('\\');
				writer.write('\n');
				writer.write(commandLine);
				contChar = commandLine.trim().endsWith("/");
			}
			writer.write('\n');
		}

		protected void writeField(Writer writer, int[] value) throws IOException {
			if(value == null || value.length == 0)
				writer.write('*');
			else {
				writer.write(String.valueOf(value[0]));
				if(value.length > 2 && value[value.length - 1] - value[0] == value.length - 1) {
					writer.write('-');
					writer.write(String.valueOf(value[value.length - 1]));
				} else {
					for(int i = 1; i < value.length; i++) {
						writer.write(',');
						writer.write(String.valueOf(value[i]));
					}
				}
			}
		}

		@Override
		public int hashCode() {
			return SystemUtils.addHashCodes(0, minutes, hours, daysOfMonth, months, daysOfWeek, command);
		}

		@Override
		public boolean equals(Object obj) {
			if(!(obj instanceof Job))
				return false;
			Job other = (Job) obj;
			return ConvertUtils.areEqual(command, other.command) && ConvertUtils.areEqual(minutes, other.minutes)
					&& ConvertUtils.areEqual(hours, other.hours) && ConvertUtils.areEqual(daysOfMonth, other.daysOfMonth)
					&& ConvertUtils.areEqual(months, other.months) && ConvertUtils.areEqual(daysOfWeek, other.daysOfWeek);
		}

		@Override
		public String toString() {
			StringWriter writer = new StringWriter();
			try {
				writeCommandLine(writer);
			} catch(IOException e) {
				writer.write("<EXCEPTION: ");
				writer.write(e.getMessage());
				writer.write(">");
			}
			writer.flush();
			return writer.toString();
		}
	}

	protected final Map<Pattern, Job> jobs = new LinkedHashMap<Pattern, Job>();
	protected final String user;
	protected boolean keepExisting;
	
	public CronUpdateTask(String user, boolean keepExisting) {
		this.user = user;
		this.keepExisting = keepExisting;
	}

	public String registerJob(Pattern match, int[] minutes, int[] hours, int[] daysOfMonth, int[] months, int[] daysOfWeek, String command, String comments) {
		Job old = jobs.put(match, new Job(minutes, hours, daysOfMonth, months, daysOfWeek, command, comments));
		return old != null ? old.toString() : null;
	}

	@Override
	public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
		String[] results = ExecuteTask.executeCommands(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.getSsh(), GET_CRON_COMMAND_SUCCESS, "sudo su", "crontab -l -u '" + user + "'");
		BufferedReader reader = new BufferedReader(new StringReader(results[1]));
		HeapBufferStream bufferStream = new HeapBufferStream();
		Writer writer = new OutputStreamWriter(bufferStream.getOutputStream());
		String line;
		StringBuilder output = new StringBuilder();
		boolean updated = false;
		Map<Pattern, Job> remainingJobs = new LinkedHashMap<Pattern, Job>(jobs);
		while((line = reader.readLine()) != null) {
			if(line.startsWith("#"))
				output.append(line).append('\n');
			else {
				Matcher matcher = cronPattern.matcher(line);
				if(matcher.matches()) {
					try {
						// 1 = minutes, 2 = hours, 3 = days of month, 4 = months, 5 = days of week, 6 = command
						Job job = new Job();
						job.minutes = parsePiece(matcher.group(1), 0, 59, null);
						job.hours = parsePiece(matcher.group(2), 0, 23, null);
						job.daysOfMonth = parsePiece(matcher.group(3), 1, 31, null);
						job.months = parsePiece(matcher.group(4), 1, 12, MONTHS);
						job.daysOfWeek = parsePiece(matcher.group(5), 0, 7, DAYS_OF_WEEK);
						job.command = matcher.group(6);
						Job newJob = findAndRemoveJob(remainingJobs, job);
						if(newJob != null) {
							if(!newJob.equals(job))
								updated = true;
							newJob.writeJob(writer);
						} else if(keepExisting) {
							writer.append(output);
							output.setLength(0);
							job.writeJob(writer);
						} else {
							updated = true;
						}
					} catch(IOException e) {
						throw new IOException("Exception while parsing rule '" + line + "'", e);
					} catch(ConvertException e) {
						throw new IOException("Exception while parsing rule '" + line + "'", e);
					}
				} else if(!line.startsWith("no crontab for ")) {
					writer.append(output);
					output.setLength(0);
					writer.write(line);
					writer.write('\n');
				}
			}
		}
		for(Job job : remainingJobs.values()) {
			updated = true;
			job.writeJob(writer);
		}
		if(updated) {
			deployTaskInfo.interaction.printf("Crontab changed for '%1$s'; updating...", user);
			writer.flush();
			String filePath = UploadTask.getTmpFilePath(deployTaskInfo.user);
			deployTaskInfo.sshClientCache.getSftpClient(deployTaskInfo.host, deployTaskInfo.user).put(bufferStream.getInputStream(), filePath, null);
			ExecuteTask.executeCommands(deployTaskInfo, "sudo su", "crontab -u '" + user + "' " + filePath);
			return "Updated crontab for " + user + " - " + jobs.size() + " jobs updated; " + remainingJobs.size() + " jobs untouched";
		}
		return "Crontab up-to-date with " + jobs.size() + " jobs";
	}

	protected static Job findAndRemoveJob(Map<Pattern, Job> registeredJobs, Job job) {
		for(Iterator<Map.Entry<Pattern, Job>> iter = registeredJobs.entrySet().iterator(); iter.hasNext();) {
			Map.Entry<Pattern, Job> registered = iter.next();
			Matcher matcher = registered.getKey().matcher(job.command);
			if(matcher.matches()) {
				iter.remove();
				return registered.getValue();
			}
		}
		return null;
	}

	protected static int[] parsePiece(String piece, int min, int max, String[] words) throws ConvertException {
		String[] parts = StringUtils.split(piece, ',');
		Set<Integer> items = new HashSet<Integer>();
		for(String part : parts) {
			if(part.equals("*")) {
				return null;
			}
			int factor;
			int pos = part.indexOf('/');
			if(pos >= 0) {
				factor = ConvertUtils.getInt(part.substring(pos + 1));
				part = part.substring(0, pos);
			} else
				factor = 1;
			pos = part.indexOf('-');
			if(pos >= 0) {
				int start;
				try {
					start = ConvertUtils.getInt(part.substring(0, pos));
				} catch(ConvertException e) {
					if(words == null)
						throw e;
					int n = CollectionUtils.iterativeSearch(words, part.substring(0, pos).toUpperCase());
					if(n < 0)
						throw e;
					start = n + min;
				}
				int end;
				try {
					end = ConvertUtils.getInt(part.substring(pos + 1));
				} catch(ConvertException e) {
					if(words == null)
						throw e;
					int n = CollectionUtils.iterativeSearch(words, part.substring(pos + 1).toUpperCase());
					if(n < 0)
						throw e;
					end = n + min;
				}
				for(int i = start; i <= end; i += factor)
					items.add(i);
			} else if(part.equals("*")) {
				for(int i = min; i <= max; i += factor)
					items.add(i);
			} else {
				items.add(ConvertUtils.getInt(part));
			}
		}
		if(items.size() == (max - min) + 1)
			return null;
		int[] array = new int[items.size()];
		int i = 0;
		for(Integer item : items) {
			array[i++] = item;
		}
		Arrays.sort(array);
		return array;
	}

	public int getJobCount() {
		return jobs.size();
	}

	public boolean isKeepExisting() {
		return keepExisting;
	}

	public void setKeepExisting(boolean keepExisting) {
		this.keepExisting = keepExisting;
	}

	@Override
	public String getDescription(DeployTaskInfo deployTaskInfo) {
		StringBuilder sb = new StringBuilder();
		sb.append("Registering ").append(jobs.size()).append(" cron jobs");
		return sb.toString();
	}
}