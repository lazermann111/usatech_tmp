package com.usatech.tools.deploy;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import simple.io.IOUtils;
import simple.text.StringUtils;

@Deprecated
public abstract class AbstractLinuxPatchChangeSet extends AbstractLinuxChangeSet {
	// protected final Set<File> localFiles = new LinkedHashSet<File>();
	protected final Set<Object> localFiles = new LinkedHashSet<Object>();
	protected final Set<String> appDirs = new LinkedHashSet<String>();
	protected final Set<File> appFiles = new LinkedHashSet<File>();
	protected final Set<String> libFiles = new LinkedHashSet<String>();
	protected final Set<File> srcFiles = new LinkedHashSet<File>();
	protected final Set<Host> uploaded = new HashSet<Host>();
	
	public AbstractLinuxPatchChangeSet() throws UnknownHostException {
		super();
	}

	@Override
	protected boolean shouldAddHostBasedTask(DeployTaskInfo deployTaskInfo) {
		return true;
	}

	@Override
	protected Set<App> filterApps(Set<App> apps, DeployTaskInfo deployTaskInfo, String serverType) {
		return apps;
	}

	protected void registerClass(File compiledDirectory, final Class<?> clazz) {
		final String simpleName = clazz.getSimpleName();
		final String packageDir = clazz.getPackage().getName().replace('.', '/');
		File[] files = new File(compiledDirectory, packageDir).listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.startsWith(simpleName) && name.endsWith(".class") && (name.length() == simpleName.length() + 6 || name.charAt(simpleName.length()) == '$');
			}
		});
		if(files == null || files.length == 0)
			throw new IllegalArgumentException("Class '" + clazz.getName() + "' not found in directory '" + compiledDirectory.getAbsolutePath() + "'");
		final String fullPackageDir = IOUtils.getFullPath("classes", packageDir);
		for(File f : files) {
			localFiles.add(f);
			appDirs.add(fullPackageDir);
			appFiles.add(new File(fullPackageDir, f.getName()));
		}
	}
	protected void registerResource(File sourceDirectory, final String resourcePath) {
		registerResource(sourceDirectory, resourcePath, null);
	}

	protected void registerResource(File sourceDirectory, final String resourcePath, String appSubDirPath) {
		File file = new File(sourceDirectory, resourcePath);
		if(!file.canRead())
			throw new IllegalArgumentException("Resource '" + resourcePath + "' not found in directory '" + sourceDirectory.getAbsolutePath() + "'");
		localFiles.add(file);
		String dir = IOUtils.getFullPath(appSubDirPath, StringUtils.substringBeforeLast(resourcePath, "\\/".toCharArray()));
		if(dir != null && (dir=dir.trim()).length() > 0)
			appDirs.add(dir);
		appFiles.add(new File(dir, file.getName()));
	}
	/**
	 * 
	 * @param cvsFilePath eg. usalive/web/css/usalive-app-style.css
	 * @param appSubDirPath eg. target machine's app subpath such as 'web/css' so the file will go to usalive/web/css
	 * @param cvsRevision
	 */
	protected void registerResource(String cvsFilePath, String appSubDirPath, String cvsRevision) {
		CvsPullTask cvsPullTask = new CvsPullTask(cvsFilePath, cvsRevision);
		localFiles.add(cvsPullTask);
		String fileName=IOUtils.getFileName(cvsFilePath);
		if(appSubDirPath != null && (appSubDirPath = appSubDirPath.trim()).length() > 0) {
			if(!appSubDirPath.endsWith("/"))
				appSubDirPath = appSubDirPath + '/';
			appDirs.add(appSubDirPath);
		}
		appFiles.add(new File(appSubDirPath, fileName));
	}

	protected void registerLib(File libFile) {
		if(!libFile.canRead())
			throw new IllegalArgumentException("Library '" + libFile + "' not found");
		localFiles.add(libFile);
		libFiles.add(libFile.getName());
	}

	protected void registerLib(String cvsLibPath, String cvsRevision) {
		CvsPullTask cvsPullTask = new CvsPullTask(cvsLibPath, cvsRevision);
		localFiles.add(cvsPullTask);
		libFiles.add(IOUtils.getFileName(cvsLibPath));
	}

	protected void registerSource(File sourceDirectory, final Class<?> clazz) {
		final String simpleName = clazz.getSimpleName();
		final String packageDir = clazz.getPackage().getName().replace('.', '/');
		File file = new File(sourceDirectory, packageDir + "/" + simpleName + ".java");
		if(!file.exists())
			throw new IllegalArgumentException("Source for '" + clazz.getName() + "' not found in directory '" + sourceDirectory.getAbsolutePath() + "'");
		localFiles.add(file);
		final String fullPackageDir = IOUtils.getFullPath("classes", packageDir);
		appDirs.add(fullPackageDir);
		srcFiles.add(new File(fullPackageDir, file.getName()));
	}

	protected void registerSource(String cvsSourceDirectory, final Class<?> clazz, String cvsRevision) {
		final String simpleName = clazz.getSimpleName();
		final String packageDir = clazz.getPackage().getName().replace('.', '/');
		CvsPullTask cvsPullTask = new CvsPullTask(IOUtils.getFullPath(cvsSourceDirectory, packageDir + "/" + simpleName + ".java"), cvsRevision);
		localFiles.add(cvsPullTask);
		final String fullPackageDir = IOUtils.getFullPath("classes", packageDir);
		appDirs.add(fullPackageDir);
		srcFiles.add(new File(fullPackageDir, simpleName + ".java"));
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		uploaded.remove(host);
		// Do nothing		
	}
	@Override
	protected void addTasks(Host host, App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(!uploaded.contains(host)) {
			for(Object f : localFiles) {
				if(f instanceof File)
					tasks.add(new UploadTask((File) f, null, 0644, null, null));
				else if(f instanceof CvsPullTask) {
					tasks.add(new UploadTask((CvsPullTask) f, 0644, null, null));
				}
			}
			uploaded.add(host);
		}
		if(app.isStandardJavaApp()) {
			String appDir = "/opt/USAT/" + app.getName() + (instance == null ? "" : instance);
			if(commands.isEmpty())
				commands.add("sudo su");
			for(String dir : appDirs)
				commands.add("mkdir -p " + IOUtils.getFullPath(appDir, dir));
			for(File f : appFiles) {
				commands.add("/bin/cp -p /home/`logname`/" + f.getName() + " " + IOUtils.getFullPath(appDir, f.getParent().replace(File.separatorChar, '/')));
			}
			String libDir = appDir + "/lib/";
			for(String f : libFiles) {
				commands.add("/bin/cp -p /home/`logname`/" + f + " " + libDir + f);
				commands.add("chown " + app.getUserName() + ":" + app.getUserName() + " " + libDir + f);
				commands.add("chmod 644 " + libDir + f);
				String scriptFile = appDir + "/bin/" + app.getServiceName() + ".sh";
				commands.add("if ! grep -c 'CLASSPATH=\".*:lib/" + f + "' " + scriptFile + "; then /bin/cp " + scriptFile + " " + scriptFile + ".ORIG && sed 's#\\(CLASSPATH=\"[^\"]*\\)\"#\\1:lib/" + f + "\"#' " + scriptFile + ".ORIG > " + scriptFile + "; fi");
			}
			for(String dir : appDirs) {
				String fullDir = IOUtils.getFullPath(appDir, dir.substring(0, dir.indexOf('/')));
				commands.add("chown -R " + app.getUserName() + ":" + app.getUserName() + " " + fullDir);
				commands.add("chmod -R 755 " + fullDir);
			}
			if(!srcFiles.isEmpty()) {
				commands.add("cd " + appDir);
				StringBuilder sb = new StringBuilder();
				sb.append("/usr/jdk/latest/bin/javac -g -cp $(grep -m 1 \"CLASSPATH=\" ").append(appDir);
				sb.append("/bin/").append(app.getServiceName()).append(".sh | cut -d= -f2- | sed 's/^\"\\(.*\\)\"$/\\1/') -d ").append(appDir).append("/classes");
				for(File f : srcFiles) {
					sb.append(" /home/`logname`/").append(f.getName());
				}
				commands.add(sb.toString());
				commands.add("chown -R " + app.getUserName() + ':' + app.getUserName() + ' ' + appDir + "/classes/*");
			}
		}
		Set<App> subApps = registry.getServerSet(host.getServerEnv()).getSubApps(app);
		if(subApps != null && !subApps.isEmpty()) {
			for(App subApp : subApps) {
				String appDir = "/opt/USAT/" + app.getName() + (instance == null ? "" : instance) + "/" + subApp.getName();
				if(commands.isEmpty())
					commands.add("sudo su");
				for(String dir : appDirs) {
					if((dir = getDir(dir, app)) != null)
						commands.add("mkdir -p " + IOUtils.getFullPath(appDir, dir));
				}
				for(File f : appFiles) {
					String dir = getDir(f.getParent().replace(File.separatorChar, '/'), app);
					if(dir != null)
						commands.add("/bin/cp -p /home/`logname`/" + f.getName() + " " + IOUtils.getFullPath(appDir, dir));
				}
				for(String dir : appDirs) {
					if((dir = getDir(dir, app)) != null) {
						String fullDir = IOUtils.getFullPath(appDir, dir.substring(0, dir.indexOf('/')));
						commands.add("chown -R " + app.getUserName() + ":" + app.getUserName() + " " + fullDir);
						commands.add("chmod -R 755 " + fullDir);
					}
				}
			}
		}
		addRestartAppCommands(host, app, instance, instanceNum, instanceCount, tasks, commands, cache);
	}

	protected String getDir(String dir, App app) {
		if(app.getName().equals("httpd")) {
			if(dir.startsWith("web/"))
				return dir.substring(4);
			return null;
		}
		return dir;
	}
	/**
	 * @throws IOException
	 */
	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(enableAppIfDisabled(host, app, instance == null ? null : Integer.parseInt(instance)))
			commands.add("/usr/monit-latest/bin/monit restart " + app.getName() + (instance == null ? "" : instance));
		else
			commands.add("/opt/USAT/" + app.getName() + (instance == null ? "" : instance) + "/bin/" + app.getServiceName() + ".sh stop 10000");
	}
}
