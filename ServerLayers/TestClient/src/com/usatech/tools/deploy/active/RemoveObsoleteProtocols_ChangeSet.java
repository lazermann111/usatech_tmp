package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.FromCvsUploadTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.OptionalTask;
import com.usatech.tools.deploy.PasswordCache;

public class RemoveObsoleteProtocols_ChangeSet implements ChangeSet {

	public RemoveObsoleteProtocols_ChangeSet() {

	}

	@Override
	public String getName() {
		return "Remove Obsolete Protocols";
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("NET") || host.isServerType("WEB") || host.isServerType("KLS");
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		List<DeployTask> tasks = new ArrayList<>();
		List<String> cmds = new ArrayList<>();
		cmds.add("sudo su");
		if(host.isServerType("NET"))
			cmds.add("for file in /opt/USAT/netlayer*/specific/USAT_app_settings.properties; do if [ `grep -c 'PublicSslContextFactory.includeProtocols' $file` -eq 0 ]; then echo 'PublicSslContextFactory.includeProtocols=TLSv1.1,TLSv1.2' >> $file; monit restart $(basename $(dirname $(dirname $file))); fi; done");
		if(host.isServerType("WEB")) {
			// install httpd2.2.29
			String httpdVersion = "2.2.29";
			tasks.add(new OptionalTask("if [ -x /usr/local/apache2/bin/apachectl ]; then /usr/local/apache2/bin/apachectl -v fi; fi", Pattern.compile("Server version: Apache/" + httpdVersion.replaceAll("\\.", "\\\\.") + " .*", Pattern.DOTALL), true,
					new FromCvsUploadTask("server_app_config/WEB/Httpd/linux/httpd-" + httpdVersion + "-usat-build.tar.gz", "HEAD", "httpd-" + httpdVersion + "-usat-build.tar.gz", 0644),
					new ExecuteTask("sudo su",
							"mv httpd-" + httpdVersion + "-usat-build.tar.gz /usr/local",
							"cd /usr/local",
							"tar xvfz httpd-" + httpdVersion + "-usat-build.tar.gz",
							"/bin/rm httpd-" + httpdVersion + "-usat-build.tar.gz")));
			// remove old conf files for unused websites
			cmds.add("if [ -r /opt/USAT/httpd/conf.d/energymisers.conf ]; then for file in /opt/USAT/httpd/conf.d/energymisers.conf*; do /bin/rm $file; done; fi");
			cmds.add("if [ -r /opt/USAT/httpd/conf.d/eportgo.conf ]; then for file in /opt/USAT/httpd/conf.d/eportgo.conf*; do /bin/rm $file; done; fi");
			cmds.add("if [ -r /opt/USAT/httpd/conf.d/eportmobile.conf ]; then for file in /opt/USAT/httpd/conf.d/eportmobile.conf*; do /bin/rm $file; done; fi");
			cmds.add("if [ -r /opt/USAT/httpd/conf.d/php.conf ]; then for file in /opt/USAT/httpd/conf.d/php.conf*; do /bin/rm $file; done; fi");
			cmds.add("if [ -r /opt/USAT/httpd/conf.d/usatech.conf ]; then for file in /opt/USAT/httpd/conf.d/usatech.conf*; do /bin/rm $file; done; fi");
		}
		if(host.isServerType("NET") || host.isServerType("WEB")) {
			cmds.add("sed -i 's/SSLProtocol All -SSLv2/SSLProtocol +TLSv1.1 +TLSv1.2/g' /opt/USAT/httpd/conf.d/*.conf");
			cmds.add("/usr/local/apache2/bin/httpd -f /opt/USAT/httpd/conf/httpd.conf -t");
			cmds.add("monit restart httpd");
		}
		if(host.isServerType("KLS"))
			cmds.add("for file in /opt/USAT/keymgr*/specific/USAT_app_settings.properties; do if [ `grep -c 'simple.app.Service.SslJettyServer.includeProtocols' $file` -eq 0 ]; then echo 'simple.app.Service.SslJettyServer.includeProtocols=TLSv1.1,TLSv1.2' >> $file; monit restart $(basename $(dirname $(dirname $file))); fi; done");

		tasks.add(new ExecuteTask(cmds.toArray(new String[cmds.size()])) );
		return tasks.toArray(new DeployTask[tasks.size()]);
	}

	@Override
	public String toString() {
		return getName();
	}
}
