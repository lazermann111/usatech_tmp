package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class MORE_SpringOfferPatch_ChangeSet extends MultiLinuxPatchChangeSet {
	public MORE_SpringOfferPatch_ChangeSet() throws UnknownHostException {
		super();
		registerResource("usalive/web/public/more_spring_special_order.html.jsp", "web/public", "REL_USALive_1_21_2", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/xsl/other-data-layer.xml", "classes", "REL_USALive_1_21_2", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/css/more_normalize.css", "web/css", "REL_USALive_1_21_2", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/css/more_main.css", "web/css", "REL_USALive_1_21_2", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/scripts/modernizr-2.8.3.min.js", "web/scripts", "REL_USALive_1_21_2", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/images/more-logo.png", "web/images", "REL_USALive_1_21_2", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/images/thankyou.png", "web/images", "REL_USALive_1_21_2", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/images/free2.png", "web/images", "REL_USALive_1_21_2", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/USAT-Rental-Terms-Conditions.pdf", "web", "REL_USALive_1_21_2", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/ePort_Micro_Markets.pdf", "web", "REL_USALive_1_21_2", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/USAT-Lease-Terms-Conditions.pdf", "web", "REL_USALive_1_21_2", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/ePort-Rental-and-Lease-Programs.pdf", "web", "REL_USALive_1_21_2", false, USATRegistry.USALIVE_APP);
		

		registerResource("usalive/web/css/more_normalize.css", "web/css", "REL_USALive_1_21_2", false, USATRegistry.HTTPD_WEB);
		registerResource("usalive/web/css/more_main.css", "web/css", "REL_USALive_1_21_2", false, USATRegistry.HTTPD_WEB);
		registerResource("usalive/web/scripts/modernizr-2.8.3.min.js", "web/scripts", "REL_USALive_1_21_2", false, USATRegistry.HTTPD_WEB);
		registerResource("usalive/web/images/more-logo.png", "web/images", "REL_USALive_1_21_2", false, USATRegistry.HTTPD_WEB);
		registerResource("usalive/web/images/thankyou.png", "web/images", "REL_USALive_1_21_2", false, USATRegistry.HTTPD_WEB);
		registerResource("usalive/web/images/free2.png", "web/images", "REL_USALive_1_21_2", false, USATRegistry.HTTPD_WEB);
		registerResource("usalive/web/USAT-Rental-Terms-Conditions.pdf", "web", "REL_USALive_1_21_2", false, USATRegistry.HTTPD_WEB);
		registerResource("usalive/web/ePort_Micro_Markets.pdf", "web", "REL_USALive_1_21_2", false, USATRegistry.HTTPD_WEB);
		registerResource("usalive/web/USAT-Lease-Terms-Conditions.pdf", "web", "REL_USALive_1_21_2", false, USATRegistry.HTTPD_WEB);
		registerResource("usalive/web/ePort-Rental-and-Lease-Programs.pdf", "web", "REL_USALive_1_21_2", false, USATRegistry.HTTPD_WEB);
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.USALIVE_APP);
	}

	@Override
	public String getName() {
		return "More spring offer patch";
	}
}
