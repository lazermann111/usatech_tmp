package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.dms.transaction.PendingReversalListStep;
import com.usatech.dms.user.DmsMenuManager;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R57_Update_40_ChangeSet extends MultiLinuxPatchChangeSet {
	public R57_Update_40_ChangeSet() throws UnknownHostException {
		super();
		
		registerSource("DMS/src", PendingReversalListStep.class, "REL_R57_40", USATRegistry.DMS_APP);
		registerSource("DMS/src", DmsMenuManager.class, "REL_R57_40", USATRegistry.DMS_APP);		
		registerResource("DMS/resources/actions/general-actions.xml", "classes", "REL_R57_40", false, USATRegistry.DMS_APP);		
		registerResource("DMS/resources/datalayers/transaction-data-layer.xml", "classes", "REL_R57_40", false, USATRegistry.DMS_APP);
		registerResource("DMS/resources/jsp/include/leftMenu.jsp", "web/jsp/include", "REL_R57_40", false, USATRegistry.DMS_APP);
		registerResource("DMS/resources/jsp/transactions/pendingReversalList.jsp", "web/jsp/transactions", "REL_R57_40", false, USATRegistry.DMS_APP);
		registerResource("DMS/webroot/js/cssverticalmenu.js", "web/js", "REL_R57_40", false, USATRegistry.DMS_APP);
		registerResource("DMS/webroot/js/cssverticalmenu.js", "dms/js", "REL_R57_40", false, USATRegistry.HTTPD_NET);
	}
	
	@Override
	public String getName() {
		return "R57 Update 40 - Auto-reversal limit and approval";
	}
}
