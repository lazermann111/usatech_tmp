package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATHelper;

public class JDKDownloadCVS_ChangeSet implements ChangeSet {
	protected final int majorVersion = 8;
	protected final int minorVersion = 181;
	protected final int securityVersion = 0;

	@Override
	public String getName() {
		return "JDK Download from CVS for jdk " + majorVersion + "." + minorVersion + "." + securityVersion
				+ " Install";
	}

	@Override
	public boolean isApplicable(Host host) {
		return true;
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		List<DeployTask> tasks = new ArrayList<DeployTask>();
		USATHelper.downloadJavaFromCVS(host, cache, tasks, majorVersion, minorVersion, securityVersion, "/home/`logname`");
		return tasks.toArray(new DeployTask[tasks.size()]);
	}

	@Override
	public String toString() {
		return getName();
	}
}
