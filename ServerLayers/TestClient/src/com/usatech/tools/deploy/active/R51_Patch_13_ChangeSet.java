package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.authoritylayer.AprivaAuthorityTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R51_Patch_13_ChangeSet extends MultiLinuxPatchChangeSet {
	public R51_Patch_13_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/AuthorityLayer/src", AprivaAuthorityTask.class, "REL_authoritylayer_1_37_13", USATRegistry.OUTAUTH_LAYER);
	}
	
	@Override
	public String getName() {
		return "R51 Patch 13";
	}

}
