package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.OptionalTask;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.PgDeployMap;
import com.usatech.tools.deploy.PgDeployMap.PGDATADir;
import com.usatech.tools.deploy.Server;
import com.usatech.tools.deploy.USATHelper;
import com.usatech.tools.deploy.USATRegistry;

public class RemoveObsoleteProtocolsPostgres_ChangeSet implements ChangeSet {

	public RemoveObsoleteProtocolsPostgres_ChangeSet() {

	}

	@Override
	public String getName() {
		return "Remove Obsolete Protocols from Postgres";
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("MSR") || host.isServerType("MST") || host.isServerType("KLS") || host.isServerType("KLS");
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		List<DeployTask> tasks = new ArrayList<>();
		USATRegistry registry = new USATRegistry();
		String pgRestartCmd;
		if(host.isServerType("PGS")) {// freeze it first so cluster will not auto execute commands
			tasks.add(new ExecuteTask("sudo su", "if [ -x /usr/sbin/clusvcadm ];  then clusvcadm -Z primary; fi", "if [ -x /usr/sbin/clusvcadm ];  then clusvcadm -Z stdby; fi"));
			StringBuilder sb = new StringBuilder();
			sb.append("if [ `clustat | sed -n 's/^ service:\\([A-Za-z0-9._][A-Za-z0-9._]*\\)  *").append(host.getSimpleName()).append("-priv .*$/\1/gp' | grep -c 'primary'` -gt 0 ]; then su postgres -c '/opt/USAT/postgres/bin/postgresql.sh stop; /opt/USAT/postgres/bin/postgresql.sh start &'; fi; ");
			sb.append("if [ `clustat | sed -n 's/^ service:\\([A-Za-z0-9._][A-Za-z0-9._]*\\)  *").append(host.getSimpleName()).append("-priv .*$/\1/gp' | grep -c 'stdby'` -gt 0 ]; then su postgres -c '/opt/USAT/postgres_stdby/bin/postgresql.sh stop; /opt/USAT/postgres_stdby/bin/postgresql.sh start &'; fi");
			pgRestartCmd = sb.toString();
		} else if(host.isServerType("KLS")) {
			PGDATADir pgdd = PgDeployMap.hostPGDATADirMap.get(host.getSimpleName());
			StringBuilder sb = new StringBuilder();
			sb.append("/usr/monit-latest/bin/monit restart ").append(pgdd.getPrimaryDir(), pgdd.getPrimaryDir().lastIndexOf("/") + 1, pgdd.getPrimaryDir().length()).append("; /usr/monit-latest/bin/monit restart ").append(pgdd.getStandbyDir(), pgdd.getStandbyDir().lastIndexOf("/") + 1, pgdd.getStandbyDir().length());
			pgRestartCmd = sb.toString();
		} else {
			Server server = registry.getServerSet(host.getServerEnv()).getServer(host, host.getServerTypes().iterator().next());
			if(server.instances == null || server.instances.length <= 1)
				pgRestartCmd = "/usr/monit-latest/bin/monit restart postgres";
			else {
				StringBuilder sb = new StringBuilder();
				for(int i = 0; i < server.instances.length; i++) {
					if(sb.length() > 0)
						sb.append("; ");
					sb.append("/usr/monit-latest/bin/monit restart postgres").append(server.instances[i]);
				}
				pgRestartCmd = sb.toString();
			}
		}
		USATHelper.addPostgresConfTasks(tasks, host, registry);
		tasks.add(new OptionalTask("sudo /usr/pgsql-latest/bin/pg_config --version | cut -d ' ' -f 2", Pattern.compile(".*^(9.3.8|9.2.12)$.*", Pattern.DOTALL | Pattern.MULTILINE), 
				new DeployTask[] { new ExecuteTask("sudo su", pgRestartCmd) }, 
				new DeployTask[] { new ExecuteTask(
				"sudo su", 
				"pgVersion=`/usr/pgsql-latest/bin/pg_config --version | cut -d ' ' -f 2`",
				"pgVersionMajor=`cut -d '.' -f1 <<< $pgVersion`",
				"pgVersionMinor=`cut -d '.' -f2 <<< $pgVersion`",
				"if [ $pgVersionMinor -eq 2 ]; then pgVersionRel=7; else pgVersionRel=1; fi",
				"rpm -Uvh --force http://yum.postgresql.org/$pgVersionMajor.$pgVersionMinor/redhat/rhel-6-x86_64/pgdg-redhat$pgVersionMajor$pgVersionMinor-$pgVersionMajor.$pgVersionMinor-$pgVersionRel.noarch.rpm; echo $? | egrep '0|1'",
				"yum -y --enablerepo=pgdg$pgVersionMajor$pgVersionMinor upgrade postgresql$pgVersionMajor$pgVersionMinor",
				pgRestartCmd,
				"try=1; while [[ $try -lt 60 ]] && [[ `su - postgres -c '/usr/pgsql-latest/bin/pg_ctl status -D /opt/USAT/postgres/data' > /dev/null; echo $?` -eq 3 ]]; do sleep 1; ((try++)); done") }));
		return tasks.toArray(new DeployTask[tasks.size()]);
	}

	@Override
	public String toString() {
		return getName();
	}
}
