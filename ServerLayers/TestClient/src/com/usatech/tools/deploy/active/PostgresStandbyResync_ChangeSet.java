package com.usatech.tools.deploy.active;

import java.io.IOException;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.PgDeployMap;
import com.usatech.tools.deploy.ReinitStandbyTask;

/**
 * This changeset is a standalone task that can be applied to the primary to take a base backup and restore the standby to the latest.
 * 
 * This can be used when the standby is somehow too stale and we want to restore it to the latest of the primary. 
 * 
 * This changeset assumes that the primary is running and both primary and standby is already setup initially on the hosts.
 * @author yhe
 *
 */
public class PostgresStandbyResync_ChangeSet implements ChangeSet {

	@Override
	public String getName() {
		return "Postgres Standby Resync Task";
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("KLS");
	}
	
	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		PgDeployMap.PGDATADir pgDataDir = PgDeployMap.hostPGDATADirMap.get(host.getSimpleName());
		Host primaryHost = PgDeployMap.primaryMap.get(host.getSimpleName());
		String appDir = pgDataDir.getStandbyDir();
		String postgresAppName = appDir.substring(appDir.lastIndexOf("/") + 1);
		return new DeployTask[] { new ReinitStandbyTask(postgresAppName, primaryHost, host) };
		/*
		
		//((HostImpl)host).setStandby(true);
		if(host.isStandby()) {
			// String standbyPath=PgDeployMap.hostPGDATADirMap.get(host.getSimpleName()).getStandbyDir();
			PgDeployMap.PGDATADir pgDataDir=PgDeployMap.hostPGDATADirMap.get(host.getSimpleName());
			Host primaryHost = PgDeployMap.primaryMap.get(host.getSimpleName());
			String appDir = pgDataDir.getStandbyDir();
			String postgresAppName = appDir.substring(appDir.lastIndexOf("/") + 1);
			return new DeployTask[] { new ReinitStandbyTask(postgresAppName, primaryHost, host) };
			String backupPathSuffix=String.valueOf(System.currentTimeMillis());
			BufferStream recoveryBufferStream = new HeapBufferStream();
			PrintWriter recoveryWriter = new PrintWriter(recoveryBufferStream.getOutputStream());
			recoveryWriter.println("standby_mode = 'on'");
			recoveryWriter.println("primary_conninfo = 'host="+primaryHost+" port=5432 user=replica sslmode=require sslcert="+standbyPath+"/data/client.crt sslkey="+standbyPath+"/data/client.key sslrootcert="+standbyPath+"/data/root.crt'");
			recoveryWriter.flush();
			return new DeployTask[] {
					new ExecuteTask("sudo su",
							"/usr/monit-latest/bin/monit stop "+postgresAppName,
							"/usr/monit-latest/bin/monit unmonitor "+postgresAppName,
							"rm -rf "+standbyPath+"/data",
							"rm -rf "+standbyPath+"/tblspace",
							"mv /home/`logname`/data "+standbyPath+"/data",
							"mv /home/`logname`/tblspace "+standbyPath+"/tblspace"
							),
					new UnixUploadTask(new File(baseDir, "server_app_config/KLS/postgresql_standby.conf"), standbyPath+"/data", "postgresql.conf", 0644, "postgres", "postgres", true),
					new UnixUploadTask(recoveryBufferStream.getInputStream(), standbyPath+"/data","recovery.conf", 0644, "postgres", "postgres", true),
					new RenewFileCertTask(new Date(System.currentTimeMillis() + 30 * 24 * 60 * 60 * 1000L), standbyPath+"/data/client.crt",standbyPath+"/data/client.key", 0644, 0600, "postgres", "postgres", null, "replica", "USATAuthenticatedSession", "replica"),
					new ExecuteTask("sudo su",
							"chown -R postgres:postgres "+standbyPath,
							"chmod -R 700 "+standbyPath+"/data",
							"/usr/monit-latest/bin/monit start "+postgresAppName)
			};
			
			}else{
			String primaryPath=PgDeployMap.hostPGDATADirMap.get(host.getSimpleName()).getPrimaryDir();
			return new DeployTask[] {
					new ExecuteTask(
							"sudo su - postgres",
							"/usr/pgsql-latest/bin/psql postgres -U postgres -p 5432 -c \"SELECT pg_start_backup('replica', true)\"",
							"exit",
							"sudo su",
							"rsync -av --exclude postgresql.conf --exclude postmaster.pid " + primaryPath + "/data "+primaryPath + "/tblspace `logname`@"+PgDeployMap.standbyMap.get(host.getSimpleName()).getSimpleName()+":/home/`logname`",
							"sudo su - postgres",
							"/usr/pgsql-latest/bin/psql postgres -U postgres -p 5432 -c \"SELECT pg_stop_backup()\""
					)
			};
		}
		return null;
		*/
	}

	@Override
	public String toString() {
		return getName();
	}
}
