package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class ElavonMarketSegmentation_ChangeSet extends MultiLinuxPatchChangeSet {
	public ElavonMarketSegmentation_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/AuthorityLayer/src/InsideAuthorityLayerService.properties", "classes", "REL_authoritylayer_1_29_1", true, USATRegistry.INAUTH_LAYER);
		registerResource("ServerLayers/AuthorityLayer/src/OutsideAuthorityLayerService.properties", "classes", "REL_authoritylayer_1_29_1", true, USATRegistry.OUTAUTH_LAYER);
	}

	@Override
	public String getName() {
		return "Elavon Market Segmentation Configuration Update";
	}
}
