package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;
import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.AppSettingUpdateFileTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.tools.deploy.UsatSettingUpdateFileTask;

public class LogLayer_Linux_ChangeSet extends AbstractLinuxChangeSet {
	public LogLayer_Linux_ChangeSet() throws UnknownHostException {
		super();
	}

	@Override
	protected void registerApps() {

		registerApp(USATRegistry.LOGS_AGENT_APR_APP, "1.0.2");
		registerApp(USATRegistry.LOGS_LAYER_DEV, "1.0.2");
		registerApp(USATRegistry.LOGS_LAYER_INT, "1.0.2");
		registerApp(USATRegistry.LOGS_LAYER_ECC, "1.0.2");
		registerApp(USATRegistry.LOGS_AGENT_NET_APP, "1.0.2");
		registerApp(USATRegistry.LOGS_AGENT_KLS_APP, "1.0.2");
	}

	@Override
	public boolean isApplicable(Host host) {
		return (!host.getServerEnv().equalsIgnoreCase("USA"));
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache)
			throws IOException {

		if (!host.isServerType("LOGS")) {
			return;
		}

		addPrepareHostTasks(host, tasks, cache);
		tasks.add(new UsatSettingUpdateFileTask(registry.getServerSet(host.getServerEnv())));
	}

	@Override
	public String getName() {
		return "Logs Layer Install - Linux";
	}

	@Override
	protected void addTasks(Host host, App app, Integer ordinal, String instance, int instanceNum, int instanceCount,
			List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {

		if ((app.getAppDesc().contains("DEV"))) ordinal = 1;
		if ((app.getAppDesc().contains("INT"))) ordinal = 2;
		if ((app.getAppDesc().contains("ECC"))) ordinal = 3;
		
		addPrepareAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache);

		if (app.getInstallFileName() != null) {
			tasks.add(new ExecuteTask("sudo touch /opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal) + "/specific/USAT_app_settings.properties"));
			tasks.add(new AppSettingUpdateFileTask(app, ordinal, instanceNum, instanceCount, cache,
					registry.getServerSet(host.getServerEnv()), false));
		}
		addInstallAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache, commands);
	}

}
