package com.usatech.tools.deploy.active;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;
import java.net.UnknownHostException;

public class R53_Patch_3_ChangeSet extends MultiLinuxPatchChangeSet {
	public R53_Patch_3_ChangeSet() throws UnknownHostException {
		super();
		registerResource("usalive/xsl/device-data-layer.xml", "classes/", "REL_USALive_1_29_3", false, USATRegistry.USALIVE_APP);
		registerResource("ReportGenerator/src/rgr-data-layer.xml", "classes/", "REL_report_generator_3_27_3", false, USATRegistry.RPTGEN_LAYER);
	}

	@Override
	public String getName() {
		return "R53 Patch 3";
	}
}
