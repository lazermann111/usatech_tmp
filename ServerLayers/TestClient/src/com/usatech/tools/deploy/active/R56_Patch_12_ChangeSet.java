package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R56_Patch_12_ChangeSet extends MultiLinuxPatchChangeSet {
	public R56_Patch_12_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/AuthorityLayer/src/authority-data-layer.xml", "classes", "REL_R56_12", false, USATRegistry.INAUTH_LAYER);
	}
	
	@Override
	public String getName() {
		return "R56 Patch 12 - Internal Card Authorization Performance";
	}
}
