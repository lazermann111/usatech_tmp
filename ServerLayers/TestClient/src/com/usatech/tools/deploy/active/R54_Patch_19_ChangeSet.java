package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.posm.process.MassEftAdjustment;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R54_Patch_19_ChangeSet extends MultiLinuxPatchChangeSet {
	public R54_Patch_19_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/LayersCommon/src/common-data-layer.xml", "classes", "REL_posmlayer_1_31_19", false, USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/POSMLayer/src/main", MassEftAdjustment.class, "REL_posmlayer_1_31_19", USATRegistry.POSM_LAYER);
	}
	
	@Override
	public String getName() {
		return "R54 Patch 19 - Mass EFT Adjustments";
	}
}
