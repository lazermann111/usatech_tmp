package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.authoritylayer.AramarkGatewayTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R51_Patch_1_ChangeSet extends MultiLinuxPatchChangeSet {
	public R51_Patch_1_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/AuthorityLayer/src/", AramarkGatewayTask.class, "REL_authoritylayer_1_37_1", USATRegistry.OUTAUTH_LAYER);
	}
	
	@Override
	public String getName() {
		return "R51 Patch 1";
	}
}
