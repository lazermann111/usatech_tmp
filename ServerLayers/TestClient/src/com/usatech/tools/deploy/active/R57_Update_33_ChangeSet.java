package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.dms.file.DFRFileFuncStep;
import com.usatech.dms.transaction.DFRRejectionListStep;
import com.usatech.posm.schedule.dfr.DFRSetup;
import com.usatech.posm.tasks.POSMSaleUpdateTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R57_Update_33_ChangeSet extends MultiLinuxPatchChangeSet {
	public R57_Update_33_ChangeSet() throws UnknownHostException {
		super();
		
		registerSource("DMS/src", DFRFileFuncStep.class, "REL_R57_33", USATRegistry.DMS_APP);
		registerSource("DMS/src", DFRRejectionListStep.class, "REL_R57_33", USATRegistry.DMS_APP);
		registerResource("DMS/resources/actions/general-actions.xml", "classes", "REL_R57_33", false, USATRegistry.DMS_APP);
		registerResource("DMS/resources/datalayers/device-data-layer.xml", "classes", "REL_R57_33", false, USATRegistry.DMS_APP);
		registerResource("DMS/resources/datalayers/general-data-layer.xml", "classes", "REL_R57_33", false, USATRegistry.DMS_APP);
		registerResource("DMS/resources/jsp/devices/paymentConfig/auth.jsp", "web/jsp/devices/paymentConfig", "REL_R57_33", false, USATRegistry.DMS_APP);
		registerResource("DMS/resources/jsp/devices/paymentConfig/refund.jsp", "web/jsp/devices/paymentConfig", "REL_R57_33", false, USATRegistry.DMS_APP);
		registerResource("DMS/resources/jsp/file/DFRFile.jsp", "web/jsp/file", "REL_R57_33", false, USATRegistry.DMS_APP);
		registerResource("DMS/resources/jsp/transactions/dfrRejectionList.jsp", "web/jsp/transactions", "REL_R57_33", false, USATRegistry.DMS_APP);
		registerResource("DMS/resources/jsp/include/leftMenu.jsp", "web/jsp/include", "REL_R57_33", false, USATRegistry.DMS_APP);
		registerResource("DMS/webroot/js/cssverticalmenu.js", "web/js", "REL_R57_33", false, USATRegistry.DMS_APP);
		registerResource("DMS/webroot/js/cssverticalmenu.js", "dms/js", "REL_R57_33", false, USATRegistry.HTTPD_NET);
		
		registerResource("ServerLayers/AppLayer/src/loader-data-layer.xml", "classes", "REL_R57_33", false, USATRegistry.LOADER);

		registerSource("ServerLayers/POSMLayer/src/main", DFRSetup.class, "REL_R57_33", USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/POSMLayer/src/main", POSMSaleUpdateTask.class, "REL_R57_33", USATRegistry.POSM_LAYER);
		registerResource("ServerLayers/POSMLayer/conf/posm-data-layer.xml", "classes", "REL_R57_33", false, USATRegistry.POSM_LAYER);
	}
	
	@Override
	public String getName() {
		return "R57 Update 33 - EFT Funding";
	}
}
