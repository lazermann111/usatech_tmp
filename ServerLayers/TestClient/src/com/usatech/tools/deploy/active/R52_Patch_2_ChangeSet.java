package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import org.apache.axis.transport.http.USATCommonsHTTPSender;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R52_Patch_2_ChangeSet extends MultiLinuxPatchChangeSet {
	public R52_Patch_2_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/LayersCommon/src", USATCommonsHTTPSender.class, "REL_report_generator_3_26_2", USATRegistry.TRANSPORT_LAYER);
	}
	
	@Override
	public String getName() {
		return "R52 Patch 2";
	}
}
