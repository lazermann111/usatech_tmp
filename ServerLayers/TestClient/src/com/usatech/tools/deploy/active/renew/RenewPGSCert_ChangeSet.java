package com.usatech.tools.deploy.active.renew;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.RenewFileCertTask;
/**
 * Before running this task, make sure the primary and stdby are on the same host. 
 * Use clustat to check, also ls -l /data, ls -l /stdby/data
 * If not on the same host, use clusvcadm relocate to relocate stdby to the same host as primary:
 * clusvcadm -r stdby -m devpgs11-priv
 * 
 * This class does the following step:
 * 1. backup primary cert related files at /home/"+runUseName+"/pgsPrimaryCertBackup/
 * 2. backup stdby cert related files at /home/"+runUseName+"/pgsStdbyCertBackup/
 * 3. renew primary server cert
 * 4. renew stdby server cert
 * 5. renew stdby replica client cert that is used to connect to the primary to do streaming
 * 
 * After renew, restart using cluster command:
 * clusvcadm -R primary
 * clusvcadm -R stdby
 * 
 * Verify:
 * 1. check cert
 * sudo cat /opt/USAT/postgres/data/server.crt| openssl x509 -noout -enddate
 * sudo cat /opt/USAT/postgres_stdby/data/server.crt| openssl x509 -noout -enddate
 * sudo cat /opt/USAT/postgres_stdby/data/client.crt| openssl x509 -noout -enddate
 * check logs
 * ps -ef | grep sender 	--	should return valid process
 * ps -ef | grep receiver 	--	should return valid process
 * 
 * 2. Login to usalive, then connect to pgs via pgAdmin and run to see the new request is inserted.
 * 
 * SELECT app_request_id, app_cd, request_url, request_ts, remote_addr, 
       remote_port, referer, http_user_agent, session_id, username, 
       request_headers, request_parameters, process_time_ms, exception_flag, 
       exception_text, request_handler, folio_id, report_id, profile_id, 
       action_name, object_type_cd, object_cd, user_id
  FROM main._app_request_2016_03 where request_ts> current_timestamp-interval '15 minutes' order by request_ts desc;
  
  Cert is valid for 3 years
 * @author yhe
 *
 */
public class RenewPGSCert_ChangeSet implements ChangeSet {

	@Override
	public String getName() {
		return "Renew PGS Certificate";
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("PGS");
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		String username="postgres";
		String runUseName=System.getProperty("user.name");
		String backupPrimaryFolderPath="/home/"+runUseName+"/pgsPrimaryCertBackup/";
		String backupStdbyFolderPath="/home/"+runUseName+"/pgsStdbyCertBackup/";
		List<DeployTask> tasks = new ArrayList<DeployTask>();
		
		String pgPrimaryDir="/opt/USAT/postgres";
		RenewFileCertTask primaryServerCert=new RenewFileCertTask(new Date(Long.MAX_VALUE), pgPrimaryDir + "/data/server.crt", pgPrimaryDir + "/data/root.crt", pgPrimaryDir + "/data/server.key", 0644, 0600, username, username);
		primaryServerCert.setCheckExpiration(false);
		
		String pgStdbyDir="/opt/USAT/postgres_stdby";
		RenewFileCertTask stdbyServerCert=new RenewFileCertTask(new Date(Long.MAX_VALUE), pgStdbyDir + "/data/server.crt", pgStdbyDir + "/data/root.crt", pgStdbyDir + "/data/server.key", 0644, 0600, username, username);
		stdbyServerCert.setCheckExpiration(false);
		
		RenewFileCertTask replicaCert=new RenewFileCertTask(new Date(Long.MAX_VALUE), pgStdbyDir+"/data/client.crt",pgStdbyDir+"/data/client.key", 0644, 0600, "postgres", "postgres", null, "replica", "USATAuthenticatedSession", "replica");
		replicaCert.setCheckExpiration(false);
		ExecuteTask backupPrimaryTask=new ExecuteTask("if [ ! -d "+backupPrimaryFolderPath+" ]; then mkdir "+backupPrimaryFolderPath+"; fi",
				"sudo /bin/cp /opt/USAT/postgres/data/server.crt "+backupPrimaryFolderPath+"server.crt.`date +%Y%m%d-%H%M%S`",
				"sudo /bin/cp /opt/USAT/postgres/data/root.crt "+backupPrimaryFolderPath+"root.crt.`date +%Y%m%d-%H%M%S`",
				"sudo /bin/cp /opt/USAT/postgres/data/server.key "+backupPrimaryFolderPath+"server.key.`date +%Y%m%d-%H%M%S`"
				);
		ExecuteTask backupStdbyTask=new ExecuteTask("if [ ! -d "+backupStdbyFolderPath+" ]; then mkdir "+backupStdbyFolderPath+"; fi",
				"sudo /bin/cp /opt/USAT/postgres_stdby/data/server.crt "+backupStdbyFolderPath+"server.crt.`date +%Y%m%d-%H%M%S`",
				"sudo /bin/cp /opt/USAT/postgres_stdby/data/root.crt "+backupStdbyFolderPath+"root.crt.`date +%Y%m%d-%H%M%S`",
				"sudo /bin/cp /opt/USAT/postgres_stdby/data/server.key "+backupStdbyFolderPath+"server.key.`date +%Y%m%d-%H%M%S`",
				"sudo /bin/cp /opt/USAT/postgres_stdby/data/client.key "+backupStdbyFolderPath+"client.key.`date +%Y%m%d-%H%M%S`",
				"sudo /bin/cp /opt/USAT/postgres_stdby/data/client.crt "+backupStdbyFolderPath+"client.crt.`date +%Y%m%d-%H%M%S`"
				);
		tasks.add(backupPrimaryTask);
		tasks.add(backupStdbyTask);
		tasks.add(primaryServerCert);
		tasks.add(stdbyServerCert);
		tasks.add(replicaCert);
		return tasks.toArray(new DeployTask[tasks.size()]);
	}

	@Override
	public String toString() {
		return getName();
	}
}
