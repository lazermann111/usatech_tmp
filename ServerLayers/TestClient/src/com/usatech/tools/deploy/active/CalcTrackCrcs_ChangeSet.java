package com.usatech.tools.deploy.active;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.DeployTaskInfo;
import com.usatech.tools.deploy.DeployUtils;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;

import oracle.jdbc.driver.OracleDriver;
import simple.bean.ConvertException;
import simple.db.Argument;
import simple.db.Call;
import simple.db.Cursor;
import simple.db.ParameterException;
import simple.io.ByteOutput;
import simple.io.ConfigSource;
import simple.io.IOUtils;
import simple.io.Log;
import simple.io.OutputStreamByteOutput;
import simple.results.DatasetUtils;
import simple.results.Results;
import simple.security.SecureHash;
import simple.lang.sun.misc.CRC16;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;

public class CalcTrackCrcs_ChangeSet implements ChangeSet {
	private static final Log log = Log.getLog();
	@Override
	public String getName() {
		return "Calc Track Crcs";
	}

	@Override
	public boolean isApplicable(Host host) {
		return true;
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		return new DeployTask[] { new DeployTask() {
			public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
				int cnt = createCrcFile(deployTaskInfo);
				return "Successfully calculated CRC for " + cnt + " accounts";
			}
			public String getDescription(DeployTaskInfo deployTaskInfo) {
				return "Calculating the CRC";
			}
		} };
	}

	protected int createCrcFile(DeployTaskInfo deployTaskInfo) throws IOException {
		String operUrl;
		if("DEV".equals(deployTaskInfo.host.getServerEnv()))
			operUrl = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb011.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=usadev03.world)))";
		else if("INT".equals(deployTaskInfo.host.getServerEnv()))
			operUrl = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=intdb011.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=usadev02.world)))";
		else if("ECC".equals(deployTaskInfo.host.getServerEnv()))
			operUrl = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=eccscan01.usatech.com)(PORT=1525))(ADDRESS=(PROTOCOL=TCP)(HOST=eccscan02.usatech.com)(PORT=1525)))(CONNECT_DATA=(SERVICE_NAME=usaecc_db.world)))";
		else if("USA".equals(deployTaskInfo.host.getServerEnv()))
			operUrl = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.120)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.121)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.122)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.220)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.221)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.222)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)))";
		else
			throw new IOException("Server Type '" + deployTaskInfo.host.getServerEnv() + "' is not recognized");

		new OracleDriver();
		char[] password = DeployUtils.getPassword(deployTaskInfo, "SYSTEM@OPER", "SYSTEM user on OPER database");
		Connection conn;
		try {
			conn = DriverManager.getConnection(operUrl, "SYSTEM", new String(password));
		} catch(SQLException e) {
			deployTaskInfo.passwordCache.setPassword(deployTaskInfo.host, "SYSTEM@OPER", null);
			throw new IOException(e);
		}
		try {
			return createCrcFile(conn, deployTaskInfo.host.getServerEnv());
		} catch(ParameterException e) {
			throw new IOException(e);
		} catch(SQLException e) {
			throw new IOException(e);
		} finally {
			try {
				conn.close();
			} catch(SQLException e) {
			}
		}
	}

	protected static int createCrcFile(Connection conn, String env) throws IOException, ParameterException, SQLException {
		Call call = new Call();
		ConfigSource src = ConfigSource.createConfigSource("CalcTrackCrc.sql");
		InputStream in = src.getInputStream();
		String sql;
		try {
			sql = IOUtils.readFully(in);
		} finally {
			in.close();
		}
		/*
		call.setSql("SELECT CA.CONSUMER_ACCT_ID GLOBAL_ACCOUNT_ID, CA.CONSUMER_ACCT_CD, CA.CONSUMER_ACCT_FMT_ID, M.MERCHANT_CD, CA.CONSUMER_ACCT_CD_HASH, TO_CHAR(CA.CONSUMER_ACCT_DEACTIVATION_TS, 'YYMM') || CA.CONSUMER_ACCT_ISSUE_CD || CA.CONSUMER_ACCT_VALIDATION_CD EXTRA, CA.CONSUMER_ACCT_RAW_HASH, CA.CONSUMER_ACCT_RAW_ALG, CA.CONSUMER_ACCT_RAW_SALT FROM PSS.CONSUMER_ACCT CA LEFT OUTER JOIN (PSS.MERCHANT_CONSUMER_ACCT MCA JOIN PSS.MERCHANT M ON MCA.MERCHANT_ID = M.MERCHANT_ID) ON MCA.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID WHERE CA.CONSUMER_ACCT_FMT_ID IS NOT NULL AND CA.CONSUMER_ACCT_CD_HASH IS NOT NULL"
				+ " and CONSUMER_ACCT_IDENTIFIER = 125724"); 
				*/
		call.setSql(sql);
		Cursor cursor = new Cursor();
		cursor.setAllColumns(true);
		call.setArguments(new Argument[] { cursor });
		Results results = call.executeQuery(conn, null, null);
		int cnt = 0;
		Set<Long> failures = new HashSet<Long>();
		try {
			// TODO: eventually use a resource outputstream
			OutputStream out = new FileOutputStream("C:\\Users\\bkrug\\Downloads\\TrackCrcs_" + env + ".dat");
			try {
				ByteOutput output = new OutputStreamByteOutput(out);
				String[] header = new String[] { 
						AuthorityAttrEnum.ATTR_ACCOUNT_CD_HASH.getValue(), 
						AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID.getValue(), 
						AuthorityAttrEnum.ATTR_TRACK_CRC.getValue(), 
						AuthorityAttrEnum.ATTR_INSTANCE.getValue() 
				};
				Object[] values = new Object[header.length];
				DatasetUtils.writeHeader(output, header);
				// CharsetEncoder ce = Charset.defaultCharset().newEncoder();
				while(results.next()) {
					String cardNumber = results.getValue("CONSUMER_ACCT_CD", String.class);
					Integer fmtId = results.getValue("CONSUMER_ACCT_FMT_ID", Integer.class);
					byte[] acctCdHash = results.getValue("CONSUMER_ACCT_CD_HASH", byte[].class);
					long globalAccountId = results.getValue("GLOBAL_ACCOUNT_ID", long.class);
					if(fmtId != null) {
						if(!cardNumber.startsWith("639621")) {
							log.warn("Unrecognized card prefix '" + cardNumber.substring(0, 6));
							failures.add(globalAccountId);
							continue;
						}
						String merchantCd = results.getValue("MERCHANT_CD", String.class);
						byte[] groupIDPadded = StringUtils.pad(String.valueOf(Integer.parseInt(merchantCd.substring(merchantCd.length() - 3))), '0', 3, Justification.RIGHT).getBytes();
						byte[] cardArray = cardNumber.getBytes();
						switch(fmtId) {
							case 0:
							case 2:
								if(cardArray[6] == '*')
									cardArray[6] = (byte) ('0' + fmtId);
								for(int i = 7; i < 10; i++)
									if(cardArray[i] == '*')
										cardArray[i] = groupIDPadded[i - 7];
								int start = 10;
								for(; start < cardArray.length; start++)
									if(cardArray[start] == '*')
										break;
								int end = cardArray.length - 1;
								for(; end >= start; end--)
									if(cardArray[end] == '*')
										break;
								int baseChecksum = addChecksum(cardArray, 0, start) + addChecksum(cardArray, end + 1, cardArray.length);
								log.info("Must guess " + (end - start + 1) + " digits for " + cardNumber);
								for(int i = start; i <= end; i++)
									cardArray[i] = '0';
								int max = (int) Math.pow(10, (end - start + 1));
								int n;
								CARD_MATCH: for(n = 1; n < max; n++) {
									int checksum = baseChecksum + addChecksum(cardArray, start, end + 1);
									if((checksum % 10) == 0) {
										byte[] guessHash = SecureHash.getHash(cardArray, null, "SHA-256/0");
										if(Arrays.equals(acctCdHash, guessHash)) {
											log.info("Found account cd for " + cardNumber);
											// calc track & crc
											StringBuilder sb = new StringBuilder(40);
											String fullCard = new String(cardArray);
											String expDate = results.getValue("EXP_DATE", String.class);
											String extra = results.getValue("EXTRA", String.class);
											byte[] trackHash = results.getValue("CONSUMER_ACCT_RAW_HASH", byte[].class);
											byte[] trackSalt = results.getValue("CONSUMER_ACCT_RAW_SALT", byte[].class);
											String trackAlg = results.getValue("CONSUMER_ACCT_RAW_ALG", String.class);
											Integer crc;
											if(!StringUtils.isBlank(trackAlg) && trackHash != null && trackHash.length > 0) {
												if(!testTrack(fullCard, expDate, extra, trackHash, trackSalt, trackAlg, sb)) {
													boolean matched = false;
													char[] expDateArray = new char[4];
													for(int d = 0; d < 1200; d++) {
														sb.setLength(0);
														int year = 2010 + (d / 12);
														int month = 1 + (d % 12);
														expDateArray[0] = (char) ('0' + ((year % 100) / 10));
														expDateArray[1] = (char) ('0' + (year % 10));
														expDateArray[2] = (char) ('0' + (month / 10));
														expDateArray[3] = (char) ('0' + (month % 10));
														if(testTrack(fullCard, new String(expDateArray), extra, trackHash, trackSalt, trackAlg, sb)) {
															log.info("Found real exp date: " + new String(expDateArray) + " for " + cardNumber);
															matched = true;
															break;
														}
													}
													if(!matched) {
														log.warn("Track Hash Does NOT match for " + cardNumber + "; expdate=" + expDate);
														failures.add(globalAccountId);
														break CARD_MATCH;
													}
												} else {
													log.info("Track Hash matches for " + cardNumber);
												}
												String track = sb.toString();
												crc = getCRC16(track);
											} else {
												crc = null;
											}

											values[0] = SecureHash.getHash(cardArray, null, "SHA-256/1000");
											values[1] = globalAccountId;
											values[2] = crc;
											values[3] = 0;
											DatasetUtils.writeRow(output, values);
											cnt++;
											break;
										}
									}
									String s = Integer.toString(n);
									s.getBytes(0, s.length(), cardArray, end - s.length() + 1);
									// ce.encode(in)
								}
								if(n == max) {
									log.info("Could NOT find account cd after " + n + " guesses for " + cardNumber);
									failures.add(globalAccountId);
								}
								break;
							case 1:
								if(cardArray[6] == '*')
									cardArray[6] = (byte) ('0' + fmtId);
								// cardArray[7] = process code and is unknown
								for(int i = 8; i < 11; i++)
									if(cardArray[i] == '*')
										cardArray[i] = groupIDPadded[i - 8];
								start = 11;
								for(; start < cardArray.length; start++)
									if(cardArray[start] == '*')
										break;
								end = cardArray.length - 1;
								for(; end >= start; end--)
									if(cardArray[end] == '*')
										break;
								baseChecksum = addChecksum(cardArray, 0, 7) + addChecksum(cardArray, 8, start) + addChecksum(cardArray, end + 1, cardArray.length);
								log.info("Must guess " + (end - start + 2) + " digits for " + cardNumber);
								cardArray[7] = '0';
								for(int i = start; i <= end; i++)
									cardArray[i] = '0';
								max = (int) Math.pow(10, (end - start + 2));
								CARD_MATCH: for(n = 1; n < max; n++) {
									int checksum = baseChecksum + addChecksum(cardArray, start, end + 1);
									if((checksum % 10) == 0) {
										byte[] guessHash = SecureHash.getHash(cardArray, null, "SHA-256/0");
										if(Arrays.equals(acctCdHash, guessHash)) {
											log.info("Found account cd for " + cardNumber);
											// calc track & crc
											StringBuilder sb = new StringBuilder(40);
											String fullCard = new String(cardArray);
											String expDate = results.getValue("EXP_DATE", String.class);
											String extra = results.getValue("EXTRA", String.class);
											byte[] trackHash = results.getValue("CONSUMER_ACCT_RAW_HASH", byte[].class);
											byte[] trackSalt = results.getValue("CONSUMER_ACCT_RAW_SALT", byte[].class);
											String trackAlg = results.getValue("CONSUMER_ACCT_RAW_ALG", String.class);
											Integer crc;
											if(!StringUtils.isBlank(trackAlg) && trackHash != null && trackHash.length > 0) {
												if(!testTrack(fullCard, expDate, extra, trackHash, trackSalt, trackAlg, sb)) {
													boolean matched = false;
													char[] expDateArray = new char[4];
													for(int d = 0; d < 1200; d++) {
														sb.setLength(0);
														int year = 2010 + (d / 12);
														int month = 1 + (d % 12);
														expDateArray[0] = (char) ('0' + ((year % 100) / 10));
														expDateArray[1] = (char) ('0' + (year % 10));
														expDateArray[2] = (char) ('0' + (month / 10));
														expDateArray[3] = (char) ('0' + (month % 10));
														if(testTrack(fullCard, new String(expDateArray), extra, trackHash, trackSalt, trackAlg, sb)) {
															log.info("Found real exp date: " + new String(expDateArray) + " for " + cardNumber);
															matched = true;
															break;
														}
													}
													if(!matched) {
														log.warn("Track Hash Does NOT match for " + cardNumber + "; expdate=" + expDate);
														failures.add(globalAccountId);
														break CARD_MATCH;
													}
												} else {
													log.info("Track Hash matches for " + cardNumber);
												}
												String track = sb.toString();
												crc = getCRC16(track);
											} else {
												crc = null;
											}

											values[0] = SecureHash.getHash(cardArray, null, "SHA-256/1000");
											values[1] = globalAccountId;
											values[2] = crc;
											values[3] = 0;
											DatasetUtils.writeRow(output, values);
											cnt++;
											break;
										}
									}
									String s = Integer.toString(n);
									if(s.length() >= end - start + 2)
										s.getBytes(0, 1, cardArray, 7);
									s.getBytes(1, s.length(), cardArray, end - s.length() + 2);
									// ce.encode(in)
								}
								if(n == max) {
									log.info("Could NOT find account cd after " + n + " guesses for " + cardNumber);
									failures.add(globalAccountId);
								}
								break;
						}
					} else if(!cardNumber.contains("*")) {
						String accountCd = MessageResponseUtils.getCardNumber(cardNumber);
						values[0] = SecureHash.getHash(accountCd.getBytes(), null, "SHA-256/1000");
						values[1] = globalAccountId;
						values[2] = null;
						values[3] = 0;
						DatasetUtils.writeRow(output, values);
						cnt++;
						log.info("Using orginal card number for " + accountCd.substring(0, 6) + "****");
					} else if(acctCdHash != null) {
						values[0] = SecureHash.getHash(acctCdHash, null, "SHA-256/999");
						values[1] = globalAccountId;
						values[2] = null;
						values[3] = 0;
						DatasetUtils.writeRow(output, values);
						cnt++;
						log.info("Using hash only for " + cardNumber);
					} else {
						log.warn("Account Cd Hash is null for " + cardNumber + "; cannot process");
						failures.add(globalAccountId);
					}
				}
				DatasetUtils.writeFooter(output);
			} catch(ConvertException e) {
				throw new IOException(e);
			} catch(NoSuchAlgorithmException e) {
				throw new IOException(e);
			} finally {
				out.close();
			}
		} finally {
			results.close();
		}
		log.warn("List of uncalculable global account ids: " + failures);
		return cnt;
	}

	protected static boolean testTrack(String cardNumber, String expDate, String extra, byte[] trackHash, byte[] trackSalt, String trackAlg, StringBuilder sb) throws NoSuchAlgorithmException {
		sb.append(cardNumber).append('=').append(expDate).append(extra);
		int mod97 = mod97(sb.toString().replace('=', '0'));
		sb.append((mod97 / 10) % 10).append(mod97 % 10);
		String track = sb.toString();
		byte[] checkHash = SecureHash.getHash(track.getBytes(), trackSalt, trackAlg);
		if(!Arrays.equals(trackHash, checkHash)) {
			// log.warn("Track Hash Does NOT match for " + cardNumber + "; expdate=" + expDate + "; mod97=" + mod97);
			return false;
		}
		return true;
	}

	protected static String buildTrack(String cardNumber, String expDate, String extra, StringBuilder sb) {
		sb.append(cardNumber).append('=').append(expDate).append(extra);
		int mod97 = mod97(sb.toString().replace('=', '0'));
		sb.append((mod97 / 10) % 10).append(mod97 % 10);
		return sb.toString();
	}

	public static int calcDigit(String accountCd, int index) {
		int[] a = new int[accountCd.length()];
		for(int i = 0; i < a.length; i++)
			a[i] = Character.getNumericValue(accountCd.charAt(i));

		int len = a.length;
		int checksum = 0;
		int tmp;

		for(int i = (len % 2); i < len; i += 2) {
			if(i == index)
				continue;
			tmp = a[i] * 2;
			while(tmp > 9)
				tmp = (tmp % 10) + (tmp / 10);
			checksum += tmp;
		}
		for(int i = ((len + 1) % 2); i < len; i += 2) {
			if(i == index)
				continue;
			tmp = a[i];
			while(tmp > 9)
				tmp = (tmp % 10) + (tmp / 10);
			checksum += tmp;
		}

		checksum = (10 - (checksum % 10)) % 10;
		if(((len - index) % 2) == 0)
			return (checksum % 2) == 0 ? checksum / 2 : (9 + checksum) / 2;
		return checksum;
	}
	
	public static int addChecksum(byte[] cardArray, int start, int end) {
		int len = cardArray.length;
		int checksum = 0;
		int tmp;

		for(int i = start + ((len + start) % 2); i < end; i += 2) {
			tmp = Character.getNumericValue(cardArray[i]) * 2;
			while(tmp > 9)
				tmp = (tmp % 10) + (tmp / 10);
			checksum += tmp;
		}
		for(int i = start + ((len + start + 1) % 2); i < end; i += 2) {
			tmp = Character.getNumericValue(cardArray[i]);
			checksum += tmp;
		}

		return checksum;
	}

	protected static int getCRC16(String s) {
		CRC16 crc = new CRC16();
		for(byte b : s.getBytes())
			crc.update(b);
		return crc.value;
	}
	private static final BigInteger bi97 = BigInteger.valueOf(97);
	private static final BigInteger bi98 = BigInteger.valueOf(98);
	private static final BigInteger bi100 = BigInteger.valueOf(100);

	public static int mod97(String s) {
		BigInteger i = new BigInteger(s);
		return bi98.subtract(i.multiply(bi100).mod(bi97)).mod(bi97).intValue();
	}

	@Override
	public String toString() {
		return getName();
	}

	public static void main(String[] args) throws Exception {
		// String operUrl = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb011.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=usadev03.world)))";
		String operUrl = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb012.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=USADEV04.WORLD)))";

		new OracleDriver();
		char[] password = "usatmanager".toCharArray();
		Connection conn = DriverManager.getConnection(operUrl, "SYSTEM", new String(password));
		createCrcFile(conn, "local");
	}
}
