package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R50_Patch_5_ChangeSet extends MultiLinuxPatchChangeSet {
	public R50_Patch_5_ChangeSet() throws UnknownHostException {
		super();
		registerResource("usalive/xsl/rma-data-layer.xml", "classes/", "REL_USALive_1_26_5", true, USATRegistry.USALIVE_APP);
	}

	@Override
	public String getName() {
		return "R50 Patch 5";
	}
}
