package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import simple.security.crypt.CryptUtils;

import com.usatech.app.MessageChainV11;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R44_KeySupplyPatch_ChangeSet extends MultiLinuxPatchChangeSet {
	public R44_KeySupplyPatch_ChangeSet() throws UnknownHostException {
		super();
		registerSource("Simple1.5/src", CryptUtils.class, "HEAD", USATRegistry.APP_LAYER);
		registerSource("USATMessaging/src", MessageChainV11.class, "HEAD", USATRegistry.APP_LAYER);
	}

	@Override
	public String getName() {
		return "R44 Key Supply Patch";
	}
}
