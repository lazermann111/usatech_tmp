package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R51_Patch_5_ChangeSet extends MultiLinuxPatchChangeSet {
	public R51_Patch_5_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/LayersCommon/src/common-data-layer.xml", "classes", "REL_posmlayer_1_28_5", false, USATRegistry.POSM_LAYER);
		registerResource("ServerLayers/LayersCommon/src/common-data-layer.xml", "classes", "REL_DMS_1_13_5", false, USATRegistry.DMS_APP);
	}
	
	@Override
	public String getName() {
		return "R51 Patch 5";
	}
}
