package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.app.ParallelBatchUpdateDatasetHandler;
import com.usatech.posm.schedule.dfr.AbstractDFRParser;
import com.usatech.posm.schedule.dfr.DFRSetup;
import com.usatech.posm.schedule.dfr.ParseFinancialDFRTask;
import com.usatech.posm.schedule.dfr.ParseFinancialDFRTaskMXBean;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R48_Patch_5_ChangeSet extends MultiLinuxPatchChangeSet {
	public R48_Patch_5_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/POSMLayer/conf/POSMLayerService.properties", "classes/", "REL_posmlayer_1_25_5", false, USATRegistry.POSM_LAYER);
		registerSource("USATMessaging/src", ParallelBatchUpdateDatasetHandler.class, "REL_posmlayer_1_25_5", USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/POSMLayer/src/main", DFRSetup.class, "REL_posmlayer_1_25_5", USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/POSMLayer/src/main", ParseFinancialDFRTaskMXBean.class, "REL_posmlayer_1_25_5", USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/POSMLayer/src/main", AbstractDFRParser.class, "REL_posmlayer_1_25_5", USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/POSMLayer/src/main", ParseFinancialDFRTask.class, "REL_posmlayer_1_25_5", USATRegistry.POSM_LAYER);
	}
	
	@Override
	public String getName() {
		return "R48 Patch 5";
	}
}
