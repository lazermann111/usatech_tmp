package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class USALiveThanksgivingRollback_ChangeSet extends MultiLinuxPatchChangeSet {
	public USALiveThanksgivingRollback_ChangeSet() throws UnknownHostException {
		super();
		registerResource("usalive/web/login.html.jsp", "web/", "REL_USALive_1_29_0", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/customerreporting_web/home.jsp", "web/", "REL_USALive_1_29_0", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/customerreporting_web/login.jsp", "web/", "REL_USALive_1_29_0", false, USATRegistry.USALIVE_APP);
	}
	
	@Override
	public String getName() {
		return "Rollback Usalive Thanksgiving 2016";
	}
}
