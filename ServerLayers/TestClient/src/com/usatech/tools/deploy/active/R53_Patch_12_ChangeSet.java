package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.layers.common.sprout.SproutUtils;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R53_Patch_12_ChangeSet extends MultiLinuxPatchChangeSet {
	
	public R53_Patch_12_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/LayersCommon/src", SproutUtils.class, "REL_authoritylayer_1_39_12", USATRegistry.OUTAUTH_LAYER);
	}
	
	@Override
	public String getName() {
		return "R53 Patch 12";
	}
}
