package com.usatech.tools.deploy.active;

import java.io.IOException;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.CvsPullTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.ImportJKSCertTask;
import com.usatech.tools.deploy.PasswordCache;

public class ImportTokenCACertificate_ChangeSet implements ChangeSet {
	public ImportTokenCACertificate_ChangeSet() {
		super();
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		return new DeployTask[] {
				getCertTask(cache)
		};
	}
	
	public ImportJKSCertTask getCertTask(PasswordCache cache){
		ImportJKSCertTask webCertsTask = new ImportJKSCertTask("/opt/USAT/conf/webtruststore.ts", cache, 0640, false);
		webCertsTask.addCertificate("usattokenca_20200203", new CvsPullTask("ServerLayers/LayersCommon/conf/usat_token_20200203.crt"));
		return webCertsTask;
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("KLS");
	}
	
	@Override
	public String toString() {
		return getName();
	}
	@Override
	public String getName() {
		return "Import Token CA Certificate";
	}
}
