package com.usatech.tools.deploy.active;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import com.usatech.authoritylayer.InternalAuthorityTask;
import com.usatech.dms.consumer.NewConsumerAcct5FuncStep;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.DeployTaskInfo;
import com.usatech.tools.deploy.DeployUtils;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.tools.deploy.UploadAndRunPostgresSQLTask;
import com.usatech.tools.deploy.UsatSettingUpdateFileTask;

import oracle.jdbc.driver.OracleDriver;
import simple.app.Processor;
import simple.app.ServiceException;
import simple.db.DataLayerMgr;
import simple.db.ParameterException;
import simple.results.Results;

public class R50_Patch_9_ChangeSet extends MultiLinuxPatchChangeSet {
	public R50_Patch_9_ChangeSet() throws UnknownHostException {
		super();
		registerSource("DMS/src/", NewConsumerAcct5FuncStep.class, "REL_DMS_1_12_9", USATRegistry.DMS_APP);
		registerSource("ServerLayers/AuthorityLayer/src", InternalAuthorityTask.class, "REL_authoritylayer_1_36_9", USATRegistry.INAUTH_LAYER);
		registerResource("DMS/resources/datalayers/consumer-data-layer.xml", "classes", "REL_DMS_1_12_9", false, USATRegistry.DMS_APP);
	}
	
	@Override
	public String getName() {
		return "R50 Patch 9";
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("KLS") || super.isApplicable(host);
	}

	@Override
	protected void addPreHostTasks(Host host, List<DeployTask> tasks, PasswordCache cache) throws IOException {
		if(host.isServerType("KLS")) {
			String[] databases;
			switch(host.getSimpleName()) {
				case "devtool11":
					databases = new String[] { "bkkm1", "bkkm2", "dkkm1", "dkkm2", "dlkm1", "dlkm2", "evkm1", "evkm2" };
					break;
				case "devdbs11":
					databases = new String[] { "dkkm1", "dkkm2", "jskkm1", "jskm2", "pckm1", "pckm2", "yhkm1", "yhkm2" };
					break;
				default:
					databases = new String[] { "km" };
			}
			final String sql = "SELECT /*+USE_NL(CAB CA) */ DISTINCT GA.GLOBAL_ACCOUNT_ID   FROM PSS.CONSUMER_ACCT CA   JOIN PSS.CONSUMER_ACCT_BASE CAB ON CAB.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID   JOIN BKRUG.TMP_GA_DELETED GA ON CAB.GLOBAL_ACCOUNT_ID = GA.GLOBAL_ACCOUNT_ID  WHERE (CA.CONSUMER_ACCT_RAW_HASH IS NULL OR CA.CONSUMER_ACCT_VALIDATION_CD IS NULL) UNION SELECT /*+USE_NL(CAB CA) */ CAB.GLOBAL_ACCOUNT_ID   FROM PSS.CONSUMER_ACCT CA   JOIN PSS.CONSUMER_ACCT_BASE CAB ON CAB.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID   JOIN PSS.CONSUMER_ACCT CA0 ON CA.CONSUMER_ACCT_CD_HASH = CA0.CONSUMER_ACCT_CD_HASH   WHERE CA0.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'N'    AND CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y'    AND CA.CONSUMER_ACCT_TYPE_ID != 4    AND CA.CONSUMER_ACCT_CD_HASH IS NOT NULL";
			tasks.add(new UploadAndRunPostgresSQLTask("DatabaseScripts/releases/REL_Edge_Update_R50/R50_Patch_9.KM.sql", "HEAD", false, databases));
			tasks.add(new UploadAndRunPostgresSQLTask(new Processor<DeployTaskInfo, InputStream>() {
				public InputStream process(DeployTaskInfo argument) throws ServiceException {
					StringBuilder sb = new StringBuilder();
					sb.append("UPDATE KM.ACCOUNT SET ENCRYPTED_CRC = NULL WHERE GLOBAL_ACCOUNT_ID IN(0");
					try {
						try (Connection conn = getConnection(argument)) {
							try (Results results = DataLayerMgr.executeSQL(conn, sql, null, null)) {
								while(results.next()) {
									sb.append(',').append(results.getValue(1));
								}
							}
						}
					} catch(SQLException | IOException | ParameterException e) {
						throw new ServiceException(e);
					}
					sb.append(')');
					return new ByteArrayInputStream(sb.toString().getBytes());
				}

				public java.lang.Class<DeployTaskInfo> getArgumentType() {
					return DeployTaskInfo.class;
				}

				public java.lang.Class<InputStream> getReturnType() {
					return InputStream.class;
				}

			}, false, null, databases));
		}
	}

	protected Connection getConnection(DeployTaskInfo deployTaskInfo) throws IOException {
		String operUrl;
		if("LOCAL".equals(deployTaskInfo.host.getServerEnv()))
			operUrl = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb012.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=USADEV04.WORLD)))";
		else if("DEV".equals(deployTaskInfo.host.getServerEnv()))
			operUrl = UsatSettingUpdateFileTask.OPER_DEV;
		else if("INT".equals(deployTaskInfo.host.getServerEnv()))
			operUrl = UsatSettingUpdateFileTask.OPER_INT;
		else if("ECC".equals(deployTaskInfo.host.getServerEnv()))
			operUrl = UsatSettingUpdateFileTask.OPER_ECC;
		else if("USA".equals(deployTaskInfo.host.getServerEnv()))
			operUrl = UsatSettingUpdateFileTask.OPER_PROD_DEFAULT;
		else
			throw new IOException("Server Type '" + deployTaskInfo.host.getServerEnv() + "' is not recognized");

		new OracleDriver();
		char[] password = DeployUtils.getPassword(deployTaskInfo, "SYSTEM@OPER", "SYSTEM user on OPER database");
		Connection conn;
		try {
			conn = DriverManager.getConnection(operUrl, "SYSTEM", new String(password));
		} catch(SQLException e) {
			deployTaskInfo.passwordCache.setPassword(deployTaskInfo.host, "SYSTEM@OPER", null);
			throw new IOException(e);
		}
		return conn;
	}
	
	@Override
	public String toString() {
		return getName();
	}
}
