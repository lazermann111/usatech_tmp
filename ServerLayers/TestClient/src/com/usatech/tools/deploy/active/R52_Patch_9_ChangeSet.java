package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R52_Patch_9_ChangeSet extends MultiLinuxPatchChangeSet {
	public R52_Patch_9_ChangeSet() throws UnknownHostException {
		super();
		registerResource("eSudsWebsite/web/Security/tiles/pages/resetPassword.jsp", "web/Security/tiles/pages", "REL_eSudsWebsite_1_2_9", false, USATRegistry.ESUDS_APP);
		registerResource("eSudsWebsite/web/Security/tiles/pages/changePassword.jsp", "web/Security/tiles/pages", "REL_eSudsWebsite_1_2_9", false, USATRegistry.ESUDS_APP);
		registerResource("eSudsWebsite/web/Security/tiles/pages/login.jsp", "web/Security/tiles/pages", "REL_eSudsWebsite_1_2_9", false, USATRegistry.ESUDS_APP);
		registerResource("eSudsWebsite/web/WEB-INF/classes/templates/esuds/login.xsl", "web/WEB-INF/classes/templates/esuds", "REL_eSudsWebsite_1_2_9", true, USATRegistry.ESUDS_APP);
		
	}
	
	@Override
	public String getName() {
		return "R52 Patch 9";
	}
}
