package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class USALiveThanksgiving2016_ChangeSet extends MultiLinuxPatchChangeSet {
	public USALiveThanksgiving2016_ChangeSet() throws UnknownHostException {
		super();
		registerResource("usalive/web/images/win-four-tickets.gif", "web/images", "REL_USALive_1_29_8", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/login.html.jsp", "web/", "REL_USALive_1_29_8", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/customerreporting_web/home.jsp", "web/", "REL_USALive_1_29_8", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/customerreporting_web/login.jsp", "web/", "REL_USALive_1_29_8", false, USATRegistry.USALIVE_APP);
	}
	
	@Override
	public String getName() {
		return "Usalive Thanksgiving 2016";
	}
}
