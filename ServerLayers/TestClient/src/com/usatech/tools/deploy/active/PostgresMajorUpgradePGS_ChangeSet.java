package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.regex.Pattern;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.DeployTaskInfo;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.FromCvsUploadTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.HostImpl;
import com.usatech.tools.deploy.OptionalTask;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.Registry;
import com.usatech.tools.deploy.USATHelper;
import com.usatech.tools.deploy.USATRegistry;

import simple.io.Log;
import simple.text.RegexUtils;
import simple.text.StringUtils;
import simple.util.CollectionUtils;

public class PostgresMajorUpgradePGS_ChangeSet implements ChangeSet {
	private static final Log log = Log.getLog();
	protected final String upgradeVersion = "9.5.1";
	protected final Registry registry;

	public PostgresMajorUpgradePGS_ChangeSet() throws UnknownHostException {
		super();
		this.registry = new USATRegistry();
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("PGS");
	}
	
	public static void addInstallPgMajor95BinariesTasks(Host host, String version, String workDir, List<DeployTask> tasks) throws IOException {
		String[] parts = StringUtils.split(version, '.', 3);
		if(parts.length != 3)
			throw new IllegalArgumentException("Invalid version '" + version + "'");
		String dash;
		if("5".equals(parts[1]))
			dash = "2";
		else
			dash = "1";
		tasks.add(new ExecuteTask("sudo su", 
				"rpm -Uh /root/postgresql95/postgresql95-9.5.1-1PGDG.rhel6.x86_64.rpm /root/postgresql95/postgresql95-contrib-9.5.1-1PGDG.rhel6.x86_64.rpm /root/postgresql95/postgresql95-devel-9.5.1-1PGDG.rhel6.x86_64.rpm /root/postgresql95/postgresql95-libs-9.5.1-1PGDG.rhel6.x86_64.rpm /root/postgresql95/postgresql95-server-9.5.1-1PGDG.rhel6.x86_64.rpm",
				"unlink /usr/pgsql-latest", 
				"ln -s /usr/pgsql-" +  parts[0] + '.' + parts[1] + " /usr/pgsql-latest"));
		if(host.isServerType("PGS")){
			USATHelper.addOdbcLinkTasks(host, null, tasks, workDir);
			tasks.add(new OptionalTask("if [ ! -x /usr/pgsql-latest/lib/pg_repack.so ]; then echo 'Y'; fi", Pattern.compile("^Y\\s*"), false,
					new FromCvsUploadTask("server_app_config/Postgresql/pg_repack/pg_repack-1.3.3.zip", "HEAD", "pg_repack-1.3.3.zip", 0644),
					new ExecuteTask("sudo su",
									//"yum -y install readline*",
									"if [ -d /home/`logname`/pg_repack-1.3.3 ]; then /bin/rm -r /home/`logname`/pg_repack-1.3.3; fi",
									"cd /home/`logname`",
									"unzip pg_repack-1.3.3.zip",
									"cd pg_repack-1.3.3",
									"export PATH=$PATH:/usr/pgsql-latest/bin",
									"make",
									"make install")
					));
		}
	}
	
	protected Host getOtherHost(Host host) {
		char[] array = host.getFullName().toCharArray();
		int pos = CollectionUtils.iterativeSearch(array, '.', 0, array.length);
		if(pos < 1)
			pos = array.length;
		switch(array[pos - 1]) {
			case '1':
				array[pos - 1] = '2';
				break;
			case '2':
				array[pos - 1] = '1';
				break;
			default:
				throw new IllegalArgumentException("Can't determine other host for '" + host.getSimpleName() + "'");
		}
		return new HostImpl(host.getServerEnv(), host.getServerTypes().toArray(new String[0]), new String(array));
	}
	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		// upgrade
		final Pattern fullVersionRegex = Pattern.compile(RegexUtils.escape(upgradeVersion));
		final Pattern majorVersionRegex = Pattern.compile(StringUtils.join(StringUtils.split(upgradeVersion, '.'), "\\.", 0, 2));
		final BitSet ALLOW_0_1_BITSET = new BitSet();
		ALLOW_0_1_BITSET.set(0);
		ALLOW_0_1_BITSET.set(1);
		DeployTask pgUpgradeTask = new DeployTask() {
			@Override
			public String getDescription(DeployTaskInfo deployTaskInfo) {
				return "Upgrading Postgres to " + upgradeVersion;
			}

			@Override
			public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
				String notifyUser="yhe@usatech.com,kchigurupati@usatech.com";
				log.info("PostgresMajorUpgradePGS_ChangeSet start notifyUser="+notifyUser);
				if(notifyUser!=null){
					ExecuteTask.executeCommands(deployTaskInfo, "sudo su - postgres", 
						"echo \"[`date`] PostgresMajorUpgradePGS_ChangeSet start\"| mail -s \"PostgresMajorUpgradePGS_ChangeSet start\" \""+notifyUser+"\"",
						"exit");
				}
				StringBuilder desc = new StringBuilder();
				Host other = getOtherHost(deployTaskInfo.host);
				deployTaskInfo.interaction.printf("Determining existing state of binaries and data");
				// determine state - binary version (local) [we know it's not up-to-date], binary version (other), data version (primary), data version (standby)
				String[] commands = new String[] {
					"/usr/pgsql-latest/bin/pg_config --version | cut -d ' ' -f 2", 
					"sudo su", 
					"if [ -f /opt/USAT/postgres/data/PG_VERSION -o `df /data | grep -c 'primaryvg'` -eq 0 ]; then cat /opt/USAT/postgres/data/PG_VERSION; else cat /opt/USAT/postgres/old_data/PG_VERSION; fi", 
					"if [ -f /opt/USAT/postgres_stdby/data/PG_VERSION -o `df /stdby/data | grep -c 'stdbyvg'` -eq 0 ]; then cat /opt/USAT/postgres_stdby/data/PG_VERSION; elif [ ! -f /opt/USAT/postgres_stdby/old_data/PG_VERSION ]; then echo '<empty>'; else cat /opt/USAT/postgres_stdby/old_data/PG_VERSION; fi"
				};
				
				String[] results = ExecuteTask.executeCommands(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.getSsh(), ALLOW_0_1_BITSET, commands);
				String[] otherResults = ExecuteTask.executeCommands(other, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache.getSshClient(other, deployTaskInfo.user), ALLOW_0_1_BITSET, commands);
				String localBinariesVersion = results[0].trim();
				String otherBinariesVersion = otherResults[0].trim();
				boolean hasStandby;
				boolean hasPrimary;
				String primaryDataVersion;
				String standbyDataVersion;
				if(!results[results.length - 1].endsWith("No such file or directory")) {
					standbyDataVersion = results[results.length - 1].trim();
					hasStandby = true;
				} else {
					if(otherResults[otherResults.length - 1].endsWith("No such file or directory"))
						throw new IOException("Standby data directory is not on either server. Please check cluster status");
					standbyDataVersion = otherResults[otherResults.length - 1].trim();
					hasStandby = false;
				}
				if(!results[results.length - 2].endsWith("No such file or directory")) {
					primaryDataVersion = results[results.length - 2].trim();
					hasPrimary = true;
				} else {
					if(otherResults[otherResults.length - 2].endsWith("No such file or directory"))
						throw new IOException("Primary data directory is not on either server. Please check cluster status");
					primaryDataVersion = otherResults[otherResults.length - 2].trim();
					hasPrimary = false;
				}
				boolean upgradeLocal = !fullVersionRegex.matcher(localBinariesVersion).matches();
				boolean upgradeOther = !fullVersionRegex.matcher(otherBinariesVersion).matches();
				boolean upgradeStandbyData = !majorVersionRegex.matcher(standbyDataVersion).matches();
				boolean upgradePrimaryData = !majorVersionRegex.matcher(primaryDataVersion).matches();
				log.info("upgradeLocal="+upgradeLocal);
				log.info("upgradeOther="+upgradeOther);
				log.info("upgradeStandbyData="+upgradeStandbyData);
				log.info("upgradePrimaryData="+upgradePrimaryData);
				DeployTaskInfo otherDeployTaskInfo = new DeployTaskInfo(deployTaskInfo.interaction, other, deployTaskInfo.user, deployTaskInfo.passwordCache, deployTaskInfo.sshClientCache);
				String workDir = "/home/`logname`/pg_update_" + upgradeVersion;
				if(!upgradeLocal && !upgradeOther) {
					deployTaskInfo.interaction.printf("Both Server binaries have already been upgraded");
					if(!upgradeStandbyData && !upgradePrimaryData) {
						deployTaskInfo.interaction.printf("Both Primary and Standby data directories have already been upgraded");
						return "Upgrade already complete";
					}
					// freeze cluster
					ExecuteTask.executeCommands(deployTaskInfo, "sudo su", "clusvcadm -Z stdby", "clusvcadm -Z primary");
				} else {
					// if we need to upgrade make sure standby is on opposite server as primary
					if(hasPrimary && hasStandby) {
						// relocate standby on other
						ExecuteTask.executeCommands(deployTaskInfo, "sudo su", "clusvcadm -r stdby -m " + other.getSimpleName() + "-priv");
						hasStandby = false;
					}
					if(!hasPrimary && !hasStandby) {
						// relocate standby on local
						ExecuteTask.executeCommands(deployTaskInfo, "sudo su", "clusvcadm -r stdby -m " + deployTaskInfo.host.getSimpleName() + "-priv");
						hasStandby = true;
					}
					// freeze cluster
					ExecuteTask.executeCommands(deployTaskInfo, "if [ ! -d " + workDir + " ]; then mkdir -p " + workDir + "; fi", "sudo su", "clusvcadm -Z stdby", "clusvcadm -Z primary");

					// upgrade binaries on primary
					if(upgradeLocal && hasPrimary) {
						List<DeployTask> tasks = new ArrayList<>();
						addInstallPgMajor95BinariesTasks(host, upgradeVersion, workDir, tasks);
						for(DeployTask task : tasks)
							task.perform(deployTaskInfo);
						desc.append("; Upgraded Primary binaries to " + upgradeVersion);
					} else if(upgradeOther && !hasPrimary) {
						ExecuteTask.executeCommands(otherDeployTaskInfo, "if [ ! -d " + workDir + " ]; then mkdir -p " + workDir + "; fi");
						List<DeployTask> tasks = new ArrayList<>();
						addInstallPgMajor95BinariesTasks(other, upgradeVersion, workDir, tasks);
						for(DeployTask task : tasks)
							task.perform(otherDeployTaskInfo);
						desc.append("; Upgraded Primary binaries to " + upgradeVersion);
					}
				}
				log.info("upgrade binaries on primary done");
				// upgrade primary data (if necessary)
				if(upgradePrimaryData) {
					String confFilePath = "/home/postgres/new_postgresql.conf";
					new FromCvsUploadTask("server_app_config/PGS/postgresql_primary.conf", "HEAD", confFilePath, 0644, "postgres", "postgres", true).perform(hasPrimary ? deployTaskInfo : otherDeployTaskInfo);
					List<String> subcommands = new ArrayList<>();
					USATHelper.addUpgradePgDataCommands(hasPrimary ? host : other, "postgres", true, confFilePath, subcommands, notifyUser);
					ExecuteTask.executeCommands(hasPrimary ? deployTaskInfo : otherDeployTaskInfo, subcommands);
					desc.append("; Upgraded primary data to " + upgradeVersion);
				} else if(upgradeLocal && hasPrimary) {
					// restart primary
					ExecuteTask.executeCommands(deployTaskInfo, "sudo su", "su - postgres", "/usr/pgsql-latest/bin/pg_ctl -w -D /opt/USAT/postgres/data stop -m fast", "/usr/pgsql-latest/bin/pg_ctl -w -D /opt/USAT/postgres/data start");
					desc.append("; Restarted postgres");
				} else if(upgradeOther && !hasPrimary) {
					// restart primary
					ExecuteTask.executeCommands(otherDeployTaskInfo, "sudo su", "su - postgres", "/usr/pgsql-latest/bin/pg_ctl -w -D /opt/USAT/postgres/data stop -m fast", "/usr/pgsql-latest/bin/pg_ctl -w -D /opt/USAT/postgres/data start");
					desc.append("; Restarted postgres");
				}
				log.info("upgrade primary data done");
				String line= deployTaskInfo.interaction.readLine("Please switch applications back to the primary postgres first. Proceed to upgrade standby(yes, no)? [no]?");
				desc.append("; Proceed to upgrade stadby receives: " + line);
				log.info("Proceed to upgrade stadby receives:" + line);
				if(notifyUser!=null){
					ExecuteTask.executeCommands(deployTaskInfo, "sudo su - postgres", 
						"echo \"[`date`] Proceed to upgrade stadby\"| mail -s \""+"Proceed to upgrade stadby receives:" + line+"\" \""+notifyUser+"\"",
						"exit");
				}
				if(line!=null&&line.toLowerCase().equals("yes")){
					// upgrade binaries on standby server
					if(upgradeLocal && !hasPrimary) {
						List<DeployTask> tasks = new ArrayList<>();
						addInstallPgMajor95BinariesTasks(host, upgradeVersion, workDir, tasks);
						for(DeployTask task : tasks)
							task.perform(deployTaskInfo);
						desc.append("; Upgraded Standby binaries to " + upgradeVersion);
					} else if(upgradeOther && hasPrimary) {
						ExecuteTask.executeCommands(otherDeployTaskInfo, "if [ ! -d " + workDir + " ]; then mkdir -p " + workDir + "; fi");
						List<DeployTask> tasks = new ArrayList<>();
						addInstallPgMajor95BinariesTasks(other, upgradeVersion, workDir, tasks);
						for(DeployTask task : tasks)
							task.perform(otherDeployTaskInfo);
						desc.append("; Upgraded Standby binaries to " + upgradeVersion);
					}
	
					// New conf file
					new FromCvsUploadTask("server_app_config/PGS/postgresql_standby.conf", "HEAD", "/opt/USAT/postgres_stdby/data/postgresql.conf", 0600, "postgres", "postgres", true).perform(hasStandby ? deployTaskInfo : otherDeployTaskInfo);
	
					// re-init standby
					results = ExecuteTask.executeCommands(hasStandby ? deployTaskInfo : otherDeployTaskInfo, 
							"sudo su", 
							//"if [ `grep /opt/USAT /etc/fstab | sed -r 's/ +/ /g' | cut -d' ' -f4 | grep -c acl` -eq 0 ]; then sed -ri 's#(\\S+\\s+/opt/USAT\\s+\\S+\\s+\\S+(,\\S+)*)(\\s+)#\\1,acl\\3#' /etc/fstab; mount -o remount /opt/USAT; fi", 
							"su - replica",
							"if [ ! -r ~/.ssh/id_rsa ]; then ssh-keygen -f ~/.ssh/id_rsa -q -P \"\"; fi", 
							"cat ~/.ssh/id_rsa.pub");
	
					ExecuteTask.executeCommands(hasPrimary ? deployTaskInfo : otherDeployTaskInfo, 
							"sudo su",
							"if [ ! -f /home/replica/.ssh/authorized_keys ] || [ `grep -c 'replica@" + (hasPrimary ? other : deployTaskInfo.host).getFullName() + "' /home/replica/.ssh/authorized_keys` -eq 0 ]; then mkdir /home/replica/.ssh; chmod 0700 /home/replica/.ssh; echo '" + results[results.length - 1] + "' >> /home/replica/.ssh/authorized_keys; chown -R replica:replica /home/replica/.ssh; fi",
							//"if [ `grep /opt/USAT /etc/fstab | sed -r 's/ +/ /g' | cut -d' ' -f4 | grep -c acl` -eq 0 ]; then sed -ri 's#(\\S+\\s+/opt/USAT\\s+\\S+\\s+\\S+(,\\S+)*)(\\s+)#\\1,acl\\3#' /etc/fstab; mount -o remount /opt/USAT; fi", 
							"setfacl -RL -m d:u:replica:rX,u:replica:rX /opt/USAT/postgres/data /opt/USAT/postgres/tblspace /opt/USAT/postgres/T2tblspace /opt/USAT/postgres/T3tblspace");
	
					String rsyncCmd = "rsync -e 'ssh -i /home/replica/.ssh/id_rsa' -avzH --delete --include '/data' --include '/*tblspace' --include '/data/*/' --include '/data/*/**' --include '/*tblspace/**' --include '/data/PG_VERSION' --include '/data/backup_label' --exclude '/*' --exclude '/data/*' --exclude 'lost+found' replica@" + (hasPrimary ? deployTaskInfo.host : other).getSimpleName()
							+ ":/ /stdby/ 2>&1 | tee /var/log/deploymgr.log";
					String rsyncCmdFinal = "rsync --checksum -e 'ssh -i /home/replica/.ssh/id_rsa' -avzH --delete --include '/data' --include '/*tblspace' --include '/data/*/' --include '/data/*/**' --include '/*tblspace/**' --include '/data/PG_VERSION' --include '/data/backup_label' --exclude '/*' --exclude '/data/*' --exclude 'lost+found' replica@"
							+ (hasPrimary ? deployTaskInfo.host : other).getSimpleName()
							+ ":/ /stdby/ 2>&1 | tee /var/log/deploymgr.log";
	
					ExecuteTask.executeCommands(hasStandby ? deployTaskInfo.host : other, deployTaskInfo.passwordCache, deployTaskInfo.interaction, hasStandby ? deployTaskInfo.getSsh() : otherDeployTaskInfo.getSsh(), 5000, null, 
							"sudo su",
							"su - postgres",
							"if [ -f /opt/USAT/postgres_stdby/data/postmaster.pid ]; then /usr/pgsql-latest/bin/pg_ctl -w -D /opt/USAT/postgres_stdby/data stop -m fast; fi",
							"exit",
							/* "if [ `ls -1 /opt/USAT/" + name + "/data/pg_xlog | grep -c '.'` -gt 0 ]; then /bin/rm -r /opt/USAT/" + name + "/data/pg_xlog/*; fi" ,*/
							rsyncCmd);
					
					ExecuteTask.executeCommands(hasPrimary ? deployTaskInfo : otherDeployTaskInfo, 
							"sudo su",
							"su - postgres", 
							"if [ ! -s /opt/USAT/postgres/data/backup_label ]; then /usr/pgsql-latest/bin/psql -c \"SELECT pg_start_backup('replica', true);\"; fi", 
							"exit",
							"setfacl -RL -m d:u:replica:rX,u:replica:rX /opt/USAT/postgres/data /opt/USAT/postgres/tblspace /opt/USAT/postgres/T2tblspace /opt/USAT/postgres/T3tblspace");
					if(notifyUser!=null){
						ExecuteTask.executeCommands(deployTaskInfo, "sudo su - postgres", 
							"echo \"[`date`] stadby pg_start_backup done rsync next\"| mail -s \"stadby pg_start_backup done rsync next\" \""+notifyUser+"\"",
							"exit");
					}
					ExecuteTask.executeCommands(hasStandby ? deployTaskInfo.host : other, deployTaskInfo.passwordCache, deployTaskInfo.interaction, hasStandby ? deployTaskInfo.getSsh() : otherDeployTaskInfo.getSsh(), 5000, null,
							"sudo su", 
							rsyncCmd, 
							rsyncCmdFinal, 
							"chmod 0700 /opt/USAT/postgres_stdby/data", 
							"chmod 0600 /opt/USAT/postgres_stdby/data/server.key", 
							"find /stdby/ -type l -printf 'ln -s -f %l %p\n' | sed 's#/opt/USAT/postgres/#/opt/USAT/postgres_stdby/#g' | bash", // fix symbolic links
							"su - postgres",
							"/usr/pgsql-latest/bin/pg_ctl -w -t 3600 -D /opt/USAT/postgres_stdby/data start");
					if(notifyUser!=null){
						ExecuteTask.executeCommands(deployTaskInfo, "sudo su - postgres", 
							"echo \"[`date`] stadby rsync done pg_stop_backup next\"| mail -s \"stadby rsync done pg_stop_backup next\" \""+notifyUser+"\"",
							"exit");
					}
					ExecuteTask.executeCommands(hasPrimary ? deployTaskInfo : otherDeployTaskInfo, 
							"sudo su",
							"su - postgres", 
							"/usr/pgsql-latest/bin/psql -c \"SELECT pg_stop_backup();\"", 
							"chmod 0700 /opt/USAT/postgres/data", 
							"chmod 0600 /opt/USAT/postgres/data/server.key");
					if(notifyUser!=null){
						ExecuteTask.executeCommands(deployTaskInfo, "sudo su - postgres", 
							"echo \"[`date`] stadby pg_stop_backup done\"| mail -s \"stadby pg_stop_backup done\" \""+notifyUser+"\"",
							"exit");
					}
					desc.append("; Reinitialized standby on " + (hasStandby ? deployTaskInfo.host : other).getSimpleName() + " from " + (hasPrimary ? other : deployTaskInfo.host).getSimpleName());
					// ensure replication
				}

				// unfreeze cluster
				ExecuteTask.executeCommands(deployTaskInfo, "sudo su", "clusvcadm -U primary", "clusvcadm -U stdby");

				new FromCvsUploadTask("server_app_config/PGS/pgs_cal_replication_lag.sh", "HEAD", "/opt/USAT/postgres/bin/pgs_cal_replication_lag.sh", 0755, "postgres", "postgres", true).perform(hasPrimary ? deployTaskInfo : otherDeployTaskInfo);

				return desc.substring(2);
			}
		};
		return new DeployTask[] { pgUpgradeTask };
	}

	@Override
	public String getName() {
		return "Postgres Upgrade Major for PGS";
	}

	@Override
	public String toString() {
		return getName();
	}
}
