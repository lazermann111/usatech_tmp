package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.layers.common.constants.FileType;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R52_Patch_4_ChangeSet extends MultiLinuxPatchChangeSet {
	public R52_Patch_4_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/LayersCommon/src", FileType.class, "REL_applayer_1_38_4", USATRegistry.APP_LAYER);
		registerSource("ServerLayers/LayersCommon/src", FileType.class, "REL_netlayer_1_38_4", USATRegistry.NET_LAYER);
		registerSource("ServerLayers/LayersCommon/src", FileType.class, "REL_dms_1_14_4", USATRegistry.DMS_APP);
	}
	
	@Override
	public String getName() {
		return "R52 Patch 4";
	}
}
