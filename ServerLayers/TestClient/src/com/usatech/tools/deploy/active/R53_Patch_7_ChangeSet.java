package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R53_Patch_7_ChangeSet extends MultiLinuxPatchChangeSet {
	public R53_Patch_7_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/AppLayer/src/LoaderService.properties", "classes", "REL_applayer_1_39_7", true, USATRegistry.LOADER);
		registerResource("ServerLayers/POSMLayer/conf/POSMLayerService.properties", "classes", "REL_posmlayer_1_30_7", true, USATRegistry.POSM_LAYER);
	}
	
	@Override
	public String getName() {
		return "R53 Patch 7";
	}
}
