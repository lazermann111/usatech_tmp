package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.CvsPullTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.DeployTaskInfo;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.UploadCoteTask;

public class R50PrepaidCote_ChangeSet implements ChangeSet {
	public R50PrepaidCote_ChangeSet() {
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		return getTasks(new DeployTaskInfo(null, host, null, cache, null));
	}

	public DeployTask[] getTasks(DeployTaskInfo deployTaskInfo) throws IOException {
		List<DeployTask> tasks = new ArrayList<DeployTask>();
		if(deployTaskInfo.host.isServerType("ODB")) {
			String subdomain;
			if("USA".equalsIgnoreCase(deployTaskInfo.host.getServerEnv()))
				subdomain = "getmore.usatech.com";
			else if("LOCAL".equalsIgnoreCase(deployTaskInfo.host.getServerEnv()))
				subdomain = "localhost"; // java.net.Inet4Address.getLocalHost().getCanonicalHostName();
			else
				subdomain = "getmore-" + deployTaskInfo.host.getServerEnv().toLowerCase() + ".usatech.com";
			String brandingDir = "pepi";
			tasks.add(new UploadCoteTask("email_logo", "image/png", subdomain, new CvsPullTask("Prepaid/branded/default/images/more-logo.png", "HEAD")));
			tasks.add(new UploadCoteTask("email_logo", "image/png", subdomain+ "/" + brandingDir, new CvsPullTask("Prepaid/branded/pepi/images/pepi-email-logo.png", "HEAD")));
		}
		return tasks.toArray(new DeployTask[tasks.size()]);
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("ODB");
	}

	@Override
	public String getName() {
		return "R50 Prepaid Cote update";
	}

	@Override
	public String toString() {
		return getName();
	}
}
