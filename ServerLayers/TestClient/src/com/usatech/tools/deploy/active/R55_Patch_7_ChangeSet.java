package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.layers.common.model.PayrollSchedule;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R55_Patch_7_ChangeSet extends MultiLinuxPatchChangeSet {
	public R55_Patch_7_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/LayersCommon/src", PayrollSchedule.class, "REL_report_generator_3_29_7", USATRegistry.RPTREQ_LAYER);
	}
	
	@Override
	public String getName() {
		return "R55 Patch 7 - Payroll Deduct Timed Reports";
	}
}
