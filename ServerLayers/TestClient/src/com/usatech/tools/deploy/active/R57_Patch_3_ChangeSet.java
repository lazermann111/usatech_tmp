package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R57_Patch_3_ChangeSet extends MultiLinuxPatchChangeSet {
	public R57_Patch_3_ChangeSet() throws UnknownHostException {
		super();
		registerResource("DMS/resources/jsp/devices/configuration/editConfig.jsp", "web/jsp/devices/configuration", "REL_R57_3", false, USATRegistry.DMS_APP);
		
		registerResource("Prepaid/src/prepaid-data-layer.xml", "classes", "REL_R57_3", false, USATRegistry.PREPAID_APP);
	}
	
	@Override
	public String getName() {
		return "R57 Patch 3 - Device setting. Campaign coupon.";
	}
}
