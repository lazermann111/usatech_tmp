package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R49_Patch_10_ChangeSet extends MultiLinuxPatchChangeSet {
	public R49_Patch_10_ChangeSet() throws UnknownHostException {
		super();
		registerResource("usalive/web/rma_create_device.html.jsp", "web", "REL_USALive_1_25_10", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/rma_att.html.jsp", "web", "REL_USALive_1_25_10", false, USATRegistry.USALIVE_APP);
	}

	@Override
	public String getName() {
		return "R49 Patch 10";
	}
}
