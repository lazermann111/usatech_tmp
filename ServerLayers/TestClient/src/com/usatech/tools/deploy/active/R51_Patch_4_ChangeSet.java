package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import org.apache.axis.transport.http.USATCommonsHTTPSender;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R51_Patch_4_ChangeSet extends MultiLinuxPatchChangeSet {
	public R51_Patch_4_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ReportGenerator/src/client-config.wsdd", "classes/", "REL_report_generator_3_25_4", true, USATRegistry.TRANSPORT_LAYER);
		registerSource("ServerLayers/LayersCommon/src", USATCommonsHTTPSender.class, "REL_report_generator_3_25_4", USATRegistry.TRANSPORT_LAYER);
	}
	
	@Override
	public String getName() {
		return "R51 Patch 4";
	}
}
