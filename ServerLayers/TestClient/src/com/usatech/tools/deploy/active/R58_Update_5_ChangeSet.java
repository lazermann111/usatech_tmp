package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

import simple.servlet.JspRequestDispatcherFactory;

public class R58_Update_5_ChangeSet extends MultiLinuxPatchChangeSet {
	public R58_Update_5_ChangeSet() throws UnknownHostException {
		super();
		registerSource("Simple1.5/src", JspRequestDispatcherFactory.class, "REL_R58_5", USATRegistry.RPTGEN_LAYER, USATRegistry.USALIVE_APP);
	}
	
	@Override
	public String getName() {
		return "R58 Update 5 - Email receipts";
	}
}
