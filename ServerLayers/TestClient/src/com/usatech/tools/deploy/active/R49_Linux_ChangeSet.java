package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.AppSettingUpdateFileTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.tools.deploy.UploadAndRunPostgresSQLTask;
import com.usatech.tools.deploy.UsatSettingUpdateFileTask;

public class R49_Linux_ChangeSet extends AbstractLinuxChangeSet {
	public R49_Linux_ChangeSet() throws UnknownHostException {
		super();
	}

	@Override
	protected void registerApps() {
		registerApp(USATRegistry.KEYMGR, "2.17.0");
		registerApp(USATRegistry.LOADER, "1.35.0");
		registerApp(USATRegistry.APP_LAYER, "1.35.0");
		registerApp(USATRegistry.INAUTH_LAYER, "1.35.0");
		registerApp(USATRegistry.POSM_LAYER, "1.26.0");
		registerApp(USATRegistry.NET_LAYER, "1.35.0");
		registerApp(USATRegistry.OUTAUTH_LAYER, "1.35.0");
		registerApp(USATRegistry.DMS_APP, "1.11.0");
		registerApp(USATRegistry.HTTPD_NET, null);
		registry.registerSubApps(USATRegistry.HTTPD_NET, USATRegistry.DMS_APP);
		registerApp(USATRegistry.RPTGEN_LAYER, "3.23.0");
		registerApp(USATRegistry.RPTREQ_LAYER, "3.23.0");
		registerApp(USATRegistry.USALIVE_APP, "1.25.0");
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.USALIVE_APP);
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.VERIZON_APP);
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.HOTCHOICE_APP);
		registerApp(USATRegistry.PREPAID_APP, "1.13.0");
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.GETMORE_APP); // The APP server needs PREPAID_APP, but the WEB server also needs the GETMORE_APP as subapp
		// registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.PREPAID_APP);
		registerApp(USATRegistry.HTTPD_WEB, null);
		registerApp(USATRegistry.TRANSPORT_LAYER, "3.23.0");
		// registerApp(USATRegistry.POSTGRES_KLS);
		// registerApp(USATRegistry.POSTGRES_MST);
		// registerApp(USATRegistry.POSTGRES_MSR);
		// registerApp(USATRegistry.POSTGRES_PGS);
	}

	@Override
	public boolean isApplicable(Host host) {
		return super.isApplicable(host)||host.isServerType("MST");
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(host.isServerType("MST")) {
			String[] instances = registry.getServerSet(host.getServerEnv()).getServer(host, "MST").instances;
			Map<Integer, String[]> dbInst = new HashMap<>();
			if("LOCAL".equalsIgnoreCase(host.getServerEnv()) && instances != null) {
				Set<String> dbNames = new LinkedHashSet<>();
				for(int i = 0; i < instances.length; i++) {
					if(instances[i] == null || instances[i].length() < 2)
						continue;
					switch(instances[i].substring(0, 2)) {
						case "dk":
							dbNames.add("app_1");
							break;
						case "bk":
							dbNames.add("app_2");
							break;
						case "yh":
							dbNames.add("app_3");
							break;
						case "js":
							dbNames.add("app_4");
							break;
						case "pc":
							dbNames.add("app_5");
							break;
						case "ml":
							dbNames.add("app_6");
							break;
					}
				}
				dbInst.put(null, dbNames.toArray(new String[dbNames.size()]));
			} else {
				int count = instances == null ? 1 : instances.length;
				int n = (host.getSimpleName().charAt(host.getSimpleName().length() - 1) - '0') - 1;
				for(int i = 0; i < count; i++) {
					String appdb = "app_" + String.valueOf(n * count + i + 1);
					Integer ordinal = (instances == null || instances[0] == null ? null : i + 1);
					dbInst.put(ordinal, new String[] { appdb });
				}
			}
			for(Map.Entry<Integer, String[]> entry : dbInst.entrySet()) {
				tasks.add(new UploadAndRunPostgresSQLTask("DatabaseScripts/releases/REL_Edge_Update_R49/R49.APP.sql", "HEAD", Boolean.FALSE, entry.getKey(), entry.getValue()));
				tasks.add(new UploadAndRunPostgresSQLTask("DatabaseScripts/releases/REL_Edge_Update_R49/APP_CLUSTER.BIN_RANGE_UPDATE.sql", "HEAD", Boolean.FALSE, entry.getKey(), entry.getValue()));
			}
		} else {
			//USATHelper.installJava(host, cache, tasks, 8, 60, getWorkDir());
			addPrepareHostTasks(host, tasks, cache);
			tasks.add(new UsatSettingUpdateFileTask(registry.getServerSet(host.getServerEnv())));
			tasks.add(new ExecuteTask("sudo find /opt/USAT/ -name '*.dmp' -delete"));
		}
	}
	
	@Override
	protected void addTasks(Host host, final App app, Integer ordinal, String instance, final int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache);
		if(app.getInstallFileName() != null)
			tasks.add(new AppSettingUpdateFileTask(app, ordinal, instanceNum, instanceCount, cache, registry.getServerSet(host.getServerEnv()), false));
		addInstallAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache, commands);
	}

	@Override
	public String getName() {
		return "Edge Server - R49 Install - Apps";
	}
}
