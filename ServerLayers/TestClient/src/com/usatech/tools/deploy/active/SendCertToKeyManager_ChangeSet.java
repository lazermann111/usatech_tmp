package com.usatech.tools.deploy.active;

import static com.usatech.tools.deploy.USATRegistry.baseDir;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.HttpsWithJKSCertTask;
import com.usatech.tools.deploy.PasswordCache;

public class SendCertToKeyManager_ChangeSet implements ChangeSet {
	protected static final File setenvFile = new File(baseDir, "LayersCommon/conf/setenv-gctest.sh");
	@Override
	public String getName() {
		return "Send Certificates to Key Manager";
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		List<DeployTask> tasks = new ArrayList<DeployTask>(1);
		String[] urls;
		if("DEV".equalsIgnoreCase(host.getServerEnv())) {
			urls = new String[] { "https://usadev01.usatech.com/KeyManager/soapservice/KeyManager" };
		} else if("INT".equalsIgnoreCase(host.getServerEnv())) {
			urls = new String[] { "https://ecckls01.usatech.com/KeyManager/soapservice/KeyManager" };
		} else if("ECC".equalsIgnoreCase(host.getServerEnv())) {
			urls = new String[] { "https://ecckls01.usatech.com/KeyManager/soapservice/KeyManager" };
		} else if("USA".equalsIgnoreCase(host.getServerEnv())) {
			urls = new String[] { "https://usakls21.trooper.usatech.com/KeyManager/soapservice/KeyManager", "https://usakls22.trooper.usatech.com/KeyManager/soapservice/KeyManager" };
		} else {
			return new DeployTask[0];
		}

		tasks.add(new HttpsWithJKSCertTask(urls, "applayer.keymanager", "/opt/USAT/conf/keystore.ks", cache));
		tasks.add(new HttpsWithJKSCertTask(urls, "posmlayer.keymanager", "/opt/USAT/conf/keystore.ks", cache));
		return tasks.toArray(new DeployTask[tasks.size()]);
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("APR");
	}
	@Override
	public String toString() {
		return getName();
	}
}
