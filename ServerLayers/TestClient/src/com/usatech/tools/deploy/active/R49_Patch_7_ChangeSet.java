package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R49_Patch_7_ChangeSet extends MultiLinuxPatchChangeSet {
	public R49_Patch_7_ChangeSet() throws UnknownHostException {
		super();
		registerResource("usalive/src/com/usatech/usalive/report/BuildReportPillars.properties", "classes/com/usatech/usalive/report", "REL_USALive_1_25_7", false, USATRegistry.USALIVE_APP);
	}

	@Override
	public String getName() {
		return "R49 Patch 7";
	}
}
