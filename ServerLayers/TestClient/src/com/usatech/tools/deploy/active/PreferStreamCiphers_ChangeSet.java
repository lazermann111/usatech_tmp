package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;

public class PreferStreamCiphers_ChangeSet implements ChangeSet {
	protected static final DateFormat df = new SimpleDateFormat("yyyyMMdd");
	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("WEB");
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		return new DeployTask[] { new ExecuteTask("sudo su",
				"find /opt/USAT/httpd/conf.d/ -name \"*.conf\" | xargs grep -l 'SSLCipherSuite HIGH:!aNULL:!kEDH:+SHA1:+MD5:+HIGH' | xargs sed -i.bak" + df.format(new Date())
					+ " 's/SSLCipherSuite HIGH:!aNULL:!kEDH:+SHA1:+MD5:+HIGH/SSLCipherSuite ECDHE-RSA-AES128-SHA256:AES128-GCM-SHA256:RC4:HIGH:!aNULL:!kEDH:!EXP:+SHA1:+MD5\\\n\tSSLHonorCipherOrder on/g'", 
				"if [ $? -eq 0 ]; then /usr/local/apache2/bin/httpd -f /opt/USAT/httpd/conf/httpd.conf -k restart; fi") };
	}

	@Override
	public String getName() {
		return "Prefer Stream Ciphers";
	}

	@Override
	public String toString() {
		return getName();
	}
}
