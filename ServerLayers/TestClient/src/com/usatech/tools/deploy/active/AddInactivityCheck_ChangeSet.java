package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATHelper;
import com.usatech.tools.deploy.USATRegistry;

public class AddInactivityCheck_ChangeSet extends AbstractLinuxChangeSet {
	public AddInactivityCheck_ChangeSet() throws UnknownHostException {
		super();
	}

	@Override
	protected void registerApps() {
		registerApp(USATRegistry.LOADER);
		registerApp(USATRegistry.APP_LAYER);
		registerApp(USATRegistry.INAUTH_LAYER);
		registerApp(USATRegistry.POSM_LAYER);
		registerApp(USATRegistry.NET_LAYER);
		registerApp(USATRegistry.OUTAUTH_LAYER);
		registerApp(USATRegistry.KEYMGR);
		registerApp(USATRegistry.RPTGEN_LAYER);
		registerApp(USATRegistry.RPTREQ_LAYER);
		registerApp(USATRegistry.TRANSPORT_LAYER);
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		USATHelper.addMonitInstallTasks(tasks, host, "/home/`logname`");
	}
	
	@Override
	protected void addTasks(Host host, final App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		USATHelper.uploadAppMonitConf(host, tasks, app, ordinal);
	}

	@Override
	public String getName() {
		return "Add Inactivity Check";
	}
}
