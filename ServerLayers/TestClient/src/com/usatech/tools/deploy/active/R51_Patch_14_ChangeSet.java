package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.layers.common.process.ProcessRequestTask;
import com.usatech.layers.common.process.Processor;
import com.usatech.posm.process.MassEftAdjustment;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R51_Patch_14_ChangeSet extends MultiLinuxPatchChangeSet {
	public R51_Patch_14_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/LayersCommon/src/common-data-layer.xml", "classes", "REL_DMS_1_13_14", false, USATRegistry.DMS_APP);
		registerResource("ServerLayers/LayersCommon/src/common-data-layer.xml", "classes", "REL_posmlayer_1_28_14", false, USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/LayersCommon/src", Processor.class, "REL_posmlayer_1_28_14", USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/LayersCommon/src", ProcessRequestTask.class, "REL_posmlayer_1_28_14", USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/POSMLayer/src/main", MassEftAdjustment.class, "REL_posmlayer_1_28_14", USATRegistry.POSM_LAYER);
	}
	
	@Override
	public String getName() {
		return "R51 Patch 14";
	}
}
