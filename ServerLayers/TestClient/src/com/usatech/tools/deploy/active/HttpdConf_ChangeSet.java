package com.usatech.tools.deploy.active;

import static com.usatech.tools.deploy.USATRegistry.subHostPrefixMap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.FromCvsUploadTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.LoopbackInterfaceMap;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;

import simple.io.EncodingInputStream;
import simple.io.ReplacementsLineFilteringReader;

public class HttpdConf_ChangeSet implements ChangeSet {
	/*protected static App[] APPS_TO_UPDATE = new App[] { USATRegistry.ESUDS_APP,
			USATRegistry.GETMORE_APP, USATRegistry.PREPAID_APP, 
			USATRegistry.USALIVE_APP, USATRegistry.HOTCHOICE_APP, USATRegistry.VERIZON_APP,
			USATRegistry.DMS_APP};*/
	
	protected static App[] APPS_TO_UPDATE = new App[] { USATRegistry.GETMORE_APP, USATRegistry.PREPAID_APP };

	@Override
	public String getName() {
		return "Getmore Httpd Conf Update";
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("WEB")||host.isServerType("NET");
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		List<DeployTask> tasks = new ArrayList<DeployTask>();
		//backup in case
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US);
		System.out.println(sdf.format(new Date()));
		String runUseName=System.getProperty("user.name");
		String folderName="httpBackup"+sdf.format(new Date());
		String backupFolderPath="/home/"+runUseName+"/"+folderName+"/";
		ExecuteTask backupTask=new ExecuteTask("if [ ! -d "+backupFolderPath+" ]; then mkdir "+backupFolderPath+"; fi",
				"sudo /bin/cp -pr /opt/USAT/httpd/conf.d "+backupFolderPath
				);
		tasks.add(backupTask);
		App app;
		if(host.isServerType("WEB")){
			app = USATRegistry.HTTPD_WEB;
		}else{
			app = USATRegistry.HTTPD_NET;
		}
		String appDir = "/opt/USAT/" + app.getName();
		// ----------
		String last2 = host.getSimpleName().substring(6);
		String appServer;
		if("NET".equalsIgnoreCase(app.getServerType()))
			appServer = "apr";
		else if("WEB".equalsIgnoreCase(app.getServerType())) {
			if("USA".equalsIgnoreCase(host.getServerEnv())) {
				appServer = "app";
			} else {
				appServer = "apr";
			}
		} else
			throw new IOException("Don't know app server for httpd on '" + app.getServerType() + "'");
		String lb_ip_escaped;
		if("DEV".equalsIgnoreCase(host.getServerEnv()))
			lb_ip_escaped = "10\\\\.0\\\\.0\\\\.64";
		else if("INT".equalsIgnoreCase(host.getServerEnv()))
			lb_ip_escaped = "10\\\\.0\\\\.0\\\\.64";
		else if("ECC".equalsIgnoreCase(host.getServerEnv()))
			lb_ip_escaped = "192\\\\.168\\\\.4\\\\.65";
		else if("USA".equalsIgnoreCase(host.getServerEnv()))
			lb_ip_escaped = "192\\\\.168\\\\.79\\\\.(?:18[34]|19[56])";
		else
			lb_ip_escaped = "";

		int instanceNum = Integer.parseInt(last2.substring(1));
		for(App subApp : APPS_TO_UPDATE) {
			// install sub app portion
			final Map<Pattern, String> replacements = new HashMap<Pattern, String>();
			replacements.put(Pattern.compile("<APP_SERVER_NAME>"), host.getServerEnv().toLowerCase() + appServer + last2);
			replacements.put(Pattern.compile("<WORKER_NAME>"), "worker_" + instanceNum);
			String subHostPrefix = subHostPrefixMap.get(subApp.getName());
			if(subHostPrefix == null) {
				subHostPrefix = subApp.getName();
			}
			String subhostname = subHostPrefix + ("USA".equalsIgnoreCase(host.getServerEnv()) ? ".usatech.com" : "-" + host.getServerEnv().toLowerCase() + ".usatech.com");
			String subhostip = LoopbackInterfaceMap.getHostRealIp(host.getServerEnv(), subhostname);
			replacements.put(Pattern.compile("<NON_PROD_COMMENT>"), ("USA".equalsIgnoreCase(host.getServerEnv()) ? "" : "#"));
			replacements.put(Pattern.compile("<ENV_SUFFIX>"), ("USA".equalsIgnoreCase(host.getServerEnv()) ? "" : "-" + host.getServerEnv().toLowerCase()));
			replacements.put(Pattern.compile("<WEB_SERVER_NAME>"), subhostname);
			replacements.put(Pattern.compile("<WEB_SERVER_IP>"), subhostip);
			replacements.put(Pattern.compile("<WEB_SERVER_IP_ESCAPED>"), subhostip.replace(".", "\\\\."));
			replacements.put(Pattern.compile("<LOADBALANCER_IP_ESCAPED>"), lb_ip_escaped);

			tasks.add(new FromCvsUploadTask("server_app_config/WEB/Httpd/conf.d/" + subApp.getName() + ".conf", "HEAD", appDir + "/conf.d/" + subApp.getName() + ".conf", 0644, app.getUserName(), app.getUserName(), true) {
				@Override
				protected InputStream decorate(InputStream in) {
					return new EncodingInputStream(new ReplacementsLineFilteringReader(new BufferedReader(new InputStreamReader(in)), "\n", replacements));
				}
			});
		}
		//final String httpdRoot = "/usr/local/apache2";
		//tasks.add(new ExecuteTask("sudo " + httpdRoot + "/bin/httpd  -f " + appDir + "/conf/httpd.conf -k restart"));
		tasks.add(new ExecuteTask("sudo monit restart httpd"));
		return tasks.toArray(new DeployTask[tasks.size()]);
	}

	@Override
	public String toString() {
		return getName();
	}
}
