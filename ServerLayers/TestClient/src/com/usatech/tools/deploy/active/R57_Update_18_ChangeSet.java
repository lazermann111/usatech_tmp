package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.applayer.InboundFileTransferTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R57_Update_18_ChangeSet extends MultiLinuxPatchChangeSet {
	public R57_Update_18_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/AppLayer/src", InboundFileTransferTask.class, "REL_R57_18", USATRegistry.LOADER);
	}
	
	@Override
	public String getName() {
		return "R57 Update 18 - Database performance - inbound files";
	}
}
