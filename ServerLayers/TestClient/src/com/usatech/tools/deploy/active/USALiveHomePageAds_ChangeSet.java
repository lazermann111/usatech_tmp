package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class USALiveHomePageAds_ChangeSet extends MultiLinuxPatchChangeSet {
	public USALiveHomePageAds_ChangeSet() throws UnknownHostException {
		super();
		registerResource("usalive/web/images/more-applepay2.png", "web/images", "REL_USALive_1_30_0", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/login.html.jsp", "web/", "REL_USALive_1_30_0", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/customerreporting_web/home.jsp", "web/", "REL_USALive_1_30_0", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/customerreporting_web/login.jsp", "web/", "REL_USALive_1_30_0", false, USATRegistry.USALIVE_APP);
	}
	
	@Override
	public String getName() {
		return "Usalive HomePage Ads 02012017";
	}
}
