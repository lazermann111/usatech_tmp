package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;

public class R48_Patch_4_ChangeSet extends MultiLinuxPatchChangeSet {
	public R48_Patch_4_ChangeSet() throws UnknownHostException {
		super();
		//registerSource("usalive/src/", MassDeviceUpdateStep.class, "REL_USALive_1_24_4", USATRegistry.USALIVE_APP);
	}
	
	@Override
	public String getName() {
		return "R48 Patch 4";
	}
}
