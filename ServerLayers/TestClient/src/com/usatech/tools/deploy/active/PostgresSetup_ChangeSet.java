package com.usatech.tools.deploy.active;

import static com.usatech.tools.deploy.USATRegistry.buildsDir;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.StringReader;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Pattern;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.ChooseTask;
import com.usatech.tools.deploy.ConcatenatingContent;
import com.usatech.tools.deploy.CronUpdateTask;
import com.usatech.tools.deploy.CvsPullTask;
import com.usatech.tools.deploy.DefaultHostSpecificValue;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.DeployTaskInfo;
import com.usatech.tools.deploy.DeployUtils;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.FromCvsUploadTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.ImportJKSCertTask;
import com.usatech.tools.deploy.OptionalTask;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.PgDeployMap;
import com.usatech.tools.deploy.PgDeployMap.PGDATADir;
import com.usatech.tools.deploy.PostgresSetupTask;
import com.usatech.tools.deploy.PropertiesUpdateFileTask;
import com.usatech.tools.deploy.RenewJKSCertTask;
import com.usatech.tools.deploy.RotateMessageChainEncryptionKeyTask;
import com.usatech.tools.deploy.ServerSet;
import com.usatech.tools.deploy.USATHelper;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.tools.deploy.UnixUploadTask;
import com.usatech.tools.deploy.UsatIPTablesUpdateTask;
import com.usatech.tools.deploy.YumOutputFilter;

import simple.io.BufferStream;
import simple.io.HeapBufferStream;
import simple.text.StringUtils;

public class PostgresSetup_ChangeSet extends AbstractLinuxChangeSet {
	protected final Map<String, String> upgradeVersions;

	public PostgresSetup_ChangeSet() throws UnknownHostException {
		super();
		upgradeVersions = new LinkedHashMap<>();
		upgradeVersions.put("9.2", "9.2.13");
		upgradeVersions.put("9.3", "9.3.10");
		upgradeVersions.put("9.4", "9.4.4");
		upgradeVersions.put(null, "9.4.4");
	}

	@Override
	protected void registerApps() {

		// String version = "9.3.4";
		// String version = "9.2.6";
		String version = null;
		registerApp(USATRegistry.POSTGRES_KLS, version);
		registerApp(USATRegistry.POSTGRES_MST, version);
		registerApp(USATRegistry.POSTGRES_MSR, version);
		registerApp(USATRegistry.POSTGRES_PGS, version);
	}
	
	@Override
	protected void addPrepareHostTasks(Host host, List<DeployTask> tasks, PasswordCache cache) throws IOException {
		// Make group and directory
		List<String> commands = new ArrayList<String>();
		commands.add("sudo su");
		commands.add("chmod 777 /tmp");
		commands.add("grep -c 'usat:' /etc/group || groupadd -g 900 usat");
		commands.add("test -d /opt/USAT || (mkdir -p /opt/USAT && chmod 0755 /opt/USAT)");
		commands.add("test -d /opt/USAT/conf || (mkdir -p /opt/USAT/conf && chown root:root /opt/USAT && chown root:usat /opt/USAT/conf)");
		commands.add("test -d /usr/local/USAT/bin || (mkdir -p /usr/local/USAT/bin && chown root:root /usr/local/USAT && chown root:usat /usr/local/USAT/bin)");
		commands.add("grep -c '@usat .* nproc ' /etc/security/limits.conf || echo '@usat        soft    nproc           10000' >> /etc/security/limits.conf");
		commands.add("grep -c '@usat .* nofile ' /etc/security/limits.conf || echo '@usat        -       nofile          20000' >> /etc/security/limits.conf");
		commands.add("grep -c '@usat .* memlock ' /etc/security/limits.conf || echo '@usat        -       memlock         1024' >> /etc/security/limits.conf");
		commands.add(DeployUtils.getMakeDir("/etc/init/USAT", "root", "root", null));
		/**
		PropertiesUpdateFileTask sysctlUpdateTask = new PropertiesUpdateFileTask(true, 0600, "root", "root");
		sysctlUpdateTask.setFilePath("/etc/sysctl.conf");
		sysctlUpdateTask.registerValue("net.ipv4.tcp_keepalive_time", new DefaultHostSpecificValue(String.valueOf(5)));
		sysctlUpdateTask.registerValue("net.ipv4.tcp_keepalive_intvl", new DefaultHostSpecificValue(String.valueOf(1)));
		sysctlUpdateTask.registerValue("net.ipv4.tcp_keepalive_probes", new DefaultHostSpecificValue(String.valueOf(5)));
		tasks.add(sysctlUpdateTask);
		commands.add("sudo sysctl -e -p");
		*/
		tasks.add(new ExecuteTask(commands.toArray(new String[commands.size()])));
		File confDir = new File(buildsDir.getParentFile(), "LayersCommon/conf");
		if((host.isServerType("KLS") && !"LOCAL".equalsIgnoreCase(host.getServerEnv()))) {
			// Upload jmx.access, jmx.login.config, translations.properties
			tasks.add(new FromCvsUploadTask("ServerLayers/LayersCommon/conf/jmx.login.config_" + host.getServerEnv().toLowerCase(), "HEAD", "/opt/USAT/conf/jmx.login.config", 0640, "root", "usat", true));
			tasks.add(new FromCvsUploadTask("ServerLayers/LayersCommon/conf/logging.properties", "HEAD", "/opt/USAT/conf/logging.properties", 0640, "root", "usat", true));

			BufferStream buffer = new HeapBufferStream();
			PrintStream ps = new PrintStream(buffer.getOutputStream());
			ps.println("monitor readonly");
			String[] users = USATHelper.getJmxUsers(host);
			if(users != null)
				for(String user : users) {
					ps.print(user);
					ps.println(" readwrite");
				}
			ps.flush();
			tasks.add(new UnixUploadTask(buffer.getInputStream(), "/opt/USAT/conf", "jmx.access", 0640, "root", "usat", true, "Access for users " + Arrays.toString(users)));

			// Create keystore.ks and truststore.ts
			tasks.add(new RenewJKSCertTask(new Date(System.currentTimeMillis() + 30 * 24 * 60 * 60 * 1000L), "/opt/USAT/conf/keystore.ks", cache, 0640));
			tasks.add(new ImportJKSCertTask("usatrootca", new File(confDir, "usat_root.crt"), "/opt/USAT/conf/truststore.ts", cache, 0640, false));
			if(!host.getServerEnv().equalsIgnoreCase("USA"))
				tasks.add(new ImportJKSCertTask("usattestrootca", new File(confDir, "usat_test_root.crt"), "/opt/USAT/conf/truststore.ts", cache, 0640, false));
			tasks.add(new RotateMessageChainEncryptionKeyTask(new Date(System.currentTimeMillis() + 180 * 24 * 60 * 60 * 1000L), "/opt/USAT/conf/keystore.ks", "/opt/USAT/conf/truststore.ts", 0640, 0640, registry.getServerSet(host.getServerEnv()), true, new Date(System.currentTimeMillis() - 365 * 24 * 60 * 60 * 1000L)));
		}
		tasks.add(new FromCvsUploadTask("server_app_config/common/archive", "HEAD", "/usr/local/USAT/bin/archive", 0555, "root", "root", true));
		//tasks.add(new UsatIPTablesUpdateTask(registry.getServerSet(host.getServerEnv())));
		 // Do this after IPTablesTask
		tasks.add(new ExecuteTask("sudo su", 
					//"if ! grep -c 'kernel.sem =' /etc/sysctl.conf; then echo 'kernel.sem = 250 256000 32 1024' >> /etc/sysctl.conf; sysctl -e -p; fi",
					"if [ `egrep -c '^postgres:' /etc/passwd` -eq 0 ]; then groupadd -g 26 postgres; useradd -c 'PostgreSQL Server' -d /home/postgres -g 26 -G usat -m -u 26 postgres; elif [ `egrep -c '^usat:x:900:(\\w\\w*,)*postgres' /etc/group` -eq 0 ]; then usermod -G usat postgres; fi",
					DeployUtils.getMakeDir("/home/postgres", "postgres", "postgres", null)));			
		
		// Install additional jarsigner key for KeyManager
		if(host.isServerType("KLS")) {
			tasks.add(new RenewJKSCertTask(new Date(System.currentTimeMillis() + 30 * 24 * 60 * 60 * 1000L), "/opt/USAT/conf/keystore.ks", cache, 0640, new DefaultHostSpecificValue("jarsigner"), new DefaultHostSpecificValue(host.getSimpleName()), "usasubca.usatech.com", "USATCodeSigning", System.getProperty("user.name")));
			// conf/webtruststore.ts
			tasks.add(new ImportJKSCertTask("usattokenca", new File(confDir, "usat_token.crt"), "/opt/USAT/conf/webtruststore.ts", cache, 0640, false));
			tasks.add(new ImportJKSCertTask("usattokenca2", new File(confDir, "usat_token_2.crt"), "/opt/USAT/conf/webtruststore.ts", cache, 0640, false));
		}
		
		if(!host.isServerType("PGS") || host.getServerTypes().size() > 1)
			USATHelper.addMonitInstallTasks(tasks, host, "/home/`logname`");
	}
	
	@Override
	protected void addPrepareAppTasks(final Host host, final App app, final Integer ordinal, int instanceNum, final int instanceCount, List<DeployTask> tasks, PasswordCache cache) throws IOException {
		if(host.isServerType("PGS")){//freeze it first so cluster will not auto execute commands
			tasks.add(new ExecuteTask(
					"sudo su",
					"if [ -x /usr/sbin/clusvcadm ];  then clusvcadm -Z primary; fi",
					"if [ -x /usr/sbin/clusvcadm ];  then clusvcadm -Z stdby; fi"));
		}
		List<String> subcommands = new ArrayList<String>();
		subcommands.add("sudo su");
		if(ordinal == null || ordinal == 1) {
			subcommands.add(DeployUtils.getMakeDir("/home/" + app.getName(), app.getUserName(), app.getUserName(), null));
			subcommands.add("grep -c " + app.getUserName() + " /etc/cron.d/cron.allow || echo '" + app.getUserName() + "' >> /etc/cron.d/cron.allow");
		}
		// Make directories
		final String appDir;
		PGDATADir tmpPgDir;
		if(USATRegistry.POSTGRES_KLS.equals(app) && (tmpPgDir = PgDeployMap.hostPGDATADirMap.get(host.getSimpleName())) != null) {
			appDir = tmpPgDir.getPrimaryDir();
		} else
			appDir = "/opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal);
		subcommands.add(DeployUtils.getMakeDir(appDir, app.getUserName(), app.getUserName(), null));
		if(!subcommands.isEmpty())
			tasks.add(new ExecuteTask(subcommands.toArray(new String[subcommands.size()])));
		subcommands.clear();
		
		if(!host.isServerType("PGS")){
			USATHelper.uploadAppMonitConf(host, tasks, app, ordinal);
		}
		
		CronUpdateTask cronUpdateTask = new CronUpdateTask(app.getUserName(), true);
		subcommands.add("sudo su - postgres");
		subcommands.add("if [ ! -r ~/.bash_profile ]; then echo 'export LD_LIBRARY_PATH=/usr/pgsql-latest/lib/" + (host.isServerType("PGS") ? ":/usr/unixODBC/lib/:/usr/lib/oracle/11.2/client64/lib/" : "") + "' > ~/.bash_profile; chown " + app.getUserName() + ":" + app.getUserName() + " ~/.bash_profile; chmod 0744 ~/.bash_profile; elif [ `grep -c 'LD_LIBRARY_PATH=/usr/pgsql-latest/lib"
				+ (host.isServerType("PGS") ? ":/usr/unixODBC/lib/:/usr/lib/oracle/11.2/client64/lib/" : "") + "' ~/.bash_profile` -eq 0 ]; then echo 'export LD_LIBRARY_PATH=/usr/pgsql-latest/lib/"
				+ (host.isServerType("PGS") ? ":/usr/unixODBC/lib/:/usr/lib/oracle/11.2/client64/lib/" : "") + "' >> ~/.bash_profile; fi");
		subcommands.add("if [ ! -x ~/.bash_profile ]; then chmod u+x ~/.bash_profile; fi");
		subcommands.add("exit");
		subcommands.add("sudo su");
		final String[] pgDirs;
		final String[] pgNames;
		final String[] postgresqlConfs;
		ServerSet serverSet = registry.getServerSet(host.getServerEnv());
		if(USATRegistry.POSTGRES_KLS.equals(app)) {
			if("LOCAL".equalsIgnoreCase(host.getServerEnv())) {
				pgDirs = new String[] { appDir };
				pgNames = new String[] { app.getName() + (ordinal == null ? "" : ordinal) };
				postgresqlConfs = new String[] { "postgresql_primary.conf" };
			} else {
				PGDATADir pgdd = PgDeployMap.hostPGDATADirMap.get(host.getSimpleName());
				pgDirs = new String[] { pgdd.getPrimaryDir(), pgdd.getStandbyDir() };
				postgresqlConfs = new String[] { "postgresql_primary.conf", "postgresql_standby.conf" };
				pgNames = new String[pgDirs.length];
				for(int i = 0; i < pgDirs.length; i++)
					pgNames[i] = pgDirs[i].substring(pgDirs[i].lastIndexOf("/") + 1);
			}
		} else if(USATRegistry.POSTGRES_PGS.equals(app)) {
			pgDirs = new String[] { appDir };
			pgNames = new String[] { app.getName() + (ordinal == null ? "" : ordinal) };
			postgresqlConfs = new String[] { "postgresql_primary.conf" };
		} else {
			pgDirs = new String[] { appDir };
			pgNames = new String[] { app.getName() + (ordinal == null ? "" : ordinal) };
			postgresqlConfs = new String[] { "postgresql.conf" };
		}
		for(int i = 0; i < pgDirs.length; i++) {
			subcommands.add("if [ ! -d " + pgDirs[i] + "/logs ]; then  mkdir " + pgDirs[i] + "/logs; chown postgres:postgres " + pgDirs[i] + "/logs; fi");
			subcommands.add("if [ ! -d " + pgDirs[i] + "/tmp ]; then  mkdir " + pgDirs[i] + "/tmp; chown postgres:postgres " + pgDirs[i] + "/tmp; fi");
			if(host.isServerType("PGS")) {
				subcommands.add("if [ ! -h " + pgDirs[i] + "/tblspace ]; then  ln -s /tblspace " + pgDirs[i] + "/tblspace; chown postgres:postgres " + pgDirs[i] + "/tblspace; chmod 0700 " + pgDirs[i] + "/tblspace; fi");
				subcommands.add("if [ ! -h " + pgDirs[i] + "/data ]; then  ln -s /data " + pgDirs[i] + "/data; chown postgres:postgres " + pgDirs[i] + "/data; chmod 0700 " + pgDirs[i] + "/data; fi");
			} else
				subcommands.add("if [ ! -d " + pgDirs[i] + "/tblspace ]; then  mkdir " + pgDirs[i] + "/tblspace; chown postgres:postgres " + pgDirs[i] + "/tblspace; fi");
			subcommands.add("if [ ! -d " + pgDirs[i] + "/bin ]; then  mkdir " + pgDirs[i] + "/bin; chown postgres:postgres " + pgDirs[i] + "/bin; fi");
		}
		if(!subcommands.isEmpty())
			tasks.add(new ExecuteTask(subcommands.toArray(new String[subcommands.size()])));
		subcommands.clear();
		
		final int port;
		if(ordinal != null)
			port = 5430 + ordinal;
		else
			port = 5432;
		final String pgName = pgNames[0];
		//upgrade if necessary
		String pgRestartCmd;
		if(host.isServerType("PGS")) {
			pgRestartCmd = "/opt/USAT/postgres/bin/postgresql.sh start &";
		} else {
			pgRestartCmd = "/usr/monit-latest/bin/monit restart " + pgName;
		}
		
		final AtomicBoolean reinitStandby = new AtomicBoolean(false);
		if(!StringUtils.isBlank(app.getVersion())) {
			List<DeployTask> subTasks = new ArrayList<DeployTask>();
			String[] versionArray = StringUtils.split(app.getVersion(), ".-/_".toCharArray(), false);
			final String majorVersion = StringUtils.join(versionArray, ".", 0, 2);
			String majorRegex = StringUtils.escape(majorVersion, ".+*!\\".toCharArray(), '\\');
			ChooseTask pgInstallTask = new ChooseTask("/usr/pgsql-latest/bin/pg_config --version | cut -d ' ' -f 2", true);
			tasks.add(pgInstallTask);
			subTasks.add(new ExecuteTask(new YumOutputFilter(), "sudo su",
			// "yum -y install uuid | grep -v '#'",
					"yum -v -y install http://yum.postgresql.org/" + majorVersion + "/redhat/rhel-6-x86_64/pgdg-redhat" + versionArray[0] + versionArray[1] + '-' + majorVersion + "-1.noarch.rpm | grep -v '#'", 
					"yum -v -y --disablerepo=* --enablerepo=pgdg" + versionArray[0] + versionArray[1] + " install postgresql" + versionArray[0] + versionArray[1] + "-server postgresql" + versionArray[0] + versionArray[1] + "-contrib | grep -v '#'", 
					"unlink /usr/pgsql-latest", 
					"ln -s /usr/pgsql-" + majorVersion + " /usr/pgsql-latest"));
			if(host.isServerType("PGS")){
				USATHelper.addOdbcLinkTasks(host, cache, subTasks, getWorkDir());
				subTasks.add(new OptionalTask("if [ ! -x /usr/pgsql-latest/lib/pg_repack.so ]; then echo 'Y'; fi", Pattern.compile("^Y\\s*"), false,
						new FromCvsUploadTask("server_app_config/Postgresql/pg_repack/pg_repack-1.2.0-beta1.zip", "HEAD", "pg_repack-1.2.0-beta1.zip", 0644),
						new ExecuteTask("sudo su",
										//"yum -y install readline*",
										"if [ -d /home/`logname`/pg_repack-1.2.0-beta1 ]; then /bin/rm -r /home/`logname`/pg_repack-1.2.0-beta1; fi",
										"cd /home/`logname`",
										"unzip pg_repack-1.2.0-beta1.zip",
										"cd pg_repack-1.2.0-beta1",
										"export PATH=$PATH:/usr/pgsql-latest/bin",
										"make",
										"make install")
						));
			}
			subcommands.add("sudo su");
			subcommands.add("su - " + app.getUserName());
			if((USATRegistry.POSTGRES_MST.equals(app) || USATRegistry.POSTGRES_MSR.equals(app)) && !"LOCAL".equalsIgnoreCase(host.getServerEnv())) {
				subcommands.add("/usr/pgsql-latest/bin/psql -p " + port + " -d mq -c 'select count(*) from mq.q_;'");
				subcommands.add("/usr/pgsql-latest/bin/psql -p " + port + " -d filerepo -c 'select count(*) from file_repo.resource;'");
			}
			if(USATRegistry.POSTGRES_KLS.equals(app) && !"LOCAL".equalsIgnoreCase(host.getServerEnv())) {
				subcommands.add("/usr/pgsql-latest/bin/psql -p " + port + " -d km -c 'select count(*) from km.encrypted;'");
			}
			subcommands.add("exit");
			subcommands.add("PG_PID=`su postgres -c '/usr/pgsql-latest/bin/pg_ctl -D /opt/USAT/postgres/data status' | grep PID | cut -c 33- | cut -d')' -f1`");
			if(host.isServerType("PGS")){
				subcommands.add("su - " + app.getUserName());
				subcommands.add("if [ `/usr/pgsql-latest/bin/pg_ctl -D /opt/USAT/postgres/data status| grep PID| wc -l` -ne 0 ]; then /opt/USAT/postgres/bin/postgresql.sh stop;fi");
				subcommands.add("exit");
			}else{
				subcommands.add("if [ -x /usr/monit-latest/bin/monit ]; then /usr/monit-latest/bin/monit stop " + pgName + "; while [ `/usr/monit-latest/bin/monit summary | grep -c \"Process '" + pgName + "'.* Running\"` -gt 0 ]; do sleep 1; done; else initctl stop USAT/" + pgName + "; fi");
			}
			subTasks.add(new ExecuteTask(subcommands.toArray(new String[subcommands.size()])));
			subcommands.clear();
			//full install
			pgInstallTask.registerChoice(Pattern.compile(majorRegex + "\\.\\d+"), subTasks.toArray(new DeployTask[subTasks.size()]));
			subTasks.clear();
			//minor version does not match
			pgInstallTask.registerChoice(Pattern.compile(StringUtils.escape(app.getVersion(), ".+*!\\".toCharArray(), '\\')), 
					new ExecuteTask(new YumOutputFilter(), "sudo su",
							"yum -v -y --disablerepo=* --enablerepo=pgdg" + versionArray[0] + versionArray[1] + " install postgresql" + versionArray[0] + versionArray[1] + "-server postgresql" + versionArray[0] + versionArray[1] + "-contrib | grep -v '#'"));
		
			ChooseTask pgUpgradeTask = new ChooseTask("sudo su -c \"if [ -f " + appDir + "/data/PG_VERSION ]; then cat " + appDir + "/data/PG_VERSION; else echo 'No such file or directory'; fi\"");
			// Choice #1: Initialize
			subTasks.add(new ExecuteTask("sudo su - postgres", "/usr/pgsql-latest/bin/initdb -D " + appDir + "/data"));
			USATHelper.addPostgresConfTasks(subTasks, app, pgDirs, postgresqlConfs, host, port, serverSet);
			if(host.isServerType("PGS")){
				subTasks.add(new ExecuteTask("sudo su postgres -c '" + pgRestartCmd+"'"));
			}else{
				subTasks.add(new ExecuteTask("sudo " + pgRestartCmd));
			}
			pgUpgradeTask.registerChoice(Pattern.compile(".*No such file or directory.*"), subTasks.toArray(new DeployTask[subTasks.size()]));
			subTasks.clear();

			// Choice #2: Update config, remove old data directories, restart
			USATHelper.addPostgresConfTasks(subTasks, app, pgDirs, postgresqlConfs, host, port, serverSet);
			subTasks.add(new DeployTask() {
				@Override
				public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
					try{
						String[] ret = ExecuteTask.executeCommands(deployTaskInfo, "sudo find " + appDir + "/ -maxdepth 1 -name '*_data'", "sudo find " + appDir + "/tblspace -maxdepth 2 -mindepth 2 -name 'PG_*' | grep -v '/PG_" + majorVersion + "_'");
						String line;
						BufferedReader reader;
						for(String s : ret) {
							reader = new BufferedReader(new StringReader(s));
							while((line = reader.readLine()) != null) {
								String answer = deployTaskInfo.interaction.readLine("Postgres was previously upgraded to " + majorVersion + " for " + appDir + ". Should I delete the directory '" + line + "' (Y/N)?");
								if(answer != null && answer.toUpperCase().startsWith("Y")) {
									ExecuteTask.executeCommands(deployTaskInfo, "sudo /bin/rm -r " + line);
								}
							}
						}
					}catch(IOException e){
						return e.getMessage();
					}
					return "Complete";
				}

				@Override
				public String getDescription(DeployTaskInfo deployTaskInfo) {
					return "Remove old postgres data folders";
				}
			});
			if(host.isServerType("PGS")){
				subTasks.add(new ExecuteTask("sudo su - postgres", pgRestartCmd));
			}else{
				subTasks.add(new ExecuteTask("sudo " + pgRestartCmd));
			}
			pgUpgradeTask.registerChoice(Pattern.compile(majorRegex), subTasks.toArray(new DeployTask[subTasks.size()]));
			subTasks.clear();
			//pgs active
			if(host.isServerType("PGS")) {
				tasks.add(new OptionalTask("if [ `sudo clustat -s primary| sed -n 3p| awk '{print $2}'` = \""+host.getSimpleName()+"-priv"+"\" ]; then echo 'Y';fi", Pattern.compile("^Y\\s*"), false, pgUpgradeTask));
			} else {
				tasks.add(pgUpgradeTask);
			}

			// Choice #3: Upgrade
			// we must make sure monit is down and cannot restart postgres until we are done the data upgrade
			subcommands.add("sudo su");
			if(host.isServerType("PGS")) {
				subcommands.add("sudo su - postgres");
				subcommands.add("if [ `/usr/pgsql-latest/bin/pg_ctl -D /opt/USAT/postgres/data status| grep PID| wc -l` -ne 0 ]; then /usr/pgsql-latest/bin/pg_ctl -w -D /opt/USAT/postgres/data stop -o '-p 5432' -m fast;fi");
				subcommands.add("exit");
			} else {
				subcommands.add("if [ `su postgres -c '/usr/pgsql-latest/bin/pg_ctl -D /opt/USAT/postgres/data status'| grep PID| wc -l` -ne 1 ]; then /usr/monit-latest/bin/monit restart " + pgName + "; while [ `/usr/monit-latest/bin/monit summary | grep -c \"Process '" + pgName + "'.* Running\"` -eq 0 ]; do sleep 1; done; fi");
				subcommands.add("sudo su - " + app.getUserName());
				if((USATRegistry.POSTGRES_MST.equals(app) || USATRegistry.POSTGRES_MSR.equals(app)) && !"LOCAL".equalsIgnoreCase(host.getServerEnv())) {
					subcommands.add("/usr/pgsql-latest/bin/psql -p " + port + " -d mq -c 'select count(*) from mq.q_;'");
					subcommands.add("/usr/pgsql-latest/bin/psql -p " + port + " -d filerepo -c 'select count(*) from file_repo.resource;'");
				}
				if(USATRegistry.POSTGRES_KLS.equals(app) && !"LOCAL".equalsIgnoreCase(host.getServerEnv())) {
					subcommands.add("/usr/pgsql-latest/bin/psql -p " + port + " -d km -c 'select count(*) from km.encrypted;'");
				}
				subcommands.add("exit");
				subcommands.add("if [ -x /usr/monit-latest/bin/monit ]; then /usr/monit-latest/bin/monit stop " + pgName + "; while [ `/usr/monit-latest/bin/monit summary | grep -c \"Process '" + pgName + "'.* Running\"` -eq 0 ]; do sleep 1; done; else initctl stop USAT/" + pgName + "; fi");
				subTasks.add(new OptionalTask("if [ `/usr/pgsql-latest/bin/pg_config --version | cut -d ' ' -f 2 | cut -d '.' -f1-2` = `sudo cat " + appDir + "/data/PG_VERSION` ]; then echo 'Y'; else echo 'N'; fi", Pattern.compile("Y"), false, new ExecuteTask(subcommands.toArray(new String[subcommands.size()]))));
				subcommands.clear();
				subcommands.add("sudo su");
			}

			USATHelper.addPostgresConfTasks(subTasks, app, pgDirs, postgresqlConfs, host, port, serverSet);
			subcommands.add("if [ -d " + appDir + "/old_data -a  \\( -h " + appDir + "/data -o -d " + appDir + "/data \\) ]; then mv " + appDir + "/old_data " + appDir + "/old_data_`date +%Y%m%d_%k%M%S`/; fi");
			subcommands.add("if [ -h " + appDir + "/data ]; then mkdir " + appDir + "/old_data; mv " + appDir + "/data/* " + appDir + "/old_data/; elif [ -d " + appDir + "/data ]; then mv " + appDir + "/data " + appDir + "/old_data; fi");
			subcommands.add("if [ -d " + appDir + "/new_data ] && [ \"`ls -ld " + appDir + "/new_data | cut -d' ' -f3`\" != \"postgres\" ] ; then chown -R postgres:postgres " + appDir + "/new_data; fi");
			subcommands.add("if [ -s " + appDir + "/new_data ]; then /bin/rm -r " + appDir + "/new_data/*; fi");
			subcommands.add("chown postgres:postgres " + appDir + "/old_data");
			subcommands.add("chmod -R 700 " + appDir + "/old_data");
			subcommands.add("sudo su - postgres");
			subcommands.add("/usr/pgsql-latest/bin/initdb -D " + appDir + "/new_data");
			subcommands.add("/bin/cp " + appDir + "/old_data/*.conf " + appDir + "/new_data");
			subcommands.add("/bin/cp " + appDir + "/old_data/*.crt " + appDir + "/new_data");
			subcommands.add("/bin/cp " + appDir + "/old_data/*.key " + appDir + "/new_data");
			subTasks.add(new ExecuteTask(subcommands.toArray(new String[subcommands.size()])));
			subcommands.clear();

			subcommands.add("sudo su - postgres");
			//subcommands.add("echo \"[`date`] pg_upgrade start\"| mail -s \"pg_upgrade start\" \"yhe@usatech.com\"");
			
			subcommands.add("/usr/pgsql-latest/bin/pg_upgrade -b /usr/pgsql-`cat " + appDir + "/old_data/PG_VERSION`/bin -B /usr/pgsql-latest/bin -d " + appDir + "/old_data -D " + appDir + "/new_data");
			
			//subcommands.add("echo \"[`date`] pg_upgrade done\"| mail -s \"pg_upgrade done\" \"yhe@usatech.com\"");
			
			subcommands.add("/usr/pgsql-latest/bin/pg_ctl -w -o '-p " + (port + 45000) + "' -D " + appDir + "/new_data start > " + appDir + "/logs/server.log 2>&1");
			subcommands.add("vacuumdb -avz -p " + (port + 45000));
			
			//subcommands.add("echo \"[`date`] vacuumdb done\"| mail -s \"vacuumdb done\" \"yhe@usatech.com\"");
			
			if((USATRegistry.POSTGRES_MST.equals(app) || USATRegistry.POSTGRES_MSR.equals(app)) && !"LOCAL".equalsIgnoreCase(host.getServerEnv())) {
				subcommands.add("/usr/pgsql-latest/bin/psql -p " + (port + 45000) + " -d mq -c 'select count(*) from mq.q_;'");
				subcommands.add("/usr/pgsql-latest/bin/psql -p " + (port + 45000) + " -d filerepo -c 'select count(*) from file_repo.resource;'");
			}
			
			subcommands.add("/usr/pgsql-latest/bin/pg_ctl -o '-p " + (port + 45000) + "' -D " + appDir + "/new_data stop -m fast");
			subcommands.add("exit");
			subcommands.add("sudo su");
			if(host.isServerType("PGS")){
				//subcommands.add("if [ -d " + appDir + "/data ]; then /bin/rm -r " + appDir + "/data/*; fi");
				subcommands.add("/bin/mv " + appDir + "/new_data/* " + appDir + "/data");
			}else{
				subcommands.add("if [ -d " + appDir + "/data ]; then /bin/rm -r " + appDir + "/data; fi");
				subcommands.add("/bin/mv " + appDir + "/new_data " + appDir + "/data");
			}
			if(host.isServerType("PGS")){
				subcommands.add("sudo su - postgres");
				subcommands.add(pgRestartCmd);
			}else{
				subcommands.add(pgRestartCmd);
			}
			subTasks.add(new ExecuteTask(subcommands.toArray(new String[subcommands.size()])));
			subcommands.clear();

			subTasks.add(new DeployTask() {
				@Override
				public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
					if(USATRegistry.POSTGRES_KLS.equals(app) && !"LOCAL".equalsIgnoreCase(host.getServerEnv()))
						reinitStandby.set(true);
					String answer = deployTaskInfo.interaction.readLine("Postgres was upgraded to " + majorVersion + " for " + appDir + ". Should I delete the old data (Y/N)?");
					if(answer != null && answer.toUpperCase().startsWith("Y")) {
						if(USATRegistry.POSTGRES_PGS.equals(app)&&!"LOCAL".equalsIgnoreCase(host.getServerEnv())){
							ExecuteTask.executeCommands(deployTaskInfo, "sudo su", 
									"/bin/rm -r " + appDir + "/old_data", 
									"/bin/rm -r `ls -d " + appDir + "/tblspace/*/PG_* | grep -v '/PG_" + majorVersion + "_'`", 
									"/bin/rm -r `ls -d " + appDir + "/T2tblspace/*/PG_* | grep -v '/PG_" + majorVersion + "_'`",
									"/bin/rm -r `ls -d " + appDir + "/T3tblspace/*/PG_* | grep -v '/PG_" + majorVersion + "_'`"
									);
						}else{
							ExecuteTask.executeCommands(deployTaskInfo, "sudo su", 
								"/bin/rm -r " + appDir + "/old_data", 
								"/bin/rm -r `ls -d " + appDir + "/tblspace/*/PG_* | grep -v '/PG_" + majorVersion + "_'`"
								);
						}
						return "Complete";
					}
					return "Skipping";
				}

				@Override
				public String getDescription(DeployTaskInfo deployTaskInfo) {
					return "Remove old postgres data folders";
				}
			});
			pgUpgradeTask.registerChoice(Pattern.compile("\\d+\\.\\d+"), subTasks.toArray(new DeployTask[subTasks.size()]));
			subTasks.clear();
		} else {
			USATHelper.addPostgresConfTasks(tasks, app, pgDirs, postgresqlConfs, host, port, serverSet);
			if(host.isServerType("PGS")){
				USATHelper.addOdbcLinkTasks(host, cache, tasks, getWorkDir());
			}
		}
		tasks.add(new FromCvsUploadTask("server_app_config/Postgresql/cleanup_dead_connections.sh", "HEAD", appDir + "/bin/cleanup_dead_connections.sh", 0754, app.getUserName(), app.getUserName(), true));
		tasks.add(new FromCvsUploadTask("server_app_config/Postgresql/delete_expired_resources.sh", "HEAD", appDir + "/bin/delete_expired_resources.sh", 0754, app.getUserName(), app.getUserName(), true));
		for(int i = 0; i < pgDirs.length; i++) {
			if(!appDir.equals(pgDirs[i])) {
				tasks.add(new ExecuteTask("sudo su", "if [ ! -h " + pgDirs[i] + "/bin/cleanup_dead_connections.sh ]; then ln -s " + appDir + "/bin/cleanup_dead_connections.sh " + pgDirs[i] + "/bin/cleanup_dead_connections.sh; fi"));
			}
			cronUpdateTask.registerJob(Pattern.compile(".*" + pgDirs[i] + "/archivelogs\\.sh.*"), new int[] { 9 }, null, null, null, null, pgDirs[i] + "/archivelogs.sh >> " + pgDirs[i] + "/archivelogs.log 2>&1", "# Rename and gzip rotated log files");
			int pgPort = port + (i * 10000);
			cronUpdateTask.registerJob(Pattern.compile(".*" + pgDirs[i] + "/bin/cleanup_dead_connections\\.sh.*"), new int[] { 0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55 }, null, null, null, null, pgDirs[i] + "/bin/cleanup_dead_connections.sh " + pgPort + " >> " + pgDirs[i] + "/logs/cleanup_dead_connections.log 2>&1", "# Clean up dead connections");
		}
		if(host.isServerType("MST") || host.isServerType("MSR")) {
			int days;
			if(host.isServerType("MSR"))
				days = 36;
			else
				days = 7;
			cronUpdateTask.registerJob(Pattern.compile(".*" + appDir + "/bin/delete_expired_resources\\.sh.*"), new int[] { 3 }, new int[] { instanceNum }, null, null, null, appDir + "/bin/delete_expired_resources.sh " + days + " >> " + appDir + "/logs/delete_expired_resources.log 2>&1", "# Delete expired resource files");
			// build mq and filerepo databases
			PostgresSetupTask pst = new PostgresSetupTask(appDir, pgName, port);
			tasks.add(pst);
			pst.registerRole("admin", null, "read_mq", "use_mq", "write_file_repo");
			pst.registerRole("admin_1", new String[] { "SUPERUSER", "LOGIN" }, "admin");
			pst.registerUser("monitor", "read_mq");
			Set<App> remoteApps = new HashSet<App>();
			if(host.isServerType("MST")) {
				remoteApps.add(USATRegistry.NET_LAYER);
				remoteApps.add(USATRegistry.OUTAUTH_LAYER);
				remoteApps.add(USATRegistry.APP_LAYER);
				remoteApps.add(USATRegistry.INAUTH_LAYER);
				remoteApps.add(USATRegistry.POSM_LAYER);
				remoteApps.add(USATRegistry.KEYMGR);
				remoteApps.add(USATRegistry.LOADER);
				remoteApps.add(USATRegistry.DMS_APP);
			}
			if(host.isServerType("MSR")) {
				remoteApps.add(USATRegistry.RPTREQ_LAYER);
				remoteApps.add(USATRegistry.RPTGEN_LAYER);
				remoteApps.add(USATRegistry.TRANSPORT_LAYER);
				remoteApps.add(USATRegistry.USALIVE_APP);
				remoteApps.add(USATRegistry.LOADER);
			}
			for(App remoteApp : remoteApps) {
				for(int i = 1; i <= 4; i++)
					pst.registerUser(remoteApp.getDbUserName() + '_' + i, "use_mq", "write_file_repo");
			}
			if("LOCAL".equalsIgnoreCase(host.getServerEnv())) {
				for(int i = 1; i <= 2; i++)
					for(String prefix : new String[] { "dk", "bk", "yh" }) {
						pst.registerSetup(prefix + "filerepo" + i, "file_repo", new CvsPullTask("DatabaseScripts/filerepo/setup_file_repo_objects.sql", "HEAD"), prefix + "_file_repo_data_" + i, "admin");
						pst.registerSetup(prefix + "mq" + i, "mq", new CvsPullTask("Simple1.5/src/simple/mq/peer/mq_peer_postgres_setup.sql", "HEAD"), prefix + "_mq_data_" + i, "admin");
					}
			} else {
				pst.registerSetup("filerepo", "file_repo", new CvsPullTask("DatabaseScripts/filerepo/setup_file_repo_objects.sql", "HEAD"), "file_repo_data", "admin");
				pst.registerSetup("mq", "mq", new CvsPullTask("Simple1.5/src/simple/mq/peer/mq_peer_postgres_setup.sql", "HEAD"), "mq_data", "admin");
			}
			if(host.isServerType("MST")) {
				pst.registerRole("admin", new String[] { "SUPERUSER" }, "read_app", "write_app");
				pst.registerUser(USATRegistry.APP_LAYER.getDbUserName() + '_' + instanceNum, "write_app", "read_app");
				pst.registerSetup("app_" + instanceNum, "app_cluster", new CvsPullTask("DatabaseScripts/edge_implementation/setup_app_db.sql", "HEAD"), "app_data");
			}
		}
		if(host.isServerType("KLS")) {
			tasks.add(new FromCvsUploadTask("server_app_config/Postgresql/drop_old_partitions.sh", "HEAD", appDir + "/bin/drop_old_partitions.sh", 0754, app.getUserName(), app.getUserName(), true));
			cronUpdateTask.registerJob(Pattern.compile(".*" + appDir + "/bin/drop_old_partitions\\.sh.*"), new int[] { 44 }, new int[] { instanceNum }, null, null, null, appDir + "/bin/drop_old_partitions.sh >> " + appDir + "/logs/drop_old_partitions.log 2>&1", "# Drop old partitions");
			PostgresSetupTask pst = new PostgresSetupTask(appDir, pgName, port);
			tasks.add(pst);
			pst.registerRole("replica", new String[] { "SUPERUSER", "CREATEDB", "CREATEROLE", "REPLICATION", "LOGIN" });
			pst.registerUser("keymgr", "use_km");
			if("LOCAL".equalsIgnoreCase(host.getServerEnv())) {
				for(int i = 1; i <= 2; i++)
					for(String prefix : new String[] { "dk", "bk", "yh" }) {
						pst.registerSetup(prefix + "km" + i, "km", new CvsPullTask("KeyManagerService/src/km_setup.sql", "HEAD"), prefix + "_km_data_" + i, "postgres");
					}
			} else {
				pst.registerSetup("km", "km", new CvsPullTask("KeyManagerService/src/km_setup.sql", "HEAD"), "km_data", "postgres");
			}
		}
		if(host.isServerType("PGS")) {

			PostgresSetupTask pst = new PostgresSetupTask(appDir, pgName, port);
			//only add PostgresSetupTask when it is active node
			tasks.add(new OptionalTask("if [ `sudo su postgres -c '/usr/pgsql-latest/bin/pg_ctl -D /opt/USAT/postgres/data status'| grep PID| wc -l` -eq 1 ]; then echo 'Y'; fi", Pattern.compile("^Y\\s*"), false,pst));
			pst.registerRole("admin", null, "read_rdw", "read_main", "write_rdw", "write_main");
			pst.registerRole("admin_1", new String[] { "SUPERUSER", "LOGIN" }, "admin");
			pst.registerUser("monitor", "read_rdw", "read_main");
			App[] remoteApps = new App[] { USATRegistry.RPTGEN_LAYER };
			for(App remoteApp : remoteApps) {
				for(int i = 1; i <= 4; i++)
					pst.registerUser(remoteApp.getDbUserName() + '_' + i, "read_rdw", "read_main", "write_main");
			}
			remoteApps = new App[] { USATRegistry.USALIVE_APP };
			for(App remoteApp : remoteApps) {
				for(int i = 1; i <= 4; i++)
					pst.registerUser(remoteApp.getDbUserName() + '_' + i, "read_rdw", "read_main");
			}
			remoteApps = new App[] { USATRegistry.LOADER };
			for(App remoteApp : remoteApps) {
				for(int i = 1; i <= 4; i++)
					pst.registerUser(remoteApp.getDbUserName() + '_' + i, "read_main", "write_main");
			}
			remoteApps = new App[] { USATRegistry.RDW_LOADER };
			for(App remoteApp : remoteApps) {
				for(int i = 1; i <= 4; i++)
					pst.registerUser(remoteApp.getDbUserName() + '_' + i, "read_rdw", "write_rdw", "read_main");
			}
			remoteApps = new App[] { USATRegistry.DMS_APP, USATRegistry.POSM_LAYER };
			for(App remoteApp : remoteApps) {
				for(int i = 1; i <= 4; i++)
					pst.registerUser(remoteApp.getDbUserName() + '_' + i, "read_main", "write_main");
			}
			
			if("LOCAL".equalsIgnoreCase(host.getServerEnv()) || "DEV".equalsIgnoreCase(host.getServerEnv())) {
				pst.registerSetup("main", "RDW", new CvsPullTask("DatabaseScripts/REPTP/RDW/pg_rdw_setup.sql", "HEAD"), "rdw_data_t1", "postgres");
				pst.registerTablespace("rdw_data_t2", "postgres");
				pst.registerTablespace("rdw_data_t3", "postgres");
			}
			pst.registerFunction("main", "public", "hex_to_int", "CREATE OR REPLACE FUNCTION hex_to_int(hexval varchar) RETURNS bigint AS $$\nDECLARE\n    result  bigint;\nBEGIN\n    EXECUTE 'SELECT x''' || hexval || '''::bigint' INTO result;\n    RETURN result;\nEND;\n$$ LANGUAGE 'plpgsql' IMMUTABLE STRICT;\n");
			pst.registerSetup("main", "MAIN", new ConcatenatingContent(new CvsPullTask("DatabaseScripts/postgresql/pg_main_setup.sql", "HEAD"), new CvsPullTask("DatabaseScripts/postgresql/device_session_and_message.sql", "HEAD")), "main_data", "postgres");
			tasks.add(new FromCvsUploadTask("server_app_config/Postgresql/partition_maintenance.sh", "HEAD", appDir + "/bin/partition_maintenance.sh", 0754, app.getUserName(), app.getUserName(), true));
			cronUpdateTask.registerJob(Pattern.compile(".*" + appDir + "/bin/partition_maintenance\\.sh.*"), new int[] { 30 }, new int[] { 2 }, null, null, null, appDir + "/bin/partition_maintenance.sh >> " + appDir + "/logs/partition_maintenance.log 2>&1", "# Partition maintenance");
			cronUpdateTask.registerJob(Pattern.compile(".*" + appDir + "/bin/pgs_trigger_backup\\.sh.*"), new int[] { 30 }, new int[] { 6 }, null, null, null, appDir + "/bin/pgs_trigger_backup.sh >> " + appDir + "/logs/postgresqlbackup.log 2>&1", "# Daily PGS postgres backup");
		}
		if(!subcommands.isEmpty())
			tasks.add(new ExecuteTask(subcommands.toArray(new String[subcommands.size()])));
		subcommands.clear();
		if(cronUpdateTask.getJobCount() > 0 || !cronUpdateTask.isKeepExisting())
			tasks.add(cronUpdateTask);

		if(app.getServiceName() != null) {
			BufferStream bufferStream = new HeapBufferStream();
			PrintStream ps = new PrintStream(bufferStream.getOutputStream());
			printArchiveLogCommand(ps, app.equals(USATRegistry.POSTGRES_KLS) ? 100 : 40, true, appDir + "/logs/" + app.getServiceName(), appDir + "/logs/" + app.getServiceName() + "-*.log");
			ps.println("exit 0");
			ps.flush();
			tasks.add(new UnixUploadTask(bufferStream.getInputStream(), appDir, "archivelogs.sh", 0754, app.getUserName(), "usat", true, "Log archive script for '" + app.getName() + (ordinal == null ? "" : ordinal.toString()) + "'"));
		}

	}


	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareHostTasks(host, tasks, cache);
	}
	
	@Override
	protected void addTasks(Host host, App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache);
	}

	protected void addBeforeRestartCommands(Host host, App app, Integer ordinal, int instanceNum, List<String> commands, List<DeployTask> tasks) {
	}

	@Override
	public String getName() {
		return "Postgres Install";
	}
}
