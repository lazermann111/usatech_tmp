package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.networklayer.protocol.DeviceInfoReceiver;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R49_Patch_1_ChangeSet extends MultiLinuxPatchChangeSet {
	public R49_Patch_1_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/NetLayer/src/", DeviceInfoReceiver.class, "REL_netlayer_1_35_1", USATRegistry.NET_LAYER);
	}
	
	@Override
	public String getName() {
		return "R49 Patch 1";
	}
}
