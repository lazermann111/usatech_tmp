package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.InteractiveChangeSet;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATHelper;

public class JDKUpgrade_ChangeSet extends InteractiveChangeSet implements ChangeSet {
	protected String jdkVersion = null;
	protected String jdkFileName = null;
	protected int majorVersion;
	protected int minorVersion;
	protected int securityVersion;

	@Override
	public String getName() {
		return "JDK Upgrade";
	}

	@Override
	public boolean isApplicable(Host host) {
		return true;
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		if (jdkVersion == null) {
			interaction.printf("Before proceeding, put local_policy.jar, US_export_policy.jar and new JDK file, for example jdk-8u192-linux-x64.tar.gz, in your home directory on every Linux server your are upgrading");
			while(true) {
				jdkVersion = interaction.readLine("Enter new JDK major.minor.security version, for example: 8.192.0 or 11.0.1", "");
				if (jdkVersion.matches("^[0-9]+\\.[0-9]+\\.[0-9]+$"))
					break;
				else
					interaction.printf("Invalid JDK version");
			}
			String versions[] = jdkVersion.split("\\.");
			majorVersion = Integer.valueOf(versions[0]);
			minorVersion = Integer.valueOf(versions[1]);
			securityVersion = Integer.valueOf(versions[2]);			
		}
		if (jdkFileName == null) {
			while(true) {
				jdkFileName = interaction.readLine("Enter new JDK file name in your Linux home directory, for example: jdk-8u192-linux-x64.tar.gz or jdk-11.0.1_linux-x64_bin.tar.gz", "");
				if (jdkFileName.matches("^jdk-[0-9]+.+linux-x64.*\\.tar\\.gz$"))
					break;
				else
					interaction.printf("Invalid JDK file name");
			}
		}
		
		List<DeployTask> tasks = new ArrayList<DeployTask>();
		USATHelper.installJava(host, cache, tasks, majorVersion, minorVersion, securityVersion, jdkFileName, "/home/`logname`");
		return tasks.toArray(new DeployTask[tasks.size()]);
	}

	@Override
	public String toString() {
		return getName();
	}
}
