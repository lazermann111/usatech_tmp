package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R50_Patch_10_ChangeSet extends MultiLinuxPatchChangeSet {
	public R50_Patch_10_ChangeSet() throws UnknownHostException {
		super();
		registerResource("usalive/xsl/rma-data-layer.xml", "classes/", "BRN_R50", true, USATRegistry.USALIVE_APP);
	}

	@Override
	public String getName() {
		return "R50 Patch 10";
	}
}
