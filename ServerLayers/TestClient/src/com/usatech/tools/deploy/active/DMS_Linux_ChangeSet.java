package com.usatech.tools.deploy.active;

import static com.usatech.tools.deploy.USATRegistry.DMS_APP;
import static com.usatech.tools.deploy.USATRegistry.HTTPD_NET;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.AppSettingUpdateFileTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.tools.deploy.UsatSettingUpdateFileTask;

public class DMS_Linux_ChangeSet extends AbstractLinuxChangeSet {
	public DMS_Linux_ChangeSet() throws UnknownHostException {
		super();
	}

	@Override
	protected void registerApps() {
		registerApp(HTTPD_NET, "2.2.23-usat-build");
		registerApp(DMS_APP, "1.0.8");
		registry.registerSubApps(USATRegistry.HTTPD_NET, USATRegistry.DMS_APP);
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareHostTasks(host, tasks, cache);
		if(host.isServerType("NET") || host.isServerType("APR")) {
			tasks.add(new UsatSettingUpdateFileTask(registry.getServerSet(host.getServerEnv())));
		}
	}
	
	@Override
	protected void addTasks(Host host, App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if("httpd".equals(app.getName()) || "dms".equals(app.getName())) {
			if(ordinal != null) {
				if(ordinal == 1)
					ordinal = null;
				else
					return;
			}
		}
		addPrepareAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache);
		if(!"httpd".equals(app.getName()) && !"postgres".equals(app.getName()))
			tasks.add(new AppSettingUpdateFileTask(app, ordinal, instanceNum, instanceCount, cache, registry.getServerSet(host.getServerEnv())));
		addInstallAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache, commands);
	}

	protected void addBeforeRestartCommands(Host host, App app, Integer ordinal, int instanceNum, List<String> commands, List<DeployTask> tasks) {
		/*
		if(app.getName().equals("applayer")) {
			commands.add("/bin/rm -r /opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal) + "/db/AppLayerDBOld");
			commands.add("/bin/mv /opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal) + "/db/AppLayerDB /opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal) + "/db/AppLayerDBOld");
		}
		//*/
	}
	@Override
	public String getName() {
		return "DMS Install - Linux";
	}
}
