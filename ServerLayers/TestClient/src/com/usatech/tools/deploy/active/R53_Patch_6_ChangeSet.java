package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.ccs.CampaignBlastTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R53_Patch_6_ChangeSet extends MultiLinuxPatchChangeSet {
	public R53_Patch_6_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ReportGenerator/src", CampaignBlastTask.class, "REL_report_generator_3_27_6", USATRegistry.RPTGEN_LAYER);
		registerResource("ReportGenerator/src/rg-campaign-data-layer.xml", "classes/", "REL_report_generator_3_27_6", false, USATRegistry.RPTGEN_LAYER);
	}
	
	@Override
	public String getName() {
		return "R53 Patch 6";
	}
}
