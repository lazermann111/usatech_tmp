package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.FromCvsUploadTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;

public class UpdatePGCronScripts_ChangeSet implements ChangeSet {
	@Override
	public String getName() {
		return "Update Postgres Cron Scripts";
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("MST") || host.isServerType("MSR") || host.isServerType("KLS") || host.isServerType("PGS");
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		List<DeployTask> tasks = new ArrayList<DeployTask>();
		String appDir = "/opt/USAT/postgres";
		String username = "postgres";
		tasks.add(new FromCvsUploadTask("server_app_config/Postgresql/cleanup_dead_connections.sh", "HEAD", appDir + "/bin/cleanup_dead_connections.sh", 0754, username, username, true));
		tasks.add(new FromCvsUploadTask("server_app_config/Postgresql/delete_expired_resources.sh", "HEAD", appDir + "/bin/delete_expired_resources.sh", 0754, username, username, true));
		return tasks.toArray(new DeployTask[tasks.size()]);
	}

	@Override
	public String toString() {
		return getName();
	}
}
