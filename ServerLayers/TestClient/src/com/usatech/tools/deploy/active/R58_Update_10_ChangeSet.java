package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R58_Update_10_ChangeSet extends MultiLinuxPatchChangeSet {
	public R58_Update_10_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/AppLayer/src/loader-data-layer.xml", "classes", "REL_R58_10", false, USATRegistry.LOADER);
	}
	
	@Override
	public String getName() {
		return "R58 Update 10 - Operator Merchant of Record";
	}
}
