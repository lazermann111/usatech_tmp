package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R50_Patch_2_ChangeSet extends MultiLinuxPatchChangeSet {
	public R50_Patch_2_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/POSMLayer/conf/posm-data-layer.xml", "classes", "REL_posmlayer_1_27_2", false, USATRegistry.POSM_LAYER);
		registerResource("ServerLayers/POSMLayer/conf/POSMLayerService.properties", "classes", "REL_posmlayer_1_27_2", true, USATRegistry.POSM_LAYER);
	}
	
	@Override
	public String getName() {
		return "R50 Patch 2";
	}
}
