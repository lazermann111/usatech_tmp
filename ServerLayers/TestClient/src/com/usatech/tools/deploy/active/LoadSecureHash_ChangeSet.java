package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import oracle.aurora.server.tools.loadjava.LoadJava;
import oracle.aurora.server.tools.loadjava.ToolsException;
import oracle.jdbc.driver.OracleDriver;
import simple.app.ServiceException;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.CvsPullTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.DeployTaskInfo;
import com.usatech.tools.deploy.DeployUtils;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;

public class LoadSecureHash_ChangeSet implements ChangeSet {

	@Override
	public String getName() {
		return "Load SecureHash to Oracle";
	}

	@Override
	public boolean isApplicable(Host host) {
		return true;
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		final LoadJava loadJava = new LoadJava();
		final CvsPullTask cvsPullTask = new CvsPullTask("Simple1.5/src/simple/security/SecureHash.java", "HEAD");
		return new DeployTask[] {
			new DeployTask() {
				@Override
				public String getDescription(DeployTaskInfo deployTaskInfo) {
					return "Loading Java Source 'simple/security/SecureHash.java' to Oracle";
				}
				@Override
				public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
					String operUrl;
					if("DEV".equals(deployTaskInfo.host.getServerEnv()))
						operUrl = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb011.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=usadev03.world)))";
					else if("INT".equals(deployTaskInfo.host.getServerEnv()))
						operUrl = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=intdb011.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=usadev02.world)))";
					else if("ECC".equals(deployTaskInfo.host.getServerEnv()))
						operUrl = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=eccscan01.usatech.com)(PORT=1525))(ADDRESS=(PROTOCOL=TCP)(HOST=eccscan02.usatech.com)(PORT=1525)))(CONNECT_DATA=(SERVICE_NAME=usaecc_db.world)))";
					else if("USA".equals(deployTaskInfo.host.getServerEnv()))
						operUrl = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.120)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.121)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.122)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.220)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.221)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.222)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)))";
					else
						throw new IOException("Server Type '" + deployTaskInfo.host.getServerEnv() + "' is not recognized");
					
					try {
						InputStream in = cvsPullTask.process(deployTaskInfo);
					} catch(ServiceException e) {
						throw new IOException(e);
					}
					new OracleDriver();
					char[] password = DeployUtils.getPassword(deployTaskInfo, "SYSTEM@OPER", "SYSTEM user on OPER database");
					Connection conn;
					try {
						conn = DriverManager.getConnection(operUrl, "SYSTEM", new String(password));
					} catch(SQLException e) {
						deployTaskInfo.passwordCache.setPassword(deployTaskInfo.host, "SYSTEM@OPER", null);					
						throw new IOException(e);
					}
					loadJava.set("-debug");
					loadJava.set("-resolve");
					loadJava.set("-schema", "DBADMIN");
					try {
						loadJava.setConnection(conn);
						loadJava.add(cvsPullTask.getResultInputStream(), "simple/security/SecureHash.java", loadJava.getOpts());
						loadJava.process();
						loadJava.finish();
					} catch(ToolsException e) {
						throw new IOException(e);
					} finally {
						try {
							conn.close();
						} catch(SQLException e) {
							// ignore
						}
					}
					return "Loaded 'simple/security/SecureHash.java' to Oracle";
				}
			}
		};
	}

	@Override
	public String toString() {
		return getName();
	}
}
