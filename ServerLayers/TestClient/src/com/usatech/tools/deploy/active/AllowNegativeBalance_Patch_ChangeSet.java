package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class AllowNegativeBalance_Patch_ChangeSet extends MultiLinuxPatchChangeSet {
	public AllowNegativeBalance_Patch_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/AuthorityLayer/src/authority-data-layer.xml", "classes/", "BRN_R52", false, USATRegistry.INAUTH_LAYER);
	}
	
	@Override
	public String getName() {
		return "AllowNegativeBalance Patch For Prepaid";
	}
}
