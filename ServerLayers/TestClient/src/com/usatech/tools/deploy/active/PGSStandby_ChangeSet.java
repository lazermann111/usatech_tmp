package com.usatech.tools.deploy.active;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import simple.io.BufferStream;
import simple.io.EncodingInputStream;
import simple.io.HeapBufferStream;
import simple.io.ReplacementsLineFilteringReader;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.CronUpdateTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.FromCvsUploadTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.OptionalTask;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.Registry;
import com.usatech.tools.deploy.RenewFileCertTask;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.tools.deploy.UnixUploadTask;
import com.usatech.tools.deploy.UsatIPTablesUpdateTask;

/**
 * @author yhe
 *
 */
public class PGSStandby_ChangeSet implements ChangeSet {
	

	@Override
	public String getName() {
		return "PGS Standby setup Task";
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("PGS");
	}
	
	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		List<DeployTask> tasks =new ArrayList<DeployTask>();
		addInstallPgRepack(tasks);
		tasks.add(new ExecuteTask("sudo su", "if [ `egrep -c '^replica:' /etc/passwd` -eq 0 ]; then groupadd -g " + USATRegistry.REPLICA_LOCAL_USER_UID + " replica; useradd -g " + USATRegistry.REPLICA_LOCAL_USER_UID + " replica; fi"));
		tasks.add(new FromCvsUploadTask("server_app_config/PGS/replaceSymlinks.sh", "HEAD", "/opt/USAT/postgres/bin/replaceSymlinks.sh", 0755,"postgres", "postgres", true));
		tasks.add(new FromCvsUploadTask("server_app_config/PGS/pgs_trigger_set_tablespace.sh","HEAD", "/opt/USAT/postgres/bin/pgs_trigger_set_tablespace.sh", 0755, "postgres", "postgres", true));
		tasks.add(new FromCvsUploadTask("server_app_config/PGS/pgs_set_tablespace.sh","HEAD", "/opt/USAT/postgres/bin/pgs_set_tablespace.sh", 0755, "postgres", "postgres", true));
		tasks.add(new FromCvsUploadTask("server_app_config/PGS/pgs_cal_replication_lag.sh","HEAD", "/opt/USAT/postgres/bin/pgs_cal_replication_lag.sh", 0755, "postgres", "postgres", true));
		Registry registry=new USATRegistry();
		//tasks.add(new UsatIPTablesUpdateTask(registry.getServerSet(host.getServerEnv())));
		
		//standby tasks
		tasks.add(new ExecuteTask("sudo su",
				"if [ ! -d /opt/USAT/postgres_stdby ]; then  mkdir /opt/USAT/postgres_stdby; mkdir /opt/USAT/postgres_stdby/bin; mkdir /opt/USAT/postgres_stdby/logs; ln -s /stdby/data/ /opt/USAT/postgres_stdby/data; ln -s /stdby/tblspace/ /opt/USAT/postgres_stdby/tblspace; ln -s /stdby/T2tblspace/ /opt/USAT/postgres_stdby/T2tblspace; ln -s /stdby/T3tblspace/ /opt/USAT/postgres_stdby/T3tblspace; chown -R postgres:postgres /opt/USAT/postgres_stdby;fi"
				));
		tasks.add(new FromCvsUploadTask("server_app_config/Postgresql/cleanup_dead_connections.sh", "HEAD", "/opt/USAT/postgres_stdby/bin/cleanup_dead_connections.sh", 0754, "postgres", "postgres", true));
		BufferStream archiveLogsBufferStream = new HeapBufferStream();
		PrintWriter archiveLogsWriter = new PrintWriter(archiveLogsBufferStream.getOutputStream());
		archiveLogsWriter.println("/usr/local/USAT/bin/archive -v -a 40 -exclude_last_file -t=/opt/USAT/postgres_stdby/logs/postgresql /opt/USAT/postgres_stdby/logs/postgresql-*.log");
		archiveLogsWriter.println("exit 0");
		archiveLogsWriter.flush();
		tasks.add(new UnixUploadTask(archiveLogsBufferStream.getInputStream(), "/opt/USAT/postgres_stdby","archivelogs.sh", 0754, "postgres", "postgres", true));
		
		tasks.add(new FromCvsUploadTask("server_app_config/PGS/pgs_standby_basebackup.sh", "HEAD", "/opt/USAT/postgres/bin/pgs_standby_basebackup.sh", 0755,"postgres", "postgres", true));
		//tasks.add(new ExecuteTask("sudo su postgres","/opt/USAT/postgres/bin/pgs_standby_basebackup.sh"));
		String standbyPath="/opt/USAT/postgres_stdby";
		String primaryVipHost="devtool10";
		if("DEV".equalsIgnoreCase(host.getServerEnv())) {
			primaryVipHost = "devpgs10";
		} 
		else if("INT".equalsIgnoreCase(host.getServerEnv())) {
			primaryVipHost = "intpgs10";
		} 
		else if("ECC".equalsIgnoreCase(host.getServerEnv())) {
			primaryVipHost = "eccpgs10";
		} else if("USA".equalsIgnoreCase(host.getServerEnv())) {
			primaryVipHost = "usapgs30";
		} else { 
			primaryVipHost = "devtool10";
		}
		BufferStream recoveryBufferStream = new HeapBufferStream();
		PrintWriter recoveryWriter = new PrintWriter(recoveryBufferStream.getOutputStream());
		recoveryWriter.println("standby_mode = 'on'");
		recoveryWriter.println("primary_conninfo = 'host="+primaryVipHost+" port=5432 user=replica sslmode=require sslcert="+standbyPath+"/data/client.crt sslkey="+standbyPath+"/data/client.key sslrootcert="+standbyPath+"/data/root.crt'");
		recoveryWriter.println("restore_command = 'cp /backup/pg_xlog/%f %p'");
		recoveryWriter.flush();
		
		final Map<Pattern, String> hbaReplacements= new HashMap<Pattern, String>();
		String pgsIpRange = null;
		if("ECC".equalsIgnoreCase(host.getServerEnv())) {
			pgsIpRange = "192.168.4.0/25";
		} else if("USA".equalsIgnoreCase(host.getServerEnv())) {
			pgsIpRange = "192.168.71.0/24";
		} else { // DEV and INT
			pgsIpRange = "10.0.0.0/24";
		}
		hbaReplacements.put(Pattern.compile("<PGS_IP_RANGE>"), pgsIpRange);
		FromCvsUploadTask pghbaChange=new FromCvsUploadTask("server_app_config/PGS/pg_hba.conf",  "HEAD", standbyPath+"/data/pg_hba.conf", 0644,"postgres", "postgres", true) {
			@Override
			protected InputStream decorate(InputStream in) {
				if(hbaReplacements != null)
					in = new EncodingInputStream(new ReplacementsLineFilteringReader(new BufferedReader(new InputStreamReader(in)), "\n", hbaReplacements));
				return in;
			}
		};
		
		tasks.add(new OptionalTask("if [ `df -h | grep stdby|wc -l` != 0 ]; then echo 'Y'; fi", Pattern.compile("^Y\\s*"), false, 
				new FromCvsUploadTask("server_app_config/PGS/postgresql_standby.conf","HEAD", standbyPath+"/data/postgresql.conf", 0644, "postgres", "postgres", true),
				pghbaChange,
				new FromCvsUploadTask("server_app_config/PGS/pg_ident.conf", "HEAD",standbyPath+"/data/pg_ident.conf", 0644, "postgres", "postgres", true),
				new UnixUploadTask(recoveryBufferStream.getInputStream(), standbyPath+"/data","recovery.conf", 0644, "postgres", "postgres", true),
				new ExecuteTask("sudo su - postgres",
						"touch /opt/USAT/postgres_stdby/data/client.crt",
						"touch /opt/USAT/postgres_stdby/data/client.key"),
				new RenewFileCertTask(new Date(System.currentTimeMillis() + 30 * 24 * 60 * 60 * 1000L), standbyPath+"/data/client.crt",standbyPath+"/data/client.key", 0644, 0600, "postgres", "postgres", null, "replica", "USATAuthenticatedSession", "replica"),
				new ExecuteTask("sudo su",
						"chown -R postgres:postgres "+standbyPath,
						"chmod -R 700 "+standbyPath+"/data")));
		addCronJob(tasks);
		DeployTask[] allTask=new DeployTask[tasks.size()];
		return tasks.toArray(allTask);
	}
	/**
	 * Install pg_repack binary
	 * */
	protected void addInstallPgRepack(List<DeployTask> tasks){
		tasks.add(new OptionalTask("if [ ! -x /usr/pgsql-latest/lib/pg_repack.so ]; then echo 'Y'; fi", Pattern.compile("^Y\\s*"), false,
				new FromCvsUploadTask("server_app_config/Postgresql/pg_repack/pg_repack-1.2.0-beta1.zip", "HEAD", "pg_repack-1.2.0-beta1.zip", 0644),
				new ExecuteTask("sudo su",
								//"yum -y install readline*",
								"if [ -d /home/`logname`/pg_repack-1.2.0-beta1 ]; then /bin/rm -r /home/`logname`/pg_repack-1.2.0-beta1; fi",
								"cd /home/`logname`",
								"unzip pg_repack-1.2.0-beta1.zip",
								"cd pg_repack-1.2.0-beta1",
								"export PATH=$PATH:/usr/pgsql-latest/bin",
								"make",
								"make install")
				));
		tasks.add(new OptionalTask("if [ `/usr/pgsql-latest/bin/pg_ctl -D /opt/USAT/postgres/data status| grep PID| wc -l` != 0 ]; then echo 'Y'; fi", Pattern.compile("^Y\\s*"), false,
				new ExecuteTask("sudo su postgres",
								"psql -c \"CREATE EXTENSION pg_repack\" -d main")
				));
	}
	
	/**
	 * Install cron jobs
	 * */
	protected void addCronJob(List<DeployTask> tasks){
		CronUpdateTask cronUpdateTask = new CronUpdateTask("postgres", true);
		int pgPort=15432;
		cronUpdateTask.registerJob(Pattern.compile(".*/opt/USAT/postgres/bin/pgs_trigger_set_tablespace\\.sh.*"), new int[] { 0 }, new int[] { 4 }, new int[] { 1 }, null, null, "/opt/USAT/postgres/bin/pgs_trigger_set_tablespace.sh >> " + "/opt/USAT/postgres/logs/postgresqlbackup.log 2>&1", "# Monthly PGS set tablespace");
		cronUpdateTask.registerJob(Pattern.compile(".*/opt/USAT/postgres_stdby/archivelogs\\.sh.*"), new int[] { 9 }, null, null, null, null, "/opt/USAT/postgres_stdby/archivelogs.sh >> " + "/opt/USAT/postgres_stdby/archivelogs.log 2>&1", "# Rename and gzip rotated log files");
		cronUpdateTask.registerJob(Pattern.compile(".*/opt/USAT/postgres_stdby/bin/cleanup_dead_connections\\.sh.*"), new int[] { 0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55 }, null, null, null, null, "/opt/USAT/postgres_stdby/bin/cleanup_dead_connections.sh " + pgPort + " >> " + "/opt/USAT/postgres_stdby/logs/cleanup_dead_connections.log 2>&1", "# Clean up dead connections");
		cronUpdateTask.registerJob(Pattern.compile(".*/opt/USAT/postgres/bin/pgs_cal_replication_lag\\.sh.*"), new int[] { 0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55 }, null, null, null, null, "/opt/USAT/postgres/bin/pgs_cal_replication_lag.sh 2>&1", "# Calculate primary standby replication lag by bytes");
		
		tasks.add(cronUpdateTask);
	}

	@Override
	public String toString() {
		return getName();
	}
}
