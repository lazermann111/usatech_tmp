package com.usatech.tools.deploy.active;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.usalive.servlet.userreports.RegisterUserReportStep;
import java.net.UnknownHostException;

public class R52_Patch_1_ChangeSet extends MultiLinuxPatchChangeSet {
	public R52_Patch_1_ChangeSet() throws UnknownHostException {
		super();
		registerSource("usalive/src", RegisterUserReportStep.class, "REL_USALive_1_28_1", USATRegistry.USALIVE_APP);
		registerResource("usalive/xsl/ccs-data-layer.xml", "classes/", "REL_USALive_1_28_1", true, USATRegistry.USALIVE_APP);
	}

	@Override
	public String getName() {
		return "R52 Patch 1";
	}
}
