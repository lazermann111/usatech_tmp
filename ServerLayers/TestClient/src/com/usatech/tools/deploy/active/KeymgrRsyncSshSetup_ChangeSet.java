package com.usatech.tools.deploy.active;

import java.io.IOException;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.PgDeployMap;
import com.usatech.tools.deploy.RsaSshSetupTask;
import com.usatech.tools.deploy.USATRegistry;

/**
 * The task is part of AbstractLinuxChangeSet
 * @author yhe
 *
 */
public class KeymgrRsyncSshSetup_ChangeSet implements ChangeSet {

	@Override
	public String getName() {
		return "Setup primary keymgr rynch using replica to the standby host - Test Task";
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("KLS");
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		String primaryBackupDir=PgDeployMap.primaryBackupDirMap.get(host.getSimpleName());
		//String standbyBackupDir=PgDeployMap.standbyBackupDirMap.get(host.getSimpleName());
		Host standbyHost = PgDeployMap.standbyMap.get(host.getSimpleName());
		if(!"USA".equalsIgnoreCase(host.getServerEnv())) {
			// currently only prod is setup. we want to run on all other env
			return new DeployTask[] {
					new ExecuteTask(
							"sudo su",
							"if [ `egrep -c '^replica:' /etc/passwd` -eq 0 ]; then groupadd -g "+USATRegistry.REPLICA_LOCAL_USER_UID+" replica; useradd -g "+USATRegistry.REPLICA_LOCAL_USER_UID+" replica; fi",
							"test -d "+primaryBackupDir+" || (mkdir -p "+primaryBackupDir+" && chown replica:replica "+primaryBackupDir+")"
							),
					new RsaSshSetupTask(host, standbyHost, USATRegistry.KEYMGR.getUserName(),USATRegistry.KEYMGR.getAppUid(), "replica", USATRegistry.REPLICA_LOCAL_USER_UID)
			};
		}else{
			return new DeployTask[]{};
		}
		
	}

	@Override
	public String toString() {
		return getName();
	}
}
