package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.messagedata.MessageDataUtils;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.FromCvsUploadTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;

public class R56_Patch_4_ChangeSet extends MultiLinuxPatchChangeSet {
	public R56_Patch_4_ChangeSet() throws UnknownHostException {
		super();
		registerResource("DMS/resources/jsp/include/header.jsp", "web/jsp/include", "REL_R56_4", false, USATRegistry.DMS_APP);
		registerResource("usalive/web/device_configuration.html.jsp", "web", "REL_R56_4", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/mass_device_configuration_2.html.jsp", "web", "REL_R56_4", false, USATRegistry.USALIVE_APP);

		registerSource("ServerLayers/LayersCommon/src", ProcessingConstants.class, "REL_R56_4", USATRegistry.APP_LAYER);
		registerSource("ServerLayers/LayersCommon/src", ProcessingConstants.class, "REL_R56_4", USATRegistry.DMS_APP);
		registerSource("ServerLayers/LayersCommon/src", ProcessingConstants.class, "REL_R56_4", USATRegistry.NET_LAYER);

		registerSource("ServerLayers/LayersCommon/src", MessageDataUtils.class, "REL_R56_4", USATRegistry.APP_LAYER);
		registerSource("ServerLayers/LayersCommon/src", MessageDataUtils.class, "REL_R56_4", USATRegistry.DMS_APP);		
		registerSource("ServerLayers/LayersCommon/src", MessageDataUtils.class, "REL_R56_4", USATRegistry.NET_LAYER);
	}
	
	@Override
	protected void addPreHostTasks(Host host, List<DeployTask> tasks, PasswordCache cache) throws IOException {
			tasks.add(new FromCvsUploadTask("ServerLayers/LayersCommon/src/translations.properties", "REL_R56_4", "/opt/USAT/conf/translations.properties", 0640, "root", "usat", true, true));
	}
	
	@Override
	public String getName() {
		return "R56 Patch 4 - French Language";
	}
}
