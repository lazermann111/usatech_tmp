package com.usatech.tools.deploy.active;

import com.usatech.authoritylayer.USConnectApolloAuthorityTask;
import com.usatech.layers.common.HttpRequester;
import com.usatech.layers.common.constants.AuthPermissionActionCode;
import com.usatech.layers.common.messagedata.MessageData_C3;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

public class R54_Patch_8_ChangeSet extends MultiLinuxPatchChangeSet {
	public R54_Patch_8_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/AuthorityLayer/src", USConnectApolloAuthorityTask.class, "REL_authoritylayer_1_40_8", USATRegistry.OUTAUTH_LAYER);
		registerSource("ServerLayers/LayersCommon/src", HttpRequester.class, "REL_authoritylayer_1_40_8", USATRegistry.OUTAUTH_LAYER);

		Map<App, String> apps = new HashMap<>();
		apps.put(USATRegistry.OUTAUTH_LAYER, "REL_authoritylayer_1_40_8");
		apps.put(USATRegistry.DMS_APP, "REL_DMS_1_16_8");
		apps.put(USATRegistry.APP_LAYER, "REL_applayer_1_40_8");
		apps.put(USATRegistry.INAUTH_LAYER, "REL_authoritylayer_1_40_8");
		apps.put(USATRegistry.NET_LAYER, "REL_netlayer_1_40_8");
		apps.put(USATRegistry.POSM_LAYER, "REL_posmlayer_1_31_8");
		apps.put(USATRegistry.USALIVE_APP, "REL_USALive_1_30_8");
		apps.put(USATRegistry.KEYMGR, "REL_keymgr_2_21_8");
		apps.put(USATRegistry.RPTGEN_LAYER, "REL_report_generator_3_28_8");
		apps.put(USATRegistry.RPTREQ_LAYER, "REL_report_generator_3_28_8");
		apps.put(USATRegistry.PREPAID_APP, "REL_prepaid_1_17_8");
		apps.put(USATRegistry.LOADER, "REL_applayer_1_40_8");
		apps.put(USATRegistry.TRANSPORT_LAYER, "REL_report_generator_3_28_8");

		for (Map.Entry<App, String> entry : apps.entrySet()) {
			registerSource("ServerLayers/LayersCommon/src", AuthPermissionActionCode.class, entry.getValue(), entry.getKey());
			registerSource("ServerLayers/LayersCommon/src", MessageData_C3.class, entry.getValue(), entry.getKey());
		}
	}

	@Override
	public String getName() {
		return "R54 Patch 8";
	}
}
