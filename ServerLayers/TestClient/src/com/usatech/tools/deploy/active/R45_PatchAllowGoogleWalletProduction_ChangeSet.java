package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.applayer.AuthorizeTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R45_PatchAllowGoogleWalletProduction_ChangeSet extends MultiLinuxPatchChangeSet {
	public R45_PatchAllowGoogleWalletProduction_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/AppLayer/src", AuthorizeTask.class, "REL_applayer_1_31_2", USATRegistry.APP_LAYER);
	}

	@Override
	public String getName() {
		return "R45 Patch to Allow Google Wallet - Production";
	}
}
