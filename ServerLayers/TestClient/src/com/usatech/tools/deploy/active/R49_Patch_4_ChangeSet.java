package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R49_Patch_4_ChangeSet extends MultiLinuxPatchChangeSet {
	public R49_Patch_4_ChangeSet() throws UnknownHostException {
		super();

		registerResource("usalive/web/rma_rental_for_credit_display.jsp", "web", "REL_USALive_1_25_4", false, USATRegistry.USALIVE_APP);
	}

	@Override
	public String getName() {
		return "R49 Patch 4";
	}
}
