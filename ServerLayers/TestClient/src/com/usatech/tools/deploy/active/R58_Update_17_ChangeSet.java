package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.usalive.servlet.LoginStep;

public class R58_Update_17_ChangeSet extends MultiLinuxPatchChangeSet {
	public R58_Update_17_ChangeSet() throws UnknownHostException {
		super();
		registerSource("usalive/src", LoginStep.class, "BRN_USAT_1312", USATRegistry.USALIVE_APP);
	}
	
	@Override
	public String getName() {
		return "R58 Update 17 - USALive password expiration issue";
	}
}
