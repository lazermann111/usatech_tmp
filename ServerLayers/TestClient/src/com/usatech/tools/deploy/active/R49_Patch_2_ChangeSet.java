package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.applayer.InboundFileTransferTask;
import com.usatech.posm.tasks.RiskCheckTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R49_Patch_2_ChangeSet extends MultiLinuxPatchChangeSet {
	public R49_Patch_2_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/POSMLayer/src/main", RiskCheckTask.class, "REL_posmlayer_1_26_2", USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/AppLayer/src", InboundFileTransferTask.class, "REL_applayer_1_35_2", USATRegistry.LOADER);
		registerLib("ThirdPartyJavaLibraries/lib/dom4j-1.6.1.jar", "HEAD", USATRegistry.RPTGEN_LAYER);
	}
	
	@Override
	public String getName() {
		return "R49 Patch 2";
	}
}
