package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.applayer.AuthorizeTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R57_Patch_14_ChangeSet extends MultiLinuxPatchChangeSet {
	public R57_Patch_14_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/AppLayer/src", AuthorizeTask.class, "REL_R57_14", USATRegistry.APP_LAYER);
		registerResource("ServerLayers/LayersCommon/src/common-data-layer.xml", "classes", "REL_R57_14", false, USATRegistry.INAUTH_LAYER, USATRegistry.LOADER, USATRegistry.USALIVE_APP);
	}
	
	@Override
	public String getName() {
		return "R57 Patch 14 - Storing authorizations in database";
	}
}
