package com.usatech.tools.deploy.active;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import simple.io.EncodingInputStream;
import simple.io.ReplacementsLineFilteringReader;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.FromCvsUploadTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.PostgresSetupTask;

/**
 * @author yhe
 *
 */
public class PGSPrimary_ChangeSet implements ChangeSet {
	

	@Override
	public String getName() {
		return "PGS Primary setup Task";
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("PGS");
	}
	
	
	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		List<DeployTask> tasks =new ArrayList<DeployTask>();
		
		//Primary tasks
		tasks.add(new ExecuteTask("sudo su",
				"if [ ! -d /opt/USAT/postgres/T2tblspace ]; then ln -s /T2tblspace /opt/USAT/postgres/T2tblspace ; fi"
				));
		tasks.add(new ExecuteTask("sudo su",
				"if [ ! -d /opt/USAT/postgres/T3tblspace ]; then ln -s /T3tblspace /opt/USAT/postgres/T3tblspace ; fi"
				));
		String primaryPath="/opt/USAT/postgres";
		final Map<Pattern, String> hbaReplacements= new HashMap<Pattern, String>();
		String klsIpRange = null;
		if("ECC".equalsIgnoreCase(host.getServerEnv())) {
			klsIpRange = "192.168.4.0/25";
		} else if("USA".equalsIgnoreCase(host.getServerEnv())) {
			klsIpRange = "192.168.71.0/24";
		} else { // DEV and INT
			klsIpRange = "10.0.0.0/24";
		}
		hbaReplacements.put(Pattern.compile("<PGS_IP_RANGE>"), klsIpRange);
		tasks.add(new FromCvsUploadTask("server_app_config/PGS/postgresql_primary.conf", "HEAD", primaryPath+"/data/postgresql.conf", 0644, "postgres", "postgres", true));
		FromCvsUploadTask pghbaChange=new FromCvsUploadTask("server_app_config/PGS/pg_hba.conf.replica",  "HEAD", "/opt/USAT/postgres/data/pg_hba.conf", 0644,"postgres", "postgres", true) {
			@Override
			protected InputStream decorate(InputStream in) {
				if(hbaReplacements != null)
					in = new EncodingInputStream(new ReplacementsLineFilteringReader(new BufferedReader(new InputStreamReader(in)), "\n", hbaReplacements));
				return in;
			}
		};
		tasks.add(pghbaChange);
		PostgresSetupTask pst = new PostgresSetupTask("/opt/USAT/postgres", "postgres", 5432);
		pst.registerRole("replica", new String[] { "SUPERUSER", "CREATEDB", "CREATEROLE", "REPLICATION", "LOGIN" });
		tasks.add(pst);
		//tasks.add(new ExecuteTask("sudo su",
				//"clusvcadm -R primary"));
		
		DeployTask[] allTask=new DeployTask[tasks.size()];
		return tasks.toArray(allTask);
	}
	
	@Override
	public String toString() {
		return getName();
	}
}
