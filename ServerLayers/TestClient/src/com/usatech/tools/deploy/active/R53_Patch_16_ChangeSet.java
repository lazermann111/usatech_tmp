package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.ccs.CampaignBlastTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R53_Patch_16_ChangeSet extends MultiLinuxPatchChangeSet {
	public R53_Patch_16_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ReportGenerator/src", CampaignBlastTask.class, "REL_report_generator_3_27_16", USATRegistry.RPTGEN_LAYER);
	}
	
	@Override
	public String getName() {
		return "R53 Patch 16";
	}
}
