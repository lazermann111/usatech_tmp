package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

import simple.falcon.engine.standard.CSVGenerator;

public class R58_Update_7_ChangeSet extends MultiLinuxPatchChangeSet {
	public R58_Update_7_ChangeSet() throws UnknownHostException {
		super();
		registerSource("Falcon/src", CSVGenerator.class, "REL_R58_7", USATRegistry.RPTGEN_LAYER, USATRegistry.USALIVE_APP);
	}
	
	@Override
	public String getName() {
		return "R58 Update 7 - CSV generator";
	}
}
