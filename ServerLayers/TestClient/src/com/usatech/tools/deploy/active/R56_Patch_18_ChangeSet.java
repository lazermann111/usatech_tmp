package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R56_Patch_18_ChangeSet extends MultiLinuxPatchChangeSet {
	public R56_Patch_18_ChangeSet() throws UnknownHostException {
		super();
		registerResource("DMS/resources/jsp/terminals/updateServiceFees.jsp", "web/jsp/terminals", "REL_R56_18", false, USATRegistry.DMS_APP);
	}
	
	@Override
	public String getName() {
		return "R56 Patch 18 - Update Service Fees. Daily available.";
	}
}
