package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.device.DbVerizonLocationUpdateService;
import com.usatech.device.VerizonLocationUpdateService;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R57_Update_25_ChangeSet extends MultiLinuxPatchChangeSet {
	public R57_Update_25_ChangeSet() throws UnknownHostException {
		super();		
		registerSource("ReportGenerator/src", DbVerizonLocationUpdateService.class, "REL_R57_25", USATRegistry.RPTGEN_LAYER);
		registerSource("ReportGenerator/src", VerizonLocationUpdateService.class, "REL_R57_25", USATRegistry.TRANSPORT_LAYER);
		registerResource("ReportGenerator/src/rgr-data-layer.xml", "classes", "REL_R57_25", true, USATRegistry.RPTGEN_LAYER);
	}
	
	@Override
	public String getName() {
		return "R57 Update 25 - Duplicated Verizon MDN and Invalid Credentials";
	}
}
