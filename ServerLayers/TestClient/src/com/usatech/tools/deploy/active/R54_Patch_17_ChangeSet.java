package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R54_Patch_17_ChangeSet extends MultiLinuxPatchChangeSet {
	public R54_Patch_17_ChangeSet() throws UnknownHostException {
		super();
		registerResource("usalive/web/images/login-banner-ad.jpg", "web/images", "REL_USALive_1_31_17", false, USATRegistry.HTTPD_WEB);
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.USALIVE_APP);
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.VERIZON_APP);
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.HOTCHOICE_APP);

		registerResource("usalive/web/images/login-banner-ad.jpg", "web/images", "REL_USALive_1_31_17", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/login.html.jsp", "web/", "REL_USALive_1_31_17", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/customerreporting_web/home.jsp", "web/", "REL_USALive_1_31_17", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/customerreporting_web/login.jsp", "web/", "REL_USALive_1_31_17", false, USATRegistry.USALIVE_APP);
	}
	
	@Override
	public String getName() {
		return "R54 Patch 17 - Win a Tesla!";
	}
}
