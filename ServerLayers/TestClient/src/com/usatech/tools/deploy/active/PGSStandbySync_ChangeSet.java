package com.usatech.tools.deploy.active;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import simple.io.BufferStream;
import simple.io.EncodingInputStream;
import simple.io.HeapBufferStream;
import simple.io.ReplacementsLineFilteringReader;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.FromCvsUploadTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.OptionalTask;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.RenewFileCertTask;
import com.usatech.tools.deploy.UnixUploadTask;

/**
 * This task has no postgres binary update and initial dir and scripts setup.
 * See 'PGS Standby setup Task' for original setup
 * The main goal is sync up the data for stdby with the primary and restart the stream process.
 * @author yhe
 *
 */
public class PGSStandbySync_ChangeSet implements ChangeSet {
	

	@Override
	public String getName() {
		return "PGS Standby Sync Task";
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("PGS");
	}
	
	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		List<DeployTask> tasks =new ArrayList<DeployTask>();
		
		String standbyPath="/opt/USAT/postgres_stdby";
		String primaryVipHost="devtool10";
		if("DEV".equalsIgnoreCase(host.getServerEnv())) {
			primaryVipHost = "devpgs10";
		} 
		else if("INT".equalsIgnoreCase(host.getServerEnv())) {
			primaryVipHost = "intpgs10";
		} 
		else if("ECC".equalsIgnoreCase(host.getServerEnv())) {
			primaryVipHost = "eccpgs10";
		} else if("USA".equalsIgnoreCase(host.getServerEnv())) {
			primaryVipHost = "usapgs30";
		} else { 
			primaryVipHost = "devtool10";
		}
		BufferStream recoveryBufferStream = new HeapBufferStream();
		PrintWriter recoveryWriter = new PrintWriter(recoveryBufferStream.getOutputStream());
		recoveryWriter.println("standby_mode = 'on'");
		recoveryWriter.println("primary_conninfo = 'host="+primaryVipHost+" port=5432 user=replica sslmode=require sslcert="+standbyPath+"/data/client.crt sslkey="+standbyPath+"/data/client.key sslrootcert="+standbyPath+"/data/root.crt'");
		recoveryWriter.println("restore_command = 'cp /backup/pg_xlog/%f %p'");
		recoveryWriter.flush();
		
		final Map<Pattern, String> hbaReplacements= new HashMap<Pattern, String>();
		String pgsIpRange = null;
		if("ECC".equalsIgnoreCase(host.getServerEnv())) {
			pgsIpRange = "192.168.4.0/25";
		} else if("USA".equalsIgnoreCase(host.getServerEnv())) {
			pgsIpRange = "192.168.71.0/24";
		} else { // DEV and INT
			pgsIpRange = "10.0.0.0/24";
		}
		hbaReplacements.put(Pattern.compile("<PGS_IP_RANGE>"), pgsIpRange);
		FromCvsUploadTask pghbaChange=new FromCvsUploadTask("server_app_config/PGS/pg_hba.conf",  "HEAD", standbyPath+"/data/pg_hba.conf", 0644,"postgres", "postgres", true) {
			@Override
			protected InputStream decorate(InputStream in) {
				if(hbaReplacements != null)
					in = new EncodingInputStream(new ReplacementsLineFilteringReader(new BufferedReader(new InputStreamReader(in)), "\n", hbaReplacements));
				return in;
			}
		};
		
		tasks.add(new OptionalTask("if [ `df -h | grep stdby|wc -l` != 0 ]; then echo 'Y'; fi", Pattern.compile("^Y\\s*"), false, 
				new FromCvsUploadTask("server_app_config/PGS/postgresql_standby.conf","HEAD", standbyPath+"/data/postgresql.conf", 0644, "postgres", "postgres", true),
				pghbaChange,
				new FromCvsUploadTask("server_app_config/PGS/pg_ident.conf", "HEAD",standbyPath+"/data/pg_ident.conf", 0644, "postgres", "postgres", true),
				new UnixUploadTask(recoveryBufferStream.getInputStream(), standbyPath+"/data","recovery.conf", 0644, "postgres", "postgres", true),
				new ExecuteTask("sudo su - postgres",
						"touch /opt/USAT/postgres_stdby/data/client.crt",
						"touch /opt/USAT/postgres_stdby/data/client.key"),
				//new RenewFileCertTask(new Date(System.currentTimeMillis() + 30 * 24 * 60 * 60 * 1000L), standbyPath+"/data/client.crt",standbyPath+"/data/client.key", 0644, 0600, "postgres", "postgres", null, "replica", "USATAuthenticatedSession", "replica"),
				new RenewFileCertTask(new Date(Long.MAX_VALUE), standbyPath+"/data/client.crt",standbyPath+"/data/client.key", 0644, 0600, "postgres", "postgres", null, "replica", "USATAuthenticatedSession", "replica"),
				
				new ExecuteTask("sudo su",
						"chown -R postgres:postgres "+standbyPath,
						"chmod -R 700 "+standbyPath+"/data")));
		DeployTask[] allTask=new DeployTask[tasks.size()];
		return tasks.toArray(allTask);
	}

	@Override
	public String toString() {
		return getName();
	}
}
