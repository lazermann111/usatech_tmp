package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.usalive.report.AbstractRunCustomFolioStep;

public class R50_Patch_11_ChangeSet extends MultiLinuxPatchChangeSet {
	public R50_Patch_11_ChangeSet() throws UnknownHostException {
		super();
		registerSource("usalive/src/", AbstractRunCustomFolioStep.class, "REL_USALive_1_26_11", USATRegistry.USALIVE_APP);
	}
	
	@Override
	public String getName() {
		return "R50 Patch 11";
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("APP");
	}
	
	@Override
	public String toString() {
		return getName();
	}
}
