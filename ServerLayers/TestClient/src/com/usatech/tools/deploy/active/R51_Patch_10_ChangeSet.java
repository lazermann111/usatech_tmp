package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R51_Patch_10_ChangeSet extends MultiLinuxPatchChangeSet {
	public R51_Patch_10_ChangeSet() throws UnknownHostException {
		super();
		registerResource("usalive/xsl/rma-data-layer.xml", "classes/", "REL_USALive_1_27_10", true, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/rma_device_search.html.jsp", "web", "REL_USALive_1_27_10", false, USATRegistry.USALIVE_APP);
	}

	@Override
	public String getName() {
		return "R51 Patch 10";
	}
}
