package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.networklayer.processors.EnqueueOnlyProcessor_Auth;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class USConnectDemo_ChangeSet extends MultiLinuxPatchChangeSet {
	public USConnectDemo_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/NetLayer/src", EnqueueOnlyProcessor_Auth.class, "REL_USConnectDemo", USATRegistry.NET_LAYER);
	}

	@Override
	public String getName() {
		return "USConnect Demo";
	}
}
