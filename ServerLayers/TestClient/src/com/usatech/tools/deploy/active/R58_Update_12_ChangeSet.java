package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

import simple.mq.peer.DbPoller;

public class R58_Update_12_ChangeSet extends MultiLinuxPatchChangeSet {
	public R58_Update_12_ChangeSet() throws UnknownHostException {
		super();
		registerSource("Simple1.5/src", DbPoller.class, "REL_R58_12", USATRegistry.LOADER);
	}
	
	@Override
	public String getName() {
		return "R58 Update 12 - MST server performance";
	}
}
