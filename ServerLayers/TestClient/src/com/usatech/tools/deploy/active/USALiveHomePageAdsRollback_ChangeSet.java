package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class USALiveHomePageAdsRollback_ChangeSet extends MultiLinuxPatchChangeSet {
	public USALiveHomePageAdsRollback_ChangeSet() throws UnknownHostException {
		super();
		registerResource("usalive/web/login.html.jsp", "web/", "1.11", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/customerreporting_web/home.jsp", "web/", "1.30", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/customerreporting_web/login.jsp", "web/", "1.45", false, USATRegistry.USALIVE_APP);
	}
	
	@Override
	public String getName() {
		return "Rollback Usalive HomePage Ads 02012017";
	}
}
