package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.usalive.web.RMAType;

public class Rollback_R49_Patch_5_ChangeSet extends MultiLinuxPatchChangeSet {
	public Rollback_R49_Patch_5_ChangeSet() throws UnknownHostException {
		super();
		registerResource("usalive/customerreporting_web/home.jsp", "web", "REL_USALive_1_25_3", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/include/rmaBottom.jsp", "web/include", "REL_USALive_1_25_0", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/rma_create_device.html.jsp", "web", "REL_USALive_1_25_0", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/rma_receipt.jsp", "web", "REL_USALive_1_25_0", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/rma_att.html.jsp", "web", "REL_USALive_1_25_0", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/rma_device_search.html.jsp", "web", "REL_USALive_1_25_0", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/rma_replacement.jsp", "web", "REL_USALive_1_25_0", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/rma_att_display.jsp", "web", "REL_USALive_1_25_0", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/rma_receipt_usalive.html.jsp", "web", "REL_USALive_1_25_0", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/rma_view.html.jsp", "web", "REL_USALive_1_25_0", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/xsl/rma-data-layer.xml", "classes/", "REL_USALive_1_25_0", true, USATRegistry.USALIVE_APP);
		registerSource("usalive/src", RMAType.class, "REL_USALive_1_25_0", USATRegistry.USALIVE_APP);
	}

	@Override
	public String getName() {
		return "Rollback R49 Patch 5";
	}
}
