package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.regex.Pattern;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.DeployTaskInfo;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.FromCvsUploadTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.HostImpl;
import com.usatech.tools.deploy.OptionalTask;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.Registry;
import com.usatech.tools.deploy.USATHelper;
import com.usatech.tools.deploy.USATRegistry;

import simple.io.Log;
import simple.text.RegexUtils;
import simple.text.StringUtils;
import simple.util.CollectionUtils;
/**
 * README:
 * For upgrading postgres 9.3 to 9.5, there are some issues that needs to be addressed first before using this class:
 * 
 * 1. postgis 2.1 is not compatible with 9.5 and pg_upgrade will fail giving 
 * ERROR:  could not access file "$libdir/rtpostgis-2.1": No such file or directory
solution: remove the postgis reference in the database for schema app_1 to app_x, make sure remove all of them, otherwise it will fail at pg_upgrade, login and do the following:
DROP EXTENSION postgis CASCADE;
drop table app_cluster.geocode_country;
drop table app_cluster.geocode_source;

Then remove the rpms that has postgis:
rpm -e --nodeps postgis2_93-2.1.8-1.rhel6.x86_64
rpm -e --nodeps postgis2_93-client-2.1.8-1.rhel6.x86_64

without removing rpms, pg_upgrade will fail 

	2. checkpoint_segments in postgresql.conf is deprecated, as documented here: https://www.postgresql.org/docs/9.5/static/release-9-5.html
	Replace configuration parameter checkpoint_segments with min_wal_size and max_wal_size (Heikki Linnakangas)
solution: cp the original /opt/USAT/postgres/data/postgresql.conf to /home/postgresql.conf with owner postgres and 644 and replace 
checkpoint_segments = 32 to
max_wal_size = 1536MB

NOTE: 
For boxes sudo su - postgres will give exit code error 1 via echo $?
make sure /etc/passwd postgres user has /home/postgres as its home dir
postgres:x:26:26:PostgreSQL Server:/home/postgres:/bin/bash

 * @author yhe
 *
 */
public class PostgresMajorUpgradeStandAlone_ChangeSet implements ChangeSet {
	private static final Log log = Log.getLog();
	protected final String upgradeVersion = "9.5.4";
	protected final Registry registry;

	public PostgresMajorUpgradeStandAlone_ChangeSet() throws UnknownHostException {
		super();
		this.registry = new USATRegistry();
	}

	@Override
	public boolean isApplicable(Host host) {
		return true;
	}
	
	public static void addInstallPgMajor95BinariesTasks(Host host, String version, String workDir, List<DeployTask> tasks) throws IOException {
		String[] parts = StringUtils.split(version, '.', 3);
		if(parts.length != 3)
			throw new IllegalArgumentException("Invalid version '" + version + "'");
		tasks.add(new ExecuteTask("sudo su", 
				"rpm -Uh /root/postgresql95/postgresql95-9.5.4-1PGDG.rhel6.x86_64.rpm /root/postgresql95/postgresql95-contrib-9.5.4-1PGDG.rhel6.x86_64.rpm /root/postgresql95/postgresql95-devel-9.5.4-1PGDG.rhel6.x86_64.rpm /root/postgresql95/postgresql95-libs-9.5.4-1PGDG.rhel6.x86_64.rpm /root/postgresql95/postgresql95-server-9.5.4-1PGDG.rhel6.x86_64.rpm",
				"unlink /usr/pgsql-latest", 
				"ln -s /usr/pgsql-" +  parts[0] + '.' + parts[1] + " /usr/pgsql-latest"));
		if(host.isServerType("PGS")){
			USATHelper.addOdbcLinkTasks(host, null, tasks, workDir);
			tasks.add(new OptionalTask("if [ ! -x /usr/pgsql-latest/lib/pg_repack.so ]; then echo 'Y'; fi", Pattern.compile("^Y\\s*"), false,
					new FromCvsUploadTask("server_app_config/Postgresql/pg_repack/pg_repack-1.3.3.zip", "HEAD", "pg_repack-1.3.3.zip", 0644),
					new ExecuteTask("sudo su",
									//"yum -y install readline*",
									"if [ -d /home/`logname`/pg_repack-1.3.3 ]; then /bin/rm -r /home/`logname`/pg_repack-1.3.3; fi",
									"cd /home/`logname`",
									"unzip pg_repack-1.3.3.zip",
									"cd pg_repack-1.3.3",
									"export PATH=$PATH:/usr/pgsql-latest/bin",
									"make",
									"make install")
					));
		}
	}
	
	protected Host getOtherHost(Host host) {
		char[] array = host.getFullName().toCharArray();
		int pos = CollectionUtils.iterativeSearch(array, '.', 0, array.length);
		if(pos < 1)
			pos = array.length;
		switch(array[pos - 1]) {
			case '1':
				array[pos - 1] = '2';
				break;
			case '2':
				array[pos - 1] = '1';
				break;
			default:
				throw new IllegalArgumentException("Can't determine other host for '" + host.getSimpleName() + "'");
		}
		return new HostImpl(host.getServerEnv(), host.getServerTypes().toArray(new String[0]), new String(array));
	}
	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		// upgrade
		final Pattern fullVersionRegex = Pattern.compile(RegexUtils.escape(upgradeVersion));
		final Pattern majorVersionRegex = Pattern.compile(StringUtils.join(StringUtils.split(upgradeVersion, '.'), "\\.", 0, 2));
		final BitSet ALLOW_0_1_BITSET = new BitSet();
		ALLOW_0_1_BITSET.set(0);
		ALLOW_0_1_BITSET.set(1);
		DeployTask pgUpgradeTask = new DeployTask() {
			@Override
			public String getDescription(DeployTaskInfo deployTaskInfo) {
				return "Upgrading Postgres to " + upgradeVersion;
			}

			@Override
			public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
				String notifyUser="yhe@usatech.com";
				log.info("PostgresMajorUpgradeStandAlone_ChangeSet start notifyUser="+notifyUser);
				if(notifyUser!=null){
					ExecuteTask.executeCommands(deployTaskInfo, "sudo su - postgres", 
						"echo \"[`date`] PostgresMajorUpgradeStandAlone_ChangeSet start\"| mail -s \"PostgresMajorUpgradeStandAlone_ChangeSet start\" \""+notifyUser+"\"",
						"exit");
				}
				StringBuilder desc = new StringBuilder();
				deployTaskInfo.interaction.printf("Determining existing state of binaries and data");
				String[] commands = new String[] {
					"/usr/pgsql-latest/bin/pg_config --version | cut -d ' ' -f 2", 
					"sudo su", 
					"if [ -f /opt/USAT/postgres/data/PG_VERSION -o `df /data | grep -c 'primaryvg'` -eq 0 ]; then cat /opt/USAT/postgres/data/PG_VERSION; else cat /opt/USAT/postgres/old_data/PG_VERSION; fi"
				};
				
				String[] results = ExecuteTask.executeCommands(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.getSsh(), ALLOW_0_1_BITSET, commands);
				String localBinariesVersion = results[0].trim();
				String primaryDataVersion=null;
				
				if(!results[results.length - 1].endsWith("No such file or directory")) {
					primaryDataVersion = results[results.length - 1].trim();
				} 
				boolean upgradeLocal = !fullVersionRegex.matcher(localBinariesVersion).matches();
				boolean upgradePrimaryData = !majorVersionRegex.matcher(primaryDataVersion).matches();
				log.info("localBinariesVersion="+localBinariesVersion);
				log.info("primaryDataVersion="+primaryDataVersion);
				log.info("upgradeLocal="+upgradeLocal);
				log.info("upgradePrimaryData="+upgradePrimaryData);
				String workDir = "/home/`logname`/pg_update_" + upgradeVersion;
					// upgrade binaries on primary
				ExecuteTask.executeCommands(deployTaskInfo, "if [ ! -d " + workDir + " ]; then mkdir -p " + workDir + "; fi");
				if(upgradeLocal) {
					List<DeployTask> tasks = new ArrayList<>();
					addInstallPgMajor95BinariesTasks(host, upgradeVersion, workDir, tasks);
					for(DeployTask task : tasks)
							task.perform(deployTaskInfo);
					desc.append("; Upgraded Primary binaries to " + upgradeVersion);
				}
				log.info("upgrade binaries on primary done");
				// upgrade primary data (if necessary)
				if(upgradePrimaryData) {
					List<String> subcommands = new ArrayList<>();
					USATHelper.addUpgradePgDataCommands(host, "postgres", true, "/home/postgres/postgresql.conf", subcommands, notifyUser);
					ExecuteTask.executeCommands(deployTaskInfo, subcommands);
					desc.append("; Upgraded primary data to " + upgradeVersion);
				} 
				log.info("upgrade primary data done");
				return desc.toString();
			}
		};
		return new DeployTask[] { pgUpgradeTask };
	}

	@Override
	public String getName() {
		return "Postgres Upgrade Major StandAlone";
	}

	@Override
	public String toString() {
		return getName();
	}
}
