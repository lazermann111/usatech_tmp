package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.CvsPullTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.DeployTaskInfo;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.UploadCoteTask;

public class CanteenPrepaidBranding_ChangeSet implements ChangeSet {
	public CanteenPrepaidBranding_ChangeSet() {
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		return getTasks(new DeployTaskInfo(null, host, null, cache, null));
	}

	public DeployTask[] getTasks(DeployTaskInfo deployTaskInfo) throws IOException {
		List<DeployTask> tasks = new ArrayList<DeployTask>();
		if(deployTaskInfo.host.isServerType("ODB")) {
			String subdomain;
			if("USA".equalsIgnoreCase(deployTaskInfo.host.getServerEnv()))
				subdomain = "getmore.usatech.com";
			else if("LOCAL".equalsIgnoreCase(deployTaskInfo.host.getServerEnv()))
				subdomain = "localhost"; // java.net.Inet4Address.getLocalHost().getCanonicalHostName();
			else
				subdomain = "getmore-" + deployTaskInfo.host.getServerEnv().toLowerCase() + ".usatech.com";
			String brandingDir = "canteen";
			tasks.add(new UploadCoteTask("top_logo", "image/png", subdomain + "/" + brandingDir, new CvsPullTask("Prepaid/branded/canteen/images/icon-logo_canteen.png", "HEAD")));
			tasks.add(new UploadCoteTask("top_logo_welcome", "image/png", subdomain + "/" + brandingDir, new CvsPullTask("Prepaid/branded/canteen/images/icon-logo_canteen.png", "HEAD")));
			tasks.add(new UploadCoteTask("extra_style", "text/css", subdomain + "/" + brandingDir, new CvsPullTask("Prepaid/branded/canteen/css/canteen.css", "HEAD")));
			// tasks.add(new UploadCoteTask("bottom_logo", "image/png", subdomain + "/" + brandingDir, new CvsPullTask("Prepaid/branded/canteen/images/logo-pepi-footer200x80.png", "HEAD")));
			tasks.add(new UploadCoteTask("home_image", "image/png", subdomain + "/" + brandingDir, new CvsPullTask("Prepaid/branded/canteen/images/home-logo_canteen.png", "HEAD")));
			tasks.add(new UploadCoteTask("cash_icon", "image/png", subdomain + "/" + brandingDir, new CvsPullTask("Prepaid/branded/canteen/images/icon-cash60x50_canteen.png", "HEAD")));
			tasks.add(new UploadCoteTask("sale_icon", "image/png", subdomain + "/" + brandingDir, new CvsPullTask("Prepaid/branded/canteen/images/icon-sale60x50_canteen.png", "HEAD")));
			tasks.add(new UploadCoteTask("new_icon", "image/png", subdomain + "/" + brandingDir, new CvsPullTask("Prepaid/branded/canteen/images/icon-new60x50_canteen.png", "HEAD")));
			// tasks.add(new UploadCoteTask("new_user_image", "image/png", subdomain + "/" + brandingDir, new CvsPullTask("Prepaid/branded/canteen/images/logo-pepi-signup150x60.png", "HEAD")));
			// tasks.add(new UploadCoteTask("email_logo", "image/png", subdomain + "/" + brandingDir, new CvsPullTask("Prepaid/branded/canteen/images/more-logo315x40_pepi.png", "HEAD")));
		}
		return tasks.toArray(new DeployTask[tasks.size()]);
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("ODB");
	}

	@Override
	public String getName() {
		return "Canteen Prepaid Branding";
	}

	@Override
	public String toString() {
		return getName();
	}
}
