package com.usatech.tools.deploy.active;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import simple.io.EncodingInputStream;
import simple.io.ReplacementsLineFilteringReader;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.FromCvsUploadTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.tools.deploy.UploadTask;

public class EPortGO_ChangeSet implements ChangeSet {
	protected String version = "1.0.0";
	@Override
	public String getName() {
		return "ePort GO Install";
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("WEB");
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		List<DeployTask> tasks = new ArrayList<DeployTask>();
		String installFile = "eport-go-" + version.replace('.', '_') + ".tar.gz";
		String appDir = "/opt/USAT/httpd/eportgo";
		// tasks.add(new FromCvsUploadTask("ApplicationBuilds/ePortGO/" + installFile, "HEAD", installFile, 0644));
		tasks.add(new UploadTask(new File(USATRegistry.buildsDir, "ePortGO/" + installFile), 0644));
		final Map<Pattern, String> replacements = new HashMap<Pattern, String>();
		replacements.put(Pattern.compile("<NON_PROD_COMMENT>"), ("USA".equalsIgnoreCase(host.getServerEnv()) ? "" : "#"));
		replacements.put(Pattern.compile("<ENV_SUFFIX>"), ("USA".equalsIgnoreCase(host.getServerEnv()) ? "" : "-" + host.getServerEnv().toLowerCase()));
		replacements.put(Pattern.compile("<ENV_PREFIX>"), ("USA".equalsIgnoreCase(host.getServerEnv()) ? "www" : host.getServerEnv().toLowerCase()));
		tasks.add(new FromCvsUploadTask("server_app_config/WEB/Httpd/conf.d/eportgo.conf", "HEAD", appDir + "/../conf.d/eportgo.conf", 0644, "bin", "bin", true) {
			@Override
			protected InputStream decorate(InputStream in) {
				return new EncodingInputStream(new ReplacementsLineFilteringReader(new BufferedReader(new InputStreamReader(in)), "\n", replacements));
			}
		});
		tasks.add(new ExecuteTask("sudo su",
				"mkdir -p " + appDir,
				"chown bin:webdev " + appDir,
				"chmod 0775 " + appDir,
				"cd " + appDir,
				"/bin/rm -rf *",
				"gunzip -c /home/`logname`/" + installFile + " | tar xvf -",
				"kill -USR1 `cat " + appDir + "/../logs/httpd.pid`"));
		return tasks.toArray(new DeployTask[tasks.size()]);
	}

	@Override
	public String toString() {
		return getName();
	}
}
