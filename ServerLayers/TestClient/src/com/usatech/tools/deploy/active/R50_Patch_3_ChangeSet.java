package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R50_Patch_3_ChangeSet extends MultiLinuxPatchChangeSet {
	public R50_Patch_3_ChangeSet() throws UnknownHostException {
		super();
		registerResource("usalive/web/rma_rental_for_credit_display.jsp", "web", "REL_USALive_1_26_3", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/rma_all_device_display.jsp", "web", "REL_USALive_1_26_3", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/rma_att_display.jsp", "web", "REL_USALive_1_26_3", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/xsl/rma-data-layer.xml", "classes/", "REL_USALive_1_26_3", true, USATRegistry.USALIVE_APP);
	}

	@Override
	public String getName() {
		return "R50 Patch 3";
	}
}
