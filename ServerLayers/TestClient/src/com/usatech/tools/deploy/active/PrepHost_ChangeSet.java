package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;

public class PrepHost_ChangeSet extends AbstractLinuxChangeSet {
	public PrepHost_ChangeSet() throws UnknownHostException {
		super();
	}

	@Override
	public boolean isApplicable(Host host) {
		return true;
	}

	@Override
	protected void registerApps() {
	}


	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareHostTasks(host, tasks, cache);
		addPostAppsTasks(tasks);
	}
	
	@Override
	protected void addTasks(Host host, final App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
	}

	@Override
	public String getName() {
		return "Prepare Host";
	}
}
