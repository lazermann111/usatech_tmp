package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.prepaid.PSRequestHandler;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R56_Patch_17_ChangeSet extends MultiLinuxPatchChangeSet {
	public R56_Patch_17_ChangeSet() throws UnknownHostException {
		super();
		registerResource("Prepaid/src/prepaid-data-layer.xml", "classes/", "REL_R56_17", false, USATRegistry.PREPAID_APP);
		registerSource("Prepaid/src", PSRequestHandler.class, "REL_R56_17", USATRegistry.PREPAID_APP);		
	}
	
	@Override
	public String getName() {
		return "R56 Patch 17 - MORE Card Activation from ECRS Kiosk";
	}
}
