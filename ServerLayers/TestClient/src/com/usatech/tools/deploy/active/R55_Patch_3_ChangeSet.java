package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.dms.terminal.UpdateTerminalsStep;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R55_Patch_3_ChangeSet extends MultiLinuxPatchChangeSet {
	public R55_Patch_3_ChangeSet() throws UnknownHostException {
		super();
		registerSource("DMS/src", UpdateTerminalsStep.class, "REL_DMS_1_17_3", USATRegistry.DMS_APP);
	}
	
	@Override
	public String getName() {
		return "R55 Patch 3 - Update Terminals";
	}
}
