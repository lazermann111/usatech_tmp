package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.AppInstanceSplitterTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;


public class SplitEdgeServer_ChangeSet extends AbstractLinuxChangeSet {

	public SplitEdgeServer_ChangeSet() throws UnknownHostException {
		super();
	}

	public String getName() {
		return "Split Edge Server";
	}

	@Override
	protected void registerApps() {
		registerApp(USATRegistry.NET_LAYER);
		registerApp(USATRegistry.APP_LAYER);
		registerApp(USATRegistry.INAUTH_LAYER);
		registerApp(USATRegistry.OUTAUTH_LAYER);
		registerApp(USATRegistry.POSM_LAYER);
		registerApp(USATRegistry.KEYMGR);
		registerApp(USATRegistry.LOADER);
	}

	@Override
	protected void addTasks(Host host, App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		tasks.add(new AppInstanceSplitterTask(app, ordinal, instanceNum, instanceCount));
		// commands.add("/usr/monit-latest/bin/monit restart " + app.getName() + (ordinal != null ? ordinal : ""));
		commands.add("/opt/USAT/" + app.getName() + (ordinal != null ? ordinal : "") + "/bin/" + app.getServiceName() + ".sh stop");
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		commands.add("sudo su");
	}

	protected void addPostAppsTasks() {
		// Do nothing
	}

	public String toString() {
		return getName();
	}
}
