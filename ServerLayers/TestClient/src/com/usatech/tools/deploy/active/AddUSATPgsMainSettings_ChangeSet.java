package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.BasicServerSet;
import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.CopyTask;
import com.usatech.tools.deploy.DefaultHostSpecificValue;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.MappedHostSpecificValue;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.PropertiesUpdateFileTask;
import com.usatech.tools.deploy.Server;
import com.usatech.tools.deploy.USATRegistry;

public class AddUSATPgsMainSettings_ChangeSet implements ChangeSet {

	private static final String CONFIG_FILE_PATH = "/opt/USAT/conf/USAT_environment_settings.properties";

	private static final String OPER_PGS_DEV = "jdbc:postgresql://devpgs10:5432/main?ssl=true&connectTimeout=5&tcpKeepAlive=true";
	private static final String REPORT_PGS_DEV = OPER_PGS_DEV;
	private static final String CORP_PGS_DEV = OPER_PGS_DEV;

	private static final App[] APPS = { USATRegistry.APP_LAYER, USATRegistry.LOADER, USATRegistry.POSM_LAYER,
			USATRegistry.INAUTH_LAYER, USATRegistry.DMS_APP, USATRegistry.KEYMGR, USATRegistry.OUTAUTH_LAYER,
			USATRegistry.RPTGEN_LAYER, USATRegistry.RPTREQ_LAYER, USATRegistry.USALIVE_APP, USATRegistry.PREPAID_APP,
			USATRegistry.TRANSPORT_LAYER, };

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		List<DeployTask> tasks = new ArrayList<DeployTask>();

		appendBackupConfigFileTasks(host, tasks);

		tasks.add(new MigrateDbConfigToPgsTask());

		USATRegistry registry = new USATRegistry();
		BasicServerSet serverSet = registry.getServerSet(host.getServerEnv());

		List<String> commands = new ArrayList<String>();
		commands.add("sudo su");

		for (App app : APPS) {
			if (!host.isServerType(app.getServerType())) {
				continue;
			}

			List<Server> servers = serverSet.getServers(app.getServerType());
			if (servers == null) {
				continue;
			}

			Server server = findServer(servers, host);
			if (server != null) {
				appendReloadServiceCmd(commands, app, server);
			}
		}

		if (!commands.isEmpty()) {
			tasks.add(new ExecuteTask(commands.toArray(new String[commands.size()])));
		}

		return tasks.toArray(new DeployTask[tasks.size()]);
	}

	private void appendReloadServiceCmd(List<String> commands, App app, Server server) {
		String[] instances = app.filterInstances(server.instances);
		for (String instance : instances) {
			commands.add("/opt/USAT/" + app.getName() + (instance == null ? "" : instance) + "/bin/"
					+ app.getServiceName() + ".sh stop 10000");
		}
	}

	private Server findServer(List<Server> servers, Host host) {
		for (Server server : servers) {
			if (server.isOnHost(host)) {
				return server;
			}
		}
		return null;
	}

	private void appendBackupConfigFileTasks(Host host, List<DeployTask> tasks) {
		CopyTask copyTask = new CopyTask(host, host);
		copyTask.addCopy(CONFIG_FILE_PATH, CONFIG_FILE_PATH + ".oracle.backup", true, 640, "root", "usat");
		tasks.add(copyTask);
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("APR") || host.isServerType("APP");
	}

	@Override
	public String toString() {
		return getName();
	}

	@Override
	public String getName() {
		return "Add USAT Pgs main settings";
	}

	private static final class MigrateDbConfigToPgsTask extends PropertiesUpdateFileTask {

		public MigrateDbConfigToPgsTask() {
			super(true, 0640, "root", "usat");
			setFilePath(CONFIG_FILE_PATH);

			String[] appAprServerTypes = new String[] { "APR", "APP" };

			// changing connection URL base on environments type
			MappedHostSpecificValue operUrl = new MappedHostSpecificValue();
			operUrl.registerValue("DEV", appAprServerTypes, OPER_PGS_DEV);
			registerValue("USAT.database.OPER.url", operUrl);

			MappedHostSpecificValue reportUrl = new MappedHostSpecificValue();
			reportUrl.registerValue("DEV", appAprServerTypes, REPORT_PGS_DEV);
			registerValue("USAT.database.REPORT.url", reportUrl);

			MappedHostSpecificValue corpUrl = new MappedHostSpecificValue();
			corpUrl.registerValue("DEV", appAprServerTypes, CORP_PGS_DEV);
			registerValue("USAT.database.CORP.url", corpUrl);

			// changing driver name
			registerValue("USAT.database.datasource.REPORT.driverClassName",
					new DefaultHostSpecificValue("org.postgresql.Driver"));
			registerValue("USAT.database.datasource.OPER.driverClassName",
					new DefaultHostSpecificValue("org.postgresql.Driver"));
			registerValue("USAT.quartz.jobStore.driverDelegateClass",
					new DefaultHostSpecificValue("org.quartz.impl.jdbcjobstore.PostgreSQLDelegate"));

			registerValue("USAT.database.dialect", new DefaultHostSpecificValue("pgs"));

			// changing validation query
			registerValue("USAT.database.datasource.OPER.validationQuery", new DefaultHostSpecificValue("VALUES(1)"));
			registerValue("USAT.database.datasource.REPORT.validationQuery", new DefaultHostSpecificValue("VALUES(1)"));

			

		}
	}
}
