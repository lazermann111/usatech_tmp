package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R52_Patch_5_ChangeSet extends MultiLinuxPatchChangeSet {
	public R52_Patch_5_ChangeSet() throws UnknownHostException {
		super();
		registerResource("DMS/resources/jsp/login.jsp", "web/jsp", "REL_DMS_1_14_5", false, USATRegistry.DMS_APP);
		registerResource("eSudsWebsite/web/WEB-INF/web.xml", "web/WEB-INF", "REL_eSudsWebsite_1_2_5", true, USATRegistry.ESUDS_APP);
	}
	
	@Override
	public String getName() {
		return "R52 Patch 5";
	}
}
