package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.authoritylayer.AprivaAuthorityTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R50_Patch_12_ChangeSet extends MultiLinuxPatchChangeSet {
	public R50_Patch_12_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/AuthorityLayer/src", AprivaAuthorityTask.class, "REL_authoritylayer_1_36_12", USATRegistry.OUTAUTH_LAYER);
	}
	
	@Override
	public String getName() {
		return "R50 Patch 12";
	}

}
