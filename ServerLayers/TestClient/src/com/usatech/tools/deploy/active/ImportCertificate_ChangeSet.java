package com.usatech.tools.deploy.active;

import java.io.File;
import java.io.IOException;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.ImportJKSCertTask;
import com.usatech.tools.deploy.PasswordCache;

public class ImportCertificate_ChangeSet implements ChangeSet {
	public ImportCertificate_ChangeSet() {
		super();
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		return new DeployTask[] {
				new ImportJKSCertTask("ec.usatech.com", new File("C:\\Users\\bkrug\\Downloads\\ec.usatech.com.cer"), "/opt/USAT/conf/truststore.ts", cache, 0640, false)
		};
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("APP");
	}
	
	@Override
	public String toString() {
		return getName();
	}
	@Override
	public String getName() {
		return "Import Certificate";
	}
}
