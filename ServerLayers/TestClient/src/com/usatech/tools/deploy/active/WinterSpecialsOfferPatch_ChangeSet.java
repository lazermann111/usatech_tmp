package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class WinterSpecialsOfferPatch_ChangeSet extends MultiLinuxPatchChangeSet {
	public WinterSpecialsOfferPatch_ChangeSet() throws UnknownHostException {
		super();
		registerResource("usalive/web/winter_special_order.html.jsp", "web", "REL_USALive_1_19_4", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/offer_signups.html.jsp", "web", "REL_USALive_1_19_4", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/public/winter_special_order.html.jsp", "web/public", "REL_USALive_1_19_4", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/xsl/other-data-layer.xml", "classes", "REL_USALive_1_19_4", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/css/winter_main.css", "web/css", "REL_USALive_1_19_4", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/images/usat-winterspecials.gif", "web/images", "REL_USALive_1_19_4", false, USATRegistry.USALIVE_APP);

		registerResource("usalive/web/css/winter_main.css", "web/css", "REL_USALive_1_19_4", false, USATRegistry.HTTPD_WEB);
		registerResource("usalive/web/images/usat-winterspecials.gif", "web/images", "REL_USALive_1_19_4", false, USATRegistry.HTTPD_WEB);
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.USALIVE_APP);
	}

	@Override
	public String getName() {
		return "Winter Specials Offer Patch";
	}
}
