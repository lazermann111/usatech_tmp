package com.usatech.tools.deploy.active;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import simple.io.EncodingInputStream;
import simple.io.ReplacementsLineFilteringReader;

import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.UnixUploadTask;
/**
 * NOTE: on ecc /opt/USAT/httpd/eportmobile permission is 775 and prod has 775 for Ron to use USATPushToEnv eportmobile usa to work.
 * on prod mkdir /opt/USAT/httpd/eportmobile 
 * chmod 775 /opt/USAT/httpd/eportmobile
 * @author yhe
 *
 */
public class EportMobileWebsiteChangeSet implements ChangeSet {
	
	public static final File baseDir = new File(System.getProperty("rootSourceDir", "D:\\Development\\Java Projects"));
	protected static final File buildsDir = new File(baseDir, "ApplicationBuilds");
	protected static final File serverAppConfigDir = new File(baseDir, "server_app_config");
	public static final App HTTPD_WEB = new App("httpd", "WEB", "server_app_config/WEB/Httpd/linux", "httpd-", 0, 1 /*bin*/, "httpd", "httpd", null, "bin");
	public static final App EPORTMOBILE_WEBSITE_APP = new App("eportmobile", "WEB", "ApplicationBuilds/eportmobile", "eportmobile-", 1300, 953, "usatech", "usatech", "usatech");
	
	static{
		EPORTMOBILE_WEBSITE_APP.setVersion("1.0");
	}
	

	@Override
	public String getName() {
		return "EportMobile website task";
	}
	
	@Override
	public String toString() {
		return getName();
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("WEB");
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache)
			throws IOException {
		ArrayList<DeployTask> tasks=new ArrayList<DeployTask>();
		/**
		HashMap<String,String> loopbackMap=LoopbackInterfaceMap.getMapByEnv(host.getServerEnv());
		//1. add loopback interface
		for(App subApp : HTTPD_WEB.getSubApps()) {
			String subHostPrefix=subApp.getName();
			String subhostname;
			if("USA".equalsIgnoreCase(host.getServerEnv())){
				subhostname="www.eportmobile.com";
			}else{
				subhostname=subHostPrefix+"-" + host.getServerEnv().toLowerCase() + ".usatech.com";
			}
			String subhostip = InetAddress.getByName(subhostname).getHostAddress();
			String deviceName="lo:"+loopbackMap.get(subhostip);
			tasks.add(new ExecuteTask("sudo su",
					"test -d /etc/sysconfig/network-scripts/ifcfg-"+deviceName+" || echo 'DEVICE="+deviceName+"\nIPADDR="+subhostip+"\nNETMASK=0.0.0.0\nONBOOT=yes' >> /etc/sysconfig/network-scripts/ifcfg-"+deviceName,
					"ifup "+deviceName)
					);
		}
		*/
		//2. add conf.d folder file
		String appServer;
		String last2 = host.getSimpleName().substring(6);
		String instanceNum = host.getSimpleName().substring(7);
		if("USA".equalsIgnoreCase(host.getServerEnv())) {
			appServer = "app";
		} else {
			appServer = "apr";
		}

		String lb_ip_escaped;
		if("DEV".equalsIgnoreCase(host.getServerEnv()))
			lb_ip_escaped = "10\\\\.0\\\\.0\\\\.64";
		else if("INT".equalsIgnoreCase(host.getServerEnv()))
			lb_ip_escaped = "10\\\\.0\\\\.0\\\\.64";
		else if("ECC".equalsIgnoreCase(host.getServerEnv()))
			lb_ip_escaped = "192\\\\.168\\\\.4\\\\.65";
		else if("USA".equalsIgnoreCase(host.getServerEnv()))
			lb_ip_escaped = "192\\\\.168\\\\.79\\\\.(?:18[34]|19[56])";
		else
			lb_ip_escaped = "";
		
		App subApp=EPORTMOBILE_WEBSITE_APP;
		String subHostPrefix=subApp.getName();//eportmobile
		String subhostname;
		if("USA".equalsIgnoreCase(host.getServerEnv())){
			subhostname="www.eportmobile.com";
		}else{
			subhostname=subHostPrefix+"-" + host.getServerEnv().toLowerCase() + ".usatech.com";
		}
		String subhostip = InetAddress.getByName(subhostname).getHostAddress();
		Map<Pattern, String> replacements = new HashMap<Pattern, String>();
		replacements.put(Pattern.compile("<APP_SERVER_NAME>"), host.getServerEnv().toLowerCase() + appServer + last2);
		replacements.put(Pattern.compile("<WORKER_NAME>"), "worker_" + instanceNum);
		replacements.put(Pattern.compile("<WEB_SERVER_NAME>"), subhostname);
		replacements.put(Pattern.compile("<WEB_SERVER_IP>"), subhostip);
		replacements.put(Pattern.compile("<WEB_SERVER_IP_ESCAPED>"), subhostip.replace(".", "\\\\."));
		replacements.put(Pattern.compile("<LOADBALANCER_IP_ESCAPED>"), lb_ip_escaped);
		File file = new File(serverAppConfigDir, "WEB/Httpd/conf.d/" + subApp.getName() + ".conf");
		tasks.add(new UnixUploadTask(new EncodingInputStream(new ReplacementsLineFilteringReader(new BufferedReader(new FileReader(file)), "\n", replacements)), "/opt/USAT/httpd/conf.d/" + file.getName(), 0644, HTTPD_WEB.getUserName(), HTTPD_WEB.getUserName(), true, file.getAbsolutePath()));
	
		
		String subAppDir="/opt/USAT/httpd/eportmobile";
		/**
		tasks.add(new ExecuteTask("sudo su",
				"rsync -avz --delete --rsync-path=\"rsync\" `logname`@devnet11:/opt/USAT/httpd/"+subApp.getName()+" /opt/USAT/httpd",
				"echo "+subApp.getVersion()+" >"+subAppDir+"/version.txt",
				"chown -R bin:webdev " + subAppDir));
			*/	
		
		// monit restart httpd
		tasks.add(new ExecuteTask("sudo su",
				"/usr/monit-latest/bin/monit restart httpd"
			));
		
		DeployTask[] deployTasks=new DeployTask[tasks.size()];
		for(int i=0; i<tasks.size();i++){
			deployTasks[i]=tasks.get(i);
		}
		return deployTasks;
	}

}
