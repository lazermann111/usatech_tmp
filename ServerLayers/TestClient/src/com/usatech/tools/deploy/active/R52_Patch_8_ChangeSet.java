package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R52_Patch_8_ChangeSet extends MultiLinuxPatchChangeSet {
	public R52_Patch_8_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/AppLayer/src/AppLayerService.properties", "classes", "REL_applayer_1_38_8", true, USATRegistry.APP_LAYER);
	}
	
	@Override
	public String getName() {
		return "R52 Patch 8";
	}
}
