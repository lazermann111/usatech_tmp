package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.AppSettingUpdateFileTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.tools.deploy.UsatSettingUpdateFileTask;

public class AppMonitor_Linux_ChangeSet extends AbstractLinuxChangeSet {
	public AppMonitor_Linux_ChangeSet() throws UnknownHostException {
		super();
	}

	@Override
	protected void registerApps() {
		registerApp(USATRegistry.APP_MONITOR, "1.0.0");
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		tasks.add(new UsatSettingUpdateFileTask(registry.getServerSet(host.getServerEnv())));
		addPrepareHostTasks(host, tasks, cache);
	}
	
	@Override
	protected void addTasks(Host host, final App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache);
		if(app.getInstallFileName() != null)
			tasks.add(new AppSettingUpdateFileTask(app, ordinal, instanceNum, instanceCount, cache, registry.getServerSet(host.getServerEnv()), false));
		addInstallAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache, commands);
	}

	@Override
	public String getName() {
		return "AppMonitor Install";
	}
}
