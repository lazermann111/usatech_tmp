package com.usatech.tools.deploy.active;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.text.Format;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import simple.bean.ConvertUtils;
import simple.text.LiteralFormat;
import simple.text.MessageFormat;
import simple.text.StringUtils;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.DeployTaskInfo;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.tools.deploy.UploadAndRunPostgresSQLTask;

public class PostgresRunScript_ChangeSet extends AbstractLinuxChangeSet {
	protected Map<App, Map<File, Format[]>> filesByApp;
	public PostgresRunScript_ChangeSet() throws UnknownHostException {
		super();
	}
	protected void registerApps() {
		filesByApp = new HashMap<App, Map<File, Format[]>>();
		registerFile(USATRegistry.POSTGRES_PGS, new File("D:\\Development\\Database Projects\\DatabaseScripts\\REPTP\\RDW\\SourceViews_MAIN.sql"), "main");
		registerFile(USATRegistry.POSTGRES_PGS, new File("D:\\Development\\Database Projects\\DatabaseScripts\\postgresql\\pg_main_setup.sql"), "main");
		registerFile(USATRegistry.POSTGRES_PGS, new File("D:\\Development\\Database Projects\\DatabaseScripts\\REPTP\\RDW\\pg_rdw_setup.sql"), "main");
		registerFile(USATRegistry.POSTGRES_PGS, new File("D:\\Development\\Database Projects\\DatabaseScripts\\postgresql\\device_session_and_message.sql"), "main");
		registerFile(USATRegistry.POSTGRES_PGS, new File("D:\\Development\\Database Projects\\DatabaseScripts\\REPTP\\RDW\\pg_rdw_functions.sql"), "main");
		registerFile(USATRegistry.POSTGRES_MSR, new File("D:\\Development\\Java Projects\\Simple1.5\\src\\simple\\mq\\peer\\mq_peer_postgres_setup.sql"), new MessageFormat("{0,NULL,mq,'{1,NULL,''{1,simple.text.PadFormat,*__}mq{1,simple.text.PadFormat,_*}'',mq}'}"));
		registerFile(USATRegistry.POSTGRES_MST, new File("D:\\Development\\Java Projects\\Simple1.5\\src\\simple\\mq\\peer\\mq_peer_postgres_setup.sql"), new MessageFormat("{0,NULL,mq,'{1,NULL,''{1,simple.text.PadFormat,*__}mq{1,simple.text.PadFormat,_*}'',mq}'}"));
		registerFile(USATRegistry.POSTGRES_MST, new File("D:\\Development\\Java Projects\\Simple1.5\\src\\simple\\mq\\peer\\mq_peer_postgres_setup_8.sql"), new MessageFormat("{0,NULL,mq,'{1,NULL,''{1,simple.text.PadFormat,*__}mq{1,simple.text.PadFormat,_*}'',mq}'}"));
		registerFile(USATRegistry.POSTGRES_KLS, new File("D:\\Development\\Java Projects\\KeyManagerService\\src\\km_account_setup.sql"), new MessageFormat("{1,NULL,'{1,simple.text.PadFormat,*__}km{1,simple.text.PadFormat,_*}',km}"));
		registerFile(USATRegistry.POSTGRES_KLS, new File("D:\\Development\\Java Projects\\KeyManagerService\\src\\km_token_setup.sql"), new MessageFormat("{1,NULL,'{1,simple.text.PadFormat,*__}km{1,simple.text.PadFormat,_*}',km}"));
		registerFile(USATRegistry.POSTGRES_MST, new File("D:\\Development\\Database Projects\\DatabaseScripts\\edge_implementation\\setup_app_db_v3.sql"), new MessageFormat("app_{2}"));
		registerFile(USATRegistry.POSTGRES_KLS, new File("D:\\Development\\Database Projects\\DatabaseScripts\\releases\\REL_Edge_Update_R33\\KeyMgrUpdate.sql"), new MessageFormat("{1,NULL,'{1,simple.text.PadFormat,*__}km{1,simple.text.PadFormat,_*}',km}"));
		registerFile(USATRegistry.POSTGRES_MST, new File("D:\\Development\\Database Projects\\DatabaseScripts\\releases\\REL_Edge_Update_R34\\R34.APP.1.sql"), new MessageFormat("app_{2}"));
		registerFile(USATRegistry.POSTGRES_KLS, new File("D:\\Development\\Java Projects\\TestClient\\resources\\FixEliazbethtownCards_20140226.sql"), new MessageFormat("{1,NULL,'{1,simple.text.PadFormat,*__}km{1,simple.text.PadFormat,_*}',km}"));

		registerFile(USATRegistry.POSTGRES_MST, new File("D:\\Development\\Database Projects\\DatabaseScripts\\releases\\REL_Edge_Update_R42\\R42_Elavon.APP.1.sql"), new MessageFormat("app_{2}"));
		//registerFile(USATRegistry.POSTGRES_MST, new File("D:\\Development\\Database Projects\\DatabaseScripts\\postgresql\\app_cluster.consumer_acct.sql"), "app_" + instanceNum);
	}

	protected void registerFile(App app, File file, String... databases) {
		Format[] dbs = new Format[databases.length];
		for(int k = 0; k < databases.length; k++)
			dbs[k] = new LiteralFormat(databases[k]);
		registerFile(app, file, dbs);
	}

	protected void registerFile(App app, File file, Format... databases) {
		Map<File, Format[]> files = filesByApp.get(app);
		if(files == null) {
			registerApp(app);
			files = new LinkedHashMap<File, Format[]>();
			filesByApp.put(app, files);
		}
		files.put(file, databases);
	}
	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) {
	}

	@Override
	public String getName() {
		return "Postgres - Run Script";
	}
	@Override
	protected void addTasks(Host host, App app, final Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		final Map<File, Format[]> files = filesByApp.get(app);
		if(files == null || files.isEmpty())
			return;
		final Object[] params = new Object[] { ordinal, instance, instanceNum };
		tasks.add(new DeployTask() {
			@Override
			public String getDescription(DeployTaskInfo deployTaskInfo) {
				return "Run Postgres Script";
			}

			@Override
			public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
				if(files.size() == 1) {
					Map.Entry<File, Format[]> entry = files.entrySet().iterator().next();
					String[] databases = new String[entry.getValue().length];
					for(int k = 0; k < databases.length; k++)
						databases[k] = entry.getValue()[k].format(params);
					String line = deployTaskInfo.interaction.readLine("Enter 'Y' to confirm that you would like to run script ''%1$s'' on database(s): %2$s", entry.getKey().getName(), StringUtils.join(databases, ", "));
					if(ConvertUtils.getBooleanSafely(line, false))
						return new UploadAndRunPostgresSQLTask(entry.getKey(), false, ordinal, databases).perform(deployTaskInfo);
					return "Skipping script";
				}
				StringBuilder sb = new StringBuilder();
				sb.append("Please choose which script to execute:");
				int i = 1;
				for(Map.Entry<File, Format[]> entry : files.entrySet()) {
					sb.append("\n\t").append(i++).append(") ").append(entry.getKey().getName()).append(" on ");
					String[] databases = new String[entry.getValue().length];
					for(int k = 0; k < databases.length; k++)
						databases[k] = entry.getValue()[k].format(params);
					for(int k = 0; k < databases.length; k++) {
						if(k > 0)
							sb.append(", ");
						sb.append(databases[k]);
					}
				}
				String line = deployTaskInfo.interaction.readLine(sb.toString());
				if(StringUtils.isBlank(line))
					return "Skipping script";
				int choice = ConvertUtils.getIntSafely(line, 0);
				if(choice < 1 || choice > files.size())
					return "Invalid choice (" + line.trim() + ")";
				i = 1;
				for(Map.Entry<File, Format[]> entry : files.entrySet()) {
					if(i++ == choice) {
						String[] databases = new String[entry.getValue().length];
						for(int k = 0; k < databases.length; k++)
							databases[k] = entry.getValue()[k].format(params);

						return new UploadAndRunPostgresSQLTask(entry.getKey(), false, ordinal, databases).perform(deployTaskInfo);
					}
				}
				return "Choice (" + line.trim() + ") not found";
			}

			@Override
			public String toString() {
				return "Run Postgres Script";
			}
		});
	}
}
