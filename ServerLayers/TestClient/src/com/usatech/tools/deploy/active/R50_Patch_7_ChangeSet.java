package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.applayer.AuthorizeTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R50_Patch_7_ChangeSet extends MultiLinuxPatchChangeSet {
	public R50_Patch_7_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/AppLayer/src/", AuthorizeTask.class, "REL_applayer_1_36_7", USATRegistry.APP_LAYER);
	}
	
	@Override
	public String getName() {
		return "R50 Patch 7";
	}
}
