package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.applayer.AuthorizeTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R50_Patch_1_ChangeSet extends MultiLinuxPatchChangeSet {
	public R50_Patch_1_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/AppLayer/src/", AuthorizeTask.class, "REL_applayer_1_36_1", USATRegistry.APP_LAYER);
		registerResource("DMS/resources/jsp/devices/paymentConfig/edit_pta_settings.jsp", "web/jsp/devices/paymentConfig", "REL_DMS_1_12_1", false, USATRegistry.DMS_APP);
	}
	
	@Override
	public String getName() {
		return "R50 Patch 1";
	}
}
