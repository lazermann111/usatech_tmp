package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class RDW_Patch_3_19_0_ChangeSet extends MultiLinuxPatchChangeSet {
	public RDW_Patch_3_19_0_ChangeSet() throws UnknownHostException, ClassNotFoundException {
		super();
		registerResource("Simple1.5/src/simple/mq/peer/mq-peer-data-layer.xml", "classes/simple/mq/peer", "REL_report_generator_3_17_0", false, USATRegistry.RDW_LOADER);
	}

	@Override
	public String getName() {
		return "RDW Patch 3.19.0";
	}
}
