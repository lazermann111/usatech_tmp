package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R49_Patch_3_ChangeSet extends MultiLinuxPatchChangeSet {
	public R49_Patch_3_ChangeSet() throws UnknownHostException {
		super();

		registerResource("usalive/web/images/holiday2015/image003.jpg", "web/images/holiday2015", "REL_USALive_1_25_3", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/images/holiday2015/image026.jpg", "web/images/holiday2015", "REL_USALive_1_25_3", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/customerreporting_web/home.jsp", "web", "REL_USALive_1_25_3", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/images/holiday2015/image003.jpg", "web/images/holiday2015", "REL_USALive_1_25_3", false, USATRegistry.HTTPD_WEB);
		registerResource("usalive/web/images/holiday2015/image026.jpg", "web/images/holiday2015", "REL_USALive_1_25_3", false, USATRegistry.HTTPD_WEB);
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.USALIVE_APP);
	}

	@Override
	public String getName() {
		return "R49 Patch 3";
	}
}
