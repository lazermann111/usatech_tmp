package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.authoritylayer.TNSGatewayTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R56_Patch_11_ChangeSet extends MultiLinuxPatchChangeSet {
	public R56_Patch_11_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/AuthorityLayer/src", TNSGatewayTask.class, "REL_R56_11", USATRegistry.INAUTH_LAYER);
	}
	
	@Override
	public String getName() {
		return "R56 Patch 11 - Send Expired Cards to NRT";
	}
}
