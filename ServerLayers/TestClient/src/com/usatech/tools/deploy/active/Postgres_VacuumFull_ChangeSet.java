package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.tools.deploy.UploadAndRunPostgresSQLTask;

public class Postgres_VacuumFull_ChangeSet extends AbstractLinuxChangeSet {
	protected String sql = "VACUUM FULL VERBOSE;";

	public Postgres_VacuumFull_ChangeSet() throws UnknownHostException {
		super();
	}
	protected void registerApps() {
		registerApp(USATRegistry.POSTGRES_MST);
		registerApp(USATRegistry.POSTGRES_MSR);
		registerApp(USATRegistry.POSTGRES_PGS);
	}
	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) {
	}
	@Override
	protected void addTasks(Host host, App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if("postgres".equalsIgnoreCase(app.getName()) && (instance == null || instanceNum == 1)) {
			String[] databases;
			if("LOCAL".equalsIgnoreCase(host.getServerEnv())) {
				databases = new String[] { "bkmq1", "bkmq2", "dkmq1", "dkmq2", "phmq1", "phmq2" };
			} else {
				databases = new String[] { "mq" };
				// databases = new String[] { "filerepo" };
			}
			for(String database : databases)
				tasks.add(new UploadAndRunPostgresSQLTask(sql, true, database));
		}
	}
	
	@Override
	public String getName() {
		return "Postgres - Vacuum Full";
	}
}
