package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R56_Patch_9_ChangeSet extends MultiLinuxPatchChangeSet {
	public R56_Patch_9_ChangeSet() throws UnknownHostException {
		super();

		registerResource("DMS/resources/datalayers/terminal-data-layer.xml", "classes", "REL_R56_9", false, USATRegistry.DMS_APP);
	}
	
	@Override
	public String getName() {
		return "R56 Patch 9 - Deactivate details";
	}
}
