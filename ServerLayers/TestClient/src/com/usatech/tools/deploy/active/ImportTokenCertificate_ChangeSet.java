package com.usatech.tools.deploy.active;

import static com.usatech.tools.deploy.USATRegistry.baseDir;

import java.io.File;
import java.io.IOException;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.ImportJKSCertTask;
import com.usatech.tools.deploy.PasswordCache;

public class ImportTokenCertificate_ChangeSet implements ChangeSet {
	public ImportTokenCertificate_ChangeSet() {
		super();
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		return new DeployTask[] {
				new ImportJKSCertTask("usat_token_20110414", new File(baseDir, "/LayersCommon/conf/usat_token_20110414.cer"), "/opt/apache-tomcat-5.5.23-keymgr/conf/truststore.jks", cache, 0644, false)
		};
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("KLS");
	}
	
	@Override
	public String toString() {
		return getName();
	}
	@Override
	public String getName() {
		return "Import Token Certificate";
	}
}
