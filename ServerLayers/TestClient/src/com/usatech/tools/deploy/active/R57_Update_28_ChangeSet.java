package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R57_Update_28_ChangeSet extends MultiLinuxPatchChangeSet {
	public R57_Update_28_ChangeSet() throws UnknownHostException {
		super();		
		registerResource("usalive/xsl/refund-data-layer.xml", "classes", "REL_R57_28", false, USATRegistry.USALIVE_APP);
	}
	
	@Override
	public String getName() {
		return  "R57 Update 28 - Database performance - unused indexes";
	}
}
