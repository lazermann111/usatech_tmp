package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.keymanager.master.MasterKeyLoader;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R54_Patch_7_ChangeSet extends MultiLinuxPatchChangeSet {
	public R54_Patch_7_ChangeSet() throws UnknownHostException {
		super();
		registerSource("KeyManager/src-common", MasterKeyLoader.class, "REL_keymgr_2_21_7", USATRegistry.KEYMGR);
	}
	
	@Override
	public String getName() {
		return "R54 Patch 7";
	}
}
