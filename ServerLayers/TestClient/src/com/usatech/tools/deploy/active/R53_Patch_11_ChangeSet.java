package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.authoritylayer.UGrydAuthorityTask;
import com.usatech.tools.deploy.*;

public class R53_Patch_11_ChangeSet extends MultiLinuxPatchChangeSet {
	
	public R53_Patch_11_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/AuthorityLayer/src", UGrydAuthorityTask.class, "REL_authoritylayer_1_39_11", USATRegistry.OUTAUTH_LAYER);
	}
	
	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(app == USATRegistry.OUTAUTH_LAYER) {
			String appPropertiesFile = String.format("/opt/USAT/%s%s/specific/USAT_app_settings.properties", app.getName(), instance == null ? "" : instance);
			PropertiesUpdateFileTask puft = new PropertiesUpdateFileTask(true, 0640, app.getUserName(), app.getUserName());
			puft.setFilePath(appPropertiesFile);
			
			puft.registerValue("USAT.authoritylayer.ugryd.url", new ServerEnvHostSpecificValue(
					"http://nm.uat.ugryd.com/services.php", 
					"http://nm.uat.ugryd.com/services.php", 
					"http://nm.uat.ugryd.com/services.php", 
					"https://nm.ugryd.com/services.php"));
			
			puft.registerValue("USAT.authoritylayer.ugryd.username", new ServerEnvHostSpecificValue(
					"usatech", 
					"usatech", 
					"usatech", 
					"usat"));
			
			puft.registerValue("USAT.authoritylayer.ugryd.password", new ServerEnvHostSpecificValue(
					new GetPasswordHostSpecificValue(cache, "ugryd.password", "CBORD UGryd TEST", true),
					new GetPasswordHostSpecificValue(cache, "ugryd.password", "CBORD UGryd TEST", true),
					new GetPasswordHostSpecificValue(cache, "ugryd.password", "CBORD UGryd TEST", true),
					new GetPasswordHostSpecificValue(cache, "ugryd.password", "CBORD UGryd PROD", true)));
			
			tasks.add(puft);
		}
		
		super.addRestartAppCommands(host, app, instance, instanceNum, instanceCount, tasks, commands, cache);
	}
	
	@Override
	public String getName() {
		return "R53 Patch 11";
	}
}
