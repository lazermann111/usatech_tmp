package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.*;

public class R51_Patch_2_ChangeSet extends MultiLinuxPatchChangeSet {
	public R51_Patch_2_ChangeSet() throws UnknownHostException {
		super();
		registerAppRestart(USATRegistry.INAUTH_LAYER);
	}

	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(app == USATRegistry.INAUTH_LAYER) {
			
			String[] merchantCds = {"612000000000", "641000000000", "645200000000", "677500000000", "032258065000", "000000311007", "000390048102", "920000000000",  "741000000000", "774000000000", "129000000000", "230000002200", "064999999000"};
			
			String appPropertiesFile = String.format("/opt/USAT/%s%s/specific/USAT_app_settings.properties",
					app.getName(),
					instance == null ? "" : instance);
			
			PropertiesUpdateFileTask puft = new PropertiesUpdateFileTask(true, 0640, app.getUserName(), app.getUserName());
			puft.setFilePath(appPropertiesFile);
			
			for(String merchantCd : merchantCds) {
				String propertyKey = String.format("com.usatech.authoritylayer.TandemGatewayTask.authPartiallyReversed.whenLessByMerchant(%s)", merchantCd);
				puft.registerValue(propertyKey, new DefaultHostSpecificValue("true"));
			}

			tasks.add(puft);
		}
		
		super.addRestartAppCommands(host, app, instance, instanceNum, instanceCount, tasks, commands, cache);
	}	

	
	@Override
	public String getName() {
		return "R51 Patch 2";
	}
}
