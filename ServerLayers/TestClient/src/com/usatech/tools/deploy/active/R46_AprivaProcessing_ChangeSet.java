package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.AppSettingUpdateFileTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.Server;
import com.usatech.tools.deploy.UploadAndRunPostgresSQLTask;

public class R46_AprivaProcessing_ChangeSet extends AbstractLinuxChangeSet {
	public R46_AprivaProcessing_ChangeSet() throws UnknownHostException {
		super();
	}

	@Override
	protected void registerApps() {

	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("MST");
	}
	
	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(host.isServerType("MST")) {
			String[] instances = registry.getServerSet(host.getServerEnv()).getServer(host, "MST").instances;
			Map<Integer, String[]> dbInst = new HashMap<>();
			if("LOCAL".equalsIgnoreCase(host.getServerEnv()) && instances != null) {
				Set<String> dbNames = new LinkedHashSet<>();
				for(int i = 0; i < instances.length; i++) {
					if(instances[i] == null || instances[i].length() < 2)
						continue;
					switch(instances[i].substring(0, 2)) {
						case "dk":
							dbNames.add("app_1");
							break;
						case "bk":
							dbNames.add("app_2");
							break;
						case "yh":
							dbNames.add("app_3");
							break;
						case "js":
							dbNames.add("app_4");
							break;
						case "pc":
							dbNames.add("app_5");
							break;
						case "ml":
							dbNames.add("app_6");
							break;
					}
				}
				dbInst.put(null, dbNames.toArray(new String[dbNames.size()]));
			} else {
				int count = instances == null ? 1 : instances.length;
				int n = (host.getSimpleName().charAt(host.getSimpleName().length() - 1) - '0') - 1;
				for(int i = 0; i < count; i++) {
					String appdb = "app_" + String.valueOf(n * count + i + 1);
					Integer ordinal = (instances == null || instances[0] == null ? null : i + 1);
					dbInst.put(ordinal, new String[] { appdb });
				}
			}
			for(Map.Entry<Integer, String[]> entry : dbInst.entrySet()) {
				tasks.add(new UploadAndRunPostgresSQLTask("DatabaseScripts/releases/REL_Edge_Update_R46/R46.APP.AprivaProcessing.sql", "HEAD", Boolean.FALSE, entry.getKey(), entry.getValue()));
			}
		}
	}
	
	@Override
	protected void addTasks(Host host, final App app, Integer ordinal, String instance, final int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache);
		if(app.getInstallFileName() != null)
			tasks.add(new AppSettingUpdateFileTask(app, ordinal, instanceNum, instanceCount, cache, registry.getServerSet(host.getServerEnv()), false));
		addInstallAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache, commands);
	}

	@Override
	protected void addBeforeRestartCommands(Host host, App app, Integer ordinal, final int instanceNum, List<String> commands, List<DeployTask> tasks) {		
		int i = 0;
		Server server = null;
		Iterator<Server> iter = registry.getServerSet(host.getServerEnv()).getServers("MST").iterator();
		for(; i < instanceNum;) {
			server = iter.next();
			i += server.instances.length;
		}
		if(!commands.isEmpty())
			tasks.add(new ExecuteTask(commands.toArray(new String[commands.size()])));
		commands.clear();
		super.addBeforeRestartCommands(host, app, ordinal, instanceNum, commands, tasks);
	}

	@Override
	public String getName() {
		return "R46 - Apriva Processing Changes";
	}
}
