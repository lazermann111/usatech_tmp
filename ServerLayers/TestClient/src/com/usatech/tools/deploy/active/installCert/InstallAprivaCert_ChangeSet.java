package com.usatech.tools.deploy.active.installCert;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.CvsPullTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.ImportJKSCertTask;
import com.usatech.tools.deploy.PasswordCache;
/**
 * The certificates we need to import to our keystore and truststore for Apriva
 * 
 * @author yhe
 *
 */
public class InstallAprivaCert_ChangeSet implements ChangeSet {

	@Override
	public String getName() {
		return "Import Apriva Certificates";
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("NET");
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		List<DeployTask> tasks = new ArrayList<DeployTask>();
		ImportJKSCertTask keystoreImport = new ImportJKSCertTask("/opt/USAT/conf/keystore.ks", cache, 0640, false);
		ImportJKSCertTask truststoreImport = new ImportJKSCertTask("/opt/USAT/conf/truststore.ts", cache, 0640, false);
		ImportJKSCertTask truststoreImport2 = new ImportJKSCertTask("/opt/USAT/conf/truststore.ts", cache, 0640, false);
		
		switch(host.getServerEnv().toUpperCase()) {
		case "USA":
			truststoreImport.addCertificate("apriva-root-2015", new CvsPullTask("ServerLayers/LayersCommon/conf/net/apriva-root-2015.crt"));
			keystoreImport.addCertificate("apriva-client-prod", new CvsPullTask("ServerLayers/LayersCommon/conf/net/apriva-client-prod.crt"));
			break;
		case "ECC":
			truststoreImport.addCertificate("apriva-root-2015", new CvsPullTask("ServerLayers/LayersCommon/conf/net/apriva-root-2015.crt"));
			keystoreImport.addCertificate("apriva-client-prod", new CvsPullTask("ServerLayers/LayersCommon/conf/net/apriva-client-prod.crt"));
			// fall-through
		default:
			truststoreImport.addCertificate("apriva-test-root-2015", new CvsPullTask("ServerLayers/LayersCommon/conf/net/apriva-test-root-2015.crt"));
			truststoreImport2.addCertificate("apriva-test-cert-126", new CvsPullTask("ServerLayers/LayersCommon/conf/net/apriva-test-cert-126.crt"));
			keystoreImport.addCertificate("apriva-client", new CvsPullTask("ServerLayers/LayersCommon/conf/net/apriva-client.crt"));
			tasks.add(truststoreImport2);
		}
		tasks.add(keystoreImport);
		tasks.add(truststoreImport);
		return tasks.toArray(new DeployTask[tasks.size()]);
	}

	@Override
	public String toString() {
		return getName();
	}
}
