package com.usatech.tools.deploy.active;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.UnknownHostException;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import jlibdiff.Hunk;
import simple.bean.ConvertUtils;
import simple.io.HeapBufferStream;
import simple.io.Log;

import com.sshtools.j2ssh.SftpClient;
import com.sshtools.j2ssh.sftp.FileAttributes;
import com.sshtools.j2ssh.sftp.SftpFile;
import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.DeployTaskInfo;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.Server;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.tools.deploy.UploadTask;

public class UpgradeVerification_ChangeSet implements ChangeSet {
	private static final Log log = Log.getLog();
	protected final USATRegistry registry;
	protected String[] dirsToCheck = { "/opt/USAT", "/etc", "/usr" };
	protected int maxDiffSize = 16 * 1024 * 1024;
	protected Pattern excludeDirPattern = Pattern.compile("(?:.*/)?(?:\\.{1,2}|.*\\.log)");
	public UpgradeVerification_ChangeSet() throws UnknownHostException {
		registry = new USATRegistry();
	}

	@Override
	public String getName() {
		return "Verify host upgrades";
	}

	@Override
	public boolean isApplicable(Host host) {
		return true;
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		return new DeployTask[] {
				new DeployTask() {
			public String getDescription(DeployTaskInfo deployTaskInfo) {
				return "Checking " + deployTaskInfo.host.getServerEnv() + " " + deployTaskInfo.host.getServerTypes() + " hosts against each other";
			}

			public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
				List<Server> servers = registry.getServerSet(deployTaskInfo.host.getServerEnv()).getServers(deployTaskInfo.host.getServerTypes());
				Set<Host> hosts = new LinkedHashSet<Host>();
				for(Server server : servers)
					hosts.add(server.host);
				Set<String> differences = new LinkedHashSet<String>();
				for(String dir : dirsToCheck)
					findDifferences(deployTaskInfo, hosts, dir, differences);
				if(differences.isEmpty())
					return "No differences amongst " + servers;
				return "Found " + differences.size() + " differences amongst " + servers;
			}

			protected void findDifferences(DeployTaskInfo deployTaskInfo, Set<Host> hosts, String path, Set<String> differences) throws IOException, InterruptedException {
				findDifferences(deployTaskInfo, hosts.toArray(new Host[hosts.size()]), path, new LinkedHashMap<String, FileAttributes[]>(), differences);
			}
				
			protected void findDifferences(DeployTaskInfo deployTaskInfo, Host[] hosts, String path, Map<String, FileAttributes[]> subpaths, Set<String> differences) throws IOException, InterruptedException {
				subpaths.clear();
				log.info("Comparing '" + path + "'");
				for(int i = 0; i < hosts.length; i++) {
					SftpClient sftp = deployTaskInfo.sshClientCache.getSftpClient(hosts[i], deployTaskInfo.user);
					FileAttributes fa;
					try {
						fa = sftp.stat(path);
					} catch(IOException e) {
						log.info("Could not stat '" + path + "' on " + hosts[i].getSimpleName(), e);
						continue;
					}
					if(fa.isDirectory()) {
						List<?> files;
						try {
							files = sftp.ls(path);
						} catch(IOException e) {
							log.info("Could not ls '" + path + "' on " + hosts[i].getSimpleName(), e);
							continue;
						}
						for(Object o : files) {
							SftpFile f = (SftpFile)o;
							if(excludeDirPattern.matcher(f.getAbsolutePath()).matches())
								continue;
							FileAttributes[] subfas = subpaths.get(f.getAbsolutePath());
							if(subfas == null) {
								subfas = new FileAttributes[hosts.length];
								subpaths.put(f.getAbsolutePath(), subfas);
							}
							subfas[i] = f.getAttributes();
						}
					} else {
						FileAttributes[] subfas = subpaths.get(".");
						if(subfas == null) {
							subfas = new FileAttributes[hosts.length];
							subpaths.put(".", subfas);
						}
						subfas[i] = fa;
					}

				}
				Set<String> subs = new LinkedHashSet<String>();
				for(Map.Entry<String, FileAttributes[]> subpath : subpaths.entrySet()) {
					FileAttributes baseFa = null;
					Host baseHost = null;
					HeapBufferStream baseBuffer = null;
					for(int i = 0; i < hosts.length; i++) {
						FileAttributes fa = subpath.getValue()[i];
						if(fa == null) {
							addDifference(differences, "Host " + hosts[i].getSimpleName() + " is missing '" + subpath.getKey() + "'");
							continue;
						}
						if(fa.isDirectory())
							subs.add(subpath.getKey());
						if(baseFa == null) {
							baseFa = fa;
							baseHost = hosts[i];
							continue;
						}
						if(!ConvertUtils.areEqual(baseFa.getPermissions(), fa.getPermissions()))
							addDifference(differences, "Host " + hosts[i].getSimpleName() + " has different file attributes for " + getTypeName(fa) + " '" + subpath.getKey() + "'");
						if(!ConvertUtils.areEqual(baseFa.getUID(), fa.getUID()))
							addDifference(differences, "Host " + hosts[i].getSimpleName() + " has a different owner for " + getTypeName(fa) + " '" + subpath.getKey() + "'");
						if(!ConvertUtils.areEqual(baseFa.getGID(), fa.getGID()))
							addDifference(differences, "Host " + hosts[i].getSimpleName() + " has a different group for " + getTypeName(fa) + " '" + subpath.getKey() + "'");
						if(!fa.isDirectory() && !baseFa.isDirectory()) {
							long m0 = baseFa.getModifiedTime().longValue();
							long m1 = fa.getModifiedTime().longValue();
							long s0 = baseFa.getSize().longValue();
							long s1 = fa.getSize().longValue();
							if(m0 == m1 && s0 == s1)
								continue; // assume file is same
							StringBuilder sb = new StringBuilder();
							if(m0 > m1)
								sb.append("Host ").append(hosts[i].getSimpleName()).append(" has an old version of ").append(getTypeName(fa)).append(" '").append(subpath.getKey()).append("'");
							else if(m0 < m1)
								sb.append("Host ").append(hosts[0].getSimpleName()).append(" has an old version of ").append(getTypeName(fa)).append(" '").append(subpath.getKey()).append("'");
							else
								sb.append("Host ").append(hosts[i].getSimpleName()).append(" has a different version of ").append(getTypeName(fa)).append(" '").append(subpath.getKey()).append("'");
							if(s0 > 0 && s1 > 0 && s0 < maxDiffSize && s1 < maxDiffSize) {
								if(baseBuffer == null) {
									baseBuffer = new HeapBufferStream(1024, (int) s0, true);
									UploadTask.readFileUsingTmp(baseHost, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, subpath.getKey(), baseBuffer.getOutputStream(), null);
								}
								jlibdiff.Diff diff = new jlibdiff.Diff();
								InputStream in = UploadTask.readFileUsingTmp(baseHost, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, subpath.getKey());
								try {
									diff.diffBuffer(new BufferedReader(new InputStreamReader(baseBuffer.getInputStream())), new BufferedReader(new InputStreamReader(in)));
								} finally {
									in.close();
								}
								if(diff.numberOfHunk() == 0)
									continue;
								sb.append(':');
								for(Object o : diff.getHunks())
									sb.append('\n').append(((Hunk) o).convert());
							}
							addDifference(differences, sb.toString());
						}
					}
				}
				for(String sub : subs)
					findDifferences(deployTaskInfo, hosts, sub, subpaths, differences);
			}

			protected String getTypeName(FileAttributes fa) {
				if(fa.isDirectory())
					return "directory";
				if(fa.isLink())
					return "link";
				if(fa.isFifo())
					return "fifo";
				if(fa.isFile())
					return "file";

				return "item";
			}

			protected void addDifference(Set<String> differences, String text) {
				differences.add(text);
				log.info("Found difference: " + text);
			}
		} };
	}

	@Override
	public String toString() {
		return getName();
	}
}
