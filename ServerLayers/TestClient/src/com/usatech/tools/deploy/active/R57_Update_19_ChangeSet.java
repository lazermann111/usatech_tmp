package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.posm.tasks.POSMSaleUpdateTask;
import com.usatech.posm.tasks.POSMSettlementFeedbackTask;
import com.usatech.posm.utils.POSMUtils;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R57_Update_19_ChangeSet extends MultiLinuxPatchChangeSet {
	public R57_Update_19_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/AppLayer/src/loader-data-layer.xml", "classes", "REL_R57_19", false, USATRegistry.LOADER);
		registerResource("ServerLayers/POSMLayer/conf/posm-data-layer.xml", "classes", "REL_R57_19", false, USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/POSMLayer/src/main", POSMSaleUpdateTask.class, "REL_R57_19", USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/POSMLayer/src/main", POSMSettlementFeedbackTask.class, "REL_R57_19", USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/POSMLayer/src/main", POSMUtils.class, "REL_R57_19", USATRegistry.POSM_LAYER);
	}
	
	@Override
	public String getName() {
		return "R57 Update 19 - Database performance - transaction import";
	}
}
