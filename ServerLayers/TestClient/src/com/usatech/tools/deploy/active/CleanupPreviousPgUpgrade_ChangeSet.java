package com.usatech.tools.deploy.active;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import simple.text.StringUtils;

import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.BasicServerSet;
import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.DeployTaskInfo;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.PgDeployMap;
import com.usatech.tools.deploy.PgDeployMap.PGDATADir;
import com.usatech.tools.deploy.Registry;
import com.usatech.tools.deploy.Server;
import com.usatech.tools.deploy.USATRegistry;

public class CleanupPreviousPgUpgrade_ChangeSet implements ChangeSet {
	protected final Registry registry;
	protected static final BitSet FIND_COMMAND_SUCCESS = new BitSet();
	static {
		FIND_COMMAND_SUCCESS.set(0);
		FIND_COMMAND_SUCCESS.set(1);
	}
	public CleanupPreviousPgUpgrade_ChangeSet() throws UnknownHostException {
		registry = new USATRegistry();
		registry.registerApp(USATRegistry.POSTGRES_KLS);
		registry.registerApp(USATRegistry.POSTGRES_MSR);
		registry.registerApp(USATRegistry.POSTGRES_MST);
		registry.registerApp(USATRegistry.POSTGRES_PGS);
	}
	@Override
	public String getName() {
		return "Cleanup Previous Postgres Upgrade";
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("MST") || host.isServerType("MSR") || host.isServerType("KLS") || host.isServerType("PGS");
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		List<DeployTask> tasks = new ArrayList<DeployTask>();
		BasicServerSet serverSet = registry.getServerSet(host.getServerEnv());
		for(String serverType : host.getServerTypes()) {
			Set<App> apps = serverSet.getApps(serverType);
			if(apps != null) {
				List<Server> servers = serverSet.getServers(serverType);
				if(servers != null) {
					for(App app : apps) {
						for(Iterator<Server> iter = servers.iterator(); iter.hasNext();) {
							Server server = iter.next();
							String[] instances = app.filterInstances(server.instances);
							if(server.isOnHost(host)) {
								for(String instance : instances) {
									tasks.add(createTask(host, app, instances.length > 1 && !StringUtils.isBlank(instance) && (instance = instance.trim()).matches("\\d+") ? Integer.parseInt(instance) : null));
								}
								break;
							}
						}
					}
				}
			}
		}
		return tasks.toArray(new DeployTask[tasks.size()]);
	}

	protected DeployTask createTask(Host host, App app, Integer ordinal) {
		final String appDir;
		PGDATADir tmpPgDir;
		if(USATRegistry.POSTGRES_KLS.equals(app) && (tmpPgDir = PgDeployMap.hostPGDATADirMap.get(host.getSimpleName())) != null) {
			appDir = tmpPgDir.getPrimaryDir();
		} else
			appDir = "/opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal);
		return new DeployTask() {
			@Override
			public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
				String[] ret = ExecuteTask.executeCommands(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.getSsh(), FIND_COMMAND_SUCCESS,
						"sudo su",
						"PG_VER=`cat " + appDir + "/data/PG_VERSION`; echo $PG_VER",
						"find " + appDir + "/ -maxdepth 1 -name '*_data'", 
						"find " + appDir + "/tblspace -maxdepth 2 -mindepth 2 -name 'PG_*' | grep -v \"/PG_$PG_VER\"_");

				String majorVersion = ret[1];
				String line;
				BufferedReader reader;
				for(int i = 2; i < ret.length; i++) {
					reader = new BufferedReader(new StringReader(ret[i]));
					while((line = reader.readLine()) != null) {
						String answer = deployTaskInfo.interaction.readLine("Postgres was previously upgraded to " + majorVersion + " for " + appDir + ". Should I delete the directory '" + line + "' (Y/N)?");
						if(answer != null && answer.toUpperCase().startsWith("Y")) {
							ExecuteTask.executeCommands(deployTaskInfo, "sudo /bin/rm -r " + line);
						}
					}
				}
				return "Complete";
			}

			@Override
			public String getDescription(DeployTaskInfo deployTaskInfo) {
				return "Remove old postgres data folders";
			}
		};
	}

	@Override
	public String toString() {
		return getName();
	}
}
