package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.report.ReportRequestFactory;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R50_Patch_8_ChangeSet extends MultiLinuxPatchChangeSet {
	public R50_Patch_8_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ReportGenerator/src/", ReportRequestFactory.class, "REL_report_generator_3_24_8", USATRegistry.RPTGEN_LAYER, USATRegistry.USALIVE_APP);
		registerResource("usalive/customerreporting_web/home_stat.jsp", "web", "REL_USALive_1_26_8", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/xsl/usalive-data-layer.xml", "classes", "REL_USALive_1_26_8", false, USATRegistry.USALIVE_APP);
	}
	
	@Override
	public String getName() {
		return "R50 Patch 8";
	}
}
