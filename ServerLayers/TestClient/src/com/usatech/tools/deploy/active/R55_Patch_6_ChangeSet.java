package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import simple.falcon.engine.standard.DirectXHTMLGenerator;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R55_Patch_6_ChangeSet extends MultiLinuxPatchChangeSet {
	public R55_Patch_6_ChangeSet() throws UnknownHostException {
		super();
		registerSource("Falcon/src", DirectXHTMLGenerator.class, "REL_report_generator_3_29_6", USATRegistry.RPTGEN_LAYER, USATRegistry.USALIVE_APP);
	}
	
	@Override
	public String getName() {
		return "R55 Patch 6 - Transactions in Payment";
	}
}
