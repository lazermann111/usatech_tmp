package com.usatech.tools.deploy.active;

import static com.usatech.tools.deploy.USATRegistry.POSTGRES_MST;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.UploadAndRunPostgresSQLTask;

public class PostgresAdminAsSuperuser_ChangeSet extends AbstractLinuxChangeSet {
	protected String sql = "ALTER USER ADMIN_1 SUPERUSER;";

	public PostgresAdminAsSuperuser_ChangeSet() throws UnknownHostException {
		super();
				
	}
	protected void registerApps() {
		registerApp(POSTGRES_MST);
	}
	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) {
	}

	@Override
	public String getName() {
		return "Postgres - Admin as Superuser";
	}
	@Override
	protected void addTasks(Host host, App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		tasks.add(new UploadAndRunPostgresSQLTask(sql, false, ordinal, "postgres"));
	}
}
