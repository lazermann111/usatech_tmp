package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R49_Patch_8_ChangeSet extends MultiLinuxPatchChangeSet {
	public R49_Patch_8_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/POSMLayer/conf/riskcheck-data-layer.xml", "classes", "REL_posmlayer_1_26_8", false, USATRegistry.POSM_LAYER);
		registerResource("ServerLayers/POSMLayer/conf/POSMLayerService.properties", "classes", "REL_posmlayer_1_26_8", true, USATRegistry.POSM_LAYER);
	}

	@Override
	public String getName() {
		return "R49 Patch 8";
	}
}
