package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.authoritylayer.InternalAuthorityTask;
import com.usatech.layers.common.constants.CardType;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.messagedata.MessageData_C2;
import com.usatech.networklayer.processors.EnqueueOnlyProcessor_Auth;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class AppleWalletVASDemo_ChangeSet extends MultiLinuxPatchChangeSet {
	public AppleWalletVASDemo_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/LayersCommon/src", EntryType.class, "HEAD", USATRegistry.NET_LAYER);
		registerSource("ServerLayers/LayersCommon/src", CardType.class, "HEAD", USATRegistry.NET_LAYER);
		registerSource("ServerLayers/LayersCommon/src", MessageData_C2.class, "HEAD", USATRegistry.NET_LAYER);
		registerSource("ServerLayers/NetLayer/src", EnqueueOnlyProcessor_Auth.class, "HEAD", USATRegistry.NET_LAYER);
		
		registerSource("ServerLayers/LayersCommon/src", EntryType.class, "HEAD", USATRegistry.APP_LAYER);
		registerSource("ServerLayers/LayersCommon/src", CardType.class, "HEAD", USATRegistry.APP_LAYER);
		registerSource("ServerLayers/LayersCommon/src", MessageData_C2.class, "HEAD", USATRegistry.APP_LAYER);
		
		registerSource("ServerLayers/LayersCommon/src", EntryType.class, "HEAD", USATRegistry.LOADER);
		registerSource("ServerLayers/LayersCommon/src", CardType.class, "HEAD", USATRegistry.LOADER);
		registerSource("ServerLayers/LayersCommon/src", MessageData_C2.class, "HEAD", USATRegistry.LOADER);
		
		registerSource("ServerLayers/LayersCommon/src", EntryType.class, "HEAD", USATRegistry.KEYMGR);
		registerSource("ServerLayers/LayersCommon/src", CardType.class, "HEAD", USATRegistry.KEYMGR);
		registerSource("ServerLayers/LayersCommon/src", MessageData_C2.class, "HEAD", USATRegistry.KEYMGR);
		
		registerSource("ServerLayers/LayersCommon/src", EntryType.class, "HEAD", USATRegistry.INAUTH_LAYER);
		registerSource("ServerLayers/LayersCommon/src", CardType.class, "HEAD", USATRegistry.INAUTH_LAYER);
		registerSource("ServerLayers/LayersCommon/src", MessageData_C2.class, "HEAD", USATRegistry.INAUTH_LAYER);
		registerSource("ServerLayers/AuthorityLayer/src", InternalAuthorityTask.class, "HEAD", USATRegistry.INAUTH_LAYER);
		
		registerSource("ServerLayers/LayersCommon/src", EntryType.class, "HEAD", USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/LayersCommon/src", CardType.class, "HEAD", USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/LayersCommon/src", MessageData_C2.class, "HEAD", USATRegistry.POSM_LAYER);
		
		registerSource("ServerLayers/LayersCommon/src", EntryType.class, "HEAD", USATRegistry.DMS_APP);
		registerSource("ServerLayers/LayersCommon/src", CardType.class, "HEAD", USATRegistry.DMS_APP);
		registerSource("ServerLayers/LayersCommon/src", MessageData_C2.class, "HEAD", USATRegistry.DMS_APP);
	}
	
	@Override
	public String getName() {
		return "Apple Wallet VAS Demo";
	}
}
