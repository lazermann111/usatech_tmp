package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.report.rest.CreditData;
import com.usatech.report.rest.CreditTran;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R48_Patch_3_ChangeSet extends MultiLinuxPatchChangeSet {
	public R48_Patch_3_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ReportGenerator/src/rgg-data-layer.xml", "classes/", "REL_report_generator_3_22_3", true, USATRegistry.RPTGEN_LAYER);
		registerSource("ReportGenerator/src/", CreditData.class, "REL_report_generator_3_22_3", USATRegistry.RPTGEN_LAYER);
		registerSource("ReportGenerator/src/", CreditTran.class, "REL_report_generator_3_22_3", USATRegistry.RPTGEN_LAYER);
	}
	
	@Override
	public String getName() {
		return "R48 Patch 3";
	}
}
