package com.usatech.tools.deploy.active.renew;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DefaultHostSpecificValue;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.FromCvsUploadTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.RenewJKSCertTask;

/**
 * NOTE: in cvs ApplicationBuilds/KeyManager/applet.jar needs to be latestest code in keymgr release gz file, which is an unsigned jar file
 * 
 * This class does the following step:
 * 1. back up /opt/USAT/keymgr/web/admin/applet.jar and /opt/USAT/conf/keystore.ks to /home/<userName>/jarsignerBackup with timestamp
 * 2. renew /opt/USAT/conf/keystore.ts jarsigner cert
 * 3. upload unsigned applet.jar to /opt/USAT/keymgr/web/admin/
 * 4. sign the applet.jar with the new jarsigner cert
 * 
 * After running, restart keymgr, do a testload to make sure it is working, do auth tests
 * 
 * Verify:
 * 1. /usr/jdk/latest/bin/keytool -list -rfc -keystore /opt/USAT/conf/keystore.ks -alias jarsigner| openssl x509 -noout -enddate
 * 2. /usr/jdk/latest/bin/jarsigner -verify -verbose -keystore /opt/USAT/conf/keystore.ks /opt/USAT/keymgr/web/admin/applet.jar
 * 3. from https://devkls11.usatech.com/admin do a "Test Load" to see it succeeds
 * 4. test a auth and sale
 * 5. check logs
 * 
 * Cert is valid for 2 years
 * @author yhe
 *
 */
public class RenewKLSJarsignerCert_ChangeSet implements ChangeSet {

	@Override
	public String getName() {
		return "Renew KLS jarsigner Certificate";
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("KLS");
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		String userName=System.getProperty("user.name");
		String backupFolderPath="/home/"+userName+"/jarsignerBackup/";
		List<DeployTask> tasks = new ArrayList<DeployTask>();
		ExecuteTask backupTask=new ExecuteTask("if [ ! -d "+backupFolderPath+" ]; then mkdir "+backupFolderPath+"; fi",
				"sudo /bin/cp /opt/USAT/conf/keystore.ks "+backupFolderPath+"keystore.ks.`date +%Y%m%d-%H%M%S`",
				"sudo /bin/cp /opt/USAT/keymgr/web/admin/applet.jar "+backupFolderPath+"applet.jar.`date +%Y%m%d-%H%M%S`");
		FromCvsUploadTask uploadUnsignedJar=new FromCvsUploadTask("ApplicationBuilds/KeyManager/applet.jar", "HEAD", "/opt/USAT/keymgr/web/admin/applet.jar", 0644, "keymgr", "keymgr", false);
		RenewJKSCertTask  renewCertTask = new RenewJKSCertTask(new Date(Long.MAX_VALUE), "/opt/USAT/conf/keystore.ks", cache, 0640, new DefaultHostSpecificValue("jarsigner"), new DefaultHostSpecificValue(host.getSimpleName()), "usasubca.usatech.com", "USATCodeSigning", System.getProperty("user.name"));
		ExecuteTask signJarTask=new ExecuteTask("sudo su",
				"/usr/jdk/latest/bin/jarsigner -keystore /opt/USAT/conf/keystore.ks /opt/USAT/keymgr/web/admin/applet.jar jarsigner",
				"chown keymgr:keymgr /opt/USAT/keymgr/web/admin/applet.jar",
				"chmod 644 /opt/USAT/keymgr/web/admin/applet.jar");
		tasks.add(backupTask);
		tasks.add(renewCertTask);
		tasks.add(uploadUnsignedJar);
		tasks.add(signJarTask);
		return tasks.toArray(new DeployTask[tasks.size()]);
	}

	@Override
	public String toString() {
		return getName();
	}
}
