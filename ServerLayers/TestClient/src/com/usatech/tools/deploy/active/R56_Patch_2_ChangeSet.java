package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.authoritylayer.USConnectApolloAuthorityTask;
import com.usatech.posm.tasks.POSMNextTask;
import com.usatech.report.MassDeviceUpdate;
import com.usatech.report.TerminalInfo;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.usalive.servlet.ColumnMapManageStep;
import com.usatech.usalive.servlet.ColumnMapSearchStep;

public class R56_Patch_2_ChangeSet extends MultiLinuxPatchChangeSet {
	public R56_Patch_2_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/AuthorityLayer/src", USConnectApolloAuthorityTask.class, "REL_R56_2", USATRegistry.OUTAUTH_LAYER);
		registerResource("ServerLayers/LayersCommon/src/common-data-layer.xml", "classes", "REL_R56_2", false, USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/POSMLayer/src/main", POSMNextTask.class, "REL_R56_2", USATRegistry.POSM_LAYER);
		registerResource("ReportGenerator/src/shared-data-layer.xml", "classes", "REL_R56_2", false, USATRegistry.RPTGEN_LAYER);
		registerSource("ReportGenerator/src/", MassDeviceUpdate.class, "REL_R56_2", USATRegistry.RPTGEN_LAYER);
		registerSource("ReportGenerator/src/", TerminalInfo.class, "REL_R56_2", USATRegistry.RPTGEN_LAYER);
		registerResource("usalive/xsl/device-data-layer.xml", "classes", "REL_R56_2", false, USATRegistry.USALIVE_APP);
		registerSource("usalive/src/", ColumnMapManageStep.class, "REL_R56_2", USATRegistry.USALIVE_APP);
		registerSource("usalive/src/", ColumnMapSearchStep.class, "REL_R56_2", USATRegistry.USALIVE_APP);
		registerResource("usalive/customerreporting_web/column_mapping.jsp", "web", "REL_R56_2", false, USATRegistry.USALIVE_APP);
	}
	
	@Override
	public String getName() {
		return "R56 Patch 2";
	}
}
