package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.posm.process.MassEftAdjustment;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R51_Patch_15_ChangeSet extends MultiLinuxPatchChangeSet {
	public R51_Patch_15_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/LayersCommon/src/common-data-layer.xml", "classes", "REL_posmlayer_1_28_15", false, USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/POSMLayer/src/main", MassEftAdjustment.class, "REL_posmlayer_1_28_15", USATRegistry.POSM_LAYER);
	}
	
	@Override
	public String getName() {
		return "R51 Patch 15";
	}
}
