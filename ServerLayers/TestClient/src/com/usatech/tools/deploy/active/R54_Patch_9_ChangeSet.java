package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.layers.common.messagedata.WSRequestMessageData;
import com.usatech.report.BasicFrequency;
import com.usatech.report.ReportRequestFactory;
import com.usatech.report.RequestUnsentTimedReportTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R54_Patch_9_ChangeSet extends MultiLinuxPatchChangeSet {
	public R54_Patch_9_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ReportGenerator/src/rgr-data-layer.xml", "classes/", "REL_report_generator_3_28_9", false, USATRegistry.RPTGEN_LAYER, USATRegistry.RPTREQ_LAYER);
		registerSource("ReportGenerator/src/", BasicFrequency.class, "REL_report_generator_3_28_9", USATRegistry.RPTGEN_LAYER, USATRegistry.RPTREQ_LAYER);
		registerSource("ReportGenerator/src/", ReportRequestFactory.class, "REL_report_generator_3_28_9", USATRegistry.RPTGEN_LAYER, USATRegistry.RPTREQ_LAYER, USATRegistry.USALIVE_APP);
		registerSource("ReportGenerator/src/", RequestUnsentTimedReportTask.class, "REL_report_generator_3_28_9", USATRegistry.RPTGEN_LAYER, USATRegistry.RPTREQ_LAYER);
		registerSource("ServerLayers/LayersCommon/src/", WSRequestMessageData.class, "REL_netlayer_igrinenko_test1", USATRegistry.NET_LAYER, USATRegistry.APP_LAYER);
	}
	
	@Override
	public String getName() {
		return "R54 Patch 9";
	}
}
