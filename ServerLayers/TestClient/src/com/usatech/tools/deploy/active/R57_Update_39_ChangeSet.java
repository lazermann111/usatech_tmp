package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.posm.tasks.POSMSettleUpdateTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R57_Update_39_ChangeSet extends MultiLinuxPatchChangeSet {
	public R57_Update_39_ChangeSet() throws UnknownHostException {
		super();		
		registerSource("ServerLayers/POSMLayer/src/main", POSMSettleUpdateTask.class, "REL_R57_39", USATRegistry.POSM_LAYER);
	}
	
	@Override
	public String getName() {
		return "R57 Update 39 - POSMLayer source_tran_id";
	}
}
