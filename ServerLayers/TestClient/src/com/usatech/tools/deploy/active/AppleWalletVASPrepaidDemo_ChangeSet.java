package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.ccs.ConsumerCommunicationTask;
import com.usatech.layers.common.constants.ConsumerCommAttrEnum;
import com.usatech.layers.common.constants.ConsumerCommType;
import com.usatech.prepaid.web.PrepaidUtils;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;
/**
 * The following is added to /opt/USAT/prepaid/specific/USAT_app_settings.properties and only need to be done once:
 * 
USAT.database.MSR_MQ.password.RPTREQ=RPTREQ_1
USAT.simplemq.msrPrimaryDataSource=MQ_1
USAT.simplemq.msrSecondaryDataSources=MQ_2,MQ_3,MQ_4
USAT.simplemq.msrClusterName=MSR

/opt/USAT/prepaid/lib/simple-selected.jar is patched using the prepaid-1_16_VAS.tar.gz This is for the mq support in prepaid
 
 * @author yhe
 *
 */
public class AppleWalletVASPrepaidDemo_ChangeSet extends MultiLinuxPatchChangeSet {
	public AppleWalletVASPrepaidDemo_ChangeSet() throws UnknownHostException {
		super();
		//prepaid
		registerSource("ServerLayers/LayersCommon/src", ConsumerCommType.class, "HEAD", USATRegistry.PREPAID_APP);
		registerSource("ServerLayers/LayersCommon/src", ConsumerCommAttrEnum.class, "HEAD", USATRegistry.PREPAID_APP);
		registerResource("Prepaid/web/images/Add_to_Apple_Wallet_rgb_US-UK.svg", "web/images/", "HEAD", false, USATRegistry.PREPAID_APP);
		registerResource("Prepaid/web/images/downarrow-gray.png", "web/images/", "HEAD", false, USATRegistry.PREPAID_APP);
		registerResource("Prepaid/web/images/save_to_android_pay_light.png", "web/images/", "HEAD", false, USATRegistry.PREPAID_APP);
		registerResource("Prepaid/web/images/m.svg", "web/images/", "HEAD", false, USATRegistry.PREPAID_APP);
		registerResource("Prepaid/web/images/more-card.svg", "web/images/", "HEAD", false, USATRegistry.PREPAID_APP);
		registerResource("Prepaid/web/walletDemo.html.jsp", "web/", "HEAD", false, USATRegistry.PREPAID_APP);
		registerResource("Prepaid/web/send_to_pass_demo.html.jsp", "web/", "HEAD", false, USATRegistry.PREPAID_APP);
		registerResource("Prepaid/src/prepaid.properties", "classes/", "HEAD", false, USATRegistry.PREPAID_APP);
		registerResource("Prepaid/src/prepaid-data-layer.xml", "classes/", "HEAD", false, USATRegistry.PREPAID_APP);
		registerSource("Prepaid/src", PrepaidUtils.class, "HEAD", USATRegistry.PREPAID_APP);
		registerResource("Prepaid/web/apass.html.jsp", "web/", "HEAD", false, USATRegistry.PREPAID_APP);
		
		//rptgen
		registerSource("ServerLayers/LayersCommon/src", ConsumerCommType.class, "HEAD", USATRegistry.RPTGEN_LAYER);
		registerSource("ServerLayers/LayersCommon/src", ConsumerCommAttrEnum.class, "HEAD", USATRegistry.RPTGEN_LAYER);
		registerSource("ReportGenerator/src", ConsumerCommunicationTask.class, "HEAD", USATRegistry.RPTGEN_LAYER);
		
		registerResource("Prepaid/web/images/icon-logo_more-white.png", "web/images/", "HEAD", false, USATRegistry.PREPAID_APP);
		registerResource("Prepaid/web/images/icon-logo_more-white.png", "web/images/", "HEAD", false, USATRegistry.PREPAID_APP);
		registerResource("Prepaid/web/demojs/vendor/jquery-1.9.1.min.js", "web/demojs/vendor/", "HEAD", false, USATRegistry.PREPAID_APP);
		registerResource("Prepaid/web/demojs/vendor/modernizr-2.6.2.min.js", "web/demojs/vendor/", "HEAD", false, USATRegistry.PREPAID_APP);
		registerResource("Prepaid/web/demojs/bootstrapBAK.js", "web/demojs/", "HEAD", false, USATRegistry.PREPAID_APP);
		registerResource("Prepaid/web/demojs/main.js", "web/demojs/", "HEAD", false, USATRegistry.PREPAID_APP);
		registerResource("Prepaid/web/demojs/bootstrap.minBAK.js", "web/demojs/", "HEAD", false, USATRegistry.PREPAID_APP);
		registerResource("Prepaid/web/demojs/bootstrap.min.js", "web/demojs/", "HEAD", false, USATRegistry.PREPAID_APP);
		registerResource("Prepaid/web/demojs/plugins.js", "web/demojs/", "HEAD", false, USATRegistry.PREPAID_APP);
		registerResource("Prepaid/web/demojs/bootstrap.js", "web/demojs/", "HEAD", false, USATRegistry.PREPAID_APP);
		registerResource("Prepaid/web/walletDemo2.html.jsp", "web/", "HEAD", false, USATRegistry.PREPAID_APP);
		registerResource("Prepaid/web/democss/main.css", "web/democss/", "HEAD", false, USATRegistry.PREPAID_APP);
		registerResource("Prepaid/web/democss/normalize.css", "web/democss/", "HEAD", false, USATRegistry.PREPAID_APP);
		registerResource("Prepaid/web/democss/fonts.css", "web/democss/", "HEAD", false, USATRegistry.PREPAID_APP);
		
	}
	
	@Override
	public String getName() {
		return "Apple Wallet VAS Prepaid Demo";
	}
}
