package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.dms.consumer.ConsumerConstants;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R57_Update_26_ChangeSet extends MultiLinuxPatchChangeSet {
	public R57_Update_26_ChangeSet() throws UnknownHostException {
		super();		
		registerSource("DMS/src", ConsumerConstants.class, "REL_R57_26", USATRegistry.DMS_APP);
	}
	
	@Override
	public String getName() {
		return "R57 Update 26 - Card Search ConsumerTransactionsStep index hint";
	}
}
