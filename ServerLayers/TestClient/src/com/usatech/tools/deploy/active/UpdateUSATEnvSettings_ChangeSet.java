package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import simple.bean.ConvertUtils;

import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.AppSettingUpdateFileTask;
import com.usatech.tools.deploy.BasicServerSet;
import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.Server;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.tools.deploy.UsatSettingUpdateFileTask;

public class UpdateUSATEnvSettings_ChangeSet implements ChangeSet {
	public UpdateUSATEnvSettings_ChangeSet() {
		super();
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		USATRegistry registry = new USATRegistry();
		App[] apps = new App[] { 
				USATRegistry.APP_LAYER,
				USATRegistry.LOADER,
				USATRegistry.POSM_LAYER,
				USATRegistry.INAUTH_LAYER,
				USATRegistry.DMS_APP,
				
				USATRegistry.KEYMGR, 

				USATRegistry.NET_LAYER,
				USATRegistry.OUTAUTH_LAYER,
				
				USATRegistry.RPTGEN_LAYER,
				USATRegistry.RPTREQ_LAYER,
				USATRegistry.USALIVE_APP, 
				USATRegistry.PREPAID_APP,
								
				USATRegistry.TRANSPORT_LAYER,				
		};
		List<DeployTask> tasks = new ArrayList<DeployTask>();
		BasicServerSet serverSet = registry.getServerSet(host.getServerEnv());
		tasks.add(new UsatSettingUpdateFileTask(serverSet));
		List<String> commands = new ArrayList<String>();
		for(App app : apps) {
			if(host.isServerType(app.getServerType())) {
				List<Server> servers = serverSet.getServers(app.getServerType());
				if(servers != null) {
					int instanceNum = 0;
					int instanceCount = app.getInstanceCount(servers);
					for(Iterator<Server> iter = servers.iterator(); iter.hasNext();) {
						Server server = iter.next();
						String[] instances = app.filterInstances(server.instances);
						if(server.isOnHost(host)) {
							for(String instance : instances) {
								instanceNum++;
								tasks.add(new AppSettingUpdateFileTask(app, ConvertUtils.convertSafely(Integer.class, instance, null), instanceNum, instanceCount, cache, serverSet));

								// restart apps
								commands.add("/opt/USAT/" + app.getName() + (instance == null ? "" : instance) + "/bin/" + app.getServiceName() + ".sh stop 10000");
							}
							break;
						}
						instanceNum += instances.length;
					}
				}
			}
		}
		if(!commands.isEmpty()) {
			commands.add(0, "sudo su");
			tasks.add(new ExecuteTask(commands.toArray(new String[commands.size()])));
		}
		return tasks.toArray(new DeployTask[tasks.size()]);
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("APR") || host.isServerType("NET") || host.isServerType("KLS") || host.isServerType("WEB") || host.isServerType("APP");
	}
	
	@Override
	public String toString() {
		return getName();
	}
	@Override
	public String getName() {
		return "Update USAT Environment Settings";
	}
}
