package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import simple.db.ConnectionGroup;
import simple.db.DefaultConnectionGroup;
import simple.db.PlainConnectionGroup;
import simple.mq.peer.*;

import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R53_Patch_8_ChangeSet extends MultiLinuxPatchChangeSet {
	public R53_Patch_8_ChangeSet() throws UnknownHostException {
		super();
		
		Map<App, String> apps = new HashMap<>();
		apps.put(USATRegistry.APP_LAYER, "REL_applayer_1_39_8");
		apps.put(USATRegistry.LOADER, "REL_applayer_1_39_8");
		apps.put(USATRegistry.POSM_LAYER, "REL_posmlayer_1_30_8");
		apps.put(USATRegistry.NET_LAYER, "REL_netlayer_1_39_8");
		apps.put(USATRegistry.INAUTH_LAYER, "REL_authoritylayer_1_39_8");
		apps.put(USATRegistry.OUTAUTH_LAYER, "REL_authoritylayer_1_39_8");
		apps.put(USATRegistry.KEYMGR, "REL_keymgr_2_20_8");
		apps.put(USATRegistry.RPTGEN_LAYER, "REL_report_generator_3_27_8");
		apps.put(USATRegistry.RPTREQ_LAYER, "REL_report_generator_3_27_8");
		apps.put(USATRegistry.USALIVE_APP, "REL_USALive_1_29_8");
		apps.put(USATRegistry.DMS_APP, "REL_DMS_1_15_8");
		//apps.put(USATRegistry.PREPAID_APP, "REL_prepaid_1_16_8");
		
		for(Map.Entry<App, String> entry : apps.entrySet()) {
			registerSource("Simple1.5/src", DirectPeerProducer.class, entry.getValue(), entry.getKey());
			registerSource("Simple1.5/src", DirectPeerProducerMXBean.class, entry.getValue(), entry.getKey());
			registerSource("Simple1.5/src", DirectPoller.class, entry.getValue(), entry.getKey());
			registerSource("Simple1.5/src", HybridDirectPoller.class, entry.getValue(), entry.getKey());
			registerSource("Simple1.5/src", DbPoller.class, entry.getValue(), entry.getKey());
			registerSource("Simple1.5/src", HybridDbPoller.class, entry.getValue(), entry.getKey());
			registerSource("Simple1.5/src", PeerSupervisor.class, entry.getValue(), entry.getKey());
			registerSource("Simple1.5/src", ConnectionGroup.class, entry.getValue(), entry.getKey());
			registerSource("Simple1.5/src", DefaultConnectionGroup.class, entry.getValue(), entry.getKey());
			registerSource("Simple1.5/src", PlainConnectionGroup.class, entry.getValue(), entry.getKey());
		}
	}
	
	@Override
	protected void addPostHostTasks(List<DeployTask> tasks) {
		super.addPostAppsTasks(tasks);
		tasks.add(new ExecuteTask("sudo su", "echo 'pcowan readwrite' >> /opt/USAT/conf/jmx.access"));
	}
	
	@Override
	public String getName() {
		return "R53 Patch 8";
	}
}
