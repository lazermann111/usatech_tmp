package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.usalive.servlet.IssueRefundsetStep;

public class R54_Patch_16_ChangeSet extends MultiLinuxPatchChangeSet {

	public R54_Patch_16_ChangeSet() throws UnknownHostException {
		super();
		registerSource("usalive/src/", IssueRefundsetStep.class, "REL_USALive_1_30_16", USATRegistry.USALIVE_APP);
	}

	@Override
	public String getName() {
		return "R54 Patch 16";
	}

}
