package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R57_Update_31_ChangeSet extends MultiLinuxPatchChangeSet {
	public R57_Update_31_ChangeSet() throws UnknownHostException {
		super();		
		registerResource("ServerLayers/LayersCommon/src/common-data-layer.xml", "classes", "REL_R57_31", false, USATRegistry.APP_LAYER);
	}
	
	@Override
	public String getName() {
		return "R57 Update 31 - Duplicate pending commands";
	}
}
