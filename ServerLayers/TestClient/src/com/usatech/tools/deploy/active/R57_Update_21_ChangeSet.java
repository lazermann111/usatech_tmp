package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.usalive.report.BuildFolioStep;

public class R57_Update_21_ChangeSet extends MultiLinuxPatchChangeSet {
	public R57_Update_21_ChangeSet() throws UnknownHostException {
		super();
		registerSource("usalive/src", BuildFolioStep.class, "REL_R57_21", USATRegistry.USALIVE_APP);
	}
	
	@Override
	public String getName() {
		return "R57 Update 21 - Duplicate report requests";
	}
}
