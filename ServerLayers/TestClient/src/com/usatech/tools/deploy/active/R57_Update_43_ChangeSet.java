package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.posm.tasks.EFTFinalizerTask;
import com.usatech.posm.tasks.EFTProcessorTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R57_Update_43_ChangeSet extends MultiLinuxPatchChangeSet {
	public R57_Update_43_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/POSMLayer/conf/POSMLayerService.properties", "classes", "REL_R57_43", true, USATRegistry.POSM_LAYER);
		registerResource("ServerLayers/POSMLayer/conf/eft-process-data-layer.xml", "classes", "REL_R57_43", false, USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/POSMLayer/src/main", EFTFinalizerTask.class, "REL_R57_43", USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/POSMLayer/src/main", EFTProcessorTask.class, "REL_R57_43", USATRegistry.POSM_LAYER);		
	}
	
	@Override
	public String getName() {
		return "R57 Update 43 - Updating ledger.partition_ts in POSMLayer";
	}
}
