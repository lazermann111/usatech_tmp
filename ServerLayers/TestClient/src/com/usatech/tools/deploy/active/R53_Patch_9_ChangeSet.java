package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import com.usatech.layers.common.messagedata.MessageData_C2;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;


public class R53_Patch_9_ChangeSet extends MultiLinuxPatchChangeSet {
	public R53_Patch_9_ChangeSet() throws UnknownHostException {
		super();
		
		Map<App, String> apps = new HashMap<>();
		apps.put(USATRegistry.APP_LAYER, "REL_applayer_1_39_9");
		apps.put(USATRegistry.NET_LAYER, "REL_netlayer_1_39_9");
		apps.put(USATRegistry.INAUTH_LAYER, "REL_authoritylayer_1_39_9");
		apps.put(USATRegistry.OUTAUTH_LAYER, "REL_authoritylayer_1_39_9");
		apps.put(USATRegistry.USALIVE_APP, "REL_USALive_1_29_9");
		apps.put(USATRegistry.DMS_APP, "REL_DMS_1_15_9");
		
		for(Map.Entry<App, String> entry : apps.entrySet()) {
			registerSource("ServerLayers/LayersCommon/src", MessageData_C2.class, entry.getValue(), entry.getKey());
		}
	}
	
	
	@Override
	public String getName() {
		return "R53 Patch 9";
	}
}
