package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.etl.DataLoader;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class RDW_Patch_3_17_2_ChangeSet extends MultiLinuxPatchChangeSet {
	public RDW_Patch_3_17_2_ChangeSet() throws UnknownHostException, ClassNotFoundException {
		super();
		registerSource("ReportGenerator/src", DataLoader.class, "REL_report_generator_3_17_2", USATRegistry.RDW_LOADER);
		registerSource("ReportGenerator/src", Class.forName("com.usatech.etl.DataLoaderTab"), "REL_report_generator_3_17_2", USATRegistry.RDW_LOADER);
	}

	@Override
	public String getName() {
		return "RDW Patch 3.17.2";
	}
}
