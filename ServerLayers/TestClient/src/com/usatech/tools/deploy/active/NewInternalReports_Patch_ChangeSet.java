package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.dms.report.DeviceActivationChangesStep;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class NewInternalReports_Patch_ChangeSet extends MultiLinuxPatchChangeSet {
	public NewInternalReports_Patch_ChangeSet() throws UnknownHostException {
		super();
		registerResource("Prepaid/src/prepaid.properties", "classes/", "BRN_R52", true, USATRegistry.PREPAID_APP);
		registerResource("DMS/resources/actions/general-actions.xml", "classes", "REL_device_activation_changes_report", false, USATRegistry.DMS_APP);
		registerResource("DMS/resources/datalayers/device-data-layer.xml", "classes", "REL_device_activation_changes_report", false, USATRegistry.DMS_APP);
		registerResource("DMS/resources/jsp/reports/reports_utilityMenu.jsp", "web/jsp/reports", "REL_device_activation_changes_report", false, USATRegistry.DMS_APP);
		registerSource("DMS/src", DeviceActivationChangesStep.class, "REL_device_activation_changes_report", USATRegistry.DMS_APP);		
	}
	
	@Override
	public String getName() {
		return "New Internal Reports";
	}
}
