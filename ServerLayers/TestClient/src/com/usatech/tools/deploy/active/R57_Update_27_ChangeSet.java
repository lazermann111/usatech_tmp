package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.dms.device.DeviceInitStep;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R57_Update_27_ChangeSet extends MultiLinuxPatchChangeSet {

	public R57_Update_27_ChangeSet() throws UnknownHostException {
		super();
		registerSource("DMS/src", DeviceInitStep.class, "REL_R57_27", USATRegistry.DMS_APP);
		registerResource("DMS/resources/jsp/devices/deviceInit.jsp", "web/jsp/devices", "REL_R57_27", true, USATRegistry.DMS_APP);
		registerResource("DMS/resources/datalayers/device-data-layer.xml", "classes", "REL_R57_27", true, USATRegistry.DMS_APP);
	}

	@Override
	public String getName() {
		return "R57 Update 27 - Device Activations - Error on Multiple Devices per Terminal";
	}

}
