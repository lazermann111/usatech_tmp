package com.usatech.tools.deploy.active;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;
import java.net.UnknownHostException;

public class R53_Patch_2_ChangeSet extends MultiLinuxPatchChangeSet {
	public R53_Patch_2_ChangeSet() throws UnknownHostException {
		super();
		registerResource("usalive/xsl/report-data-layer.xml", "classes/", "REL_USALive_1_29_2", false, USATRegistry.USALIVE_APP);
	}

	@Override
	public String getName() {
		return "R53 Patch 2";
	}
}
