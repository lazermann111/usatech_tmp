package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.dms.eft.SearchACHStep;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R53_Patch_5_ChangeSet extends MultiLinuxPatchChangeSet {
	public R53_Patch_5_ChangeSet() throws UnknownHostException {
		super();
		registerSource("DMS/src", SearchACHStep.class, "REL_DMS_1_15_5", USATRegistry.DMS_APP);
	}
	
	@Override
	public String getName() {
		return "R53 Patch 5";
	}
}
