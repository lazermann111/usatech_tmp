package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R46_Patch_AprivaProcessing_ChangeSet extends MultiLinuxPatchChangeSet {
	public R46_Patch_AprivaProcessing_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/AppLayer/src/applayer-data-layer.xml", "classes", "REL_applayer_1_32_AprivaProcessing", false, USATRegistry.APP_LAYER);
	}

	@Override
	public String getName() {
		return "R46 Patch Apriva Processing";
	}
}
