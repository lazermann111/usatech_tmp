package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.*;

public class R50_Patch_17_ChangeSet extends MultiLinuxPatchChangeSet {

	public R50_Patch_17_ChangeSet() throws UnknownHostException {
		super();
		registerAppRestart(USATRegistry.INAUTH_LAYER);
	}

	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(app == USATRegistry.INAUTH_LAYER) {
			String merchantCd = null;
			if (host.getServerEnv().equals("USA"))
				merchantCd = "920000000000";
			else
				merchantCd = "700000008124";
			
			String appPropertiesFile = String.format("/opt/USAT/%s%s/specific/USAT_app_settings.properties",
					app.getName(),
					instance == null ? "" : instance);
			
			String propertyKey = String.format("com.usatech.authoritylayer.TandemGatewayTask.authPartiallyReversed.whenLessByMerchant(%s)", merchantCd);
			
			PropertiesUpdateFileTask puft = new PropertiesUpdateFileTask(true, 0640, app.getUserName(), app.getUserName());
			puft.setFilePath(appPropertiesFile);
			puft.registerValue(propertyKey, new DefaultHostSpecificValue("true"));

			tasks.add(puft);
		}
		
		super.addRestartAppCommands(host, app, instance, instanceNum, instanceCount, tasks, commands, cache);
	}

	@Override
	public String getName() {
		return "R50 Patch 17";
	}
}
