package com.usatech.tools.deploy.active;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.List;
import java.util.regex.Pattern;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.IPTablesUpdateTask;
import com.usatech.tools.deploy.OptionalTask;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.UploadTask;

public class POSInterface_Setup_ChangeSet extends AbstractLinuxChangeSet {
	public static final File baseDir = new File(System.getProperty("rootSourceDir", "D:\\Development\\Java Projects"));
	protected static final File serverAppConfigDir = new File(baseDir, "server_app_config");
	
	public POSInterface_Setup_ChangeSet() throws UnknownHostException {
		super();
	}
	
	@Override
	protected void registerApps() {
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("NET") && host.getSimpleName().endsWith("1");
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		tasks.add(new OptionalTask("if [ -x /opt/USAT/posinterface/bin/jsvc ]; then echo 'YES'; else echo 'NO'; fi", Pattern.compile("NO"), false,				
				new UploadTask(new URL("http://mirror.symnds.com/software/Apache/tomcat/tomcat-7/v7.0.33/bin/apache-tomcat-7.0.33.tar.gz"), ".", 0640, null, null, false),
				new ExecuteTask("sudo su", 
				"tar xzvf apache-tomcat-7.0.33.tar.gz", 
				"mv apache-tomcat-7.0.33/ /opt/USAT/posinterface/", 
				"cd /opt/USAT/posinterface/bin",
				"export CATALINA_HOME=/opt/USAT/posinterface",
				"tar xvfz commons-daemon-native.tar.gz",
			    "cd commons-daemon-1.0.*-native-src/unix",
			    "./configure --with-java=/usr/jdk/latest",
			    "make",
			    "/bin/cp jsvc ../..",
				"/bin/cp /opt/USAT/posinterface/conf/server.xml /opt/USAT/posinterface/conf/server.xml.ORIG",
				"mkdir /opt/USAT/posinterface/webservices")
		));

		tasks.add(new UploadTask(new File(baseDir, "POSGateway/posinterface.war"), "/opt/USAT/posinterface/webservices", 0640, null, null, true));
		File file = new File(baseDir, "POSGateway/resources/config/POSServer/server_xml_service.xml");
		tasks.add(new UploadTask(new FileInputStream(file), 0640, "outauthlayer", "outauthlayer", file.getCanonicalPath(), true, "/opt/USAT/posinterface/conf/server.xml"));
		tasks.add(new ExecuteTask("sudo chown -R outauthlayer:usat /opt/USAT/posinterface"));
		file = new File(baseDir, "POSGateway/resources/config/POSServer/posinterface.monit.conf");
		tasks.add(new UploadTask(new FileInputStream(file), 0644, "root", "root", file.getCanonicalPath(), true, "/opt/USAT/monit/conf.d/posinterface.conf"));

		IPTablesUpdateTask iptTask = new IPTablesUpdateTask();
		iptTask.allowTcpIn(null, 9100);
		iptTask.allowTcpIn(null, 9101);
		tasks.add(iptTask);
	}

	@Override
	protected void addTasks(Host host, App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
	}

	@Override
	public String getName() {
		return "POSInterface Install";
	}

}
