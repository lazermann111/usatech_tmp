package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.SyncWithJKSTask;

public class JDKSyncJKS_ChangeSet implements ChangeSet {

	@Override
	public String getName() {
		return "JDK Sync JKS task";
	}

	@Override
	public boolean isApplicable(Host host) {
		return true;
		//return host.isServerType("MSR")||host.isServerType("APP")||host.isServerType("WEB");
	}

	
	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		String remoteDir="/usr/java/jdk1.8.0_144";
		List<DeployTask> tasks = new ArrayList<DeployTask>();
		if(host.isServerType("NET") || host.isServerType("WEB"))
			tasks.add(new SyncWithJKSTask(remoteDir + "/jre/lib/security/cacerts", "/opt/USAT/conf/truststore.ts", cache, 0640, "__jdk__"));
		else if(host.isServerType("APP"))
			tasks.add(new SyncWithJKSTask(remoteDir + "/jre/lib/security/cacerts", "/opt/USAT/conf/truststore.ts", cache, 0640, "__jdk__", Collections.singleton("addtrustexternalca")));
		return tasks.toArray(new DeployTask[tasks.size()]);
	}

	@Override
	public String toString() {
		return getName();
	}
}
