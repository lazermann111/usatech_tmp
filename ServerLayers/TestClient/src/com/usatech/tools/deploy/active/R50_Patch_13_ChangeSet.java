package com.usatech.tools.deploy.active;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;
import java.net.UnknownHostException;

public class R50_Patch_13_ChangeSet extends MultiLinuxPatchChangeSet {
    public R50_Patch_13_ChangeSet() throws UnknownHostException {
        super();
        registerResource("usalive/xsl/templates/ccs/user-report-frame.xsl", "classes/templates/ccs/", "REL_USALive_1_26_13", false, USATRegistry.USALIVE_APP);
    }

    @Override
    public String getName() {
        return "R50 Patch 13";
    }
}
