package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.layers.common.AppLayerUtils;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R51_Patch_Disable_Scheduled_Settlement_ChangeSet extends MultiLinuxPatchChangeSet {
	public R51_Patch_Disable_Scheduled_Settlement_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/AppLayer/src/applayer-data-layer.xml", "classes", "REL_applayer_1_37_Disable85", false, USATRegistry.APP_LAYER);
		registerResource("ServerLayers/AppLayer/src/applayer-data-layer.xml", "classes", "REL_applayer_1_37_Disable85", false, USATRegistry.LOADER);
		registerSource("ServerLayers/LayersCommon/src", AppLayerUtils.class, "REL_applayer_1_37_Disable85", USATRegistry.APP_LAYER, USATRegistry.LOADER);
	}
	
	@Override
	public String getName() {
		return "R51 Allow Disabling Scheduled Settlement";
	}
}
