package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.dms.consumer.ConsumerAccountStep;
import com.usatech.dms.consumer.ConsumerConstants;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R54_Patch_6_ChangeSet extends MultiLinuxPatchChangeSet {
	public R54_Patch_6_ChangeSet() throws UnknownHostException {
		super();
		registerSource("DMS/src", ConsumerAccountStep.class, "REL_DMS_1_16_6", USATRegistry.DMS_APP);
		registerSource("DMS/src", ConsumerConstants.class, "REL_DMS_1_16_6", USATRegistry.DMS_APP);
	}
	
	@Override
	public String getName() {
		return "R54 Patch 6";
	}
}
