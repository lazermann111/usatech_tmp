package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.report.MassDeviceUpdate;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R57_Update_30_ChangeSet extends MultiLinuxPatchChangeSet {
	public R57_Update_30_ChangeSet() throws UnknownHostException {
		super();		
		registerSource("ReportGenerator/src/", MassDeviceUpdate.class, "REL_R57_30", USATRegistry.RPTGEN_LAYER);
	}
	
	@Override
	public String getName() {
		return "R57 Update 30 - Enqueues - RPTGEN";
	}
}
