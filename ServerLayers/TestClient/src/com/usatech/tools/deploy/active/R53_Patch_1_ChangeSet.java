package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.dms.file.DFRFileFuncStep;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R53_Patch_1_ChangeSet extends MultiLinuxPatchChangeSet {
	public R53_Patch_1_ChangeSet() throws UnknownHostException {
		super();
		registerSource("DMS/src", DFRFileFuncStep.class, "REL_DMS_1_15_1", USATRegistry.DMS_APP);
		registerResource("DMS/resources/jsp/file/DFRFile.jsp", "web/jsp/file", "REL_DMS_1_15_1", false, USATRegistry.DMS_APP);
	}
	
	@Override
	public String getName() {
		return "R53 Patch 1";
	}
}
