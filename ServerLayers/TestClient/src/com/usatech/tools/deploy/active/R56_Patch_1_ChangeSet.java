package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.keymanager.service.CreateTokenTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R56_Patch_1_ChangeSet extends MultiLinuxPatchChangeSet {
	public R56_Patch_1_ChangeSet() throws UnknownHostException {
		super();
		registerSource("KeyManagerService/src", CreateTokenTask.class, "REL_R56_1", USATRegistry.KEYMGR);
		registerResource("KeyManagerService/src/KeyManagerService.properties", "classes", "REL_R56_1", true, USATRegistry.KEYMGR);
		registerResource("Prepaid/web/cards.html.jsp", "web", "REL_R56_1", false, USATRegistry.PREPAID_APP);
		registerResource("Prepaid/src/prepaid.properties", "classes", "REL_R56_1", true, USATRegistry.PREPAID_APP);
		registerResource("ReportGenerator/src/TransportService.properties", "classes", "REL_R56_1", true, USATRegistry.TRANSPORT_LAYER);
	}
	
	@Override
	public String getName() {
		return "R56 Patch 1";
	}
}
