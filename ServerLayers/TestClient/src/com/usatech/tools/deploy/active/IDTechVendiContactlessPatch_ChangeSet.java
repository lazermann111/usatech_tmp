package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.eportconnect.ECRequestHandler;
import com.usatech.keymanager.service.KeyManagerEngine;
import com.usatech.networklayer.processors.EnqueueOnlyProcessor_Auth;
import com.usatech.networklayer.processors.EnqueueOnlyProcessor_CardId;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class IDTechVendiContactlessPatch_ChangeSet extends MultiLinuxPatchChangeSet {
	public IDTechVendiContactlessPatch_ChangeSet() throws UnknownHostException {
		super();
		registerSource("KeyManagerService/src", KeyManagerEngine.class, "REL_vendi_contactless_decryption", USATRegistry.KEYMGR);
		registerSource("ServerLayers/NetLayer/src", ECRequestHandler.class, "REL_vendi_contactless_decryption", USATRegistry.NET_LAYER);
		registerSource("ServerLayers/NetLayer/src", EnqueueOnlyProcessor_Auth.class, "REL_vendi_contactless_decryption", USATRegistry.NET_LAYER);
		registerSource("ServerLayers/NetLayer/src", EnqueueOnlyProcessor_CardId.class, "REL_vendi_contactless_decryption", USATRegistry.NET_LAYER);
	}

	@Override
	public String getName() {
		return "IDTech Vendi Contactless Patch";
	}
}
