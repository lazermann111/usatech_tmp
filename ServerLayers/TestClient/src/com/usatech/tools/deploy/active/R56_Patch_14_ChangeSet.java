package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R56_Patch_14_ChangeSet extends MultiLinuxPatchChangeSet {
	public R56_Patch_14_ChangeSet() throws UnknownHostException {
		super();
		registerResource("Simple1.5/src/simple/db/specific/oracle-specific-data-layer.xml", "classes/simple/db/specific", "REL_R56_14", true, USATRegistry.USALIVE_APP, USATRegistry.RPTGEN_LAYER);
	}
	
	@Override
	public String getName() {
		return "R56 Patch 14";
	}
}
