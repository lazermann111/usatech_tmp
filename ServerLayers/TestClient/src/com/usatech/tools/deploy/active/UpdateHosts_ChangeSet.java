package com.usatech.tools.deploy.active;

import java.io.IOException;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;

public class UpdateHosts_ChangeSet implements ChangeSet {

	@Override
	public String getName() {
		return "Update Hosts";
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("APP");
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		return new DeployTask[] {
 new ExecuteTask("sudo su", "grep -c 'ec.usatech.com' /etc/hosts || echo '192.168.79.163 ec.usatech.com' >> /etc/hosts", "sed -i 's/javax\\.net\\.ssl\\.clientKeyAliasMap={\\.\\*:636=-,\\.\\*:5432=-}/javax.net.ssl.clientKeyAliasMap={.*:636=-,.*:5432=-,.*:9443=-}/g' /opt/USAT/prepaid/specific/USAT_app_settings.properties")
		};
	}

	@Override
	public String toString() {
		return getName();
	}
}
