package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.report.MassDeviceUpdate;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.usalive.servlet.ColumnMapManageStep;
import com.usatech.usalive.servlet.ColumnMapSearchStep;
import com.usatech.usalive.servlet.UserCustomerSettingsStep;

public class R56_Patch_3_ChangeSet extends MultiLinuxPatchChangeSet {
	public R56_Patch_3_ChangeSet() throws UnknownHostException {
		super();

		registerResource("ReportGenerator/src/shared-data-layer.xml", "classes", "REL_R56_3", false, USATRegistry.RPTGEN_LAYER);
		registerSource("ReportGenerator/src/", MassDeviceUpdate.class, "REL_R56_3", USATRegistry.RPTGEN_LAYER);

		registerResource("usalive/xsl/device-data-layer.xml", "classes", "REL_R56_3", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/xsl/user-data-layer.xml", "classes", "REL_R56_3", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/xsl/user-actions.xml", "classes", "REL_R56_3", false, USATRegistry.USALIVE_APP);
		registerSource("usalive/src/", ColumnMapManageStep.class, "REL_R56_3", USATRegistry.USALIVE_APP);
		registerSource("usalive/src/", ColumnMapSearchStep.class, "REL_R56_3", USATRegistry.USALIVE_APP);
		registerSource("usalive/src/", UserCustomerSettingsStep.class, "REL_R56_3", USATRegistry.USALIVE_APP);
		registerResource("usalive/customerreporting_web/column_mapping.jsp", "web", "REL_R56_3", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/customerreporting_web/preferences.jsp", "web", "REL_R56_3", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/xsl/templates/device/mass-update-instructs.xsl", "classes/templates/device/", "REL_R56_3", false, USATRegistry.USALIVE_APP);

		registerResource("ServerLayers/LayersCommon/src/common-data-layer.xml", "classes", "REL_R56_3", false, USATRegistry.POSM_LAYER);
	}

	@Override
	public String getName() {
		return "R56 Patch 3 - Planograms and Product Descriptions";
	}
}
