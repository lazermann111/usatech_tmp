package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.authoritylayer.AprivaAuthorityTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R50_Patch_15_ChangeSet extends MultiLinuxPatchChangeSet {
	public R50_Patch_15_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/AuthorityLayer/src/InsideAuthorityLayerService.properties", "classes", "REL_authoritylayer_1_36_15", true, USATRegistry.INAUTH_LAYER);
		registerSource("ServerLayers/AuthorityLayer/src", AprivaAuthorityTask.class, "REL_authoritylayer_1_36_15", USATRegistry.OUTAUTH_LAYER);
		registerResource("ServerLayers/AuthorityLayer/src/OutsideAuthorityLayerService.properties", "classes", "REL_authoritylayer_1_36_15", true, USATRegistry.OUTAUTH_LAYER);
		registerResource("ServerLayers/POSMLayer/conf/POSMLayerService.properties", "classes", "REL_posmlayer_1_27_15", true, USATRegistry.POSM_LAYER);
	}
	
	@Override
	public String getName() {
		return "R50 Patch 15";
	}

	@Override
	public String toString() {
		return getName();
	}
}
