package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

import simple.falcon.engine.standard.DirectXHTMLGenerator;

public class R58_Update_8_ChangeSet extends MultiLinuxPatchChangeSet {
	public R58_Update_8_ChangeSet() throws UnknownHostException {
		super();
		registerSource("Falcon/src", DirectXHTMLGenerator.class, "REL_R58_8", USATRegistry.RPTGEN_LAYER, USATRegistry.USALIVE_APP);
	}
	
	@Override
	public String getName() {
		return "R58 Update 8 - Alternate report output error";
	}
}
