package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.text.StringUtils;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.DeployTaskInfo;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;

public class GetFolioSql_ChangeSet implements ChangeSet {
	protected static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm");
	@Override
	public String getName() {
		return "Folio SQL Extraction";
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("APP");
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		return new DeployTask[] { extractFolioSqlTask };
	}

	protected final DeployTask extractFolioSqlTask = new DeployTask() {
		@Override
		public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
			String line = deployTaskInfo.interaction.readLine("For which User Groups would you like to extract folio sql?");
			if(StringUtils.isBlank(line))
				return "Skipping folio sql extract";
			long[] userGroupIds;
			try {
				userGroupIds = ConvertUtils.convert(long[].class, line);
			} catch(ConvertException e) {
				throw new IOException(e);
			}
			if(userGroupIds == null || userGroupIds.length == 0)
				return "Skipping folio sql extract";
			String env = deployTaskInfo.host.getServerEnv().toLowerCase();
			// write java source
			StringBuilder sb = new StringBuilder();
			sb.append(USATRegistry.baseDir.getAbsolutePath()).append("/ReportGenerator/test_src/ExtractFolioSql.java");
			// upload java source
			deployTaskInfo.getSftp().put(sb.toString(), "ExtractFolioSql.java");
			// compile java source
			List<String> commands = new ArrayList<String>();
			commands.add("sudo su");
			sb.setLength(0);
			sb.append("cd /opt/USAT/rptgen");
			if(!env.isEmpty() && !"USA".equalsIgnoreCase(env))
				sb.append('1');
			commands.add(sb.toString());
			sb.setLength(0);
			sb.append("/usr/jdk/latest/bin/javac -g -cp $(grep -m 1 \"CLASSPATH=\" bin/ReportGeneratorService.sh | cut -d= -f2- | sed 's/^\"\\(.*\\)\"$/\\1/') -d /home/`logname` /home/`logname`/ExtractFolioSql.java");
			commands.add(sb.toString());

			// create directory
			sb.setLength(0);
			sb.append("folio_sql_");
			if(env.isEmpty() || "USA".equalsIgnoreCase(env))
				sb.append("prod");
			else
				sb.append(env);
			sb.append('_').append(dateFormat.format(new Date())).append('_');
			for(long ug : userGroupIds)
				sb.append(ug).append('-');
			String baseFileName = sb.substring(0, sb.length() - 1);

			sb.setLength(0);
			sb.append("mkdir ").append(baseFileName);
			commands.add(sb.toString());

			// run java source to put into directory
			sb.setLength(0);
			sb.append("/usr/jdk/latest/bin/java -cp /home/`logname`:$(grep -m 1 \"CLASSPATH=\" bin/ReportGeneratorService.sh | cut -d= -f2- | sed 's/^\"\\(.*\\)\"$/\\1/') ExtractFolioSql -p ReportGeneratorService.properties /home/`logname`/");
			sb.append(baseFileName).append(' ');
			for(long ug : userGroupIds)
				sb.append(ug).append(',');
			
			commands.add(sb.substring(0, sb.length() - 1));

			// tar & gzip directory
			commands.add("cd /home/`logname`");
			sb.setLength(0);
			sb.append("zip ").append(baseFileName).append(".tar.gz ").append(baseFileName).append("/*");
			commands.add(sb.toString());
			sb.setLength(0);
			sb.append("chmod 0644 ").append(baseFileName).append(".tar.gz ");
			commands.add(sb.toString());
			sb.setLength(0);
			sb.append("/bin/rm -r ").append(baseFileName);
			commands.add(sb.toString());

			ExecuteTask.executeCommands(deployTaskInfo, commands);
			// download gzipped file
			sb.setLength(0);
			sb.append(baseFileName).append(".tar.gz");
			String zipFileName = sb.toString();
			sb.setLength(0);
			sb.append(USATRegistry.baseDir.getAbsolutePath()).append("/usalive/folio-sql/").append(zipFileName);
			deployTaskInfo.getSftp().get(zipFileName, sb.toString());
			sb.setLength(0);
			sb.append("Extracted Folios into ").append(zipFileName);
			return sb.toString();
		}

		@Override
		public String getDescription(DeployTaskInfo deployTaskInfo) {
			return "Extract Folio Sql";
		}
	};

	@Override
	public String toString() {
		return getName();
	}

}
