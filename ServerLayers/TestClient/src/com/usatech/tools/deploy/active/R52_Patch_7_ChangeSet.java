package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R52_Patch_7_ChangeSet extends MultiLinuxPatchChangeSet {
	public R52_Patch_7_ChangeSet() throws UnknownHostException {
		super();
		registerResource("usalive/web/include/rmaBottom.jsp", "web/include", "REL_USALive_1_28_7", false, USATRegistry.USALIVE_APP);
	}
	
	@Override
	public String getName() {
		return "R52 Patch 7";
	}
}
