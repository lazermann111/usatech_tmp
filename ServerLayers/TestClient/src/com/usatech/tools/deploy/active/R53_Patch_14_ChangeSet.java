package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.authoritylayer.UGrydAuthorityTask;
import com.usatech.authoritylayer.USConnectApolloAuthorityTask;
import com.usatech.layers.common.HttpRequester;
import com.usatech.layers.common.process.Processor;
import com.usatech.posm.schedule.dfr.ParseFinancialDFRTaskMXBean;
import com.usatech.posm.tasks.POSMNextTask;
import com.usatech.tools.deploy.*;

public class R53_Patch_14_ChangeSet extends MultiLinuxPatchChangeSet {
	
	public R53_Patch_14_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/AuthorityLayer/src/OutsideAuthorityLayerService.properties", "classes", "REL_authoritylayer_1_39_14", true, USATRegistry.OUTAUTH_LAYER);
		registerSource("ServerLayers/AuthorityLayer/src", USConnectApolloAuthorityTask.class, "REL_authoritylayer_1_39_14", USATRegistry.OUTAUTH_LAYER);
		registerSource("ServerLayers/LayersCommon/src", HttpRequester.class, "REL_authoritylayer_1_39_14", USATRegistry.OUTAUTH_LAYER);
		
		registerResource("ServerLayers/LayersCommon/src/common-data-layer.xml", "classes", "REL_posmlayer_1_30_14", false, USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/POSMLayer/src/main", POSMNextTask.class, "REL_posmlayer_1_30_14", USATRegistry.POSM_LAYER);
	}
	
	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(app == USATRegistry.OUTAUTH_LAYER) {
			String appPropertiesFile = String.format("/opt/USAT/%s%s/specific/USAT_app_settings.properties", app.getName(), instance == null ? "" : instance);
			PropertiesUpdateFileTask puft = new PropertiesUpdateFileTask(true, 0640, app.getUserName(), app.getUserName());
			puft.setFilePath(appPropertiesFile);
			
			puft.registerValue("USAT.authoritylayer.USConnectApollo.url", new ServerEnvHostSpecificValue(
					"http://dev.apollo.usconnect.blueworldinc.com", 
					"http://dev.apollo.usconnect.blueworldinc.com", 
					"http://dev.apollo.usconnect.blueworldinc.com", 
					"https://hercules.usconnectme.com"));
			
			puft.registerValue("USAT.authoritylayer.USConnectApollo.token", new ServerEnvHostSpecificValue(
					new GetPasswordHostSpecificValue(cache, "usconnect.token", "USConnect Apollo DEV", true),
					new GetPasswordHostSpecificValue(cache, "usconnect.token", "USConnect Apollo DEV", true),
					new GetPasswordHostSpecificValue(cache, "usconnect.token", "USConnect Apollo DEV", true),
					new GetPasswordHostSpecificValue(cache, "usconnect.token", "USConnect Apollo PROD", true)));
			
			puft.registerValue("USAT.authoritylayer.USConnectApollo.minimumBalanceAmount", new DefaultHostSpecificValue("0"));
			
			tasks.add(puft);
		}
		
		super.addRestartAppCommands(host, app, instance, instanceNum, instanceCount, tasks, commands, cache);
	}
	
	@Override
	public String getName() {
		return "R53 Patch 14";
	}
}
