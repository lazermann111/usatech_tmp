package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.authoritylayer.InternalAuthorityTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R57_Patch_13_ChangeSet extends MultiLinuxPatchChangeSet {
	public R57_Patch_13_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/AuthorityLayer/src/authority-data-layer.xml", "classes", "REL_R57_13", false, USATRegistry.INAUTH_LAYER);		
		registerSource("ServerLayers/AuthorityLayer/src", InternalAuthorityTask.class, "REL_R57_13", USATRegistry.INAUTH_LAYER);
	}
	
	@Override
	public String getName() {
		return "R57 Patch 13 - Internal Card Authorization Performance";
	}
}
