package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R51_Patch_3_ChangeSet extends MultiLinuxPatchChangeSet {
	public R51_Patch_3_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/POSMLayer/conf/posm-data-layer.xml", "classes", "REL_posmlayer_1_28_3", false, USATRegistry.POSM_LAYER);
	}
	
	@Override
	public String getName() {
		return "R51 Patch 3";
	}
}
