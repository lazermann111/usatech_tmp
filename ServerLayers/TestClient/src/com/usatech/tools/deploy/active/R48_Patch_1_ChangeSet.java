package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.app.ParallelBatchUpdateDatasetHandler;
import com.usatech.posm.schedule.dfr.DFRSetup;
import com.usatech.posm.schedule.dfr.ParseFinancialDFRTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R48_Patch_1_ChangeSet extends MultiLinuxPatchChangeSet {
	public R48_Patch_1_ChangeSet() throws UnknownHostException {
		super();
		registerResource("usalive/xsl/templates/selection/select-terminal-regions.xsl", "classes/templates/selection/", "REL_USALive_1_24_1", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/xsl/templates/selection/select-terminal-regions2.xsl", "classes/templates/selection/", "REL_USALive_1_24_1", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/xsl/templates/selection/select-terminal-customers.xsl", "classes/templates/selection/", "REL_USALive_1_24_1", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/xsl/templates/selection/select-terminal-customers2.xsl", "classes/templates/selection/", "REL_USALive_1_24_1", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/xsl/templates/device/terminal-regions.xsl", "classes/templates/device/", "REL_USALive_1_24_1", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/xsl/templates/device/terminal-customers.xsl", "classes/templates/device/", "REL_USALive_1_24_1", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/xsl/device-data-layer.xml", "classes/", "REL_USALive_1_24_1", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/xsl/selection-data-layer.xml", "classes/", "REL_USALive_1_24_1", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/region_admin.html.jsp", "web/", "REL_USALive_1_24_1", false, USATRegistry.USALIVE_APP);

		registerResource("ServerLayers/POSMLayer/conf/posm-data-layer.xml", "classes/", "REL_posmlayer_1_25_1", false, USATRegistry.POSM_LAYER);
		registerSource("USATMessaging/src", ParallelBatchUpdateDatasetHandler.class, "REL_posmlayer_1_25_1", USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/POSMLayer/src/main", DFRSetup.class, "REL_posmlayer_1_25_1", USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/POSMLayer/src/main", ParseFinancialDFRTask.class, "REL_posmlayer_1_25_1", USATRegistry.POSM_LAYER);
	}
	
	@Override
	public String getName() {
		return "R48 Patch 1";
	}
}
