package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.transport.TransportTask;

public class R57_Update_22_ChangeSet extends MultiLinuxPatchChangeSet {
	public R57_Update_22_ChangeSet() throws UnknownHostException {
		super();		
		registerSource("ReportGenerator/src", TransportTask.class, "REL_R57_22", USATRegistry.TRANSPORT_LAYER);
		registerResource("ReportGenerator/src/TransportService.properties", "classes", "REL_R57_22", true, USATRegistry.TRANSPORT_LAYER);
	}
	
	@Override
	public String getName() {
		return "R57 Update 22 - Seed EFT Detail Feed timeout";
	}
}
