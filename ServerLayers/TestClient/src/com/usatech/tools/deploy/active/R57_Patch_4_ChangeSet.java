package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.dms.action.ConfigFileActions;
import com.usatech.dms.device.BulkConfigWizard7Step;
import com.usatech.dms.device.BulkConfigWizard8Step;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R57_Patch_4_ChangeSet extends MultiLinuxPatchChangeSet {
	public R57_Patch_4_ChangeSet() throws UnknownHostException {
		super();
		registerResource("DMS/resources/actions/config-actions.xml", "classes", "REL_R57_4", false, USATRegistry.DMS_APP);
		registerResource("DMS/resources/datalayers/device-data-layer.xml", "classes", "REL_R57_4", false, USATRegistry.DMS_APP);
		registerSource("DMS/src", BulkConfigWizard7Step.class, "REL_R57_4", USATRegistry.DMS_APP);
		registerSource("DMS/src", BulkConfigWizard8Step.class, "REL_R57_4", USATRegistry.DMS_APP);
		registerSource("DMS/src", ConfigFileActions.class, "REL_R57_4", USATRegistry.DMS_APP);
	}
	
	@Override
	public String getName() {
		return "R57 Patch 4 - Device Configuration Wizard";
	}
}
