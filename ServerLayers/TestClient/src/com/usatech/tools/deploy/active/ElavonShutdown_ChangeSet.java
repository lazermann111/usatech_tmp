package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class ElavonShutdown_ChangeSet extends MultiLinuxPatchChangeSet {
	public ElavonShutdown_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/AuthorityLayer/src/InsideAuthorityLayerService.properties", "classes", "REL_elavon_shutdown", true, USATRegistry.INAUTH_LAYER);
		registerResource("ServerLayers/AuthorityLayer/src/OutsideAuthorityLayerService.properties", "classes", "REL_elavon_shutdown", true, USATRegistry.OUTAUTH_LAYER);
	}

	@Override
	public String getName() {
		return "Elavon Shutdown";
	}
}
