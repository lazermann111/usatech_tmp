package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;

public class R51_Patch_17_ChangeSet extends MultiLinuxPatchChangeSet {
	public R51_Patch_17_ChangeSet() throws UnknownHostException {
		super();
	}

	@Override
	public String getName() {
		return "R51 Patch 17";
	}
	
	@Override
	public boolean isApplicable(Host host) {
		return super.isApplicable(host) || host.isServerType("APR") || host.isServerType("NET") || host.isServerType("WEB") || host.isServerType("APP") || host.isServerType("KLS");
	}

	@Override
	protected void addPostHostTasks(List<DeployTask> tasks) {
		super.addPostAppsTasks(tasks);
		tasks.add(new ExecuteTask("sudo su", "echo 'pcowan readwrite' >> /opt/USAT/conf/jmx.access"));
	}
}
