package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.layers.common.messagedata.WS2GetCardMessageData;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R57_Update_16_ChangeSet extends MultiLinuxPatchChangeSet {
	public R57_Update_16_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/LayersCommon/src", WS2GetCardMessageData.class, "REL_R57_16", USATRegistry.APP_LAYER, USATRegistry.NET_LAYER);
	}
	
	@Override
	public String getName() {
		return "R57 Update 16 - Loading cardInfo auths into database";
	}
}
