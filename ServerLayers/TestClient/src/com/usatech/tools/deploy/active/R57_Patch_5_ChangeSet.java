package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R57_Patch_5_ChangeSet extends MultiLinuxPatchChangeSet {
	public R57_Patch_5_ChangeSet() throws UnknownHostException {
		super();
		registerResource("usalive/customerreporting_web/preferences.jsp", "web", "REL_R57_5", false, USATRegistry.USALIVE_APP);
	}
	
	@Override
	public String getName() {
		return "R57 Patch 5 - Error on USALive Preferences and Setup";
	}
}
