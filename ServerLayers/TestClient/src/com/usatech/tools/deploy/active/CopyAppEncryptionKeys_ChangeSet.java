package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.RotateMessageChainEncryptionKeyTask;
import com.usatech.tools.deploy.USATRegistry;

public class CopyAppEncryptionKeys_ChangeSet extends AbstractLinuxChangeSet {
	public CopyAppEncryptionKeys_ChangeSet() throws UnknownHostException {
		super();
	}
	@Override
	protected void registerApps() {
		registerApp(USATRegistry.APP_LAYER);
		registerApp(USATRegistry.DMS_APP);
		registerApp(USATRegistry.INAUTH_LAYER);
		registerApp(USATRegistry.POSM_LAYER);
		registerApp(USATRegistry.NET_LAYER);
		registerApp(USATRegistry.OUTAUTH_LAYER);
		registerApp(USATRegistry.LOADER);
		registerApp(USATRegistry.KEYMGR);
		registerApp(USATRegistry.PREPAID_APP);
		registerApp(USATRegistry.RDW_LOADER);
		registerApp(USATRegistry.RPTGEN_LAYER);
		registerApp(USATRegistry.RPTREQ_LAYER);
		registerApp(USATRegistry.TRANSPORT_LAYER);
		registerApp(USATRegistry.USALIVE_APP);
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		tasks.add(new RotateMessageChainEncryptionKeyTask(new Date(System.currentTimeMillis() + 180 * 24 * 60 * 60 * 1000L), "/opt/USAT/conf/keystore.ks", "/opt/USAT/conf/truststore.ts", 0640, 0640, registry.getServerSet(host.getServerEnv()), true, new Date(System.currentTimeMillis() - 365 * 24 * 60 * 60 * 1000L)));
	}
	
	@Override
	protected void addTasks(Host host, App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {

	}

	@Override
	public String getName() {
		return "Copy App Encryption Keys";
	}
}
