package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class BetaUpdate_ChangeSet extends MultiLinuxPatchChangeSet {
	public BetaUpdate_ChangeSet() throws UnknownHostException {
		super();
		registerResource("usalive/customerreporting_web/manage_campaign.jsp", "web", "REL_beta_update", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/css/usalive-app-style.css", "web/css", "REL_beta_update", false, USATRegistry.HTTPD_WEB, USATRegistry.USALIVE_APP);
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.USALIVE_APP);
	}
	
	@Override
	public String getName() {
		return "Beta Update";
	}
}
