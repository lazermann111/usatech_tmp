package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.authoritylayer.TandemGatewayTask;
import com.usatech.iso8583.interchange.tandem.TandemMsg;
import com.usatech.layers.common.constants.TLVTag;
import com.usatech.layers.common.util.EMVIdtechDecryptValueHandler;
import com.usatech.layers.common.util.VendXParsing;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R58_Update_2_ChangeSet extends MultiLinuxPatchChangeSet {
	public R58_Update_2_ChangeSet() throws UnknownHostException {
		super();
		registerSource("middleware/ISO8583/src", TandemMsg.class, "BRN_R58_2", USATRegistry.INAUTH_LAYER);
		registerSource("ServerLayers/LayersCommon/src", EMVIdtechDecryptValueHandler.class, "BRN_R58_2", USATRegistry.INAUTH_LAYER);
		registerSource("ServerLayers/LayersCommon/src", TLVTag.class, "BRN_R58_2", USATRegistry.INAUTH_LAYER);
		registerSource("ServerLayers/LayersCommon/src", VendXParsing.class, "BRN_R58_2", USATRegistry.INAUTH_LAYER);
		registerSource("ServerLayers/AuthorityLayer/src", TandemGatewayTask.class, "BRN_R58_2", USATRegistry.INAUTH_LAYER);
	}
	
	@Override
	public String getName() {
		return "R58 Update 2 - Results of Canadian Retail Regression testing";
	}
}
