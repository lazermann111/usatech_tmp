package com.usatech.tools.deploy.active;

import java.io.IOException;

import simple.io.HeapBufferStream;

import com.sshtools.j2ssh.SftpClient;
import com.sshtools.j2ssh.sftp.FileAttributes;
import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.DeployTaskInfo;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;

public class Test_ChangeSet implements ChangeSet {

	@Override
	public String getName() {
		return "Test Features";
	}

	@Override
	public boolean isApplicable(Host host) {
		return true;
		//return host.isServerType("MSR")||host.isServerType("APP")||host.isServerType("WEB");
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		/*
		CvsPullTask pullTask = new CvsPullTask("ApplicationBuilds/KeyManager/keymgr-2_1_0.tar.gz", "HEAD");
		
		return new DeployTask[] {
				pullTask,
				new UploadTask(pullTask.getResultInputStream(), 0644, null, null, StringUtils.substringAfterLast(pullTask.getFilePath(), "/"), true, "./keymgr-2_1_0.tar.gz.fromcvs")
		};
		*/
		return new DeployTask[] { new DeployTask() {
			@Override
			public String getDescription(DeployTaskInfo deployTaskInfo) {
				return "Testing privileged SFTP";
			}

			@Override
			public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
				String path = "/opt/USAT/lost+found";
				// ExecuteTask.executeCommands(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.getSsh(), "sudo su");
				// SftpClient sftp = ExecuteTask.getPrivilegedSftp(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.getSsh());
				SftpClient sftp = deployTaskInfo.getSftp();
				HeapBufferStream buffer = new HeapBufferStream();
				FileAttributes fa = sftp.stat(path); // sftp.get(path, buffer.getOutputStream());
				return "It worked! Got file attributes " + fa + " and read " + buffer.size() + " bytes";
			}
		} };
	}

	@Override
	public String toString() {
		return getName();
	}
}
