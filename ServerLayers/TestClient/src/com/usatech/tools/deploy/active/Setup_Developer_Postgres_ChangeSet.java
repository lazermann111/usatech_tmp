package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Set;

import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.DeployTaskInfo;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.GitPullTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.PostgresSetupTask;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.tools.deploy.UploadAndRunPostgresSQLTask;

import simple.bean.ConvertUtils;
import simple.text.StringUtils;

public class Setup_Developer_Postgres_ChangeSet implements ChangeSet {
	protected static final GitPullTask mqSetupCmd = new GitPullTask("Simple1.5/src/simple/mq/peer/mq_peer_postgres_setup.sql");
	protected static final GitPullTask frSetupCmd = new GitPullTask("DatabaseScripts/filerepo/setup_file_repo_objects.sql");
	protected static final GitPullTask kmSetupCmd = new GitPullTask("KeyManagerService/src/km_setup.sql");
	protected static final GitPullTask appSetupCmd = new GitPullTask("DatabaseScripts/edge_implementation/setup_app_db.sql");
	protected static final GitPullTask geoSetupCmd = new GitPullTask("DatabaseScripts/releases/REL_Edge_Update_R46/INSERT_GEOCODE_COUNTRY.sql");

	protected static final BitSet COMMAND_SUCCESS_EXTRA = new BitSet(2);

	static {
		COMMAND_SUCCESS_EXTRA.set(0);
		COMMAND_SUCCESS_EXTRA.set(1);
	}
	
	protected static interface DbSetup {
		public String getTablespace(String inits, int dbNum, int instanceNum);

		public String getDatabase(String inits, int dbNum, int instanceNum);

		public String getSchema(String inits, int dbNum, int instanceNum);

		public GitPullTask[] getSetupFiles();
	}
	
	protected final static DbSetup DB_SETUP_MQ = new DbSetup() {
		public String getTablespace(String inits, int dbNum, int instanceNum) {
			return inits == null ? "mq_data" : inits + "_mq_data_" + dbNum;
		}

		public String getSchema(String inits, int dbNum, int instanceNum) {
			return "mq";
		}

		public String getDatabase(String inits, int dbNum, int instanceNum) {
			return inits == null ? "mq" : inits + "mq" + dbNum;
		}

		public GitPullTask[] getSetupFiles() {
			return new GitPullTask[] { mqSetupCmd };
		}
	};

	protected final static DbSetup DB_SETUP_FILEREPO = new DbSetup() {
		public String getTablespace(String inits, int dbNum, int instanceNum) {
			return inits == null ? "file_repo_data" : inits + "_file_repo_data_" + dbNum;
		}

		public String getSchema(String inits, int dbNum, int instanceNum) {
			return "file_repo";
		}

		public String getDatabase(String inits, int dbNum, int instanceNum) {
			return inits == null ? "filerepo" : inits + "filerepo" + dbNum;
		}

		public GitPullTask[] getSetupFiles() {
			return new GitPullTask[] { frSetupCmd };
		}
	};

	protected final static DbSetup DB_SETUP_KM = new DbSetup() {
		public String getTablespace(String inits, int dbNum, int instanceNum) {
			return inits == null ? "km_data" : inits + "_km_data_" + dbNum;
		}

		public String getSchema(String inits, int dbNum, int instanceNum) {
			return "km";
		}

		public String getDatabase(String inits, int dbNum, int instanceNum) {
			return inits == null ? "km" : inits + "km" + dbNum;
		}

		public GitPullTask[] getSetupFiles() {
			return new GitPullTask[] { kmSetupCmd };
		}
	};

	protected final static DbSetup DB_SETUP_APP = new DbSetup() {
		public String getTablespace(String inits, int dbNum, int instanceNum) {
			if(dbNum == 1)
				return "app_data_" + instanceNum;
			return null;
		}

		public String getSchema(String inits, int dbNum, int instanceNum) {
			return "app_cluster";
		}

		public String getDatabase(String inits, int dbNum, int instanceNum) {
			if(dbNum == 1)
				return "app_" + instanceNum;
			return null;
		}

		public GitPullTask[] getSetupFiles() {
			return new GitPullTask[] { appSetupCmd, geoSetupCmd };
		}
	};
	
	public Setup_Developer_Postgres_ChangeSet() {
		super();
	}

	public boolean isApplicable(Host host) {
		return (host.isServerType("MST") || host.isServerType("PGS")) && "LOCAL".equalsIgnoreCase(host.getServerEnv());
	}

	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		DeployTask compositeTask;
		if(host.isServerType("PGS")) {
			compositeTask = new DeployTask() {
				@Override
				public String getDescription(DeployTaskInfo deployTaskInfo) {
					return "Setting up users on PGS database for new developer";
				}

				@Override
				public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
					String instance = deployTaskInfo.interaction.readLine("Enter the instance number the new developer will use (1-99)");
					if(StringUtils.isBlank(instance))
						return "Canceled by user";
					int instanceNum = ConvertUtils.getIntSafely(instance, -1);
					if(instanceNum < 1 || instanceNum > 99)
						throw new IOException("Invalid instance '" + instance + "'. Must be 1-99");
					PostgresSetupTask pst = new PostgresSetupTask("/opt/USAT/postgres", "postgres", 5432);
					// only add PostgresSetupTask when it is active node
					pst.registerRole("admin", null, "read_rdw", "read_main", "write_rdw", "write_main");
					pst.registerRole("admin_1", new String[] { "SUPERUSER", "LOGIN" }, "admin");
					pst.registerUser("monitor", "read_rdw", "read_main");
					App[] remoteApps = new App[] { USATRegistry.RPTGEN_LAYER };
					for(App remoteApp : remoteApps) {
						pst.registerUser(remoteApp.getDbUserName() + '_' + instanceNum, "read_rdw", "read_main", "write_main");
					}
					remoteApps = new App[] { USATRegistry.USALIVE_APP };
					for(App remoteApp : remoteApps) {
						pst.registerUser(remoteApp.getDbUserName() + '_' + instanceNum, "read_rdw", "read_main");
					}
					remoteApps = new App[] { USATRegistry.LOADER };
					for(App remoteApp : remoteApps) {
						pst.registerUser(remoteApp.getDbUserName() + '_' + instanceNum, "read_main", "write_main");
					}
					remoteApps = new App[] { USATRegistry.RDW_LOADER };
					for(App remoteApp : remoteApps) {
						pst.registerUser(remoteApp.getDbUserName() + '_' + instanceNum, "read_rdw", "write_rdw", "read_main");
					}
					remoteApps = new App[] { USATRegistry.DMS_APP, USATRegistry.POSM_LAYER };
					for(App remoteApp : remoteApps) {
						pst.registerUser(remoteApp.getDbUserName() + '_' + instanceNum, "read_main", "write_main");
					}
					return pst.perform(deployTaskInfo);
				}
			};
		} else {
			compositeTask = new DeployTask() {
			@Override
			public String getDescription(DeployTaskInfo deployTaskInfo) {
				return "Setting up PG databases for new developer";
			}

			@Override
			public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
				String inits = deployTaskInfo.interaction.readLine("Enter the initials of the new developer (2 characters)");
				if(StringUtils.isBlank(inits))
					return "Canceled by user";
				if(inits.length() != 2)
					throw new IOException("Invalid initials '" + inits + "'. Must be two characters");
				String instance = deployTaskInfo.interaction.readLine("Enter the instance number the new developer will use (1-99)");
				if(StringUtils.isBlank(instance))
					return "Canceled by user";
				int instanceNum = ConvertUtils.getIntSafely(instance, -1);
				if(instanceNum < 1 || instanceNum > 99)
					throw new IOException("Invalid instance '" + instance + "'. Must be 1-99");
				PostgresSetupTask pst = new PostgresSetupTask("/opt/USAT/postgres", "postgres", 5432);
				pst.registerRole("admin", null, "read_mq", "use_mq", "write_file_repo");
				pst.registerRole("admin_1", new String[] { "SUPERUSER", "LOGIN" }, "admin");
				pst.registerUser("monitor", "read_mq");
				Set<App> remoteApps = new HashSet<App>();
				if(host.isServerType("MST")) {
					remoteApps.add(USATRegistry.NET_LAYER);
					remoteApps.add(USATRegistry.OUTAUTH_LAYER);
					remoteApps.add(USATRegistry.APP_LAYER);
					remoteApps.add(USATRegistry.INAUTH_LAYER);
					remoteApps.add(USATRegistry.POSM_LAYER);
					remoteApps.add(USATRegistry.KEYMGR);
					remoteApps.add(USATRegistry.LOADER);
					remoteApps.add(USATRegistry.DMS_APP);
				}
				if(host.isServerType("MSR") || (host.isServerType("MST") && host.getServerEnv().equalsIgnoreCase("LOCAL"))) {
					remoteApps.add(USATRegistry.RPTREQ_LAYER);
					remoteApps.add(USATRegistry.RPTGEN_LAYER);
					remoteApps.add(USATRegistry.TRANSPORT_LAYER);
					remoteApps.add(USATRegistry.USALIVE_APP);
					remoteApps.add(USATRegistry.LOADER);
				}
				for(App remoteApp : remoteApps) {
					pst.registerUser(remoteApp.getDbUserName() + '_' + instanceNum, "use_mq", "write_file_repo");
					if(USATRegistry.APP_LAYER.equals(remoteApp))
						pst.registerRole(remoteApp.getDbUserName() + '_' + instanceNum, null, "read_app", "write_app");
					if(USATRegistry.KEYMGR.equals(remoteApp)) {
						pst.registerRole(remoteApp.getDbUserName() + '_' + instanceNum, null, "use_km");
						pst.registerRole(remoteApp.getDbUserName() + '_' + instanceNum, null, "read_km", "use_km");
						pst.registerUser("audit", "read_km");
					}
				}
				deployTaskInfo.interaction.printf(pst.getDescription(deployTaskInfo));
				deployTaskInfo.interaction.printf(pst.perform(deployTaskInfo));

				DbSetup[] dbSetups = new DbSetup[] { DB_SETUP_MQ, DB_SETUP_FILEREPO, DB_SETUP_APP, DB_SETUP_KM };

				String tbsDir = "/opt/USAT/postgres/tblspace";

				for(DbSetup dbSetup : dbSetups) {
					for(int dbNum = 1; dbNum <= 2; dbNum++) {
						String database = dbSetup.getDatabase(inits, dbNum, instanceNum);
						if(database == null)
							continue;
						String tablespace = dbSetup.getTablespace(inits, dbNum, instanceNum);
						if(tablespace == null)
							continue;
						String schema = dbSetup.getSchema(inits, dbNum, instanceNum);
						String[] result = ExecuteTask.executeCommands(deployTaskInfo, "sudo su root -c \"if [ ! -d " + tbsDir + "/" + tablespace + " ]; then echo 'yes'; fi\"");
						if("yes".equals(result[0])) {
							deployTaskInfo.interaction.printf("Creating tablespace '%1$s' and database '%2$s'", tablespace, database);
							ExecuteTask.executeCommands(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.getSsh(), COMMAND_SUCCESS_EXTRA,
									"sudo su", 
									"mkdir " + tbsDir + "/" + tablespace, 
									"chown -R postgres:postgres " + tbsDir + "/" + tablespace,
									"su - postgres", 
									"/usr/pgsql-latest/bin/psql postgres -c \"CREATE TABLESPACE " + tablespace + " LOCATION '" + tbsDir + "/" + tablespace + "';\"",
									"/usr/pgsql-latest/bin/psql postgres -c \"GRANT CREATE ON TABLESPACE " + tablespace + " TO admin;\"", 
									"/usr/pgsql-latest/bin/createdb -D " + tablespace + " " + database);
						}
						result = ExecuteTask.executeCommands(deployTaskInfo, "sudo su - postgres -c '/usr/pgsql-latest/bin/psql " + database + " -P t=on -q -c \"SELECT COUNT(*) FROM PG_TABLES WHERE SCHEMANAME = '\"'" + schema + "'\"';\"'");
						if(result[0] != null && result[0].trim().equals("0")) {
							GitPullTask[] setupFiles = dbSetup.getSetupFiles();
							for(int i = 0; i < setupFiles.length; i++) {
								DeployTask task = new UploadAndRunPostgresSQLTask(setupFiles[i], Boolean.FALSE, database);
								deployTaskInfo.interaction.printf(task.getDescription(deployTaskInfo));
								deployTaskInfo.interaction.printf(task.perform(deployTaskInfo));
							}
						}
					}
				}
				return "Setup complete for developer '" + inits + "', instance " + instance;
			}
		};
		}
		return new DeployTask[] { compositeTask };
	}
	
	@Override
	public String getName() {
		return "Postgres Developer Setup";
	}

	public String toString() {
		return getName();
	}
}
