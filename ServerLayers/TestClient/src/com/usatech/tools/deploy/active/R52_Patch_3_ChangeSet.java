package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.transport.AbstractHttpTransporter;

public class R52_Patch_3_ChangeSet extends MultiLinuxPatchChangeSet {
	public R52_Patch_3_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ReportGenerator/src", AbstractHttpTransporter.class, "REL_report_generator_3_26_3", USATRegistry.TRANSPORT_LAYER);
	}
	
	@Override
	public String getName() {
		return "R52 Patch 3";
	}
}
