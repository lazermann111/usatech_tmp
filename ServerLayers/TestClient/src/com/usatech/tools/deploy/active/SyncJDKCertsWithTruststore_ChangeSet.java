package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.util.Collections;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.SyncWithJKSTask;

public class SyncJDKCertsWithTruststore_ChangeSet implements ChangeSet {

	@Override
	public String getName() {
		return "Sync JDK Certs With Truststore";
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("NET") || host.isServerType("WEB") || host.isServerType("APP");
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		DeployTask task;
		if(host.isServerType("NET") || host.isServerType("WEB"))
			task = new SyncWithJKSTask("/usr/jdk/latest" + "/jre/lib/security/cacerts", "/opt/USAT/conf/truststore.ts", cache, 0640, "__jdk__");
		else if(host.isServerType("APP"))
			task = new SyncWithJKSTask("/usr/jdk/latest" + "/jre/lib/security/cacerts", "/opt/USAT/conf/truststore.ts", cache, 0640, "__jdk__", Collections.singleton("addtrustexternalca"));
		else
			throw new IOException("Not a valid host");
		return new DeployTask[] { task };
	}

	@Override
	public String toString() {
		return getName();
	}
}
