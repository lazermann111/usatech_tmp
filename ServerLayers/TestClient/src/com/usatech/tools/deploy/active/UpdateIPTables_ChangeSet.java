package com.usatech.tools.deploy.active;

import static com.usatech.tools.deploy.USATRegistry.APP_LAYER;
import static com.usatech.tools.deploy.USATRegistry.DMS_APP;
import static com.usatech.tools.deploy.USATRegistry.ENERGYMISERS_WEBSITE_APP;
import static com.usatech.tools.deploy.USATRegistry.ESUDS_APP;
import static com.usatech.tools.deploy.USATRegistry.HOTCHOICE_APP;
import static com.usatech.tools.deploy.USATRegistry.HTTPD_NET;
import static com.usatech.tools.deploy.USATRegistry.HTTPD_WEB;
import static com.usatech.tools.deploy.USATRegistry.INAUTH_LAYER;
import static com.usatech.tools.deploy.USATRegistry.KEYMGR;
import static com.usatech.tools.deploy.USATRegistry.LOADER;
import static com.usatech.tools.deploy.USATRegistry.NET_LAYER;
import static com.usatech.tools.deploy.USATRegistry.OUTAUTH_LAYER;
import static com.usatech.tools.deploy.USATRegistry.POSM_LAYER;
import static com.usatech.tools.deploy.USATRegistry.POSTGRES_KLS;
import static com.usatech.tools.deploy.USATRegistry.POSTGRES_MSR;
import static com.usatech.tools.deploy.USATRegistry.POSTGRES_MST;
import static com.usatech.tools.deploy.USATRegistry.POSTGRES_PGS;
import static com.usatech.tools.deploy.USATRegistry.RPTGEN_LAYER;
import static com.usatech.tools.deploy.USATRegistry.RPTREQ_LAYER;
import static com.usatech.tools.deploy.USATRegistry.TRANSPORT_LAYER;
import static com.usatech.tools.deploy.USATRegistry.USALIVE_APP;
import static com.usatech.tools.deploy.USATRegistry.USATECH_WEBSITE_APP;
import static com.usatech.tools.deploy.USATRegistry.VERIZON_APP;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.AbstractLinuxChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DedupIPTablesTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.DeployTaskInfo;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.tools.deploy.UsatIPTablesUpdateTask;

public class UpdateIPTables_ChangeSet extends AbstractLinuxChangeSet {
	public UpdateIPTables_ChangeSet() throws UnknownHostException {
		super();
	}

	@Override
	protected void registerApps() {
		registerApp(APP_LAYER);
		registerApp(LOADER);
		registerApp(INAUTH_LAYER);
		registerApp(POSM_LAYER);
		registerApp(NET_LAYER);
		registerApp(OUTAUTH_LAYER);
		registerApp(KEYMGR);

		registerApp(POSTGRES_MST);
		registerApp(POSTGRES_MSR);
		registerApp(POSTGRES_KLS);
		registerApp(POSTGRES_PGS);

		registerApp(USALIVE_APP);
		registerApp(DMS_APP);
		registerApp(ESUDS_APP);
		registerApp(USATECH_WEBSITE_APP);
		registerApp(ENERGYMISERS_WEBSITE_APP);
		registerApp(HOTCHOICE_APP);
		registerApp(VERIZON_APP);
		registerApp(USATRegistry.PREPAID_APP);
		registerApp(USATRegistry.GETMORE_APP);

		registerApp(HTTPD_NET);
		registerApp(HTTPD_WEB);

		registerApp(RPTGEN_LAYER);
		registerApp(TRANSPORT_LAYER);
		registerApp(RPTREQ_LAYER);
	}

	@Override
	public DeployTask[] getTasks(DeployTaskInfo deployTaskInfo) throws IOException {
		return new DeployTask[] {
				new DedupIPTablesTask(),
				new UsatIPTablesUpdateTask(registry.getServerSet(deployTaskInfo.host.getServerEnv()))
		};
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
	}
	
	@Override
	protected void addTasks(Host host, App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
	}

	@Override
	public String getName() {
		return "Update IPTables";
	}
}
