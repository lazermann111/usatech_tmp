package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R49_Patch_9_ChangeSet extends MultiLinuxPatchChangeSet {
	public R49_Patch_9_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ReportGenerator/src/rgr-data-layer.xml", "classes/", "REL_report_generator_3_23_9", true, USATRegistry.RPTREQ_LAYER);
	}
	
	@Override
	public String getName() {
		return "R49 Patch 9";
	}
}
