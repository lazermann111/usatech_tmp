package com.usatech.tools.deploy.active;

import com.usatech.report.MassDeviceUpdate;
import com.usatech.tools.deploy.*;

import java.net.UnknownHostException;

public class R51_Patch_12_ChangeSet extends MultiLinuxPatchChangeSet {
    public R51_Patch_12_ChangeSet() throws UnknownHostException {
        super();
        registerSource("ReportGenerator/src", MassDeviceUpdate.class, "REL_report_generator_3_25_12", USATRegistry.RPTGEN_LAYER);
    }

    @Override
    public String getName() {
        return "R51 Patch 12";
    }
}
