package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DefaultHostSpecificValue;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.PropertiesUpdateFileTask;
import com.usatech.tools.deploy.USATRegistry;

public class R51_Patch_8_ChangeSet extends MultiLinuxPatchChangeSet {
	public R51_Patch_8_ChangeSet() throws UnknownHostException {
		super();
		registerAppRestart(USATRegistry.INAUTH_LAYER);
	}

	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(app == USATRegistry.INAUTH_LAYER) {
			String appPropertiesFile = String.format("/opt/USAT/%s%s/specific/USAT_app_settings.properties",
					app.getName(),
					instance == null ? "" : instance);
			
			PropertiesUpdateFileTask puft = new PropertiesUpdateFileTask(true, 0640, app.getUserName(), app.getUserName());
			puft.setFilePath(appPropertiesFile);
			puft.registerValue("com.usatech.authoritylayer.TandemGatewayTask.authPartiallyReversed.whenLess", new DefaultHostSpecificValue("true"));
			tasks.add(puft);
		}
		
		super.addRestartAppCommands(host, app, instance, instanceNum, instanceCount, tasks, commands, cache);
	}	

	
	@Override
	public String getName() {
		return "R51 Patch 8";
	}
}
