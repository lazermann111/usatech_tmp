package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.posm.schedule.POSMRefundPropagatorTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R51_Patch_7_ChangeSet extends MultiLinuxPatchChangeSet {
	public R51_Patch_7_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/POSMLayer/conf/posm-data-layer.xml", "classes", "REL_posmlayer_1_28_7", false, USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/POSMLayer/src/main/", POSMRefundPropagatorTask.class, "REL_posmlayer_1_28_7", USATRegistry.POSM_LAYER);
		registerResource("usalive/web/include/rmaBottom.jsp", "web/include", "REL_USALive_1_27_7", false, USATRegistry.USALIVE_APP);
	}
	
	@Override
	public String getName() {
		return "R51 Patch 7";
	}
}
