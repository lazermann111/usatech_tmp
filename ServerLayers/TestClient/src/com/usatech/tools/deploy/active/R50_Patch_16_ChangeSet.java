package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.authoritylayer.AprivaAuthorityTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R50_Patch_16_ChangeSet extends MultiLinuxPatchChangeSet {
	public R50_Patch_16_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/AuthorityLayer/src", AprivaAuthorityTask.class, "REL_authoritylayer_1_36_16", USATRegistry.OUTAUTH_LAYER);
	}
	
	@Override
	public String getName() {
		return "R50 Patch 16";
	}

	@Override
	public String toString() {
		return getName();
	}
}
