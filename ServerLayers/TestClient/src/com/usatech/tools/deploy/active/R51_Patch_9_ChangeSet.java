package com.usatech.tools.deploy.active;

import com.usatech.report.build.AbstractBuildActivityFolio;
import com.usatech.tools.deploy.*;
import java.net.UnknownHostException;

public class R51_Patch_9_ChangeSet extends MultiLinuxPatchChangeSet {
    public R51_Patch_9_ChangeSet() throws UnknownHostException {
        super();
        registerSource("ReportGenerator/src", AbstractBuildActivityFolio.class, "REL_report_generator_3_25_9", USATRegistry.USALIVE_APP, USATRegistry.RPTGEN_LAYER, USATRegistry.RPTREQ_LAYER);
        registerResource("ReportGenerator/src/com/usatech/report/build/BuildReportPillars.properties", "classes/com/usatech/report/build", "REL_report_generator_3_25_9", true, USATRegistry.USALIVE_APP, USATRegistry.RPTGEN_LAYER, USATRegistry.RPTREQ_LAYER);
    }

    @Override
    public String getName() {
        return "R51 Patch 9";
    }
}
