package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.FromCvsUploadTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;

/**
 * @author yhe
 *
 */
public class PGS95_Patch_ChangeSet implements ChangeSet {
	

	@Override
	public String getName() {
		return "PGS 9.5 Patch Task";
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("PGS");
	}
	
	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		List<DeployTask> tasks =new ArrayList<DeployTask>();
		tasks.add(new FromCvsUploadTask("server_app_config/PGS/pgs_cal_replication_lag.sh", "HEAD", "/opt/USAT/postgres/bin/pgs_cal_replication_lag.sh", 0755,"postgres", "postgres", true));
		tasks.add(new FromCvsUploadTask("server_app_config/PGS/pgs_archive_cleanup.sh", "HEAD", "/opt/USAT/postgres/bin/pgs_archive_cleanup.sh", 0755,"postgres", "postgres", true));
		tasks.add(new FromCvsUploadTask("server_app_config/PGS/pgs_standby_basebackup.sh", "HEAD", "/opt/USAT/postgres/bin/pgs_standby_basebackup.sh", 0755,"postgres", "postgres", true));
		tasks.add(new FromCvsUploadTask("server_app_config/PGS/pgs_trigger_set_tablespace.sh", "HEAD", "/opt/USAT/postgres/bin/pgs_trigger_set_tablespace.sh", 0755,"postgres", "postgres", true));
		tasks.add(new FromCvsUploadTask("server_app_config/PGS/pgs_trigger_backup.sh", "HEAD", "/opt/USAT/postgres/bin/pgs_trigger_backup.sh", 0755,"postgres", "postgres", true));
		tasks.add(new FromCvsUploadTask("server_app_config/PGS/pgs_backup.sh", "HEAD", "/opt/USAT/postgres/bin/pgs_backup.sh", 0755,"postgres", "postgres", true));
		
		DeployTask[] allTask=new DeployTask[tasks.size()];
		return tasks.toArray(allTask);
	}

	@Override
	public String toString() {
		return getName();
	}
}
