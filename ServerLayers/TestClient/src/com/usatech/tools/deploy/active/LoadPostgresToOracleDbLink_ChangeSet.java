package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATHelper;

public class LoadPostgresToOracleDbLink_ChangeSet implements ChangeSet {

	@Override
	public String getName() {
		return "Load Postgres to Oracle DB Link";
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("PGS");
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		List<DeployTask> tasks = new ArrayList<DeployTask>();
		USATHelper.addOdbcLinkTasks(host, cache, tasks, "release");
		return tasks.toArray(new DeployTask[tasks.size()]);
	}

	@Override
	public String toString() {
		return getName();
	}
}
