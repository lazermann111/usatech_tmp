package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.PropertiesUpdateFileTask;
import com.usatech.tools.deploy.ServerEnvHostSpecificValue;
import com.usatech.tools.deploy.USATRegistry;

public class R54_Patch_15_ChangeSet extends MultiLinuxPatchChangeSet {
	public final static String LOGS_DEV  = "jdbc:postgresql://ecclog11:5432/devmain?ssl=true&connectTimeout=5&tcpKeepAlive=true";
	public final static String LOGS_INT  = "jdbc:postgresql://ecclog11:5432/intmain?ssl=true&connectTimeout=5&tcpKeepAlive=true";
	public final static String LOGS_ECC  = "jdbc:postgresql://ecclog11:5432/eccmain?ssl=true&connectTimeout=5&tcpKeepAlive=true";
	public final static String LOGS_PROD = "jdbc:postgresql://usalog31:5432/main?ssl=true&connectTimeout=5&tcpKeepAlive=true";
	
	public R54_Patch_15_ChangeSet() throws UnknownHostException {
		super();
		registerResource("DMS/resources/jsp/devices/profile/deviceCallInLog.jsp", "web/jsp/devices/profile", "REL_DMS_1_16_15", false, USATRegistry.DMS_APP);
		registerResource("DMS/resources/jsp/devices/profile/profile.jsp", "web/jsp/devices/profile", "REL_DMS_1_16_15", false, USATRegistry.DMS_APP);
		registerResource("DMS/resources/jsp/include/leftMenu.jsp", "web/jsp/include", "REL_DMS_1_16_15", false, USATRegistry.DMS_APP);
			}
	
	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(app == USATRegistry.DMS_APP) {
			PropertiesUpdateFileTask puft = new PropertiesUpdateFileTask(true, 0640, "root", "usat");
			puft.setFilePath("/opt/USAT/conf/USAT_environment_settings.properties");
			
			puft.registerValue("USAT.database.LOGS.url", new ServerEnvHostSpecificValue(
					LOGS_DEV, 
					LOGS_INT, 
					LOGS_ECC, 
					LOGS_PROD));
			
			tasks.add(puft);
		}
		restarts.add(app);
		super.addRestartAppCommands(host, app, instance, instanceNum, instanceCount, tasks, commands, cache);
	}
	
	@Override
	public String getName() {
		return "R54 Patch 15";
	}
}
