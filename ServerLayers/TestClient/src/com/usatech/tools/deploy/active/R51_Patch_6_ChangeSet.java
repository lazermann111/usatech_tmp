package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R51_Patch_6_ChangeSet extends MultiLinuxPatchChangeSet {
	public R51_Patch_6_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/AuthorityLayer/src/InsideAuthorityLayerService.properties", "classes", "REL_authoritylayer_1_37_6", true, USATRegistry.INAUTH_LAYER);
	}
	
	@Override
	public String getName() {
		return "R51 Patch 6";
	}
}
