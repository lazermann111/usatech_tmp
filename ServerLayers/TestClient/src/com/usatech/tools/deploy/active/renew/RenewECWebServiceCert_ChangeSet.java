package com.usatech.tools.deploy.active.renew;


import java.io.IOException;
import java.util.Date;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DefaultHostSpecificValue;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.RenewJKSCertTask;
import com.usatech.tools.deploy.USATRegistry;
/**
 * ec-dev.usatech.com
 * ec-int.usatech.com
 * ec-ecc.usatech.com
 * ec.usatech.com
 * are in /opt/USAT/conf/keystore.ks on NET servers
 * Use this task to for update the cert in /opt/USAT/conf/keystore.ks on NET server
 * For Production, use usanet31 to complete the csr, csr result import using this Task. For the rest usanet use usanet31 keystore as srckeystore:
 * /usr/jdk/latest/bin/keytool -importkeystore -destkeystore /opt/USAT/conf/keystore.ks -srckeystore ./keystore.ks -alias eportconnect
 * 
 * Once applied, need to import public key cert to APP and APR server truststore
 * APP server usalive use refund search verify card to test
 * APR server dms use consumer card search to test
 * To export and import:
 * 1. export public key:
 * /usr/jdk/latest/bin/keytool -keystore /opt/USAT/conf/keystore.ks -export -file eportconnect.crt  -alias eportconnect
 * 2. import to APP, APR truststore:
 * /usr/jdk/latest/bin/keytool -keystore /opt/USAT/conf/truststore.ts -importcert -alias ec.usatech.com -file eportconnect.crt
 * Restart APP, APR server once applied to pick up the cert.
 * @author yhe
 *
 */
public class RenewECWebServiceCert_ChangeSet implements ChangeSet {

	@Override
	public String getName() {
		return "Renew ec.usatech.com Certificate";
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("NET");
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		//Net Ops now get a cert from Comodo for Prod, ECC and Int  
		if ("USA".equalsIgnoreCase(host.getServerEnv()) || "ECC".equalsIgnoreCase(host.getServerEnv()) || "INT".equalsIgnoreCase(host.getServerEnv()))
			return new DeployTask[] {};
		String commonName = "ec" + ("USA".equalsIgnoreCase(host.getServerEnv()) ? ".usatech.com" : "-" + host.getServerEnv().toLowerCase() + ".usatech.com");
		RenewJKSCertTask ecCertTask = new RenewJKSCertTask(new Date(Long.MAX_VALUE), "/opt/USAT/conf/keystore.ks", cache, 0640, new DefaultHostSpecificValue("eportconnect"), new DefaultHostSpecificValue(commonName), "usasubca.usatech.com", "USA".equalsIgnoreCase(host.getServerEnv()) ? null : "USATWebServer", System.getProperty("user.name"), new USATRegistry().getServerSet(host.getServerEnv())
				.getHosts("NET"));
		return new DeployTask[] { ecCertTask };
	}

	@Override
	public String toString() {
		return getName();
	}
}
