package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.dms.consumer.CardConfigWizard2Step;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R53_Patch_4_ChangeSet extends MultiLinuxPatchChangeSet {
	public R53_Patch_4_ChangeSet() throws UnknownHostException {
		super();
		registerSource("DMS/src", CardConfigWizard2Step.class, "REL_DMS_1_15_4", USATRegistry.DMS_APP);
	}
	
	@Override
	public String getName() {
		return "R53 Patch 4";
	}
}
