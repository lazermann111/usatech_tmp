package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.applayer.AuthorizeTask;
import com.usatech.keymanager.service.KeyManagerEngine;
import com.usatech.layers.common.MessageProcessingUtils;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.networklayer.app.AbstractOutboundTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R53_Patch_17_ChangeSet extends MultiLinuxPatchChangeSet {
	public R53_Patch_17_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/LayersCommon/src", AuthorityAttrEnum.class, "REL_applayer_1_39_17",
				USATRegistry.APP_LAYER);
		registerSource("ServerLayers/AppLayer/src", AuthorizeTask.class, "REL_applayer_1_39_17",
				USATRegistry.APP_LAYER);
		registerSource("ServerLayers/LayersCommon/src", MessageProcessingUtils.class, "REL_applayer_1_39_17",
				USATRegistry.APP_LAYER);
		registerSource("ServerLayers/LayersCommon/src", CardReaderType.class, "REL_applayer_1_39_17",
				USATRegistry.APP_LAYER);
		registerSource("ServerLayers/LayersCommon/src", EntryType.class, "REL_applayer_1_39_17",
				USATRegistry.APP_LAYER);
		registerSource("ServerLayers/LayersCommon/src", CardReaderType.class, "REL_applayer_1_39_17",
				USATRegistry.NET_LAYER);
		registerSource("ServerLayers/LayersCommon/src", EntryType.class, "REL_applayer_1_39_17",
				USATRegistry.NET_LAYER);
		registerSource("ServerLayers/LayersCommon/src", CardReaderType.class, "REL_applayer_1_39_17",
				USATRegistry.KEYMGR);
		registerSource("KeyManagerService/src", KeyManagerEngine.class, "REL_keymgr_2_20_17", USATRegistry.KEYMGR);
		registerResource("KeyManagerService/src/KeyManagerService.properties", "classes", "REL_keymgr_2_20_17", true,
				USATRegistry.KEYMGR);
		registerSource("ServerLayers/NetLayer/src", AbstractOutboundTask.class, "REL_netlayer_1_39_17",
				USATRegistry.NET_LAYER);

	}

	@Override
	public String getName() {
		return "R53 Patch 17";
	}
}
