package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.posm.schedule.dfr.AbstractDFRParser;
import com.usatech.posm.schedule.dfr.ParseFinancialDFRTask;
import com.usatech.posm.schedule.dfr.ProcessFinancialDFRJob;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R56_Patch_22_ChangeSet extends MultiLinuxPatchChangeSet {
	public R56_Patch_22_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/POSMLayer/conf/posm-data-layer.xml", "classes", "REL_R56_22", false, USATRegistry.POSM_LAYER);
		registerResource("ServerLayers/POSMLayer/conf/POSMLayerService.properties", "classes", "REL_R56_22", true, USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/POSMLayer/src/main", AbstractDFRParser.class, "REL_R56_22", USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/POSMLayer/src/main", ParseFinancialDFRTask.class, "REL_R56_22", USATRegistry.POSM_LAYER);
		registerSource("ServerLayers/POSMLayer/src/main", ProcessFinancialDFRJob.class, "REL_R56_22", USATRegistry.POSM_LAYER);
	}
	
	@Override
	public String getName() {
		return "R56 Patch 22 - DFR Processing Retries";
	}
}
