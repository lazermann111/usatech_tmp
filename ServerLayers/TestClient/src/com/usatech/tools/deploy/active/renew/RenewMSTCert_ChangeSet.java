package com.usatech.tools.deploy.active.renew;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.RenewFileCertTask;
import com.usatech.tools.deploy.Server;
import com.usatech.tools.deploy.USATRegistry;
/**
 * NOTE: dev environment is the same filesystem setup as production. INT and ECC, each server has two instance running. 
 * This class does the following step:
 * 1. backup related files to "/home/"+runUseName+"/mstCertBackup/
 * 2. Renew cert in /opt/USAT/postgres
 * 
 * Verify:
 * 1. sudo cat /opt/USAT/postgres/data/server.crt| openssl x509 -noout -enddate
 * 2. restart mst postgres
 * 3. check mst postgres log
 * 4. check app log
 * 5. test a auth and sale
 * 
 * Cert is valid for 3 years
 * 
 * @author yhe
 *
 */
public class RenewMSTCert_ChangeSet implements ChangeSet {

	@Override
	public String getName() {
		return "Renew MST/MSR Certificate";
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("MST")||host.isServerType("MSR");
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		String username="postgres";
		String runUseName=System.getProperty("user.name");
		String backupFolderPath="/home/"+runUseName+"/mstCertBackup/";
		
		List<DeployTask> tasks = new ArrayList<DeployTask>();
		USATRegistry registry = new USATRegistry();
		String serverType = host.getServerTypes().iterator().next();
		Server server = registry.getServerSet(host.getServerEnv()).getServer(host, serverType);
		if(host.getServerEnv().toUpperCase().equals("LOCAL")){
			String pgDir="/opt/USAT/postgres";
			tasks.add(new RenewFileCertTask(new Date(Long.MAX_VALUE), pgDir + "/data/server.crt", pgDir + "/data/root.crt", pgDir + "/data/server.key", 0644, 0600, username, username));
		}
		else if(server.instances == null || server.instances.length <= 1){
			String pgDir="/opt/USAT/postgres";
			//prod setup 
			ExecuteTask backupTask=new ExecuteTask("if [ ! -d "+backupFolderPath+" ]; then mkdir "+backupFolderPath+"; fi",
					"sudo /bin/cp /opt/USAT/postgres/data/server.crt "+backupFolderPath+"server.crt.`date +%Y%m%d-%H%M%S`",
					"sudo /bin/cp /opt/USAT/postgres/data/root.crt "+backupFolderPath+"root.crt.`date +%Y%m%d-%H%M%S`",
					"sudo /bin/cp /opt/USAT/postgres/data/server.key "+backupFolderPath+"server.key.`date +%Y%m%d-%H%M%S`"
					);
			tasks.add(backupTask);
			tasks.add(new RenewFileCertTask(new Date(Long.MAX_VALUE), pgDir + "/data/server.crt", pgDir + "/data/root.crt", pgDir + "/data/server.key", 0644, 0600, username, username));
		}
		else{
			for(int i = 0; i < server.instances.length; i++){
				String pgDir="/opt/USAT/postgres" + server.instances[i];
				tasks.add(new RenewFileCertTask(new Date(Long.MAX_VALUE), pgDir + "/data/server.crt", pgDir + "/data/root.crt", pgDir + "/data/server.key", 0644, 0600, username, username));
			}
		}
		return tasks.toArray(new DeployTask[tasks.size()]);
	}

	@Override
	public String toString() {
		return getName();
	}
}
