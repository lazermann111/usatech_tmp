package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class Patch_R43M_ChangeSet extends MultiLinuxPatchChangeSet {
	public Patch_R43M_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/AuthorityLayer/src/authority-data-layer.xml", "classes", "BRN_R43", false,USATRegistry.INAUTH_LAYER);
		registerResource("Prepaid/web/create_user.html.jsp", "web/", "BRN_R43", false,USATRegistry. PREPAID_APP);

	}

	@Override
	public String getName() {
		return "Edge Server - R43M Patch";
	}
}

