package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.applayer.AuthorizeTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R58_Update_3_ChangeSet extends MultiLinuxPatchChangeSet {
	public R58_Update_3_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/AppLayer/src", AuthorizeTask.class, "REL_R58_3", USATRegistry.APP_LAYER);
	}
	
	@Override
	public String getName() {
		return "R58 Update 3 - Apple Pay Cash payments not always recognized and flagged";
	}
}
