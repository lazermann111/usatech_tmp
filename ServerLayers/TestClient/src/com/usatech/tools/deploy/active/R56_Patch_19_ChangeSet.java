package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.report.BasicFrequency;
import com.usatech.report.FrequencyManager;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R56_Patch_19_ChangeSet extends MultiLinuxPatchChangeSet {
	public R56_Patch_19_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ReportGenerator/src/rgr-data-layer.xml", "classes", "REL_R56_19", false, USATRegistry.RPTREQ_LAYER);
		registerResource("ReportGenerator/src/ReportRequesterService.properties", "classes", "REL_R56_19", true, USATRegistry.RPTREQ_LAYER);
		registerSource("ReportGenerator/src", BasicFrequency.class, "REL_R56_19", USATRegistry.RPTGEN_LAYER, USATRegistry.RPTREQ_LAYER);
		registerSource("ReportGenerator/src", FrequencyManager.class, "REL_R56_19", USATRegistry.RPTGEN_LAYER, USATRegistry.RPTREQ_LAYER);
	}
	
	@Override
	public String getName() {
		return "R56 Patch 19 - Seed Data Feed";
	}
}
