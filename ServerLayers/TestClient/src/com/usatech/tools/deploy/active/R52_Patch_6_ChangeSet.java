package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.ccs.CampaignBlastTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R52_Patch_6_ChangeSet extends MultiLinuxPatchChangeSet {
	public R52_Patch_6_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ReportGenerator/src", CampaignBlastTask.class, "REL_report_generator_3_26_6", USATRegistry.RPTGEN_LAYER);
		registerResource("usalive/web/promo_email.html.jsp", "web/", "REL_USALive_1_28_6", false, USATRegistry.USALIVE_APP);
	}
	
	@Override
	public String getName() {
		return "R52 Patch 6";
	}
}
