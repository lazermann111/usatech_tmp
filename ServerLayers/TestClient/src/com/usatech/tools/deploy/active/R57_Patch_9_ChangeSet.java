package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.prepaid.pass.PassServletUtils;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R57_Patch_9_ChangeSet extends MultiLinuxPatchChangeSet {
	public R57_Patch_9_ChangeSet() throws UnknownHostException {
		super();
		registerSource("Prepaid/src", PassServletUtils.class, "REL_R57_9", USATRegistry.PREPAID_APP);
	}
	
	@Override
	public String getName() {
		return "R57 Patch 9 - No VAS Passes by Default";
	}
}
