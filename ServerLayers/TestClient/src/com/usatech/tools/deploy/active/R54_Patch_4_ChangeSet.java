package com.usatech.tools.deploy.active;

import com.usatech.dms.util.DMSPaginationStep;
import com.usatech.layers.common.util.WebHelper;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;
import simple.db.helpers.OracleHelper;
import java.net.UnknownHostException;

public class R54_Patch_4_ChangeSet extends MultiLinuxPatchChangeSet {
	public R54_Patch_4_ChangeSet() throws UnknownHostException {
		super();
		registerSource("DMS/src",
				DMSPaginationStep.class,
				"REL_DMS_1_16_4",
				USATRegistry.DMS_APP);

		registerResource("DMS/resources/jsp/file/fileDetailsFunc.jsp",
						 "web/jsp/file",
						 "REL_DMS_1_16_4",
						 false,
						 USATRegistry.DMS_APP);

		registerSource("ServerLayers/LayersCommon/src",
				WebHelper.class,
				"REL_DMS_1_16_4",
				USATRegistry.DMS_APP);

		registerSource("Simple1.5/src",
				OracleHelper.class,
				"REL_DMS_1_16_4",
				USATRegistry.DMS_APP);
	}

	@Override
	public String getName() {
		return "R54 Patch 4";
	}
}
