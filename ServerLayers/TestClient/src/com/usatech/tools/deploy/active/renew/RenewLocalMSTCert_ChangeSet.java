package com.usatech.tools.deploy.active.renew;


import java.io.IOException;
import java.util.Date;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.RenewFileCertTask;

public class RenewLocalMSTCert_ChangeSet implements ChangeSet {

	@Override
	public String getName() {
		return "Renew local mst Certificate";
	}

	@Override
	public boolean isApplicable(Host host) {
		return true;
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		String pgDir="/opt/USAT/postgres";
		String username="postgres";
		RenewFileCertTask rfct=new RenewFileCertTask(new Date(System.currentTimeMillis() + 180 * 24 * 60 * 60 * 1000L), pgDir + "/data/server.crt", pgDir + "/data/root.crt", pgDir + "/data/server.key", 0644, 0600, username, username);
		return new DeployTask[] { rfct };
	}

	@Override
	public String toString() {
		return getName();
	}
}
