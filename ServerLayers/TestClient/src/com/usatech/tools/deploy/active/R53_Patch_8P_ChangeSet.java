package com.usatech.tools.deploy.active;

import static com.usatech.tools.deploy.USATRegistry.APP_LAYER;

import java.io.File;
import java.net.UnknownHostException;

import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.Host;

public class R53_Patch_8P_ChangeSet extends AbstractLinuxPatchChangeSet {
	protected String baseDir;

	public R53_Patch_8P_ChangeSet() throws UnknownHostException {
		super();
		File simpleBinDir = new File("C:/Development/Workspace/Simple1.5/bin");
		//registerClass(simpleBinDir, DirectPeerProducer.class);
	}

	protected void registerApps() {
		registerApp(APP_LAYER);
	}

	@Override
	public String getName() {
		return "Edge Server - Paul Test Patch";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}
}
