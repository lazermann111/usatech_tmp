package com.usatech.tools.deploy.active;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import simple.text.StringUtils;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.UnixUploadTask;
import com.usatech.tools.deploy.UploadTask;


public class PHPUpgrade_ChangeSet implements ChangeSet {
	
	//public static final File baseDir = new File(System.getProperty("rootSourceDir", "D:\\Development\\Java Projects"));
	public static final File baseDir = new File(System.getProperty("rootSourceDir", "D:\\public\\project2011"));
	protected static final File serverAppConfigDir = new File(baseDir, "server_app_config");
	@Override
	public String getName() {
		return "PHP 5.5.19 Install";
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("WEB") || host.isServerType("MON");
	}
	
	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		String appDir="/opt/USAT/httpd";
		String appUserName="bin";
		Set<String> phpConfigOptions = new LinkedHashSet<String>(Arrays.asList(new String[] {
				"--disable-all",
				"--prefix=/usr/local/php",
				"--with-apxs2=/usr/local/apache2/bin/apxs",
				"--with-config-file-path=/usr/local/apache2/conf",
				"--with-zlib",
				"--with-openssl",
				"--without-pear",
				"--disable-cli",
				"--enable-session"
		}));
		if(host.isServerType("MON")) {
			phpConfigOptions.add("--enable-pdo");
			phpConfigOptions.add("--with-pdo-pgsql=/usr/pgsql-latest/");
		}
		return new DeployTask[] {
				new UploadTask(new File(serverAppConfigDir, "WEB/Httpd/source/php-5.5.19/php-5.5.19.tar.gz"), 0644),
				new ExecuteTask("sudo su",
						"/bin/rm -rf /usr/local/php",
						"cd /home/`logname`",
						"gunzip -c php-5.5.19.tar.gz | tar -xv --no-same-owner",
						"cd php-5.5.19",
						"./configure "+StringUtils.join(phpConfigOptions, " "),
						"make",
						"make install"),
						new UnixUploadTask(new File(serverAppConfigDir, "WEB/Httpd/conf/php.ini"), appDir + "/conf", 0644, appUserName, appUserName, true),
						new UnixUploadTask(new File(serverAppConfigDir, "WEB/Httpd/conf.d/php.conf"), appDir + "/conf.d", 0644, appUserName, appUserName, true)
		};
		
	}

	@Override
	public String toString() {
		return getName();
	}
}
