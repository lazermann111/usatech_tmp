package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.applayer.MessageProcessor_0219;
import com.usatech.layers.common.LegacyUpdateStatusProcessor;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R54_Patch_3_ChangeSet extends MultiLinuxPatchChangeSet {
	public R54_Patch_3_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/LayersCommon/src", LegacyUpdateStatusProcessor.class, "REL_applayer_1_40_3", USATRegistry.APP_LAYER);
		registerSource("ServerLayers/AppLayer/src", MessageProcessor_0219.class, "REL_applayer_1_40_3", USATRegistry.APP_LAYER);
	}
	
	@Override
	public String getName() {
		return "R54 Patch 3";
	}
}
