package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.authoritylayer.TandemGatewayTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R51_Patch_11_ChangeSet extends MultiLinuxPatchChangeSet {
	public R51_Patch_11_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/AuthorityLayer/src/", TandemGatewayTask.class, "REL_authoritylayer_1_37_11", USATRegistry.INAUTH_LAYER);
		registerResource("ServerLayers/AppLayer/src/loader-data-layer.xml", "classes", "REL_applayer_1_37_11", false, USATRegistry.LOADER);
	}

	@Override
	public String getName() {
		return "R51 Patch 11";
	}
}
