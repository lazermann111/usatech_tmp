package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R58_Update_15_ChangeSet extends MultiLinuxPatchChangeSet {
	public R58_Update_15_ChangeSet() throws UnknownHostException {
		super();
		registerResource("usalive/web/process_request_history.html.jsp", "web", "REL_R58_15", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/process_request_history_refresh.html.jsp", "web", "REL_R58_15", false, USATRegistry.USALIVE_APP);
	}
	
	@Override
	public String getName() {
		return "R58 Update 15 - Mass Device Updatel";
	}
}
