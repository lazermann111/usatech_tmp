package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.report.ReportRequestFactory;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R54_Patch_5_ChangeSet extends MultiLinuxPatchChangeSet {
	public R54_Patch_5_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ReportGenerator/src/", ReportRequestFactory.class, "REL_report_generator_3_28_5", USATRegistry.RPTGEN_LAYER, USATRegistry.USALIVE_APP);
	}
	
	@Override
	public String getName() {
		return "R54 Patch 5";
	}
}
