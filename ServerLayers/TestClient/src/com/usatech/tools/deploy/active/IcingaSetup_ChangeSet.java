package com.usatech.tools.deploy.active;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import simple.io.EncodingInputStream;
import simple.io.ReplacementsLineFilteringReader;
import simple.text.StringUtils;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.DeployTaskInfo;
import com.usatech.tools.deploy.DeployUtils;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.FromCvsUploadTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.IPTablesUpdateTask;
import com.usatech.tools.deploy.OptionalTask;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.PostgresSetupTask;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.tools.deploy.UploadTask;

public class IcingaSetup_ChangeSet implements ChangeSet {
	protected final String coreVersion = "1.8.2";
	protected final String pluginVersion = "1.4.16";
	protected final String webVersion = "1.8.1";
	protected final String nconfVersion = "1.3.0-0";
	protected final String pnpVersion = "0.6.19";
	@Override
	public String getName() {
		return "Icinga " + coreVersion + " with plugins " + pluginVersion + " Install";
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("MST");
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		List<DeployTask> tasks = new ArrayList<DeployTask>();
		List<String> commands = new ArrayList<String>();
		commands.add("sudo su");
		// commands.add("rpm -ivh http://pkgs.repoforge.org/rpmforge-release/rpmforge-release-0.5.2-2.el6.rf.x86_64.rpm");

		commands.add("yum -y install httpd php php-cli php-pear php-xmlrpc php-xsl php-pdo php-gd php-ldap php-pgsql | grep -v '#'");
		commands.add("yum -y install gcc glibc glibc-common | grep -v '#'");
		commands.add("yum -y install gd gd-devel | grep -v '#'");
		commands.add("yum -y install libjpeg libjpeg-devel libpng libpng-devel");
		commands.add("yum -y install libdbi libdbi-devel libdbi-drivers libdbi-dbd-pgsql rrdtool rrdtool-perl");
		commands.add("yum -y update openssl");
		commands.add("grep -c 'monitor:' /etc/group || groupadd -g 1001 monitor");
		commands.add("grep -c 'moncmd:' /etc/group || groupadd -g 1000 moncmd");
		commands.add("if [ `egrep -c '^monitor:' /etc/passwd` -eq 0 ]; then useradd -c 'Monitor' -g 1000 -G moncmd -m -u 1001 monitor; fi");
		commands.add("if [ `egrep -c '^moncmd:x:1000:(\\w\\w*,)*apache' /etc/group` -eq 0 ]; then usermod -G moncmd apache; fi");

		tasks.add(new ExecuteTask(commands.toArray(new String[commands.size()])));
		PostgresSetupTask pst = new PostgresSetupTask(USATRegistry.POSTGRES_PGS, null, 5432);
		pst.registerUser("icinga");
		pst.registerDatabase("icinga", "icinga_data", "icinga");
		pst.registerUser("icinga_web");
		pst.registerDatabase("icinga_web", "icinga_web_data", "icinga_web");
		tasks.add(pst);

		// pnp4nagios  icinga-web-module-pnp
		commands.clear();
		commands.add("sudo su");
		commands.add("tar xzf icinga-" + coreVersion + ".tar.gz");
		commands.add("cd icinga-" + coreVersion);
		commands.add("./configure --enable-embedded-perl --with-perlcache --with-icinga-user=monitor --with-icinga-group=monitor --with-command-user=monitor --enable-ssl --enable-idoutils --with-command-group=moncmd --sysconfdir=/opt/USAT/icinga/conf --datadir=/opt/USAT/icinga/web --prefix=/opt/USAT/icinga");
		// --with-httpd-conf
		commands.add("make all");
		commands.add("make install");
		commands.add("make install-init");
		commands.add("make install-config");
		commands.add("make install-eventhandlers");
		commands.add("make install-commandmode");
		commands.add("make install-idoutils ");
		commands.add("su - postgres -c '/usr/pgsql-latest/bin/psql -d icinga' < ./module/idoutils/db/pgsql/pgsql.sql");		
		//commands.add("sed -i 's/process_performance_data=0/process_performance_data=1/g\ns/^external_command_buffer_slots=4096/#external_command_buffer_slots=4096/g\ns/#external_command_buffer_slots=32768/external_command_buffer_slots=32768/g' /opt/USAT/icinga/conf/icinga.cfg; fi");
		commands.add("/bin/mv icinga.cfg /opt/USAT/icinga/conf");
		commands.add("mkdir /opt/USAT/icinga/conf/appmonitor; chown monitor:monitor /opt/USAT/icinga/conf/appmonitor; chmod 0755 /opt/USAT/icinga/conf/appmonitor");
		commands.add("/bin/mv app-monitor-templates.cfg /opt/USAT/icinga/conf/appmonitor");
		
		commands.add("if [ ! -s /opt/USAT/icinga/conf/ido2db.cfg ]; then sed 's/db_servertype=mysql/db_servertype=pgsql/g\ns/db_port=3306/db_port=5432/g\ns/db_pass=icinga/db_pass=ICINGA/g' /opt/USAT/icinga/conf/ido2db.cfg-sample > /opt/USAT/icinga/conf/ido2db.cfg; fi");
		commands.add("if [ ! -s /opt/USAT/icinga/conf/idomod.cfg ]; then sed '' /opt/USAT/icinga/conf/idomod.cfg-sample > /opt/USAT/icinga/conf/idomod.cfg; fi");
		// commands.add("if [ ! -s /opt/USAT/icinga/conf/modules/idoutils.cfg ]; then cp /opt/USAT/icinga/conf/modules/idoutils.cfg-sample /opt/USAT/icinga/conf/modules/idoutils.cfg; fi");
		commands.add("chcon -R -t httpd_sys_script_exec_t /opt/USAT/icinga/sbin/");
		commands.add("chcon -R -t httpd_sys_content_t /opt/USAT/icinga/share/");
		commands.add("chcon -R -t httpd_sys_script_rw_t /opt/USAT/icinga/var/rw/");

		// NOT sure if we need the following
		/*
		commands.add("make install-webconf");
		commands.add("mkdir /opt/USAT/icinga/bin/");
		commands.add("/bin/cp daemon-init /opt/USAT/icinga/bin/icinga.sh");
		commands.add("chown -R monitor:monitor /opt/USAT/icinga/bin/");
		commands.add("chmod u+x /opt/USAT/icinga/bin/icinga.sh");
		// TODO: Update /opt/USAT/conf/objects/contacts.cfg (replace email address)
		// commands.add("htpasswd -c /opt/USAT/icinga/conf/htpasswd.users monitoradmin");
		// TODO: update /etc/httpd/conf.d/nagios.conf (add "SSLRequireSSL")
		// commands.add("service httpd restart"); // */
		long time = System.currentTimeMillis();
		String mirror = "iweb";
		tasks.add(new OptionalTask("sudo /opt/USAT/icinga/bin/icinga --help", Pattern.compile(".*^Icinga " + coreVersion.replaceAll("([^\\w\\s])", "\\\\$1") + "$.*", Pattern.DOTALL | Pattern.MULTILINE), true,
				new UploadTask(new URL("http://downloads.sourceforge.net/project/icinga/icinga/" + coreVersion + "/icinga-" + coreVersion + ".tar.gz?r=&ts=" + time + "&use_mirror=" + mirror),	null, 0644, "root", "root", false),
				new FromCvsUploadTask("server_app_config/MON/icinga.cfg", "HEAD", "icinga.cfg", 0644, "monitor", "monitor", true, true),
				new FromCvsUploadTask("server_app_config/MON/app-monitor-template.cfg", "HEAD", "app-monitor-template.cfg", 0644, "monitor", "monitor", true, true),
				new ExecuteTask(commands.toArray(new String[commands.size()]))));

		commands.clear();
		commands.add("sudo su");
		commands.add("tar xzf nagios-plugins-" + pluginVersion + ".tar.gz");
		commands.add("cd nagios-plugins-" + pluginVersion);
		commands.add("./configure --with-pgsql=/usr/pgsql-latest --without-world-permissions --with-cgiurl=/icinga/cgi-bin --with-nagios-user=monitor --with-nagios-group=monitor --sysconfdir=/opt/USAT/icinga/conf --datadir=/opt/USAT/icinga/web --prefix=/opt/USAT/icinga");
		commands.add("make");
		commands.add("make install");

		tasks.add(new OptionalTask("if [ `sudo du -s /opt/USAT/icinga/libexec/ | cut -f1` -gt 0 ]; then echo 'Y'; else echo 'N'; fi", Pattern.compile("^Y$"), true,
				new UploadTask(new URL("http://downloads.sourceforge.net/project/nagiosplug/nagiosplug/" + pluginVersion + "/nagios-plugins-" + pluginVersion + ".tar.gz?r=&ts=" + time + "&use_mirror=" + mirror), null, 0644, "root", "root", false),
				new ExecuteTask(commands.toArray(new String[commands.size()]))));

		commands.clear();
		commands.add("sudo su");
		commands.add("tar xzf icinga-web-" + webVersion + ".tar.gz");
		commands.add("cd icinga-web-" + webVersion);
		commands.add("./configure --with-db-type=pgsql --with-db-port=5432 --with-api-subtype=pgsql --with-api-port=5432 --sysconfdir=/opt/USAT/icinga-web/conf --datadir=/opt/USAT/icinga-web/web --prefix=/opt/USAT/icinga-web");
		commands.add("make install");
		commands.add("make install-apache-config");
		commands.add("make install-done");
		commands.add("/opt/USAT/icinga-web/bin/clearcache.sh");
		commands.add("chcon -R -t httpd_sys_script_rw_t /opt/USAT/icinga-web/app/cache/");
		commands.add("chcon -R -t httpd_sys_script_rw_t /opt/USAT/icinga-web/log/");
		commands.add("setsebool -P httpd_can_network_connect 1");
		commands.add("sed -i 's#pgsql://icinga_web:icinga_web@localhost:5432/icinga_web#pgsql://icinga_web:ICINGA_WEB@127.0.0.1:5432/icinga_web#g\ns#pgsql://icinga:icinga@localhost:5432/icinga#pgsql://icinga:ICINGA@127.0.0.1:5432/icinga#g' /opt/USAT/icinga-web/app/config/databases.xml");
		commands.add("echo 'GRANT ALL ON ALL TABLES IN SCHEMA public TO icinga;' >> /opt/USAT/icinga-web/etc/schema/pgsql.sql");
		commands.add("su - postgres -c '/usr/pgsql-latest/bin/psql -d icinga_web' < /opt/USAT/icinga-web/etc/schema/pgsql.sql");
		commands.add("service httpd restart");
		
		tasks.add(new OptionalTask("if [ -x /opt/USAT/icinga-web/bin/console.php ]; then echo 'Y'; else echo 'N'; fi", Pattern.compile("^Y$"), true,
				new UploadTask(new URL("http://downloads.sourceforge.net/project/icinga/icinga-web/" + webVersion + "/icinga-web-" + webVersion + ".tar.gz?r=&ts=" + time + "&use_mirror=" + mirror), null, 0644, "root", "root", false),
				new ExecuteTask(commands.toArray(new String[commands.size()]))));
		/*
		commands.clear();
		commands.add("sudo su");
		commands.add("tar xzf nconf-" + nconfVersion + ".tz");
		tasks.add(new OptionalTask("if [ -x /usr/nconf-" + nconfVersion + "/bin/console.php ]; then echo 'Y'; else echo 'N'; fi", Pattern.compile("^Y$"), true, 
				new UploadTask(new URL("http://downloads.sourceforge.net/project/nconf/nconf/" + nconfVersion + "/nconf-" + nconfVersion + ".tgz?r=&ts=" + time + "&use_mirror=" + mirror), null, 0644, "root", "root", false),
				new ExecuteTask(commands.toArray(new String[commands.size()]))));
		*/
		commands.clear();
		commands.add("sudo su");
		commands.add("tar xzf pnp4nagios-" + pnpVersion + ".tar.gz");
		commands.add("cd pnp4nagios-" + pnpVersion);
		commands.add("./configure --with-nagios-user=monitor --with-nagios-group=monitor --sysconfdir=/opt/USAT/icinga/conf --prefix=/opt/USAT/pnp4nagios");
		commands.add("make all");
		commands.add("make fullinstall");
		commands.add("if [ ! -s /opt/USAT/icinga/conf/modules/pnp4nagios.cfg ]; then echo 'define module{\n    module_name npcd_mod\n    path /opt/USAT/pnp4nagios/lib/npcdmod.o\n    module_type neb\n    args config_file=/opt/USAT/pnp4nagios/etc/npcd.cfg\n}' > /opt/USAT/icinga/conf/modules/pnp4nagios.cfg; fi");
		commands.add("/bin/mv /opt/USAT/pnp4nagios/share/install.php /opt/USAT/pnp4nagios/share/install.php.ORIG");
		// TODO: we need to include this (npcd) in monit or init
		commands.add("/opt/USAT/pnp4nagios/bin/npcd -d -f /opt/USAT/pnp4nagios/etc/npcd.cfg");
		// TODO: add /opt/USAT/icinga/conf/appmonitor/app-monitor-templates.cfg
		commands.add("sed -i 's#$conf['nagios_base'] = \"/nagios/cgi-bin\";#$conf['nagios_base'] = \"/icinga/cgi-bin\";#g' /usr/pnp4nagios-0.6.19/etc/config_local.php");
		commands.add("service httpd restart");
		tasks.add(new OptionalTask("if [ -x /opt/USAT/pnp4nagios/bin/npcd ]; then echo 'Y'; else echo 'N'; fi", Pattern.compile("^Y$"), true,
				new UploadTask(new URL("http://downloads.sourceforge.net/project/pnp4nagios/PNP-" + StringUtils.substringBeforeLast(pnpVersion, ".".toCharArray()) + "/pnp4nagios-" + pnpVersion + ".tar.gz?r=&ts=" + time + "&use_mirror=" + mirror), null, 0644, "root", "root", false), 
				new ExecuteTask(commands.toArray(new String[commands.size()]))));

		//http://www.nsca-ng.org/download/nsca-ng-1.0.tar.gz
		// TODO: add http://bucardo.org/downloads/check_postgres.tar.gz
		commands.clear();
		commands.add("sudo su");
		// commands.add("tar xzf confuse-2.7.tar.gz");
		// commands.add("cd confuse-2.7");
		// commands.add("./configure --prefix /opt/USAT/nsca-ng");
		// commands.add("make all");
		// commands.add("make install");

		commands.add("cd ..");
		commands.add("tar xzf nsca-ng-1.1.tar.gz");
		commands.add("cd nsca-ng-1.1");
		commands.add("./build-aux/make-confuse");
		commands.add("./configure --enable-server --disable-client --prefix /opt/USAT/nsca-ng");
		commands.add("make all");
		commands.add("make install");
		commands.add("/bin/mv nsca-ng.cfg /opt/USAT/nsca-ng/etc/nsca-ng.cfg");
		// commands.add("user = \"nagios\"");
		final Map<Pattern, String> replacements = new HashMap<Pattern, String>();

		tasks.add(new OptionalTask("if [ -x /opt/USAT/nsca-ng/sbin/nsca-ng ]; then echo 'Y'; else echo 'N'; fi", Pattern.compile("^Y$"), true,
				new UploadTask(new URL("http://www.nsca-ng.org/download/nsca-ng-1.1.tar.gz"), null, 0644, "root", "root", false), 
				//new UploadTask(new URL("http://savannah.nongnu.org/download/confuse/confuse-2.7.tar.gz"), null, 0644, "root", "root", false), 
				new FromCvsUploadTask("server_app_config/MON/nsca-ng.cfg", "HEAD", "nsca-ng.cfg", 0600, "monitor", "monitor", true, true) {
					@Override
					public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
						char[] password = DeployUtils.getPassword(deployTaskInfo, "nsca-ng.root", "NSCA-NG Super User");
						replacements.put(Pattern.compile("<PASSWORD>"), new String(password));
						return super.perform(deployTaskInfo);
					}

					@Override
					protected InputStream decorate(InputStream in) {
						return new EncodingInputStream(new ReplacementsLineFilteringReader(new BufferedReader(new InputStreamReader(in)), "\n", replacements));
					}
				},
				new ExecuteTask(commands.toArray(new String[commands.size()]))));

		commands.clear();
		commands.add("sudo su");
		commands.add("service ido2db start");
		commands.add("service icinga start");
		tasks.add(new ExecuteTask(commands.toArray(new String[commands.size()])));
		
		// TODO: create nagios program in monit
		IPTablesUpdateTask iptTask = new IPTablesUpdateTask();
		iptTask.allowTcpIn("10.0.0.110", 80);
		iptTask.allowTcpIn("10.0.0.110", 443);
		tasks.add(iptTask);
		return tasks.toArray(new DeployTask[tasks.size()]);
	}

	@Override
	public String toString() {
		return getName();
	}
}
