package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R51_Patch_16_ChangeSet extends MultiLinuxPatchChangeSet {
	public R51_Patch_16_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ReportGenerator/src/rgr-data-layer.xml", "classes/", "REL_report_generator_3_25_16", false, USATRegistry.RPTREQ_LAYER);
		registerResource("ReportGenerator/src/rgr-data-layer.xml", "classes/", "REL_report_generator_3_25_16", false, USATRegistry.RPTGEN_LAYER);
	}

	@Override
	public String getName() {
		return "R51 Patch 16";
	}
}
