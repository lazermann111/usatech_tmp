package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R50_Patch_4_ChangeSet extends MultiLinuxPatchChangeSet {
	public R50_Patch_4_ChangeSet() throws UnknownHostException {
		super();
		registerResource("ServerLayers/AppLayer/src/applayer-data-layer.xml", "classes/", "REL_applayer_1_36_4", false, USATRegistry.LOADER, USATRegistry.APP_LAYER);
	}

	@Override
	public String getName() {
		return "R50 Patch 4";
	}
}
