package com.usatech.tools.deploy.active.renew;

import static com.usatech.tools.deploy.USATRegistry.subHostPrefixMap;

import java.io.IOException;
import java.util.Date;

import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.BasicServerSet;
import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.Registry;
import com.usatech.tools.deploy.RenewFileCertTask;
import com.usatech.tools.deploy.Server;
import com.usatech.tools.deploy.USATRegistry;

public class RenewHotchoiceCert_ChangeSet implements ChangeSet {

	@Override
	public String getName() {
		return "Renew Hotchoice Certificate";
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("WEB");
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		Registry registry = new USATRegistry();
		App app = USATRegistry.HTTPD_WEB;
		App subApp = USATRegistry.HOTCHOICE_APP;
		String appDir = "/opt/USAT/" + app.getName();
		String subHostPrefix = subHostPrefixMap.get(subApp.getName());
		if(subHostPrefix == null) {
			subHostPrefix = subApp.getName();
		}
		String subhostname = subHostPrefix + ("USA".equalsIgnoreCase(host.getServerEnv()) ? ".usatech.com" : "-" + host.getServerEnv().toLowerCase() + ".usatech.com");

		RenewFileCertTask rfct = new RenewFileCertTask(new Date(Long.MAX_VALUE /*System.currentTimeMillis() + 30 * 24 * 60 * 60 * 1000L*/), appDir + "/ssl/" + subApp.getName() + ".crt", appDir + "/ssl/" + subApp.getName() + ".key", 0644, 0640, app.getUserName(), app.getUserName(), subhostname, null, "USATWebServer", null);
		BasicServerSet serverSet = registry.getServerSet(host.getServerEnv());
		for(Server server : serverSet.getServers(app.getServerType()))
			if(!server.host.equals(host))
				rfct.addCopyToHosts(server.host);
		return new DeployTask[] { rfct };
	}

	@Override
	public String toString() {
		return getName();
	}
}
