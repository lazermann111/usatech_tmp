package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.CvsPullTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.DeployTaskInfo;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.UploadCoteTask;

public class R51PremierFoodCote_ChangeSet implements ChangeSet {
	public R51PremierFoodCote_ChangeSet() {
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		return getTasks(new DeployTaskInfo(null, host, null, cache, null));
	}

	public DeployTask[] getTasks(DeployTaskInfo deployTaskInfo) throws IOException {
		List<DeployTask> tasks = new ArrayList<DeployTask>();
		if(deployTaskInfo.host.isServerType("ODB")) {
			String subdomain;
			if("USA".equalsIgnoreCase(deployTaskInfo.host.getServerEnv()))
				subdomain = "getmore.usatech.com";
			else if("LOCAL".equalsIgnoreCase(deployTaskInfo.host.getServerEnv()))
				subdomain = "yhe.usatech.com"; // java.net.Inet4Address.getLocalHost().getCanonicalHostName();
			else
				subdomain = "getmore-" + deployTaskInfo.host.getServerEnv().toLowerCase() + ".usatech.com";
			String brandingDir = "premier";
			tasks.add(new UploadCoteTask("home_image", "image/png", subdomain+ "/" + brandingDir, new CvsPullTask("Prepaid/branded/premier/images/home-logo_premiermarkets-with-slogan.jpg", "HEAD")));
			tasks.add(new UploadCoteTask("cash_icon", "image/png", subdomain+ "/" + brandingDir, new CvsPullTask("Prepaid/branded/premier/images/icon-cash60x50_premier.png", "HEAD")));
			tasks.add(new UploadCoteTask("new_icon", "image/png", subdomain+ "/" + brandingDir, new CvsPullTask("Prepaid/branded/premier/images/icon-new60x50_premier.png", "HEAD")));
			tasks.add(new UploadCoteTask("sale_icon", "image/png", subdomain+ "/" + brandingDir, new CvsPullTask("Prepaid/branded/premier/images/icon-sale60x50_premier.png", "HEAD")));
			tasks.add(new UploadCoteTask("bottom_logo", "image/png", subdomain+ "/" + brandingDir, new CvsPullTask("Prepaid/branded/premier/images/logo-premier-footer200x80.png", "HEAD")));
			tasks.add(new UploadCoteTask("new_user_image", "image/png", subdomain+ "/" + brandingDir, new CvsPullTask("Prepaid/branded/premier/images/logo-premier-signup150x60.png", "HEAD")));
			tasks.add(new UploadCoteTask("top_logo_welcome", "image/png", subdomain+ "/" + brandingDir, new CvsPullTask("Prepaid/branded/premier/images/logo-premier-small65x30.png", "HEAD")));
			tasks.add(new UploadCoteTask("top_logo", "image/png", subdomain+ "/" + brandingDir, new CvsPullTask("Prepaid/branded/premier/images/logo-premier-loggedin-small65x30.png", "HEAD")));
			tasks.add(new UploadCoteTask("extra_style", "text/css", subdomain + "/" + brandingDir, new CvsPullTask("Prepaid/branded/premier/css/premier.css", "HEAD")));
		}
		return tasks.toArray(new DeployTask[tasks.size()]);
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("ODB");
	}

	@Override
	public String getName() {
		return "R51 Premier Food Prepaid Branding Update";
	}

	@Override
	public String toString() {
		return getName();
	}
}
