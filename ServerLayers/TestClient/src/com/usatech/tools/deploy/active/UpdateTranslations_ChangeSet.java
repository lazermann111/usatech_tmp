package com.usatech.tools.deploy.active;

import java.io.IOException;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.FromCvsUploadTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;

public class UpdateTranslations_ChangeSet implements ChangeSet {

	@Override
	public String getName() {
		return "Update Translations";
	}

	@Override
	public boolean isApplicable(Host host) {
		return true;
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		return new DeployTask[] { 
				new FromCvsUploadTask("ServerLayers/LayersCommon/src/translations.properties", "HEAD", "/opt/USAT/conf/translations.properties", 0640, "root", "usat", true, true) 
		};
	}

	@Override
	public String toString() {
		return getName();
	}
}
