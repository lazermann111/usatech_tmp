package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R56_Patch_23_ChangeSet extends MultiLinuxPatchChangeSet {
	public R56_Patch_23_ChangeSet() throws UnknownHostException {
		super();
		registerResource("usalive/web/images/login-banner-ad-nama2018.jpg", "web/images", "REL_R56_23", false, USATRegistry.HTTPD_WEB);
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.USALIVE_APP);
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.VERIZON_APP);
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.HOTCHOICE_APP);

		registerResource("usalive/web/images/login-banner-ad-nama2018.jpg", "web/images", "REL_R56_23", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/web/login.html.jsp", "web/", "REL_R56_23", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/customerreporting_web/home.jsp", "web/", "REL_R56_23", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/customerreporting_web/login.jsp", "web/", "REL_R56_23", false, USATRegistry.USALIVE_APP);
	}
	
	@Override
	public String getName() {
		return "R56 Patch 23 - 2018 NAMA";
	}
}
