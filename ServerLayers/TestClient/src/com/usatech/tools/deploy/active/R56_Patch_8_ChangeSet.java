package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.authoritylayer.TandemGatewayTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R56_Patch_8_ChangeSet extends MultiLinuxPatchChangeSet {
	public R56_Patch_8_ChangeSet() throws UnknownHostException {
		super();

		registerSource("ServerLayers/AuthorityLayer/src", TandemGatewayTask.class, "REL_R56_8", USATRegistry.INAUTH_LAYER);
	}
	
	@Override
	public String getName() {
		return "R56 Patch 8 - MasterCard Facilitator ID";
	}
}
