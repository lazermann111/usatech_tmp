package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.usatech.eportconnect.ECSessionManager;
import com.usatech.networklayer.app.AbstractClientSession;
import com.usatech.tools.deploy.*;

public class R53_Patch_10_ChangeSet extends MultiLinuxPatchChangeSet {
	
	public R53_Patch_10_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/NetLayer/src", AbstractClientSession.class, "REL_netlayer_1_39_10", USATRegistry.NET_LAYER);
		registerSource("ServerLayers/NetLayer/src", ECSessionManager.class, "REL_netlayer_1_39_10", USATRegistry.NET_LAYER);
	}
	
	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(app == USATRegistry.NET_LAYER) {
			String appPropertiesFile = String.format("/opt/USAT/%s%s/specific/USAT_app_settings.properties", app.getName(), instance == null ? "" : instance);
			PropertiesUpdateFileTask puft = new PropertiesUpdateFileTask(true, 0640, app.getUserName(), app.getUserName());
			puft.setFilePath(appPropertiesFile);
			puft.registerValue("simple.mq.Supervisor.temporaryPublishType(usat.inbound.session.control)", new DefaultHostSpecificValue("DB"));
			puft.registerValue("simple.mq.Supervisor.temporaryPublishType(usat.audit.message.processed)", new DefaultHostSpecificValue("DB"));
			tasks.add(puft);
		}
		
		super.addRestartAppCommands(host, app, instance, instanceNum, instanceCount, tasks, commands, cache);
	}
	
	@Override
	public String getName() {
		return "R53 Patch 10";
	}
}
