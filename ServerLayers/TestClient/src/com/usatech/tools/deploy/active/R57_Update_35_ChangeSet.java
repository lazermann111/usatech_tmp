package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.transport.soap.EasitraxSOAPTransporter;
import com.usatech.transport.soap.easitrax.VDIUploadDEX;
import com.usatech.transport.soap.easitrax.VDIUploadDEXLocator;
import com.usatech.transport.soap.easitrax.VDIUploadDEXSoap12Impl;
import com.usatech.transport.soap.easitrax.VDIUploadDEXSoap12Stub;
import com.usatech.transport.soap.easitrax.VDIUploadDEXSoap_BindingImpl;
import com.usatech.transport.soap.easitrax.VDIUploadDEXSoap_BindingStub;
import com.usatech.transport.soap.easitrax.VDIUploadDEXSoap_PortType;

public class R57_Update_35_ChangeSet extends MultiLinuxPatchChangeSet {
	public R57_Update_35_ChangeSet() throws UnknownHostException {
		super();		
		registerSource("ReportGenerator/src", EasitraxSOAPTransporter.class, "REL_R57_35", USATRegistry.TRANSPORT_LAYER);
		registerSource("ReportGenerator/src", VDIUploadDEX.class, "REL_R57_35", USATRegistry.TRANSPORT_LAYER);
		registerSource("ReportGenerator/src", VDIUploadDEXLocator.class, "REL_R57_35", USATRegistry.TRANSPORT_LAYER);
		registerSource("ReportGenerator/src", VDIUploadDEXSoap_BindingImpl.class, "REL_R57_35", USATRegistry.TRANSPORT_LAYER);
		registerSource("ReportGenerator/src", VDIUploadDEXSoap_BindingStub.class, "REL_R57_35", USATRegistry.TRANSPORT_LAYER);
		registerSource("ReportGenerator/src", VDIUploadDEXSoap_PortType.class, "REL_R57_35", USATRegistry.TRANSPORT_LAYER);
		registerSource("ReportGenerator/src", VDIUploadDEXSoap12Impl.class, "REL_R57_35", USATRegistry.TRANSPORT_LAYER);
		registerSource("ReportGenerator/src", VDIUploadDEXSoap12Stub.class, "REL_R57_35", USATRegistry.TRANSPORT_LAYER);
		registerResource("ReportGenerator/src/TransportService.properties", "classes", "REL_R57_35", true, USATRegistry.TRANSPORT_LAYER);
	}
	
	@Override
	public String getName() {
		return "R57 Update 35 - Easitrax VDI Transport";
	}
}
