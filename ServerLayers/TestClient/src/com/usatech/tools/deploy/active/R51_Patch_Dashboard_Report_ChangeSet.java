package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R51_Patch_Dashboard_Report_ChangeSet extends MultiLinuxPatchChangeSet {
	public R51_Patch_Dashboard_Report_ChangeSet() throws UnknownHostException {
		super();
		registerResource("usalive/xsl/other-data-layer.xml", "classes", "BRN_R51", false, USATRegistry.USALIVE_APP);
	}
	
	@Override
	public String getName() {
		return "R51 Dashboard Report Change";
	}
}
