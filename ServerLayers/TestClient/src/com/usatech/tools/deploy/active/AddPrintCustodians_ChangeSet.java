package com.usatech.tools.deploy.active;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.List;

import simple.app.Processor;
import simple.app.ServiceException;

import com.usatech.keymanager.util.PrintCustodians;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.DeployTaskInfo;
import com.usatech.tools.deploy.DeployUtils;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.USATRegistry;
import com.usatech.tools.deploy.UploadTask;

public class AddPrintCustodians_ChangeSet extends MultiLinuxPatchChangeSet {
	public AddPrintCustodians_ChangeSet() throws UnknownHostException {
		super();
		registerSource("KeyManager/src-common", PrintCustodians.class, "HEAD", USATRegistry.KEYMGR);
	}

	@Override
	protected void addRestartAppCommands(Host host, App app, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		//don't restart but create script file
		String appDir = "/opt/USAT/" + app.getName() + (instance == null ? "" : instance);
		tasks.add(new UploadTask(new Processor<DeployTaskInfo, InputStream>() {			
			public InputStream process(DeployTaskInfo argument) throws ServiceException {
				char[] trustStorePassword = DeployUtils.getPassword(argument, "truststore", "Trust Store");
				StringBuilder content = new StringBuilder();
				content.append("#!/bin/bash\ncd `dirname \"$0\"`/..\n/usr/jdk/latest/bin/java -Djavax.net.ssl.trustStore=../conf/webtruststore.ts -Djavax.net.ssl.trustStorePassword=");
				if(trustStorePassword != null)
					content.append(trustStorePassword);
				content.append(" -cp \"classes:lib/keymgrcommon.jar\" com.usatech.keymanager.util.PrintCustodians store/secret.store\n");
				return new ByteArrayInputStream(content.toString().getBytes());
			}
			
			public Class<InputStream> getReturnType() {
				return InputStream.class;
			}
			
			public Class<DeployTaskInfo> getArgumentType() {
				return DeployTaskInfo.class;
			}
		}, 0750, "keymgr", "keymgr", "PrintCustodians.sh", true, appDir + "/bin/PrintCustodians.sh"));
	}

	@Override
	public String getName() {
		return "Add PrintCustodians";
	}
}
