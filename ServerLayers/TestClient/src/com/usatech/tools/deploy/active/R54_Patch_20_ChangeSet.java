package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usat.logslayer.LogAgentService;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R54_Patch_20_ChangeSet extends MultiLinuxPatchChangeSet {
	public R54_Patch_20_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/LogLayer/src/", LogAgentService.class, "REL_logslayer_1_0_20",
				USATRegistry.LOGS_AGENT_APP, USATRegistry.LOGS_AGENT_APR_APP, USATRegistry.LOGS_AGENT_NET_APP,
				USATRegistry.LOGS_AGENT_KLS_APP, USATRegistry.LOGS_AGENT_WEB);
	}

	@Override
	public String getName() {
		return "R54 Patch 20 - Log Layer";
	}
}
