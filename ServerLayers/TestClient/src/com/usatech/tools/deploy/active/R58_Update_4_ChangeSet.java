package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.applayer.AuthorizeTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R58_Update_4_ChangeSet extends MultiLinuxPatchChangeSet {
	public R58_Update_4_ChangeSet() throws UnknownHostException {
		super();
		registerResource("usalive/xsl/templates/device/terminal-customer-privs.xsl", "classes/templates/device", "REL_R58_4", false, USATRegistry.USALIVE_APP);
		registerResource("usalive/customerreporting_web/terminal_details.jsp", "web", "REL_R58_4", false, USATRegistry.USALIVE_APP);
	}
	
	@Override
	public String getName() {
		return "R58 Update 4 - User devices, Terminal details: caption, map overlaps other data";
	}
}
