package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.applayer.SessionControlTask;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R58_Update_14_ChangeSet extends MultiLinuxPatchChangeSet {
	public R58_Update_14_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/AppLayer/src", SessionControlTask.class, "REL_R58_14", USATRegistry.LOADER);
	}
	
	@Override
	public String getName() {
		return "R58 Update 14 - Loader not-null constraint violation";
	}
}
