package com.usatech.tools.deploy.active;

import com.usatech.tools.deploy.*;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

public class R52_Linux_ChangeSet extends AbstractLinuxChangeSet {
	public R52_Linux_ChangeSet() throws UnknownHostException {
		super();
	}

	@Override
	protected void registerApps() {
		//registerApp(USATRegistry.KEYMGR, "2.19.0");
		registerApp(USATRegistry.LOADER, "1.38.0");
		registerApp(USATRegistry.APP_LAYER, "1.38.0");
		registerApp(USATRegistry.INAUTH_LAYER, "1.38.0");
		registerApp(USATRegistry.POSM_LAYER, "1.29.0");
		registerApp(USATRegistry.NET_LAYER, "1.38.0");
		registerApp(USATRegistry.OUTAUTH_LAYER, "1.38.0");
		registerApp(USATRegistry.DMS_APP, "1.14.0");
		registerApp(USATRegistry.HTTPD_NET, null);
		registry.registerSubApps(USATRegistry.HTTPD_NET, USATRegistry.DMS_APP);
		registerApp(USATRegistry.RPTGEN_LAYER, "3.26.0");
		registerApp(USATRegistry.RPTREQ_LAYER, "3.26.0");
		registerApp(USATRegistry.USALIVE_APP, "1.28.0");
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.USALIVE_APP);
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.VERIZON_APP);
		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.HOTCHOICE_APP);
//		registerApp(USATRegistry.PREPAID_APP, "1.15.0");
//		registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.GETMORE_APP); // The APP server needs PREPAID_APP, but the WEB server also needs the GETMORE_APP as subapp
		// registry.registerSubApps(USATRegistry.HTTPD_WEB, USATRegistry.PREPAID_APP);
		registerApp(USATRegistry.HTTPD_WEB, null);
		registerApp(USATRegistry.TRANSPORT_LAYER, "3.26.0");
		// registerApp(USATRegistry.POSTGRES_KLS);
		// registerApp(USATRegistry.POSTGRES_MST);
		// registerApp(USATRegistry.POSTGRES_MSR);
		//registerApp(USATRegistry.POSTGRES_PGS);
	}

	@Override
	public String getName() {
		return "Edge Server - R52 Install - Apps";
	}

	@Override
	public boolean isApplicable(Host host) {
		return super.isApplicable(host) || host.isServerType("PGS");
	}

	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		//USATHelper.installJava(host, cache, tasks, 8, 60, getWorkDir());
		addPrepareHostTasks(host, tasks, cache);
		tasks.add(new UsatSettingUpdateFileTask(registry.getServerSet(host.getServerEnv())));
		tasks.add(new ExecuteTask("sudo find /opt/USAT/ -name '*.dmp' -delete"));
	}

	@Override
	protected void addTasks(Host host, final App app, Integer ordinal, String instance, final int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		addPrepareAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache);
		if(app.getInstallFileName() != null)
			tasks.add(new AppSettingUpdateFileTask(app, ordinal, instanceNum, instanceCount, cache, registry.getServerSet(host.getServerEnv()), false));
		addInstallAppTasks(host, app, ordinal, instanceNum, instanceCount, tasks, cache, commands);
	}
}
