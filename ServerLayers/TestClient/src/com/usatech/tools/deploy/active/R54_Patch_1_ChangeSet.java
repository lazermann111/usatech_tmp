package com.usatech.tools.deploy.active;

import java.net.UnknownHostException;

import com.usatech.applayer.InboundFileTransferTask;
import com.usatech.layers.common.dex.InboundFileImportTask;
import com.usatech.layers.common.model.Device;
import com.usatech.tools.deploy.MultiLinuxPatchChangeSet;
import com.usatech.tools.deploy.USATRegistry;

public class R54_Patch_1_ChangeSet extends MultiLinuxPatchChangeSet {
	public R54_Patch_1_ChangeSet() throws UnknownHostException {
		super();
		registerSource("ServerLayers/LayersCommon/src", Device.class, "REL_DMS_1_16_1", USATRegistry.DMS_APP);
		registerSource("ServerLayers/LayersCommon/src", InboundFileImportTask.class, "REL_applayer_1_40_1", USATRegistry.LOADER);
		registerSource("ServerLayers/AppLayer/src", InboundFileTransferTask.class, "REL_applayer_1_40_1", USATRegistry.LOADER);
	}
	
	@Override
	public String getName() {
		return "R54 Patch 1";
	}
}
