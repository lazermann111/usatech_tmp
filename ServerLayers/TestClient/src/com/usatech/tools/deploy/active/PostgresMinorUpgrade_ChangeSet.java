package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.ChooseTask;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.Registry;
import com.usatech.tools.deploy.USATHelper;
import com.usatech.tools.deploy.USATRegistry;

import simple.text.RegexUtils;

public class PostgresMinorUpgrade_ChangeSet implements ChangeSet {
	protected final Map<String, String> upgradeVersions = new LinkedHashMap<>();
	protected final Registry registry;

	public PostgresMinorUpgrade_ChangeSet() throws UnknownHostException {
		super();
		upgradeVersions.put("9.2", "9.2.13");
		upgradeVersions.put("9.3", "9.3.10");
		upgradeVersions.put("9.4", "9.4.4");
		upgradeVersions.put(null, "9.4.4");
		this.registry = new USATRegistry();
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("KLS") || host.isServerType("MST") || host.isServerType("MSR") || host.isServerType("PGS");
	}
	
	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		List<DeployTask> tasks = new ArrayList<>();
		// upgrade
		ChooseTask pgUpgradeTask = new ChooseTask("/usr/pgsql-latest/bin/pg_config --version | cut -d ' ' -f 2");
		tasks.add(pgUpgradeTask);
		for(Map.Entry<String, String> entry : upgradeVersions.entrySet()) {
			Pattern match;
			if(entry.getKey() == null)
				match = Pattern.compile(".*No such file or directory\\s*");
			else if(entry.getValue().startsWith(entry.getKey() + '.'))
				match = Pattern.compile(RegexUtils.escape(entry.getKey()) + "\\.(?!" + RegexUtils.escape(entry.getValue().substring(entry.getKey().length() + 1)) + ")\\d+\\s*");
			else
				match = Pattern.compile(RegexUtils.escape(entry.getKey()) + "\\..*");
			pgUpgradeTask.registerChoice(match, USATHelper.createUpgradePgMinorTask(host, entry.getValue()));
		}
		// pg_hba
		USATHelper.addPgHbaConfTasks(tasks, host, registry.getServerSet(host.getServerEnv()));
		// restart
		tasks.add(USATHelper.createPgRestartTask(host));
		return tasks.toArray(new DeployTask[tasks.size()]);
	}

	@Override
	public String getName() {
		return "Postgres Upgrade Minor";
	}

	@Override
	public String toString() {
		return getName();
	}
}
