package com.usatech.tools.deploy.active;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import simple.io.EncodingInputStream;
import simple.io.ReplacementsLineFilteringReader;

import com.usatech.tools.deploy.AbstractLinuxPatchChangeSet;
import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.FromCvsUploadTask;
import com.usatech.tools.deploy.GetPasswordHostSpecificValue;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.PropertiesUpdateFileTask;
import com.usatech.tools.deploy.ServerEnvHostSpecificValue;
import com.usatech.tools.deploy.USATRegistry;

public class UpdateXmxSetting_ChangeSet extends AbstractLinuxPatchChangeSet {
	public UpdateXmxSetting_ChangeSet() throws UnknownHostException {
		super();
	}
	protected void registerApps() {
		// registerApp(USATRegistry.LOADER);
		registerApp(USATRegistry.APP_LAYER);
		// registerApp(USATRegistry.INAUTH_LAYER);
		// registerApp(USATRegistry.POSM_LAYER);
		// registerApp(USATRegistry.NET_LAYER);
		// registerApp(USATRegistry.OUTAUTH_LAYER);
		// registerApp(USATRegistry.KEYMGR);
	}
	@Override
	public String getName() {
		return "Update Xmx Setting";
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return false;
	}

	@Override
	protected void addTasks(final Host host, final App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		final String appDir = "/opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal);
		tasks.add(new FromCvsUploadTask("ServerLayers/LayersCommon/conf/setenv.sh", "HEAD", appDir + "/bin/setenv.sh", 0750, app.getUserName(), app.getUserName(), true) {
			@Override
			protected InputStream decorate(InputStream in) {
				if(("USA".equalsIgnoreCase(host.getServerEnv()) || "ECC".equalsIgnoreCase(host.getServerEnv())) && (USATRegistry.APP_LAYER.equals(app) || USATRegistry.DMS_APP.equals(app) || USATRegistry.USALIVE_APP.equals(app) || USATRegistry.RPTGEN_LAYER.equals(app))) {
					Map<Pattern, String> replacements = new HashMap<Pattern, String>();
					replacements.put(Pattern.compile("\\B-Xmx\\d+[A-Za-z]?\\b"), USATRegistry.APP_LAYER.equals(app) || USATRegistry.RPTGEN_LAYER.equals(app) || USATRegistry.USALIVE_APP.equals(app) ? "-Xmx8192M" : "-Xmx3272M");
					in = new EncodingInputStream(new ReplacementsLineFilteringReader(new BufferedReader(new InputStreamReader(in)), "\n", replacements));
				}
				return in;
			}
		});
		if(USATRegistry.APP_LAYER.equals(app)) {
			PropertiesUpdateFileTask derbyProperties = new PropertiesUpdateFileTask(true, null, null, null);
			derbyProperties.setFilePath(appDir + "/db/derby.properties");
			derbyProperties.registerValue("derby.storage.pageCacheSize", new ServerEnvHostSpecificValue("1000", "1000", "50000", "50000"));
			derbyProperties.registerValue("derby.user.APP_LAYER", new ServerEnvHostSpecificValue("APP_LAYER", "APP_LAYER", new GetPasswordHostSpecificValue(cache, "database.EMBED.APP_LAYER", "APP_LAYER for AppLayerDB", true), new GetPasswordHostSpecificValue(cache, "database.EMBED.APP_LAYER", "APP_LAYER for AppLayerDB", true)));
			derbyProperties.registerValue("derby.user.APP_CLUSTER", new ServerEnvHostSpecificValue("APP_CLUSTER", "APP_CLUSTER", new GetPasswordHostSpecificValue(cache, "database.EMBED.APP_CLUSTER", "APP_CLUSTER for AppLayerDB", true), new GetPasswordHostSpecificValue(cache, "database.EMBED.APP_CLUSTER", "APP_CLUSTER for AppLayerDB", true)));
			tasks.add(derbyProperties);
		}
		commands.add("sudo su");
		addRestartAppCommands(host, app, instance, instanceNum, instanceCount, tasks, commands, cache);
	}
}
