package com.usatech.tools.deploy.active;

import java.io.IOException;
import java.util.Date;

import com.usatech.tools.deploy.ChangeSet;
import com.usatech.tools.deploy.DeployTask;
import com.usatech.tools.deploy.ExecuteTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.RenewFileCertTask;

public class RenewPostgresCert_ChangeSet implements ChangeSet {

	@Override
	public String getName() {
		return "Renew Postgres Certificate";
	}

	@Override
	public boolean isApplicable(Host host) {
		return host.isServerType("MST") || host.isServerType("MSR") || host.isServerType("PGS") || host.isServerType("KLS");
	}

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		String[] paths;
		String[] names;
		if(host.isServerType("KLS")) {
			if("LOCAL".equalsIgnoreCase(host.getServerEnv())) {
				paths = new String[] { "/opt/USAT/postgres/latest/data/" };
				names = new String[] { "postgres" };
			} else {
				char last = host.getSimpleName().charAt(host.getSimpleName().length() - 1);
				char max;
				if("DEV".equalsIgnoreCase(host.getServerEnv()) || "INT".equalsIgnoreCase(host.getServerEnv()))
					max = '2';
				else
					max = '4';
				char next = (char) (last + 1);
				if(next > max)
					next = '1';
				paths = new String[] { "/opt/USAT/postgres_" + last + "/data/", "/opt/USAT/postgres_" + next + "/data/" };
				names = new String[] { "postgres_" + last, "postgres_" + next };
			}
		} else if(host.isServerType("MST") && "INT".equalsIgnoreCase(host.getServerEnv())) {
			paths = new String[] { "/opt/USAT/postgres1/data/", "/opt/USAT/postgres2/data/" };
			names = new String[] { "postgres1", "postgres2" };
		} else {
			paths = new String[] { "/opt/USAT/postgres/data/" };
			names = new String[] { "postgres" };
		}
		/*final BufferStream buffer = new HeapBufferStream(true);
		try {
			Certificate rootCert = SecurityUtils.getDefaultTrustStore().getCertificate("usat-root-2015");
			Writer writer = new OutputStreamWriter(buffer.getOutputStream());
			CryptUtils.writeCertificate(rootCert, writer);
			writer.flush();
		} catch(KeyStoreException | NoSuchProviderException | NoSuchAlgorithmException | CertificateException e) {
			throw new IOException(e);
		}
		*/
		DeployTask[] tasks = new DeployTask[paths.length + 1];
		for(int i = 0; i < paths.length; i++) {
			tasks[i] = new RenewFileCertTask(new Date(System.currentTimeMillis() + 30 * 24 * 60 * 60 * 1000L), paths[i] + "server.crt", paths[i] + "root.crt", paths[i] + "server.key", 0644, 0600, "postgres", "postgres");
		}
		String[] cmds = new String[names.length];
		for(int i = 0; i < names.length; i++)
			cmds[i] = "sudo monit restart " + names[i];
		tasks[paths.length] = new ExecuteTask(cmds);
		return tasks;
	}

	@Override
	public String toString() {
		return getName();
	}
}
