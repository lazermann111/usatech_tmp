package com.usatech.tools.deploy;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import simple.app.Processor;
import simple.app.ServiceException;
import simple.io.ConcatenatingInputStream;

public class ConcatenatingContent implements Processor<DeployTaskInfo, InputStream> {
	protected final List<Processor<DeployTaskInfo, InputStream>> delegates = new ArrayList<Processor<DeployTaskInfo, InputStream>>();

	public ConcatenatingContent() {
	}

	@SafeVarargs
	public ConcatenatingContent(Processor<DeployTaskInfo, InputStream>... sources) {
		if(sources != null)
			for(Processor<DeployTaskInfo, InputStream> source : sources)
				delegates.add(source);
	}

	public void addSources(@SuppressWarnings("unchecked") Processor<DeployTaskInfo, InputStream>... sources) {
		if(sources != null)
			for(Processor<DeployTaskInfo, InputStream> source : sources)
				delegates.add(source);
	}

	public void removeSources(@SuppressWarnings("unchecked") Processor<DeployTaskInfo, InputStream>... sources) {
		if(sources != null)
			for(Processor<DeployTaskInfo, InputStream> source : sources)
				delegates.remove(source);
	}

	public InputStream process(DeployTaskInfo argument) throws ServiceException {
		InputStream[] ins = new InputStream[delegates.size()];
		int i = 0;
		boolean okay = false;
		try {
			for(Processor<DeployTaskInfo, InputStream> source : delegates)
				ins[i++] = source.process(argument);
			okay = true;
		} finally {
			if(!okay) {
				for(int k = 0; k < i; k++) {
					if(ins[k] != null)
						try {
							ins[k].close();
						} catch(IOException e) {
							// ignore
						}
				}
			}
		}
		return new ConcatenatingInputStream(ins);
	}

	public Class<InputStream> getReturnType() {
		return InputStream.class;
	}

	public Class<DeployTaskInfo> getArgumentType() {
		return DeployTaskInfo.class;
	}
}
