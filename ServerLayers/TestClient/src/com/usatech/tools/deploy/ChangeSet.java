package com.usatech.tools.deploy;

import java.io.IOException;


public interface ChangeSet {
	public String getName() ;
	public boolean isApplicable(Host host) ;
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException ;
}
