package com.usatech.tools.deploy;

import java.util.HashMap;

public class PgDeployMap {
	public static HashMap<String, Host> primaryMap = new HashMap<String, Host>();
	public static HashMap<String,Host> standbyMap=new HashMap<String, Host>();
	public static HashMap<String,PGDATADir> hostPGDATADirMap=new HashMap<String, PGDATADir>();
	public static HashMap<String, String> primaryBackupDirMap = new HashMap<String, String>();
	public static HashMap<String, String> standbyBackupDirMap = new HashMap<String, String>();
	
	static{
		//DEV
		standbyMap.put("devkls11", new HostImpl("dev", "kls", "devkls12.usatech.com", true));
		standbyMap.put("devkls12", new HostImpl("dev", "kls", "devkls11.usatech.com", true));
		primaryMap.put("devkls11", new HostImpl("dev", "kls", "devkls12.usatech.com"));
		primaryMap.put("devkls12", new HostImpl("dev", "kls", "devkls11.usatech.com"));
		hostPGDATADirMap.put("devkls11", new PGDATADir("/opt/USAT/postgres_1", "/opt/USAT/postgres_2"));
		hostPGDATADirMap.put("devkls12", new PGDATADir("/opt/USAT/postgres_2", "/opt/USAT/postgres_1"));
		primaryBackupDirMap.put("devkls11", "/opt/USAT/keymgr_backup_2");
		primaryBackupDirMap.put("devkls12", "/opt/USAT/keymgr_backup_1");
		standbyBackupDirMap.put("devkls11", "/opt/USAT/keymgr_backup_1");
		standbyBackupDirMap.put("devkls12", "/opt/USAT/keymgr_backup_2");
		
		//INT
		standbyMap.put("intkls11", new HostImpl("int", "kls", "intkls12.usatech.com", true));
		standbyMap.put("intkls12", new HostImpl("int", "kls", "intkls11.usatech.com", true));
		primaryMap.put("intkls11", new HostImpl("int", "kls", "intkls12.usatech.com"));
		primaryMap.put("intkls12", new HostImpl("int", "kls", "intkls11.usatech.com"));
		hostPGDATADirMap.put("intkls11", new PGDATADir("/opt/USAT/postgres_1", "/opt/USAT/postgres_2"));
		hostPGDATADirMap.put("intkls12", new PGDATADir("/opt/USAT/postgres_2", "/opt/USAT/postgres_1"));
		primaryBackupDirMap.put("intkls11", "/opt/USAT/keymgr_backup_2");
		primaryBackupDirMap.put("intkls12", "/opt/USAT/keymgr_backup_1");
		standbyBackupDirMap.put("intkls11", "/opt/USAT/keymgr_backup_1");
		standbyBackupDirMap.put("intkls12", "/opt/USAT/keymgr_backup_2");
		
		//ECC
		standbyMap.put("ecckls11", new HostImpl("ecc", "kls", "ecckls12.usatech.com", true));
		standbyMap.put("ecckls12", new HostImpl("ecc", "kls", "ecckls13.usatech.com", true));
		standbyMap.put("ecckls13", new HostImpl("ecc", "kls", "ecckls14.usatech.com", true));
		standbyMap.put("ecckls14", new HostImpl("ecc", "kls", "ecckls11.usatech.com", true));
		primaryMap.put("ecckls11", new HostImpl("ecc", "kls", "ecckls14.usatech.com"));
		primaryMap.put("ecckls12", new HostImpl("ecc", "kls", "ecckls11.usatech.com"));
		primaryMap.put("ecckls13", new HostImpl("ecc", "kls", "ecckls12.usatech.com"));
		primaryMap.put("ecckls14", new HostImpl("ecc", "kls", "ecckls13.usatech.com"));
		hostPGDATADirMap.put("ecckls11", new PGDATADir("/opt/USAT/postgres_1", "/opt/USAT/postgres_4"));
		hostPGDATADirMap.put("ecckls12", new PGDATADir("/opt/USAT/postgres_2", "/opt/USAT/postgres_1"));
		hostPGDATADirMap.put("ecckls13", new PGDATADir("/opt/USAT/postgres_3", "/opt/USAT/postgres_2"));
		hostPGDATADirMap.put("ecckls14", new PGDATADir("/opt/USAT/postgres_4", "/opt/USAT/postgres_3"));
		primaryBackupDirMap.put("ecckls11", "/opt/USAT/keymgr_backup_4");
		primaryBackupDirMap.put("ecckls12", "/opt/USAT/keymgr_backup_1");
		primaryBackupDirMap.put("ecckls13", "/opt/USAT/keymgr_backup_2");
		primaryBackupDirMap.put("ecckls14", "/opt/USAT/keymgr_backup_3");
		standbyBackupDirMap.put("ecckls11", "/opt/USAT/keymgr_backup_1");
		standbyBackupDirMap.put("ecckls12", "/opt/USAT/keymgr_backup_2");
		standbyBackupDirMap.put("ecckls13", "/opt/USAT/keymgr_backup_3");
		standbyBackupDirMap.put("ecckls14", "/opt/USAT/keymgr_backup_4");
		
		//USA
		standbyMap.put("usakls31", new HostImpl("usa", "kls", "usakls32.trooper.usatech.com", true));
		standbyMap.put("usakls32", new HostImpl("usa", "kls", "usakls33.trooper.usatech.com", true));
		standbyMap.put("usakls33", new HostImpl("usa", "kls", "usakls34.trooper.usatech.com", true));
		standbyMap.put("usakls34", new HostImpl("usa", "kls", "usakls31.trooper.usatech.com", true));
		primaryMap.put("usakls31", new HostImpl("usa", "kls", "usakls34.trooper.usatech.com"));
		primaryMap.put("usakls32", new HostImpl("usa", "kls", "usakls31.trooper.usatech.com"));
		primaryMap.put("usakls33", new HostImpl("usa", "kls", "usakls32.trooper.usatech.com"));
		primaryMap.put("usakls34", new HostImpl("usa", "kls", "usakls33.trooper.usatech.com"));
		hostPGDATADirMap.put("usakls31", new PGDATADir("/opt/USAT/postgres_1", "/opt/USAT/postgres_4"));
		hostPGDATADirMap.put("usakls32", new PGDATADir("/opt/USAT/postgres_2", "/opt/USAT/postgres_1"));
		hostPGDATADirMap.put("usakls33", new PGDATADir("/opt/USAT/postgres_3", "/opt/USAT/postgres_2"));
		hostPGDATADirMap.put("usakls34", new PGDATADir("/opt/USAT/postgres_4", "/opt/USAT/postgres_3"));
		primaryBackupDirMap.put("usakls31", "/opt/USAT/keymgr_backup_4");
		primaryBackupDirMap.put("usakls32", "/opt/USAT/keymgr_backup_1");
		primaryBackupDirMap.put("usakls33", "/opt/USAT/keymgr_backup_2");
		primaryBackupDirMap.put("usakls34", "/opt/USAT/keymgr_backup_3");
		standbyBackupDirMap.put("usakls31", "/opt/USAT/keymgr_backup_1");
		standbyBackupDirMap.put("usakls32", "/opt/USAT/keymgr_backup_2");
		standbyBackupDirMap.put("usakls33", "/opt/USAT/keymgr_backup_3");
		standbyBackupDirMap.put("usakls34", "/opt/USAT/keymgr_backup_4");
	}
	public static class PGDATADir{
		String primaryDir,standbyDir;

		public PGDATADir(String primaryDir, String standbyDir) {
			super();
			this.primaryDir = primaryDir;
			this.standbyDir = standbyDir;
		}

		public String getPrimaryDir() {
			return primaryDir;
		}

		public String getStandbyDir() {
			return standbyDir;
		}
		
	}
	
	
}
