package com.usatech.tools.deploy;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import simple.io.UnixEolInputStream;

public class UnixUploadTask extends UploadTask {
	public UnixUploadTask(File file, int permissions, boolean overwriteExisting, String contentPath) throws IOException {
		this(new FileInputStream(file), file.getName(), permissions, null, null, overwriteExisting, contentPath);
	}
	public UnixUploadTask(File file, int permissions, boolean overwriteExisting) throws IOException {
		this(new FileInputStream(file), file.getName(), permissions, null, null, overwriteExisting, file.getCanonicalPath());
	}
	public UnixUploadTask(File file, int permissions, String owner, String group, boolean overwriteExisting) throws IOException {
		this(new FileInputStream(file), file.getName(), permissions, owner, group, overwriteExisting, file.getCanonicalPath());
	}

	public UnixUploadTask(File file, String remoteDir, int permissions, String owner, String group, boolean overwriteExisting) throws IOException {
		this(new FileInputStream(file), toUnixDirectory(remoteDir) + file.getName(), permissions, owner, group, overwriteExisting, file.getCanonicalPath());
	}

	public UnixUploadTask(File file, String remoteDir, String fileName, int permissions, String owner, String group, boolean overwriteExisting) throws IOException {
		this(new FileInputStream(file), toUnixDirectory(remoteDir) + fileName, permissions, owner, group, overwriteExisting, file.getCanonicalPath());
	}

	public UnixUploadTask(InputStream content, String remoteDir, String fileName, int permissions, String owner, String group, boolean overwriteExisting) {
		this(content, toUnixDirectory(remoteDir) + fileName, permissions, owner, group, overwriteExisting, fileName);
	}

	public UnixUploadTask(InputStream content, String remoteDir, String fileName, int permissions, String owner, String group, boolean overwriteExisting, String contentPath) {
		this(content, toUnixDirectory(remoteDir) + fileName, permissions, owner, group, overwriteExisting, contentPath);
	}

	public UnixUploadTask(InputStream content, String remoteName, int permissions, String owner, String group, boolean overwriteExisting, String contentPath) {
		super(new UnixEolInputStream(content), permissions, owner, group, contentPath, overwriteExisting, remoteName);
	}

}
