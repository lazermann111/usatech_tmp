package com.usatech.tools.deploy;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.security.GeneralSecurityException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.X509ExtendedKeyManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpState;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.auth.BasicScheme;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpConnectionParams;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.apache.commons.httpclient.protocol.SecureProtocolSocketFactory;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.sshtools.j2ssh.SshClient;
import com.sshtools.j2ssh.session.SessionChannelClient;
import com.sshtools.j2ssh.transport.HostKeyVerification;
import com.sshtools.j2ssh.transport.TransportProtocolException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.HeapBufferStream;
import simple.io.Interaction;
import simple.io.Log;
import simple.io.resource.sftp.SftpUtils;
import simple.security.SecurityUtils;
import simple.text.StringUtils;
import sun.security.ssl.SSLContextImpl;
import sun.security.ssl.SSLSocketFactoryImpl;

public class DeployUtils {
	private static final Log log = Log.getLog();
	protected static final Pattern aliasPattern = Pattern.compile("mce\\.([^.]+)\\.data\\.([^.]+)");

	public static InputStream signCertificate(Interaction interaction, PasswordCache passwordCache, String csrContent) throws HttpException, IOException {
		return signCertificate("usasubca.usatech.com", "USATWebServer", System.getProperty("user.name"), interaction, passwordCache, csrContent);
	}

	public static InputStream signCertificate(String certificateAuthority, String templateName, String causername, Interaction interaction, PasswordCache passwordCache, String csrContent) throws HttpException, IOException {
		//Sign entries
		HttpClient httpClient = new HttpClient();
		String url = "https://" + certificateAuthority + "/certsrv/certfnsh.asp";
		HttpState state = new HttpState();
		PostMethod method = new PostMethod(url);
		method.addParameter("CertRequest", csrContent);
		method.addParameter("TargetStoreFlags", "0");
		method.addParameter("SaveCert", "yes");
		method.addParameter("Mode", "newreq");
		method.addParameter("CertAttrib", "CertificateTemplate:" + templateName); // USATSmartcardUser or USATWebServer or USATCodeSigning
		method.getHostAuthState().setAuthScheme(new BasicScheme());
		
		for(int i = 0; i < 10; i++) {
			char[] capassword = getWebPassword(passwordCache, url, causername, interaction);
			try {
				state.setCredentials(new AuthScope(null, -1, null, "basic"), new UsernamePasswordCredentials(causername, new String(capassword)));

				int rc = httpClient.executeMethod(null, method, state);
				if(rc != 200)
					throw new IOException("Could not sign certificate. Got http code " + rc);
				break;
			} catch(IOException e) {
				log.warn("Could not log into '" + url + "'", e);
				interaction.printf("Could not log into '%1$s' because: %2$s", url, e.getMessage());
			}
			DeployUtils.storeWebPassword(passwordCache, url, causername, capassword);
		}
		int requestId = 0;
		BufferedReader reader = new BufferedReader(new InputStreamReader(method.getResponseBodyAsStream()));
		Pattern pattern = Pattern.compile("\\?ReqID=(\\d+)\\&");
		String line;
		while((line=reader.readLine()) != null) {
			Matcher matcher = pattern.matcher(line);
			if(matcher.find()) {
				try {
					requestId = ConvertUtils.getInt(matcher.group(1));
				} catch(ConvertException e) {
					log.warn("Could not convert requestId", e);
				}
				break;
			}
		}
		if(requestId == 0)
			throw new IOException("Could not find requestId in response");
		url = "https://" + certificateAuthority + "/certsrv/certnew.p7b?ReqID=" + requestId + "&Enc=b64";
		GetMethod getMethod = new GetMethod(url);
		getMethod.getHostAuthState().setAuthScheme(new BasicScheme());
		
		int rc = httpClient.executeMethod(null, getMethod, state);
		if(rc != 200)
			throw new IOException("Could not get certificate. Got http code " + rc);
		Header contentTypeHeader = getMethod.getResponseHeader("Content-Type");
		if(contentTypeHeader != null && contentTypeHeader.getValue() != null && contentTypeHeader.getValue().startsWith("text/html")) {
			throw new IOException("Did not get DER-encoded certificate chain. Instead got:\n" + getMethod.getResponseBodyAsString());
		}
		return getMethod.getResponseBodyAsStream();
	}
	
	public static char[] getPassword(DeployTaskInfo taskInfo, String key, String desc) {
		return getPassword(taskInfo.passwordCache, taskInfo.host, key, desc, taskInfo.interaction);
	}
	
	public static char[] getPassword(PasswordCache cache, Host host, String key, String desc, Interaction interaction) {
		char[] pwd = cache.getPassword(host, key);
		if(pwd == null) {
			pwd = interaction.readPassword("Please enter the value/password for %1$s on %2$s:", desc, host.getSimpleName());
			if(pwd == null)
				pwd = new char[0];
			cache.setPassword(host, key, pwd);
		}
		return pwd;
	}

	public static char[] getWebPassword(PasswordCache cache, String url, String user, Interaction interaction) {
		char[] pwd = cache.getWebPassword(url, user);
		if(pwd == null)
			pwd = interaction.readPassword("Please enter the value/password for  user '%1$s' at '%2$s':", user, url);
		return pwd;
	}

	public static void storeWebPassword(PasswordCache cache, String url, String user, char[] pwd) {
		if(pwd == null)
			pwd = new char[0];
		cache.setWebPassword(url, user, pwd);
	}
	
	public static boolean connectAndAuthenticate(Session ssh, Host host, String username, Interaction interaction,
			PasswordCache passwordCache) throws IOException {
		boolean okay = false;
		boolean reconnected = false;
		if (!ssh.isConnected()) {
			try {
				char[] password = passwordCache.getPassword(host, "ssh");
				if (password == null) {
					password = interaction.readPassword("Please enter the value/password for %1$s on %2$s:", username,
							host.getSimpleName());
					if (password == null)
						password = new char[0];
				}
				ssh.setPassword(String.valueOf(password));
				try {
					ssh.connect();
					okay = true;
					reconnected = true;
				} catch (JSchException e) {
					interaction.printf("Could not connect to %1$s because: %2$s", host.getSimpleName(), e.getMessage());
					throw new IOException("Could not connect to " + host.getSimpleName(), e);
				}

			} finally {
				if (!okay) {
					ssh.disconnect();
					ssh = null;
				}
			}
		}
		return reconnected;

	}

	public static boolean connectAndAuthenticate(SshClient ssh, Host host, String username, Interaction interaction, HostKeyVerification hostKeyVerification, PasswordCache passwordCache) throws IOException {
		boolean okay = false;
		boolean reconnected = false;
		try {
			while(true) {
				if(!ssh.isConnected()) {
					try {
						ssh.connect(host.getFullName(), hostKeyVerification);
						reconnected = true;
					} catch(IOException e) {
						interaction.printf("Could not connect to %1$s because: %2$s", host.getSimpleName(), e.getMessage());
						throw new IOException("Could not connect to " + host.getSimpleName(), e);
					}
				} else if(ssh.isAuthenticated()) {
					okay = true;
					return reconnected;
				}
				char[] password = passwordCache.getPassword(host, "ssh");
				if(password == null) {
					password = interaction.readPassword("Please enter the value/password for %1$s on %2$s:", username, host.getSimpleName());
					if(password == null)
						password = new char[0];
				}
				try {
					SftpUtils.authenticate(ssh, username, new String(password));
					passwordCache.setPassword(host, "ssh", password);
					okay = true;
					break;
				} catch(TransportProtocolException e) {
					if(e.getMessage().equals("The transport protocol is disconnected")) {
						ssh.disconnect();
					}
				} catch(IOException e) {
					log.warn("Login failed", e);
					interaction.printf("Login to %1$s failed because: %2$s", host.getSimpleName(), e.getMessage());
				}
			}
		} finally {
			if(!okay) {
				ssh.disconnect();
				ssh = null;
			}
		}
		return reconnected;
	}
	protected static class OneCertChainKeyManager extends X509ExtendedKeyManager {
		protected final X509Certificate[] certs;
		protected final PrivateKey key;

		public OneCertChainKeyManager(X509Certificate[] certs, PrivateKey key) {
			super();
			this.certs = certs;
			this.key = key;
		}

		@Override
		public String chooseClientAlias(String[] keyType, Principal[] issuers, Socket socket) {
			return "ANY";
		}

		@Override
		public String chooseServerAlias(String keyType, Principal[] issuers, Socket socket) {
			return "ANY";
		}

		@Override
		public X509Certificate[] getCertificateChain(String alias) {
			return certs;
		}

		@Override
		public String[] getClientAliases(String keyType, Principal[] issuers) {
			return new String[] { "ANY" };
		}

		@Override
		public PrivateKey getPrivateKey(String alias) {
			return key;
		}

		@Override
		public String[] getServerAliases(String keyType, Principal[] issuers) {
			return new String[] { "ANY" };
		}
	}

	public static boolean[] attemptHttps(Certificate[] certificateChain, PrivateKey privateKey, Interaction interaction, String... urls) throws Exception {
		List<X509Certificate> x509certs = new ArrayList<X509Certificate>();
		for(Certificate cert : certificateChain)
			if(cert instanceof X509Certificate)
				x509certs.add((X509Certificate) cert);
		return attemptHttps(x509certs.toArray(new X509Certificate[x509certs.size()]), privateKey, interaction, urls);
	}

	protected static SSLContextImpl createSelectedSSLContext(X509ExtendedKeyManager keyManager, X509TrustManager trustManager) throws Exception {
		SSLContextImpl ssl = new SSLContextImpl.DefaultSSLContext();
		if(keyManager != null)
			setPrivateField(ssl, "keyManager", keyManager);
		if(trustManager != null)
			setPrivateField(ssl, "trustManager", trustManager);
		return ssl;
	}

	protected static void setPrivateField(Object object, String fieldName, Object value) throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		Field field = object.getClass().getDeclaredField(fieldName);
		boolean accessible = field.isAccessible();
		field.setAccessible(true);
		try {
			field.set(object, value);
		} finally {
			field.setAccessible(accessible);
		}
	}
	protected static class SelectedSSLProtocolSocketFactory implements SecureProtocolSocketFactory {
		protected final SSLSocketFactoryImpl factory;

		/**
		 * Constructor for SSLProtocolSocketFactory.
		 * 
		 * @throws NoSuchMethodException
		 * @throws SecurityException
		 * @throws InvocationTargetException
		 * @throws IllegalAccessException
		 * @throws InstantiationException
		 * @throws IllegalArgumentException
		 * @throws KeyManagementException
		 */
		public SelectedSSLProtocolSocketFactory(X509ExtendedKeyManager keyManager, X509TrustManager trustManager) throws Exception {
			super();
			Constructor<SSLSocketFactoryImpl> constructor = SSLSocketFactoryImpl.class.getDeclaredConstructor(SSLContextImpl.class);
			constructor.setAccessible(true);
			factory = constructor.newInstance(createSelectedSSLContext(keyManager, trustManager));
		}

		/**
		 * @see SecureProtocolSocketFactory#createSocket(java.lang.String,int,java.net.InetAddress,int)
		 */
		public Socket createSocket(
				String host,
				int port,
				InetAddress clientHost,
				int clientPort)
				throws IOException, UnknownHostException {
			return factory.createSocket(
					host,
					port,
					clientHost,
					clientPort
					);
		}

		/**
		 * Attempts to get a new socket connection to the given host within the given time limit.
		 * <p>
		 * This method employs several techniques to circumvent the limitations of older JREs that do not support connect timeout. When running in JRE 1.4 or above reflection is used to call
		 * Socket#connect(SocketAddress endpoint, int timeout) method. When executing in older JREs a controller thread is executed. The controller thread attempts to create a new socket within the
		 * given limit of time. If socket constructor does not return until the timeout expires, the controller terminates and throws an {@link ConnectTimeoutException}
		 * </p>
		 * 
		 * @param host
		 *            the host name/IP
		 * @param port
		 *            the port on the host
		 * @param localAddress
		 *            the local host name/IP to bind the socket to
		 * @param localPort
		 *            the port on the local machine
		 * @param params
		 *            {@link HttpConnectionParams Http connection parameters}
		 * 
		 * @return Socket a new socket
		 * 
		 * @throws IOException
		 *             if an I/O error occurs while creating the socket
		 * @throws UnknownHostException
		 *             if the IP address of the host cannot be
		 *             determined
		 * 
		 * @since 3.0
		 */
		public Socket createSocket(
				final String host,
				final int port,
				final InetAddress localAddress,
				final int localPort,
				final HttpConnectionParams params
				) throws IOException, UnknownHostException, ConnectTimeoutException {
			if(params == null) {
				throw new IllegalArgumentException("Parameters may not be null");
			}
			int timeout = params.getConnectionTimeout();
			if(timeout == 0)
				return createSocket(host, port, localAddress, localPort);
			Socket socket = factory.createSocket();
			socket.bind(new InetSocketAddress(localAddress, localPort));
			SocketAddress socketAddress = new InetSocketAddress(host, port);
			socket.connect(socketAddress, timeout);
			return socket;
		}

		/**
		 * @see SecureProtocolSocketFactory#createSocket(java.lang.String,int)
		 */
		public Socket createSocket(String host, int port)
				throws IOException, UnknownHostException {
			return factory.createSocket(
					host,
					port
					);
		}

		/**
		 * @see SecureProtocolSocketFactory#createSocket(java.net.Socket,java.lang.String,int,boolean)
		 */
		public Socket createSocket(
				Socket socket,
				String host,
				int port,
				boolean autoClose)
				throws IOException, UnknownHostException {
			return factory.createSocket(
					socket,
					host,
					port,
					autoClose
					);
		}

		/**
		 * All instances of SSLProtocolSocketFactory are the same.
		 */
		public boolean equals(Object obj) {
			return ((obj != null) && obj.getClass().equals(getClass()));
		}

		/**
		 * All instances of SSLProtocolSocketFactory have the same hash code.
		 */
		public int hashCode() {
			return getClass().hashCode();
		}

	}

	public static abstract class KeyStoreInfo {
		protected final KeyStore keyStore;
		protected final Map<String, SortedSet<String>> aliasesByApp = new HashMap<String, SortedSet<String>>();
	
		protected KeyStoreInfo(KeyStore keyStore) throws KeyStoreException {
			super();
			this.keyStore = keyStore;
			for(Enumeration<String> en = keyStore.aliases(); en.hasMoreElements();) {
				String alias = en.nextElement();
				Matcher matcher = aliasPattern.matcher(alias);
				if(matcher.matches()) {
					String appName = matcher.group(1);
					SortedSet<String> ss = aliasesByApp.get(appName);
					if(ss == null) {
						ss = new TreeSet<>();
						aliasesByApp.put(appName, ss);
					}
					ss.add(alias);
				}
			}
		}

		public Set<String> getAppNames() {
			return Collections.unmodifiableSet(aliasesByApp.keySet());
		}

		public SortedSet<String> getAliases(String appName) {
			SortedSet<String> ss = aliasesByApp.get(appName);
			if(ss == null || ss.isEmpty())
				return null;
			return Collections.unmodifiableSortedSet(ss);
		}

		public String getLastAliasSuffix(String appName) {
			SortedSet<String> ss = aliasesByApp.get(appName);
			if(ss == null || ss.isEmpty())
				return null;
			Matcher matcher = aliasPattern.matcher(ss.last());
			if(matcher.matches())
				return matcher.group(2);
			return null;
		}

		public String getLastAlias(String appName) {
			SortedSet<String> ss = aliasesByApp.get(appName);
			if(ss == null || ss.isEmpty())
				return null;
			return ss.last();
		}

		public X509Certificate getLastCertificate(String appName) throws KeyStoreException {
			String alias = getLastAlias(appName);
			if(alias == null)
				return null;
			Certificate cert = keyStore.getCertificate(alias);
			if(cert instanceof X509Certificate)
				return (X509Certificate) cert;
			return null;
		}

		public abstract KeyStore getWritableKeyStore() throws IOException, GeneralSecurityException;

		public KeyStore getReadableKeyStore() {
			return keyStore;
		}

		public abstract char[] getPassword();
	}

	public static class PreLoadKeyStoreInfo extends KeyStoreInfo {
		protected final char[] password;
		protected final Map<String, String> lastAliasSuffix = new HashMap<String, String>();

		public PreLoadKeyStoreInfo(KeyStore keyStore, char[] password) throws KeyStoreException {
			super(keyStore);
			this.password = password;
		}

		public KeyStore getWritableKeyStore() {
			return keyStore;
		}

		public char[] getPassword() {
			return password;
		}
	}

	protected static KeyStore loadReadOnlyKeyStore(InputStream keyStoreStream) throws KeyStoreException, NoSuchProviderException, NoSuchAlgorithmException, CertificateException, IOException {
		keyStoreStream.mark(Integer.MAX_VALUE);
		return SecurityUtils.getKeyStore(keyStoreStream, "JKS", null, null);
	}
	public static class LazyLoadKeyStoreInfo extends KeyStoreInfo {
		protected KeyStore writableKeyStore;
		protected char[] password;
		protected InputStream keyStoreStream;
		protected final Host targetHost;
		protected final String targetKeystorePath;
		protected final String passwordKey;
		protected final Interaction interaction;
		protected final PasswordCache passwordCache;

		public LazyLoadKeyStoreInfo(InputStream keyStoreStream, Host targetHost, String targetKeystorePath, String passwordKey, Interaction interaction, PasswordCache passwordCache) throws KeyStoreException, NoSuchProviderException, NoSuchAlgorithmException, CertificateException, IOException {
			super(loadReadOnlyKeyStore(keyStoreStream));
			this.keyStoreStream = keyStoreStream;
			this.targetHost = targetHost;
			this.targetKeystorePath = targetKeystorePath;
			this.passwordKey = passwordKey;
			this.interaction = interaction;
			this.passwordCache = passwordCache;
		}

		public KeyStore getWritableKeyStore() throws IOException, KeyStoreException, NoSuchProviderException, NoSuchAlgorithmException, CertificateException {
			if(writableKeyStore == null) {
				getPassword();
				if(keyStoreStream != null)
					keyStoreStream.reset();
				writableKeyStore = SecurityUtils.getKeyStore(keyStoreStream, "JKS", null, password);
				if(keyStoreStream != null)
					keyStoreStream.close();
				keyStoreStream = null;
			}
			return writableKeyStore;
		}

		public char[] getPassword() {
			if(password == null)
				password = DeployUtils.getPassword(passwordCache, targetHost, passwordKey, "the " + passwordKey + " at '" + targetKeystorePath + "'", interaction);
			return password;
		}
	}
	/**
	 * 
	 * 
	 * @param certificateChain
	 * @param privateKey
	 * @param interaction
	 * @param urls
	 * @throws SecurityException
	 * @throws NoSuchFieldException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws InstantiationException
	 * @throws NoSuchMethodException
	 * @throws KeyManagementException
	 */
	public static boolean[] attemptHttps(X509Certificate[] certificateChain, PrivateKey privateKey, Interaction interaction, String... urls) throws Exception {
		HttpClient httpClient = new HttpClient();
		Protocol httpsProtocol = new Protocol("https", (ProtocolSocketFactory) new SelectedSSLProtocolSocketFactory(new OneCertChainKeyManager(certificateChain, privateKey), null), 443);
		HostConfiguration hostConfig = new HostConfiguration();
		boolean[] ret = new boolean[urls.length];
		for(int i = 0; i < urls.length; i++) {
			interaction.printf("Connecting to '%1$s'...", urls[i]);
			try {
				URI uri = new URI(urls[i]);
				if("https".equals(uri.getScheme())) {
					hostConfig.setHost(uri.getHost(), uri.getPort(), httpsProtocol);
					uri = new URI(null, null, uri.getPath(), uri.getQuery(), uri.getFragment());
				}

				httpClient.executeMethod(hostConfig, new PostMethod(uri.toString()));
				interaction.printf("Connected successfully to '%1$s'", urls[i]);
				ret[i] = true;
			} catch(HttpException e) {
				interaction.printf("Could not connect to '%1$s' because: %2$s", urls[i], e.getMessage());
				log.warn("Could not connect to " + urls[i], e);
			} catch(IOException e) {
				interaction.printf("Could not connect to '%1$s' because: %2$s", urls[i], e.getMessage());
				log.warn("Could not connect to " + urls[i], e);
			} catch(URISyntaxException e) {
				interaction.printf("Could not connect to '%1$s' because: %2$s", urls[i], e.getMessage());
				log.warn("Could not connect to " + urls[i], e);
			}
		}
		return ret;
	}

	public static DeployUtils.KeyStoreInfo createKeyStoreInfo(Host targetHost, String targetKeystorePath, String passwordKey, Interaction interaction, String user, SshClientCache sshClientCache, PasswordCache passwordCache) throws IOException, KeyStoreException, NoSuchAlgorithmException, NoSuchProviderException,
			CertificateException, InterruptedException {
		try {
			log.info("Updating keystore '" + targetKeystorePath + "' on " + targetHost.getSimpleName() + "...");
			return createKeyStoreInfo(sshClientCache, targetHost, targetKeystorePath, passwordKey, interaction, user, passwordCache);
		} catch(IOException e) {
			interaction.printf("Error while updating keystore '%1$s' on %2$s because: %3$s", targetKeystorePath, targetHost.getSimpleName(), e.getMessage());
			throw new IOException("Could not update keystore '" + targetKeystorePath + "' on " + targetHost.getSimpleName(), e);
		} catch(InterruptedException e) {
			interaction.printf("Interrupted while updating keystore '%1$s' on %2$s", targetKeystorePath, targetHost.getSimpleName());
			throw e;
		}
	}

	public static DeployUtils.KeyStoreInfo createKeyStoreInfo(SshClientCache sshClientCache, Host targetHost, String targetKeystorePath, String passwordKey, Interaction interaction, String user, PasswordCache passwordCache) throws IOException, KeyStoreException, NoSuchAlgorithmException, NoSuchProviderException,
			CertificateException, InterruptedException {
		log.info("Retrieving " + passwordKey + " '" + targetKeystorePath + "' on " + targetHost.getSimpleName() + "...");
		try {
			// Get the keystore stream
			HeapBufferStream bufferStream = new HeapBufferStream(true);
			InputStream in;
			if(UploadTask.checkFileUsingCmd(targetHost, passwordCache, interaction, sshClientCache, user, targetKeystorePath)) {
				interaction.printf("Retrieving contents of " + passwordKey + " at '%1$s'", targetKeystorePath);
				UploadTask.readFileUsingTmp(targetHost, passwordCache, interaction, sshClientCache, user, targetKeystorePath, bufferStream.getOutputStream(), null);
				in = bufferStream.getInputStream();
			} else {
				in = null;
			}
			// Decode with password
			return new DeployUtils.LazyLoadKeyStoreInfo(in, targetHost, targetKeystorePath, passwordKey, interaction, passwordCache);
		} catch(IOException e) {
			interaction.printf("Error while updating " + passwordKey + " '%1$s' on %2$s because: %3$s", targetKeystorePath, targetHost.getSimpleName(), e.getMessage());
			throw new IOException("Could not update " + passwordKey + " '" + targetKeystorePath + "' on " + targetHost.getSimpleName(), e);
		} catch(InterruptedException e) {
			interaction.printf("Interrupted while updating " + passwordKey + " '%1$s' on %2$s", targetKeystorePath, targetHost.getSimpleName());
			throw e;
		}
	}

	public static String getMakeDir(String dir, String owner, String group, Integer permissions) {
		StringBuilder sb = new StringBuilder();
		sb.append("test -d ").append(dir).append(" || (mkdir ");
		if(permissions != null && permissions > -1)
			sb.append("-m ").append(Integer.toOctalString(permissions)).append(' ');
		sb.append(dir).append(" && chown ").append(owner).append(':').append(group).append(' ').append(dir).append(')');
		return sb.toString();
	}

	public static String getMoveIfDifferentCommand(String sourceName, String targetName, String targetDir) {
		return "if diff -b /home/`logname`/" + sourceName + " " + targetDir + "/" + targetName + "; then /bin/rm /home/`logname`/" + sourceName + "; else /bin/mv /home/`logname`/" + sourceName + " " + targetDir + "/" + targetName + "; fi";
	}

	public static final Host CVS_HOST = new HostImpl("LOCAL", "CVS", "cvs.usatech.com");
	protected static final String CVS_ROOT = "/usr/local/cvsroot/NetworkServices";
	protected static final char NEWLINE = '\n';

	public static String sendCvsCommand(Interaction interaction, SshClientCache sshClientCache, String user, String localDirectory, String command, String... arguments) throws IOException {
		interaction.getWriter().println("Executing cvs command '" + command + "' with arguments:\n\t" + StringUtils.join(arguments, " "));
		interaction.getWriter().flush();

		SshClient ssh = sshClientCache.getSshClient(CVS_HOST, user);
		SessionChannelClient sshSession = ssh.openSessionChannel();
		try {
			if(!sshSession.executeCommand("cvs server"))
				throw new IOException("Could not start cvs server on remote host");
			InputStream in = new DataInputStream(sshSession.getInputStream());
			OutputStream out = new DataOutputStream(sshSession.getOutputStream());
			Writer writer = new OutputStreamWriter(out);
			writer.append("Root ").append(CVS_ROOT).append(NEWLINE);
			if(arguments != null)
				for(String a : arguments)
					writer.append("Argument ").append(a).append(NEWLINE);
			if(!StringUtils.isBlank(localDirectory))
				writer.append("Directory ").append(localDirectory).append(NEWLINE).append(CVS_ROOT).append(NEWLINE);
			writer.append(command).append(NEWLINE);
			writer.flush();

			StringBuilder message = new StringBuilder();
			String line;
			while((line = readLine(in)) != null) {
				interaction.getWriter().print("< ");
				interaction.getWriter().println(line);
				interaction.getWriter().flush();
				if("ok".equals(line)) {
					return "ok";
				} else if(line.startsWith("error")) {
					throw new IOException("Could not execute cvs " + command + " because: " + message.toString());
				} else if(line.startsWith("M ") || line.startsWith("E ")) {
					message.append(StringUtils.substringAfter(line, " "));
				}
			}
			return message.toString();
		} finally {
			sshSession.close();
		}
	}

	protected static String readLine(InputStream in) throws IOException {
		StringBuilder line = new StringBuilder(512);
		while(true) {
			int b = in.read();
			if(b == -1) {
				if(line.length() == 0)
					return null;
				return line.toString();
			}
			char ch = (char) b;
			if(ch == NEWLINE)
				return line.toString();
			line.append(ch);
		}
	}

}
