/**
 * 
 */
package com.usatech.tools.deploy;

import java.io.File;
import java.io.IOException;

import simple.app.Processor;


public class UploadAndExecuteTask extends UploadTask {
	protected final String executeDir;
	protected final Processor<String, String> interactiveInput;
	protected final long idleTime;
	public UploadAndExecuteTask(File file) throws IOException {
		this(file, null, 2000, null);
	}
	public UploadAndExecuteTask(File file, String executeDir) throws IOException {
		this(file, executeDir, 2000, null);
	}
	public UploadAndExecuteTask(File file, String executeDir, long idleTime, Processor<String, String> interactiveInput) throws IOException {
		super(file, 0744, null, null);
		this.executeDir = executeDir;
		this.interactiveInput = interactiveInput;
		this.idleTime = idleTime;
		this.privileged = false;
	}

	@Override
	public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
		String ret = super.perform(deployTaskInfo);
		int offset = (executeDir != null && !executeDir.trim().isEmpty() ? 2 : 1);
		String[] commands = new String[remoteFilePaths.size() + offset];
		commands[0] = "sudo su";
		if(executeDir != null && !executeDir.trim().isEmpty())
			commands[1] = "cd " + executeDir;
		StringBuilder sb = new StringBuilder();
		for(String path : remoteFilePaths) {
			sb.setLength(0);
			sb.append(deployTaskInfo.sshClientCache.getSftpClient(deployTaskInfo.host, deployTaskInfo.user).pwd()).append('/').append(path);
			commands[offset++] = sb.toString();
		}
		ExecuteTask.executeCommands(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.getSsh(), idleTime, interactiveInput, commands);
		return ret + " and executed " + commands.length + " commands";
	}	
}