package com.usatech.tools.deploy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.lang.SystemUtils;
import simple.text.StringUtils;
import simple.util.CollectionUtils;


public class IPTablesUpdateTask implements DeployTask {
	private final static Log log = Log.getLog();
	protected static final Pattern rulePattern = Pattern.compile("(\\S+)\\s+(\\S+)\\s+(\\S+)\\s+(\\S+)\\s+(\\S+)(?:\\s+tcp(?:(?:\\s+\\S+)*\\s+spt:(\\d+))?(?:(?:\\s+\\S+)*\\s+dpt:(\\d+))?)?(?:\\s+(.+))??(?:\\s+state\\s+(\\S+))?\\s*");
	// 1 = target, 2 = protocol, 3 = ?, 4 = source ip, 5 = dest ip, 6 = src port, 7 = dest port, 8 = other, 9 = states
	protected static final Pattern ipPattern = Pattern.compile("(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})(?:/(\\d{1,2}))?");
	protected static final Pattern redirPattern = Pattern.compile("redir\\s+ports\\s+(\\d+)");
	protected static enum Direction {
		SERVER, CLIENT, SERVER_IN, SERVER_OUT, CLIENT_IN, CLIENT_OUT
	};

	protected static enum ChainType {
		INPUT, OUTPUT, PREROUTING
	};
	protected final String routingChainName;
	protected final String inputChainName;
	protected final String outputChainName;
	protected final Map<String, Rule> rules = new LinkedHashMap<String, Rule>();
	protected static final char SEPARATOR = '|';

	protected interface Rule {
		public String getIdentifier();

		public String getAppendCommand();

		public String getRemoveCommand();
	}

	protected class FilterRule implements Rule {
		protected Direction direction;
		public final String protocol;
		public final String remoteIp;
		public final Integer port;
		public final String target;
		protected boolean passive;
		protected final int hashCode;
		protected final String identifier;

		public FilterRule(Direction direction, String protocol, String remoteIp, Integer port, String target) {
			this(direction, protocol, remoteIp, port, target, false);
		}

		public FilterRule(Direction direction, String protocol, String remoteIp, Integer port, String target, boolean passive) {
			super();
			this.direction = direction;
			this.protocol = protocol;
			this.remoteIp = remoteIp;
			this.port = port;
			this.target = target;
			this.passive = passive;
			this.hashCode = SystemUtils.addHashCodes(0, protocol, remoteIp, port);
			this.identifier = buildIdentifier(direction, protocol, remoteIp, port);
		}

		@Override
		public int hashCode() {
			return hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if(obj == this)
				return true;
			if(!(obj instanceof FilterRule) || obj.hashCode() != hashCode)
				return false;
			FilterRule r = (FilterRule) obj;
			return ConvertUtils.areEqual(r.port, port) && ConvertUtils.areEqual(r.direction, direction)
					&& ConvertUtils.areEqual(r.protocol, protocol) && ConvertUtils.areEqual(r.remoteIp, remoteIp)
					&& ConvertUtils.areEqual(r.target, target) && ConvertUtils.areEqual(r.passive, passive);
		}

		public String getIdentifier() {
			return identifier;
		}

		public String getAppendCommand() {
			return buildCommand('A');
		}

		public String getRemoveCommand() {
			return buildCommand('D');
		}

		protected String buildCommand(char action) {
			StringBuilder sb = new StringBuilder();
			switch(direction) {
				case SERVER_IN:
					appendCommand(sb, action, null, inputChainName, protocol, remoteIp, null, null, port, "NEW,ESTABLISHED", null, target);
					break;
				case SERVER_OUT:
					appendCommand(sb, action, null, outputChainName, protocol, null, remoteIp, port, null, "ESTABLISHED", null, target);
					break;
				case SERVER:
					appendCommand(sb, action, null, inputChainName, protocol, remoteIp, null, null, port, "NEW,ESTABLISHED", null, target);
					appendCommand(sb, action, null, outputChainName, protocol, null, remoteIp, port, null, "ESTABLISHED", null, target);
					if(passive) {
						appendCommand(sb, action, null, inputChainName, protocol, remoteIp, null, "1024:", "1024:", "ESTABLISHED,RELATED", null, target);
						appendCommand(sb, action, null, inputChainName, protocol, null, remoteIp, "1024:", "1024:", "ESTABLISHED", null, target);
					}
					break;
				case CLIENT_IN:
					appendCommand(sb, action, null, inputChainName, protocol, remoteIp, null, port, null, "ESTABLISHED", null, target);
					break;
				case CLIENT_OUT:
					appendCommand(sb, action, null, outputChainName, protocol, null, remoteIp, null, port, "NEW,ESTABLISHED", null, target);
					break;
				case CLIENT:
					appendCommand(sb, action, null, inputChainName, protocol, remoteIp, null, port, null, "ESTABLISHED", null, target);
					appendCommand(sb, action, null, outputChainName, protocol, null, remoteIp, null, port, "NEW,ESTABLISHED", null, target);
					if(passive) {
						appendCommand(sb, action, null, inputChainName, protocol, remoteIp, null, "1024:", "1024:", "ESTABLISHED", null, target);
						appendCommand(sb, action, null, inputChainName, protocol, null, remoteIp, "1024:", "1024:", "ESTABLISHED,RELATED", null, target);
					}
					break;
			}
			return sb.toString();
		}

		public Direction getDirection() {
			return direction;
		}

		public void setDirection(Direction direction) {
			this.direction = direction;
		}

		public boolean isPassive() {
			return passive;
		}

		public void setPassive(boolean passive) {
			this.passive = passive;
		}
	}

	protected class RoutingRule implements Rule {
		public final String protocol;
		public final String sourceIp;
		public final String destIp;
		public final Integer sourcePort;
		public final Integer destPort;
		public final String target;
		public final String match;
		public final String extra;
		protected final int hashCode;
		protected final String identifier;

		public RoutingRule(String protocol, String sourceIp, Integer sourcePort, String destIp, Integer destPort, String target, String match, String extra) {
			super();
			this.protocol = protocol;
			this.sourceIp = sourceIp;
			this.sourcePort = sourcePort;
			this.destIp = destIp;
			this.destPort = destPort;
			this.target = target;
			this.match = match;
			this.extra = extra;
			this.hashCode = SystemUtils.addHashCodes(0, protocol, sourceIp, sourcePort, destIp, destPort, match);
			this.identifier = buildIdentifier(protocol, sourceIp, sourcePort, destIp, destPort, match);
		}

		@Override
		public int hashCode() {
			return hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if(obj == this)
				return true;
			if(!(obj instanceof RoutingRule) || obj.hashCode() != hashCode)
				return false;
			RoutingRule r = (RoutingRule) obj;
			return ConvertUtils.areEqual(r.sourcePort, sourcePort) && ConvertUtils.areEqual(r.destPort, destPort) && ConvertUtils.areEqual(r.protocol, protocol) && ConvertUtils.areEqual(r.sourceIp, sourceIp) && ConvertUtils.areEqual(r.destIp, destIp) && ConvertUtils.areEqual(r.target, target) && ConvertUtils.areEqual(r.match, match) && ConvertUtils.areEqual(r.extra, extra);
		}

		public String getIdentifier() {
			return identifier;
		}

		public String getAppendCommand() {
			return buildCommand('A');
		}

		public String getRemoveCommand() {
			return buildCommand('D');
		}

		protected String buildCommand(char action) {
			StringBuilder sb = new StringBuilder();
			appendCommand(sb, action, "nat", routingChainName, protocol, sourceIp, destIp, sourcePort, destPort, null, match, target);
			if(extra != null)
				sb.append(' ').append(extra);
			return sb.toString();
		}
	}

	protected static StringBuilder appendCommand(StringBuilder sb, char action, String tableName, String chainName, String protocol, String sourceIp, String destIp, Integer sourcePort, Integer destPort, String states, String match, String target) {
		return appendCommand(sb, action, tableName, chainName, protocol, sourceIp, destIp, sourcePort == null ? null : sourcePort.toString(), destPort == null ? null : destPort.toString(), states, match, target);
	}

	protected static StringBuilder appendCommand(StringBuilder sb, char action, String tableName, String chainName, String protocol, String sourceIp, String destIp, String sourcePort, String destPort, String states, String match, String target) {
		if(sb.length() > 0)
			sb.append("; ");
		sb.append("iptables ");
		if(tableName != null && (tableName = tableName.trim()).length() > 0)
			sb.append("-t ").append(tableName).append(' ');
		sb.append('-').append(action).append(' ').append(chainName);
		if(protocol != null)
			sb.append(" -p ").append(protocol);
		if(sourceIp != null)
			sb.append(" -s ").append(sourceIp);
		if(destIp != null)
			sb.append(" -d ").append(destIp);
		if(sourcePort != null)
			sb.append(" --sport ").append(sourcePort);
		if(destPort != null)
			sb.append(" --dport ").append(destPort);
		if(states != null)
			sb.append(" -m state --state ").append(states);
		if(match != null)
			sb.append(" -m string --string '").append(match).append("'");
		sb.append(" -j ").append(target);
		return sb;
	}

	protected static String buildIdentifier(Direction direction, String protocol, String remoteIp, Integer port) {
		StringBuffer sb = new StringBuffer();
		switch(direction) {
			case CLIENT:
			case CLIENT_IN:
			case CLIENT_OUT:
				sb.append("client");
				break;
			case SERVER:
			case SERVER_IN:
			case SERVER_OUT:
				sb.append("server");
				break;
		}
		sb.append(SEPARATOR);
		if(protocol != null)
			sb.append(protocol);
		sb.append(SEPARATOR);
		if(remoteIp != null)
			sb.append(remoteIp);
		sb.append(SEPARATOR);
		if(port != null)
			sb.append(port);
		return sb.toString();
	}

	protected static String buildIdentifier(String protocol, String sourceIp, Integer sourcePort, String destIp, Integer destPort, String match) {
		StringBuffer sb = new StringBuffer();
		sb.append("raw");
		sb.append(SEPARATOR);
		if(protocol != null)
			sb.append(protocol);
		sb.append(SEPARATOR);
		if(sourceIp != null)
			sb.append(sourceIp);
		sb.append(SEPARATOR);
		if(sourcePort != null)
			sb.append(sourcePort);
		sb.append(SEPARATOR);
		if(destIp != null)
			sb.append(destIp);
		sb.append(SEPARATOR);
		if(destPort != null)
			sb.append(destPort);
		sb.append(SEPARATOR);
		if(match != null)
			sb.append(match);
		return sb.toString();
	}
	protected final static String[] LOCALHOST_IPS = new String[] { null };
	protected static String[] normalizeIp(String remoteHost) throws UnknownHostException {
		if(remoteHost == null)
			return LOCALHOST_IPS;
		String ip;
		Matcher matcher = ipPattern.matcher(remoteHost);
		if(matcher.matches()) {
			String range = matcher.group(2);
			if(range == null || range.length() == 0 || range.equals("32")) {
				ip = matcher.group(1);
				if(ip == null || ip.equals("0.0.0.0") || ip.equals("::0") || ip.equals("127.0.0.1"))
					return new String[] { null };
				return new String[] { ip };
			} else if(range.equals("0"))
				return LOCALHOST_IPS;
			else
				return new String[] { remoteHost };
		} else if(remoteHost.equals("localhost") || remoteHost.equals(""))
			return LOCALHOST_IPS;
		InetAddress[] addresses = InetAddress.getAllByName(remoteHost);
		String[] ips = new String[addresses.length];
		for(int i = 0; i < addresses.length; i++) {
			ips[i] = addresses[i].getHostAddress();
		}
		return ips;
	}

	public IPTablesUpdateTask() {
		this("INPUT", "OUTPUT", "PREROUTING");
	}

	public IPTablesUpdateTask(String inputChainName, String outputChainName, String routingChainName) {
		this.inputChainName = inputChainName;
		this.outputChainName = outputChainName;
		this.routingChainName = routingChainName;
	}

	public void prerouteTcp(String sourceHost, Integer sourcePort, String destHost, Integer destPort, String match, Integer toPort) throws IOException {
		addRoutingRule(rules, "tcp", sourceHost, sourcePort, destHost, destPort, match, "REDIRECT", toPort == null ? null : "--to-ports " + toPort);
	}

	public void allowTcpOutAny() throws IOException {
		addFilterRule(rules, Direction.CLIENT, "tcp", null, null, "ACCEPT", false);
	}

	public void allowUdpOutAny() throws IOException {
		addFilterRule(rules, Direction.CLIENT, "udp", null, null, "ACCEPT", false);
	}

	public void allowTcpOut(String remoteHost, int port) throws IOException {
		addFilterRule(rules, Direction.CLIENT, "tcp", remoteHost, port, "ACCEPT", false);
	}

	public void allowTcpOutWithPassive(String remoteHost, int port) throws IOException {
		addFilterRule(rules, Direction.CLIENT, "tcp", remoteHost, port, "ACCEPT", true);
	}

	public void allowTcpOutWithRelated(String remoteHost, int port, int relatedPort) throws IOException {
		allowTcpOutWithRelated(remoteHost, port, new String[] { remoteHost }, relatedPort);
	}

	public void allowTcpOutWithRelated(String remoteHost, int port, String[] relatedHosts, int relatedPort) throws IOException {
		addFilterRule(rules, Direction.CLIENT, "tcp", remoteHost, port, "ACCEPT", false);
		for(String relatedHost : relatedHosts)
			addFilterRule(rules, Direction.CLIENT_IN, "tcp", relatedHost, relatedPort, "ACCEPT", false);
	}

	public void allowTcpIn(String remoteHost, int port) throws IOException {
		addFilterRule(rules, Direction.SERVER, "tcp", remoteHost, port, "ACCEPT", false);
	}

	protected void addRoutingRule(Map<String, Rule> rules, String protocol, String sourceHost, Integer sourcePort, String destHost, Integer destPort, String match, String target, String extra) throws IOException {
		if("ALL".equalsIgnoreCase(protocol))
			protocol = null;
		for(String sourceIp : (sourceHost == null ? new String[1] : normalizeIp(sourceHost))) {
			for(String destIp : (destHost == null ? new String[1] : normalizeIp(destHost))) {
				RoutingRule rule = new RoutingRule(protocol, sourceIp, sourcePort, destIp, destPort, target, match, extra);
				rules.put(rule.getIdentifier(), rule);
			}
		}
	}
	protected void addFilterRule(Map<String, Rule> rules, Direction direction, String protocol, String remoteHost, Integer port, String target, boolean passive) throws IOException {
		if("ALL".equalsIgnoreCase(protocol))
			protocol = null;
		for(String ip : remoteHost == null ? new String[1] : normalizeIp(remoteHost)) {
			String identifier = buildIdentifier(direction, protocol, ip, port);
			FilterRule rule = (FilterRule) rules.get(identifier);
			if(rule == null)
				rules.put(identifier, new FilterRule(direction, protocol, ip, port, target, passive));
			else {
				switch(direction) {
					case SERVER:
						switch(rule.getDirection()) {
							case SERVER:
								if(passive)
									rule.setPassive(passive);
								break;
							case SERVER_IN:
							case SERVER_OUT:
								rule.setDirection(direction);
								if(passive)
									rule.setPassive(passive);
								break;
							default:
								throw new IOException("Client Rule(s) already exist for port " + port + "; Does not make sense to have Server rule(s) on this port as well");
						}
						break;
					case CLIENT:
						switch(rule.getDirection()) {
							case CLIENT:
								if(passive)
									rule.setPassive(passive);
								break;
							case CLIENT_IN:
							case CLIENT_OUT:
								rule.setDirection(direction);
								if(passive)
									rule.setPassive(passive);
								break;
							default:
								throw new IOException("Server Rule(s) already exist for port " + port + "; Does not make sense to have Client rule(s) on this port as well");
						}
						break;
					case SERVER_IN:
						switch(rule.getDirection()) {
							case SERVER:
							case SERVER_IN:
								if(passive)
									rule.setPassive(passive);
								break;
							case SERVER_OUT:
								rule.setDirection(Direction.SERVER);
								if(passive)
									rule.setPassive(passive);
								break;
							default:
								throw new IOException("Client Rule(s) already exist for port " + port + "; Does not make sense to have Server rule(s) on this port as well");
						}
						break;
					case SERVER_OUT:
						switch(rule.getDirection()) {
							case SERVER:
							case SERVER_OUT:
								if(passive)
									rule.setPassive(passive);
								break;
							case SERVER_IN:
								rule.setDirection(Direction.SERVER);
								if(passive)
									rule.setPassive(passive);
								break;
							default:
								throw new IOException("Client Rule(s) already exist for port " + port + "; Does not make sense to have Server rule(s) on this port as well");
						}
						break;
					case CLIENT_IN:
						switch(rule.getDirection()) {
							case CLIENT:
							case CLIENT_IN:
								if(passive)
									rule.setPassive(passive);
								break;
							case CLIENT_OUT:
								rule.setDirection(Direction.CLIENT);
								if(passive)
									rule.setPassive(passive);
								break;
							default:
								throw new IOException("Server Rule(s) already exist for port " + port + "; Does not make sense to have Client rule(s) on this port as well");
						}
						break;
					case CLIENT_OUT:
						switch(rule.getDirection()) {
							case CLIENT:
							case CLIENT_OUT:
								if(passive)
									rule.setPassive(passive);
								break;
							case CLIENT_IN:
								rule.setDirection(Direction.CLIENT);
								if(passive)
									rule.setPassive(passive);
								break;
							default:
								throw new IOException("Server Rule(s) already exist for port " + port + "; Does not make sense to have Client rule(s) on this port as well");
						}
						break;
				}
			}

		}
	}

	@Override
	public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
		List<ChainType> chainTypes = new ArrayList<ChainType>();
		List<String> readCommands = new ArrayList<String>();
		if(inputChainName != null) {
			readCommands.add("sudo iptables -n -L " + inputChainName);
			chainTypes.add(ChainType.INPUT);
		}
		if(outputChainName != null) {
			readCommands.add("sudo iptables -n -L " + outputChainName);
			chainTypes.add(ChainType.OUTPUT);
		}
		if(routingChainName != null) {
			readCommands.add("sudo iptables -t nat -n -L " + routingChainName);
			chainTypes.add(ChainType.PREROUTING);
		}

		Collection<String> results = ExecuteTask.executeCommands(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.getSsh(), readCommands);
		Map<String, Rule> existing = new LinkedHashMap<String, Rule>();
		int i = 0;
		for(String result : results) {
			parseRules(existing, result, chainTypes.get(i++));
		}
		List<String> commands = new ArrayList<String>(rules.size() + 5);
		commands.add("sudo su");
		commands.add("iptables-save > /etc/sysconfig/iptables.`date '+%m%d%y%H%M%S'`");
		boolean added = false;
		for(Map.Entry<String, Rule> entry : rules.entrySet()) {
			Rule existingRule = existing.get(entry.getKey());
			if(existingRule == null) {
				commands.add(entry.getValue().getAppendCommand());
				added = true;
			} else if(!existingRule.equals(entry.getValue())) {
				commands.add(existingRule.getRemoveCommand());
				commands.add(entry.getValue().getAppendCommand());
				added = true;
			}
		}
		if(added) {
			commands.add("service iptables save");
			ExecuteTask.executeCommands(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.getSsh(), commands);
			return "Updated IPTables with " + (commands.size() - 2) + " new rules";
		}
		deployTaskInfo.interaction.printf("IPTables is up to date. Not changing");
		return "IPTables is up to date";
	}

	protected void parseRules(Map<String, Rule> rules, String content, ChainType chainType) throws UnknownHostException, IOException {
		BufferedReader reader = new BufferedReader(new StringReader(content));
		String line;
		boolean start = false;
		while((line = reader.readLine()) != null) {
			if(!start) {
				if(line.startsWith("target "))
					start = true;
			} else {
				Matcher matcher = rulePattern.matcher(line);
				if(matcher.matches()) {
					try {
						// 1 = target, 2 = protocol, 3 = ?, 4 = source ip, 5 = dest ip, 6 = src port, 7 = dest port, 9 = states, 8 = other
						String protocol = matcher.group(2);
						Integer sourcePort = ConvertUtils.convert(Integer.class, matcher.group(6));
						Integer destPort = ConvertUtils.convert(Integer.class, matcher.group(7));
						String states = matcher.group(9);
						String[] stateArray = StringUtils.split(states, ',');
						boolean hasNewState;
						boolean hasEstState;
						if(stateArray == null) {
							hasNewState = false;
							hasEstState = false;
						} else {
							hasNewState = CollectionUtils.iterativeSearch(stateArray, "NEW") >= 0;
							hasEstState = CollectionUtils.iterativeSearch(stateArray, "ESTABLISHED") >= 0;
						}
						Integer port;
						String remoteHost;
						Direction direction;
						if(chainType == ChainType.INPUT && sourcePort == null && hasNewState && hasEstState) {
							direction = Direction.SERVER_IN;
							port = destPort;
							remoteHost = matcher.group(4);
						} else if(chainType == ChainType.OUTPUT && destPort == null && !hasNewState && hasEstState) {
							direction = Direction.SERVER_OUT;
							port = sourcePort;
							remoteHost = matcher.group(5);
						} else if(chainType == ChainType.INPUT && destPort == null && !hasNewState && hasEstState) {
							direction = Direction.CLIENT_IN;
							port = sourcePort;
							remoteHost = matcher.group(4);
						} else if(chainType == ChainType.OUTPUT && sourcePort == null && hasNewState && hasEstState) {
							direction = Direction.CLIENT_OUT;
							port = destPort;
							remoteHost = matcher.group(5);
						} else if(chainType == ChainType.PREROUTING) {
							String other = matcher.group(8);
							StringBuilder extra = new StringBuilder();
							if(other != null && (other = other.trim()).length() > 0) {
								Matcher redirMatcher = redirPattern.matcher(other);
								if(redirMatcher.find()) {
									extra.append(" --to-ports ").append(redirMatcher.group(1));
								}
							}
							addRoutingRule(rules, protocol, matcher.group(4), sourcePort, matcher.group(5), destPort, null, matcher.group(1), extra.length() > 0 ? extra.toString().trim() : null);
							continue;
						} else {
							log.info("Could not interpret iptables rule '" + line + "' because it doesn't match any known direction");
							continue;
						}

						String target = matcher.group(1);
						StringBuilder sb = new StringBuilder();
						sb.append('^').append(target).append("\\s+").append(protocol).append("\\s+(\\S+)\\s+").append(matcher.group(4)).append("\\s+").append(matcher.group(5)).append("\\s+tcp\\s+spts:1024:65535\\s+dpts:1024:65535\\s+state\\s+(?:(?:ESTABLISHED,RELATED)|(?:RELATED,ESTABLISHED))\\s*");
						// ACCEPT tcp -- 0.0.0.0/0 0.0.0.0/0 tcp spts:1024:65535 dpts:1024:65535 state ESTABLISHED,RELATED
						// "(\\S+)\\s+(\\S+)\\s+(\\S+)\\s+(\\S+)\\s+(\\S+)(?:\\s+tcp(?:(?:\\s+\\S+)*\\s+spt:(\\d+))?(?:(?:\\s+\\S+)*\\s+dpt:(\\d+))?(?:\\s+\\S+)*?)?(?:\\s+state\\s+(\\S+))?\\s*"
						boolean passive = Pattern.compile(sb.toString(), Pattern.MULTILINE).matcher(content).find();
						addFilterRule(rules, direction, protocol, remoteHost, port, target, passive);
					} catch(IOException e) {
						throw new IOException("Exception while parsing rule '" + line + "'", e);
					} catch(ConvertException e) {
						throw new IOException("Exception while parsing rule '" + line + "'", e);
					}
				}
			}
		}
	}

	protected static Direction opposite(Direction direction) {
		switch(direction) {
			case SERVER:
				return Direction.CLIENT;
			case CLIENT:
				return Direction.SERVER;
		}
		return null;
	}

	public String printAddCommands() {
		StringBuilder sb = new StringBuilder();
		for(Map.Entry<String, Rule> entry : rules.entrySet()) {
			sb.append(entry.getValue().getAppendCommand()).append(SystemUtils.getNewLine());
		}
		return sb.toString();
	}

	public void clearRules() {
		this.rules.clear();
	}

	@Override
	public String getDescription(DeployTaskInfo deployTaskInfo) {
		StringBuilder sb = new StringBuilder();
		sb.append("Updating IPTables with ").append(rules.size()).append(" rules");
		return sb.toString();
	}
}
