package com.usatech.tools.deploy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import simple.io.Interaction;
import simple.text.StringUtils;

public class IPFUpdateFileTask extends AbstractUpdateFileTask {
	protected static final Pattern ipfPattern = Pattern.compile("(pass|block)\\s+(in|out)(?:\\s+(log))?(?:\\s+(quick))?(?:\\s+on\\s+(\\w+))?(?:\\s+proto\\s+(tcp|udp|icmp))?(?:\\s+from\\s+(\\S+)\\s+to\\s+(\\S+))?(?:\\s+port\\s*=\\s*(\\d+))?(?:\\s.*)?\\s*");
	protected static final int[] ipfKeyIndexes = {1, 2, 5, 6, 7, 8, 9};
	
	public IPFUpdateFileTask(boolean keepExisting) {
		super(keepExisting, 0444, "root", "root");
	}

	public void allowTcp(String remoteHost, int port) {
		registerValue("tcp." + remoteHost + "." + port, new DefaultHostSpecificValue("pass in quick proto tcp from " + remoteHost + " to any port = " + port));
	}
	public void allowTcp(final HostSpecificValue remoteHost, final int port) {
		registerValue("tcp.~DYNO~" + (valueMap.size()+1) + "." + port, new HostSpecificValue() {
			public String getValue(Host host, Interaction interaction) {
				String h = remoteHost.getValue(host, interaction);
				return h == null ? null : "pass in quick proto tcp from " + h + " to any port = " + port;
			}
			@Override
			public boolean isOverriding() {
				return true;
			}
		});
	}
	protected Map<String,Object> getValues(Map<String,Object> existingValues, Interaction interaction, Host host) {
		final Map<String,Object> values = new LinkedHashMap<String, Object>(valueMap.size());
		if(keepExisting)
			values.putAll(existingValues);
		int i = 1;
		for(Map.Entry<String, HostSpecificValue> entry : valueMap.entrySet()) {
			String line = entry.getValue().getValue(host,interaction);
			String key = calcKey(line);
			if(key == null)
				values.put("#NEW_UNPARSABLE_" + i++, line);
			else if(entry.getValue().isOverriding() || !existingValues.containsKey(key))
				values.put(key, line);
			else if(!keepExisting)
				values.put(key, existingValues.get(entry.getKey()));				
		}
		return values;
	}
	@Override
	protected void readFile(InputStream in, Map<String, Object> values) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		int i = 1;
		for(String line; (line=reader.readLine()) != null; ) {
			String key = calcKey(line);
			if(key == null)
				key = "#EXISTING_UNPARSABLE_" + i++;
			values.put(key, line);
		}
	}

	protected String calcKey(String line) {
		Matcher matcher = ipfPattern.matcher(line);
		int pos = line.indexOf('#');
		if(pos >= 0)
			matcher.region(0, pos);
		if(matcher.matches()) {
			StringBuilder sb = new StringBuilder();
			for(int index : ipfKeyIndexes)
				sb.append('|').append(matcher.group(index));
			return sb.toString();
		}
		return null;
	}
	@Override
	protected void writeFile(OutputStream out, Map<String, Object> values) throws IOException {
		for(Map.Entry<String, Object> entry : values.entrySet()) {
			if(entry.getValue() != null) {
				out.write(entry.getValue().toString().getBytes());
				out.write('\n');
			}
		}
		out.flush();
	}
	protected StringBuilder appendEscaped(StringBuilder line, CharSequence text, boolean full) {
		return appendEscaped(line, text, 0, text.length(), full);
	}
	protected StringBuilder appendEscaped(StringBuilder line, CharSequence text, int start, int end, boolean full) {
		for(int i = start; i < end; i++) {
			char ch = text.charAt(i);
			switch(ch) {
				case '=': case ':': case ' ':
					if(!full) {
						line.append(ch);
						break;
					}
					//fall-through
				case '\\': case '#': case '!':
					line.append('\\').append(ch);
					break;
				case '\t': 
					line.append("\\t"); 
					break;
				case '\n': 
					line.append("\\n"); 
					break;
				case '\f': 
					line.append("\\f"); 
					break;
				case '\r': 
					line.append("\\r"); 
					break;
                default:
                	if(ch >= ' ' && ch <= '~')
                		line.append(ch);
                	else {
                		line.append("\\u");
                		StringUtils.appendHex(line, (short)ch);
                	}
			}
		}
		return line;
	}

	protected int appendUnescaped(StringBuilder key, CharSequence line, int start, int end, boolean stopOnSeparator) {
		LOOP: for(; start < end; start++) {
			char ch = line.charAt(start);
			switch(ch) {
				case '\\': // escaped char
					if(++start < end) {
						ch = line.charAt(start);
						switch(ch) {
							case 'u':
								if(start + 4 < end) {
									start++;
				                    // Read the xxxx
				                    int value = 0;
				                    for(int i = 0; i < 4; i++) {
				                    	ch = line.charAt(start++);												
				                        switch (ch) {
				                          case '0': case '1': case '2': case '3': case '4':
				                          case '5': case '6': case '7': case '8': case '9':
				                             value = (value << 4) + ch - '0';
				                             break;
				                          case 'a': case 'b': case 'c':
				                          case 'd': case 'e': case 'f':
				                             value = (value << 4) + 10 + ch - 'a';
				                             break;
				                          case 'A': case 'B': case 'C':
				                          case 'D': case 'E': case 'F':
				                             value = (value << 4) + 10 + ch - 'A';
				                             break;
				                          default:
				                              throw new IllegalArgumentException("Malformed \\uxxxx encoding.");
				                        }
				                     }
				                     key.append((char)value);
								} else {
									key.append(line, start - 1, end);
									start = end;
								}
								break;
							case 't': 
								key.append('\t'); 
								break;
							case 'n': 
								key.append('\n'); 
								break;
							case 'f': 
								key.append('\f'); 
								break;
							case 'r': 
								key.append('\r'); 
								break;
		                    default:
		                    	key.append(ch);
		                }
					}
					break;
				case '=': case ':':
					if(stopOnSeparator)
						break LOOP;
					//fall-thru
				default:
					key.append(ch);
			}	
		}
		return start;
	}
}
