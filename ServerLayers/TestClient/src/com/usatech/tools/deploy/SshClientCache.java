package com.usatech.tools.deploy;

import java.io.IOException;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.sshtools.j2ssh.SftpClient;
import com.sshtools.j2ssh.SshClient;

public interface SshClientCache {
	public SshClient getSshClient(Host host, String user) throws IOException;
	
	public Session getJSchClient(Host host, String user) throws IOException, JSchException;

	public SftpClient getSftpClient(Host host, String user) throws IOException;

	public void shutdown();
}
