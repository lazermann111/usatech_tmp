package com.usatech.tools.deploy;

import java.io.IOException;

import simple.text.StringUtils;

import com.sshtools.j2ssh.SshClient;

public class ReinitStandbyTask implements DeployTask {
	protected final String name;
	protected final Host primaryHost;
	protected final Host standbyHost;

	public ReinitStandbyTask(String name, Host primaryHost, Host standbyHost) {
		super();
		this.name = name;
		this.primaryHost = primaryHost;
		this.standbyHost = standbyHost;
	}

	@Override
	public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
		deployTaskInfo.interaction.printf("Reinitializing %1s on %2s from %3s", name, standbyHost, primaryHost);
		// check versions
		SshClient primarySsh = deployTaskInfo.sshClientCache.getSshClient(primaryHost, deployTaskInfo.user);
		SshClient standbySsh = deployTaskInfo.sshClientCache.getSshClient(standbyHost, deployTaskInfo.user);
		String[] ret;
		ret = ExecuteTask.executeCommands(standbyHost, deployTaskInfo.passwordCache, deployTaskInfo.interaction, standbySsh, 
				"sudo su",
				"if [ `grep /opt/USAT /etc/fstab | sed -r 's/ +/ /g' | cut -d' ' -f4 | grep -c acl` -eq 0 ]; then sed -ri 's#(\\S+\\s+/opt/USAT\\s+\\S+\\s+\\S+(,\\S+)*)(\\s+)#\\1,acl\\3#' /etc/fstab; mount -o remount /opt/USAT; fi",
				"su - replica",
				"if [ ! -r ~/.ssh/id_rsa ]; then ssh-keygen -f ~/.ssh/id_rsa -q -P \"\"; fi",
				"cat ~/.ssh/id_rsa.pub");
		
		ExecuteTask.executeCommands(primaryHost, deployTaskInfo.passwordCache, deployTaskInfo.interaction, primarySsh, 
				"sudo su",
				"if [ `grep -c 'replica@" + standbyHost.getFullName() + "' /home/replica/.ssh/authorized_keys` -eq 0 ]; then mkdir /home/replica/.ssh; chmod 0700 /home/replica/.ssh; echo '" + ret[ret.length - 1] + "' >> /home/replica/.ssh/authorized_keys; chown -R replica:replica /home/replica/.ssh; fi",
				"if [ `grep /opt/USAT /etc/fstab | sed -r 's/ +/ /g' | cut -d' ' -f4 | grep -c acl` -eq 0 ]; then sed -ri 's#(\\S+\\s+/opt/USAT\\s+\\S+\\s+\\S+(,\\S+)*)(\\s+)#\\1,acl\\3#' /etc/fstab; mount -o remount /opt/USAT; fi",
				"setfacl -R -m d:u:replica:rX,u:replica:rX /opt/USAT/" + name + "/data /opt/USAT/" + name + "/tblspace");
		
		ret = ExecuteTask.executeCommands(primaryHost, deployTaskInfo.passwordCache, deployTaskInfo.interaction, primarySsh, "sudo cat /opt/USAT/" + name + "/data/PG_VERSION");
		if(StringUtils.isBlank(ret[0]))
			throw new IOException("Could not determine version of data on primary host " + primaryHost + " for " + name);
		String primaryVersion = ret[0];
		deployTaskInfo.interaction.printf("Primary data is version %1s", primaryVersion);
		ret = ExecuteTask.executeCommands(standbyHost, deployTaskInfo.passwordCache, deployTaskInfo.interaction, standbySsh, "sudo /usr/pgsql-latest/bin/pg_config --version | cut -d ' ' -f 2 | cut -d '.' -f1-2");
		if(StringUtils.isBlank(ret[0]))
			throw new IOException("Could not determine version of postgres on " + standbyHost);
		String standbyVersion = ret[0];
		deployTaskInfo.interaction.printf("Standby postgres is version %1s", standbyVersion);

		if(!primaryVersion.equalsIgnoreCase(standbyVersion))
			throw new IOException("Remote database " + name + " is version " + primaryVersion + ", but local postgres binaries are version " + standbyVersion + ". Cannot re-init standby");

		String rsyncCmd = "rsync -e 'ssh -i /home/replica/.ssh/id_rsa' -av --delete --include '/data' --include '/tblspace' --include '/data/*/' --include '/data/*/**' --include '/tblspace/**' --include '/data/PG_VERSION' --include '/data/backup_label' --exclude '/*' --exclude '/data/*' replica@" + primaryHost.getSimpleName()
				+ ":/opt/USAT/" + name + "/ /opt/USAT/" + name + "/ 2>&1 | tee /var/log/deploymgr.log";
		ExecuteTask.executeCommands(standbyHost, deployTaskInfo.passwordCache, deployTaskInfo.interaction, standbySsh, 5000, null,
				"sudo su",		
				"/usr/monit-latest/bin/monit stop " + name,
				"while [ `/usr/monit-latest/bin/monit summary | grep -c \"Process '$1'.* pending\"` -gt 0 ]; do\nsleep 1\ndone", 
				/* "if [ `ls -1 /opt/USAT/" + name + "/data/pg_xlog | grep -c '.'` -gt 0 ]; then /bin/rm -r /opt/USAT/" + name + "/data/pg_xlog/*; fi" ,*/
				rsyncCmd);
		ExecuteTask.executeCommands(primaryHost, deployTaskInfo.passwordCache, deployTaskInfo.interaction, primarySsh, 
				"sudo su - postgres",
				"/usr/pgsql-latest/bin/psql -c \"SELECT pg_start_backup('replica', true);\"",
				"setfacl -R -m d:u:replica:rX,u:replica:rX /opt/USAT/" + name + "/data /opt/USAT/" + name + "/tblspace");

		ExecuteTask.executeCommands(standbyHost, deployTaskInfo.passwordCache, deployTaskInfo.interaction, standbySsh, 5000, null,
				"sudo su", 
				rsyncCmd, 
				rsyncCmd,
				"chmod 0700 /opt/USAT/" + name + "/data",
				"chmod 0600 /opt/USAT/" + name + "/data/server.key",
				"if [ -x /usr/pgsql-latest/bin/postgres ]; then\n/usr/monit-latest/bin/monit start " + name + "\nfi");

		ExecuteTask.executeCommands(primaryHost, deployTaskInfo.passwordCache, deployTaskInfo.interaction, primarySsh, 
				"sudo su - postgres", 
				"/usr/pgsql-latest/bin/psql -c \"SELECT pg_stop_backup();\"",
				"chmod 0700 /opt/USAT/" + name + "/data",
				"chmod 0600 /opt/USAT/" + name + "/data/server.key"
				);
		return "Reinitialized " + name + " on " + standbyHost.getSimpleName() + " from " + primaryHost.getSimpleName();
	}

	@Override
	public String getDescription(DeployTaskInfo deployTaskInfo) {
		return "Reinitialize " + name + " on " + standbyHost.getSimpleName() + " from " + primaryHost.getSimpleName();
	}
}
