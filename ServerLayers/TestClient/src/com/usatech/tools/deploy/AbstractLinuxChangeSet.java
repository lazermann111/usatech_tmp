package com.usatech.tools.deploy;

import static com.usatech.tools.deploy.USATRegistry.ENERGYMISERS_WEBSITE_APP;
import static com.usatech.tools.deploy.USATRegistry.ESUDS_APP;
import static com.usatech.tools.deploy.USATRegistry.HOTCHOICE_APP;
import static com.usatech.tools.deploy.USATRegistry.KEYMGR;
import static com.usatech.tools.deploy.USATRegistry.OUTAUTH_LAYER;
import static com.usatech.tools.deploy.USATRegistry.RPTGEN_LAYER;
import static com.usatech.tools.deploy.USATRegistry.USALIVE_APP;
import static com.usatech.tools.deploy.USATRegistry.USATECH_WEBSITE_APP;
import static com.usatech.tools.deploy.USATRegistry.VERIZON_APP;
import static com.usatech.tools.deploy.USATRegistry.subHostPrefixMap;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.text.SimpleDateFormat;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import com.usatech.tools.deploy.DeployUtils.KeyStoreInfo;
import com.usatech.tools.deploy.PgDeployMap.PGDATADir;

import simple.io.BufferStream;
import simple.io.EncodingInputStream;
import simple.io.HeapBufferStream;
import simple.io.ReplacementsLineFilteringReader;
import simple.lang.SystemUtils;
import simple.security.crypt.CryptUtils;
import simple.text.StringUtils;
import simple.util.CollectionUtils;

public abstract class AbstractLinuxChangeSet implements ChangeSet {
	// private static final Log log = Log.getLog();
	protected static final Random RANDOM = new SecureRandom();
	protected static final Comparator<App> APP_COMPARATOR = new Comparator<App>() {
		public int compare(App o1, App o2) {
			if(o1 == null) {
				if(o2 == null)
					return 0;
				return -1;
			} else if(o2 == null)
				return 1;
			return SystemUtils.nvl(o1.getAppDesc(), o1.getName(), "").compareToIgnoreCase(SystemUtils.nvl(o2.getAppDesc(), o2.getName(), ""));
		}
	};
	protected final Registry registry;
	protected String releaseDir = "release";
	protected AbstractLinuxChangeSet() throws UnknownHostException {
		this(new USATRegistry());
	}

	protected AbstractLinuxChangeSet(Registry registry) {
		this.registry = registry;
		registerApps();
	}

	protected void registerApp(App app) {
		registry.registerApp(app);
	}

	protected void registerApp(App app, String version) {
		registry.registerApp(app, version);
	}
	
	protected void registerApp(App app, String version, String revision) {
		registry.registerApp(app, version);
	}


	protected abstract void registerApps();

	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		return getTasks(new DeployTaskInfo(null, host, null, cache, null));
	}

	public DeployTask[] getTasks(DeployTaskInfo deployTaskInfo) throws IOException {
		if(!isApplicable(deployTaskInfo.host))
			return null;
		registry.updateAppVersions();
		BasicServerSet serverSet = registry.getServerSet(deployTaskInfo.host.getServerEnv());
		List<DeployTask> tasks = new ArrayList<DeployTask>();
		List<String> cmds = new ArrayList<String>();
		if(shouldAddHostBasedTask(deployTaskInfo))
			addTasks(deployTaskInfo.host, tasks, cmds, deployTaskInfo.passwordCache);
		boolean checkVersions = false;
		for(String serverType : deployTaskInfo.host.getServerTypes()) {
			Set<App> apps = serverSet.getApps(serverType);
			if(apps != null) {
				List<Server> servers = serverSet.getServers(serverType);
				if(servers != null) {
					for(App app : filterApps(apps, deployTaskInfo, serverType)) {
						if(app.isStandardJavaApp())
							checkVersions = true;
						int instanceNum = 0;
						for(Iterator<Server> iter = servers.iterator(); iter.hasNext();) {
							Server server = iter.next();
							String[] instances = app.filterInstances(server.instances);
							int instanceCount = instanceNum + instances.length;
							if(server.isOnHost(deployTaskInfo.host)) {
								while(iter.hasNext())
									instanceCount += app.filterInstances(iter.next().instances).length;
								for(String instance : instances) {
									instanceNum++;
									addTasks(deployTaskInfo.host, app, instances.length > 1 && !StringUtils.isBlank(instance) && (instance = instance.trim()).matches("\\d+") ? Integer.parseInt(instance) : null, instance, instanceNum, instanceCount, tasks, cmds, deployTaskInfo.passwordCache);
								}
								break;
							}
							instanceNum = instanceCount;
						}
					}
				}
			}
		}
		if(!CollectionUtils.containsOnly(deployTaskInfo.host.getServerTypes(), "PGS", "ODB"))
			addPostAppsTasks(tasks);

		if(checkVersions) {
			// show new versions of apps
			cmds.add("grep -H '.' /opt/USAT/*/classes/version.txt | sed 's/\\([\"\\\\]\\)/\\\\\\1/g' | sed 's#/opt/USAT/\\(.*\\)/classes/version.txt:\\(.*\\)#PID=`cat /opt/USAT/\\1/logs/*.pid | tail -n 1`; printf \"%-30s %-30s %-15s %5g%% %10u\\n\" \"\\1\" \"\\2\" `if (( PID > 0 )); then ps -o etime=,%cpu=,rss= $PID; else echo \"Not_Running\"; fi`#g' | bash");
		}
		if(!cmds.isEmpty())
			tasks.add(new ExecuteTask(cmds.toArray(new String[cmds.size()])));
		addPostHostTasks(tasks);
		return tasks.toArray(new DeployTask[tasks.size()]);
	}

	protected boolean shouldAddHostBasedTask(DeployTaskInfo deployTaskInfo) {
		if(!(deployTaskInfo.interaction instanceof ExtendedInteraction))
			return true;
		int[] result = ((ExtendedInteraction) deployTaskInfo.interaction).readChoice("Execute Host-based Updates?", new String[] { "Yes", "No" }, false, new int[] { 1 });
		return (result != null && result.length > 0 && result[0] == 1);
	}

	protected Set<App> filterApps(Set<App> apps, DeployTaskInfo deployTaskInfo, String serverType) {
		if(!(deployTaskInfo.interaction instanceof ExtendedInteraction))
			return apps;

		final App[] appArray = apps.toArray(new App[apps.size()]);
		Arrays.sort(appArray, APP_COMPARATOR);
		final String[] options = new String[appArray.length];
		int[] defaultChoices = new int[appArray.length];
		for(int i = 0; i < appArray.length; i++) {
			options[i] = SystemUtils.nvl(appArray[i].getAppDesc(), appArray[i].getName());
			defaultChoices[i] = i + 1;
		}
		final int[] choices = ((ExtendedInteraction) deployTaskInfo.interaction).readChoice("Choose the apps to install for %1$s:", options, true, defaultChoices, serverType);
		return new AbstractSet<App>() {
			@Override
			public Iterator<App> iterator() {
				return new Iterator<App>() {
					protected int index = 0;

					@Override
					public boolean hasNext() {
						return index < choices.length;
					}

					@Override
					public App next() {
						return appArray[choices[index++] - 1];
					}
				};
			}

			@Override
			public int size() {
				return choices.length;
			}
		};
	}

	protected void addPostHostTasks(List<DeployTask> tasks) {
		tasks.add(new ExecuteTask("if [ -d " + getWorkDir() + " ]; then sudo /bin/rm -r "+getWorkDir()+"; fi"));
	}

	protected abstract void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException;

	protected abstract void addTasks(Host host, App app, Integer ordinal, String instance, int instanceNum, int instanceCount, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException;

	@Override
	public boolean isApplicable(Host host) {
		BasicServerSet serverSet = registry.getServerSet(host.getServerEnv());
		if(serverSet.getServers(host).isEmpty())
			return false;
		for(String serverType : host.getServerTypes()) {
			Set<App> apps = serverSet.getApps(serverType);
			if(apps != null && !apps.isEmpty())
				return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return getName();
	}

	protected void addPrepareHostTasks(Host host, List<DeployTask> tasks, PasswordCache cache) throws IOException {
		// Make group and directory
		List<String> commands = new ArrayList<String>();
		commands.add("sudo su");
		commands.add("chmod 777 /tmp");
		commands.add("grep -c 'usat:' /etc/group || groupadd -g 900 usat");
		commands.add("test -d /opt/USAT || (mkdir -p /opt/USAT && chmod 0755 /opt/USAT)");
		commands.add("if [ -d /opt/USAT/tmp ]; then chown root:usat /opt/USAT/tmp; chmod 0775 /opt/USAT/tmp; fi");
		commands.add("test -d /opt/USAT/conf || (mkdir -p /opt/USAT/conf && chown root:root /opt/USAT && chown root:usat /opt/USAT/conf)");
		commands.add("test -d /usr/local/USAT/bin || (mkdir -p /usr/local/USAT/bin && chown root:root /usr/local/USAT && chown root:usat /usr/local/USAT/bin)");
		commands.add("grep -c '@usat .* nproc ' /etc/security/limits.conf || echo '@usat        soft    nproc           10000' >> /etc/security/limits.conf");
		commands.add("grep -c '@usat .* nofile ' /etc/security/limits.conf || echo '@usat        -       nofile          20000' >> /etc/security/limits.conf");
		commands.add("grep -c '@usat .* memlock ' /etc/security/limits.conf || echo '@usat        -       memlock         1024' >> /etc/security/limits.conf");
		commands.add(DeployUtils.getMakeDir("/etc/init/USAT", "root", "root", null));
		commands.add("if [ -d " + getWorkDir() + " ]; then find " + getWorkDir() + " -mindepth 1 -delete; else mkdir -p " + getWorkDir() + " && chown `logname` " + getWorkDir() + "; fi");
		if(host.isServerType("NET"))
			commands.add("grep -c 'netlayer .* nofile ' /etc/security/limits.conf || echo 'netlayer        -       nofile       200000' >> /etc/security/limits.conf");
		/**
		PropertiesUpdateFileTask sysctlUpdateTask = new PropertiesUpdateFileTask(true, 0600, "root", "root");
		sysctlUpdateTask.setFilePath("/etc/sysctl.conf");
		sysctlUpdateTask.registerValue("net.ipv4.tcp_keepalive_time", new DefaultHostSpecificValue(String.valueOf(5)));
		sysctlUpdateTask.registerValue("net.ipv4.tcp_keepalive_intvl", new DefaultHostSpecificValue(String.valueOf(1)));
		sysctlUpdateTask.registerValue("net.ipv4.tcp_keepalive_probes", new DefaultHostSpecificValue(String.valueOf(5)));
		tasks.add(sysctlUpdateTask);
		commands.add("sudo sysctl -e -p");
		*/
		tasks.add(new ExecuteTask(commands.toArray(new String[commands.size()])));
		if(host.isServerType("MON") || host.isServerType("APR") || host.isServerType("NET") || host.isServerType("APP") || host.isServerType("WEB") || host.isServerType("LOGS") || (host.isServerType("KLS") && !"LOCAL".equalsIgnoreCase(host.getServerEnv()))) {
			// Upload jmx.access, jmx.login.config, translations.properties
			tasks.add(new FromCvsUploadTask("ServerLayers/LayersCommon/conf/jmx.login.config_" + host.getServerEnv().toLowerCase(), "HEAD", "/opt/USAT/conf/jmx.login.config", 0640, "root", "usat", true));
			if(host.isServerType("APR") || host.isServerType("NET"))
				tasks.add(new FromCvsUploadTask("ServerLayers/LayersCommon/src/translations.properties", "HEAD", "/opt/USAT/conf/translations.properties", 0640, "root", "usat", true, true));
			tasks.add(new FromCvsUploadTask("ServerLayers/LayersCommon/conf/logging.properties", "HEAD", "/opt/USAT/conf/logging.properties", 0640, "root", "usat", true));

			BufferStream buffer = new HeapBufferStream();
			PrintStream ps = new PrintStream(buffer.getOutputStream());
			ps.println("monitor readonly");
			String[] users = USATHelper.getJmxUsers(host);
			if(users != null)
				for(String user : users) {
					ps.print(user);
					ps.println(" readwrite");
				}
			ps.flush();
			tasks.add(new UnixUploadTask(buffer.getInputStream(), "/opt/USAT/conf", "jmx.access", 0640, "root", "usat", true, "Access for users " + Arrays.toString(users)));

			// Create keystore.ks and truststore.ts
			tasks.add(new RenewJKSCertTask(new Date(System.currentTimeMillis() + 180 * 24 * 60 * 60 * 1000L), "/opt/USAT/conf/keystore.ks", cache, 0640));
			tasks.add(new ImportJKSCertTask("usatrootca", new GitPullTask("ServerLayers/LayersCommon/conf/usat_root.crt"), "/opt/USAT/conf/truststore.ts", cache, 0640, false));
			tasks.add(new ImportJKSCertTask("usatrootca_20250203", new GitPullTask("ServerLayers/LayersCommon/conf/usat_root_20250203.crt"), "/opt/USAT/conf/truststore.ts", cache, 0640, false));
			if(!host.getServerEnv().equalsIgnoreCase("USA"))
				tasks.add(new ImportJKSCertTask("usattestrootca", new GitPullTask("ServerLayers/LayersCommon/conf/usat_test_root.crt"), "/opt/USAT/conf/truststore.ts", cache, 0640, false));
			tasks.add(new RotateMessageChainEncryptionKeyTask(new Date(System.currentTimeMillis() + 180 * 24 * 60 * 60 * 1000L), "/opt/USAT/conf/keystore.ks", "/opt/USAT/conf/truststore.ts", 0640, 0640, registry.getServerSet(host.getServerEnv()), true, new Date(System.currentTimeMillis() - 365 * 24 * 60 * 60 * 1000L)));
		}
		tasks.add(new FromCvsUploadTask("server_app_config/common/archive", "HEAD", "/usr/local/USAT/bin/archive", 0555, "root", "root", true));
		//tasks.add(new UsatIPTablesUpdateTask(registry.getServerSet(host.getServerEnv())));
		if(host.isServerType("MST") || host.isServerType("MSR") || host.isServerType("KLS")|| host.isServerType("PGS")) { // Do this after IPTablesTask
			tasks.add(new ExecuteTask("sudo su", 
					//"if ! grep -c 'kernel.sem =' /etc/sysctl.conf; then echo 'kernel.sem = 250 256000 32 1024' >> /etc/sysctl.conf; sysctl -e -p; fi",
					"if [ `egrep -c '^postgres:' /etc/passwd` -eq 0 ]; then groupadd -g 26 postgres; useradd -c 'PostgreSQL Server' -d /home/postgres -g 26 -G usat -m -u 26 postgres; elif [ `egrep -c '^usat:x:900:(\\w\\w*,)*postgres' /etc/group` -eq 0 ]; then usermod -G usat postgres; fi",
					DeployUtils.getMakeDir("/home/postgres", "postgres", "postgres", null)));			
		}
		// Install additional certs for Netlayer
		if(host.isServerType("NET")) {
			CryptUtils.MapCopyEntryFilter trustFilter = new CryptUtils.MapCopyEntryFilter();
			trustFilter.addAlias("verisign-class3-public-primary-ca-root", "verisign-class3-public-primary-ca-root");
			trustFilter.addAlias("geo-trust-root", "geo-trust-root");
			trustFilter.addAlias("ppdwatercard.aquafill.net", "ppdwatercard.aquafill.net");
			//trustFilter.addAlias("sftp.novainfo.net", "sftp.novainfo.net");
			CryptUtils.MapCopyEntryFilter keyFilter = new CryptUtils.MapCopyEntryFilter();
			//keyFilter.addAlias("gateway.usatech.com", "gateway.usatech.com");
			tasks.add(new ImportJKSEntriesTask(new GitPullTask("ServerLayers/LayersCommon/conf/net/truststore.ts"), "usatech".toCharArray(), "default.usat.keystore.password", "/opt/USAT/conf/truststore.ts", cache, 0640, trustFilter));
			tasks.add(new ImportJKSEntriesTask(new GitPullTask("ServerLayers/LayersCommon/conf/net/keystore.ks"), "usatech".toCharArray(), "default.usat.keystore.password", "/opt/USAT/conf/keystore.ks", cache, 0640, keyFilter));
			IPTablesUpdateTask iptRoutingTask = new IPTablesUpdateTask();
			iptRoutingTask.prerouteTcp(null, null, host.getFullName(), 443, null, 14143);
			iptRoutingTask.prerouteTcp(null, null, "USA".equalsIgnoreCase(host.getServerEnv()) ? "usanet2x-virtual.trooper.usatech.com" : host.getServerEnv().toLowerCase() + "net0x-virtual.usatech.com", 443, null, 14143);
//			tasks.add(iptRoutingTask);
		}
		// Install additional jarsigner key for KeyManager
		if(host.isServerType("KLS")) {
			//tasks.add(new RenewJKSCertTask(new Date(System.currentTimeMillis() + 30 * 24 * 60 * 60 * 1000L), "/opt/USAT/conf/keystore.ks", cache, 0640, new DefaultHostSpecificValue("jarsigner"), new DefaultHostSpecificValue(host.getSimpleName()), "usasubca.usatech.com", "USATCodeSigning", System.getProperty("user.name")));
			// conf/webtruststore.ts
			ImportJKSCertTask webCertsTask = new ImportJKSCertTask("/opt/USAT/conf/webtruststore.ts", cache, 0640, false);
			webCertsTask.addCertificate("usattokenca", new GitPullTask("ServerLayers/LayersCommon/conf/usat_token.crt"));
			webCertsTask.addCertificate("usattokenca2", new GitPullTask("ServerLayers/LayersCommon/conf/usat_token_2.crt"));
			webCertsTask.addCertificate("usattokenca_20200203", new GitPullTask("ServerLayers/LayersCommon/conf/usat_token_20200203.crt"));
			tasks.add(webCertsTask);
		}
		//For app load rptgen ccefill private key
		if(host.isServerType("APP") && registry.getRegisteredApps().contains(RPTGEN_LAYER)) {
			//load cceus cceCanada private key to keystore
			File publicCertCanada = File.createTempFile("USALive_170_CCE_BC", ".pub");
			File unencryptedKeyCanada = File.createTempFile("USALive_170_CCE_BC", ".pk");
			File publicCertUS = File.createTempFile("USALive_170_CCE_US", ".pub");
			File unencryptedKeyUS = File.createTempFile("USALive_170_CCE_US", ".pk");
			final String privateKeyHostFrom, privateKeyPathCCEUS, privateKeyPathCCECanada, cceUSKeystoreAlias, cceCanadaKeystoreAlias;
			if(host.getServerEnv().equalsIgnoreCase("USA")) {
				privateKeyHostFrom="usaapp21";
				cceUSKeystoreAlias="ccefill_2974";
				cceCanadaKeystoreAlias="ccefill_9955";
			}else{
				privateKeyHostFrom="usadev01";
				cceUSKeystoreAlias="ccefill_2974";
				cceCanadaKeystoreAlias="ccefill_3156";
			}
			privateKeyPathCCEUS="/opt/jobs/CCE_Fill_Reconciliation/.ssh-keys/2974/cce_fill_rsa";
			privateKeyPathCCECanada="/opt/jobs/CCE_Fill_Reconciliation/.ssh-keys/9955/cce_fill_rsa";
			final DeployTask[] cceUSTasks = new DeployTask[] { 
					new OpenSSLTask(OpenSSLTask.Operation.NEW_X509_CERTIFICATE_REQUEST_WITH_PRECONVERT, new GitPullTask("server_app_config/RPT/rptgen/cce_fill_cert_batch.cfg"), cceUSKeystoreAlias, "CCE - US", publicCertUS, unencryptedKeyUS, privateKeyHostFrom, privateKeyPathCCEUS),
					new ImportJKSPrivateKeyTask(cceUSKeystoreAlias, unencryptedKeyUS, publicCertUS, "/opt/USAT/conf/keystore.ks", cache, 0640, false) };
			final DeployTask[] cceCATasks = new DeployTask[] { 
					new OpenSSLTask(OpenSSLTask.Operation.NEW_X509_CERTIFICATE_REQUEST_WITH_PRECONVERT, new GitPullTask("server_app_config/RPT/rptgen/cce_fill_cert_batch.cfg"), cceCanadaKeystoreAlias, "CCE - Canada", publicCertCanada, unencryptedKeyCanada, privateKeyHostFrom, privateKeyPathCCECanada),
					new ImportJKSPrivateKeyTask(cceCanadaKeystoreAlias, unencryptedKeyCanada, publicCertCanada, "/opt/USAT/conf/keystore.ks", cache, 0640, false) };
			tasks.add(new DeployTask() {
				@Override
				public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
					KeyStoreInfo ksi;
					try {
						ksi = DeployUtils.createKeyStoreInfo(deployTaskInfo.sshClientCache, deployTaskInfo.host, "/opt/USAT/conf/keystore.ks", "keystore", deployTaskInfo.interaction, deployTaskInfo.user, deployTaskInfo.passwordCache);
					} catch(KeyStoreException e) {
						throw new IOException(e);
					} catch(NoSuchAlgorithmException e) {
						throw new IOException(e);
					} catch(NoSuchProviderException e) {
						throw new IOException(e);
					} catch(CertificateException e) {
						throw new IOException(e);
					}
					int cnt = 0;
					try {
						if(!ksi.getReadableKeyStore().containsAlias(cceUSKeystoreAlias)) {
							for(DeployTask subtask : cceUSTasks)
								subtask.perform(deployTaskInfo);
							cnt++;
						}
						if(!ksi.getReadableKeyStore().containsAlias(cceCanadaKeystoreAlias)) {
							for(DeployTask subtask : cceCATasks)
								subtask.perform(deployTaskInfo);
							cnt++;
						}
					} catch(KeyStoreException e) {
						throw new IOException(e);
					}
					return cnt == 0 ? "Already update to date" : "Imported " + cnt + " certificates";
				}

				@Override
				public String getDescription(DeployTaskInfo deployTaskInfo) {
					return "Import CCE Fill Certificates";
				}
			});
		}
		if(!host.isServerType("PGS") || host.getServerTypes().size() > 1)
			USATHelper.addMonitInstallTasks(tasks, host, getWorkDir());
		// add USATPushToEnv setup
		if(host.isServerType("WEB")) {
			//tasks.add(new UploadTask(new File(serverAppConfigDir, "WEB/bin/USATPushToEnv"), "/usr/bin", 0755, "root", "root", true));
			//tasks.add(new UploadTask(new File(serverAppConfigDir, "WEB/bin/USATPushToHost"), "/usr/bin", 0755, "root", "root", true));
			tasks.add(new OptionalTask("if [ `sudo grep -c 'Defaults:%devteam !requiretty' /etc/sudoers` = 0 ]; then echo 'Y'; fi", Pattern.compile("^Y\\s*"), false,
					new ExecuteTask("sudo su", "echo 'Defaults:%devteam !requiretty' >> /etc/sudoers", "exit")
					));
		}
	}

	protected String getWorkDir() {
		return "/home/`logname`/" + getReleaseDir();
	}

	protected void addPostAppsTasks(List<DeployTask> tasks) {
		//restart monit only after all apps monit conf are in place and will be picked up.
		USATHelper.addMonitRestartTasks(tasks, getWorkDir());
	}

	public boolean isAppRegistered(App app) {
		return registry.getRegisteredApps().contains(app);
	}

	protected void addPrepareAppTasks(final Host host, final App app, final Integer ordinal, int instanceNum, final int instanceCount, List<DeployTask> tasks, PasswordCache cache) throws IOException {
		List<String> subcommands = new ArrayList<String>();
		subcommands.add("sudo su");
		subcommands.add("if [ ! -d " + getWorkDir() + " ]; then mkdir -p " + getWorkDir() + " && chown `logname` " + getWorkDir() + "; fi");
		if(ordinal == null || ordinal == 1) {
			if(app.getAppUid() > 99) {
				// Make user and ensure group includes usat
				subcommands.add("if [ `egrep -c '^" + app.getUserName() + ":' /etc/passwd` -eq 0 ]; then groupadd -g " + app.getAppUid() + " " + app.getUserName() + "; useradd -c '" + app.appDesc + "' -d /home/" + app.getName() + " -g " + app.getAppUid() + " -G usat -m -u " + app.getAppUid() + " " + app.getUserName() 
						+ "; elif [ `egrep -c '^usat:x:900:(\\w\\w*,)*" + app.getUserName()
						+ "' /etc/group` -eq 0 ]; then usermod -G usat " + app.getUserName() + "; fi");
				// Make home dir
				subcommands.add(DeployUtils.getMakeDir("/home/" + app.getName(), app.getUserName(), app.getUserName(), null));
				subcommands.add("test `ls -ld /home/" + app.getName() + " | cut -d' ' -f3` = '" + app.getUserName() + "' || chown " + app.getUserName() + ":" + app.getUserName() + " /home/" + app.getName());
				subcommands.add("if [ `grep -c '/home/" + app.getName() + "' /etc/passwd` -eq 0 ]; then usermod -d /home/" + app.getName() + " " + app.getUserName() + "; fi;");
				subcommands.add("if [ `egrep '^" + app.getUserName() + ":' /etc/shadow | cut -d: -f5` -ne 99999 ]; then passwd -x 99999 " + app.getUserName() + "; fi;");
			} else if("postgres".equals(app.getUserName())) {
				subcommands.add(DeployUtils.getMakeDir("/home/" + app.getName(), app.getUserName(), app.getUserName(), null));
			}
			subcommands.add("grep -c " + app.getUserName() + " /etc/cron.d/cron.allow || echo '" + app.getUserName() + "' >> /etc/cron.d/cron.allow");
		}
		// Make directories
		final String appDir;
		PGDATADir tmpPgDir;
		if(USATRegistry.POSTGRES_KLS.equals(app) && (tmpPgDir = PgDeployMap.hostPGDATADirMap.get(host.getSimpleName())) != null) {
			appDir = tmpPgDir.getPrimaryDir();
		} else
			appDir = "/opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal);
		subcommands.add(DeployUtils.getMakeDir(appDir, app.getUserName(), app.getUserName(), null));
		if(app.getAppUid() > 99) {
			subcommands.add(DeployUtils.getMakeDir(appDir + "/specific", app.getUserName(), app.getUserName(), null));
			subcommands.add(DeployUtils.getMakeDir(appDir + "/bin", app.getUserName(), app.getUserName(), null));
		} else if("httpd".equals(app.getName())) {
			subcommands.add(DeployUtils.getMakeDir(appDir + "/conf", app.getUserName(), app.getUserName(), null));
			subcommands.add(DeployUtils.getMakeDir(appDir + "/conf.d", app.getUserName(), app.getUserName(), null));
			subcommands.add(DeployUtils.getMakeDir(appDir + "/logs", "daemon", "bin", null)); // XXX:do we need this to be owned by daemon?
			subcommands.add(DeployUtils.getMakeDir(appDir + "/ssl", app.getUserName(), app.getUserName(), null));
		} else if("jstatd".equals(app.getName())) {
			subcommands.add(DeployUtils.getMakeDir(appDir + "/bin", app.getUserName(), app.getUserName(), null));
		}

		if(!subcommands.isEmpty())
			tasks.add(new ExecuteTask(subcommands.toArray(new String[subcommands.size()])));
		subcommands.clear();
		
		if(!host.isServerType("PGS")){
			USATHelper.uploadAppMonitConf(host, tasks, app, ordinal);
		}
		
		CronUpdateTask cronUpdateTask = new CronUpdateTask(app.getUserName(), true);
		if("httpd".equals(app.getName())) {
			/**
			 * String interfaceStr;
			 * if("ECC".equalsIgnoreCase(host.getServerEnv())||"USA".equalsIgnoreCase(host.getServerEnv())) {
			 * interfaceStr="bond0";
			 * } else {
			 * interfaceStr="eth0";
			 * }
			 * 
			 * ExecuteTask addToSysctlConf=new ExecuteTask("sudo su",
			 * "/bin/cp /etc/sysctl.conf /etc/sysctl.conf.bak",
			 * "grep -c 'net.ipv4.conf.all.arp_ignore=1' /etc/sysctl.conf || echo 'net.ipv4.conf.all.arp_ignore=1' >> /etc/sysctl.conf",
			 * "grep -c 'net.ipv4.conf."+interfaceStr+".arp_ignore=1' /etc/sysctl.conf || echo 'net.ipv4.conf."+interfaceStr+".arp_ignore=1' >> /etc/sysctl.conf",
			 * "grep -c 'net.ipv4.conf.all.arp_announce=2' /etc/sysctl.conf || echo 'net.ipv4.conf.all.arp_announce=2' >> /etc/sysctl.conf",
			 * "grep -c 'net.ipv4.conf."+interfaceStr+".arp_announce=2' /etc/sysctl.conf || echo 'net.ipv4.conf."+interfaceStr+".arp_announce=2' >> /etc/sysctl.conf",
			 * "/sbin/sysctl -e -p"
			 * );
			 * tasks.add(addToSysctlConf);
			 */
			String workDir = getWorkDir();
			final String httpdRoot = "/usr/local/apache2";
			final String installFileName;
			Set<App> subApps = registry.getServerSet(host.getServerEnv()).getSubApps(app);
			boolean phpNeeded = app.equals(USATRegistry.HTTPD_MON) || subApps.contains(USATECH_WEBSITE_APP) || subApps.contains(ENERGYMISERS_WEBSITE_APP);
			DefaultPullTask installFileTask = app.getRetrieveInstallFileTask();
			if(app.getInstallFileName() != null && app.getVersion().contains("usat-build") && (installFileName = app.getInstallFileName().trim()).length() > 0) {
				String httpdVersion = app.getVersion().substring(0, app.getVersion().indexOf('-'));
				tasks.add(new OptionalTask("if [ -x " + httpdRoot + "/bin/apachectl ]; then " + httpdRoot + "/bin/apachectl -v fi; fi", Pattern.compile("Server version: Apache/" + httpdVersion.replaceAll("\\.", "\\\\.") + " .*", Pattern.DOTALL), true,
						installFileTask,
						new UploadTask(installFileTask.getResultInputStream(), 0644, null, null, installFileName, true, workDir + '/' + installFileName),
						new ExecuteTask("sudo su",
								"mv " + workDir + '/' + installFileName + " /usr/local",
								"cd /usr/local",
								"tar xvfz " + installFileName,
								"mv " + installFileName + " " + workDir)));
			} else {
				if(installFileTask != null) {
					Set<String> httpdModules = new LinkedHashSet<String>(Arrays.asList(new String[] {
							"reqtimeout",
							"deflate",
							"log-config",// this may not be needed
							"expires",
							"headers",
							"usertrack",
							"unique-id",
							"setenvif",// this may not be needed
							"proxy",
							"proxy-http",
							"proxy-balancer",
							"ssl",
							"mime",// this may not be needed
							"status",// this may not be needed
							"dir",// this may not be needed
							"alias",// this may not be needed
							"rewrite"
					}));
					String untarDir = StringUtils.substringBefore(app.getInstallFileName(), ".tar.gz");
					tasks.add(new OptionalTask(httpdRoot + "/bin/apachectl -v fi", Pattern.compile("Server version: Apache/" + app.getVersion().replaceAll("\\.", "\\\\.") + " .*", Pattern.DOTALL), true,
							installFileTask,
							new UploadTask(installFileTask.getResultInputStream(), 0644, null, null, app.getInstallFileName(), true, workDir + '/' + app.getInstallFileName()),
							new ExecuteTask("sudo su",
											"/bin/rm -r " + workDir + '/' + untarDir,
											"cd " + workDir,
											"gunzip -c " + app.getInstallFileName() + " | tar -xv --no-same-owner",
											"cd " + untarDir,
											"./configure --prefix=" + httpdRoot + " --enable-" + StringUtils.join(httpdModules, " --enable-"),
											"make",
											"make install"/*XXX: do we need this?,
											"chown -R bin:bin " + httpdRoot */)
							));
				}
				//compile and install php for usatech website, may need for other website later
				if(phpNeeded){				
					Set<String> phpConfigOptions = new LinkedHashSet<String>(Arrays.asList(new String[] {
							"--disable-all",
							"--prefix=/usr/local/php",
							"--with-apxs2=/usr/local/apache2/bin/apxs",
							"--with-config-file-path=/usr/local/apache2/conf",
							"--with-zlib",
							"--with-openssl",
							"--without-pear",
							"--disable-cli",
							"--enable-session"
					}));
					if(app.equals(USATRegistry.HTTPD_MON)) {
						phpConfigOptions.add("--enable-pdo");
						phpConfigOptions.add("--with-pdo-pgsql=/usr/pgsql-latest/");
					}
					tasks.add(new OptionalTask("if [ ! -x " + httpdRoot + "/modules/libphp5.so ]; then echo 'Y'; fi", Pattern.compile("^Y\\s*"), false,
							new FromCvsUploadTask("server_app_config/WEB/Httpd/source/php-5.4.7/php-5.4.7.tar.gz", "HEAD", workDir + "/php-5.4.7.tar.gz", 0644),
							new ExecuteTask("sudo su",
											"/bin/rm -r " + workDir + "/php-5.4.7",
											"cd " + workDir,
											"gunzip -c php-5.4.7.tar.gz | tar -xv --no-same-owner",
											"cd php-5.4.7",
											"./configure "+StringUtils.join(phpConfigOptions, " "),
											"make",
											"make install")
							));
				}
			}
			if(phpNeeded){
				tasks.add(new FromCvsUploadTask("server_app_config/WEB/Httpd/conf/php.ini", "HEAD", appDir + "/conf/php.ini", 0644, app.getUserName(), app.getUserName(), true));
				tasks.add(new FromCvsUploadTask("server_app_config/WEB/Httpd/conf.d/php.conf", "HEAD", appDir + "/conf.d/php.conf", 0644, app.getUserName(), app.getUserName(), true));
			}
			//tasks.add(new FromCvsUploadTask("server_app_config/WEB/Httpd/conf/httpd.conf", "HEAD", appDir + "/conf/httpd.conf", 0644, app.getUserName(), app.getUserName(), true));
			//tasks.add(new FromCvsUploadTask("server_app_config/WEB/Httpd/x86/rpaf-0.6/mod_rpaf-2.0.so", "HEAD", "/usr/local/apache2/modules/mod_rpaf-2.0.so", 0755, "root", "root", false));
			//tasks.add(new FromCvsUploadTask("server_app_config/WEB/Httpd/conf.d/rpaf.conf", "HEAD", appDir + "/conf.d/rpaf.conf", 0644, app.getUserName(), app.getUserName(), true));
			subcommands.add("sudo su");
			subcommands.add("if [ ! -r " + appDir + "/conf/magic ]; then cp " + httpdRoot + "/conf/magic " + appDir + "/conf; fi");
			subcommands.add("if [ ! -r " + appDir + "/conf/mime.types ]; then cp " + httpdRoot + "/conf/mime.types " + appDir + "/conf; fi");
			subcommands.add("grep -c root /etc/cron.d/cron.allow || echo 'root' >> /etc/cron.d/cron.allow");
			CronUpdateTask rootCronUpdateTask = new CronUpdateTask("root", true);
			rootCronUpdateTask.registerJob(Pattern.compile(".*" + appDir + "/archivelogs\\.sh.*"), new int[] { 4 + (ordinal == null ? 1 : ordinal) * 8 }, null, null, null, null, appDir + "/archivelogs.sh >> " + appDir + "/archivelogs.log 2>&1", "# Rename and gzip rotated log files");
			tasks.add(rootCronUpdateTask);
			cronUpdateTask.setKeepExisting(false);
			// tasks.add(new UnixUploadTask(new EncodingInputStream(new StringReader("OK\n"), Charset.defaultCharset().newEncoder(), 1024), httpdRoot + "/htdocs/monit.chk", 0644, app.getUserName(),
			// app.getUserName(), false, "monit.chk"),
			// Charset.defaultCharset().newEncoder(), 1024);
			//IPTablesUpdateTask routingIPTTask = new IPTablesUpdateTask(null, null, "PREROUTING");
			//tasks.add(routingIPTTask);
			String lb_ip_escaped;
			if("DEV".equalsIgnoreCase(host.getServerEnv()))
				lb_ip_escaped = "10\\\\.0\\\\.0\\\\.64";
			else if("INT".equalsIgnoreCase(host.getServerEnv()))
				lb_ip_escaped = "10\\\\.0\\\\.0\\\\.64";
			else if("ECC".equalsIgnoreCase(host.getServerEnv()))
				lb_ip_escaped = "192\\\\.168\\\\.4\\\\.65";
			else if("USA".equalsIgnoreCase(host.getServerEnv()))
				lb_ip_escaped = "192\\\\.168\\\\.79\\\\.(?:18[34]|19[56])";
			else
				lb_ip_escaped = "";
			BufferStream confBufferStream = new HeapBufferStream();
			PrintWriter confWriter = new PrintWriter(confBufferStream.getOutputStream());
			/*
			if(!"USAT".equalsIgnoreCase(host.getServerEnv())) {
				for(InetAddress ia : InetAddress.getAllByName(host.getSimpleName() + "-vip.usatech.com")) {
					confWriter.print("NameVirtualHost ");
					confWriter.print(ia.getHostAddress());
					confWriter.println(":*");
				}
			}*/
			confWriter.print("SetEnvIf Remote_Addr ^");
			confWriter.print(lb_ip_escaped);
			confWriter.println("$ helper_file_request");
			if(host.isServerType("WEB")) {
				confWriter.println("Listen 0.0.0.0:443");
				confWriter.println("NameVirtualHost *:443");
			}
			confWriter.flush();
			//tasks.add(new UnixUploadTask(confBufferStream.getInputStream(), appDir + "/conf.d", "httpd_extra.conf", 0644, app.getUserName(), app.getUserName(), true));
			String last2 = host.getSimpleName().substring(6);
			if(!"MON".equalsIgnoreCase(app.getServerType())) {
			String appServer;
			if("NET".equalsIgnoreCase(app.getServerType()))
				appServer = "apr";
			else if("WEB".equalsIgnoreCase(app.getServerType())) {
				if("USA".equalsIgnoreCase(host.getServerEnv())) {
					appServer = "app";
				} else {
					appServer = "apr";
				}
				} else
				throw new IOException("Don't know app server for httpd on '" + app.getServerType() + "'");
			/*
			final CopyTask copyTask;
			if("USA".equalsIgnoreCase(host.getServerEnv()) && "WEB".equalsIgnoreCase(app.getServerType())) {
				copyTask = new CopyTask(new HostImpl("USA", "usaweb21.trooper.usatech.com"), host);
				tasks.add(copyTask);
			} else
				copyTask = null;// */
			//HashMap<String, String> loopbackMap = LoopbackInterfaceMap.getMapByEnv(host.getServerEnv());
			for(App subApp : subApps) {
				// install sub app portion
				final Map<Pattern, String> replacements = new HashMap<Pattern, String>();
				replacements.put(Pattern.compile("<APP_SERVER_NAME>"), host.getServerEnv().toLowerCase() + appServer + last2);
				replacements.put(Pattern.compile("<WORKER_NAME>"), "worker_" + instanceNum);
				String subHostPrefix=subHostPrefixMap.get(subApp.getName());
				if(subHostPrefix==null){
					subHostPrefix=subApp.getName();
				}
				String subhostname = subHostPrefix + ("USA".equalsIgnoreCase(host.getServerEnv()) ? ".usatech.com" : "-" + host.getServerEnv().toLowerCase() + ".usatech.com");
				String subhostip = LoopbackInterfaceMap.getHostRealIp(host.getServerEnv(), subhostname);
				// **
				 //for loopback interface to work
				/** 
				if(!subApp.getName().equals("energymisers")){//energymisers shares ip with usatech
					String deviceNameSuffix = loopbackMap.get(subhostip);
					if(deviceNameSuffix == null)
						throw new IOException("DeviceNameSuffix is null for " + subhostip);
					String deviceName = "lo:" + deviceNameSuffix;
					tasks.add(new ExecuteTask("sudo su",
							"test -d /etc/sysconfig/network-scripts/ifcfg-"+deviceName+" || echo 'DEVICE="+deviceName+"\nIPADDR="+subhostip+"\nNETMASK=0.0.0.0\nONBOOT=yes' >> /etc/sysconfig/network-scripts/ifcfg-"+deviceName,
							"ifup "+deviceName)
							);
				}
				*/
				// */
					
				//routingIPTTask.prerouteTcp(null, null, subhostip, null, null, null);
				/*
				if(copyTask != null && (subApp.equals(ESUDS_APP) || subApp.equals(USALIVE_APP))) {
					copyTask.addCopy("/usr/local/apache2/ssl/" + subApp.getName() + ".crt", appDir + "/ssl/" + subApp.getName() + ".crt", false, 0644, app.getUserName(), app.getUserName());
					copyTask.addCopy("/usr/local/apache2/ssl/" + subApp.getName() + ".key", appDir + "/ssl/" + subApp.getName() + ".key", false, 0640, app.getUserName(), app.getUserName());
				} else // */
				RenewFileCertTask rfct = new RenewFileCertTask(new Date(System.currentTimeMillis() + 30 * 24 * 60 * 60 * 1000L), appDir + "/ssl/" + subApp.getName() + ".crt", appDir + "/ssl/" + subApp.getName() + ".key", 0644, 0640, app.getUserName(), app.getUserName(), subhostname, null, "USA".equalsIgnoreCase(host.getServerEnv()) ? null : "USATWebServer", null);
				BasicServerSet serverSet = registry.getServerSet(host.getServerEnv());
				for(Server server : serverSet.getServers(app.getServerType()))
					if(!server.host.equals(host))
						rfct.addCopyToHosts(server.host);
				//tasks.add(rfct);
				replacements.put(Pattern.compile("<NON_PROD_COMMENT>"), ("USA".equalsIgnoreCase(host.getServerEnv()) ? "" : "#"));
				replacements.put(Pattern.compile("<ENV_SUFFIX>"), ("USA".equalsIgnoreCase(host.getServerEnv()) ? "" : "-" + host.getServerEnv().toLowerCase()));
				replacements.put(Pattern.compile("<WEB_SERVER_NAME>"), subhostname);
				replacements.put(Pattern.compile("<WEB_SERVER_IP>"), subhostip);
				replacements.put(Pattern.compile("<WEB_SERVER_IP_ESCAPED>"), subhostip.replace(".", "\\\\."));
				replacements.put(Pattern.compile("<LOADBALANCER_IP_ESCAPED>"), lb_ip_escaped);

//				tasks.add(new FromCvsUploadTask("server_app_config/WEB/Httpd/conf.d/" + subApp.getName() + ".conf", "HEAD", appDir + "/conf.d/" + subApp.getName() + ".conf", 0644, app.getUserName(), app.getUserName(), true) {
//					@Override
//					protected InputStream decorate(InputStream in) {
//						return new EncodingInputStream(new ReplacementsLineFilteringReader(new BufferedReader(new InputStreamReader(in)), "\n", replacements));
//					}
//				});
				String subAppDir = appDir + '/' + subApp.getName();
				if(subApp.getName().equals("usatech")||subApp.getName().equals("energymisers")){
					/*
					//upload & unzip the content only when not exist or the version is different
					String sourceHost=null;
					String hostEnv=host.getServerEnv();
					if("DEV".equalsIgnoreCase(hostEnv)){
						sourceHost="devnet01";
					}else if("USA".equalsIgnoreCase(hostEnv)){
						sourceHost="usaweb21";
					}else{
						//INT and ECC both source from eccnet01
						sourceHost="eccnet01";
					}
					tasks.add(new ExecuteTask("sudo su",
											"rsync -avz --delete --rsync-path=\"sudo /usr/local/bin/rsync\" `logname`@"+sourceHost+":/opt/USAT/httpd/"+subApp.getName()+" /opt/USAT/httpd",
											"echo "+subApp.getVersion()+" >"+subAppDir+"/version.txt",
											"chown -R " + app.getUserName() + ':' + app.getUserName() + " " + subAppDir));
					*/
				} else if(subApp.getRetrieveInstallFileTask() != null) {
					DefaultPullTask subInstallFileTask = subApp.getRetrieveInstallFileTask();
					tasks.add(subInstallFileTask);
					tasks.add(new UploadTask(subInstallFileTask.getResultInputStream(), 0644, null, null, subApp.getInstallFileName(), true, workDir + '/' + subApp.getInstallFileName()));
					subcommands.add(DeployUtils.getMakeDir(subAppDir, app.getUserName(), app.getUserName(), null));
					subcommands.add("/bin/rm -rf " + subAppDir + "/*");
					Set<String> staticWebFiles = new HashSet<String>(Arrays.asList(new String[] {
							"web/*.pdf",
							"web/*.ico",
							"web/*.htm",
							"web/*.html",
							"web/*.css",
							"web/*.gif",
							"web/*.png",
							"web/*.jpg",
							"web/*.js",
							"web/*.swf",
							"web/*.xls",
							"web/*.xlsx",
							"web/*.csv",
							"web/*.eot",
							"web/*.svg",
							"web/*.ttf",
							"web/*.woff",
							"web/*.txt"
					})); //TODO: may need to change this for dms / usalive / esuds
					StringBuilder sb = new StringBuilder();
					if(USALIVE_APP.equals(subApp)) {
						subcommands.add(DeployUtils.getMakeDir(subAppDir+USATRegistry.USALIVE_CONTEXT_PATH, app.getUserName(), app.getUserName(), null));
							sb.append("gunzip -c ").append(workDir).append('/').append(subApp.getInstallFileName()).append(" | tar -C ").append(subAppDir).append(USATRegistry.USALIVE_CONTEXT_PATH).append(" -xvp --strip-components=1");
						for(String staticWebFile : staticWebFiles)
							sb.append(' ').append(staticWebFile);
						sb.append("; echo $? | egrep '0|2'");
						subcommands.add(sb.toString());
						if(!USATRegistry.USALIVE_CONTEXT_PATH.trim().isEmpty())
							subcommands.add("/bin/cp --preserve " + subAppDir + USATRegistry.USALIVE_CONTEXT_PATH + "/favicon.ico " + subAppDir);
					} else if(HOTCHOICE_APP.equals(subApp) || VERIZON_APP.equals(subApp)) {
						subcommands.add(DeployUtils.getMakeDir(subAppDir+USATRegistry.USALIVE_CONTEXT_PATH, app.getUserName(), app.getUserName(), null));
							sb.append("gunzip -c ").append(workDir).append('/').append(subApp.getInstallFileName()).append(" | tar -C ").append(subAppDir).append(USATRegistry.USALIVE_CONTEXT_PATH).append(" -xvp");
						subcommands.add(sb.toString());
						if(!USATRegistry.USALIVE_CONTEXT_PATH.trim().isEmpty())
							subcommands.add("/bin/cp --preserve " + subAppDir + USATRegistry.USALIVE_CONTEXT_PATH + "/favicon.ico " + subAppDir);
					} else {
							sb.append("gunzip -c ").append(workDir).append('/').append(subApp.getInstallFileName()).append(" | tar -C ").append(subAppDir).append(" -xvp --strip-components=1");
						for(String staticWebFile : staticWebFiles)
							sb.append(' ').append(staticWebFile);
						sb.append("; echo $? | egrep '0|2'");
						subcommands.add(sb.toString());
					}
					subcommands.add("chown -R " + app.getUserName() + ':' + app.getUserName() + " " + subAppDir);
					subcommands.add("find " + subAppDir + " -type d | xargs chmod 0755");
					subcommands.add("find " + subAppDir + " -type f | xargs chmod 0644");
				}
				}
				//if("WEB".equalsIgnoreCase(app.getServerType()) && !"USA".equalsIgnoreCase(host.getServerEnv()))
					//routingIPTTask.prerouteTcp(null, null, host.getSimpleName() + "-vip.usatech.com", null, null, null);
			}

			if(!subcommands.isEmpty())
				subcommands.add(httpdRoot + "/bin/httpd  -f " + appDir + "/conf/httpd.conf -k restart");
		} else if(app.getServiceName() != null) {
			int min = (ordinal == null ? 1 : ordinal) * 8;
			if("inauthlayer".equalsIgnoreCase(app.getName()) || "outauthlayer".equalsIgnoreCase(app.getName()))
				min += 2;
			else if("posmlayer".equalsIgnoreCase(app.getName()))
				min += 4;
			cronUpdateTask.registerJob(Pattern.compile(".*" + appDir + "/archivelogs\\.sh.*"), new int[] { min }, null, null, null, null, appDir + "/archivelogs.sh >> " + appDir + "/archivelogs.log 2>&1", "# Rename and gzip rotated log files");
			if(USATRegistry.KEYMGR.equals(app)) {
				Host standbyHost = PgDeployMap.standbyMap.get(host.getSimpleName());
				if(standbyHost != null) {
					String primaryBackupDir = PgDeployMap.primaryBackupDirMap.get(host.getSimpleName());
					String standbyBackupDir = PgDeployMap.standbyBackupDirMap.get(host.getSimpleName());
					if(!"USA".equalsIgnoreCase(host.getServerEnv())) {
						// currently only prod is setup. we want to run on all other env
						tasks.add(new ExecuteTask("sudo su", "if [ `egrep -c '^replica:' /etc/passwd` -eq 0 ]; then groupadd -g " + USATRegistry.REPLICA_LOCAL_USER_UID + " replica; useradd -g " + USATRegistry.REPLICA_LOCAL_USER_UID + " replica; fi", "test -d " + primaryBackupDir + " || (mkdir -p " + primaryBackupDir + " && chown replica:replica " + primaryBackupDir + ")"));
						tasks.add(new RsaSshSetupTask(host, standbyHost, app.getUserName(), app.getAppUid(), "replica", USATRegistry.REPLICA_LOCAL_USER_UID));
					}
					StringBuilder sb = new StringBuilder();
					sb.append("(date;/usr/bin/rsync -aAvxXz --no-R --no-D --files-from=").append(appDir).append("/rsync-files-list.txt / replica@").append(standbyHost.getSimpleName()).append(':').append(standbyBackupDir).append(") >> ").append(appDir).append("/logs/rsync-keymgr-files.log 2>&1");

					BufferStream filesBufferStream = new HeapBufferStream();
					PrintWriter filesWriter = new PrintWriter(filesBufferStream.getOutputStream());
					filesWriter.print(appDir);
					filesWriter.println("/store/kek.store");
					filesWriter.print(appDir);
					filesWriter.println("/store/secret.store");
					filesWriter.print(appDir);
					filesWriter.println("/keydb/keymanager.script");
					filesWriter.print(appDir);
					filesWriter.println("/keydb/keymanager.properties");
					filesWriter.flush();
					tasks.add(new UnixUploadTask(filesBufferStream.getInputStream(), appDir, "rsync-files-list.txt", 0644, app.getUserName(), app.getUserName(), true));
					cronUpdateTask.registerJob(Pattern.compile(".*/rsync-.* replica@.*"), new int[] { 0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55 }, null, null, null, null, sb.toString(), "# Backup keymgr data files");
				}
			}
		}
		if(!subcommands.isEmpty())
			tasks.add(new ExecuteTask(subcommands.toArray(new String[subcommands.size()])));
		subcommands.clear();
		if(cronUpdateTask.getJobCount() > 0 || !cronUpdateTask.isKeepExisting())
			tasks.add(cronUpdateTask);

		if(app.getServiceName() != null) {
			BufferStream bufferStream = new HeapBufferStream();
			PrintStream ps = new PrintStream(bufferStream.getOutputStream());
			if("httpd".equalsIgnoreCase(app.getName())) {
				printArchiveLogCommand(ps, 40, true, appDir + "/logs/error_log", appDir + "/logs/error_log.*");
				printArchiveLogCommand(ps, 40, true, appDir + "/logs/access_log", appDir + "/logs/access_log.*");
				Set<App> subApps = registry.getServerSet(host.getServerEnv()).getSubApps(app);
				for(App subApp : subApps) {
					printArchiveLogCommand(ps, 40, true, appDir + "/logs/" + subApp.getName() + "_error_log", appDir + "/logs/" + subApp.getName() + "_error_log.*");
					printArchiveLogCommand(ps, 40, true, appDir + "/logs/" + subApp.getName() + "access_log", appDir + "/logs/" + subApp.getName() + "_access_log.*");
					printArchiveLogCommand(ps, 40, true, appDir + "/logs/" + subApp.getName() + "_error_log", appDir + "/logs/" + subApp.getName() + "_ssl_error_log.*");
					printArchiveLogCommand(ps, 40, true, appDir + "/logs/" + subApp.getName() + "_access_log", appDir + "/logs/" + subApp.getName() + "_ssl_access_log.*");
				}
			} else if("postgres".equalsIgnoreCase(app.getName())) {
				printArchiveLogCommand(ps, app.equals(USATRegistry.POSTGRES_KLS) ? 100 : 40, true, appDir + "/logs/" + app.getServiceName(), appDir + "/logs/" + app.getServiceName() + "-*.log");
			} else {
				printArchiveLogCommand(ps, 40, false, appDir + "/logs/" + app.getServiceName(), appDir + "/logs/" + app.getServiceName() + ".log.*");
				printArchiveLogCommand(ps, 40, false, appDir + "/logs/" + app.getServiceName() + ".stdout", appDir + "/logs/" + app.getServiceName() + ".stdout.log.*");
			}
			ps.println("exit 0");
			ps.flush();
			tasks.add(new UnixUploadTask(bufferStream.getInputStream(), appDir, "archivelogs.sh", 0754, app.getUserName(), "usat", false, "Log archive script for '" + app.getName() + (ordinal == null ? "" : ordinal.toString()) + "'"));
		}

		/*
		if("applayer".equals(app.getName()) || "posmlayer".equals(app.getName())) {
			final String[] urls;
			if("DEV".equalsIgnoreCase(host.getServerEnv())) {
				urls = new String[] { "https://usadev01.usatech.com/KeyManager/soapservice/KeyManager" };
			} else if("INT".equalsIgnoreCase(host.getServerEnv())) {
				urls = new String[] { "https://ecckls01.usatech.com/KeyManager/soapservice/KeyManager" };
			} else if("ECC".equalsIgnoreCase(host.getServerEnv())) {
				urls = new String[] { "https://ecckls01.usatech.com/KeyManager/soapservice/KeyManager" };
			} else if("USA".equalsIgnoreCase(host.getServerEnv())) {
				urls = new String[] { "https://usakls21.trooper.usatech.com/KeyManager/soapservice/KeyManager", "https://usakls22.trooper.usatech.com/KeyManager/soapservice/KeyManager" };
			} else {
				urls = new String[0];
			}

			tasks.add(new RenewJKSCertTask(new Date(System.currentTimeMillis() + 30 * 24 * 60 * 60 * 1000L), "/opt/USAT/conf/keystore.ks", cache, 0640,
					new DefaultHostSpecificValue(app.getName() + ".keymanager"), new HostSpecificValue() {
						public boolean isOverriding() {
							return true;
						}

						public String getValue(Host host, Interaction interaction) {
							return app.getName() + '@' + host.getSimpleName();
						}

			}, "usatokenca.usatech.com", "USATKeyManagerAuthenticatedSession", app.getUserName()) {
				@Override
				protected void onNewEntry(String alias, Certificate[] certificateChain, PrivateKey privateKey, DeployTaskInfo deployTaskInfo) throws IOException {
					try {
						DeployUtils.attemptHttps(certificateChain, privateKey, deployTaskInfo.interaction, urls);
					} catch(Exception e) {
						throw new IOException(e);
					}
				}
			});
		} else */
		if(USATRegistry.DMS_APP.equals(app) || USATRegistry.USALIVE_APP.equals(app)) {
			BufferStream bufferStream = new HeapBufferStream();
			PrintStream ps = new PrintStream(bufferStream.getOutputStream());
			if("USA".equalsIgnoreCase(host.getServerEnv())) {
				ps.println("ldap.type=389_DIRECTORY_SERVER");
				ps.println("ldap.url=ldaps://usaldp22.trooper.usatech.com:636 ldaps://usaldp21.trooper.usatech.com:636");
				ps.println("ldap.dn.base=OU=USA Users,DC=usatech,DC=com");
			} else {
				ps.println("ldap.type=389_DIRECTORY_SERVER");
				ps.println("ldap.url=ldaps://devldp01.usatech.com:636 ldaps://devldp02.usatech.com:636");
				ps.println("ldap.dn.base=OU=USA Users,DC=usatech,DC=com");
			}
			ps.flush();
			tasks.add(new UnixUploadTask(bufferStream.getInputStream(), appDir + "/specific/ldap_conf.properties", 0640, app.getUserName(), app.getUserName(), true, "ldap_conf.properties"));
		} else if(USATRegistry.KEYMGR.equals(app) && (ordinal == null || ordinal == 1)) {
			IPTablesUpdateTask routingIPTTask = new IPTablesUpdateTask(null, null, "PREROUTING");
			int port = 7080 + app.getPortOffset() + (ordinal == null ? 700 : ordinal * 100);
			int sslport = 7043 + app.getPortOffset() + (ordinal == null ? 700 : ordinal * 100);
			routingIPTTask.prerouteTcp(null, null, null, 80, null, port);
			routingIPTTask.prerouteTcp(null, null, null, 443, null, sslport);
//			tasks.add(routingIPTTask);
		}
	}

	protected void printArchiveLogCommand(PrintStream ps, int retentionDays, boolean excludeLastFile, String targetPrefix, String... filePaths) {
		ps.print("/usr/local/USAT/bin/archive -v -a ");
		ps.print(retentionDays);
		ps.print(' ');
		if(excludeLastFile)
			ps.print("-exclude_last_file ");
		ps.print("-t=");
		ps.print(targetPrefix);
		if(filePaths != null)
			for(String filePath : filePaths) {
				ps.print(' ');
				ps.print(filePath);
			}
		ps.println();
	}

	protected int getMaxHeapMB(Host host, App app) {
		if("USA".equalsIgnoreCase(host.getServerEnv()) || "ECC".equalsIgnoreCase(host.getServerEnv()) || "INT".equalsIgnoreCase(host.getServerEnv())) {
			if(USATRegistry.DMS_APP.equals(app))
				return 3272;
			if(USATRegistry.APP_LAYER.equals(app) || USATRegistry.RPTGEN_LAYER.equals(app) || USATRegistry.USALIVE_APP.equals(app) || USATRegistry.OUTAUTH_LAYER.equals(app))
				return 8192;
			if(USATRegistry.ESUDS_APP.equals(app) || USATRegistry.NET_LAYER.equals(app) || USATRegistry.RPTREQ_LAYER.equals(app))
				return 2048;
			return 1024;
		}
		return 512;
	}
	/**
	 * @param commands TODO
	 * @throws IOException
	 */
	protected void addInstallAppTasks(final Host host, final App app, Integer ordinal, int instanceNum, int instanceCount, List<DeployTask> tasks, PasswordCache cache, List<String> commands) throws IOException {
		DefaultPullTask installFileTask = app.getRetrieveInstallFileTask();
		if(installFileTask != null && !"httpd".equals(app.getName())) {
			final String appDir = "/opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal);
			String workDir = getWorkDir();
			// upload zip file
			if(ordinal == null || ordinal == 1) {
				tasks.add(installFileTask);
				tasks.add(new UploadTask(installFileTask.getResultInputStream(), 0644, null, null, app.getInstallFileName(), true, workDir + '/' + app.getInstallFileName()));
			}
			final int xmx = getMaxHeapMB(host, app);
			tasks.add(new FromCvsUploadTask("ServerLayers/LayersCommon/conf/setenv.sh", "HEAD", appDir + "/bin/setenv.sh", 0750, app.getUserName(), app.getUserName(), true) {
				@Override
				protected InputStream decorate(InputStream in) {
					if(xmx > 0) {
						Map<Pattern, String> replacements = new HashMap<Pattern, String>();
						replacements.put(Pattern.compile("\\B-Xmx\\d+[A-Za-z]?\\b"), "-Xmx" + xmx + "M");
						in = new EncodingInputStream(new ReplacementsLineFilteringReader(new BufferedReader(new InputStreamReader(in)), "\n", replacements));
					}
					return in;
				}
			});
			commands.add("sudo su");
			commands.add("if [ `ls -1 " + appDir + "/classes | grep . -c` -gt 0 ]; then /bin/rm -r " + appDir + "/classes/*; fi");
			commands.add("if [ `ls -1 " + appDir + "/lib | grep . -c` -gt 0 ]; then /bin/rm -r " + appDir + "/lib/*; fi");
			commands.add("if [ `ls -1 " + appDir + "/web | grep . -c` -gt 0 ]; then /bin/rm -r " + appDir + "/web/*; fi");
			// extract zip fill
			commands.add("cd " + appDir);
			commands.add("gunzip -c \"" + workDir + '/' + app.getInstallFileName() + "\" | tar xvf -");
			if(KEYMGR.equals(app)) {
				// Get settings from loader.properties and put into instance_settings.properties
				tasks.add(new ExecuteTask("sudo su", 
						"if [ ! -r " + appDir + "/store/instance_settings.properties ]; then touch " + appDir + "/store/instance_settings.properties; chown " + app.getUserName() + ':' + app.getUserName() + ' ' + appDir + "/store/instance_settings.properties; fi",
						"if [ `grep -E -c '^kekStore\\.accountKEKAlias *=' " + appDir + "/store/instance_settings.properties` -eq 0 ] && [ `grep -E -c '^kekStore\\.accountKEKAlias *=' " + appDir + "/classes/loader.properties` -gt 0 ]; then grep -E '^kekStore\\.accountKEKAlias *=' " + appDir + "/classes/loader.properties >> " + appDir + "/store/instance_settings.properties; fi"));

				// sign applet.jar
				/*commands.add("/usr/jdk/latest/bin/jarsigner -keystore /opt/USAT/conf/keystore.ks " + appDir + "/web/admin/applet.jar jarsigner");
				commands.add("chown "+KEYMGR.getUserName()+":"+ KEYMGR.getUserName()+ " "+appDir + "/web/admin/applet.jar");
				commands.add("chmod 644 "+ appDir + "/web/admin/applet.jar");*/
				// compile ssss
				commands.add("cd " + appDir + "/store/ssss && make all");
			
				tasks.add(new ExecuteTask(commands.toArray(new String[commands.size()])));
				commands.clear();
				commands.add("sudo su");

				tasks.add(new FromCvsUploadTask("KeyManagerService/keydb/keymanager.properties", "HEAD", appDir + "/keydb/keymanager.properties", 0600, app.getUserName(), app.getUserName(), false, false));
				tasks.add(new UploadTask(new ByteArrayInputStream(new byte[0]), 0600, app.getUserName(), app.getUserName(), "Blank keymanager.crl", false, appDir + "/specific/keymanager.crl"));

				// Copy from old if not exists
				String srcServer;
				if("DEV".equalsIgnoreCase(host.getServerEnv()))
					srcServer = "usadev01.usatech.com";
				else if("INT".equalsIgnoreCase(host.getServerEnv()))
					srcServer = "ecckls01.usatech.com";
				else if ("ECC".equalsIgnoreCase(host.getServerEnv()))
					srcServer = "ecckls01.usatech.com";
				else if("USA".equalsIgnoreCase(host.getServerEnv()))
					srcServer = "usakls21.trooper.usatech.com";
				else 
					srcServer = "usadev01.usatech.com";
				CopyTask copyTask = new CopyTask(new HostImpl(host.getServerEnv(), srcServer), host);
				copyTask.addCopy("/opt/USAT/keymgr/webapps/KeyManager/WEB-INF/secret.store", appDir + "/store/secret.store", false, 0600, app.getUserName(), app.getUserName());
				copyTask.addCopy("/opt/USAT/keymgr/webapps/KeyManager/WEB-INF/kek.store", appDir + "/store/kek.store", false, 0600, app.getUserName(), app.getUserName());
				String expDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS000000").format(new Date(System.currentTimeMillis() + TimeUnit.DAYS.toMillis(90)));
				copyTask.addCopy("/opt/USAT/keymgr/webapps/KeyManager/WEB-INF/hsql/keymanager.script", appDir + "/keydb/keymanager.script", false, 0600, app.getUserName(), app.getUserName(),
						new ExecuteTask("sudo su", 
								"/bin/cp " + appDir + "/keydb/keymanager.script " + appDir + "/keydb/keymanager.script.ORIG",
								"grep -v \"INSERT INTO ROLE VALUES('[^']*','[^']*',[2-57-9])\" " + appDir + "/keydb/keymanager.script.ORIG | grep -v \"INSERT INTO USER_ROLE VALUES([2-57-9],\" > " + appDir + "/keydb/keymanager.script",
								"/bin/cp " + appDir + "/keydb/keymanager.script " + appDir + "/keydb/keymanager.script.ORIG2",
								"if [ `grep -c 'CREATE MEMORY TABLE KEY(.*EXPIRATION_TS' " + appDir + "/keydb/keymanager.script` -eq 0 ]; then " 
									+ "sed \"s/CREATE MEMORY TABLE KEY(CIPHER_TEXT_BASE64 VARCHAR(2048) NOT NULL,CREATED_BY_USER_ID INTEGER NOT NULL,CREATED_TS TIMESTAMP NOT NULL,IV_SIZE_BYTES INTEGER NOT NULL,KEK_ALIAS VARCHAR(32) NOT NULL,KEY_ID INTEGER NOT NULL PRIMARY KEY,KEY_SIZE_BITS INTEGER NOT NULL,PROVIDER_NAME VARCHAR(256) NOT NULL,TRANSFORMATION VARCHAR(256) NOT NULL)/CREATE MEMORY TABLE KEY(CIPHER_TEXT_BASE64 VARCHAR(2048) NOT NULL,CREATED_BY_USER_ID INTEGER NOT NULL,CREATED_TS TIMESTAMP NOT NULL,IV_SIZE_BYTES INTEGER NOT NULL,KEK_ALIAS VARCHAR(32) NOT NULL,KEY_ID INTEGER NOT NULL PRIMARY KEY,KEY_SIZE_BITS INTEGER NOT NULL,PROVIDER_NAME VARCHAR(256) NOT NULL,TRANSFORMATION VARCHAR(256) NOT NULL,EXPIRATION_TS TIMESTAMP NOT NULL)/\ns/\\(INSERT INTO KEY VALUES([^)]*\\))/\\1,'"
									+ expDate + "')/g\" "
									+ appDir + "/keydb/keymanager.script.ORIG2 > " + appDir + "/keydb/keymanager.script; fi"));
				tasks.add(copyTask);
			} else if(ESUDS_APP.equals(app)) {
				commands.add("echo '\n' >> " + appDir + "/classes/jaas.properties");
				commands.add("cat /opt/USAT/conf/jmx.login.config >> " + appDir + "/classes/jaas.properties");
			}
			commands.add("cd " + workDir);
			addBeforeStopCommands(host, app, ordinal, commands);
			StringBuilder sb = new StringBuilder();
			sb.append("if [ -r /etc/init/USAT/").append(app.getName());
			if(ordinal != null)
				sb.append(ordinal);
			sb.append(".conf ]; then ");

			sb.append("if initctl status USAT/").append(app.getName());
			if(ordinal != null)
				sb.append(ordinal);
			sb.append(" | grep -c 'USAT/").append(app.getName());
			if(ordinal != null)
				sb.append(ordinal);
			sb.append(" start/'; then if initctl stop USAT/").append(app.getName());
			if(ordinal != null)
				sb.append(ordinal);
			sb.append("; then SA_RESULT=0; echo 'App ").append(app.getName()).append(" was stopped in Upstart'; else SA_RESULT=1;  echo 'App ").append(app.getName()).append(" could NOT be stopped in Upstart';fi; else SA_RESULT=");
			if(enableAppIfDisabled(host, app, ordinal))
				sb.append(0);
			else
				sb.append(1);
			sb.append(";  echo 'App ").append(app.getName()).append(" was not running'; fi");

			sb.append("; /bin/rm /etc/init/USAT/").append(app.getName());
			if(ordinal != null)
				sb.append(ordinal);
			sb.append(".conf; else ");
			sb.append("APP_PID=`cat ").append(appDir).append("/logs/").append(app.getServiceName());
			sb.append(".pid`; ");
			sb.append("case `/usr/monit-latest/bin/monit summary | grep \"Process '").append(app.getName());
			if(ordinal != null)
				sb.append(ordinal);
			sb.append("'\"` in *Running) SA_RESULT=0; /usr/monit-latest/bin/monit stop ").append(app.getName());
			if(ordinal != null)
				sb.append(ordinal);
			sb.append("; while [ `/usr/monit-latest/bin/monit summary | grep -c \"Process '").append(app.getName());
			if(ordinal != null)
				sb.append(ordinal);
			sb.append("'.* pending\"` -gt 0 ]; do sleep 1; done; echo 'App ").append(app.getName()).append(" was stopped in Monit';; *) SA_RESULT=");
			if(enableAppIfDisabled(host, app, ordinal))
				sb.append(0);
			else
				sb.append(1);
			sb.append("; echo 'App ").append(app.getName()).append(" was not running';; esac");

			sb.append("; fi");
			commands.add(sb.toString());
			addBeforeRestartCommands(host, app, ordinal, instanceNum, commands, tasks);
			sb.setLength(0);
			sb.append("if ((SA_RESULT == 0)); then /usr/monit-latest/bin/monit start ").append(app.getName());
			if(ordinal != null)
				sb.append(ordinal);
			sb.append("; fi");
			commands.add(sb.toString());
			if(OUTAUTH_LAYER.equals(app)) {
				// update ReuploadFiles with truststore password
				tasks.add(new DeployTask() {
					@Override
					public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
						char[] pwd = DeployUtils.getPassword(deployTaskInfo.passwordCache, deployTaskInfo.host, "truststore", "Trust Store", deployTaskInfo.interaction);
						ExecuteTask.executeCommands(deployTaskInfo, "sudo su", "/bin/cp " + appDir + "/bin/ReuploadFiles.sh " + appDir + "/bin/ReuploadFiles.sh.ORIG", "sed -i 's/<TRUSTSTORE_PASSWORD>/" + new String(pwd).replaceAll("([\\W])", "\\\\$1") + "/g' " + appDir + "/bin/ReuploadFiles.sh");
						return "Complete";
					}

					@Override
					public String getDescription(DeployTaskInfo deployTaskInfo) {
						return "ReuploadFiles with truststore password";
					}
				});
			}
		} else if("postgres".equals(app.getName())) {
			if(!host.isServerType("PGS")){
				StringBuilder sb = new StringBuilder();
				sb.append("if [ -r /etc/init/USAT/").append(app.getName());
				if(ordinal != null)
					sb.append(ordinal);
				sb.append(".conf ]; then ");
				sb.append("if initctl status USAT/").append(app.getName());
				if(ordinal != null)
					sb.append(ordinal);
				sb.append(" | grep -c 'USAT/").append(app.getName());
				if(ordinal != null)
					sb.append(ordinal);
				sb.append(" start/'; then initctl stop USAT/").append(app.getName());
				if(ordinal != null)
					sb.append(ordinal);
				sb.append("; echo 'App ").append(app.getName()).append(" was stopped in Upstart'; fi; /bin/rm /etc/init/USAT/").append(app.getName());
				if(ordinal != null)
					sb.append(ordinal);
				String postgresAppName;
				PgDeployMap.PGDATADir pgDataDir;
				if(host.isServerType("KLS") && (pgDataDir = PgDeployMap.hostPGDATADirMap.get(host.getSimpleName())) != null) {
					String appDir;
					if(host.isStandby()){
						appDir=pgDataDir.getStandbyDir();
					}else{
						appDir=pgDataDir.getPrimaryDir();
					}
					postgresAppName=appDir.substring(appDir.lastIndexOf("/")+1);
				}else{
					postgresAppName=app.getName();
				}
				sb.append(".conf; /usr/monit-latest/bin/monit start ").append(postgresAppName);
				if(ordinal != null)
					sb.append(ordinal);
				sb.append("; fi;");
				tasks.add(new ExecuteTask("sudo su ", sb.toString()));
			}
		}
	}

	protected void addBeforeRestartCommands(Host host, App app, Integer ordinal, int instanceNum, List<String> commands, List<DeployTask> tasks) {

	}

	protected void addBeforeStopCommands(Host host, App app, Integer ordinal, List<String> commands) {
		
	}

	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return (ordinal == null || ordinal == 1);
	}

	public String getReleaseDir() {
		return releaseDir;
	}

	public void setReleaseDir(String releaseDir) {
		this.releaseDir = releaseDir;
	}
}