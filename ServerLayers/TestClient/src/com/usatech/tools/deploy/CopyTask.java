package com.usatech.tools.deploy;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

import simple.bean.ConvertUtils;
import simple.io.HeapBufferStream;
import simple.lang.SystemUtils;

public class CopyTask implements DeployTask {

	protected static class SrcDest {
		public final String srcPath;
		public final String destPath;
		public final boolean overwrite;
		public final Integer permissions;
		public final String owner;
		public final String group;
		public final DeployTask postCopyTask;

		public SrcDest(String srcPath, String destPath, boolean overwrite, Integer permissions, String owner, String group, DeployTask postCopyTask) {
			this.srcPath = srcPath;
			this.destPath = destPath;
			this.overwrite = overwrite;
			this.permissions = permissions;
			this.owner = owner;
			this.group = group;
			this.postCopyTask = postCopyTask;
		}

		@Override
		public int hashCode() {
			return SystemUtils.addHashCodes(0, srcPath, destPath);
		}

		@Override
		public boolean equals(Object obj) {
			if(!(obj instanceof SrcDest))
				return false;
			SrcDest sd = (SrcDest) obj;
			return ConvertUtils.areEqual(srcPath, sd.srcPath) && ConvertUtils.areEqual(destPath, sd.destPath);
		}
	}

	protected final Set<SrcDest> files = new LinkedHashSet<SrcDest>();
	protected final Host srcHost;
	protected final Host destHost;

	public CopyTask(Host srcHost, Host destHost) {
		this.srcHost = srcHost;
		this.destHost = destHost;
	}

	@Override
	public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
		int cnt = 0;
		for(SrcDest sd : files) {
			if(sd.overwrite || !UploadTask.checkFileUsingCmd(destHost, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, sd.destPath)) {
				InteractiveProgress progress = new InteractiveProgress(deployTaskInfo.interaction);
				StringBuilder sb = new StringBuilder();
				sb.append("Downloading file: ").append(sd.srcPath).append(" from ").append(srcHost.getSimpleName()).append('.');
				progress.setStatusDescription(sb.toString());
				HeapBufferStream bufferStream = new HeapBufferStream();
				if(!UploadTask.readFileUsingTmp(srcHost, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, sd.srcPath, bufferStream.getOutputStream(), progress))
					throw new IOException("Source file '" + sd.srcPath + "' not found on " + srcHost.getSimpleName());
				sb.setLength(0);
				sb.append("Uploading file: ").append(sd.destPath).append(" to ").append(destHost.getSimpleName()).append('.');
				progress.setStatusDescription(sb.toString());
				UploadTask.writeFileUsingTmp(destHost, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, bufferStream.getInputStream(), sd.permissions, sd.owner, sd.group, progress, sd.destPath);
				if(sd.postCopyTask != null)
					sd.postCopyTask.perform(deployTaskInfo);
				cnt++;
			} else {
				deployTaskInfo.interaction.printf("Skipping file '%1$s' because it already exists on %2$s", sd.destPath, destHost.getSimpleName());
			}
		}
		return "Copied " + cnt + " files";
	}

	public void addCopy(String srcPath, String destPath, boolean overwrite, Integer permissions, String owner, String group) {
		addCopy(srcPath, destPath, overwrite, permissions, owner, group, null);
	}

	public void addCopy(String srcPath, String destPath, boolean overwrite, Integer permissions, String owner, String group, DeployTask postCopyTask) {
		files.add(new SrcDest(srcPath, destPath, overwrite, permissions, owner, group, postCopyTask));
	}

	@Override
	public String getDescription(DeployTaskInfo deployTaskInfo) {
		StringBuilder sb = new StringBuilder();
		sb.append("Copying ");
		if(files.size() == 1) {
			SrcDest sd = files.iterator().next();
			sb.append('\'').append(sd.srcPath).append("' on ").append(srcHost.getSimpleName()).append(" to '").append(sd.destPath).append("' on ").append(destHost.getSimpleName());
		} else {
			sb.append(files.size()).append(" files from ").append(srcHost.getSimpleName()).append(" to ").append(destHost.getSimpleName());
		}
		return sb.toString();
	}
}
