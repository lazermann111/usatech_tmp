package com.usatech.tools.deploy;

import simple.io.Interaction;

public interface HostSpecificValue {
	public String getValue(Host host, Interaction interaction) ;
	public boolean isOverriding() ;
}
