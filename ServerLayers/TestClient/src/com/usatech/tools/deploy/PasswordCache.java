package com.usatech.tools.deploy;

public interface PasswordCache {
	public char[] getPassword(Host host, String key) ;
	public void setPassword(Host host, String key, char[] password) ;

	public char[] getWebPassword(String url, String user);

	public void setWebPassword(String url, String user, char[] password);
}
