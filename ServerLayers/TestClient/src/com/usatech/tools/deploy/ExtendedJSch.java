package com.usatech.tools.deploy;

import java.io.IOException;
import java.util.Properties;

import javax.swing.JOptionPane;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UserInfo;
import com.usatech.tools.deploy.DefaultSshClientCache.CacheKey;

public class ExtendedJSch extends JSch {

	private Session session;
	ExtendedJSch(String username, String host, int port) throws IOException {
		super();
		try {
			setKnownHosts(System.getProperty("user.home", ".") + "/knownhosts.lst");
			session = getSession(username, host, port);
			
			Properties config = new Properties();            
			config.put("StrictHostKeyChecking", "ask");
			session.setConfig(config);

		    session.setUserInfo(new UserInfo() {
		        @Override
		        public String getPassphrase() {
		            return null;
		        }

		        @Override
		        public String getPassword() {
		            return null;
		        }

		        @Override
		        public boolean promptPassword(String s) {
		            return false;
		        }

		        @Override
		        public boolean promptPassphrase(String s) {
		            return false;
		        }

		        @Override
		        public boolean promptYesNo(String s) {
		        	return JOptionPane.showConfirmDialog(null, s, "Confirm Host", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION;
		        }

		        @Override
		        public void showMessage(String s) {
		        }
		    });			
			
		} catch (JSchException e) {
			throw new IOException(e);
		}
	}
	
	public ExtendedJSch(CacheKey key) throws IOException  {
		this(key.user, key.hostFullName, 22);
	}

	public Session getSession() {
		return session;
	}

}
