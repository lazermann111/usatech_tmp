package com.usatech.tools.deploy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import simple.text.StringUtils;


public class DedupIPTablesTask implements DeployTask {
	protected final String routingChainName;
	protected final String inputChainName;
	protected final String outputChainName;

	public DedupIPTablesTask() {
		this("INPUT", "OUTPUT", "PREROUTING");
	}

	public DedupIPTablesTask(String inputChainName, String outputChainName, String routingChainName) {
		this.inputChainName = inputChainName;
		this.outputChainName = outputChainName;
		this.routingChainName = routingChainName;
	}
	@Override
	public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
		List<String> commandPrefixes = new ArrayList<String>();
		List<String> readCommands = new ArrayList<String>();
		if(inputChainName != null) {
			readCommands.add("sudo iptables -n --line-numbers -L " + inputChainName);
			commandPrefixes.add("iptables -D " + inputChainName + " ");
		}
		if(outputChainName != null) {
			readCommands.add("sudo iptables -n --line-numbers -L " + outputChainName);
			commandPrefixes.add("iptables -D " + outputChainName + " ");
		}
		if(routingChainName != null) {
			readCommands.add("sudo iptables -t nat --line-numbers -n -L " + routingChainName);
			commandPrefixes.add("iptables -t nat -D " + routingChainName + " ");
		}

		Collection<String> results = ExecuteTask.executeCommands(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.getSsh(), readCommands);
		List<String> commands = new ArrayList<String>(5);
		commands.add("sudo su");
		commands.add("iptables-save > /etc/sysconfig/iptables.`date '+%m%d%y%H%M%S'`");
		int i = 0;
		for(String result : results) {
			dedupRules(result, commandPrefixes.get(i++), commands);
		}
		if(commands.size() > 2) {
			commands.add("service iptables save");
			ExecuteTask.executeCommands(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.getSsh(), commands);
			return "Updated IPTables with " + (commands.size() - 2) + " new rules";
		}
		deployTaskInfo.interaction.printf("IPTables is up to date. Not changing");
		return "IPTables is up to date";
	}

	protected void dedupRules(String content, String commandPrefix, List<String> commands) throws UnknownHostException, IOException {
		Set<String> rules = new HashSet<String>();
		List<Integer> delete = new ArrayList<Integer>();
		BufferedReader reader = new BufferedReader(new StringReader(content));
		String line;
		boolean start = false;
		while((line = reader.readLine()) != null) {
			if(!start) {
				if(line.startsWith("num "))
					start = true;
			} else {
				String[] parts = StringUtils.split(line, ' ', 1);
				int lineNumber = Integer.parseInt(parts[0]);
				String signature = parts[1].trim();
				if(!rules.add(signature)) {
					delete.add(lineNumber);
				}
			}
		}
		ListIterator<Integer> iter = delete.listIterator(delete.size());
		while(iter.hasPrevious()) {
			commands.add(commandPrefix + iter.previous());
		}
	}

	@Override
	public String getDescription(DeployTaskInfo deployTaskInfo) {
		StringBuilder sb = new StringBuilder();
		sb.append("Deduping IPTables");
		return sb.toString();
	}
}
