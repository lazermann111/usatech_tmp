package com.usatech.tools.deploy;

import java.util.List;
import java.util.Set;

public interface ServerSet {
	public Server getServer(Host host, String serverType);

	public List<Server> getServers(Host host);

	public List<Server> getServers(String serverType);

	public List<Server> getServers(Set<String> serverTypes);

	public Set<App> getApps(String serverType);

	public Set<App> getSubApps(App app);

	public Set<Host> getHosts(String serverType);

	public Set<Host> getHosts(Set<String> serverTypes);

}
