package com.usatech.tools.deploy;

import java.util.Set;

public interface Host {
	public String getSimpleName() ;
	public String getFullName() ;
	public Set<String> getServerTypes() ;
	public boolean isServerType(String serverType) ;
	public String getServerEnv() ;
	public boolean isStandby();
}
