package com.usatech.tools.deploy;

import java.io.InputStream;
import java.io.PrintWriter;

import simple.app.Processor;
import simple.app.ServiceException;
import simple.io.HeapBufferStream;

public abstract class ContentProducer implements Processor<DeployTaskInfo, InputStream> {

	@Override
	public InputStream process(DeployTaskInfo argument) throws ServiceException {
		HeapBufferStream buffer = new HeapBufferStream();
		PrintWriter pw = new PrintWriter(buffer.getOutputStream());
		writeContent(pw, argument);
		pw.flush();
		return buffer.getInputStream();
	}

	protected abstract void writeContent(PrintWriter writer, DeployTaskInfo taskInfo);

	@Override
	public Class<InputStream> getReturnType() {
		return InputStream.class;
	}

	@Override
	public Class<DeployTaskInfo> getArgumentType() {
		return DeployTaskInfo.class;
	}
}
