/**
 * 
 */
package com.usatech.tools.deploy;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.util.concurrent.ExecutionException;

import simple.io.HeapBufferStream;
import simple.io.IOUtils;
import simple.security.SecurityUtils;
import simple.security.crypt.CryptUtils;

/** 
 * This task requires a PKCS#8 unencrypted private key file, and 
 * an X509 certificate representing the public key. You will be prompted 
 * for the key store password, but also the key password to be used 
 * for protecting the secret key once it has been imported.  
 * 
 * The certificate file will need to be generated using openssl, which
 * is installed on our servers. Use the OpenSSL task to perform the 
 * conversion. 
 * 
 * This class brings the keyStore locally and manipulates it prior to 
 * re-uploading it. 
 * 
 */
public class ImportJKSPrivateKeyTask implements DeployTask {
	private static final String LOCAL_KEY_STORE = "../../LayersCommon/conf/net/keystore.ks";
	protected final String alias;
	protected final File pkFile;
	protected final File certFile;
	protected final String keyStorePath;
	protected final PasswordCache cache;
	protected final boolean replace;
	protected final int keystorePermissions;

	public ImportJKSPrivateKeyTask(String alias, File pkFile, File certFile, String keyStorePath, PasswordCache cache, int keystorePermissions, boolean replace) {
		this.alias = alias;
		this.pkFile = pkFile;
		this.certFile = certFile;
		this.keyStorePath = keyStorePath;
		this.cache = cache;
		this.replace = replace;
		this.keystorePermissions = keystorePermissions;
	}
	
	@Override
	public String perform(DeployTaskInfo taskInfo) throws IOException, InterruptedException {
		taskInfo.interaction.printf("Performing RSA key import for to Java Key Store as alias '%s' on server %s", alias, taskInfo.host.getFullName());
		taskInfo.interaction.printf("Step 1: Downloading JKS keystore");

		//Get the key store stream
		InputStream in;
		HeapBufferStream bufferStream = new HeapBufferStream();
		if(UploadTask.checkFileUsingCmd(taskInfo.host, taskInfo.passwordCache, taskInfo.interaction, taskInfo.sshClientCache, taskInfo.user, keyStorePath)) {
			taskInfo.interaction.printf("Retrieving contents of keystore at '%1$s'", keyStorePath);
			UploadTask.readFileUsingTmp(taskInfo.host, taskInfo.passwordCache, taskInfo.interaction, taskInfo.sshClientCache, taskInfo.user, keyStorePath, bufferStream.getOutputStream(), null);
			in = bufferStream.getInputStream();
		} else {
			in = null;
		}
		
		// Get the key files from usaapp01
		File rsaFileForInclusion = null; 
		File rsaPublicKeyForInclusion = null;
		
		taskInfo.interaction.printf("Step 2: Load CLEAR TEXT, PRIVATE KEY/PUBLIC CERR pair of files for alias '%s'.", alias);
		if(!this.pkFile.exists() || !this.certFile.exists())
		{
			taskInfo.interaction.printf("Step 2a: Please browse for CLEAR TEXT, PRIVATE KEY/PUBLIC CERR pair of files for alias '%s'.", alias);
			try {
				
				rsaFileForInclusion = taskInfo.interaction.browseForFile("Select PRIVATE RSA CLEARTEXT keyfile", this.pkFile.getPath(), "Please identify the file likely named '%s' for upload.", this.pkFile.getPath()).get();
				rsaPublicKeyForInclusion = taskInfo.interaction.browseForFile("Select PUBLIC RSA keyfile", this.certFile.getPath(), "Please identify the file likely named '%s' for upload.", this.certFile.getPath()).get();
			}
			catch(ExecutionException ex)
			{
				throw new IOException(ex);
			}			
			if(!rsaFileForInclusion.canRead()) throw new FileNotFoundException(rsaFileForInclusion.getAbsolutePath());
			if(!rsaPublicKeyForInclusion.canRead()) throw new FileNotFoundException(rsaPublicKeyForInclusion.getAbsolutePath());
		}
		else
		{
			taskInfo.interaction.printf("===========================================================");
			taskInfo.interaction.printf("====== Found encryption key files for alias '%s'.", alias);
			taskInfo.interaction.printf("===========================================================");
			rsaFileForInclusion = pkFile;
			rsaPublicKeyForInclusion = certFile;
		}

		// RSA Key pair found.
		
		taskInfo.interaction.printf("Step 4: Supply passwords for the JKS Keystore itself, and the key we will store there.");
		//get the password for the keystore	
		char[] ksPassword = DeployUtils.getPassword(cache, taskInfo.host, "keystore", "the keystore at '" + keyStorePath + "'", taskInfo.interaction);
		// get the password for the private key
		// WARNING: We cannot allow this because the SUN JKS provider for SSL will break if different passwords are used.
		// char[] keyPassword = DeployUtils.getPassword(cache, taskInfo.host, alias, "the '" + alias + "' private key in the keystore at '" + keyStorePath + "'", taskInfo.interaction);
 
		//Decode with password
		try {
			taskInfo.interaction.printf("Step 5: Loading JKS Keystore");
			KeyStore keyStore = SecurityUtils.getKeyStore(in, "JKS", null, ksPassword);
			
				KeyStore.Entry ks = keyStore.getEntry(alias, new KeyStore.PasswordProtection(ksPassword));
				if(replace || ks == null || !(ks instanceof KeyStore.PrivateKeyEntry)) {

					taskInfo.interaction.printf("Step 6: Performing key import...");
					CryptUtils.importUnencryptedPKCS8DERPrivateKey(
							keyStore,
							alias, 
							new FileInputStream(rsaFileForInclusion), 
							ksPassword, 
							new FileInputStream(rsaPublicKeyForInclusion)
					);
					
					taskInfo.interaction.printf("===========================================================");
				taskInfo.interaction.printf("Private Key imported. Saving the keystore");
					
					bufferStream.clear();						
					keyStore.store(bufferStream.getOutputStream(), ksPassword);
					
					if(taskInfo.getSsh() != null && taskInfo.getSftp() != null)
					{
						UploadTask.writeFileUsingTmp(taskInfo, bufferStream.getInputStream(), keystorePermissions, "root", "usat", null, keyStorePath);
					}
					else
					{
						// for testing, write locally.
						IOUtils.copy(bufferStream.getInputStream(), new FileOutputStream(new File(LOCAL_KEY_STORE)));
					}
				return "Imported private key into keystore";
			} else if(replace == false && ks instanceof KeyStore.PrivateKeyEntry) {
					taskInfo.interaction.printf("===========================================================");
					taskInfo.interaction.printf("** Alias already exists in the key store, and 'replace=true'");
					taskInfo.interaction.printf("** parameter not used.");
					taskInfo.interaction.printf("===========================================================");
				// taskInfo.interaction.readLine("Enter anything to acknowledge.");
				return "Private key already exists";
				}
			//}
			return "Entry not recognized";
		} catch(KeyStoreException e) {
			throw new IOException(e);
		} catch(NoSuchProviderException e) {
			throw new IOException(e);
		} catch(NoSuchAlgorithmException e) {
			throw new IOException(e);
		} catch(CertificateException e) {
			throw new IOException(e);
		} catch (UnrecoverableEntryException e) {
			throw new IOException(e);
		}			
	}

	@Override
	public String getDescription(DeployTaskInfo deployTaskInfo) {
		StringBuilder sb = new StringBuilder();
		sb.append("Importing user selected private key as '").append(alias).append("' into keystore ").append(keyStorePath);
		return sb.toString();
	}
}
