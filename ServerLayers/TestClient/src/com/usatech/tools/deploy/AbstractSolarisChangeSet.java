package com.usatech.tools.deploy;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.tools.ant.filters.TokenFilter;
import org.apache.tools.ant.filters.TokenFilter.ReplaceRegex;

import simple.io.BufferStream;
import simple.io.EncodingInputStream;
import simple.io.HeapBufferStream;
import simple.util.CaseInsensitiveHashMap;

public abstract class AbstractSolarisChangeSet implements ChangeSet {
	//private static final Log log = Log.getLog();
	protected final Set<App> apps = new HashSet<App>();
	protected final Map<String, Set<App>> appMap = new CaseInsensitiveHashMap<Set<App>>();
	protected final Map<String, Integer[]> ordMap = new CaseInsensitiveHashMap<Integer[]>();
	protected final File baseDir = new File(System.getProperty("rootSourceDir", "D:\\Development\\Java Projects"));
	protected final File buildsDir = new File(baseDir, "ApplicationBuilds");
	protected final Map<String, String> appVersionMap = new CaseInsensitiveHashMap<String>();
	protected App APP_LAYER = new App("applayer", "APR", "ApplicationBuilds/AppLayer", "applayer-", 50, 910, "App Layer", "AppLayerService", "app_layer");
	protected App INAUTH_LAYER = new App("inauthlayer", "APR", "ApplicationBuilds/AuthorityLayer", "authoritylayer-inside-", 70, 903, "Inside Authority Layer", "InsideAuthorityLayerService", "auth_layer");
	protected App POSM_LAYER = new App("posmlayer", "APR", "ApplicationBuilds/POSMLayer", "posmlayer-", 0, 918, "POSM Layer", "POSMLayerService", "posm_layer");
	protected App NET_LAYER = new App("netlayer", "NET", "ApplicationBuilds/NetLayer", "netlayer-", 40, 902, "Network Layer", "NetLayerService", "net_layer");
	protected App OUTAUTH_LAYER = new App("outauthlayer", "NET", "ApplicationBuilds/AuthorityLayer", "authoritylayer-outside-", 80, 903, "Outside Authority Layer", "OutsideAuthorityLayerService", "auth_layer");
	
	protected App POSTGRES = new App("postgres", "MST", null, null, 5432, 90, "PostgreSQL", null, "postgres");
	protected int instanceCount = 5;
	
	protected App USALIVE_APP = new App("usalive", "APP_OLD", "ApplicationBuilds/USALive", "usalive-", 1100, 951, "USALive", "USALive", "usalive");
	
	protected App RPT_GEN = new App("rptgen", "RPT_OLD", "ApplicationBuilds/ReportGenerator", "report-generator-", 10, 921, "Report Generator", "ReportGeneratorService", "rptgen");
	protected App RPT_TRAN = new App("transport", "XFR", "ApplicationBuilds/ReportGenerator", "transporter-", 20, 922, "Report Transporter", "TransportService", "transport");
	
	protected AbstractSolarisChangeSet() {
		registerApps();
		registerOrds();
	}
	protected void registerApp(App app) {
		if(apps.add(app)) {
			Set<App> stApps = appMap.get(app.getServerType());
			if(stApps == null) {
				stApps = new HashSet<App>();
				appMap.put(app.getServerType(), stApps);
			}
			stApps.add(app);
		}
	}
	protected void registerApps() {
		registerApp(APP_LAYER);
		registerApp(INAUTH_LAYER);
		registerApp(POSM_LAYER);
		registerApp(NET_LAYER);
		registerApp(OUTAUTH_LAYER);
		registerApp(POSTGRES);
	}
	protected void registerOrds() {
		ordMap.put("LOCAL", new Integer[] {null});
		Integer[] ords = new Integer[instanceCount];
		for(int i = 0; i < ords.length; i++)
			ords[i] = i+1;
		ordMap.put("DEV", ords);
		ordMap.put("INT", ords);
		ordMap.put("ECC", ords);
		ordMap.put("USA", new Integer[]{null});
	}
	@Override
	public DeployTask[] getTasks(Host host, PasswordCache cache) throws IOException {
		if(!isApplicable(host))
			return null;
		List<DeployTask> tasks = new ArrayList<DeployTask>();
		List<String> cmds = new ArrayList<String>();
		addTasks(host, tasks, cmds, cache);
		for(String serverType : host.getServerTypes()) {
			Set<App> apps = appMap.get(serverType);
			Integer[] ords = ordMap.get(host.getServerEnv());
			if(apps != null && ords != null) {
				for(Integer ord : ords)
					for(App app : apps)
						addTasks(host, app, ord, tasks, cmds, cache);
			}
		}
		if(!cmds.isEmpty())
			tasks.add(new ExecuteTask(cmds.toArray(new String[cmds.size()])));
		return tasks.toArray(new DeployTask[tasks.size()]);
	}

	protected abstract void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException ;
	protected abstract void addTasks(Host host, App app, Integer ordinal, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException ;
	
	@Override
	public boolean isApplicable(Host host) {
		if(!ordMap.containsKey(host.getServerEnv()))
			return false;
		for(String serverType : host.getServerTypes()) {
			if(appMap.containsKey(serverType))
				return true;
		}
		return false;
	}
	@Override
	public String toString() {
		return getName();
	}
	protected void addPrepareHostTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		List<String> subcommands = new ArrayList<String>();
		subcommands.add("sudo su");
		//usat:x:900:900:USAT Applications:/home/usat:/bin/bash
		//Make group
		subcommands.add("grep -c 'usat:' /etc/group || groupadd -g 900 usat");
		//Make directory
		subcommands.add("test -d /opt/USAT/conf || (mkdir -p /opt/USAT/conf && chown root:root /opt/USAT && chown root:usat /opt/USAT/conf)");
		
		//Upload log4j.properties, jmx.access, jmx.login.config, translations.properties
		File confDir = new File(buildsDir.getParentFile(), "LayersCommon/conf");
		tasks.add(new UnixUploadTask(new File(confDir, "log4j.properties"), 0640, true));
		subcommands.add(getCopyCommand(host, "log4j.properties", "/opt/USAT/conf"));
		File jmxLoginConf = new File(confDir, "jmx.login.config_" + host.getServerEnv().toLowerCase());
		tasks.add(new UnixUploadTask(new FileInputStream(jmxLoginConf), null, "jmx.login.config", 0640, null, null, true, jmxLoginConf.getCanonicalPath()));
		subcommands.add(getCopyCommand(host, "jmx.login.config", "/opt/USAT/conf"));
		File translations = new File(buildsDir.getParentFile(), "LayersCommon/src/translations" + ("DEV".equalsIgnoreCase(host.getServerEnv()) || "INT".equalsIgnoreCase(host.getServerEnv()) ? "-debug" : "") + ".properties");
		tasks.add(new UnixUploadTask(new FileInputStream(translations), null, "translations.properties", 0640, null, null, true, translations.getCanonicalPath()));
		subcommands.add(getCopyCommand(host, "translations.properties", "/opt/USAT/conf"));
		
		BufferStream buffer = new HeapBufferStream();
		PrintStream ps = new PrintStream(buffer.getOutputStream());
		ps.println("# The \"monitorRole\" role has readonly access.");
		ps.println("# The \"controlRole\" role has readwrite access.");
		ps.println("monitorRole readonly");
		ps.println("controlRole readwrite");
		String [] users = getJmxUsers(host);
		if(users != null)
			for(String user : users) {
				ps.print(user);
				ps.println(" readwrite");
			}
		ps.flush();
		tasks.add(new UnixUploadTask(buffer.getInputStream(), null, "jmx.access", 0640, null, null, true, "Access for users " + Arrays.toString(users)));
		subcommands.add(getCopyCommand(host, "jmx.access", "/opt/USAT/conf"));

		//Create keystore.ks and truststore.ts
		tasks.add(new ExecuteTask("sudo su", "cp /opt/USAT/conf/keystore.ks ~", "chown `logname` ~/keystore.ks","cp /opt/USAT/conf/truststore.ts ~", "chown `logname` ~/truststore.ts"));
		tasks.add(new RenewJKSCertTask(new Date(System.currentTimeMillis() + 30 * 24 * 60 * 60 * 1000L), "keystore.ks", cache, 0640));
		tasks.add(new ImportJKSCertTask("usatrootca", new File(confDir, "usat_root.crt"), "truststore.ts", cache, 0640, false));
		
		subcommands.add("diff -b ~/keystore.ks /opt/USAT/conf/keystore.ks || (cp ~/keystore.ks /opt/USAT/conf/keystore.ks)");
		subcommands.add("rm ~/keystore.ks");
		subcommands.add("diff -b ~/truststore.ts /opt/USAT/conf/truststore.ts || (cp ~/truststore.ts /opt/USAT/conf/truststore.ts)");
		subcommands.add("rm ~/truststore.ts");
		
		subcommands.add("chown -R root:usat /opt/USAT/conf");
		
		tasks.add(new UnixUploadTask(new File(buildsDir.getParentFile(), "server_app_config/common/archive"), 0555, true));
		subcommands.add("chown root:root ~/archive");
		subcommands.add(getCopyCommand(host, "archive", "/usr/local/USAT/bin"));
		if(!"USA".equalsIgnoreCase(host.getServerEnv())) {
			IPFUpdateFileTask ipfTask = new IPFUpdateFileTask(true);
			ipfTask.setFilePath("ipf.conf");
			String aprHost;
			String netHost;
			if("DEV".equalsIgnoreCase(host.getServerEnv())) {
				aprHost = "10.0.0.248";
				netHost = "10.0.0.253";
			} else if("INT".equalsIgnoreCase(host.getServerEnv())) {
				aprHost = "10.0.0.246";
				netHost = "10.0.0.245";
			} else if("ECC".equalsIgnoreCase(host.getServerEnv())) {
				aprHost = "192.168.4.41";
				netHost = "192.168.4.61";
			} else {
				aprHost = null;
				netHost = null;
			}
			if(host.isServerType("NET")) {
				allowAppPorts(ipfTask, NET_LAYER, 5, aprHost);
				allowAppPorts(ipfTask, OUTAUTH_LAYER, 5, aprHost, netHost);	
				ipfTask.allowTcp("any", 14507);
				ipfTask.allowTcp("any", 14508);
				ipfTask.allowTcp("any", 14509);
				ipfTask.allowTcp("any", 14543);
				ipfTask.allowTcp("any", 14568);
			} else if(host.isServerType("APR")) {
				allowAppPorts(ipfTask, APP_LAYER, 5, aprHost, netHost);
				allowAppPorts(ipfTask, INAUTH_LAYER, 5, aprHost, netHost);	
				allowAppPorts(ipfTask, POSM_LAYER, 5, aprHost);	
				ipfTask.allowTcp("any", 15527);
			} 
			tasks.add(new ExecuteTask("sudo su", "cp /etc/ipf/ipf.conf ~", "chmod 0666 ~/ipf.conf"));
			tasks.add(ipfTask);
			subcommands.add("diff -b ~/ipf.conf /etc/ipf/ipf.conf || (chmod 0444 ~/ipf.conf && cp ~/ipf.conf /etc/ipf/ipf.conf && /usr/sbin/ipf -Fa -f /etc/ipf/ipf.conf)");
		}
		if(!subcommands.isEmpty())
			tasks.add(new ExecuteTask(subcommands.toArray(new String[subcommands.size()])));
	}
	protected void allowAppPorts(IPFUpdateFileTask ipfTask, App app, Integer ordinal, String... ips) {
		ipfTask.allowTcp("any", 7001 + (ordinal == null ? 7 : ordinal) * 100 + app.getPortOffset());
		ipfTask.allowTcp("any", 7002 + (ordinal == null ? 7 : ordinal) * 100 + app.getPortOffset());
		if(ips != null)
			for(String ip : ips) {
				if(ip != null)
					ipfTask.allowTcp(ip, 7007 + (ordinal == null ? 700 : 10000 + ordinal * 100) + app.getPortOffset());
			}
	}
	protected String getCopyCommand(Host host, String remoteName, String remoteDir) {
		return getCopyCommand(host, remoteName, remoteName, remoteDir);	
	}
	protected String getCopyCommand(Host host, String sourceName, String targetName, String targetDir) {
		if(overrideExisting(host, null, null, targetName))
			return "cp ~/" + sourceName + " " + targetDir + "/" + targetName;
		return "test -r " + targetDir + "/" + targetName + " || cp ~/" + sourceName + " " + targetDir + "/" + targetName;
	}
	protected String[] getJmxUsers(Host host) {
		if("USA".equalsIgnoreCase(host.getServerEnv())) {
			return new String[] {
					"aroyce",
					"mnjanje",
					"dkouznetsov",
					"bkrug",
					"phorsfield",
					"rtview",
					"kchigurupati",
					"jlucchesi",
					"jolsen"};
		} else if("ECC".equalsIgnoreCase(host.getServerEnv())) {
			return new String[] {
					"aroyce-ecc",
					"mnjanje-ecc",
					"dkouznetsov-ecc",
					"phorsfield-ecc",
					"bkrug-ecc"};
		} else if("INT".equalsIgnoreCase(host.getServerEnv())) {
			return new String[] {
					"aroyce",
					"mnjanje",
					"dkouznetsov",
					"phorsfield",
					"bkrug"};
		} else if("DEV".equalsIgnoreCase(host.getServerEnv())) {
			return new String[] {
					"aroyce",
					"mnjanje",
					"dkouznetsov",
					"phorsfield",
					"bkrug"};
		} 
		return null;		
	}
	protected void addPrepareAppTasks(Host host, final App app, final Integer ordinal, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		List<String> subcommands = new ArrayList<String>();
		subcommands.add("sudo su");
		// Make home dir
		subcommands.add("test -d /home/" + app.getName() + " || (mkdir /home/" + app.getName() + " && chown " + app.getName() + ":" + app.getName() + " /home/" + app.getName() + ")");

		//Make user
		subcommands.add("grep -c '" + app.getName() + ":' /etc/passwd || (groupadd -g " + app.getAppUid() + " " + app.getName()
				+ " && useradd -K type=role -c '" + app.appDesc + "' -d /home/" + app.getName() + " -g " 
				+ app.getAppUid() + " -G usat -m -u " + app.getAppUid() + " " + app.getName() + ")");

		//Ensure user is role
		subcommands.add("grep -c '" + app.getName() + ":\\*LK\\*' /etc/shadow && (grep -c '"
				+ app.getName() + ".*type=role' /etc/user_attr || (echo 'Please set the blank password for role "
				+ app.getName() + "' && passwd " + app.getName() + " && passwd -x -1 " + app.getName() + "))");
		                                                       	
		//Make directories
		String appDir = "/opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal);
		subcommands.add("test -d " + appDir + " || (mkdir " + appDir + " && chown " + app.getName() + ":" + app.getName() + " " + appDir + ")");
		
		//Upload SMF files also
		for(String serverType : host.getServerTypes()) {
			File smfDir = new File(buildsDir.getParentFile(), "server_app_config/" + serverType + "/SMF");
			if(!smfDir.canRead())
				throw new IllegalStateException("Could not find SMF directory '" + smfDir.getAbsolutePath() + "' in file system");
			String smfName;
			if("postgres".equalsIgnoreCase(app.getName()))
				smfName = "postgresql";
			else
				smfName = app.getName();
			File manifest = new File(smfDir, smfName + ".xml");
			File method = new File(smfDir, "USAT-" + smfName);
			tasks.add(new UnixUploadTask(method, 750, true));
			String manifestRemoteName;
			InputStream manifestContent;
			if(ordinal == null) {
				manifestRemoteName = manifest.getName();
				manifestContent = new FileInputStream(manifest);
			} else {
				manifestRemoteName = smfName + ordinal + ".xml";
				TokenFilter tf = new TokenFilter(new FileReader(manifest));
				ReplaceRegex rr = new ReplaceRegex();
				rr.setPattern("([\"'])((?:/opt|site)/USAT/" + app.getName() + ")\\1");
				rr.setReplace("\\1\\2" + ordinal + "\\1");
				tf.addReplaceRegex(rr);
				rr = new ReplaceRegex();
				rr.setPattern("(<service_bundle[^>]*\\sname=)([\"'])(" + app.getName() + ")\\2");
				rr.setReplace("\\1\\2\\3" + ordinal + "\\2");
				tf.addReplaceRegex(rr);
				manifestContent = new EncodingInputStream(tf);
			}
			tasks.add(new UploadTask(manifestContent, 640, null, null, manifest.getCanonicalPath(), true, manifestRemoteName));
			subcommands.add("diff -b ~/" + method.getName() + " /lib/svc/method/" + method.getName()
					+ " || (cp ~/" + method.getName() + " /lib/svc/method/" + method.getName()
					+ " && chown root:root /lib/svc/method/" + method.getName() + ")");
			subcommands.add("diff -b ~/" + manifestRemoteName + " /var/svc/manifest/site/USAT/" + manifestRemoteName
					+ " || (cp ~/" + manifestRemoteName + " /var/svc/manifest/site/USAT/" + manifestRemoteName
					+ " && chown root:root /var/svc/manifest/site/USAT/" + manifestRemoteName 
					+ " && svccfg -v import /var/svc/manifest/site/USAT/"+ manifestRemoteName + ")");
		}
		if(!"postgres".equalsIgnoreCase(app.getName()) && app.getServiceName() != null) {			
			BufferStream buffer = new HeapBufferStream();
			PrintStream ps = new PrintStream(buffer.getOutputStream());
			ps.println("#!/usr/bin/sh");
			ps.print("/usr/local/USAT/bin/archive -v -t=");
			ps.print(appDir);
			ps.print("/logs/");
			ps.print(app.getServiceName());
			ps.print(" ");
			ps.print(appDir);
			ps.print("/logs/");
			ps.print(app.getServiceName());
			ps.println(".log.*");
			ps.println("exit 0");
			ps.flush();
			String fileName = app.getName() + (ordinal == null ? "" : ordinal.toString()) + "_archivelogs.sh";
			tasks.add(new UnixUploadTask(buffer.getInputStream(), null, fileName, 0754, null, null, true, "Log archive script for '" + app.getName() + (ordinal == null ? "" : ordinal.toString()) + "'"));
			subcommands.add("chown " + app.getName() + ":" + app.getName() + " ~/" + fileName);
			subcommands.add(getCopyCommand(host, fileName, "archivelogs.sh", appDir));
			
			subcommands.add("grep -c " + app.getName() + " /etc/cron.d/cron.allow || echo '" + app.getName() + "' >> /etc/cron.d/cron.allow");
			String crontabFile = "/home/" + app.getName() + "/" + app.getName() + ".crontab";
			subcommands.add("crontab -l " + app.getName() + " > " + crontabFile);
			int min = (ordinal == null ? 1 : ordinal) * 8;
			if("inauthlayer".equalsIgnoreCase(app.getName()) || "outauthlayer".equalsIgnoreCase(app.getName()))
				min += 2;
			else if("posmlayer".equalsIgnoreCase(app.getName()))
				min += 4;
			String line = "" + min + " * * * * " + appDir + "/archivelogs.sh >> " + appDir + "/archivelogs.log 2>&1";
			subcommands.add("grep -c '" + line + "' " + crontabFile + " ||  (grep -v '" + appDir + "/archivelogs.sh' " + crontabFile + " > " 
					+ crontabFile + ".NEW && echo '" + line + "' >> " + crontabFile + ".NEW && su " + app.getName() + " -c 'crontab " + crontabFile + ".NEW')");
		}
		if(!subcommands.isEmpty())
			tasks.add(new ExecuteTask(subcommands.toArray(new String[subcommands.size()])));
	}
	protected void addInstallAppTasks(Host host, App app, Integer ordinal, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		DefaultPullTask installFileTask = app.getRetrieveInstallFileTask();
		if(installFileTask != null) {
			// upload zip file
			if(ordinal == null || ordinal == 1) {
				tasks.add(installFileTask);
				tasks.add(new UploadTask(installFileTask.getResultInputStream(), 0644, null, null, app.getInstallFileName(), true, app.getInstallFileName()));
				File confDir = new File(buildsDir.getParentFile(), "LayersCommon/conf");
				tasks.add(new UploadTask(new File(confDir, "setenv.sh"), 0750));
			}
			List<String> subcommands = new ArrayList<String>();
			String baseDir = "/opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal);
			subcommands.add("sudo su");
			subcommands.add("rm -r " + baseDir + "/classes/*");
			subcommands.add("rm -r " + baseDir + "/lib/*");
			// extract zip fill
			subcommands.add("cd " + baseDir);
			subcommands.add("gunzip -c ~/'" + app.getInstallFileName() + "' | tar xvf -");
			subcommands.add("cd ~");
			//copy setup files
			if(overrideExisting(host, app, ordinal, "setenv.sh"))
				subcommands.add("cp setenv.sh "+ baseDir + "/bin/setenv.sh && chown " + app.getName() + ":" + app.getName() + " " + baseDir + "/bin/setenv.sh");		
			else
				subcommands.add("test -x " + baseDir + "/bin/setenv.sh || (cp setenv.sh "+ baseDir + "/bin/setenv.sh && chown " + app.getName() + ":" + app.getName() + " " + baseDir + "/bin/setenv.sh)");		
			//subcommands.add("chown -R " + app.getName() + ":" + app.getName() + " " + baseDir);
			addBeforeStopCommands(host, app, ordinal, subcommands);
			if(!enableAppIfDisabled(host, app, ordinal)) {
				subcommands.add("case `svcs -H -o STA " + app.getName() + (ordinal == null ? "" : ordinal) + "` in ON|ON*) svcadm -v disable -st " + app.getName() + (ordinal == null ? "" : ordinal) + "; SA_RESULT=$?;; *) SA_RESULT=1;; esac");				
			} else {
				subcommands.add("svcadm -v disable -st " + app.getName() + (ordinal == null ? "" : ordinal) + "; SA_RESULT=$?");				
			}
			addBeforeRestartCommands(host, app, ordinal, subcommands);
			subcommands.add("test $SA_RESULT -eq 0 && svcadm -v enable -s " + app.getName() + (ordinal == null ? "" : ordinal));

			tasks.add(new ExecuteTask(subcommands.toArray(new String[subcommands.size()])));			
		}
	}
	protected void addBeforeRestartCommands(Host host, App app, Integer ordinal, List<String> commands) {
		
	}
	protected void addBeforeStopCommands(Host host, App app, Integer ordinal, List<String> commands) {
		
	}
	protected boolean overrideExisting(Host host, App app, Integer ordinal, String filePath) {
		return false;
	}
	protected boolean enableAppIfDisabled(Host host, App app, Integer ordinal) {
		return true;
	}
}
