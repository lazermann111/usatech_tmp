package com.usatech.tools.deploy;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import simple.util.CaseInsensitiveHashMap;

public class BasicServerSet implements ServerSet {
	protected final Map<String, List<Server>> serverMap = new CaseInsensitiveHashMap<List<Server>>();
	protected final Map<String, HostImpl> hosts = new HashMap<String, HostImpl>();
	protected final Collection<? extends Host> unmodHosts = Collections.unmodifiableCollection(hosts.values());
	protected final Set<App> apps = new HashSet<App>();
	protected final Map<String, Set<App>> appMap = new CaseInsensitiveHashMap<Set<App>>();
	protected final Map<App, Set<App>> subAppMap = new HashMap<App, Set<App>>();

	public void registerApp(App app) {
		if(apps.add(app)) {
			getOrCreateAppSet(app.getServerType()).add(app);
			if(app.getServerType() == null)
				for(Set<App> apps : appMap.values())
					apps.add(app);
		}
	}

	public void unregisterApp(App app) {
		if(apps.remove(app)) {
			Set<App> stApps = appMap.get(app.getServerType());
			if(stApps != null)
				stApps.remove(app);
			if(app.getServerType() == null)
				for(Set<App> apps : appMap.values())
					apps.remove(app);
		}
	}

	public void registerSubApps(App app, App... subApps) {
		Set<App> subs = subAppMap.get(app);
		if(subs == null) {
			subs = new HashSet<App>();
			subAppMap.put(app, subs);
		}
		for(App subApp : subApps)
			subs.add(subApp);
	}

	public void unregisterSubApps(App app, App... subApps) {
		Set<App> subs = subAppMap.get(app);
		if(subs == null)
			return;

		for(App subApp : subApps)
			subs.remove(subApp);
	}

	public void clearSubApps(App app) {
		subAppMap.remove(app);
	}

	@Override
	public Server getServer(Host host, String serverType) {
		List<Server> servers = serverMap.get(serverType);
		if(servers != null) {
			for(Server server : servers) {
				if(server.isOnHost(host))
					return server;
			}
		}
		return null;
	}

	@Override
	public List<Server> getServers(Host host) {
		List<Server> result = new ArrayList<Server>();
		for(String serverType : host.getServerTypes()) {
			Server server = getServer(host, serverType);
			if(server != null)
				result.add(server);
		}
		return result;
	}

	@Override
	public List<Server> getServers(String serverType) {
		List<Server> servers = serverMap.get(serverType);
		return servers == null ? null : Collections.unmodifiableList(servers);
	}

	@Override
	public List<Server> getServers(Set<String> serverTypes) {
		List<Server> result = new ArrayList<Server>();
		for(String serverType : serverTypes) {
			List<Server> servers = serverMap.get(serverType);
			if(servers != null)
				result.addAll(servers);
		}
		return result;
	}

	@Override
	public Set<Host> getHosts(String serverType) {
		List<Server> servers = getServers(serverType);
		if(servers == null)
			return null;

		Set<Host> hosts = new LinkedHashSet<Host>(servers.size());
		for(Server server : servers)
			hosts.add(server.host);
		return hosts;
	}

	@Override
	public Set<Host> getHosts(Set<String> serverTypes) {
		List<Server> servers = getServers(serverTypes);
		if(servers == null)
			return null;

		Set<Host> hosts = new LinkedHashSet<Host>(servers.size());
		for(Server server : servers)
			hosts.add(server.host);
		return hosts;
	}

	public void registerServerSet(String serverEnv, String serverType, char seventh, int numServers, int numInstances) throws UnknownHostException {
		for(int i = 0; i < numServers; i++) {
			String[] instances;
			if(numInstances < 2)
				instances = null;
			else {
				instances = new String[numInstances];
				for(int k = 0; k < numInstances; k++)
					instances[k] = String.valueOf(k + 1);
			}
			String hostNameMidPart=null;
			if(serverEnv.equalsIgnoreCase("USA")) {
				if(serverType.endsWith("_OLD"))
					hostNameMidPart = serverType.substring(0, serverType.length() - 4).toLowerCase();
				else
					hostNameMidPart = serverType.toLowerCase();
			} else {
				if(serverType.equalsIgnoreCase("APP")) {
					hostNameMidPart = "apr";
				} else if(serverType.equalsIgnoreCase("WEB")) {
					hostNameMidPart = "net";
				} else if(serverType.equalsIgnoreCase("MSR")) {
					hostNameMidPart = "mst";
				} else if(serverType.equalsIgnoreCase("MON")) {
					hostNameMidPart = "mst"; // TODO: for right now
				} else if(serverType.endsWith("_OLD")) {
					hostNameMidPart = serverType.substring(0, serverType.length() - 4).toLowerCase();
				} else {
					hostNameMidPart = serverType.toLowerCase();
				}
			}
			registerServerSet(serverEnv, serverType, serverEnv.toLowerCase() + hostNameMidPart + seventh + (i + 1), instances);
		}
	}

	public void registerServerSet(String serverEnv, String serverType, String hostName, String... instances) throws UnknownHostException {
		registerServerSet(serverEnv, serverType, hostName, false, instances);
	}

	public void registerServerSet(String serverEnv, String serverType, String hostName, boolean fake, String... instances) throws UnknownHostException {
		List<Server> servers = serverMap.get(serverType);
		if(servers == null) {
			servers = new ArrayList<Server>();
			serverMap.put(serverType, servers);
		}
		HostImpl host = getHost(hostName, fake, serverEnv);
		host.addServerType(serverType);
		servers.add(new Server(host, serverType, getOrCreateAppSet(serverType), instances));
	}

	protected HostImpl getHost(String hostName, boolean fake, String serverEnv) {
		String fullName;
		String lookup;
		if(fake) {
			fullName = "";
			lookup = "~" + hostName;
		} else {
			int pos = hostName.indexOf('.');
			if(pos > 0) {
				fullName = hostName;
			} else {
				if("USA".equalsIgnoreCase(serverEnv))
					fullName = hostName + ".trooper.usatech.com";
				else
					fullName = hostName + ".usatech.com";
			}
			lookup = fullName;
		}
		HostImpl host = hosts.get(lookup);
		if(host == null) {
			host = new HostImpl(serverEnv, fullName);
			hosts.put(lookup, host);
		}
		return host;
	}

	public Set<App> getApps(String serverType) {
		Set<App> apps = appMap.get(serverType);
		if(apps != null)
			apps = Collections.unmodifiableSet(apps);
		return apps;
	}

	protected Set<App> getOrCreateAppSet(String serverType) {
		Set<App> apps = appMap.get(serverType);
		if(apps == null) {
			apps = new HashSet<App>();
			appMap.put(serverType, apps);
			if(serverType != null) {
				Set<App> defaultapps = appMap.get(null);
				if(defaultapps != null)
					apps.addAll(defaultapps);
			}
		}
		return apps;
	}

	public Collection<? extends Host> getHosts() {
		return unmodHosts;
	}

	@Override
	public Set<App> getSubApps(App app) {
		Set<App> subApps = subAppMap.get(app);
		if(subApps == null)
			subApps = Collections.emptySet();
		return subApps;
	}
}
