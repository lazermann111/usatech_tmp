package com.usatech.tools.deploy;

public class ServerEnvHostSpecificValue extends MappedHostSpecificValue {
	public ServerEnvHostSpecificValue(String devValue, String intValue, String eccValue, String prodValue) {
		registerValue("DEV", (String) null, devValue);
		registerValue("INT", (String) null, intValue);
		registerValue("ECC", (String) null, eccValue);
		registerValue("USA", (String) null, prodValue);
	}
	public ServerEnvHostSpecificValue(String devValue, String intValue, String eccValue, HostSpecificValue prodValue) {
		registerValue("DEV", (String) null, devValue);
		registerValue("INT", (String) null, intValue);
		registerValue("ECC", (String) null, eccValue);
		registerValue("USA", (String) null, prodValue);
	}
	public ServerEnvHostSpecificValue(String devValue, String intValue, HostSpecificValue eccValue, HostSpecificValue prodValue) {
		registerValue("DEV", (String) null, devValue);
		registerValue("INT", (String) null, intValue);
		registerValue("ECC", (String) null, eccValue);
		registerValue("USA", (String) null, prodValue);
	}
	public ServerEnvHostSpecificValue(String devValue, HostSpecificValue intValue, HostSpecificValue eccValue, HostSpecificValue prodValue) {
		registerValue("DEV", (String) null, devValue);
		registerValue("INT", (String) null, intValue);
		registerValue("ECC", (String) null, eccValue);
		registerValue("USA", (String) null, prodValue);
	}
	public ServerEnvHostSpecificValue(HostSpecificValue devValue, HostSpecificValue intValue, HostSpecificValue eccValue, HostSpecificValue prodValue) {
		registerValue("DEV", (String) null, devValue);
		registerValue("INT", (String) null, intValue);
		registerValue("ECC", (String) null, eccValue);
		registerValue("USA", (String) null, prodValue);
	}
}
