package com.usatech.tools.deploy;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import simple.text.StringUtils;

public abstract class AbstractSolarisPatchChangeSet extends AbstractSolarisChangeSet {
	protected Set<File> localFiles = new LinkedHashSet<File>();
	protected Set<String> appDirs = new LinkedHashSet<String>();
	protected Set<File> appFiles = new LinkedHashSet<File>();
	protected Set<File> srcFiles = new LinkedHashSet<File>();
	protected boolean filesAdded = false;
	public AbstractSolarisPatchChangeSet() {
		super();
	}
	protected void registerClass(File compiledDirectory, final Class<?> clazz) {
		final String simpleName = clazz.getSimpleName();
		final String packageDir = clazz.getPackage().getName().replace('.', '/');
		File[] files = new File(compiledDirectory, packageDir).listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.startsWith(simpleName) && name.endsWith(".class") && (name.length() == simpleName.length() + 6 || name.charAt(simpleName.length()) == '$');
			}
		});
		if(files == null || files.length == 0)
			throw new IllegalArgumentException("Class '" + clazz.getName() + "' not found in directory '" + compiledDirectory.getAbsolutePath() + "'");
		for(File f : files) {
			localFiles.add(f);
			appDirs.add(packageDir);
			appFiles.add(new File(packageDir, f.getName()));
		}
	}

	protected void registerSource(File sourceDirectory, final Class<?> clazz) {
		final String simpleName = clazz.getSimpleName();
		final String packageDir = clazz.getPackage().getName().replace('.', '/');
		File file = new File(sourceDirectory, packageDir + "/" + simpleName + ".java");
		if(!file.exists())
			throw new IllegalArgumentException("Source for '" + clazz.getName() + "' not found in directory '" + sourceDirectory.getAbsolutePath() + "'");
		localFiles.add(file);
		appDirs.add(packageDir);
		srcFiles.add(new File(packageDir, file.getName()));
	}

	protected void registerResource(File sourceDirectory, final String resourcePath) {
		File file = new File(sourceDirectory, resourcePath);
		if(!file.canRead())
			throw new IllegalArgumentException("Resource '" + resourcePath + "' not found in directory '" + sourceDirectory.getAbsolutePath() + "'");
		localFiles.add(file);
		String dir = StringUtils.substringBeforeLast(resourcePath, "\\/".toCharArray());
		if(dir != null && (dir=dir.trim()).length() > 0)
			appDirs.add(dir);
		appFiles.add(new File(dir, file.getName()));
	}
	@Override
	protected void addTasks(Host host, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		filesAdded = false;
		// Do nothing		
	}
	@Override
	protected void addTasks(Host host, App app, Integer ordinal, List<DeployTask> tasks, List<String> commands, PasswordCache cache) throws IOException {
		if(!filesAdded) {
			for(File f : localFiles) {
				tasks.add(new UploadTask(f, 0644));
			}
			filesAdded = true;
		}
		String baseDir = "/opt/USAT/" + app.getName() + (ordinal == null ? "" : ordinal.toString());
		if(commands.isEmpty())
			commands.add("sudo su");
		String classesDir = baseDir + "/classes/";
		for(String dir : appDirs)
			commands.add("mkdir -p " + classesDir + dir);
		for(File f : appFiles) {
			commands.add("cp '" + f.getName() + "' " + classesDir + f.getParent().replace(File.separatorChar, '/'));
		}
		if(!srcFiles.isEmpty()) {
			commands.add("cd " + baseDir);
			StringBuilder sb = new StringBuilder();
			sb.append("/usr/jdk/latest/bin/javac -g -cp $(grep \"CLASSPATH=\" ").append(baseDir);
			sb.append("/bin/").append(app.getServiceName()).append(".sh | cut -d= -f2-) -d ").append(classesDir);
			for(File f : srcFiles) {
				sb.append(" /home/`logname`/").append(f.getName());
			}
			commands.add(sb.toString());
		}
		// commands.add("svcadm restart " + app.getName() + (ordinal == null ? "" : ordinal.toString()));
		addBeforeStopCommands(host, app, ordinal, commands);
		if(!enableAppIfDisabled(host, app, ordinal)) {
			commands.add("case `svcs -H -o STA " + app.getName() + (ordinal == null ? "" : ordinal) + "` in ON|ON*) svcadm -v disable -st " + app.getName() + (ordinal == null ? "" : ordinal) + "; SA_RESULT=$?;; *) SA_RESULT=1;; esac");
		} else {
			commands.add("svcadm -v disable -st " + app.getName() + (ordinal == null ? "" : ordinal) + "; SA_RESULT=$?");
		}
		addBeforeRestartCommands(host, app, ordinal, commands);
		commands.add("test $SA_RESULT -eq 0 && svcadm -v enable -s " + app.getName() + (ordinal == null ? "" : ordinal));

	}
}
