/**
 * 
 */
package com.usatech.tools.deploy;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import simple.bean.ConvertUtils;
import simple.io.HeapBufferStream;
import simple.io.StartAtPrefixInputStream;
import simple.security.crypt.CryptUtils;
import simple.text.StringUtils;
import sun.security.x509.X500Name;

import com.sshtools.j2ssh.sftp.FileAttributes;


public class RenewFileCertTask implements DeployTask {
	protected final Date expiration;
	protected final String certificatePath;
	protected final String rootCertPath;
	protected final String privateKeyPath;
	protected final int certificatePermissions;
	protected final int privateKeyPermissions;
	protected final String owner;
	protected final String group;
	protected final String overrideHostName;
	protected final String templateName;
	protected String commonName;
	protected String signUser;
	protected final Set<Host> copyToHosts = new LinkedHashSet<Host>();
	protected final Set<Host> copyToHostsUnmod = Collections.unmodifiableSet(copyToHosts);
	protected boolean checkExpiration=true;
	
	
	
	public boolean isCheckExpiration() {
		return checkExpiration;
	}

	public void setCheckExpiration(boolean checkExpiration) {
		this.checkExpiration = checkExpiration;
	}

	public RenewFileCertTask(Date expiration, String certificatePath, String privateKeyPath, int certificatePermissions, int privateKeyPermissions, String owner, String group) {
		this(expiration, certificatePath, null, privateKeyPath, certificatePermissions, privateKeyPermissions, owner, group, null);
	}

	public RenewFileCertTask(Date expiration, String certificatePath, String rootCertPath, String privateKeyPath, int certificatePermissions, int privateKeyPermissions, String owner, String group) {
		this(expiration, certificatePath, rootCertPath, privateKeyPath, certificatePermissions, privateKeyPermissions, owner, group, null);
	}
	public RenewFileCertTask(Date expiration, String certificatePath, String privateKeyPath, int certificatePermissions, int privateKeyPermissions, String owner, String group, String overrideHostName) {
		this(expiration, certificatePath, null, privateKeyPath, certificatePermissions, privateKeyPermissions, owner, group, overrideHostName, owner, group, overrideHostName);
	}

	public RenewFileCertTask(Date expiration, String certificatePath, String rootCertPath, String privateKeyPath, int certificatePermissions, int privateKeyPermissions, String owner, String group, String overrideHostName) {
		this.expiration = expiration;
		this.certificatePath = certificatePath;
		this.rootCertPath = rootCertPath;
		this.privateKeyPath = privateKeyPath;
		this.certificatePermissions = certificatePermissions;
		this.privateKeyPermissions = privateKeyPermissions;
		this.owner = owner;
		this.group = group;
		this.overrideHostName = overrideHostName;
		this.templateName="USATWebServer";
	}
	public RenewFileCertTask(Date expiration, String certificatePath, String privateKeyPath, int certificatePermissions, int privateKeyPermissions, String owner, String group, String overrideHostName, String signUser, String templateName, String commonName) {
		this(expiration, certificatePath, null, privateKeyPath, certificatePermissions, privateKeyPermissions, owner, group, overrideHostName,signUser, templateName, commonName);
	}

	public RenewFileCertTask(Date expiration, String certificatePath, String rootCertPath, String privateKeyPath, int certificatePermissions, int privateKeyPermissions, String owner, String group, String overrideHostName, String signUser, String templateName, String commonName) {
		this.expiration = expiration;
		this.certificatePath = certificatePath;
		this.rootCertPath = rootCertPath;
		this.privateKeyPath = privateKeyPath;
		this.certificatePermissions = certificatePermissions;
		this.privateKeyPermissions = privateKeyPermissions;
		this.owner = owner;
		this.group = group;
		this.overrideHostName = overrideHostName;
		this.signUser=signUser;
		this.templateName=templateName;
		this.commonName=commonName;
	}


	@Override
	public String perform(DeployTaskInfo deployTaskInfo) throws IOException, InterruptedException {
		//get the alias		
		String hostname = overrideHostName != null && overrideHostName.length() > 0 ? overrideHostName : deployTaskInfo.host.getFullName();
		//TODO: For now use buffer stream, but consider updating SftpClient or using SftpSubsystem to stream data
		Host[] hosts = this.copyToHosts.toArray(new Host[copyToHosts.size()]);
		HeapBufferStream bufferStream = new HeapBufferStream(hosts.length > 0);
		try {
			boolean expired = false;
			CertificateFactory cf = CertificateFactory.getInstance("X509");
			if(isCheckExpiration()){
				if(UploadTask.checkFileUsingCmd(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, certificatePath)) {
					deployTaskInfo.interaction.printf("Retrieving contents of keystore at '%1$s'", certificatePath);
					UploadTask.readFileUsingTmp(deployTaskInfo.host, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, certificatePath, bufferStream.getOutputStream(), null);
					InputStream in;
					if(privateKeyPath.equals(certificatePath))
						in = new StartAtPrefixInputStream(bufferStream.getInputStream(), "-----BEGIN CERTIFICATE-----");
					else
						in = bufferStream.getInputStream();
					Collection<? extends Certificate> certs = cf.generateCertificates(in);
					boolean first = true;
					for(Certificate cert : certs) {
						if(cert instanceof X509Certificate) {
							X509Certificate x509 = (X509Certificate) cert;
							Date expiredDate = x509.getNotAfter();
							String cn = ((X500Name) x509.getSubjectDN()).getCommonName();
							if(expiration.after(expiredDate) || (first && !cn.equals(commonName == null ? hostname : commonName))) {
								expired = true;
								break;
							}
							first = false;
						}
					}
				} else {
					expired = true;
				}
			}else{
				expired = true;
			}
			// check if expired
			if(expired) {
				//Create new entry
				StringBuilder csr = new StringBuilder();
				if(commonName==null){
					commonName=hostname;
				}
				if(signUser==null){
					signUser=System.getProperty("user.name");
				}
				// Check if csr is already created
				boolean reuse = false;
				if(templateName == null && !privateKeyPath.equals(certificatePath)) {
					FileAttributes fa;
					try {
						fa = deployTaskInfo.getSftp().stat(privateKeyPath + ".NEW");
					} catch(IOException e) {
						fa = null;
					}
					if(fa != null && fa.isFile() && fa.getSize() != null && fa.getSize().longValue() > 0 && fa.getModifiedTime() != null && fa.getModifiedTime().longValue() * 1000 > System.currentTimeMillis() - (30 * 24 * 60 * 60 * 1000L)) {
						reuse = ConvertUtils.getBooleanSafely(deployTaskInfo.interaction.readLine("Re-use CSR generated on %1$tm/%<td/%<tY %<tH:%<tM:%<tS?", fa.getModifiedTime().longValue() * 1000), false);
					}
				}
				bufferStream.clear();
				Writer writer = new OutputStreamWriter(bufferStream.getOutputStream());
				InputStream certStream;
				if(reuse){
					String csrResultInputFilePath=deployTaskInfo.interaction.readLine("Sign the previous CSR,  then enter the result file path:\n%1s", csr);
					if(StringUtils.isBlank(csrResultInputFilePath)){
						return "No CSR result file path entered. Rerun when the cert is ready.";
					}else{
						certStream = new FileInputStream(csrResultInputFilePath);
						//String test=IOUtils.readFully(certStream);
						//System.out.println(test);
						//certStream=new ByteArrayInputStream(test.getBytes());
					}
				}
				else {
					KeyPair pair = CryptUtils.generateCSR("RSA", 2048, "CN=" + commonName + ", O=USA Technologies\\, Inc, L=Malvern, S=PA, C=US", csr);
					CryptUtils.writePEMKey("PRIVATE KEY", pair.getPrivate().getEncoded(), writer);
					writer.flush();
					if(!privateKeyPath.equals(certificatePath)) {
						String cmd = "if [ -f " + privateKeyPath + " ]; then /bin/cp -p " + privateKeyPath + " " + privateKeyPath + ".`date +%Y%m%d-%H%M%S`; fi";
						ExecuteTask.executeCommands(deployTaskInfo, "sudo su", cmd);
						UploadTask.writeFileUsingTmp(deployTaskInfo, bufferStream.getInputStream(), privateKeyPermissions, owner, group, null, privateKeyPath + ".NEW");
						if(hosts.length > 0)
							for(Host h : hosts)
								if(!h.equals(deployTaskInfo.host)) {
									UploadTask.writeFileUsingTmp(h, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, bufferStream.getInputStream(), privateKeyPermissions, owner, group, null, privateKeyPath + ".NEW");
									ExecuteTask.executeCommands(h, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache.getSshClient(h, deployTaskInfo.user), "sudo su", cmd);
								}
						bufferStream.clear();
					}
					// InputStream certStream = DeployUtils.signCertificate(deployTaskInfo.interaction, deployTaskInfo.passwordCache, csr.toString());
					if(templateName == null){
						String csrResultInputFilePath=deployTaskInfo.interaction.readLine("Sign the following CSR,  then enter the result file path:\n%1s", csr);
						if(StringUtils.isBlank(csrResultInputFilePath)){
							return "No CSR result file path entered. Rerun when the cert is ready.";
						}else{
							certStream = new FileInputStream(csrResultInputFilePath);
						}
					}
					else{
						certStream = DeployUtils.signCertificate("usasubca.usatech.com", templateName, signUser, deployTaskInfo.interaction, deployTaskInfo.passwordCache, csr.toString());
					}
				}
				// deployTaskInfo.interaction.readLine("Certificate created and signed. Press any key to swap out the old with the new one");
				deployTaskInfo.interaction.printf("Certificate created and signed. Swapping out the old with the new one");
				Collection<? extends Certificate> newCerts = cf.generateCertificates(certStream);
				//X509Certificate newCert = (X509Certificate)newCerts.iterator().next();
				boolean first = true;
				X509Certificate rootCert = null;
				for(Certificate newCert : newCerts) {
					CryptUtils.writeCertificate(newCert, writer);
					if(first)
						first = false;
					else if(newCert instanceof X509Certificate)
						rootCert = (X509Certificate) newCert;
				}
				writer.flush();
				List<String> cmds = new ArrayList<>();
				cmds.add("sudo su");
				cmds.add("if [ -f " + certificatePath + " ]; then /bin/cp -p " + certificatePath + " " + certificatePath + ".`date +%Y%m%d-%H%M%S`; fi");

				if(!privateKeyPath.equals(certificatePath))
					cmds.add("/bin/cp -p " + privateKeyPath + ".NEW " + privateKeyPath);
				boolean writeRootCert = false;
				if(rootCertPath != null && !rootCertPath.trim().isEmpty() && rootCert != null && (rootCert.getIssuerX500Principal() == null || rootCert.getIssuerX500Principal().equals(rootCert.getSubjectX500Principal()))) {
					cmds.add("if [ -f " + rootCertPath + " ]; then /bin/cp -p " + rootCertPath + " " + rootCertPath + ".`date +%Y%m%d-%H%M%S`; fi");
					writeRootCert = true;
				}
				ExecuteTask.executeCommands(deployTaskInfo, cmds);

				UploadTask.writeFileUsingTmp(deployTaskInfo, bufferStream.getInputStream(), certificatePermissions, owner, group, null, certificatePath);

				if(hosts.length > 0)
					for(Host h : hosts)
						if(!h.equals(deployTaskInfo.host)) {
							ExecuteTask.executeCommands(h, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache.getSshClient(h, deployTaskInfo.user), cmds);
							UploadTask.writeFileUsingTmp(h, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, bufferStream.getInputStream(), certificatePermissions, owner, group, null, certificatePath);
						}

				if(writeRootCert) {
					bufferStream.clear();
					CryptUtils.writeCertificate(rootCert, writer);
					writer.flush();
					UploadTask.writeFileUsingTmp(deployTaskInfo, bufferStream.getInputStream(), certificatePermissions, owner, group, null, rootCertPath);
					if(hosts.length > 0)
						for(Host h : hosts)
							if(!h.equals(deployTaskInfo.host))
								UploadTask.writeFileUsingTmp(h, deployTaskInfo.passwordCache, deployTaskInfo.interaction, deployTaskInfo.sshClientCache, deployTaskInfo.user, bufferStream.getInputStream(), certificatePermissions, owner, group, null, rootCertPath);
				}

				return "Renewed certificate and private key";
			}
		} catch(NoSuchAlgorithmException e) {
			throw new IOException(e);
		} catch(CertificateException e) {
			throw new IOException(e);
		} catch(InvalidKeyException e) {
			throw new IOException(e);
		} catch(SignatureException e) {
			throw new IOException(e);
		}
		return "Certificate is up to date";
	}

	@Override
	public String getDescription(DeployTaskInfo deployTaskInfo) {
		StringBuilder sb = new StringBuilder();
		sb.append("Renewing certificate and private key at '").append(privateKeyPath).append("'");
		return sb.toString();
	}

	public Set<Host> getCopyToHosts() {
		return copyToHostsUnmod;
	}

	public boolean addCopyToHosts(Host copyToHost) {
		return this.copyToHosts.add(copyToHost);
	}

	public boolean removeCopyToHosts(Host copyToHost) {
		return this.copyToHosts.remove(copyToHost);
	}

}