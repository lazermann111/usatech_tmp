package com.usatech.tools.deploy;

import java.lang.reflect.UndeclaredThrowableException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import simple.util.ExcludingSet;

public class FilteredServerSet implements ServerSet {
	protected final Set<App> excludedApps;
	protected final ServerSet delegate;
	protected final Map<Server, Server> serverMap = new HashMap<Server, Server>();

	public FilteredServerSet(ServerSet delegate, Set<App> excludedApps) {
		this.delegate = delegate;
		this.excludedApps = excludedApps;
	}

	protected Server convertServer(Server server) {
		if(server == null)
			return null;
		Server converted = serverMap.get(server);
		if(converted == null) {
			try {
				converted = new Server(server.host, server.serverType, new ExcludingSet<App>(server.apps, excludedApps), server.instances);
			} catch(UnknownHostException e) {
				throw new UndeclaredThrowableException(e);
			}
			serverMap.put(server, converted);
		}
		return converted;
	}

	protected List<Server> convertServers(List<Server> servers) {
		List<Server> converteds = new ArrayList<Server>();
		for(Server server : servers)
			converteds.add(convertServer(server));
		return converteds;
	}
	@Override
	public Server getServer(Host host, String serverType) {
		return convertServer(delegate.getServer(host, serverType));
	}

	@Override
	public List<Server> getServers(Host host) {
		return convertServers(delegate.getServers(host));
	}

	@Override
	public List<Server> getServers(String serverType) {
		return convertServers(delegate.getServers(serverType));
	}

	@Override
	public List<Server> getServers(Set<String> serverTypes) {
		return convertServers(delegate.getServers(serverTypes));
	}

	@Override
	public Set<App> getApps(String serverType) {
		return new ExcludingSet<App>(delegate.getApps(serverType), excludedApps);
	}

	@Override
	public Set<App> getSubApps(App app) {
		return new ExcludingSet<App>(delegate.getSubApps(app), excludedApps);
	}

	@Override
	public Set<Host> getHosts(String serverType) {
		List<Server> servers = getServers(serverType);
		if(servers == null)
			return null;

		Set<Host> hosts = new LinkedHashSet<Host>(servers.size());
		for(Server server : servers)
			hosts.add(server.host);
		return hosts;
	}

	@Override
	public Set<Host> getHosts(Set<String> serverTypes) {
		List<Server> servers = getServers(serverTypes);
		if(servers == null)
			return null;

		Set<Host> hosts = new LinkedHashSet<Host>(servers.size());
		for(Server server : servers)
			hosts.add(server.host);
		return hosts;
	}
}
