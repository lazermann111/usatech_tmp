package com.usatech.tools.deploy;

import java.net.UnknownHostException;
import java.util.Set;

import simple.io.IOUtils;
import simple.text.StringUtils;

public class Server {
	public final Host host;
	public final String ip;
	public final String serverType;
	public final Set<App> apps;
	public final String[] instances;

	public Server(Host host, String serverType, Set<App> apps, String... instances) throws UnknownHostException {
		super();
		this.host = host;
		this.ip = StringUtils.isBlank(host.getFullName()) ? null : IOUtils.getServerIP(host.getFullName());
		this.serverType = serverType;
		this.apps = apps;
		this.instances = (instances == null || instances.length == 0 ? new String[] { null } : instances);
	}

	public boolean isOnHost(Host host) {
		return host != null && host.equals(this.host);
	}
}