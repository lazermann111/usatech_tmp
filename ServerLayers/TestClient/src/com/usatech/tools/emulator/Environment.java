package com.usatech.tools.emulator;

import java.sql.Connection;
import java.sql.SQLException;

import simple.app.ServiceException;
import simple.db.DataLayerException;

import com.usatech.layers.common.constants.DeviceType;

public interface Environment {
	public String getName();

	public String getNetlayerHost();

	public String getDeviceSerialCd(DeviceType deviceType, int deviceSubType, int index) throws ServiceException;

	public Connection getConnection(String dataSourceName, PasswordCache passwordCache) throws DataLayerException, SQLException;
}
