package com.usatech.tools.emulator;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.config.ConfigException;
import simple.db.config.ConfigLoader;
import simple.io.Interaction;
import simple.io.LoadingException;
import simple.io.Log;
import simple.results.BeanException;
import simple.text.StringUtils;

import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.networklayer.NetworkLayerException;

public class EmulationUtils {
	private static final Log log = Log.getLog();
	static {
		String file = StringUtils.packageAsPath(EmulationUtils.class) + "emulation-data-layer.xml";
		try {
			ConfigLoader.loadConfig(file, 15000);
		} catch(ConfigException e) {
			log.error("Could not load data-layer file, '" + file + "'", e);
		} catch(IOException e) {
			log.error("Could not load data-layer file, '" + file + "'", e);
		} catch(LoadingException e) {
			log.error("Could not load data-layer file, '" + file + "'", e);
		}
	}

	public static interface Responder {
		public MessageData[] next(Client client, MessageData reply) throws ServiceException;
	}

	public static void runSession(Environment environment, Interaction interaction, PasswordCache passwordCache, DeviceType deviceType, int deviceSubType, int deviceIndex, Responder responder) throws ServiceException {
		// 1. get device name and encryption key
		String deviceSerialCd = environment.getDeviceSerialCd(deviceType, deviceSubType, deviceIndex);
		if(deviceSerialCd == null)
			throw new ServiceException("No " + deviceType + " device at index " + deviceIndex);
		Connection conn;
		try {
			conn = environment.getConnection("OPER", passwordCache);
		} catch(DataLayerException e) {
			throw new ServiceException(e);
		} catch(SQLException e) {
			throw new ServiceException(e);
		}
		try {
			Client client = new Client(deviceSerialCd, deviceType);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("deviceSerialCd", deviceSerialCd);
			MessageData[] messages = responder.next(client, null);
			if(messages == null || messages.length == 0)
				throw new ServiceException("Initial message is null");
			boolean init = messages[0].getMessageType().isInitMessage();
			params.put("initialization", init ? 'Y' : 'N');
			DataLayerMgr.selectInto(conn, "GET_DEVICE_CREDENTIALS", params);
			String deviceName = ConvertUtils.getString(params.get("deviceName"), true);
			byte[] encryptKey = ConvertUtils.convertRequired(byte[].class, params.get("encryptKey"));
			boolean retrieveDeviceNameAgain = init && ConvertUtils.getInt(params.get("priority")) > 1;

			// 2. figure out port
			int port = getPort(deviceType, deviceSubType, messages[0].getMessageType().isAuthRequest());
			int protocol;
			switch(port) {
				case 14107:
					protocol = 4;
					break;
				default:
					protocol = 7;
					break;
			}
			// TODO: handle WS
			client.setHost(environment.getNetlayerHost());
			client.setPort(port);
			client.setEvNumber(deviceName);
			client.setEncryptionKey(encryptKey);
			client.setMasterIdIfGreater(1 + ConvertUtils.getLong(params.get("masterId")));
			client.setTimeZoneGuid(ConvertUtils.getString(params.get("timeZoneGuid"), false));

			interaction.printf("Using " + deviceType + " Device " + deviceSerialCd + " (" + deviceName + ")");
			// <column property="activationStatus"/>
			// <column property="credentialUserName"/>
			messages = responder.next(client, null);

			// 3. connect
			client.startCommunication(protocol);
			MessageData reply;
			try {
				for(MessageData message : messages)
					interaction.printf("SENT: %1s", message);
				reply = client.sendMessages(messages);
				if(reply != null)
					interaction.printf("RECEIVED: %1s", reply);
				//4. Loop until done
				messages = responder.next(client, reply);
				if(messages != null) {
					port = getPort(deviceType, deviceSubType, messages[0].getMessageType().isAuthRequest());
					if(retrieveDeviceNameAgain) {
						// get device name again
						DataLayerMgr.selectInto(conn, "GET_DEVICE_INFO", params);
						deviceName = ConvertUtils.getString(params.get("deviceName"), true);
						encryptKey = ConvertUtils.convertRequired(byte[].class, params.get("encryptKey"));
						if(!deviceName.equals(client.getEvNumber()) || port != client.getPort()) {
							client.stopCommunication();
							client.setEvNumber(deviceName);
							client.setEncryptionKey(encryptKey);
							client.setPort(port);
							client.startCommunication(protocol);
						}
					} else if(port != client.getPort()) {
						client.stopCommunication();
						client.setPort(port);
						client.startCommunication(protocol);
					}
					do {
						for(MessageData message : messages)
							interaction.printf("SENT: %1s", message);
						reply = client.sendMessages(messages);
						if(reply != null)
							interaction.printf("RECEIVED: %1s", reply);
						messages = responder.next(client, reply);
					} while(messages != null);
				}
			} finally {
				client.stopCommunication();
			}
		} catch(SQLException e) {
			throw new ServiceException(e);
		} catch(DataLayerException e) {
			throw new ServiceException(e);
		} catch(BeanException e) {
			throw new ServiceException(e);
		} catch(UnknownHostException e) {
			throw new ServiceException(e);
		} catch(SecurityException e) {
			throw new ServiceException(e);
		} catch(IllegalArgumentException e) {
			throw new ServiceException(e);
		} catch(ClassNotFoundException e) {
			throw new ServiceException(e);
		} catch(NetworkLayerException e) {
			throw new ServiceException(e);
		} catch(IOException e) {
			throw new ServiceException(e);
		} catch(NoSuchMethodException e) {
			throw new ServiceException(e);
		} catch(IllegalAccessException e) {
			throw new ServiceException(e);
		} catch(InvocationTargetException e) {
			throw new ServiceException(e);
		} catch(ConvertException e) {
			throw new ServiceException(e);
		} catch(Exception e) {
			throw new ServiceException(e);
		} finally {
			try {
				conn.close();
			} catch(SQLException e) {
				// ignore
			}
		}
	}

	public static int getPort(DeviceType deviceType, int deviceSubType, boolean authRequest) {
		switch(deviceType) {
			case EDGE:
				return authRequest ? 14109 : 14108;
			case GX:
			case G4:
				return 14107;
			case KIOSK:
				if(deviceSubType >= 3)
					return 9443;
			default:
				return 443;
		}
	}
}
