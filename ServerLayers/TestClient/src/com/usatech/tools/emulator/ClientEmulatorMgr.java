package com.usatech.tools.emulator;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.SwingWorker;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.MapConfiguration;

import simple.app.BaseWithConfig;
import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.io.GuiInteraction;
import simple.io.Interaction;
import simple.io.Log;
import simple.swt.ChangeableComboBoxModel;
import simple.swt.SortedTrackableListModel;
import simple.util.concurrent.CustomThreadFactory;
import simple.util.concurrent.WaitForUpdateFuture;

public class ClientEmulatorMgr extends BaseWithConfig {
	private static final Log log = Log.getLog();
	public static final DateFormat fileDateFormat = new SimpleDateFormat("yyyyMMdd-HHmm");
	protected final ScheduledThreadPoolExecutor scheduler = new ScheduledThreadPoolExecutor(5, new CustomThreadFactory("Verification-", true, 5));
	
	protected static final FileFilter emulationFileFilter = new FileFilter() {
		@Override
		public boolean accept(File pathname) {
			return pathname.isDirectory() || pathname.getName().endsWith("Emulation.class") && !pathname.getName().equals("Emulation.class");
		}		
	};

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new ClientEmulatorMgr().run(args);
	}

	public Configuration getConfig(String propertiesFile, Class<?> appClass, Configuration defaults) throws IOException {
		return new MapConfiguration(new HashMap<String, Object>());
	}
	@Override
	protected void registerDefaultCommandLineArguments() {
		super.registerDefaultCommandLineArguments();
		registerCommandLineSwitch('d', "searchPath", true, true, "searchPath", "The path in which to look for Emulations");
	}
	@Override
	protected void execute(Map<String, Object> argMap, Configuration config) {
		final CountDownLatch exit = new CountDownLatch(1);
		final JFrame frame = new JFrame("USAT Emulator");
		frame.setSize(600, 400);
		frame.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				exit.countDown();
			}
		});
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		frame.getContentPane().add(mainPanel);
		final JButton deployButton = new JButton("Run");
		JPanel buttonPanel = new JPanel(new FlowLayout());
		buttonPanel.add(deployButton);
		mainPanel.add(buttonPanel, BorderLayout.SOUTH);
		
		JPanel envPanel = new JPanel();
		envPanel.setLayout(new FlowLayout());
		envPanel.add(new JLabel("Environment:"));
		
		final ChangeableComboBoxModel<Environment> envModel = new ChangeableComboBoxModel<Environment>();
		envModel.setItems(new Environment[] {
				new BaseEnvironment("LOCAL", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb012.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=USADEV04.WORLD)))", "jdbc:postgresql://devdbs11:5432/main?ssl=true&connectTimeout=5&tcpKeepAlive=true", "localhost"),
				new BaseEnvironment("DEV", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb011.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=usadev03.world)))", "jdbc:postgresql://devpgs10:5432/main?ssl=true&connectTimeout=5&tcpKeepAlive=true", "10.0.0.67")
		});
		final JComboBox envList = new JComboBox(envModel);
		envPanel.add(envList);
		mainPanel.add(envPanel, BorderLayout.NORTH);

		JPanel interactionPanel = new JPanel();
		interactionPanel.setLayout(new BorderLayout());
		JPanel emulationsPanel = new JPanel();
		
		SortedTrackableListModel<Emulation<?>> emulationModel = new SortedTrackableListModel<Emulation<?>>(false, new Comparator<Emulation<?>>() {
			public int compare(Emulation<?> o1, Emulation<?> o2) {
				return o1.toString().compareToIgnoreCase(o2.toString());
			}
		});
		// Add emulations
		String searchPath = (String)argMap.get("searchPath");
		if(searchPath != null) {
			File searchDir = new File(searchPath);
			if(searchDir.isAbsolute())
				addEmulations(new File(searchPath), emulationFileFilter, null, emulationModel);
			else {
				String packageName = searchPath.replaceAll("[/\\\\]", ".");
				URL[] urls = ((URLClassLoader) getClass().getClassLoader()).getURLs();
				for(URL url : urls) {
					if("file".equals(url.getProtocol())) {
						String file = url.getFile();
						if(file != null && file.endsWith("/")) {
							// its a directory - search for classes
							try {
								addEmulations(new File(new File(url.toURI()), searchPath), emulationFileFilter, packageName, emulationModel);
							} catch(URISyntaxException e) {
								log.warn("Could not read directory '" + file + "'", e);
								continue;
							}
						} else {
							// Its a jar file - search for classes
							JarFile jarFile;
							try {
								jarFile = new JarFile(new File(url.toURI()));
							} catch(IOException e) {
								log.warn("Could not read jar file '" + file + "'", e);
								continue;
							} catch(URISyntaxException e) {
								log.warn("Could not read jar file '" + file + "'", e);
								continue;
							}
							for(Enumeration<JarEntry> entries = jarFile.entries(); entries.hasMoreElements();) {
								JarEntry entry = entries.nextElement();
								String name = entry.getName();
								if(name.startsWith(packageName.replace('.', '/')) && name.endsWith("Emulation.class"))
									addEmulation(name.substring(0, name.length() - 6).replace('/', '.').replace('$', '.'), emulationModel);
							}
						}
					}
				}
			}
		} else if(getClass().getClassLoader() instanceof URLClassLoader) {
			URL[] urls = ((URLClassLoader)getClass().getClassLoader()).getURLs();
			for(URL url : urls) {
				if("file".equals(url.getProtocol())) {
					String file = url.getFile();
                    if(file != null && file.endsWith("/")) {
                    	//its a directory - search for classes
                    	try {
							addEmulations(new File(url.toURI()), emulationFileFilter, null, emulationModel);
                    	} catch(URISyntaxException e) {
							log.warn("Could not read directory '" + file + "'", e);
							continue;
						}
                    } else {
                    	//Its a jar file - search for classes
                    	JarFile jarFile;
						try {
							jarFile = new JarFile(new File(url.toURI()));
						} catch(IOException e) {
							log.warn("Could not read jar file '" + file + "'", e);
							continue;
						} catch(URISyntaxException e) {
							log.warn("Could not read jar file '" + file + "'", e);
							continue;
						}
                    	for(Enumeration<JarEntry> entries = jarFile.entries(); entries.hasMoreElements(); ) {
                    		JarEntry entry = entries.nextElement();
                    		String name = entry.getName(); 
							if(name.endsWith("Emulation.class"))
								addEmulation(name.substring(0, name.length() - 6).replace('/', '.').replace('$', '.'), emulationModel);
                    	}
                    }
				}
			}
		}
		final JList emulationList = new JList(emulationModel);
		emulationList.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				Emulation<?> emulation = (Emulation<?>) emulationList.getSelectedValue();
				if(emulation == null)
					deployButton.setEnabled(false);
				else {
					deployButton.setEnabled(true);
				}
			}
		});
		emulationsPanel.setLayout(new BorderLayout());
		emulationsPanel.add(new JScrollPane(emulationList), BorderLayout.CENTER);
		emulationsPanel.add(new JLabel("Emulations:"), BorderLayout.NORTH);

		final Interaction interaction = new GuiInteraction(interactionPanel);		
		mainPanel.add(new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true, emulationsPanel, interactionPanel), BorderLayout.CENTER);
		final PasswordCache passwordCache = new MapPasswordCache(interaction);
		deployButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deployButton.setEnabled(false);
				final Object[] emus = emulationList.getSelectedValues();
				final Environment environment = envModel.getSelectedItem();
				@SuppressWarnings("unchecked")
				final Emulation<Object>[] emulations = (Emulation<Object>[]) new Emulation<?>[emus.length];
				for(int i = 0; i < emus.length; i++)
					emulations[i] = ConvertUtils.cast(emus[i]);
				(new SwingWorker<Void, Void>() {
					@Override
					protected Void doInBackground() throws Exception {
						executeEmulation(frame, emulations, environment, interaction, passwordCache);
						return null;
					}
					@Override
					protected void done() {
						deployButton.setEnabled(true);
					}
				}).execute();				
			}
		});
		frame.setVisible(true);
		try {
			exit.await();
		} catch(InterruptedException e) {
			//ignore
		}
	}

	protected void addEmulations(File dir, FileFilter fileFilter, String packageName, Collection<Emulation<?>> emulations) {
		File[] files = dir.listFiles(fileFilter);
		if(files != null)
			for(File file : files) {
				if(file.isDirectory())
					addEmulations(file, fileFilter, (packageName == null || packageName.length() == 0 ? "" : packageName + ".") + file.getName(), emulations);
				else {
					String className = (packageName == null || packageName.length() == 0 ? "" : packageName + ".") + file.getName().substring(0, file.getName().length() - 6).replace('$', '.');
					addEmulation(className, emulations);
				}
			}	
	}
	
	protected boolean addEmulation(String className, Collection<Emulation<?>> emulations) {
		Class<?> type;
		try {
			type = Class.forName(className);
		} catch(ClassNotFoundException e) {
			return false;
		}
		if(Emulation.class.isAssignableFrom(type) && !type.isInterface() && !type.isMemberClass() && !Modifier.isAbstract(type.getModifiers()) && Modifier.isPublic(type.getModifiers())) {
			try {
				emulations.add(type.asSubclass(Emulation.class).newInstance());
				return true;
			} catch(InstantiationException e) {
				log.debug("Could not instantiate instance of class " + type.getName(), e);
			} catch(IllegalAccessException e) {
				log.debug("Could not instantiate instance of class " + type.getName(), e);
			}
		}
		return false;
	}

	protected void executeEmulation(JFrame frame, Emulation<Object>[] emulations, Environment environment, Interaction interaction, PasswordCache passwordCache) {
		if(emulations == null || emulations.length == 0) {
			JOptionPane.showMessageDialog(frame, "No Emulations are selected. Please select the Emulations to Run", "Emulations Not Selected", JOptionPane.WARNING_MESSAGE);
			return;
		}
		if(environment == null) {
			JOptionPane.showMessageDialog(frame, "No Environment is selected. Please select the Environment on which to run", "Environment Not Selected", JOptionPane.WARNING_MESSAGE);
			return;
		}

		StringBuilder sb = new StringBuilder();
		sb.append("Run emulations:");
		for(Emulation<?> emulation : emulations)
			sb.append("\n\t").append(emulation);
		sb.append("\n on ").append(environment);

		if(JOptionPane.showConfirmDialog(frame, sb.toString(), "Confirm Deploy", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
			interaction.clear();
			interaction.printf("Starting emulations on %1$s at %2$tD %2$tT", environment, new Date());
			@SuppressWarnings("unchecked")
			Future<String>[] verifications = (Future<String>[]) new Future<?>[emulations.length];
			for(int i = 0; i < emulations.length; i++) {
				verifications[i] = runEmulation(emulations[i], environment, interaction, passwordCache);
			}
			interaction.printf("Completed emulations on %1$s at %2$tD %2$tT; now verifying...", environment, new Date());
			for(int i = 0; i < emulations.length; i++) {
				int sectionId = interaction.startSection("Verifying " + emulations[i]);
				try {
					interaction.endSection(sectionId, true, verifications[i].get());
				} catch(InterruptedException e) {
					interaction.endSection(sectionId, false, "ERROR: " + e.getMessage());
				} catch(ExecutionException e) {
					interaction.endSection(sectionId, false, "ERROR: " + e.getCause().getMessage());
				}
			}
		}
	}
	
	protected Future<String> runEmulation(final Emulation<Object> emulation, final Environment environment, final Interaction interaction, final PasswordCache passwordCache) {
		int sectionId = interaction.startSection(emulation + " on " + environment);
		final WaitForUpdateFuture<String> verifiedFuture = new WaitForUpdateFuture<String>();
		try {
			long start = System.currentTimeMillis();
			final Object result = emulation.run(environment, interaction, passwordCache);
			long delay = emulation.getVerifyDelay(result, environment, interaction, passwordCache);
			scheduler.schedule(new Runnable() {
				@Override
				public void run() {
					try {
						verifiedFuture.set(emulation.verify(result, environment, interaction, passwordCache));
					} catch(ServiceException e) {
						verifiedFuture.setException(new ExecutionException(e));
					}
				}
			}, delay, TimeUnit.MILLISECONDS);
			interaction.endSection(sectionId, true, "DONE in " + (System.currentTimeMillis() - start) + " ms");
			interaction.printf("Successfully ran %1$s on %2$s", emulation, environment);
		} catch(ServiceException e) {
			log.warn("Could not execute tasks", e);
			interaction.endSection(sectionId, false, "ERROR: " + e.getMessage());
			interaction.printf("Error while running %1$s on %2$s because: %3$s", emulation, environment, e.getMessage());
			verifiedFuture.setException(new ExecutionException(e));
		} catch(RuntimeException e) {
			log.warn("Could not execute tasks", e);
			interaction.endSection(sectionId, false, "ERROR: " + e.getMessage());
			interaction.printf("Error while running %1$s on %2$s because: %3$s", emulation, environment, e.getMessage());
			verifiedFuture.setException(new ExecutionException(e));
		} catch(Error e) {
			log.warn("Could not execute tasks", e);
			interaction.endSection(sectionId, false, "ERROR: " + e.getMessage());
			interaction.printf("Error while running %1$s on %2$s because: %3$s", emulation, environment, e.getMessage());
			verifiedFuture.setException(new ExecutionException(e));
		}
		return verifiedFuture;
	}


}
