package com.usatech.tools.emulator;


public interface PasswordCache {
	public char[] getPassword(String key);

	public void setPassword(String key, char[] password);
}
