package com.usatech.tools.emulator;

import simple.app.ServiceException;
import simple.io.Interaction;

import com.usatech.layers.common.constants.DeviceType;

public class EdgeAuthSmallSingleSaleEmulation implements Emulation<String> {

	@Override
	public String run(Environment environment, Interaction interaction, PasswordCache passwordCache) throws ServiceException {
		AuthSaleResponder responder = new AuthSaleResponder();
		EmulationUtils.runSession(environment, interaction, passwordCache, DeviceType.EDGE, 0, 0, responder);
		return responder.getGlobalTranCd();
	}

	@Override
	public String verify(String result, Environment environment, Interaction interaction, PasswordCache passwordCache) throws ServiceException {
		return "OK";
	}

	@Override
	public long getVerifyDelay(String result, Environment environment, Interaction interaction, PasswordCache passwordCache) {
		return 0;
	}

}
