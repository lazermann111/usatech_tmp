package com.usatech.tools.emulator;

import java.util.HashMap;
import java.util.Map;

import simple.io.Interaction;

public class MapPasswordCache implements PasswordCache {
	protected final Map<String, char[]> map = new HashMap<String, char[]>();
	protected final Interaction interaction;

	public MapPasswordCache(Interaction interaction) {
		this.interaction = interaction;
	}

	@Override
	public char[] getPassword(String key) {
		char[] pwd = map.get(key);
		if(pwd == null) {
			pwd = interaction.readPassword("Please enter the value/password for %1$s:", key);
			if(pwd == null)
				pwd = new char[0];
		}
		return pwd;
	}

	@Override
	public void setPassword(String key, char[] password) {
		map.put(key, password);
	}
}
