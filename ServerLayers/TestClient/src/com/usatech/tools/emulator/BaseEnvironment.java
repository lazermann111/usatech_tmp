package com.usatech.tools.emulator;

import java.sql.Connection;
import java.sql.SQLException;

import simple.app.ServiceException;
import simple.db.DataLayerException;
import simple.db.ExtendedDataSource;
import simple.db.LocalDataSourceFactory;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;

import com.usatech.layers.common.constants.DeviceType;

public class BaseEnvironment implements Environment {
	protected final String name;
	protected final String netlayerHost;
	protected final LocalDataSourceFactory dataSourceFactory = new LocalDataSourceFactory();
	public BaseEnvironment(String name, String operDbUrl, String mainDbUrl, String netlayerHost) {
		super();
		this.name = name;
		dataSourceFactory.addDataSource("OPER", "oracle.jdbc.driver.OracleDriver", operDbUrl, "SYSTEM", null, true);
		dataSourceFactory.addDataSource("MAIN", "org.postgresql.Driver", mainDbUrl, "admin_1", null, true);
		this.netlayerHost = netlayerHost;
	}

	public String getName() {
		return name;
	}

	public String getNetlayerHost() {
		return netlayerHost;
	}

	@Override
	public String getDeviceSerialCd(DeviceType deviceType, int deviceSubType, int index) throws ServiceException {
		switch(deviceType) {
			case GX:
				return StringUtils.appendPadded(new StringBuilder(8).append("G8"), Integer.toString(999900 + index), '0', 6, Justification.RIGHT).toString();
			case EDGE:
				return StringUtils.appendPadded(new StringBuilder(8).append("EE1"), Integer.toString(99999900 + index), '0', 8, Justification.RIGHT).toString();
		}
		return null;
	}

	@Override
	public String toString() {
		return name;
	}

	public Connection getConnection(String dataSourceName, PasswordCache passwordCache) throws DataLayerException, SQLException {
		ExtendedDataSource ds = dataSourceFactory.getDataSource(dataSourceName);
		String passwordKey;
		if(ds.getPassword() == null) {
			passwordKey = ds.getUsername() + '@' + dataSourceName;
			char[] pwd = passwordCache.getPassword(passwordKey);
			if(pwd == null)
				throw new SQLException("Cancelled by user");
			ds.setPassword(new String(pwd));
		} else
			passwordKey = null;
		Connection conn = null;
		try {
			conn = ds.getConnection();
			return conn;
		} finally {
			if(passwordKey != null) {
				if(conn == null)
					ds.setPassword(null);
				else
					passwordCache.setPassword(passwordKey, ds.getPassword().toCharArray());
			}
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof BaseEnvironment))
			return false;
		BaseEnvironment env = (BaseEnvironment) obj;
		return name.equals(env.name);
	}

	@Override
	public int hashCode() {
		return name.hashCode();
	}
}
