package com.usatech.tools.emulator;

import simple.app.ServiceException;
import simple.io.Interaction;

public interface Emulation<R> {
	public R run(Environment environment, Interaction interaction, PasswordCache passwordCache) throws ServiceException;

	public String verify(R result, Environment environment, Interaction interaction, PasswordCache passwordCache) throws ServiceException;

	public long getVerifyDelay(R result, Environment environment, Interaction interaction, PasswordCache passwordCache);
}
