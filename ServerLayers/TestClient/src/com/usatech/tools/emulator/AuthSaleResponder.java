package com.usatech.tools.emulator;

import java.math.BigDecimal;
import java.util.Calendar;

import simple.app.ServiceException;

import com.usatech.layers.common.constants.AuthResponseCodeC3;
import com.usatech.layers.common.constants.AuthResponseType;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.ReceiptResult;
import com.usatech.layers.common.constants.SaleResult;
import com.usatech.layers.common.constants.SaleType;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageData_C2;
import com.usatech.layers.common.messagedata.MessageData_C2.GenericTracksCardReader;
import com.usatech.layers.common.messagedata.MessageData_C3;
import com.usatech.layers.common.messagedata.MessageData_C3.AuthResponseTypeData;
import com.usatech.layers.common.messagedata.MessageData_C3.AuthorizationAuthResponse;
import com.usatech.layers.common.messagedata.MessageData_C4;
import com.usatech.layers.common.messagedata.MessageData_C4.Format0LineItem;
import com.usatech.tools.LuhnUtils;
import com.usatech.tools.emulator.EmulationUtils.Responder;

public class AuthSaleResponder implements Responder {
	protected long tranId;
	protected String globalTranCd;
	protected AuthResultCode authResultCode;
	protected BigDecimal saleTotal = new BigDecimal(250);
	protected boolean convFee = false;
	protected String card = null;

	public static String generateCard(String prefix, int length) {
		return LuhnUtils.fillInTrackData(LuhnUtils.generateCard(prefix, length));
	}
	public MessageData[] next(Client client, MessageData reply) throws ServiceException {
		if(reply == null) {
			tranId = client.getAndIncrementMasterId();
			globalTranCd = "A:" + client.getEvNumber() + ":" + tranId;
			MessageData_C2 dataC2 = new MessageData_C2();
			dataC2.setAmount(new BigDecimal(1000));
			dataC2.setCardReaderType(CardReaderType.GENERIC);
			dataC2.setEntryType(EntryType.SWIPE);
			dataC2.setTransactionId(tranId);
			String card = getCard();
			((GenericTracksCardReader) dataC2.getCardReader()).setAccountData2(card);
			return new MessageData[] { dataC2 };
		}
		switch(reply.getMessageType()) {
			case AUTH_RESPONSE_4_1:
				MessageData_C3 dataC3 = (MessageData_C3) reply;
				AuthResponseTypeData artd = dataC3.getAuthResponseTypeData();
				AuthResponseType art = dataC3.getAuthResponseType();
				switch(art) {
					case AUTHORIZATION:
						AuthorizationAuthResponse aar = (AuthorizationAuthResponse) artd;
						AuthResponseCodeC3 rc = aar.getResponseCode();
						authResultCode = rc.getAuthResultCode();
						switch(rc) {
							case CONDITIONAL_SUCCESS:
								if(aar.getResultAmount() == null || aar.getResultAmount().compareTo(saleTotal) < 0)
									return new MessageData[] { createSaleCancelled(SaleResult.CANCELLED_BY_AUTH_FAILURE) };
							case SUCCESS:
								return new MessageData[] { createSale() };
							case FAILURE:
								return new MessageData[] { createSaleCancelled(SaleResult.CANCELLED_BY_AUTH_FAILURE) };
						}
						break;
					default:
						authResultCode = null;
				}
				break;
			default:
				authResultCode = null;
		}
		return null;
	}

	public String getCard() {
		if(card == null)
			card = generateCard("43", 16);
		return card;
	}
	protected MessageData createSale() {
		MessageData_C4 md = new MessageData_C4();
		md.setBatchId(1);
		md.setReceiptResult(ReceiptResult.UNAVAILABLE);
		Calendar cal = Calendar.getInstance();
		md.setSaleSessionStart(cal);
		md.setSaleType(SaleType.ACTUAL);
		md.setTransactionId(tranId);
		md.setLineItemFormat((byte) 0);
		if(saleTotal.signum() == 0) {
			md.setSaleResult(SaleResult.CANCELLED_BY_USER);
		} else {
			md.setSaleResult(SaleResult.SUCCESS);
			Format0LineItem li = (Format0LineItem) md.addLineItem();
			li.setComponentNumber(1);
			li.setItem(200);
			li.setDescription("001C");
			li.setDuration(2);
			li.setPrice(saleTotal);
			li.setQuantity(1);
			li.setSaleResult(SaleResult.SUCCESS);
			if(convFee) {
				li = (Format0LineItem) md.addLineItem();
				li.setComponentNumber(1);
				li.setItem(203);
				li.setDuration(0);
				li.setPrice(new BigDecimal(7));
				li.setQuantity(1);
				li.setSaleResult(SaleResult.SUCCESS);
				saleTotal = saleTotal.add(li.getPrice().multiply(BigDecimal.valueOf(li.getQuantity())));
			}
		}
		md.setSaleAmount(saleTotal);
		return md;
	}

	protected MessageData createSaleCancelled(SaleResult saleResult) {
		MessageData_C4 md = new MessageData_C4();
		md.setBatchId(1);
		md.setReceiptResult(ReceiptResult.UNAVAILABLE);
		Calendar cal = Calendar.getInstance();
		md.setSaleSessionStart(cal);
		md.setSaleType(SaleType.ACTUAL);
		md.setTransactionId(tranId);
		md.setLineItemFormat((byte) 0);
		md.setSaleResult(saleResult);
		md.setSaleAmount(BigDecimal.ZERO);
		return md;
	}
	public String getGlobalTranCd() {
		return globalTranCd;
	}

	public AuthResultCode getAuthResultCode() {
		return authResultCode;
	}

	public BigDecimal getSaleTotal() {
		return saleTotal;
	}

	public void setSaleTotal(BigDecimal saleTotal) {
		this.saleTotal = saleTotal;
	}

	public boolean isConvFee() {
		return convFee;
	}

	public void setConvFee(boolean convFee) {
		this.convFee = convFee;
	}

	public void setCard(String card) {
		this.card = card;
	}
}
