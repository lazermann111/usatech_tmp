package com.usatech.tools.emulator;

import java.util.Map;

import simple.app.ServiceException;
import simple.io.Interaction;

import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.GenericResponseServerActionCode;
import com.usatech.layers.common.constants.InitializationReason;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageData_C0;
import com.usatech.layers.common.messagedata.MessageData_CB;
import com.usatech.layers.common.messagedata.MessageData_CB.PropertyValueListAction;
import com.usatech.tools.emulator.EmulationUtils.Responder;

public class EdgeInitEmulation implements Emulation<String> {
	@Override
	public String run(Environment environment, final Interaction interaction, PasswordCache passwordCache) throws ServiceException {
		final String deviceFirmwareVersion = "BK Test 2.0";
		Responder responder = new Responder() {
			@Override
			public MessageData[] next(Client client, MessageData reply) throws ServiceException {
				if(reply == null) {
					MessageData_C0 dataC0 = new MessageData_C0();
					dataC0.setDeviceFirmwareVersion(deviceFirmwareVersion);
					dataC0.setDeviceInfo("Client Emulator 2.0 Device Info");
					dataC0.setDeviceSerialNum(client.getDeviceSerialCd());
					dataC0.setDeviceType(client.getDeviceType());
					dataC0.setPropertyListVersion(11);
					dataC0.setProtocolComplianceRevision(4001005);
					dataC0.setReasonCode(InitializationReason.NEW_DEVICE);
					dataC0.setTerminalInfo("Client Emulator 2.0 Terminal Info");
					return new MessageData[] { dataC0 };
				}
				if(reply.getMessageType() == MessageType.GENERIC_RESPONSE_4_1) {
					MessageData_CB dataCB = (MessageData_CB) reply;
					if(dataCB.getServerActionCode() == GenericResponseServerActionCode.PROCESS_PROP_LIST) {
						Map<Integer, String> propertyValues = ((PropertyValueListAction) dataCB.getServerActionCodeData()).getPropertyValues();
						String deviceName = propertyValues.get(51);
						String encryptionKeyHex = propertyValues.get(52);
						interaction.printf("Got DeviceName: " + deviceName + "; Encryption Key: " + encryptionKeyHex);
					}
				}
				return null;
			}
		};
		EmulationUtils.runSession(environment, interaction, passwordCache, DeviceType.EDGE, 0, 0, responder);
		return deviceFirmwareVersion;
	}

	@Override
	public String verify(String result, Environment environment, Interaction interaction, PasswordCache passwordCache) throws ServiceException {
		return "OK";
	}

	@Override
	public long getVerifyDelay(String result, Environment environment, Interaction interaction, PasswordCache passwordCache) {
		return 0;
	}


}
