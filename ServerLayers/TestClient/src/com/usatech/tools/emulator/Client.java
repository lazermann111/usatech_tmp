package com.usatech.tools.emulator;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import simple.io.ByteArrayUtils;
import simple.io.UUEncode;
import simple.text.StringUtils;
import simple.util.concurrent.Cache;
import simple.util.concurrent.LockSegmentCache;

import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.constants.DeviceProperty;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.GenericResponseServerActionCode;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageDataFactory;
import com.usatech.layers.common.messagedata.MessageData_8F;
import com.usatech.layers.common.messagedata.MessageData_CB;
import com.usatech.layers.common.messagedata.MessageData_CB.PropertyValueListAction;
import com.usatech.layers.common.messagedata.MessageDirection;
import com.usatech.networklayer.NetworkLayerException;
import com.usatech.networklayer.net.TransmissionProtocol;
import com.usatech.networklayer.net.TransmissionProtocolV4;
import com.usatech.networklayer.protocol.ProtocolV1;
import com.usatech.util.crypto.CRCException;
import com.usatech.util.crypto.Crypt;
import com.usatech.util.crypto.CryptException;
import com.usatech.util.crypto.CryptUtil;


public class Client {
	private static Log log = LogFactory.getLog(Client.class);
	protected String host = "usaapd1";
	protected int port = 443;
	protected String evNumber = "EV035179";
	protected byte[] encryptionKey;
	protected int msg = 0;
	protected int protocolVersion = 3;
	protected TransmissionProtocol protocol;
	protected Socket socket;
	protected class LoggingInputStream extends BufferedInputStream {
		protected final StringBuilder sb = new StringBuilder();
		public LoggingInputStream(InputStream in) {
			super(in);		
		}
		@Override
		public int read() throws IOException {
			int r = super.read();
			if(r > -1)
				StringUtils.appendHex(sb, (byte)r);
			return r;
		}
		@Override
		public int read(byte[] b, int off, int len) throws IOException {
			int r = super.read(b, off, len);
			if(r > 0)
				StringUtils.appendHex(sb, b, off, r);
			return r;
		}
		public String drainLog() {
			String s = sb.toString();
			sb.setLength(0);
			return s;
		}
		
	}
	protected DataInputStream input;
	protected LoggingInputStream inputLogging;
	protected DataOutputStream output;
	protected static Crypt[] crypts = new Crypt[10];
	protected String timeZoneGuid;
	protected final String deviceSerialCd;
	protected final DeviceType deviceType;
	protected static Cache<String, AtomicLong, RuntimeException> messageIdCache = new LockSegmentCache<String, AtomicLong, RuntimeException>() {
		@Override
		protected AtomicLong createValue(String key, Object... additionalInfo) throws RuntimeException {
			return new AtomicLong(System.currentTimeMillis() / 1000);
		}
	};

	protected static Cache<String, AtomicLong, RuntimeException> masterIdCache = new LockSegmentCache<String, AtomicLong, RuntimeException>() {
		@Override
		protected AtomicLong createValue(String key, Object... additionalInfo) throws RuntimeException {
			return new AtomicLong(1);
		}
	};
	static {
		crypts[1] = new Crypt() {
			public byte calcCheckSum(byte[] data) {
				byte sum = 0;
				for(int i = 0; i < data.length; i++)
					sum += data[i];
				return sum;
			}
			@Override
			public byte[] decrypt(byte[] encrypted, byte[] key) throws InvalidKeyException, CRCException, CryptException {
				byte[] decrypted = new byte[encrypted.length - 1];
				System.arraycopy(encrypted, 0, decrypted, 0, decrypted.length);
				byte ck = calcCheckSum(decrypted);
				if(ck != encrypted[encrypted.length-1])
					throw new CRCException("Expected 0x" + StringUtils.toHex(encrypted[encrypted.length-1]) + " check sum; but calculated 0x" + StringUtils.toHex(ck));
				return decrypted;
			}
			
			@Override
			public byte[] decrypt(byte[] encrypted, byte[] key, int blockSize) throws InvalidKeyException, CRCException, CryptException {
				return decrypt(encrypted, key);
			}

			@Override
			public byte[] decrypt(byte[] encrypted, byte[] key, int unencryptedLength, int blockSize) throws InvalidKeyException, CryptException {
				try {
					return decrypt(encrypted, key);
				} catch(CRCException e) {
					throw new CryptException(e.getMessage());
				}
			}

			@Override
			public byte[] encrypt(byte[] unencrypted, byte[] key) throws InvalidKeyException, CryptException {
				byte[] encrypted = new byte[unencrypted.length + 1];
				System.arraycopy(unencrypted, 0, encrypted, 0, unencrypted.length);
				encrypted[encrypted.length-1] = calcCheckSum(unencrypted);
				return encrypted;
			}

			@Override
			public byte[] encrypt(byte[] unencrypted, byte[] key, int blockSize) throws InvalidKeyException, CRCException, CryptException {
				return encrypt(unencrypted, key);
			}

			@Override
			public byte[] encrypt(byte[] unencrypted, byte[] key, byte padding) throws InvalidKeyException, CryptException {
				return encrypt(unencrypted, key);
			}

			@Override
			public byte[] encrypt(byte[] unencrypted, byte[] key, byte padding, int blockSize) throws InvalidKeyException, CryptException {
				return encrypt(unencrypted, key);
			}
		};
		crypts[3] = CryptUtil.getCrypt("RIJNDAEL");
		crypts[4] = CryptUtil.getCrypt("TEA_CRC16");
		crypts[5] = CryptUtil.getCrypt("RIJNDAEL_CRC16");
		crypts[6] = CryptUtil.getCrypt("RIJNDAEL_CRC16");
		crypts[7] = CryptUtil.getCrypt("AES_CBC_CRC16");
		crypts[8] = CryptUtil.getCrypt("AES_CBC_CRC16");
	}

	public Client(String deviceSerialCd, DeviceType deviceType) {
		this.deviceSerialCd = deviceSerialCd;
		this.deviceType = deviceType;
	}

	public int getMessageNumber() {
		return msg;
	}
	public void setMessageNumber(int messageNumber) {
		this.msg = messageNumber;
	}

	public MessageData sendMessages(MessageData[] datas) throws Exception {
		for(MessageData data : datas)
			transmitMessage(data);
		return receiveReply();
	}

	public MessageData sendMessage(MessageData data) throws Exception {
		transmitMessage(data);
		return receiveReply();
	}
	public void transmitMessage(MessageData data) throws Exception {
		String evNumber;
		if (protocolVersion == 2)
			evNumber = new String(ByteArrayUtils.fromHex(this.evNumber));
        else
        	evNumber = this.evNumber;
		data.setDirection(MessageDirection.CLIENT_TO_SERVER);
		data.setMessageId(getAndIncrementMessageId());
		data.setMessageNumber((byte)msg++);
		ByteBuffer buffer = ByteBuffer.allocate(1024);
		data.writeData(buffer, false);
		buffer.flip();
		byte[] bytes = new byte[buffer.remaining()];
		buffer.get(bytes);
		String hex = StringUtils.toHex(bytes);
		log.info("Sending: " + data + " on " + socket.getLocalSocketAddress().toString() + " to " + socket.getRemoteSocketAddress().toString());
		log.info("<- " + hex);
		transmit((byte)protocolVersion, evNumber, bytes, encryptionKey, crypts[protocolVersion], output, protocolVersion > 0);

		output.flush();
	}
	public MessageData receiveReply() {
		byte[] reply = null;
		switch(protocolVersion) {
			case 2:
			case 4:
				try {
					reply = TransmissionProtocol.getProtocol(protocolVersion).receiveFromServer(input, evNumber, encryptionKey).getData();
				} catch(Exception e) {
					log.debug("Input Bytes: " + inputLogging.drainLog());
					log.warn("While reading response for device " + evNumber, e);
					return null;
				}
				break;
			default:
				try {
					/*int version = */input.read();
				} catch(Exception e) {
					log.warn("While waiting for response from server for device " + evNumber, e);
					return null;
				}
				try {
					reply = receive(protocolVersion == 6 || protocolVersion == 8 || protocolVersion == 0 ? evNumber : null, encryptionKey, crypts[protocolVersion], input, protocolVersion > 0);
				} catch(Exception e) {
					log.debug("Input Bytes: " + inputLogging.drainLog());
					log.warn("While reading response for device " + evNumber, e);
					return null;
				}
		}
		log.debug("Input Bytes: " + inputLogging.drainLog());
		log.info("-> " + StringUtils.toHex(reply));
		try {
			MessageData replyData = MessageDataFactory.readMessage(ByteBuffer.wrap(reply), MessageDirection.SERVER_TO_CLIENT);
			log.info("Received: " + replyData  + " on " + socket.getLocalSocketAddress().toString() + " from " + socket.getRemoteSocketAddress().toString());
			switch(replyData.getMessageType()) {
				case GENERIC_RESPONSE_4_1:
					MessageData_CB replyCB = (MessageData_CB) replyData;
					if(replyCB.getServerActionCode() == GenericResponseServerActionCode.PROCESS_PROP_LIST) {
						Map<Integer, String> propertyValues = ((PropertyValueListAction) replyCB.getServerActionCodeData()).getPropertyValues();
						String hex = propertyValues.get(DeviceProperty.ENCRYPTION_KEY.getValue());
						if(hex != null && hex.length() > 0)
							this.encryptionKey = ByteArrayUtils.fromHex(hex);	
					}
					break;
				case SET_ID_NUMBER_AND_KEY:
					MessageData_8F reply8F = (MessageData_8F) replyData;
					this.encryptionKey = reply8F.getEncryptionKey();
					break;
			}
			return replyData;
		} catch(Exception e) {
			log.warn("While reading response for device " + evNumber, e);
			return null;
		}
	}

	public void sendRawBytes(byte[] data) throws IOException {
		output.write(data);
		output.flush();
	}

	public void startCommunication(int protocolVersion) throws ClassNotFoundException, NetworkLayerException, UnknownHostException, IOException, SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		this.protocolVersion = protocolVersion;
		int readTimeout = 15000;
		int writeTimeout = 15000;

		Class<?>[] parameterTypes = { Integer.TYPE, Integer.TYPE };
		Object[] constructorArgs = { new Integer(readTimeout), new Integer(writeTimeout) };
		String className = "com.usatech.networklayer.net.TransmissionProtocolV" + protocolVersion;
		try	{
			Class<?> protocolClass = Class.forName(className);
			Constructor<?> constructor = protocolClass.getConstructor(parameterTypes);
			protocol = (TransmissionProtocol) constructor.newInstance(constructorArgs);
		} catch(InstantiationException e) {
			throw new NetworkLayerException("Failed to create TransmissionProtocol " + className, e);
		}
		socket = new Socket(host, port);
		socket.setSoTimeout(15000);
		inputLogging = new LoggingInputStream(socket.getInputStream());
		input = new DataInputStream(inputLogging);
		output = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
		msg = 0;
		if(log.isDebugEnabled())
			log.debug("Connected device '" + evNumber + "' to " + host + ":" + port + " from port " + socket.getLocalPort());
	}
	public boolean isConnected() {
		return socket != null && !socket.isClosed();
	}

	public int getLocalPort() {
		return socket != null ? socket.getLocalPort() : -1;
	}
	protected static final byte[] HANGUP_BYTES = {(byte)'+', (byte)'+', (byte)'+'};
	public void sendHangup() throws IOException {
		sendHangup(true);
	}

	public void sendHangup(boolean flush) throws IOException {
		output.write(HANGUP_BYTES);
		if(flush)
			output.flush();
	}
	public void stopCommunication() throws IOException {
		input.close();
		output.close();
		socket.close();
	}

	public String getEvNumber() {
		return evNumber;
	}

	public void setEvNumber(String evNumber) {
		this.evNumber = evNumber;
	}

	// tranmission / encryption

	protected int transmit(byte protocol, String deviceName, byte[] data, byte[] encryptionKey, Crypt crypt, DataOutputStream out, boolean sendLength) throws IOException, NetworkLayerException {
        switch(protocol) {
        	case 4:
	        	byte[] encrypted = encryptData(data, encryptionKey, crypt);        
	        	byte[] array = new byte[encrypted.length + deviceName.length()];
	            System.arraycopy(deviceName.getBytes(), 0, array, 0, deviceName.length());
	            System.arraycopy(encrypted, 0, array, 8, encrypted.length);
	
	            byte[] encoded = UUEncode.encode(array);
	            
	            if (log.isDebugEnabled())
	                log.debug("transmitToServer: " + deviceName + ": Encoded: " + StringUtils.toHex(encoded));
	
	            out.write(encoded);
	            out.write(TransmissionProtocolV4.PACKET_DELIMITER);
	            
	        	return encoded.length;
        	case 1:
        		int cnt = 0;
        		StringBuilder header = new StringBuilder();
        		out.write(protocol);
        		StringUtils.appendHex(header, protocol);
        		cnt++;
        		byte[] deviceNameBytes = deviceName.getBytes(ProcessingConstants.US_ASCII_CHARSET);
		        if(sendLength) {
		        	int length = data.length + Math.max(8, deviceNameBytes.length) + 1;
		        	out.write((byte)(length >> 8));
		        	out.write((byte)(length >> 0));
		        	StringUtils.appendHex(header, (short)length);   
		        	cnt += 2;
	        	}
		        for(int i = 0; i < 8 - deviceNameBytes.length; i++) {
		        	out.write(0);// this will pad with 0x00 if length < 8
		        	header.append("00");
		        }
		        out.write(deviceNameBytes);
		        StringUtils.appendHex(header, deviceNameBytes, 0, deviceNameBytes.length);       		
		        cnt += Math.max(8, deviceNameBytes.length);
		        out.write(data);
		        cnt += data.length;
		        int sum = ProtocolV1.calcCheckSum(data, ProtocolV1.calcCheckSum(deviceNameBytes, (byte)0));
		        out.write(sum);
		        cnt++;
		        if (log.isDebugEnabled())
		            log.debug("transmit " + deviceName + ": Header: " + header.toString());
		        return cnt;
        	default:
				encrypted = encryptData(data, encryptionKey, crypt);
		        array = new byte[encrypted.length + (sendLength ? 11 : 9)];
		
		        array[0] = protocol;
		
		        if(sendLength)
		        	ByteArrayUtils.writeUnsignedShort(array, 1, encrypted.length + 8);
		
		        deviceNameBytes = deviceName.getBytes();
		        System.arraycopy(deviceNameBytes, 0, array, (sendLength ? 3 : 1), Math.min(8, deviceNameBytes.length)); // this will pad with 0x00 if length < 8
		        System.arraycopy(encrypted, 0, array, (sendLength ? 11 : 9), encrypted.length);
		        if (log.isDebugEnabled())
		            log.debug("transmit " + deviceName + ": Header: " + StringUtils.toHex(array, 0, (sendLength ? 11 : 9)));
		
		        out.write(array);
		        return array.length;
        }
    }

	protected byte[] encryptData(byte[] data, byte[] encryptionKey, Crypt crypt) throws NetworkLayerException {
		if (log.isDebugEnabled())
			log.debug("Unencrypted: " + StringUtils.toHex(data));
		byte[] encrypted;
		if(crypt != null) {
			try {
				encrypted = crypt.encrypt(data, encryptionKey);
	        } catch(InvalidKeyException e) {
	                throw new NetworkLayerException("Failed to encrypt message (" + StringUtils.toHex(data) + ") : Invalid Key: " + StringUtils.toHex(encryptionKey), e);
	        } catch(CRCException e) {
	                throw new NetworkLayerException("Failed to encrypt message (" + StringUtils.toHex(data) + ") : CRC Check Failed!", e);
			} catch(CryptException e) {
	                throw new NetworkLayerException("Failed to encrypt message (" + StringUtils.toHex(data) + ") : Decryption Algorithm Failure!", e);
			} catch(Exception e) {
	                throw new NetworkLayerException("Caught unexpected exception encrypting message (" + StringUtils.toHex(data) + ") : " + e, e);
	        }
		} else {
			encrypted = data;
		}

		if (log.isDebugEnabled())
	        log.debug("Encrypted: " + StringUtils.toHex(encrypted) + " with key: " + StringUtils.toHex(encryptionKey));

	    return encrypted;
    }

	protected byte[] receive(String deviceName, byte[] encryptionKey, Crypt crypt, DataInputStream in, boolean readLength) throws IOException, NetworkLayerException {
		int expectedLength;
		if(readLength) {
			byte[] lengthArray = new byte[2];
		    try {
		        in.readFully(lengthArray);
		    } catch (EOFException e) {
		        throw new NetworkLayerException("End of stream encountered while reading length bytes.");
		    }
		    expectedLength = ByteArrayUtils.readUnsignedShort(lengthArray, 0);
		} else {
			expectedLength = 9;
		}
	    if(deviceName != null) {
	    	byte[] dn = new byte[8];
	    	in.readFully(dn);
	    	String readDeviceName=new String(dn);
	    	log.info("Readback deviceName:"+readDeviceName);
	    	if(!readDeviceName.equals(deviceName))
	    		throw new NetworkLayerException("Device Name does not match!");
	    	expectedLength = expectedLength - 8;
	    }

	    byte[] encrypted = new byte[expectedLength];
	    try {
	        in.readFully(encrypted);
	    } catch (EOFException e) {
	        throw new NetworkLayerException("End of stream encountered while reading message bytes.");
	    }

	    return decryptData(encrypted, encryptionKey, crypt);
	}
	protected byte[] decryptData(byte[] data, byte[] encryptionKey, Crypt crypt) throws NetworkLayerException {
		if (log.isDebugEnabled())
			log.debug("Undecrypted: " + StringUtils.toHex(data));
		byte[] decrypted;
		if(crypt != null) {
			try {
				decrypted = crypt.decrypt(data, encryptionKey);
	        } catch(InvalidKeyException e) {
	                throw new NetworkLayerException("Failed to decrypt message (" + StringUtils.toHex(data) + ") : Invalid Key: " + StringUtils.toHex(encryptionKey), e);
	        } catch(CRCException e) {
	                throw new NetworkLayerException("Failed to decrypt message (" + StringUtils.toHex(data) + ") : CRC Check Failed!", e);
			} catch(CryptException e) {
	                throw new NetworkLayerException("Failed to decrypt message (" + StringUtils.toHex(data) + ") : Decryption Algorithm Failure!", e);
			} catch(Exception e) {
	                throw new NetworkLayerException("Caught unexpected exception decrypting message (" + StringUtils.toHex(data) + ") : " + e, e);
	        }
		} else {
			decrypted = data;
		}

		if (log.isDebugEnabled())
	        log.debug("Decrypted: " + StringUtils.toHex(decrypted));

	    return decrypted;
    }

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public long getMasterId() {
		return masterIdCache.getOrCreate(getDeviceSerialCd()).get();
	}

	public long getAndIncrementMasterId() {
		return masterIdCache.getOrCreate(getDeviceSerialCd()).getAndIncrement();
	}
	public byte[] getEncryptionKey() {
		return encryptionKey;
	}
	public void setEncryptionKey(byte[] encryptionKey) {
		this.encryptionKey = encryptionKey;
	}

	public long getMessageId() {
		return messageIdCache.getOrCreate(getDeviceSerialCd()).get();
	}

	public long getAndIncrementMessageId() {
		return messageIdCache.getOrCreate(getDeviceSerialCd()).getAndIncrement();
	}

	public void setMessageIdIfGreater(long messageId) {
		AtomicLong cached = messageIdCache.getOrCreate(getDeviceSerialCd());
		while(true) {
			long prev = cached.get();
			if(messageId > prev) {
				if(cached.compareAndSet(prev, messageId))
					return;
			} else
				return;
		}
	}

	public void setMasterIdIfGreater(long masterId) {
		AtomicLong cached = masterIdCache.getOrCreate(getDeviceSerialCd());
		while(true) {
			long prev = cached.get();
			if(masterId > prev) {
				if(cached.compareAndSet(prev, masterId))
					return;
			} else
				return;
		}
	}

	public String getTimeZoneGuid() {
		return timeZoneGuid;
	}

	public void setTimeZoneGuid(String timeZoneGuid) {
		this.timeZoneGuid = timeZoneGuid;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getDeviceSerialCd() {
		return deviceSerialCd;
	}

	public DeviceType getDeviceType() {
		return deviceType;
	}

}
