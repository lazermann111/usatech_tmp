package com.usatech.tools;

import org.junit.Test;

import com.usatech.layers.common.MessageResponseUtils;

import simple.security.SecurityUtils;

public class LuhnUtils {		
	public static void main(String args[]) throws Exception {
		System.out.println("Mastercard : " + LuhnUtils.fillInTrackData(LuhnUtils.generateCard("51", 16)));
		System.out.println("Visa       : " + LuhnUtils.fillInTrackData(LuhnUtils.generateCard("4", 16)));
		System.out.println("Discover   : " + LuhnUtils.fillInTrackData(LuhnUtils.generateCard("6011", 16)));
		System.out.println("Amex       : " + LuhnUtils.fillInTrackData(LuhnUtils.generateCard("34", 15)));
	}

	public static String fillInTrackData(String trackData) {
		return SecurityUtils.fillInTrackData(trackData);
	}
	public static boolean check(String accountNumberWithCheckDigit)
	{
		int[] a = new int[accountNumberWithCheckDigit.length()];
		for(int i=0; i<a.length; i++)
			a[i] = Character.getNumericValue(accountNumberWithCheckDigit.charAt(i));
		
		int len = a.length;
		int checksum = 0;
		int temp;
		
		for(int i = 1; i < len; i++) {
			// don't include last digit
			temp = a[len - i - 1] * (1 + (i % 2));
			if (temp < 10)
				checksum += temp;
			else
				checksum += temp - 9;
		}
		checksum = (10 - (checksum % 10)) % 10;
		
		if (a[a.length-1] == checksum)
			return true;
		else
			return false;
	}

	public static int calcDigit(String accountCd, int index) {
		return SecurityUtils.calcDigit(accountCd, index);
	}

	public static int calcChecksum(String accountCd) {
		int[] a = new int[accountCd.length()];
		for(int i = 0; i < a.length; i++)
			a[i] = Character.getNumericValue(accountCd.charAt(i));

		int len = a.length;
		int checksum = 0;
		int tmp;

		for(int i = (len % 2); i < len; i += 2) {
			tmp = a[i] * 2;
			while(tmp > 9)
				tmp = (tmp % 10) + (tmp / 10);
			checksum += tmp;
		}
		for(int i = ((len + 1) % 2); i < len; i += 2) {
			tmp = a[i];
			while(tmp > 9)
				tmp = (tmp % 10) + (tmp / 10);
			checksum += tmp;
		}

		return checksum % 10;
	}
	@Test
	public void junit_luhn() throws Exception {
		String trackData = generateCard("41", 16);
		System.out.println("Generated masked card: " + trackData);
		String filledIn = fillInTrackData(trackData.toString());
		System.out.println("Generated filled card: " + filledIn);
		String cardNum = MessageResponseUtils.getCardNumber(filledIn);
		System.out.println("Card Number: " + cardNum);
		boolean okay = check(cardNum);
		System.out.println("Luhn Check: " + okay);	
	}
	
	@Test
	public void junit_find_digit() throws Exception {
		String trackData = generateCard("41", 16);
		String filledIn = fillInTrackData(trackData.toString());
		String cardNum = MessageResponseUtils.getCardNumber(filledIn);
		System.out.println("Generated Card Number: " + cardNum);
		boolean okay = check(cardNum);
		System.out.println("Luhn Check: " + okay);
		for(int i = 0; i < cardNum.length(); i++) {
			System.out.println("Calced Digit " + (i + 1) + " = " + calcDigit(cardNum, i) + " vs " + cardNum.charAt(i));
		}
	}

	@Test
	public void junit_checksum() throws Exception {
		for(int i = 0; i < 10; i++) {
			String trackData = generateCard("41", 16);
			String filledIn = fillInTrackData(trackData.toString());
			String cardNum = MessageResponseUtils.getCardNumber(filledIn);
			System.out.println("Generated Card Number: " + cardNum);
			boolean okay = check(cardNum);
			System.out.println("Luhn Check: " + okay);
			System.out.println("Checksum = " + calcChecksum(cardNum));
		}
	}

	@Test
	public void junit_checksum2() throws Exception {
		String cardNum = "6396212009676752901";
		System.out.println("Generated Card Number: " + cardNum);
		boolean okay = check(cardNum);
		System.out.println("Luhn Check: " + okay);
		System.out.println("Checksum = " + calcChecksum(cardNum));
		System.out.println("Checksum1 = " + addChecksum(cardNum.getBytes(), 0, cardNum.length()));
	}

	@Test
	public void junit_generate() throws Exception {
		String trackData = LuhnUtils.generateCard("40", 16);
		String filledIn = LuhnUtils.fillInTrackData(trackData);
		System.out.println("Generated Track: " + filledIn);
		String cardNum = MessageResponseUtils.getCardNumber(filledIn);
		boolean okay = check(cardNum);
		System.out.println("Luhn Check: " + okay);
		System.out.println("Checksum = " + calcChecksum(cardNum));
		System.out.println("Checksum1 = " + addChecksum(cardNum.getBytes(), 0, cardNum.length()));
	}

	protected int addChecksum(byte[] cardArray, int start, int end) {
		int len = cardArray.length;
		int checksum = 0;
		int tmp;

		for(int i = start + (len % 2); i < end; i += 2) {
			tmp = Character.getNumericValue(cardArray[i]) * 2;
			while(tmp > 9)
				tmp = (tmp % 10) + (tmp / 10);
			checksum += tmp;
		}
		for(int i = start + ((len + 1) % 2); i < end; i += 2) {
			tmp = Character.getNumericValue(cardArray[i]);
			while(tmp > 9)
				tmp = (tmp % 10) + (tmp / 10);
			checksum += tmp;
		}

		return checksum;
	}

	public static String generateCard(String prefix, int len) {
		return generateCard(prefix, len, null);
	}

	public static String generateCard(String prefix, int len, Integer addMonths) {
		return SecurityUtils.generateCard(prefix, len, addMonths);
	}

	@Test
	public void junit_luhn_2() throws Exception {
		for(int i = 0; i < 100; i++) {
			String trackData = generateCard("53", 16);
			String filledIn = fillInTrackData(trackData.toString());
			String cardNum = MessageResponseUtils.getCardNumber(filledIn);
			boolean okay = check(cardNum);
			System.out.println("For card " + filledIn + ", Luhn Check: " + okay);	
		}
	}	

	public static int mod97(String s) {
		return SecurityUtils.mod97(s);
	}
}
