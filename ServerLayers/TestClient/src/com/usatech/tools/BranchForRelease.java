package com.usatech.tools;

import java.awt.Window;
import java.io.Console;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpState;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.PostMethod;

import com.sshtools.j2ssh.transport.HostKeyVerification;
import com.usatech.tools.deploy.DefaultSshClientCache;
import com.usatech.tools.deploy.DeployUtils;
import com.usatech.tools.deploy.MapPasswordCache;
import com.usatech.tools.deploy.PasswordCache;
import com.usatech.tools.deploy.SshClientCache;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.ConsoleInteraction;
import simple.io.GuiInteraction;
import simple.io.InOutInteraction;
import simple.io.Interaction;
import simple.io.Log;
import simple.io.resource.sftp.SftpUtils;
import simple.text.StringUtils;

public class BranchForRelease {
	private static final Log log = Log.getLog();
	protected static final Pattern TAG_PATTERN = Pattern.compile("REL_(\\w+)(?:_\\d+){3}");
	protected static class SlashCheck extends FilterOutputStream {
		protected int pos = 0;
		public SlashCheck(OutputStream out) {
			super(out);
		}
		@Override
		public void write(int b) throws IOException {
			checkChar((char) b);
			super.write(b);
		}
		@Override
		public void write(byte[] b, int off, int len) throws IOException {
			for(int i = off; i < off + len; i++) {
				checkChar((char) b[i]);
			}
			super.write(b, off, len);
		}

		public boolean isNeedsSlash() {
			return pos == 4;
		}

		protected void checkChar(char ch) {
			switch(pos) {
				case 0: // e, E
					if('e' == ch || 'E' == ch)
						pos++;
					break;
				case 1:
					if('n' == ch || 'N' == ch)
						pos++;
					else
						pos = 0;
					break;
				case 2:
					if('d' == ch || 'D' == ch)
						pos++;
					else
						pos = 0;
					break;
				case 3:
					if(';' == ch)
						pos++;
					else if(!Character.isWhitespace(ch))
						pos = 0;
					break;
				case 4:
					if('/' == ch)
						pos++;
					else if(!Character.isWhitespace(ch))
						pos = 0;
					break;
				default:
					if(!Character.isWhitespace(ch))
						pos = 0;
			}
		}
	};
	protected static String getSystemProperty(String systemProperty, String defaultValue) {
		String value = System.getProperty(systemProperty);
		if (value == null || value.trim().length() == 0)
			return defaultValue;
		else
			return value;
	}
	
	/**
	 * @param args
	 * @throws IOException
	 * @throws URISyntaxException
	 * @throws ConvertException
	 */
	public static void main(String[] args) {
		int release = 58;
		String kbPagePrefix = getSystemProperty("kb.page.prefix", "Edge Server ");
		Interaction interaction;
		Console console;
		if((console = System.console()) != null) {
			interaction = new ConsoleInteraction(console);
		} else if(!java.awt.GraphicsEnvironment.isHeadless()) {
			interaction = new GuiInteraction((Window)null);
		} else {
			interaction = new InOutInteraction(System.out, System.in);
		}
		String user = System.getProperty("user.name");
		String releaseStr = interaction.readLine("Enter the release number [%1$s]", release);

		try {
			if(!StringUtils.isBlank(releaseStr))
				release = ConvertUtils.getInt(releaseStr);

			String username1 = user;
			char[] password1 = interaction.readPassword("Enter the knowledgebase password for %1$s:", username1);
			if(password1 == null)
				return;
			Pattern contextPattern = Pattern.compile("Associated CVS Tags:?.*?</h4>\\s*<[ou]l>", Pattern.DOTALL);
			Pattern tagPattern = Pattern.compile("\\s*<li>\\s*(\\w+)\\s*</li>", Pattern.DOTALL);

			String kbPageContent = getKBPageContent(interaction, kbPagePrefix + 'R' + release, username1, new String(password1));
			Matcher matcher = contextPattern.matcher(kbPageContent);
			PrintWriter pw = interaction.getWriter();
			if(!matcher.find()) {
				pw.println("<==========>");
				pw.println(kbPageContent);
				pw.println("<==========>");
				pw.flush();
				throw new IOException("Context not found");
			}
			Matcher tagMatcher = tagPattern.matcher(kbPageContent);
			tagMatcher.region(matcher.end(), kbPageContent.length());
			Set<String> tags = new LinkedHashSet<String>();
			while(tagMatcher.lookingAt()) {
				tags.add(tagMatcher.group(1));
				tagMatcher.region(tagMatcher.end(), kbPageContent.length());
			}
			if(tags.isEmpty())
				throw new IOException("No CVS tags found");
			interaction.getWriter().println("Create branch 'BRN_R" + release + " on the following tags:");
			for(String tag : tags)
				interaction.getWriter().println("\t" + tag);
			interaction.getWriter().flush();
			String answer = interaction.readLine("Proceed (yes, no)? [no]");
			if(ConvertUtils.getBoolean(answer, false)) {
				final PasswordCache passwordCache = new MapPasswordCache();
				HostKeyVerification hostKeyVerification = SftpUtils.createHostKeyVerification(System.getProperty("user.home", ".") + "/knownhosts.lst");
				SshClientCache sshClientCache = new DefaultSshClientCache(interaction, hostKeyVerification, passwordCache);
				for(String tag : tags) {
					int section = interaction.startSection("Create branch 'BRN_R" + release + " on '" + tag + "'...");
					Matcher relMatcher = TAG_PATTERN.matcher(tag);
					if(!relMatcher.matches()) {
						interaction.endSection(section, false, "Invalid tag '" + tag + "'");
						continue;
					}
					String app = relMatcher.group(1);
					Collection<String> modules = getModules(app);
					if(modules == null) {
						interaction.endSection(section, false, "No modules are specified for App '" + app + "'. Please adjust code in " + BranchForRelease.class.getName());
						continue;
					}
					List<String> arguments = new ArrayList<String>();
					arguments.add("-b");
					arguments.add("-F");
					arguments.add("-B");// move branch
					arguments.add("-r" + tag);
					arguments.add("BRN_R" + release);
					arguments.addAll(modules);
					try {
						DeployUtils.sendCvsCommand(interaction, sshClientCache, user, null, "rtag", arguments.toArray(new String[arguments.size()]));
					} catch(IOException e) {
						e.printStackTrace();
						interaction.getWriter().println("Error Occurred: " + e.getMessage());
						interaction.getWriter().flush();
						interaction.endSection(section, false, "Failed to create branch because " + e.getMessage());
					}
					interaction.endSection(section, true, "Branch created");
				}
			}
		} catch(IOException e) {
			e.printStackTrace();
			interaction.getWriter().println("Error Occurred: " + e.getMessage());
			interaction.getWriter().flush();
		} catch(ConvertException e) {
			e.printStackTrace();
			interaction.getWriter().println("Error Occurred: " + e.getMessage());
		} catch(URISyntaxException e) {
			e.printStackTrace();
			interaction.getWriter().println("Error Occurred: " + e.getMessage());
		}
	}


	protected static Collection<String> getModules(String app) {
		switch(app.toUpperCase()) {
			case "APPLAYER":
				return Arrays.asList(new String[] {"ServerLayers/AppLayer/src", "ServerLayers/AppLayer/db",
						"ServerLayers/LayersCommon/src", "USATMessaging/src",
						"ePortConnectSDK/ePortConnectClient/src",
						"Simple1.5/src"});
			case "AUTHORITYLAYER":
				return Arrays.asList(new String[] {"ServerLayers/AuthorityLayer/src","ServerLayers/AuthorityLayer/override_src",
						"middleware/ISO8583", "posinterface", "aramark", "blackboard",
						"ServerLayers/LayersCommon/src", "USATMessaging/src",
						"ePortConnectSDK/ePortConnectClient/src",
						"Simple1.5/src"});
			case "DMS":
				return Arrays.asList(new String[] {"DMS/src","DMS/resources","DMS/webroot",
						"Falcon/web/scripts", "CardNumberGenerator/src",
						"ServerLayers/LayersCommon/src", "USATMessaging/src",
						"ePortConnectSDK/ePortConnectClient/src",
						"Simple1.5/src"});
			case "KEYMGR":
				return Arrays.asList(new String[] {"KeyManagerService/src","KeyManagerService/keydb","KeyManagerService/web","KeyManagerService/classes","KeyManager",
						"ServerLayers/LayersCommon/src", "USATMessaging/src",
						"Falcon/src","Falcon/web/css","Falcon/web/general","Falcon/web/images","Falcon/web/scripts",
						"ePortConnectSDK/ePortConnectClient/src",
						"Simple1.5/src"});
			case "NETLAYER":
				return Arrays.asList(new String[] { "ServerLayers/NetLayer/src", "ServerLayers/NetLayer/web",
						"ServerLayers/LayersCommon/src", "USATMessaging/src",
						"ePortConnectSDK/ePortConnectClient/src",
						"Simple1.5/src"});
			case "POSMLAYER":
				return Arrays.asList(new String[] { "ServerLayers/POSMLayer/src", "ServerLayers/POSMLayer/conf",
						"ServerLayers/LayersCommon/src", "USATMessaging/src",
						"ePortConnectSDK/ePortConnectClient/src",
						"Simple1.5/src"});
			case "PREPAID":
				return Arrays.asList(new String[] { "Prepaid/src","Prepaid/web",
						"Falcon/web",
						"ServerLayers/LayersCommon/src", "USATMessaging/src",
						"ePortConnectSDK/ePortConnectClient/src",
						"Simple1.5/src"});
			case "REPORT_GENERATOR":
				return Arrays.asList(new String[] { "ReportGenerator/src", "ReportGenerator/resources", "usalive/src", "usalive/xsl", "usalive/override-src",
						"ServerLayers/LayersCommon/src", "USATMessaging/src",
						"Falcon/src","Falcon/web/css","Falcon/web/general","Falcon/web/images","Falcon/web/scripts",
						"ePortConnectSDK/ePortConnectClient/src",
						"Simple1.5/src"});
			case "USALIVE":
				return Arrays.asList(new String[] {"usalive/src","usalive/xsl","usalive/override-src","usalive/customerreporting_web","usalive/web","usalive/hotchoice_web","usalive/verizon_web","ReportGenerator/src/com/usatech/report","ReportGenerator/src/com/usatech/transport", 
						"ServerLayers/LayersCommon/src", "USATMessaging/src",
						"Falcon/src","Falcon/web/css","Falcon/web/general","Falcon/web/images","Falcon/web/scripts",
						"USALiveClient/src", "ePortConnectSDK/ePortConnectClient/src",
						"Simple1.5/src"});
			case "ESUDSWEBSITE":
				return Arrays.asList(new String[] {"eSudsWebsite/src","eSudsWebsite/conf","eSudsWebsite/web",
						"Simple1.5/src", "Falcon/src","Falcon/web/css","Falcon/web/general","Falcon/web/images","Falcon/web/scripts"});
		}
		return null;
	}
	protected static String getKBPageContent(Interaction interaction, String kbPage, String username, String password) throws IOException, URISyntaxException {
		URI kbLoginUri = new URI("https://knowledgebase.usatech.com/JSPWiki/Login.jsp");
		PostMethod method = new PostMethod(kbLoginUri.toString());
		method.addParameter("redirect", kbPage);
		HttpState state = new HttpState();
		state.setCredentials(new AuthScope(null, -1, null, "https"), new UsernamePasswordCredentials(username, password));
		//HttpClientParams params = new HttpClientParams();
		HttpClient client = new HttpClient();
		int code = client.executeMethod(null, method, state);
        if(code != HttpStatus.SC_OK) {
        	throw new IOException("Request failed with status code " + code + " (" + method.getStatusText() + ")");
        }
        
        URI kbUri = new URI("https://knowledgebase.usatech.com/JSPWiki/j_security_check");
		method = new PostMethod(kbUri.toString()) {
			public boolean getFollowRedirects() {
		        return true;
		    }
		};
		method.addParameter("redirect", kbPage);
		method.addParameter("j_username", username);
		method.addParameter("j_password", password);
		method.addParameter("submitlogin", "Login");
		
		code = client.executeMethod(null, method, state);
        if(code != HttpStatus.SC_OK) {
        	throw new IOException("Request failed with status code " + code + " (" + method.getStatusText() + ")");
        }
        String response = method.getResponseBodyAsString();
		return response;
	}
}
