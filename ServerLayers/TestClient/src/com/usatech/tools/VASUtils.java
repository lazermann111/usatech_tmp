package com.usatech.tools;

import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.KeyAgreement;

import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.params.KDFParameters;
import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECParameterSpec;
import org.bouncycastle.jce.spec.ECPublicKeySpec;
import org.bouncycastle.math.ec.ECCurve;
import org.bouncycastle.math.ec.ECPoint;

import simple.io.Base64;

/**
 * This is the util to decrypt apple vas see Wallet Loyalty Enhancements latest document
 * @author yhe
 *
 */
public class VASUtils {
	
	public static long appleToEpochTimeDiff=978307200000l;
	
	static {
		initialize();
	}

	private static void initialize() {
		Security.addProvider(new BouncyCastleProvider());
	}
	
	public static PublicKey getECDHPublicKeyFromCoordinate(byte[] encoded) throws NoSuchProviderException, NoSuchAlgorithmException, InvalidKeySpecException{
	  ECParameterSpec ecSpec = ECNamedCurveTable.getParameterSpec("prime256v1");
	  ECCurve curve = ecSpec.getCurve(); 
		byte[] compressed=new byte[33];
		compressed[0]=0x02;//assumming y is 0
		System.arraycopy(encoded, 0, compressed, 1, compressed.length-1);
		System.out.println("publicKeyBack="+Base64.encodeBytes(compressed,true));
    ECPoint ptFlat = curve.decodePoint(compressed);
    ECPublicKeySpec pubKeySpec = new ECPublicKeySpec(
    		ptFlat, 
				ecSpec);
    KeyFactory keyFactory = KeyFactory.getInstance("ECDH", "BC");
    PublicKey pubKeyBack = keyFactory.generatePublic(pubKeySpec);
    return pubKeyBack;
	}
	
	public static byte[] getECDHSecret (PrivateKey privateKey, PublicKey publicKey) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException{
		KeyAgreement ka = KeyAgreement.getInstance("ECDH", "BC");
		ka.init(privateKey);
		ka.doPhase(publicKey, true);
		byte [] secret = ka.generateSecret();
		return secret;
	}
	
	
	/**
	 * 978307200000 is the milliseconds diff from midnight UTC 1 January 2001 to java epoch
	 * @param seconds
	 * @return the currentTime - input time
	 */
	public static long passedTimeDiff(long seconds){
		long currentTime=System.currentTimeMillis();
		long inputTime=seconds*1000+appleToEpochTimeDiff;
		return currentTime-inputTime;
	}
	
}
