package com.usatech.tools;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.math.BigInteger;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.spec.X509EncodedKeySpec;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.configuration.ConfigurationUtils;
import org.apache.commons.configuration.FileSystem;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.util.BigIntegers;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemWriter;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.usatech.layers.common.HttpRequester;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.AuthResponseCodeA1;
import com.usatech.layers.common.constants.AuthResponseCodeAF;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.FileType;
import com.usatech.layers.common.constants.GenericResponseResultCode;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageDataUtils;
import com.usatech.layers.common.messagedata.MessageData_C7;
import com.usatech.layers.common.messagedata.MessageData_CB;
import com.usatech.report.GenerateAttrEnum;
import com.usatech.report.StandbyAllowance;
import com.usatech.test.ClientEmulator;

import de.flexiprovider.core.md.SHA256;

import simple.app.Base;
import simple.app.MainWithConfig;
import simple.bean.ConvertUtils;
import simple.bean.converters.DateConverter;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.NotEnoughRowsException;
import simple.db.config.ConfigLoader;
import simple.db.pool.PoolDataSource;
import simple.db.pool.PoolDataSourceFactory;
import simple.io.Base64;
import simple.io.ByteArrayUtils;
import simple.io.ConfigSource;
import simple.io.IOUtils;
import simple.io.Log;
import simple.results.Results;
import simple.security.SecureHash;
import simple.security.crypt.CryptUtils;
import simple.servlet.RequestUtils;
import simple.test.UnitTest;
import simple.text.IntervalFormat;
import simple.text.StringUtils;
import simple.util.CollectionUtils;
import simple.util.FilterMap;
import simple.util.concurrent.BoundedCache;
import simple.util.concurrent.LockLinkedSegmentCache;
import simple.util.concurrent.SegmentCache;

import java.security.KeyFactory;

public class ReleaseUnitTests extends UnitTest {
	protected static final ClientEmulatorHelper helper = new ClientEmulatorHelper();
	
	protected static final String outputPath="D:\\public\\project2014\\TestClient\\output";

	static {
		System.setProperty("app.servicename", "ePortConnectHessianTest");
		System.setProperty("javax.net.ssl.trustStore", "../LayersCommon/conf/net/truststore.ts");
		System.setProperty("javax.net.ssl.trustStorePassword", "usatech");
		System.setProperty("javax.net.ssl.keyStore", "../LayersCommon/conf/kls/keystore.ks");
		System.setProperty("javax.net.ssl.keyStorePassword", "usatech");
	}

	@Before
	public void setUp() throws Exception {
		setupLog();
		System.setProperty("javax.xml.parsers.SAXParserFactory", "com.sun.org.apache.xerces.internal.jaxp.SAXParserFactoryImpl");
	}

	/**
	 * @since R51
	 */
	@Test
	public void maskCardData() throws Exception {
		checkCardMasking("1234567890", "1234567890", "1234567890", "1234567890");
		checkCardMasking("5454545454545454", "545454******5454", "545454******5454", "545454******5454");
		checkCardMasking(";5454545454545454=12345677897?3", ";545454******5454=***********?*", "545454******5454", ";545454******5454=***********?*");
		checkCardMasking("11156005=Pin#5277", "11156005=@@@#****", "11156005=Pin#5277", "11156005=Pin#5277");
		checkCardMasking("1115600598765432=Pin#5277", "111560******5432=@@@#****", "111560******5432", "111560******5432=@@@#****");
		checkCardMasking("+123456789", "+*********", "*********", "+*********");
		checkCardMasking("0987654321+123456789", "0987654321+*********", "0987654321", "0987654321+*********");
		checkCardMasking("09876543210987654321+123456789", "098765**********4321+*********", "098765**********4321", "098765**********4321+*********");
		checkCardMasking("4141237418974952|1505|1234||19350|", "414123******4952|****|****||*****|", "414123******4952", "414123******4952|****|****||*****|");
		checkCardMasking("4141237418974952|1505|1234||A7B9J0|", "414123******4952|****|****||@*@*@*|", "414123******4952", "414123******4952|****|****||@*@*@*|");

	}

	protected void checkCardMasking(String cardData, String expectedTrackMask, String expectedCardMask, String expectedAnyMask) {
		String trackMask = MessageResponseUtils.maskTrackData(cardData);
		String cardMask = MessageResponseUtils.maskCardNumber(cardData);
		String anyMask = MessageResponseUtils.maskAnyCardNumbers(cardData);
		log.info("Masking '" + cardData + "' yielded '" + trackMask + "' or '" + cardMask + "' or '" + anyMask + "'");
		assert expectedTrackMask.equals(trackMask) : "Masked Track Data '" + trackMask + "' is not as expected '" + expectedTrackMask + "' for '" + cardData + "'";
		assert expectedCardMask.equals(cardMask) : "Masked Card Data '" + cardMask + "' is not as expected '" + expectedCardMask + "' for '" + cardData + "'";
		assert expectedAnyMask.equals(anyMask) : "Masked Any Data '" + anyMask + "' is not as expected '" + expectedAnyMask + "' for '" + cardData + "'";
	}

	/**
	 * @since R51
	 */
	@Test
	public void parseHttpParameters() throws Exception {
		Map<String, Object> result = RequestUtils.parseParameters("abc=1&def=2&abc=3&ghi=%20spaces%20&xyz=%3D");
		Map<String, Object> expected = new HashMap<String, Object>();
		expected.put("abc", new String[] { "1", "3" });
		expected.put("def", "2");
		expected.put("ghi", " spaces ");
		expected.put("xyz", "=");
		if(CollectionUtils.getDiffCount(result, expected) > 0)
			throw new Exception("Result: " + result + " does not match expected: " + expected);
	}

	/**
	 * Tests data source factory connection to standby (read-only) database with failover to primary when staleness is too much
	 * 
	 * @since R51
	 */
	@Test
	public void failoverStandbyDataSource() throws Exception {
		failoverStandbyDataSource(10L);
		failoverStandbyDataSource(Long.MAX_VALUE - 1);// 172800000L
	}

	protected void failoverStandbyDataSource(long maxStaleness) throws Exception {
		Properties props = new Properties();
		props.put("tracking", "false");
		props.put("dynamicLoadingAllowed(TEST)", "true");
		props.put("TEST.driverClassName", "oracle.jdbc.driver.OracleDriver");
		props.put("TEST.url", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS=(PROTOCOL=TCP)(HOST=devdb013.usatech.com)(PORT=1521))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=usasdev04.world)))");
		props.put("TEST.failoverUrls", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb012.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=USADEV04.WORLD)))");
		props.put("TEST.username", "APP_LAYER_1");
		props.put("TEST.password", "APP_LAYER_1");
		props.put("TEST.testOnBorrow", "true");
		props.put("TEST.validationQuery", "{call DBMS_SESSION.MODIFY_PACKAGE_STATE(DBMS_SESSION.REINITIALIZE)}");
		props.put("TEST.maxIdle", "1");
		props.put("TEST.maxWait", "6000");
		props.put("TEST.createTimeout", "5000");
		props.put("TEST.maxActive", "3");
		props.put("TEST.defaultAutoCommit", "false");
		props.put("TEST.maxAge", "12000");
		props.put("TEST.connectionProperties", "oracle.net.disableOob=true;oracle.net.CONNECT_TIMEOUT=5000");
		props.put("TEST.stalenessQuery", "SELECT 1000 * (EXTRACT(SECOND FROM DIFF) + EXTRACT(MINUTE FROM DIFF) * 60 + EXTRACT(HOUR FROM DIFF) * 60 * 60 + EXTRACT(DAY FROM DIFF) * 60 * 60 * 24) FROM (SELECT CURRENT_TIMESTAMP - SCN_TO_TIMESTAMP(CURRENT_SCN) DIFF FROM V$DATABASE)");
		props.put("TEST.maxStaleness", String.valueOf(maxStaleness));
		PoolDataSourceFactory dsf = new PoolDataSourceFactory(props);
		PoolDataSource ds = (PoolDataSource) dsf.getDataSource("TEST");
		IntervalFormat intervalFormat = new IntervalFormat();
		String infoQuery = "SELECT DB_UNIQUE_NAME, CONTROLFILE_TYPE, 1000 * (EXTRACT(SECOND FROM DIFF) + EXTRACT(MINUTE FROM DIFF) * 60 + EXTRACT(HOUR FROM DIFF) * 60 * 60 + EXTRACT(DAY FROM DIFF) * 60 * 60 * 24) FROM (SELECT DB_UNIQUE_NAME, CONTROLFILE_TYPE, CURRENT_TIMESTAMP - SCN_TO_TIMESTAMP(CURRENT_SCN) DIFF FROM V$DATABASE)";
		String type;
		long staleness;
		try (Connection conn = ds.getConnection()) {
			// find out which db it is
			try (Results results = DataLayerMgr.executeSQLx(conn, infoQuery, null, null)) {
				if(!results.next())
					throw new NotEnoughRowsException();
				String name = results.getValue(1, String.class);
				type = results.getValue(2, String.class);
				staleness = results.getValue(3, Long.class);
				log.info("Connected to database '" + name + "' in mode '" + type + "' where staleness is " + intervalFormat.format(staleness));
			}
		}
		switch(type) {
			case "CURRENT": // the primary read-write db
				//ds.setMaxStaleness(Long.MAX_VALUE - 1);
				boolean done = false;
				int attempts = ds.getMaxActive() * 2;
				try (Connection conn = ds.getConnection()) { // need to hold onto the first connection so another one is created
					for(int i = 0; i < attempts; i++) {
						try (Connection cycleConn = ds.getConnection()) {
							try (Results results = DataLayerMgr.executeSQLx(cycleConn, infoQuery, null, null)) {
								if(!results.next())
									throw new NotEnoughRowsException();
								String name = results.getValue(1, String.class);
								type = results.getValue(2, String.class);
								staleness = results.getValue(3, Long.class);
								log.info("Connected to database '" + name + "' in mode '" + type + "' where staleness is " + intervalFormat.format(staleness));
								if(!"CURRENT".equals(type)) {
									done = true;
									break;
								}
							}
						}
					}
				}
				if(!done)
					throw new DataLayerException("Did not connect to secondary database after " + attempts + " attempts");
				break;
			case "STANDBY": // the secondary read-only db
				if(staleness < 10L)
					log.warn("Staleness is only " + staleness + " ms - can't effectively test failover");
				else {
					//ds.setMaxStaleness(staleness / 2);
					// cycle through all connections to ensure expiration
					done = false;
					attempts = ds.getMaxActive() * 2;
					for(int i = 0; i < attempts; i++) {
						try (Connection cycleConn = ds.getConnection()) {
							try (Results results = DataLayerMgr.executeSQLx(cycleConn, infoQuery, null, null)) {
								if(!results.next())
									throw new NotEnoughRowsException();
								String name = results.getValue(1, String.class);
								type = results.getValue(2, String.class);
								staleness = results.getValue(3, Long.class);
								log.info("Connected to database '" + name + "' in mode '" + type + "' where staleness is " + intervalFormat.format(staleness));
								if(!"STANDBY".equals(type)) {
									done = true;
									break;
								}
							}
						}
					}
					if(!done)
						throw new DataLayerException("Did not connect to primary database after " + attempts + " attempts");
				}
		}

		log.info("Test succeeded");
	}


	/**
	 * @since R50
	 */
	@Test
	public void hessianChargeManualBadCvv() throws Exception {
		helper.hessianChargeManual("K3MTB000001", "5454545454545454", helper.formatExpirationDate(12), "123", "44444", 200, 9);
	}

	/**
	 * @since R50
	 */
	@Test
	public void hessianChargeManualShortCvv() throws Exception {
		helper.hessianChargeManual("K3MTB000001", "5454545454545454", helper.formatExpirationDate(12), "12", "44444", 200, 3);
	}

	/**
	 * @since R50
	 */
	@Test
	public void hessianChargeManualExpired() throws Exception {
		helper.hessianChargeManual("K3MTB000001", "5454545454545454", helper.formatExpirationDate(-2), "111", "44444", 200, 3);
	}

	/**
	 * @since R50
	 */
	@Test
	public void hessianChargeManualExpiresThisMonth() throws Exception {
		helper.hessianChargeManual("K3MTB000001", "5454545454545454", helper.formatExpirationDate(0), "111", "44444", 200, 2);
	}

	/**
	 * @since R50
	 */
	@Test
	public void hessianChargeManualBadExpireDate() throws Exception {
		helper.hessianChargeManual("K3MTB000001", "5454545454545454", "11", "111", "44444", 200, 3);
	}

	/**
	 * @since R50
	 */
	@Test
	public void hessianChargeManualGood() throws Exception {
		helper.hessianChargeManual("K3MTB000001", "5454545454545454", helper.formatExpirationDate(12), "111", "44444", 200, 2);
	}

	/**
	 * @since R50
	 */
	@Test
	public void hessianChargeManualBadPostal() throws Exception {
		helper.hessianChargeManual("K3MTB000001", "5454545454545454", helper.formatExpirationDate(12), "111", "11111", 200, 10);
	}

	/**
	 * @since R50
	 */
	@Test
	public void hessianChargeSwipeExpired() throws Exception {
		helper.hessianChargeSwipe("K3MTB000001", ";5454545454545454=" + helper.formatExpirationDate(-1) + "1015432112345601?", 200, 3);
	}

	/**
	 * @since R50
	 */
	@Test
	public void hessianChargeSwipeGood() throws Exception {
		helper.hessianChargeSwipe("K3MTB000001", ";5454545454545454=" + helper.formatExpirationDate(12) + "1015432112345601?", 200, 2);
	}

	/**
	 * @since R51
	 */
	@Test
	public void hessianChargeSwipeBadCountry() throws Exception {
		helper.hessianChargeSwipe("K3MTB000002", ";5454545454545454=" + helper.formatExpirationDate(12) + "1015432112345601?", 200, 3);
	}

	/**
	 * @since R51
	 */
	@Test
	public void gxAuthCreditNoPartial() throws Exception {
		helper.gxAuthAndSale("G5060363", ";4788250000028291=" + helper.formatExpirationDate(12) + "1019206100000014?", EntryType.SWIPE, 278L, 0, 0, true, AuthResponseCodeA1.DENIED, null);
	}

	/**
	 * @since R51
	 */
	@Test
	public void gxAuthCreditPartial() throws Exception {
		helper.gxAuthAndSale("G5060281", ";4788250000028291=" + helper.formatExpirationDate(12) + "1019206100000014?", EntryType.SWIPE, 278L, 0, 0, true, AuthResponseCodeA1.CONDITIONALLY_APPROVED, 257L);
	}

	/**
	 * @since R51
	 */
	@Test
	public void esudsAuthCreditPartial() throws Exception {
		helper.gxAuthAndSale("101400000406", ";4788250000028291=" + helper.formatExpirationDate(12) + "1019206100000014?", EntryType.SWIPE, 278L, 0, 0, true, AuthResponseCodeA1.CONDITIONALLY_APPROVED, 257L);
	}
	/**
	 * @since R51
	 */
	@Test
	public void esudsAuthInternal() throws Exception {
		helper.gxAuthAndSale("101400000406", "999999010=0044", EntryType.SWIPE, 500L, 0, 0, true, AuthResponseCodeA1.APPROVED, 9996175L);
	}

	/**
	 * @since R51
	 */
	@Test
	public void gxAuthCreditBalance() throws Exception {
		helper.gxAuthAndSale("G5060363", ";4788250000028291=" + helper.formatExpirationDate(12) + "1019206100000014?", EntryType.SWIPE, 964L, 100, 0, true, AuthResponseCodeA1.APPROVED, null);
	}

	/**
	 * @since R51
	 */
	@Test
	public void esudsAuthCreditBalance() throws Exception {
		helper.gxAuthAndSale("101400000406", ";4788250000028291=" + helper.formatExpirationDate(12) + "1019206100000014?", EntryType.SWIPE, 964L, 100, 0, true, AuthResponseCodeA1.APPROVED, 1266L);
	}

	/**
	 * @since R51
	 */
	@Test
	public void gx32AuthCreditPartial() throws Exception {
		helper.gx32AuthAndSale("G5060281", ";4788250000028291=" + helper.formatExpirationDate(12) + "1019206100000014?", EntryType.SWIPE, 500L, 0, 0, AuthResponseCodeAF.APPROVED, null);
		helper.gx32AuthAndSale("G5060281", ";4788250000028291=" + helper.formatExpirationDate(12) + "1019206100000014?", EntryType.SWIPE, 278L, 0, 0, AuthResponseCodeAF.CONDITIONALLY_APPROVED, 257L);
		helper.gx32AuthAndSale("G5060281", ";4788250000028291=" + helper.formatExpirationDate(12) + "1019206100000014?", EntryType.SWIPE, 964L, 0, 0, AuthResponseCodeAF.APPROVED, null);
	}

	/**
	 * @since R51
	 */
	@Test
	public void hessianAuthBadCountry() throws Exception {
//		helper.hessianAuthSaleSwipe("K3MTB000002", ";5454545454545454=" + helper.formatExpirationDate(12) + "1015432112345601?", 0, 100, "Test Item", 3, 0);
	}

	/**
	 * @since R51
	 */
	@Test
	public void hessianAuthZeroAndSale() throws Exception {
//		helper.hessianAuthSaleSwipe("K3MTB000004", ";5454545454545454=" + helper.formatExpirationDate(12) + "1015432112345601?", 0, 100, "Test Item", 2, 1);
	}

	protected void checkDateConversion(Date date, String pattern, int leastSignificantCalendarField) throws Exception {
		String df = ConvertUtils.formatObject(date, new SimpleDateFormat(pattern));
		Date res = ConvertUtils.convert(Date.class, df);
		log.info("Formatted " + date + " using " + pattern + " into " + df + " then parsed and got " + res);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		switch(leastSignificantCalendarField) {
			case Calendar.YEAR:
				cal.set(Calendar.MONTH, 0);
			case Calendar.MONTH:
				cal.set(Calendar.DATE, 1);
			case Calendar.DATE:
				cal.set(Calendar.HOUR_OF_DAY, 0);
			case Calendar.HOUR_OF_DAY:
				cal.set(Calendar.MINUTE, 0);
			case Calendar.MINUTE:
				cal.set(Calendar.SECOND, 0);
			case Calendar.SECOND:
				cal.set(Calendar.MILLISECOND, 0);
			case Calendar.MILLISECOND:
				break;
			default:
				throw new IllegalArgumentException("Calendar Field " + leastSignificantCalendarField + " not recognized");
		}
		if(cal.getTime().compareTo(res) != 0)
			throw new Exception("Converted date '" + res + " doesn't match truncated original '" + cal.getTime() + "'");
	}
	
	/**
	 * @since R51
	 */
	@Test
	public void dateConversion() throws Exception {
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(date.getTime() % (1000L * 60 * 60 * 24));
		Date time = cal.getTime();
		checkDateConversion(date, ((SimpleDateFormat) DateFormat.getDateInstance()).toPattern(), Calendar.DATE);
		checkDateConversion(date, "MM-dd-yyyy HH:mm:ss.SSS", Calendar.MILLISECOND);
		checkDateConversion(date, "MMM dd yy", Calendar.DATE);
		checkDateConversion(date, "MM/dd/yy", Calendar.DATE);
		checkDateConversion(date, "EEE MMM dd yyyy", Calendar.DATE);
		checkDateConversion(date, "EEE MM-dd-yyyy", Calendar.DATE);
		checkDateConversion(date, "yyyy-MM-dd HH:mm", Calendar.MINUTE);
		checkDateConversion(date, "dd-MMM-yyyy GG hh:mm a Z", Calendar.MINUTE);
		checkDateConversion(date, "EEEE, MMMM dd, yyyy hh:mm a z", Calendar.MINUTE);
		checkDateConversion(date, "MMMd,yy HH-mm-ss.S", Calendar.MILLISECOND);
		checkDateConversion(date, "MMddyyyy", Calendar.DATE);
		checkDateConversion(date, "MMM   \t yyyy", Calendar.MONTH);
		checkDateConversion(date, "MM-yyyy", Calendar.MONTH);
		checkDateConversion(date, "MMMM , yyyy", Calendar.MONTH);
		checkDateConversion(date, "M - yyyy", Calendar.MONTH);
		checkDateConversion(time, "HH:mm:ss.SSS", Calendar.MILLISECOND);
		checkDateConversion(time, "hh-mm a", Calendar.MINUTE);
		checkDateConversion(date, "MM-dd-yyyy HH-mm-ss", Calendar.SECOND);
		checkDateConversion(date, "MMddyyyy HHmmss", Calendar.SECOND);
		checkDateConversion(date, "EEE MMM dd HH:mm:ss zzz yyyy", Calendar.SECOND);
		checkDateConversion(date, "MM.dd.yyyy HH.mm.ss", Calendar.SECOND);
		checkDateConversion(date, "MM.dd.yyyy", Calendar.DATE);
	}
	
	/**
	 * @throws Exception 
	 * @throws  
	 * @since R50.9
	 */
	@Test
	public void generatePrepaidCardsNoDups() throws Exception {
		Map<String, String> params = new HashMap<>();
		params.put("action", "Finish");
		params.put("consumer_acct_fmt_id", "2");
		params.put("consumer_acct_fmt_name", "Prepaid Card");
		params.put("customer_id", "6");
		params.put("corp_customer_id", "8");
		params.put("currency_cd", "USD");
		params.put("customer_name", "USA Technologies Inc");
		params.put("corp_customer_name", "ABC Company, Inc.");
		params.put("location_id", "870");
		params.put("location_name", "USA Technologies");
		params.put("authority_id", "306");
		params.put("merchant_id", "640");
		params.put("merchant_cd", "1009");
		params.put("merchant_name", "USA Technologies Inc - USAT Internal Prepaid Merchant");
		params.put("merchant_desc", "USA Technologies Inc - USAT Internal Prepaid Merchant");
		params.put("terminal_id", "410");
		params.put("terminal_desc", "USA Technologies Inc - USAT Internal Prepaid Terminal");
		params.put("num_cards", "1");
		params.put("exp", "2012");
		params.put("exp_display", "12/2020");
		params.put("deactivation_date", "2020-12-31 23:59:59");
		params.put("create_date", "02/2016");
		params.put("balance", "0");
		params.put("device_types", "");
		params.put("consumer_acct_sub_type_id", "1");

		// reset last_consumer_acct_cd
		ClientEmulatorHelper.initDataLayer();
		DataLayerMgr.executeCall("RESET_LAST_CONSUMER_ACCT_CD", params, true);
		
		//submit request
		WebRequestHelper helper = new WebRequestHelper();
		HttpClient client = helper.loginDMS();

		PostMethod method = new PostMethod(helper.getDMSUrl() + "newConsumerAcct5.i");
		for(Map.Entry<String, String> entry : params.entrySet())
			method.addParameter(entry.getKey(), entry.getValue());
		// method.setRequestHeader("Content-type", "text/xml; charset=UTF-8");
		int rc = client.executeMethod(method);
		if(rc != 200)
			throw new Exception("Response code is " + rc);
		String responseBody = method.getResponseBodyAsString();
			
		//parse result
		Pattern dupAvoidedPattern = Pattern.compile("[\\r\\n]Card \\d+ already exists![\\r\\n]");
		Pattern cardInfoPattern = Pattern.compile("[\\r\\n]Card: ((\\d+)=(\\d{4})\\d*), Security Code: (\\d+), Card ID: (\\d+)[\\r\\n]");
		
		if(!dupAvoidedPattern.matcher(responseBody).find())
			throw new Exception("Duplicate Card Number not avoided in :\n" + responseBody);
		Matcher matcher = cardInfoPattern.matcher(responseBody);
		if(!matcher.find())
			throw new Exception("Card Info not provided in :\n" + responseBody);
		String trackData = matcher.group(1);
		String cardId = matcher.group(5);
		String securityCode = matcher.group(4);
		String cardNumber = matcher.group(2);
		log.info("Created card id " + cardId + ": " + cardNumber);
				
		//check db
		params.put("globalAccountId", cardId);
		params.put("consumerAcctCdHashHex", StringUtils.toHex(SecureHash.getUnsaltedHash(cardNumber.getBytes())));
		DataLayerMgr.executeCall("CHECK_NEW_CARD_COUNTS", params, false);
		int gaCount = ConvertUtils.getInt(params.get("globalAccountIdCount"));
		int caCount = ConvertUtils.getInt(params.get("consumerAcctCdCount"));
		if(gaCount != 1)
			throw new Exception("New card id " + cardId + " has " + gaCount + " entries in PSS.CONSUMER_ACCT_BASE");
		if(caCount != 1)
			throw new Exception("New card number " + cardNumber + " has " + caCount + " entries in PSS.CONSUMER_ACCT");
	}

	/**
	 * @throws Exception
	 * @since R51
	 */
	@Test
	public void addAprivaPTA() throws Exception {
		ClientEmulatorHelper.initDataLayer();
		// reset db
		String deviceSerialCd = "K3MTB000001";
		Map<String, String> params = new HashMap<>();
		params.put("deviceSerialCd", deviceSerialCd);
		DataLayerMgr.executeCall("RESET_APRIVA_PTA", params, true);

		// add pta
		DataLayerMgr.selectInto("GET_APRIVA_PTA_SETTINGS", params);

		WebRequestHelper helper = new WebRequestHelper();
		HttpClient client = helper.loginDMS();
		PostMethod method = new PostMethod(helper.getDMSUrl() + "addPtaSuccess.i");
		method.addParameter("device_id", ConvertUtils.getString(params.get("device_id"), true));
		method.addParameter("payment_subtype_id", ConvertUtils.getString(params.get("payment_subtype_id"), true));
		method.addParameter("pos_pta_duplicate_settings", "Y");
		method.addParameter("myaction", "Add / Reactivate");

		int rc = client.executeMethod(method);
		assert rc == 200 : "Response code is " + rc;
		String responseBody = method.getResponseBodyAsString();
		assert !responseBody.contains("Application Error") : "Error in response: " + responseBody;
		
		// check result
		DataLayerMgr.selectInto("CHECK_APRIVA_PTA_SETTINGS", params);
		String terminalCd = params.get("terminalCd");
		String authorityName = params.get("authorityName");
		long posPtaId = ConvertUtils.getLong(params.get("posPtaId"));
		
		assert terminalCd != null : "Terminal Cd is null for posPtaId " + posPtaId;
		assert terminalCd.equals(deviceSerialCd) : "Terminal Cd does not match device serial cd for posPtaId " + posPtaId;
		assert authorityName != null : "AuthorityName is null for posPtaId " + posPtaId;
		assert authorityName.equals("Apriva") : "AuthorityName does not match 'Apriva' for posPtaId " + posPtaId;

		// now update to activate
		params.clear();
		params.put("pos_pta_id", String.valueOf(posPtaId));
		DataLayerMgr.selectInto("GET_PTA_SETTINGS", params);
		method = new PostMethod(helper.getDMSUrl() + "editPosPtaSettingsFunc.i");
		for(Map.Entry<String, String> entry : params.entrySet())
			if(entry.getValue() != null)
				method.addParameter(entry.getKey(), ConvertUtils.getString(entry.getValue(), false));
		// set activation date
		method.addParameter("toggle_date", "now");
		method.addParameter("myaction", "Save");

		HttpMethod m = WebRequestHelper.executeFollowRedirects(client, method);
		responseBody = m.getResponseBodyAsString();
		assert !responseBody.contains("Application Error") : "Error in response: " + responseBody;

		// check activation date and terminal cd again
		DataLayerMgr.selectInto("CHECK_APRIVA_PTA_SETTINGS", params);
		terminalCd = params.get("terminalCd");
		authorityName = params.get("authorityName");
		posPtaId = ConvertUtils.getLong(params.get("posPtaId"));
		String activationDate = ConvertUtils.getString(params.get("activationTs"), false);

		assert terminalCd != null : "Terminal Cd is null for posPtaId " + posPtaId;
		assert terminalCd.equals(deviceSerialCd) : "Terminal Cd does not match device serial cd for posPtaId " + posPtaId;
		assert authorityName != null : "AuthorityName is null for posPtaId " + posPtaId;
		assert authorityName.equals("Apriva") : "AuthorityName does not match 'Apriva' for posPtaId " + posPtaId;
		assert !StringUtils.isBlank(activationDate) : "posPtaId " + posPtaId + " was not activated";
		log.info("Finished creating and updating Apriva postPtaId " + posPtaId);
	}

	/**
	 * @throws Exception
	 * @since R51
	 */
	@Test
	public void importAprivaTmpl() throws Exception {
		ClientEmulatorHelper.initDataLayer();
		// reset db
		String deviceSerialCd = "K3MTB000002";
		Map<String, String> params = new HashMap<>();
		params.put("deviceSerialCd", deviceSerialCd);
		DataLayerMgr.executeCall("RESET_APRIVA_PTA", params, true);

		// add pta
		DataLayerMgr.selectInto("GET_APRIVA_PTA_SETTINGS", params);

		WebRequestHelper helper = new WebRequestHelper();
		HttpClient client = helper.loginDMS();
		PostMethod method = new PostMethod(helper.getDMSUrl() + "importPosptaTmplFunc.i");
		method.addParameter("device_id", ConvertUtils.getString(params.get("device_id"), true));
		method.addParameter("pos_pta_tmpl_id", "893");
		method.addParameter("mode_cd", "MO");
		method.addParameter("order_cd", "AE");
		method.addParameter("action", "Import");

		HttpMethod m = WebRequestHelper.executeFollowRedirects(client, method);
		String responseBody = m.getResponseBodyAsString();
		assert !responseBody.contains("Application Error") : "Error in response: " + responseBody;

		// check result
		params.put("payment_subtype_id", "643");
		DataLayerMgr.selectInto("CHECK_APRIVA_PTA_SETTINGS", params);
		String terminalCd = params.get("terminalCd");
		String authorityName = params.get("authorityName");
		long posPtaId = ConvertUtils.getLong(params.get("posPtaId"));
		String activationDate = ConvertUtils.getString(params.get("activationTs"), false);

		assert terminalCd != null : "Terminal Cd is null for posPtaId " + posPtaId;
		assert terminalCd.equals(deviceSerialCd) : "Terminal Cd does not match device serial cd for posPtaId " + posPtaId;
		assert authorityName != null : "AuthorityName is null for posPtaId " + posPtaId;
		assert authorityName.equals("Apriva") : "AuthorityName does not match 'Apriva' for posPtaId " + posPtaId;
		assert !StringUtils.isBlank(activationDate) : "posPtaId " + posPtaId + " was not activated";
		log.info("Finished creating and updating Apriva postPtaId " + posPtaId);
	}

	/**
	 * @throws Exception
	 * @since R51
	 */
	@Test
	public void cloneAprivaPTA() throws Exception {
		ClientEmulatorHelper.initDataLayer();
		// reset db
		String deviceSerialCd = "K3MTB000003";
		Map<String, String> params = new HashMap<>();
		params.put("deviceSerialCd", deviceSerialCd);
		DataLayerMgr.executeCall("RESET_APRIVA_PTA", params, true);

		// add pta
		DataLayerMgr.selectInto("GET_APRIVA_PTA_SETTINGS", params);
		WebRequestHelper helper = new WebRequestHelper();
		HttpClient client = helper.loginDMS();
		PostMethod method = new PostMethod(helper.getDMSUrl() + "deviceCloning.i");
		method.addParameter("sourceNumber", "K3MTB000002");
		method.addParameter("targetNumber", deviceSerialCd);
		method.addParameter("cloningType", "S");
		method.addParameter("userOP", "Clone");

		HttpMethod m = WebRequestHelper.executeFollowRedirects(client, method);
		String responseBody = m.getResponseBodyAsString();
		assert !responseBody.contains("Application Error") : "Error in response: " + responseBody;

		// check result
		params.put("payment_subtype_id", "643");
		DataLayerMgr.selectInto("CHECK_APRIVA_PTA_SETTINGS", params);
		String terminalCd = params.get("terminalCd");
		String authorityName = params.get("authorityName");
		long posPtaId = ConvertUtils.getLong(params.get("posPtaId"));
		String activationDate = ConvertUtils.getString(params.get("activationTs"), false);

		assert terminalCd != null : "Terminal Cd is null for posPtaId " + posPtaId;
		assert terminalCd.equals(deviceSerialCd) : "Terminal Cd does not match device serial cd for posPtaId " + posPtaId;
		assert authorityName != null : "AuthorityName is null for posPtaId " + posPtaId;
		assert authorityName.equals("Apriva") : "AuthorityName does not match 'Apriva' for posPtaId " + posPtaId;
		assert !StringUtils.isBlank(activationDate) : "posPtaId " + posPtaId + " was not activated";
		log.info("Finished creating and updating Apriva postPtaId " + posPtaId);
	}

	/**
	 * @throws Exception
	 * @since R51
	 */
	@Test
	public void vendScreenProcessUpdates() throws Exception {
//		helper.hessianProcessUpdates("K3VS15109898", "VSRevolution", "1.5.3-8");
	}

	/**
	 * @throws Exception
	 * @since R51
	 */
//	@Test
//	public void vendScreenCashSale() throws Exception {
//		String deviceSerialCd = "K3VS15109898";
//		long tranId = helper.nextMasterId();
//		String item = "{" + SecurityUtils.getRandomPassword(8) + "}";
//		Date beginDate = new Date();
//		helper.hessianCashSale(deviceSerialCd, tranId, 125, item, 0, ECResponse.RES_OK);
//		Date endDate = new Date(Math.max(System.currentTimeMillis(), beginDate.getTime() + 1000L));
//		// wait 1 minute
//		Thread.sleep(60 * 1000L);
//
//		// run detail report and find it
//		WebRequestHelper webHelper = new WebRequestHelper();
//		String reportHtml = webHelper.runActivityDetailReport(deviceSerialCd, beginDate, endDate, 22);
//
//		// find transaction
//		int index = reportHtml.indexOf(item);
//		assert index >= 0 : "Could not find item (" + item + ") in html: " + reportHtml;
//
//		log.info("Found transaction in html: ..." + reportHtml.substring(Math.max(0, index - 500), Math.min(reportHtml.length(), index + 500)) + "...");
//	}

	@Test
	public void vendScreenNewCoupon() throws Exception {
		// hit coupon server to create new coupon
		HttpRequester requester = new HttpRequester();
		String url = "https://api-qa.vsm2m.net/2.0/coupon/bulk/99?amount=1&type=PROMO";
		Reader result = requester.get(url, null, new HttpRequester.BasicAuthenticator("usatqcuser", "MmFyUpjOmXkz"));
		log.info("Create coupons response: " + IOUtils.readFully(result, 2048));
	}

	/**
	 * @throws Exception
	 * @since R51
	 */
//	@Test
//	public void vendScreenCouponSale_promo() throws Exception {
//		vendScreenCouponSale("PROMO");
//	}
//
//	/**
//	 * @throws Exception
//	 * @since R51
//	 */
//	@Test
//	public void vendScreenCouponSale_manual() throws Exception {
//		vendScreenCouponSale("MANUAL");
//	}
//
//	/**
//	 * @throws Exception
//	 * @since R51
//	 */
//	@Test
//	public void vendScreenCouponSale_instant() throws Exception {
//		vendScreenCouponSale("INSTANT");
//	}

//	protected void vendScreenCouponSale(String type) throws Exception {
//		String deviceSerialCd = "K3VS15107501";
//		// hit coupon server to create new coupon
//		HttpRequester requester = new HttpRequester();
//		String url = "https://api-qa.vsm2m.net/2.0/coupon/bulk/99?amount=1&type=" + type.toUpperCase();
//		String coupon;
//		try (BufferedReader result = new BufferedReader(requester.get(url, null, new HttpRequester.BasicAuthenticator("usatqcuser", "MmFyUpjOmXkz")))) {
//			String header = result.readLine();
//			assert "\"Coupon Code\"".equals(header) : "Unrecognized header response from coupon server: " + header;
//			String line = result.readLine();
//			assert line != null && line.startsWith("\"") && line.endsWith("\"") : "Unrecognized data from coupon server: " + line;
//			coupon = line.substring(1, line.length() - 1);
//		}
//		log.info("Using coupon: " + coupon);
//		String item = "{" + SecurityUtils.getRandomPassword(8) + "}";
//		Date beginDate = new Date();
//		helper.hessianAuthSaleCoupon(deviceSerialCd, coupon, 500, 175, item, ECResponse.RES_APPROVED, ECResponse.RES_OK);
//		Date endDate = new Date(Math.max(System.currentTimeMillis(), beginDate.getTime() + 1000L));
//		log.info("Successfully sent coupon sale");
//		// wait 1 minute
//		Thread.sleep(60 * 1000L);
//
//		// run detail report and find it
//		WebRequestHelper webHelper = new WebRequestHelper();
//		String reportHtml = webHelper.runActivityDetailReport(deviceSerialCd, beginDate, endDate, 63);
//		
//		IOUtils.copy(new ByteArrayInputStream(reportHtml.getBytes(StandardCharsets.UTF_8)),new FileOutputStream(outputPath+"/vendScreenCouponSale.html"));
//
//		// find transaction
//		int index = reportHtml.indexOf(item);
//		assert index >= 0 : "Could not find item (" + item + ") in html: " + reportHtml;
//
//		log.info("Found transaction in html: ..." + reportHtml.substring(Math.max(0, index - 500), Math.min(reportHtml.length(), index + 500)) + "...");
//	}

	/**
	 * @throws Exception
	 * @since R51
	 */
//	@Test
//	public void gdiFileG9() throws Exception {
//		String deviceSerialCd = "VJ000000004";
//		String firmwareVersion = "Server Test - " + SecurityUtils.getRandomPassword(8);
//		String machineModel = "[" + SecurityUtils.getRandomPassword(8) + "]";
//		String machineSerialCd = SecurityUtils.getRandomPassword(8);
//		String machineManufacturer = "AVI";
//		helper.edgeGdiFile(deviceSerialCd, firmwareVersion, machineManufacturer, machineModel, machineSerialCd);
//		// check
//		Map<String, Object> params = new HashMap<>();
//		params.put("deviceSerialCd", deviceSerialCd);
//		DataLayerMgr.selectInto("GET_HOST_INFO", params, true);
//		assert ConvertUtils.areEqual(firmwareVersion, params.get("firmwareVersion")) : "Firmware Version was not updated for " + deviceSerialCd;
//		assert ConvertUtils.areEqual(machineSerialCd, params.get("machineSerialCd")) : "Machine Serial Number was not updated for " + deviceSerialCd;
//		int machineSource = ConvertUtils.getInt(params.get("machineSource"), -1);
//		if(machineSource == 3) {
//			assert ConvertUtils.areEqual(machineManufacturer, params.get("machineManufacturer")) : "Machine Manufacturer was not updated for " + deviceSerialCd;
//			assert ConvertUtils.areEqual(machineModel, params.get("machineModel")) : "Machine Model was not updated for " + deviceSerialCd;
//		} else {
//			assert !ConvertUtils.areEqual(machineManufacturer, params.get("machineManufacturer")) : "Machine Manufacturer was updated for " + deviceSerialCd;
//			assert !ConvertUtils.areEqual(machineModel, params.get("machineModel")) : "Machine Model was updated for " + deviceSerialCd;
//		}
//	}

	/**
	 * @throws Exception
	 * @since R51
	 */
//	@Test
//	public void roomLayoutEsuds() throws Exception {
//		String deviceSerialCd = "101200000068";
//		int modelCode1 = 100000 + SecurityUtils.getSecureRandom().nextInt(900000);
//		int modelCode2 = 100000 + SecurityUtils.getSecureRandom().nextInt(900000);
//		int modelCode3 = 100000 + SecurityUtils.getSecureRandom().nextInt(900000);
//
//		helper.eSudsRoomLayout(deviceSerialCd, modelCode1, modelCode2, modelCode3);
//		Map<String, Object> params = new HashMap<>();
//		params.put("deviceSerialCd", deviceSerialCd);
//		DataLayerMgr.selectInto("GET_ESUDS_HOST_INFO", params, true);
//		int[] modelCodes = ConvertUtils.convert(int[].class, params.get("modelCodes"));
//		assert modelCodes != null && modelCodes.length == 3 : "Invalid number of machines (should be 3): " + (modelCodes == null ? 0 : modelCodes.length);
//		assert modelCodes[0] == modelCode1 : "Invalid model for machine #1 (should be " + modelCode1 + "): " + modelCodes[0];
//		assert modelCodes[1] == modelCode2 : "Invalid model for machine #2 (should be " + modelCode2 + "): " + modelCodes[1];
//		assert modelCodes[2] == modelCode3 : "Invalid model for machine #3 (should be " + modelCode3 + "): " + modelCodes[2];
//	}

	/**
	 * @throws Exception
	 * @since R51
	 */
	@Test
	public void bezelInfoGx() throws Exception {
		// make sure it will be requested
		// do call session
	}

	/**
	 * @throws Exception
	 * @since R51
	 */
	@Test
	public void dexFileMachineTypeG9() throws Exception {

	}

	/**
	 * @throws Exception
	 * @since R51
	 */
	@Test
	public void dexFileMachineTypeGx() throws Exception {

	}

	/**
	 * @throws Exception
	 * @since R51
	 */
	@Test
	public void usaliveMakeModelUpdate() throws Exception {

	}
	
	@Test
	public void dexFileSent() throws Exception {
		String deviceName="EE100000001";
		String dexFilePath="D:\\public\\test\\fileTransfer\\TD000021-DEX.10112016.txt";
		ClientEmulator ce = helper.getClientEmulator(deviceName);
		ce.startCommunication(7);
		try {
			
			MessageData_C7 dataC7 = new MessageData_C7();
			byte[] encoded = Files.readAllBytes(Paths.get(dexFilePath));
			dataC7.setContent(encoded);
			dataC7.setCreationTime(Calendar.getInstance());
			dataC7.setFileType(FileType.DEX_FILE);
			MessageData reply = ce.sendMessage(dataC7);

			assert (reply instanceof MessageData_CB) : "Invalid reply received: " + reply;
			MessageData_CB replyCB = (MessageData_CB) reply;
			assert replyCB.getResultCode() == GenericResponseResultCode.OKAY : "Invalid result code received: " + reply;
		} finally {
			ce.stopCommunication();
		}
	}
	
	@Test
	public void testCalendarParsers() throws Exception {
		SimpleDateFormat format=new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
		Date now=new Date();
		String dateStr=format.format(now);
		log.info(dateStr);
		DateConverter converter=new DateConverter();
		Date aDate=converter.convert(dateStr);
		assert now.equals(aDate);
		format=new java.text.SimpleDateFormat("MM.dd.yyyy hh:mm:ss");
		dateStr=format.format(now);
		log.info(dateStr);
		aDate=converter.convert(dateStr);
		assert now.equals(aDate);
	}
	
	protected final BoundedCache<String, AtomicLong, RuntimeException> messageIds = new LockLinkedSegmentCache<String, AtomicLong, RuntimeException>(1000, SegmentCache.DEFAULT_INITIAL_CAPACITY, SegmentCache.DEFAULT_LOAD_FACTOR, SegmentCache.DEFAULT_SEGMENTS) {
		@Override
		protected AtomicLong createValue(String key, Object... additionalInfo) throws RuntimeException {
			return new AtomicLong();
		}
	};
	
	protected AtomicLong getLastMessageId(String deviceNameOrSerial) {
		return messageIds.getOrCreate(deviceNameOrSerial);
	}

	@Test
	public void testMessageId() throws Exception {
		String deviceNameOrSerial=null;
		AtomicLong lastMessageId = getLastMessageId(deviceNameOrSerial);
		long currentMillis = System.currentTimeMillis();
		long currentSeconds = currentMillis / 1000;
		long messageId=1459826721;
		boolean isPassed=false;
		if(messageId <= currentSeconds + ProcessingUtils.MAX_ID_ADVANCE) {
			while(true) {
				long current = lastMessageId.get();
				if(messageId <= current)
					break;
				else if(lastMessageId.compareAndSet(current, messageId))
					isPassed=true;
			}
		}
		log.info("message id="+messageId+" isPassed="+isPassed+lastMessageId);
		messageId=1459826716;
		isPassed=false;
		if(messageId <= currentSeconds + ProcessingUtils.MAX_ID_ADVANCE) {
			while(true) {
				long current = lastMessageId.get();
				if(messageId <= current)
					break;
				else if(lastMessageId.compareAndSet(current, messageId))
					isPassed=true;
			}
		}
		log.info("message id="+messageId+" isPassed="+isPassed+lastMessageId);
		messageId=1459826880;
		isPassed=false;
		if(messageId <= currentSeconds + ProcessingUtils.MAX_ID_ADVANCE) {
			while(true) {
				long current = lastMessageId.get();
				if(messageId <= current)
					break;
				else if(lastMessageId.compareAndSet(current, messageId))
					isPassed=true;
			}
		}
		log.info("message id="+messageId+" isPassed="+isPassed+lastMessageId);
		
		
	}
	
	@Test
	public void testFilterMap() throws Exception {
		Map<String,Object> params=new HashMap<String,Object>();
		params.put("report.title","testTitle");
		params.put("report.name","testName");
		params.put(GenerateAttrEnum.ATTR_USE_STANDBY.getValue(), StandbyAllowance.WAIT_ON_REQUEST);
		params.put(null, "testNull");
		params.put("notMatchPrefix", "notMatchPrefix");
		
		Map<String, Object> paramsFilterOrg=new FilterMap(params, "report.", null);
		
		log.info(paramsFilterOrg);
		assert  paramsFilterOrg.size()==3;
		
		Map<String, Object> paramsNew = new LinkedHashMap<>();
		for(Map.Entry<String, Object> entry : params.entrySet())
			if(entry.getKey() != null && entry.getKey().startsWith("report.") && !GenerateAttrEnum.ATTR_MAX_STANDBY_WAIT.getValue().equals(entry.getKey()) && !GenerateAttrEnum.ATTR_USE_STANDBY.getValue().equals(entry.getKey()))
				paramsNew.put(entry.getKey().substring(7), entry.getValue());
		
		log.info(paramsNew);
		assert  paramsNew.size()==2;
		for(Map.Entry<String, Object> entry : paramsFilterOrg.entrySet()){
			if(!GenerateAttrEnum.ATTR_MAX_STANDBY_WAIT.getValue().equals(entry.getKey())){
				assert paramsFilterOrg.get(entry.getKey()).equals(paramsNew.get(entry.getKey()));
			}
		}
		
	}
	
	@Test
	public void testPropertyValue() throws Exception {
		URL appConfigUrl = ConfigurationUtils.locate(FileSystem.getDefaultFileSystem(), null, "AppLayerService.properties");
		
		String resoureBase = appConfigUrl.toString().substring(0, appConfigUrl.toString().lastIndexOf('/')+1);
		String dataLayerFilePath = resoureBase + "applayer-data-layer.xml";
		//ConfigLoader.loadConfig(ConfigSource.createConfigSource(dataLayerFilePath), dataLayer);
		
		Properties props = MainWithConfig.loadPropertiesWithConfig( resoureBase + "AppLayerService.properties", ReleaseUnitTests.class, null);
  	Base.configureDataSourceFactory(props, null);
  	ConfigLoader.loadConfig(ConfigSource.createConfigSource(dataLayerFilePath));
  	Collection<Integer> properList=new ArrayList<Integer>();
  	properList.add(1527);
  	properList.add(85);
		Map<String, Object> params = new HashMap<String, Object>();
		params.clear();
		params.put("deviceName", "TD000951");
		params.put("propertyList", properList);
		params.put("includeDefaults", "Y");
		
		Map<Integer, String> propertyValueList = new HashMap<Integer, String>();

		Results results = DataLayerMgr.executeQuery("GET_PROPERTY_LIST_VALUES", params);
		try {
			while(results.next()) {
				
				String propertyValue=results.getValue("propertyValue", String.class);
				String isEqualToDefault=results.getValue("isEqualToDefault", String.class);
				Integer propertyIndex=results.getValue("propertyIndex", Integer.class);
				log.info(propertyIndex);	
				log.info(propertyValue);	
				log.info(isEqualToDefault);	
				if(isEqualToDefault.equals("Y")){
						propertyValueList.put(propertyIndex, propertyValue);
				}else{
					if(propertyValue==null){
						propertyValueList.put(propertyIndex, "");
					}else{
						propertyValueList.put(propertyIndex, propertyValue);
					}
				}
			}
		} finally {
			results.close();
		}
		for(Map.Entry<Integer, String> entry:propertyValueList.entrySet()){
			StringBuilder sb=new StringBuilder();
			sb.append(entry.getKey());
			if(entry.getValue()==null){
				sb.append(MessageDataUtils.PROPERTY_DEFAULT);
			}else{
				sb.append(MessageDataUtils.PROPERTY_EQUALS).append(entry.getValue());
			}
			log.info(sb.toString());
		}
	}
	
	@Test
	public void testStringSplit() throws Exception {
		String[] vdiTranGlobalTransCd =StringUtils.split("V:deviceName:eventId", ':', 3);
		assert vdiTranGlobalTransCd[0].equals("V");
		assert vdiTranGlobalTransCd[1].equals("deviceName");
		assert vdiTranGlobalTransCd[2].equals("eventId");
	}
	
	public PEMParser openPEMResource(String          fileName){
      InputStream res = this.getClass().getClassLoader().getResourceAsStream(fileName);
      Reader fRd = new BufferedReader(new InputStreamReader(res));
      return new PEMParser(fRd);
  }
	
	
	@Test
	/**
	 * This is a test to show how ECDH is working
	 * @throws Exception
	 */
	public void testVASECDH() throws Exception {
		Security.addProvider(new BouncyCastleProvider());
		PEMParser pemRd = openPEMResource("private.pem");

    PEMKeyPair pemPair = (PEMKeyPair)pemRd.readObject();

    KeyPair pair = new JcaPEMKeyConverter().setProvider("BC").getKeyPair(pemPair);

    log.info("Alice: " + pair.getPrivate());
    
    log.info("Alice: " + pair.getPublic());
    
    PEMParser pemRd2 = openPEMResource("private2.pem");

    PEMKeyPair pemPair2 = (PEMKeyPair)pemRd2.readObject();

    KeyPair pair2 = new JcaPEMKeyConverter().setProvider("BC").getKeyPair(pemPair2);

    log.info("Bob: " + pair2.getPrivate());
    
    log.info("Bob: " + pair2.getPublic().getEncoded());
    
		PEMParser pemRd3 = openPEMResource("ecpubCompressed.pem");//public key generated via alice's private key

		SubjectPublicKeyInfo pubKey = (SubjectPublicKeyInfo)pemRd3.readObject();
		
		JcaPEMKeyConverter   converter = new JcaPEMKeyConverter().setProvider("BC");

    PublicKey pKey = converter.getPublicKey(pubKey);
    
    byte[] encoded=pKey.getEncoded();
    String b64 = Base64.encodeBytes(encoded, false);
    log.info("ecpubkey: " + b64+"|");
    String ecpubkeyBase64="MDkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDIgAD0TDG7WzxFXJc9FZ0rE8qG5n6Z5voU5axJYDV5nViYEM=";
    byte[] compressedKey=Base64.decode(ecpubkeyBase64);
    log.info("compressedKey: " + compressedKey.length);
    log.info("ecpubkey: " + b64.equals(ecpubkeyBase64));
    
    byte[] publicBytes = Base64.decode(ecpubkeyBase64);
    X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicBytes);
    KeyFactory keyFactory = KeyFactory.getInstance("ECDH", "BC");
    PublicKey pubKeyBack = keyFactory.generatePublic(keySpec);
    
    PEMParser pemRd4 = openPEMResource("ecpubkey2.pem");//public key generated via bob's private key

		SubjectPublicKeyInfo pubKey2 = (SubjectPublicKeyInfo)pemRd4.readObject();
		PublicKey pKey2 = converter.getPublicKey(pubKey2);

    log.info("--------------------------------------");
    log.info("random: " + pKey);
    log.info("Alice's secret: "+ StringUtils.toHex(VASUtils.getECDHSecret(pair.getPrivate(),pKey2)));
    log.info("Bob's secret:   "+ StringUtils.toHex(VASUtils.getECDHSecret(pair2.getPrivate(),pKey)));
	}
	
	public static byte[] getSchemeSharedInfo() throws Exception{
		String password ="pass.com.usatech.more.pass2";
		//byte[] salt = SecureHash.getSalt();
		byte[] merchantId = SecureHash.getUnsaltedHash(password.getBytes());
		byte[] asciiStr="ApplePay encrypted VAS data".getBytes();
		int sharedInfoLength=1+asciiStr.length+merchantId.length;
    byte[] sharedInfo=new byte[sharedInfoLength];
    System.arraycopy(asciiStr, 0, sharedInfo, 0, asciiStr.length);
    System.arraycopy(merchantId, 0, sharedInfo, asciiStr.length+1, merchantId.length);
    return sharedInfo;
	}
	
	public static byte[] getScheme2SharedInfo() throws Exception{
		byte afterZ=0x0D;
		byte[] asciiStr="id-aes256-GCMApplePay encrypted VAS data".getBytes();
		byte[] merchantId = SecureHash.getUnsaltedHash("pass.com.usatech.more.pass2".getBytes());
		int sharedInfoLength=1+asciiStr.length+merchantId.length;
    byte[] sharedInfo=new byte[sharedInfoLength];
    sharedInfo[0]=afterZ;
    System.arraycopy(asciiStr, 0, sharedInfo, 1, asciiStr.length);
    System.arraycopy(merchantId, 0, sharedInfo, 1+asciiStr.length, merchantId.length);
    return sharedInfo;
	}
	
	public static byte[] getScheme1SharedInfo() throws Exception{
		return "ApplePay encrypted VAS data".getBytes();
	}
	@Test
	/**
	 * This is the test we want to make it work. vasHex is from intnet11 system.out log
	 * 
	 * This is the pass created that is used to tap on the G9 reader on Ying's desk:
	 * {"url":"https://wallet-api.urbanairship.com/v1/pass/dynamic/92fd6628-e593-4054-ad65-693a10d6b039"}
	 * The pass is using encyrptionPublicKey MDkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDIgAD0TDG7WzxFXJc9FZ0rE8qG5n6Z5voU5axJYDV5nViYEM=
	 * @throws Exception
	 */
	public void testVASDecrypt2() throws Exception {
		Security.addProvider(new BouncyCastleProvider());
		
		String vasHex="557C7E204FA64D4C852D7A65F0665AB1BF96C9F8C67DCCD3C7BCB4488B3128E0715B3054EB870EBCB22C8991AE6FA603A32058A6163A9A3B7F9E288CAD86657235BEE0397F55A8BB97883506736DAC0071C4C381B593054F0B616B76F72407E2CA";
		//String vasHex="557C7E204FA62C0CE3BE27CDB101C85C25242836AB106E957BE43D68FD14DDDA3C950853A3199B6827806BC424CD4B68E46A9AE7520E609AF69CA4AB7BCB782E4197D581AF883C5000AB04867DDD2A586073CBD1E1A2D74650A1D65BE10E3FA785";
		byte[] vasData=ByteArrayUtils.fromHex(vasHex);
		String myPublicKey="MDkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDIgAD0TDG7WzxFXJc9FZ0rE8qG5n6Z5voU5axJYDV5nViYEM=";
		PEMParser pemRd3 = openPEMResource("ecpubCompressed.pem");//public key generated via alice's private key

		SubjectPublicKeyInfo pubKey = (SubjectPublicKeyInfo)pemRd3.readObject();
		
		JcaPEMKeyConverter   converter = new JcaPEMKeyConverter().setProvider("BC");

    PublicKey pKey = converter.getPublicKey(pubKey);
    byte[] myPublicKeyEncoded=pKey.getEncoded();
    log.info("myPublicKeyEncoded="+myPublicKeyEncoded);
    byte[] base64Decoded=Base64.decode(myPublicKey);
    byte[] xCor=Arrays.copyOfRange(base64Decoded,base64Decoded.length-32,base64Decoded.length);
		byte[] hash = SecureHash.getUnsaltedHash(xCor);//SHA-256
		
		
		byte[] vasEncrypted=Arrays.copyOfRange(vasData, 2, vasData.length);
		log.info("first 2 bytes: " + new String(Arrays.copyOfRange(vasData, 0, 2)));
		log.info("first 2 bytes: " + new String(ByteArrayUtils.fromHex("557C")));
		String publicKeyBytes=StringUtils.toHex(Arrays.copyOfRange(vasEncrypted, 0, 4));
		log.info("vasEncrypted      first 4 bytes: " + publicKeyBytes);
		log.info("my pubKey sha-256 first 4 bytes: " + StringUtils.toHex((Arrays.copyOfRange(hash, 0, 4))));
		log.info("vasEncrypted: " + StringUtils.toHex(vasEncrypted));
		
		log.info("vasEncrypted point: " + StringUtils.toHex(Arrays.copyOfRange(vasEncrypted, 4, 36)).toLowerCase());
		String publicKeyBase64=Base64.encodeBytes(ByteArrayUtils.readByteString(vasEncrypted, 4, 32).getBytes(), true);
		log.info("publicKeyBase64: " + publicKeyBase64);
		//X509EncodedKeySpec keySpec = new X509EncodedKeySpec(ByteArrayUtils.readByteString(vasEncrypted, 4, 32).getBytes());
    //KeyFactory keyFactory = KeyFactory.getInstance("ECDH", "BC");
    //PublicKey pubKeyBack = keyFactory.generatePublic(keySpec);
    PublicKey pubKeyBack = VASUtils.getECDHPublicKeyFromCoordinate(Arrays.copyOfRange(vasEncrypted, 4, 36));
    log.info("pubKeyBack1: " + pubKeyBack);
    
		
    PEMParser pemRd = openPEMResource("private.pem");

    PEMKeyPair pemPair = (PEMKeyPair)pemRd.readObject();

    KeyPair pair = new JcaPEMKeyConverter().setProvider("BC").getKeyPair(pemPair);

    byte[] secret=VASUtils.getECDHSecret(pair.getPrivate(), pubKeyBack);
    
    PrivateKey privateFromKeystore=(PrivateKey)CryptUtils.getDefaultKeySupply().getKey("keymgr.ecdh.pass."+publicKeyBytes.toLowerCase());
    
    PublicKey publicFromKeystore=CryptUtils.getDefaultKeySupply().getPublicKey("keymgr.ecdh.pass."+publicKeyBytes.toLowerCase());
    
    log.info("pubKeyBack2: " + publicFromKeystore);
    
    byte[] secret2=VASUtils.getECDHSecret(privateFromKeystore, pubKeyBack);
    log.info("secret2= "+StringUtils.toHex(secret2));
    
    PemObject pemPrivate = new PemObject("Private Key",pair.getPrivate().getEncoded());
    PemWriter pemWriter = new PemWriter(new OutputStreamWriter(new FileOutputStream("D:\\public\\test\\ecPrivateKeyOut.pem")));
		try {
			pemWriter.writeObject(pemPrivate);
			pemWriter.flush();
		} finally {
			pemWriter.close();
		}
    
    log.info("secret1= "+StringUtils.toHex(secret));
    
    log.info("privateKey base64="+Base64.encodeBytes(pair.getPrivate().getEncoded(), true));
    log.info("public Key base64="+Base64.encodeBytes(pubKeyBack.getEncoded(), true));
    
    byte[] sharedInfo=getScheme2SharedInfo();
    log.info("sharedInfo="+new String(sharedInfo));
    log.info("sharedInfo Hex="+StringUtils.toHex(sharedInfo));
    
    int keySize=32;//256 bit
    //X963USAT kek=new X963USAT(secret, sharedInfo, new SHA256());
    //byte[] derivedKey = kek.deriveKey(256);
    USATECDHKEKGenerator kek=new USATECDHKEKGenerator(secret, sharedInfo, new SHA256Digest());
    byte[] derivedKey=new byte[32];
    kek.generateBytes(derivedKey, 0, 32);
    log.info("derivedKey ="+StringUtils.toHex(derivedKey).toLowerCase());
    
    //derivedKey=ByteArrayUtils.fromHex("31B33DBA12E7833457D10D6CA4F19EA9E1C5E9B2F235930312500D6EBCB2A0DE");
    
    byte[] encrypted1=Arrays.copyOfRange(vasEncrypted, 36, 40);
    byte[] encrypted2=Arrays.copyOfRange(vasEncrypted, 40, vasEncrypted.length-16);
    byte[] tag=Arrays.copyOfRange(vasEncrypted, vasEncrypted.length-16, vasEncrypted.length);
    log.info("encrypted1 ="+StringUtils.toHex(encrypted1).toLowerCase());
    log.info("encrypted2 ="+StringUtils.toHex(encrypted2).toLowerCase());
    log.info("encrypted ="+StringUtils.toHex(Arrays.copyOfRange(vasEncrypted, 36, vasEncrypted.length )).toLowerCase());
    log.info("tag ="+StringUtils.toHex(tag).toLowerCase());
    SecretKeySpec k = new SecretKeySpec(derivedKey, "AES");

    Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding","SunJCE");
    byte[] iv = new byte[16];
    for(int i=0;i<16;i++){
    	iv[i]=(byte)0;
    }
    log.info("iv ="+StringUtils.toHex(iv).toLowerCase());
    byte[] encryptedText=Arrays.copyOfRange(vasEncrypted, 36, vasEncrypted.length);
    GCMParameterSpec spec = new GCMParameterSpec(128, iv);
    cipher.init(Cipher.DECRYPT_MODE, k, spec);
    //cipher.updateAAD("pass.com.usatech.more.pass2".getBytes());
    // UNEXPECTED: Uncommenting this will cause an AEADBadTagException when decrypting 
//cipher.updateAAD(cipherBuffer); 
    final byte[] decryptedMessage = cipher.doFinal(encryptedText);
    /**
    USATGCMBlockCipher cipher2 = new USATGCMBlockCipher(new AESEngine()); 
  	cipher2.init(false, new AEADParameters(new KeyParameter(derivedKey), 128, iv));
  	byte[] decryptedMessage = new byte[1024];
  	int outputLen=cipher2.processBytes(vasEncrypted, 40, vasEncrypted.length-40, decryptedMessage, 0);
  	cipher2.doFinal(Arrays.copyOfRange(decryptedMessage, 0, outputLen), 0);
  	*/
    long seconds=ByteArrayUtils.readUnsignedInt(Arrays.copyOfRange(decryptedMessage, 0,4), 0);
    Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
    cal.setTimeInMillis(seconds*1000+VASUtils.appleToEpochTimeDiff);
    log.info("decryptedDate="+cal.getTime());
    log.info("decryptedMessage="+new String(decryptedMessage));
    
	}
	
	@Test
	public void testVASDecrypt1() throws Exception {
		Security.addProvider(new BouncyCastleProvider());
		String vasHex="557C7E204FA64D4C852D7A65F0665AB1BF96C9F8C67DCCD3C7BCB4488B3128E0715B3054EB870EBCB22C8991AE6FA603A32058A6163A9A3B7F9E288CAD86657235BEE0397F55A8BB97883506736DAC0071C4C381B593054F0B616B76F72407E2CA";
		//String vasHex="557C7E204FA62C0CE3BE27CDB101C85C25242836AB106E957BE43D68FD14DDDA3C950853A3199B6827806BC424CD4B68E46A9AE7520E609AF69CA4AB7BCB782E4197D581AF883C5000AB04867DDD2A586073CBD1E1A2D74650A1D65BE10E3FA785";
		byte[] vasData=ByteArrayUtils.fromHex(vasHex);
		String myPublicKey="MDkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDIgAD0TDG7WzxFXJc9FZ0rE8qG5n6Z5voU5axJYDV5nViYEM=";
		PEMParser pemRd3 = openPEMResource("ecpubCompressed.pem");//public key generated via alice's private key

		SubjectPublicKeyInfo pubKey = (SubjectPublicKeyInfo)pemRd3.readObject();
		
		JcaPEMKeyConverter   converter = new JcaPEMKeyConverter().setProvider("BC");

    PublicKey pKey = converter.getPublicKey(pubKey);
    byte[] myPublicKeyEncoded=pKey.getEncoded();
    log.info("myPublicKeyEncoded="+myPublicKeyEncoded);
    byte[] base64Decoded=Base64.decode(myPublicKey);
    byte[] xCor=Arrays.copyOfRange(base64Decoded,base64Decoded.length-32,base64Decoded.length);
		byte[] hash = SecureHash.getUnsaltedHash(xCor);//SHA-256
		
		
		byte[] vasEncrypted=Arrays.copyOfRange(vasData, 2, vasData.length);
		log.info("first 2 bytes: " + new String(Arrays.copyOfRange(vasData, 0, 2)));
		log.info("first 2 bytes: " + new String(ByteArrayUtils.fromHex("557C")));
		log.info("vasEncrypted      first 4 bytes: " + StringUtils.toHex(Arrays.copyOfRange(vasEncrypted, 0, 4)));
		log.info("my pubKey sha-256 first 4 bytes: " + StringUtils.toHex((Arrays.copyOfRange(hash, 0, 4))));
		log.info("vasEncrypted: " + StringUtils.toHex(vasEncrypted));
		
		log.info("vasEncrypted point: " + StringUtils.toHex(Arrays.copyOfRange(vasEncrypted, 4, 36)).toLowerCase());
		String publicKeyBase64=Base64.encodeBytes(ByteArrayUtils.readByteString(vasEncrypted, 4, 32).getBytes(), true);
		log.info("publicKeyBase64: " + publicKeyBase64);
		//X509EncodedKeySpec keySpec = new X509EncodedKeySpec(ByteArrayUtils.readByteString(vasEncrypted, 4, 32).getBytes());
    //KeyFactory keyFactory = KeyFactory.getInstance("ECDH", "BC");
    //PublicKey pubKeyBack = keyFactory.generatePublic(keySpec);
    PublicKey pubKeyBack = VASUtils.getECDHPublicKeyFromCoordinate(Arrays.copyOfRange(vasEncrypted, 4, 36));
    log.info("pubKeyBack: " + pubKeyBack);
    PEMParser pemRd = openPEMResource("private.pem");

    PEMKeyPair pemPair = (PEMKeyPair)pemRd.readObject();

    KeyPair pair = new JcaPEMKeyConverter().setProvider("BC").getKeyPair(pemPair);

    byte[] secret=VASUtils.getECDHSecret(pair.getPrivate(), pubKeyBack);
    
    log.info("secret= "+StringUtils.toHex(secret));
    
    log.info("privateKey base64="+Base64.encodeBytes(pair.getPrivate().getEncoded(), true));
    log.info("public Key base64="+Base64.encodeBytes(pubKeyBack.getEncoded(), true));
    
    byte[] sharedInfo=getScheme1SharedInfo();
    log.info("sharedInfo="+new String(sharedInfo));
    
    int keySize=32;//256 bit
    //X963USAT kek=new X963USAT(secret, sharedInfo, new SHA256());
    //byte[] derivedKey = kek.deriveKey(256);
    USATECDHKEKGenerator kek=new USATECDHKEKGenerator(secret, sharedInfo, new SHA256Digest());
    byte[] derivedKey=new byte[32];
    kek.generateBytes(derivedKey, 0, 32);
    log.info("derivedKey ="+StringUtils.toHex(derivedKey).toLowerCase());
    
    byte[] encrypted1=Arrays.copyOfRange(vasEncrypted, 36, 40);
    byte[] encrypted2=Arrays.copyOfRange(vasEncrypted, 40, vasEncrypted.length-16);
    byte[] tag=Arrays.copyOfRange(vasEncrypted, vasEncrypted.length-16, vasEncrypted.length);
    log.info("encrypted1 ="+StringUtils.toHex(encrypted1).toLowerCase());
    log.info("encrypted2 ="+StringUtils.toHex(encrypted2).toLowerCase());
    log.info("encrypted ="+StringUtils.toHex(Arrays.copyOfRange(vasEncrypted, 36, vasEncrypted.length )).toLowerCase());
    log.info("tag ="+StringUtils.toHex(tag).toLowerCase());
    SecretKeySpec k = new SecretKeySpec(derivedKey, "AES");

    Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding","SunJCE");
    byte[] iv = new byte[16];
    for(int i=0;i<16;i++){
    	iv[i]=(byte)0;
    }
    log.info("iv ="+StringUtils.toHex(iv).toLowerCase());
    byte[] encryptedText=Arrays.copyOfRange(vasEncrypted, 36, vasEncrypted.length);
    GCMParameterSpec spec = new GCMParameterSpec(128, iv);
    cipher.init(Cipher.DECRYPT_MODE, k, spec);
    cipher.updateAAD("pass.com.usatech.more.pass2".getBytes());
    final byte[] decryptedMessage = cipher.doFinal(encryptedText);
    log.info("decryptedMessage="+new String(decryptedMessage));
    
	}
	
	@Test 
	/**
	 * This is to test out X963 with sha-256 AES/GCM encrypt and decrypt process
	 * @throws Exception
	 */
	public void testVASKDF() throws Exception{
		byte[] secret=ByteArrayUtils.fromHex("0EE2D9FB322D4DE0B14DFF91035B1E3170A331DDB9ABBC0C06858181F5103686");
		byte[] sharedInfo=getScheme2SharedInfo();
    int keySize=32;
    String message="125855|8BD29F182DE29848E5A02FAE749DA545";
    X963USAT kek=new X963USAT(secret, sharedInfo, new SHA256());
    byte[] derivedKey = kek.deriveKey(256);
    log.info("derivedKey ="+StringUtils.toHex(derivedKey));
    
    assert new String(derivedKey).equals(new String(derivedKey));
    
    int GCM_TAG_LENGTH = 16; 
    SecretKeySpec k = new SecretKeySpec(derivedKey, "AES");

    // Encrypt
    Cipher cipherEncrypt = Cipher.getInstance("AES/GCM/NoPadding");
    byte[] iv = new byte[16];
    for(int i=0;i<16;i++){
    	iv[i]=(byte)0;
    }
    byte[] time=toArray((int)System.currentTimeMillis());
    log.info("time             ="+time);
    GCMParameterSpec spec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, iv);
    cipherEncrypt.init(Cipher.ENCRYPT_MODE, k, spec);
    byte[] encryptedMessage=cipherEncrypt.doFinal(message.getBytes());
    log.info("encrypted           ="+StringUtils.toHex(encryptedMessage).toLowerCase());
    Cipher cipherDecrypt = Cipher.getInstance("AES/GCM/NoPadding");
    cipherDecrypt.init(Cipher.DECRYPT_MODE, k, spec);
    byte[] decryptedMessage=cipherDecrypt.doFinal(encryptedMessage);
    
    log.info("message             ="+message);
    log.info("decryptedMessage    ="+new String(decryptedMessage));
    assert new String(decryptedMessage).equals(message);
	}
	@Test 
	/**
	 * Use r and d R:\Projects\VAS\X963KDFTestVector test vector from http://csrc.nist.gov/groups/STM/cavp/component-testing.html
	 * This is to test out X963 KDF function
	 * @throws Exception
	 */
	public void testVASX963() throws Exception{
		byte[] secret=ByteArrayUtils.fromHex("0EE2D9FB322D4DE0B14DFF91035B1E3170A331DDB9ABBC0C06858181F5103686");
		byte[] sharedInfo=getScheme2SharedInfo();
    int keySize=32;
    X963USAT kek=new X963USAT(secret, sharedInfo, new SHA256());
    byte[] derivedKey = kek.deriveKey(keySize*8);
    
    USATECDHKEKGenerator kek2=new USATECDHKEKGenerator(secret, sharedInfo, new SHA256Digest());
    byte[] derivedKey2=new byte[32];
    kek2.generateBytes(derivedKey2, 0, 32);
    log.info("derivedKey1 ="+StringUtils.toHex(derivedKey).toLowerCase());
    log.info("derivedKey2 ="+StringUtils.toHex(derivedKey2).toLowerCase());
	}
	
	@Test
	public void testBigInt() throws Exception{
		byte[] encoded=ByteArrayUtils.fromHex("4d4c852d7a65f0665ab1bf96c9f8c67dccd3c7bcb4488b3128e0715b3054eb87");
		 BigInteger X = BigIntegers.fromUnsignedByteArray(encoded, 0, 32);
		 BigInteger X2 = new BigInteger(encoded);
		 System.out.println(X);
		 System.out.println(X2);
		 System.out.println(new BigInteger("1296860461"));
	}
	
	@Test
	public void testUTCTime() throws Exception{
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		cal.set(Calendar.YEAR, 1970);
		cal.set(Calendar.MONTH, 0);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		long epochTime=cal.getTimeInMillis();
		cal.clear();
		cal.set(Calendar.YEAR, 2001);
		cal.set(Calendar.MONTH, 0);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		long appleTime=cal.getTimeInMillis();
		log.info("epochTime="+epochTime);
		log.info("AppleTime to epoch diff="+appleTime);
	}
	public static byte[] toArray(int value){
    ByteBuffer buffer = ByteBuffer.allocate(4);
    buffer.order(ByteOrder.BIG_ENDIAN);
    buffer.putInt(value);
    buffer.flip();
    return buffer.array();
}
	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
}
