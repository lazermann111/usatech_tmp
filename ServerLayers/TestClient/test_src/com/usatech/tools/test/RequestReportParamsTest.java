package com.usatech.tools.test;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.SortedMap;

import com.usatech.report.ReportRequest;

import simple.app.Base;
import simple.app.MainWithConfig;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;

/**
 * This test will query report_request and output report params etc to an outputFilePath/RequestReportParamsTest.txt
 * The following 2 properties are configurable
 * 1. outputFilePath=D:\\public\\test\\reportGenerator\\
 * 2. reportRequestCreatedTsDiff=172800000	(this properties determines the createdTs for query report.report_request table)
 * @author yhe
 *
 */
public class RequestReportParamsTest extends MainWithConfig {
	private static final Log log = Log.getLog();

	
	protected Properties props;	

	@Override
	protected void registerDefaultCommandLineArguments() {
		registerCommandLineSwitch('p', "propertiesFile", true, true, "properties-file", "The properties file to use");
	}

	@Override
	public void run(String[] args) {
		Map<String, Object> argMap;
		try {
			argMap = parseArguments(args);
		} catch(IOException e) {
			finishAndExit(e.getMessage(), 100, true, null);
			return;
		}
		try {
			props = getProperties(ConvertUtils.getStringSafely(argMap.get("properties-file")), getClass(), null);
			Base.configureDataSourceFactory(props, null);
			Base.configureDataLayer(props);
		} catch(IOException e) {
			finishAndExit("Could not read properties file", 120, e);
			return;
		} catch(ServiceException e) {
			finishAndExit("Could not configure publisher", 120, e);
			return;
		}
		for(Map.Entry<String, Object> entry : argMap.entrySet()) {
			props.put(entry.getKey(), entry.getValue());
		}
		
		try {
			FileWriter fstream = new FileWriter(props.getProperty("outputFilePath")+ this.getClass().getSimpleName()+".txt");
			BufferedWriter out = new BufferedWriter(fstream);
			long now=System.currentTimeMillis();
			long createdTimeDefault=now-2*24*60*60*1000;//two days ago
			long createdTime=now - ConvertUtils.getLong(props.getProperty("reportRequestCreatedTsDiff"), createdTimeDefault);
			Date createdTs = new Date(createdTime);
			final Results results = DataLayerMgr.executeQuery("GET_REPORT_REQUEST", new Object[] { createdTs }, false);
			int reportRequestCount=0;
			
			while(results.next()) {
				reportRequestCount++;
				String userName = ConvertUtils.getStringSafely(results.getValue("userName"));
				int reportRequestId = ConvertUtils.getInt(results.getValue("reportRequestId"));
				String reportName = ConvertUtils.getStringSafely(results.getValue("reportName"));
				int reportRequestStatusId = ConvertUtils.getInt(results.getValue("reportRequestStatusId"));
				byte[] reportParams = ConvertUtils.convert(byte[].class, results.getValue("reportParams"));
				SortedMap<String,String> reportParamsMap=ReportRequest.decodeParameters(reportParams, false);
				out.write("|userName="+userName+"|reportRequestId="+reportRequestId+"|reportName="+reportName+"|reportRequestStatusId="+reportRequestStatusId+"|reportParamsMap="+reportParamsMap+"|\n");
				out.flush();
			}
			out.write("reportRequestCount="+reportRequestCount);
			out.close();
		} catch(SQLException e) {
			log.warn("Failure in RequestReportTest run method", e);
		} catch(ConvertException e) {
			log.warn("Failure in RequestReportTest run method", e);
		} catch(DataLayerException e) {
			log.warn("Failure in RequestReportTest run method", e);
		} catch(IOException e) {
			log.warn("Failure in RequestReportTest run method", e);
		}
		Log.finish();
		System.exit(0);
	}




	public static void main(String[] args) {
		new RequestReportParamsTest().run(args);
	}

}
