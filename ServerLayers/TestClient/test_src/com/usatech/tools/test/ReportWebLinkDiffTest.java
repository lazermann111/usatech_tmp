package com.usatech.tools.test;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import simple.app.Base;
import simple.app.MainWithConfig;
import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.BeanException;
import simple.results.Results;

/**
 * This test compares two database's web_content.web_link table and generate update/insert statement.
 * @author yhe
 *
 */
public class ReportWebLinkDiffTest extends MainWithConfig {
	private static final Log log = Log.getLog();

	
	protected Properties props;	

	@Override
	protected void registerDefaultCommandLineArguments() {
		registerCommandLineSwitch('p', "propertiesFile", true, true, "properties-file", "The properties file to use");
	}
	
	protected class WebLinkKey{
		String webLinkLabel;
		String webLinkDesc;
		char webLinkUsage;
		int[] requirementIds;
		public WebLinkKey(String webLinkLabel, String webLinkDesc,
				char webLinkUsage, int[] requirementIds) {
			super();
			this.webLinkLabel = webLinkLabel;
			this.webLinkDesc = webLinkDesc;
			this.webLinkUsage = webLinkUsage;
			this.requirementIds = requirementIds;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + Arrays.hashCode(requirementIds);
			result = prime * result
					+ ((webLinkDesc == null) ? 0 : webLinkDesc.hashCode());
			result = prime * result
					+ ((webLinkLabel == null) ? 0 : webLinkLabel.hashCode());
			result = prime * result + webLinkUsage;
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			WebLinkKey other = (WebLinkKey) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (!Arrays.equals(requirementIds, other.requirementIds))
				return false;
			if (webLinkDesc == null) {
				if (other.webLinkDesc != null)
					return false;
			} else if (!webLinkDesc.equals(other.webLinkDesc))
				return false;
			if (webLinkLabel == null) {
				if (other.webLinkLabel != null)
					return false;
			} else if (!webLinkLabel.equals(other.webLinkLabel))
				return false;
			if (webLinkUsage != other.webLinkUsage)
				return false;
			return true;
		}
		private ReportWebLinkDiffTest getOuterType() {
			return ReportWebLinkDiffTest.this;
		}
		
		
	}
	protected class WebLink{
		int webLinkId;
		String webLinkLabel;
		String webLinkUrl;
		String webLinkDesc;
		String webLinkGroup;
		char webLinkUsage;
		int webLinkOrder;
		int[] requirementIds;
		
		public WebLink() {
			super();
		}

		public int getWebLinkId() {
			return webLinkId;
		}


		public void setWebLinkId(int webLinkId) {
			this.webLinkId = webLinkId;
		}


		public String getWebLinkLabel() {
			return webLinkLabel;
		}


		public void setWebLinkLabel(String webLinkLabel) {
			this.webLinkLabel = webLinkLabel;
		}


		public String getWebLinkUrl() {
			return webLinkUrl;
		}


		public void setWebLinkUrl(String webLinkUrl) {
			this.webLinkUrl = webLinkUrl;
		}


		public String getWebLinkDesc() {
			return webLinkDesc;
		}


		public void setWebLinkDesc(String webLinkDesc) {
			this.webLinkDesc = webLinkDesc;
		}


		public String getWebLinkGroup() {
			return webLinkGroup;
		}


		public void setWebLinkGroup(String webLinkGroup) {
			this.webLinkGroup = webLinkGroup;
		}


		public char getWebLinkUsage() {
			return webLinkUsage;
		}


		public void setWebLinkUsage(char webLinkUsage) {
			this.webLinkUsage = webLinkUsage;
		}


		public int getWebLinkOrder() {
			return webLinkOrder;
		}


		public void setWebLinkOrder(int webLinkOrder) {
			this.webLinkOrder = webLinkOrder;
		}
         
		

		public int[] getRequirementIds() {
			return requirementIds;
		}

		public void setRequirementIds(int[] requirementIds) {
			this.requirementIds = requirementIds;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + Arrays.hashCode(requirementIds);
			result = prime * result
					+ ((webLinkDesc == null) ? 0 : webLinkDesc.hashCode());
			result = prime * result
					+ ((webLinkGroup == null) ? 0 : webLinkGroup.hashCode());
			result = prime * result
					+ ((webLinkLabel == null) ? 0 : webLinkLabel.hashCode());
			result = prime * result + webLinkOrder;
			result = prime * result
					+ ((webLinkUrl == null) ? 0 : webLinkUrl.hashCode());
			result = prime * result + webLinkUsage;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			WebLink other = (WebLink) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (!Arrays.equals(requirementIds, other.requirementIds))
				return false;
			if (webLinkDesc == null) {
				if (other.webLinkDesc != null)
					return false;
			} else if (!webLinkDesc.equals(other.webLinkDesc))
				return false;
			if (webLinkGroup == null) {
				if (other.webLinkGroup != null)
					return false;
			} else if (!webLinkGroup.equals(other.webLinkGroup))
				return false;
			if (webLinkLabel == null) {
				if (other.webLinkLabel != null)
					return false;
			} else if (!webLinkLabel.equals(other.webLinkLabel))
				return false;
			if (webLinkOrder != other.webLinkOrder)
				return false;
			if (webLinkUrl == null) {
				if (other.webLinkUrl != null)
					return false;
			} else if (!webLinkUrl.equals(other.webLinkUrl))
				return false;
			if (webLinkUsage != other.webLinkUsage)
				return false;
			return true;
		}

		public String toString(){
			return "webLinkId="+webLinkId+"|webLinkLabel="+webLinkLabel+"|webLinkUrl="+webLinkUrl+"|requirementIds="+requirementIds;
		}


		private ReportWebLinkDiffTest getOuterType() {
			return ReportWebLinkDiffTest.this;
		}
		
	}

	@Override
	public void run(String[] args) {
		Map<String, Object> argMap;
		try {
			argMap = parseArguments(args);
		} catch(IOException e) {
			finishAndExit(e.getMessage(), 100, true, null);
			return;
		}
		try {
			props = getProperties(ConvertUtils.getStringSafely(argMap.get("properties-file")), getClass(), null);
			Base.configureDataSourceFactory(props, null);
			Base.configureDataLayer(props);
		} catch(IOException e) {
			finishAndExit("Could not read properties file", 120, e);
			return;
		} catch(ServiceException e) {
			finishAndExit("Could not configure publisher", 120, e);
			return;
		}
		for(Map.Entry<String, Object> entry : argMap.entrySet()) {
			props.put(entry.getKey(), entry.getValue());
		}
		
		try {
			FileWriter fstream = new FileWriter(props.getProperty("outputFilePath")+ this.getClass().getSimpleName()+".txt");
			BufferedWriter out = new BufferedWriter(fstream);
			final Results results = DataLayerMgr.executeQuery("GET_WEB_LINK_OLD",false);
			HashMap<WebLinkKey,WebLink> oldMap=new HashMap<WebLinkKey,WebLink>();
			StringBuilder updateBuilder=new StringBuilder();
			StringBuilder insertBuilder=new StringBuilder();
			while(results.next()) {
				WebLink oldWebLink=new WebLink();
				results.fillBean(oldWebLink);
				if(oldWebLink.getWebLinkUsage()=='-'){
					continue;
				}
				oldMap.put(new WebLinkKey(oldWebLink.getWebLinkLabel(), oldWebLink.getWebLinkDesc(), oldWebLink.getWebLinkUsage(), oldWebLink.getRequirementIds()), oldWebLink);
			}
			final Results results2 = DataLayerMgr.executeQuery("GET_WEB_LINK_NEW",false);
			int totalUpdate=0;
			while(results2.next()) {
				WebLink newWebLink=new WebLink();
				results2.fillBean(newWebLink);
				if(newWebLink.getWebLinkUsage()=='-'){
					continue;
				}
				WebLinkKey newWebLinkKey=new WebLinkKey(newWebLink.getWebLinkLabel(), newWebLink.getWebLinkDesc(), newWebLink.getWebLinkUsage(), newWebLink.getRequirementIds());
				if(oldMap.containsKey(newWebLinkKey)){
					//compare
					if(!newWebLink.equals(oldMap.get(newWebLinkKey))){
						totalUpdate++;
						out.write("old "+oldMap.get(newWebLinkKey)+"\n");
						out.write("new "+newWebLink+"\n");
						out.write("-------------------------------------------\n");
						updateBuilder.append("update web_content.web_link set web_link_url='"+newWebLink.getWebLinkUrl()+"' where web_link_id="+newWebLink.getWebLinkId()+";\n");
					}
				}else{
					totalUpdate++;
					out.write("## In new but missing in old:"+newWebLink+"\n");
					out.write("-------------------------------------------\n");
					insertBuilder.append("insert into web_content.web_link values(").append(newWebLink.getWebLinkId()).append(",'").append(newWebLink.getWebLinkLabel()).append("','").append(newWebLink.getWebLinkUrl()).append("','").append(newWebLink.getWebLinkDesc()).append("','").append(newWebLink.getWebLinkGroup()).append("','").append(newWebLink.getWebLinkUsage()).append("',").append(newWebLink.getWebLinkOrder()).append(");\n");
				}
				
				out.flush();
			}
			out.write(insertBuilder.toString());
			out.write(updateBuilder.toString());
			out.write("totalUpdate="+totalUpdate);
			out.close();
		} catch(BeanException e) {
			log.warn("Failure in ReportWebLinkDiffTest run method", e);
		} catch(SQLException e) {
			log.warn("Failure in ReportWebLinkDiffTest run method", e);
		} catch(DataLayerException e) {
			log.warn("Failure in ReportWebLinkDiffTest run method", e);
		} catch(IOException e) {
			log.warn("Failure in ReportWebLinkDiffTest run method", e);
		}
		Log.finish();
		System.exit(0);
	}




	public ReportWebLinkDiffTest() {
		super();
		// TODO Auto-generated constructor stub
	}




	public static void main(String[] args) {
		new ReportWebLinkDiffTest().run(args);
	}

}
