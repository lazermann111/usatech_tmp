package com.usatech.tools.test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;

import simple.app.Base;
import simple.app.MainWithConfig;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.ByteArrayUtils;
import simple.io.Log;
import simple.results.Results;

/**
 * This test will grab inbound file transfer file for a device

the following 3 properties are configurable
outputFilePath=D:\\public\\test\\fileTransfer\\
getFileCount=10
deviceName=TD000589
fileTransferTypeCd=0 (0 is dex)

Dex Alert Test note:
InboundFileTransferTask will receive dex file will mark importFile and process return code will be 30 and publish to usat.inbound.fileimport
InboundFileImportTask will process usat.inbound.fileimport

C7 short file transfer, use CA, then C7 to send file, use GetFileTransferFileTest to retreive some existing dex file content.
C8 use CA then C8
readDatabaseInfo first to check g4op.dex_file, if exists, not importing.
G4OP.DEX_CODE_ALERT has dex code alert, when parsing dex, only when it matches dex code, it will alert
when there is dex alert insert into g4op.dex_file_line

Test Prerequisite:
	Run Applayer
	Run NetLayer
	Run GeneratorService
	Run TransportService
	Register DeviceAlerts report in usalive
	Make sure the user can view the terminal which will have alerts. insert into report.user_terminal values(931,20018, 'Y') 

Test check:
1. check it shows in Diagnostic report select * from report.terminal_alert order by terminal_alert_id desc
2. alert sent via transport
3. check g4op.dex_file has new entry and g4op.dex_file_line has the added alert details.

some handy sql:
delete from g4op.dex_file_line where dex_file_id = (select dex_file_id from g4op.dex_file where file_name like 'DEX-EE190000124-090158-050911-FILL.log%')
delete from g4op.dex_file where file_name like 'DEX-EE190000124-090158-050911-FILL.log%'

select * from g4op.dex_file where file_name like 'DEX-EE190000124-090158-050911-FILL.log%'
select * from g4op.dex_file_line where dex_file_id = (select dex_file_id from g4op.dex_file where file_name like 'DEX-EE190000124-090158-050911-FILL.log%')

Device Alert Test note:
	use CA->C6 to send test alert
 * @author yhe
 *
 */
public class GetFileTransferFileTest extends MainWithConfig {
	private static final Log log = Log.getLog();

	
	protected Properties props;	

	@Override
	protected void registerDefaultCommandLineArguments() {
		registerCommandLineSwitch('p', "propertiesFile", true, true, "properties-file", "The properties file to use");
	}

	@Override
	public void run(String[] args) {
		Map<String, Object> argMap;
		try {
			argMap = parseArguments(args);
		} catch(IOException e) {
			finishAndExit(e.getMessage(), 100, true, null);
			return;
		}
		try {
			props = getProperties(ConvertUtils.getStringSafely(argMap.get("properties-file")), getClass(), null);
			Base.configureDataSourceFactory(props, null);
			Base.configureDataLayer(props);
		} catch(IOException e) {
			finishAndExit("Could not read properties file", 120, e);
			return;
		} catch(ServiceException e) {
			finishAndExit("Could not configure publisher", 120, e);
			return;
		}
		for(Map.Entry<String, Object> entry : argMap.entrySet()) {
			props.put(entry.getKey(), entry.getValue());
		}
		
		try {
			String fileFolderPath=props.getProperty("outputFilePath");
			String deviceName=props.getProperty("deviceName");
			int rowNum=ConvertUtils.getInt(props.getProperty("getFileCount"), 10);
			int fileTransferTypeCd=ConvertUtils.getInt(props.getProperty("fileTransferTypeCd"), 0);
			if(!new File(fileFolderPath).exists()){
				new File(fileFolderPath).mkdir();
			}
			
			final Results results = DataLayerMgr.executeQuery("GET_FILE_TRANSER_BY_DEVICE_NAME", new Object[] { deviceName, fileTransferTypeCd, rowNum }, false);
			
			while(results.next()) {
				String fileTransferName = ConvertUtils.getStringSafely(results.getValue("fileTransferName"));
				String fileTransferId = ConvertUtils.getStringSafely(results.getValue("fileTransferId"));
				byte[] fileTransferContent=ByteArrayUtils.fromHex(ConvertUtils.getStringSafely(results.getValue("fileTransferContent")));
				FileOutputStream fstream = new FileOutputStream(fileFolderPath+fileTransferName+"."+fileTransferId);
				fstream.write(fileTransferContent);
				fstream.flush();
				fstream.close();
			}
			
		} catch(SQLException e) {
			log.warn("Failure in GetFileTransferFileTest run method", e);
		} catch(ConvertException e) {
			log.warn("Failure in GetFileTransferFileTest run method", e);
		} catch(DataLayerException e) {
			log.warn("Failure in GetFileTransferFileTest run method", e);
		} catch(IOException e) {
			log.warn("Failure in GetFileTransferFileTest run method", e);
		}
		Log.finish();
		System.exit(0);
	}




	public static void main(String[] args) {
		new GetFileTransferFileTest().run(args);
	}

}
