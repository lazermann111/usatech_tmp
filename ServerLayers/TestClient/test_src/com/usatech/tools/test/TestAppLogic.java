package com.usatech.tools.test;

import static com.usatech.tools.deploy.USATRegistry.subHostPrefixMap;

import com.usatech.tools.deploy.App;
import com.usatech.tools.deploy.LoopbackInterfaceMap;
import com.usatech.tools.deploy.USATRegistry;

public class TestAppLogic {
	
	public static void checkIp(App subApp, String serverEnv)throws Exception{
		String subHostPrefix = subHostPrefixMap.get(subApp.getName());
		if(subHostPrefix == null) {
			subHostPrefix = subApp.getName();
		}
		String subhostname = subHostPrefix + ("USA".equalsIgnoreCase(serverEnv) ? ".usatech.com" : "-" + serverEnv.toLowerCase() + ".usatech.com");
		String subhostip = LoopbackInterfaceMap.getHostRealIp(serverEnv, subhostname);
		System.out.println("subhostname=:"+subhostname);
		System.out.println("subhostip=:"+subhostip);
	}

	public static void main(String[] args) throws Exception{
		
		checkIp(USATRegistry.ESUDS_APP,"ECC");
		checkIp(USATRegistry.ESUDS_APP,"USA");
		
		checkIp(USATRegistry.GETMORE_APP,"ECC");
		checkIp(USATRegistry.GETMORE_APP,"USA");
		
		checkIp(USATRegistry.PREPAID_APP,"ECC");
		checkIp(USATRegistry.PREPAID_APP,"USA");
		
		checkIp(USATRegistry.USALIVE_APP,"ECC");
		checkIp(USATRegistry.USALIVE_APP,"USA");
		
		checkIp(USATRegistry.HOTCHOICE_APP,"ECC");
		checkIp(USATRegistry.HOTCHOICE_APP,"USA");
		
		checkIp(USATRegistry.VERIZON_APP,"ECC");
		checkIp(USATRegistry.VERIZON_APP,"USA");
		
		checkIp(USATRegistry.DMS_APP,"ECC");
		checkIp(USATRegistry.DMS_APP,"USA");
	}

}
