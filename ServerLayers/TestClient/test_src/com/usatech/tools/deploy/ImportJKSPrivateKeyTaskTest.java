/**
 * 
 */
package com.usatech.tools.deploy;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.Future;

import org.junit.Before;
import org.junit.Test;

import simple.event.ProgressListener;
import simple.io.Interaction;
import simple.util.concurrent.TrivialFuture;

import com.sshtools.j2ssh.SftpClient;
import com.sshtools.j2ssh.SshClient;
import com.sshtools.j2ssh.transport.HostKeyVerification;
import com.sshtools.j2ssh.transport.TransportProtocolException;
import com.sshtools.j2ssh.transport.publickey.SshPublicKey;

/**
 * Test new deployment code to import RSA keys into a JKS
 * (Java Key Store)
 * @author phorsfield
 *
 */
public class ImportJKSPrivateKeyTaskTest {

	protected static final String UNIT_TEST_KEY_ROOT = "D:/development/CCEFill/";
	
	private MapPasswordCache cache = new MapPasswordCache();
	private Interaction interaction = new Interaction() {
		
		@Override
		public char[] readPassword(String fmt, Object... args) {
			// TODO Auto-generated method stub
			return null;
		}
		
		@Override
		public String readLine(String fmt, Object... args) {
			// TODO Auto-generated method stub
			return null;
		}
		
		@Override
		public void printf(String format, Object... args) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public PrintWriter getWriter() {
			// TODO Auto-generated method stub
			return null;
		}
		
		@Override
		public Future<char[]> getPasswordFuture(String fmt, Object... args) {
			// TODO Auto-generated method stub
			return null;
		}
		
		@Override
		public Future<String> getLineFuture(String fmt, Object... args) {
			// TODO Auto-generated method stub
			return null;
		}
		
		@Override
		public ProgressListener createProgressMeter() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Future<File> browseForFile(String caption, String baseDir,
				String fmt, Object... args) {
			if(baseDir.endsWith("cert"))
			{
				return new TrivialFuture<File>(new File(UNIT_TEST_KEY_ROOT + "cce_fill_rsa_cert"));
			}
			else if(baseDir.endsWith("private"))
			{
				return new TrivialFuture<File>(new File(UNIT_TEST_KEY_ROOT + "cce_fill_rsa_clearprivate"));
			}
			return null;
		}
	};
	private Host host = new HostImpl("local", "rpt", "rpt21.usatech.com");
	private String user = "";

	private SshClient ssh = null;
	private SftpClient sftp = null;
	
	private DeployTaskInfo deployTaskInfo;
	
	private HostKeyVerification hostKeyVerification = new HostKeyVerification() {
		
		@Override
		public boolean verifyHost(String host, SshPublicKey pk)
				throws TransportProtocolException {
			
			return true; /*, what? */
		}
	}; 

	@Before
	public void setup() throws IOException
	{
		cache.setPassword(host, "keystore", "usatech".toCharArray() );
		cache.setPassword(host, "ccefill_us", "test".toCharArray() );
		cache.setPassword(host, "ccefill_bc", "test".toCharArray() );
		deployTaskInfo = new DeployTaskInfo(interaction, ssh, sftp, host,  user, cache, hostKeyVerification);
	}

	@Test
	public void testCceFillDummyBc() throws IOException, InterruptedException {		
		DeployTask  task = new ImportJKSPrivateKeyTask("ccefill_bc",
				new File(UNIT_TEST_KEY_ROOT + "cce_fill_rsa_clearprivate"),
				new File(UNIT_TEST_KEY_ROOT + "cce_fill_rsa_cert"), 
				"/opt/USAT/conf/keystore.ks", 
				cache , 
				0640, 
				true);
		
		task.perform(deployTaskInfo);
	}

	@Test
	public void testCceFillDummyUS() throws IOException, InterruptedException {		
		DeployTask  task = new ImportJKSPrivateKeyTask("ccefill_us",
				new File(UNIT_TEST_KEY_ROOT + "cce_fill_rsa_clearprivate"),
				new File(UNIT_TEST_KEY_ROOT + "cce_fill_rsa_cert"), 
				"/opt/USAT/conf/keystore.ks", 
				cache , 
				0640, 
				true);
		
		task.perform(deployTaskInfo);
	}

	@Test
	public void testBrowseFile() throws IOException, InterruptedException {		
		DeployTask  task = new ImportJKSPrivateKeyTask("ccefill_bc",
				new File("cce_fill_rsa_clearprivate"), // no prefix here
				new File("cce_fill_rsa_cert"), 
				"/opt/USAT/conf/keystore.ks", 
				cache , 
				0640, 
				true);
		
		task.perform(deployTaskInfo);
	}

}
