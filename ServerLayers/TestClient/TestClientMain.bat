@echo off
REM This script starts the TestClientMain
REM JAVA_HOME JAVA_OPTS APP_OPTS
if exist "%~dp0\setenv.bat" call "%~dp0\setenv.bat"
echo USING JAVA_HOME: %JAVA_HOME%
echo USING JAVA_OPTS: %JAVA_OPTS%
echo USING APP_OPTS: %APP_OPTS%
if not "%JAVA_HOME%" == "" set JAVA=%JAVA_HOME%\bin\java
if not "%JAVA%" == "" goto gotJava
set JAVA=java
:gotJava
REM cd "%~dp0/.."

set USAT_LIB=..\ThirdPartyJavaLibraries\lib
echo USING USAT_LIB: %USAT_LIB%

echo [%DATE% %TIME%] TestClientMain ...
"%JAVA%" %JAVA_OPTS% -Dfile.encoding=ISO8859-1 -cp "bin;..\Simple1.5\bin;..\LayersCommon\bin;..\UnifiedLayer\src;%USAT_LIB%\usat-unified-layer.jar;%USAT_LIB%\common-2007-08-09.jar;%USAT_LIB%\commons-beanutils-1.6.jar;%USAT_LIB%\commons-collections-3.1.jar;%USAT_LIB%\commons-configuration-1.5.jar;%USAT_LIB%\commons-dbcp-1.2.1.jar;%USAT_LIB%\commons-lang-2.4.jar;%USAT_LIB%\commons-logging-1.0.4.jar;%USAT_LIB%\commons-pool-1.2.jar;%USAT_LIB%\crypto-2008-10-09.jar;%USAT_LIB%\log4j-1.2.11.jar;%USAT_LIB%\mail-1.4.jar;%USAT_LIB%\ojdbc14_g-10.0.2.0.3.jar" com.usatech.test.ClientMain %APP_OPTS% %*
echo [%DATE% %TIME%] TestClientMain Finished
echo on
			
