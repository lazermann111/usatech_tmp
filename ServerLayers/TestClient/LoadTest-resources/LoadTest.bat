@echo off
if "%1" == "" goto usage
if exist "%~dp0\setenv.bat" call "%~dp0\setenv.bat"
echo USING JAVA_HOME: %JAVA_HOME%
echo USING JAVA_OPTS: %JAVA_OPTS%
if not "%JAVA_HOME%" == "" set JAVA=%JAVA_HOME%\bin\java
if "%JAVA%" == "" set JAVA=java
if "%JAVA_OPTS%" == "" set JAVA_OPTS=-Xmx512M
"%JAVA%" %JAVA_OPTS% -cp .;lib;lib\* -Dlog4j.configuration=log4j.properties -Dfile.encoding=ISO8859-1 -Dapp.servicename=LoadTest com.usatech.loadtest.LoadTest -p %1 start
exit /B 0
:usage
echo Usage: %0 LoadTest-[env].properties
exit /B 1
