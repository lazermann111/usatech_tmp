package helper;

import com.google.common.io.Resources;
import org.apache.log4j.Logger;
import org.junit.Assert;
import pages.UsaLive.Reports.ReportsPage;

import java.io.File;

/**
 * Created by ahachikyan on 5/12/2016.
 */
public class FileUtils
{
    private static final Logger logger = Logger.getLogger(ReportsPage.class);
    public static final String tempFolderForDownloads = System.getProperty("java.io.tmpdir") + "\\autotests";

    public static void waitForNotNullFileSize()
    {
        String filePath = CompareUtils.getLastModifiedFile(Core.tempFolderForDownloads);
        logger.info("Waiting for file size to stop increasing:" + filePath);
        Integer currentRoundsCount = 1;
        Integer maxRoundsCount = 60;
        long fileSizeBefore = CompareUtils.getFileSize(filePath);
        long fileSizeAfter = CompareUtils.getFileSize(filePath);
        while ((CompareUtils.getFileSize(filePath) == 0 )&& (currentRoundsCount < maxRoundsCount))
        {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            currentRoundsCount++;
        }
        Assert.assertFalse("File size is 0 for 1 minute:" + fileSizeAfter, CompareUtils.getFileSize(filePath) == 0);
        logger.info("Last modified file is:" + filePath);
    }

    public static void waitForFileSizeIncreasingToStop()
    {
        String filePath = CompareUtils.getLastModifiedFile(Core.tempFolderForDownloads);
        logger.info("Waiting for file size to stop increasing:" + filePath);
        Integer currentRoundsCount = 1;
        Integer maxRoundsCount = 60;
        long fileSizeBefore = CompareUtils.getFileSize(filePath);
        long fileSizeAfter = CompareUtils.getFileSize(filePath);
        while ((fileSizeBefore==fileSizeAfter) && (currentRoundsCount < maxRoundsCount))
        {
            fileSizeBefore = CompareUtils.getFileSize(filePath);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            fileSizeAfter = CompareUtils.getFileSize(filePath);
            currentRoundsCount++;
        }
        Assert.assertFalse("File size has not been increased in 1 minute:" + fileSizeAfter, fileSizeBefore==fileSizeAfter);
        logger.info("Last modified file is:" + filePath);
    }

    public static void waitForFileToBeDownloaded(Integer filesCountBefore) {
        Integer roundsCount = 0;
        while((ReportsPage.getFilesCountInDownloadingDir() != (filesCountBefore + 1)) && roundsCount < 1800 )
        {
            try {
                //TODO:Remove debug info
                logger.info ("Try #" + roundsCount + ". Files count is " + new File(Core.tempFolderForDownloads).list().length);
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            roundsCount++;
        }
        //TODO: Change to waiting for file size increasing
        if (ReportsPage.getFilesCountInDownloadingDir() <= filesCountBefore)
        {
            Assert.fail("Report has not been downloaded");
        }
        else {
            logger.info("Report has been successfully downloaded. Downloaded files count was:" + filesCountBefore + ", become:" + (new File(Core.tempFolderForDownloads).list().length));
            waitForNotNullFileSize();
//            waitForFileSizeIncreasingToStop();
            //Wait to avoid zero-size file manipulation
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static String getResoucePath(String resourceFolder)
    {
        return Resources.getResource(resourceFolder).getPath().replaceFirst("/", "");
    }
    
    public static Integer getFilesCountInDownloadingDir()
    {
        return new File(tempFolderForDownloads).list().length;
    }
}
