package helper;

import org.junit.Ignore;
//
import org.junit.Test;

import com.usatech.test.ClientEmulatorDevTest;

public class SimulateSellUtils  {

//	@Ignore
//	@Test
    public void TestOracleSell() throws Exception 
	{      
        String deviceSerialNumber= "K3VS20000001";
        String deviceName = "WS003584";
        String env = "ecc";
//        String deviceName = "TD001143";
//        String CC_DATA =  "6396212019713949006=2212004057591";//temp
////    	String CC_DATA = "4276380086871717=12102011511101700000"; //Svetlana`s expired visa credit card
//    	String CC_DATA =  "4173270594781389=25121019999888877776";//Chase Tandem credit card
        String CC_DATA =  "4055011111111111=20121015432112345678";//Test Eport
//    	String CC_DATA =  "4788250000028291=15121015432112345601";//Chase Tandem credit card
//		String CC_DATA =  "6396212019374821536=2112005917734";//Ecc prepaid
//		String CC_DATA =  "6396213035384949553=2112001399715";//Ecc payroll
          
//        String deviceSerialNumber= "VJ001100101";
//        String env = "ecc";
       
                
//    	String CC_DATA = "4276380086871717=12102011511101700000"; //Svetlana`s expired visa credit card
//    	String CC_DATA =  "6396212020038465728=2212003752189";//
    	
    	OracleConnector oracleConnector = new OracleConnector(env);       	
    	String encKeyFormDb= oracleConnector.getEncryptionKey(deviceName);	
    	
    	ClientEmulatorDevTest clientEmulatorDevTest = new ClientEmulatorDevTest();
    	clientEmulatorDevTest.testSell(env, CC_DATA, deviceSerialNumber.trim(), deviceName, encKeyFormDb);    
    }
	
	@Ignore
	@Test
    public void TestPostgreSQlSell() throws Exception 
	{   
        String deviceSerialNumber= "VJ011000114";
        String env = "dev";
        String deviceName = "TD010547";
                
//    	String CC_DATA = "4276380086871717=12102011511101700000"; //Svetlana`s expired visa credit card
//    	String CC_DATA =  "4173270594781389=25121019999888877776";// 1-st Chase Tandem credit card
//        String CC_DATA =  "4761739001010267=22122011317125989";// 2-nd Chase Tandem credit card
//        String CC_DATA =  "6396213027014236477=2112004675591";//Payroll Gmail 
        String CC_DATA =  "6396212005578182379=2112001678220";//Prepaid Gmail
    	
//        String deviceSerialNumber= "VJ000100003";
//        String env = "dev";
//        String deviceName = "TD010607";
//        String CC_DATA =  "6396212006370470574=2212001749373";//SEA
//        String CC_DATA =  "6396212006740941155=2212000262845";//Elenas
//                
////    	String CC_DATA = "4276380086871717=12102011511101700000"; //Svetlana`s expired visa credit card
//    	String CC_DATA =  "4173270594781389=25121019999888877776";//Chase Tandem credit card
		
    	PostgresqlConnector postgresqlConnector = new PostgresqlConnector(env);  
		String encKeyFormDb= postgresqlConnector.getEncryptionKey(deviceName);	
		
		ClientEmulatorDevTest clientEmulatorDevTest = new ClientEmulatorDevTest();
		clientEmulatorDevTest.testSell(env, CC_DATA, deviceSerialNumber, deviceName, encKeyFormDb);      
    }
}
    

