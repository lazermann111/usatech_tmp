package helper;

/**
 * Created by ahachikyan on 3/9/2016.
 */

import org.apache.log4j.Logger;
import org.junit.Assert;

import java.io.IOException;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMultipart;

public class EmailUtils{
    private static final Logger logger = Logger.getLogger(EmailUtils.class);
    static String host = "pop.gmail.com";// change accordingly
    static String user = "usalive.test@gmail.com";// change accordingly
    static String password = "Dab00581";// change accordingly

//    static String host = "mail.office365.com";// change accordingly
//    static String user = "ahachikyan@usatech.com";// change accordingly
//    static String password = "!@(QWEasdzxc";// change accordingly
    
    public static Message[] getMessages(String user, String password) throws MessagingException, IOException {
    	if(user.contains("@usatech.com"))
    	{
    		host = "mail.office365.com";// change accordingly
    	}
//      Message[] messages = new Message[0];


          //create properties field
          Properties properties = new Properties();

          properties.put("mail.pop3.host", host);
          properties.put("mail.pop3.port", "995");
          properties.put("mail.pop3.starttls.enable", "true");
          Session emailSession = Session.getDefaultInstance(properties);

          //create the POP3 store object and connect with the pop server
          Store store = emailSession.getStore("pop3s");

          store.connect(host, user, password);

          //create the folder object and open it
          Folder emailFolder = store.getFolder("INBOX");
          emailFolder.open(Folder.READ_ONLY);

          // retrieve the messages from the folder in an array and print it
          Message[] messages = emailFolder.getMessages();
          emailFolder.close(false);
          store.close();
          return messages;
  }
    
    public static String getMessageSubject(Integer id, String user, String password) throws MessagingException, IOException {
    	if(user.contains("@usatech.com"))
    	{
    		host = "mail.office365.com";// change accordingly
    	}
//      Message[] messages = new Message[0];


          //create properties field
          Properties properties = new Properties();

          properties.put("mail.pop3.host", host);
          properties.put("mail.pop3.port", "995");
          properties.put("mail.pop3.starttls.enable", "true");
          Session emailSession = Session.getDefaultInstance(properties);

          //create the POP3 store object and connect with the pop server
          Store store = emailSession.getStore("pop3s");

          store.connect(host, user, password);

          //create the folder object and open it
          Folder emailFolder = store.getFolder("INBOX");
          emailFolder.open(Folder.READ_ONLY);

          // retrieve the messages from the folder in an array and print it
          Message message = emailFolder.getMessages()[id];
          String messageContent = message.getSubject();
          emailFolder.close(false);
          store.close();
          return messageContent;
  }
    
    public static String getMessageContent(Integer id, String user, String password) throws MessagingException, IOException {
    	if(user.contains("@usatech.com"))
    	{
    		host = "mail.office365.com";// change accordingly
    	}


          //create properties field
          Properties properties = new Properties();

          properties.put("mail.pop3.host", host);
          properties.put("mail.pop3.port", "995");
          properties.put("mail.pop3.starttls.enable", "true");
          Session emailSession = Session.getDefaultInstance(properties);

          //create the POP3 store object and connect with the pop server
          Store store = emailSession.getStore("pop3s");

          store.connect(host, user, password);

          //create the folder object and open it
          Folder emailFolder = store.getFolder("INBOX");
          emailFolder.open(Folder.READ_ONLY);

          // retrieve the messages from the folder in an array and print it
          Message message = emailFolder.getMessages()[id];
          String messageContent = getTextFromMimeMultipart((MimeMultipart)message.getContent());
          emailFolder.close(false);
          store.close();
          return messageContent;
  }
    
    private static String getTextFromMimeMultipart(
            MimeMultipart mimeMultipart)  throws MessagingException, IOException{
        String result = "";
        int count = mimeMultipart.getCount();
        for (int i = 0; i < count; i++) {
            BodyPart bodyPart = mimeMultipart.getBodyPart(i);
            if (bodyPart.isMimeType("text/plain")) {
                result = result + "\n" + bodyPart.getContent();
                break; // without break same text appears twice in my tests
            } else if (bodyPart.isMimeType("text/html")) {
                String html = (String) bodyPart.getContent();
                result = result + "\n" + org.jsoup.Jsoup.parse(html).text();
            } else if (bodyPart.getContent() instanceof MimeMultipart){
                result = result + getTextFromMimeMultipart((MimeMultipart)bodyPart.getContent());
            }
        }
        return result;
    }
    public static Message[] getMessages() throws MessagingException, IOException {
//        Message[] messages = new Message[0];


            //create properties field
            Properties properties = new Properties();

            properties.put("mail.pop3.host", host);
            properties.put("mail.pop3.port", "995");
            properties.put("mail.pop3.starttls.enable", "true");
            Session emailSession = Session.getDefaultInstance(properties);

            //create the POP3 store object and connect with the pop server
            Store store = emailSession.getStore("pop3s");

            store.connect(host, user, password);

            //create the folder object and open it
            Folder emailFolder = store.getFolder("INBOX");
            emailFolder.open(Folder.READ_ONLY);

            // retrieve the messages from the folder in an array and print it
            Message[] messages = emailFolder.getMessages();
//            Message m0 = messages[0];
//            String s0 = m0.getContent().toString();
//            Message m1 = messages[426];
//            String s1 = m1.getContent().toString();
//            System.out.println("messages.length---" + messages.length);
//
//            for (int i = 0, n = messages.length; i < n; i++) {
//                Message message = messages[i];
//                System.out.println("---------------------------------");
//                System.out.println("Email Number " + (i + 1));
//                System.out.println("Subject: " + message.getSubject());
//                System.out.println("From: " + message.getFrom()[0]);
//                System.out.println("Text: " + message.getContent().toString());
//
//            }

            //close the store and folder objects
            emailFolder.close(false);
            store.close();
            return messages;
//
//        }
//        catch (NoSuchProviderException e)
//        {
//            e.printStackTrace();
//        }
//        catch (MessagingException e)
//        {
//            e.printStackTrace();
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//        finally {
//            return messages;
//        }
    }


    public static Integer getEmailsCount() {
        try {
            return getMessages().length;
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public static Integer getEmailsCount(String email, String password) throws MessagingException, IOException {
            return getMessages(email, password).length;
    }

    public static void waitForNewEmail(Integer emailCountBefore, Integer maxWaitTimeInSeconds, String user, String password) throws MessagingException, IOException
    {
        Integer i = 0;
        Integer emailCountAfter = EmailUtils.getEmailsCount(user, password);
        while( (i < maxWaitTimeInSeconds) && (emailCountAfter <= emailCountBefore))
        {
            i++;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            emailCountAfter = EmailUtils.getEmailsCount(user, password);
            if(emailCountAfter > emailCountBefore)
            {
                break;
            }
        }

        if(emailCountAfter <= emailCountBefore)
        {
            Assert.assertNotEquals("Email has not been received! Emails count is " + emailCountBefore, emailCountAfter, emailCountBefore);
        }
        else
        {
            logger.info("Email has been successfully received. Was " + emailCountBefore + ", become " + emailCountAfter);
        }
    }
    
    public static void waitForNewEmail(Integer emailCountBefore, Integer maxWaitTimeInSeconds)
    {
        Integer i = 0;
        Integer emailCountAfter = EmailUtils.getEmailsCount();
        while( (i < maxWaitTimeInSeconds) && (emailCountAfter <= emailCountBefore))
        {
            i++;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            emailCountAfter = EmailUtils.getEmailsCount();
            if(emailCountAfter > emailCountBefore)
            {
                break;
            }
        }

        if(emailCountAfter <= emailCountBefore)
        {
            Assert.assertNotEquals("Email has not been received! Emails count is " + emailCountBefore, emailCountAfter, emailCountBefore);
        }
        else
        {
            logger.info("Email has been successfully received. Was " + emailCountBefore + ", become " + emailCountAfter);
        }
    }
}
