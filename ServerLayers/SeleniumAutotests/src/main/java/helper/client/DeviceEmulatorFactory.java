package helper.client;

import java.util.HashMap;
import java.util.Map;

import com.usatech.layers.common.constants.InitializationReason;
import com.usatech.layers.common.messagedata.MessageData_C2;
import com.usatech.layers.common.messagedata.MessageData_C3;
import com.usatech.layers.common.messagedata.MessageData_C4;
import com.usatech.layers.common.messagedata.MessageData_CB;
import com.usatech.test.ClientEmulator;

import simple.io.ByteArrayUtils;

public class DeviceEmulatorFactory {

	private static final Map<String, DeviceEmulator> cachedClient = new HashMap<>();

	public static DeviceEmulator createClient(String serialNumber) {
		if (cachedClient.containsKey(serialNumber)) {
			return cachedClient.get(serialNumber);
		}

		DeviceEmulatorImpl ret = buildClient(serialNumber);
		cachedClient.put(serialNumber, ret);
		return ret;
	}

	private static DeviceEmulatorImpl buildClient(String serialNumber) {
		DeviceEmulatorImpl ret = new DeviceEmulatorImpl();
		ret.setEncryptKey(ByteArrayUtils.fromHex("6DF7E5A6FBC84C50A79F76762826C0DC"));
		ret.setNetLayerAuthPort(14109);
		ret.setNetLayerFormat(7);
		ret.setNetLayerHost("127.0.0.1");
		ret.setNetLayerPort(14108);
		ret.setPropertyListVersion(19);
		ret.setSerialNumber(serialNumber);
		return ret;
	}

	private static final class DeviceEmulatorImpl implements DeviceEmulator {

		private byte[] encryptKey;
		private String netLayerHost = "127.0.0.1";
		private int netLayerPort = 14108;
		private int netLayerAuthPort = 14109;
		private int netLayerFormat = 7;
		private int propertyListVersion = 19;
		private String serialNumber;
		private ClientEmulator ce = null;

		@Override
		public MessageData_C3 authV4(MessageData_C2 request) throws Exception {
			initDevice();
			ce.setPort(netLayerAuthPort);
			ce.startCommunication(netLayerFormat);
			MessageData_C3 response = (MessageData_C3) ce.sendMessage(request);
			ce.stopCommunication();
			return response;
		}

		@Override
		public MessageData_CB sale(MessageData_C4 request) throws Exception {
			initDevice();
			ce.setPort(netLayerPort);
			ce.startCommunication(netLayerFormat);
			MessageData_CB response = (MessageData_CB) ce.sendMessage(request);
			ce.stopCommunication();
			return response;
		}

		private void initDevice() throws Exception {
			if (ce == null) {
				ce = new ClientEmulator(netLayerHost, netLayerPort, "TD000000", encryptKey);
				ce.startCommunication(netLayerFormat);
				ce.testInitV4(serialNumber, InitializationReason.APPLICATION_UPGRADE, propertyListVersion);
				ce.testComponents(serialNumber);
				ce.stopCommunication();
			}
		}

		public void setEncryptKey(byte[] encryptKey) {
			this.encryptKey = encryptKey;
		}

		public void setNetLayerHost(String netLayerHost) {
			this.netLayerHost = netLayerHost;
		}

		public void setNetLayerPort(int netLayerPort) {
			this.netLayerPort = netLayerPort;
		}

		public void setNetLayerAuthPort(int netLayerAuthPort) {
			this.netLayerAuthPort = netLayerAuthPort;
		}

		public void setNetLayerFormat(int netLayerFormat) {
			this.netLayerFormat = netLayerFormat;
		}

		public void setPropertyListVersion(int propertyListVersion) {
			this.propertyListVersion = propertyListVersion;
		}

		public void setSerialNumber(String serialNumber) {
			this.serialNumber = serialNumber;
		}

	}

}
