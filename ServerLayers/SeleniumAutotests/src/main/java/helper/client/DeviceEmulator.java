package helper.client;

import com.usatech.layers.common.messagedata.MessageData_C2;
import com.usatech.layers.common.messagedata.MessageData_C3;
import com.usatech.layers.common.messagedata.MessageData_C4;
import com.usatech.layers.common.messagedata.MessageData_CB;

public interface DeviceEmulator {

	MessageData_C3 authV4(MessageData_C2 request) throws Exception;

	MessageData_CB sale(MessageData_C4 request) throws Exception;

}
