package helper;

//import mx4j.log.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;

import org.junit.Test;

import com.usatech.test.ClientEmulatorDevTest;


//Install packages:
//C:\apache-maven-3.3.9\bin\mvn install:install-file -Dfile=ojdbc14.jar -DgroupId=com.oracle -DartifactId=ojdbc14 -Dversion=10.2.0.1.0 -Dpackaging=jar
//C:\apache-maven-3.3.9\bin\mvn install:install-file -Dfile=ojdbc6.jar -DgroupId=com.oracle -DartifactId=ojdbc6 -Dversion=11.2.0.3 -Dpackaging=jar
//C:\apache-maven-3.3.9\bin\mvn install:install-file -Dfile=sqljdbc4.jar -Dpackaging=jar -DgroupId=com.microsoft.sqlserver -DartifactId=sqljdbc4 -Dversion=4.0
public class OracleConnector  {
    public static String environment;
    public static Connection conn;
    public OracleConnector(String environment) {
       this.environment = environment;
    }
    
    public static Statement getConnectionStatement() throws Exception {
//        String OracleHost = getData("OracleHost", "Connection");
//        String OracleUser = getData("OracleUser", "Connection");
//        String OraclePassword = getData("OraclePassword", "Connection");
//        String OraclePort = getData("OraclePort", "Connection");
//        String OracleSID = getData("OracleSID", "Connection");
        String OracleHost = "eccdb013.usatech.com";
        String OracleUser = Core.getProperty("localDbUser");
        String OraclePassword = Core.getProperty("localDbPassword");
        String OraclePort = "1523";
        String ServiceName = "usaodev04";
        if(environment.equalsIgnoreCase("int"))
        {
            OracleHost = "eccdb014.usatech.com";
            OracleUser = Core.getProperty("intDbUser");
            //OraclePassword = "dab00581";
            OraclePassword =  Core.getProperty("intDbPassword");
            OraclePort = "1524";
            ServiceName = "usadev02";
        }
        else if(environment.equalsIgnoreCase("ecc"))
        {
            OracleHost = "eccscan03.usatech.com";
            OracleUser = Core.getProperty("eccDbUser");
            OraclePassword = Core.getProperty("eccDbPassword");
            OraclePort = "1525";
            ServiceName = "usaecc_db.world";
        }



        //logger.info("Getting connection. Connection string = jdbc:oracle:thin:" + OracleUser + "/" + OraclePassword
        //		+ "@" + OracleHost + ":" + OraclePort + ":" + OracleSID + "");
        Class.forName("oracle.jdbc.OracleDriver");
        // Class.forName("com.oracle");

        // DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
        //ECC	ahachikyan@//eccscan03.usatech.com:1525/usaecc_db.world
        //jdbc:oracle:thin:ahachikyan/!@^QWEasdzxc@eccscan03.usatech.com:1525:usaecc_db.world
        String connectionString = "jdbc:oracle:thin:" + OracleUser + "/" + OraclePassword + "@"
                + OracleHost + ":" + OraclePort + "/" + ServiceName;
        System.out.println("Connection String=" + connectionString);
        conn = DriverManager.getConnection(connectionString);
        Statement statement = conn.createStatement();
        conn.setAutoCommit(true);
        //logger.info("Successfully connected");
        return statement;
    }

    public static void closeConnection() throws Exception {
        conn.close();
    }

    public static void commit() throws Exception {
        //TODO conn.autoCommit = true
        conn.commit();
    }

    public static ResultSet getQueryResultSet(String query) throws Exception {

        Statement statement = getConnectionStatement();
        //logger.info("Executing query:" + query);
        ResultSet rs = statement.executeQuery(query);
        //logger.info("Query executed successfully");
        return rs;

    }

    public static ArrayList<String> getArrayFromResultSet(String query) throws Exception {
        ArrayList<ArrayList> lines = new ArrayList<ArrayList>();
        ArrayList<String> columns = new ArrayList<String>();

        ResultSet rs = getQueryResultSet(query);

        ResultSetMetaData rsmd = rs.getMetaData();
        int numberOfColumns = rsmd.getColumnCount();

        while (rs.next() == true) {
            for (int columnIndex = 1; columnIndex <= numberOfColumns; columnIndex++) {
                columns.add(rs.getString(columnIndex));
            }
            lines.add(columns);
        }
        closeConnection();
        return lines.get(0);
    }

//    public static ArrayList<ArrayList> getArrayFromResultSet(String query) throws Exception {
//        ArrayList<ArrayList> lines = new ArrayList<ArrayList>();
//        ArrayList<String> columns = new ArrayList<String>();
//
//        ResultSet rs = getQueryResultSet(query);
//
//        ResultSetMetaData rsmd = rs.getMetaData();
//        int numberOfColumns = rsmd.getColumnCount();
//
//        while (rs.next() == true) {
//            for (int columnIndex = 1; columnIndex <= numberOfColumns; columnIndex++) {
//                columns.add(rs.getString(columnIndex));
//            }
//            lines.add(columns);
//        }
//        return lines;
//    }
    public static String getEncryptionKey(String deviceName) throws Exception {
    {
    	return getFirstValueFromQuery("select rawtohex(encryption_key) from device.device where device_name = '" + deviceName + "' and device_active_yn_flag = 'Y'");
    }
    	
    }
    public static String getFirstValueFromQuery(String query) throws Exception {
        ResultSet rs;
        rs = getQueryResultSet(query);
//        System.out.print(rs);
        if (!rs.next())
            return "";
        else
            return rs.getString(1);
//		ArrayList<ArrayList> resultArray = getArrayFromResultSet(query);
//		if(resultArray.size() == 0)
//		{
//			return "";
//		}
//		String firstResult = (String) resultArray.get(0).get(0);
//		return firstResult;
    }

    public static void executeQuery(String query) throws Exception {
        getQueryResultSet(query);

//		ArrayList<ArrayList> resultArray = getArrayFromResultSet(query);
//		if(resultArray.size() == 0)
//		{
//			return "";
//		}
//		String firstResult = (String) resultArray.get(0).get(0);
//		return firstResult;
    }

    public static int executeUpdate(String query) throws Exception {
        Statement statement = getConnectionStatement();
        int updatedRowsCount = statement.executeUpdate(query);
//        commit();
        return updatedRowsCount;
    }
    
    public static String getDeviceIdBySerialNumber(String serialNumber) throws Exception
    {
    	return getFirstValueFromQuery("select DEVICE_ID from DEVICE.DEVICE where DEVICE_SERIAL_CD = '" + serialNumber + "'");
    	}
    
    public static String getDeviceNameBySerialNumber(String serialNumber) throws Exception
    {
    	return getFirstValueFromQuery("select DEVICE_NAME from DEVICE.DEVICE where DEVICE_SERIAL_CD = '" + serialNumber + "'");
    	}
    
    public static String getDeviceSettingValueByDeviceIdAndParameterCd(String deviceId, String parameterCd) throws Exception
    {
    	return getFirstValueFromQuery("select DEVICE_SETTING_VALUE from DEVICE.DEVICE_SETTING where DEVICE_ID='" + deviceId + "' and DEVICE_SETTING_PARAMETER_CD='" + parameterCd + "'");
    	}
    
    
}
