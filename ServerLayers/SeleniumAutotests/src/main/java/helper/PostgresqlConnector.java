package helper;

//import mx4j.log.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;

import org.junit.Ignore;
import org.junit.Test;

import com.usatech.test.ClientEmulatorDevTest;

//Install packages:
//C:\apache-maven-3.3.9\bin\mvn install:install-file -Dfile=ojdbc14.jar -DgroupId=com.oracle -DartifactId=ojdbc14 -Dversion=10.2.0.1.0 -Dpackaging=jar
//C:\apache-maven-3.3.9\bin\mvn install:install-file -Dfile=ojdbc6.jar -DgroupId=com.oracle -DartifactId=ojdbc6 -Dversion=11.2.0.3 -Dpackaging=jar
//C:\apache-maven-3.3.9\bin\mvn install:install-file -Dfile=sqljdbc4.jar -Dpackaging=jar -DgroupId=com.microsoft.sqlserver -DartifactId=sqljdbc4 -Dversion=4.0
public class PostgresqlConnector  {
    public static String environment;
    public static Connection conn;
    public PostgresqlConnector(String environment) {
       this.environment = environment;
    }

//    @Test
//    @Ignore
    public void TestPostgreSQlSell() throws Exception 
	{      
        String deviceSerialNumber= "VJ011000110";
        String env = "dev";
        String deviceName = "TD010707";
                
//    	String CC_DATA = "4276380086871717=12102011511101700000"; //Svetlana`s expired visa credit card
    	String CC_DATA =  "4173270594781389=25121019999888877776";//Chase Tandem credit card
		
		PostgresqlConnector postgresqlConnector = new PostgresqlConnector(env);  

		String encKeyFormDb= postgresqlConnector.getEncryptionKey(deviceName);	
		ClientEmulatorDevTest clientEmulatorDevTest = new ClientEmulatorDevTest();
		clientEmulatorDevTest.testSell(env, CC_DATA, deviceSerialNumber, deviceName, encKeyFormDb);      
    }
	
    public static Statement getConnectionStatement() throws Exception {Connection connection = null;
    //URL к базе состоит из протокола:подпротокола://[хоста]:[порта_СУБД]/[БД] и других_сведений
    //String url = "jdbc:postgresql://devpgs10:5432/main";
    String url = "jdbc:postgresql://usadb051.trooper.usatech.com:5432/usat_test5";
    
    //Имя пользователя БД
    String name = "usatuser";
    //Пароль
    String password = "usatuser";
   
        //Загружаем драйвер
        Class.forName("org.postgresql.Driver");
        System.out.println("Драйвер подключен");
        //Создаём соединение
        connection = DriverManager.getConnection(url, name, password);
        System.out.println("Соединение установлено");
       
        //Для использования SQL запросов существуют 3 типа объектов:
        //1.Statement: используется для простых случаев без параметров
        Statement statement = null;

        statement = connection.createStatement();
        return statement;
//        //Выполним запрос
//        ResultSet result1 = statement.executeQuery(
//                "SELECT * FROM users where id >2 and id <10");
//        //result это указатель на первую строку с выборки
//        //чтобы вывести данные мы будем использовать 
//        //метод next() , с помощью которого переходим к следующему элементу
//        System.out.println("Выводим statement");
//        while (result1.next()) {
//            System.out.println("Номер в выборке #" + result1.getRow()
//                    + "\t Номер в базе #" + result1.getInt("id")
//                    + "\t" + result1.getString("username"));
//        }
//        // Вставить запись
//        statement.executeUpdate(
//                "INSERT INTO users(username) values('name')");
//        //Обновить запись
//        statement.executeUpdate(
//                "UPDATE users SET username = 'admin' where id = 1");
        
        

        //2.PreparedStatement: предварительно компилирует запросы, 
        //которые могут содержать входные параметры
//        PreparedStatement preparedStatement = null;
//        // ? - место вставки нашего значеня
//        preparedStatement = connection.prepareStatement(
//                "SELECT * FROM users where id > ? and id < ?");
//        //Устанавливаем в нужную позицию значения определённого типа
//        preparedStatement.setInt(1, 2);
//        preparedStatement.setInt(2, 10);
//        //выполняем запрос
//        ResultSet result2 = preparedStatement.executeQuery();
//        
//        System.out.println("Выводим PreparedStatement");
//        while (result2.next()) {
//            System.out.println("Номер в выборке #" + result2.getRow()
//                    + "\t Номер в базе #" + result2.getInt("id")
//                    + "\t" + result2.getString("username"));
//        }
//        
//        preparedStatement = connection.prepareStatement(
//                "INSERT INTO users(username) values(?)");
//        preparedStatement.setString(1, "user_name");
//        //метод принимает значение без параметров
//        //темже способом можно сделать и UPDATE
//        preparedStatement.executeUpdate();
//        
//        
//
//        //3.CallableStatement: используется для вызова хранимых функций,
//        // которые могут содержать входные и выходные параметры
//        CallableStatement callableStatement = null;
//        //Вызываем функцию myFunc (хранится в БД)
//        callableStatement = connection.prepareCall(
//                " { call myfunc(?,?) } ");
//        //Задаём входные параметры
//        callableStatement.setString(1, "Dima");
//        callableStatement.setString(2, "Alex");
//        //Выполняем запрос
//        ResultSet result3 = callableStatement.executeQuery();
//        //Если CallableStatement возвращает несколько объектов ResultSet,
//        //то нужно выводить данные в цикле с помощью метода next
//        //у меня функция возвращает один объект
//        result3.next();
//        System.out.println(result3.getString("MESSAGE"));
        //если функция вставляет или обновляет, то используется метод executeUpdate()

//    } catch (Exception ex) {
//        //выводим наиболее значимые сообщения
//        Logger.getLogger(JDBCtest.class.getName()).log(Level.SEVERE, null, ex);
//    } finally {
//        if (connection != null) {
//            try {
//                connection.close();
//            } catch (SQLException ex) {
//                Logger.getLogger(JDBCtest.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//    }
    }
    public static String getEncryptionKey(String deviceName) throws Exception {
        {
        	return getFirstValueFromQuery("select encode(encryption_key, 'hex') from device.device where device_name = '" + deviceName + "' and device_active_yn_flag = 'Y'");
        }
        	
        }
        public static String getFirstValueFromQuery(String query) throws Exception {
            ResultSet rs;
            rs = getQueryResultSet(query);
//            System.out.print(rs);
            if (!rs.next())
                return "";
            else
                return rs.getString(1);
//    		ArrayList<ArrayList> resultArray = getArrayFromResultSet(query);
//    		if(resultArray.size() == 0)
//    		{
//    			return "";
//    		}
//    		String firstResult = (String) resultArray.get(0).get(0);
//    		return firstResult;
        }
        public static ResultSet getQueryResultSet(String query) throws Exception {

            Statement statement = getConnectionStatement();
            //logger.info("Executing query:" + query);
            ResultSet rs = statement.executeQuery(query);
            //logger.info("Query executed successfully");
            return rs;

        }
    }
//
//    public static void closeConnection() throws Exception {
//        conn.close();
//    }
//
//    public static void commit() throws Exception {
//        //TODO conn.autoCommit = true
//        conn.commit();
//    }

//    public static ResultSet getQueryResultSet(String query) throws Exception {
//
//        Statement statement = getConnectionStatement();
//        //logger.info("Executing query:" + query);
//        ResultSet rs = statement.executeQuery(query);
//        //logger.info("Query executed successfully");
//        return rs;
//
//    }
//
//    public static ArrayList<String> getArrayFromResultSet(String query) throws Exception {
//        ArrayList<ArrayList> lines = new ArrayList<ArrayList>();
//        ArrayList<String> columns = new ArrayList<String>();
//
//        ResultSet rs = getQueryResultSet(query);
//
//        ResultSetMetaData rsmd = rs.getMetaData();
//        int numberOfColumns = rsmd.getColumnCount();
//
//        while (rs.next() == true) {
//            for (int columnIndex = 1; columnIndex <= numberOfColumns; columnIndex++) {
//                columns.add(rs.getString(columnIndex));
//            }
//            lines.add(columns);
//        }
//        closeConnection();
//        return lines.get(0);
//    }

//    public static ArrayList<ArrayList> getArrayFromResultSet(String query) throws Exception {
//        ArrayList<ArrayList> lines = new ArrayList<ArrayList>();
//        ArrayList<String> columns = new ArrayList<String>();
//
//        ResultSet rs = getQueryResultSet(query);
//
//        ResultSetMetaData rsmd = rs.getMetaData();
//        int numberOfColumns = rsmd.getColumnCount();
//
//        while (rs.next() == true) {
//            for (int columnIndex = 1; columnIndex <= numberOfColumns; columnIndex++) {
//                columns.add(rs.getString(columnIndex));
//            }
//            lines.add(columns);
//        }
//        return lines;
//    }

//    public static String getFirstValueFromQuery(String query) throws Exception {
//        ResultSet rs;
//        rs = getQueryResultSet(query);
////        System.out.print(rs);
//        if (!rs.next())
//            return "";
//        else
//            return rs.getString(1);
////		ArrayList<ArrayList> resultArray = getArrayFromResultSet(query);
////		if(resultArray.size() == 0)
////		{
////			return "";
////		}
////		String firstResult = (String) resultArray.get(0).get(0);
////		return firstResult;
//    }
//
//    public static void executeQuery(String query) throws Exception {
//        getQueryResultSet(query);
//
////		ArrayList<ArrayList> resultArray = getArrayFromResultSet(query);
////		if(resultArray.size() == 0)
////		{
////			return "";
////		}
////		String firstResult = (String) resultArray.get(0).get(0);
////		return firstResult;
//    }

//    public static int executeUpdate(String query) throws Exception {
//        Statement statement = getConnectionStatement();
//        int updatedRowsCount = statement.executeUpdate(query);
////        commit();
//        return updatedRowsCount;
//    }
//    
//    public static String getDeviceIdBySerialNumber(String serialNumber) throws Exception
//    {
//    	return getFirstValueFromQuery("select DEVICE_ID from DEVICE.DEVICE where DEVICE_SERIAL_CD = '" + serialNumber + "'");
//    	}
//    
//    public static String getDeviceNameBySerialNumber(String serialNumber) throws Exception
//    {
//    	return getFirstValueFromQuery("select DEVICE_NAME from DEVICE.DEVICE where DEVICE_SERIAL_CD = '" + serialNumber + "'");
//    	}
//    
//    public static String getDeviceSettingValueByDeviceIdAndParameterCd(String deviceId, String parameterCd) throws Exception
//    {
//    	return getFirstValueFromQuery("select DEVICE_SETTING_VALUE from DEVICE.DEVICE_SETTING where DEVICE_ID='" + deviceId + "' and DEVICE_SETTING_PARAMETER_CD='" + parameterCd + "'");
//    	}
//    
    

