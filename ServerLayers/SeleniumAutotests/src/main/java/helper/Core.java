package helper;

import org.junit.Assert;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.junit.*;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.*;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.event.InputEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Core {
    public WebDriver driver;
    public  WebDriverWait wait;
    public static final Logger logger = Logger.getLogger(Core.class);
    public static final String tempFolderForDownloads = System.getProperty("java.io.tmpdir") + "\\autotests";

    //Get browser name and environment from the command line. If not set then use defaults
    private static String driverName = System.getProperty("browser");
    private static String environment = System.getProperty("environment");

    private final static String defaultEnvironment = "int";
    private final static String defaultBrowser = "ff";
    private static String startPageUrl = "";

    @Before
    public void setUp() throws Exception {
    	// Previous Firefox 39.0 + selenium-firefox-driver 2.47.0
    	// Current  Firefox 53.0 + Selenium 3.40 + Geckodriver 0.16.0).

        new File(tempFolderForDownloads).mkdir(); //Create temporary
//        Files.createDirectory(tempFolderForDownloads);
//        if(driver == null) {

        //Set default browser
        if (driverName == null) {
            driverName = defaultBrowser;
        }
        logger.info("Browser = " + driverName);

        //Set default environment
        if (environment == null) {
            environment = defaultEnvironment;
        }
        logger.info("Environment = " + environment);


        if (driverName.equalsIgnoreCase("Firefox") || driverName.equalsIgnoreCase("FF")) {
        	String pathToGeckoDriver=new File("").getAbsolutePath() + "\\lib\\geckodriver-v0.22.0-win64\\geckodriver.exe";
        	System.out.println("Gecko is here:" + pathToGeckoDriver); 
        	System.setProperty("webdriver.gecko.driver",pathToGeckoDriver);
            FirefoxProfile profile = new FirefoxProfile();
            profile.setAcceptUntrustedCertificates(true);
            profile.setPreference("browser.helperApps.alwaysAsk.force", false);
            profile.setPreference("browser.download.manager.showWhenStarting", false);
            profile.setPreference("browser.download.folderList", 2);
            profile.setPreference("browser.download.dir", tempFolderForDownloads); // my downloading dir
            profile.setPreference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", false);
            profile.setPreference("browser.download.useDownloadDir", true);
            profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/pdf,text/html,application/msword,application/xls,text/comma-separated-values,application/vnd.ms-excel,application/xml,application/csv,text/csv,image/png,image/jpeg,application/octet-stream,application/force-download");
            profile.setPreference("pdfjs.disabled", true);
//             Firebug
//            if (java.lang.management.ManagementFactory.getRuntimeMXBean().
//                    getInputArguments().toString().indexOf("-agentlib:jdwp") > 0) {
//                logger.info(">>> DEBUG MODE");
//                profile.addExtension(ResourceReader.getInstance().getFileFromResource(
//                        "fireFoxPlugins/firebug@software.joehewitt.com.xpi"));
//                profile.setPreference("extensions.firebug.currentVersion", "9.9.9");
////                 FirePath
//                profile.addExtension(ResourceReader.getInstance().getFileFromResource(
//                        "fireFoxPlugins/FireXPath@pierre.tholence.com.xpi"));
//            } else {
//                logger.info(">>> RUN MODE");
//            }
    		
//    		options.setBinary("C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe"); //Location where Firefox is installed

//    		DesiredCapabilities capabilities = DesiredCapabilities.firefox();
    		
//    		capabilities.setCapability(FirefoxDriver.BINARY, pathToBinary);
//    		capabilities.setCapability(CapabilityType.BROWSER_NAME, "firefox");
//    		capabilities.setCapability("marionette", true);
//            
//            	      .addPreference("browser.startup.page", 1)
//            	      .addPreference("browser.startup.homepage", "https://www.google.co.uk");
        	FirefoxOptions firefoxOptions = new FirefoxOptions();
//            firefoxOptions.addPreference("browser.helperApps.alwaysAsk.force", false);
//            firefoxOptions.addPreference("browser.download.manager.showWhenStarting", false);
//            firefoxOptions.addPreference("browser.download.folderList", 2);
//            firefoxOptions.addPreference("browser.download.dir", tempFolderForDownloads); // my downloading dir
//            firefoxOptions.addPreference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", false);
//            firefoxOptions.addPreference("browser.download.useDownloadDir", true);
//            firefoxOptions.addPreference("browser.helperApps.neverAsk.saveToDisk", "application/pdf,text/html,application/msword,application/xls,text/comma-separated-values,application/vnd.ms-excel,application/xml,application/csv,text/csv,image/png,image/jpeg,application/octet-stream,application/force-download");
//            firefoxOptions.addPreference("pdfjs.disabled", true);
//            firefoxOptions.setCapability("platformName", "windowsnt");
    		//set more capabilities as per your requirements
//        	String pathToBinary = "C:\\Program Files\\Mozilla Firefox\\firefox.exe";
//        	FirefoxBinary binary = new FirefoxBinary(new File(pathToBinary));
//        	FirefoxOptions firefoxOptions = new FirefoxOptions();
//        	firefoxOptions.setBinary(pathToBinary);
      
        	firefoxOptions.setProfile(profile);
        	driver = new FirefoxDriver(firefoxOptions);
            driver.manage().timeouts().implicitlyWait(180, TimeUnit.SECONDS);
//            driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
//            driver.get("https://www.google.co.za");
            driver.manage().window().setSize(new Dimension(1600, 900));
//            driver.manage().window().maximize();
//            wait = new WebDriverWait(driver, 30);
//            driver = new FirefoxDriver(profile);
        } 
        else if (driverName.equalsIgnoreCase("Chrome")) {
            //System.setProperty("webdriver.chrome.driver", "/path/to/chromedriver");
            HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
            chromePrefs.put("profile.default_content_settings.popups", 0);
            chromePrefs.put("download.default_directory", tempFolderForDownloads);
            ChromeOptions options = new ChromeOptions();
            HashMap<String, Object> chromeOptionsMap = new HashMap<String, Object>();
            options.setExperimentalOption("prefs", chromePrefs);
            options.addArguments("--test-type");
            DesiredCapabilities cap = DesiredCapabilities.chrome();
            cap.setCapability(ChromeOptions.CAPABILITY, chromeOptionsMap);
            cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
            cap.setCapability(ChromeOptions.CAPABILITY, options);

            System.setProperty("webdriver.chrome.driver", new File("").getAbsolutePath() + "\\lib\\chromedriver\\chromedriver.exe");
            driver = new ChromeDriver(cap);
            
//            driver.manage().window().maximize();
        } else if (driverName.equalsIgnoreCase("IE") || driverName.equalsIgnoreCase("iexplorer") || driverName.equalsIgnoreCase("InternetExplorer") || driverName.equalsIgnoreCase("Internet Explorer")) {
            // You would need to set the protected mode settings to same for all zones in IE
            // See details here http://jimevansmusic.blogspot.in/2012/08/youre-doing-it-wrong-protected-mode-and.html
            String s = System.getProperty("os.arch");
            if (s.equals("x86")) {
                System.setProperty("webdriver.ie.driver", new File(
                        "src/main/resources/drivers/ie/x86/IEDriverServer.exe").getAbsolutePath());

            } else if (s.equals("amd64")) {
                System.setProperty("webdriver.ie.driver", new File(
                        "src/main/resources/drivers/ie/x64/IEDriverServer.exe").getAbsolutePath());

            } else {
                throw new Exception("Cannot set properly the IEDriverServer!");
            }
            driver = new InternetExplorerDriver();
        } else {
            throw new Exception("Browser is incorrect = " + driverName);
        }

        //Sets timeout for waiting for element state (present or visible). See details here http://docs.seleniumhq.org/docs/04_webdriver_advanced.jsp#explicit-and-implicit-waits

        //Move mouse randomly to avoid getting computer to sleep
        Random r = new Random();
        int randomNumber = r.ints(1, 0, 480).findFirst().getAsInt();
        Robot bot = new Robot();
        bot.mouseMove(randomNumber, randomNumber);
        // bot.mousePress(InputEvent.BUTTON1_MASK);
        wait = new WebDriverWait(driver, 30);

        //openUsaLive();
//        driver.manage().window().setSize(new Dimension(1040, 758));
//            LogInPage logInPage = new LogInPage(driver);
//        if(environment.equalsIgnoreCase("ecc")) {
//            logInPage.logIn(eccUserName, eccUserPassword);
//        }
//        else if(environment.equalsIgnoreCase("int")) {
//            logInPage.logIn(intUserName, intUserPassword);
//        }
//        else
//        {
//            Assert.fail("Unexpected environment:" + environment);
//        }
//        }
//        else
//        {
//            driver.get(getStartPageUrl());
//            logger.info("Browser is already started");
//        }
    }

    public void openUsaLive() {
        if (environment.equalsIgnoreCase("ecc")) {
            startPageUrl = "https://usalive-ecc.usatech.com";
            driver.get(startPageUrl + "/home.i");
        } else if (environment.equalsIgnoreCase("int")) {
            startPageUrl = "https://usalive-int.usatech.com";
            driver.get(startPageUrl + "/home.i");
        } else if (environment.equalsIgnoreCase("local")) {
            startPageUrl = "http://localhost:8880";
            driver.get(startPageUrl + "/login.i");

        } 
    else if (environment.equalsIgnoreCase("dev")) {
        startPageUrl = "https://usalive-dev.usatech.com";
        driver.get(startPageUrl + "/login.i");
    }else
        {
            Assert.fail("Environment is incorrect:" + environment);
        }
    }

    public void openDms() {
        if (environment.equalsIgnoreCase("ecc")) {
            startPageUrl = "https://dms-ecc.usatech.com:8443/";
            driver.get(startPageUrl + "/home.i");
        } else if (environment.equalsIgnoreCase("int")) {
            startPageUrl = "https://dms-int.usatech.com:8443/";
            driver.get(startPageUrl + "/home.i");
        } else if (environment.equalsIgnoreCase("dev")) {
            startPageUrl = "https://dms-dev.usatech.com:8443/";
            driver.get(startPageUrl + "/home.i");
        } else if (environment.equalsIgnoreCase("local")) {
            startPageUrl = "http://www.localhost.com:8780";
            driver.get(startPageUrl + "/home.i");
        } else if (environment.equalsIgnoreCase("prod")) {
        startPageUrl = "https://dms.usatech.com:8443";
        driver.get(startPageUrl + "/home.i");
        }else
        {
            Assert.fail("Environment is incorrect:" + environment);
        }
    }

    @After
    public void tearDown() throws Exception {
        //Not closing browser here because actions on fail and success are override and also close the browser
//        if(driver!=null)
//            driver.quit();
    }

    @Rule
    public TestWatcher watchman = new TestWatcher() {
        //Override actions on fail to make screenshots and do other actions
        @Override
        protected void failed(Throwable e, Description description) {
            File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            try {
                logger.error("Screenshot on fail stored at:" + Paths.get(".").toAbsolutePath().normalize().toString() + "\\screenshots\\" + description.getMethodName() + ".png");
                FileUtils.copyFile(scrFile, new File(
                        "screenshots\\" + description.getMethodName() + ".png"));
            } catch (IOException e1) {
                logger.error("Fail to take screen shot");
            }
            logger.info("Test '" + description.getMethodName() + "' is Failed. Closing browser...");
            driver.quit();
        }

        //Override actions on success to only clg success and close browser
        @Override
        protected void succeeded(Description description) {
            logger.info("Test '" + description.getMethodName() + "' is Passed. Closing browser...");
            driver.quit();
        }
    };

    public static String getEnvironment()
    {
        return environment;
    }

    public static String getStartPageUrl() {
        return startPageUrl;
    }

    public  WebDriver getDriver()
    {
        return this.driver;
    }

    public static String getProperty(String propertyName)
    {
    	Properties prop = new Properties();
    	InputStream input = null;
		try {
			input = new FileInputStream("config.properties");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// load a properties file
		try {
			prop.load(input);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return prop.getProperty(propertyName);
				
    }
    
    public String[] getUsaLiveWeakUserNameAndPassword()
    {

        if(environment.equalsIgnoreCase("ecc")) {
        	return new String[]{getProperty("eccUsaLiveWeakUserLogin"), getProperty("eccUsaLiveWeakUserPassword")};
        }
        else if(environment.equalsIgnoreCase("int")) {
        	return new String[]{getProperty("intUsaLiveWeakUserLogin"), getProperty("intUsaLiveWeakUserPassword")};
        }
        else if(environment.equalsIgnoreCase("dev")) {
        	return new String[]{getProperty("devUsaLiveWeakUserLogin"), getProperty("devUsaLiveWeakUserPassword")};
        }
        else
        {
            org.junit.Assert.fail("Unexpected environment:" + environment);
            return null;
        }
    }
    
    public String[] getEmailUserNameAndPassword()
    {
        	return new String[]{getProperty("emailUser"), getProperty("emailPassword")};
    }
    

    public String[] getUsaLivePowerUserNameAndPassword() 
    
    {

        if(environment.equalsIgnoreCase("ecc")) {
            return new String[]{getProperty("eccUsaLivePowerUserLogin"), getProperty("eccUsaLivePowerUserPassword")};
        }
        else if(environment.equalsIgnoreCase("int")) {
            return new String[]{getProperty("intUsaLivePowerUserLogin"), getProperty("intUsaLivePowerUserPassword")};
        }
        else if(environment.equalsIgnoreCase("dev")) {
            return new String[]{getProperty("devUsaLivePowerUserLogin"), getProperty("devUsaLivePowerUserPassword")};
        }
        else if(environment.equalsIgnoreCase("local")) {
        	return new String[]{getProperty("localUsaLivePowerUserLogin"), getProperty("localUsaLivePowerUserPassword")};
        }
        else
        {
            org.junit.Assert.fail("Unexpected environment:" + environment);
            return null;
        }
    }
    
    public String[] getUsaLiveCustomerUserNameAndPassword() 
    
    {

        if(environment.equalsIgnoreCase("ecc")) {
            return new String[]{getProperty("customerUserEccLogin"), getProperty("customerUserEccPassword")};
        }
//        else if(environment.equalsIgnoreCase("int")) {
//            return new String[]{getProperty("intUsaLivePowerUserLogin"), getProperty("intUsaLivePowerUserPassword")};
//        }
//        else if(environment.equalsIgnoreCase("dev")) {
//            return new String[]{getProperty("devUsaLivePowerUserLogin"), getProperty("devUsaLivePowerUserPassword")};
//        }
//        else if(environment.equalsIgnoreCase("local")) {
//        	return new String[]{getProperty("localUsaLivePowerUserLogin"), getProperty("localUsaLivePowerUserPassword")};
//        }
        else
        {
            org.junit.Assert.fail("Unexpected environment:" + environment);
            return null;
        }
    }

    
//    public String[] getUsaLiveNotLdapUserNameAndPassword() 
//    
//    {
//        if(environment.equalsIgnoreCase("ecc")) {
//        	return new String[]{getProperty("eccUsaLiveNotLdapUserLogin"), getProperty("eccUsaLiveNotLdapUserPassword")};
//        }
//        else if(environment.equalsIgnoreCase("int")) {
//        	return new String[]{getProperty("intUsaLiveNotLdapUserLogin"), getProperty("intUsaLiveNotLdapUserPassword")};
//        }
//        else if(environment.equalsIgnoreCase("dev")) {
//        	return new String[]{getProperty("devUsaLiveNotLdapUserLogin"), getProperty("devUsaLiveNotLdapUserPassword")};
//        }
//        else
//        {
//            org.junit.Assert.fail("Unexpected environment:" + environment);
//            return null;
//        }
//    	
//
//    }
    
    public String[] getDmsCommonUserNameAndPassword()
    {
    	return new String[]{getProperty("commonDmsUserLogin"), getProperty("commonDmsUserPassword")};
    }

}
