package helper;

import com.google.common.io.Resources;
import com.opencsv.CSVReader;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.util.ImageIOUtil;
import org.apache.pdfbox.util.PDFTextStripper;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.XMLUnit;
import org.custommonkey.xmlunit.exceptions.XpathException;
import org.dbunit.Assertion;
import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.excel.XlsDataSet;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.imageio.ImageIO;
import javax.xml.parsers.ParserConfigurationException;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.*;

import static helper.FileUtils.getResoucePath;

public class CompareUtils {
    public static final Boolean generateEtalons = false;
    public static final Logger logger = Logger.getLogger(CompareUtils.class);
    public static void compareImages(String expectedImageUrl, String actualImageUrl, String differenceImageUrl) throws IOException {
        compareImages(expectedImageUrl, actualImageUrl, differenceImageUrl,0);
    }
    public static void compareImages(String expectedImageUrl, String actualImageUrl, String differenceImageUrl, Integer yOffset) throws IOException {
        logger.info("Loading expected file...");
        Assert.assertTrue("Expected image is missed here:" + expectedImageUrl,FileUtils.getFile(expectedImageUrl).exists());
        BufferedImage expectedImage	= ImageIO.read(new File(expectedImageUrl));
        logger.info("Loading actual file...");
        BufferedImage actualImage = ImageIO.read(new File(actualImageUrl));

        Integer expectedWidth = expectedImage.getWidth();
        Integer actualWidth = actualImage.getWidth();
        Assert.assertEquals("Width is not same! See images below:\n\tExpected:" + expectedImageUrl + "\n\tActual:" + actualImageUrl, expectedWidth, actualWidth);

        Integer expectedHeight = expectedImage.getHeight();
        Integer actualHeight = actualImage.getHeight();
        Assert.assertEquals("Height is not same! See  images below: \n\tExpected:" + expectedImageUrl + "\n\tActual:" + actualImageUrl, expectedHeight, actualHeight);

        BufferedImage differenceImage = imageToBufferedImage(actualImage);
        Graphics2D outImgGraphics = differenceImage.createGraphics();
        outImgGraphics.setColor(Color.RED);

        Boolean isEqual = true;
        for (Integer y = yOffset; y < expectedHeight -  100; y++) {
            for (Integer x = 0; x < expectedWidth; x++) {
                if (expectedImage.getRGB(x, y) != actualImage.getRGB(x, y))
                {
                    isEqual = false;
//                    differenceImage.setRGB(x, y, Color.red.getRGB());
                    differenceImage.setRGB(x, y, Color.GREEN.getRGB());
                }
            }
        }
        if(!isEqual && differenceImage != null && !differenceImageUrl.isEmpty())
            saveImage(differenceImage,differenceImageUrl);
        Assert.assertTrue("Images are not equal. See  images below: \n\tExpected:" + expectedImageUrl + "\n\tActual:" + actualImageUrl + "\n\tDiff:" + differenceImageUrl, isEqual);
        logger.info("Images are equal.");
    }

    private static BufferedImage imageToBufferedImage(Image img) {
        BufferedImage bi = new BufferedImage(img.getWidth(null),
                img.getHeight(null), BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = bi.createGraphics();
        g2.drawImage(img, null, null);
        return bi;
    }

    public static void saveImage(Image img, String filename) throws IOException {
        BufferedImage bi = imageToBufferedImage(img);
        try {
            File outputfile = new File(filename);
            ImageIO.write(bi, "png", outputfile);
        } catch (java.io.FileNotFoundException io) {
            System.out.println("File Not Found");
        }
    }

    public static void writePdfPageToImage(PDPage pdfPage, String imagePath) throws IOException {
        BufferedImage  image = pdfPage.convertToImage();;
        ImageIOUtil.writeImage(image, imagePath , 300);
    }


    public static String extractTextFromPdf(byte[] pdfData) throws IOException, URISyntaxException {
        PDDocument pdfDocument = PDDocument.loadNonSeq(new ByteArrayInputStream(pdfData), null);
        try {
            return new PDFTextStripper().getText(pdfDocument);
        } finally {
            pdfDocument.close();
        }
    }

    public static String getLastModifiedFile(String filePath, String ext) {
        logger.info("Searching for last modified file...");
        File theNewestFile = null;
        File dir = new File(filePath);
        FileFilter fileFilter = new WildcardFileFilter("*." + ext);
        File[] files = dir.listFiles(fileFilter);

        if (files.length > 0) {
            /** The newest file comes first **/
            Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
            theNewestFile = files[0];
        }
        logger.info("Last modified file is:" + theNewestFile);
        return theNewestFile == null ? null : theNewestFile.getPath();
    }

    public static String getLastModifiedFile(String filePath) {
        logger.info("Searching for last modified file...");
        File theNewestFile = null;
        File dir = new File(filePath);
        FileFilter fileFilter = new WildcardFileFilter("*");
        File[] files = dir.listFiles(fileFilter);

        if (files.length > 0) {
            /** The newest file comes first **/
            Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
            theNewestFile = files[0];
        }
        logger.info("Last modified file is:" + theNewestFile);
        return theNewestFile == null ? null : theNewestFile.getPath();
    }

//    public static void verifyPng(WebDriver driver, WebDriverWait wait, String reportName, String testGroupFolder) throws IOException {
//    	WebElement actualWebElement = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='reportContent' or @class='offer-signups' or @class='full']")));
//    	//File actualDriverScreenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
//    	File actualDriverScreenshot = ((TakesScreenshot)actualWebElement).getScreenshotAs(OutputType.FILE);
//
//        BufferedImage wholeWindowScreenshot	= ImageIO.read(actualDriverScreenshot);
//        //This workaround needs because getScreenshotAs returns screenshot with max width of 32766pix. But height of report web element arep is could be much higher and fails with raster exception
//        if(wholeWindowScreenshot.getHeight() > 32000)
//        {
//            wholeWindowScreenshot = wholeWindowScreenshot.getSubimage(0, 0, wholeWindowScreenshot.getWidth(), 32000);
//        }
//
//        // Web element to make screenshot from
//        
////        String fullImageUrl = getScreenshotReportPath(reportName + "_full.png");;
////        CompareUtils.saveImage(wholeWindowScreenshot, fullImageUrl);
////        logger.info("Full not cropped image stored at:" + fullImageUrl);
//        //Create an actual screenshot
//        Integer webElementX = actualWebElement.getLocation().getX();
//        Integer webElementY =  actualWebElement.getLocation().getY();
//
//        Integer webElementWidth = actualWebElement.getSize().getWidth();
//        Integer webElementHeight = actualWebElement.getSize().getHeight();
//
//        Integer wholeImageWidth = wholeWindowScreenshot.getWidth();
//        Integer wholeImageHeight= wholeWindowScreenshot.getHeight();
//
//        //If the Image too large then cut
//        if(webElementHeight >= 32000)
//        {
//            webElementHeight = 32000 -  webElementY;
//        }
//        BufferedImage actualImageCropped = wholeWindowScreenshot.getSubimage(webElementX, webElementY, webElementWidth, webElementHeight);
////        BufferedImage actualImageCropped = screenshot.getImage();
//
//        String actualImageUrl = getScreenshotReportPath(testGroupFolder + reportName + "_act.png");;
//        String differenceImageUrl = getScreenshotReportPath(testGroupFolder + reportName + "_dif.png");
//        CompareUtils.saveImage(actualImageCropped, actualImageUrl);
//        logger.info("Actual image stored at:" + actualImageUrl);
//        String expectedImageUrl = getScreenshotReportPath(testGroupFolder + reportName + "_exp.png");
//        if(!generateEtalons)
//            CompareUtils.compareImages(expectedImageUrl,actualImageUrl,differenceImageUrl);
//    }
//    
    public static void verifyPng(WebDriver driver, WebDriverWait wait, String reportName, String testGroupFolder) throws IOException {
    	WebElement actualWebElement = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='reportContent' or @class='offer-signups' or @class='full']")));
    	File actualDriverScreenshot = ((TakesScreenshot)actualWebElement).getScreenshotAs(OutputType.FILE);

        BufferedImage wholeWindowScreenshot	= ImageIO.read(actualDriverScreenshot);
        //This workaround needs because getScreenshotAs returns screenshot with max width of 32766pix. But height of report web element arep is could be much higher and fails with raster exception
        if(wholeWindowScreenshot.getHeight() > 32000)
        {
            wholeWindowScreenshot = wholeWindowScreenshot.getSubimage(0, 0, wholeWindowScreenshot.getWidth(), 32000);
        }

        //Create an actual screenshot
//        Integer webElementX = actualWebElement.getLocation().getX();
//        Integer webElementY =  actualWebElement.getLocation().getY();
//
//        Integer webElementWidth = actualWebElement.getSize().getWidth();
//        Integer webElementHeight = actualWebElement.getSize().getHeight();
//
//        Integer wholeImageWidth = wholeWindowScreenshot.getWidth();
//        Integer wholeImageHeight= wholeWindowScreenshot.getHeight();

        //If the Image too large then cut
//        if(webElementHeight >= 32000)
//        {
//            webElementHeight = 32000 -  webElementY;
//        }
//        BufferedImage actualImageCropped = wholeWindowScreenshot.getSubimage(webElementX, webElementY, webElementWidth, webElementHeight);
//        BufferedImage actualImageCropped = screenshot.getImage();

        String actualImageUrl = getScreenshotReportPath(testGroupFolder + reportName + "_act.png");;
        String differenceImageUrl = getScreenshotReportPath(testGroupFolder + reportName + "_dif.png");
        CompareUtils.saveImage(wholeWindowScreenshot, actualImageUrl);
        logger.info("Actual image stored at:" + actualImageUrl);
        String expectedImageUrl = getScreenshotReportPath(testGroupFolder + reportName + "_exp.png");
        if(!generateEtalons)
            CompareUtils.compareImages(expectedImageUrl,actualImageUrl,differenceImageUrl);
    }

    public static String copyDownloadedExpectedReportToResources(String newActualFilePath) throws IOException {
        logger.info("Copying last downloaded report to Resources from: " + Core.tempFolderForDownloads );
        String fileExtention = newActualFilePath.substring(newActualFilePath.indexOf(".") + 1);
        String oldActualFilePath = CompareUtils.getLastModifiedFile(Core.tempFolderForDownloads, fileExtention);

        Files.copy(new File(oldActualFilePath).toPath(), new File(newActualFilePath).toPath(), StandardCopyOption.REPLACE_EXISTING);

        return newActualFilePath;
    }

    public static void verifyDownloadedCsv(String reportName, String testGroupFolder) throws IOException, URISyntaxException {
        String actualFilePath = copyDownloadedExpectedReportToResources(getCsvReportPath(testGroupFolder + reportName + "_act.csv"));
        String expectedCsvPath = getCsvReportPath(testGroupFolder + reportName + "_exp.csv");
        if(!generateEtalons)
            compareCsv(expectedCsvPath, actualFilePath);
    }

    public static void compareCsv(String expectedCsvPath, String actualCsvPath) throws IOException {
        CSVReader actualCsvReader = new CSVReader(new FileReader(actualCsvPath));
        java.util.List actualCsvEntries = actualCsvReader.readAll();

        CSVReader expectedCsvReader= new CSVReader(new FileReader(expectedCsvPath));
        java.util.List expectedCsvEntries = expectedCsvReader.readAll();

        Assert.assertArrayEquals("Csv files are not equal! \n\tExpected:" + expectedCsvPath + "\n\tActual:" + actualCsvPath + "\n", expectedCsvEntries.toArray(), actualCsvEntries.toArray());
        logger.info("Csv files are equal.  \n\tExpected:" + expectedCsvPath + "\n\tActual:" + actualCsvPath);
    }

    public static void verifyDownloadedDoc(String reportName, String testGroupFolder) throws URISyntaxException, IOException, SAXException, ParserConfigurationException, XpathException {
        String actualFilePath = copyDownloadedExpectedReportToResources(getDocReportPath(testGroupFolder + reportName + "_act.doc"));
        String expectedFilePath = getDocReportPath(testGroupFolder + reportName + "_exp.doc");
        final java.util.List<String> ignorableXPathsRegex = new ArrayList<String>();
        ignorableXPathsRegex.add("\\/body\\[1\\]\\/div\\[1\\]\\/div\\[1\\]");
        ignorableXPathsRegex.add("\\/head\\[1\\]\\/link\\[1\\]");
        ignorableXPathsRegex.add("\\/html\\[1\\]\\/head\\[1\\]\\/comment\\(\\)\\[1\\]");
        ignorableXPathsRegex.add("\\/html\\[1\\]\\/body\\[1\\]\\/div\\[3\\]");
        if(!generateEtalons)
            compareXml(expectedFilePath, actualFilePath, ignorableXPathsRegex);
    }

    public static void verifyDownloadedXml(String reportName, String testGroupFolder) throws URISyntaxException, IOException, SAXException, ParserConfigurationException, XpathException {
        String actualFilePath = copyDownloadedExpectedReportToResources(getXlsReportPath(testGroupFolder + reportName + "_act.xls"));
        String expectedFilePath = getXlsReportPath(testGroupFolder + reportName + "_exp.xls");
        final java.util.List<String> ignorableXPathsRegex = new ArrayList<String>();
        ignorableXPathsRegex.add("\\/body\\[1\\]\\/div\\[1\\]\\/div\\[1\\]");
        ignorableXPathsRegex.add("\\/head\\[1\\]\\/link\\[1\\]");
        ignorableXPathsRegex.add("\\/html\\[1\\]\\/head\\[1\\]\\/comment\\(\\)\\[1\\]");
        ignorableXPathsRegex.add("\\/html\\[1\\]\\/body\\[1\\]\\/div\\[3\\]");
        ignorableXPathsRegex.add("\\/html\\[1\\]\\/head\\[1\\]\\/title\\[1\\]");
        ignorableXPathsRegex.add("\\/html\\[1\\]\\/body\\[1\\]\\/div\\[1\\]\\/div\\[2\\]");

        if(!generateEtalons)
            compareXml(expectedFilePath, actualFilePath, ignorableXPathsRegex);
    }
    
    public static void verifyDownloadedXls(String reportName, String testGroupFolder) throws URISyntaxException, IOException, SAXException, ParserConfigurationException, XpathException, DatabaseUnitException {
    	verifyDownloadedXlsOrXlsx(reportName, testGroupFolder, "xls");
    }
    
    public static void verifyDownloadedXlsx(String reportName, String testGroupFolder) throws URISyntaxException, IOException, SAXException, ParserConfigurationException, XpathException, DatabaseUnitException {
    	verifyDownloadedXlsOrXlsx(reportName, testGroupFolder, "xlsx");
    }
    
    public static void verifyDownloadedXlsOrXlsx(String reportName, String testGroupFolder, String extention) throws URISyntaxException, IOException, SAXException, ParserConfigurationException, XpathException, DatabaseUnitException {
        String actualFilePath = copyDownloadedExpectedReportToResources(getXlsReportPath(testGroupFolder + reportName + "_act." + extention));
        String expectedFilePath = getXlsReportPath(testGroupFolder + reportName + "_exp." + extention);
        final java.util.List<String> ignorableXPathsRegex = new ArrayList<String>();
        ignorableXPathsRegex.add("\\/body\\[1\\]\\/div\\[1\\]\\/div\\[1\\]");
        ignorableXPathsRegex.add("\\/head\\[1\\]\\/link\\[1\\]");
        ignorableXPathsRegex.add("\\/html\\[1\\]\\/head\\[1\\]\\/comment\\(\\)\\[1\\]");
        ignorableXPathsRegex.add("\\/html\\[1\\]\\/body\\[1\\]\\/div\\[3\\]");
        ignorableXPathsRegex.add("\\/html\\[1\\]\\/head\\[1\\]\\/title\\[1\\]");
        ignorableXPathsRegex.add("\\/html\\[1\\]\\/body\\[1\\]\\/div\\[1\\]\\/div\\[2\\]");

        if(!generateEtalons)
            compareXls(expectedFilePath, actualFilePath, ignorableXPathsRegex);
    }
    
    public static void compareXls(String expectedFilePath, String actualFilePath, java.util.List<String> ignorableXPathsRegex) throws SAXException, IOException, DatabaseUnitException {
        XMLUnit.setIgnoreWhitespace(true);
        XMLUnit.setIgnoreAttributeOrder(true);
        Diff diff;
        FileInputStream expectedFileStream = null;
        FileInputStream actualFileStream = null;

        try
        {
            expectedFileStream = new FileInputStream(expectedFilePath);
            InputSource expectedInputSource = new InputSource(expectedFileStream);
            
            actualFileStream = new FileInputStream(actualFilePath);
            InputSource actualInputSource = new InputSource(actualFileStream);
            
            Assertion.assertEquals(new XlsDataSet(expectedFileStream), new XlsDataSet(actualFileStream));
            
//            Workbook expectedWorkbook = new HSSFWorkbook(expectedFileStream);
//            Workbook actualWorkbook = new HSSFWorkbook(actualFileStream);
//            Assertion.assertEquals(expectedWorkbook, actualWorkbook);
//            expectedInputSource.setEncoding("UTF-8");
//            actualInputSource.setEncoding("UTF-8");
//            diff = new Diff(expectedInputSource, actualInputSource);
//            RegexBasedDifferenceListener ignorableElementsListener = new RegexBasedDifferenceListener(ignorableXPathsRegex);
//            // setting our custom difference listener
//            diff.overrideDifferenceListener(ignorableElementsListener);
//            if (diff.similar()){
//                System.out.println("Both xmls are same: " + diff.toString());
//            } else{
//                Assert.fail("Expected and generated xmls are different: " + diff.toString());
//            }
        }
        finally {
            if (expectedFileStream != null) {
                expectedFileStream.close();
            }
            if (actualFileStream != null) {
                actualFileStream.close();
            }
        }
    }

    public static void compareXml(String expectedFilePath, String actualFilePath, java.util.List<String> ignorableXPathsRegex) throws SAXException, IOException {
        XMLUnit.setIgnoreWhitespace(true);
        XMLUnit.setIgnoreAttributeOrder(true);
        Diff diff;
        FileInputStream expectedFileStream = null;
        FileInputStream actualFileStream = null;

        try
        {
            expectedFileStream = new FileInputStream(expectedFilePath);
            InputSource expectedInputSource = new InputSource(expectedFileStream);
            
            actualFileStream = new FileInputStream(actualFilePath);
            InputSource actualInputSource = new InputSource(actualFileStream);
            
//            expectedInputSource.setEncoding("UTF-8");
//            actualInputSource.setEncoding("UTF-8");
            diff = new Diff(expectedInputSource, actualInputSource);
            RegexBasedDifferenceListener ignorableElementsListener = new RegexBasedDifferenceListener(ignorableXPathsRegex);
            // setting our custom difference listener
            diff.overrideDifferenceListener(ignorableElementsListener);
            if (diff.similar()){
                System.out.println("Both xmls are same: " + diff.toString());
            } else{
                Assert.fail("Expected and generated xmls are different: " + diff.toString());
            }
        }
        finally {
            if (expectedFileStream != null) {
                expectedFileStream.close();
            }
            if (actualFileStream != null) {
                actualFileStream.close();
            }
        }
    }

    public static void verifyDownloadedPdf(String reportName,/* Integer expectedPagesCount,*/ Integer yOffset, String testGroupFolder) throws IOException, URISyntaxException {
        String actualPdfPath = copyDownloadedExpectedReportToResources(getPdfReportPath(testGroupFolder + reportName + "_act.pdf"));

        //Verify pages count
        byte[] actualPdfData = java.nio.file.Files.readAllBytes(new File(actualPdfPath).toPath());
        PDDocument actualPdfDocument = PDDocument.loadNonSeq(new ByteArrayInputStream(actualPdfData), null);

        /*Integer currentPagesCount = actualPdfDocument.getDocumentCatalog().getAllPages().size();
        Assert.assertEquals("Current pages count is incorrect", expectedPagesCount.longValue(), currentPagesCount.longValue());
        logger.info("Page counts are equal.");*/
        //Verify Image of 1-st page of PDF document
        PDPage actualPage = (PDPage) actualPdfDocument.getDocumentCatalog().getAllPages().get(0);

        String actualImagePath = getPdfReportPath(testGroupFolder + reportName + "_act.png");;
        CompareUtils.writePdfPageToImage(actualPage, actualImagePath);

        if(!generateEtalons) {
            String expectedImagePath = getPdfReportPath(testGroupFolder + reportName + "_exp.png");
            String differenceImageUrl = getPdfReportPath(testGroupFolder + reportName + "_dif.png");
            CompareUtils.compareImages(expectedImagePath, actualImagePath, differenceImageUrl, yOffset);
        }
    }

    public static String getScreenshotReportPath(String fileFullName)
    {
        return getResoucePath("reports/" + Core.getEnvironment() + "/screenshots/") + fileFullName;
    }

    private static String getPdfReportPath(String fileFullName)
    {
        return getResoucePath("reports/" + Core.getEnvironment() + "/pdf/") + fileFullName;
    }

    private static String getDocReportPath(String fileFullName)
    {
        return getResoucePath("reports/" + Core.getEnvironment() + "/doc/") + fileFullName;
    }

    private static String getXlsReportPath(String fileFullName)
    {
        return getResoucePath("reports/" + Core.getEnvironment() + "/xls/") + fileFullName;
    }

    public static String getCsvReportPath(String fileFullName)
    {
        return getResoucePath(getCsvReportsFolderName()) + fileFullName;
    }

    public static String getCsvReportsFolderName()
    {
        return "reports/" + Core.getEnvironment() + "/csv/";
    }

    public static long getFileSize(String filePath) {
        File file = new File(filePath);
        return file.length();
    }
}
