package pages.Dms.EPortManager;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import helper.Core;
import pages.Dms.Devices.DeviceListPage;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class BankAccountsPage extends Core{
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(DeviceListPage.class);

    public BankAccountsPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

	public void verifyCustomerPresence(String customerName) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//tbody/tr/td[count(//a[text()='Customer']/../preceding-sibling::*)+1 and contains(text(), '" + customerName + "')]")));		
	}
}
