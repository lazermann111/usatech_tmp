package pages.Dms.Consumers;

import helper.Core;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ConsumerAccountPage extends Core{
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(ConsumerAccountPage.class);

    public ConsumerAccountPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 1800);//30 minutes wait
    }

    public void verifyAccountID(String value) {
        verifyValue("Account ID", value);
    }

    public void verifyCardNumber(String value) {
        verifyValue("Card Number", value);
    }

    public void verifyCardID(String value) {
        verifyValue("Card ID", value);
    }


    public void verifyConsumerID(String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Consumer ID']/following-sibling::td[1]//a[text()='" + value + "']")));
    }

    public void verifyConsumerAccountType(String value) {
        verifyValue("Consumer Account Type", value);
    }

    private void verifyValue(String labelText, String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='" + labelText + "']/following-sibling::td[1][text()='" + value + "']")));
    }
}
