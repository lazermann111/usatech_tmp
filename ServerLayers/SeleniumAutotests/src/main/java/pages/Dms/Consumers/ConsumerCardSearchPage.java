package pages.Dms.Consumers;

import helper.Core;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ConsumerCardSearchPage extends Core{
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(ConsumerCardSearchPage.class);

    public ConsumerCardSearchPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    public void setInputValue(String id, String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='" + id + "']"))).clear();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='" + id + "']"))).sendKeys(value);
    }

    public void setCardNumber(String value) {
        setInputValue("consumer_card_number", value);
    }

    public void setAccountNumber(String value) {
        setInputValue("account_number", value);
    }

    public void setStartCardID(String value) {
        setInputValue("start_card_id", value);
    }

    public void setEndCardID(String value) {
        setInputValue("end_card_id", value);
    }

    public void setFirstName(String value) {
        setInputValue("consumer_first_name", value);
    }


    public void setSelectValue(String id, String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='" + id + "']/option[text()='" + value + "']"))).click();
    }

    public void setConsumerMerchant(String value) {
        setSelectValue("consumer_merchant_id", value);
    }

    public void setConsumerAccountType(String value) {
        setSelectValue("consumer_acct_type_id", value);
    }

    public void setConsumerAccountSubtype(String value) {
        setSelectValue("consumer_acct_sub_type_id", value);
    }


    public void verifySearchResultsConsumerID(String value) {
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table[@class='tabDataDisplayBorder']//*[text()='Consumer ID']/following-sibling::*[text()='" + value + "']")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table[@class='tabDataDisplayBorder']//a[text()='Consumer ID']/../../..//td[1]/a[text()='" + value + "']")));
    }

    public void verifySearchResultsFirstName(String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table[@class='tabDataDisplayBorder']//a[text()='First Name']/../../..//td[2][text()='" + value + "']")));
    }

    public void verifySearchResultsLastName(String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table[@class='tabDataDisplayBorder']//a[text()='Last Name']/../../..//td[3][text()='" + value + "']")));
    }

    public void verifySearchResultsEmail(String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table[@class='tabDataDisplayBorder']//a[text()='Email']/../../..//td[4][contains(text(),'" + value + "')]")));
    }

    public void clickSearchButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//form[@id='searchConsumer']//input[@value='Search']"))).click();
    }

    public void clickFirstConsumerId() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//form[@id='searchConsumer']//a[text()='Consumer ID']/../../following-sibling::tr[1]/td[1]/a"))).click();
    }

    public void clickCardCode(String modifiedCardNumber) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='Consumer Accounts (Cards)']/following-sibling::table[1]//tr/td[1]/a[text()='" + modifiedCardNumber + "']"))).click();
    }

    public void verifyConsumerInfoConsumerId(String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Consumer ID']/following-sibling::td[1][text()='" + value + "']")));
    }

    public void verifyConsumerInfoFirstName(String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='First Name']/following-sibling::td[1][text()='" + value + "']")));
    }

    public void verifyConsumerInfoLastName(String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Last Name']/following-sibling::td[1][text()='" + value + "']")));
    }

    public void verifyConsumerPrimaryEmail(String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Primary Email']/following-sibling::td[1][text()='" + value + "']")));
    }
}
