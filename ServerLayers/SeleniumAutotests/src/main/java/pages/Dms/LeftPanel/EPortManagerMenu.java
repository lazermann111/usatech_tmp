package pages.Dms.LeftPanel;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EPortManagerMenu {
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(EPortManagerMenu.class);

    public EPortManagerMenu(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    public void searchBankAccountByCustomerName(String customerName) throws InterruptedException {    	
//    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='tooltip_mm_eport_manager']"))).click();
//    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[text()='Bank Accounts']"))).click();
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='tooltip_sm_bank_search_customer_name']"))).sendKeys(customerName);  
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='tooltip_sm_bank_search_customer_name']/../input[@type='submit']"))).click();
	
	Actions builder = new Actions(driver); 
	builder.moveToElement(driver.findElement(By.xpath("//*[@id='tooltip_mm_eport_manager']")));
	builder.moveToElement(driver.findElement(By.xpath("//li[text()='ePort Manager']")));
	builder.moveToElement(driver.findElement(By.xpath("//a[text()='Bank Accounts']")));
	builder.moveToElement(driver.findElement(By.xpath("//a[text()='Bank Accounts']/..//li[text()='Customer Name']")));
	builder.moveToElement(driver.findElement(By.xpath("//input[@id='tooltip_sm_bank_search_customer_name']")));
	builder.perform();
	wait.until(ExpectedConditions
	.visibilityOfElementLocated(By.xpath("//input[@id='tooltip_sm_bank_search_customer_name']")))
	.sendKeys(customerName);
	builder.moveToElement(driver.findElement(By.xpath("//input[@id='tooltip_sm_bank_search_customer_name']/../input[@type='submit']"))).click().perform();
	Thread.sleep(5000);

    }//a[text()='Bank Accounts']/..//li[text()='Customer Name']

}
