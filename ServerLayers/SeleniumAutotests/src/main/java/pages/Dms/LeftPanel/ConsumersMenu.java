package pages.Dms.LeftPanel;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.Select;

public class ConsumersMenu {
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(ConsumersMenu.class);

    public ConsumersMenu(WebDriver driver) {
	this.driver = driver;
	wait = new WebDriverWait(driver, 30);
    }

    public WebElement getConsumerMenuItem() {
	return wait.until(ExpectedConditions
		.presenceOfElementLocated(By.xpath("//ul[@id='verticalmenu']//a[text()='Consumers']")));
    }

    public void consumerSearch() {
//	WebElement customersMenuItem = wait.until(ExpectedConditions
//		.presenceOfElementLocated(By.xpath("//ul[@id='verticalmenu']//a[text()='Consumers']")));
//	new Actions(driver).moveToElement(customersMenuItem).perform(); // Gecko
//									// does
//									// not
//									// cannot
//									// work
//									// with
//									// it
//	// ((JavascriptExecutor)
//	// driver).executeScript("arguments[0].scrollIntoView(true);",
//	// getConsumerMenuItem());
//	getConsumerMenuItem().click();
//	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@id='tooltip_mm_consumers2']"))).click();
	Actions builder = new Actions(driver); 
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='Consumers']")));
	builder.moveToElement(driver.findElement(By.xpath("//li[text()='Consumers']")));
	builder.moveToElement(driver.findElement(By.xpath("//a[@id='tooltip_mm_consumers2']")));
	builder.click().perform();
    }

    public void consumerCardSearch() {
//	WebElement customersMenuItem = wait.until(ExpectedConditions
//		.presenceOfElementLocated(By.xpath("//ul[@id='verticalmenu']//a[text()='Consumers']")));
	Actions builder = new Actions(driver); // Gecko does not cannot work
					       // with it
					       // //.moveToElement(customersMenuItem).perform()
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='Consumers']")));
	builder.moveToElement(driver.findElement(By.xpath("//li[text()='Consumers']")));
	builder.moveToElement(driver.findElement(By.xpath("//a[@id='tooltip_mm_cards']")));
	builder.click().perform();
	// ((JavascriptExecutor)
	// driver).executeScript("arguments[0].scrollIntoView(true);",
	// getConsumerMenuItem());
	// getConsumerMenuItem().click();
	// wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@id='tooltip_mm_cards']"))).click();
	// driver.findElement(By.id("tooltip_mm_cards")).click();
    }
}
