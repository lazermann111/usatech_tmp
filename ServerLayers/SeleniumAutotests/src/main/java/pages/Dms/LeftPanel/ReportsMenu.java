package pages.Dms.LeftPanel;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ReportsMenu {
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(ReportsMenu.class);

    public ReportsMenu(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    public void open() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@id='tooltip_mm_reports']"))).click();
    }

}
