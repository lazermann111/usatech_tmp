package pages.Dms.LeftPanel;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FilesMenu {
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(FilesMenu.class);

    public FilesMenu(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }
    private WebElement getMenuItemByText(String linkText) {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//ul[@id='verticalmenu']//a[text()='" + linkText + "']")));
    }

    public void clickMenuItemByText(String linkText) {
        getMenuItemByText(linkText).click();
    }

    public void viewByType(String deviceType) {
//        getMenuItemByText("Files").click();
	Actions builder = new Actions(driver);
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='Files']")));
	builder.moveToElement(driver.findElement(By.xpath("//li[text()='File Name']")));	
	builder.click().perform();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='verticalmenu']//li[text()='View Files']/..//a[text()='by Type']"))).click();
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//li[text()='View Files']/..//a[text()='by Type']")));
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//li[text()='File Type']")));	
	builder.perform();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='verticalmenu']//li[text()='File Type']/..//a[text()='" + deviceType + "']"))).click();

        ////ul[@id='verticalmenu']//li[text()='File Type']
    }

    public void searchByName(String value) {
//        new Actions(driver).moveToElement(getMenuItemByText("Files")).perform();
	Actions builder = new Actions(driver);
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='Files']")));
	builder.moveToElement(driver.findElement(By.xpath("//li[text()='File Name']")));	
	builder.click().perform();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='tooltip_sm_files_search_file_name']"))).sendKeys(value);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='tooltip_sm_files_search_file_name']/following-sibling::input[1]"))).click();
    }

    public void searchById(String value) {
//        new Actions(driver).moveToElement(getMenuItemByText("Files")).perform();
	Actions builder = new Actions(driver);
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='Files']")));
	builder.moveToElement(driver.findElement(By.xpath("//li[text()='File Name']")));	
	builder.click().perform();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='tooltip_sm_files_search_file_id']"))).sendKeys(value);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='tooltip_sm_files_search_file_id']/following-sibling::input[1]"))).click();
    }

    public void uploadFileToServer() {
//        new Actions(driver).moveToElement(getMenuItemByText("Files")).perform();
	Actions builder = new Actions(driver);
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='Files']")));
	builder.moveToElement(driver.findElement(By.xpath("//li[text()='File Name']")));	
	builder.click().perform();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//li[text()='Upload File to Server']/..//input[@value='Next'])[1]"))).click();
    }
}
