package pages.Dms.LeftPanel;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DevicesMenu {
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(DevicesMenu.class);

    public DevicesMenu(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }
    private WebElement getMenuItemByText(String linkText) {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//ul[@id='verticalmenu']//a[text()='" + linkText + "']")));
    }

    public void clickMenuItemByText(String linkText) {
        getMenuItemByText(linkText).click();
    }

    public void searchDevice(String textToSearch) {
//        new Actions(driver).moveToElement(getMenuItemByText("Devices")).perform();
//    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='tooltip_sm_device_search']"))).sendKeys(textToSearch);
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[text()='Device Search']/..//input[@value='Search']"))).click();
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@class='shadeTabs']//a[text()='Device Profile']/../../..//div[@class='headerTab']//td[contains(text(), '" + textToSearch + "')]")));
    	Actions builder = new Actions(driver);
        builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='Devices']")));
        builder.moveToElement(driver.findElement(By.xpath("//input[@id='tooltip_sm_device_search']"))).perform();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='tooltip_sm_device_search']"))).sendKeys(textToSearch);
        builder.moveToElement(driver.findElement(By.xpath("//li[text()='Device Search']/..//input[@value='Search']"))).click().perform();        
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@class='shadeTabs']//a[text()='Device Profile']/../../..//div[@class='headerTab']//td[contains(text(), '" + textToSearch + "')]")));
    }

    public void viewByType(String deviceType) {
//        getMenuItemByText("Devices").click();
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='verticalmenu']//li[text()='View Devices']/..//a[text()='by Type']"))).click();
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='verticalmenu']//li[text()='Device Type']/..//a[text()='" + deviceType + "']"))).click();
	Actions builder = new Actions(driver);	
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='Devices']")));
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//select[@id='tooltip_sm_device_search_type']")));
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='by Type']")));
	builder.moveToElement(driver.findElement(By.xpath("//li[text()='Device Type']")));
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//li[text()='Device Type']/..//a[text()='" + deviceType + "']")));
	builder.click();
	builder.perform();
    }

    public void viewByCommMethod(String commMethod) throws InterruptedException {
//        getMenuItemByText("Devices").click();
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='verticalmenu']//li[text()='View Devices']/..//a[text()='by Comm Method']"))).click();
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='verticalmenu']//li[text()='Comm Method']/..//a[text()='" + commMethod + "']"))).click();

	Actions builder = new Actions(driver);	
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='Devices']")));
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//select[@id='tooltip_sm_device_search_type']")));
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='by Comm Method']")));
	builder.moveToElement(driver.findElement(By.xpath("//li[text()='Comm Method']")));
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//li[text()='Comm Method']/..//a[text()='" + commMethod + "']")));
	builder.click();
	builder.perform();
	Thread.sleep(1000);
    }

    public void viewByCustomer(String groupName, String customerName) throws InterruptedException {
//        getMenuItemByText("Devices").click();
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='verticalmenu']//li[text()='View Devices']/..//a[text()='by DMS Customer']"))).click();
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='menu_devices_by_customer']//a[text()='" + groupName + "']"))).click();
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='menu_devices_by_customer']//li[text()='" + groupName + "']/..//a[contains(text(),'" + customerName + "')]"))).click();
	Actions builder = new Actions(driver);	
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='Devices']")));
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//select[@id='tooltip_sm_device_search_type']")));
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='by DMS Customer']")));
	builder.click();
	builder.perform();
	Thread.sleep(5000);
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='Devices']")));
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//select[@id='tooltip_sm_device_search_type']")));
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='by DMS Customer']")));
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='menu_devices_by_customer']/li/a[text()='I']")));
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='menu_devices_by_customer']//a[contains(text(),'" + groupName + "')]")));
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='menu_devices_by_customer']//li[text()='" + groupName + "']/..//a[contains(text(),'" + customerName + "')]")));
	builder.click();
	builder.perform();

    }

//    public void verifyCustomerMenuItem(String groupName, String customerName) {
//        getMenuItemByText("Devices").click();
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='verticalmenu']//li[text()='View Devices']/..//a[text()='by Customer']"))).click();
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='menu_devices_by_customer']//a[text()='" + groupName + "']"))).click();
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='menu_devices_by_customer']//li[text()='" + groupName + "']/..//a[text()='" + customerName + "']")));
//    }

    public void viewByCustomer() throws InterruptedException {
//        getMenuItemByText("Devices").click();
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='verticalmenu']//li[text()='View Devices']/..//a[text()='by DMS Customer']"))).click();
	Actions builder = new Actions(driver);	
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='Devices']")));
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//select[@id='tooltip_sm_device_search_type']")));
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='by DMS Customer']")));
	builder.click();
	builder.perform();
	Thread.sleep(5000);
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='Devices']")));
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//select[@id='tooltip_sm_device_search_type']")));
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='by DMS Customer']")));
//	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='menu_devices_by_location']/li/a[text()='I']")));
//	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='menu_devices_by_location']//a[contains(text(),'" + groupName + "')]")));
//	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='menu_devices_by_location']//li[text()='" + groupName + "']/..//a[contains(text(),'" + location + "')]")));
//	builder.click();
//	builder.perform();

    }

    public void viewByLocation() {
//        getMenuItemByText("Devices").click();
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='verticalmenu']//li[text()='View Devices']/..//a[text()='by DMS Location']"))).click();
	Actions builder = new Actions(driver);	
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='Devices']")));
	builder.moveToElement(driver.findElement(By.xpath("//select[@id='tooltip_sm_device_search_type']")));
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//li[text()='View Devices']/..//a[text()='by DMS Location']")));
	builder.click().perform();
    }

    public void viewByLocation(String groupName, String location) throws InterruptedException {
//        getMenuItemByText("Devices").click();
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='verticalmenu']//li[text()='View Devices']/..//a[text()='by DMS Location']"))).click();
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='menu_devices_by_location']//a[contains(text(),'" + groupName + "')]"))).click();
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='menu_devices_by_location']//li[text()='" + groupName + "']/..//a[contains(text(),'" + location + "')]"))).click();
//	Thread.sleep(5000);
	Actions builder = new Actions(driver);	
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='Devices']")));
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//select[@id='tooltip_sm_device_search_type']")));
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='by DMS Location']")));
	builder.click();
	builder.perform();
	Thread.sleep(5000);
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='Devices']")));
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//select[@id='tooltip_sm_device_search_type']")));
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='by DMS Location']")));
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='menu_devices_by_location']/li/a[text()='J']")));
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='menu_devices_by_location']//a[contains(text(),'" + groupName + "')]")));
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='menu_devices_by_location']//li[text()='" + groupName + "']/..//a[contains(text(),'" + location + "')]")));
	builder.click();
	builder.perform();

    }

    public void viewByFirmwareVersion() {
        getMenuItemByText("Devices").click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='verticalmenu']//li[text()='View Devices']/..//a[text()='by Firmware Version']"))).click();
    }

    public void viewByFirmwareVersion(String groupName, String firmwareVersionPartialName) throws InterruptedException {
//        getMenuItemByText("Devices").click();
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='verticalmenu']//li[text()='View Devices']/..//a[text()='by Firmware Version']"))).click();
//
//        new Actions(driver).moveToElement(wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//ul[@id='verticalmenu']//li[text()='Device Type']/..//a[text()='" + groupName + "'])[2]")))).perform();
//
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='verticalmenu']//li[text()='Device Type']/..//a[contains(text(),'" + firmwareVersionPartialName + "')]"))).click();
        
	Actions builder = new Actions(driver);	
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='Devices']")));
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//select[@id='tooltip_sm_device_search_type']")));
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='by Firmware Version']")));
	builder.click();
	builder.perform();
	Thread.sleep(4000);
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='Devices']")));
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//select[@id='tooltip_sm_device_search_type']")));
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='by Firmware Version']")));
	builder.moveToElement(driver.findElement(By.xpath("//a[text()='by Firmware Version']/..//li[text()='Device Type']"))); ////a[text()='by Firmware Version']//..//li/a[text()='ePort Edge/G9']
	builder.moveToElement(driver.findElement(By.xpath("//a[text()='by Firmware Version']//..//li/a[text()='" + groupName + "']")));////a[text()='ePort Edge/G9']/../ul/li[text()='Firmware Version']
	builder.moveToElement(driver.findElement(By.xpath("//a[text()='ePort Edge/G9']/../ul/li[text()='Firmware Version']")));
	builder.moveToElement(driver.findElement(By.xpath("//a[text()='by Firmware Version']//..//li/a[text()='" + groupName + "']/..//a[contains(text(),'" + firmwareVersionPartialName + "')]")));
	builder.click();
	builder.perform();
    }

//    public void searchBySerialNumber(String searchValue) {
//        searchDeviceByTypeAndValue("Serial Number", searchValue);
//    }
//
    public void searchDeviceByTypeAndValue(String searchType, String searchValue) {
    	new Actions(driver).moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='Devices']"))).perform();

    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='tooltip_sm_device_search_type']/option[text()='" + searchType + "']"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='tooltip_sm_device_search']"))).sendKeys(searchValue);
        
        // need to fix issue when hint to "Device" menu hides the "Search" button
        new Actions(driver).moveToElement(driver.findElement(By.xpath("//li[text()='Device Search']/..//input[@value='Search']"))).perform();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[text()='Device Search']/..//input[@value='Search']"))).click();
        }

    public void clickIncludeDisabled() {
        new Actions(driver).moveToElement(getMenuItemByText("Devices")).perform();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='tooltip_sm_device_search_checkbox']"))).click();

    }
    
    public void openDeviceConfigurationWizard() {
//        getMenuItemByText("Devices").click();
	Actions builder = new Actions(driver);
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='Devices']")));
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//select[@id='tooltip_sm_device_search_type']")));	
	builder.click().perform();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@id='tooltip_sm_devices_device_configuration_wizard']"))).click();
    }
    
	public void openDeviceMessages() {
//	      getMenuItemByText("Devices").click();
		Actions builder = new Actions(driver);
		builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='Devices']")));
		builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//select[@id='tooltip_sm_device_search_type']")));	
		builder.click().perform();
	      wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@id='tooltip_sm_devices_device_messages']"))).click();		
	}
}
