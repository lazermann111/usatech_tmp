package pages.Dms.LeftPanel;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CustomersMenu {
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(CustomersMenu.class);
    
    @FindBy(xpath="//ul[@id='verticalmenu']//a[text()='DMS Customers']/..")
    private static WebElement DMSCustomer;
    
    @FindBy(xpath="//li[text()='DMS Customer Name']")
    private static WebElement DMSCustomerName;

    
    @FindBy(xpath="//input[@id='tooltip_sm_customers_search_customer_name']")
    private static WebElement searchCustomerField;
    
    
    @FindBy(xpath="//input[@id='tooltip_sm_customers_search_customer_name']/../input[@type='submit']")
    private static WebElement submitCustomerSearch; 
 

    public CustomersMenu(WebDriver driver) {
	PageFactory.initElements(driver, this);
	this.driver = driver;
	wait = new WebDriverWait(driver, 30);
	
    }

    public void searchCustomer(String textToSearch) throws InterruptedException {
//	WebElement customersMenuItem = wait.until(ExpectedConditions
//		.presenceOfElementLocated(By.xpath("//ul[@id='verticalmenu']//a[text()='DMS Customers']")));
//	new Actions(driver).moveToElement(customersMenuItem).perform();
//	Thread.sleep(1000);
//	wait.until(ExpectedConditions
//		.visibilityOfElementLocated(By.xpath("//input[@id='tooltip_sm_customers_search_customer_name']")))
//		.sendKeys(textToSearch);
//	wait.until(ExpectedConditions.visibilityOfElementLocated(
//		By.xpath("//input[@id='tooltip_sm_customers_search_customer_name']/../input[@type='submit']"))).click();
//	// wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='tabHeadTxt'and
//	// text()='Customer List']")));
	
//	wait.withTimeout(180,  TimeUnit.SECONDS).until(ExpectedConditions
//		.visibilityOfElementLocated(By.xpath("//ul[@id='verticalmenu']//a[text()='DMS Customers']")))
//		.click();
//	wait.withTimeout(180,  TimeUnit.SECONDS).until(ExpectedConditions
//		.visibilityOfElementLocated(By.xpath("//input[@id='tooltip_sm_customers_search_customer_name']")))
//		.click();
    	
    	
//    	Actions builder = new Actions(driver); // Gecko does not cannot work
    	// with it
    	// //.moveToElement(customersMenuItem).perform()
    	//
//    	wait.withTimeout(30,  TimeUnit.SECONDS).until(ExpectedConditions
//        		.visibilityOfElementLocated(By.xpath("//input[@id='tooltip_sm_customers_search_customer_name']"))).click();
//    	DMSCustomer.click();
    	
//    	wait.withTimeout(10,  TimeUnit.SECONDS).until(ExpectedConditions
//    		.visibilityOfElementLocated(By.xpath("//input[@id='tooltip_sm_customers_search_customer_name']")))
//    		.sendKeys(textToSearch);
//    	wait.withTimeout(10,  TimeUnit.SECONDS).until(ExpectedConditions.visibilityOfElementLocated(
//    		By.xpath("//input[@id='tooltip_sm_customers_search_customer_name']/../input[@type='submit']"))).click();
    	
    	
//    	Actions builder = new Actions(driver);
//        builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='DMS Customers']")));
    	
    	Actions builder = new Actions(driver); // Gecko does not cannot work
    	// with it
    	// //.moveToElement(customersMenuItem).perform()
    	builder.moveToElement(DMSCustomer).click().perform();
//        DMSCustomer.click();
//    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='verticalmenu']//a[text()='DMS Customers']"))).click();
        Thread.sleep(2000);
//        DMSCustomer.click();
//    	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='DMS Customers']")));
//        DMSCustomerName.click();
//        Thread.sleep(2000);
//    	builder.moveToElement(driver.findElement(By.xpath("//li[text()='DMS Customer Name']")));
    	
        searchCustomerField.click();
//        builder.moveToElement(driver.findElement(By.xpath("//input[@id='tooltip_sm_customers_search_customer_name']")));
        Thread.sleep(2000);
        
        searchCustomerField.sendKeys(textToSearch);
        Thread.sleep(2000);
        submitCustomerSearch.click();
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='tooltip_sm_customers_search_customer_name']\""))).sendKeys(textToSearch);
//        builder.moveToElement(driver.findElement(By.xpath("//input[@id='tooltip_sm_customers_search_customer_name']/../input[@type='submit']"))).click().perform();        
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@class='shadeTabs']//a[text()='Device Profile']/../../..//div[@class='headerTab']//td[contains(text(), '" + textToSearch + "')]")));
    	
    	
    	
    	
//	Actions builder = new Actions(driver); 
//	//builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='DMS Customers']"))).perform();
//
//	builder.moveToElement(DMSCustomer);
//	DMSCustomer.click();
//	builder.moveToElement(driver.findElement(By.xpath("//input[@id='tooltip_sm_customers_search_customer_name']")));
//	searchCustomerField.sendKeys(textToSearch);
//	submitCustomerSearch.click();
//	builder.moveToElement(searchCustpmerField).perform();
//	builder.moveToElement(driver.findElement(By.xpath("//input[@id='tooltip_sm_customers_search_customer_name']")));
//	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='tooltip_sm_customers_search_customer_name']"))).sendKeys(textToSearch);
//	builder.moveToElement(driver.findElement(By.xpath("//input[@id='tooltip_sm_customers_search_customer_name']/../input[@type='submit']")));
//	builder.click().perform();
    }
    

    public void viewByType(String customerType) throws InterruptedException {
	// wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//ul[@id='verticalmenu']//a[text()='DMS
	// Customers']"))).click();
	// wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='verticalmenu']//li[text()='View
	// DMS Customers']/..//a[text()='by Type']"))).click();
	// wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='verticalmenu']//li[text()='DMS
	// Customer Type']/..//a[text()='" + customerType + "']"))).click();

	Actions builder = new Actions(driver); // Gecko does not cannot work
	// with it
	// //.moveToElement(customersMenuItem).perform()
	builder.moveToElement(driver.findElement(By.xpath("//ul[@id='verticalmenu']//a[text()='DMS Customers']")));
	builder.moveToElement(driver.findElement(By.xpath("//li[text()='DMS Customer Name']")));
	builder.moveToElement(driver.findElement(
		By.xpath("//ul[@id='verticalmenu']//li[text()='View DMS Customers']/..//a[text()='by Type']")));
	builder.moveToElement(driver.findElement(By.xpath(
		"//ul[@id='verticalmenu']//li[text()='DMS Customer Type']/..//a[text()='" + customerType + "']")));
	builder.click().perform();
    }

    public void newCustomerName(String name) {
//	WebElement customersMenuItem = wait.withTimeout(180,  TimeUnit.SECONDS).until(ExpectedConditions
//		.presenceOfElementLocated(By.xpath("//ul[@id='verticalmenu']//a[text()='DMS Customers']")));
	Actions builder = new Actions(driver); // Gecko does not cannot work
	// with it
	// //.moveToElement(customersMenuItem).perform()
	builder.moveToElement(DMSCustomer).click().perform();
	
	wait.withTimeout(180,  TimeUnit.SECONDS).until(ExpectedConditions
		.visibilityOfElementLocated(By.xpath("//input[@id='tooltip_sm_customers_new_customer']")))
		.sendKeys(name);
	wait.withTimeout(180,  TimeUnit.SECONDS).until(ExpectedConditions.visibilityOfElementLocated(
		By.xpath("//input[@id='tooltip_sm_customers_new_customer']/../input[@type='submit']"))).click();

    }
}
