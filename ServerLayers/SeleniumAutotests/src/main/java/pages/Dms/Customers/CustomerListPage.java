package pages.Dms.Customers;

import helper.Core;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Arrays;
import java.util.List;

public class CustomerListPage extends Core{
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(CustomerListPage.class);

    public CustomerListPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    public void verifyCustomerNameFiltration(String deviceType) {
        verifyFiltrationByColumnId(deviceType, 1);
    }


    public void verifyColumnsHeaders() throws InterruptedException {
        List<String> expectedHeaders = Arrays.asList("Name", "City", "Country", "Type");
        Thread.sleep(1000);
        List<WebElement> actualHeaders = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//table[@class='tabDataDisplayBorderNoFixedLayout']/thead//td/a")));
        for (WebElement actualHeader:actualHeaders) {
            Assert.assertTrue("Unexpected header:" + actualHeader.getText(), expectedHeaders.contains(actualHeader.getText()));
        }
    }



    public void verifyFiltrationByColumnId(String expectedValueFilteredBy, Integer filteredColumnId) {
        List<WebElement> deviceTypeCells = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//table[@class='tabDataDisplayBorderNoFixedLayout']/tbody/tr/td[" + filteredColumnId + "]/a")));
        for (WebElement deviceCell:deviceTypeCells) {
            Assert.assertTrue("Expected text is not contained in results! Expected=" + expectedValueFilteredBy + ", current=" + deviceCell.getText() + ".", deviceCell.getText().contains(expectedValueFilteredBy));
        }
    }

    public void verifyCustomerTypeFiltration(String customerType) {
        List<WebElement> deviceTypeCells = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//table[@class='tabDataDisplayBorderNoFixedLayout']/tbody/tr/td[" + 4 + "]")));
        for (WebElement deviceCell:deviceTypeCells) {
            Assert.assertTrue("Expected text is not contained in results! Expected=" + customerType + ", current=" + deviceCell.getText() + ".", deviceCell.getText().contains(customerType));
        }
    }
}
