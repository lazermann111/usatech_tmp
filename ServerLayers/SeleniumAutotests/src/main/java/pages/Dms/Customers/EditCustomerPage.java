package pages.Dms.Customers;

import helper.Core;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Arrays;
import java.util.List;

public class EditCustomerPage extends Core{
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(EditCustomerPage.class);

    public EditCustomerPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    public void verifyName(String name) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='customer_name' and @value='" + name + "']")));
    }

    public void save() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='Save']"))).click();
    }

    public void delete() throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='Delete']"))).click();
        Thread.sleep(3000);
        Alert alert = driver.switchTo().alert();
        alert.accept();
        Thread.sleep(3000);
    }

    public void undelete() throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='Undelete']"))).click();
        Thread.sleep(3000);
        Alert alert = driver.switchTo().alert();
        alert.accept();
        Thread.sleep(3000);
    }


    public void saveNew() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='Save New']"))).click();
//        Alert alert = driver.switchTo().alert();
//        alert.accept();
    }

    public void setType(String type) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@name='customer_type_id']/option[contains(text(),'" + type + "')]"))).click();
    }


    public void setState(String state) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@id='state']/option[contains(text(),'" + state + "')]"))).click();
    }

    public void setAddressLine1(String value) throws InterruptedException {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='customer_addr1']"))).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='customer_addr1']"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
        Thread.sleep(1000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='customer_addr1']"))).sendKeys(Keys.DELETE);
        Thread.sleep(1000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='customer_addr1']"))).sendKeys(value);
    }

    public void setAddressLine2(String value) throws InterruptedException {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='customer_addr2']"))).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='customer_addr2']"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
        Thread.sleep(1000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='customer_addr2']"))).sendKeys(Keys.DELETE);
        Thread.sleep(1000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='customer_addr2']"))).sendKeys(value);
    }

    public void setPostalCode(String value) throws InterruptedException {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='customer_postal_cd']"))).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='customer_postal_cd']"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
        Thread.sleep(1000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='customer_postal_cd']"))).sendKeys(Keys.DELETE);
        Thread.sleep(1000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='customer_postal_cd']"))).sendKeys(value);
    }

    public void setCity(String value) throws InterruptedException {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='customer_city']"))).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='customer_city']"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
        Thread.sleep(1000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='customer_city']"))).sendKeys(Keys.DELETE);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='customer_city']"))).clear();
        Thread.sleep(1000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='customer_city']"))).sendKeys(value);
    }

    public void setUserName(String value) throws InterruptedException {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='app_user_name']"))).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='app_user_name']"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
        Thread.sleep(1000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='app_user_name']"))).sendKeys(Keys.DELETE);
        Thread.sleep(1000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='app_user_name']"))).sendKeys(value);
    }

    public void setEmailAddress(String value) throws InterruptedException {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='email_addr']"))).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='email_addr']"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
        Thread.sleep(1000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='email_addr']"))).sendKeys(Keys.DELETE);
        Thread.sleep(1000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='email_addr']"))).sendKeys(value);
    }

    public void verifyType(String value) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@name='customer_type_id']/option[@selected and text()='" + value + "']")));
    }

    public void verifyAddressLine1(String value) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='customer_addr1' and @value='" + value + "']")));
    }

    public void verifyAddressLine2(String value) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='customer_addr2' and @value='" + value + "']")));
    }

    public void verifyPostalCode(String value) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='customer_postal_cd' and @value='" + value + "']")));
    }

    public void verifyCity(String value) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='customer_city' and @value='" + value + "']")));
    }

    public void verifyState(String value) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@name='customer_state_cd']/option[@selected and text()='" + value + "']")));
    }

    public void verifyUserName(String value) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='customer_city' and @value='" + value + "']")));
    }
}
