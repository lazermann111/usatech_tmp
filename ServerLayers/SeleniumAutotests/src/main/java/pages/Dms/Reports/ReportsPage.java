package pages.Dms.Reports;

import helper.Core;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ReportsPage extends Core{
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(ReportsPage.class);

    public ReportsPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    public void clickViewDeviceCountDetailsButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Device Count Details']/following-sibling::td[2]/input"))).click();
    }

    public void setDeviceCountDetailsDateFrom(String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='device_count_from_date']"))).clear();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='device_count_from_date']"))).sendKeys(value);
    }

    public void setDeviceCountDetailsDateTo(String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='device_count_to_date']"))).clear();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='device_count_to_date']"))).sendKeys(value);
    }

    public void setPostalCode(String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//textarea[@id='postalCodes']"))).clear();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//textarea[@id='postalCodes']"))).sendKeys(value);
    }

    public void clickViewDevicesByPostalCodeButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Customer Devices by Postal Code']/..//input[@type='submit']"))).click();
    }

    public void clickViewDeviceCountButton() {
        clickViewReportByName("Device Count");
    }

    public void clickViewESudsDeviceCountButton() {
        clickViewReportByName("eSuds Device Count");
    }

    public void clickViewESudsSchoolsButton() {
        clickViewReportByName("eSuds Schools");
    }

    public void clickViewReportByName(String reportName) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='" + reportName + "']/following-sibling::td[2]/input"))).click();
        try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void clickViewActiveDeviceCountButton() {
//        clickViewReportByName("Active Device Count");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Active Device Count']/following-sibling::td[2]/input"))).click();
    }


    public void clickViewDeviceCountByStateButton() {
        clickViewReportByName("Device Count by State");
    }

    public void verifyGPRSDeviceRSSILogSerialNumber(String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[contains(text(),'" + value + "')]")));
    }

    public void clickViewGPRSDeviceRSSILogButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='GPRS Device RSSI Log']/following-sibling::td[2]/input"))).click();
    }

    public void clickViewNetworkDevicesButton() {
        clickViewReportByName("Network Devices");
    }

    public void clickViewFirmwareUpgradeStatusButton() {
        clickViewReportByName("Firmware Upgrade Status");
    }

    public void verifyActiveDevicesTextVisibility() {
        driver.switchTo().frame(0);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'Active Devices Since')]")));
    }

    public void verifyDeviceCountByStateTextVisibility() {
//        driver.switchTo().frame(0);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='Device Count by State']")));
    }

    public void clickViewKioskActivityByStateButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Kiosk Activity by State']/following-sibling::td[2]/input"))).click();
    }

    public void verifyKioskActivityByStateTextVisibility() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='Kiosk Activity by State']")));
    }

    public void clickViewInactiveKiosksButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Inactive Kiosks']/following-sibling::td[2]/input"))).click();
    }

    public void verifyInactiveKiosksTextVisibility() {
        driver.switchTo().frame(0);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='Inactive Kiosks']")));
    }

    public void clickViewgxDevicesWithPollDEXFlagButton() {
        clickViewReportByName("Gx Devices with Poll DEX Flag");
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Gx Devices with Poll DEX Flag']/following-sibling::td[2]/input"))).click();
    }

    public void clickViewAuthExecTimesButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Auth Exec Times']/following-sibling::td[2]/input"))).click();
    }

    public void verifyAuthExecTimesVisibility() {
        driver.switchTo().frame(0);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='Authorization Execution Times']")));
    }


    public void clickViewAuthLayerExecTimesButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Auth Layer Exec Times']/following-sibling::td[2]/input"))).click();
    }

    public void clickViewAuthRoundTripReportButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Auth Round Trip Report']/following-sibling::td[2]/input"))).click();
    }

    public void verifyAuthRoundTripReportTextVisibility() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(), 'Auth Round Trip Report')]")));
    }

    public void verifyPaymentStatsTextVisibility() {
        driver.switchTo().frame(0);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='POSM Payment Statistics']")));
    }

    public void clickViewPaymentStatsButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Payment Stats']/following-sibling::td[2]/input"))).click();

    }

    public void clickAuthorityAuthStatsButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Authority Auth Stats']/following-sibling::td[2]/input"))).click();
    }

    public void clickMerchantAuthStatsButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Merchant Auth Stats']/following-sibling::td[2]/input"))).click();
    }

    public void verifyAuthorityAuthStatsTextVisibility() {
        driver.switchTo().frame(0);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='Authorization Statistics']")));
    }

    public void verifyMerchantAuthStatsTextVisibility() {
        driver.switchTo().frame(0);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='Merchant Authorization Statistics']")));
    }

    public void clickViewPaymentTypeCountsButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Payment Type Counts']/following-sibling::td[2]/input"))).click();
    }

    public void verifyPaymentTypeCountsTextVisibility() {
        driver.switchTo().frame(0);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='Configured Payment Type Counts']")));
    }

    public void verifyAnyTableValue(String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='" + value + "']")));

    }

    public void clickViewBatchSuccessReportButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Batch Success Report']/following-sibling::td[2]/input"))).click();
    }

    public void clickViewCallInStatsButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Call-In Stats']/following-sibling::td[2]/input"))).click();
    }

    public void verifyBatchSuccessReportsTextVisibility() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'Batch Success Report')]")));
    }

    public void verifyCallInStatsTextVisibility() {
        driver.switchTo().frame(0);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='Call-In Metrics']")));
    }

}
