package pages.Dms.Files;

import helper.Core;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class FileListPage extends Core{
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(FileListPage.class);

    public FileListPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    public void verifyFiltrationByColumnId(String expectedValueFilteredBy, Integer filteredColumnId) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//img[@id='from_date_trigger']"))).click(); 
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='‹']"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//tr[contains(@class, 'daysrow')]/td[text()='1']"))).click();;
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='userOP']"))).click();
        List<WebElement> deviceTypeCells = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//table[@class='tabDataDisplayBorderNoFixedLayout']/tbody/tr/td[" + filteredColumnId + "]")));
        for (WebElement deviceCell:deviceTypeCells) {
            Assert.assertEquals(expectedValueFilteredBy, deviceCell.getText());
        }
    }

    public void verifyFiltrationByType(String value) {
        verifyFiltrationByColumnId(value, 3);
    }

    public void verifyFiltrationByName(String value) {
        verifyFiltrationByColumnId(value, 3);
    }

    public void show2500RowsPerPage() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select/option[text()='25000']"))).click();
    }
    
    public void setFrom(String date) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='call_from_date']"))).clear();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='call_from_date']"))).sendKeys(date);
    }
    
    public void clickId(String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@href='fileDetails.i?file_transfer_id=" + value + "']"))).click();
    }

    public void setSearchOptionToBegins() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='file_name_search_char']/option[text()='Begins']"))).click();
    }

    public void setSearchOptionToEquals() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='file_name_search_char']/option[text()='Equals']"))).click();
    }

    public void setFileName(String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='file_transfer_name']"))).clear();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='file_transfer_name']"))).sendKeys(value);

    }

    public void search() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='Submit']"))).click();
    }
}
