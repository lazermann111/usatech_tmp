package pages.Dms.Files;

import helper.Core;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class FileDetailsPage extends Core{
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(FileDetailsPage.class);

    public FileDetailsPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }
    private void verifyValue(String labelText, String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='" + labelText + "']/following-sibling::td[1][text()='" + value +"']")));
    }

    public void verifyFileId(String value) {
        verifyValue("File ID", value);
    }

    public void verifyFileName(String value) {
        verifyValue("File Name", value);
    }

    public void verifyFileType(String value) {
        verifyValue("File Type", value);
    }

    public void verifyFileSize(String value) {
        verifyValue("File Size", value);
    }

    public void verifyFileContentPreview(String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'File Content Preview (First 100 KB,')]/following-sibling::textarea[text()='" + value + "']")));
    }

    public void downloadFile() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='Download File']"))).click();
    }

    public void verifyFileComment(String value) {
        verifyValue("Comment", value);
    }
}
