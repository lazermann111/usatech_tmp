package pages.Dms;

import helper.Core;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LogInPage extends Core{
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(LogInPage.class);

    public LogInPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

     public void logIn(String name, String password)
    {
        logger.info("Logging in. User=" + name + ", Password=" + password + "...");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='userName']"))).sendKeys(name);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='credential']"))).sendKeys(password);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='submit']"))).click();
        logger.info("Verifying Welcome page...");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//td[contains(text(), 'Welcome,')]")));
    }

    public void logInAsCommonUser() {
        String[] loginAndPassword = getDmsCommonUserNameAndPassword();
        logIn(loginAndPassword[0], loginAndPassword[1]);
    }

}
