package pages.Dms.Devices;

import helper.Core;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FileTransferTab extends Core{
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(FileTransferTab.class);

    public FileTransferTab(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    public void clickFileTransferTab() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@class='shadeTabs']//a[text()='File Transfer']"))).click();
    }

    public void clickDownloadConfigButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='submit' and @value='Download Config']"))).click();
    }

    public void clickListButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='submit' and @value='List']"))).click();
    }

    public String getInnerHtml() {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//table[@class='tabDataDisplay'])[2]"))).getAttribute("innerHTML");
    }
}
