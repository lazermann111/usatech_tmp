package pages.Dms.Devices;

import helper.Core;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DeviceConfigurationWizardPage4 extends Core
{
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(DeviceConfigurationWizardPage4.class);

    public DeviceConfigurationWizardPage4(WebDriver driver)
    {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

	public void clickNextButton() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='Next >']"))).click();
	}    

	public void searchTemplate(String template) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='templateId_search']"))).sendKeys(template);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@id='templateId_search_link']/img"))).click();
	}  
	
	public void setTemplate(String template) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='templateId']/option[text()='" + template + "']"))).click();
	}

}
