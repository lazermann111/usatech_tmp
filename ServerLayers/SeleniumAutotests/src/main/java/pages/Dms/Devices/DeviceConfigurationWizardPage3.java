package pages.Dms.Devices;

import helper.Core;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DeviceConfigurationWizardPage3 extends Core
{
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(DeviceConfigurationWizardPage3.class);

    public DeviceConfigurationWizardPage3(WebDriver driver)
    {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

	public void clickNextButton() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='Next >']"))).click();
	}    

	public void searchCustomer(String customerName) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='customer_search']"))).sendKeys(customerName);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@id='customer_search_link']/img"))).click();
	}  
	
	public void setCustomer(String customerName) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='customer_id']/option[contains(text(), '" + customerName + "')]"))).click();
	}

	public void searchLocation(String customerName) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='location_search']"))).sendKeys(customerName);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@id='location_search_link']/img"))).click();
	}  
	
	public void setLocation(String customerName) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='location_id']/option[contains(text(), '" + customerName + "')]"))).click();
	
	}
}
