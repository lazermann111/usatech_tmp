package pages.Dms.Devices;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import helper.Core;

public class EditTerminalPage extends Core {

	
    public final WebDriver driver;
    public final WebDriverWait wait;
    @FindBy(xpath="//title")
    private static WebElement title;
    private static final Logger logger = Logger.getLogger(DeviceProfileTab.class);

    public EditTerminalPage(WebDriver driver) {
    	PageFactory.initElements(driver, this);
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }
    
    public String getTitle() {
    	return title.getAttribute("innerHTML");
    }
    
}
