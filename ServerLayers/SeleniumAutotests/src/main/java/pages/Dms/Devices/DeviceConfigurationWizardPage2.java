package pages.Dms.Devices;

import helper.Core;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DeviceConfigurationWizardPage2 extends Core
{
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(DeviceConfigurationWizardPage2.class);

    public DeviceConfigurationWizardPage2(WebDriver driver)
    {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

	public void clickNextButton() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='Next >']"))).click();
	}    
    
}
