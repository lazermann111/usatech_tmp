package pages.Dms.Devices;

import helper.Core;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DeviceConfigurationTab extends Core{
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(DeviceConfigurationTab.class);

    public DeviceConfigurationTab(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    public void clickDeviceConfigurationTab() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@class='shadeTabs']//a[text()='Device Configuration']"))).click();
    }

    public void verifySelected() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='selected' and text()='Device Configuration']")));
    }

    public void clickButtonByValue(String buttonValue) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='" + buttonValue + "']"))).click();
    }
}
