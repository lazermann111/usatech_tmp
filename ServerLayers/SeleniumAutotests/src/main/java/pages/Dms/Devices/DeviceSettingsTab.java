package pages.Dms.Devices;

import helper.Core;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DeviceSettingsTab extends Core{
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(DeviceSettingsTab.class);

    public DeviceSettingsTab(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    public void clickDeviceSettingsTab() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@class='shadeTabs']//a[text()='Device Settings']"))).click();
    }

    public void clickListAllButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='tabDataContent']//input[@value='List All']"))).click();
    }

    public String getValue(String parameterName) {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='contentMiddle']//td[contains(text(), '" + parameterName + "')]/../td[2]"))).getText().trim();
    }

    public String getAuthorizationAmountValue() {
        return getValue("Authorization Amount");
    }

    public String getTwoTierPricingValue()
    {
        return getValue("Two-Tier Pricing");
    }

    public String getAttractMessageLine1Value()
    {
        return getValue("Attract Message Line 1");
    }

    public String getAttractMessageLine2Value()
    {
        return getValue("Attract Message Line 2");
    }

    public String getWelcomeMessageLine1Value()
    {
        return getValue("Welcome Message Line 1");
    }

    public String getWelcomeMessageLine2Value()
    {
        return getValue("Welcome Message Line 2");
    }

//        Assert.assertEquals(minimumAllowedVendAmountValue1, deviceSettingsTab.getMinimumAllowedVendAmountValue()

    public String getDisableMessageLine1Value()
    {
        return getValue("Disable Message Line 1");
    }

    public String getDisableMessageLine2Value()
    {
        return getValue("Disable Message Line 2");
    }

    public String getServerOverrideCallInTimeWindowStartActivatedValue()
    {
        return getValue("Call-in Time Window Start - Activated");
    }

    public String getServerOverrideCallInTimeWindowHoursActivatedValue()
    {
        return getValue("Call-in Time Window Hours - Activated");
    }

    public String getServerOverrideCallInTimeWindowStartDEXValue()
    {
        return getValue("Call-in Time Window Start - DEX");
    }

    public String getServerOverrideCallInTimeWindowHoursDEXValue()
    {
        return getValue("Call-in Time Window Hours - DEX");
    }

    //        Assert.assertEquals(serverOverrideMaximumAuthorizationAmountValue1, deviceSettingsTab.getServerOverrideMaximumAuthorizationAmountValue());
    public String getMDBSettingMaxNumberOfItemsValue()
    {
        return getValue("MDB Setting Max Number Of Items");
    }

    public String getMDBSettingTimeoutfor1stSelectionValue()
    {
        return getValue("MDB Setting Timeout for 1st Selection");
    }

    public String getMDBSettingTimeoutfor2nd3rdEtcSelectionsValue()
    {
        return getValue("MDB Setting Timeout for 2nd 3rd Etc Selections");
    }

    public String getMDBSettingVendSessionTimeoutValue()
    {
        return getValue("MDB Setting Vend Session Timeout");
    }

    public String getCashTransactionRecordingValue()
    {
        return getValue("Cash Transaction Recording");
    }

    public String getDEXSettingScheduleValue()
    {
        return getValue("DEX Setting Schedule");
    }

    public String getAuxSerialPortSelectValue()
    {
        return getValue("Aux Serial Port Select");
    }

    public String getVMCInterfaceTypeValue()
    {
        return getValue("VMC Interface Type");
    }

    public String getCoinPulseDurationValue()
    {
        return getValue("Coin Pulse Duration");
    }

    public String getCoinPulseSpacingValue()
    {
        return getValue("Coin Pulse Spacing");
    }

    public String getCoinPulseValueValue()
    {
        return getValue("Coin Pulse Value");
    }

    public String getCoinAutoTimeoutValue()
    {
        return getValue("Coin Auto Timeout");
    }

    public String getCoinReaderEnableActiveStateValue()
    {
        return getValue("Coin Reader Enable Active State");
    }

    public String getCoinItemPriceN1Value()
    {
        return getValue("Coin Item Price #1");
    }

    public String getCoinItemPriceN1LabelValue()
    {
        return getValue("Coin Item Price #1 Label");
    }

    public String getCoinItemPriceN2Value()
    {
        return getValue("Coin Item Price #2");
    }

    public String getCoinItemPriceN2LabelValue()
    {
        return getValue("Coin Item Price #2 Label");
    }

    public String getCoinItemPriceN3Value()
    {
        return getValue("Coin Item Price #3");
    }

    public String getCoinItemPriceN3LabelValue()
    {
        return getValue("Coin Item Price #3 Label");
    }

    public String getCoinItemPriceN4Value()
    {
        return getValue("Coin Item Price #4");
    }

    public String getCoinItemPriceN4LabelValue()
    {
        return getValue("Coin Item Price #4 Label");
    }

    public String getCoinItemPriceN5Value()
    {
        return getValue("Coin Item Price #5");
    }

    public String getCoinItemPriceN5LabelValue()
    {
        return getValue("Coin Item Price #5 Label");
    }

    public String getCoinItemPriceN6Value()
    {
        return getValue("Coin Item Price #6");
    }

    public String getCoinItemPriceN6LabelValue()
    {
        return getValue("Coin Item Price #6 Label");
    }

    public String getCoinItemPriceN7Value()
    {
        return getValue("Coin Item Price #7");
    }

    public String getCoinItemPriceN7LabelValue()
    {
        return getValue("Coin Item Price #7 Label");
    }

    public String getCoinItemPriceN8Value()
    {
        return getValue("Coin Item Price #8");
    }

    public String getCoinItemPriceN8LabelValue()
    {
        return getValue("Coin Item Price #8 Label");
    }

    public String getPulseTimeValueValue()
    {
        return getValue("Pulse Time Value");
    }

    public String getTopOffValueValue()
    {
        return getValue("Top-off Value");
    }

    public String getPulseCaptureValueValue()
    {
        return getValue("Pulse Capture Value");
    }

    public String getInnerHtml() {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='contentMiddle']/div"))).getAttribute("innerHTML");
    }

    public void setEventHistoryFromDate(String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='event_from_date']"))).clear();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='event_from_date']"))).sendKeys(value);
    }

    public void setEventHistoryFromTime(String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='event_from_time']"))).clear();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='event_from_time']"))).sendKeys(value);
    }

    public void setEventHistoryToDate(String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='event_to_date']"))).clear();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='event_to_date']"))).sendKeys(value);
    }

    public void setEventHistoryToTime(String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='event_to_time']"))).clear();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='event_to_time']"))).sendKeys(value);
    }

    public void clickButtonByValue(String buttonValue) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='" + buttonValue + "']"))).click();
    }

    public void clickListDeviceSettingsButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'Device Settings')]//input[@type='button']"))).click();
    }

    public void clickListCountersHistoryButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'Counters History')]//input[@type='button']"))).click();
    }

    public void clickListEventHistoryButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'Event History')]//input[@type='button']"))).click();
    }
}
