package pages.Dms.Devices;

import helper.Core;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DeviceConfigurationWizardPage1 extends Core
{
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(DeviceConfigurationWizardPage1.class);

    public DeviceConfigurationWizardPage1(WebDriver driver)
    {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    public void setSerialNumber(String serialNumbers) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//textarea[@id='serial_number_list_id']"))).sendKeys(serialNumbers);
    }

    public void setDeviceType(String deviceType) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Device Type']/..//select[@id='device_type_id']/option[text()='" + deviceType + "']"))).click();
    }

	public void clickNextButton() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='Next >']"))).click();
	}    
    
}
