package pages.Dms.Devices;

import helper.Core;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DeviceConfigurationWizardPage5 extends Core
{
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(DeviceConfigurationWizardPage5.class);

    public DeviceConfigurationWizardPage5(WebDriver driver)
    {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

	public void clickNextButton() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='Next >']"))).click();
	}    

	public void verifyTemplateName(String template) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='tableDataHead']/div[@class='txtWhiteBold' and contains(text(), '" + template + "')]")));		
	}  
	
	public String setPropertyTextBoxByLabel(String label, String value) {
		String oldValue = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@label='" + label + "']"))).getAttribute("value");
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@label='" + label + "']"))).clear();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@label='" + label + "']"))).sendKeys(value);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@label='" + label + "']"))).click();
		return oldValue;
	}
	
	//input[@label='Attract Message Line 1']

}
