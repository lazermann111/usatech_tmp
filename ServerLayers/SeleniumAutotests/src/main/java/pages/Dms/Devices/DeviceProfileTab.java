package pages.Dms.Devices;

import helper.Core;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DeviceProfileTab extends Core{
    public final WebDriver driver;
    public final WebDriverWait wait;
    @FindBy(xpath="//input[@value='Change Program']")
    private static WebElement changeProgramButton;
    @FindBys(@FindBy(xpath="//select[@name='termList']/option"))
    private static List<WebElement> terminals;
    @FindBy(xpath="//input[@value='Edit Terminal']")
    private static WebElement editTerminalButton;
    @FindBy(xpath="//td[text()='DMS Customer']//../td/a")
    private static WebElement dmsCustomerALHValue;
    @FindBy(xpath="//td[text()='DMS Customer']//../td/input")
    private static WebElement changeCustomerALHButton;
  
    private static final Logger logger = Logger.getLogger(DeviceProfileTab.class);

    public DeviceProfileTab(WebDriver driver) {
    	PageFactory.initElements(driver, this);
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }
    public String getValue(String parameterName) {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='contentMiddle']//td[contains(text(), '" + parameterName + "')]/../td[2]"))).getText().trim();
    }


    public void verifySerialNumber(String serialNumber) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Serial Number']/following-sibling::td[1]/a[text()='" + serialNumber + "']")));
    }

    public void verifyDeviceName(String deviceName) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Device Name']/following-sibling::td[1][text()='" + deviceName + "']")));
    }

    public void verifyCustomerName(String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Customer Name']/following-sibling::td[1][text()='" + value + "']")));
    }

    public void verifyStatus(String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[@class='label' and text()='Status']/following-sibling::td//b[text()='" + value + "']")));
    }

    public void clickListAllButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='button' and @value='List All']"))).click();
    }

    public void clickListLastCallDatesButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'Last Call Dates')]//input[@type='button']"))).click();
    }

    public void clickListUSALiveLocationButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'USALive Location')]//input[@type='button']"))).click();
    }

    public void clickListCallsButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='submit' and @value='List Calls']"))).click();
    }

    public void clickListSaleRepsAndAffiliationsButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'Sale Reps and Affiliations')]//input[@type='button']"))).click();
    }

    public void clickListAuthorizationLocationAndHostsButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'Authorization Location and Hosts')]//input[@type='button']"))).click();
    }

    public void clickListEPortDealerAndTerminalsButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'ePort Program and Terminals')]//input[@type='button']"))).click();
    }

    public void clickListAllLink() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='List All']"))).click();
    }

    public void clickSerialNumberLink() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(@href, 'SerialNumber')]"))).click();
    }

    public void clickStartOverButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='Start Over']"))).click();
    }

    public void clickToggleButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='Toggle']"))).click();
    }

    public void verifyFirmwareVersion(String firmware) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Firmware Version']/following-sibling::td[text()='" + firmware + "']"/*"//tr[1]/td[6][text()='" + firmware + "']"*/)));

    }
    public void clickChangeProgramButton() {
    	changeProgramButton.click();
    }
    
    public void clickEditTerminalButton() {
    	terminals.get(0).click();
    	editTerminalButton.click();
    }
    
    public	String getDmsCustomerALHValue()
    	{
    		return dmsCustomerALHValue.getText();    		
    	}
    
    public void clickChangeCustomerALHButton()
    {
    	changeCustomerALHButton.click();
    }
   }

