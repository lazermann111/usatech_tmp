package pages.Dms.Devices;

import helper.Core;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DeviceConfigurationWizardPage7 extends Core
{
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(DeviceConfigurationWizardPage7.class);

    public DeviceConfigurationWizardPage7(WebDriver driver)
    {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

	public void clickNextButton() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='Next >']"))).click();
	}    

	public void setPaymentTypeTemplate(String paymentTypeTemplate) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='pos_pta_tmpl_id']/option[text()='" + paymentTypeTemplate + "']"))).click();		
	}

	public void setImportMode(String string) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Import Mode']/..//input[5]"))).click();
		
	}  
	


}
