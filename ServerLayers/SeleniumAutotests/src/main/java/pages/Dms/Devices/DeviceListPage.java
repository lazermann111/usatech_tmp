package pages.Dms.Devices;

import helper.Core;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Arrays;
import java.util.List;

public class DeviceListPage extends Core{
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(DeviceListPage.class);

    public DeviceListPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    public void verifyDeviceTypeFiltration(String deviceType) {
        verifyFiltrationByColumnId(deviceType, 1);
    }


    public void verifyColumnsHeaders() throws InterruptedException {
        List<String> expectedHeaders = Arrays.asList("Device Type", "Device Name", "Serial Number", "Reporting Customer", "DMS Customer", "DMS Location", "Firmware", "Diagnostic", "PTest", "Bezel Mfgr", "Bezel App Version", "Comm Method", "Last Activity", "Status");
        Thread.sleep(2000);



        List<WebElement> actualHeaders = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//table[@class='tabDataDisplayBorder']/thead//td/a")));
        for (WebElement actualHeader:actualHeaders) {
            Assert.assertTrue("Unexpected header:" + actualHeader.getText(), expectedHeaders.contains(actualHeader.getText()));
        }
    }

    public void verifyCommMethodFiltration(String commMethod) {
        verifyFiltrationByColumnId(commMethod, 12);
    }

    public void verifyZeroRecordsFound() {
        Assert.assertEquals("Records count is not 0!", 0, driver.findElements(By.xpath("//table[@class='tabDataDisplayBorder']/tbody/tr")).size());
    }

    public void verifyOnlyOneRecordFound(String commMethodName) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='headerTab']//td[contains(text(), '" + commMethodName + "')]")));
    }
    
    public void verifyReportingCustomer(String reportingCustomer) {
        verifyElementPresenceByColumnId(reportingCustomer, 4);
    } 
    
    public void verifyReportingCustomerFiltration(String reportingCustomer) {
        verifyFiltrationByColumnId(reportingCustomer, 4);
    }

    public void verifyCustomerFiltration(String customerName) {
        verifyFiltrationByColumnId(customerName, 5);
    }

    public void verifyCustomerPresence(String customerName) {
        verifyElementPresenceByColumnId(customerName, 5);
    }

    public void verifyDeviceNamePresence(String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table[@class='tabDataDisplayBorder']/tbody/tr/td[2]/a[text()='" + value + "']")));
    }

    public void verifyLocationFiltration(String locationPartialName) {
        verifyFiltrationByColumnId(locationPartialName, 6);
    }

    public void verifyLocationPresence(String value) {
        verifyElementPresenceByColumnId(value, 6);
    }

    public void verifyFirmwareVersionPresence(String value) {
        verifyElementPresenceByColumnId(value, 7);
    }

    public void verifyDiagnosticVersionPresence(String value) {
        verifyElementPresenceByColumnId(value, 8);
    }

    public void verifyPTestVersionPresence(String value) {
        verifyElementPresenceByColumnId(value, 9);
    }

    public void verifyBezelManufacturerPresence(String value) {
        verifyElementPresenceByColumnId(value, 10);
    }

    public void verifyBezelAppVersionPresence(String value) {
        verifyElementPresenceByColumnId(value, 11);
    }

    public void verifyFiltrationByColumnId(String expectedValueFilteredBy, Integer filteredColumnId) {
        List<WebElement> deviceTypeCells = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//table[@class='tabDataDisplayBorder']/tbody/tr/td[" + filteredColumnId + "]")));
        for (WebElement deviceCell:deviceTypeCells) {
            Assert.assertEquals(expectedValueFilteredBy, deviceCell.getText());
        }
    }

    public void verifyElementPresenceByColumnId(String expectedValue, Integer filteredColumnId) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table[@class='tabDataDisplayBorder']/tbody/tr/td[" + filteredColumnId + "][text()='" + expectedValue + "']")));
    }

    public void verifyFirmwareVersionFiltration(String firmwareVersionPartialName) {
        verifyFiltrationByColumnId(firmwareVersionPartialName, 7);
    }

    public void verifyDiagnosticVersionFiltration(String diagnosticVersionPartialName) {
        verifyFiltrationByColumnId(diagnosticVersionPartialName, 7);
    }

    public void clickDeviceNameLink(String deviceName) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table//a[text()='" + deviceName + "']"))).click();
    }

    public void verifyPTestVersionFiltration(String pTest) {
        verifyFiltrationByColumnId(pTest, 8);
    }

    public void verifyBezelManufacturerFiltration(String bezelMfgr) {
        verifyFiltrationByColumnId(bezelMfgr, 9);
    }

    public void verifyBezelAppVersionFiltration(String bezelAppVersion) {
        verifyFiltrationByColumnId(bezelAppVersion, 10);
    }

    public void show2500RowsPerPage() {
        Boolean b = driver.findElements(By.xpath("//select/option[text()='25000']")).isEmpty();
        if(!b)
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select/option[text()='25000']"))).click();
    }


    public void clickAnyDeviceName() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//a[contains(@href, 'device_id')])[1]"))).click();
    }
}
