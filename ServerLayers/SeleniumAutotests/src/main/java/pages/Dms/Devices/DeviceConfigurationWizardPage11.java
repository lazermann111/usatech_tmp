package pages.Dms.Devices;

import helper.Core;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DeviceConfigurationWizardPage11 extends Core
{
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(DeviceConfigurationWizardPage11.class);

    public DeviceConfigurationWizardPage11(WebDriver driver)
    {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }
	
	public void clickStatrOverButton() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='Start Over']"))).click();
	}

	public void verifyLog(String expectedLog) {
		String actualLog = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='contentMiddle']/div/pre"))).getText();
		Assert.assertEquals(expectedLog.trim().replaceAll("\\s+", " "), actualLog.trim().replaceAll("\\s+", " "));
	}
	
}
