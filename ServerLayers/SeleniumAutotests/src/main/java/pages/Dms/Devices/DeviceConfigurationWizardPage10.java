package pages.Dms.Devices;

import helper.Core;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DeviceConfigurationWizardPage10 extends Core
{
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(DeviceConfigurationWizardPage10.class);

    public DeviceConfigurationWizardPage10(WebDriver driver)
    {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }
	
	public void clickFinishButton() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='Finish >']"))).click();
	}

	public void verifyCustomer(String customer) {
		String actualCustomer = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//th[text()='Customer']/../../tr[2]/td[count(//th[text()='Customer']/preceding-sibling::*)+1]"))).getText();
		Assert.assertEquals(customer, actualCustomer);
	}

	public void verifyLocation(String location) {
		String actualLocation = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//th[text()='Location']/../../tr[2]/td[count(//th[text()='Location']/preceding-sibling::*)+1]"))).getText();
		Assert.assertTrue("Current location is " + actualLocation + ", but expected " + location, actualLocation.contains(location)) ;
	} 
	
	public void verifySerialNumber(String serialNumber) {
		String actualSerialNumber = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Serial Number']/../../tr[2]/td[count(//td[text()='Serial Number']/preceding-sibling::*)+1]"))).getText();
		Assert.assertEquals(serialNumber, actualSerialNumber);
	}

	public void verifyTemplateName(String paymentTypeTemplate) {
		String actualPaymentTypeTemplate = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//th[text()='Template Name']/../../tr[2]/td[count(//th[text()='Template Name']/preceding-sibling::*)+1]"))).getText();
		Assert.assertTrue("Current location is " + actualPaymentTypeTemplate + ", but expected " + paymentTypeTemplate, actualPaymentTypeTemplate.contains(paymentTypeTemplate)) ;
		
	}

	public void verifyParameter(String propertyName) {
		String actualPropertyName = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//th[text()='Parameter']/../../tr[2]/td[count(//th[text()='Parameter']/preceding-sibling::*)+1]"))).getText();
		Assert.assertTrue("Current PropertyName is " + actualPropertyName + ", but expected " + propertyName, actualPropertyName.contains(propertyName)) ;		
	}

	public void verifyValue(String propertyValue) {
		String actualPropertyValue = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//th[text()='Value']/../../tr[2]/td[count(//th[text()='Value']/preceding-sibling::*)+1]"))).getText();
		Assert.assertEquals(propertyValue, actualPropertyValue);
		
	}

	public void clickOkInAlert() {
		wait.until(ExpectedConditions.alertIsPresent());
		Alert alert = driver.switchTo().alert();
		alert.accept();
	}
	
}
