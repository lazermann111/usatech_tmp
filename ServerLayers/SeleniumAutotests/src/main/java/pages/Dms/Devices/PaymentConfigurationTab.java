package pages.Dms.Devices;

import helper.Core;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PaymentConfigurationTab extends Core{
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(PaymentConfigurationTab.class);
    private String transactionHistoryInnerHtml;

    public PaymentConfigurationTab(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    public void clickPaymentConfigurationTab() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@class='shadeTabs']//a[text()='Payment Configuration']"))).click();
    }

    public String getPaymentTypesInnerHtml() {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='contentMiddle']/div/div[2]"))).getAttribute("innerHTML");
    }

    public void clickPaymentTypesListButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(), 'Payment Types')]/input[@type='button']"))).click();
    }

    public void setEventHistoryFromDate(String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='transaction_from_date']"))).clear();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='transaction_from_date']"))).sendKeys(value);
    }

    public void setEventHistoryFromTime(String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='transaction_from_time']"))).clear();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='transaction_from_time']"))).sendKeys(value);
    }

    public void setEventHistoryToDate(String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='transaction_to_date']"))).clear();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='transaction_to_date']"))).sendKeys(value);
    }

    public void setEventHistoryToTime(String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='transaction_to_time']"))).clear();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='transaction_to_time']"))).sendKeys(value);
    }

    public void clickTransactionHistoryListButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(), 'Transaction History From')]/input[@type='button']"))).click();
    }

    public String getTransactionHistoryInnerHtml() {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='contentMiddle']/div/div[2]/table"))).getAttribute("innerHTML");
    }

    public void clickEditSettingsButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='Edit Settings']"))).click();
    }

    public void clickGoBackToDeviceProfileButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='Go Back to Device Profile']"))).click();
    }

    public void clickReorderButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='Reorder']"))).click();
    }

    public void clickButtonByValue(String buttonValue) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='" + buttonValue + "']"))).click();
    }

	public void verifyPaymentTypePresence(String paymentType) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Payment Type']/../../..//td[contains(text(), '" + paymentType + "')]")));
		
	}
}
