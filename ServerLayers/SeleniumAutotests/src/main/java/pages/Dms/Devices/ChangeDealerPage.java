package pages.Dms.Devices;

import helper.Core;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ChangeDealerPage extends Core{

	
	    public final WebDriver driver;
	    public final WebDriverWait wait;
	    @FindBy(xpath="//title")
	    private static WebElement title;
	    private static final Logger logger = Logger.getLogger(DeviceProfileTab.class);

	    public ChangeDealerPage(WebDriver driver) {
	    	PageFactory.initElements(driver, this);
	        this.driver = driver;
	        wait = new WebDriverWait(driver, 30);
	    }
	    
	    public String getTitle() {
	    	return title.getAttribute("innerHTML");
	    }
	    

	}
