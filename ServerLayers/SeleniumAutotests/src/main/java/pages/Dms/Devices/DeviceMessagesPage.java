package pages.Dms.Devices;

import helper.Core;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DeviceMessagesPage extends Core
{
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(DeviceMessagesPage.class);

    public DeviceMessagesPage(WebDriver driver)
    {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

	public void setStartDateTimeToNow() {
		Date dateNow = new Date();
		//dateNow = new Date(dateNow.getTime());//To make sure transaction appears in report
		String timeNow = new SimpleDateFormat("hh:mm:ss").format(dateNow);	
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='StartTime']"))).clear();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='StartTime']"))).sendKeys(timeNow);
	}  
	
	public void setEndDateTimeToTomorrow() {
		Date dateNow = new Date();
		Date dateTomorrow = new Date(dateNow.getTime() + (1000 * 60 * 60 * 24));//To make sure transaction appears in report
		String tomorrow = new SimpleDateFormat("MM/dd/yyyy").format(dateTomorrow);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='EndDate']"))).clear();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='EndDate']"))).sendKeys(tomorrow);
	}

	public void clickSubmitButton() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='Submit']"))).click();		
	}

	public void setDeviceNameOrSerial(String deviceName) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='searchDevice']"))).clear();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='searchDevice']"))).sendKeys(deviceName);

	} 

}
