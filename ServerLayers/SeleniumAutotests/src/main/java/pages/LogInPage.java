package pages;

import helper.Core;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LogInPage extends Core{
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(LogInPage.class);

    public LogInPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    private WebElement getMenuItemByText(String linkText) {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table[@class='menuTable']//a[text()='" + linkText + "']")));
    }

    public void logIn(String name, String password)
    {
        logger.info("Logging in. User=" + name + ", Password=" + password + "...");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='username']"))).sendKeys(name);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='password']"))).sendKeys(password);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='submit']"))).click();
        logger.info("Verifying Welcome page...");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@class='welcome']")));
    }

    public void logInAsWeakUser() {
        String[] loginAndPassword = getUsaLiveWeakUserNameAndPassword();
        logIn(loginAndPassword[0], loginAndPassword[1]);
    }

    public void logInAsPowerUser() {
        String[] loginAndPassword = getUsaLivePowerUserNameAndPassword();
        logIn(loginAndPassword[0], loginAndPassword[1]);
    }
}
