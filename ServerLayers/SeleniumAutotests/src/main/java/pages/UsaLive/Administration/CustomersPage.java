package pages.UsaLive.Administration;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class CustomersPage {
    public final WebDriver driver;
    public final WebDriverWait wait;
    @FindBy(xpath="//td[text()='No user reports found for this customer.']")
    private static WebElement noUserReportsLabel;
    @FindBy(xpath="//select[@id='editUserId']")
    private static WebElement customerListSelect;
    private static final Logger logger = Logger.getLogger(CustomersPage.class);

    public CustomersPage(WebDriver driver) {
	PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    public void searchByCustomerName(String customerName)
    {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='customerName']"))).sendKeys(customerName);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@value='Search']"))).click();
    }

    public void clickUserByNameContains(String namePart) {
//        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@id='editUserId']/option[contains(text(), '" + namePart + "')]"))).click();
	Select select = new Select(customerListSelect);
	select.selectByVisibleText(namePart);
	
    }

    public void verifyTextboxTextByLabel(String label, String expectedValue) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='" + label + "']/following-sibling::td/span[contains(text(),'" + expectedValue + "')]")));
    }

    public void clickAddBestVendorReportingButton() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='addBestVendorReporting']"))).click();
    }

    public void clickRemoveBestVendorReportingButton() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='removeBestVendorReporting']"))).click();
    }

    public void verifyAddBestVendorReportingMessage() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='message']/p[text()='Successfully added best vendor reporting for the customer.']")));
    }

    public void verifyRemoveBestVendorReportingMessage() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='message']/p[text()='Successfully removed best vendor reporting for the customer.']")));
    }

    public void clickLoginAsButton() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@value='Login As']"))).click();
    }

    public WebElement getRandomCustomerByNameFromList() {
        List<WebElement> allCustomers =  wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//select[@id='editUserId']/option")));
         WebElement randomCustomer = allCustomers.get((new Random()).nextInt(allCustomers.size()));
        return randomCustomer;
    }

    public void clickViewCustomersUserReportsLink() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@id='viewCustomerUserReport']"))).click();
    }

    public List<String> getAllReportNamesText() {
        List<String>  allReportsNamesText = new ArrayList<String>();

//
//        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        if(driver.findElements(By.xpath("//td[text()='No user reports found for this customer.']")).size()==1)
//        if(noUserReportsLabel.isDisplayed())
//        if(wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//td[text()='No user reports found for this customer.']"))).size()==1)

            return allReportsNamesText;
        List<WebElement> allReportsNamesWebElements =  wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//form[@id='beUserForm']/table/tbody[2]/tr/td[3]")));
        for (WebElement reportNameWebElement: allReportsNamesWebElements) {
            allReportsNamesText.add(reportNameWebElement.getText());
        }
        return allReportsNamesText;
    }

    public List<String> getAllReportNamesText(String userName) {
        List<String>  allReportsNamesText = new ArrayList<String>();

//
//        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        if(driver.findElements(By.xpath("//td[text()='No user reports found for this customer.']")).size()==1)
//        if(wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//td[text()='No user reports found for this customer.']"))).size()==1)
            return allReportsNamesText;
        List<WebElement> allReportsNamesWebElements =  wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//form[@id='beUserForm']/table/tbody[2]/tr/td[3]")));//+[text()='" + userName + "']/../td[3]
        for (WebElement reportNameWebElement: allReportsNamesWebElements) {
            allReportsNamesText.add(reportNameWebElement.getText());
        }
        return allReportsNamesText;
    }

    public List<String> getAllTransportNamesText(String userName) {
        List<String>  allReportsTransportNamesText = new ArrayList<String>();
        List<WebElement> allReportsTransportNamesWebElements =  wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//form[@id='beUserForm']/table/tbody[2]/tr/td[2][text()='" + userName + "']/../td[10]")));
        for (WebElement reportNameWebElement: allReportsTransportNamesWebElements) {
            allReportsTransportNamesText.add(reportNameWebElement.getText());
        }
        return allReportsTransportNamesText;
    }

    public String getPrimaryContactMemberIdText() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@id='user_name']"))).getText();
    }
}
