package pages.UsaLive.Administration.RMA;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RMAPage {
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(RMAPage.class);

    public RMAPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    public void verifyRecordValuesByID(String requestDate, String rmaNumber, String receipt, String type, String replacementKitQuantity, String description, String status, String actions) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table/tbody[2]/tr/td[text()='" + rmaNumber + "']/..//*[text()='" + requestDate + "']")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table/tbody[2]/tr/td[text()='" + rmaNumber + "']/..//*[text()='" + receipt + "']")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table/tbody[2]/tr/td[text()='" + rmaNumber + "']/..//*[text()='" + type + "']")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table/tbody[2]/tr/td[text()='" + rmaNumber + "']/..//*[text()='" + replacementKitQuantity + "']")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table/tbody[2]/tr/td[text()='" + rmaNumber + "']/..//*[text()='" + description + "']")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table/tbody[2]/tr/td[text()='" + rmaNumber + "']/..//*[text()='" + status + "']")));

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table/tbody[2]/tr/td[text()='" + rmaNumber + "']/..//*[@value = '" + actions + "']")));

    }

    public void setRmaNumber(String rmaNumber) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='rmaNumber']"))).sendKeys(rmaNumber);
    }

    public void clickSearchButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='search']"))).click();
    }

    public boolean isRmaFound(String rmaNumber) {
        return !driver.findElements(By.xpath("//td[text()='" + rmaNumber + "']")).isEmpty();
    }

    public void clickDeleteButton(String rmaNumber) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='" + rmaNumber + "']/..//input[@value='Delete']"))).click();
    }

    public void verifyPage() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='content']/div[.='RMA']")));
    }
}
