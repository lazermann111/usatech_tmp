package pages.UsaLive.Administration.Devices;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by ahachikyan on 3/2/2016.
 */
public class DeviceProfilePage {
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(DeviceProfilePage.class);
    @FindBy(xpath = "//select[@name='locationTypeId']")
    private static WebElement locationTypeIdSelect;
    @FindBy(xpath = "//select[@name='timeZone']")
    private static WebElement timeZoneSelect;
    @FindBy(xpath = "//select[@name='primaryContactId']")
    private static WebElement primaryContactIdSelect;
    @FindBy(xpath = "//select[@name='secondaryContactId']")
    private static WebElement secondaryContactIdSelect;
    
    public DeviceProfilePage(WebDriver driver) {
	PageFactory.initElements(new AjaxElementLocatorFactory(driver, 30), this);
        this.driver = driver;
        wait = new WebDriverWait(driver, 60);
    }

    public WebElement getClient() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='client']")));
    }

    public WebElement getOverrideDoingBusinessAs() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='doingBusinessAs']")));
    }

    public WebElement getOverrideCustomerServicePhone() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='customerServicePhone']")));
    }

    public WebElement getOverrideCustomerServiceEmail() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='customerServiceEmail']")));
    }

    public WebElement getRegion() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@name='regionId']//option[@selected='selected']")));
    }

    public void setRegion(String region) {
        setComboboxValue("regionId", region);
    }

    public WebElement getLocationName() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='location']")));
    }

    public WebElement getAddress() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='address1']")));
    }

    public WebElement getCity() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='city']")));
    }

    public WebElement getState() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='state']")));
    }

    public WebElement getZipCode() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='postal']")));
    }

    public WebElement getCountry() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@name='country']/option[@selected='selected']")));
    }

    public WebElement getSpecificLocationAtThisAddress() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='locationDetails']")));
    }

    public WebElement getPhoneNumberAtLocation() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='telephone']")));
    }

    public WebElement getLocationType() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@name='locationTypeId']/option[@selected='selected']")));
    }

    public void setLocationType(String locationType) {
//        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@name='locationTypeId']/option[text()='" + locationType + "']"))).click();
	Select select = new Select( locationTypeIdSelect);
	select.selectByVisibleText(locationType);
//        setComboboxValue("locationTypeId", locationType);
    }

    public WebElement getLocationTimeZone() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@name='timeZone']/option[@selected='selected']")));
    }

    public void setLocationTimeZone(String locationTimeZone) {
//        setComboboxValue("timeZone", locationTimeZone);
	Select select = new Select( timeZoneSelect);
	select.selectByVisibleText(locationTimeZone);
//        setComboboxValue("locationTypeId", locationTimeZone);
    }

    public WebElement getMachineAssetNumber() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='asset']")));
    }

    public WebElement getMachinesMakeAndModel() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@name='machineId']/option[@selected='selected']")));
    }

    public void setMachinesMakeAndModel(String module) {
        setComboboxValue("machineId", module);
    }

    public WebElement getProductType() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@name='productTypeId']/option[@selected='selected']")));
    }

    public void setProductType(String productType) {
        setComboboxValue("productTypeId", productType);
    }

    public void setComboboxValue(String comboName, String value) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@name='" + comboName + "']//option[contains(text(),'" + value + "')]"))).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@name='" + comboName + "']//option[contains(text(),'" + value + "')]"))).sendKeys(Keys.ENTER);
    }

    public WebElement getOwner() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//th[text()='Owner']/../td")));
    }

    public WebElement getBankAccountToPay() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@name='custBankId']/option[@selected='selected']")));
    }

    public void setBankAccountToPay(String bankAccountToPay) {
        setComboboxValue("custBankId", bankAccountToPay);
    }

    public WebElement getPaymentSchedule() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@name='paymentScheduleId']/option[@selected='selected']")));
    }

    public void setPaymentSchedule(String value) {
        setComboboxValue("paymentScheduleId", value);
    }

    public WebElement getBusinessType() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@name='businessTypeId']/option[@selected='selected']")));
    }

    public void setBusinessType(String value) {
        setComboboxValue("businessTypeId", value);
    }

    public WebElement getPrimaryContact() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@name='primaryContactId']/option[@selected='selected']")));
    }

    public void setPrimaryContact(String value) {
//        setComboboxValue("primaryContactId", value);
	Select select = new Select( primaryContactIdSelect);
	select.selectByVisibleText(value);
    }

    public WebElement getSecondaryContact() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@name='secondaryContactId']/option[@selected='selected']")));
    }

    public void setSecondaryContact(String value) {
//        setComboboxValue("secondaryContactId", value);
	Select select = new Select( secondaryContactIdSelect);
	select.selectByVisibleText(value);
    }


    public void saveChanges() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@value='Save Changes']"))).click();
    }


    public void setClient(String client)
    {
        getClient().clear();
        getClient().sendKeys(client);
    }

    public void setOverrideDoingBusinessAs(String overrideDoingBusinessAs)
    {
        getOverrideDoingBusinessAs().clear();
        getOverrideDoingBusinessAs().sendKeys(overrideDoingBusinessAs);
    }

    public void setOverrideCustomerServicePhone(String overrideCustomerServicePhone)
    {
        getOverrideCustomerServicePhone().clear();
        getOverrideCustomerServicePhone().sendKeys(overrideCustomerServicePhone);
    }

    public void setOverrideCustomerServiceEmail(String overrideCustomerServiceEmail)
    {
        getOverrideCustomerServiceEmail().clear();
        getOverrideCustomerServiceEmail().sendKeys(overrideCustomerServiceEmail);
    }

    public void setLocationName(String locationName)
    {
        getLocationName().clear();
        getLocationName().sendKeys(locationName);
    }

    public void setAddress(String address)
    {
        getAddress().clear();
        getAddress().sendKeys(address);
    }

    public void setZipCode(String postalCode)
    {
        getZipCode().clear();
        getZipCode().sendKeys(postalCode);
        getCity().click();//To apply zip code
    }

    public void setSpecificLocationAtThisAddress(String value)
    {
        getSpecificLocationAtThisAddress().clear();
        getSpecificLocationAtThisAddress().sendKeys(value);
    }

    public void setPhoneNumberAtLocation(String value)
    {
        getPhoneNumberAtLocation().clear();
        getPhoneNumberAtLocation().sendKeys(value);
    }

    public void setMachineAssetNumber(String value)
    {
        getMachineAssetNumber().clear();
        getMachineAssetNumber().sendKeys(value);
    }

    public void setCountry(String value) {
        setComboboxValue("country", value);
    }

    public void verifySuccessfullyUpdatedMessage(String deviceId, String location) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[@class='message-success' and text()='Successfully updated Device \"" + deviceId + "\" at location \"" + location + "\"']")));
    }

    public void setState(String value) {
        getState().clear();
        getState().sendKeys(value);
    }

    public void setCity(String value) {
        getCity().clear();
        getCity().sendKeys(value);
    }

    public WebElement getDeviceConfiguration() {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='Device Configuration']")));
    }

    public String getUserNameText() {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//th[text()='Username']/../td"))).getText();
    }

    public void clickRefreshButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='refreshNetGeoDataButton']"))).click();
    }

    public void verifyRefreshButtonIsPresent() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='refreshNetGeoDataButton']")));
    }

    public void verifyRefreshButtonIsMissed() {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//input[@id='refreshNetGeoDataButton']")));
    }

    public void waitForRequestHasBeenQueueedMessageInvivibility() {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//b[text()='Request has been queued']")));
    }

    public String getNetworkInformationCity() {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@id='nlCity']"))).getText().trim();
    }

    public String getNetworkInformationState() {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@id='nlState']"))).getText().trim();
    }

    public String getNetworkInformationZip() {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@id='nlZip']"))).getText().trim();
    }

    public String getNetworkInformationGeoCoordinates() {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@id='nlGeo']"))).getText().trim();
    }

    public void verifyDeviceSerialVisibility(String deviceSerial) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//th[text()='Device Serial Number']/..//span[text()='" + deviceSerial + "']")));
    }
}
