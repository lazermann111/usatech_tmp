package pages.UsaLive.Administration;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;

import java.util.List;

/**
 * Created by ahachikyan on 3/2/2016.
 */
public class MassDeviceUpdatePage {
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(MassDeviceUpdatePage.class);

    public MassDeviceUpdatePage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }


    public void clickCurrentActiveDevicesLink() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[text()='current active devices']"))).click();
    }


	public void clickTreeItemByText(String itemText) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='treeLabel' and text()='" + itemText + "']"))).click();		
	}
	
	public void clickCheckboxByDeviceSerialNumber(String deviceSerialNumber) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='" + deviceSerialNumber + "']/..//input[@type='checkbox']"))).click();		
	}
	
	public void clickCsvRadioButton() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='download_devices_csv.i']"))).click();		
	}	
	
	public void clickOkButton() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//form[@name='selectDevices']//input[@value='OK']"))).click();		
	}


	public void clickSampleExcelLink() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@title='Download Sample Update Excel File']"))).click();			
	}


	public void clickSampleCsvLink() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@title='Download Sample Update Comma-Separated Values File']"))).click();	
	}


	public void uploadFile(String fileFullPath) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='updateFile']"))).sendKeys(fileFullPath);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='Submit']"))).click();
	}


	public void verifyFirstRowText(String expectedText) {		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//table[contains(@class,'sortable-table')]//a[text()='Mass Device Update'])[1]/../..//div[text()='" + expectedText + "']")));
		
	}
	

}
