package pages.UsaLive.Administration.RMA;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RMACreatePartPage extends RMACreatePage{
//    public final WebDriver driver;
//    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(RMACreatePartPage.class);

    public RMACreatePartPage(WebDriver driver) {
        super(driver);
    }

//    public RMACreatePartPage(WebDriver driver) {
//        this.driver = driver;
//        wait = new WebDriverWait(driver, 30);
//    }

    public void clickPartComboBox(String partNumber) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[contains(., '" + partNumber + "')]/following-sibling::td[2]/input[@type='checkbox']"))).click();
    }

    public void setReturnQuantity(String partNumber, String returnQuantity) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[contains(., '" + partNumber + "')]/following-sibling::td/input[contains(@id, 'rmaPartsQuantity')]"))).sendKeys(returnQuantity);
    }
}
