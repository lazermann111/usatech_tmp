package pages.UsaLive.Administration;

import helper.EmailUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * Created by ahachikyan on 3/2/2016.
 */
public class TransportDialog extends RegisteredReportsPage{
//    public final WebDriver driver;
//    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(TransportDialog.class);

    public TransportDialog(WebDriver driver) {
        super(driver);
    }

    public void addTransportWithEmailType(String transportName, String email) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id = 'transportName']"))).sendKeys(transportName);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id = 'tpv_1']"))).sendKeys(email);

        Integer emailsCountBefore = EmailUtils.getEmailsCount();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@value='Test Transport']"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id = 'transportTestStatus' and text()='Transport Result Success: \"mailto: " + email + "\"']")));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='saveTransport' and @value='Add Transport']"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id = 'transportTestStatus' and text()='Transport was saved.']")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id = 'transportStatus' and text()='Valid']")));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='close-btn']"))).click();

        EmailUtils.waitForNewEmail(emailsCountBefore, 60);

    }

    public void editTransport(String newTransportName) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@value='Edit Transport']"))).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id = 'transportName']"))).clear();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id = 'transportName']"))).sendKeys(newTransportName);
    }
}
