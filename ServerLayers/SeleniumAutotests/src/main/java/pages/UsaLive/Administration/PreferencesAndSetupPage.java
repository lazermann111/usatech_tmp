package pages.UsaLive.Administration;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by ahachikyan on 3/2/2016.
 */
public class PreferencesAndSetupPage {
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(PreferencesAndSetupPage.class);

    public PreferencesAndSetupPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }


    public void setFirstComboboxValue(String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//select[@name='terminalDisplay'])[1]/option[text()='" + value + "']"))).click();//.sendKeys(value);
    }

    public void setSecondComboboxValue(String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//select[@name='terminalDisplay'])[2]/option[text()='" + value + "']"))).click();//.sendKeys(value);
    }

    public void setThirdComboboxValue(String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//select[@name='terminalDisplay'])[3]/option[text()='" + value + "']"))).click();//.sendKeys(value);
    }

    public void clickSaveChanges() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='Save Changes']"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='Save Changes']"))).click();

    }

    public void verifySuccessMessage() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[@class='message-success' and text()='Your setup parameters have been updated']")));
    }

    public void verifyFirstComboboxAvailableValueAbsence(String expectedValueToBeMissed) {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("(//select[@name='terminalDisplay'])[1]/option[text()='" + expectedValueToBeMissed + "']")));

    }
}
