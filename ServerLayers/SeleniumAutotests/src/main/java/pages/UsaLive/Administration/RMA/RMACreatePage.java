package pages.UsaLive.Administration.RMA;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RMACreatePage {
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(RMACreatePage.class);

    public RMACreatePage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    public void clickYesButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//form[@id='rmaQuestionForm']/input[@value='Yes']"))).click();
    }

    public void setSearchByDeviceSerialNumber(String deviceSerialNumber) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//textarea[@id='deviceSearchByText']"))).sendKeys(deviceSerialNumber);
    }

    public void clickSearchButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='search']"))).click();
    }

    public void clickDeviceCombobox(String deviceSerialNumber) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='searchResultSection']//input[@name='rmaDevices' and @data-serial-cd='" + deviceSerialNumber + "']"))).click();
    }

    public void clickAddSelectedButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='Add selected']"))).click();
    }

    public void setRMADescription(String description) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='rmaDescription']"))).sendKeys(description);
    }

    public void setContactName(String contactName) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='contactName']"))).sendKeys(contactName);
    }

    public void setStreetAddress(String streetAddress) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='address']"))).sendKeys(streetAddress);
    }
    
    public void setCity(String city) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='city']"))).sendKeys(city);
    }
    
    public void setState(String state) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='state']"))).sendKeys(state);
    }

    public void setPostalCode(String postalCode) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='postalCd']"))).sendKeys(postalCode);
    }

    public void setEmail(String email) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='email']"))).sendKeys(email);
    }

    public void setPhone(String phone) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='phone']"))).sendKeys(phone);
    }

    public void clickCreateRMAButton() throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='createRMA']"))).click();
        Thread.sleep(5000);
    }
}
