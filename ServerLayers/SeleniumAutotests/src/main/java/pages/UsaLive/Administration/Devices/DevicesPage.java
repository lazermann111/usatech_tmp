package pages.UsaLive.Administration.Devices;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ahachikyan on 3/2/2016.
 */
public class DevicesPage {
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(DevicesPage.class);

    public DevicesPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    public void searchFor(String searchText) {
        logger.info("Searching for:" + searchText);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='searchText']"))).clear();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='searchText']"))).sendKeys(searchText);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='button' and @value = 'Search']"))).click();
    }

    public void verifySearchedValuePresenceInResults(String expectedSearchedValue) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table[@class = 'terminalChecklist']//tr/td[@data-sort-value= '" + expectedSearchedValue + "']")));
    }

    public void verifySearchedValueAbsenceInResults(String expectedSearchedValue) {
        Assert.assertEquals("Value =" + expectedSearchedValue + " should not be present in search results!", 0,driver.findElements(By.xpath("//table[@class = 'terminalChecklist']//tr/td[@data-sort-value= '" + expectedSearchedValue + "']")).size());
        ;
    }

    public WebElement getDevice(String expectedSearchedDevice) {
        //return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table[@class = 'terminalChecklist']//tr/td[1][@data-sort-value= '" + expectedSearchedDevice + "']")));
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//td[@data-sort-value= '" + expectedSearchedDevice + "']/a")));
    }

    public void verifySearchedLocationPresenceInResults(String expectedSearchedLocation) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table[@class = 'terminalChecklist']//tr/td[2][@data-sort-value= '" + expectedSearchedLocation + "']")));
    }

    public void expandTreeRootNode() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//ul[@id='terminalsTreeCustomerAll']/li[@class='closedTreeNode']/a[@class='treeLabel']"))).click();
    }

    public void collapseTreeRootNode() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//ul[@id='terminalsTreeCustomerAll']/li[@class='openTreeNode']/a[@class='treeLabel']"))).click();
    }

    public void verifyTreeElementVisibility(String expectedTreeElement) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='treeLabel' and text()='" + expectedTreeElement + "']")));
    }

    public void verifyTreeElementInvisibility(String expectedTreeElement) {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//a[@class='treeLabel' and text()='" + expectedTreeElement + "']")));
    }


    public void sortByDevice() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table//li[@class='openTreeNode']//tr[@class='tableHeader']//a[text()='Device']"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table//li[@class='openTreeNode']//tr[@class='tableHeader']//a[text()='Device' and @data-toggle='sort']")));
    }

    public void sortByLocation() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table//li[@class='openTreeNode']//tr[@class='tableHeader']//a[text()='Location']"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table//li[@class='openTreeNode']//tr[@class='tableHeader']//a[text()='Location' and @data-toggle='sort']")));
    }

    public void expandTreeNode(String nodeName) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='terminalsTreeCustomerAll']/li[@class='openTreeNode']//li[@class='closedTreeNode']//a[@class='treeLabel' and text()='" + nodeName + "']"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='terminalsTreeCustomerAll']/li[@class='openTreeNode']//li[@class='openTreeNode']//a[@class='treeLabel' and text()='" + nodeName + "']")));
    }

    public void verifySortingByDevice(String nodeName) {
        logger.info("Verifying sorting...");
        verifySortingBy(1, nodeName);
    }

    public void verifySortingByLocation(String nodeName) {
        verifySortingBy(2, nodeName);
    }

    public void verifySortingBy(Integer sortColumnIndex, String nodeName) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='terminalsTreeCustomerAll']/li[@class='openTreeNode']//li[@class='openTreeNode']//a[@class='treeLabel' and text()='" + nodeName + "']")));
        List<WebElement> devicesWebElementsList =  wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//ul[@id='terminalsTreeCustomerAll']/li[@class='openTreeNode']//li[@class='openTreeNode']//a[@class='treeLabel' and text()='" + nodeName + "']/..//table/tbody/tr/td[" + sortColumnIndex + "]")));
        ArrayList<String> devicesTextList = new ArrayList<String>();
        for (WebElement deviceWebElement : devicesWebElementsList) {
            String deviceText = deviceWebElement.getAttribute("data-sort-value");
            devicesTextList.add(deviceText);
        }
        ArrayList<String> devicesTextListBeforeSorting = new ArrayList<String>();
        devicesTextListBeforeSorting = (ArrayList)devicesTextList.clone();
        java.util.Collections.sort(devicesTextList);
        ArrayList<String> devicesTextListAfterSorting = devicesTextList;
        Assert.assertEquals(devicesTextListAfterSorting, devicesTextListBeforeSorting);

    }

    public void editRegionName(String regionNameBeforeChange, String regionNameAfterChange) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='terminalsTreeCustomerAll']/li[@class='openTreeNode']//li[@class='openTreeNode']//a[@class='treeLabel' and text()='" + regionNameBeforeChange + "']/../button[@title='Edit Region Name...']"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='terminalsTreeCustomerAll']/li[@class='openTreeNode']//li[@class='openTreeNode']//a[@class='treeLabel' and text()='" + regionNameBeforeChange + "']/../input[@type='text']"))).clear();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='terminalsTreeCustomerAll']/li[@class='openTreeNode']//li[@class='openTreeNode']//a[@class='treeLabel' and text()='" + regionNameBeforeChange + "']/../input[@type='text']"))).sendKeys(regionNameAfterChange);
        //TODO: uncomment and change to proper Region
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='Update']"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='terminalsTreeCustomerAll']/li[@class='openTreeNode']//li[@class='openTreeNode']//a[@class='treeLabel' and text()='" + regionNameAfterChange + "']/../button[@title='Edit Region Name...']")));

    }

    public void editRegionDetails(String regionName) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='terminalsTreeCustomerAll']/li[@class='openTreeNode']//li[@class='openTreeNode']//a[@class='treeLabel' and text()='" + regionName + "']/../a[@title='View/Edit Region Details...']"))).click();
    }

    public void clickByDeviceIdLink(String deviceId) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(), '" + deviceId + "')]"))).click();
    }
}
