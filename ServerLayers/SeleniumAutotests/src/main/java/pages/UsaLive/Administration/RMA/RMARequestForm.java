package pages.UsaLive.Administration.RMA;

import junit.framework.Assert;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RMARequestForm {
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(RMARequestForm.class);

    public RMARequestForm(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    public void verifyUSALiveUser(String value) {
        org.junit.Assert.assertEquals(value, wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='USALive User:']/following-sibling::td"))).getText());
    }

    public void verifyUSALiveCustomer(String value) {
        org.junit.Assert.assertEquals(value, wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='USALive Customer:']/following-sibling::td"))).getText());
    }

    public void verifyRMAType(String value) {
        org.junit.Assert.assertEquals(value, wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='RMA Type:']/following-sibling::td"))).getText());
    }

    public void verifyRMADescription(String value) {
        org.junit.Assert.assertEquals(value, wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='RMA Description:']/following-sibling::td"))).getText());
    }

    public void verifyEPortSerial(String value) {
        org.junit.Assert.assertEquals(value, wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//th[.='ePort Serial #']/../..//tr[2]/td[count(//th[text()='ePort Serial #']/preceding-sibling::*) +1]"))).getText());
    }

    public void verifyContactName(String value) {
        org.junit.Assert.assertEquals(value, wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Contact Name:']/following-sibling::td"))).getText());
    }

    public void verifyAddress(String value) {
        org.junit.Assert.assertEquals(value, wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Address:']/following-sibling::td"))).getText());
    }

    public void verifyCity(String value) {
        org.junit.Assert.assertEquals(value, wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='City:']/following-sibling::td"))).getText());
    }

    public void verifyState(String value) {
        org.junit.Assert.assertEquals(value, wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='State:']/following-sibling::td"))).getText());
    }

    public void verifyPostalCode(String value) {
        org.junit.Assert.assertEquals(value, wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Postal Code:']/following-sibling::td"))).getText());
    }

    public void verifyCountry(String value) {
        org.junit.Assert.assertEquals(value, wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Country:']/following-sibling::td"))).getText());
    }

    public void verifyEmail(String value) {
        org.junit.Assert.assertEquals(value, wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Email:']/following-sibling::td"))).getText());
    }

    public void verifyPhone(String value) {
        org.junit.Assert.assertEquals(value, wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Phone:']/following-sibling::td"))).getText());
    }

    public String getRMANumber() {
        return wait.withTimeout(180, TimeUnit.SECONDS).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//th[contains(text(),' RMA # Assigned:')]"))).getText().replace("RMA # Assigned:", "");
    }

    public void verifyQuantity(String value) {
        org.junit.Assert.assertEquals(value, wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//th[.='Miscellaneous Parts:']/../following-sibling::tr[2]/td[1]"))).getText());
    }

    public void verifyPartNumber(String value) {
        org.junit.Assert.assertEquals(value, wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//th[.='Miscellaneous Parts:']/../following-sibling::tr[2]/td[2]"))).getText());
    }
}
