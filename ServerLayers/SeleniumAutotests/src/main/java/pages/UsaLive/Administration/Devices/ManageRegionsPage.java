package pages.UsaLive.Administration.Devices;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Random;

/**
 * Created by ahachikyan on 3/2/2016.
 */
public class ManageRegionsPage {
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(ManageRegionsPage.class);

    public ManageRegionsPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }


    public void setRegionName(String regionName)
    {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='regionName']"))).clear();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='regionName']"))).sendKeys(regionName);
    }

    public void setParentRegion(String parentRegion)
    {
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@name='parentRegionId']"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@name='parentRegionId']/option[text()='" + parentRegion + "']"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@name='parentRegionId']/option[text()='" + parentRegion + "']"))).sendKeys(Keys.ENTER);
    }

    public void save()
    {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='saveButton']"))).click();
    }

    public void verifyRequiredFieldWarningVisibility() {

    }

    public String setAnyParentRegion() {
        List<WebElement> regionOptions = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//select[@name='parentRegionId']/option[count(@disabled)=0 and text()!='-- None --']")));
        Assert.assertNotEquals("No available regions found!", 0, regionOptions.size());
        int randomAvailableRegionIndex = new Random().nextInt(regionOptions.size());
        String newParentRegion = regionOptions.get(randomAvailableRegionIndex).getText();
        setParentRegion(newParentRegion);
        return newParentRegion;
    }


    public void verifySuccessMessage(String regionNameAfter) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[@class='message-success' and text()='Region " + regionNameAfter + " was updated']")));
    }
}
