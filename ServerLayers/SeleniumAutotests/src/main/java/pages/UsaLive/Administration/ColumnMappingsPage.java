package pages.UsaLive.Administration;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class ColumnMappingsPage {
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(ColumnMappingsPage.class);

    public ColumnMappingsPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 60);
    }

    public void searchCustomerByName(String customerName)
    {    	
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='customerName']"))).sendKeys(customerName);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@value='Search Customer']"))).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@id='customerId']/option[text()='" + customerName + "']"))).click();            
    }
    
    public void searchMappedDevice(String deviceSerial)
    {    	
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='mappedDeviceSerialCd']"))).sendKeys(deviceSerial);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='mappedDeviceSearch']"))).click();          
    }
    
    public void clickMappedDevice(String deviceSerial)
    {    	        
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@id='mappedDeviceList']/option[contains(text(), '" + deviceSerial + "')]"))).click();          
    }
    
    public String  getColumnMapContent() throws InterruptedException
    { 
    	Thread.sleep(2000);
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//textarea[@id='columnMapContent']"))).getAttribute("value");          
    }

	public String getHighlightedColumnMapName(String columnMapName) throws InterruptedException {
    	Thread.sleep(2000);
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@id='columnMapId']/option[@title='" + columnMapName + "']"))).getAttribute("selected"); 
	}
  
    

}
