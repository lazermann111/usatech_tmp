package pages.UsaLive.Administration;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

//import UsaLive.Reports.SavedReports.Card;
//import UsaLive.Reports.SavedReports.To;

public class IssueRefundsPage extends RegisteredReportsPage{
//    public final WebDriver driver;
//    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(IssueRefundsPage.class);

    public IssueRefundsPage(WebDriver driver) {
        super(driver);
    }

    public void setStartDate(String date) {
    	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='startDateId']"))).clear();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='startDateId']"))).sendKeys(date);
    }
    
    public void setEndDate(String date) {
    	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='endDateId']"))).clear();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='endDateId']"))).sendKeys(date);
    }
    
    public void clickFindTransactionsButton() {
    	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@value='Find Transactions']"))).click();
    }
    
//    public void setCardId(String cardId) {
//    	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='searchCardId']"))).clear();
//        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='searchCardId']"))).sendKeys(cardId);
//    }
    
    public void setCardFirstTwoToSix(String cardId) {
    	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='searchCCFirst']"))).clear();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='searchCCFirst']"))).sendKeys(cardId);
    }
    
    

	public void checkTransactionComboBox() {
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("(//input[@type='checkbox'])[last()]"))).click();
	}

	public void clickIssueRefundsButton() {
		 wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='button' and @value='Issue Refunds']"))).click();
	}
 
	public void clickIssueRefundButton() {
		 wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='button' and @value='Issue Refund']"))).click();
	}

	public void setRefundAmount(String refundAmount) {
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='refundAmount']"))).clear();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='refundAmount']"))).sendKeys(refundAmount);				
	}

	public void setComment(String refundComment) {
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='refundComment']"))).clear();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='refundComment']"))).sendKeys(refundComment);
		
	}

	public void verifySuccessfullyCreatedRefundMessage() {
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='status-info-success']")));		
	}

	public void setRefundReason(String text) {
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@name='refundReason']/option[text()='" + text + "']"))).click();		
	}

}
