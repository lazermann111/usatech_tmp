package pages.UsaLive.Administration;

import org.junit.Assert;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ahachikyan on 3/2/2016.
 */
public class RegisteredReportsPage {
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(RegisteredReportsPage.class);

    public RegisteredReportsPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    public void setReportName(String reportName) {
        //wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@id='reportId']"))).sendKeys(reportName);
//        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@id='reportId']"))).sendKeys(reportName);
//        String s = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@id='reportId']"))).getText();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@id='reportId']/option[text()='" + reportName + "']"))).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@id='reportId']/option[text()='" + reportName + "']"))).sendKeys(Keys.ENTER);
    }

    public void verifyDescription(String expectedDescription) {
        Assert.assertEquals(expectedDescription, wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@id='description']"))).getText());
        //wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@id='description' and text()='" + expectedDescription + "']")));
    }

    public void verifyFormatType(String expectedFormatType) {
        Assert.assertEquals(expectedFormatType, wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@id='formatType']"))).getText());
//        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@id='formatType' and text()='" + expectedFormatType + "']")));
    }



    public void verifyFrequencyValues(List<String> expectedFrequencyValues) {
        String currentAvailableChoisesAsString =  wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@id='frequencyId']"))).getText();
        for(Integer i = 0; i < expectedFrequencyValues.size(); i++)
        {
            Assert.assertTrue("Frequency value is incorrect", currentAvailableChoisesAsString.contains(expectedFrequencyValues.get(i)));
        }
    }

    public void verifyRegisteredReportsPage() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//tr[@class='itemMasterTitle']/th[text()='Registered Reports']")));
    }

    public void verifyAvailableTransportsValuesPresence(List<String> expectedTransportValues) {
        String currentAvailableChoisesAsString =  wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@id='transportSelect']"))).getText();
        for(Integer i = 0; i < expectedTransportValues.size(); i++)
        {
            Assert.assertTrue("Transport value is incorrect", currentAvailableChoisesAsString.contains(expectedTransportValues.get(i)));
        }
    }

    public void verifyAvailableTransportsValuesAbsence(List<String> expectedTransportValues) {
        String currentAvailableChoisesAsString =  wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@id='transportSelect']"))).getText();
        for(Integer i = 0; i < expectedTransportValues.size(); i++)
        {
            Assert.assertTrue("Transport value is incorrect", !currentAvailableChoisesAsString.contains(expectedTransportValues.get(i)));
        }
    }

    public void verifyLabelVisibility(String labelText, Boolean expectedVisibility) {
        if(expectedVisibility)
        {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[text()='" + labelText + "']")));
        }
        else
        {
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//label[text()='" + labelText + "']")));
        }
    }

    public boolean isTransportExist(String transportName) {
        String currentAvailableChoisesAsString =  wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@id='transportSelect']"))).getText();
        return currentAvailableChoisesAsString.contains(transportName);
    }

    public void deleteTransport(String transportName) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@id='transportSelect']/option[text()='" + transportName + "']"))).click();
        getDeleteTransportButton().click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='Transport was deleted.']")));
    }

    public WebElement getDeleteTransportButton() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@value='Delete Transport']")));
    }

    public void addTransportWithEmailType(String transportName, String email) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@value='Add Transport']"))).click();
        TransportDialog transportDialog = new TransportDialog(driver);
        transportDialog.addTransportWithEmailType(transportName, email);
    }

    public void setTransport(String transportName) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@id='transportSelect']/option[text()='" + transportName + "']"))).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@id='transportSelect']/option[text()='" + transportName + "']"))).sendKeys(Keys.ENTER);
    }

    public void editTransport(String oldTransportName, String newTransportName) {
        setTransport(oldTransportName);
        new TransportDialog(driver).editTransport(newTransportName);
    }

    public WebElement getSaveButton() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='saveBtn']")));
    }

    public void verifySaveButtonEnabled(Boolean expectedEnabledState) {
        Boolean currentEnabledState = getSaveButton().isEnabled();
        Assert.assertEquals("Unexpected enabled state", expectedEnabledState, currentEnabledState);
    }


    public WebElement getCancelButton() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='cancelBtn']")));
    }

    public void verifyCancelButtonEnabled(Boolean expectedEnabledState) {
        Boolean currentEnabledState = getCancelButton().isEnabled();
        Assert.assertEquals("Unexpected enabled state", expectedEnabledState, currentEnabledState);
    }

    public WebElement getCreateButton() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='createBtn']")));
    }

    public void verifyCreateButtonEnabled(Boolean expectedEnabledState) {
        Boolean currentEnabledState = getCreateButton().isEnabled();
        Assert.assertEquals("Unexpected enabled state", expectedEnabledState, currentEnabledState);
    }

    public WebElement getDeleteButton() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='deleteBtn']")));
    }

    public void verifyDeleteButtonEnabled(Boolean expectedEnabledState) {
        Boolean currentEnabledState = getDeleteButton().isEnabled();
        Assert.assertEquals("Unexpected enabled state", expectedEnabledState, currentEnabledState);
    }

    public void verifyAvailableTransportsRequired() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='Error: This field is required.']/../select[@id='transportSelect']")));
    }

    public Integer getReportsCount(String registeredReportName) {
        return driver.findElements(By.xpath("//label[text()='Reports']/..//option[text()='" + registeredReportName + "']")).size();
    }

    public Integer getReportsCount() {
        return driver.findElements(By.xpath("//label[text()='Reports']/..//option")).size();
    }

    public void selectRegisteredReport(String registeredReportName) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//label[text()='Reports']/..//option[text()='" + registeredReportName + "']"))).click();
    }

    public void confirmDeletion() {
        Alert javascriptAlert = driver.switchTo().alert();
        logger.info(javascriptAlert.getText()); // Get text on alert box
        javascriptAlert.accept();
    }

    public void verifyStatusMessage(String expectedStatusMessage) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='" + expectedStatusMessage + "']")));
    }

    public void setFrequency(String frequency) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select/option[text()='" + frequency + "']"))).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select/option[text()='" + frequency + "']"))).sendKeys(Keys.ENTER);
    }

    public void deleteAllExistingRegisteredReports() {
        while(driver.findElements(By.xpath("//label[text()='Reports']/..//option")).size() != 0)
        {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//label[text()='Reports']/..//option"))).click();
            getDeleteButton().click();
            confirmDeletion();
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public String getAnyExistingReportName() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//label[text()='Reports']/..//option"))).getText();
    }

    public void verifyFrequencySelectedValue(String expectedFrequency) {
        String currentFrequency = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@id='frequencyId']/option[@selected='selected']"))).getText();
        Assert.assertEquals(expectedFrequency, currentFrequency);
    }

    public WebElement getTransportDeleteFailedMessage() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='transportDeleteStatus' and text() = 'Transport delete failed. There is user report associated with this transport.']")));
    }

    public void verifyFrequencyVisibility(boolean expectedVisibility) {
        if(expectedVisibility)
        {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[text()='Frequency']/../..//select[@id='frequencyId']")));
        }
        else
        {
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//label[text()='Frequency']/../..//select[@id='frequencyId']")));
        }
    }

    public void verifyTransportsVisibility(boolean expectedVisibility) {
        if(expectedVisibility)
        {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//th[text()='Available Transports']/../..//select[@name='transportId']")));
        }
        else
        {
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//th[text()='Available Transports']/../..//select[@name='transportId']")));
        }
    }

    public void verifyBatchType(String expectedFormatType) {
        Assert.assertEquals(expectedFormatType, wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@id='batchType']"))).getText());
//        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@id='formatType' and text()='" + expectedFormatType + "']")));
    }

    public void verifyBatchType(boolean expectedVisibility) {
        if(expectedVisibility)
        {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[text()='Batch Type']/../..//span[@id='batchType']")));
        }
        else
        {
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//label[text()='Batch Type']/../..//span[@id='batchType']")));
        }
    }

    public void verifyAlertTypes(String environment, boolean expectedVisibility) {
        if(expectedVisibility)
        {
            String actualInnerHtml = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//th[text()='Alert Types']/../td"))).getAttribute("innerHTML");
            String expectedInnerHtml = "";
            if(environment.equals("int"))
            	expectedInnerHtml = "<fieldset class=\"checkbox-grouping\"><legend>Device Events (Edge/G9 Devices Only)</legend><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"18\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Device Alert </label></fieldset><fieldset class=\"checkbox-grouping\"><legend>DEX Alerts</legend><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"4\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Bill Validator Error </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"6\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Cabinet Temperature </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"8\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Card Reader Error </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"5\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Coin Mech Error </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"2\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Column Jam </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"9\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Communications Error </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"3\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Door Opened </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"7\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Other </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"10\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Product Sold Out </label><input class=\"checkbox-button\" onclick=\"var ops=['Select All','Deselect All'];var i=ops.indexOf(this.value);this.value=ops[(i+1)%2];Array.from($(this).getParent().getElements('input[type=checkbox]')).each(function(cb) {cb.checked=(i==0);});onDetailChange();\" value=\"Select All\" type=\"button\"></fieldset><fieldset class=\"checkbox-grouping\"><legend>DEX Exception Generated by ePort</legend><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"34\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Dex Exception </label></fieldset><fieldset class=\"checkbox-grouping\"><legend>MDB Alerts</legend><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"27\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Bill Acceptor Alert: bad motor </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"28\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Bill Acceptor Alert: bad sensor </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"29\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Bill Acceptor Alert: corrupt ROM </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"31\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Bill Acceptor Alert: credit stolen </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"33\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Bill Acceptor Alert: disabled </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"30\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Bill Acceptor Alert: jammed </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"32\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Bill Acceptor Alert: stacker out of position </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"20\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Coin Changer Alert: bad tube sensor </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"21\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Coin Changer Alert: changer unplugged </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"25\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Coin Changer Alert: coin jam </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"24\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Coin Changer Alert: coin routing error </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"23\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Coin Changer Alert: corrupt ROM </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"26\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Coin Changer Alert: credit stolen </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"22\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Coin Changer Alert: tube jam </label><input class=\"checkbox-button\" onclick=\"var ops=['Select All','Deselect All'];var i=ops.indexOf(this.value);this.value=ops[(i+1)%2];Array.from($(this).getParent().getElements('input[type=checkbox]')).each(function(cb) {cb.checked=(i==0);});onDetailChange();\" value=\"Select All\" type=\"button\"></fieldset><input class=\"checkbox-button\" onclick=\"var ops=['Select All','Deselect All'];var i=ops.indexOf(this.value);this.value=ops[(i+1)%2];Array.from($(this).getParent().getElements('input[type=checkbox]')).each(function(cb) {cb.checked=(i==0);});onDetailChange();\" value=\"Select All\" type=\"button\">";
            if(environment.equals("ecc"))
            	expectedInnerHtml = "<fieldset class=\"checkbox-grouping\"><legend>Device Events (Edge/G9 Devices Only)</legend><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"18\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Device Alert </label></fieldset><fieldset class=\"checkbox-grouping\"><legend>DEX Alerts</legend><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"4\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Bill Validator Error </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"6\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Cabinet Temperature </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"8\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Card Reader Error </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"5\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Coin Mech Error </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"2\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Column Jam </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"9\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Communications Error </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"3\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Door Opened </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"7\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Other </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"1\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Product Sold Out </label><input class=\"checkbox-button\" onclick=\"var ops=['Select All','Deselect All'];var i=ops.indexOf(this.value);this.value=ops[(i+1)%2];Array.from($(this).getParent().getElements('input[type=checkbox]')).each(function(cb) {cb.checked=(i==0);});onDetailChange();\" value=\"Select All\" type=\"button\"></fieldset><fieldset class=\"checkbox-grouping\"><legend>DEX Exception Generated by ePort</legend><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"34\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Dex Exception </label></fieldset><fieldset class=\"checkbox-grouping\"><legend>MDB Alerts</legend><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"27\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Bill Acceptor Alert: bad motor </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"28\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Bill Acceptor Alert: bad sensor </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"29\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Bill Acceptor Alert: corrupt ROM </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"31\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Bill Acceptor Alert: credit stolen </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"33\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Bill Acceptor Alert: disabled </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"30\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Bill Acceptor Alert: jammed </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"32\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Bill Acceptor Alert: stacker out of position </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"20\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Coin Changer Alert: bad tube sensor </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"21\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Coin Changer Alert: changer unplugged </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"25\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Coin Changer Alert: coin jam </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"24\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Coin Changer Alert: coin routing error </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"23\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Coin Changer Alert: corrupt ROM </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"26\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Coin Changer Alert: credit stolen </label><label class=\"checkbox\"><input name=\"params.alertTypes\" value=\"22\" data-validators=\"validate-reqchk-byname label: 'Alert Types'\" onchange=\"onDetailChange()\" type=\"checkbox\">Coin Changer Alert: tube jam </label><input class=\"checkbox-button\" onclick=\"var ops=['Select All','Deselect All'];var i=ops.indexOf(this.value);this.value=ops[(i+1)%2];Array.from($(this).getParent().getElements('input[type=checkbox]')).each(function(cb) {cb.checked=(i==0);});onDetailChange();\" value=\"Select All\" type=\"button\"></fieldset><input class=\"checkbox-button\" onclick=\"var ops=['Select All','Deselect All'];var i=ops.indexOf(this.value);this.value=ops[(i+1)%2];Array.from($(this).getParent().getElements('input[type=checkbox]')).each(function(cb) {cb.checked=(i==0);});onDetailChange();\" value=\"Select All\" type=\"button\">";
            Assert.assertEquals(expectedInnerHtml, actualInnerHtml);
        }
        else
        {
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//th[text()='Alert Types']/../td")));
        }
    }

    public List<String> getAllReportsText() {
        List<String>  allReportsText = new ArrayList<String>();
        List<WebElement> allReportsWebElements =  wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//select[@id='mainSelect']/option")));
        for (WebElement reportWebElement: allReportsWebElements) {
            allReportsText.add(reportWebElement.getText().substring(0, reportWebElement.getText().lastIndexOf(")")).trim());
        }

        return allReportsText;
    }

    public void clickRunReportSampleLink() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@id='runSample']"))).click();

    }

    public Boolean isNoBathesForSampleMessagePresent() {
        if (driver.findElements(By.xpath("//message[contains(text(),'You do not have any ')]")).size() == 0)
            return false;
        else
            return true;
    }
//    public List<String>  getAllTemp() {
//        List<String>  allReportsText = new ArrayList<String>();
//        List<WebElement> allReportsWebElements =  wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//select[@id='reportId']/option")));
//        for (WebElement reportWebElement: allReportsWebElements) {
//            allReportsText.add(reportWebElement.getText());
//        }
//
//        return allReportsText;
//
//    }
}
