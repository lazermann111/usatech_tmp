package pages.UsaLive.Administration.Devices;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by ahachikyan on 3/2/2016.
 */
public class DeviceConfigurationPage {
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(DeviceConfigurationPage.class);

    public DeviceConfigurationPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }



    public WebElement getAuthorizationAmountWebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Authorization Amount']")));
    }

    public WebElement getTwoTierPricingWebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Two-Tier Pricing']")));
    }

    public WebElement getAttractMessageLine1WebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Attract Message Line 1']")));
    }

    public WebElement getAttractMessageLine2WebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Attract Message Line 2']")));
    }

    public WebElement getWelcomeMessageLine1WebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Welcome Message Line 1']")));
    }

    public WebElement getWelcomeMessageLine2WebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Welcome Message Line 2']")));
    }

    public WebElement getMinimumAllowedVendAmountWebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Minimum Allowed Vend Amount']")));
    }

    public WebElement getDisableMessageLine1WebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Disable Message Line 1']")));
    }

    public WebElement getDisableMessageLine2WebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Disable Message Line 2']")));
    }

    public WebElement getServerOverrideCallInTimeWindowStartActivatedWebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Server Setting: Call-in Time Window Start - Activated']")));
    }

    public WebElement getServerOverrideCallInTimeWindowHoursActivatedWebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Server Setting: Call-in Time Window Hours - Activated']")));
    }

    public WebElement getServerOverrideCallInTimeWindowStartDEXWebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Server Setting: Call-in Time Window Start - DEX']")));
    }

    public WebElement getServerOverrideCallInTimeWindowHoursDEXWebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Server Setting: Call-in Time Window Hours - DEX']")));
    }

    public WebElement getServerOverrideMaximumAuthorizationAmountWebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Server Setting: Maximum Authorization Amount']")));
    }

    public WebElement getMDBSettingMaxNumberOfItemsWebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='MDB Setting Max Number Of Items']")));
    }

    public WebElement getMDBSettingTimeoutfor1stSelectionWebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='MDB Setting Timeout for 1st Selection']")));
    }

    public WebElement getMDBSettingTimeoutfor2nd3rdEtcSelectionsWebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='MDB Setting Timeout for 2nd 3rd Etc Selections']")));
    }

    public WebElement getMDBSettingVendSessionTimeoutWebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='MDB Setting Vend Session Timeout']")));
    }

    public WebElement getCashTransactionRecordingWebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@label='Cash Transaction Recording']/option[@selected='selected']")));
    }

    public WebElement getDEXSettingScheduleWebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@label='DEX Setting Schedule']/option[@selected='selected']")));
    }

      public WebElement getAuxSerialPortSelectWebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@label='Aux Serial Port Select']/option[@selected='selected']")));
    }

    public WebElement getVMCInterfaceTypeWebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@label='VMC Interface Type']/option[@selected='selected']")));
    }

    public WebElement getCoinPulseDurationWebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Coin Pulse Duration']")));
    }

    public WebElement getCoinPulseSpacingWebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Coin Pulse Spacing']")));
    }

    public WebElement getCoinPulseValueWebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Coin Pulse Value']")));
    }

    public WebElement getCoinAutoTimeoutWebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Coin Auto Timeout']")));
    }

    public WebElement getCoinReaderEnableActiveStateWebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@label='Coin Reader Enable Active State']/option[@selected='selected']")));
    }

    public WebElement getCoinItemPriceN1WebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Coin Item Price #1']")));
    }

    public WebElement getCoinItemPriceN1LabelWebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Coin Item Price #1 Label']")));
    }

    public WebElement getCoinItemPriceN2WebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Coin Item Price #2']")));
    }

    public WebElement getCoinItemPriceN2LabelWebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Coin Item Price #2 Label']")));
    }

    public WebElement getCoinItemPriceN3WebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Coin Item Price #3']")));
    }

    public WebElement getCoinItemPriceN3LabelWebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Coin Item Price #3 Label']")));
    }

    public WebElement getCoinItemPriceN4WebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Coin Item Price #4']")));
    }

    public WebElement getCoinItemPriceN4LabelWebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Coin Item Price #4 Label']")));
    }

    public WebElement getCoinItemPriceN5WebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Coin Item Price #5']")));
    }

    public WebElement getCoinItemPriceN5LabelWebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Coin Item Price #5 Label']")));
    }

    public WebElement getCoinItemPriceN6WebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Coin Item Price #6']")));
    }

    public WebElement getCoinItemPriceN6LabelWebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Coin Item Price #6 Label']")));
    }

    public WebElement getCoinItemPriceN7WebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Coin Item Price #7']")));
    }

    public WebElement getCoinItemPriceN7LabelWebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Coin Item Price #7 Label']")));
    }

    public WebElement getCoinItemPriceN8WebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Coin Item Price #8']")));
    }

    public WebElement getCoinItemPriceN8LabelWebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Coin Item Price #8 Label']")));
    }

    public WebElement getPulseTimeValueWebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Pulse Time Value']")));
    }

    public WebElement getTopOffValueWebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Top-off Value']")));
    }

    public WebElement getPulseCaptureValueWebElement()
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@label='Pulse Capture Value']")));
    }

    public void setAuthorizationAmountWebElement(String value)
    {
        getAuthorizationAmountWebElement().clear();
        getAuthorizationAmountWebElement().sendKeys(value);
    }

    public void setTwoTierPricingWebElement(String value)
    {
        getTwoTierPricingWebElement().clear();
        getTwoTierPricingWebElement().sendKeys(value);
    }

    public void setAttractMessageLine1WebElement(String value)
    {
        getAttractMessageLine1WebElement().clear();
        getAttractMessageLine1WebElement().sendKeys(value);
    }

    public void setAttractMessageLine2WebElement(String value)
    {
        getAttractMessageLine2WebElement().clear();
        getAttractMessageLine2WebElement().sendKeys(value);
    }

    public void setWelcomeMessageLine1WebElement(String value)
    {
        getWelcomeMessageLine1WebElement().clear();
        getWelcomeMessageLine1WebElement().sendKeys(value);
    }

    public void setWelcomeMessageLine2WebElement(String value)
    {
        getWelcomeMessageLine2WebElement().clear();
        getWelcomeMessageLine2WebElement().sendKeys(value);
    }

    public void setMinimumAllowedVendAmountWebElement(String value)
    {
        getMinimumAllowedVendAmountWebElement().clear();
        getMinimumAllowedVendAmountWebElement().sendKeys(value);
    }

    public void setDisableMessageLine1WebElement(String value)
    {
        getDisableMessageLine1WebElement().clear();
        getDisableMessageLine1WebElement().sendKeys(value);
    }

    public void setDisableMessageLine2WebElement(String value)
    {
        getDisableMessageLine2WebElement().clear();
        getDisableMessageLine2WebElement().sendKeys(value);
    }

    public void setServerOverrideCallInTimeWindowStartActivatedWebElement(String value)
    {
        getServerOverrideCallInTimeWindowStartActivatedWebElement().clear();
        getServerOverrideCallInTimeWindowStartActivatedWebElement().sendKeys(value);
    }

    public void setServerOverrideCallInTimeWindowHoursActivatedWebElement(String value)
    {
        getServerOverrideCallInTimeWindowHoursActivatedWebElement().clear();
        getServerOverrideCallInTimeWindowHoursActivatedWebElement().sendKeys(value);
    }

    public void setServerOverrideCallInTimeWindowStartDEXWebElement(String value)
    {
        getServerOverrideCallInTimeWindowStartDEXWebElement().clear();
        getServerOverrideCallInTimeWindowStartDEXWebElement().sendKeys(value);
    }

    public void setServerOverrideCallInTimeWindowHoursDEXWebElement(String value)
    {
        getServerOverrideCallInTimeWindowHoursDEXWebElement().clear();
        getServerOverrideCallInTimeWindowHoursDEXWebElement().sendKeys(value);
    }

    public void setServerOverrideMaximumAuthorizationAmountWebElement(String value)
    {
        getServerOverrideMaximumAuthorizationAmountWebElement().clear();
        getServerOverrideMaximumAuthorizationAmountWebElement().sendKeys(value);
    }

    public void setMDBSettingMaxNumberOfItemsWebElement(String value)
    {
        getMDBSettingMaxNumberOfItemsWebElement().clear();
        getMDBSettingMaxNumberOfItemsWebElement().sendKeys(value);
    }

    public void setMDBSettingTimeoutfor1stSelectionWebElement(String value)
    {
        getMDBSettingTimeoutfor1stSelectionWebElement().clear();
        getMDBSettingTimeoutfor1stSelectionWebElement().sendKeys(value);
    }

    public void setMDBSettingTimeoutfor2nd3rdEtcSelectionsWebElement(String value)
    {
        getMDBSettingTimeoutfor2nd3rdEtcSelectionsWebElement().clear();
        getMDBSettingTimeoutfor2nd3rdEtcSelectionsWebElement().sendKeys(value);
    }

    public void setMDBSettingVendSessionTimeoutWebElement(String value)
    {
        getMDBSettingVendSessionTimeoutWebElement().clear();
        getMDBSettingVendSessionTimeoutWebElement().sendKeys(value);
    }

    public void setCashTransactionRecordingWebElement(String value)
    {
        setComboboxValue("Cash Transaction Recording", value);
    }

    public void setDEXSettingScheduleWebElement(String value)
    {
        setComboboxValue("DEX Setting Schedule", value);
    }

    public void setAuxSerialPortSelectWebElement(String value)
    {
        setComboboxValue("Aux Serial Port Select", value);
    }

    public void setVMCInterfaceTypeWebElement(String value)
    {
        setComboboxValue("VMC Interface Type", value);
    }

    public void setCoinPulseDurationWebElement(String value)
    {
        getCoinPulseDurationWebElement().clear();
        getCoinPulseDurationWebElement().sendKeys(value);
    }

    public void setCoinPulseSpacingWebElement(String value)
    {
        getCoinPulseSpacingWebElement().clear();
        getCoinPulseSpacingWebElement().sendKeys(value);
    }

    public void setCoinPulseValueWebElement(String value)
    {
        getCoinPulseValueWebElement().clear();
        getCoinPulseValueWebElement().sendKeys(value);
    }

    public void setCoinAutoTimeoutWebElement(String value)
    {
        getCoinAutoTimeoutWebElement().clear();
        getCoinAutoTimeoutWebElement().sendKeys(value);
    }

    public void setCoinReaderEnableActiveStateWebElement(String value)
    {
        setComboboxValue("Coin Reader Enable Active State", value);
    }

    public void setCoinItemPriceN1WebElement(String value)
    {
        getCoinItemPriceN1WebElement().clear();
        getCoinItemPriceN1WebElement().sendKeys(value);
    }

    public void setCoinItemPriceN1LabelWebElement(String value)
    {
        getCoinItemPriceN1LabelWebElement().clear();
        getCoinItemPriceN1LabelWebElement().sendKeys(value);
    }

    public void setCoinItemPriceN2WebElement(String value)
    {
        getCoinItemPriceN2WebElement().clear();
        getCoinItemPriceN2WebElement().sendKeys(value);
    }

    public void setCoinItemPriceN2LabelWebElement(String value)
    {
        getCoinItemPriceN2LabelWebElement().clear();
        getCoinItemPriceN2LabelWebElement().sendKeys(value);
    }

    public void setCoinItemPriceN3WebElement(String value)
    {
        getCoinItemPriceN3WebElement().clear();
        getCoinItemPriceN3WebElement().sendKeys(value);
    }

    public void setCoinItemPriceN3LabelWebElement(String value)
    {
        getCoinItemPriceN3LabelWebElement().clear();
        getCoinItemPriceN3LabelWebElement().sendKeys(value);
    }

    public void setCoinItemPriceN4WebElement(String value)
    {
        getCoinItemPriceN4WebElement().clear();
        getCoinItemPriceN4WebElement().sendKeys(value);
    }

    public void setCoinItemPriceN4LabelWebElement(String value)
    {
        getCoinItemPriceN4LabelWebElement().clear();
        getCoinItemPriceN4LabelWebElement().sendKeys(value);
    }

    public void setCoinItemPriceN5WebElement(String value)
    {
        getCoinItemPriceN5WebElement().clear();
        getCoinItemPriceN5WebElement().sendKeys(value);
    }

    public void setCoinItemPriceN5LabelWebElement(String value)
    {
        getCoinItemPriceN5LabelWebElement().clear();
        getCoinItemPriceN5LabelWebElement().sendKeys(value);
    }

    public void setCoinItemPriceN6WebElement(String value)
    {
        getCoinItemPriceN6WebElement().clear();
        getCoinItemPriceN6WebElement().sendKeys(value);
    }

    public void setCoinItemPriceN6LabelWebElement(String value)
    {
        getCoinItemPriceN6LabelWebElement().clear();
        getCoinItemPriceN6LabelWebElement().sendKeys(value);
    }

    public void setCoinItemPriceN7WebElement(String value)
    {
        getCoinItemPriceN7WebElement().clear();
        getCoinItemPriceN7WebElement().sendKeys(value);
    }

    public void setCoinItemPriceN7LabelWebElement(String value)
    {
        getCoinItemPriceN7LabelWebElement().clear();
        getCoinItemPriceN7LabelWebElement().sendKeys(value);
    }

    public void setCoinItemPriceN8WebElement(String value)
    {
        getCoinItemPriceN8WebElement().clear();
        getCoinItemPriceN8WebElement().sendKeys(value);
    }

    public void setCoinItemPriceN8LabelWebElement(String value)
    {
        getCoinItemPriceN8LabelWebElement().clear();
        getCoinItemPriceN8LabelWebElement().sendKeys(value);
    }

    public void setPulseTimeValueWebElement(String value)
    {
        getPulseTimeValueWebElement().clear();
        getPulseTimeValueWebElement().sendKeys(value);
    }

    public void setTopOffValueWebElement(String value)
    {
        getTopOffValueWebElement().clear();
        getTopOffValueWebElement().sendKeys(value);
    }

    public void setPulseCaptureValueWebElement(String value)
    {
        getPulseCaptureValueWebElement().clear();
        getPulseCaptureValueWebElement().sendKeys(value);
    }

    public void setComboboxValue(String comboLabel, String value) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@label='" + comboLabel + "']//option[contains(text(),'" + value + "')]"))).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@label='" + comboLabel + "']//option[contains(text(),'" + value + "')]"))).sendKeys(Keys.ENTER);
    }

    public void save() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@value='Save and Send']"))).click();
    }

    public void verifySuccessUpdateMessage() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//li[text()='Configuration update scheduled' or text()='No configuration changes']")));
    }
}
