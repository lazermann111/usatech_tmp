package pages.UsaLive.Administration.Users;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.UsaLive.Administration.TransportDialog;

import java.util.List;

/**
 * Created by ahachikyan on 3/2/2016.
 */
public class UsersPage {
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(UsersPage.class);

    public UsersPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    public void setReportName(String reportName) {
        //wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@id='reportId']"))).sendKeys(reportName);
//        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@id='reportId']"))).sendKeys(reportName);
//        String s = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@id='reportId']"))).getText();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@id='reportId']/option[text()='" + reportName + "']"))).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@id='reportId']/option[text()='" + reportName + "']"))).sendKeys(Keys.ENTER);
    }

    public void searchUserByName(String userName) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='userNameSearch']"))).clear();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='userNameSearch']"))).sendKeys(userName);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='submit' and @value='Search']"))).click();
    }

    public void clickUserItemInList(String userName) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@id='userId']/option[contains(text(),'" + userName + "')]"))).click();
    }

    public void clickDeleteUserButton() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='button' and @value='Delete User']"))).click();
    }

    public boolean isUserItemPresentInList(String userName) {
        return !driver.findElements(By.xpath("//select[@id='userId']/option[contains(text(),'" + userName + "')]")).isEmpty();
    }

    public void deleteUserByName(String userName) throws Exception {
        searchUserByName(userName);
        if(isUserItemPresentInList(userName)) {
            clickUserItemInList(userName);
            clickDeleteUserButton();
            driver.switchTo().alert().accept();
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[text()='Successfully deleted user.']")));
        }
    }

    public void setUserName(String value) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='userNameField']"))).sendKeys(value);
    }

    public void clickAddUserButton() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='button' and @value='Add User']"))).click();
            }

    public void setFirstName(String value) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='editFirstName']"))).sendKeys(value);
    }

    public void setLastName(String value) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='editLastName']"))).sendKeys(value);
    }

    public void setPassword(String value) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='editPassword']"))).sendKeys(value);
    }

    public void setConfirmPassword(String value) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='editConfirm']"))).sendKeys(value);
    }

    public void setEmail(String value) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='editEmail']"))).sendKeys(value);
    }

    public void setTelephone(String value) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='editTelephone']"))).sendKeys(value);
    }

    public void clickSaveChangesButton() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='submit' and @value='Save Changes']"))).click();
    }

    public void checkComboBoxByLabel(String label) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//td[text()='" + label + "']/following-sibling::td/input"))));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//td[text()='" + label + "']/following-sibling::td/input"))).click();

//        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//td[text()='" + label + "']/following-sibling::td/input[@type='checkbox' and @internalexternalflag='B']")));
    }

    public void verifyComboboxChecked(String comboboxLabelText, boolean isExpectedChecked) {
        if(isExpectedChecked)
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//td[text()='" + comboboxLabelText + "']/following-sibling::td/input[@type='checkbox' and @internalexternalflag='B']")));
        else
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//td[text()='" + comboboxLabelText + "']/following-sibling::td/input[@type='checkbox' and @internalexternalflag='I' or @internalexternalflag='E']")));

    }

    public void clickLogInAsButton() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='button' and @value='Login As']"))).click();
    }
}
