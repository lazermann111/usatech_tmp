package pages.UsaLive.Reports.SavedReports;

import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import helper.CompareUtils;

import org.junit.Assert;

import pages.UsaLive.Reports.ReportsPage;

public class SavedReportsPage extends ReportsPage {

	private static final Logger logger = Logger.getLogger(SavedReportsPage.class);

	@FindBys({ @FindBy(xpath = "//ul[@class='cannedReportList']//a") })
	private static List<WebElement> reportNames;
	@FindBy(xpath = "//input[@id='startDateId' or @id='beginDateId' or @id='startDate' or @label='StartDate']")
	private static WebElement startDate;
	@FindBy(xpath = "//input[@name='endDate' or @id='endDateId' or @label='EndDate']")
	private static WebElement endDate;
	@FindBy(xpath = "//input[@value='Run Report']")
	private static WebElement submitReportButton;
	@FindBy(xpath = "//ul[@id='terminalsTreeCustomerAll']//a[@class='treeLabel']")
	private static WebElement deviceTree;
	@FindBy(xpath = "//input[@id='selectAllCheckbox']")
	private static WebElement selectAllDevicesCheckbox;
	@FindBy(xpath = "//label[@id='report-button-html']/input")
	private static WebElement HTMLReportCheckBox;	
	@FindBy(xpath = "//a[text()='year to date']")
	private static WebElement yearToDateTab;
	@FindBy(xpath = "//a[text()='sales analysis']")
	private static WebElement salesAnalysisTab;
	@FindBy(xpath = "//a[text()='YTD Sales By Day of Week For All Devices']")
	private static WebElement salesByDayLink;
	@FindBy(xpath = "//a[text()='daily operations']")
	private static WebElement dailyOperationsTab;

	public SavedReportsPage(WebDriver driver) {
		super(driver);
	}

	public void clickTransactionSummariesTab() {
		logger.info("Clicking Transaction Summaries tab...");
		wait.until(ExpectedConditions
				.presenceOfElementLocated(By.xpath("//div[@class='tabnav']//a[text()='Transaction Summaries']")))
				.click();
		// wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='tabnav']//span[@class='selectedTab']//a[text()='User
		// Defined']"))).click();
		wait.until(ExpectedConditions.presenceOfElementLocated(
				By.xpath("//div[@class='tabnav']//span[@class='selectedTab']//a[text()='Transaction Summaries']")));
		logger.info("USER DEFINED tab is opened");
	}

	public void clickUserDefinedTab() {
		logger.info("Clicking USER DEFINED tab...");
		wait.until(ExpectedConditions
				.presenceOfElementLocated(By.xpath("//div[@id='tabContainer']//a[text()='user defined']"))).click();
		// wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='tabnav']//span[@class='selectedTab']//a[text()='User
		// Defined']"))).click();
		wait.until(ExpectedConditions.presenceOfElementLocated(
				By.xpath("//div[@id='tabContainer']//li[@class='topMenuItem topMenuItemSelected']//a[text()='user defined']")));
		logger.info("USER DEFINED tab is opened");
	}

	public void clickDailyOperationsTab() {
		logger.info("Clicking Daily Operations tab...");
//		wait.until(ExpectedConditions
//				.presenceOfElementLocated(By.xpath("//div[@class='tabnav']//a[text()='Daily Operations']"))).click();
//		wait.until(ExpectedConditions.presenceOfElementLocated(
//				By.xpath("//div[@class='tabnav']//span[@class='selectedTab']//a[text()='Daily Operations']")));
		dailyOperationsTab.click();
		logger.info("Daily Operations tab is opened");
	}

	public void clickReportRegisterReportsTab() {
		logger.info("Clicking Report Register Reports tab...");
		wait.until(ExpectedConditions
				.presenceOfElementLocated(By.xpath("//div[@id='tabContainer']//a[text()='report register']")))
				.click();
		wait.until(ExpectedConditions.presenceOfElementLocated(
				By.xpath("//div[@id='tabContainer']//li[@class='topMenuItem topMenuItemSelected']//a[text()='report register']")));
		logger.info("Report Register Reports tab is opened");
	}

	public void clickAccountingTab() {
		logger.info("Clicking Accounting tab...");
		wait.until(
				ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='tabContainer']//a[text()='accounting']")))
				.click();
		wait.until(ExpectedConditions.presenceOfElementLocated(
				By.xpath("//div[@id='tabContainer']//li[@class='topMenuItem topMenuItemSelected']//a[text()='accounting']")));
		logger.info("Accounting tab is opened");
	}
	
	public SavedReportsPage clickYearToDateTab() {
		logger.info("Clicking Year To Date tab...");
		yearToDateTab.click();
//		wait.until(ExpectedConditions.presenceOfElementLocated(
//				By.xpath("//div[@class='tabnav']//span[@class='selectedTab']//a[text()='Accounting']")));
		logger.info("Accounting tab is opened");
		return this;
	}
	
	public SavedReportsPage clickSalesAnalysisTab() {
		logger.info("Clicking Sales Analysis tab...");
		salesAnalysisTab.click();
//		wait.until(ExpectedConditions.presenceOfElementLocated(
//				By.xpath("//div[@class='tabnav']//span[@class='selectedTab']//a[text()='Accounting']")));
		logger.info("Sales Analysis tab is opened");
		return this;
	}

	public void verifyReportCaptionVisibility(String reportName) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//caption[text()='" + reportName + "']")));
	}

	public void clickByReportName(String name) {

		for (WebElement obj : reportNames) {
			if (obj.getText().equals(name)) {
				obj.click();
				break;
			}
		}
	}

	public void setStartDate(String date) {
		startDate.clear();
		startDate.sendKeys(date);
	}

	public void setEndDate(String date) {
		endDate.clear();
		endDate.sendKeys(date);
	}

	public Integer runSalesRollupReport() throws IOException {
		String testGroupFolder = "dailyOperations/";
		String reportName = "Sales Rollup";
		submitReportButton.click();
		Instant startTime = Instant.now();
		CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
		Long period = ChronoUnit.MILLIS.between(startTime, Instant.now());
		return period.intValue();
		// System.out.println("Report generated: "+ period +" milliseconds");
	}
	
	public Integer runYearToDateByDayReport() throws IOException {
		String testGroupFolder = "yeartodate/";
		String reportName = "YTD Sales By Day of Week For All Devices";
//		clickByReportName("YTD Sales By Day of Week For All Devices");
		runHtmlReportUsingModifiedUrl("YTD Sales By Day of Week For All Devices", "01/01/2017", "31/12/2017", "YTD Sales By Day of Week For All Devices - All from January 01, 2017 00:00 to December 31, 2017 00:00");
		Instant startTime = Instant.now();
		CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
		Long period = ChronoUnit.MILLIS.between(startTime, Instant.now());
		return period.intValue();
		// System.out.println("Report generated: "+ period +" milliseconds");
	}
	
	public Integer runYearToDateByHourReport() throws IOException {
		String testGroupFolder = "yeartodate/";
		String reportName = "YTD Sales By Hour of Day For All Devices";
//		clickByReportName("YTD Sales By Day of Week For All Devices");
		runHtmlReportUsingModifiedUrl("YTD Sales By Hour of Day For All Devices", "01/01/2017", "31/12/2017", "Sales By Hour - January 1, 2017 to December 31, 2017");
		Instant startTime = Instant.now();
		CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
		Long period = ChronoUnit.MILLIS.between(startTime, Instant.now());
		return period.intValue();
		// System.out.println("Report generated: "+ period +" milliseconds");
	}
	
	public Integer runYearToDateForEachReport() throws IOException {
		String testGroupFolder = "yeartodate/";
		String reportName = "YTD Sales for each Device By Transaction Type";
//		clickByReportName("YTD Sales By Day of Week For All Devices");
		runHtmlReportUsingModifiedUrl("YTD Sales for each Device By Transaction Type", "01/01/2017", "31/12/2017", "YTD Sales for each Device By Transaction Type - All from January 01, 2017 00:00 to December 31, 2017 00:00");
		Instant startTime = Instant.now();
		CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
		Long period = ChronoUnit.MILLIS.between(startTime, Instant.now());
		return period.intValue();
		// System.out.println("Report generated: "+ period +" milliseconds");
	}
	
	public Integer runYearToDateSummaryReport() throws IOException {
		String testGroupFolder = "yeartodate/";
		String reportName = "YTD Sales Summary";
//		clickByReportName("YTD Sales By Day of Week For All Devices");
		runHtmlReportUsingModifiedUrl("YTD Sales Summary", "01/01/2017", "31/12/2017", "YTD Sales Summary - All from January 01, 2017 00:00 to December 31, 2017 00:00");
		Instant startTime = Instant.now();
		CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
		Long period = ChronoUnit.MILLIS.between(startTime, Instant.now());
		return period.intValue();
		// System.out.println("Report generated: "+ period +" milliseconds");
	}
	
	public Integer runCashCreditAnalysisReport() throws IOException {
		String testGroupFolder = "salesanalysis/";
		String reportName = "Cash vs. Credit Analysis";
//		clickByReportName("YTD Sales By Day of Week For All Devices");
		runHtmlReportUsingModifiedUrl("Cash vs. Credit Analysis", "01/01/2017", "01/31/2017", "Cash vs. Credit Analysis - All from January, 2017 to January, 2017");
		Instant startTime = Instant.now();
		CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
		Long period = ChronoUnit.MILLIS.between(startTime, Instant.now());
		return period.intValue();
		// System.out.println("Report generated: "+ period +" milliseconds");
	}
	
	public Integer runLast60DaysAnalysisReport() throws IOException {
		String testGroupFolder = "salesanalysis/";
		String reportName = "Device vs. Device (Last 60 Days)";
//		clickByReportName("YTD Sales By Day of Week For All Devices");
		runHtmlReportUsingModifiedUrl("Device vs. Device (Last 60 Days)", "01/01/2017", "02/28/2017", "Device vs. Device (Last 60 Days) - All from January 01, 2017 00:00 to February 28, 2017 00:00");
		Instant startTime = Instant.now();
		CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
		Long period = ChronoUnit.MILLIS.between(startTime, Instant.now());
		return period.intValue();
		// System.out.println("Report generated: "+ period +" milliseconds");
	}

	public Integer runHottestItemsReport() throws IOException {
		String testGroupFolder = "salesanalysis/";
		String reportName = "Hottest Items";
//		clickByReportName("YTD Sales By Day of Week For All Devices");
		runHtmlReportUsingModifiedUrl("Hottest Items", "01/01/2017", "01/31/2017", "Hottest Items - All from January, 2017 to January, 2017");
		Instant startTime = Instant.now();
		CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
		Long period = ChronoUnit.MILLIS.between(startTime, Instant.now());
		return period.intValue();
		// System.out.println("Report generated: "+ period +" milliseconds");
	}
	
	public Integer runTransactionTrendsReport() throws IOException {
		String testGroupFolder = "salesanalysis/";
		String reportName = "Transaction Trends";
		clickByReportName("Transaction Trends");
//		runHtmlReportUsingModifiedUrl("Transaction Trends", "01/01/2017", "01/31/2017", "Hottest Items - All from January, 2017 to January, 2017");
		new TransactionTotalsPage(driver).generateGraphAllTransactionTypes();
		Instant startTime = Instant.now();
		CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
		Long period = ChronoUnit.MILLIS.between(startTime, Instant.now());
		return period.intValue();
		// System.out.println("Report generated: "+ period +" milliseconds");
	}
	
	public void selectAllDevices() {
		deviceTree.click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='params.customerIds']")));
		selectAllDevicesCheckbox.click();
	}

	public void selectHTMLReport() {
		HTMLReportCheckBox.click();
	}
}
