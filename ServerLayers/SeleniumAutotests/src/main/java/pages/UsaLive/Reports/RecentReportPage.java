package pages.UsaLive.Reports;

import java.io.IOException;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import javax.xml.parsers.ParserConfigurationException;

import org.custommonkey.xmlunit.exceptions.XpathException;
import org.dbunit.DatabaseUnitException;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.xml.sax.SAXException;

import helper.CompareUtils;

public class RecentReportPage extends ReportsPage {

    @FindBy(xpath = "//div[@class='title2']")
    private static WebElement pageTitle;
    @FindBy(xpath = "//div[@class='title2']")
    private static WebElement reportHeader;
    @FindBy(xpath = "//div[@class='title2']")
    private static WebElement reportContent;
    @FindBy(xpath = "//button[@title='Refresh Data']")
    private static WebElement refreshCurrentButton;
    @FindBy(xpath = "//button[@title='Configure Parameters']")
    private static WebElement configureParametrsButton;
    @FindBy(xpath = "//input[@value='Run Report']")
    private static WebElement runReportButton;
    @FindBy(xpath = "//button[@title='View in PDF']")
    private static WebElement runPDFReportButton;
    @FindBy(xpath = "//button[@title='View in Word Document']")
    private static WebElement runWordReportButton;
    @FindBys({ @FindBy(xpath = "//a[@title='View Report']") })
    private static List<WebElement> viewLinks;
    @FindBys({ @FindBy(xpath = "//table[@class='folio']//input[@value='Refresh Data']") })
    private static List<WebElement> refreshButtons;
    @FindBy(xpath = "//button[@title='View in Excel']")
    private static WebElement runExcelReportButton;
    @FindBy(xpath = "//button[@title='View in Comma-separated Values']")
    private static WebElement runCSVReportButton;


    public RecentReportPage(WebDriver driver) {
	super(driver);
    }

    public void runYearToDateByDayReport() throws IOException {

	runHtmlReportUsingModifiedUrl("YTD Sales By Day of Week For All Devices", "01/01/2017", "31/12/2017",
		"YTD Sales By Day of Week For All Devices - All from January 01, 2017 00:00 to December 31, 2017 00:00");

    }

    public String getPageTitle() throws IOException {

	return pageTitle.getText();

    }

    public void openRandomRecentReport() throws IOException {

	int randomNum = ThreadLocalRandom.current()
					 .nextInt(0, viewLinks.size());
	viewLinks.get(randomNum)
		 .click();

    }

    public Boolean isReportHeaderExist() throws IOException {

	return reportHeader.isDisplayed();

    }

    public Boolean isReportContentExist() throws IOException {

	return reportContent.isDisplayed();

    }

    public void openLastReport() throws IOException {

	viewLinks.get(0)
		      .click();

    }

    public void clickRefreshCurrentButton() throws IOException {

	refreshCurrentButton.click();

    }

    public void clickConfigureParametrsButton() throws IOException {

	configureParametrsButton.click();
	;

    }

    public void runConfiguredReport() throws IOException {

	runReportButton.click();
	try {
	    driver.switchTo()
		  .alert()
		  .dismiss();
	} catch (NoAlertPresentException e) {
	    // ничего не делаем, алерта итак нет
	}

    }

    public void runPDFReport() throws IOException, URISyntaxException {
	runPDFReportButton.click();
	CompareUtils.verifyDownloadedPdf("Recent PDF Report", 120, "recentReports/");
    }
    
    public void runWordReport() throws IOException, URISyntaxException, XpathException, SAXException, ParserConfigurationException {
	runWordReportButton.click();
	CompareUtils.verifyDownloadedDoc("Recent Word Report", "recentReports/");
    }
    
    public void runExcelReport() throws IOException, URISyntaxException, XpathException, SAXException, ParserConfigurationException, DatabaseUnitException {
	runExcelReportButton.click();
	CompareUtils.verifyDownloadedXls("Recent Excel Report", "recentReports/");
    }
    
    public void runCSVReport() throws IOException, URISyntaxException, XpathException, SAXException, ParserConfigurationException {
	runCSVReportButton.click();
	CompareUtils.verifyDownloadedCsv("Recent Excel Report", "recentReports/");
    }

}
