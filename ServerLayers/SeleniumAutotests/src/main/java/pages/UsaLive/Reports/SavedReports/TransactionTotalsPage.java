package pages.UsaLive.Reports.SavedReports;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import pages.UsaLive.Reports.ReportsPage;

public class TransactionTotalsPage extends ReportsPage {
    
	private static final Logger logger = Logger.getLogger(SavedReportsPage.class);
    
	@FindBy(xpath = "//input[@id='startDateId']")
	private static WebElement startDate;
	@FindBy(xpath = "//input[@id='endDateId']")
	private static WebElement endtDate;
	@FindBy(xpath = "//input[@value='Select All']")
	private static WebElement selectAllButton;//input[@value='Create Graph']
	@FindBy(xpath = "//input[@value='Create Graph']")
	private static WebElement createGraph;
	
	public TransactionTotalsPage(WebDriver driver) {
		super(driver);	
	}
	
	public void generateGraphAllTransactionTypes() {
	    startDate.clear();
	    startDate.sendKeys("01/01/2017");
	    endtDate.clear();
	    endtDate.sendKeys("01/31/2017");
	    selectAllButton.click();
	    createGraph.click();
	}
}
