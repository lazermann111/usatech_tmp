package pages.UsaLive.Reports.BuildReport;

import java.io.Console;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import org.omg.CORBA.CTX_RESTRICT_SCOPE;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import helper.CompareUtils;
import helper.Core;

public class BuildReportPage extends Core {

    @FindBy(xpath = "//a[text()='summary report']")
    private static WebElement summaryReportTab;
    @FindBy(xpath = "//a[text()='detailed report']")
    private static WebElement detailedReportTab;
    @FindBy(xpath = "//a[text()='bar graph report']")
    private static WebElement barGraphReportTab;
    @FindBy(xpath = "//a[text()='sales rollup report']")
    private static WebElement salesRollupReportTab;
    @FindBy(xpath = "//select[@id='beginMonth']")
    private static WebElement summaryReportBeginMonth;
    @FindBy(xpath = "//select[@id='beginDay']")
    private static WebElement summaryReportBeginDay;
    @FindBy(xpath = "//select[@id='beginYear']")
    private static WebElement summaryReportBeginYear;
    @FindBy(xpath = "//select[@id='endMonth']")
    private static WebElement summaryReportEndMonth;
    @FindBy(xpath = "//select[@id='endDay']")
    private static WebElement summaryReportEndDay;
    @FindBy(xpath = "//select[@id='endYear']")
    private static WebElement summaryReportEndYear;
    @FindBy(xpath = "//td[text()='Total By:']//..//select[@name='sortBy']")
    private static WebElement columnsTotalBy;
    @FindBy(xpath = "//input[@title='Toggle Data Values']")
    private static WebElement checkBoxAllDataValues;
    @FindBy(xpath = "//ul[@id='terminalsTreeCustomerAll']//a[@class='treeLabel']")
    private static WebElement deviceSearchTree;
    @FindAll({
	    @FindBy(xpath = "//ul[@id='terminalsTreeCustomerAll']//input"),
	    @FindBy(xpath = "//input[@id='selectAllCheckbox']")
    })
    private static WebElement deviceSearchTreeCheckbox;
    @FindBy(xpath = "//input[@id='terminals']")
    private static WebElement deviceSearchTreeCheckboxFiltered;
    @FindBy(xpath = "//input[@value='Run Report']")
    private static WebElement submitReportButton;
    @FindBy(xpath = "//a[text()='simple report']")
    private static WebElement simpleReportTab;
    @FindBy(xpath = "//select[@id='rangeType']")
    private static WebElement simpleRangeTypeSelect;
    @FindBy(xpath = "//input[@id='startDateId']")
    private static WebElement startDate;
    @FindBy(xpath = "//input[@id='endDateId']")
    private static WebElement endDate;
    @FindBy(xpath = "//label[@id='report-button-html']/input")
    private static WebElement reportFormatHtmlRadioButton;
    @FindBys({ @FindBy(xpath = "//td[input[@name='showValues']]") })
    private static List<WebElement> dataValuesRadioButtonText;
    @FindBys({ @FindBy(xpath = "//input[@name='showValues']") })
    private static List<WebElement> dataValuesRadioButton;
    @FindBys({ @FindBy(xpath = "//input[@name='params.customerIds']/../a") })
    private static  List<WebElement> deviceCheckboxLabel;
    @FindBys({ @FindBy(xpath = "//input[@name='params.customerIds']") })
    private static  List<WebElement> deviceCheckbox;
    @FindBys({ @FindBy(xpath = "//input[@id='terminals']") })
    private static  List<WebElement> deviceFilteredCheckbox;
    @FindBy(xpath = "//input[@id='searchText']")
    private static WebElement searchDeviceField;
    @FindBy(xpath = "//input[@value='Search']")
    private static WebElement searchDeviceButton;

    public BuildReportPage(WebDriver driver) {
	PageFactory.initElements(driver, this);
	this.driver = driver;
	wait = new WebDriverWait(driver, 30);
    }
    
   public void waitForDeviceSearchTreeExpanded() throws InterruptedException{
       wait.until(ExpectedConditions.visibilityOf(deviceCheckbox.get(0)));
   }
   


    public BuildReportPage switchToSummaryTab() {
	summaryReportTab.click();

	return this;
    }

    public BuildReportPage switchToSimpleTab() {
	simpleReportTab.click();

	return this;
    }

    public BuildReportPage switchToDetailedTab() {
	detailedReportTab.click();

	return this;
    }

    public BuildReportPage switchToBarGraphTab() {
	barGraphReportTab.click();
	return this;
    }

    public BuildReportPage switchToSalesRollupTab() {
	salesRollupReportTab.click();
	return this;
    }

    public BuildReportPage setStartDate(String date) {
	startDate.clear();
	startDate.sendKeys(date);
	return this;

    }

    // TODO add other formats
    public BuildReportPage setReportFormatType(String type) {
	switch (type) {
	case "html":
	    reportFormatHtmlRadioButton.click();
	    return this;

	default:
	    reportFormatHtmlRadioButton.click();
	    return this;
	}
    }

    public BuildReportPage setEndDate(String date) {
	endDate.clear();
	endDate.sendKeys(date);
	return this;

    }

    public BuildReportPage setReportBeginMonth(String month) {
	Select select = new Select(summaryReportBeginMonth);
	select.selectByVisibleText(month);
	return this;
    }

    public BuildReportPage setReportBeginDay(String day) {
	Select select = new Select(summaryReportBeginDay);
	select.selectByVisibleText(day);
	return this;
    }

    public BuildReportPage setReportBeginYear(String year) {
	Select select = new Select(summaryReportBeginYear);
	select.selectByVisibleText(year);

	return this;
    }

    public BuildReportPage setReportEndMonth(String month) {
	Select select = new Select(summaryReportEndMonth);
	select.selectByVisibleText(month);

	return this;
    }

    public BuildReportPage setRangeTypeSimpleReport(String rangeType) {
	Select select = new Select(simpleRangeTypeSelect);
	select.selectByVisibleText(rangeType);

	return this;
    }

    public BuildReportPage setReportEndDay(String day) {
	Select select = new Select(summaryReportEndDay);
	select.selectByVisibleText(day);
	return this;
    }

    public BuildReportPage setReportEndYear(String year) {
	Select select = new Select(summaryReportEndYear);
	select.selectByVisibleText(year);

	return this;
    }

    public BuildReportPage setColumnsTotalByValue(String value) {
	Select select = new Select(columnsTotalBy);
	select.selectByVisibleText(value);
	return this;
    }

    public BuildReportPage setAllDataValuesChecked() {
	if (!checkBoxAllDataValues.isSelected()) {
	    checkBoxAllDataValues.click();
	}
	return this;
    }
    
    

    public BuildReportPage expandDeviceTree() {
	deviceSearchTree.click();
	return this;
    }
    
    public BuildReportPage clickSubmitButton() {
	submitReportButton.click();
	return this;
    }

    public BuildReportPage setAllDeviceSearchTreeChecked() {
	if (!deviceSearchTreeCheckbox.isSelected()) {
	    deviceSearchTreeCheckbox.click();
	}
	return this;
    }
    
    public BuildReportPage setDeviceSearchTreeCheckedByCustomerName(String name) {

	for (int x=0; x<deviceCheckboxLabel.size(); x= x+1) {
	    logger.info(deviceCheckboxLabel.get(x).getText());
		if (deviceCheckboxLabel.get(x).getText().equals(name)) {
			deviceCheckbox.get(x).click();
			break;
		}
	}
	return this;
    }
    public BuildReportPage setDeviceSearchTreeCheckedByDeviceId(String name) {

	searchDeviceField.clear();
	searchDeviceField.sendKeys(name);
	searchDeviceButton.click();
	deviceSearchTreeCheckboxFiltered.click();
	return this;
    }

    public Integer runSummaryReport() throws IOException {
	String testGroupFolder = "buildReport/";
	String reportName = "Activity Summary";
	submitReportButton.click();
	Instant startTime = Instant.now();
	CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
	Long period = ChronoUnit.MILLIS.between(startTime, Instant.now());
	return period.intValue();
	// System.out.println("Report generated: "+ period +" milliseconds");
    }

    public Integer runSimpleReportByDay() throws IOException {
	String testGroupFolder = "buildReport/";
	String reportName = "Simple report by day";
	submitReportButton.click();
	Instant startTime = Instant.now();
	CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
	Long period = ChronoUnit.MILLIS.between(startTime, Instant.now());
	return period.intValue();
	// System.out.println("Report generated: "+ period +" milliseconds");
    }

    public Integer runSimpleReportByWeek() throws IOException {
	String testGroupFolder = "buildReport/";
	String reportName = "Simple report by week";
	submitReportButton.click();
	Instant startTime = Instant.now();
	CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
	Long period = ChronoUnit.MILLIS.between(startTime, Instant.now());
	return period.intValue();
	// System.out.println("Report generated: "+ period +" milliseconds");
    }

    public Integer runSimpleReportByMonth() throws IOException {
	String testGroupFolder = "buildReport/";
	String reportName = "Simple report by month";
	submitReportButton.click();
	Instant startTime = Instant.now();
	CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
	Long period = ChronoUnit.MILLIS.between(startTime, Instant.now());
	return period.intValue();
	// System.out.println("Report generated: "+ period +" milliseconds");
    }

    public Integer runSimpleReportEntireRange() throws IOException {
	String testGroupFolder = "buildReport/";
	String reportName = "Simple report entire range";
	submitReportButton.click();
	Instant startTime = Instant.now();
	CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
	Long period = ChronoUnit.MILLIS.between(startTime, Instant.now());
	return period.intValue();
	// System.out.println("Report generated: "+ period +" milliseconds");
    }

    public Integer runSimpleReportFillDate() throws IOException {
	String testGroupFolder = "buildReport/";
	String reportName = "Simple report fill date";
	submitReportButton.click();
	Instant startTime = Instant.now();
	CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
	Long period = ChronoUnit.MILLIS.between(startTime, Instant.now());
	return period.intValue();
	// System.out.println("Report generated: "+ period +" milliseconds");
    }

    public Integer runDetailedReport() throws IOException {
	String testGroupFolder = "buildReport/";
	String reportName = "Detailed report";
	submitReportButton.click();
	Instant startTime = Instant.now();
	CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
	Long period = ChronoUnit.MILLIS.between(startTime, Instant.now());
	return period.intValue();
	// System.out.println("Report generated: "+ period +" milliseconds");
    }

    public Integer runBarGraphReport() throws IOException {
	String testGroupFolder = "buildReport/";
	String reportName = "Bar Graph report";
	submitReportButton.click();
	Instant startTime = Instant.now();
	CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
	Long period = ChronoUnit.MILLIS.between(startTime, Instant.now());
	return period.intValue();
	// System.out.println("Report generated: "+ period +" milliseconds");
    }
    
    public Integer runSalesRollupReport() throws IOException {
	String testGroupFolder = "buildReport/";
	String reportName = "Sales Rollup report";
	submitReportButton.click();
	Instant startTime = Instant.now();
	CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
	Long period = ChronoUnit.MILLIS.between(startTime, Instant.now());
	return period.intValue();
	// System.out.println("Report generated: "+ period +" milliseconds");
    }
    
    public BuildReportPage setDataValuesRadioButton(String name) {

	for (int i = 0; i < dataValuesRadioButton.size(); i++) {
	    if (dataValuesRadioButtonText.get(i)
					 .getText()
					 .equals(name)) {
		dataValuesRadioButton.get(i)
				     .click();
	    }
	}
	return this;
    }
}
