package pages.UsaLive.Reports;

import helper.CompareUtils;
import helper.Core;
import helper.FileUtils;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ReportsPage extends Core {
    public final WebDriver driver;
    public final WebDriverWait wait;

    private static final Logger logger = Logger.getLogger(ReportsPage.class);

    public ReportsPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 60);
		PageFactory.initElements(driver, this);
    }

    private WebElement getNavigationElementByText(String textOfNavigationElement) {
        logger.info("Getting navigation element by text:" + textOfNavigationElement);
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='tabContentDiv selectedTabContentDiv']//a[text()='" + textOfNavigationElement + "']")));
    }

    public void runCsvReportUsingUrl(String reportName, String beginDate, String endDate)
    {
        runReportWithSpecificTypeUsingUrl(reportName, "csv", beginDate, endDate);
    }

    public void runPdfReportUsingUrl(String reportName, String beginDate, String endDate)
    {
        runReportWithSpecificTypeUsingUrl(reportName, "pdf", beginDate, endDate);
    }

    public void runXlsReportUsingUrl(String reportName, String beginDate, String endDate)
    {
        runReportWithSpecificTypeUsingUrl(reportName, "excel", beginDate, endDate);
    }
    public void downloadReportByLinkClicking(String reportName)
    {
        Integer filesCountBefore = new File(tempFolderForDownloads).list().length;
        clickReportLink(reportName);
        FileUtils.waitForFileToBeDownloaded(filesCountBefore);
//        Integer roundsCount = 0;
//        while(((new File(System.getProperty("java.io.tmpdir")).list().length) <= filesCountBefore) && roundsCount < 60 )
//        {
//            try {
//                //TODO:Remove debug info
//                logger.info ("Try #" + roundsCount);
//                Thread.sleep(1000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            roundsCount++;
//        }
//        //TODO: Change to waiting for file size increasing
//        if ((new File(System.getProperty("java.io.tmpdir")).list().length) <= filesCountBefore)
//        {
//            Assert.fail("Report has not been downloaded");
//        }
//        else
//        {
//            logger.info("Report has been successfully downloaded");
//            //Wait to avoid zero-size file manipulation
//            try
//            {
//                Thread.sleep(1000);
//            }
//            catch (InterruptedException e)
//            {
//                e.printStackTrace();
//            }
//        }
    }

    public void clickReportLink(String reportName) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='tabContentDiv selectedTabContentDiv']//a[text()='" + reportName + "']"))).click();
    }

    public void runDocReportUsingUrl(String reportName, String beginDate, String endDate)
    {
        runReportWithSpecificTypeUsingUrl(reportName, "doc", beginDate, endDate);
    }

    public void runHtmlReportUsingModifiedUrl(String reportName, String beginDate, String endDate, String expectedReportTitle)
    {
        WebElement reportLinkWebElement = getNavigationElementByText(reportName);
        String url = reportLinkWebElement.getAttribute("href");
        url = replaceParameterInUrl(url, "params.beginDate", beginDate);
        url = replaceParameterInUrl(url, "params.StartDate", beginDate);
        url = replaceParameterInUrl(url, "params.endDate", endDate);
        url = replaceParameterInUrl(url, "params.EndDate", endDate);
        logger.info("Opening URL:" + url);
        driver.get(url);
        verifyReportTitleVisibility(expectedReportTitle);
    }


    /**
     * @param reportName
     * @param beginDate
     * @param endDate
     * @param allDevices Tick All devices checkbox
     * @param format Could be 'excel', 'html', 'pdf', 'doc' or 'csv'
     */
    public Long runReportWithSettings(String reportName, String beginDate, String endDate, Boolean allDevices, String format)
    {

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='tabContentDiv selectedTabContentDiv']//a[text()='" + reportName + "']"))).click();

        if(beginDate != null )
        {
            setBeginDate(beginDate);
        }

        if(endDate != null)
        {
            setEndDate(endDate);
        }

        if(allDevices!=null)
        {
            setAllDevices();
        }

        if(format != null)
        {
            logger.info("Setting format to:" + format);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[contains(@id, '" + format + "')]/input[@type='radio']"))).click();
        }
        Integer filesCountBefore = new File(tempFolderForDownloads).list().length;
        logger.info("before report");
        Date d1 = new Date();
        submitReport();
        //if not html then wait for downloaded file
        if((format != null) && !(format.equals("html")))
        {
            FileUtils.waitForFileToBeDownloaded(filesCountBefore);
        }
        else if((format != null) && format.equals("html"))
        {
            verifyReportTitleVisibility(reportName);
        }
        Date d2 = new Date();
        long diffInMillies = d2.getTime() - d1.getTime();      
        //TimeUnit timeUnit = TimeUnit.SECONDS;
        Long reportSeconds = diffInMillies / 1000;
        logger.info(">>>>>" + reportSeconds + " seconds");
        return reportSeconds;
    }

    public static Integer getFilesCountInDownloadingDir()
    {
        return FileUtils.getFilesCountInDownloadingDir();
    }

    public void submitReport() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@type='submit']"))).click();
    }

    public void setAllDevices() {
        logger.info("Setting all devices");
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[@class='closedTreeNode']/a[1]"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='treeLabel' and text()='All']"))).click();      
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name = 'selectAllCheckbox']"))).click();
    }

    public void setBeginDate(String beginDate)
    {
        logger.info("Setting beginDate to:" + beginDate);
        WebElement beginDateWebElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='startDateId' or @id='beginDateId' or @id='startDate' or @label='StartDate']")));
        beginDateWebElement.click();
        beginDateWebElement.clear();
        beginDateWebElement.sendKeys(beginDate);
    }

    public void setBeginMonth(String beginMonth) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='beginMonth']"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='beginMonth']/option[text()='" + beginMonth + "']"))).click();
    }

    public void setBeginYear(String beginYear) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='beginYear']"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='beginYear']/option[text()='" + beginYear + "']"))).click();
    }

    public void setBeginDate(String[] beginDateArray)
    {
        logger.info("Setting beginDate to:" + beginDateArray);

        setBeginMonth(beginDateArray[0]);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='beginDay']"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='beginDay']/option[text()='" + beginDateArray[1] + "']"))).click();
       // beginDayWebElement.sendKeys(beginDateArray[1]);

        setBeginYear(beginDateArray[2]);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='beginTime']"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='beginTime']/option[text()='" + beginDateArray[3] + "']"))).click();
        //beginTimeWebElement.sendKeys(beginDateArray[3]);
    }

    public void setEndDate(String endDate)
    {
        logger.info("Setting endDate to:" + endDate);
        WebElement endDateWebElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='endDateId' or @name='endDate' or @id='endDate' or @label='EndDate']")));
        endDateWebElement.click();
        endDateWebElement.clear();
        endDateWebElement.sendKeys(endDate);
    }
    
    public void setCardId(String cardId)
    {
        logger.info("Setting cardId to:" + cardId);
        WebElement cardIdWebElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@label='CardId']")));
        cardIdWebElement.click();
        cardIdWebElement.clear();
        cardIdWebElement.sendKeys(cardId);
    }

    public void setEndMonth(String endMonth)
    {
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='endMonth']"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='endMonth']/option[text()='" + endMonth + "']"))).click();
    }

    public void setEndYear(String endYear)
    {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='endYear']"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='endYear']/option[text()='" + endYear + "']"))).click();
    }

    public void setMonth(String month)
    {
        WebElement monthWebElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='month']")));
        monthWebElement.sendKeys(month);
    }

    public void setYear(String year)
    {
        WebElement yearWebElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='year']")));
        yearWebElement.clear();
        yearWebElement.sendKeys(year);
    }

    public void setEndDate(String[] endDateArray)
    {
        logger.info("Setting endDate to:" + endDateArray);
        setEndMonth(endDateArray[0]);

         wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='endDay']"))).click();
         wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='endDay']/option[text()='" + endDateArray[1] + "']"))).click();

        setEndYear(endDateArray[2]);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='endTime']"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='endTime']/option[text()='" + endDateArray[3] + "']"))).click();
    }

    public void verifyReportTitleVisibility(String reportName)
    {

        wait.withTimeout(180,  TimeUnit.SECONDS).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(@title, '" + reportName + "') or text()='" + reportName + "'] ")));
    }

    public WebElement getReportIconByType(String reportName, String reportType)
    {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='tabContentDiv selectedTabContentDiv']//a[text()='" + reportName + "']/..//a[@class='report-link-" + reportType + "']")));
    }

    public void runReportWithSpecificTypeUsingUrl(String reportName, String reportType, String beginDate, String endDate)
    {
        WebElement reportLinkWebElement = getReportIconByType(reportName, reportType);
        String url = reportLinkWebElement .getAttribute("href");
        url = replaceParameterInUrl(url, "params.beginDate", beginDate);
        url = replaceParameterInUrl(url, "params.endDate", endDate);
        logger.info("Running report with opening URL:" + url);

        Integer filesCountBefore = new File(tempFolderForDownloads).list().length;  
        
        //TODO: Remove this workaround. In some reason gecko does not work fine with downloading file by URL
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(1, TimeUnit.SECONDS);
        
        try 
        {   
        	
        	driver.get(url);
        	
        }
        catch(Exception e)
        {}
//        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        FileUtils.waitForFileToBeDownloaded(filesCountBefore);
    }

    protected String replaceParameterInUrl(String url, String parameterName, String replacementValue)
    {
        String[] splittedUrl = url.split("&");
        String resultUrl = "";
        for(Integer x = 0; x < splittedUrl.length; x++)
        {
            if(splittedUrl[x].startsWith(parameterName))
            {
                splittedUrl[x]= parameterName + "=" + replacementValue;
            }
            if(x==0)
            {
                resultUrl += splittedUrl[x];
            }
            else
            {
                resultUrl += "&" + splittedUrl[x];
            }
        }
        return resultUrl;
    }

    public void verifyReportPngUsingUrl(String reportName, String testGroupFolder) throws Exception {
        verifyReportPngUsingUrl(reportName, reportName, testGroupFolder);
    }

    public void verifyReportPngUsingUrl(String reportName, String expectedReportTitle, String testGroupFolder) throws Exception {
        runHtmlReportUsingModifiedUrl(reportName, "01/01/2015", "03/03/2015", expectedReportTitle);
        CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
    }

    public void verifyLabelPresence(String labelText) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='groupLabel' and contains(text(),'" + labelText + ":')]")));
    }
    
	public void clickRunReport() {
		 wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@value='Run Report']"))).click();
		
	}

	public void clickRefreshDataButton() {
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@id='report-button-refresh']"))).click();
		
	}

	public void verifyNoDataFoundMessagePresence() {
		 wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//td[text()='No data found']")));
		
	}

	public void verifyCardNumber(String maskedCC_DATA) {
		String currentValue = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[text()='Card Number:']/span[@class='groupValue']/span"))).getText();
		Assert.assertEquals(maskedCC_DATA, currentValue);
	}
	
	public void verifyLocation(String location) {
		String currentValue = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[text()='Location:']/span[@class='groupValue']/span"))).getText();
		Assert.assertEquals(location, currentValue);
	}
	
	public void verifyTableValue(Integer rowIdex, String columnName, String expectedValue) {
		String currentValue = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//tr[contains(@class,'Row')][" + rowIdex + "]/td[@data-sort-value][count((//a[text()='" + columnName + "'])[1]/../preceding-sibling::th)+1]"))).getText();
		Assert.assertEquals(expectedValue, currentValue);
	}

	// 1-st param is header of the table. There could be several tables on 1 page so it is required to split data.
	public void verifyTableValue(String tableHeader, String columnName, String expectedValue) {
		List<String> valuesList = new ArrayList<String>();

		List<WebElement> currenwebElementsList = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//tbody//span[text()='" + tableHeader + "']/../../../../../../../../../following-sibling::tbody[2]//tr[contains(@class,'Row')]/td[@data-sort-value][count((//a[text()='" + columnName + "'])[1]/../preceding-sibling::th)+1]")));
//		List<WebElement> currenwebElementsList = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//tr[contains(@class,'Row')]/td[@data-sort-value][count((//a[text()='" + columnName + "'])[1]/../preceding-sibling::th)+1]")));
		
		for (int i = 0; i< currenwebElementsList.size(); i++)
		{
			valuesList.add(currenwebElementsList.get(i).getText());
		}
		Assert.assertTrue("Expected value (" + expectedValue + ") is not contain in current list:" + valuesList, valuesList.contains(expectedValue));

	}
	
	public void verifyTableValueContains(Integer rowIdex, String columnName, String expectedValue) {
		String currentValue = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//tr[contains(@class,'Row')][" + rowIdex + "]/td[@data-sort-value][count((//a[text()='" + columnName + "'])[1]/../preceding-sibling::th)+1]"))).getText();
		Assert.assertTrue("Current value (" + currentValue + ") is not countain expected value (" + expectedValue + ")", currentValue.contains(expectedValue));
	}
	
	public void verifyTableValueContains(String deviceSerialNumber, String columnName, String expectedValue) {		
		List<WebElement> currenwebElementsList = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//tbody//span[text()='" + deviceSerialNumber + "']/../../../../../../../../../following-sibling::tbody[2]//tr[contains(@class,'Row')]/td[@data-sort-value][count((//a[text()='" + columnName + "'])[1]/../preceding-sibling::th)+1]")));
		for (int i = 0; i< currenwebElementsList.size(); i++)
		{
			String currentValue = currenwebElementsList.get(i).getText();
			if (currentValue.contains(expectedValue))
				return;
			//else i++;
			
		}
		Assert.fail("Expected value (" + expectedValue + ") is not contain in current list");
	}
}
