package pages.UsaLive;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import helper.Core;

public class TopPanel extends Core {
    @FindBy(linkText = "general")
    private static WebElement generalButton;
    @FindBy(linkText = "administration")
    private static WebElement administrationButton;
    @FindBy(linkText = "configuration")
    private static WebElement configurationButton;
    @FindBy(linkText = "reports")
    private static WebElement reportsButton;
    @FindBy(linkText = "logout")
    private static WebElement logoutButton;
    @FindBy(linkText = "home")
    private static WebElement homeButton;
    @FindBy(linkText = "customers")
    private static WebElement customersButton;
    @FindBy(linkText = "dashboard")
    private static WebElement dashboardButton;
    // @FindBy(linkText = "devices")
    @FindBy(linkText = "devices")
    private static WebElement devicesButton;
    @FindBy(linkText = "users")
    private static WebElement usersButton;
    @FindBy(linkText = "change password")
    private static WebElement changepasswordButton;
    @FindBy(linkText = "admin refunds")
    private static WebElement adminRefundsButton;
    @FindBy(linkText = "chargebacks")
    private static WebElement chargebacksButton;
    @FindBy(linkText = "data exchange")
    private static WebElement dataExchangeButton;
    @FindBy(linkText = "new customer")
    private static WebElement newCustomerButton;
    @FindBy(linkText = "refunds")
    private static WebElement refundsButton;
    @FindBy(linkText = "welcome info")
    private static WebElement welcomeInfoButton;
    @FindBy(linkText = "parts")
    private static WebElement partsButton;
    @FindBy(linkText = "rma")
    private static WebElement rmaButton;
    @FindBy(xpath = "//input[@value='Create Parts RMA']")
    private static WebElement createPartsRmaButton;
    @FindBy(xpath = "//input[@value='View RMA']")
    private static WebElement viewRmaButton;
    @FindBy(xpath = "//input[@value='Create RMA']")
    private static WebElement createRmaButton;
    @FindBy(linkText = "campaings")
    private static WebElement campaingsButton;
    @FindBy(linkText = "column maps")
    private static WebElement columnMapsButton;
    @FindBy(linkText = "consumers")
    private static WebElement consumersButton;
    @FindBy(xpath = "//input[@value='Mass Card Update']")
    private static WebElement massCardUpdateButton;
    @FindBy(xpath = "//input[@value='Mass Device Configuration']")
    private static WebElement massDeviceConfigurationButton;
    @FindBy(xpath = "//input[@value='Mass Device Update']")
    private static WebElement massDeviceUpdateButton;
    @FindBy(linkText = "preferences")
    private static WebElement preferencesButton;
    @FindBy(linkText = "build a report")
    private static WebElement buildReportButton;
    @FindBy(linkText = "dex status")
    private static WebElement dexStatusButton;
    @FindBy(linkText = "diagnostics")
    private static WebElement diagnosticsButton;
    @FindBy(linkText = "folio designer")
    private static WebElement folioDesignerButton;
    @FindBy(linkText = "payment detail")
    private static WebElement paymentDetailButton;
    @FindBy(linkText = "recent reports")
    private static WebElement recentReportsButton;
    @FindBy(linkText = "report register")
    private static WebElement reportRegisterButton;
    @FindBy(linkText = "saved reports")
    private static WebElement savedReportsButton;
    @FindBy(linkText = "login to original")
    private static WebElement loginOriginalButton;
    @FindBy(linkText = "mass updates")
    private static WebElement massUpdatesButton;

    private static final Logger logger = Logger.getLogger(LogInPage.class);

    public General general = new General();
    public Administration administration = new Administration();
    public Configuration configuration = new Configuration();
    public Reports reports = new Reports();

    public TopPanel(WebDriver driver) {
	PageFactory.initElements(new AjaxElementLocatorFactory(driver, 90), this);
	this.driver = driver;
	wait = new WebDriverWait(driver, 30);
    }

    public class General {

	public Users users = new Users();
	public Customers customers = new Customers();
	public Logout logout = new Logout();
	public Dashboard dashboard = new Dashboard();
	public Home home = new Home();
	public Devices devices = new Devices();
	public ChangePassword changePassword = new ChangePassword();
	public LoginToOriginal loginToOriginal = new LoginToOriginal();

	public class Home {

	    public void navigateTo() {
		generalButton.click();
		homeButton.click();
	    }

	    public boolean isDisplayed() {
		generalButton.click();
		return homeButton.isDisplayed();
	    }
	}

	public class Logout {

	    public void navigateTo() {
		generalButton.click();
		logoutButton.click();
	    }

	    public boolean isDisplayed() {
		generalButton.click();
		return logoutButton.isDisplayed();
	    }
	}

	public class Customers {

	    public void navigateTo() {
		generalButton.click();
		customersButton.click();
	    }

	    public boolean isDisplayed() {
		generalButton.click();
		return customersButton.isDisplayed();
	    }
	}

	public class Dashboard {

	    public void navigateTo() {
		generalButton.click();
		dashboardButton.click();
	    }

	    public boolean isDisplayed() {
		generalButton.click();
		return dashboardButton.isDisplayed();
	    }
	}

	public class Devices {

	    public void navigateTo() {
		generalButton.click();
		devicesButton.click();
	    }

	    public boolean isDisplayed() {
		generalButton.click();
		return devicesButton.isDisplayed();
	    }
	}

	public class Users {
	    public void navigateTo() {
		generalButton.click();
		usersButton.click();
	    }

	    public boolean isDisplayed() {
		generalButton.click();
		return usersButton.isDisplayed();
	    }
	}

	public class ChangePassword {
	    public void navigateTo() {
		generalButton.click();
		changepasswordButton.click();
	    }

	    public boolean isDisplayed() {
		generalButton.click();
		return changepasswordButton.isDisplayed();
	    }
	}

	public class LoginToOriginal {
	    public void navigateTo() {
		generalButton.click();
		loginOriginalButton.click();
	    }

	    public boolean isDisplayed() {
		generalButton.click();
		return loginOriginalButton.isDisplayed();
	    }
	}
    }

    public class Administration {
	public AdminRefunds adminRefunds = new AdminRefunds();
	public Chargebacks chargebacks = new Chargebacks();
	public DataExchange dataExchange = new DataExchange();
	public NewCustomer newCustomer = new NewCustomer();
	public Refunds refunds = new Refunds();
	public WelcomeInfo welcomeInfo = new WelcomeInfo();
	public Parts parts = new Parts();
	public Rma rma = new Rma();

	public class AdminRefunds {

	    public void navigateTo() {
		administrationButton.click();
		adminRefundsButton.click();
	    }

	    public boolean isDisplayed() {
		administrationButton.click();
		return adminRefundsButton.isDisplayed();
	    }
	}

	public class Chargebacks {

	    public void navigateTo() {
		administrationButton.click();
		chargebacksButton.click();
	    }

	    public boolean isDisplayed() {
		administrationButton.click();
		return chargebacksButton.isDisplayed();
	    }
	}

	public class DataExchange {

	    public void navigateTo() {
		administrationButton.click();
		dataExchangeButton.click();
	    }

	    public boolean isDisplayed() {
		administrationButton.click();
		return dataExchangeButton.isDisplayed();
	    }
	}

	public class NewCustomer {

	    public void navigateTo() {
		administrationButton.click();
		newCustomerButton.click();
	    }

	    public boolean isDisplayed() {
		administrationButton.click();
		return newCustomerButton.isDisplayed();
	    }
	}

	public class Refunds {

	    public void navigateTo() {
		administrationButton.click();
		refundsButton.click();
	    }

	    public boolean isDisplayed() {
		administrationButton.click();
		return refundsButton.isDisplayed();
	    }
	}

	public class WelcomeInfo {

	    public void navigateTo() {
		administrationButton.click();
		welcomeInfoButton.click();
	    }

	    public boolean isDisplayed() {
		administrationButton.click();
		return welcomeInfoButton.isDisplayed();
	    }
	}

	public class Parts {

	    public void navigateTo() {
		administrationButton.click();
		partsButton.click();
	    }

	    public boolean isDisplayed() {
		administrationButton.click();
		return partsButton.isDisplayed();
	    }
	}

	public class Rma {
	    public CreatePartsRma createPartsRma = new CreatePartsRma();
	    public ViewRma viewRma = new ViewRma();
	    public CreateRma createRma = new CreateRma();

	    public class CreatePartsRma {

		public void navigateTo() {
		    administrationButton.click();
		    rmaButton.click();
		    createPartsRmaButton.click();
		}

		public boolean isDisplayed() {
		    administrationButton.click();
		    rmaButton.click();
		    return createPartsRmaButton.isDisplayed();
		}
	    }

	    public class ViewRma {

		public void navigateTo() {
		    administrationButton.click();
		    rmaButton.click();
		    viewRmaButton.click();
		}

		public boolean isDisplayed() {
		    administrationButton.click();
		    rmaButton.click();
		    return viewRmaButton.isDisplayed();
		}
	    }
	    
	    public class CreateRma {

		public void navigateTo() {
		    administrationButton.click();
		    rmaButton.click();
		    createRmaButton.click();
		}

		public boolean isDisplayed() {
		    administrationButton.click();
		    rmaButton.click();
		    return createRmaButton.isDisplayed();
		}
	    }
	}

    }

    public class Configuration {

	public Campaings campaings = new Campaings();
	public ColumnMaps columnMaps = new ColumnMaps();
	public Consumers consumers = new Consumers();
	public MassUpdate massUpdate = new MassUpdate();
	public Preferences preferences = new Preferences();

	public class Campaings {

	    public void navigateTo() {
		configurationButton.click();
		campaingsButton.click();
	    }

	    public boolean isDisplayed() {
		configurationButton.click();
		return campaingsButton.isDisplayed();
	    }
	}

	public class ColumnMaps {

	    public void navigateTo() {
		configurationButton.click();
		columnMapsButton.click();
	    }

	    public boolean isDisplayed() {
		configurationButton.click();
		return columnMapsButton.isDisplayed();
	    }
	}

	public class Consumers {

	    public void navigateTo() {
		configurationButton.click();
		consumersButton.click();
	    }

	    public boolean isDisplayed() {
		configurationButton.click();
		return consumersButton.isDisplayed();
	    }
	}

	public class MassUpdate {
	    public MassCardUpdate massCardUpdate = new MassCardUpdate();
	    public MassDeviceConfiguration massDeviceConfiguration = new MassDeviceConfiguration();
	    public MassDeviceUpdate massDeviceUpdate = new MassDeviceUpdate();

	    public class MassCardUpdate {

		public void navigateTo() {
		    configurationButton.click();
		    massUpdatesButton.click();
		    massCardUpdateButton.click();
		}

		public boolean isDisplayed() {
		    configurationButton.click();
		    massUpdatesButton.click();
		    return massCardUpdateButton.isDisplayed();
		}
	    }

	    public class MassDeviceConfiguration {

		public void navigateTo() {
		    configurationButton.click();
		    massUpdatesButton.click();
		    massDeviceConfigurationButton.click();
		}

		public boolean isDisplayed() {
		    configurationButton.click();
		    massUpdatesButton.click();
		    return massDeviceConfigurationButton.isDisplayed();
		}
	    }

	    public class MassDeviceUpdate {

		public void navigateTo() {
		    configurationButton.click();
		    massUpdatesButton.click();
		    massDeviceUpdateButton.click();
		}

		public boolean isDisplayed() {
		    configurationButton.click();
		    massUpdatesButton.click();
		    return massDeviceUpdateButton.isDisplayed();
		}
	    }
	}

	public class Preferences {

	    public void navigateTo() {
		configurationButton.click();
		preferencesButton.click();
	    }

	    public boolean isDisplayed() {
		configurationButton.click();
		return preferencesButton.isDisplayed();
	    }
	}

    }

    public class Reports {

	public BuildReport buildReport = new BuildReport();
	public DexStatus dexStatus = new DexStatus();
	public Diagnostics diagnostics = new Diagnostics();
	public FolioDesigner folioDesigner = new FolioDesigner();
	public PaymentDetail paymentDetail = new PaymentDetail();
	public RecentReports recentReports = new RecentReports();
	public ReportRegister reportRegister = new ReportRegister();
	public SavedReports savedReports = new SavedReports();

	public class BuildReport {

	    public void navigateTo() {
		reportsButton.click();
		buildReportButton.click();
	    }

	    public boolean isDisplayed() {
		reportsButton.click();
		return buildReportButton.isDisplayed();
	    }
	}

	public class DexStatus {

	    public void navigateTo() {
		reportsButton.click();
		dexStatusButton.click();
	    }

	    public boolean isDisplayed() {
		reportsButton.click();
		return dexStatusButton.isDisplayed();
	    }
	}

	public class Diagnostics {

	    public void navigateTo() {
		reportsButton.click();
		diagnosticsButton.click();
	    }

	    public boolean isDisplayed() {
		reportsButton.click();
		return diagnosticsButton.isDisplayed();
	    }
	}

	public class FolioDesigner {

	    public void navigateTo() {
		reportsButton.click();
		folioDesignerButton.click();
	    }

	    public boolean isDisplayed() {
		reportsButton.click();
		return folioDesignerButton.isDisplayed();
	    }
	}

	public class PaymentDetail {

	    public void navigateTo() {
		reportsButton.click();
		paymentDetailButton.click();
	    }

	    public boolean isDisplayed() {
		reportsButton.click();
		return paymentDetailButton.isDisplayed();
	    }
	}

	public class RecentReports {

	    public void navigateTo() {
		reportsButton.click();
		recentReportsButton.click();
	    }

	    public boolean isDisplayed() {
		reportsButton.click();
		return recentReportsButton.isDisplayed();
	    }
	}

	public class ReportRegister {

	    public void navigateTo() {
		reportsButton.click();
		reportRegisterButton.click();
	    }

	    public boolean isDisplayed() {
		reportsButton.click();
		return reportRegisterButton.isDisplayed();
	    }
	}

	public class SavedReports {

	    public void navigateTo() {
		reportsButton.click();
		savedReportsButton.click();
	    }

	    public boolean isDisplayed() {
		reportsButton.click();
		return savedReportsButton.isDisplayed();
	    }
	}
    }
}
