package pages.UsaLive.General;

import helper.Core;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ContactUsPage extends Core{
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(HomePage.class);

    public ContactUsPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    public void verifyContent() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table[@class='contactInfo']//th[text()='USA Technologies, Inc.']")));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table[@class='contactInfo']//td[text()='100 Deerfield Lane, Suite 300' or text()='100 Deerfield Lane, Suite 140']")));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table[@class='contactInfo']//td[text()='Malvern, PA 19355']")));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table[@class='contactInfo']//td[text()='(888) 561-4748']")));
    }

}
