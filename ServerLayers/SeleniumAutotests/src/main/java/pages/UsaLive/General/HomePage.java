package pages.UsaLive.General;

import helper.Core;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage extends Core{
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(HomePage.class);

    public HomePage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    public void verifyTitle(String userFirstName) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='welcome' and contains(text(), '" + userFirstName + "')]")));
    }

    public void clickFullDeviceHealthReportButton() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@value='Full Device Health Report']"))).click();
    }
}
