package pages.UsaLive;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import helper.Core;

public class ResetPasswordPage extends Core {
	public final WebDriver driver;
	public final WebDriverWait wait;
	@FindBy(xpath = "//input[@name='username']")
	private static WebElement resetPasswordField;
	@FindBy(xpath = "//input[@value='Reset Password']")
	private static WebElement resetPasswordButton;
	@FindBy(xpath = "//div[@class='successText']")
	private static WebElement netOpsWarning;

	private static final Logger logger = Logger.getLogger(LogInPage.class);

	public ResetPasswordPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
		wait = new WebDriverWait(driver, 30);
	}

	public void sendEmail() throws InterruptedException {
		String[] loginAndPassword = getEmailUserNameAndPassword();
		resetPasswordField.sendKeys(loginAndPassword[0]);
		resetPasswordButton.click();
	}

	public String getWarningText() {
		return netOpsWarning.getText();
	}
}