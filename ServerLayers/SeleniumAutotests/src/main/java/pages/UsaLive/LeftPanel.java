package pages.UsaLive;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LeftPanel{
    public final WebDriver driver;
    public final WebDriverWait wait;
    private static final Logger logger = Logger.getLogger(LeftPanel.class);

    public LeftPanel(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }
    private WebElement getMenuItemByText(String linkText) {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table[@class='menuTable']//a[text()='" + linkText + "']")));
    }

    public void clickSavedReports() {
        getMenuItemByText("Saved Reports").click();
    }
    
    public void clickRecentReports() {
        getMenuItemByText("Recent Reports").click();
    }
    
    public void clickBuildReport() {
        getMenuItemByText("Build a Report").click();
    }

    public void clickMenuItemByText(String linkText) {
        getMenuItemByText(linkText).click();
    }

    public void verifyMenuItemVisibility(String menuItemText, boolean isExpectedVisibility) {
        if(isExpectedVisibility)
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table[@class='menuTable']//a[text()='" + menuItemText + "']")));
        else
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//table[@class='menuTable']//a[text()='" + menuItemText + "']")));
    }
}
