package pages.UsaLive;

import helper.Core;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.server.handler.FindActiveElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LogInPage extends Core {
	public final WebDriver driver;
	public final WebDriverWait wait;
    @FindBy(xpath="//a[text()='Forgot your password?']")
    private static WebElement resetPassword;
    @FindBy(xpath="//input[@name='username']")
    private static WebElement usernameField;
    @FindBy(xpath="//input[@name='password']")
    private static WebElement passwordField;
    @FindBy(xpath="//input[@type='submit']")
    private static WebElement submitButton;
    @FindBy(xpath="//div[@class='welcome']")
    private static WebElement welcomeLabel;
	
	private static final Logger logger = Logger.getLogger(LogInPage.class);

	public LogInPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
		wait = new WebDriverWait(driver, 30);
	}

	private WebElement getMenuItemByText(String linkText) {
		return wait.until(ExpectedConditions
				.presenceOfElementLocated(By.xpath("//table[@class='menuTable']//a[text()='" + linkText + "']")));
	}

	public void logIn(String name, String password) {
		logger.info("Logging in. User=" + name + ", Password=" + password + "...");
//		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='username']"))).clear();
//		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='username']"))).sendKeys(name);
//		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='password']"))).clear();
//		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='password']")))
//				.sendKeys(password);
//		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='submit']"))).click();
		usernameField.clear();
		usernameField.sendKeys(name);
		passwordField.clear();
		passwordField.sendKeys(password);
		submitButton.click();
		logger.info("Verifying Welcome page...");
		welcomeLabel.isDisplayed();
	}

	public void logInToUsaLiveAsWeakUser() {
		String[] loginAndPassword = getUsaLiveWeakUserNameAndPassword();
		logIn(loginAndPassword[0], loginAndPassword[1]);
	}

	public void logInToUsaLiveAsPowerUser() {
		String[] loginAndPassword = getUsaLivePowerUserNameAndPassword();
		logIn(loginAndPassword[0], loginAndPassword[1]);
	}
	
	public void logInToUsaLiveAsCustomerUser() {
		String[] loginAndPassword = getUsaLiveCustomerUserNameAndPassword();
		logIn(loginAndPassword[0], loginAndPassword[1]);
	}

	public void logInToUsaLiveAsNotLdapUser() {
		logInToUsaLiveAsWeakUser();
	}
	
	public void resetPassowrd() {
		resetPassword.click();
	}
}
