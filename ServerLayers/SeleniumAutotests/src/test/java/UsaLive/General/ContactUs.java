package UsaLive.General;


import helper.*;

import org.junit.Ignore;
import org.junit.Test;

import pages.UsaLive.LeftPanel;
import pages.UsaLive.LogInPage;
import pages.UsaLive.General.ContactUsPage;


public class ContactUs extends Core
{
    //TODO: remove ignore
    @Test
    @Ignore
    public void USAT_101_USALive_General_ContactUs() throws Exception 
    {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();

        LeftPanel leftPanel =  new LeftPanel(driver);
        leftPanel.clickMenuItemByText("Contact Us");

        ContactUsPage contactUsPage = new ContactUsPage(driver);

        contactUsPage.verifyContent();        
    }

}
