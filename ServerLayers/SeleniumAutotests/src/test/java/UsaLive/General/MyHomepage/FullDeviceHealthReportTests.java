package UsaLive.General.MyHomepage;

import com.google.common.io.Files;
import helper.*;
import org.apache.commons.codec.Charsets;
import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
//import org.openqa.jetty.util.URI;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import pages.UsaLive.Administration.CustomersPage;
import pages.UsaLive.General.HomePage;
import pages.UsaLive.LeftPanel;
import pages.UsaLive.LogInPage;
import pages.UsaLive.TopPanel;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class FullDeviceHealthReportTests extends Core
{
    //TODO: remove ignore
    @Test
    public void USAT_1_USALive_General_MyHomepage_FullDeviceHealthReport_ReportDisplaysTimeInDeviceTimezoneButNotInServerTimezone() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();

//        LeftPanel leftPanel =  new LeftPanel(driver);
//        leftPanel.clickMenuItemByText("My Homepage");
        TopPanel topPanel = new TopPanel(driver);
        topPanel.general.home.navigateTo();

        HomePage homePage = new HomePage(driver);

        Integer filesCountBefore = new File(tempFolderForDownloads).list().length;
        homePage.clickFullDeviceHealthReportButton();
        FileUtils.waitForFileToBeDownloaded(filesCountBefore);

        String actualFilePath = CompareUtils.getLastModifiedFile(Core.tempFolderForDownloads, "xls");
        long fileSize = new File(actualFilePath).length();
        Assert.assertTrue("File size is too big " + fileSize/1000000 + "(mb), path=" + actualFilePath, fileSize<104857600);
        DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
        domFactory.setNamespaceAware(true);
        DocumentBuilder builder = domFactory.newDocumentBuilder();

        //Getting time from DB
        OracleConnector oracleConnector = new OracleConnector(getEnvironment());

        String deviceId = oracleConnector.getFirstValueFromQuery("select EPORT_SERIAL_NUM from report.time_zone tz join report.terminal term on tz.TIME_ZONE_ID = term.TIME_ZONE_ID join report.vw_terminal_eport vte on vte.TERMINAL_ID = term.TERMINAL_ID join report.eport ep on ep.eport_id = vte.eport_id where tz.time_zone_guid not in (select PKG_CONST.GET_DB_TIME_ZONE from dual) and TIME_ZONE_GUID = 'US/Central'");
        String dateInDb = oracleConnector.getFirstValueFromQuery("select LAST_TRAN_DATE from report.time_zone tz join report.terminal term on tz.TIME_ZONE_ID = term.TIME_ZONE_ID join report.vw_terminal_eport vte on vte.TERMINAL_ID = term.TERMINAL_ID join report.eport ep on ep.eport_id = vte.eport_id where tz.time_zone_guid not in (select PKG_CONST.GET_DB_TIME_ZONE from dual) and TIME_ZONE_GUID = 'US/Central' and EPORT_SERIAL_NUM = '" + deviceId + "'");
        if(dateInDb != null) {
            Date parsedDateInDb = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(dateInDb);
            System.out.println("deviceId=" + deviceId);
            System.out.println("DateDb=" + parsedDateInDb);
            System.out.println("TimeDb=" + parsedDateInDb.getTime());

            //Getting time from xls report
            Document doc = builder.parse(new File(actualFilePath));
            XPath xpath = XPathFactory.newInstance().newXPath();
            XPathExpression expr = xpath.compile("//*[text()='" + deviceId + "']/../following-sibling::*[2]");
            Object result = expr.evaluate(doc, XPathConstants.NODESET);
            NodeList nodes = (NodeList) result;
            String timeDate = nodes.item(0).getTextContent().trim();
            Date parsedDateInReport = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a", Locale.ENGLISH).parse(timeDate);
            System.out.println("DateRep=" + parsedDateInReport);
            System.out.println("TimeRep=" + parsedDateInReport.getTime());

            Assert.assertEquals("Time difference is incorrect", 3600000, parsedDateInDb.getTime() - parsedDateInReport.getTime());
        }
    }

}
