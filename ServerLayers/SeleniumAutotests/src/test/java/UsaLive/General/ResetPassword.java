package UsaLive.General;

import helper.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import pages.UsaLive.LeftPanel;
import pages.UsaLive.LogInPage;
import pages.UsaLive.ResetPasswordPage;
import pages.UsaLive.General.ContactUsPage;

public class ResetPassword extends Core {
	@Test
	public void USAT_600_USALive_General_ResetPasswordForInternalUser() throws Exception {
		openUsaLive();
		new LogInPage(driver).resetPassowrd();
		ResetPasswordPage resetPasswordPage = new ResetPasswordPage(driver);
		resetPasswordPage.sendEmail();

		assertEquals(resetPasswordPage.getWarningText(), "Please contact NetOps to reset your password");

	}
}
