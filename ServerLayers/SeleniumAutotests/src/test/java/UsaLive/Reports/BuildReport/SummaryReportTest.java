package UsaLive.Reports.BuildReport;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.*;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Repeatable;

import org.junit.Test;

import com.usatech.layers.common.LegacyUpdateStatusProcessor;

import helper.CompareUtils;
import helper.Core;
import pages.UsaLive.LeftPanel;
import pages.UsaLive.LogInPage;
import pages.UsaLive.TopPanel;
import pages.UsaLive.Reports.BuildReport.BuildReportPage;
import pages.UsaLive.Reports.SavedReports.SavedReportsPage;

public class SummaryReportTest extends Core {

	@Test
	public void USAT82_USALive_Reports_BuildReport_SummaryReport() throws InterruptedException, IOException {

		openUsaLive();
		new LogInPage(driver).logInToUsaLiveAsPowerUser();
		TopPanel topPanel = new TopPanel(driver);
		topPanel.reports.buildReport.navigateTo();
		BuildReportPage buildReportPage = new BuildReportPage(driver);
		buildReportPage.switchToSummaryTab();

		buildReportPage.setReportBeginMonth("May");
		buildReportPage.setReportBeginDay("01");
		buildReportPage.setReportBeginYear("2018");
		buildReportPage.setReportEndMonth("May");
		buildReportPage.setReportEndDay("31");
		buildReportPage.setReportEndYear("2018");
		// buildReportPage.setColumnsTotalByValue("Device");
		buildReportPage.setColumnsTotalByValue("Client");
		buildReportPage.setAllDataValuesChecked();
		buildReportPage.expandDeviceTree();
		buildReportPage.waitForDeviceSearchTreeExpanded();
		buildReportPage.setAllDeviceSearchTreeChecked();
		Integer buildReportTime = buildReportPage.runSummaryReport();
		logger.info("The report has built in " + buildReportTime + " milliseconds");
		assertTrue(buildReportTime < 11270);

	}
}
