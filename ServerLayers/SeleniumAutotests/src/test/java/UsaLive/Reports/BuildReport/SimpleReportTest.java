package UsaLive.Reports.BuildReport;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.apache.commons.exec.environment.DefaultProcessingEnvironment;
import org.junit.Test;

import helper.Core;
import pages.UsaLive.LeftPanel;
import pages.UsaLive.LogInPage;
import pages.UsaLive.TopPanel;
import pages.UsaLive.Reports.BuildReport.BuildReportPage;

public class SimpleReportTest extends Core {
    @Test
    public void USAT_79_USALive_Reports_BuildReport_SimpleReport_byDay() throws InterruptedException, IOException {

	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.buildReport.navigateTo();
	BuildReportPage buildReportPage = new BuildReportPage(driver);
	buildReportPage.switchToSimpleTab();
	buildReportPage.expandDeviceTree();
	buildReportPage.waitForDeviceSearchTreeExpanded();
	buildReportPage.setAllDeviceSearchTreeChecked();
	buildReportPage.setReportBeginMonth("May");
	buildReportPage.setReportBeginDay("01");
	buildReportPage.setReportBeginYear("2018");
	buildReportPage.setReportEndMonth("May");
	buildReportPage.setReportEndDay("31");
	buildReportPage.setReportEndYear("2018");
	buildReportPage.setRangeTypeSimpleReport("Transaction Date - By Day");
	Integer buildReportTime = buildReportPage.runSimpleReportByDay();
	logger.info("The report has built in " + buildReportTime + " milliseconds");

	if (getEnvironment().equals("int")) {
	    assertTrue(buildReportTime < 15982);
	}
	if (getEnvironment().equals("ecc")) {
	    assertTrue(buildReportTime < 40513);
	}

    }

    @Test
    public void USAT_79_USALive_Reports_BuildReport_SimpleReport_byWeek() throws InterruptedException, IOException {

	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.buildReport.navigateTo();
	BuildReportPage buildReportPage = new BuildReportPage(driver);
	buildReportPage.switchToSimpleTab();
	buildReportPage.expandDeviceTree();
	buildReportPage.waitForDeviceSearchTreeExpanded();
	buildReportPage.setAllDeviceSearchTreeChecked();
	buildReportPage.setReportBeginMonth("May");
	buildReportPage.setReportBeginDay("01");
	buildReportPage.setReportBeginYear("2018");
	buildReportPage.setReportEndMonth("May");
	buildReportPage.setReportEndDay("31");
	buildReportPage.setReportEndYear("2018");
	buildReportPage.setRangeTypeSimpleReport("Transaction Date - By Week");
	Integer buildReportTime = buildReportPage.runSimpleReportByWeek();
	logger.info("The report has built in " + buildReportTime + " milliseconds");

	if (getEnvironment().equals("int")) {
	    assertTrue(buildReportTime < 18137);
	}
	if (getEnvironment().equals("ecc")) {
	    assertTrue(buildReportTime < 48229);
	}
    }

    @Test
    public void USAT_79_USALive_Reports_BuildReport_SimpleReport_byMonth() throws InterruptedException, IOException {

	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.buildReport.navigateTo();
	BuildReportPage buildReportPage = new BuildReportPage(driver);
	buildReportPage.switchToSimpleTab();
	buildReportPage.expandDeviceTree();
	buildReportPage.waitForDeviceSearchTreeExpanded();
	buildReportPage.setAllDeviceSearchTreeChecked();
	buildReportPage.setReportBeginMonth("May");
	buildReportPage.setReportBeginDay("01");
	buildReportPage.setReportBeginYear("2018");
	buildReportPage.setReportEndMonth("May");
	buildReportPage.setReportEndDay("31");
	buildReportPage.setReportEndYear("2018");
	buildReportPage.setRangeTypeSimpleReport("Transaction Date - By Month");
	Integer buildReportTime = buildReportPage.runSimpleReportByMonth();
	logger.info("The report has built in " + buildReportTime + " milliseconds");

	if (getEnvironment().equals("int")) {
	    assertTrue(buildReportTime < 15424);
	}
	if (getEnvironment().equals("ecc")) {
	    assertTrue(buildReportTime < 33143);
	}
    }

    @Test
    public void USAT_79_USALive_Reports_BuildReport_SimpleReport_EntireRange() throws InterruptedException, IOException {

	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.buildReport.navigateTo();
	BuildReportPage buildReportPage = new BuildReportPage(driver);
	buildReportPage.switchToSimpleTab();
	buildReportPage.expandDeviceTree();
	buildReportPage.waitForDeviceSearchTreeExpanded();
	buildReportPage.setAllDeviceSearchTreeChecked();
	buildReportPage.setReportBeginMonth("May");
	buildReportPage.setReportBeginDay("01");
	buildReportPage.setReportBeginYear("2018");
	buildReportPage.setReportEndMonth("May");
	buildReportPage.setReportEndDay("31");
	buildReportPage.setReportEndYear("2018");
	buildReportPage.setRangeTypeSimpleReport("Transaction Date - Entire Range");
	Integer buildReportTime = buildReportPage.runSimpleReportEntireRange();
	logger.info("The report has built in " + buildReportTime + " milliseconds");

	if (getEnvironment().equals("int")) {
	    assertTrue(buildReportTime < 15715);
	}
	if (getEnvironment().equals("ecc")) {
	    assertTrue(buildReportTime < 62683);
	}
    }

    @Test
    public void USAT_79_USALive_Reports_BuildReport_SimpleReport_FillDate() throws InterruptedException, IOException {

	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.buildReport.navigateTo();
	BuildReportPage buildReportPage = new BuildReportPage(driver);
	buildReportPage.switchToSimpleTab();
	buildReportPage.expandDeviceTree();
	buildReportPage.waitForDeviceSearchTreeExpanded();
	buildReportPage.setAllDeviceSearchTreeChecked();
	buildReportPage.setReportBeginMonth("May");
	buildReportPage.setReportBeginDay("01");
	buildReportPage.setReportBeginYear("2018");
	buildReportPage.setReportEndMonth("May");
	buildReportPage.setReportEndDay("31");
	buildReportPage.setReportEndYear("2018");
	buildReportPage.setRangeTypeSimpleReport("Fill Date");
	Integer buildReportTime = buildReportPage.runSimpleReportFillDate();
	logger.info("The report has built in " + buildReportTime + " milliseconds");
	if (getEnvironment().equals("int")) {
	    assertTrue(buildReportTime < 13984);
	}
	if (getEnvironment().equals("ecc")) {
	    assertTrue(buildReportTime < 90421);
	}
    }

}
