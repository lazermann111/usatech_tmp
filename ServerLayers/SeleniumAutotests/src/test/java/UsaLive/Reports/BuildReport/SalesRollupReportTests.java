package UsaLive.Reports.BuildReport;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Test;

import helper.Core;
import pages.UsaLive.LeftPanel;
import pages.UsaLive.LogInPage;
import pages.UsaLive.TopPanel;
import pages.UsaLive.Reports.BuildReport.BuildReportPage;

public class SalesRollupReportTests extends Core {
    @Test
    public void USAT_80_USALive_Reports_BuildReport_SalesRollupReport() throws InterruptedException, IOException {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();
//	new LeftPanel(driver).clickBuildReport();
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.buildReport.navigateTo();
	
	BuildReportPage buildReportPage = new BuildReportPage(driver);
	buildReportPage.switchToSalesRollupTab();
	buildReportPage.setStartDate("05/01/2018");
	buildReportPage.setEndDate("05/31/2018");
	buildReportPage.expandDeviceTree();
	buildReportPage.waitForDeviceSearchTreeExpanded();
	buildReportPage.setAllDeviceSearchTreeChecked();
	buildReportPage.setReportFormatType("html");
	Integer buildReportTime = buildReportPage.runSalesRollupReport();
	logger.info("The report has built in " + buildReportTime + " milliseconds");

	if (getEnvironment().equals("int")) {
		assertTrue(buildReportTime < 13607);
	}
	if (getEnvironment().equals("ecc")) {
	    assertTrue(buildReportTime < 26972);
	}
    }
}
