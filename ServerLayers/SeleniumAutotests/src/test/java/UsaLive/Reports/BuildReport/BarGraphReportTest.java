package UsaLive.Reports.BuildReport;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Test;

import helper.Core;
import pages.UsaLive.LeftPanel;
import pages.UsaLive.LogInPage;
import pages.UsaLive.TopPanel;
import pages.UsaLive.Reports.BuildReport.BuildReportPage;

public class BarGraphReportTest extends Core {
    @Test
    public void USAT_83_USALive_Reports_BuildReport_BarGraphReport() throws InterruptedException, IOException {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();
//	new LeftPanel(driver).clickBuildReport();
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.buildReport.navigateTo();
	
	BuildReportPage buildReportPage = new BuildReportPage(driver);
	buildReportPage.switchToBarGraphTab();
	buildReportPage.setReportBeginMonth("May");
	buildReportPage.setReportBeginDay("01");
	buildReportPage.setReportBeginYear("2018");
	buildReportPage.setReportEndMonth("May");
	buildReportPage.setReportEndDay("31");
	buildReportPage.setReportEndYear("2018");
	buildReportPage.setDataValuesRadioButton("Sale Amount ");
	if (getEnvironment().equals("int")) {
		buildReportPage.expandDeviceTree();
		buildReportPage.waitForDeviceSearchTreeExpanded();
		buildReportPage.setAllDeviceSearchTreeChecked();
	}
	if (getEnvironment().equals("ecc")) {
	    buildReportPage.setDeviceSearchTreeCheckedByDeviceId("VJ000000003");
//		buildReportPage.waitForFilteredDeviceSearchTreeExpanded();
	}
	Integer buildReportTime = buildReportPage.runBarGraphReport();
	logger.info("The report has built in " + buildReportTime + " milliseconds");
	assertTrue(buildReportTime < 17925);
    }
}
