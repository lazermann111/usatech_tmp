package UsaLive.Reports;

import helper.CompareUtils;
import helper.Core;
import org.junit.Test;
import pages.UsaLive.LeftPanel;
import pages.UsaLive.LogInPage;
import pages.UsaLive.TopPanel;
import pages.UsaLive.Reports.ReportsPage;

public class DiagnosticsTests extends Core
{
    private static String testGroupFolder = "";
    @Test
    public void USAT_84_USALive_Reports_Diagnostics() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        String reportName = "Diagnostics";
//        LeftPanel leftPanel =  new LeftPanel(driver);
//        leftPanel.clickMenuItemByText(reportName);
        
        TopPanel topPanel = new TopPanel(driver);
        topPanel.reports.diagnostics.navigateTo();

        ReportsPage reportsPage = new ReportsPage(driver);
        
        reportsPage.setAllDevices();
//        Thread.sleep(3000);
        reportsPage.setBeginDate(new String[]{"June", "01", "2015", "9:00 am"});
//        Thread.sleep(3000);
        reportsPage.setEndDate(new String[]{"June", "03", "2015", "9:00 am"});
//        reportsPage.setAllDevices();
        reportsPage.submitReport();
        reportsPage.verifyReportTitleVisibility(reportName);
        Thread.sleep(3000);
        CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
    }

}
