package UsaLive.Reports.SavedReports;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Test;

import helper.Core;
import pages.UsaLive.LogInPage;
import pages.UsaLive.TopPanel;
import pages.UsaLive.Reports.ReportsPage;

public class PayrollDeductTest extends Core {

    @Test
    public void USAT_592_USALive_Reports_SavedReports_PayrollDeductCardDetails_Html () throws IOException {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.savedReports.navigateTo();
	
	ReportsPage reportsPage = new ReportsPage(driver);
	reportsPage.morePlatformTab.payrollDeductCardDetails.html.open();
	
	Integer buildReportTime = reportsPage.compareHtmlReport("Payroll Deduct - Card Details", "morePlatform/");
	logger.info("The report has built in " + buildReportTime + " milliseconds");
	assertTrue(buildReportTime < 17925);
    }
    
    @Test
    public void USAT_592_USALive_Reports_SavedReports_PayrollDeductTotalSalesByEmployeeID_Html () throws IOException {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.savedReports.navigateTo();
	
	ReportsPage reportsPage = new ReportsPage(driver);
	reportsPage.morePlatformTab.payrollDeductTotalSalesByEmployeeId.html.open();
	reportsPage.setBeginDate("01/01/2018");
	reportsPage.setEndDate("01/31/2018");
	reportsPage.clickRunReport();
	
	Integer buildReportTime = reportsPage.compareHtmlReport("Payroll Deduct - Total Sales by Employee ID", "morePlatform/");
	logger.info("The report has built in " + buildReportTime + " milliseconds");
	assertTrue(buildReportTime < 17925);
    }
    
    @Test
    public void USAT_592_USALive_Reports_SavedReports_PayrollDeductTransactionsByEmployeeID_Html () throws IOException {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.savedReports.navigateTo();
	
	ReportsPage reportsPage = new ReportsPage(driver);
	reportsPage.morePlatformTab.payrollDeductTransactionsByEmployeeId.html.open();
	reportsPage.setBeginDate("01/01/2018");
	reportsPage.setEndDate("01/31/2018");
	reportsPage.clickRunReport();
	
	Integer buildReportTime = reportsPage.compareHtmlReport("Payroll Deduct - Transactions by Employee ID", "morePlatform/");
	logger.info("The report has built in " + buildReportTime + " milliseconds");
	assertTrue(buildReportTime < 17925);
    }
    
    @Test
    public void USAT_592_USALive_Reports_SavedReports_PayrollDeductUnactivatedCards_Html () throws IOException {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.savedReports.navigateTo();
	
	ReportsPage reportsPage = new ReportsPage(driver);
	reportsPage.morePlatformTab.payrollDeductUnactivatedCards.html.open();

	
	Integer buildReportTime = reportsPage.compareHtmlReport("Payroll Deduct - Unactivated Cards", "morePlatform/");
	logger.info("The report has built in " + buildReportTime + " milliseconds");
	assertTrue(buildReportTime < 27925);
    }
    
    
}
