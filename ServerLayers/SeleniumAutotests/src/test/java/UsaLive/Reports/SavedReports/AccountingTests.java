package UsaLive.Reports.SavedReports;

import helper.CompareUtils;
import helper.Core;
import helper.FileUtils;
import org.junit.Test;
import pages.UsaLive.LeftPanel;
import pages.UsaLive.LogInPage;
import pages.UsaLive.TopPanel;
import pages.UsaLive.Reports.SavedReports.SavedReportsPage;

import java.io.File;

public class AccountingTests extends Core
{
    private static String testGroupFolder = "accounting/";

    //Dynamic
    @Test
    public void USAT_71_USALive_ReportsS_avedReports_Accounting_AllOtherFeeEntriesCsv() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();

        openAccountingTab();
        String reportName = "All Other Fee Entries";

        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.clickReportLink(reportName);
        savedReportsPage.setBeginDate("01/01/2014");
        savedReportsPage.setEndDate("03/03/2014");
        Integer filesCountBefore = new File(tempFolderForDownloads).list().length;
        savedReportsPage.submitReport();
        FileUtils.waitForFileToBeDownloaded(filesCountBefore);

//        CompareUtils.verifyDownloadedCsv(reportName, testGroupFolder);

    }

    //Dynamic
    @Test
    public void USAT_71_USALive_ReportsS_avedReports_Accounting_allOtherFeeEntriesWithDeviceCsv() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();

        openAccountingTab();
        String reportName = "All Other Fee Entries With Device";

        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.clickReportLink(reportName);
        savedReportsPage.setBeginDate("03/01/2015");
        savedReportsPage.setEndDate("03/03/2015");
        Integer filesCountBefore = new File(tempFolderForDownloads).list().length;
        savedReportsPage.submitReport();
        FileUtils.waitForFileToBeDownloaded(filesCountBefore);

//        CompareUtils.verifyDownloadedCsv(reportName, testGroupFolder);
    }

    //No data
    @Test
    public void USAT_71_USALive_ReportsS_avedReports_Accounting_cpsChargebacksTestPng()
    {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();

        openAccountingTab();
        String reportName = "CPS Chargebacks";

        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.clickReportLink(reportName);
        savedReportsPage.setBeginDate("01/01/2011");
        savedReportsPage.setEndDate("03/03/2015");
        savedReportsPage.verifyReportCaptionVisibility(reportName);
    }

    @Test
    public void USAT_71_USALive_ReportsS_avedReports_Accounting_cpsDowngradesPng() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();

        openAccountingTab();
        String reportName = "CPS Downgrades";

        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.clickReportLink(reportName);
        savedReportsPage.setBeginDate("07/06/2015");
        savedReportsPage.setEndDate("07/06/2015");
        Integer filesCountBefore = new File(tempFolderForDownloads).list().length;
        savedReportsPage.submitReport();
        savedReportsPage.verifyReportTitleVisibility(reportName);
        CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
    }

    //No data
    @Test
    public void USAT_71_USALive_ReportsS_avedReports_Accounting_cpsRetrievalsPng()
    {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();

        openAccountingTab();
        String reportName = "CPS Retrievals";

        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.clickReportLink(reportName);
        savedReportsPage.setBeginDate("01/01/2011");
        savedReportsPage.setEndDate("03/03/2015");
        savedReportsPage.verifyReportCaptionVisibility(reportName);
    }

    //No data
    @Test
    public void USAT_71_USALive_ReportsS_avedReports_Accounting_customersWithManyInactiveDevicesPng()
    {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();

        openAccountingTab();
        String reportName = "Customers with Many Inactive Devices";

        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.clickReportLink(reportName);
        savedReportsPage.verifyReportTitleVisibility(reportName);
    }

    @Test
    public void USAT_71_USALive_ReportsS_avedReports_Accounting_declinedMasterCardDebitTransactionCountsCsv() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();

        openAccountingTab();
        String reportName = "Declined MasterCard Debit Transaction Counts";

        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.clickReportLink(reportName);
        savedReportsPage.setBeginDate("01/01/2011");
        savedReportsPage.setEndDate("01/01/2015");
        Integer filesCountBefore = new File(tempFolderForDownloads).list().length;
        savedReportsPage.submitReport();
        FileUtils.waitForFileToBeDownloaded(filesCountBefore);

        CompareUtils.verifyDownloadedCsv(reportName, testGroupFolder);

    }

    @Test
    public void USAT_71_USALive_ReportsS_avedReports_Accounting_deviceDeactivationsPng() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();

        openAccountingTab();
        String reportName = "Device Deactivations";

        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.clickReportLink(reportName);
        savedReportsPage.setBeginDate("02/02/2011");
        savedReportsPage.setEndDate("02/02/2011");
        savedReportsPage.submitReport();
        savedReportsPage.verifyReportTitleVisibility(reportName);
        CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
    }

    //Dynamic
    @Test
    public void USAT_71_USALive_ReportsS_avedReports_Accounting_deviceMIDConfigurationCsv() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();

        openAccountingTab();
        String reportName = "Device MID Configuration";

        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        Integer filesCountBefore = new File(tempFolderForDownloads).list().length;
        savedReportsPage.getReportIconByType(reportName, "csv").click();

//        savedReportsPage.submitReport();
        FileUtils.waitForFileToBeDownloaded(filesCountBefore);

//        CompareUtils.verifyDownloadedCsv(reportName, testGroupFolder);

    }

    //Dynamic
    @Test
    public void USAT_71_USALive_ReportsS_avedReports_Accounting_deviceSalesDetailsCsv() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();

        openAccountingTab();
        String reportName = "Device Sales Details";

        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        Integer filesCountBefore = new File(tempFolderForDownloads).list().length;
        savedReportsPage.getReportIconByType(reportName, "csv").click();

//        savedReportsPage.submitReport();
        FileUtils.waitForFileToBeDownloaded(filesCountBefore);

//        CompareUtils.verifyDownloadedCsv(reportName, testGroupFolder);

    }

    //Dynamic
    @Test
    public void USAT_71_USALive_ReportsS_avedReports_Accounting_eftSummaryAccountingCsv() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();

        openAccountingTab();
        String reportName = "EFT Summary Accounting";

        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);

        savedReportsPage.clickReportLink(reportName);
        savedReportsPage.setBeginDate("01/14/2011");
        savedReportsPage.setEndDate("01/14/2011");
        Integer filesCountBefore = new File(tempFolderForDownloads).list().length;
        savedReportsPage.submitReport();
        FileUtils.waitForFileToBeDownloaded(filesCountBefore);

//        CompareUtils.verifyDownloadedCsv(reportName, testGroupFolder);

    }

    @Test
    public void USAT_71_USALive_ReportsS_avedReports_Accounting_highProcessFeeTransactionSummaryWithDeviceCsv() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();

        openAccountingTab();
        String reportName = "High Process Fee Transaction Summary With Device";

        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);

        savedReportsPage.clickReportLink(reportName);
        savedReportsPage.setBeginDate("12/01/2014");
        savedReportsPage.setEndDate("01/01/2015");
        Integer filesCountBefore = new File(tempFolderForDownloads).list().length;
        savedReportsPage.submitReport();
        FileUtils.waitForFileToBeDownloaded(filesCountBefore);

        CompareUtils.verifyDownloadedCsv(reportName, testGroupFolder);

    }

    //Dynamic
    @Test
    public void USAT_71_USALive_ReportsS_avedReports_Accounting_newCustomersPng() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();

        openAccountingTab();
        String reportName = "New Customers";

        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.clickReportLink(reportName);
        savedReportsPage.setBeginDate("03/07/2012");
        savedReportsPage.setEndDate("06/06/2012");
        savedReportsPage.submitReport();
        savedReportsPage.verifyReportTitleVisibility(reportName);
        CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
//        System.out.println(driver.findElements(By.xpath("//*[@class='suggestions-results']/a")).size());
    }

    @Test
    public void USAT_71_USALive_ReportsS_avedReports_Accounting_offerSignupsPng() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();

        openAccountingTab();
        String reportName = "Offer Signups";

        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.clickReportLink(reportName);
        savedReportsPage.setBeginDate("01/01/2014");
        savedReportsPage.setEndDate("08/08/2014");
        savedReportsPage.submitReport();
//        savedReportsPage.verifyReportTitleVisibility(reportName);
        CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
    }

    @Test
    public void USAT_71_USALive_ReportsS_avedReports_Accounting_orphanDeviceHistoryCsv() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();

        openAccountingTab();
        String reportName = "Orphan Device History";

        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);

        savedReportsPage.clickReportLink(reportName);
        savedReportsPage.setMonth("February");
        savedReportsPage.setYear("2010");
        Integer filesCountBefore = new File(tempFolderForDownloads).list().length;
        savedReportsPage.submitReport();
        FileUtils.waitForFileToBeDownloaded(filesCountBefore);

        CompareUtils.verifyDownloadedCsv(reportName, testGroupFolder);
    }

//Dynamic
    @Test
    public void USAT_71_USALive_ReportsS_avedReports_Accounting_prepaidBalanceByCustomerPng() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();

        openAccountingTab();
        String reportName = "Prepaid Balance by Customer";

        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.clickReportLink(reportName);
        savedReportsPage.verifyReportTitleVisibility(reportName);
//        CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
    }

    //Dynamic
    @Test
    public void USAT_71_USALive_ReportsS_avedReports_Accounting_transactionProcessingEntrySummaryCsv() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();

        openAccountingTab();
        String reportName = "Transaction Processing Entry Summary";

        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);

        savedReportsPage.clickReportLink(reportName);
        savedReportsPage.setBeginDate("01/01/2011");
        savedReportsPage.setEndDate("01/01/2012");
        Integer filesCountBefore = new File(tempFolderForDownloads).list().length;
        savedReportsPage.submitReport();
        FileUtils.waitForFileToBeDownloaded(filesCountBefore);

//        CompareUtils.verifyDownloadedCsv(reportName, testGroupFolder);
    }

    //Dynamic
    @Test
    public void USAT_71_USALive_ReportsS_avedReports_Accounting_transactionProcessingEntrySummaryWithDeviceCsv() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();

        openAccountingTab();
        String reportName = "Transaction Processing Entry Summary With Device";

        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);

        savedReportsPage.clickReportLink(reportName);
        savedReportsPage.setBeginDate("03/01/2011");
        savedReportsPage.setEndDate("03/01/2011");
        Integer filesCountBefore = new File(tempFolderForDownloads).list().length;
        savedReportsPage.submitReport();
//        savedReportsPage.waitForFileToBeDownloaded(filesCountBefore);

//        CompareUtils.verifyDownloadedCsv(reportName, testGroupFolder);
    }

    //Dynamic
    @Test
    public void USAT_71_USALive_ReportsS_avedReports_Accounting_unpaidMoniesCsv() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();

        openAccountingTab();
        String reportName = "Unpaid Monies";

        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);

        savedReportsPage.clickReportLink(reportName);
        savedReportsPage.setMonth("February");
        savedReportsPage.setYear("2010");
        Integer filesCountBefore = new File(tempFolderForDownloads).list().length;
        savedReportsPage.submitReport();
//        savedReportsPage.waitForFileToBeDownloaded(filesCountBefore);

//        CompareUtils.verifyDownloadedCsv(reportName, testGroupFolder);
    }

    @Test
    public void USAT_71_USALive_ReportsS_avedReports_Accounting_userPrivilegesPng() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();

        openAccountingTab();
        String reportName = "User Privileges";

        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.clickReportLink(reportName);
        savedReportsPage.submitReport();
//        savedReportsPage.verifyReportTitleVisibility(reportName);
        CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
    }

    private void openAccountingTab()
    {
//        LeftPanel leftPanel =  new LeftPanel(driver);
//        leftPanel.clickSavedReports();
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.savedReports.navigateTo();
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.clickAccountingTab();
    }

}
