package UsaLive.Reports.SavedReports;

import helper.CompareUtils;
import helper.Core;
import helper.FileUtils;
import org.junit.Ignore;
import org.junit.Test;
import pages.UsaLive.LeftPanel;
import pages.UsaLive.LogInPage;
import pages.UsaLive.TopPanel;
import pages.UsaLive.Reports.SavedReports.SavedReportsPage;

public class DailyOperationsTests extends Core
{
    private static String testGroupFolder = "dailyOperations/";

    @Test
    public void USAT_72_USALive_ReportsS_avedReports_DailyOperations_busiestTimesReportPng() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openDailyOperationsTab();
        verifyReportPngUsingModifiedUrl("Busiest Times", "01/01/2015", "02/02/2015");
    }

    @Test
    public void USAT_72_USALive_ReportsS_avedReports_DailyOperations_dailyBreakdownReportPng() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openDailyOperationsTab();
        verifyReportPngUsingModifiedUrl("Daily Breakdown", "01/15/2015", "01/20/2015");
    }

    @Test
    public void USAT_72_USALive_ReportsS_avedReports_DailyOperations_dayByDayDetailReportPng() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openDailyOperationsTab();
        verifyReportPngUsingModifiedUrl("Day By Day Detail", "01/15/2015", "01/19/2015");
    }

    @Test
    public void USAT_72_USALive_ReportsS_avedReports_DailyOperations_deviceDetailsReportPng() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openDailyOperationsTab();
        verifyReportPngUsingModifiedUrl("Device Details", "Terminal Details", "01/01/2015", "01/15/2015");
    }

    @Ignore("Dynamic")
    @Test
    public void USAT_72_USALive_ReportsS_avedReports_DailyOperations_deviceHealthReportReportPng() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openDailyOperationsTab();
        verifyReportPngUsingModifiedUrl("Device Health Report", "01/01/2015", "01/15/2015");
    }

    @Test
    public void USAT_72_USALive_ReportsS_avedReports_DailyOperations_deviceSettingsReportPng() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openDailyOperationsTab();
        verifyReportPngUsingModifiedUrl("Device Settings", "01/01/2015", "01/15/2015");
    }

    //Dynamic
    @Test
    public void USAT_72_USALive_ReportsS_avedReports_DailyOperations_devicesForRecentCallInsReportPng() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openDailyOperationsTab();
        verifyReportPngUsingModifiedUrl("Devices For Recent Call-Ins", "Device Call In - Please Select a Device", "01/01/2015", "01/15/2015");
    }

    @Ignore ("Dynamic")
    @Test
    public void USAT_72_USALive_ReportsS_avedReports_DailyOperations_ePortVendingEquipmentCompatibilityChartExcelReportXlsx() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        String reportName = "ePort Vending Equipment Compatibility Chart(Excel)";
        openDailyOperationsTab();

        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.downloadReportByLinkClicking(reportName);

        CompareUtils.verifyDownloadedXml(reportName, testGroupFolder);
    }

    @Test
    public void USAT_72_USALive_ReportsS_avedReports_DailyOperations_fillSummaryReportPng() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        String reportName = "Fill Summary";
        openDailyOperationsTab();

        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.clickReportLink(reportName);
        savedReportsPage.setBeginDate("01/01/2014");
        savedReportsPage.setEndDate("03/03/2014");
        savedReportsPage.submitReport();
        savedReportsPage.verifyReportTitleVisibility(reportName);

        CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
    }

    @Ignore("No data")
    @Test
    public void USAT_72_USALive_ReportsS_avedReports_DailyOperations_paymentReconciliationReportPng() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        String reportName = "Payment Reconciliation";
        openDailyOperationsTab();

        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.setBeginDate("01/01/2011");
        savedReportsPage.setEndDate("01/01/2016");
        savedReportsPage.submitReport();
        savedReportsPage.verifyReportTitleVisibility(reportName);

        CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
    }

    @Test
    public void USAT_72_USALive_ReportsS_avedReports_DailyOperations_salesByCardIdReportPng() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        String reportName = "Sales by Card Id";
        openDailyOperationsTab();

        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.clickReportLink(reportName);
        savedReportsPage.setBeginDate("01/01/2015");
        savedReportsPage.setEndDate("03/03/2015");
        savedReportsPage.submitReport();
        savedReportsPage.verifyReportTitleVisibility(reportName);

        CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
    }

    @Test
    public void USAT_72_USALive_ReportsS_avedReports_DailyOperations_salesRollupReportXml() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        String reportName = "Sales Rollup";
        openDailyOperationsTab();
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.clickReportLink(reportName);
        savedReportsPage.setAllDevices();
        savedReportsPage.setBeginDate("01/01/2015");
        savedReportsPage.setEndDate("03/03/2015");
        Integer filesCountBefore = savedReportsPage.getFilesCountInDownloadingDir();
        savedReportsPage.submitReport();
        FileUtils.waitForFileToBeDownloaded(filesCountBefore);
        CompareUtils.verifyDownloadedXml(reportName, testGroupFolder);
    }

    @Test
    public void USAT_72_USALive_ReportsS_avedReports_DailyOperations_sevenDaySummaryReportPng() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openDailyOperationsTab();
        verifyReportPngUsingModifiedUrl("Seven Day Summary", "01/01/2015", "03/03/2015");
    }

    @Ignore //Sprout is not used anymore
    @Test
    public void USAT_72_USALive_ReportsS_avedReports_DailyOperations_sproutDevicesReportPng() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openDailyOperationsTab();
        verifyReportPngUsingModifiedUrl("Sprout Devices", "01/01/2015", "03/03/2015");
    }

    @Test
    public void USAT_72_USALive_ReportsS_avedReports_DailyOperations_unsettledTransactionsLast30DaysReportPng() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openDailyOperationsTab();
        verifyReportPngUsingModifiedUrl("Unsettled Transactions (Last 30 Days)", "01/01/2015", "03/03/2015");
    }

    public void verifyReportPngUsingModifiedUrl(String reportName, String expectedReportTitle, String beginDate, String endDate)  throws Exception {
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.runHtmlReportUsingModifiedUrl(reportName, beginDate, endDate, expectedReportTitle);
        CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
    }

    public void verifyReportPngUsingModifiedUrl(String reportName, String beginDate, String endDate) throws Exception {
        verifyReportPngUsingModifiedUrl(reportName, reportName, beginDate, endDate);
    }

    private void openDailyOperationsTab()
    {
//        LeftPanel leftPanel =  new LeftPanel(driver);
//        leftPanel.clickSavedReports();
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.savedReports.navigateTo();
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.clickDailyOperationsTab();
    }

}
