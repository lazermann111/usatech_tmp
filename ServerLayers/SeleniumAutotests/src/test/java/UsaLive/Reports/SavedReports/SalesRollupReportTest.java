package UsaLive.Reports.SavedReports;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import helper.Core;
import pages.UsaLive.LeftPanel;
import pages.UsaLive.LogInPage;
import pages.UsaLive.TopPanel;
import pages.UsaLive.Reports.SavedReports.SavedReportsPage;

public class SalesRollupReportTest extends Core{
	@Test
	public void USAT72_USALive_Reports_SavedReport_SalesRollupReport() throws InterruptedException, IOException {
		openUsaLive();
		new LogInPage(driver).logInToUsaLiveAsPowerUser();
//		new LeftPanel(driver).clickSavedReports();
		TopPanel topPanel = new TopPanel(driver);
		topPanel.reports.savedReports.navigateTo();
		
		SavedReportsPage savedReportPage = new SavedReportsPage(driver);
		savedReportPage.clickDailyOperationsTab();

		savedReportPage.clickByReportName("Sales Rollup");
		savedReportPage.selectAllDevices();
		savedReportPage.setStartDate("05/01/2018");
		savedReportPage.setEndDate("05/31/2018");
		savedReportPage.selectHTMLReport();
		Integer buildReportTime = savedReportPage.runSalesRollupReport();
		logger.info("The report has built in " + buildReportTime + " milliseconds");
		assertTrue(buildReportTime < 13238);
	}
}
