package UsaLive.Reports.SavedReports;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Test;

import helper.Core;
import pages.UsaLive.LogInPage;
import pages.UsaLive.TopPanel;
import pages.UsaLive.Reports.ReportsPage;

public class MonthToDateTests extends Core {

    @Test
    public void USAT_73_USALive_Reports_SavedReports_MtdSalesByDayOfWeekForAllDevices_Html() throws IOException {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.savedReports.navigateTo();

	ReportsPage reportsPage = new ReportsPage(driver);
	reportsPage.salesAnalysisTab.mtdSalesByDayOfWeekForAllDevices.html.open();

	Integer buildReportTime = reportsPage.compareHtmlReport("MTD Sales By Day of Week For All Devices", "salesanalysis/");
	logger.info("The report has built in " + buildReportTime + " milliseconds");
	assertTrue(buildReportTime < 17925);
    }

    @Test
    public void USAT_73_USALive_Reports_SavedReports_MtdSalesByHourOfDayForAllDevices_Html() throws IOException {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.savedReports.navigateTo();

	ReportsPage reportsPage = new ReportsPage(driver);
	reportsPage.salesAnalysisTab.mtdSalesByHourOfDayForAllDevices.html.customPeriodReport("01/01/2018", "01/31/2018");


	Integer buildReportTime = reportsPage.compareHtmlReport("MTD Sales By Hour of Day For All Devices",
		"salesanalysis/");
	logger.info("The report has built in " + buildReportTime + " milliseconds");
	assertTrue(buildReportTime < 17925);
    }

    @Test
    public void USAT_73_USALive_Reports_SavedReports_MtdSalesForEachDeviceByTransactionType_Html() throws IOException {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.savedReports.navigateTo();

	ReportsPage reportsPage = new ReportsPage(driver);
	reportsPage.salesAnalysisTab.mtdSalesForEachDeviceByTransactionType.html.open();

	Integer buildReportTime = reportsPage.compareHtmlReport("MTD Sales for each Device By Transaction Type",
		"salesanalysis/");
	logger.info("The report has built in " + buildReportTime + " milliseconds");
	assertTrue(buildReportTime < 17925);
    }

    @Test
    public void USAT_73_USALive_Reports_SavedReports_MtdSalesSummary_Html() throws IOException {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.savedReports.navigateTo();

	ReportsPage reportsPage = new ReportsPage(driver);
	reportsPage.salesAnalysisTab.mtdSalesSummary.html.open();

	Integer buildReportTime = reportsPage.compareHtmlReport("MTD Sales Summary", "salesanalysis/");
	logger.info("The report has built in " + buildReportTime + " milliseconds");
	assertTrue(buildReportTime < 27925);
    }
}
