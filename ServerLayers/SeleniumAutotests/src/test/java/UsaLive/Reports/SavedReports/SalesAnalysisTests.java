package UsaLive.Reports.SavedReports;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import helper.Core;
import pages.UsaLive.LeftPanel;
import pages.UsaLive.LogInPage;
import pages.UsaLive.TopPanel;
import pages.UsaLive.Reports.SavedReports.SavedReportsPage;

public class SalesAnalysisTests extends Core {
    @Test
    public void USAT_74_USALive_Reports_SavedReports_SalesAnalysis_CashCredit() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();
//	new LeftPanel(driver).clickSavedReports();
        
        TopPanel topPanel = new TopPanel(driver);
        topPanel.reports.savedReports.navigateTo();
	
	SavedReportsPage savedReportPage = new SavedReportsPage(driver);
	savedReportPage.clickSalesAnalysisTab();


	Integer buildReportTime = savedReportPage.runCashCreditAnalysisReport();
	logger.info("The report has built in " + buildReportTime + " milliseconds");
	assertTrue(buildReportTime < 13108);


    }
    
    @Test
    public void USAT_74_USALive_Reports_SavedReports_SalesAnalysis_Last60Days() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();
//	new LeftPanel(driver).clickSavedReports();
        
        TopPanel topPanel = new TopPanel(driver);
        topPanel.reports.savedReports.navigateTo();
	
	SavedReportsPage savedReportPage = new SavedReportsPage(driver);
	savedReportPage.clickSalesAnalysisTab();


	Integer buildReportTime = savedReportPage.runLast60DaysAnalysisReport();
	logger.info("The report has built in " + buildReportTime + " milliseconds");
	assertTrue(buildReportTime < 11930);


    }
    
    @Test
    public void USAT_74_USALive_Reports_SavedReports_SalesAnalysis_HottestItems() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();
//	new LeftPanel(driver).clickSavedReports();
        
        TopPanel topPanel = new TopPanel(driver);
        topPanel.reports.savedReports.navigateTo();
	
	SavedReportsPage savedReportPage = new SavedReportsPage(driver);
	savedReportPage.clickSalesAnalysisTab();


	Integer buildReportTime = savedReportPage.runHottestItemsReport();
	logger.info("The report has built in " + buildReportTime + " milliseconds");
	assertTrue(buildReportTime < 15968);


    }
    
    @Test
    public void USAT_74_USALive_Reports_SavedReports_SalesAnalysis_TransactionTrends() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();
//	new LeftPanel(driver).clickSavedReports();
        
        TopPanel topPanel = new TopPanel(driver);
        topPanel.reports.savedReports.navigateTo();
	
	SavedReportsPage savedReportPage = new SavedReportsPage(driver);
	savedReportPage.clickSalesAnalysisTab();


	Integer buildReportTime = savedReportPage.runTransactionTrendsReport();
	logger.info("The report has built in " + buildReportTime + " milliseconds");
	assertTrue(buildReportTime < 10824);


    }
}
