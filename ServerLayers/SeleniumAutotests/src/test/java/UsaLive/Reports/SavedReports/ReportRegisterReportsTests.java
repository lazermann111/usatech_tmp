package UsaLive.Reports.SavedReports;

import helper.CompareUtils;
import helper.Core;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
//import org.seleniumhq.jetty7.util.log.Log;

import pages.UsaLive.LeftPanel;
import pages.UsaLive.LogInPage;
import pages.UsaLive.TopPanel;
import pages.UsaLive.Reports.SavedReports.SavedReportsPage;

public class ReportRegisterReportsTests extends Core {
    private static String testGroupFolder = "reportRegisterReports/";

    @Test
    public void USAT_77_USALive_ReportsS_avedReports_ReportRegisterReports_eightDaySummaryByRegionXls() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openReportRegisterReportsTab();
        String reportName = "8 Day Summary By Region";
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);        
        Long reportSeconds = savedReportsPage.runReportWithSettings(reportName, "12/12/2014", "01/01/2015", null, "excel");             
        CompareUtils.verifyDownloadedXml(reportName, testGroupFolder);
        Assert.assertTrue("Content is fine but reports buids too long." + reportSeconds + " seconds instead of 15", reportSeconds < 15);
        
    }

    @Test
    public void USAT_77_USALive_ReportsS_avedReports_ReportRegisterReports_activityByFillDatePng() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openReportRegisterReportsTab();
        String reportName = "Activity By Fill Date";
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        Long reportSeconds = savedReportsPage.runReportWithSettings(reportName, "01/01/2015", "03/03/2015", null, "html");
        CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
        Assert.assertTrue("Content is fine but reports buids too long." + reportSeconds + " seconds instead of 20", reportSeconds < 20);
    }

    @Ignore("No data")
    @Test
    public void USAT_77_USALive_ReportsS_avedReports_ReportRegisterReports_declinedMOREAuthorizationsPng() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openReportRegisterReportsTab();
        String reportName = "Declined MORE Authorizations";
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.runReportWithSettings(reportName, "01/01/2015", "03/03/2015", null, "html");
        CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
    }

    @Ignore("Dynamic data")
    @Test
    public void USAT_77_USALive_ReportsS_avedReports_ReportRegisterReports_deviceExceptionReportXls() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openReportRegisterReportsTab();
        String reportName = "Device Exception Report";
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.runReportWithSettings(reportName, null, "03/03/2013", null, "excel");
        CompareUtils.verifyDownloadedXml(reportName, testGroupFolder);
    }

    @Ignore("Dynamic data")
    @Test
    public void USAT_77_USALive_ReportsS_avedReports_ReportRegisterReports_deviceHealthReportXls() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openReportRegisterReportsTab();
        String reportName = "Device Health Report";
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.runReportWithSettings(reportName, null, null, null, "excel");
        CompareUtils.verifyDownloadedXml(reportName, testGroupFolder);
    }

    @Test
    public void USAT_77_USALive_ReportsS_avedReports_ReportRegisterReports_dexExceptionsXls() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openReportRegisterReportsTab();
        String reportName = "Dex Exceptions";
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        Long reportSeconds = savedReportsPage.runReportWithSettings(reportName, "04/30/2015", "05/08/2015", null, "excel");
        CompareUtils.verifyDownloadedXml(reportName, testGroupFolder);
        Assert.assertTrue("Content is fine but reports buids too long." + reportSeconds + " seconds instead of 80", reportSeconds < 80);
    }

    @Test
    public void USAT_77_USALive_ReportsS_avedReports_ReportRegisterReports_dexStatusXls() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openReportRegisterReportsTab();
        String reportName = "Dex Status";
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.runReportWithSettings(reportName, "07/01/2015", "07/01/2015", null, "excel");
        CompareUtils.verifyDownloadedXml(reportName, testGroupFolder);
    }

    @Test
    public void USAT_77_USALive_ReportsS_avedReports_ReportRegisterReports_fillSummaryXls() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openReportRegisterReportsTab();
        String reportName = "Fill Summary";
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.runReportWithSettings(reportName, "02/20/2014", "05/05/2014", null, "excel");
        CompareUtils.verifyDownloadedXml(reportName, testGroupFolder);
    }

    @Test
    public void USAT_77_USALive_ReportsS_avedReports_ReportRegisterReports_fillSummaryCSVCsv() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openReportRegisterReportsTab();
        String reportName = "Fill Summary - CSV";
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        Long reportSeconds = savedReportsPage.runReportWithSettings(reportName, "02/20/2014", "05/05/2014", null, "csv");
        CompareUtils.verifyDownloadedCsv(reportName, testGroupFolder);
        Assert.assertTrue("Content is fine but reports buids too long." + reportSeconds + " seconds instead of 15", reportSeconds < 15);
    }

    @Ignore("No data")
    @Test
    public void USAT_77_USALive_ReportsS_avedReports_ReportRegisterReports_fillSummaryNoCashXls() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openReportRegisterReportsTab();
        String reportName = "Fill Summary - no Cash";
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.runReportWithSettings(reportName, "01/01/2013", "03/03/2015", null, "excel");
        CompareUtils.verifyDownloadedXml(reportName, testGroupFolder);
    }

    @Ignore("No data")
    @Test
    public void USAT_77_USALive_ReportsS_avedReports_ReportRegisterReports_moreActivationsPng() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openReportRegisterReportsTab();
        String reportName = "MORE Activations";
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.runReportWithSettings(reportName, "01/01/2015", "03/03/2015", null, "html");
        CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
    }

    @Ignore("No data")
    @Test
    public void USAT_77_USALive_ReportsS_avedReports_ReportRegisterReports_paymentExceptionsPng() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openReportRegisterReportsTab();
        String reportName = "Payment Exceptions";
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.runReportWithSettings(reportName, "01/01/2015", "03/03/2015", null, "html");
        CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
    }

    @Ignore("No data")
    @Test
    public void USAT_77_USALive_ReportsS_avedReports_ReportRegisterReports_paymentReconciliationByDateRangeCSVCsv() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openReportRegisterReportsTab();
        String reportName = "Payment Reconciliation by Date Range - CSV";
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.runReportWithSettings(reportName, "01/01/2015", "03/03/2015", null, "csv");
        CompareUtils.verifyDownloadedCsv(reportName, testGroupFolder);
    }

    @Test
    public void USAT_77_USALive_ReportsS_avedReports_ReportRegisterReports_salesActivityByDayPng() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openReportRegisterReportsTab();
        String reportName = "Sales Activity By Day";
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.runReportWithSettings(reportName, "01/10/2015", "01/22/2015", null, "html");
        CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
    }

    @Test
    public void USAT_77_USALive_ReportsS_avedReports_ReportRegisterReports_salesByCardIdCsv() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openReportRegisterReportsTab();
        String reportName = "Sales by Card Id";
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.runReportWithSettings(reportName, "01/01/2015", "03/03/2015", null, "csv");
        CompareUtils.verifyDownloadedCsv(reportName, testGroupFolder);
    }

    @Test
    public void USAT_77_USALive_ReportsS_avedReports_ReportRegisterReports_salesRollupXls() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openReportRegisterReportsTab();
        String reportName = "Sales Rollup";
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.runReportWithSettings(reportName, "01/01/2015", "03/03/2015", null, "excel");
        CompareUtils.verifyDownloadedXml(reportName, testGroupFolder);
//        savedReportsPage.runReportWithSettings(reportName, "01/01/2015", "03/03/2015", null, "html");
//        CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
    }

    @Ignore("No data")
    @Test
    public void USAT_77_USALive_ReportsS_avedReports_ReportRegisterReports_settledCreditExportCsv() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openReportRegisterReportsTab();
        String reportName = "Settled Credit Export";
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.runReportWithSettings(reportName, "01/01/2015", "03/03/2015", null, "csv");
        CompareUtils.verifyDownloadedCsv(reportName, testGroupFolder);
    }

    @Test
    public void USAT_77_USALive_ReportsS_avedReports_ReportRegisterReports_terminalDetailsCSVCsv() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openReportRegisterReportsTab();
        String reportName = "Terminal Details (CSV)";
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.runReportWithSettings(reportName, null, null, null, "csv");
        CompareUtils.verifyDownloadedCsv(reportName, testGroupFolder);
    }

    @Test
    public void USAT_77_USALive_ReportsS_avedReports_ReportRegisterReports_terminalDetailsExcelXls() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openReportRegisterReportsTab();
        String reportName = "Terminal Details (Excel)";
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.runReportWithSettings(reportName, null, null, null, "excel");
        CompareUtils.verifyDownloadedXml(reportName, testGroupFolder);
    }

    @Ignore("Dynamic")
    @Test
    public void USAT_77_USALive_ReportsS_avedReports_ReportRegisterReports_zeroTransReportCsv() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openReportRegisterReportsTab();
        String reportName = "Zero Trans Report";
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.downloadReportByLinkClicking(reportName);
        CompareUtils.verifyDownloadedCsv(reportName, testGroupFolder);
    }

    private void openReportRegisterReportsTab()
    {
//        LeftPanel leftPanel =  new LeftPanel(driver);
//        leftPanel.clickSavedReports();
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.savedReports.navigateTo();
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.clickReportRegisterReportsTab();
    }
}
