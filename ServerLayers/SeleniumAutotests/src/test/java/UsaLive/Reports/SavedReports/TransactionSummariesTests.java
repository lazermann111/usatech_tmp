package UsaLive.Reports.SavedReports;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Ignore;
import org.junit.Test;

import com.usatech.test.ClientEmulatorDevTest;

import helper.Core;
import helper.OracleConnector;
import pages.UsaLive.LeftPanel;
import pages.UsaLive.LogInPage;
import pages.UsaLive.Administration.IssueRefundsPage;
import pages.UsaLive.Reports.SavedReports.SavedReportsPage;

public class TransactionSummariesTests extends Core {
	
	@Test
	@Ignore
	public void USAT_176_USALive_Reports_SavedReports_TransactionSummaries_RefundSummary() throws Exception {
		Date dateNow = new Date();
		dateNow = new Date(dateNow.getTime() - (1000 * 60));//To make sure transaction appears in report
		String formattedDateNow = new SimpleDateFormat("MM/dd/yyyy").format(dateNow);	
		String formattedDateNowMDYH = new SimpleDateFormat("MM/dd/yyyy hh:").format(dateNow); //AM/PM
		String formattedDateNowMDY = new SimpleDateFormat("MM/dd/yyyy").format(dateNow);
		Date dateTomorrow = new Date(dateNow.getTime() + (1000 * 60 * 60 * 24));
		String formattedDateTomorrow = new SimpleDateFormat("MM/dd/yyyy").format(dateTomorrow);
		
		openUsaLive(); 
		new LogInPage(driver).logInToUsaLiveAsNotLdapUser();		

        //Simulate transaction
		//To get transaction with Access Card Type type use any card with Demo payment type
		ClientEmulatorDevTest clientEmulatorDevTest = new ClientEmulatorDevTest();		
		//String CC_DATA =  "4173270594781389=25121019999888877776";
//		String CC_DATA =  "6396212016817467620=2112008374368";
//		String maskedCC_DATA="639621*********7620";
//		String cardId = "1000597421";
//		String deviceSerialNumber = "VJ011000008" ;//ECC
//		String deviceName = "TD004203";
//		
//		if(getEnvironment().equalsIgnoreCase("int"))
//		{
//			cardId = "744028";
//			deviceName = "TD002325";
//		}
		String CC_DATA =  "4173270594781389=25121019999888877776";
		String maskedCC_DATA="417327******1389";
		String cardId = "1000597421";
		String deviceSerialNumber = "VJ011000008" ;//ECC
		String deviceName = "TD004203";
		
		if(getEnvironment().equalsIgnoreCase("int"))
		{
			cardId = "1000058601";
			deviceName = "TD002325";
		}
		OracleConnector oracleConnector = new OracleConnector(getEnvironment());   
		String encKeyFormDb= oracleConnector.getEncryptionKey(deviceName);;
				
		String refundAmount = "1.00";
		String refundComment = "autotests on " + formattedDateNow;
		clientEmulatorDevTest.testSell(getEnvironment(), CC_DATA, deviceSerialNumber, deviceName, encKeyFormDb);
	
		//Wait for data appearance
		Thread.sleep(240000);	
		
		//Issue refund
        LeftPanel leftPanel =  new LeftPanel(driver);
        leftPanel.clickMenuItemByText("Issue Refunds");
        
        IssueRefundsPage issueRefundsPage = new IssueRefundsPage(driver);
        issueRefundsPage.setStartDate(formattedDateNow);
        issueRefundsPage.setEndDate(formattedDateTomorrow);
//        issueRefundsPage.setCardId(cardId);
        issueRefundsPage.setCardFirstTwoToSix(CC_DATA);
        issueRefundsPage.clickFindTransactionsButton();
        issueRefundsPage.checkTransactionComboBox();
        issueRefundsPage.clickIssueRefundsButton();
        
        issueRefundsPage.setRefundAmount(refundAmount);
        issueRefundsPage.setComment(refundComment);
        String refundReason = "Operator Test";
        issueRefundsPage.setRefundReason(refundReason);
        issueRefundsPage.clickIssueRefundButton();
        issueRefundsPage.verifySuccessfullyCreatedRefundMessage(); 
        
		//Wait for data appearance
		Thread.sleep(120000);		
		
		//Run report
        leftPanel.clickSavedReports();
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.clickTransactionSummariesTab();
        savedReportsPage.clickReportLink("Refund Summary");
        savedReportsPage.setBeginDate(formattedDateNow);
        savedReportsPage.setEndDate(formattedDateTomorrow);
        savedReportsPage.clickRunReport();
        
		//Verify data in report       
        savedReportsPage.verifyLocation("TBD");
        savedReportsPage.verifyTableValue(deviceSerialNumber, "Card Number", maskedCC_DATA);
        savedReportsPage.verifyTableValueContains(deviceSerialNumber, "Transaction Date", formattedDateNowMDYH);        
        savedReportsPage.verifyTableValue(deviceSerialNumber, "Transaction Amount", "$ 5.95 USD");
        savedReportsPage.verifyTableValue(deviceSerialNumber, "Refund Date", formattedDateNowMDY);
        savedReportsPage.verifyTableValue(deviceSerialNumber, "Refund Amount", "$ -1.00 USD");
        savedReportsPage.verifyTableValue(deviceSerialNumber, "Reason", refundReason);
        savedReportsPage.verifyTableValue(deviceSerialNumber, "Card Id", cardId);             
	}
	
	@Test
	@Ignore
	public void USAT_176_USALive_Reports_SavedReports_TransactionSummaries_PassAccessCardSummary() throws Exception {
		
		openUsaLive(); 
		new LogInPage(driver).logInToUsaLiveAsNotLdapUser();		
        
        //Simulate transaction
		//To get transaction with Access Card Type type use any card with Demo payment type !!!!!!!!!!
		ClientEmulatorDevTest clientEmulatorDevTest = new ClientEmulatorDevTest();	
//		String CC_DATA =  "6396212016817467620=2112008374368";
//		String maskedCC_DATA="639621*********7620";
//		String cardId = "1000597421";
		String CC_DATA =  "4173270594781389=25121019999888877776";
		String maskedCC_DATA="417327******1389";
		String cardId = "1000597421";
		String deviceSerialNumber = "VJ011000008" ;//ECC
		String deviceName = "TD004203";
		
		if(getEnvironment().equalsIgnoreCase("int"))
		{
			cardId = "1000058601";
			deviceName = "TD002325";
		}
		OracleConnector oracleConnector = new OracleConnector(getEnvironment());
		String encKeyFormDb= oracleConnector.getEncryptionKey(deviceName);;
		clientEmulatorDevTest.testSell(getEnvironment(), CC_DATA,  deviceSerialNumber, deviceName, encKeyFormDb);
		
		//Wait for data appearance
		Thread.sleep(120000);		
		
		//Run report
        LeftPanel leftPanel =  new LeftPanel(driver);
        leftPanel.clickSavedReports();
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.clickTransactionSummariesTab();
        savedReportsPage.clickReportLink("Pass-Access Card Summary");


		Date dateNow = new Date();
		dateNow = new Date(dateNow.getTime() - (1000 * 60));//To make sure transaction appears in report
		String formattedDateNow = new SimpleDateFormat("MM/dd/yyyy").format(dateNow);	
		String formattedDateNowCropped = new SimpleDateFormat("MM/dd/yyyy hh:").format(dateNow);
		Date dateTomorrow = new Date(dateNow.getTime() + (1000 * 60 * 60 * 24));
		String formattedDateTomorrow = new SimpleDateFormat("MM/dd/yyyy").format(dateTomorrow);
		
        savedReportsPage.setBeginDate(formattedDateNow);
        savedReportsPage.setEndDate(formattedDateTomorrow);
        savedReportsPage.clickRunReport();
        
		//Verify data in report
//        savedReportsPage.verifyLocation("TBD");
        
        
        savedReportsPage.verifyTableValue(cardId, "Location", "TBD");
        savedReportsPage.verifyTableValue(cardId, "Device", deviceSerialNumber);
        savedReportsPage.verifyTableValueContains(cardId, "Transaction Date", formattedDateNowCropped);
//        savedReportsPage.verifyCardNumber(maskedCC_DATA);   
        String details = "011E($1.10), 002F($1.10), 001E(3 * $1.25)";;
        savedReportsPage.verifyTableValue(cardId, "Details", details);
        savedReportsPage.verifyTableValue(cardId, "Quantity", "5");
        savedReportsPage.verifyTableValue(cardId, "Amount", "$ 5.95 USD");
//        savedReportsPage.verifyTableValue("Card Id", cardId);                  
	}
	
	
	@Test
	@Ignore
	public void USAT_176_USALive_Reports_SavedReports_TransactionSummaries_CreditCardSummary() throws Exception {
		ClientEmulatorDevTest clientEmulatorDevTest = new ClientEmulatorDevTest();	
//		clientEmulatorDevTest.testSell();
		
		openUsaLive(); 
		new LogInPage(driver).logInToUsaLiveAsNotLdapUser();		
        
        //Simulate transaction
//		ClientEmulatorDevTest clientEmulatorDevTest = new ClientEmulatorDevTest();		
//		String CC_DATA =  "4173270594781389=25121019999888877776";
//		String maskedCC_DATA="417327******1389";
		String CC_DATA =  "4173270594781389=25121019999888877776";
		String maskedCC_DATA="417327******1389";
		String cardId = "1000597421";//ECC
		String deviceSerialNumber = "VJ011000014";
		String deviceName = "TD004218";	//ECC	
		if(getEnvironment().equalsIgnoreCase("int"))
		{
			cardId = "1000058601";
			deviceName = "TD002331";
		}
		
//		String deviceSerialNumber = "VJ011000008" ;//ECC
//		String deviceName = "TD004203";
//		
//		if(getEnvironment().equalsIgnoreCase("int"))
//		{
//			cardId = "744028";
//			deviceName = "TD002325";
//		}
		
		OracleConnector oracleConnector = new OracleConnector(getEnvironment());
		String encKeyFormDb= oracleConnector.getEncryptionKey(deviceName);;
		
		Date dateNow = new Date();
		dateNow = new Date(dateNow.getTime() - (1000 * 60)); //To make sure transaction appears in report
		String formattedDateNow = new SimpleDateFormat("MM/dd/yyyy").format(dateNow);	
		String formattedDateNowCropped = new SimpleDateFormat("MM/dd/yyyy hh:").format(dateNow);
		Date dateTomorrow = new Date(dateNow.getTime() + (1000 * 60 * 60 * 24));
		String formattedDateTomorrow = new SimpleDateFormat("MM/dd/yyyy").format(dateTomorrow);
		
		String env = getEnvironment();
		clientEmulatorDevTest.testSell(getEnvironment(), CC_DATA, deviceSerialNumber, deviceName, encKeyFormDb);
		
		//Wait for data appearance
		Thread.sleep(60000);		
		
		//Run report
        LeftPanel leftPanel =  new LeftPanel(driver);
        leftPanel.clickSavedReports();
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.clickTransactionSummariesTab();
        savedReportsPage.clickReportLink("Credit Card Summary");
        

        
        savedReportsPage.setBeginDate(formattedDateNow);
        savedReportsPage.setEndDate(formattedDateTomorrow);
        savedReportsPage.clickRunReport();
        
		//Verify data in report
        savedReportsPage.verifyLocation("TBD");
        
        savedReportsPage.verifyTableValueContains(deviceSerialNumber, "Transaction Date", formattedDateNowCropped);
//        savedReportsPage.verifyCardNumber(maskedCC_DATA);   
//        String details = "0110($1.10), 0115($1.10), 0112(3 * $1.25)";
        String details = "011E($1.10), 002F($1.10), 001E(3 * $1.25)";
        savedReportsPage.verifyTableValue(deviceSerialNumber, "Details", details);
        savedReportsPage.verifyTableValue(deviceSerialNumber, "Quantity", "5");
        savedReportsPage.verifyTableValue(deviceSerialNumber, "Amount", "$ 5.95 CAD");
        savedReportsPage.verifyTableValue(deviceSerialNumber, "Card Id", cardId);                  
	}
	
	@Test
	@Ignore
	public void USAT_176_USALive_Reports_SavedReports_TransactionSummaries_CardUsage() throws Exception {
		Date dateNow = new Date();
		dateNow = new Date(dateNow.getTime() - (1000 * 60));//To make sure transaction appears in report
		String formattedDateNow = new SimpleDateFormat("MM/dd/yyyy hh:mm").format(dateNow);
		String formattedDateNowCropped = new SimpleDateFormat("MM/dd/yyyy hh:").format(dateNow);
		Date dateTomorrow = new Date(dateNow.getTime() + (1000 * 60 * 60 * 24));
		String formattedDateTomorrow = new SimpleDateFormat("MM/dd/yyyy hh:mm").format(dateTomorrow);
		
		openUsaLive(); 
		new LogInPage(driver).logInToUsaLiveAsNotLdapUser();		

		
        //Simulate transaction
		ClientEmulatorDevTest clientEmulatorDevTest = new ClientEmulatorDevTest();		
		String CC_DATA =  "4173270594781389=25121019999888877776";
		String maskedCC_DATA="417327******1389";
		String cardId = "1000597421";
		String deviceSerialNumber = "VJ011000008" ;//ECC
		String deviceName = "TD004203";		
		if(getEnvironment().equalsIgnoreCase("int"))
		{
			cardId = "1000058601";
			deviceName = "TD002325";
		}
		OracleConnector oracleConnector = new OracleConnector(getEnvironment());
		String encKeyFormDb= oracleConnector.getEncryptionKey(deviceName);;
		
		clientEmulatorDevTest.testSell(getEnvironment(), CC_DATA, deviceSerialNumber, deviceName, encKeyFormDb);
		
		//Wait for data appearance
		Thread.sleep(60000);		
		
        LeftPanel leftPanel =  new LeftPanel(driver);
        leftPanel.clickSavedReports();
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.clickTransactionSummariesTab();
        savedReportsPage.clickReportLink("Card Usage");
        savedReportsPage.setBeginDate(formattedDateNow);
        savedReportsPage.setEndDate(formattedDateTomorrow);
        savedReportsPage.setCardId(cardId);
        savedReportsPage.clickRunReport();
        
//        savedReportsPage.verifyNoDataFoundMessagePresence();
        

//		savedReportsPage.clickRefreshDataButton();		
        
		//Verify data in report
        savedReportsPage.verifyCardNumber(maskedCC_DATA);        
        savedReportsPage.verifyTableValue(cardId, "Location", "TBD");
        savedReportsPage.verifyTableValue(cardId, "Device", deviceSerialNumber);
        savedReportsPage.verifyTableValueContains(cardId, "Transaction Date", formattedDateNowCropped);
        String details = "011E($1.10), 002F($1.10), 001E(3 * $1.25)";;
        savedReportsPage.verifyTableValue(cardId, "Details", details);
        savedReportsPage.verifyTableValue(cardId, "Quantity", "5");
        savedReportsPage.verifyTableValue(cardId, "Amount", "$ 5.95 USD");	
	}

}
