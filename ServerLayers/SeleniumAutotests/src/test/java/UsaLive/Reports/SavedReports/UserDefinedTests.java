package UsaLive.Reports.SavedReports;

import helper.CompareUtils;
import helper.Core;
import org.junit.Test;
import pages.UsaLive.LeftPanel;
import pages.UsaLive.LogInPage;
import pages.UsaLive.TopPanel;
import pages.UsaLive.Reports.ReportsPage;
import pages.UsaLive.Reports.SavedReports.SavedReportsPage;

public class UserDefinedTests extends Core {
    private static String testGroupFolder = "userDefined/";
    //--------- *.xls ---------
    @Test
    public void USAT_78_USALive_ReportsS_avedReports_UserDefined_detailedReportXls() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        verifyUserDefinedReportXls("Detailed Report");
    }

    @Test
    public void USAT_78_USALive_ReportsS_avedReports_UserDefinedsimpleReportXls() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        verifyUserDefinedReportXls("Simple Report");
    }

    @Test
    public void USAT_78_USALive_ReportsS_avedReports_UserDefinedsummaryReportXls() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        verifyUserDefinedReportXls("Summary Report");
    }

    //--------- *.doc ---------
    @Test
    public void USAT_78_USALive_ReportsS_avedReports_UserDefineddetailedReportDoc() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        verifyUserDefinedReportDoc("Detailed Report");
    }

    @Test
    public void USAT_78_USALive_ReportsS_avedReports_UserDefinedsimpleReportDoc() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        verifyUserDefinedReportDoc("Simple Report");
    }

    @Test
    public void USAT_78_USALive_ReportsS_avedReports_UserDefinedsummaryReportDoc() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        verifyUserDefinedReportDoc("Summary Report");
    }

    //--------- *.pdf ---------
    @Test
    public void USAT_78_USALive_ReportsS_avedReports_UserDefineddetailedReportPdf() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        verifyUserDefinedReportPdf("Detailed Report");
    }

    @Test
    public void USAT_78_USALive_ReportsS_avedReports_UserDefinedsimpleReportPdf() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        verifyUserDefinedReportPdf("Simple Report");
    }

    @Test
    public void USAT_78_USALive_ReportsS_avedReports_UserDefinedsummaryReportPdf() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        verifyUserDefinedReportPdf("Summary Report");
    }

    //--------- *.csv ---------
    @Test
    public void USAT_78_USALive_ReportsS_avedReports_UserDefineddetailedReportCsv() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        verifyUserDefinedReportCsv("Detailed Report");
    }

    @Test
    public void USAT_78_USALive_ReportsS_avedReports_UserDefinedsimpleReportCsv() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        verifyUserDefinedReportCsv("Simple Report");
    }

    @Test
    public void USAT_78_USALive_ReportsS_avedReports_UserDefinedsummaryReportCsv() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        verifyUserDefinedReportCsv("Summary Report");
    }

    //--------- *.png ---------
    @Test
    public void USAT_78_USALive_ReportsS_avedReports_UserDefinedbarGraphReportPng() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openUserDefinedTab();
        new ReportsPage(driver).verifyReportPngUsingUrl("Bar Graph Report", testGroupFolder);
    }

    @Test
    public void USAT_78_USALive_ReportsS_avedReports_UserDefineddetailedReportPng() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openUserDefinedTab();
        new ReportsPage(driver).verifyReportPngUsingUrl("Detailed Report", testGroupFolder);
    }

    @Test
    public void USAT_78_USALive_ReportsS_avedReports_UserDefinedsimpleReportPng() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openUserDefinedTab();
        new ReportsPage(driver).verifyReportPngUsingUrl("Simple Report", testGroupFolder);
    }

    @Test
    public void USAT_78_USALive_ReportsS_avedReports_UserDefinedsummaryReportPng() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        openUserDefinedTab();
        new ReportsPage(driver).verifyReportPngUsingUrl("Summary Report", testGroupFolder);
    }

    public void verifyUserDefinedReportXls(String reportName) throws Exception {
//        LeftPanel leftPanel =  new LeftPanel(driver);
//        leftPanel.clickSavedReports();
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.savedReports.navigateTo();
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.clickUserDefinedTab();
        savedReportsPage.runXlsReportUsingUrl(reportName,"01/01/2015", "03/03/2015");
        CompareUtils.verifyDownloadedXml(reportName, testGroupFolder);
    }

    private void verifyUserDefinedReportDoc(String reportName) throws Exception {
//        LeftPanel leftPanel =  new LeftPanel(driver);
//        leftPanel.clickSavedReports();
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.savedReports.navigateTo();
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.clickUserDefinedTab();
        savedReportsPage.runDocReportUsingUrl(reportName, "01/01/2015", "03/03/2015");
        CompareUtils.verifyDownloadedDoc(reportName, testGroupFolder);
    }

    public void verifyUserDefinedReportPdf(String reportName/*, Integer expectedPagesCount*/) throws Exception {
//        LeftPanel leftPanel =  new LeftPanel(driver);
//        leftPanel.clickSavedReports();
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.savedReports.navigateTo();
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.clickUserDefinedTab();
        savedReportsPage.runPdfReportUsingUrl(reportName,"01/01/2015", "03/03/2015");
        CompareUtils.verifyDownloadedPdf(reportName, 120, testGroupFolder);
    }

    public void verifyUserDefinedReportCsv(String reportName) throws Exception {
//        LeftPanel leftPanel =  new LeftPanel(driver);
//        leftPanel.clickSavedReports();
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.savedReports.navigateTo();
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.clickUserDefinedTab();
        savedReportsPage.runCsvReportUsingUrl(reportName, "01/01/2015", "03/03/2015");
        CompareUtils.verifyDownloadedCsv(reportName, testGroupFolder);
    }

    public void openUserDefinedTab() throws Exception {
//        LeftPanel leftPanel =  new LeftPanel(driver);
//        leftPanel.clickSavedReports();
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.savedReports.navigateTo();
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.clickUserDefinedTab();
    }
}
