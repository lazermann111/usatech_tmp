package UsaLive.Reports.SavedReports;

import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Test;

import helper.Core;
import helper.FileUtils;
import pages.UsaLive.LeftPanel;
import pages.UsaLive.LogInPage;
import pages.UsaLive.TopPanel;
import pages.UsaLive.Reports.SavedReports.SavedReportsPage;

public class YearToDateTests extends Core {

    // Dynamic
    @Test
    public void USAT_75_USALive_Reports_SavedReports_YearToDate_ByDay() throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();
//	new LeftPanel(driver).clickSavedReports();
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.savedReports.navigateTo();

	SavedReportsPage savedReportPage = new SavedReportsPage(driver);
	savedReportPage.clickSalesAnalysisTab();

	Integer buildReportTime = savedReportPage.runYearToDateByDayReport();
	logger.info("The report has built in " + buildReportTime + " milliseconds");
	assertTrue(buildReportTime < 10970);

    }

    @Test
    public void USAT_75_USALive_Reports_SavedReports_YearToDate_ByHour() throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();
//	new LeftPanel(driver).clickSavedReports();
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.savedReports.navigateTo();

	SavedReportsPage savedReportPage = new SavedReportsPage(driver);
	savedReportPage.clickSalesAnalysisTab();

	Integer buildReportTime = savedReportPage.runYearToDateByHourReport();
	logger.info("The report has built in " + buildReportTime + " milliseconds");
	assertTrue(buildReportTime < 11433);

    }

    @Test
    public void USAT_75_USALive_Reports_SavedReports_YearToDate_ForEach() throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();
//	new LeftPanel(driver).clickSavedReports();
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.savedReports.navigateTo();

	SavedReportsPage savedReportPage = new SavedReportsPage(driver);
	savedReportPage.clickSalesAnalysisTab();
	Integer buildReportTime = savedReportPage.runYearToDateForEachReport();
	logger.info("The report has built in " + buildReportTime + " milliseconds");

	if (getEnvironment().equals("int")) {
	    assertTrue(buildReportTime < 21864);
	}
	if (getEnvironment().equals("ecc")) {
	    assertTrue(buildReportTime < 34128);
	}

    }

    @Test
    public void USAT_75_USALive_Reports_SavedReports_YearToDate_Summary() throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();
//	new LeftPanel(driver).clickSavedReports();
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.savedReports.navigateTo();

	SavedReportsPage savedReportPage = new SavedReportsPage(driver);
	savedReportPage.clickSalesAnalysisTab();

	Integer buildReportTime = savedReportPage.runYearToDateSummaryReport();
	logger.info("The report has built in " + buildReportTime + " milliseconds");
	assertTrue(buildReportTime < 13583);

    }
}
