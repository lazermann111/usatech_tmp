package UsaLive.Reports;

import helper.CompareUtils;
import helper.Core;
import org.junit.Test;
import pages.UsaLive.LeftPanel;
import pages.UsaLive.LogInPage;
import pages.UsaLive.TopPanel;
import pages.UsaLive.Reports.ReportsPage;

public class DexStatusTests extends Core
{
    private static String testGroupFolder = "";

    @Test
    public void USAT_85_USALive_Reports_DexStatus() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

        String reportName = "Dex Status";
//        LeftPanel leftPanel =  new LeftPanel(driver);
//        leftPanel.clickMenuItemByText(reportName);
        TopPanel topPanel = new TopPanel(driver);
        topPanel.reports.dexStatus.navigateTo();

        ReportsPage reportsPage = new ReportsPage(driver);
//        reportsPage.setAllDevices();
        reportsPage.setBeginDate(new String[]{"June", "01", "2015", "9:00 am"});
        reportsPage.setEndDate(new String[]{"June", "03", "2015", "9:00 am"});
        reportsPage.setAllDevices();
        reportsPage.submitReport();
        reportsPage.verifyReportTitleVisibility(reportName);

        Thread.sleep(5000);
        CompareUtils.verifyPng(driver, wait, reportName, testGroupFolder);
    }

}
