package UsaLive.Reports.RecentReports;

import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import helper.Core;
import pages.UsaLive.LeftPanel;
import pages.UsaLive.LogInPage;
import pages.UsaLive.TopPanel;
import pages.UsaLive.Reports.RecentReportPage;
import pages.UsaLive.Reports.BuildReport.BuildReportPage;
import pages.UsaLive.Reports.SavedReports.SavedReportsPage;
import simple.param.ResultsRadioParamEditor;

public class RecentReportsTests extends Core {
    @Test
    @Ignore
    public void USAT_68_USALive_Reports_RecentReports_View() throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsCustomerUser();
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.buildReport.navigateTo();

//	SavedReportsPage savedReportPage = new SavedReportsPage(driver);
//	savedReportPage.clickYearToDateTab();
	BuildReportPage buildReportPage = new BuildReportPage(driver);
	buildReportPage.switchToSummaryTab();
	buildReportPage.setReportBeginMonth("May");
	buildReportPage.setReportBeginDay("01");
	buildReportPage.setReportBeginYear("2018");
	buildReportPage.setReportEndMonth("May");
	buildReportPage.setReportEndDay("31");
	buildReportPage.setReportEndYear("2018");
	// buildReportPage.setColumnsTotalByValue("Device");
	buildReportPage.setColumnsTotalByValue("Client");
	buildReportPage.setAllDataValuesChecked();
	buildReportPage.waitForDeviceSearchTreeExpanded();
	buildReportPage.setAllDeviceSearchTreeChecked();
	buildReportPage.clickSubmitButton();

	RecentReportPage recentReportPage = new RecentReportPage(driver);
	// Integer buildReportTime = savedReportPage.runYearToDateByDayReport();
//	recentReportPage.runYearToDateByDayReport();
//	leftPanel.clickRecentReports();
//	Assert.assertTrue(recentReportPage.getPageTitle().equals("Report Request History for Arkadiy Hachikyan Gmail (arkadiy.hachikyan@gmail.com)"));
//	recentReportPage.openRandomRecentReport();
//	Assert.assertTrue(recentReportPage.isReportHeaderExist());
//	Assert.assertTrue(recentReportPage.isReportContentExist());
//	leftPanel.clickRecentReports();
	topPanel.reports.recentReports.navigateTo();
	Assert.assertTrue(recentReportPage.getPageTitle().equals("Report Request History for Arkadiy Hachikyan Gmail (arkadiy.hachikyan@gmail.com)"));
	recentReportPage.openLastReport();
	Assert.assertTrue(recentReportPage.isReportHeaderExist());
	Assert.assertTrue(recentReportPage.isReportContentExist());
	topPanel.reports.recentReports.navigateTo();
	Assert.assertTrue(recentReportPage.getPageTitle().equals("Report Request History for Arkadiy Hachikyan Gmail (arkadiy.hachikyan@gmail.com)"));
	recentReportPage.openLastReport();
	recentReportPage.clickRefreshCurrentButton();
	Assert.assertTrue(recentReportPage.isReportHeaderExist());
	Assert.assertTrue(recentReportPage.isReportContentExist());
	topPanel.reports.recentReports.navigateTo();
	Assert.assertTrue(recentReportPage.getPageTitle().equals("Report Request History for Arkadiy Hachikyan Gmail (arkadiy.hachikyan@gmail.com)"));
	recentReportPage.openLastReport();
	recentReportPage.clickConfigureParametrsButton();
	recentReportPage.runConfiguredReport();
	Assert.assertTrue(recentReportPage.isReportHeaderExist());
	Assert.assertTrue(recentReportPage.isReportContentExist());
	recentReportPage.runPDFReport();
	recentReportPage.runWordReport();
	recentReportPage.runExcelReport();
	recentReportPage.runCSVReport();
	Thread.sleep(10000);
    }
}
