package UsaLive.Administration.ReportRegister;

import helper.Core;
import helper.EmailUtils;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import pages.UsaLive.Administration.RegisteredReportsPage;
import pages.UsaLive.LeftPanel;
import pages.UsaLive.LogInPage;
import pages.UsaLive.TopPanel;
import pages.UsaLive.Reports.SavedReports.SavedReportsPage;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static helper.FileUtils.waitForFileToBeDownloaded;

/**
 * Created by ahachikyan on 3/2/2016.
 */
public class ReportsTest extends Core {

    @Test
    public void USAT_320_USALive_Administration_ReportRegister_RunReportSample() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsPowerUser();
//    	openUsaLive(); new LogInPage(driver).logInToUsaLiveAsWeakUser();

//        LeftPanel leftPanel =  new LeftPanel(driver);
//        leftPanel.clickMenuItemByText("Report Register");
        
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

        RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);

//specific "Device Alerts", "DEX File", "DWI File"
        String[] htmlReports =  new String[]{"8 Day Summary By Region","Activity By Fill Date",  "Declined MORE Authorizations",  "MORE Activations", "Payment Detail for EFT", "Payment Exceptions", "Payment Summary by Region", "Pending Payment Summary (Html)", "Sales Activity By Batch", "Sales Activity By Day" ,"Transactions Included in EFT"};
        String[] htmlReportsTitles =  new String[]{"8 Day Summary By Region", "Activity By Fill Date","Declined MORE Authorizations",  "MORE Activations", "Payment Detail for EFT", "Payment Exceptions", "Payment Summary by Region", "Pending Payment Summary", "Sales Activity By Batch", "Sales Activity By Day" ,"Transactions Included in EFT"};
        String[] notHtmlReports =  new String[]{"Activity By Fill Date And Region", "Activity By Fill Date And Region-Credit", "Daily Item Export", "Device Exception Report", "Device Health Report", "Devices with low sales", "Dex Exceptions", "Dex Status", "Custom File Upload", "EFT Data Export", "EFT Trans Reference Export", "Fill Summary", "Fill Summary - no Cash", "Fill Summary - CSV","Payment Reconciliation - CSV","Payment Batches by Region", "Payment Reconciliation by Date Range - CSV",  "Payment Reconciliation by Week", "Payment Item Export", "Payroll Deduct - Card Details", "Payroll Deduct - Total Sales by Employee Id", "Payroll Deduct - Transactions by Employee Id", "Payroll Deduct - Unactivated Cards", "Pending Payment Summary (Excel)" , "Sales by Card Id", "Sales Rollup", "Settled Credit Export", "Single Transaction Data Export(CSV)", "Sprout Transaction Line Item Data Export", "Terminal Details (CSV)", "Terminal Details (Excel)", /*"Transaction Data Export", */"Transaction Data Included in EFT (by Device)", "Transaction Line Item Data Export", "Zero Trans Report"};
//        Boolean[] notHtmlReportsHasBathes =  new Boolean[]{true, true, false, true, true, true, true, true, true, true, true, true, true, "Fill Summary - CSV","Payment Reconciliation - CSV","Payment Batches by Region", "Payment Reconciliation by Date Range - CSV",  "Payment Reconciliation by Week", "Payment Item Export", "Payroll Deduct - Card Details", "Payroll Deduct - Total Sales by Employee Id", "Payroll Deduct - Transactions by Employee Id", "Payroll Deduct - Unactivated Cards", "Pending Payment Summary (Excel)" , "Sales by Card Id", "Sales Rollup", "Settled Credit Export", "Single Transaction Data Export(CSV)", "Sprout Transaction Line Item Data Export", "Terminal Details (CSV)", "Terminal Details (Excel)", "Transaction Data Export", "Transaction Data Included in EFT (by Device)", "Transaction Line Item Data Export", "Zero Trans Report"};

        for(int i = 0; i < htmlReports.length; i++)
        {
            String reportName = htmlReports[i];
            logger.info("ReportName=" + reportName);
            registeredReportsPage.setReportName(reportName);
            registeredReportsPage.clickRunReportSampleLink();
            if(!registeredReportsPage.isNoBathesForSampleMessagePresent()) {
                savedReportsPage.verifyReportTitleVisibility(htmlReportsTitles[i]);
                driver.navigate().back();
            }

        }


        for(int i = 0; i < notHtmlReports.length; i++)
        {
            String reportName = notHtmlReports[i];
            logger.info("ReportName=" + reportName);
            registeredReportsPage.setReportName(reportName);

            if(reportName.equals("Transaction Line Item Data Export"))
            {
                int e = 3;
            }

            Integer filesCountBefore = new File(tempFolderForDownloads).list().length;


            registeredReportsPage.clickRunReportSampleLink();

            if(!registeredReportsPage.isNoBathesForSampleMessagePresent()) {
                waitForFileToBeDownloaded(filesCountBefore);
                driver.navigate().back();
            }
        }

    }

    @Test
    public void USAT_141_USALive_Administration_ReportRegister_SsaveRegisteredReportWithoutEmailVerificationTest() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();
        saveRegisteredReportWithoutEmailVerification();
    }


    public void saveRegisteredReportWithoutEmailVerification() throws Exception {
//        LeftPanel leftPanel =  new LeftPanel(driver);
//        leftPanel.clickMenuItemByText("Report Register");
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

        RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);

        registeredReportsPage.verifySaveButtonEnabled(false);
        registeredReportsPage.verifyCancelButtonEnabled(false);
        registeredReportsPage.verifyCreateButtonEnabled(true);
        registeredReportsPage.verifyDeleteButtonEnabled(false);

        String reportName = "8 Day Summary By Region";
        registeredReportsPage.setReportName(reportName);
        registeredReportsPage.verifySaveButtonEnabled(true);
        registeredReportsPage.verifyCancelButtonEnabled(true);
        registeredReportsPage.verifyCreateButtonEnabled(false);
        registeredReportsPage.verifyDeleteButtonEnabled(false);
//        registeredReportsPage.getSaveButton().click();
//        registeredReportsPage.verifyAvailableTransportsRequired();

        String transportName = "Test Transport Email 1";
        if(!registeredReportsPage.isTransportExist(transportName))
        {
            registeredReportsPage.addTransportWithEmailType(transportName, "usalive.test@gmail.com");
            registeredReportsPage.verifyAvailableTransportsValuesPresence(Arrays.asList("-- Please Select --", transportName));
        }
        registeredReportsPage.setTransport(transportName);

        String registeredReportName = reportName + " (" + transportName +  ")";
        Integer reportsCount1 = registeredReportsPage.getReportsCount(registeredReportName);

        registeredReportsPage.getSaveButton().click();
        registeredReportsPage.verifyStatusMessage("Report was saved");
        Integer reportsCount2 = registeredReportsPage.getReportsCount(registeredReportName);

        if(reportsCount2 <= reportsCount1)
        {
            Assert.fail("Report has not been added!");
        }

        registeredReportsPage.verifySaveButtonEnabled(false);
        registeredReportsPage.verifyCancelButtonEnabled(false);
        registeredReportsPage.verifyCreateButtonEnabled(true);
        registeredReportsPage.verifyDeleteButtonEnabled(true);
    }

    @Test
    public void USAT_316_USALive_Administration_ReportRegister_Edit() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsPowerUser();

//        LeftPanel leftPanel =  new LeftPanel(driver);
//        leftPanel.clickMenuItemByText("Report Register");
        
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();
	

        RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);

        if(registeredReportsPage.getReportsCount() == 0)
        {
            saveRegisteredReportWithoutEmailVerification();
        }


        String registeredReportName = registeredReportsPage.getAnyExistingReportName();
        registeredReportsPage.selectRegisteredReport(registeredReportName);
        registeredReportsPage.setFrequency("Daily");
        registeredReportsPage.getSaveButton().click();

        registeredReportsPage.selectRegisteredReport(registeredReportName);
        registeredReportsPage.verifyFrequencySelectedValue("Daily");

        registeredReportsPage.selectRegisteredReport(registeredReportName);
        registeredReportsPage.setFrequency("Hourly");
        registeredReportsPage.getSaveButton().click();

        registeredReportsPage.selectRegisteredReport(registeredReportName);
        registeredReportsPage.verifyFrequencySelectedValue("Hourly");

    }

    @Test
    public void USAT_317_USALive_Administration_ReportRegister_DeletionOfTransportAssociatedWithExistingReportsFails() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsPowerUser();

//        LeftPanel leftPanel =  new LeftPanel(driver);
//        leftPanel.clickMenuItemByText("Report Register");
        
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

        RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
        String transportName = "Test Transport Email 1";
        String reportName = "8 Day Summary By Region";
        String registeredReportName = reportName + " (" + transportName +  ")";

        Integer reportsCount1 = registeredReportsPage.getReportsCount(registeredReportName);
        if(reportsCount1 == 0)
        {
            saveRegisteredReportWithoutEmailVerification();
        }

        registeredReportsPage.selectRegisteredReport(registeredReportName);
        registeredReportsPage.getDeleteTransportButton().click();
        registeredReportsPage.getTransportDeleteFailedMessage();
    }

    @Test
    public void USAT_318_USALive_Administration_ReportRegister_DeleteRegisteredReport() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsPowerUser();

//        LeftPanel leftPanel =  new LeftPanel(driver);
//        leftPanel.clickMenuItemByText("Report Register");
        
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

        RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
        String transportName = "Test Transport Email 1";
        String reportName = "8 Day Summary By Region";
        String registeredReportName = reportName + " (" + transportName +  ")";
        Integer reportsCount1 = registeredReportsPage.getReportsCount(registeredReportName);
        if(reportsCount1 == 0)
        {
            saveRegisteredReportWithoutEmailVerification();
        }

        registeredReportsPage.selectRegisteredReport(registeredReportName);
        registeredReportsPage.getDeleteButton().click();
        registeredReportsPage.confirmDeletion();

        Integer reportsCount2 = registeredReportsPage.getReportsCount(registeredReportName);
        if(reportsCount2 >= reportsCount1 && (reportsCount1 == reportsCount2 && reportsCount1 !=0))
        {
            Assert.fail("Report has not been deleted!");
        }
        registeredReportsPage.verifyStatusMessage("Report was deleted");
    }

    @Test
//TODO: Skip for INT
    public void USAT_319_USALive_Administration_ReportRegister_SaveRegisteredReportWithEmailVerification() throws Exception {
        if (getEnvironment().equalsIgnoreCase("ecc")) {
            openUsaLive();
            new LogInPage(driver).logInToUsaLiveAsWeakUser();

//            LeftPanel leftPanel = new LeftPanel(driver);
//            leftPanel.clickMenuItemByText("Report Register");
            
    	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

            RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
            registeredReportsPage.deleteAllExistingRegisteredReports();

            String reportName = "8 Day Summary By Region";
            registeredReportsPage.setReportName(reportName);

            registeredReportsPage.getSaveButton().click();
            registeredReportsPage.verifyAvailableTransportsRequired();

            String transportName = "Test Transport Email 1";
            if (!registeredReportsPage.isTransportExist(transportName)) {
                //!!! Will not work on INT where transport service is off
                registeredReportsPage.addTransportWithEmailType(transportName, "usalive.test@gmail.com");
                registeredReportsPage.verifyAvailableTransportsValuesPresence(Arrays.asList("-- Please Select --", transportName));
            }
            registeredReportsPage.setTransport(transportName);
            registeredReportsPage.setFrequency("Hourly");

            String registeredReportName = reportName + " (" + transportName + ")";
            Integer reportsCount1 = registeredReportsPage.getReportsCount(registeredReportName);

            Integer emailsCountBefore = EmailUtils.getEmailsCount();

            registeredReportsPage.getSaveButton().click();
            registeredReportsPage.verifyStatusMessage("Report was saved");
            Integer reportsCount2 = registeredReportsPage.getReportsCount(registeredReportName);

            if (reportsCount2 <= reportsCount1) {
                Assert.fail("Report has not been added!");
            }
            EmailUtils.waitForNewEmail(emailsCountBefore, 3605);
            //TODO: Add SentReports tab verification
        }
    }
}

