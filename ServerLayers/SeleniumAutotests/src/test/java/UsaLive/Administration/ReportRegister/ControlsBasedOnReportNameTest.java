package UsaLive.Administration.ReportRegister;

import helper.Core;
import org.junit.Test;
import pages.UsaLive.Administration.RegisteredReportsPage;
import pages.UsaLive.LeftPanel;
import pages.UsaLive.LogInPage;
import pages.UsaLive.TopPanel;

import java.util.Arrays;

/**
 * Created by ahachikyan on 3/2/2016.
 */
public class ControlsBasedOnReportNameTest extends Core {

    // private void verifyControls(String reportName, String description, String
    // formatType) throws Exception {
    // openUsaLive(); new LogInPage(driver).logInAsWeakUser();
    //
    // LeftPanel leftPanel = new LeftPanel(driver);
    // leftPanel.clickMenuItemByText("Report Register");
    //
    // RegisteredReportsPage registeredReportsPage = new
    // RegisteredReportsPage(driver);
    // registeredReportsPage.setReportName(reportName);
    // registeredReportsPage.verifyDescription(description);
    // registeredReportsPage.verifyFormatType("Custom File Upload");
    // registeredReportsPage.verifyBatchType("Custom File Upload");
    //// registeredReportsPage.verifyFrequencyValues(Arrays.asList("Weekly (Sunday -
    // Saturday)", "Monthly", "Hourly", "Daily", "Yearly", "Daily (5 am)", "Daily (7
    // am)", "Weekly (Saturday - Friday)", "Weekly (Friday - Thursday)"));
    // registeredReportsPage.verifyAvailableTransportsValuesPresence(Arrays.asList("--
    // Please Select --"));
    //
    // registeredReportsPage.verifyLabelVisibility("Frequency", false);
    // }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportEightDaySummaryByRegion()
	    throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

//	LeftPanel leftPanel = new LeftPanel(driver);
//	leftPanel.clickMenuItemByText("Report Register");
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("8 Day Summary By Region");
	registeredReportsPage.verifyDescription(
		"All transactions in the last 8 days shown by Region, then by Type for each location");
	registeredReportsPage.verifyFormatType("Folio");
	registeredReportsPage.verifyFrequencyValues(
		Arrays.asList("Weekly (Sunday - Saturday)", "Monthly", "Hourly", "Daily", "Yearly", "Daily (5 am)",
			"Daily (7 am)", "Weekly (Saturday - Friday)", "Weekly (Friday - Thursday)"));
	registeredReportsPage.verifyAvailableTransportsValuesPresence(Arrays.asList("-- Please Select --"));

	registeredReportsPage.verifyLabelVisibility("Batch Type", false);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportActivityByFillDate()
	    throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

	// LeftPanel leftPanel = new LeftPanel(driver);
	// leftPanel.clickMenuItemByText("Report Register");

	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Activity By Fill Date");
	registeredReportsPage.verifyDescription(
		"Report shows transactions amount by currency,region,device,location,asset nbr and terminal during the fill begin date and fill end date.");
	registeredReportsPage.verifyFormatType("Folio");
	registeredReportsPage.verifyFrequencyVisibility(true);
	registeredReportsPage.verifyTransportsVisibility(true);
	registeredReportsPage.verifyBatchType(false);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportCustomFileUpload()
	    throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

	// LeftPanel leftPanel = new LeftPanel(driver);
	// leftPanel.clickMenuItemByText("Report Register");
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Custom File Upload");
	registeredReportsPage.verifyDescription("Custom file upload from the device.");
	registeredReportsPage.verifyFormatType("Custom File Upload");
	registeredReportsPage.verifyBatchType("Custom File Upload");
	registeredReportsPage.verifyFrequencyVisibility(false);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportDailyItemExport() throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

	// LeftPanel leftPanel = new LeftPanel(driver);
	// leftPanel.clickMenuItemByText("Report Register");

	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Daily Item Export");
	registeredReportsPage.verifyDescription("Daily transaction item details.");
	registeredReportsPage.verifyFormatType("Folio");
	registeredReportsPage.verifyBatchType("Transactions");
	registeredReportsPage.verifyFrequencyVisibility(false);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportDeclinedMOREAuthorizations()
	    throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

	// LeftPanel leftPanel = new LeftPanel(driver);
	// leftPanel.clickMenuItemByText("Report Register");
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Declined MORE Authorizations");
	registeredReportsPage.verifyDescription(
		"Report that shows detailed more card declined authorization information");
	registeredReportsPage.verifyFormatType("Folio");
	registeredReportsPage.verifyBatchType(false);
	registeredReportsPage.verifyFrequencyVisibility(true);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportDeviceAlerts() throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

	// LeftPanel leftPanel = new LeftPanel(driver);
	// leftPanel.clickMenuItemByText("Report Register");

	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Device Alerts");
	registeredReportsPage.verifyDescription(
		"Alerts will be sent upon detection. The alert parsing method can be selected on the customer preferences and setup page.");
	registeredReportsPage.verifyFormatType("Alert");
	registeredReportsPage.verifyBatchType("Device Event");
	registeredReportsPage.verifyAlertTypes(getEnvironment(), true);
	registeredReportsPage.verifyFrequencyVisibility(false);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportDeviceExceptionReport()
	    throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

	// LeftPanel leftPanel = new LeftPanel(driver);
	// leftPanel.clickMenuItemByText("Report Register");

	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Device Exception Report");
	registeredReportsPage.verifyDescription(
		"This report is the same as Device Health Report except that it only shows records of devices not called in within 3 days or with no credit transaction for 20 days.");
	registeredReportsPage.verifyFormatType("Folio");
	registeredReportsPage.verifyBatchType(false);
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(true);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportDeviceHealthReport()
	    throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

	// LeftPanel leftPanel = new LeftPanel(driver);
	// leftPanel.clickMenuItemByText("Report Register");

	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Device Health Report");
	registeredReportsPage.verifyDescription(
		"Condition report that reports eport first and last credit, cash, transaction date and other health related info.");
	registeredReportsPage.verifyFormatType("Folio");
	registeredReportsPage.verifyBatchType(false);
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(true);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportDexExceptions() throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

	// LeftPanel leftPanel = new LeftPanel(driver);
	// leftPanel.clickMenuItemByText("Report Register");

	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Dex Exceptions");
	registeredReportsPage.verifyDescription(
		"Dex Exceptions Report contains location, address, city, state, dex alert count and dex exception count. The report will only show Dex records that contain Dex exception.");
	registeredReportsPage.verifyFormatType("Folio");
	registeredReportsPage.verifyBatchType(false);
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(true);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportDexFile() throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

	// LeftPanel leftPanel = new LeftPanel(driver);
	// leftPanel.clickMenuItemByText("Report Register");

	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("DEX File");
	registeredReportsPage.verifyDescription("Raw DEX Data as captured on the machine");
	registeredReportsPage.verifyFormatType("DEX File");
	registeredReportsPage.verifyBatchType("DEX data");
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(false);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportDexStatus() throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

	// LeftPanel leftPanel = new LeftPanel(driver);
	// leftPanel.clickMenuItemByText("Report Register");

	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Dex Status");
	registeredReportsPage.verifyDescription(
		"Dex Status Report contains location, address, city, state, dex alert count and dex exception count.");
	registeredReportsPage.verifyFormatType("Folio");
	registeredReportsPage.verifyBatchType(false);
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(true);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportDwiFile() throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

	// LeftPanel leftPanel = new LeftPanel(driver);
	// leftPanel.clickMenuItemByText("Report Register");

	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("DWI File");
	registeredReportsPage.verifyDescription("DEX File in DWI Wrapper");
	registeredReportsPage.verifyFormatType("DWI File");
	registeredReportsPage.verifyBatchType("DEX data");
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(false);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportEftDataExport() throws Exception {
	// openUsaLive(); new LogInPage(driver).logInToUsaLiveAsPowerUser();
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

	// LeftPanel leftPanel = new LeftPanel(driver);
	// leftPanel.clickMenuItemByText("Report Register");

	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("EFT Data Export");
	registeredReportsPage.verifyDescription("Comma-Separated EFT data.");
	registeredReportsPage.verifyFormatType("CSV");
	registeredReportsPage.verifyBatchType("Payments");
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(false);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportEftTransReferenceExport()
	    throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

//	LeftPanel leftPanel = new LeftPanel(driver);
//	leftPanel.clickMenuItemByText("Report Register");
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("EFT Trans Reference Export");
	if (getEnvironment().equals("ecc"))
	    registeredReportsPage.verifyDescription(
		    "Comma-Separated List of transactions included in the stated EFT. Shows Eft Id and Transaction Reference Number.");
	if (getEnvironment().equals("int"))
	    registeredReportsPage.verifyDescription(
		    "Comma-Separated List of transactions included in the stated EFT. Shows details of each transaction.");
	registeredReportsPage.verifyFormatType("CSV");
	registeredReportsPage.verifyBatchType("Payments");
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(false);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportFillSummary() throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

//	LeftPanel leftPanel = new LeftPanel(driver);
//	leftPanel.clickMenuItemByText("Report Register");
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Fill Summary");
	registeredReportsPage.verifyDescription(
		"Shows transaction totals for each Fill Period that was uploaded in the specified time range for any Terminals paid on a Fill-to-Fill schedule");
	registeredReportsPage.verifyFormatType("Folio");
	registeredReportsPage.verifyBatchType(false);
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(true);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportFillSummaryCSV() throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

//	LeftPanel leftPanel = new LeftPanel(driver);
//	leftPanel.clickMenuItemByText("Report Register");
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Fill Summary - CSV");
	registeredReportsPage.verifyDescription(
		"Shows transaction totals for each Fill Period that was uploaded in the specified time range for any Terminals paid on a Fill-to-Fill schedule. Output format is CSV.");
	registeredReportsPage.verifyFormatType("Folio");
	registeredReportsPage.verifyBatchType(false);
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(true);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportFillSummaryNoCash()
	    throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

//	LeftPanel leftPanel = new LeftPanel(driver);
//	leftPanel.clickMenuItemByText("Report Register");
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Fill Summary - no Cash");
	registeredReportsPage.verifyDescription(
		"Shows transaction totals for each Fill Period that was uploaded in the specified time range for any Terminals paid on a Fill-to-Fill schedule");
	registeredReportsPage.verifyFormatType("Folio");
	registeredReportsPage.verifyBatchType(false);
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(true);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportMoreActivations() throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

//	LeftPanel leftPanel = new LeftPanel(driver);
//	leftPanel.clickMenuItemByText("Report Register");
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("MORE Activations");
	registeredReportsPage.verifyDescription("Report that shows new MORE account activations");
	registeredReportsPage.verifyFormatType("Folio");
	registeredReportsPage.verifyBatchType(false);
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(true);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportPaymentBatchesByRegion()
	    throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

//	LeftPanel leftPanel = new LeftPanel(driver);
//	leftPanel.clickMenuItemByText("Report Register");
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Payment Batches by Region");
	registeredReportsPage.verifyDescription(
		"Shows each payment batch within a payment and summarizes by Region in an Excel spreadsheet.");
	registeredReportsPage.verifyFormatType("Folio");
	registeredReportsPage.verifyBatchType("Payments");
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(false);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportPaymentDetailForEFT()
	    throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

//	LeftPanel leftPanel = new LeftPanel(driver);
//	leftPanel.clickMenuItemByText("Report Register");
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Payment Detail for EFT");
	registeredReportsPage.verifyDescription(
		"Displays the Payment Details by Terminal of the stated EFT. Gross Amounts, Fees, Refunds, and Net Amounts are shown.");
	registeredReportsPage.verifyFormatType("Folio");
	registeredReportsPage.verifyBatchType("Payments");
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(false);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportPaymentExceptions()
	    throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

//	LeftPanel leftPanel = new LeftPanel(driver);
//	leftPanel.clickMenuItemByText("Report Register");
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Payment Exceptions");
	registeredReportsPage.verifyDescription(
		"Report that shows bank account rejections and other payment exceptions");
	registeredReportsPage.verifyFormatType("Folio");
	registeredReportsPage.verifyBatchType(false);
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(true);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportPaymentItemExport()
	    throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

//	LeftPanel leftPanel = new LeftPanel(driver);
//	leftPanel.clickMenuItemByText("Report Register");
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Payment Item Export");
	registeredReportsPage.verifyDescription("Payment item details for a payment.");
	registeredReportsPage.verifyFormatType("Folio");
	registeredReportsPage.verifyBatchType("Payments");
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(false);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportPaymentReconciliationCSV()
	    throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

//	LeftPanel leftPanel = new LeftPanel(driver);
//	leftPanel.clickMenuItemByText("Report Register");
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Payment Reconciliation - CSV");
	registeredReportsPage.verifyDescription("");
	registeredReportsPage.verifyFormatType("Folio");
	registeredReportsPage.verifyBatchType("Payments");
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(false);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportPaymentReconciliationByDateRangeCSV()
	    throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

//	LeftPanel leftPanel = new LeftPanel(driver);
//	leftPanel.clickMenuItemByText("Report Register");
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Payment Reconciliation by Date Range - CSV");
	registeredReportsPage.verifyDescription("Payment details");
	registeredReportsPage.verifyFormatType("Folio");
	registeredReportsPage.verifyBatchType(false);
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(true);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportPaymentReconciliationByWeek()
	    throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

//	LeftPanel leftPanel = new LeftPanel(driver);
//	leftPanel.clickMenuItemByText("Report Register");
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Payment Reconciliation by Week");
	registeredReportsPage.verifyDescription("");
	registeredReportsPage.verifyFormatType("Folio");
	registeredReportsPage.verifyBatchType("Payments");
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(false);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportPaymentSummaryByRegion()
	    throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

//	LeftPanel leftPanel = new LeftPanel(driver);
//	leftPanel.clickMenuItemByText("Report Register");

	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();
	
	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Payment Summary by Region");
	registeredReportsPage.verifyDescription(
		"Display # of Trans, Gross Revenue and Process Fees by Region, Client, Location, Device and Asset");
	registeredReportsPage.verifyFormatType("Folio");
	registeredReportsPage.verifyBatchType("Payments");
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(false);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportPendingPaymentSummaryExcel()
	    throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

//	LeftPanel leftPanel = new LeftPanel(driver);
//	leftPanel.clickMenuItemByText("Report Register");
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Pending Payment Summary (Excel)");
	registeredReportsPage.verifyDescription(
		"Pending Payment summary report for the bank account, checked daily at 6:00 am EST, based on the pay cycle, triggered when the EFT balance is below $25.");
	registeredReportsPage.verifyFormatType("Folio");
	registeredReportsPage.verifyBatchType("Condition based");
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(false);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportPendingPaymentSummaryHtml()
	    throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

//	LeftPanel leftPanel = new LeftPanel(driver);
//	leftPanel.clickMenuItemByText("Report Register");
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Pending Payment Summary (Html)");
	registeredReportsPage.verifyDescription(
		"Pending Payment summary report for the bank account, checked daily at 6:00 am EST, based on the pay cycle, triggered when the EFT balance is below $25.");
	registeredReportsPage.verifyFormatType("Folio");
	registeredReportsPage.verifyBatchType("Condition based");
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(false);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportSalesActivityByBatch()
	    throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

//	LeftPanel leftPanel = new LeftPanel(driver);
//	leftPanel.clickMenuItemByText("Report Register");
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Sales Activity By Batch");
	registeredReportsPage.verifyDescription(
		"Displays the Transaction Details of all activity on the Terminals in a Web Page format. The transactions are grouped into batches approximately once a day. The report covers one such batch.");
	registeredReportsPage.verifyFormatType("Folio");
	registeredReportsPage.verifyBatchType("Transactions");
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(false);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportSalesActivityByDay()
	    throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

//	LeftPanel leftPanel = new LeftPanel(driver);
//	leftPanel.clickMenuItemByText("Report Register");
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Sales Activity By Day");
	registeredReportsPage.verifyDescription(
		"Displays the Transaction Details of all activity on the Terminals grouped by each day in a Web Page format.");
	registeredReportsPage.verifyFormatType("Folio");
	registeredReportsPage.verifyBatchType(false);
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(true);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportSalesByCardId() throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

//	LeftPanel leftPanel = new LeftPanel(driver);
//	leftPanel.clickMenuItemByText("Report Register");
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Sales by Card Id");
	registeredReportsPage.verifyDescription(
		"Report with sales grouped by Card Id. Includes: Trans Type Name, Currency Code, Card Id, Card Number, Total Amount, # of Trans, Trans Type Id.");
	registeredReportsPage.verifyFormatType("Folio");
	registeredReportsPage.verifyBatchType(false);
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(true);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportSalesRollup() throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

//	LeftPanel leftPanel = new LeftPanel(driver);
//	leftPanel.clickMenuItemByText("Report Register");
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Sales Rollup");
	registeredReportsPage.verifyDescription(
		"Sales rollup which contains transaciton count, vend count and transaciton amount for each transaction type and customer and location info.");
	registeredReportsPage.verifyFormatType("Folio");
	registeredReportsPage.verifyBatchType(false);
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(true);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportSettledCreditExport()
	    throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

//	LeftPanel leftPanel = new LeftPanel(driver);
//	leftPanel.clickMenuItemByText("Report Register");
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Settled Credit Export");
	registeredReportsPage.verifyDescription(
		"Settled credit transaction export that contains region, location, asset #, device, transaction date, card type, sale amount, and process fee. NOTE: the report will retrieve maximum of 500000 records.");
	registeredReportsPage.verifyFormatType("Folio");
	registeredReportsPage.verifyBatchType(false);
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(true);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportSingleTransactionDataExportCSV()
	    throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

//	LeftPanel leftPanel = new LeftPanel(driver);
//	leftPanel.clickMenuItemByText("Report Register");
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Single Transaction Data Export(CSV)");
	registeredReportsPage.verifyDescription(
		"Single Transaction data in csv format. Includes: Terminal Number, Transaction Reference Number, Transaction Type (Credit, Cash, Pass, etc), Card Number, Total Amount, Vended Columns, Number of Products Vended, Timestamp of the Transaction , Card Id");
	registeredReportsPage.verifyFormatType("CSV");
	registeredReportsPage.verifyBatchType("Single Transaction");
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(false);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportSproutTransactionLineItemDataExport()
	    throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

//	LeftPanel leftPanel = new LeftPanel(driver);
//	leftPanel.clickMenuItemByText("Report Register");
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Sprout Transaction Line Item Data Export");
	registeredReportsPage.verifyDescription(
		"Comma-Separated Sprout Transaction data. Includes: Customer ID, Customer Name, Transaction ID, Machine ID, Transaction Date/Time, Coil Name, Price, Quantity.");
	registeredReportsPage.verifyFormatType("Folio");
	registeredReportsPage.verifyBatchType("Transactions");
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(false);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportTerminalDetailsCSV()
	    throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

//	LeftPanel leftPanel = new LeftPanel(driver);
//	leftPanel.clickMenuItemByText("Report Register");
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Terminal Details (CSV)");
	registeredReportsPage.verifyDescription(
		"Shows detail information on each terminal with the following columns: Customer, Region, Device, Location, Terminal, Asset #, Address, City, State, Zip, Location Details, Location Type, Machine Make, Machine Model, Product Type, Timezone, Vends Per Swipe, Authorization Mode, DEX Data Capture, Terminal Start Date, Activation Submitted Date, Business Type, Communication Method, Firmware Version, Sales Order Number, Network City, Network State, Network Zip, Parent Region, Terminal ID");
	registeredReportsPage.verifyFormatType("Folio");
	registeredReportsPage.verifyBatchType(false);
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(true);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportTerminalDetailsExcel()
	    throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

//	LeftPanel leftPanel = new LeftPanel(driver);
//	leftPanel.clickMenuItemByText("Report Register");
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Terminal Details (Excel)");
	registeredReportsPage.verifyDescription(
		"Shows detail information on each terminal with the following columns: Customer, Region, Device, Location, Terminal, Asset #, Address, City, State, Zip, Location Details, Location Type, Machine Make, Machine Model, Product Type, Timezone, Vends Per Swipe, Authorization Mode, DEX Data Capture, Terminal Start Date, Activation Submitted Date, Business Type, Communication Method, Firmware Version, Sales Order Number, Network City, Network State, Network Zip, Parent Region, Terminal ID");
	registeredReportsPage.verifyFormatType("Folio");
	registeredReportsPage.verifyBatchType(false);
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(true);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportTransactionDataExport()
	    throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

//	LeftPanel leftPanel = new LeftPanel(driver);
//	leftPanel.clickMenuItemByText("Report Register");
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Transaction Data Export");
	registeredReportsPage.verifyDescription(
		"Comma-Separated Transaction data. Includes: Terminal Number, Transaction Reference Number, Transaction Type (Credit, Cash, Pass, etc), Card Number, Total Amount, Vended Columns, Number of Products Vended, Timestamp of the Transaction , Card Id");
	if (getEnvironment().equals("ecc"))
	    registeredReportsPage.verifyFormatType("Folio");
	if (getEnvironment().equals("int"))
	    registeredReportsPage.verifyFormatType("Folio");
	registeredReportsPage.verifyBatchType("Transactions");
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(false);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportTransactionDataIncludedInEFTByDevice()
	    throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

//	LeftPanel leftPanel = new LeftPanel(driver);
//	leftPanel.clickMenuItemByText("Report Register");
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Transaction Data Included in EFT (by Device)");
	registeredReportsPage.verifyDescription(
		"Comma-Separated List of transactions included in the stated EFT. Shows device, details of each transaction and the Transaction Reference Number.");
	registeredReportsPage.verifyFormatType("CSV");
	registeredReportsPage.verifyBatchType("Payments");
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(false);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportTransactionLineItemDataExport()
	    throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

//	LeftPanel leftPanel = new LeftPanel(driver);
//	leftPanel.clickMenuItemByText("Report Register");
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Transaction Line Item Data Export");
	registeredReportsPage.verifyDescription(
		"Comma-Separated Transaction data. Includes: Terminal Number, Transaction Reference Number, Transaction Type (Credit, Cash, Pass, etc), Card Number, Total Amount, Vended Columns, Price, MDB Number, Number of Products Vended,Timestamp Description, Card Id");
	registeredReportsPage.verifyFormatType("Folio");
	registeredReportsPage.verifyBatchType("Transactions");
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(false);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportTransactionsIncludedInEFT()
	    throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

//	LeftPanel leftPanel = new LeftPanel(driver);
//	leftPanel.clickMenuItemByText("Report Register");
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Transactions Included in EFT");
	registeredReportsPage.verifyDescription(
		"Displays the details of each transaction included in the stated EFT in a Web Page format.");
	registeredReportsPage.verifyFormatType("Folio");
	registeredReportsPage.verifyBatchType("Payments");
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(false);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportZeroTransReport() throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

//	LeftPanel leftPanel = new LeftPanel(driver);
//	leftPanel.clickMenuItemByText("Report Register");
	
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName("Zero Trans Report");
	registeredReportsPage.verifyDescription(
		"Comma-Separated List of Terminals that had no transactions in the last 2 days.");
	registeredReportsPage.verifyFormatType("CSV");
	registeredReportsPage.verifyBatchType(false);
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(true);
	registeredReportsPage.verifyTransportsVisibility(true);
    }

    @Test
    public void USAT_322_USALive_Administration_ReportRegister_SettingsForEachReportDefaultReports() throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();
	//
	// LeftPanel leftPanel = new LeftPanel(driver);
	// leftPanel.clickMenuItemByText("Report Register");

	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	registeredReportsPage.setReportName(":: Default Reports ::");
	registeredReportsPage.verifyDescription(
		"Add user default reports to report register. Reports to be restored: Payment Detail for EFT, Transactions Included in EFT, Sales Activity By Batch, Pending Payment Summary (Html).");
	registeredReportsPage.verifyFormatType("BUILD_FOLIO");
	registeredReportsPage.verifyBatchType(false);
	registeredReportsPage.verifyAlertTypes(getEnvironment(), false);
	registeredReportsPage.verifyFrequencyVisibility(false);
	registeredReportsPage.verifyTransportsVisibility(true);
    }
}
