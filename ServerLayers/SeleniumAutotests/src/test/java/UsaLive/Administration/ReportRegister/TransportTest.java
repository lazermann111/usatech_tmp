package UsaLive.Administration.ReportRegister;

import helper.Core;
import org.junit.Test;
import pages.UsaLive.Administration.RegisteredReportsPage;
import pages.UsaLive.LeftPanel;
import pages.UsaLive.LogInPage;
import pages.UsaLive.TopPanel;

import java.util.Arrays;

/**
 * Created by ahachikyan on 3/2/2016.
 */
public class TransportTest extends Core {
    @Test
    public void addTransport() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsPowerUser();

//        LeftPanel leftPanel =  new LeftPanel(driver);
//        leftPanel.clickMenuItemByText("Report Register");
        
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

        RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
        registeredReportsPage.deleteAllExistingRegisteredReports();
        String transportName = "Test Transport Email 1";

        if(registeredReportsPage.isTransportExist(transportName))
        {
            registeredReportsPage.deleteTransport(transportName);
        }

        registeredReportsPage.addTransportWithEmailType(transportName, "usalive.test@gmail.com");
        registeredReportsPage.verifyAvailableTransportsValuesPresence(Arrays.asList("-- Please Select --", transportName));
    }

    @Test
    public void editTransport() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsPowerUser();

//        LeftPanel leftPanel =  new LeftPanel(driver);
//        leftPanel.clickMenuItemByText("Report Register");
        
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

        RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
        String transportName = "Test Transport Email 1";
        if(!registeredReportsPage.isTransportExist(transportName))
        {
            registeredReportsPage.addTransportWithEmailType(transportName, "usalive.test@gmail.com");
            registeredReportsPage.verifyAvailableTransportsValuesPresence(Arrays.asList("-- Please Select --", transportName));
        }

        registeredReportsPage.editTransport(transportName, transportName + "edited");
    }


    @Test
    public void deleteTransport() throws Exception {
        openUsaLive(); new LogInPage(driver).logInToUsaLiveAsPowerUser();

//        LeftPanel leftPanel =  new LeftPanel(driver);
//        leftPanel.clickMenuItemByText("Report Register");
        
	TopPanel topPanel = new TopPanel(driver);
	topPanel.reports.reportRegister.navigateTo();

        RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
        registeredReportsPage.deleteAllExistingRegisteredReports();
        String transportName = "Test Transport Email 1";
        if(registeredReportsPage.isTransportExist(transportName))
        {
            registeredReportsPage.deleteTransport(transportName);
        }

        registeredReportsPage.verifyAvailableTransportsValuesAbsence(Arrays.asList(transportName));
    }


}

