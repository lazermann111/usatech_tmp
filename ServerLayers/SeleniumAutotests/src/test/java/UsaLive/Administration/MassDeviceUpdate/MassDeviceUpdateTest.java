package UsaLive.Administration.MassDeviceUpdate;

import static helper.FileUtils.getResoucePath;

import org.junit.Assert;
import org.junit.Test;

import helper.CompareUtils;
import helper.Core;
import helper.EmailUtils;
import helper.FileUtils;
import pages.UsaLive.LogInPage;
import pages.UsaLive.TopPanel;
import pages.UsaLive.Administration.ColumnMappingsPage;
import pages.UsaLive.Administration.MassDeviceUpdatePage;
import pages.UsaLive.Administration.Devices.DeviceProfilePage;
import pages.UsaLive.Administration.Devices.DevicesPage;

/**
 * Created by ahachikyan on 7/25/2018.
 */
public class MassDeviceUpdateTest extends Core {
	private static String testGroupFolder = "massDeviceUpdate/";

	   @Test
	    public void  USAT_1158_USALive_Administration_MassDeviceUpdate_UploadXLSFile() throws Exception {
		   UploadFileTest("xls");
	   }
	   
	   @Test
	    public void  USAT_1158_USALive_Administration_MassDeviceUpdate_UploadXLSXFile() throws Exception {
		   UploadFileTest("xlsx");
	   }
	   
	   @Test
	    public void  USAT_1158_USALive_Administration_MassDeviceUpdate_UploadCSVFile() throws Exception {
		   UploadFileTest("csv");
	   }
	   
	   
	    public void UploadFileTest(String extention) throws Exception {
	    	String customerName = "";
	        if(getEnvironment().equalsIgnoreCase("ecc")) {
	        	customerName = "Test Account Customer";
	        }
	        else {
	        	customerName = "Auriga Load Testing";
	        }
	    	  
		        String machineAssetNumber1 = "TBD1";
		        String machineAssetNumber2 = "TBD2";
		        String client1 = "client1";
		        String client2 = "client2";
		        String phoneNumberAtLocation1 = "1111111111";
		        String phoneNumberAtLocation2 = "2222222222";
		        String locationName1 = "TBD";
		        String locationName2 = "location2";
		        String address1 = "test address 1";
		        String address2 = "address2";
		        String city1 = "Malvern";
		        String city2 = "Burleson";
		        String state1 = "PA";
		        String state2 = "TX";
		        String zipCode1 = "19355";
		        String zipCode2 = "76028";
		        String country1 = "USA";
		        String country2 = "USA";
		        String specificLocationAtThisAddress1 = "locationDetail1";
		        String specificLocationAtThisAddress2 = "locationDetail2";
		        String locationType1 = "- Not Assigned -";
		        String locationType2 = "Kiosk";
		        String locationTimeZone1 = "Eastern Standard Time (GMT-05:00)";
		        String locationTimeZone2 = "Pacific Standard Time (GMT-08:00)";
		        String machinesMakeAndModel1 = "TBD - - To Be Determined";
		        String machinesMakeAndModel2 = "113 - - AP";
		        String productType1 = "- Not Assigned -";
		        String productType2 = "Snack";
		        String region1 = "AURIGA";
		        String region2 = "Test Region";
		        String paymentSchedule1 = "1";
		        String paymentSchedule2 = "8";
		        String businessType1 = "Auto Dealer";
		        String businessType2 = "Car Wash";
		        String overrideDoingBusinessAs1 = "Automated Testing";
		        String overrideDoingBusinessAs2 = "testBusiness";
		        String overrideCustomerServicePhone1 = "1111111111";
		        String overrideCustomerServicePhone2 = "2222222222";
		        String overrideCustomerServiceEmail1 = "customer.service1@com";
		        String overrideCustomerServiceEmail2 = "customer.service2@com";	 
		        String columnMapName1 = "test2";
		        String columnMapName2 = "test3";
		        String columnMap1 = "1=5|5=6";
		        String columnMap2 = "0004=130|0005=131";
		        
	        openUsaLive();
	        new LogInPage(driver).logInToUsaLiveAsPowerUser();
//	        LeftPanel leftPanel =  new LeftPanel(driver);
//	        leftPanel.clickMenuItemByText("Mass Device Update");
	        TopPanel topPanel = new TopPanel(driver);
	        topPanel.configuration.massUpdate.massDeviceUpdate.navigateTo();

	        MassDeviceUpdatePage massDeviceUpdatePage = new MassDeviceUpdatePage(driver);
	        
	        //Upload 1st file
	        String email = getProperty("emailUser");
	        String password = getProperty("emailPassword");
	        Integer emailCountBefore = EmailUtils.getEmailsCount(email, password);
	        String fileForUploadFullPath = getResoucePath("filesForUploading/massDeviceUpdate/").replace("/", "\\") + "VJ011000017_1." + extention;
	        massDeviceUpdatePage.uploadFile(fileForUploadFullPath);	 
	        String successfullyUploadedMessage = "0 row(s) failed; 0 row(s) ignored; 1 row(s) processed";
	        massDeviceUpdatePage.verifyFirstRowText(successfullyUploadedMessage);
	        
	        //Verify Email
	        EmailUtils.waitForNewEmail(emailCountBefore, 60, email, password);
	        String emailContent = EmailUtils.getMessageContent(emailCountBefore, email, password);
	        String emailSubject = EmailUtils.getMessageSubject(emailCountBefore, email, password);
	        String emailSubjectExpected1 = "Mass Device Update: Processed file \"VJ011000017_1." + extention + "\"";
	        String emailSubjectExpected2 = "Mass Device Update: Processed file \"VJ011000017_2." + extention + "\"";
	        String emailContentExpected1 = "\nUSALive - Mass Device Update 0 row(s) failed; 0 row(s) ignored; 1 row(s) processed Row #1: Device \"VJ011000017\" was updated for location \"" + locationName1 + "\"";
	        String emailContentExpected2 = "\nUSALive - Mass Device Update 0 row(s) failed; 0 row(s) ignored; 1 row(s) processed Row #1: Device \"VJ011000017\" was updated for location \"" + locationName2 + "\"";
	        Assert.assertEquals(emailSubjectExpected1 , emailSubject);
	        Assert.assertEquals(emailContentExpected1, emailContent);
	        
	        //Verify that uploaded changes have been applied to Device profile
//	        leftPanel.clickMenuItemByText("Devices");
	        topPanel.general.devices.navigateTo();
	        String deviceSerialNumber = "VJ011000017";
	        DevicesPage devicesPage = new DevicesPage(driver);
	        devicesPage.searchFor(deviceSerialNumber);
	        devicesPage.clickByDeviceIdLink(deviceSerialNumber);
	        DeviceProfilePage deviceProfilePage = new DeviceProfilePage(driver);	       	     
	        
	        Assert.assertEquals(client1, deviceProfilePage.getClient().getAttribute("value"));
	        Assert.assertEquals(overrideDoingBusinessAs1, deviceProfilePage.getOverrideDoingBusinessAs().getAttribute("value"));
	        Assert.assertEquals(overrideCustomerServicePhone1, deviceProfilePage.getOverrideCustomerServicePhone().getAttribute("value").replace("(", "").replace(")", "").replace("-", "").replace(" ", ""));
	        Assert.assertEquals(overrideCustomerServiceEmail1, deviceProfilePage.getOverrideCustomerServiceEmail().getAttribute("value"));
	        Assert.assertEquals(region1, deviceProfilePage.getRegion().getText());
	        Assert.assertEquals(locationName1, deviceProfilePage.getLocationName().getAttribute("value"));
	        Assert.assertEquals(address1, deviceProfilePage.getAddress().getAttribute("value"));
	        Assert.assertEquals(city1, deviceProfilePage.getCity().getAttribute("value"));
	        Assert.assertEquals(state1, deviceProfilePage.getState().getAttribute("value"));
	        Assert.assertEquals(zipCode1, deviceProfilePage.getZipCode().getAttribute("value").trim());
	        Assert.assertEquals(country1, deviceProfilePage.getCountry().getText());
	        Assert.assertEquals(specificLocationAtThisAddress1, deviceProfilePage.getSpecificLocationAtThisAddress().getAttribute("value"));
	        Assert.assertEquals(phoneNumberAtLocation1, deviceProfilePage.getPhoneNumberAtLocation().getAttribute("value").replace("(", "").replace(")", "").replace("-", "").replace(" ", ""));
	        Assert.assertEquals(locationType1, deviceProfilePage.getLocationType().getText());
	        Assert.assertEquals(locationTimeZone1, deviceProfilePage.getLocationTimeZone().getText());
	        Assert.assertEquals(machineAssetNumber1, deviceProfilePage.getMachineAssetNumber().getAttribute("value"));
	        Assert.assertEquals(machinesMakeAndModel1, deviceProfilePage.getMachinesMakeAndModel().getText());
	        Assert.assertEquals(productType1, deviceProfilePage.getProductType().getText());
	        Assert.assertEquals(paymentSchedule1, deviceProfilePage.getPaymentSchedule().getAttribute("value"));
	        Assert.assertEquals(businessType1, deviceProfilePage.getBusinessType().getText());
	        
	        //Verify Column Mappings	       
//	        leftPanel.clickMenuItemByText("Column Mappings");
	        topPanel.configuration.columnMaps.navigateTo();
	        ColumnMappingsPage columnMappingsPage = new ColumnMappingsPage(driver);
	        columnMappingsPage.searchCustomerByName(customerName);
	        columnMappingsPage.searchMappedDevice(deviceSerialNumber);
	        columnMappingsPage.clickMappedDevice(deviceSerialNumber);	 
	        
	        String isSelected= columnMappingsPage.getHighlightedColumnMapName(columnMapName1);
	        Assert.assertEquals("true", isSelected);
	        String actualColumnMapContent = columnMappingsPage.getColumnMapContent();
	        actualColumnMapContent = actualColumnMapContent.replace("\n", "|");
	        Assert.assertEquals(columnMap1, actualColumnMapContent);	        
	        
	        //Verify that uploaded changes have been applied. Download file
//	        leftPanel.clickMenuItemByText("Mass Device Update");
	        topPanel.configuration.massUpdate.massDeviceUpdate.navigateTo();
	        massDeviceUpdatePage.clickCurrentActiveDevicesLink();
	        massDeviceUpdatePage.clickTreeItemByText(customerName);
	        
	        String regionName1 = "AURIGA";
	        
	        massDeviceUpdatePage.clickTreeItemByText(regionName1);
	        	        
	        massDeviceUpdatePage.clickCheckboxByDeviceSerialNumber(deviceSerialNumber);
	       
	        Integer filesCountBefore = FileUtils.getFilesCountInDownloadingDir();
	        
	        massDeviceUpdatePage.clickOkButton();
	        
	        FileUtils.waitForFileToBeDownloaded(filesCountBefore);
	        
	        String fileName = "VJ011000017_1";
	        CompareUtils.verifyDownloadedXls(fileName, testGroupFolder);
	        System.out.println(extention + " is ok");
	      
	        
	        //Upload 2nd file
	        emailCountBefore = EmailUtils.getEmailsCount(email, password);
	        fileForUploadFullPath = getResoucePath("filesForUploading/massDeviceUpdate/").replace("/", "\\") + "VJ011000017_2." + extention;
	        massDeviceUpdatePage.uploadFile(fileForUploadFullPath);	        
	        massDeviceUpdatePage.verifyFirstRowText(successfullyUploadedMessage);
	        
	        //Verify Email
	        EmailUtils.waitForNewEmail(emailCountBefore, 60, email, password);
	        emailContent = EmailUtils.getMessageContent(emailCountBefore, email, password);
	        emailSubject = EmailUtils.getMessageSubject(emailCountBefore, email, password);
	        Assert.assertEquals(emailSubjectExpected2 , emailSubject);
	        Assert.assertEquals(emailContentExpected2, emailContent);
	        
	        //Verify that uploaded changes have been applied to Device profile
//	        leftPanel.clickMenuItemByText("Devices");
	        topPanel.general.devices.navigateTo();
	        devicesPage.searchFor(deviceSerialNumber);
	        devicesPage.clickByDeviceIdLink(deviceSerialNumber);
	        
	        Assert.assertEquals(client2, deviceProfilePage.getClient().getAttribute("value"));
	        Assert.assertEquals(overrideDoingBusinessAs2, deviceProfilePage.getOverrideDoingBusinessAs().getAttribute("value"));
	        Assert.assertEquals(overrideCustomerServicePhone2, deviceProfilePage.getOverrideCustomerServicePhone().getAttribute("value").replace("(", "").replace(")", "").replace("-", "").replace(" ", ""));
	        Assert.assertEquals(overrideCustomerServiceEmail2, deviceProfilePage.getOverrideCustomerServiceEmail().getAttribute("value"));
	        Assert.assertEquals(region2, deviceProfilePage.getRegion().getText());
	        Assert.assertEquals(locationName2, deviceProfilePage.getLocationName().getAttribute("value"));
	        Assert.assertEquals(address2, deviceProfilePage.getAddress().getAttribute("value"));
	        Assert.assertEquals(city2, deviceProfilePage.getCity().getAttribute("value"));
	        Assert.assertEquals(state2, deviceProfilePage.getState().getAttribute("value"));
	        Assert.assertEquals(zipCode2, deviceProfilePage.getZipCode().getAttribute("value").trim());
	        Assert.assertEquals(country2, deviceProfilePage.getCountry().getText());
	        Assert.assertEquals(specificLocationAtThisAddress2, deviceProfilePage.getSpecificLocationAtThisAddress().getAttribute("value"));
	        Assert.assertEquals(phoneNumberAtLocation2, deviceProfilePage.getPhoneNumberAtLocation().getAttribute("value").replace("(", "").replace(")", "").replace("-", "").replace(" ", ""));
	        Assert.assertEquals(locationType2, deviceProfilePage.getLocationType().getText());
	        Assert.assertEquals(locationTimeZone2, deviceProfilePage.getLocationTimeZone().getText());
	        Assert.assertEquals(machineAssetNumber2, deviceProfilePage.getMachineAssetNumber().getAttribute("value"));
	        Assert.assertEquals(machinesMakeAndModel2, deviceProfilePage.getMachinesMakeAndModel().getText());
	        Assert.assertEquals(productType2, deviceProfilePage.getProductType().getText());
	        Assert.assertEquals(paymentSchedule2, deviceProfilePage.getPaymentSchedule().getAttribute("value"));
	        Assert.assertEquals(businessType2, deviceProfilePage.getBusinessType().getText());	        
	        
	        //Verify Column Mappings	       
//	        leftPanel.clickMenuItemByText("Column Mappings");
	        topPanel.configuration.columnMaps.navigateTo();
	        columnMappingsPage.searchCustomerByName(customerName);
	        columnMappingsPage.searchMappedDevice(deviceSerialNumber);
	        columnMappingsPage.clickMappedDevice(deviceSerialNumber);	 
	        
	        isSelected = columnMappingsPage.getHighlightedColumnMapName(columnMapName2);
	        Assert.assertEquals("true", isSelected);
	        actualColumnMapContent = columnMappingsPage.getColumnMapContent();
	        actualColumnMapContent = actualColumnMapContent.replace("\n", "|");
	        Assert.assertEquals(columnMap2, actualColumnMapContent);
	        
	        
	        //Verify that uploaded changes have been applied. Download file
//	        leftPanel.clickMenuItemByText("Mass Device Update");
	        topPanel.configuration.massUpdate.massDeviceUpdate.navigateTo();

	        massDeviceUpdatePage.clickCurrentActiveDevicesLink();
	        massDeviceUpdatePage.clickTreeItemByText(customerName);
	        
	        String regionName2 = "Test Region";
	        
	        massDeviceUpdatePage.clickTreeItemByText(regionName2);
	        
	        deviceSerialNumber = "VJ011000017";
	        massDeviceUpdatePage.clickCheckboxByDeviceSerialNumber(deviceSerialNumber);
	       
	        filesCountBefore = FileUtils.getFilesCountInDownloadingDir();
	        
	        massDeviceUpdatePage.clickOkButton();
	        
	        FileUtils.waitForFileToBeDownloaded(filesCountBefore);
	        
	        fileName = "VJ011000017_2";
	        CompareUtils.verifyDownloadedXls(fileName, testGroupFolder);
	        System.out.println(extention + " is ok");
	        
	        //Upload 1st file again (revert changes)
	        fileForUploadFullPath = getResoucePath("filesForUploading/massDeviceUpdate/").replace("/", "\\") + "VJ011000017_1." + extention;
	        massDeviceUpdatePage.uploadFile(fileForUploadFullPath);	        
	        massDeviceUpdatePage.verifyFirstRowText("0 row(s) failed; 0 row(s) ignored; 1 row(s) processed");
}
	   
	   
    @Test
    public void  USAT_579_USALive_Administration_MassDeviceUpdate_DownloadTheSettingsOfYourCurrentActiveDevices() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();
//        LeftPanel leftPanel =  new LeftPanel(driver);
//        leftPanel.clickMenuItemByText("Mass Device Update");
        TopPanel topPanel = new TopPanel(driver);
        topPanel.configuration.massUpdate.massDeviceUpdate.navigateTo();
        	

        MassDeviceUpdatePage massDeviceUpdatePage = new MassDeviceUpdatePage(driver);
        
        //XLS (Default)
        massDeviceUpdatePage.clickCurrentActiveDevicesLink();
    	String customerName = "";
    	String regionName = "";
        if(getEnvironment().equalsIgnoreCase("ecc")) {
        	customerName = "Test Account Customer";
        	regionName = "<NO REGION>";
        }
        else {
        	customerName = "Auriga Load Testing";
        	regionName = "<NO REGION>";
        }
        
        
        massDeviceUpdatePage.clickTreeItemByText(customerName);
        
        
        massDeviceUpdatePage.clickTreeItemByText(regionName);
        
        String deviceSerialNumber = "VJ011000016";
        massDeviceUpdatePage.clickCheckboxByDeviceSerialNumber(deviceSerialNumber);
       
        Integer filesCountBefore = FileUtils.getFilesCountInDownloadingDir();
        
        massDeviceUpdatePage.clickOkButton();
        
        FileUtils.waitForFileToBeDownloaded(filesCountBefore);
        
        String reportName = "Devices";
        CompareUtils.verifyDownloadedXls(reportName, testGroupFolder); //Binary. Not XML
        System.out.println("XLS is ok");
        
        //CSV
        massDeviceUpdatePage.clickCurrentActiveDevicesLink();

        massDeviceUpdatePage.clickTreeItemByText(customerName);
        
        massDeviceUpdatePage.clickTreeItemByText(regionName);

        massDeviceUpdatePage.clickCheckboxByDeviceSerialNumber(deviceSerialNumber);
       
        filesCountBefore = FileUtils.getFilesCountInDownloadingDir();
        
        massDeviceUpdatePage.clickCsvRadioButton();
        
        massDeviceUpdatePage.clickOkButton();
        
        FileUtils.waitForFileToBeDownloaded(filesCountBefore);
        
        CompareUtils.verifyDownloadedCsv(reportName, testGroupFolder);
        System.out.println("CSV is ok");
    }

    @Test
    public void  USAT_1159_USALive_Administration_MassDeviceUpdate_DownloadSampleExcelFile() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();
//        LeftPanel leftPanel =  new LeftPanel(driver);
//        leftPanel.clickMenuItemByText("Mass Device Update");
        TopPanel topPanel = new TopPanel(driver);
        topPanel.configuration.massUpdate.massDeviceUpdate.navigateTo();

        MassDeviceUpdatePage massDeviceUpdatePage = new MassDeviceUpdatePage(driver);
        Integer filesCountBefore = FileUtils.getFilesCountInDownloadingDir();

        massDeviceUpdatePage.clickSampleExcelLink();
        
        FileUtils.waitForFileToBeDownloaded(filesCountBefore);        
         
        String fileName = "sample-full-update-file";
        CompareUtils.verifyDownloadedXls(fileName, testGroupFolder); //Binary. Not XML
        System.out.println("XLS is ok");     
    }
    
    @Test
    public void  USAT_1160_USALive_Administration_MassDeviceUpdate_DownloadSampleCSVFile() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();
//        LeftPanel leftPanel =  new LeftPanel(driver);
//        leftPanel.clickMenuItemByText("Mass Device Update");
        TopPanel topPanel = new TopPanel(driver);
        topPanel.configuration.massUpdate.massDeviceUpdate.navigateTo();

        MassDeviceUpdatePage massDeviceUpdatePage = new MassDeviceUpdatePage(driver);
        Integer filesCountBefore = FileUtils.getFilesCountInDownloadingDir();

        massDeviceUpdatePage.clickSampleCsvLink();
        
        FileUtils.waitForFileToBeDownloaded(filesCountBefore);        
         
        String fileName = "sample-full-update-file";
        CompareUtils.verifyDownloadedCsv(fileName, testGroupFolder); 
        System.out.println("CSV is ok");     
    }

}

