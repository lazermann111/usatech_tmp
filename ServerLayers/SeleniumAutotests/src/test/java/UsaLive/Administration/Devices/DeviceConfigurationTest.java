package UsaLive.Administration.Devices;

import helper.Core;
import org.junit.Assert;
import org.junit.Test;
import pages.UsaLive.Administration.Devices.DeviceConfigurationPage;
import pages.UsaLive.Administration.Devices.DeviceProfilePage;
import pages.UsaLive.Administration.Devices.DevicesPage;
import pages.Dms.Devices.DeviceSettingsTab;
import pages.Dms.LeftPanel.DevicesMenu;
import pages.UsaLive.LeftPanel;
import pages.UsaLive.LogInPage;
//import pages.UsaLive.LogInPage;
import pages.UsaLive.TopPanel;

/**
 * Created by ahachikyan on 3/2/2016.
 */
public class DeviceConfigurationTest extends Core {

    // @Test
    // public void
    // USAT_358_USALive_Administration_Devices_DeviceConfigurationSaveNotApplicableValue()
    // throws Exception {
    // openUsaLive();
    // new LogInPage(driver).logInAsPowerUser();
    // LeftPanel leftPanel = new LeftPanel(driver);
    // leftPanel.clickMenuItemByText("Devices");
    //
    // DevicesPage devicesPage = new DevicesPage(driver);
    //
    // String deviceId = "VJ000000021";
    // devicesPage.searchFor(deviceId);
    // devicesPage.clickByDeviceIdLink(deviceId);
    //
    //
    // DeviceProfilePage deviceProfilePage = new DeviceProfilePage(driver);
    //
    // deviceProfilePage.getDeviceConfiguration().click();
    //
    // String twoTierPricingWrongValue1 = "abc";
    // String twoTierPricingWrongValue2 = "0.3";
    // String twoTierPricingValidValue1 = "0.01";
    //
    //
    // String authorizationAmountWrongValue = "abc";
    // String authorizationAmountValidValue = "33.33";
    //
    //// TODO: Defect. Could be set even text without error
    //// String minimumAllowedVendAmountWrongValue1 = "abc";
    //
    //
    //
    //// String attractMessageLine1Value1 = "Swipe Or Tap1";
    //// String attractMessageLine1Value2 = "Swipe Or Tap2";
    //// String attractMessageLine2Value1 = "To Begin1";
    //// String attractMessageLine2Value2 = "To Begin2";
    //// String welcomeMessageLine1Value1 = "WelcomeToePort1";
    //// String welcomeMessageLine1Value2 = "WelcomeToePort2";
    //// String welcomeMessageLine2Value1 = "Same As Cash1";
    //// String welcomeMessageLine2Value2 = "Same As Cash2";
    //// String minimumAllowedVendAmountValue1 = "";
    //// String disableMessageLine1Value1 = "disable1";
    //// String disableMessageLine1Value2 = "disable1 2";
    //// String disableMessageLine2Value1 = "disable2";
    //// String disableMessageLine2Value2 = "disable2 2";
    //
    //// Valid values: 0000 - 2359
    // String serverOverrideCallInTimeWindowStartActivatedWrongValue1 = "-1";
    // String serverOverrideCallInTimeWindowStartActivatedWrongValue2 = "2400";
    // String serverOverrideCallInTimeWindowStartActivatedValidValue = "2222";
    //// Valid values: 1 - 23
    // String serverOverrideCallInTimeWindowHoursActivatedWrongValue1 = "0";
    // String serverOverrideCallInTimeWindowHoursActivatedWrongValue2 = "24";
    // String serverOverrideCallInTimeWindowHoursActivatedValidValue = "2";
    //
    //// 0000 - 2359
    // String serverOverrideCallInTimeWindowStartDEXWrongValue1 = "-1";
    // String serverOverrideCallInTimeWindowStartDEXWrongValue2 = "2400";
    // String serverOverrideCallInTimeWindowStartDEXValidValue = "2222";
    //
    //// 1 - 23
    // String serverOverrideCallInTimeWindowHoursDEXWrongValue1 = "0";
    // String serverOverrideCallInTimeWindowHoursDEXValueWrong2 = "24";
    //
    //
    // String serverOverrideMaximumAuthorizationAmountValue1 = "1";
    // String serverOverrideMaximumAuthorizationAmountValue2 = "2";
    //// TODO: Defect. Unlimited. Could be set even to 99999
    //// String mDBSettingMaxNumberOfItemsValue1 = "11";
    //// String mDBSettingMaxNumberOfItemsValue2 = "22";
    //// String mDBSettingTimeoutfor1stSelectionValue1 = "11";
    //// String mDBSettingTimeoutfor1stSelectionValue2 = "22";
    //// String mDBSettingTimeoutfor2nd3rdEtcSelectionsValue1 = "11";
    //// String mDBSettingTimeoutfor2nd3rdEtcSelectionsValue2 = "22";
    //// String mDBSettingVendSessionTimeoutValue1 = "111";
    //// String mDBSettingVendSessionTimeoutValue2 = "222";
    //// String cashTransactionRecordingValue1 = "0";
    //// String cashTransactionRecordingValue2 = "1";
    //// String dEXSettingScheduleValue1 = "I";
    //// String dEXSettingScheduleValue2 = "W";
    //// String auxSerialPortSelectValue1 = "0";
    //// String auxSerialPortSelectValue2 = "1";
    //// String vMCInterfaceTypeValue1 = "3";
    //// String vMCInterfaceTypeValue2 = "4";
    //// Valid values: 1-255. BUG
    // String coinPulseDurationValue1 = "11";
    //// Valid Values: 1 - 1000. BUG
    // String coinPulseSpacingValue1 = "111";
    //// Valid Values: 0.01 - 655.35. BUG
    // String coinPulseValueValue1 = "1.11";
    //// Valid Values: 1-255. BUG
    //// String coinAutoTimeoutValue1 = "11";
    //
    //// String coinReaderEnableActiveStateValue1 = "1";
    //// String coinReaderEnableActiveStateValue2 = "2";
    //// String coinItemPriceN1Value1 = "1.10";
    //// String coinItemPriceN1Value2 = "2.20";
    //// String coinItemPriceN1LabelValue1 = "Coin price 1.1";
    //// String coinItemPriceN1LabelValue2 = "Coin price 1.2";
    //// String coinItemPriceN2Value1 = "2.10";
    //// String coinItemPriceN2Value2 = "2.20";
    //// String coinItemPriceN2LabelValue1 = "Coin price 2.1";
    //// String coinItemPriceN2LabelValue2 = "Coin price 2.2";
    //// String coinItemPriceN3Value1 = "3.10";
    //// String coinItemPriceN3Value2 = "3.20";
    //// String coinItemPriceN3LabelValue1 = "Coin price 3.1";
    //// String coinItemPriceN3LabelValue2 = "Coin price 3.2";
    //// String coinItemPriceN4Value1 = "4.10";
    //// String coinItemPriceN4Value2 = "4.20";
    //// String coinItemPriceN4LabelValue1 = "Coin price 4.1";
    //// String coinItemPriceN4LabelValue2 = "Coin price 4.2";
    //// String coinItemPriceN5Value1 = "5.10";
    //// String coinItemPriceN5Value2 = "5.20";
    //// String coinItemPriceN5LabelValue1 = "Coin price 5.1";
    //// String coinItemPriceN5LabelValue2 = "Coin price 5.2";
    //// String coinItemPriceN6Value1 = "6.10";
    //// String coinItemPriceN6Value2 = "6.20";
    //// String coinItemPriceN6LabelValue1 = "Coin price 6.1";
    //// String coinItemPriceN6LabelValue2 = "Coin price 6.2";
    //// String coinItemPriceN7Value1 = "7.10";
    //// String coinItemPriceN7Value2 = "7.20";
    //// String coinItemPriceN7LabelValue1 = "Coin price 7.1";
    //// String coinItemPriceN7LabelValue2 = "Coin price 7.2";
    //// String coinItemPriceN8Value1 = "8.10";
    //// String coinItemPriceN8Value2 = "8.20";
    //// String coinItemPriceN8LabelValue1 = "Coin price 8.1";
    //// String coinItemPriceN8LabelValue2 = "Coin price 8.2";
    //// String pulseTimeValueValue1 = "11";
    //// String pulseTimeValueValue2 = "22";
    //
    //// Valid Values: 0.01 - 655.35.
    // String topOffValueWrondValue1 = "0.00";
    // String topOffValueWrondValue2 = "656.00";
    // String topOffValueRightValue = "5.55";
    //
    //
    //// String pulseCaptureValueValue1 = "1.10";
    //// String pulseCaptureValueValue2 = "2.10";
    //
    //
    //// Set 1-st set of values
    // DeviceConfigurationPage deviceConfigurationPage = new
    // DeviceConfigurationPage(driver);
    // deviceConfigurationPage.setAuthorizationAmountWebElement(authorizationAmountValue1);
    // deviceConfigurationPage.setTwoTierPricingWebElement(twoTierPricingValue1);
    // deviceConfigurationPage.setAttractMessageLine1WebElement(attractMessageLine1Value1);
    // deviceConfigurationPage.setAttractMessageLine2WebElement(attractMessageLine2Value1);
    // deviceConfigurationPage.setWelcomeMessageLine1WebElement(welcomeMessageLine1Value1);
    // deviceConfigurationPage.setWelcomeMessageLine2WebElement(welcomeMessageLine2Value1);
    //// deviceConfigurationPage.setMinimumAllowedVendAmountWebElement(minimumAllowedVendAmountValue1);
    // deviceConfigurationPage.setDisableMessageLine1WebElement(disableMessageLine1Value1);
    // deviceConfigurationPage.setDisableMessageLine2WebElement(disableMessageLine2Value1);
    // deviceConfigurationPage.setServerOverrideCallInTimeWindowStartActivatedWebElement(serverOverrideCallInTimeWindowStartActivatedValue1);
    // deviceConfigurationPage.setServerOverrideCallInTimeWindowHoursActivatedWebElement(serverOverrideCallInTimeWindowHoursActivatedValue1);
    // deviceConfigurationPage.setServerOverrideCallInTimeWindowStartDEXWebElement(serverOverrideCallInTimeWindowStartDEXValue1);
    // deviceConfigurationPage.setServerOverrideCallInTimeWindowHoursDEXWebElement(serverOverrideCallInTimeWindowHoursDEXValue1);
    //// deviceConfigurationPage.setServerOverrideMaximumAuthorizationAmountWebElement(serverOverrideMaximumAuthorizationAmountValue1);
    // deviceConfigurationPage.setMDBSettingMaxNumberOfItemsWebElement(mDBSettingMaxNumberOfItemsValue1);
    // deviceConfigurationPage.setMDBSettingTimeoutfor1stSelectionWebElement(mDBSettingTimeoutfor1stSelectionValue1);
    // deviceConfigurationPage.setMDBSettingTimeoutfor2nd3rdEtcSelectionsWebElement(mDBSettingTimeoutfor2nd3rdEtcSelectionsValue1);
    // deviceConfigurationPage.setMDBSettingVendSessionTimeoutWebElement(mDBSettingVendSessionTimeoutValue1);
    // deviceConfigurationPage.setCashTransactionRecordingWebElement(cashTransactionRecordingValue1);
    // deviceConfigurationPage.setDEXSettingScheduleWebElement(dEXSettingScheduleValue1);
    // deviceConfigurationPage.setAuxSerialPortSelectWebElement(auxSerialPortSelectValue1);
    // deviceConfigurationPage.setVMCInterfaceTypeWebElement(vMCInterfaceTypeValue1);
    // deviceConfigurationPage.setCoinPulseDurationWebElement(coinPulseDurationValue1);
    // deviceConfigurationPage.setCoinPulseSpacingWebElement(coinPulseSpacingValue1);
    // deviceConfigurationPage.setCoinPulseValueWebElement(coinPulseValueValue1);
    // deviceConfigurationPage.setCoinAutoTimeoutWebElement(coinAutoTimeoutValue1);
    // deviceConfigurationPage.setCoinReaderEnableActiveStateWebElement(coinReaderEnableActiveStateValue1);
    // deviceConfigurationPage.setCoinItemPriceN1WebElement(coinItemPriceN1Value1);
    // deviceConfigurationPage.setCoinItemPriceN1LabelWebElement(coinItemPriceN1LabelValue1);
    // deviceConfigurationPage.setCoinItemPriceN2WebElement(coinItemPriceN2Value1);
    // deviceConfigurationPage.setCoinItemPriceN2LabelWebElement(coinItemPriceN2LabelValue1);
    // deviceConfigurationPage.setCoinItemPriceN3WebElement(coinItemPriceN3Value1);
    // deviceConfigurationPage.setCoinItemPriceN3LabelWebElement(coinItemPriceN3LabelValue1);
    // deviceConfigurationPage.setCoinItemPriceN4WebElement(coinItemPriceN4Value1);
    // deviceConfigurationPage.setCoinItemPriceN4LabelWebElement(coinItemPriceN4LabelValue1);
    // deviceConfigurationPage.setCoinItemPriceN5WebElement(coinItemPriceN5Value1);
    // deviceConfigurationPage.setCoinItemPriceN5LabelWebElement(coinItemPriceN5LabelValue1);
    // deviceConfigurationPage.setCoinItemPriceN6WebElement(coinItemPriceN6Value1);
    // deviceConfigurationPage.setCoinItemPriceN6LabelWebElement(coinItemPriceN6LabelValue1);
    // deviceConfigurationPage.setCoinItemPriceN7WebElement(coinItemPriceN7Value1);
    // deviceConfigurationPage.setCoinItemPriceN7LabelWebElement(coinItemPriceN7LabelValue1);
    // deviceConfigurationPage.setCoinItemPriceN8WebElement(coinItemPriceN8Value1);
    // deviceConfigurationPage.setCoinItemPriceN8LabelWebElement(coinItemPriceN8LabelValue1);
    // deviceConfigurationPage.setPulseTimeValueWebElement(pulseTimeValueValue1);
    // deviceConfigurationPage.setTopOffValueWebElement(topOffValueValue1);
    // deviceConfigurationPage.setPulseCaptureValueWebElement(pulseCaptureValueValue1);
    //// 2. Save changes
    // deviceConfigurationPage.save();
    // deviceConfigurationPage.verifySuccessUpdateMessage();
    //
    //
    // openDms();
    // new pages.Dms.LogInPage(driver).logInAsCommonUser();
    // new DevicesMenu(driver).searchDevice(deviceId);
    //
    // DeviceSettingsTab deviceSettingsTab = new DeviceSettingsTab(driver);
    // deviceSettingsTab.clickDeviceSettingsTab();
    // deviceSettingsTab.clickListAllButton();
    //// 3. Verify 1-st set of values in DMS
    //
    // Assert.assertEquals(authorizationAmountValue1,
    // deviceSettingsTab.getAuthorizationAmountValue());
    // Assert.assertEquals(twoTierPricingValue1,
    // deviceSettingsTab.getTwoTierPricingValue());
    // Assert.assertEquals(attractMessageLine1Value1,
    // deviceSettingsTab.getAttractMessageLine1Value());
    // Assert.assertEquals(attractMessageLine2Value1,
    // deviceSettingsTab.getAttractMessageLine2Value());
    // Assert.assertEquals(welcomeMessageLine1Value1,
    // deviceSettingsTab.getWelcomeMessageLine1Value());
    // Assert.assertEquals(welcomeMessageLine2Value1,
    // deviceSettingsTab.getWelcomeMessageLine2Value());
    //// Assert.assertEquals(minimumAllowedVendAmountValue1,
    // deviceSettingsTab.getMinimumAllowedVendAmountValue());
    // Assert.assertEquals(disableMessageLine1Value1,
    // deviceSettingsTab.getDisableMessageLine1Value());
    // Assert.assertEquals(disableMessageLine2Value1,
    // deviceSettingsTab.getDisableMessageLine2Value());
    // Assert.assertEquals(serverOverrideCallInTimeWindowStartActivatedValue1,
    // deviceSettingsTab.getServerOverrideCallInTimeWindowStartActivatedValue());
    // Assert.assertEquals(serverOverrideCallInTimeWindowHoursActivatedValue1,
    // deviceSettingsTab.getServerOverrideCallInTimeWindowHoursActivatedValue());
    // Assert.assertEquals(serverOverrideCallInTimeWindowStartDEXValue1,
    // deviceSettingsTab.getServerOverrideCallInTimeWindowStartDEXValue());
    // Assert.assertEquals(serverOverrideCallInTimeWindowHoursDEXValue1,
    // deviceSettingsTab.getServerOverrideCallInTimeWindowHoursDEXValue());
    //// Assert.assertEquals(serverOverrideMaximumAuthorizationAmountValue1,
    // deviceSettingsTab.getServerOverrideMaximumAuthorizationAmountValue());
    // Assert.assertEquals(mDBSettingMaxNumberOfItemsValue1,
    // deviceSettingsTab.getMDBSettingMaxNumberOfItemsValue());
    // Assert.assertEquals(mDBSettingTimeoutfor1stSelectionValue1,
    // deviceSettingsTab.getMDBSettingTimeoutfor1stSelectionValue());
    // Assert.assertEquals(mDBSettingTimeoutfor2nd3rdEtcSelectionsValue1,
    // deviceSettingsTab.getMDBSettingTimeoutfor2nd3rdEtcSelectionsValue());
    // Assert.assertEquals(mDBSettingVendSessionTimeoutValue1,
    // deviceSettingsTab.getMDBSettingVendSessionTimeoutValue());
    // Assert.assertEquals(cashTransactionRecordingValue1,
    // deviceSettingsTab.getCashTransactionRecordingValue());
    //
    // if(getEnvironment().equalsIgnoreCase("ecc"))
    // Assert.assertEquals(dEXSettingScheduleValue1 + "^28800^43200",
    // deviceSettingsTab.getDEXSettingScheduleValue());
    // else
    // Assert.assertEquals(dEXSettingScheduleValue1 + "^28800^43200",
    // deviceSettingsTab.getDEXSettingScheduleValue());
    // Assert.assertEquals(auxSerialPortSelectValue1,
    // deviceSettingsTab.getAuxSerialPortSelectValue());
    // Assert.assertEquals(vMCInterfaceTypeValue1,
    // deviceSettingsTab.getVMCInterfaceTypeValue());
    // Assert.assertEquals(coinPulseDurationValue1,
    // deviceSettingsTab.getCoinPulseDurationValue());
    // Assert.assertEquals(coinPulseSpacingValue1,
    // deviceSettingsTab.getCoinPulseSpacingValue());
    // Assert.assertEquals(coinPulseValueValue1,
    // deviceSettingsTab.getCoinPulseValueValue());
    // Assert.assertEquals(coinAutoTimeoutValue1,
    // deviceSettingsTab.getCoinAutoTimeoutValue());
    // Assert.assertEquals(coinReaderEnableActiveStateValue1,
    // deviceSettingsTab.getCoinReaderEnableActiveStateValue());
    // Assert.assertEquals(coinItemPriceN1Value1,
    // deviceSettingsTab.getCoinItemPriceN1Value());
    // Assert.assertEquals(coinItemPriceN1LabelValue1,
    // deviceSettingsTab.getCoinItemPriceN1LabelValue());
    // Assert.assertEquals(coinItemPriceN2Value1,
    // deviceSettingsTab.getCoinItemPriceN2Value());
    // Assert.assertEquals(coinItemPriceN2LabelValue1,
    // deviceSettingsTab.getCoinItemPriceN2LabelValue());
    // Assert.assertEquals(coinItemPriceN3Value1,
    // deviceSettingsTab.getCoinItemPriceN3Value());
    // Assert.assertEquals(coinItemPriceN3LabelValue1,
    // deviceSettingsTab.getCoinItemPriceN3LabelValue());
    // Assert.assertEquals(coinItemPriceN4Value1,
    // deviceSettingsTab.getCoinItemPriceN4Value());
    // Assert.assertEquals(coinItemPriceN4LabelValue1,
    // deviceSettingsTab.getCoinItemPriceN4LabelValue());
    // Assert.assertEquals(coinItemPriceN5Value1,
    // deviceSettingsTab.getCoinItemPriceN5Value());
    // Assert.assertEquals(coinItemPriceN5LabelValue1,
    // deviceSettingsTab.getCoinItemPriceN5LabelValue());
    // Assert.assertEquals(coinItemPriceN6Value1,
    // deviceSettingsTab.getCoinItemPriceN6Value());
    // Assert.assertEquals(coinItemPriceN6LabelValue1,
    // deviceSettingsTab.getCoinItemPriceN6LabelValue());
    // Assert.assertEquals(coinItemPriceN7Value1,
    // deviceSettingsTab.getCoinItemPriceN7Value());
    // Assert.assertEquals(coinItemPriceN7LabelValue1,
    // deviceSettingsTab.getCoinItemPriceN7LabelValue());
    // Assert.assertEquals(coinItemPriceN8Value1,
    // deviceSettingsTab.getCoinItemPriceN8Value());
    // Assert.assertEquals(coinItemPriceN8LabelValue1,
    // deviceSettingsTab.getCoinItemPriceN8LabelValue());
    // Assert.assertEquals(pulseTimeValueValue1,
    // deviceSettingsTab.getPulseTimeValueValue());
    // Assert.assertEquals(topOffValueValue1,
    // deviceSettingsTab.getTopOffValueValue());
    // Assert.assertEquals(pulseCaptureValueValue1,
    // deviceSettingsTab.getPulseCaptureValueValue());
    // logger.info("");
    //// 4. Open device in USALive
    // openUsaLive();
    //// new LogInPage(driver).logInAsPowerUser();
    //
    // leftPanel.clickMenuItemByText("Devices");
    //
    //// devicesPage.expandTreeRootNode();
    //// devicesPage.expandTreeNode(node1);
    //// devicesPage.expandTreeNode(node2);
    //// devicesPage.expandTreeNode(region);
    //// devicesPage.getDevice(deviceId).click();
    // devicesPage.searchFor(deviceId);
    // devicesPage.clickByDeviceIdLink(deviceId);
    //
    // deviceProfilePage.getDeviceConfiguration().click();
    //// 5. Verify 1-st set of values in USALive
    // Assert.assertEquals(authorizationAmountValue1,
    // deviceConfigurationPage.getAuthorizationAmountWebElement().getAttribute("value"));
    // Assert.assertEquals(twoTierPricingValue1,
    // deviceConfigurationPage.getTwoTierPricingWebElement().getAttribute("value"));
    // Assert.assertEquals(attractMessageLine1Value1,
    // deviceConfigurationPage.getAttractMessageLine1WebElement().getAttribute("value"));
    // Assert.assertEquals(attractMessageLine2Value1,
    // deviceConfigurationPage.getAttractMessageLine2WebElement().getAttribute("value"));
    // Assert.assertEquals(welcomeMessageLine1Value1,
    // deviceConfigurationPage.getWelcomeMessageLine1WebElement().getAttribute("value"));
    // Assert.assertEquals(welcomeMessageLine2Value1,
    // deviceConfigurationPage.getWelcomeMessageLine2WebElement().getAttribute("value"));
    // Assert.assertEquals(disableMessageLine1Value1,
    // deviceConfigurationPage.getDisableMessageLine1WebElement().getAttribute("value"));
    // Assert.assertEquals(disableMessageLine2Value1,
    // deviceConfigurationPage.getDisableMessageLine2WebElement().getAttribute("value"));
    // Assert.assertEquals(serverOverrideCallInTimeWindowStartActivatedValue1,
    // deviceConfigurationPage.getServerOverrideCallInTimeWindowStartActivatedWebElement().getAttribute("value"));
    // Assert.assertEquals(serverOverrideCallInTimeWindowHoursActivatedValue1,
    // deviceConfigurationPage.getServerOverrideCallInTimeWindowHoursActivatedWebElement().getAttribute("value"));
    // Assert.assertEquals(serverOverrideCallInTimeWindowStartDEXValue1,
    // deviceConfigurationPage.getServerOverrideCallInTimeWindowStartDEXWebElement().getAttribute("value"));
    // Assert.assertEquals(serverOverrideCallInTimeWindowHoursDEXValue1,
    // deviceConfigurationPage.getServerOverrideCallInTimeWindowHoursDEXWebElement().getAttribute("value"));
    // Assert.assertEquals(mDBSettingMaxNumberOfItemsValue1,
    // deviceConfigurationPage.getMDBSettingMaxNumberOfItemsWebElement().getAttribute("value"));
    // Assert.assertEquals(mDBSettingTimeoutfor1stSelectionValue1,
    // deviceConfigurationPage.getMDBSettingTimeoutfor1stSelectionWebElement().getAttribute("value"));
    // Assert.assertEquals(mDBSettingTimeoutfor2nd3rdEtcSelectionsValue1,
    // deviceConfigurationPage.getMDBSettingTimeoutfor2nd3rdEtcSelectionsWebElement().getAttribute("value"));
    // Assert.assertEquals(mDBSettingVendSessionTimeoutValue1,
    // deviceConfigurationPage.getMDBSettingVendSessionTimeoutWebElement().getAttribute("value"));
    // Assert.assertEquals(cashTransactionRecordingValue1,
    // deviceConfigurationPage.getCashTransactionRecordingWebElement().getAttribute("value"));
    // Assert.assertEquals(dEXSettingScheduleValue1,
    // deviceConfigurationPage.getDEXSettingScheduleWebElement().getAttribute("value"));
    // Assert.assertEquals(auxSerialPortSelectValue1,
    // deviceConfigurationPage.getAuxSerialPortSelectWebElement().getAttribute("value"));
    // Assert.assertEquals(vMCInterfaceTypeValue1,
    // deviceConfigurationPage.getVMCInterfaceTypeWebElement().getAttribute("value"));
    // Assert.assertEquals(coinPulseDurationValue1,
    // deviceConfigurationPage.getCoinPulseDurationWebElement().getAttribute("value"));
    // Assert.assertEquals(coinPulseSpacingValue1,
    // deviceConfigurationPage.getCoinPulseSpacingWebElement().getAttribute("value"));
    // Assert.assertEquals(coinPulseValueValue1,
    // deviceConfigurationPage.getCoinPulseValueWebElement().getAttribute("value"));
    // Assert.assertEquals(coinAutoTimeoutValue1,
    // deviceConfigurationPage.getCoinAutoTimeoutWebElement().getAttribute("value"));
    // Assert.assertEquals(coinReaderEnableActiveStateValue1,
    // deviceConfigurationPage.getCoinReaderEnableActiveStateWebElement().getAttribute("value"));
    // Assert.assertEquals(coinItemPriceN1Value1,
    // deviceConfigurationPage.getCoinItemPriceN1WebElement().getAttribute("value"));
    // Assert.assertEquals(coinItemPriceN1LabelValue1,
    // deviceConfigurationPage.getCoinItemPriceN1LabelWebElement().getAttribute("value"));
    // Assert.assertEquals(coinItemPriceN2Value1,
    // deviceConfigurationPage.getCoinItemPriceN2WebElement().getAttribute("value"));
    // Assert.assertEquals(coinItemPriceN2LabelValue1,
    // deviceConfigurationPage.getCoinItemPriceN2LabelWebElement().getAttribute("value"));
    // Assert.assertEquals(coinItemPriceN3Value1,
    // deviceConfigurationPage.getCoinItemPriceN3WebElement().getAttribute("value"));
    // Assert.assertEquals(coinItemPriceN3LabelValue1,
    // deviceConfigurationPage.getCoinItemPriceN3LabelWebElement().getAttribute("value"));
    // Assert.assertEquals(coinItemPriceN4Value1,
    // deviceConfigurationPage.getCoinItemPriceN4WebElement().getAttribute("value"));
    // Assert.assertEquals(coinItemPriceN4LabelValue1,
    // deviceConfigurationPage.getCoinItemPriceN4LabelWebElement().getAttribute("value"));
    // Assert.assertEquals(coinItemPriceN5Value1,
    // deviceConfigurationPage.getCoinItemPriceN5WebElement().getAttribute("value"));
    // Assert.assertEquals(coinItemPriceN5LabelValue1,
    // deviceConfigurationPage.getCoinItemPriceN5LabelWebElement().getAttribute("value"));
    // Assert.assertEquals(coinItemPriceN6Value1,
    // deviceConfigurationPage.getCoinItemPriceN6WebElement().getAttribute("value"));
    // Assert.assertEquals(coinItemPriceN6LabelValue1,
    // deviceConfigurationPage.getCoinItemPriceN6LabelWebElement().getAttribute("value"));
    // Assert.assertEquals(coinItemPriceN7Value1,
    // deviceConfigurationPage.getCoinItemPriceN7WebElement().getAttribute("value"));
    // Assert.assertEquals(coinItemPriceN7LabelValue1,
    // deviceConfigurationPage.getCoinItemPriceN7LabelWebElement().getAttribute("value"));
    // Assert.assertEquals(coinItemPriceN8Value1,
    // deviceConfigurationPage.getCoinItemPriceN8WebElement().getAttribute("value"));
    // Assert.assertEquals(coinItemPriceN8LabelValue1,
    // deviceConfigurationPage.getCoinItemPriceN8LabelWebElement().getAttribute("value"));
    // Assert.assertEquals(pulseTimeValueValue1,
    // deviceConfigurationPage.getPulseTimeValueWebElement().getAttribute("value"));
    // Assert.assertEquals(topOffValueValue1,
    // deviceConfigurationPage.getTopOffValueWebElement().getAttribute("value"));
    // Assert.assertEquals(pulseCaptureValueValue1,
    // deviceConfigurationPage.getPulseCaptureValueWebElement().getAttribute("value"));
    //// 6. Set 2-nd set of values
    // deviceConfigurationPage.setAuthorizationAmountWebElement(authorizationAmountValue2);
    // deviceConfigurationPage.setTwoTierPricingWebElement(twoTierPricingValue2);
    // deviceConfigurationPage.setAttractMessageLine1WebElement(attractMessageLine1Value2);
    // deviceConfigurationPage.setAttractMessageLine2WebElement(attractMessageLine2Value2);
    // deviceConfigurationPage.setWelcomeMessageLine1WebElement(welcomeMessageLine1Value2);
    // deviceConfigurationPage.setWelcomeMessageLine2WebElement(welcomeMessageLine2Value2);
    //// deviceConfigurationPage.setMinimumAllowedVendAmountWebElement(minimumAllowedVendAmountValue2);
    // deviceConfigurationPage.setDisableMessageLine1WebElement(disableMessageLine1Value2);
    // deviceConfigurationPage.setDisableMessageLine2WebElement(disableMessageLine2Value2);
    // deviceConfigurationPage.setServerOverrideCallInTimeWindowStartActivatedWebElement(serverOverrideCallInTimeWindowStartActivatedValue2);
    // deviceConfigurationPage.setServerOverrideCallInTimeWindowHoursActivatedWebElement(serverOverrideCallInTimeWindowHoursActivatedValue2);
    // deviceConfigurationPage.setServerOverrideCallInTimeWindowStartDEXWebElement(serverOverrideCallInTimeWindowStartDEXValue2);
    // deviceConfigurationPage.setServerOverrideCallInTimeWindowHoursDEXWebElement(serverOverrideCallInTimeWindowHoursDEXValue2);
    //// deviceConfigurationPage.setServerOverrideMaximumAuthorizationAmountWebElement(serverOverrideMaximumAuthorizationAmountValue2);
    // deviceConfigurationPage.setMDBSettingMaxNumberOfItemsWebElement(mDBSettingMaxNumberOfItemsValue2);
    // deviceConfigurationPage.setMDBSettingTimeoutfor1stSelectionWebElement(mDBSettingTimeoutfor1stSelectionValue2);
    // deviceConfigurationPage.setMDBSettingTimeoutfor2nd3rdEtcSelectionsWebElement(mDBSettingTimeoutfor2nd3rdEtcSelectionsValue2);
    // deviceConfigurationPage.setMDBSettingVendSessionTimeoutWebElement(mDBSettingVendSessionTimeoutValue2);
    // deviceConfigurationPage.setCashTransactionRecordingWebElement(cashTransactionRecordingValue2);
    // deviceConfigurationPage.setDEXSettingScheduleWebElement(dEXSettingScheduleValue2);
    // deviceConfigurationPage.setAuxSerialPortSelectWebElement(auxSerialPortSelectValue2);
    // deviceConfigurationPage.setVMCInterfaceTypeWebElement(vMCInterfaceTypeValue2);
    // deviceConfigurationPage.setCoinPulseDurationWebElement(coinPulseDurationValue2);
    // deviceConfigurationPage.setCoinPulseSpacingWebElement(coinPulseSpacingValue2);
    // deviceConfigurationPage.setCoinPulseValueWebElement(coinPulseValueValue2);
    // deviceConfigurationPage.setCoinAutoTimeoutWebElement(coinAutoTimeoutValue2);
    // deviceConfigurationPage.setCoinReaderEnableActiveStateWebElement(coinReaderEnableActiveStateValue2);
    // deviceConfigurationPage.setCoinItemPriceN1WebElement(coinItemPriceN1Value2);
    // deviceConfigurationPage.setCoinItemPriceN1LabelWebElement(coinItemPriceN1LabelValue2);
    // deviceConfigurationPage.setCoinItemPriceN2WebElement(coinItemPriceN2Value2);
    // deviceConfigurationPage.setCoinItemPriceN2LabelWebElement(coinItemPriceN2LabelValue2);
    // deviceConfigurationPage.setCoinItemPriceN3WebElement(coinItemPriceN3Value2);
    // deviceConfigurationPage.setCoinItemPriceN3LabelWebElement(coinItemPriceN3LabelValue2);
    // deviceConfigurationPage.setCoinItemPriceN4WebElement(coinItemPriceN4Value2);
    // deviceConfigurationPage.setCoinItemPriceN4LabelWebElement(coinItemPriceN4LabelValue2);
    // deviceConfigurationPage.setCoinItemPriceN5WebElement(coinItemPriceN5Value2);
    // deviceConfigurationPage.setCoinItemPriceN5LabelWebElement(coinItemPriceN5LabelValue2);
    // deviceConfigurationPage.setCoinItemPriceN6WebElement(coinItemPriceN6Value2);
    // deviceConfigurationPage.setCoinItemPriceN6LabelWebElement(coinItemPriceN6LabelValue2);
    // deviceConfigurationPage.setCoinItemPriceN7WebElement(coinItemPriceN7Value2);
    // deviceConfigurationPage.setCoinItemPriceN7LabelWebElement(coinItemPriceN7LabelValue2);
    // deviceConfigurationPage.setCoinItemPriceN8WebElement(coinItemPriceN8Value2);
    // deviceConfigurationPage.setCoinItemPriceN8LabelWebElement(coinItemPriceN8LabelValue2);
    // deviceConfigurationPage.setPulseTimeValueWebElement(pulseTimeValueValue2);
    // deviceConfigurationPage.setTopOffValueWebElement(topOffValueValue2);
    // deviceConfigurationPage.setPulseCaptureValueWebElement(pulseCaptureValueValue2);
    //// 7. Save changes
    // deviceConfigurationPage.save();
    // deviceConfigurationPage.verifySuccessUpdateMessage();
    //// 8. Verify 2-nd set of values in DMS
    // openDms();
    // new DevicesMenu(driver).searchDevice(deviceId);
    //
    // deviceSettingsTab.clickDeviceSettingsTab();
    // deviceSettingsTab.clickListAllButton();
    //
    // Assert.assertEquals(authorizationAmountValue2,
    // deviceSettingsTab.getAuthorizationAmountValue());
    // Assert.assertEquals(twoTierPricingValue2,
    // deviceSettingsTab.getTwoTierPricingValue());
    // Assert.assertEquals(attractMessageLine1Value2,
    // deviceSettingsTab.getAttractMessageLine1Value());
    // Assert.assertEquals(attractMessageLine2Value2,
    // deviceSettingsTab.getAttractMessageLine2Value());
    // Assert.assertEquals(welcomeMessageLine1Value2,
    // deviceSettingsTab.getWelcomeMessageLine1Value());
    // Assert.assertEquals(welcomeMessageLine2Value2,
    // deviceSettingsTab.getWelcomeMessageLine2Value());
    //// Assert.assertEquals(minimumAllowedVendAmountValue2,
    // deviceSettingsTab.getMinimumAllowedVendAmountValue());
    // Assert.assertEquals(disableMessageLine1Value2,
    // deviceSettingsTab.getDisableMessageLine1Value());
    // Assert.assertEquals(disableMessageLine2Value2,
    // deviceSettingsTab.getDisableMessageLine2Value());
    // Assert.assertEquals(serverOverrideCallInTimeWindowStartActivatedValue2,
    // deviceSettingsTab.getServerOverrideCallInTimeWindowStartActivatedValue());
    // Assert.assertEquals(serverOverrideCallInTimeWindowHoursActivatedValue2,
    // deviceSettingsTab.getServerOverrideCallInTimeWindowHoursActivatedValue());
    // Assert.assertEquals(serverOverrideCallInTimeWindowStartDEXValue2,
    // deviceSettingsTab.getServerOverrideCallInTimeWindowStartDEXValue());
    // Assert.assertEquals(serverOverrideCallInTimeWindowHoursDEXValue2,
    // deviceSettingsTab.getServerOverrideCallInTimeWindowHoursDEXValue());
    //// Assert.assertEquals(serverOverrideMaximumAuthorizationAmountValue2,
    // deviceSettingsTab.getServerOverrideMaximumAuthorizationAmountValue());
    // Assert.assertEquals(mDBSettingMaxNumberOfItemsValue2,
    // deviceSettingsTab.getMDBSettingMaxNumberOfItemsValue());
    // Assert.assertEquals(mDBSettingTimeoutfor1stSelectionValue2,
    // deviceSettingsTab.getMDBSettingTimeoutfor1stSelectionValue());
    // Assert.assertEquals(mDBSettingTimeoutfor2nd3rdEtcSelectionsValue2,
    // deviceSettingsTab.getMDBSettingTimeoutfor2nd3rdEtcSelectionsValue());
    // Assert.assertEquals(mDBSettingVendSessionTimeoutValue2,
    // deviceSettingsTab.getMDBSettingVendSessionTimeoutValue());
    // Assert.assertEquals(cashTransactionRecordingValue2,
    // deviceSettingsTab.getCashTransactionRecordingValue());
    // Assert.assertEquals(dEXSettingScheduleValue2 + "^0300^0",
    // deviceSettingsTab.getDEXSettingScheduleValue());
    // Assert.assertEquals(auxSerialPortSelectValue2,
    // deviceSettingsTab.getAuxSerialPortSelectValue());
    // Assert.assertEquals(vMCInterfaceTypeValue2,
    // deviceSettingsTab.getVMCInterfaceTypeValue());
    // Assert.assertEquals(coinPulseDurationValue2,
    // deviceSettingsTab.getCoinPulseDurationValue());
    // Assert.assertEquals(coinPulseSpacingValue2,
    // deviceSettingsTab.getCoinPulseSpacingValue());
    // Assert.assertEquals(coinPulseValueValue2,
    // deviceSettingsTab.getCoinPulseValueValue());
    // Assert.assertEquals(coinAutoTimeoutValue2,
    // deviceSettingsTab.getCoinAutoTimeoutValue());
    // Assert.assertEquals(coinReaderEnableActiveStateValue2,
    // deviceSettingsTab.getCoinReaderEnableActiveStateValue());
    // Assert.assertEquals(coinItemPriceN1Value2,
    // deviceSettingsTab.getCoinItemPriceN1Value());
    // Assert.assertEquals(coinItemPriceN1LabelValue2,
    // deviceSettingsTab.getCoinItemPriceN1LabelValue());
    // Assert.assertEquals(coinItemPriceN2Value2,
    // deviceSettingsTab.getCoinItemPriceN2Value());
    // Assert.assertEquals(coinItemPriceN2LabelValue2,
    // deviceSettingsTab.getCoinItemPriceN2LabelValue());
    // Assert.assertEquals(coinItemPriceN3Value2,
    // deviceSettingsTab.getCoinItemPriceN3Value());
    // Assert.assertEquals(coinItemPriceN3LabelValue2,
    // deviceSettingsTab.getCoinItemPriceN3LabelValue());
    // Assert.assertEquals(coinItemPriceN4Value2,
    // deviceSettingsTab.getCoinItemPriceN4Value());
    // Assert.assertEquals(coinItemPriceN4LabelValue2,
    // deviceSettingsTab.getCoinItemPriceN4LabelValue());
    // Assert.assertEquals(coinItemPriceN5Value2,
    // deviceSettingsTab.getCoinItemPriceN5Value());
    // Assert.assertEquals(coinItemPriceN5LabelValue2,
    // deviceSettingsTab.getCoinItemPriceN5LabelValue());
    // Assert.assertEquals(coinItemPriceN6Value2,
    // deviceSettingsTab.getCoinItemPriceN6Value());
    // Assert.assertEquals(coinItemPriceN6LabelValue2,
    // deviceSettingsTab.getCoinItemPriceN6LabelValue());
    // Assert.assertEquals(coinItemPriceN7Value2,
    // deviceSettingsTab.getCoinItemPriceN7Value());
    // Assert.assertEquals(coinItemPriceN7LabelValue2,
    // deviceSettingsTab.getCoinItemPriceN7LabelValue());
    // Assert.assertEquals(coinItemPriceN8Value2,
    // deviceSettingsTab.getCoinItemPriceN8Value());
    // Assert.assertEquals(coinItemPriceN8LabelValue2,
    // deviceSettingsTab.getCoinItemPriceN8LabelValue());
    // Assert.assertEquals(pulseTimeValueValue2,
    // deviceSettingsTab.getPulseTimeValueValue());
    // Assert.assertEquals(topOffValueValue2,
    // deviceSettingsTab.getTopOffValueValue());
    // Assert.assertEquals(pulseCaptureValueValue2,
    // deviceSettingsTab.getPulseCaptureValueValue());
    // }
    //

    @Test
    public void USAT_306_USALive_Administration_Devices_DeviceConfigurationSaveApplicapleValue() throws Exception {

	// driver.get("https://dms-ecc.usatech.com:8443/");

	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();
	// LeftPanel leftPanel = new LeftPanel(driver);
	// leftPanel.clickMenuItemByText("Devices");
	TopPanel topPanel = new TopPanel(driver);
	topPanel.general.devices.navigateTo();

	DevicesPage devicesPage = new DevicesPage(driver);
	// devicesPage.expandTreeRootNode();
	// String node1 = "Test Account Customer";
	// String node2 = "#1 USAT Prepaid";
	// devicesPage.expandTreeNode(node1);
	// devicesPage.expandTreeNode(node2);
	//// String region = "Retired Devices";
	// String region = "Current Devices";
	// devicesPage.expandTreeNode(region);
	String deviceId = "VJ000000021";
	// devicesPage.getDevice(deviceId).click();
	devicesPage.searchFor(deviceId);
	devicesPage.clickByDeviceIdLink(deviceId);

	DeviceProfilePage deviceProfilePage = new DeviceProfilePage(driver);

	deviceProfilePage.getDeviceConfiguration()
			 .click();

	// String authorizationAmountValue1 = "11.11";
	// String authorizationAmountValue2 = "22.22";
	String twoTierPricingValue1 = "0.01";
	String twoTierPricingValue2 = "0.02";
	String attractMessageLine1Value1 = "Swipe Or Tap1";
	String attractMessageLine1Value2 = "Swipe Or Tap2";
	String attractMessageLine2Value1 = "To Begin1";
	String attractMessageLine2Value2 = "To Begin2";
	String welcomeMessageLine1Value1 = "WelcomeToePort1";
	String welcomeMessageLine1Value2 = "WelcomeToePort2";
	String welcomeMessageLine2Value1 = "Same As Cash1";
	String welcomeMessageLine2Value2 = "Same As Cash2";
	String minimumAllowedVendAmountValue1 = "";
	String disableMessageLine1Value1 = "disable1";
	String disableMessageLine1Value2 = "disable1 2";
	String disableMessageLine2Value1 = "disable2";
	String disableMessageLine2Value2 = "disable2 2";
	String serverOverrideCallInTimeWindowStartActivatedValue1 = "1111";
	String serverOverrideCallInTimeWindowStartActivatedValue2 = "2222";
	String serverOverrideCallInTimeWindowHoursActivatedValue1 = "1";
	String serverOverrideCallInTimeWindowHoursActivatedValue2 = "2";
	String serverOverrideCallInTimeWindowStartDEXValue1 = "1111";
	String serverOverrideCallInTimeWindowStartDEXValue2 = "2222";
	String serverOverrideCallInTimeWindowHoursDEXValue1 = "1";
	String serverOverrideCallInTimeWindowHoursDEXValue2 = "2";
	String serverOverrideMaximumAuthorizationAmountValue1 = "1.00";
	String serverOverrideMaximumAuthorizationAmountValue2 = "2.00";
	String mDBSettingMaxNumberOfItemsValue1 = "11";
	String mDBSettingMaxNumberOfItemsValue2 = "22";
	String mDBSettingTimeoutfor1stSelectionValue1 = "11";
	String mDBSettingTimeoutfor1stSelectionValue2 = "22";
	String mDBSettingTimeoutfor2nd3rdEtcSelectionsValue1 = "11";
	String mDBSettingTimeoutfor2nd3rdEtcSelectionsValue2 = "22";
	String mDBSettingVendSessionTimeoutValue1 = "111";
	String mDBSettingVendSessionTimeoutValue2 = "222";
	String cashTransactionRecordingValue1 = "0";
	String cashTransactionRecordingValue2 = "1";
	String dEXSettingScheduleValue1 = "I";
	String dEXSettingScheduleValue2 = "W";
	String auxSerialPortSelectValue1 = "0";
	String auxSerialPortSelectValue2 = "1";
	String vMCInterfaceTypeValue1 = "3";
	String vMCInterfaceTypeValue2 = "4";
	String coinPulseDurationValue1 = "11";
	String coinPulseDurationValue2 = "22";
	String coinPulseSpacingValue1 = "111";
	String coinPulseSpacingValue2 = "222";
	String coinPulseValueValue1 = "1.11";
	String coinPulseValueValue2 = "2.22";
	String coinAutoTimeoutValue1 = "11";
	String coinAutoTimeoutValue2 = "22";
	String coinReaderEnableActiveStateValue1 = "1";
	String coinReaderEnableActiveStateValue2 = "2";
	String coinItemPriceN1Value1 = "1.10";
	String coinItemPriceN1Value2 = "2.20";
	String coinItemPriceN1LabelValue1 = "Coin price 1.1";
	String coinItemPriceN1LabelValue2 = "Coin price 1.2";
	String coinItemPriceN2Value1 = "2.10";
	String coinItemPriceN2Value2 = "2.20";
	String coinItemPriceN2LabelValue1 = "Coin price 2.1";
	String coinItemPriceN2LabelValue2 = "Coin price 2.2";
	String coinItemPriceN3Value1 = "3.10";
	String coinItemPriceN3Value2 = "3.20";
	String coinItemPriceN3LabelValue1 = "Coin price 3.1";
	String coinItemPriceN3LabelValue2 = "Coin price 3.2";
	String coinItemPriceN4Value1 = "4.10";
	String coinItemPriceN4Value2 = "4.20";
	String coinItemPriceN4LabelValue1 = "Coin price 4.1";
	String coinItemPriceN4LabelValue2 = "Coin price 4.2";
	String coinItemPriceN5Value1 = "5.10";
	String coinItemPriceN5Value2 = "5.20";
	String coinItemPriceN5LabelValue1 = "Coin price 5.1";
	String coinItemPriceN5LabelValue2 = "Coin price 5.2";
	String coinItemPriceN6Value1 = "6.10";
	String coinItemPriceN6Value2 = "6.20";
	String coinItemPriceN6LabelValue1 = "Coin price 6.1";
	String coinItemPriceN6LabelValue2 = "Coin price 6.2";
	String coinItemPriceN7Value1 = "7.10";
	String coinItemPriceN7Value2 = "7.20";
	String coinItemPriceN7LabelValue1 = "Coin price 7.1";
	String coinItemPriceN7LabelValue2 = "Coin price 7.2";
	String coinItemPriceN8Value1 = "8.10";
	String coinItemPriceN8Value2 = "8.20";
	String coinItemPriceN8LabelValue1 = "Coin price 8.1";
	String coinItemPriceN8LabelValue2 = "Coin price 8.2";
	String pulseTimeValueValue1 = "11";
	String pulseTimeValueValue2 = "22";
	String topOffValueValue1 = "0.10";
	String topOffValueValue2 = "0.20";
	String pulseCaptureValueValue1 = "1.10";
	String pulseCaptureValueValue2 = "2.10";

	// Set 1-st set of values
	DeviceConfigurationPage deviceConfigurationPage = new DeviceConfigurationPage(driver);
	// deviceConfigurationPage.setAuthorizationAmountWebElement(authorizationAmountValue1);
	deviceConfigurationPage.setTwoTierPricingWebElement(twoTierPricingValue1);
	deviceConfigurationPage.setAttractMessageLine1WebElement(attractMessageLine1Value1);
	deviceConfigurationPage.setAttractMessageLine2WebElement(attractMessageLine2Value1);
	deviceConfigurationPage.setWelcomeMessageLine1WebElement(welcomeMessageLine1Value1);
	deviceConfigurationPage.setWelcomeMessageLine2WebElement(welcomeMessageLine2Value1);
	// deviceConfigurationPage.setMinimumAllowedVendAmountWebElement(minimumAllowedVendAmountValue1);
	deviceConfigurationPage.setDisableMessageLine1WebElement(disableMessageLine1Value1);
	deviceConfigurationPage.setDisableMessageLine2WebElement(disableMessageLine2Value1);
	deviceConfigurationPage.setServerOverrideCallInTimeWindowStartActivatedWebElement(
		serverOverrideCallInTimeWindowStartActivatedValue1);
	deviceConfigurationPage.setServerOverrideCallInTimeWindowHoursActivatedWebElement(
		serverOverrideCallInTimeWindowHoursActivatedValue1);
	deviceConfigurationPage.setServerOverrideCallInTimeWindowStartDEXWebElement(
		serverOverrideCallInTimeWindowStartDEXValue1);
	deviceConfigurationPage.setServerOverrideCallInTimeWindowHoursDEXWebElement(
		serverOverrideCallInTimeWindowHoursDEXValue1);
	// deviceConfigurationPage.setServerOverrideMaximumAuthorizationAmountWebElement(serverOverrideMaximumAuthorizationAmountValue1);
	deviceConfigurationPage.setMDBSettingMaxNumberOfItemsWebElement(mDBSettingMaxNumberOfItemsValue1);
	deviceConfigurationPage.setMDBSettingTimeoutfor1stSelectionWebElement(mDBSettingTimeoutfor1stSelectionValue1);
	deviceConfigurationPage.setMDBSettingTimeoutfor2nd3rdEtcSelectionsWebElement(
		mDBSettingTimeoutfor2nd3rdEtcSelectionsValue1);
	deviceConfigurationPage.setMDBSettingVendSessionTimeoutWebElement(mDBSettingVendSessionTimeoutValue1);
	deviceConfigurationPage.setCashTransactionRecordingWebElement(cashTransactionRecordingValue1);
	deviceConfigurationPage.setDEXSettingScheduleWebElement(dEXSettingScheduleValue1);
	deviceConfigurationPage.setAuxSerialPortSelectWebElement(auxSerialPortSelectValue1);
	deviceConfigurationPage.setVMCInterfaceTypeWebElement(vMCInterfaceTypeValue1);
	deviceConfigurationPage.setCoinPulseDurationWebElement(coinPulseDurationValue1);
	deviceConfigurationPage.setCoinPulseSpacingWebElement(coinPulseSpacingValue1);
	deviceConfigurationPage.setCoinPulseValueWebElement(coinPulseValueValue1);
	deviceConfigurationPage.setCoinAutoTimeoutWebElement(coinAutoTimeoutValue1);
	deviceConfigurationPage.setCoinReaderEnableActiveStateWebElement(coinReaderEnableActiveStateValue1);
	deviceConfigurationPage.setCoinItemPriceN1WebElement(coinItemPriceN1Value1);
	deviceConfigurationPage.setCoinItemPriceN1LabelWebElement(coinItemPriceN1LabelValue1);
	deviceConfigurationPage.setCoinItemPriceN2WebElement(coinItemPriceN2Value1);
	deviceConfigurationPage.setCoinItemPriceN2LabelWebElement(coinItemPriceN2LabelValue1);
	deviceConfigurationPage.setCoinItemPriceN3WebElement(coinItemPriceN3Value1);
	deviceConfigurationPage.setCoinItemPriceN3LabelWebElement(coinItemPriceN3LabelValue1);
	deviceConfigurationPage.setCoinItemPriceN4WebElement(coinItemPriceN4Value1);
	deviceConfigurationPage.setCoinItemPriceN4LabelWebElement(coinItemPriceN4LabelValue1);
	deviceConfigurationPage.setCoinItemPriceN5WebElement(coinItemPriceN5Value1);
	deviceConfigurationPage.setCoinItemPriceN5LabelWebElement(coinItemPriceN5LabelValue1);
	deviceConfigurationPage.setCoinItemPriceN6WebElement(coinItemPriceN6Value1);
	deviceConfigurationPage.setCoinItemPriceN6LabelWebElement(coinItemPriceN6LabelValue1);
	deviceConfigurationPage.setCoinItemPriceN7WebElement(coinItemPriceN7Value1);
	deviceConfigurationPage.setCoinItemPriceN7LabelWebElement(coinItemPriceN7LabelValue1);
	deviceConfigurationPage.setCoinItemPriceN8WebElement(coinItemPriceN8Value1);
	deviceConfigurationPage.setCoinItemPriceN8LabelWebElement(coinItemPriceN8LabelValue1);
	deviceConfigurationPage.setPulseTimeValueWebElement(pulseTimeValueValue1);
	deviceConfigurationPage.setTopOffValueWebElement(topOffValueValue1);
	deviceConfigurationPage.setPulseCaptureValueWebElement(pulseCaptureValueValue1);
	// 2. Save changes
	deviceConfigurationPage.save();
	deviceConfigurationPage.verifySuccessUpdateMessage();

	openDms();
	new pages.Dms.LogInPage(driver).logInAsCommonUser();
	new DevicesMenu(driver).searchDevice(deviceId);

	DeviceSettingsTab deviceSettingsTab = new DeviceSettingsTab(driver);
	deviceSettingsTab.clickDeviceSettingsTab();
	deviceSettingsTab.clickListAllButton();
	// 3. Verify 1-st set of values in DMS

	// Assert.assertEquals(authorizationAmountValue1,
	// deviceSettingsTab.getAuthorizationAmountValue());
	Assert.assertEquals(twoTierPricingValue1, deviceSettingsTab.getTwoTierPricingValue());
	Assert.assertEquals(attractMessageLine1Value1, deviceSettingsTab.getAttractMessageLine1Value());
	Assert.assertEquals(attractMessageLine2Value1, deviceSettingsTab.getAttractMessageLine2Value());
	Assert.assertEquals(welcomeMessageLine1Value1, deviceSettingsTab.getWelcomeMessageLine1Value());
	Assert.assertEquals(welcomeMessageLine2Value1, deviceSettingsTab.getWelcomeMessageLine2Value());
	// Assert.assertEquals(minimumAllowedVendAmountValue1,
	// deviceSettingsTab.getMinimumAllowedVendAmountValue());
	Assert.assertEquals(disableMessageLine1Value1, deviceSettingsTab.getDisableMessageLine1Value());
	Assert.assertEquals(disableMessageLine2Value1, deviceSettingsTab.getDisableMessageLine2Value());
	Assert.assertEquals(serverOverrideCallInTimeWindowStartActivatedValue1,
		deviceSettingsTab.getServerOverrideCallInTimeWindowStartActivatedValue());
	Assert.assertEquals(serverOverrideCallInTimeWindowHoursActivatedValue1,
		deviceSettingsTab.getServerOverrideCallInTimeWindowHoursActivatedValue());
	Assert.assertEquals(serverOverrideCallInTimeWindowStartDEXValue1,
		deviceSettingsTab.getServerOverrideCallInTimeWindowStartDEXValue());
	Assert.assertEquals(serverOverrideCallInTimeWindowHoursDEXValue1,
		deviceSettingsTab.getServerOverrideCallInTimeWindowHoursDEXValue());
	// Assert.assertEquals(serverOverrideMaximumAuthorizationAmountValue1,
	// deviceSettingsTab.getServerOverrideMaximumAuthorizationAmountValue());
	Assert.assertEquals(mDBSettingMaxNumberOfItemsValue1, deviceSettingsTab.getMDBSettingMaxNumberOfItemsValue());
	Assert.assertEquals(mDBSettingTimeoutfor1stSelectionValue1,
		deviceSettingsTab.getMDBSettingTimeoutfor1stSelectionValue());
	Assert.assertEquals(mDBSettingTimeoutfor2nd3rdEtcSelectionsValue1,
		deviceSettingsTab.getMDBSettingTimeoutfor2nd3rdEtcSelectionsValue());
	Assert.assertEquals(mDBSettingVendSessionTimeoutValue1,
		deviceSettingsTab.getMDBSettingVendSessionTimeoutValue());
	Assert.assertEquals(cashTransactionRecordingValue1, deviceSettingsTab.getCashTransactionRecordingValue());

	if (getEnvironment().equalsIgnoreCase("ecc"))
	    Assert.assertEquals(dEXSettingScheduleValue1 + "^28800^43200",
		    deviceSettingsTab.getDEXSettingScheduleValue());
	else
	    Assert.assertEquals(dEXSettingScheduleValue1 + "^25200^43200",
		    deviceSettingsTab.getDEXSettingScheduleValue());
	Assert.assertEquals(auxSerialPortSelectValue1, deviceSettingsTab.getAuxSerialPortSelectValue());
	Assert.assertEquals(vMCInterfaceTypeValue1, deviceSettingsTab.getVMCInterfaceTypeValue());
	Assert.assertEquals(coinPulseDurationValue1, deviceSettingsTab.getCoinPulseDurationValue());
	Assert.assertEquals(coinPulseSpacingValue1, deviceSettingsTab.getCoinPulseSpacingValue());
	Assert.assertEquals(coinPulseValueValue1, deviceSettingsTab.getCoinPulseValueValue());
	Assert.assertEquals(coinAutoTimeoutValue1, deviceSettingsTab.getCoinAutoTimeoutValue());
	Assert.assertEquals(coinReaderEnableActiveStateValue1, deviceSettingsTab.getCoinReaderEnableActiveStateValue());
	Assert.assertEquals(coinItemPriceN1Value1, deviceSettingsTab.getCoinItemPriceN1Value());
	Assert.assertEquals(coinItemPriceN1LabelValue1, deviceSettingsTab.getCoinItemPriceN1LabelValue());
	Assert.assertEquals(coinItemPriceN2Value1, deviceSettingsTab.getCoinItemPriceN2Value());
	Assert.assertEquals(coinItemPriceN2LabelValue1, deviceSettingsTab.getCoinItemPriceN2LabelValue());
	Assert.assertEquals(coinItemPriceN3Value1, deviceSettingsTab.getCoinItemPriceN3Value());
	Assert.assertEquals(coinItemPriceN3LabelValue1, deviceSettingsTab.getCoinItemPriceN3LabelValue());
	Assert.assertEquals(coinItemPriceN4Value1, deviceSettingsTab.getCoinItemPriceN4Value());
	Assert.assertEquals(coinItemPriceN4LabelValue1, deviceSettingsTab.getCoinItemPriceN4LabelValue());
	Assert.assertEquals(coinItemPriceN5Value1, deviceSettingsTab.getCoinItemPriceN5Value());
	Assert.assertEquals(coinItemPriceN5LabelValue1, deviceSettingsTab.getCoinItemPriceN5LabelValue());
	Assert.assertEquals(coinItemPriceN6Value1, deviceSettingsTab.getCoinItemPriceN6Value());
	Assert.assertEquals(coinItemPriceN6LabelValue1, deviceSettingsTab.getCoinItemPriceN6LabelValue());
	Assert.assertEquals(coinItemPriceN7Value1, deviceSettingsTab.getCoinItemPriceN7Value());
	Assert.assertEquals(coinItemPriceN7LabelValue1, deviceSettingsTab.getCoinItemPriceN7LabelValue());
	Assert.assertEquals(coinItemPriceN8Value1, deviceSettingsTab.getCoinItemPriceN8Value());
	Assert.assertEquals(coinItemPriceN8LabelValue1, deviceSettingsTab.getCoinItemPriceN8LabelValue());
	Assert.assertEquals(pulseTimeValueValue1, deviceSettingsTab.getPulseTimeValueValue());
	Assert.assertEquals(topOffValueValue1, deviceSettingsTab.getTopOffValueValue());
	Assert.assertEquals(pulseCaptureValueValue1, deviceSettingsTab.getPulseCaptureValueValue());
	logger.info("");
	// 4. Open device in USALive
	openUsaLive();
	// new LogInPage(driver).logInAsPowerUser();

//	leftPanel.clickMenuItemByText("Devices");
	topPanel.general.devices.navigateTo();

	// devicesPage.expandTreeRootNode();
	// devicesPage.expandTreeNode(node1);
	// devicesPage.expandTreeNode(node2);
	// devicesPage.expandTreeNode(region);
	// devicesPage.getDevice(deviceId).click();
	devicesPage.searchFor(deviceId);
	devicesPage.clickByDeviceIdLink(deviceId);

	deviceProfilePage.getDeviceConfiguration()
			 .click();
	// 5. Verify 1-st set of values in USALive
	// Assert.assertEquals(authorizationAmountValue1,
	// deviceConfigurationPage.getAuthorizationAmountWebElement().getAttribute("value"));
	Assert.assertEquals(twoTierPricingValue1, deviceConfigurationPage.getTwoTierPricingWebElement()
									 .getAttribute("value"));
	Assert.assertEquals(attractMessageLine1Value1, deviceConfigurationPage.getAttractMessageLine1WebElement()
									      .getAttribute("value"));
	Assert.assertEquals(attractMessageLine2Value1, deviceConfigurationPage.getAttractMessageLine2WebElement()
									      .getAttribute("value"));
	Assert.assertEquals(welcomeMessageLine1Value1, deviceConfigurationPage.getWelcomeMessageLine1WebElement()
									      .getAttribute("value"));
	Assert.assertEquals(welcomeMessageLine2Value1, deviceConfigurationPage.getWelcomeMessageLine2WebElement()
									      .getAttribute("value"));
	Assert.assertEquals(disableMessageLine1Value1, deviceConfigurationPage.getDisableMessageLine1WebElement()
									      .getAttribute("value"));
	Assert.assertEquals(disableMessageLine2Value1, deviceConfigurationPage.getDisableMessageLine2WebElement()
									      .getAttribute("value"));
	Assert.assertEquals(serverOverrideCallInTimeWindowStartActivatedValue1,
		deviceConfigurationPage.getServerOverrideCallInTimeWindowStartActivatedWebElement()
				       .getAttribute("value"));
	Assert.assertEquals(serverOverrideCallInTimeWindowHoursActivatedValue1,
		deviceConfigurationPage.getServerOverrideCallInTimeWindowHoursActivatedWebElement()
				       .getAttribute("value"));
	Assert.assertEquals(serverOverrideCallInTimeWindowStartDEXValue1,
		deviceConfigurationPage.getServerOverrideCallInTimeWindowStartDEXWebElement()
				       .getAttribute("value"));
	Assert.assertEquals(serverOverrideCallInTimeWindowHoursDEXValue1,
		deviceConfigurationPage.getServerOverrideCallInTimeWindowHoursDEXWebElement()
				       .getAttribute("value"));
	Assert.assertEquals(mDBSettingMaxNumberOfItemsValue1,
		deviceConfigurationPage.getMDBSettingMaxNumberOfItemsWebElement()
				       .getAttribute("value"));
	Assert.assertEquals(mDBSettingTimeoutfor1stSelectionValue1,
		deviceConfigurationPage.getMDBSettingTimeoutfor1stSelectionWebElement()
				       .getAttribute("value"));
	Assert.assertEquals(mDBSettingTimeoutfor2nd3rdEtcSelectionsValue1,
		deviceConfigurationPage.getMDBSettingTimeoutfor2nd3rdEtcSelectionsWebElement()
				       .getAttribute("value"));
	Assert.assertEquals(mDBSettingVendSessionTimeoutValue1,
		deviceConfigurationPage.getMDBSettingVendSessionTimeoutWebElement()
				       .getAttribute("value"));
	Assert.assertEquals(cashTransactionRecordingValue1,
		deviceConfigurationPage.getCashTransactionRecordingWebElement()
				       .getAttribute("value"));
	Assert.assertEquals(dEXSettingScheduleValue1, deviceConfigurationPage.getDEXSettingScheduleWebElement()
									     .getAttribute("value"));
	Assert.assertEquals(auxSerialPortSelectValue1, deviceConfigurationPage.getAuxSerialPortSelectWebElement()
									      .getAttribute("value"));
	Assert.assertEquals(vMCInterfaceTypeValue1, deviceConfigurationPage.getVMCInterfaceTypeWebElement()
									   .getAttribute("value"));
	Assert.assertEquals(coinPulseDurationValue1, deviceConfigurationPage.getCoinPulseDurationWebElement()
									    .getAttribute("value"));
	Assert.assertEquals(coinPulseSpacingValue1, deviceConfigurationPage.getCoinPulseSpacingWebElement()
									   .getAttribute("value"));
	Assert.assertEquals(coinPulseValueValue1, deviceConfigurationPage.getCoinPulseValueWebElement()
									 .getAttribute("value"));
	Assert.assertEquals(coinAutoTimeoutValue1, deviceConfigurationPage.getCoinAutoTimeoutWebElement()
									  .getAttribute("value"));
	Assert.assertEquals(coinReaderEnableActiveStateValue1,
		deviceConfigurationPage.getCoinReaderEnableActiveStateWebElement()
				       .getAttribute("value"));
	Assert.assertEquals(coinItemPriceN1Value1, deviceConfigurationPage.getCoinItemPriceN1WebElement()
									  .getAttribute("value"));
	Assert.assertEquals(coinItemPriceN1LabelValue1, deviceConfigurationPage.getCoinItemPriceN1LabelWebElement()
									       .getAttribute("value"));
	Assert.assertEquals(coinItemPriceN2Value1, deviceConfigurationPage.getCoinItemPriceN2WebElement()
									  .getAttribute("value"));
	Assert.assertEquals(coinItemPriceN2LabelValue1, deviceConfigurationPage.getCoinItemPriceN2LabelWebElement()
									       .getAttribute("value"));
	Assert.assertEquals(coinItemPriceN3Value1, deviceConfigurationPage.getCoinItemPriceN3WebElement()
									  .getAttribute("value"));
	Assert.assertEquals(coinItemPriceN3LabelValue1, deviceConfigurationPage.getCoinItemPriceN3LabelWebElement()
									       .getAttribute("value"));
	Assert.assertEquals(coinItemPriceN4Value1, deviceConfigurationPage.getCoinItemPriceN4WebElement()
									  .getAttribute("value"));
	Assert.assertEquals(coinItemPriceN4LabelValue1, deviceConfigurationPage.getCoinItemPriceN4LabelWebElement()
									       .getAttribute("value"));
	Assert.assertEquals(coinItemPriceN5Value1, deviceConfigurationPage.getCoinItemPriceN5WebElement()
									  .getAttribute("value"));
	Assert.assertEquals(coinItemPriceN5LabelValue1, deviceConfigurationPage.getCoinItemPriceN5LabelWebElement()
									       .getAttribute("value"));
	Assert.assertEquals(coinItemPriceN6Value1, deviceConfigurationPage.getCoinItemPriceN6WebElement()
									  .getAttribute("value"));
	Assert.assertEquals(coinItemPriceN6LabelValue1, deviceConfigurationPage.getCoinItemPriceN6LabelWebElement()
									       .getAttribute("value"));
	Assert.assertEquals(coinItemPriceN7Value1, deviceConfigurationPage.getCoinItemPriceN7WebElement()
									  .getAttribute("value"));
	Assert.assertEquals(coinItemPriceN7LabelValue1, deviceConfigurationPage.getCoinItemPriceN7LabelWebElement()
									       .getAttribute("value"));
	Assert.assertEquals(coinItemPriceN8Value1, deviceConfigurationPage.getCoinItemPriceN8WebElement()
									  .getAttribute("value"));
	Assert.assertEquals(coinItemPriceN8LabelValue1, deviceConfigurationPage.getCoinItemPriceN8LabelWebElement()
									       .getAttribute("value"));
	Assert.assertEquals(pulseTimeValueValue1, deviceConfigurationPage.getPulseTimeValueWebElement()
									 .getAttribute("value"));
	Assert.assertEquals(topOffValueValue1, deviceConfigurationPage.getTopOffValueWebElement()
								      .getAttribute("value"));
	Assert.assertEquals(pulseCaptureValueValue1, deviceConfigurationPage.getPulseCaptureValueWebElement()
									    .getAttribute("value"));
	// 6. Set 2-nd set of values
	// deviceConfigurationPage.setAuthorizationAmountWebElement(authorizationAmountValue2);
	deviceConfigurationPage.setTwoTierPricingWebElement(twoTierPricingValue2);
	deviceConfigurationPage.setAttractMessageLine1WebElement(attractMessageLine1Value2);
	deviceConfigurationPage.setAttractMessageLine2WebElement(attractMessageLine2Value2);
	deviceConfigurationPage.setWelcomeMessageLine1WebElement(welcomeMessageLine1Value2);
	deviceConfigurationPage.setWelcomeMessageLine2WebElement(welcomeMessageLine2Value2);
	// deviceConfigurationPage.setMinimumAllowedVendAmountWebElement(minimumAllowedVendAmountValue2);
	deviceConfigurationPage.setDisableMessageLine1WebElement(disableMessageLine1Value2);
	deviceConfigurationPage.setDisableMessageLine2WebElement(disableMessageLine2Value2);
	deviceConfigurationPage.setServerOverrideCallInTimeWindowStartActivatedWebElement(
		serverOverrideCallInTimeWindowStartActivatedValue2);
	deviceConfigurationPage.setServerOverrideCallInTimeWindowHoursActivatedWebElement(
		serverOverrideCallInTimeWindowHoursActivatedValue2);
	deviceConfigurationPage.setServerOverrideCallInTimeWindowStartDEXWebElement(
		serverOverrideCallInTimeWindowStartDEXValue2);
	deviceConfigurationPage.setServerOverrideCallInTimeWindowHoursDEXWebElement(
		serverOverrideCallInTimeWindowHoursDEXValue2);
	// deviceConfigurationPage.setServerOverrideMaximumAuthorizationAmountWebElement(serverOverrideMaximumAuthorizationAmountValue2);
	deviceConfigurationPage.setMDBSettingMaxNumberOfItemsWebElement(mDBSettingMaxNumberOfItemsValue2);
	deviceConfigurationPage.setMDBSettingTimeoutfor1stSelectionWebElement(mDBSettingTimeoutfor1stSelectionValue2);
	deviceConfigurationPage.setMDBSettingTimeoutfor2nd3rdEtcSelectionsWebElement(
		mDBSettingTimeoutfor2nd3rdEtcSelectionsValue2);
	deviceConfigurationPage.setMDBSettingVendSessionTimeoutWebElement(mDBSettingVendSessionTimeoutValue2);
	deviceConfigurationPage.setCashTransactionRecordingWebElement(cashTransactionRecordingValue2);
	deviceConfigurationPage.setDEXSettingScheduleWebElement(dEXSettingScheduleValue2);
	deviceConfigurationPage.setAuxSerialPortSelectWebElement(auxSerialPortSelectValue2);
	deviceConfigurationPage.setVMCInterfaceTypeWebElement(vMCInterfaceTypeValue2);
	deviceConfigurationPage.setCoinPulseDurationWebElement(coinPulseDurationValue2);
	deviceConfigurationPage.setCoinPulseSpacingWebElement(coinPulseSpacingValue2);
	deviceConfigurationPage.setCoinPulseValueWebElement(coinPulseValueValue2);
	deviceConfigurationPage.setCoinAutoTimeoutWebElement(coinAutoTimeoutValue2);
	deviceConfigurationPage.setCoinReaderEnableActiveStateWebElement(coinReaderEnableActiveStateValue2);
	deviceConfigurationPage.setCoinItemPriceN1WebElement(coinItemPriceN1Value2);
	deviceConfigurationPage.setCoinItemPriceN1LabelWebElement(coinItemPriceN1LabelValue2);
	deviceConfigurationPage.setCoinItemPriceN2WebElement(coinItemPriceN2Value2);
	deviceConfigurationPage.setCoinItemPriceN2LabelWebElement(coinItemPriceN2LabelValue2);
	deviceConfigurationPage.setCoinItemPriceN3WebElement(coinItemPriceN3Value2);
	deviceConfigurationPage.setCoinItemPriceN3LabelWebElement(coinItemPriceN3LabelValue2);
	deviceConfigurationPage.setCoinItemPriceN4WebElement(coinItemPriceN4Value2);
	deviceConfigurationPage.setCoinItemPriceN4LabelWebElement(coinItemPriceN4LabelValue2);
	deviceConfigurationPage.setCoinItemPriceN5WebElement(coinItemPriceN5Value2);
	deviceConfigurationPage.setCoinItemPriceN5LabelWebElement(coinItemPriceN5LabelValue2);
	deviceConfigurationPage.setCoinItemPriceN6WebElement(coinItemPriceN6Value2);
	deviceConfigurationPage.setCoinItemPriceN6LabelWebElement(coinItemPriceN6LabelValue2);
	deviceConfigurationPage.setCoinItemPriceN7WebElement(coinItemPriceN7Value2);
	deviceConfigurationPage.setCoinItemPriceN7LabelWebElement(coinItemPriceN7LabelValue2);
	deviceConfigurationPage.setCoinItemPriceN8WebElement(coinItemPriceN8Value2);
	deviceConfigurationPage.setCoinItemPriceN8LabelWebElement(coinItemPriceN8LabelValue2);
	deviceConfigurationPage.setPulseTimeValueWebElement(pulseTimeValueValue2);
	deviceConfigurationPage.setTopOffValueWebElement(topOffValueValue2);
	deviceConfigurationPage.setPulseCaptureValueWebElement(pulseCaptureValueValue2);
	// 7. Save changes
	deviceConfigurationPage.save();
	deviceConfigurationPage.verifySuccessUpdateMessage();
	// 8. Verify 2-nd set of values in DMS
	openDms();
	// new pages.Dms.LogInPage(driver).logInAsCommonUser();
	new DevicesMenu(driver).searchDevice(deviceId);

	deviceSettingsTab.clickDeviceSettingsTab();
	deviceSettingsTab.clickListAllButton();

	// Assert.assertEquals(authorizationAmountValue2,
	// deviceSettingsTab.getAuthorizationAmountValue());
	Assert.assertEquals(twoTierPricingValue2, deviceSettingsTab.getTwoTierPricingValue());
	Assert.assertEquals(attractMessageLine1Value2, deviceSettingsTab.getAttractMessageLine1Value());
	Assert.assertEquals(attractMessageLine2Value2, deviceSettingsTab.getAttractMessageLine2Value());
	Assert.assertEquals(welcomeMessageLine1Value2, deviceSettingsTab.getWelcomeMessageLine1Value());
	Assert.assertEquals(welcomeMessageLine2Value2, deviceSettingsTab.getWelcomeMessageLine2Value());
	// Assert.assertEquals(minimumAllowedVendAmountValue2,
	// deviceSettingsTab.getMinimumAllowedVendAmountValue());
	Assert.assertEquals(disableMessageLine1Value2, deviceSettingsTab.getDisableMessageLine1Value());
	Assert.assertEquals(disableMessageLine2Value2, deviceSettingsTab.getDisableMessageLine2Value());
	Assert.assertEquals(serverOverrideCallInTimeWindowStartActivatedValue2,
		deviceSettingsTab.getServerOverrideCallInTimeWindowStartActivatedValue());
	Assert.assertEquals(serverOverrideCallInTimeWindowHoursActivatedValue2,
		deviceSettingsTab.getServerOverrideCallInTimeWindowHoursActivatedValue());
	Assert.assertEquals(serverOverrideCallInTimeWindowStartDEXValue2,
		deviceSettingsTab.getServerOverrideCallInTimeWindowStartDEXValue());
	Assert.assertEquals(serverOverrideCallInTimeWindowHoursDEXValue2,
		deviceSettingsTab.getServerOverrideCallInTimeWindowHoursDEXValue());
	// Assert.assertEquals(serverOverrideMaximumAuthorizationAmountValue2,
	// deviceSettingsTab.getServerOverrideMaximumAuthorizationAmountValue());
	Assert.assertEquals(mDBSettingMaxNumberOfItemsValue2, deviceSettingsTab.getMDBSettingMaxNumberOfItemsValue());
	Assert.assertEquals(mDBSettingTimeoutfor1stSelectionValue2,
		deviceSettingsTab.getMDBSettingTimeoutfor1stSelectionValue());
	Assert.assertEquals(mDBSettingTimeoutfor2nd3rdEtcSelectionsValue2,
		deviceSettingsTab.getMDBSettingTimeoutfor2nd3rdEtcSelectionsValue());
	Assert.assertEquals(mDBSettingVendSessionTimeoutValue2,
		deviceSettingsTab.getMDBSettingVendSessionTimeoutValue());
	Assert.assertEquals(cashTransactionRecordingValue2, deviceSettingsTab.getCashTransactionRecordingValue());
	Assert.assertEquals(dEXSettingScheduleValue2 + "^0300^0", deviceSettingsTab.getDEXSettingScheduleValue());
	Assert.assertEquals(auxSerialPortSelectValue2, deviceSettingsTab.getAuxSerialPortSelectValue());
	Assert.assertEquals(vMCInterfaceTypeValue2, deviceSettingsTab.getVMCInterfaceTypeValue());
	Assert.assertEquals(coinPulseDurationValue2, deviceSettingsTab.getCoinPulseDurationValue());
	Assert.assertEquals(coinPulseSpacingValue2, deviceSettingsTab.getCoinPulseSpacingValue());
	Assert.assertEquals(coinPulseValueValue2, deviceSettingsTab.getCoinPulseValueValue());
	Assert.assertEquals(coinAutoTimeoutValue2, deviceSettingsTab.getCoinAutoTimeoutValue());
	Assert.assertEquals(coinReaderEnableActiveStateValue2, deviceSettingsTab.getCoinReaderEnableActiveStateValue());
	Assert.assertEquals(coinItemPriceN1Value2, deviceSettingsTab.getCoinItemPriceN1Value());
	Assert.assertEquals(coinItemPriceN1LabelValue2, deviceSettingsTab.getCoinItemPriceN1LabelValue());
	Assert.assertEquals(coinItemPriceN2Value2, deviceSettingsTab.getCoinItemPriceN2Value());
	Assert.assertEquals(coinItemPriceN2LabelValue2, deviceSettingsTab.getCoinItemPriceN2LabelValue());
	Assert.assertEquals(coinItemPriceN3Value2, deviceSettingsTab.getCoinItemPriceN3Value());
	Assert.assertEquals(coinItemPriceN3LabelValue2, deviceSettingsTab.getCoinItemPriceN3LabelValue());
	Assert.assertEquals(coinItemPriceN4Value2, deviceSettingsTab.getCoinItemPriceN4Value());
	Assert.assertEquals(coinItemPriceN4LabelValue2, deviceSettingsTab.getCoinItemPriceN4LabelValue());
	Assert.assertEquals(coinItemPriceN5Value2, deviceSettingsTab.getCoinItemPriceN5Value());
	Assert.assertEquals(coinItemPriceN5LabelValue2, deviceSettingsTab.getCoinItemPriceN5LabelValue());
	Assert.assertEquals(coinItemPriceN6Value2, deviceSettingsTab.getCoinItemPriceN6Value());
	Assert.assertEquals(coinItemPriceN6LabelValue2, deviceSettingsTab.getCoinItemPriceN6LabelValue());
	Assert.assertEquals(coinItemPriceN7Value2, deviceSettingsTab.getCoinItemPriceN7Value());
	Assert.assertEquals(coinItemPriceN7LabelValue2, deviceSettingsTab.getCoinItemPriceN7LabelValue());
	Assert.assertEquals(coinItemPriceN8Value2, deviceSettingsTab.getCoinItemPriceN8Value());
	Assert.assertEquals(coinItemPriceN8LabelValue2, deviceSettingsTab.getCoinItemPriceN8LabelValue());
	Assert.assertEquals(pulseTimeValueValue2, deviceSettingsTab.getPulseTimeValueValue());
	Assert.assertEquals(topOffValueValue2, deviceSettingsTab.getTopOffValueValue());
	Assert.assertEquals(pulseCaptureValueValue2, deviceSettingsTab.getPulseCaptureValueValue());
    }
}
