package UsaLive.Administration.Devices;

import helper.Core;
import helper.OracleConnector;
import org.junit.Assert;
import org.junit.Test;
import pages.UsaLive.Administration.Devices.DeviceProfilePage;
import pages.UsaLive.Administration.Devices.DevicesPage;
import pages.UsaLive.LeftPanel;
import pages.UsaLive.LogInPage;
import pages.UsaLive.TopPanel;

/**
 * Created by ahachikyan on 3/2/2016.
 */
public class DeviceProfileTest extends Core {

    @Test
    public void deviceProfileEdit() throws Exception {
//        String s = " Atlantic Standard Time (GMT-04:00)".trim();
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();
//        LeftPanel leftPanel =  new LeftPanel(driver);
//        leftPanel.clickMenuItemByText("Devices");
        TopPanel topPanel = new TopPanel(driver);
        topPanel.general.devices.navigateTo();

        DevicesPage devicesPage = new DevicesPage(driver);
//        devicesPage.expandTreeRootNode();
//        String node1 = "Test Account Customer";
//        String node2 = "#1 USAT Prepaid";
//        devicesPage.expandTreeNode(node1);
//        devicesPage.expandTreeNode(node2);
//        String region1 = "Retired Devices";
//        String region2 = "Current Devices";
//        devicesPage.expandTreeNode(region2);
        String deviceId = "VJ000000021";
//        devicesPage.getDevice(deviceId).click();
        devicesPage.searchFor(deviceId);
        devicesPage.clickByDeviceIdLink(deviceId);

        DeviceProfilePage deviceProfilePage = new DeviceProfilePage(driver);


        String client1 = "client1";
        String client2 = "client2";
        String overrideDoingBusinessAs1 = "business1";
        String overrideDoingBusinessAs2 = "business2";
        String overrideCustomerServicePhone1 = "1111111111";
        String overrideCustomerServicePhone2 = "2222222222";
        String overrideCustomerServiceEmail1 = "email1@box.com";
        String overrideCustomerServiceEmail2 = "email2@box.com";
        String locationName1 = "location1";
        String locationName2 = "location2";
        String address1 = "address1";
        String address2 = "address2";
        String city1 = "Immaculata";
        String city2 = "Cap-Sante";
        String state1 = "PA";
        String state2 = "QC";
        String zipCode1 = "19345";
        String zipCode2 = "G0A 1L0";
        String country1 = "USA";
        String country2 = "Canada";
        String specificLocationAtThisAddress1 = "location1";
        String specificLocationAtThisAddress2 = "location2";
        String phoneNumberAtLocation1 = "1111111111";
        String phoneNumberAtLocation2 = "2222222222";
        String locationType1 = "Amusement";
        String locationType2 = "Apartment";
        String locationTimeZone1 = "Atlantic Standard Time (GMT-04:00)";
        String locationTimeZone2 = "Eastern Standard Time (GMT-05:00)";
        String machineAssetNumber1 = "number1";
        String machineAssetNumber2 = "number2";
//        String machinesMakeAndModel1 = "TBD - -";
//        String machinesMakeAndModel2 = "TBD - - TBD-TBD";
        String productType1 = "Coffee";
        String productType2 = "Dog Wash";
//        String owner1 = deviceProfilePage.getOwner().getText();
//        String bankAccountToPay1 = "National Bank of Trust - #2790426634 - Richie Riche";
//        String bankAccountToPay2 = "Laundry Customer Bank #2 - #123 - Laundry Customer, Inc. #2";
        String paymentSchedule1 = "As Accumulated";
        String paymentSchedule2 = "As Exported";
        String businessType1 = "Amusement";
        String businessType2 = "Car Wash";
        String primaryContact1 = "Arthur Royce";
        String primaryContact2 = "Arkadiy Hachikyan";
        String secondaryContact1 = "Arkadiy Hachikyan";
        String secondaryContact2 = "Arthur Royce";


        deviceProfilePage.setClient(client1);
        deviceProfilePage.setOverrideDoingBusinessAs(overrideDoingBusinessAs1);
        deviceProfilePage.setOverrideCustomerServicePhone(overrideCustomerServicePhone1);
        deviceProfilePage.setOverrideCustomerServiceEmail(overrideCustomerServiceEmail1);
//        deviceProfilePage.setRegion(region1);
        deviceProfilePage.setLocationName(locationName1);
        deviceProfilePage.setAddress(address1);
        deviceProfilePage.setCountry(country1);
        deviceProfilePage.setZipCode(zipCode1);
//        deviceProfilePage.setState(state1);
        deviceProfilePage.setSpecificLocationAtThisAddress(specificLocationAtThisAddress1);
        deviceProfilePage.setPhoneNumberAtLocation(phoneNumberAtLocation1);
        deviceProfilePage.setLocationType(locationType1);
        deviceProfilePage.setLocationTimeZone(locationTimeZone1);
        deviceProfilePage.setMachineAssetNumber(machineAssetNumber1);
//        deviceProfilePage.setMachinesMakeAndModel(machinesMakeAndModel1);
        deviceProfilePage.setProductType(productType1);
//        deviceProfilePage.setBankAccountToPay(bankAccountToPay1);
        deviceProfilePage.setPaymentSchedule(paymentSchedule1);
        deviceProfilePage.setBusinessType(businessType1);
        deviceProfilePage.setPrimaryContact(primaryContact1);
        deviceProfilePage.setSecondaryContact(secondaryContact1);

        deviceProfilePage.saveChanges();
        deviceProfilePage.verifySuccessfullyUpdatedMessage(deviceId, locationName1);
//        leftPanel.clickMenuItemByText("Devices");
        topPanel.general.devices.navigateTo();
//        devicesPage.expandTreeRootNode();
//        devicesPage.expandTreeNode(node1);
//        devicesPage.expandTreeNode(node2);
//        devicesPage.expandTreeNode(region1);
//        devicesPage.getDevice(deviceId).click();
        devicesPage.searchFor(deviceId);
        devicesPage.clickByDeviceIdLink(deviceId);


        Assert.assertEquals(client1, deviceProfilePage.getClient().getAttribute("value"));
        Assert.assertEquals(overrideDoingBusinessAs1, deviceProfilePage.getOverrideDoingBusinessAs().getAttribute("value"));
        Assert.assertEquals(overrideCustomerServicePhone1, deviceProfilePage.getOverrideCustomerServicePhone().getAttribute("value").replace("(", "").replace(")", "").replace("-", "").replace(" ", ""));
        Assert.assertEquals(overrideCustomerServiceEmail1, deviceProfilePage.getOverrideCustomerServiceEmail().getAttribute("value"));
//        Assert.assertEquals(region1, deviceProfilePage.getRegion().getText());
        Assert.assertEquals(locationName1, deviceProfilePage.getLocationName().getAttribute("value"));
        Assert.assertEquals(address1, deviceProfilePage.getAddress().getAttribute("value"));
        Assert.assertEquals(city1, deviceProfilePage.getCity().getAttribute("value"));
        Assert.assertEquals(state1, deviceProfilePage.getState().getAttribute("value"));
        Assert.assertEquals(zipCode1, deviceProfilePage.getZipCode().getAttribute("value").trim());
        Assert.assertEquals(country1, deviceProfilePage.getCountry().getText());
        Assert.assertEquals(specificLocationAtThisAddress1, deviceProfilePage.getSpecificLocationAtThisAddress().getAttribute("value"));
        Assert.assertEquals(phoneNumberAtLocation1, deviceProfilePage.getPhoneNumberAtLocation().getAttribute("value").replace("(", "").replace(")", "").replace("-", "").replace(" ", ""));
        Assert.assertEquals(locationType1, deviceProfilePage.getLocationType().getText());
        Assert.assertEquals(locationTimeZone1, deviceProfilePage.getLocationTimeZone().getText());
        Assert.assertEquals(machineAssetNumber1, deviceProfilePage.getMachineAssetNumber().getAttribute("value"));
//        Assert.assertTrue(, deviceProfilePage.getMachinesMakeAndModel().getText());
        Assert.assertEquals(productType1, deviceProfilePage.getProductType().getText());
//        Assert.assertEquals(bankAccountToPay1, deviceProfilePage.getBankAccountToPay().getText());
        Assert.assertEquals(paymentSchedule1, deviceProfilePage.getPaymentSchedule().getText());
        Assert.assertEquals(businessType1, deviceProfilePage.getBusinessType().getText());
        Assert.assertEquals(primaryContact1, deviceProfilePage.getPrimaryContact().getText());
        Assert.assertEquals(secondaryContact1, deviceProfilePage.getSecondaryContact().getText());


        deviceProfilePage.setClient(client2);
        deviceProfilePage.setOverrideDoingBusinessAs(overrideDoingBusinessAs2);
        deviceProfilePage.setOverrideCustomerServicePhone(overrideCustomerServicePhone2);
        deviceProfilePage.setOverrideCustomerServiceEmail(overrideCustomerServiceEmail2);
//        deviceProfilePage.setRegion(region2);
        deviceProfilePage.setLocationName(locationName2);
        deviceProfilePage.setAddress(address2);
        deviceProfilePage.setCountry(country2);
        deviceProfilePage.setZipCode(zipCode2);
//        deviceProfilePage.setState(state2);
//        deviceProfilePage.setCity(city2);
        deviceProfilePage.setSpecificLocationAtThisAddress(specificLocationAtThisAddress2);
        deviceProfilePage.setPhoneNumberAtLocation(phoneNumberAtLocation2);
        deviceProfilePage.setLocationType(locationType2);
        deviceProfilePage.setLocationTimeZone(locationTimeZone2);
        deviceProfilePage.setMachineAssetNumber(machineAssetNumber2);
//        deviceProfilePage.setMachinesMakeAndModel(machinesMakeAndModel2);
        deviceProfilePage.setProductType(productType2);
//        deviceProfilePage.setBankAccountToPay(bankAccountToPay2);
        deviceProfilePage.setPaymentSchedule(paymentSchedule2);
        deviceProfilePage.setBusinessType(businessType2);
        deviceProfilePage.setPrimaryContact(primaryContact2);
        deviceProfilePage.setSecondaryContact(secondaryContact2);

        deviceProfilePage.saveChanges();
        deviceProfilePage.verifySuccessfullyUpdatedMessage(deviceId, locationName2);
//        leftPanel.clickMenuItemByText("Devices");
        topPanel.general.devices.navigateTo();
//        devicesPage.expandTreeRootNode();
//        devicesPage.expandTreeNode(node1);
//        devicesPage.expandTreeNode(node2);
//        devicesPage.expandTreeNode(region2);
//        devicesPage.getDevice(deviceId).click();
        devicesPage.searchFor(deviceId);
        devicesPage.clickByDeviceIdLink(deviceId);

        Assert.assertEquals(client2, deviceProfilePage.getClient().getAttribute("value"));
        Assert.assertEquals(overrideDoingBusinessAs2, deviceProfilePage.getOverrideDoingBusinessAs().getAttribute("value"));
        Assert.assertEquals(overrideCustomerServicePhone2, deviceProfilePage.getOverrideCustomerServicePhone().getAttribute("value").replace("(", "").replace(")", "").replace("-", "").replace(" ", ""));
        Assert.assertEquals(overrideCustomerServiceEmail2, deviceProfilePage.getOverrideCustomerServiceEmail().getAttribute("value"));
//        Assert.assertEquals(region2, deviceProfilePage.getRegion().getText());
        Assert.assertEquals(locationName2, deviceProfilePage.getLocationName().getAttribute("value"));
        Assert.assertEquals(address2, deviceProfilePage.getAddress().getAttribute("value"));
        Assert.assertEquals(city2, deviceProfilePage.getCity().getAttribute("value"));
        Assert.assertEquals(state2, deviceProfilePage.getState().getAttribute("value"));
        Assert.assertEquals(zipCode2, deviceProfilePage.getZipCode().getAttribute("value").trim());
        Assert.assertEquals(country2, deviceProfilePage.getCountry().getText());
        Assert.assertEquals(specificLocationAtThisAddress2, deviceProfilePage.getSpecificLocationAtThisAddress().getAttribute("value"));
        Assert.assertEquals(phoneNumberAtLocation2, deviceProfilePage.getPhoneNumberAtLocation().getAttribute("value").replace("(", "").replace(")", "").replace("-", "").replace(" ", ""));
        Assert.assertEquals(locationType2, deviceProfilePage.getLocationType().getText());
        Assert.assertEquals(locationTimeZone2, deviceProfilePage.getLocationTimeZone().getText().trim());
        Assert.assertEquals(machineAssetNumber2, deviceProfilePage.getMachineAssetNumber().getAttribute("value"));
//        Assert.assertEquals(machinesMakeAndModel2, deviceProfilePage.getMachinesMakeAndModel().getText());
        Assert.assertEquals(productType2, deviceProfilePage.getProductType().getText());
//        Assert.assertEquals(bankAccountToPay2, deviceProfilePage.getBankAccountToPay().getText());
        Assert.assertEquals(paymentSchedule2, deviceProfilePage.getPaymentSchedule().getText());
        Assert.assertEquals(businessType2, deviceProfilePage.getBusinessType().getText());
        Assert.assertEquals(primaryContact2, deviceProfilePage.getPrimaryContact().getText());
        Assert.assertEquals(secondaryContact2, deviceProfilePage.getSecondaryContact().getText());
    }

    @Test
    public void USAT_330_USALive_Administration_Devices_QuickConnectInformationSection () throws Exception {

        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();
//        LeftPanel leftPanel = new LeftPanel(driver);
//        leftPanel.clickMenuItemByText("Devices");
        TopPanel topPanel = new TopPanel(driver);
        topPanel.general.devices.navigateTo();

        
        DevicesPage devicesPage = new DevicesPage(driver);

        OracleConnector oracleConnector = new OracleConnector(getEnvironment());
//        K3VS15108540
//        K3ACS00000985
//        K3ECTEST00001
//        K3SILKTEST001
//        String deviceId  = "K3ECTEST00001";
        String deviceId=      oracleConnector.getFirstValueFromQuery("select DEVICE_SERIAL_CD from DEVICE.DEVICE where DEVICE_SERIAL_CD like 'K3%' order by dbms_random.value");
        logger.info("DeviceId=" + deviceId);
        String userNameInDB  = oracleConnector.getFirstValueFromQuery("select  DI.NEW_USERNAME from DEVICE.CREDENTIAL C JOIN DEVICE.DEVICE D ON C.CREDENTIAL_ID = D.CREDENTIAL_ID LEFT OUTER JOIN DEVICE.DEVICE_INFO DI ON D.DEVICE_NAME = DI.DEVICE_NAME where DEVICE_SERIAL_CD='" + deviceId + "'");

        if(userNameInDB==null || userNameInDB.equals("") )
            userNameInDB  = oracleConnector.getFirstValueFromQuery("select  USERNAME from DEVICE.CREDENTIAL C JOIN DEVICE.DEVICE D ON C.CREDENTIAL_ID = D.CREDENTIAL_ID where DEVICE_SERIAL_CD='" + deviceId + "'");
        userNameInDB = userNameInDB.trim();

        logger.info("UserName in DB = " + userNameInDB);

        devicesPage.searchFor(deviceId);
        devicesPage.clickByDeviceIdLink(deviceId);

        DeviceProfilePage deviceProfilePage = new DeviceProfilePage(driver);
        String userNameInUI = deviceProfilePage.getUserNameText().trim();
        logger.info("UserName in UI = " + userNameInUI);
        Assert.assertEquals("User name in Quick Connect Information is not equal to User name in DB", userNameInDB, userNameInUI);


    }
}

