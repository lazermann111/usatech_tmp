package UsaLive.Administration.Devices;

import helper.Core;
import helper.OracleConnector;

import org.apache.commons.io.output.TaggedOutputStream;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import pages.UsaLive.Administration.Devices.DeviceProfilePage;
import pages.UsaLive.Administration.Devices.DevicesPage;
import pages.UsaLive.LeftPanel;
import pages.UsaLive.LogInPage;
import pages.UsaLive.TopPanel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by ahachikyan on 3/2/2016.
 */
public class EseyeLocationServicesTest extends Core {
    //Works only on INT env
//	Bug:     USAT-756 USALive. Administration. Devices. Network Information section for Eseye devices is empty
    @Test
    public void USAT_3_USALive_Administration_Devices_NetworkInformationSection_RefreshButtonIsAvailableNotEarlierThanEach24HoursAfterPreviousRefresh() throws Exception {
        if(getEnvironment().equalsIgnoreCase("int")) {
            OracleConnector oracleConnector = new OracleConnector(getEnvironment());
//            String queryToGetRandomEseyeDevice = "select dev.DEVICE_SERIAL_CD from device.device dev join report.eport re on dev.DEVICE_SERIAL_CD = re.EPORT_SERIAL_NUM join device.comm_method cm on dev.comm_method_cd = cm.comm_method_cd join (device.host h join device.host_type ht on h.host_type_id = ht.host_type_id) on dev.device_id = h.device_id and h.host_deactivation_ts is null and ht.host_type_desc like '%Eseye%' where cm.comm_provider_id = 3 and dev.device_active_yn_flag = 'Y' and REGEXP_LIKE(h.HOST_SERIAL_CD, cm.MODEM_ID_REGEX) and (re.NET_UPDATE_REQUESTED_TS is NULL) and (re.NET_UPDATED_TS is not NULL) and re.NET_LOCATION_ERROR_CD is null and length(h.host_serial_cd) = 19 order by dbms_random.value";
            String queryToGetRandomEseyeDevice = "select dev.DEVICE_SERIAL_CD from device.device dev join report.eport re on dev.DEVICE_SERIAL_CD = re.EPORT_SERIAL_NUM join device.comm_method cm on dev.comm_method_cd = cm.comm_method_cd join (device.host h join device.host_type ht on h.host_type_id = ht.host_type_id) on dev.device_id = h.device_id and h.host_deactivation_ts is null and ht.host_type_desc like '%Eseye%' where cm.comm_provider_id = 3 and dev.device_active_yn_flag = 'Y' and REGEXP_LIKE(h.HOST_SERIAL_CD, cm.MODEM_ID_REGEX) and length(h.host_serial_cd) = 19 order by dbms_random.value";
            String deviceSerial = oracleConnector.getFirstValueFromQuery(queryToGetRandomEseyeDevice);
            String cityInDB = getNET_CITYfromDB(oracleConnector, deviceSerial);
            String stateInDB = getNET_STATE_CDfromDB(oracleConnector, deviceSerial);
            String postalCodeInDB = getNET_POSTAL_CDfromDB(oracleConnector, deviceSerial);
            String latitudeInDB = getNET_LATITUDEfromDB(oracleConnector, deviceSerial);
            String longitudeInDB = getNET_LONGITUDEfromDB(oracleConnector, deviceSerial);
            openUsaLive();
            new LogInPage(driver).logInToUsaLiveAsPowerUser();
//            LeftPanel leftPanel = new LeftPanel(driver);
//            leftPanel.clickMenuItemByText("Devices");
            TopPanel topPanel = new TopPanel(driver);
            topPanel.general.devices.navigateTo();


            DevicesPage devicesPage = new DevicesPage(driver);
            devicesPage.searchFor(deviceSerial);
            devicesPage.getDevice(deviceSerial).click();
            DeviceProfilePage deviceProfilePage = new DeviceProfilePage(driver);
            String cityInitial = deviceProfilePage.getNetworkInformationCity();
            String cityCorrupted = "TestCity";
            String stateCorrupted = "TestState";
            String postalCodeCorrupted = "TestPostalCode";
            String latitudeCorrupted = "12.0";
            String longitudeCorrupted = "34.0";

            //Change city in DB
            String query = "update REPORT.EPORT set NET_CITY = '" + cityCorrupted + "' where EPORT_SERIAL_NUM='" + deviceSerial + "'";
            oracleConnector.executeUpdate(query);
            //Get current city from DB and verify with expected
//        String cityInDb = oracleConnector.getFirstValueFromQuery("select NET_CITY from REPORT.EPORT where EPORT_SERIAL_NUM='" + deviceSerial + "' order by NET_UPDATED_TS");
//        Assert.assertEquals(cityCorrupted, cityInDb);

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String dataDayBefore = dateFormat.format(new Date(System.currentTimeMillis() - 25 * 60 * 60 * 1000));

            //Set refresh date to 1 day ago
            oracleConnector.executeUpdate("update REPORT.EPORT set NET_UPDATED_TS = TO_DATE('" + dataDayBefore + "', 'YYYY-MM-DD HH24:MI:SS') where EPORT_SERIAL_NUM='" + deviceSerial + "'");
            //oracleConnector.executeUpdate("update REPORT.EPORT set LAST_UPDATED_TS = TO_DATE('" + dataDayBefore + "', 'YYYY-MM-DD HH24:MI:SS') where EPORT_SERIAL_NUM='" + deviceSerial + "'");

            //Get current city from UI and verify with expected
            deviceProfilePage.driver.navigate().refresh();
            Assert.assertEquals(cityCorrupted, deviceProfilePage.getNetworkInformationCity());

            deviceProfilePage.verifyRefreshButtonIsPresent();
            deviceProfilePage.clickRefreshButton();
            deviceProfilePage.waitForRequestHasBeenQueueedMessageInvivibility();
            //Get current city from UI and verify with expected
            Assert.assertEquals(cityInitial, deviceProfilePage.getNetworkInformationCity());

            deviceProfilePage.verifyRefreshButtonIsMissed();
            Assert.assertEquals(cityInDB, deviceProfilePage.getNetworkInformationCity());
            Assert.assertEquals(stateInDB, deviceProfilePage.getNetworkInformationState());
            Assert.assertEquals(postalCodeInDB, deviceProfilePage.getNetworkInformationZip());
            Assert.assertEquals(latitudeInDB + ", " + longitudeInDB, deviceProfilePage.getNetworkInformationGeoCoordinates());

            //Modify state
            oracleConnector.executeUpdate("update REPORT.EPORT set NET_STATE_CD = '" + stateCorrupted + "' where EPORT_SERIAL_NUM='" + deviceSerial + "'");
            oracleConnector.executeUpdate("update REPORT.EPORT set NET_UPDATED_TS = TO_DATE('" + dataDayBefore + "', 'YYYY-MM-DD HH24:MI:SS') where EPORT_SERIAL_NUM='" + deviceSerial + "'");
            deviceProfilePage.driver.navigate().refresh();
            Assert.assertEquals(stateCorrupted, deviceProfilePage.getNetworkInformationState());

            deviceProfilePage.verifyRefreshButtonIsPresent();
            deviceProfilePage.clickRefreshButton();
            deviceProfilePage.waitForRequestHasBeenQueueedMessageInvivibility();
            //Get current city from UI and verify with expected
            Assert.assertEquals(cityInitial, deviceProfilePage.getNetworkInformationCity());

            deviceProfilePage.verifyRefreshButtonIsMissed();
            Assert.assertEquals(cityInDB, deviceProfilePage.getNetworkInformationCity());
            Assert.assertEquals(stateInDB, deviceProfilePage.getNetworkInformationState());
            Assert.assertEquals(postalCodeInDB, deviceProfilePage.getNetworkInformationZip());
            Assert.assertEquals(latitudeInDB + ", " + longitudeInDB, deviceProfilePage.getNetworkInformationGeoCoordinates());

            //Modify Postal Code
            oracleConnector.executeUpdate("update REPORT.EPORT set NET_POSTAL_CD = '" + postalCodeCorrupted + "' where EPORT_SERIAL_NUM='" + deviceSerial + "'");
            oracleConnector.executeUpdate("update REPORT.EPORT set NET_UPDATED_TS = TO_DATE('" + dataDayBefore + "', 'YYYY-MM-DD HH24:MI:SS') where EPORT_SERIAL_NUM='" + deviceSerial + "'");
            deviceProfilePage.driver.navigate().refresh();
            Assert.assertEquals(postalCodeCorrupted, deviceProfilePage.getNetworkInformationZip());

            deviceProfilePage.verifyRefreshButtonIsPresent();
            deviceProfilePage.clickRefreshButton();
            deviceProfilePage.waitForRequestHasBeenQueueedMessageInvivibility();
            //Get current city from UI and verify with expected
            Assert.assertEquals(cityInitial, deviceProfilePage.getNetworkInformationCity());

            deviceProfilePage.verifyRefreshButtonIsMissed();
            Assert.assertEquals(cityInDB, deviceProfilePage.getNetworkInformationCity());
            Assert.assertEquals(stateInDB, deviceProfilePage.getNetworkInformationState());
            Assert.assertEquals(postalCodeInDB, deviceProfilePage.getNetworkInformationZip());
            Assert.assertEquals(latitudeInDB + ", " + longitudeInDB, deviceProfilePage.getNetworkInformationGeoCoordinates());

            //Modify Geo Coordinates
            oracleConnector.executeUpdate("update REPORT.EPORT set NET_LATITUDE = " + latitudeCorrupted + " where EPORT_SERIAL_NUM='" + deviceSerial + "'");
            oracleConnector.executeUpdate("update REPORT.EPORT set NET_LONGITUDE = " + longitudeCorrupted + " where EPORT_SERIAL_NUM='" + deviceSerial + "'");
            oracleConnector.executeUpdate("update REPORT.EPORT set NET_UPDATED_TS = TO_DATE('" + dataDayBefore + "', 'YYYY-MM-DD HH24:MI:SS') where EPORT_SERIAL_NUM='" + deviceSerial + "'");
            deviceProfilePage.driver.navigate().refresh();
            Assert.assertEquals(latitudeCorrupted + ", " + longitudeCorrupted, deviceProfilePage.getNetworkInformationGeoCoordinates());

            deviceProfilePage.verifyRefreshButtonIsPresent();
            deviceProfilePage.clickRefreshButton();
            deviceProfilePage.waitForRequestHasBeenQueueedMessageInvivibility();
            //Get current city from UI and verify with expected
            Assert.assertEquals(cityInitial, deviceProfilePage.getNetworkInformationCity());

            deviceProfilePage.verifyRefreshButtonIsMissed();
            Assert.assertEquals(cityInDB, deviceProfilePage.getNetworkInformationCity());
            Assert.assertEquals(stateInDB, deviceProfilePage.getNetworkInformationState());
            Assert.assertEquals(postalCodeInDB, deviceProfilePage.getNetworkInformationZip());
            Assert.assertEquals(latitudeInDB + ", " + longitudeInDB, deviceProfilePage.getNetworkInformationGeoCoordinates());
//        NET_LONGITUDE", null, getNET_LONGITUDEfromDB(oracleConnector, deviceSerial));
        }
    }
//
//    @Test
//    public void eseyeLocationServicesMassDevicesRefreshByDaemon() throws Exception {
////        1. Считаем кол-во исай устройств, считаем общее кол-во ошибок
////        2. Обнуляем всем устройствам NET данные для 410 есай устройств
////        3. Ждем 2 часа
////        4. Проверяем что гео данные обновились > 400 устройств или == колву есей устройств.
////        5. Проверяем что кол-во ошибок не болшье часть на первом шаге
//        OracleConnector oracleConnector = new OracleConnector(getEnvironment());
//        String queryCountOfDevicesWithErrorStatus = "select count(*) from REPORT.EPORT where NET_LOCATION_ERROR_CD is not null";
//        Integer countOfDevicesWithErrorStatus1  = Integer.parseInt(oracleConnector.getFirstValueFromQuery(queryCountOfDevicesWithErrorStatus));
//        logger.info("Count of Eseye devices with Error statuses before nulling net info = " + countOfDevicesWithErrorStatus1);
//
//        String queryCountOfDevicesWithNullNetInfo = "select count(*) from device.device dev join report.eport re on dev.DEVICE_SERIAL_CD = re.EPORT_SERIAL_NUM join device.comm_method cm on dev.comm_method_cd = cm.comm_method_cd join (device.host h join device.host_type ht on h.host_type_id = ht.host_type_id) on dev.device_id = h.device_id and h.host_deactivation_ts is null and ht.host_type_desc like '%Eseye%' where cm.comm_provider_id = 3 and dev.device_active_yn_flag = 'Y' and REGEXP_LIKE(h.HOST_SERIAL_CD, cm.MODEM_ID_REGEX) and (re.NET_UPDATE_REQUESTED_TS is NULL or SYSDATE - re.NET_UPDATE_REQUESTED_TS > 1) and (re.NET_UPDATED_TS is NULL or SYSDATE - re.NET_UPDATED_TS > 0) and length(h.host_serial_cd) = 19  and re.NET_UPDATED_TS is null and re.NET_CITY is null and re.NET_LATITUDE is null and re.NET_LONGITUDE is null";
//        Integer countOfDevicesWithNullNetInfo1  = Integer.parseInt(oracleConnector.getFirstValueFromQuery(queryCountOfDevicesWithNullNetInfo));
//        logger.info("Count of Eseye devices with null network information before nulling net info = " + countOfDevicesWithNullNetInfo1);
//
//        String querySetNullNetInfoForRandom410Devices = "update REPORT.EPORT  set NET_UPDATED_TS = null, NET_CITY=null, NET_STATE_CD=null, NET_POSTAL_CD=null, NET_LATITUDE=null, NET_LONGITUDE=null, NET_UPDATE_REQUESTED_TS = null, NET_LOCATION_ERROR_CD=null where EPORT_SERIAL_NUM in (select dev.DEVICE_SERIAL_CD device_iccid from device.device dev  join report.eport re on dev.DEVICE_SERIAL_CD = re.EPORT_SERIAL_NUM join device.comm_method cm on dev.comm_method_cd = cm.comm_method_cd join (device.host h join device.host_type ht on h.host_type_id = ht.host_type_id) on dev.device_id = h.device_id and h.host_deactivation_ts is null and ht.host_type_desc like '%Eseye%' where cm.comm_provider_id = 3 and dev.device_active_yn_flag = 'Y' and REGEXP_LIKE(h.HOST_SERIAL_CD, cm.MODEM_ID_REGEX) and (re.NET_UPDATE_REQUESTED_TS is NULL or SYSDATE - re.NET_UPDATE_REQUESTED_TS > 1) and (re.NET_UPDATED_TS is NULL or SYSDATE - re.NET_UPDATED_TS > 0) and length(h.host_serial_cd) = 19 and rownum < 411 )";
//        oracleConnector.executeQuery(querySetNullNetInfoForRandom410Devices);
//        logger.info("Count of Eseye devices with Error statuses right after nulling net info = " + oracleConnector.getFirstValueFromQuery(queryCountOfDevicesWithErrorStatus));
//        logger.info("Count of Eseye devices with null network information right after nulling net info = " + oracleConnector.getFirstValueFromQuery(queryCountOfDevicesWithNullNetInfo));
//        logger.info("Waiting 1-st hour...");
//
//        Thread.sleep(3600000);
//        logger.info("Count of Eseye devices with Error statuses after nulling net info after 1 hour = " + oracleConnector.getFirstValueFromQuery(queryCountOfDevicesWithErrorStatus));
//        logger.info("Count of Eseye devices with null network information after nulling net info after 1 hour = " + oracleConnector.getFirstValueFromQuery(queryCountOfDevicesWithNullNetInfo));
//        logger.info("Waiting 2-nd hour...");
//
//        Thread.sleep(3600000);
//        Thread.sleep(600000);//extra 10 minutes
//
//        Integer countOfDevicesWithErrorStatus2  = Integer.parseInt(oracleConnector.getFirstValueFromQuery(queryCountOfDevicesWithErrorStatus));
//        logger.info("Count of Eseye devices with Error statuses after nulling net info = " + countOfDevicesWithErrorStatus2);
//        Integer countOfDevicesWithNullNetInfo2  = Integer.parseInt(oracleConnector.getFirstValueFromQuery(queryCountOfDevicesWithNullNetInfo));
//        logger.info("Count of Eseye devices with null network information after nulling net info = " + countOfDevicesWithNullNetInfo2);
//
//        Assert.assertTrue("Count of Eseye devices with Error statuses has been increased (was:" + countOfDevicesWithErrorStatus1 + "become:" + countOfDevicesWithErrorStatus2 + ")", countOfDevicesWithErrorStatus2<countOfDevicesWithErrorStatus1);
//        Assert.assertTrue("Count of Eseye devices with null network information has been increased (was:" + countOfDevicesWithNullNetInfo1 + "become:" + countOfDevicesWithNullNetInfo1 + ")", countOfDevicesWithNullNetInfo2<countOfDevicesWithNullNetInfo1);
//    }
//   Works only on INT env
    @Test
    public void USAT_6_USALive_Administration_Devices_NetworkInformationSection_RefreshButtonIsVisibleForDevicesWithNullNetworkInformation() throws Exception
    {
        if(getEnvironment().equalsIgnoreCase("int")) {
            OracleConnector oracleConnector = new OracleConnector(getEnvironment());
            //String queryToGetRandomEseyeDevice = "select dev.DEVICE_SERIAL_CD from device.device dev join report.eport re on dev.DEVICE_SERIAL_CD = re.EPORT_SERIAL_NUM join device.comm_method cm on dev.comm_method_cd = cm.comm_method_cd join (device.host h join device.host_type ht on h.host_type_id = ht.host_type_id) on dev.device_id = h.device_id and h.host_deactivation_ts is null and ht.host_type_desc like '%Eseye%' where cm.comm_provider_id = 3 and dev.device_active_yn_flag = 'Y' and REGEXP_LIKE(h.HOST_SERIAL_CD, cm.MODEM_ID_REGEX) and (re.NET_UPDATE_REQUESTED_TS is NULL) and (re.NET_UPDATED_TS is not NULL) and length(h.host_serial_cd) = 19 order by dbms_random.value";
            String queryToGetRandomEseyeDevice = "select dev.DEVICE_SERIAL_CD from device.device dev join report.eport re on dev.DEVICE_SERIAL_CD = re.EPORT_SERIAL_NUM join device.comm_method cm on dev.comm_method_cd = cm.comm_method_cd join (device.host h join device.host_type ht on h.host_type_id = ht.host_type_id) on dev.device_id = h.device_id and h.host_deactivation_ts is null and ht.host_type_desc like '%Eseye%' where cm.comm_provider_id = 3 and dev.device_active_yn_flag = 'Y' and REGEXP_LIKE(h.HOST_SERIAL_CD, cm.MODEM_ID_REGEX) and NET_UPDATED_TS is not null and NET_CITY is not null and NET_STATE_CD is not null and NET_POSTAL_CD is not null and NET_LATITUDE is not null and NET_LONGITUDE is not null and NET_LOCATION_ERROR_CD is null and length(h.host_serial_cd) = 19 order by dbms_random.value";
            String deviceSerial = oracleConnector.getFirstValueFromQuery(queryToGetRandomEseyeDevice);

            String cityInitial = oracleConnector.getFirstValueFromQuery("select NET_CITY from REPORT.EPORT where EPORT_SERIAL_NUM = '" + deviceSerial + "'");
            String stateInitial = oracleConnector.getFirstValueFromQuery("select NET_STATE_CD from REPORT.EPORT where EPORT_SERIAL_NUM = '" + deviceSerial + "'");
            String postalInitial = oracleConnector.getFirstValueFromQuery("select NET_POSTAL_CD from REPORT.EPORT where EPORT_SERIAL_NUM = '" + deviceSerial + "'");
            String latitudeInitial = oracleConnector.getFirstValueFromQuery("select NET_LATITUDE from REPORT.EPORT where EPORT_SERIAL_NUM = '" + deviceSerial + "'");
            String longitudeInitial = oracleConnector.getFirstValueFromQuery("select NET_LONGITUDE from REPORT.EPORT where EPORT_SERIAL_NUM = '" + deviceSerial + "'");

            logger.info("cityInitial=" + cityInitial);
            logger.info("stateInitial=" + stateInitial);
            logger.info("postalInitial=" + postalInitial);
            logger.info("latitudeInitial and longitudeInitial =" + latitudeInitial + ", " + longitudeInitial);

            openUsaLive();
            new LogInPage(driver).logInToUsaLiveAsPowerUser();
//            LeftPanel leftPanel = new LeftPanel(driver);
//            leftPanel.clickMenuItemByText("Devices");
            
            TopPanel topPanel = new TopPanel(driver);
            topPanel.general.devices.navigateTo();   

            DevicesPage devicesPage = new DevicesPage(driver);
            devicesPage.searchFor(deviceSerial);
            devicesPage.getDevice(deviceSerial).click();

            String querySetNullNetInfoForDevice = "update REPORT.EPORT  set NET_UPDATED_TS = null, NET_CITY=null, NET_STATE_CD=null, NET_POSTAL_CD=null, NET_LATITUDE=null, NET_LONGITUDE=null, NET_UPDATE_REQUESTED_TS = null, NET_LOCATION_ERROR_CD=null where EPORT_SERIAL_NUM='" + deviceSerial + "'";
            oracleConnector.executeQuery(querySetNullNetInfoForDevice);

            DeviceProfilePage deviceProfilePage = new DeviceProfilePage(driver);
            deviceProfilePage.verifyDeviceSerialVisibility(deviceSerial);
            driver.navigate().refresh();
            deviceProfilePage.verifyRefreshButtonIsPresent();
            deviceProfilePage.clickRefreshButton();
            deviceProfilePage.waitForRequestHasBeenQueueedMessageInvivibility();
            deviceProfilePage.verifyRefreshButtonIsMissed();
            //Get current city from UI and verify with expected
            String cityRefreshed = deviceProfilePage.getNetworkInformationCity();
            String stateRefreshed = deviceProfilePage.getNetworkInformationState();
            String postalRefreshed = deviceProfilePage.getNetworkInformationZip();
            String geoCoordinatesRefreshed = deviceProfilePage.getNetworkInformationGeoCoordinates();

            logger.info("cityRefreshed=" + cityRefreshed);
            logger.info("stateRefreshed=" + stateRefreshed);
            logger.info("postalRefreshed=" + postalRefreshed);
            logger.info("latitudeRefreshed and longitudeRefreshed =" + geoCoordinatesRefreshed);

            Assert.assertEquals(cityInitial, cityRefreshed);
            Assert.assertEquals(stateInitial, stateRefreshed);
            Assert.assertEquals(postalInitial, postalRefreshed);
            Assert.assertEquals(latitudeInitial + ", " + longitudeInitial, geoCoordinatesRefreshed);
        }
    }


    // Works only on INT env because of permissions restriction
    //Temporary ignore
    @Ignore
    @Test
    public void USAT_313_USALive_Administration_Devices_NetworkInformationSection_MassDevicesAutomaticUpdateByDaemon() throws Exception {
        if(getEnvironment().equalsIgnoreCase("int")) {
            OracleConnector oracleConnector = new OracleConnector(getEnvironment());
//        String currentStatus2 = oracleConnector.getFirstValueFromQuery("select NET_LOCATION_ERROR_CD from REPORT.EPORT where EPORT_SERIAL_NUM='G8112168'");
            //1. Find 400 random eseye devices without error in location status
            String queryRandomEseyeDevicesWithoutErrorInLocationStatus = "select * from (select EPORT_SERIAL_NUM from REPORT.EPORT  where EPORT_SERIAL_NUM in (select dev.DEVICE_SERIAL_CD device_iccid from device.device dev  join report.eport re on dev.DEVICE_SERIAL_CD = re.EPORT_SERIAL_NUM join device.comm_method cm on dev.comm_method_cd = cm.comm_method_cd join (device.host h join device.host_type ht on h.host_type_id = ht.host_type_id) on dev.device_id = h.device_id and h.host_deactivation_ts is null and ht.host_type_desc like '%Eseye%' where cm.comm_provider_id = 3 and dev.device_active_yn_flag = 'Y' and REGEXP_LIKE(h.HOST_SERIAL_CD, cm.MODEM_ID_REGEX) and (re.NET_UPDATE_REQUESTED_TS is NULL) and (re.NET_UPDATED_TS is not NULL) and length(h.host_serial_cd) = 19  and re.NET_LOCATION_ERROR_CD is null )  order by dbms_random.value) where rownum < 400";
            ArrayList<String> randomEseyeDevicesWithoutErrorInLocationStatus = oracleConnector.getArrayFromResultSet(queryRandomEseyeDevicesWithoutErrorInLocationStatus);
            String randomEseyeDeviceWithoutErrorInLocationStatus = "";
            for (int x = 0; x < randomEseyeDevicesWithoutErrorInLocationStatus.size(); x++) {
                {
                    //2. Set to null net info for these 400 devices
                    randomEseyeDeviceWithoutErrorInLocationStatus = randomEseyeDevicesWithoutErrorInLocationStatus.get(x);
                    logger.info("Processing device = " + randomEseyeDeviceWithoutErrorInLocationStatus);
                    int updatedRowsCount = oracleConnector.executeUpdate("update REPORT.EPORT  set NET_UPDATED_TS = null, NET_CITY=null, NET_STATE_CD=null, NET_POSTAL_CD=null, NET_LATITUDE=null, NET_LONGITUDE=null, NET_UPDATE_REQUESTED_TS = null, NET_LOCATION_ERROR_CD=null where EPORT_SERIAL_NUM='" + randomEseyeDeviceWithoutErrorInLocationStatus + "'");
                    Assert.assertEquals(1, updatedRowsCount);
                }
            }
            logger.info("Processing device = " + randomEseyeDeviceWithoutErrorInLocationStatus);
            //3. Wait 3 hours
            logger.info("Waiting 3 hours...");
            Thread.sleep(7200000);
            Thread.sleep(3600000);//1 hour
//        Thread.sleep(600000);//10 minutes
            logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            for (int x = 0; x < randomEseyeDevicesWithoutErrorInLocationStatus.size(); x++) {
                {

                    String deviceSerial = randomEseyeDevicesWithoutErrorInLocationStatus.get(x);

                    //4. Verify error missing in each of these these 400.  Log error devices
                    String currentErrorStatus = oracleConnector.getFirstValueFromQuery("select NET_LOCATION_ERROR_CD from REPORT.EPORT where EPORT_SERIAL_NUM='" + deviceSerial + "'");

                    logger.info("-----------------");
                    if (currentErrorStatus != null)
                        logger.info("Device " + deviceSerial + " has NET_LOCATION_ERROR_CD =" + currentErrorStatus);
                    if (getNET_UPDATED_TSfromDB(oracleConnector, deviceSerial) == null)
                        logger.info("Device " + deviceSerial + " has null NET_UPDATED_TS");
                    if (getNET_CITYfromDB(oracleConnector, deviceSerial) == null)
                        logger.info("Device " + deviceSerial + " has null NET_CITY");
                    if (getNET_STATE_CDfromDB(oracleConnector, deviceSerial) == null)
                        logger.info("Device " + deviceSerial + " has null NET_STATE_CD");
                    if (getNET_POSTAL_CDfromDB(oracleConnector, deviceSerial) == null)
                        logger.info("Device " + deviceSerial + " has null NET_POSTAL_CD");
                    if (getNET_LATITUDEfromDB(oracleConnector, deviceSerial) == null)
                        logger.info("Device " + deviceSerial + " has null NET_LATITUDE");
                    if (getNET_LONGITUDEfromDB(oracleConnector, deviceSerial) == null)
                        logger.info("Device " + deviceSerial + " has null NET_LONGITUDE");

                    ////TODO: This workaround should be removed
                    if (currentErrorStatus != null && currentErrorStatus.equals("606")) {
                        logger.info("Error status of" + deviceSerial + " is " + currentErrorStatus);
                    } else {
                        Assert.assertEquals("Device " + deviceSerial + " has NET_LOCATION_ERROR_CD =" + currentErrorStatus, null, currentErrorStatus);

                        //5. Verify null-net info of each of these these 400.  Log error devices
                        Assert.assertNotEquals("Device " + deviceSerial + " has null NET_UPDATED_TS", null, getNET_UPDATED_TSfromDB(oracleConnector, deviceSerial));
                        Assert.assertNotEquals("Device " + deviceSerial + " has null NET_CITY", null, getNET_CITYfromDB(oracleConnector, deviceSerial));
                        Assert.assertNotEquals("Device " + deviceSerial + " has null NET_STATE_CD", null, getNET_STATE_CDfromDB(oracleConnector, deviceSerial));
                        Assert.assertNotEquals("Device " + deviceSerial + " has null NET_POSTAL_CD", null, getNET_POSTAL_CDfromDB(oracleConnector, deviceSerial));
                        Assert.assertNotEquals("Device " + deviceSerial + " has null NET_LATITUDE", null, getNET_LATITUDEfromDB(oracleConnector, deviceSerial));
                        Assert.assertNotEquals("Device " + deviceSerial + " has null NET_LONGITUDE", null, getNET_LONGITUDEfromDB(oracleConnector, deviceSerial));
                    }
                }
            }
        }
    }

    private String getNET_UPDATED_TSfromDB(OracleConnector oracleConnector, String deviceSerial) throws Exception {
        return oracleConnector.getFirstValueFromQuery("select NET_UPDATED_TS   from REPORT.EPORT where EPORT_SERIAL_NUM='" + deviceSerial + "'");
    }
    private String getNET_CITYfromDB(OracleConnector oracleConnector, String deviceSerial) throws Exception {
        return oracleConnector.getFirstValueFromQuery("select NET_CITY   from REPORT.EPORT where EPORT_SERIAL_NUM='" + deviceSerial + "'");
    }
    private String getNET_STATE_CDfromDB(OracleConnector oracleConnector, String deviceSerial) throws Exception {
        return oracleConnector.getFirstValueFromQuery("select NET_STATE_CD   from REPORT.EPORT where EPORT_SERIAL_NUM='" + deviceSerial + "'");
    }
    private String getNET_POSTAL_CDfromDB(OracleConnector oracleConnector, String deviceSerial) throws Exception {
        return oracleConnector.getFirstValueFromQuery("select NET_POSTAL_CD   from REPORT.EPORT where EPORT_SERIAL_NUM='" + deviceSerial + "'");
    }
    private String getNET_LATITUDEfromDB(OracleConnector oracleConnector, String deviceSerial) throws Exception {
        return oracleConnector.getFirstValueFromQuery("select NET_LATITUDE   from REPORT.EPORT where EPORT_SERIAL_NUM='" + deviceSerial + "'");
    }
    private String getNET_LONGITUDEfromDB(OracleConnector oracleConnector, String deviceSerial) throws Exception {
        return oracleConnector.getFirstValueFromQuery("select NET_LONGITUDE   from REPORT.EPORT where EPORT_SERIAL_NUM='" + deviceSerial + "'");
    }
}