package UsaLive.Administration.Devices;

import helper.Core;
import org.junit.Test;
import pages.UsaLive.Administration.Devices.DevicesPage;
import pages.UsaLive.Administration.Devices.ManageRegionsPage;
import pages.UsaLive.LeftPanel;
import pages.UsaLive.LogInPage;
import pages.UsaLive.TopPanel;

/**
 * Created by ahachikyan on 3/2/2016.
 */
public class RegionsTest extends Core {

    @Test
    public void editRegionName() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();
//        LeftPanel leftPanel =  new LeftPanel(driver);
//        leftPanel.clickMenuItemByText("Devices");
        TopPanel topPanel = new TopPanel(driver);
        topPanel.general.devices.navigateTo();
        

        String customerName = "Customer #10015";
        String regionName1 = "Aramatic accts.";
        if(getEnvironment().equalsIgnoreCase("int"))
        {
            customerName = "USA Technologies Test Acct #1";
            regionName1 = "Such N Stuff";
        }
        String regionName2 = regionName1 + "2";
        DevicesPage devicesPage = new DevicesPage(driver);
        devicesPage.expandTreeRootNode();
        devicesPage.expandTreeNode(customerName);
        devicesPage.editRegionName(regionName1, regionName2);
        devicesPage.editRegionName(regionName2, regionName1);
    }



    @Test
    public void editRegionDetails() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();
//        LeftPanel leftPanel =  new LeftPanel(driver);
//        leftPanel.clickMenuItemByText("Devices");
        TopPanel topPanel = new TopPanel(driver);
        topPanel.general.devices.navigateTo();

        DevicesPage devicesPage = new DevicesPage(driver);
        devicesPage.expandTreeRootNode();
        String customerName = "Customer #10015";
        String regionName1 = "Aramatic accts.";
        if(getEnvironment().equalsIgnoreCase("int"))
        {
            customerName = "USA Technologies Test Acct #1";
            regionName1 = "Such N Stuff";
        }
        String regionName2 = regionName1 + "2";
        devicesPage.expandTreeNode(customerName);
//        String regionNameBefore = "Aramatic accts.";
//        String regionNameAfter = "Aramatic accts.2";
        devicesPage.editRegionDetails(regionName1);

        ManageRegionsPage manageRegionsPage= new ManageRegionsPage(driver);
        manageRegionsPage.setRegionName("");
        manageRegionsPage.save();
        manageRegionsPage.verifyRequiredFieldWarningVisibility();

        manageRegionsPage.setRegionName(regionName2);
        String regionBeforeChange = "-- None --";
        String regionAfterChange = manageRegionsPage.setAnyParentRegion();
        manageRegionsPage.save();
        manageRegionsPage.verifySuccessMessage(regionName2);


//        leftPanel.clickMenuItemByText("Devices");
        topPanel.general.devices.navigateTo();

        devicesPage.expandTreeRootNode();
        devicesPage.expandTreeNode(customerName);
        devicesPage.expandTreeNode(regionAfterChange);
        devicesPage.editRegionDetails(regionName2);
        manageRegionsPage.setParentRegion(regionBeforeChange);


        manageRegionsPage.setRegionName(regionName1);
        manageRegionsPage.save();
        manageRegionsPage.verifySuccessMessage(regionName1);
    }

}

