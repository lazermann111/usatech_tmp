package UsaLive.Administration.Devices;

import helper.Core;
import org.junit.Test;
import pages.UsaLive.Administration.Devices.DevicesPage;
import pages.UsaLive.LeftPanel;
import pages.UsaLive.LogInPage;
import pages.UsaLive.TopPanel;

/**
 * Created by ahachikyan on 3/2/2016.
 */
public class TreeTest extends Core {

    @Test
    public void USAT_113_USALive_Administration_Devices_TreeView() throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();
	// LeftPanel leftPanel = new LeftPanel(driver);
	// leftPanel.clickMenuItemByText("Devices");
	TopPanel topPanel = new TopPanel(driver);
	topPanel.general.devices.navigateTo();

	DevicesPage devicesPage = new DevicesPage(driver);
	devicesPage.verifyTreeElementInvisibility("Boom Vending Inc.");
	devicesPage.expandTreeRootNode();
	devicesPage.verifyTreeElementVisibility("Boom Vending Inc.");
	devicesPage.collapseTreeRootNode();
	devicesPage.verifyTreeElementInvisibility("Boom Vending Inc.");

	// String device = "V1-0-USD";
	// devicesPage.searchFor(device);
	// devicesPage.getDevice("V1-0-USD");
    }

    @Test
    public void USAT_114_USALive_Administration_Devices_Region() throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();
	// LeftPanel leftPanel = new LeftPanel(driver);
	// leftPanel.clickMenuItemByText("Devices");
	TopPanel topPanel = new TopPanel(driver);
	topPanel.general.devices.navigateTo();

	DevicesPage devicesPage = new DevicesPage(driver);
	
	devicesPage.expandTreeRootNode();
	devicesPage.verifyTreeElementVisibility("Boom Vending Inc.");
	devicesPage.expandTreeNode("Boom Vending Inc.");
	devicesPage.expandTreeNode("<NO REGION>");
	devicesPage.sortByDevice();
	devicesPage.verifySortingByDevice("<NO REGION>");
	devicesPage.sortByLocation();
	devicesPage.verifySortingByLocation("<NO REGION>");

	// String device = "V1-0-USD";
	// devicesPage.searchFor(device);
	// devicesPage.getDevice("V1-0-USD");
    }

}
