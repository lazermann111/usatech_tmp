package UsaLive.Administration.Devices;

import helper.Core;

import org.junit.Ignore;
import org.junit.Test;
import pages.UsaLive.Administration.Devices.DevicesPage;
import pages.UsaLive.LeftPanel;
import pages.UsaLive.LogInPage;
import pages.UsaLive.TopPanel;

/**
 * Created by ahachikyan on 3/2/2016.
 */
public class SearchTest extends Core {

    @Test
    public void searchDevice() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();
//        LeftPanel leftPanel =  new LeftPanel(driver);
//        leftPanel.clickMenuItemByText("Devices");
        TopPanel topPanel = new TopPanel(driver);
        topPanel.general.devices.navigateTo();

        DevicesPage devicesPage = new DevicesPage(driver);

        String device = "V1-0-USD";
        devicesPage.searchFor(device);
        devicesPage.getDevice("V1-0-USD");
    }


    @Test
    @Ignore
    public void searchLocation() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();
//        LeftPanel leftPanel =  new LeftPanel(driver);
//        leftPanel.clickMenuItemByText("Devices");
        TopPanel topPanel = new TopPanel(driver);
        topPanel.general.devices.navigateTo();
        
        DevicesPage devicesPage = new DevicesPage(driver);

        String location = "USA Technologies";
        devicesPage.searchFor(location);
        devicesPage.verifySearchedLocationPresenceInResults(location);
    }

    @Test
    @Ignore
    public void searchNegative() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();
//        LeftPanel leftPanel =  new LeftPanel(driver);
//        leftPanel.clickMenuItemByText("Devices");

        TopPanel topPanel = new TopPanel(driver);
        topPanel.general.devices.navigateTo();
        
        DevicesPage devicesPage = new DevicesPage(driver);

        devicesPage.searchFor("sdg234sdffg4");

    }


}

