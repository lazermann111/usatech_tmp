package UsaLive.Administration;

import helper.Core;
import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebElement;
import pages.UsaLive.Administration.RegisteredReportsPage;
import pages.UsaLive.Administration.Users.UsersPage;
import pages.UsaLive.General.HomePage;
import pages.UsaLive.LeftPanel;
import pages.UsaLive.LogInPage;
import pages.UsaLive.TopPanel;
import pages.UsaLive.Administration.CustomersPage;

import java.util.ArrayList;
import java.util.List;

public class CustomersTests extends Core {
    @Test
    public void USAT_142_USAT_143_USAT_144_USALive_Administration_Customers_SearchAndLoginAsAndCustomerInformation()
	    throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

	// LeftPanel leftPanel = new LeftPanel(driver);
	// leftPanel.clickMenuItemByText("Customers");
	TopPanel topPanel = new TopPanel(driver);
	topPanel.general.customers.navigateTo();;

	CustomersPage customersPage = new CustomersPage(driver);
	String customerName = "Boom Vending Inc.";
	String primaryContactMemberID = "BoomVend";
	String primaryContactName = "Mark Whitelock";
	String primaryContactEmail = "mwhitelock@usatech.com";
	String licenseType = "Standard";
	String userFirstName = "Mark";

	customersPage.searchByCustomerName(customerName);
	// SALES USA Technologies (E-Port Sales)
	customersPage.clickUserByNameContains(customerName + " (" + primaryContactName + ")");

	customersPage.verifyTextboxTextByLabel("Customer Name", customerName);
	customersPage.verifyTextboxTextByLabel("Primary Contact Member ID", primaryContactMemberID);
	customersPage.verifyTextboxTextByLabel("Primary Contact Name", primaryContactName);
	customersPage.verifyTextboxTextByLabel("Primary Contact Email", primaryContactEmail);
	customersPage.verifyTextboxTextByLabel("License Type", licenseType);

	customersPage.clickAddBestVendorReportingButton();
	customersPage.verifyAddBestVendorReportingMessage();
	customersPage.clickRemoveBestVendorReportingButton();
	customersPage.verifyRemoveBestVendorReportingMessage();

	customersPage.clickLoginAsButton();

	new HomePage(driver).verifyTitle(userFirstName);

    }

    // @Test
    // public void
    // USAT_321_USALive_Administration_Customers_ViewCustomersUserReports()
    // {
    // openUsaLive();
    // //1. Log in as admin
    // new LogInPage(driver).logInAsPowerUser();
    // LeftPanel leftPanel = new LeftPanel(driver);
    // //2. Open Administration->Customers
    // leftPanel.clickMenuItemByText("Customers");
    // CustomersPage customersPage = new CustomersPage(driver);
    // //3. Search all
    // //TODO: set to ""
    // String randomCustomerName="ABC Company Inc";
    // customersPage.searchByCustomerName(randomCustomerName);
    // customersPage.clickUserByNameContains(randomCustomerName);
    // customersPage.clickLoginAsButton();
    //
    //
    // //4. Remember random customer. Log in as this customer
    //// WebElement randomCustomerWebElement =
    // customersPage.getRandomCustomerByNameFromList();
    //// String randomCustomerName= randomCustomerWebElement.getText();
    //// randomCustomerName = randomCustomerName.substring(0,
    // randomCustomerName.indexOf("("));
    //// randomCustomerWebElement.click();
    //// customersPage.clickLoginAsButton();
    // //5. As a customer remember all saved reports in Administration->Report
    // Register->Reports (note that list could be empty)
    // leftPanel.clickMenuItemByText("Report Register");
    // RegisteredReportsPage registeredReportsPage = new
    // RegisteredReportsPage(driver);
    // List<String> allReportText = registeredReportsPage.getAllReportsText();
    // //6. General->Login To Original
    // leftPanel.clickMenuItemByText("Login To Original");
    // //7. Open Administration->Customers
    // leftPanel.clickMenuItemByText("Customers");
    // //8. Search the customer
    // customersPage.searchByCustomerName(randomCustomerName);
    // //9. Click on thin customer in the list
    // customersPage.clickUserByNameContains(randomCustomerName);
    // //10. Click "View Customer's User Reports" link
    // customersPage.clickViewCustomersUserReportsLink();
    // //11. Verify that User Reports list corresponds to previously remembered
    // list of saved reports
    // List<String> allReportNamesText = customersPage.getAllReportNamesText();
    // List<String> allTransportNamesText =
    // customersPage.getAllTransportNamesText();
    // List<String> allReportNamesWithTransportNames = new ArrayList<String>();
    // for (Integer i=0; i<allTransportNamesText.size(); i++) {
    // allReportNamesWithTransportNames.add(allReportNamesText.get(i) + " (" +
    // allTransportNamesText.get(i) + ")");
    // }
    // Assert.assertEquals(allReportNamesWithTransportNames, allReportText);
    // }
    @Test
    // TODO: Skip for ecc. In ecc there are a lot of users without reports at
    // all so it could take a lot of time
    public void USAT_321_USALive_Administration_Customers_ViewCustomersUserReports() {
	// In ecc there are a lot of users without reports at all so it could
	// take a lot of time
	openUsaLive();
	// 1. Log in as admin
	new LogInPage(driver).logInToUsaLiveAsPowerUser();
	// LeftPanel leftPanel = new LeftPanel(driver);
	TopPanel topPanel = new TopPanel(driver);
	// 2. Open Administration->Customers
	// leftPanel.clickMenuItemByText("Customers");
	topPanel.general.customers.navigateTo();
	CustomersPage customersPage = new CustomersPage(driver);
	// 3. Search all

	// String randomCustomerName="";
	// customersPage.searchByCustomerName("");
	String randomCustomerName = "";
	String userName = "";
	// customersPage.clickUserByNameContains(randomCustomerName);
	// customersPage.clickLoginAsButton();

	// 4. Remember random customer. Log in as this customer
	customersPage.searchByCustomerName("test");
	WebElement randomCustomerWebElement = customersPage.getRandomCustomerByNameFromList();
	randomCustomerWebElement.click();
	customersPage.clickViewCustomersUserReportsLink();
	List<String> allReportNamesText = customersPage.getAllReportNamesText();
	while (allReportNamesText.size() == 0 || userName.equals("")) {
	    // leftPanel.clickMenuItemByText("Customers");
	    topPanel.general.customers.navigateTo();
	    customersPage.searchByCustomerName("test");
	    randomCustomerWebElement = customersPage.getRandomCustomerByNameFromList();
	    randomCustomerName = randomCustomerWebElement.getText();
	    randomCustomerWebElement.click();
	    userName = customersPage.getPrimaryContactMemberIdText();
	    logger.info("UserName=" + userName);
	    if (userName.equals("test-gh"))
		logger.info("UserName=" + userName);
	    customersPage.clickViewCustomersUserReportsLink();
	    allReportNamesText = customersPage.getAllReportNamesText();
	}
	logger.info("UserNameFinal=" + userName);
	allReportNamesText = customersPage.getAllReportNamesText(userName);
	logger.info("allReportNamesText=" + allReportNamesText);

	logger.info("UserName=" + randomCustomerName);
	// leftPanel.clickMenuItemByText("Customers");
	// customersPage.searchByCustomerName("");
	// customersPage.clickUserByNameContains(randomCustomerName);
	// customersPage.clickLoginAsButton();
	// leftPanel.clickMenuItemByText("Users");
	topPanel.general.users.navigateTo();
	UsersPage usersPage = new UsersPage(driver);
	usersPage.searchUserByName(userName);
	usersPage.clickUserItemInList(userName);
	usersPage.clickLogInAsButton();
	// customersPage.clickUserByNameContains(randomCustomerName);
	// customersPage.clickLoginAsButton();

	// randomCustomerName = randomCustomerName.substring(0,
	// randomCustomerName.indexOf("("));
	// randomCustomerWebElement.click();

	// 5. As a customer remember all saved reports in Administration->Report
	// Register->Reports (note that list could be empty)
	// leftPanel.clickMenuItemByText("Report Register");
	topPanel.reports.reportRegister.navigateTo();
	RegisteredReportsPage registeredReportsPage = new RegisteredReportsPage(driver);
	List<String> allReportText = registeredReportsPage.getAllReportsText();
	// 6. General->Login To Original
	// leftPanel.clickMenuItemByText("Login To Original");
	topPanel.general.loginToOriginal.navigateTo();
	// 7. Open Administration->Customers
	// leftPanel.clickMenuItemByText("Customers");
	topPanel.general.customers.navigateTo();;
	// 8. Search the customer
	customersPage.searchByCustomerName("");
	// customersPage.clickUserByNameContains(randomCustomerName);
	// customersPage.searchByCustomerName(randomCustomerName);
	// 9. Click on thin customer in the list
	customersPage.clickUserByNameContains(randomCustomerName);
	// 10. Click "View Customer's User Reports" link
	customersPage.clickViewCustomersUserReportsLink();
	// 11. Verify that User Reports list corresponds to previously
	// remembered list of saved reports
	allReportNamesText = customersPage.getAllReportNamesText(userName);
	List<String> allTransportNamesText = customersPage.getAllTransportNamesText(userName);
	List<String> allReportNamesWithTransportNames = new ArrayList<String>();
	for (Integer i = 0; i < allTransportNamesText.size(); i++) {
	    allReportNamesWithTransportNames.add(allReportNamesText.get(i) + " (" + allTransportNamesText.get(i));
	}
	Assert.assertEquals("Count of reports is different. /n/t Expected: " + allReportNamesWithTransportNames
		+ "/n/t Actual:" + allReportText, allReportNamesWithTransportNames.size(), allReportText.size());

	for (Integer i = 0; i < allReportNamesWithTransportNames.size(); i++) {
	    Assert.assertTrue(
		    "Report '" + allReportNamesWithTransportNames.get(i) + "' is missed. /n/t Expected: "
			    + allReportNamesWithTransportNames + "/n/t Actual:" + allReportText,
		    allReportText.contains(allReportNamesWithTransportNames.get(i)));
	}
    }
}
