package UsaLive.Administration;

import helper.Core;
import org.junit.Test;
import pages.UsaLive.Administration.CustomersPage;
import pages.UsaLive.Administration.PreferencesAndSetupPage;
import pages.UsaLive.General.HomePage;
import pages.UsaLive.LeftPanel;
import pages.UsaLive.LogInPage;
import pages.UsaLive.TopPanel;
import pages.UsaLive.Reports.ReportsPage;
import pages.UsaLive.Reports.SavedReports.SavedReportsPage;

public class PreferencesAndSetupTests extends Core
{
    @Test
    public void USAT_145_USALive_Administration_PreferencesAndSetup() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();

//        LeftPanel leftPanel =  new LeftPanel(driver);
//        leftPanel.clickMenuItemByText("Preferences and Setup");
        
        TopPanel topPanel = new TopPanel(driver);
        topPanel.configuration.preferences.navigateTo();


        PreferencesAndSetupPage preferencesAndSetupPage = new PreferencesAndSetupPage(driver);
        preferencesAndSetupPage.setFirstComboboxValue("Terminal");
        preferencesAndSetupPage.setSecondComboboxValue("Customer");
        preferencesAndSetupPage.setThirdComboboxValue("Client");
        preferencesAndSetupPage.clickSaveChanges();
        preferencesAndSetupPage.verifySuccessMessage();

//        leftPanel.clickMenuItemByText("Saved Reports");
        topPanel.reports.savedReports.navigateTo();
        SavedReportsPage savedReportsPage = new SavedReportsPage(driver);
        savedReportsPage.clickDailyOperationsTab();
        savedReportsPage.clickReportLink("Busiest Times");
        ReportsPage reportsPage = new ReportsPage(driver);
        reportsPage.verifyLabelPresence("Terminal");
        reportsPage.verifyLabelPresence("Customer");
        reportsPage.verifyLabelPresence("Client");


//        leftPanel.clickMenuItemByText("Preferences and Setup");
        topPanel.configuration.preferences.navigateTo();

        preferencesAndSetupPage.setFirstComboboxValue("Client");
        preferencesAndSetupPage.setSecondComboboxValue("Location");
        preferencesAndSetupPage.setThirdComboboxValue("Device");
        preferencesAndSetupPage.clickSaveChanges();
        preferencesAndSetupPage.verifySuccessMessage();

//        leftPanel.clickMenuItemByText("Saved Reports");
        topPanel.reports.savedReports.navigateTo();
        savedReportsPage.clickDailyOperationsTab();
        savedReportsPage.clickReportLink("Busiest Times");
        reportsPage.verifyLabelPresence("Client");
        reportsPage.verifyLabelPresence("Location");
    }

    @Test
    public void requiredFields() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsPowerUser();

//        LeftPanel leftPanel =  new LeftPanel(driver);
//        leftPanel.clickMenuItemByText("Preferences and Setup");

        TopPanel topPanel = new TopPanel(driver);
        topPanel.configuration.preferences.navigateTo();

        PreferencesAndSetupPage preferencesAndSetupPage = new PreferencesAndSetupPage(driver);
        preferencesAndSetupPage.verifyFirstComboboxAvailableValueAbsence("-- None --");
    }

}
