package UsaLive.Administration.Users;

import helper.Core;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import pages.Dms.Customers.CustomerListPage;
import pages.UsaLive.Administration.Users.UsersPage;
import pages.UsaLive.General.HomePage;
import pages.UsaLive.LeftPanel;
import pages.UsaLive.LogInPage;
import pages.UsaLive.TopPanel;

import static org.junit.Assert.assertTrue;

import java.util.Random;

/**
 * Created by ahachikyan on 3/2/2016.
 */
public class UsersTest extends Core {

    @Test
    @Ignore
    public void USAT_136_USAT_137_USAT_138_USAT_139() throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();
	// LeftPanel leftPanel = new LeftPanel(driver);
	// leftPanel.clickMenuItemByText("Users");

	TopPanel topPanel = new TopPanel(driver);
	topPanel.general.users.navigateTo();

	Integer randomPart = new Random().ints(1, 0, 1000000)
					 .findFirst()
					 .getAsInt();
	String userName = "autotestUser" + randomPart;
	String firstName = "autotestFirstName";
	String lastName = "autotestLastName";
	String password = "!@#QWEasdzxc";
	String telephone = "1234567890";
	String email = "usalive.test@gmail.com";

	UsersPage usersPage = new UsersPage(driver);

	usersPage.deleteUserByName(userName);

	usersPage.clickAddUserButton();
	usersPage.setUserName(userName);
	usersPage.setFirstName(firstName);
	usersPage.setLastName(lastName);
	usersPage.setPassword(password);
	usersPage.setConfirmPassword(password);
	usersPage.setEmail(email);
	usersPage.setTelephone(telephone);

	usersPage.checkComboBoxByLabel("Admin Refund");
	usersPage.checkComboBoxByLabel("Administer All Customer Users");
	usersPage.checkComboBoxByLabel("Administer All Users");
	usersPage.checkComboBoxByLabel("Appove EFTs");
	usersPage.checkComboBoxByLabel("Bank Account Creation");
	usersPage.checkComboBoxByLabel("Column Mappings");
	usersPage.checkComboBoxByLabel("Configure Devices");
	usersPage.checkComboBoxByLabel("Customer Service");
	usersPage.checkComboBoxByLabel("Device Activation");
	usersPage.checkComboBoxByLabel("Edit All Devices");
	usersPage.checkComboBoxByLabel("ePort Online");
	usersPage.checkComboBoxByLabel("Folio Admin");
	usersPage.checkComboBoxByLabel("Folio Design");
	usersPage.checkComboBoxByLabel("Issue Chargeback");
	usersPage.checkComboBoxByLabel("Issue Refund");
	usersPage.checkComboBoxByLabel("Login As Any Customer");
	usersPage.checkComboBoxByLabel("Login Read-Only As Any Customer");
	usersPage.checkComboBoxByLabel("Manage Campaign");
	usersPage.checkComboBoxByLabel("Manage Consumers");
	usersPage.checkComboBoxByLabel("Manage Return RMA");
	usersPage.checkComboBoxByLabel("Manager Override Chargeback");
	usersPage.checkComboBoxByLabel("Manager Override Refund");
	usersPage.checkComboBoxByLabel("Partner");
	usersPage.checkComboBoxByLabel("Payments Admin");
	usersPage.checkComboBoxByLabel("Region Administration");
	usersPage.checkComboBoxByLabel("RMA Rental For Credit");
	usersPage.checkComboBoxByLabel("Set Batch Close Time");
	usersPage.checkComboBoxByLabel("System Admin");
	usersPage.checkComboBoxByLabel("User Administration");
	usersPage.checkComboBoxByLabel("View All Bank Accounts");
	usersPage.checkComboBoxByLabel("View All Devices");

	usersPage.clickSaveChangesButton();

	usersPage.searchUserByName(userName);
	Assert.assertTrue(usersPage.isUserItemPresentInList(userName));

	usersPage.clickUserItemInList(userName);
	usersPage.verifyComboboxChecked("Admin Refund", true);
	usersPage.verifyComboboxChecked("Administer All Customer Users", true);
	usersPage.verifyComboboxChecked("Administer All Users", false);
	usersPage.verifyComboboxChecked("Appove EFTs", false);
	usersPage.verifyComboboxChecked("Bank Account Creation", true);
	usersPage.verifyComboboxChecked("Column Mappings", true);
	usersPage.verifyComboboxChecked("Configure Devices", true);
	usersPage.verifyComboboxChecked("Customer Service", false);
	usersPage.verifyComboboxChecked("Device Activation", true);
	usersPage.verifyComboboxChecked("Edit All Devices", true);
	usersPage.verifyComboboxChecked("ePort Online", false);
	usersPage.verifyComboboxChecked("Folio Admin", false);
	usersPage.verifyComboboxChecked("Folio Design", false);
	usersPage.verifyComboboxChecked("Issue Chargeback", false);
	usersPage.verifyComboboxChecked("Issue Refund", true);
	usersPage.verifyComboboxChecked("Login As Any Customer", false);
	usersPage.verifyComboboxChecked("Login Read-Only As Any Customer", false);
	usersPage.verifyComboboxChecked("Manage Campaign", true);
	usersPage.verifyComboboxChecked("Manage Consumers", true);
	usersPage.verifyComboboxChecked("Manage Return RMA", false);
	usersPage.verifyComboboxChecked("Manager Override Chargeback", false);
	usersPage.verifyComboboxChecked("Manager Override Refund", false);
	usersPage.verifyComboboxChecked("Partner", true);
	usersPage.verifyComboboxChecked("Payments Admin", false);
	usersPage.verifyComboboxChecked("Region Administration", true);
	usersPage.verifyComboboxChecked("RMA Rental For Credit", false);
	usersPage.verifyComboboxChecked("Set Batch Close Time", true);
	usersPage.verifyComboboxChecked("System Admin", false);
	usersPage.verifyComboboxChecked("User Administration", true);
	usersPage.verifyComboboxChecked("View All Bank Accounts", true);
	usersPage.verifyComboboxChecked("View All Devices", true);

	usersPage.clickLogInAsButton();
	new HomePage(driver).verifyTitle(firstName);

	// leftPanel.verifyMenuItemVisibility("Admin Refunds", true);
	topPanel.administration.adminRefunds.isDisplayed();

	// leftPanel.verifyMenuItemVisibility("Customers", true);
	topPanel.general.customers.isDisplayed();
	// leftPanel.verifyMenuItemVisibility("Customers", false);

	// leftPanel.verifyMenuItemVisibility("Appove EFTs", false);
	// This is in the DMS system. I wonder if this got missed in cleanup?



	// leftPanel.verifyMenuItemVisibility("Column Mappings", true);
	topPanel.configuration.columnMaps.isDisplayed();
	// leftPanel.verifyMenuItemVisibility("Configure Devices", true);
	// TODO:Devices->Device->Device Configuration button

	// leftPanel.verifyMenuItemVisibility("Customer Service", false);

	// leftPanel.verifyMenuItemVisibility("Device Activation", true);
	// This does not work. Remove it from testing.

	// leftPanel.verifyMenuItemVisibility("Edit All Devices", true);
	// No menu change. More devices available.

	leftPanel.verifyMenuItemVisibility("ePort Online", false);

	leftPanel.verifyMenuItemVisibility("Folio Admin", false);
	// I believe this allows you to modify Folio reports. Skip this.

	// leftPanel.verifyMenuItemVisibility("Folio Design", false);
	assertTrue(!topPanel.reports.folioDesigner.isDisplayed());

	// leftPanel.verifyMenuItemVisibility("Issue Chargebacks", false);

	// Isseur
	// leftPanel.verifyMenuItemVisibility("Issue Refund", true);

	// leftPanel.verifyMenuItemVisibility("Login As Any Customer", false);
	// Customers -> Login As button

	// leftPanel.verifyMenuItemVisibility("Login Read-Only As Any Customer", false);
	// I wonder if this has been essentially deprecated.

	// leftPanel.verifyMenuItemVisibility("Manage Campaign", true);
	// leftPanel.verifyMenuItemVisibility("Manage Consumers", true);
	// Issue

	leftPanel.verifyMenuItemVisibility("Manage Return RMA", false);
	leftPanel.verifyMenuItemVisibility("Manager Override Chargeback", false);
	leftPanel.verifyMenuItemVisibility("Manager Override Refund", false);
	// Amy Seymour sets this. More capabilities. I don't have this either.

	// leftPanel.verifyMenuItemVisibility("Partner", true);
	// Issue
	// I have to research this more. We tested this and forgot about it.

	// leftPanel.verifyMenuItemVisibility("Payments Admin", false);
	// This is something else in DMS. USALive reports and little else.

	// leftPanel.verifyMenuItemVisibility("Regions", true);
	// Issue

	leftPanel.verifyMenuItemVisibility("RMA Rental For Credit", false);
	// leftPanel.verifyMenuItemVisibility("Set Batch Close Time", true);
	// Issue
	leftPanel.verifyMenuItemVisibility("System Admin", false);
	// leftPanel.verifyMenuItemVisibility("User Administration", true);
	// No menu changes. The ability to manage your account.


	// leftPanel.verifyMenuItemVisibility("Devices", true);
	topPanel.general.devices.isDisplayed();

	// usersPage.clickUserItemInList(userName);
	// usersPage.clickDeleteUserButton();
	// driver.switchTo().alert().accept();
	// wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='messageText']/p[text()='Successfully
	// deleted user.']")));
	// leftPanel.clickMenuItemByText("Users");
	topPanel.general.users.navigateTo();

	usersPage.deleteUserByName(userName);
    }

}
