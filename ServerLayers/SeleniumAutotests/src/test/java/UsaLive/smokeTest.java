package UsaLive;

import helper.Core;
import org.junit.Test;
import pages.UsaLive.LeftPanel;
import pages.UsaLive.LogInPage;
import pages.UsaLive.TopPanel;


public class smokeTest extends Core
{

    @Test
    public void logInLogOut() throws Exception {
        openUsaLive();
        new LogInPage(driver).logInToUsaLiveAsWeakUser();
//        new LeftPanel(driver).clickMenuItemByText("Logout");
        new TopPanel(driver).general.logout.navigateTo();
        new LogInPage(driver).logInToUsaLiveAsWeakUser();

    }

}
