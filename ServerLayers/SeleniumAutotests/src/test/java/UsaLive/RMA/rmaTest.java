package UsaLive.RMA;

import helper.Core;
import org.junit.Test;
import pages.UsaLive.Administration.RMA.RMACreatePage;
import pages.UsaLive.Administration.RMA.RMACreatePartPage;
import pages.UsaLive.Administration.RMA.RMAPage;
import pages.UsaLive.Administration.RMA.RMARequestForm;
import pages.UsaLive.LeftPanel;
import pages.UsaLive.LogInPage;
import pages.UsaLive.TopPanel;

public class rmaTest extends Core {

    @Test
    public void viewRma() throws Exception {
	openUsaLive();
	new LogInPage(driver).logInToUsaLiveAsPowerUser();

//	LeftPanel leftPanel = new LeftPanel(driver);
//	leftPanel.clickMenuItemByText("View RMA");
	TopPanel topPanel = new TopPanel(driver);
	topPanel.administration.rma.viewRma.navigateTo();

	RMAPage rmaPage = new RMAPage(driver);

	String requestDate = "2014-10-22 10:42:16";
	String rmaNumber = "201410220001";
	String receipt = "View Receipt";
	String type = "Device And Replacement";
	String replacementKitQuantity = "0";
	String description = "Upgrade";
	String status = "Confirmed";
	String actions = "Processed";

	if (getEnvironment().equalsIgnoreCase("int")) {
	    requestDate = "2014-10-07 13:43:13";
	    rmaNumber = "201410070002";
	    receipt = "View Receipt";
	    type = "Device And Replacement";
	    replacementKitQuantity = "11";
	    description = "test";
	    status = "New";
	    actions = "Confirm";
	}

	rmaPage.setRmaNumber(rmaNumber);
	rmaPage.clickSearchButton();

	rmaPage.verifyRecordValuesByID(requestDate, rmaNumber, receipt, type, replacementKitQuantity, description,
		status, actions);

    }

    @Test
    public void createAndDeleteRMA() throws Exception {
	openUsaLive();

	RMAPage rmaPage = new RMAPage(driver);

	String deviceSerialNumber = "K3IDT0000000004";

	String description = "autotest comment 1";
	String contactName = "test contact";
	String streetAddress = "13";
	String postalCode = "02108";
	String city = "Boston";
	String state = "MA";
	String country = "US";
	String email = "usalive.test1@gmail.com";
	String phoneToCompare = "(123) 456-7890";
	String phoneToEnter = "1234567890";
	String customer = "Test Account Customer";
	String rmaType = "Device And Replacement";

	if (getEnvironment().equalsIgnoreCase("int")) {
	    deviceSerialNumber = "K3CMS200012345";
	    customer = "Lame MP3 Technologies";
	}

	new LogInPage(driver).logInToUsaLiveAsWeakUser();

//	LeftPanel leftPanel = new LeftPanel(driver);
//
//	leftPanel.clickMenuItemByText("Create RMA");
	TopPanel topPanel = new TopPanel(driver);
	topPanel.administration.rma.createRma.navigateTo();

	RMACreatePage rmaCreatePage = new RMACreatePage(driver);
	rmaCreatePage.clickYesButton();

	rmaCreatePage.setSearchByDeviceSerialNumber(deviceSerialNumber);
	rmaCreatePage.clickSearchButton();

	rmaCreatePage.clickDeviceCombobox(deviceSerialNumber);
	rmaCreatePage.clickAddSelectedButton();
	rmaCreatePage.setRMADescription(description);
	rmaCreatePage.setContactName(contactName);
	rmaCreatePage.setStreetAddress(streetAddress);
	rmaCreatePage.setCity(city);
	rmaCreatePage.setState(state);
	rmaCreatePage.setPostalCode(postalCode);
	rmaCreatePage.setEmail(email);
	rmaCreatePage.setPhone(phoneToEnter);

	rmaCreatePage.clickCreateRMAButton();

	RMARequestForm rmaRequestForm = new RMARequestForm(driver);

	String rmaNumber = rmaRequestForm.getRMANumber();

	rmaRequestForm.verifyUSALiveUser(getUsaLiveWeakUserNameAndPassword()[0]);
	rmaRequestForm.verifyUSALiveCustomer(customer);
	rmaRequestForm.verifyRMAType(rmaType);
	rmaRequestForm.verifyRMADescription(description);
	rmaRequestForm.verifyEPortSerial(deviceSerialNumber);
	rmaRequestForm.verifyContactName(contactName);
	rmaRequestForm.verifyAddress(streetAddress);
	rmaRequestForm.verifyCity(city);
	rmaRequestForm.verifyState(state);
	rmaRequestForm.verifyPostalCode(postalCode);
	rmaRequestForm.verifyCountry(country);
	rmaRequestForm.verifyEmail(email);
	rmaRequestForm.verifyPhone(phoneToCompare);

	// Delete RMA
	new LeftPanel(driver).clickMenuItemByText("Logout");

	new LogInPage(driver).logInToUsaLiveAsPowerUser();
//	leftPanel.clickMenuItemByText("View RMA");
	topPanel.administration.rma.viewRma.navigateTo();

	rmaPage.setRmaNumber(rmaNumber);
	rmaPage.clickSearchButton();

	if (rmaPage.isRmaFound(rmaNumber)) {
	    rmaPage.clickDeleteButton(rmaNumber);
	    driver.switchTo()
		  .alert()
		  .accept();
	}

    }

    @Test
    public void createAndDeletePartRMA() throws Exception {
	openUsaLive();

	RMAPage rmaPage = new RMAPage(driver);

	String deviceSerialNumber = "K3IDT0000000004";

	String description = "autotest comment 1";
	String contactName = "test contact";
	String streetAddress = "13";
	String postalCode = "02108";
	String city = "Boston";
	String state = "MA";
	String country = "US";
	String email = "usalive.test1@gmail.com";
	String phoneToEnter = "1234567890";
	String phoneToCompare = "(123) 456-7890";
	String customer = "Test Account Customer";
	String rmaType = "Parts And Replacement";
	String partNumber = "V8SUG1435002";
	String returnQuantity = "1";

	if (getEnvironment().equalsIgnoreCase("int")) {
	    partNumber = "V6XUG1335000";
	    customer = "Lame MP3 Technologies";
	}

	new LogInPage(driver).logInToUsaLiveAsWeakUser();

	// LeftPanel leftPanel = new LeftPanel(driver);
	//
	// leftPanel.clickMenuItemByText("Create Parts RMA");
	TopPanel topPanel = new TopPanel(driver);
	topPanel.administration.rma.createPartsRma.navigateTo();

	// RMACreatePage rmaCreatePage = new RMACreatePage(driver);
	// rmaCreatePage.clickYesButton();
	//
	// rmaCreatePage.setSearchByDeviceSerialNumber(deviceSerialNumber);
	// rmaCreatePage.clickSearchButton();
	//
	// rmaCreatePage.clickDeviceCombobox(deviceSerialNumber);
	// rmaCreatePage.clickAddSelectedButton();
	RMACreatePartPage rmaCreatePartPage = new RMACreatePartPage(driver);
	rmaCreatePartPage.clickPartComboBox(partNumber);
	rmaCreatePartPage.setReturnQuantity(partNumber, returnQuantity);
	rmaCreatePartPage.setRMADescription(description);
	rmaCreatePartPage.setContactName(contactName);
	rmaCreatePartPage.setStreetAddress(streetAddress);
	rmaCreatePartPage.setCity(city);
	rmaCreatePartPage.setState(state);
	rmaCreatePartPage.setPostalCode(postalCode);
	rmaCreatePartPage.setEmail(email);
	rmaCreatePartPage.setPhone(phoneToEnter);

	rmaCreatePartPage.clickCreateRMAButton();

	RMARequestForm rmaRequestForm = new RMARequestForm(driver);

	String rmaNumber = rmaRequestForm.getRMANumber();

	rmaRequestForm.verifyUSALiveUser(getUsaLiveWeakUserNameAndPassword()[0]);
	rmaRequestForm.verifyUSALiveCustomer(customer);
	rmaRequestForm.verifyRMAType(rmaType);
	rmaRequestForm.verifyRMADescription(description);
	rmaRequestForm.verifyContactName(contactName);
	rmaRequestForm.verifyAddress(streetAddress);
	rmaRequestForm.verifyCity(city);
	rmaRequestForm.verifyState(state);
	rmaRequestForm.verifyPostalCode(postalCode);
	rmaRequestForm.verifyCountry(country);
	rmaRequestForm.verifyEmail(email);
	rmaRequestForm.verifyPhone(phoneToCompare);

	rmaRequestForm.verifyQuantity(returnQuantity);
	rmaRequestForm.verifyPartNumber(partNumber);

	// Delete RMA
	// new LeftPanel(driver).clickMenuItemByText("Logout");
	topPanel.general.logout.navigateTo();

	new LogInPage(driver).logInToUsaLiveAsPowerUser();
	// leftPanel.clickMenuItemByText("View RMA");
	topPanel.administration.rma.viewRma.navigateTo();

	rmaPage.setRmaNumber(rmaNumber);
	rmaPage.clickSearchButton();
	rmaPage.verifyPage();
	if (rmaPage.isRmaFound(rmaNumber)) {
	    rmaPage.clickDeleteButton(rmaNumber);
	    driver.switchTo()
		  .alert()
		  .accept();
	}
    }
}
