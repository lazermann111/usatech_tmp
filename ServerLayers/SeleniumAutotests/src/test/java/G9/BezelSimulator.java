package G9;

import helper.Core;

import helper.HttpUtils;
import pages.Dms.Devices.PaymentConfigurationTab;
import pages.Dms.LeftPanel.DevicesMenu;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;
import org.junit.Ignore;

import com.usatech.test.ClientEmulatorDevTest;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by ahachikyan on 3/2/2016.
 */
public class BezelSimulator extends Core {
	private final String USER_AGENT = "Mozilla/5.0";
	@Ignore
    @Test
    public void USAT48_DMS_Customers_ViewByType() throws Exception {
        openDms();
        String cardTrack2 = "6396212006892181618=1912006050012";
        String serialNumber = "VJ011000001";
        new pages.Dms.LogInPage(driver).logInAsCommonUser();
        
        ClientEmulatorDevTest clientEmulatorDevTest = new ClientEmulatorDevTest();
//        clientEmulatorDevTest.testG9(getEnvironment(), cardTrack2, "VJ011000001");        
        
        DevicesMenu devicesMenu = new DevicesMenu(driver);
        
        devicesMenu.searchDeviceByTypeAndValue("Serial Number", serialNumber);
        
        PaymentConfigurationTab paymentConfigurationTab = new PaymentConfigurationTab(driver);
        paymentConfigurationTab.clickPaymentConfigurationTab();
        paymentConfigurationTab.clickTransactionHistoryListButton();
//      DeviceSettingsTab deviceSettingsTab = new DeviceSettingsTab(driver);
//        CustomersMenu customersMenu = new CustomersMenu(driver);
        //Get Bezel status
        //Swipe card

  
        
        
        HttpUtils httpUtils = new HttpUtils();
     
        String responseFronGet = httpUtils.sendGet("http://api.konvert.me/ip-country/94.100.180.70");
        logger.info("Test 1 - Send Http GET request. Response =" + responseFronGet);


		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("CountryName", "Malta"));
		
        String responseFromPost = httpUtils.sendPost("http://www.webservicex.net/globalweather.asmx/GetCitiesByCountry", urlParameters); 	
        logger.info("Test 2 - Send Http POST request. Response = " + responseFromPost);

    

    }

}

