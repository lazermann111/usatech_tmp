package Dms.Files;

import helper.Core;
import org.junit.Test;
import pages.Dms.Consumers.ConsumerAccountPage;
import pages.Dms.Consumers.ConsumerSearchPage;
import pages.Dms.Files.FileDetailsPage;
import pages.Dms.Files.FileListPage;
import pages.Dms.Files.UploadFileToServerPage;
import pages.Dms.LeftPanel.ConsumersMenu;
import pages.Dms.LeftPanel.FilesMenu;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static helper.FileUtils.getResoucePath;
import static helper.FileUtils.waitForFileToBeDownloaded;

/**
 * Created by ahachikyan on 3/2/2016.
 */
public class FileTest extends Core {

    public List<String> getFileInfoForTesting()
    {
    String fileName = "TD000014-LOG";
    String fileId = "1680780";
    String fileType = "Log File";
    String fileSize = "433 bytes";
    String fileContentPreview = "456467652003006467564D432000564D43201212456467652000564D43201212456467652000564D43201212456467652000564D43201212456467652000564D43201212456467652000564D43201212456467652000564D43201212456467652000564D43201212456467652000564D43201212456467652000564D43201212456467652000564D43201212456467652000564D43201212456467652000564D43201212456467652000564D43201212456467652000564D43201212456467652000564D43201212456467652000564D43201212456467652000564D43201300006402047D456467652000564D43201212456467652005006469564D432000564D43201212456467652000564D43201212456467652000564D43201212456467652000564D43201212456467652000564D43201212456467652000564D43201212456467652000564D43201212456467652000564D43201212456467652000564D43201212456467652000564D43201212456467652000564D43201212456467652000564D4320130202041B456467652000564D43201212456467652000564D43201212456467652000564D4320130417";

    if(getEnvironment().equalsIgnoreCase("int"))
    {
        fileName = "TD001499-LOG";
        fileId = "187246";
        fileType = "Log File";
        fileSize = "1,024 bytes";
        fileContentPreview = "D6124C7C6D8D4FF77F8A60FD77C56EBA656F031C775246C05EF47052F28216C74ADF921D79893A37C6028648B29A8B889C0DE5CCD833224597777E9AE3C9E28D7C0C9B10CF7C57D71B8591410FE3A9F73055E240C805B1BA7E743066774448F1DA756AEF4A0670817D3C56AF051DDEEF9F9B818486FF8B2B5A2B47C079EDEB6779160779747FC9174B627CAFD41B5C652D6A11A642B29AF814B1E68130BCD05AE088FBBBEA4595B35D036F26A88791B81ED01B1E850236F91B27684675AC595CB1F1E68CC4E2C7E6B3C9A12C127A82083BCC105D04A9DF2894D197671EEAD1B633E24031AB488C6E19B1437A08B5F90DA84496E97DB1E18873DDC1A8FD7F589CC1B5C814833F11EB05C402F0FE7C97680CF46DBEA1F25D5A12640C76E403543D5947A3EB747FFDE4C05F4BB05129B77AEBE2394DCA0BC7AF94DD24B6FA0585134C1ADAD5E7FF237F63819DF1962F9666875BDC0FDE8AF039B1DC89C393589BDCD003BD428C94FD11A97430F16CCB85FA36CE5490B1CB4D758FA96F86FFB1813218E5171AB08DAC420E08364698C1C4678D04DE6DC20DF0999FBD82AF26BA6F938FA422EC4E11066C7485EE4953B8CB0E01E838538883A588F50E29EAE864F575ED38935C955A5C2769D66381C42CCD232824F5365B751F0520FF19793A3D4EE969BCFADBE7091E2324CCAAFB63F3B3B8000550B9D7E776842DBA4961678CB6A5324FF6A2B652F8481B09929C738A81E452FE8D13D67940414F3B90FEE8FA374DB2D3EBA94A59A4F2C2A7730B9D0D0E140E76BFB4B7770D8C8A355350F6DF82A1685242D9A156AFCAD0A07FCFAE6B802023A46F6F4FC4BE80A1890A4724B6DD635F93E34B60101BD1F70BDDC88AB95B9B85BC488554057EA95D5895D8B5238904CC656E12E8ECDEAA65CAD5EEF0116F1458795998ADFAA50312A592FC6ACDD2E00118C043F335BDD1A4D99B03272842A35C0FF4D93E0B212EEDB4691D9F77CD3A7E0F638A3FE23FBB44CC4B375A419DAA843C8FA87A3032276EE3646511DD5C80AD94F1F23CC6487459B7E5FF41BC822839CF3B00CAFBCFEF9FE18B467E5A08859D5AC4A57BC78E164B693539A1259D02748C967AD3741D2E76D77F16237E7BFC694124D2F2C521163090FDE95B27C96AF9A07B4CB1841F7C10DE5C203972C5B222F2F44606590F94086EEF98B9A6D2022406614173F0601F52E892B6B9AF9B28901027A86070EA0C88555E361CD914CD25742A57F230DF25CE05BD52D154C6C901D2D4F5829F33ED1B62F42E6BAAE517117E8BD8AA052E4C993178B4D1CCCF4214E75E092779A11D859522E3304804EDB793BF957A7B3904372E01113432294AACFC700C65B501D851CB833DBBA61862F39EA9C417F757937B79D7225D83D6DD6AFDB4E97CB373AC68232396AF0E0B64E8F12A181926FB985308A6F7B55FDFDE";
    }

    List<String> fileInfo = Arrays.asList(fileName, fileId, fileType, fileSize, fileContentPreview);
    return fileInfo;
    }

    @Test
    public void   USAT_62_DMS_Files_SearchByType() throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();

        FilesMenu filesMenu = new FilesMenu(driver);
        FileListPage fileListPage = new FileListPage(driver);

        String fileType = "Application Software Upgrade";
        filesMenu.viewByType(fileType);
        fileListPage.verifyFiltrationByType(fileType);

        fileType = "DEX File";
        filesMenu.viewByType(fileType);
        fileListPage.verifyFiltrationByType(fileType);

        fileType = "Log File";
        filesMenu.viewByType(fileType);
        fileListPage.verifyFiltrationByType(fileType);
    }

    @Test
    public void USAT60_DMS_Files_SearchByName() throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();

        FilesMenu filesMenu = new FilesMenu(driver);
        FileListPage fileListPage = new FileListPage(driver);

        List<String> fileInfo = getFileInfoForTesting();
        String filePartialName = fileInfo.get(0).substring(0,6);
        String fileName = fileInfo.get(0);
        String fileId = fileInfo.get(1);
        String fileType = fileInfo.get(2);
        String fileSize = fileInfo.get(3);
        String fileContentPreview = fileInfo.get(4);

        filesMenu.searchByName(filePartialName);
        fileListPage.setFrom("01/01/2014");
        fileListPage.search();
        fileListPage.show2500RowsPerPage();

        fileListPage.clickId(fileId);

        verifyFileDetails(fileName, fileId, fileType, fileSize, fileContentPreview);

        filesMenu.searchByName("");

        fileListPage.setSearchOptionToBegins();
        fileListPage.setFileName(filePartialName);
        fileListPage.setFrom("01/01/2014");
        fileListPage.search();
        fileListPage.show2500RowsPerPage();

        fileListPage.clickId(fileId);

        verifyFileDetails(fileName, fileId, fileType, fileSize, fileContentPreview);

        //equals
        filesMenu.searchByName("");
        fileListPage.setFrom("01/01/2014");
        fileListPage.setSearchOptionToEquals();

        fileListPage.setFileName(fileName);
        fileListPage.search();

        verifyFileDetails(fileName, fileId, fileType, fileSize, fileContentPreview);

        // search 1 file
        filesMenu.searchByName(fileName);
        fileListPage.setFrom("01/01/2014");
        fileListPage.search();

        verifyFileDetails(fileName, fileId, fileType, fileSize, fileContentPreview);
    }

//    @Test
//    public void severalFilesSearchFromFileListPageByName() throws Exception {
//        openDms();
//        new pages.Dms.LogInPage(driver).logInAsCommonUser();
//
//        FilesMenu filesMenu = new FilesMenu(driver);
//        FileListPage fileListPage = new FileListPage(driver);
//
//        List<String> fileInfo = getFileInfoForTesting();
//        String filePartialName = fileInfo.get(0).substring(0,6);
//        String fileName = fileInfo.get(0);
//        String fileId = fileInfo.get(1);
//        String fileType = fileInfo.get(2);
//        String fileSize = fileInfo.get(3);
//        String fileContentPreview = fileInfo.get(4);

//        if(getEnvironment().equalsIgnoreCase("int"))
//        {
//            filePartialName = "TD0014";
//
//            fileName = "TD001499-LOG";
//            fileId = "187246";
//            fileType = "Log File";
//            fileSize = "1,024 bytes";
//            fileContentPreview = "D6124C7C6D8D4FF77F8A60FD77C56EBA656F031C775246C05EF47052F28216C74ADF921D79893A37C6028648B29A8B889C0DE5CCD833224597777E9AE3C9E28D7C0C9B10CF7C57D71B8591410FE3A9F73055E240C805B1BA7E743066774448F1DA756AEF4A0670817D3C56AF051DDEEF9F9B818486FF8B2B5A2B47C079EDEB6779160779747FC9174B627CAFD41B5C652D6A11A642B29AF814B1E68130BCD05AE088FBBBEA4595B35D036F26A88791B81ED01B1E850236F91B27684675AC595CB1F1E68CC4E2C7E6B3C9A12C127A82083BCC105D04A9DF2894D197671EEAD1B633E24031AB488C6E19B1437A08B5F90DA84496E97DB1E18873DDC1A8FD7F589CC1B5C814833F11EB05C402F0FE7C97680CF46DBEA1F25D5A12640C76E403543D5947A3EB747FFDE4C05F4BB05129B77AEBE2394DCA0BC7AF94DD24B6FA0585134C1ADAD5E7FF237F63819DF1962F9666875BDC0FDE8AF039B1DC89C393589BDCD003BD428C94FD11A97430F16CCB85FA36CE5490B1CB4D758FA96F86FFB1813218E5171AB08DAC420E08364698C1C4678D04DE6DC20DF0999FBD82AF26BA6F938FA422EC4E11066C7485EE4953B8CB0E01E838538883A588F50E29EAE864F575ED38935C955A5C2769D66381C42CCD232824F5365B751F0520FF19793A3D4EE969BCFADBE7091E2324CCAAFB63F3B3B8000550B9D7E776842DBA4961678CB6A5324FF6A2B652F8481B09929C738A81E452FE8D13D67940414F3B90FEE8FA374DB2D3EBA94A59A4F2C2A7730B9D0D0E140E76BFB4B7770D8C8A355350F6DF82A1685242D9A156AFCAD0A07FCFAE6B802023A46F6F4FC4BE80A1890A4724B6DD635F93E34B60101BD1F70BDDC88AB95B9B85BC488554057EA95D5895D8B5238904CC656E12E8ECDEAA65CAD5EEF0116F1458795998ADFAA50312A592FC6ACDD2E00118C043F335BDD1A4D99B03272842A35C0FF4D93E0B212EEDB4691D9F77CD3A7E0F638A3FE23FBB44CC4B375A419DAA843C8FA87A3032276EE3646511DD5C80AD94F1F23CC6487459B7E5FF41BC822839CF3B00CAFBCFEF9FE18B467E5A08859D5AC4A57BC78E164B693539A1259D02748C967AD3741D2E76D77F16237E7BFC694124D2F2C521163090FDE95B27C96AF9A07B4CB1841F7C10DE5C203972C5B222F2F44606590F94086EEF98B9A6D2022406614173F0601F52E892B6B9AF9B28901027A86070EA0C88555E361CD914CD25742A57F230DF25CE05BD52D154C6C901D2D4F5829F33ED1B62F42E6BAAE517117E8BD8AA052E4C993178B4D1CCCF4214E75E092779A11D859522E3304804EDB793BF957A7B3904372E01113432294AACFC700C65B501D851CB833DBBA61862F39EA9C417F757937B79D7225D83D6DD6AFDB4E97CB373AC68232396AF0E0B64E8F12A181926FB985308A6F7B55FDFDE";
//        }
        //begins
//        filesMenu.searchByName("");
//
//        fileListPage.setSearchOptionToBegins();
//        fileListPage.setFileName(filePartialName);
//        fileListPage.search();
//        fileListPage.show2500RowsPerPage();
//
//        fileListPage.clickId(fileId);
//
//        verifyFileDetails(fileName, fileId, fileType, fileSize, fileContentPreview);
//
//        //equals
//        filesMenu.searchByName("");
//        fileListPage.setSearchOptionToEquals();
//
//        fileListPage.setFileName(fileName);
//        fileListPage.search();
//
//        verifyFileDetails(fileName, fileId, fileType, fileSize, fileContentPreview);
//    }

//    @Test
//    public void oneFileSearchFromMenuByName() throws Exception {
//        openDms();
//        new pages.Dms.LogInPage(driver).logInAsCommonUser();
//
//        FilesMenu filesMenu = new FilesMenu(driver);
//
//        List<String> fileInfo = getFileInfoForTesting();
//        String fileName = fileInfo.get(0);
//        String fileId = fileInfo.get(1);
//        String fileType = fileInfo.get(2);
//        String fileSize = fileInfo.get(3);
//        String fileContentPreview = fileInfo.get(4);
//
//        filesMenu.searchByName(fileName);
//
//        verifyFileDetails(fileName, fileId, fileType, fileSize, fileContentPreview);
//    }

    private void verifyFileDetails(String fileName, String fileId, String fileType, String fileSize, String fileContentPreview) {
        FileDetailsPage fileDetailsPage = new FileDetailsPage(driver);

        fileDetailsPage.verifyFileId(fileId);
        fileDetailsPage.verifyFileName(fileName);
        fileDetailsPage.verifyFileType(fileType);
        fileDetailsPage.verifyFileSize(fileSize);
        fileDetailsPage.verifyFileContentPreview(fileContentPreview);

        Integer filesCountBefore = new File(tempFolderForDownloads).list().length;

        fileDetailsPage.downloadFile();
        waitForFileToBeDownloaded(filesCountBefore);
    }

    @Test
    public void USAT_61_DMS_Files_SearchByFileID() throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();

        FilesMenu filesMenu = new FilesMenu(driver);

        List<String> fileInfo = getFileInfoForTesting();
        String fileName = fileInfo.get(0);
        String fileId = fileInfo.get(1);
        String fileType = fileInfo.get(2);
        String fileSize = fileInfo.get(3);
        String fileContentPreview = fileInfo.get(4);

        filesMenu.searchById(fileId);

        verifyFileDetails(fileName, fileId, fileType, fileSize, fileContentPreview);
    }

    @Test
    public void     USAT63_DMS_Files_UploadFileToServer() throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();

        FilesMenu filesMenu = new FilesMenu(driver);

        Date date = new Date();

        String fileName = date.toString();
        String fileType = "Log File";
        String descriptiveComment = "comment";
        String fileSize = "12 bytes";
        String fileContentPreview = "testFileText";
        String fileFullPath = getResoucePath("filesForUploading/").replace("/", "\\") + "testFile.txt";

        filesMenu.uploadFileToServer();

        UploadFileToServerPage uploadFileToServerPage = new UploadFileToServerPage(driver);

        uploadFileToServerPage.setFileName(fileName);
        uploadFileToServerPage.setFileType(fileType);
        uploadFileToServerPage.setDescriptiveComment(descriptiveComment);
        logger.info("File path=" + fileFullPath);
        uploadFileToServerPage.browseFileToUploadToServer(fileFullPath);
        uploadFileToServerPage.uploadToServer();

        FileDetailsPage fileDetailsPage = new FileDetailsPage(driver);

        fileDetailsPage.verifyFileComment(descriptiveComment);
        fileDetailsPage.verifyFileName(fileName);
        fileDetailsPage.verifyFileType(fileType);
        fileDetailsPage.verifyFileSize(fileSize);
        fileDetailsPage.verifyFileContentPreview(fileContentPreview);

        Integer filesCountBefore = new File(tempFolderForDownloads).list().length;

        fileDetailsPage.downloadFile();      
        waitForFileToBeDownloaded(filesCountBefore);

    }

//TODO: continue


}

