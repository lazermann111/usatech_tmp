package Dms.Reports;


import helper.CompareUtils;
import helper.Core;

import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.Dms.LeftPanel.ReportsMenu;
import pages.Dms.Reports.ReportsPage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static helper.CompareUtils.*;
import static helper.FileUtils.waitForFileToBeDownloaded;

/**
 * Created by ahachikyan on 3/2/2016.
 */
public class ReportsTest extends Core {
    private static String testGroupFolder = "dms/";
//    TODO: 281, 294, 295, 301

    @Test
    public void USAT_280_DMS_Reports_DeviceCountDetails () throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();

        new ReportsMenu(driver).open();
        ReportsPage reportsPage = new ReportsPage(driver);
        reportsPage.setDeviceCountDetailsDateFrom("04/04/2014");
        reportsPage.setDeviceCountDetailsDateTo("05/05/2014");
        Integer filesCountBefore = new File(tempFolderForDownloads).list().length;

        reportsPage.clickViewDeviceCountDetailsButton();

        waitForFileToBeDownloaded(filesCountBefore);

        String actualFilePath = copyDownloadedExpectedReportToResources(getCsvReportPath("dms/deviceCountDetails_act.csv"));
        String expectedCsvPath = getCsvReportPath("dms/deviceCountDetails_exp.csv");
        compareCsv(expectedCsvPath, actualFilePath);
    }

    @Test
    public void USAT_282_DMS_Reports_CustomerDevicesByPostalCode () throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();

        new ReportsMenu(driver).open();
        ReportsPage reportsPage = new ReportsPage(driver);
        reportsPage.setPostalCode("10007");

        reportsPage.clickViewDevicesByPostalCodeButton();

        String actualFilePath = copyDownloadedExpectedReportToResources(getCsvReportPath("dms/customerDevicesByPostalCode_act.csv"));
        String expectedCsvPath = getCsvReportPath("dms/customerDevicesByPostalCode_exp.csv");
        compareCsv(expectedCsvPath, actualFilePath);
    }

    @Test
    public void USAT_283_DMS_Reports_DeviceCount () throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();

        new ReportsMenu(driver).open();
        ReportsPage reportsPage = new ReportsPage(driver);
        reportsPage.clickViewDeviceCountButton();

        verifyPng(driver, wait, "deviceCount", testGroupFolder);
    }

    @Test
    public void USAT_284_DMS_Reports_eSudsDeviceCount() throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();

        new ReportsMenu(driver).open();
        ReportsPage reportsPage = new ReportsPage(driver);
        reportsPage.clickViewESudsDeviceCountButton();

        verifyPng(driver, wait, "eSudsDeviceCount", testGroupFolder);
    }

    @Test
    public void USAT_285_DMS_Reports_eSudsSchools () throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();

        new ReportsMenu(driver).open();
        ReportsPage reportsPage = new ReportsPage(driver);
        reportsPage.clickViewESudsSchoolsButton();

        verifyPng(driver, wait, "eSudsSchools", testGroupFolder);
    }

    //Dynamic
    @Test
    public void USAT_286_DMS_Reports_ActiveDeviceCount () throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();

        new ReportsMenu(driver).open();
        ReportsPage reportsPage = new ReportsPage(driver);
        reportsPage.clickViewActiveDeviceCountButton();
        reportsPage.verifyActiveDevicesTextVisibility();
//        verifyPng(driver, wait, "activeDeviceCount", testGroupFolder);
    }

    //Dynamic
    @Test
    public void USAT_287_DMS_Reports_DeviceCountByState () throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();

        new ReportsMenu(driver).open();
        ReportsPage reportsPage = new ReportsPage(driver);
        reportsPage.clickViewDeviceCountByStateButton();
        reportsPage.verifyDeviceCountByStateTextVisibility();
//        verifyPng(driver, wait, "deviceCountByState", testGroupFolder);
    }

    //Dynamic
    @Test
    public void USAT_288_DMS_Reports_GPRSDeviceRSSILog () throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();

        new ReportsMenu(driver).open();
        ReportsPage reportsPage = new ReportsPage(driver);
        reportsPage.clickViewGPRSDeviceRSSILogButton();

        reportsPage.verifyGPRSDeviceRSSILogSerialNumber("E4074081");
    }

    @Test
    public void USAT_289_DMS_Reports_NetworkDevices() throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();

        new ReportsMenu(driver).open();
        ReportsPage reportsPage = new ReportsPage(driver);

        Integer filesCountBefore = new File(tempFolderForDownloads).list().length;

        reportsPage.clickViewNetworkDevicesButton();

        waitForFileToBeDownloaded(filesCountBefore);
//
//        String actualFilePath = copyDownloadedExpectedReportToResources(getCsvReportPath("dms/networkDevices_act.txt"));
//        String expectedCsvPath = getCsvReportPath("dms/networkDevices_exp.txt");
//        compareCsv(expectedCsvPath, actualFilePath);
    }

    @Test
    public void USAT_290_DMS_Reports_FirmwareUpgradeStatus () throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();

        new ReportsMenu(driver).open();
        ReportsPage reportsPage = new ReportsPage(driver);
        reportsPage.clickViewFirmwareUpgradeStatusButton();

        String actualFilePath = copyDownloadedExpectedReportToResources(getCsvReportPath("dms/firmwareUpgradeStatus_act.txt"));
        String expectedCsvPath = getCsvReportPath("dms/firmwareUpgradeStatus_exp.txt");
        compareCsv(expectedCsvPath, actualFilePath);
    }

    @Test
    public void USAT_291_DMS_Reports_KioskActivityByState() throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();

        new ReportsMenu(driver).open();
        ReportsPage reportsPage = new ReportsPage(driver);
        reportsPage.clickViewKioskActivityByStateButton();

        reportsPage.verifyKioskActivityByStateTextVisibility();
    }

    //Dynamic
    @Test
    public void USAT_292_DMS_Reports_InactiveKiosks() throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();

        new ReportsMenu(driver).open();
        ReportsPage reportsPage = new ReportsPage(driver);
        reportsPage.clickViewInactiveKiosksButton();

        reportsPage.verifyInactiveKiosksTextVisibility();
    }

    //Dynamic
    @Test
    public void USAT_293_DMS_Reports_GxDevicesWithPollDEXFlag() throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();

        new ReportsMenu(driver).open();
        ReportsPage reportsPage = new ReportsPage(driver);

        Integer filesCountBefore = new File(tempFolderForDownloads).list().length;

        reportsPage.clickViewgxDevicesWithPollDEXFlagButton();

        waitForFileToBeDownloaded(filesCountBefore);

        String actualFilePath = copyDownloadedExpectedReportToResources(getCsvReportPath("dms/gxDevicesWithPollDEXFlag_act.txt"));
        String expectedCsvPath = getCsvReportPath("dms/gxDevicesWithPollDEXFlag_exp.txt");
        compareCsv(expectedCsvPath, actualFilePath);
//        reportsPage.verifyInactiveKiosksTextVisibility();
    }

    @Test
    public void USAT_296_DMS_Reports_AuthExecTimes () throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();

        new ReportsMenu(driver).open();
        ReportsPage reportsPage = new ReportsPage(driver);
        reportsPage.clickViewAuthExecTimesButton();
        reportsPage.verifyAuthExecTimesVisibility();
        //Dynamic
//        verifyPng(driver, wait, "authExecTimes", testGroupFolder);
    }

    @Test
    public void USAT_297_DMS_Reports_AuthLayerExecTimes() throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();

        new ReportsMenu(driver).open();
        ReportsPage reportsPage = new ReportsPage(driver);
        reportsPage.clickViewAuthLayerExecTimesButton();

        reportsPage.verifyAuthExecTimesVisibility();
//        Dynamic
        //verifyPng(driver, wait, "authLayerExecTimes", testGroupFolder);
    }

    @Test
    public void USAT_298_DMS_Reports_AuthRoundTripReport() throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();

        new ReportsMenu(driver).open();
        ReportsPage reportsPage = new ReportsPage(driver);
        reportsPage.clickViewAuthRoundTripReportButton();

        reportsPage.verifyAuthRoundTripReportTextVisibility();
    }

    @Test
    public void USAT_304_DMS_Reports_PaymentStats () throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();

        new ReportsMenu(driver).open();
        ReportsPage reportsPage = new ReportsPage(driver);
        reportsPage.clickViewPaymentStatsButton();

        reportsPage.verifyPaymentStatsTextVisibility();
    }

    @Test
    public void USAT_305_DMS_Reports_AuthorityAuthStats() throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();

        new ReportsMenu(driver).open();
        ReportsPage reportsPage = new ReportsPage(driver);
        reportsPage.clickAuthorityAuthStatsButton();

        reportsPage.verifyAuthorityAuthStatsTextVisibility();
    }

    @Test
    public void  USAT_299_DMS_Reports_MerchantAuthStats () throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();

        new ReportsMenu(driver).open();
        ReportsPage reportsPage = new ReportsPage(driver);
        reportsPage.clickMerchantAuthStatsButton();

        reportsPage.verifyMerchantAuthStatsTextVisibility();
    }

    @Test
    public void USAT_300_DMS_Reports_PaymentTypeCounts() throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();

        new ReportsMenu(driver).open();
        ReportsPage reportsPage = new ReportsPage(driver);
        reportsPage.clickViewPaymentTypeCountsButton();

        reportsPage.verifyPaymentTypeCountsTextVisibility();
        reportsPage.verifyAnyTableValue("American Express (EMV Contact) - Chase Paymentech - USA");
    }

    @Test
    public void USAT_302_DMS_Reports_BatchSuccessReport() throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();

        new ReportsMenu(driver).open();
        ReportsPage reportsPage = new ReportsPage(driver);
        reportsPage.clickViewBatchSuccessReportButton();

        reportsPage.verifyBatchSuccessReportsTextVisibility();
    }

    @Test
    public void USAT_303_DMS_Reports_CallInStats() throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();

        new ReportsMenu(driver).open();
        ReportsPage reportsPage = new ReportsPage(driver);
        reportsPage.clickViewCallInStatsButton();

        reportsPage.verifyCallInStatsTextVisibility();
    }

    public static void verifyPng(WebDriver driver, WebDriverWait wait, String reportName, String testGroupFolder) throws IOException {
        driver = driver.switchTo().frame(0);
        File actualDriverScreenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
//        File actualDriverScreenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

        BufferedImage wholeWindowScreenshot	= ImageIO.read(actualDriverScreenshot);
        //This workaround needs because getScreenshotAs returns screenshot with max width of 32766pix. But height of report web element arep is could be much higher and fails with raster exception
//        if(wholeWindowScreenshot.getHeight() > 32000)
//        {
//            wholeWindowScreenshot = wholeWindowScreenshot.getSubimage(0, 0, wholeWindowScreenshot.getWidth(), 32000);
//        }

        // Web element to make screenshot from
        //driver.switchTo().frame(0).findElement(By.xpath("//div[@id='__BIRT_ROOT']"));
//        WebElement actualWebElement = driver.findElement(By.xpath("//div[@id='__BIRT_ROOT']"));

//        WebElement actualWebElement = driver.switchTo().frame(0).findElement(By.xpath("//div[@id='__BIRT_ROOT']"));
//        String fullImageUrl = getScreenshotReportPath(reportName + "_full.png");;
//        CompareUtils.saveImage(wholeWindowScreenshot, fullImageUrl);
//        logger.info("Full not cropped image stored at:" + fullImageUrl);
        //Create an actual screenshot
//        Integer webElementX = actualWebElement.getLocation().getX();
//        Integer webElementY =  actualWebElement.getLocation().getY();
//
//        Integer webElementWidth = actualWebElement.getSize().getWidth();
//        Integer webElementHeight = actualWebElement.getSize().getHeight();
//
//        Integer wholeImageWidth = wholeWindowScreenshot.getWidth();
//        Integer wholeImageHeight= wholeWindowScreenshot.getHeight();

//        if(webElementHeight >= 32000)
//        {
//            webElementHeight = 32000 -  webElementY;
//        }
//
//        if ((webElementHeight + webElementY)>wholeImageHeight)
//            webElementHeight = wholeImageHeight - webElementY;

//        BufferedImage actualImageCropped = wholeWindowScreenshot.getSubimage(webElementX, webElementY, webElementWidth, webElementHeight);
//        BufferedImage actualImageCropped = screenshot.getImage();

        String actualImageUrl = getScreenshotReportPath(testGroupFolder + reportName + "_act.png");;
        String differenceImageUrl = getScreenshotReportPath(testGroupFolder + reportName + "_dif.png");
        CompareUtils.saveImage(wholeWindowScreenshot, actualImageUrl);
        logger.info("Actual image stored at:" + actualImageUrl);
        String expectedImageUrl = getScreenshotReportPath(testGroupFolder + reportName + "_exp.png");
        if(!generateEtalons)
            CompareUtils.compareImages(expectedImageUrl,actualImageUrl,differenceImageUrl);

        driver = driver.switchTo().defaultContent();
    }

}

