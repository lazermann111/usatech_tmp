package Dms.EPortManager;

import helper.Core;
import org.junit.Test;
import pages.Dms.LeftPanel.EPortManagerMenu;
import pages.Dms.EPortManager.BankAccountsPage;


public class BankAccounts extends Core {

    @Test
    public void USAT_238_DMS_ePortManager_BankAccounts_Search() throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();
        EPortManagerMenu ePortManagerMenu = new EPortManagerMenu(driver);
        String customerName = "Boom Vending Inc.";
        ePortManagerMenu.searchBankAccountByCustomerName(customerName);
        BankAccountsPage bankAccountsPage = new BankAccountsPage(driver);
        bankAccountsPage.verifyCustomerPresence(customerName);  
    }
}

