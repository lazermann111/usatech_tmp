package Dms.Devices;

import difflib.Delta;
import difflib.DiffUtils;
import difflib.Patch;
import helper.Core;
import junit.framework.Assert;
import org.junit.Ignore;
import org.junit.Test;
import pages.Dms.Devices.DeviceConfigurationTab;
import pages.Dms.Devices.FileTransferTab;
import pages.Dms.LeftPanel.DevicesMenu;

import java.io.File;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;

import static helper.FileUtils.getResoucePath;

/**
 * Created by ahachikyan on 3/2/2016.
 */
public class FileTransferTest extends Core {

    @Test
    // USAT-45 DMS. Devices. Device. File Transfer. Download Config
    public void USAT45_DMS_Devices_Device_FileTransfer_DownloadConfig() throws Exception {
        String deviceName = "TD000759";

        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();
        DevicesMenu devicesMenu = new DevicesMenu(driver);
        FileTransferTab fileTransferTab = new FileTransferTab(driver);

        if(getEnvironment().equalsIgnoreCase("int"))
        {
            deviceName = "TD001241";
        }

        devicesMenu.searchDeviceByTypeAndValue("Device Name", deviceName);
        fileTransferTab.clickFileTransferTab();
        fileTransferTab.clickDownloadConfigButton();

        DeviceConfigurationTab deviceConfigurationTab = new DeviceConfigurationTab(driver);
        deviceConfigurationTab.verifySelected();
    }

    @Ignore
    @Test
    //TODO: Apply normally. Fail details in HTML diff dows not make sense
    //    USAT-46  Devices. Device. File Transfer. List
    public void USAT46_Devices_Device_FileTransfer_List() throws Exception {
        String deviceName = "TD000759";

        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();
        DevicesMenu devicesMenu = new DevicesMenu(driver);
        FileTransferTab fileTransferTab = new FileTransferTab(driver);

        if(getEnvironment().equalsIgnoreCase("int"))
        {
            deviceName = "TD001241";
        }

        devicesMenu.searchDeviceByTypeAndValue("Device Name", deviceName);
        fileTransferTab.clickFileTransferTab();
        fileTransferTab.clickListButton();

        String s = fileTransferTab.getInnerHtml();
        String[] actualInnerHtml  = fileTransferTab.getInnerHtml().split("\n");;
        List<String> actual = Arrays.asList(actualInnerHtml);

        String expectedFilePath = getResoucePath("deviceInfo/") + "fileTransferList" + deviceName + "_exp.txt";
        List<String> expected = Files.readAllLines(new File(expectedFilePath).toPath())/*.toString().split("/n")*/;

        Patch patch =  DiffUtils.diff(actual, expected);
        for (Delta delta: patch.getDeltas()) {
            System.out.println(delta);
        }
        Assert.assertEquals("Current inner HTML differs from expected. See difference at the top", 0,patch.getDeltas().size() );
    }

}

