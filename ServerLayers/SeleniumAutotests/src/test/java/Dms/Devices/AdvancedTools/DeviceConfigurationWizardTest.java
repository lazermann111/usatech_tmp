package Dms.Devices.AdvancedTools;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

import helper.Core;
import helper.EmailUtils;
import helper.OracleConnector;
import pages.LeftPanel;
import pages.Dms.Devices.DeviceConfigurationTab;
import pages.Dms.Devices.DeviceConfigurationWizardPage1;
import pages.Dms.Devices.DeviceConfigurationWizardPage10;
import pages.Dms.Devices.DeviceConfigurationWizardPage11;
import pages.Dms.Devices.DeviceConfigurationWizardPage2;
import pages.Dms.Devices.DeviceConfigurationWizardPage3;
import pages.Dms.Devices.DeviceConfigurationWizardPage4;
import pages.Dms.Devices.DeviceConfigurationWizardPage5;
import pages.Dms.Devices.DeviceConfigurationWizardPage7;
import pages.Dms.Devices.DeviceConfigurationWizardPage8;
import pages.Dms.Devices.DeviceConfigurationWizardPage9;
import pages.Dms.Devices.FileTransferTab;
import pages.Dms.Devices.PaymentConfigurationTab;
import pages.Dms.LeftPanel.DevicesMenu;

public class DeviceConfigurationWizardTest extends Core {
	
	public void verifyDeviceConfigurationWizard(String deviceSerialNumber, String customer, String location, String template, String propertyName, String propertyId,
			String paymentTypeTemplate, String paymentType, String customerId, String locationId, String paymentTemplateId ) throws Exception
	{
	      	DevicesMenu devicesMenu = new DevicesMenu(driver);
	        devicesMenu.openDeviceConfigurationWizard();
	        
	        DeviceConfigurationWizardPage1 deviceConfigurationWizardPage1 = new DeviceConfigurationWizardPage1(driver);
	        deviceConfigurationWizardPage1.setDeviceType("ePort Edge/G9");
	        deviceConfigurationWizardPage1.setSerialNumber(deviceSerialNumber);
	        deviceConfigurationWizardPage1.clickNextButton();
	        
	        DeviceConfigurationWizardPage2 deviceConfigurationWizardPage2 = new DeviceConfigurationWizardPage2(driver);
	        deviceConfigurationWizardPage2.clickNextButton();
	        
	        DeviceConfigurationWizardPage3 deviceConfigurationWizardPage3 = new DeviceConfigurationWizardPage3(driver);
	        
	        deviceConfigurationWizardPage3.searchCustomer(customer);
	        deviceConfigurationWizardPage3.setCustomer(customer);
	        	        
	        deviceConfigurationWizardPage3.searchLocation(location);
	        deviceConfigurationWizardPage3.setLocation(location);
	        
	        deviceConfigurationWizardPage3.clickNextButton();
	        
	        DeviceConfigurationWizardPage4 deviceConfigurationWizardPage4 = new DeviceConfigurationWizardPage4(driver);	        
	        deviceConfigurationWizardPage4.searchTemplate(template);
	        deviceConfigurationWizardPage4.setTemplate(template);
	        deviceConfigurationWizardPage4.clickNextButton();
	        
	        DeviceConfigurationWizardPage5 deviceConfigurationWizardPage5 = new DeviceConfigurationWizardPage5(driver);
	        deviceConfigurationWizardPage5.verifyTemplateName(template);

	        Date date = new Date();
	        String propertyValue =  date.toString();
	        OracleConnector oracleConnector = new OracleConnector(getEnvironment());
	        String deviceId = oracleConnector.getDeviceIdBySerialNumber(deviceSerialNumber);
	        String deviceName = oracleConnector.getDeviceNameBySerialNumber(deviceSerialNumber);
	        String oldPropertyValue = oracleConnector.getDeviceSettingValueByDeviceIdAndParameterCd(deviceId, propertyId);
	        deviceConfigurationWizardPage5.setPropertyTextBoxByLabel(propertyName, propertyValue);
	        deviceConfigurationWizardPage5.clickNextButton();
	        
	        DeviceConfigurationWizardPage7 deviceConfigurationWizardPage7 = new DeviceConfigurationWizardPage7(driver);
	
	        deviceConfigurationWizardPage7.setPaymentTypeTemplate(paymentTypeTemplate);
	        deviceConfigurationWizardPage7.setImportMode("Complete Overwrite");
	        deviceConfigurationWizardPage7.clickNextButton();
	        
	        DeviceConfigurationWizardPage8 deviceConfigurationWizardPage8 = new DeviceConfigurationWizardPage8(driver);
	        deviceConfigurationWizardPage8.clickNextButton();
	        
	        DeviceConfigurationWizardPage9 deviceConfigurationWizardPage9 = new DeviceConfigurationWizardPage9(driver);
	        deviceConfigurationWizardPage9.clickNextButton();
	        
	        DeviceConfigurationWizardPage10 deviceConfigurationWizardPage10 = new DeviceConfigurationWizardPage10(driver);
	        deviceConfigurationWizardPage10.verifyCustomer(customer);
	        deviceConfigurationWizardPage10.verifyLocation(location);
	        deviceConfigurationWizardPage10.verifyTemplateName(paymentTypeTemplate);
	        deviceConfigurationWizardPage10.verifyParameter(propertyName);
	        deviceConfigurationWizardPage10.verifyValue(propertyValue);  
	        
	        String email = "ahachikyan@usatech.com";
	        Integer emailsCountBefore = EmailUtils.getEmailsCount(getEmailUserNameAndPassword()[0], getEmailUserNameAndPassword()[1]);
	        deviceConfigurationWizardPage10.clickFinishButton();
	        deviceConfigurationWizardPage10.clickOkInAlert();
	         
	        String expectedLog = " Selected Device IDs: " + deviceId + " Selected Customer ID: " + customerId + " Selected Location ID: " + locationId + 
	        		" Updating Customer and/or Location for " + deviceName + " CUSTOMER and LOCATION changed, customer_id = " + customerId + ", location_id = " + locationId +
	        		" Imported payment template " + paymentTemplateId + " into device " + deviceName + " Configuration Changes for " + deviceName + ":  " 
	        		+ propertyName + " [" + propertyId + "]: " + propertyValue + ", old:   " + oldPropertyValue + "   Operation Summary Parameters Changed: 1 Parameters Already Up-To-Date: 0 Customer/Locations Updated: 1 Payment Templates Imported: 1 Emailed confirmation to ahachikyan@usatech.com ";
	    
	        DeviceConfigurationWizardPage11 deviceConfigurationWizardPage11 = new DeviceConfigurationWizardPage11(driver);
	        deviceConfigurationWizardPage11.verifyLog(expectedLog);

	        deviceConfigurationWizardPage11.clickStatrOverButton();
	        EmailUtils.waitForNewEmail(emailsCountBefore, 3605, getEmailUserNameAndPassword()[0], getEmailUserNameAndPassword()[1]);
	        
	        String newPropertyValue = oracleConnector.getDeviceSettingValueByDeviceIdAndParameterCd(deviceId, propertyId);
	        Assert.assertEquals(propertyValue, newPropertyValue);
	        
	        devicesMenu.searchDevice(deviceSerialNumber);
	        PaymentConfigurationTab paymentConfigurationTab = new PaymentConfigurationTab(driver);  
	        paymentConfigurationTab.clickPaymentConfigurationTab();
	        paymentConfigurationTab.clickPaymentTypesListButton();
	        
	        paymentConfigurationTab.verifyPaymentTypePresence(paymentType);
	}
    @SuppressWarnings("deprecation")
	@Test
    public void USAT_217_DMS_Devices_AdvancedTools_DeviceConfigurationWizard() throws Exception {

        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();
        
        String deviceSerialNumber = "VJ011000012";   
        String customer = "USA Technologies Inc";
        String location = "USA Technologies Beacon Top";
        String template = "DEFAULT-CFG-13-20";
        String propertyName = "Attract Message Line 1";
        String propertyId = "1203";
        String paymentTypeTemplate = "DEMO: USAT Demo";
        String paymentType = "SPECIAL: USAT Demo - Visa (Track 2)";
        String customerId = "6";
        String locationId = "1007192";
        if(getEnvironment().equalsIgnoreCase("int"))
        	locationId = "1827";
        String paymentTemplateId = "1001536";
        if(getEnvironment().equalsIgnoreCase("int"))
        	paymentTemplateId = "570";
        verifyDeviceConfigurationWizard(deviceSerialNumber, customer, location, template, propertyName, propertyId, paymentTypeTemplate,
        		paymentType, customerId, locationId, paymentTemplateId);
  
        
      //----------------------
  
        customer = "USA Tech Demo";       
        location = "USA Tech Dev Workstation";
        template = "DEFAULT-CFG-13-20";
        propertyName = "Attract Message Line 1";
        propertyId = "1203";
        paymentTypeTemplate = "SPECIAL MERGE: USAT ISO Prepaid Card";
        paymentType = "SPECIAL: USAT ISO Card - Prepaid Card";
        customerId = "45";
        locationId = "869";
        if(getEnvironment().equalsIgnoreCase("int"))
        	locationId = "869";
        paymentTemplateId = "1001113";
        if(getEnvironment().equalsIgnoreCase("int"))
        	paymentTemplateId = "510";         
        verifyDeviceConfigurationWizard(deviceSerialNumber, customer, location, template, propertyName, propertyId, paymentTypeTemplate,
        		paymentType, customerId, locationId, paymentTemplateId);
//        
// devicesMenu.openDeviceConfigurationWizard();
//        
//        
//        deviceConfigurationWizardPage1.setDeviceType("ePort Edge/G9");
//        deviceConfigurationWizardPage1.setSerialNumber(deviceSerialNumber);
//        deviceConfigurationWizardPage1.clickNextButton();
//        
//        
//        deviceConfigurationWizardPage2.clickNextButton();
//        
//        
//        
//   
//        deviceConfigurationWizardPage3.searchLocation(location);
//        deviceConfigurationWizardPage3.setLocation(location);
//        
//        deviceConfigurationWizardPage3.clickNextButton();
//                
//        template = "DEFAULT-CFG-13-20";
//        deviceConfigurationWizardPage4.searchTemplate(template);
//        deviceConfigurationWizardPage4.setTemplate(template);
//        deviceConfigurationWizardPage4.clickNextButton();
//        
//        
//        deviceConfigurationWizardPage5.verifyTemplateName(template);
//         propertyName = "Attract Message Line 1";
//         propertyId = "1203";
//         date = new Date();
//         propertyValue =  date.toString();
        
        
//         oldPropertyValue = oracleConnector.getDeviceSettingValueByDeviceIdAndParameterCd(deviceId, propertyId);
//        deviceConfigurationWizardPage5.setPropertyTextBoxByLabel(propertyName, propertyValue);
//        deviceConfigurationWizardPage5.clickNextButton();
//        
//        paymentTypeTemplate = "SPECIAL MERGE: USAT ISO Prepaid Card";
//        paymentType = "SPECIAL: USAT ISO Card - Prepaid Card";
//        deviceConfigurationWizardPage7.setPaymentTypeTemplate(paymentTypeTemplate);
//        deviceConfigurationWizardPage7.setImportMode("Complete Overwrite");
//        deviceConfigurationWizardPage7.clickNextButton();
//        
//        deviceConfigurationWizardPage8.clickNextButton();
//        
//        deviceConfigurationWizardPage9.clickNextButton();
//        
//        deviceConfigurationWizardPage10.verifyCustomer(customer);
//        deviceConfigurationWizardPage10.verifyLocation(location);
//        deviceConfigurationWizardPage10.verifyTemplateName(paymentTypeTemplate);
//        deviceConfigurationWizardPage10.verifyParameter(propertyName);
//        deviceConfigurationWizardPage10.verifyValue(propertyValue);  
//               
//        emailsCountBefore = EmailUtils.getEmailsCount(getEmailUserNameAndPassword()[0], getEmailUserNameAndPassword()[1]);
//        deviceConfigurationWizardPage10.clickFinishButton();
//        deviceConfigurationWizardPage10.clickOkInAlert();
        
//        customerId = "45";
//        locationId = "869";
//        if(getEnvironment().equalsIgnoreCase("int"))
//        	locationId = "869";
//        paymentTemplateId = "1001113";
//        if(getEnvironment().equalsIgnoreCase("int"))
//        	paymentTemplateId = "510"; 
//        expectedLog = " Selected Device IDs: " + deviceId + " Selected Customer ID: " + customerId + " Selected Location ID: " + locationId + 
//        		" Updating Customer and/or Location for " + deviceName + " CUSTOMER and LOCATION changed, customer_id = " + customerId + ", location_id = " + locationId +
//        		" Imported payment template " + paymentTemplateId + " into device " + deviceName + " Configuration Changes for " + deviceName + ":  " 
//        		+ propertyName + " [" + propertyId + "]: " + propertyValue + ", old:   " + oldPropertyValue + "   Operation Summary Parameters Changed: 1 Parameters Already Up-To-Date: 0 Customer/Locations Updated: 1 Payment Templates Imported: 1 Emailed confirmation to ahachikyan@usatech.com ";
//    
//       
//        deviceConfigurationWizardPage11.verifyLog(expectedLog);
//
//        deviceConfigurationWizardPage11.clickStatrOverButton();
//        EmailUtils.waitForNewEmail(emailsCountBefore, 3605, getEmailUserNameAndPassword()[0], getEmailUserNameAndPassword()[1]);
//        
//        newPropertyValue = oracleConnector.getDeviceSettingValueByDeviceIdAndParameterCd(deviceId, propertyId);
//        Assert.assertEquals(propertyValue, newPropertyValue);
//        
//        devicesMenu.searchDevice(deviceSerialNumber);
//         paymentConfigurationTab = new PaymentConfigurationTab(driver);  
//        paymentConfigurationTab.clickPaymentConfigurationTab();
//        paymentConfigurationTab.clickPaymentTypesListButton();
//        
//        paymentConfigurationTab.verifyPaymentTypePresence(paymentType);
        
        
    }
}
