package Dms.Devices.AdvancedTools;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.Ignore;

import com.usatech.test.ClientEmulatorDevTest;

import helper.Core;
import helper.EmailUtils;
import helper.OracleConnector;
import helper.PostgresqlConnector;
import pages.LeftPanel;
import pages.Dms.Devices.DeviceConfigurationTab;
import pages.Dms.Devices.DeviceConfigurationWizardPage1;
import pages.Dms.Devices.DeviceConfigurationWizardPage10;
import pages.Dms.Devices.DeviceConfigurationWizardPage11;
import pages.Dms.Devices.DeviceConfigurationWizardPage2;
import pages.Dms.Devices.DeviceConfigurationWizardPage3;
import pages.Dms.Devices.DeviceConfigurationWizardPage4;
import pages.Dms.Devices.DeviceConfigurationWizardPage5;
import pages.Dms.Devices.DeviceConfigurationWizardPage7;
import pages.Dms.Devices.DeviceConfigurationWizardPage8;
import pages.Dms.Devices.DeviceConfigurationWizardPage9;
import pages.Dms.Devices.DeviceMessagesPage;
import pages.Dms.Devices.FileTransferTab;
import pages.Dms.Devices.PaymentConfigurationTab;
import pages.Dms.LeftPanel.DevicesMenu;

public class DeviceMessagesTest extends Core {
	
	@Test
	@Ignore
    public void USAT_556_DMS_Devices_AdvancedTools_DeviceMessages() throws Exception {

        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();
//        PostgresqlConnector postgresqlConnector = new PostgresqlConnector(getEnvironment());
//        postgresqlConnector.getConnectionStatement();
        
      	DevicesMenu devicesMenu = new DevicesMenu(driver);
        devicesMenu.openDeviceMessages();
        
        String deviceSerialNumber= "VJ011000014";
        String env = getEnvironment();
        String deviceName = "TD004218";
        if(env.equalsIgnoreCase("int")) 
        {
        	deviceName = "TD002331";
        }
        
        DeviceMessagesPage deviceMessagesPage = new DeviceMessagesPage(driver);
        deviceMessagesPage.setStartDateTimeToNow();
        deviceMessagesPage.setEndDateTimeToTomorrow();
        deviceMessagesPage.setDeviceNameOrSerial(deviceSerialNumber);
        
        ClientEmulatorDevTest clientEmulatorDevTest = new ClientEmulatorDevTest();
		String CC_DATA =  "4173270594781389=25121019999888877776";
		OracleConnector oracleConnector = new OracleConnector(getEnvironment());   
		String encKeyFormDb= oracleConnector.getEncryptionKey(deviceName);		
		clientEmulatorDevTest.testSell(getEnvironment(), CC_DATA, deviceSerialNumber, deviceName, encKeyFormDb);
		
        deviceMessagesPage.clickSubmitButton();
       
    }
}
