package Dms.Devices;

import helper.Core;
import org.junit.Assert;
import org.junit.Test;
import pages.Dms.Devices.DeviceListPage;
import pages.UsaLive.Administration.Devices.DeviceConfigurationPage;
import pages.UsaLive.Administration.Devices.DeviceProfilePage;
import pages.UsaLive.Administration.Devices.DevicesPage;
import pages.Dms.Devices.DeviceSettingsTab;
import pages.Dms.LeftPanel.DevicesMenu;
import pages.UsaLive.LeftPanel;
import pages.UsaLive.LogInPage;
//import pages.UsaLive.LogInPage;

/**
 * Created by ahachikyan on 3/2/2016.
 */
public class FiltrationTest extends Core {

    @Test
    public void USAT_9_DMS_Devices_ViewDevices_byType() throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();
        String deviceType = "ePort Edge/G9";
        DevicesMenu devicesMenu = new DevicesMenu(driver);
        devicesMenu.viewByType(deviceType);
        DeviceListPage deviceListPage = new DeviceListPage(driver);
        deviceListPage.verifyColumnsHeaders();
        deviceListPage.verifyDeviceTypeFiltration(deviceType);

        deviceType = "eSuds Room Controller";
        devicesMenu.viewByType(deviceType);
        deviceListPage.verifyColumnsHeaders();
        deviceListPage.verifyDeviceTypeFiltration(deviceType);

        deviceType = "G4 ePort";
        devicesMenu.viewByType(deviceType);
        deviceListPage.verifyColumnsHeaders();
        deviceListPage.verifyDeviceTypeFiltration("G4 ePort");

        deviceType = "Gx ePort";
        devicesMenu.viewByType(deviceType);
        deviceListPage.verifyColumnsHeaders();
        deviceListPage.verifyDeviceTypeFiltration(deviceType);

        deviceType = "Kiosk";
        devicesMenu.viewByType(deviceType);
        deviceListPage.verifyColumnsHeaders();
        deviceListPage.verifyDeviceTypeFiltration(deviceType);

        //Has been removed from DMS
//        deviceType = "MEI ePort";
//        devicesMenu.viewByType(deviceType);
//        deviceListPage.verifyColumnsHeaders();
//        deviceListPage.verifyDeviceTypeFiltration(deviceType);

        deviceType = "Virtual";
        devicesMenu.viewByType(deviceType);
        deviceListPage.verifyColumnsHeaders();
        deviceListPage.verifyDeviceTypeFiltration(deviceType);
    }

    @Test
    public void USAT_10_DMS_Devices_ViewDevices_byCommMethod() throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();
        DevicesMenu devicesMenu = new DevicesMenu(driver);
        DeviceListPage deviceListPage = new DeviceListPage(driver);
        String commMethod = "CDMA - US Cellular";
        devicesMenu.viewByCommMethod(commMethod);
        deviceListPage.verifyColumnsHeaders();
        deviceListPage.verifyZeroRecordsFound();

        verifyFilterByCommMethod("CDMA - Verizon");
        verifyFilterByCommMethod("Ethernet");
        verifyFilterByCommMethod("GPRS - AT&T");
        verifyFilterByCommMethod("GPRS - Eseye");

        //Only 1
        devicesMenu.viewByCommMethod("GPRS - Rogers");
        if(getEnvironment().equalsIgnoreCase("ecc"))
            deviceListPage.verifyOnlyOneRecordFound("GPRS - Rogers");
        else
            deviceListPage.verifyZeroRecordsFound();

        devicesMenu.viewByCommMethod("HSPA - AT&T");
        if(getEnvironment().equalsIgnoreCase("ecc")) {
            deviceListPage.verifyColumnsHeaders();
            deviceListPage.verifyZeroRecordsFound();
        }
//            verifyFilterByCommMethod("HSPA - AT&T");//deviceListPage.verifyOnlyOneRecordFound("HSPA - AT&T");
        else
            deviceListPage.verifyZeroRecordsFound();

        devicesMenu.viewByCommMethod("HSPA - Eseye");
        if(getEnvironment().equalsIgnoreCase("ecc"))
            verifyFilterByCommMethod("HSPA - Eseye");
        else
            deviceListPage.verifyCommMethodFiltration("HSPA - Eseye");

        //Zero
        devicesMenu.viewByCommMethod("HSPA - Rogers");
        deviceListPage.verifyColumnsHeaders();
        deviceListPage.verifyZeroRecordsFound();
        //Zero
        devicesMenu.viewByCommMethod("HSPA5 - AT&T");
        deviceListPage.verifyColumnsHeaders();
        deviceListPage.verifyZeroRecordsFound();
        //Zero
        devicesMenu.viewByCommMethod("HSPA5 - Eseye");
        deviceListPage.verifyColumnsHeaders();
        deviceListPage.verifyZeroRecordsFound();

        //Only 1
        devicesMenu.viewByCommMethod("POTS");
//        deviceListPage.verifyColumnsHeaders();
        if(getEnvironment().equalsIgnoreCase("ecc"))
        deviceListPage.verifyOnlyOneRecordFound("POTS");
        else
        deviceListPage.verifyZeroRecordsFound();

        verifyFilterByCommMethod("VLTE - Verizon");
    }


    @Test
    public void USAT_11_DMS_Devices_ViewDevices_byCustomer() throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();
        DevicesMenu devicesMenu = new DevicesMenu(driver);
        DeviceListPage deviceListPage = new DeviceListPage(driver);
        devicesMenu.viewByCustomer();//TODO: Remove. Workaround
        String customerPartialName = "3-D Me, LLC";
        if(getEnvironment().equalsIgnoreCase("int"))
        {
            customerPartialName = "6th Ave Electronics";
        }
        devicesMenu.viewByCustomer("#0-9", customerPartialName);
        deviceListPage.verifyColumnsHeaders();
        deviceListPage.verifyCustomerFiltration(customerPartialName);

//        devicesMenu.viewByCustomer();//TODO: Remove. Workaround
//        customerPartialName = "MEI Inc.";
//        devicesMenu.viewByCustomer("M", customerPartialName);
//        deviceListPage.verifyColumnsHeaders();
//        deviceListPage.verifyCustomerFiltration(customerPartialName);
//
//        devicesMenu.viewByCustomer();//TODO: Remove. Workaround
//        customerPartialName = "USA Technologies Distributor";
//        devicesMenu.viewByCustomer("U", customerPartialName);
//        deviceListPage.verifyColumnsHeaders();
//        deviceListPage.verifyCustomerFiltration(customerPartialName);
    }

    @Test
    public void USAT_12_DMS_Devices_ViewDevices_byLocation() throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();
        DevicesMenu devicesMenu = new DevicesMenu(driver);
        DeviceListPage deviceListPage = new DeviceListPage(driver);
//        devicesMenu.viewByLocation();//TODO: Remove. Workaround

        String locationPartialName = "128 South Rest Area";
        if(getEnvironment().equalsIgnoreCase("int"))
        {
            locationPartialName = "09-DFW C/16/19";
        }
        devicesMenu.viewByLocation("#0-9", locationPartialName);
        deviceListPage.verifyColumnsHeaders();
        deviceListPage.verifyLocationFiltration(locationPartialName);

//        devicesMenu.viewByLocation();//TODO: Remove. Workaround
//        locationPartialName = "M Street Hotel";
//        if(getEnvironment().equalsIgnoreCase("int"))
//        {
//            locationPartialName = "MCB-Portland OR US";
//        }
//        devicesMenu.viewByLocation("M", locationPartialName);
//        deviceListPage.verifyColumnsHeaders();
//        deviceListPage.verifyLocationFiltration(locationPartialName);
//
//        devicesMenu.viewByLocation();//TODO: Remove. Workaround
//        locationPartialName = "Xango";
//        devicesMenu.viewByLocation("X", locationPartialName);
//        deviceListPage.verifyColumnsHeaders();
//        deviceListPage.verifyLocationFiltration(locationPartialName);
    }

    @Test
    public void USAT_13_DMS_Devices_ViewDevices_byFirmwareVersion() throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();
        DevicesMenu devicesMenu = new DevicesMenu(driver);
        DeviceListPage deviceListPage = new DeviceListPage(driver);
  //      devicesMenu.viewByFirmwareVersion();//TODO: Remove. Workaround
        String firmwareVersionPartialName = "1.00.033EC";
        if(getEnvironment().equalsIgnoreCase("int"))
        {
            firmwareVersionPartialName = "1.00.033DP";
        }

        devicesMenu.viewByFirmwareVersion("ePort Edge/G9", firmwareVersionPartialName);
        deviceListPage.verifyColumnsHeaders();
        deviceListPage.verifyFirmwareVersionFiltration(firmwareVersionPartialName);

//        devicesMenu.viewByFirmwareVersion();//TODO: Remove. Workaround
//        firmwareVersionPartialName = "USA-E42 V2.2.8C";
//        devicesMenu.viewByFirmwareVersion("G4 ePort", firmwareVersionPartialName);
//        deviceListPage.verifyColumnsHeaders();
//        deviceListPage.verifyFirmwareVersionFiltration(firmwareVersionPartialName);
    }

    private void verifyFilterByCommMethod(String commMethod) throws InterruptedException
    {
        DevicesMenu devicesMenu = new DevicesMenu(driver);
        devicesMenu.viewByCommMethod(commMethod);
        DeviceListPage deviceListPage = new DeviceListPage(driver);
        deviceListPage.verifyColumnsHeaders();
        deviceListPage.verifyCommMethodFiltration(commMethod);
    }
}

