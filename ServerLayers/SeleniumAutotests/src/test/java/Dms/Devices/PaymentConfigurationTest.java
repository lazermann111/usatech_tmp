package Dms.Devices;

import difflib.Delta;
import difflib.DiffUtils;
import difflib.Patch;
import helper.Core;
import junit.framework.Assert;
import org.junit.Ignore;
import org.junit.Test;
import pages.Dms.Devices.DeviceListPage;
import pages.Dms.Devices.DeviceProfileTab;
import pages.Dms.Devices.DeviceSettingsTab;
import pages.Dms.Devices.PaymentConfigurationTab;
import pages.Dms.LeftPanel.DevicesMenu;

import java.io.File;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;

import static helper.FileUtils.getResoucePath;
//import pages.UsaLive.LogInPage;

/**
 * Created by ahachikyan on 3/2/2016.
 */
public class PaymentConfigurationTest extends Core {

    @Ignore
    //TODO: Compare HTML does not make sense
    @Test
    public void USAT_31_DMS_Devices_Device_Payment_Configuration_Lists_PaymentTypes() throws Exception {
        String deviceName = "TD000759";
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();
        DevicesMenu devicesMenu = new DevicesMenu(driver);
        PaymentConfigurationTab paymentConfigurationTab = new PaymentConfigurationTab(driver);


        if(getEnvironment().equalsIgnoreCase("int"))
        {
            deviceName = "TD001241";
        }
        devicesMenu.searchDeviceByTypeAndValue("Device Name", deviceName);

        paymentConfigurationTab.clickPaymentConfigurationTab();
        paymentConfigurationTab.clickPaymentTypesListButton();
        String s = paymentConfigurationTab.getPaymentTypesInnerHtml();
        String[] actualInnerHtml  = paymentConfigurationTab.getPaymentTypesInnerHtml().split("\n");;
        List<String> actual = Arrays.asList(actualInnerHtml);

        String expectedFilePath = getResoucePath("deviceInfo/") + "paymentTypes" + deviceName + "_exp.txt";
        List<String> expected = Files.readAllLines(new File(expectedFilePath).toPath())/*.toString().split("/n")*/;

        Patch patch =  DiffUtils.diff(actual, expected);
        for (Delta delta: patch.getDeltas()) {
            System.out.println(delta);
        }
        Assert.assertEquals("Current inner HTML differs from expected. See difference at the top", 0,patch.getDeltas().size() );
    }

    @Ignore
    //TODO: Compare HTML does not make sense
    @Test
    public void USAT_32_DMS_Devices_Device_Payment_Configuration_Lists_TransactionHistory() throws Exception {
        String deviceName = "TD000759";
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();
        DevicesMenu devicesMenu = new DevicesMenu(driver);
        PaymentConfigurationTab paymentConfigurationTab = new PaymentConfigurationTab(driver);


        if(getEnvironment().equalsIgnoreCase("int"))
        {
            deviceName = "TD001241";
        }
        devicesMenu.searchDeviceByTypeAndValue("Device Name", deviceName);

        paymentConfigurationTab.clickPaymentConfigurationTab();

        paymentConfigurationTab.setEventHistoryFromDate("07/01/2014");
        paymentConfigurationTab.setEventHistoryFromTime("09:00:00");
        paymentConfigurationTab.setEventHistoryToDate("7/20/2015");
        paymentConfigurationTab.setEventHistoryToTime("09:00:00");

        paymentConfigurationTab.clickTransactionHistoryListButton();
        String s = paymentConfigurationTab.getTransactionHistoryInnerHtml();
        String[] actualInnerHtml  = paymentConfigurationTab.getTransactionHistoryInnerHtml().split("\n");;
        List<String> actual = Arrays.asList(actualInnerHtml);

        String expectedFilePath = getResoucePath("deviceInfo/") + "transactionHistory" + deviceName + "_exp.txt";
        List<String> expected = Files.readAllLines(new File(expectedFilePath).toPath())/*.toString().split("/n")*/;

        Patch patch =  DiffUtils.diff(actual, expected);
        for (Delta delta: patch.getDeltas()) {
            System.out.println(delta);
        }
        Assert.assertEquals("Current inner HTML differs from expected. See difference at the top", 0,patch.getDeltas().size() );
    }

}

