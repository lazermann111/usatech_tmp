package Dms.Devices;

import difflib.Delta;
import difflib.DiffUtils;
import difflib.Patch;
import helper.Core;
import helper.OracleConnector;
import junit.framework.Assert;

import org.junit.Ignore;
import org.junit.Test;

import com.usatech.test.ClientEmulatorDevTest;

import pages.Dms.Devices.DeviceSettingsTab;
import pages.Dms.LeftPanel.DevicesMenu;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;

import static helper.FileUtils.getResoucePath;

/**
 * Created by ahachikyan on 3/2/2016.
 */
public class DeviceSettingsTest extends Core {

	 @Test
	 @Ignore
	    public void deviceConfigurationBlacklist() throws Exception {
	      

			ClientEmulatorDevTest clientEmulatorDevTest = new ClientEmulatorDevTest();	
//			String CC_DATA =  "6396212016817467620=2112008374368";
//			String maskedCC_DATA="639621*********7620";
//			String cardId = "1000597421";
			String CC_DATA = "4276380086871717=12102011511101700000"; //Svetlana`s expired visa credit card
			
//			String cardId = "1000597421";
			String deviceSerialNumber = "VJ011000114" ;//ECC
			String deviceName = "TD004966";
			
	for(int i=0; i<5; i++)
	{
		System.out.println("Round #=" + i);
			OracleConnector oracleConnector = new OracleConnector(getEnvironment());
			String encKeyFormDb= oracleConnector.getEncryptionKey(deviceName);
			clientEmulatorDevTest.testSell(getEnvironment(), CC_DATA,  deviceSerialNumber, deviceName, encKeyFormDb);
			Thread.sleep(3000);
	}
	    }
	 
	 //TODO: To be completed

//    @Test
//    public void deviceSettings() throws Exception {
//        String deviceName = "TD000759";
//
//        openDms();
//        new pages.Dms.LogInPage(driver).logInAsCommonUser();
//        DevicesMenu devicesMenu = new DevicesMenu(driver);
//        DeviceSettingsTab deviceSettingsTab = new DeviceSettingsTab(driver);
//
//
//        if(getEnvironment().equalsIgnoreCase("int"))
//        {
//            deviceName = "TD001241";
//        }
//
//            devicesMenu.searchDeviceByTypeAndValue("Device Name", deviceName);
//
//            deviceSettingsTab.clickDeviceSettingsTab();
//            deviceSettingsTab.setEventHistoryFromDate("07/01/2014");
//            deviceSettingsTab.setEventHistoryFromTime("09:00:00");
//            deviceSettingsTab.setEventHistoryToDate("7/20/2015");
//            deviceSettingsTab.setEventHistoryToTime("09:00:00");
//            deviceSettingsTab.clickListAllButton();
//            String s = deviceSettingsTab.getInnerHtml();
//            String[] actualInnerHtml  = deviceSettingsTab.getInnerHtml().split("\n");;
//            List<String> actual = Arrays.asList(actualInnerHtml);
//
//            String expectedFilePath = getResoucePath("deviceInfo/") + "deviceSettings" + deviceName + "_exp.txt";
//            List<String> expected = Files.readAllLines(new File(expectedFilePath).toPath())/*.toString().split("/n")*/;
//
//            Patch patch =  DiffUtils.diff(actual, expected);
//            for (Delta delta: patch.getDeltas()) {
//                System.out.println(delta);
//            }
//            if(patch.getDeltas().size()!=0)
//            {
//                String actualFilePath = getResoucePath("deviceInfo/") + "deviceSettings" + deviceName + "_act.txt";
//
//                File file = new File(actualFilePath);
//                FileWriter fw = new FileWriter(file.getAbsoluteFile());
//                BufferedWriter bw = new BufferedWriter(fw);
//                bw.write(s);
//                bw.close();
//                Assert.fail("Current inner HTML differs from expected. See difference at the top and actual file at " + actualFilePath + "/n Expected:" + expectedFilePath + "/n Actual:" + actualFilePath);
//
//            }
////            Assert.assertEquals("Current inner HTML differs from expected. See difference at the top", 0,patch.getDeltas().size() );
//
//    }

}

