package Dms.Devices;

import static org.junit.Assert.assertThat;

import org.junit.Test;

import helper.Core;
import pages.Dms.Devices.DeviceProfileTab;
import pages.Dms.LeftPanel.DevicesMenu;
import static org.hamcrest.CoreMatchers.containsString;


public class DeviceProfile extends Core {

	@Test
	public void USAT28_DMS_Devices_Device_DeviceProfile_ePortProgramandTerminalsList() throws InterruptedException {
		openDms();
		
		DeviceProfileTab deviceProfileTab = new DeviceProfileTab(driver);
		new pages.Dms.LogInPage(driver).logInAsCommonUser();
		DevicesMenu devicesMenu = new DevicesMenu(driver);
		devicesMenu.viewByType("Kiosk");
		new pages.Dms.Devices.DeviceListPage(driver).clickAnyDeviceName();
		deviceProfileTab.clickListEPortDealerAndTerminalsButton();
		deviceProfileTab.clickChangeProgramButton();
		assertThat(new pages.Dms.Devices.ChangeDealerPage(driver).getTitle(), containsString("changeDealer"));
		devicesMenu.viewByType("Kiosk");
		new pages.Dms.Devices.DeviceListPage(driver).clickAnyDeviceName();
		deviceProfileTab.clickListEPortDealerAndTerminalsButton();
		deviceProfileTab.clickEditTerminalButton();
		assertThat(new pages.Dms.Devices.EditTerminalPage(driver).getTitle(), containsString("editTerminals"));
	}
	
	@Test
	public void USAT29_DMS_Devices_Device_DeviceProfile_AuthorizationLocationandHostsList() throws InterruptedException {
		String DMSCustomerValue;
		
		openDms();		
		DeviceProfileTab deviceProfileTab = new DeviceProfileTab(driver);
		new pages.Dms.LogInPage(driver).logInAsCommonUser();
		DevicesMenu devicesMenu = new DevicesMenu(driver);
		devicesMenu.viewByType("Kiosk");
		new pages.Dms.Devices.DeviceListPage(driver).clickAnyDeviceName();
		deviceProfileTab.clickListAuthorizationLocationAndHostsButton();
		DMSCustomerValue = deviceProfileTab.getDmsCustomerALHValue();
		deviceProfileTab.clickChangeCustomerALHButton();
//		deviceProfileTab.clickListEPortDealerAndTerminalsButton();
//		deviceProfileTab.clickChangeProgramButton();
//		assertThat(new pages.Dms.Devices.ChangeDealerPage(driver).getTitle(), containsString("changeDealer"));
//		devicesMenu.viewByType("Kiosk");
//		new pages.Dms.Devices.DeviceListPage(driver).clickAnyDeviceName();
//		deviceProfileTab.clickListEPortDealerAndTerminalsButton();
//		deviceProfileTab.clickEditTerminalButton();
//		assertThat(new pages.Dms.Devices.EditTerminalPage(driver).getTitle(), containsString("editTerminals"));
	}
}
