package Dms.Devices;


import helper.Core;
import org.junit.Ignore;
import org.junit.Test;
import pages.Dms.Devices.*;
import pages.Dms.LeftPanel.DevicesMenu;

/**
 * Created by ahachikyan on 3/2/2016.
 */
public class OracleMigrationSmokeTest extends Core {

//    @Test
//    public void deviceProfileTab() throws Exception {
//        openDms();
//        new pages.Dms.LogInPage(driver).logInAsCommonUser();
//        DevicesMenu devicesMenu = new DevicesMenu(driver);
//
////        String deviceName = "";
////        devicesMenu.searchDeviceByTypeAndValue("Serial Number", deviceName);
//        String deviceType = "ePort Edge/G9";
//        devicesMenu.viewByType(deviceType);
//        DeviceListPage deviceListPage = new DeviceListPage(driver);
//        deviceListPage.clickAnyDeviceName();
//
//        DeviceProfileTab deviceProfileTab = new DeviceProfileTab(driver);
//        deviceProfileTab.clickListAllButton();
//        deviceProfileTab.clickListLastCallDatesButton();
//
//        deviceProfileTab.clickSerialNumberLink();
//        deviceProfileTab.clickStartOverButton();
//        driver.navigate().back();
//        driver.navigate().back();
//
//        deviceProfileTab.clickListCallsButton();
//        driver.navigate().back();
//
//        deviceProfileTab.clickToggleButton();
//        driver.switchTo().alert().dismiss();
//
////        deviceProfileTab.clickListUSALiveLocationButton();
//        deviceProfileTab.clickListEPortDealerAndTerminalsButton();
//        deviceProfileTab.clickListAuthorizationLocationAndHostsButton();
//
//        deviceProfileTab.clickListAllLink();
//        deviceListPage.clickAnyDeviceName();
//
//        deviceProfileTab.clickListSaleRepsAndAffiliationsButton();
//        deviceProfileTab.clickListCallsButton();
//        deviceProfileTab.clickListCallsButton();
//    }
//
//
//    @Test
//    public void paymentConfigurationTab() throws Exception {
//        openDms();
//        new pages.Dms.LogInPage(driver).logInAsCommonUser();
//        DevicesMenu devicesMenu = new DevicesMenu(driver);
//
////        String deviceName = "";
////        devicesMenu.searchDeviceByTypeAndValue("Serial Number", deviceName);
//        String deviceType = "ePort Edge/G9";
//        devicesMenu.viewByType(deviceType);
//        DeviceListPage deviceListPage = new DeviceListPage(driver);
//        deviceListPage.clickAnyDeviceName();
//
//        PaymentConfigurationTab paymentConfigurationTab = new PaymentConfigurationTab(driver);
//
//        paymentConfigurationTab.clickPaymentConfigurationTab();
//        paymentConfigurationTab.clickTransactionHistoryListButton();
//        paymentConfigurationTab.clickPaymentTypesListButton();
//        paymentConfigurationTab.clickEditSettingsButton();
//        paymentConfigurationTab.clickGoBackToDeviceProfileButton();
//        paymentConfigurationTab.clickReorderButton();
//        paymentConfigurationTab.clickButtonByValue("Whitelist/Blacklist");
//        paymentConfigurationTab.clickGoBackToDeviceProfileButton();
//        paymentConfigurationTab.clickButtonByValue("Import Template");
//        paymentConfigurationTab.clickGoBackToDeviceProfileButton();
//        paymentConfigurationTab.clickButtonByValue("Add New / Reactivate Disabled Type");
//        paymentConfigurationTab.clickGoBackToDeviceProfileButton();
//    }
//
//
//    @Test
//    public void deviceConfigurationTab() throws Exception {
//        openDms();
//        new pages.Dms.LogInPage(driver).logInAsCommonUser();
//        DevicesMenu devicesMenu = new DevicesMenu(driver);
//
////        String deviceName = "";
////        devicesMenu.searchDeviceByTypeAndValue("Serial Number", deviceName);
//        String deviceType = "ePort Edge/G9";
//        devicesMenu.viewByType(deviceType);
//
//        DeviceListPage deviceListPage = new DeviceListPage(driver);
//        deviceListPage.clickAnyDeviceName();
//
//        DeviceConfigurationTab deviceConfigurationTab = new DeviceConfigurationTab(driver);
//
//        deviceConfigurationTab.clickDeviceConfigurationTab();
//        deviceConfigurationTab.clickButtonByValue("Queue Command");
//                deviceConfigurationTab.clickButtonByValue("Edit");
//        deviceConfigurationTab.clickButtonByValue("Cancel");
//        deviceConfigurationTab.clickButtonByValue("Import");
//        deviceConfigurationTab.clickButtonByValue("Cancel");
//        deviceConfigurationTab.clickButtonByValue("Backup");
//        deviceConfigurationTab.clickButtonByValue("Clone Serial Number");
//        deviceConfigurationTab.clickButtonByValue("<-- Go Back");
//        deviceConfigurationTab.clickButtonByValue("List");
//        deviceConfigurationTab.clickButtonByValue("List");
//
////        deviceConfigurationTab.clickButtonByValue("Firmware Version");
////        deviceConfigurationTab.clickButtonByValue("Upload Config");
////        deviceConfigurationTab.clickButtonByValue("Wavecom Modem Info");
////        deviceConfigurationTab.clickButtonByValue("Boot Load/App Rev");
////        deviceConfigurationTab.clickButtonByValue("Bezel Info");
////        deviceConfigurationTab.clickButtonByValue("External File Transfer");
////        deviceConfigurationTab.clickButtonByValue("Cancel");
//
//    }

//
//    @Test
//    public void deviceSettingsTab() throws Exception {
//        openDms();
//        new pages.Dms.LogInPage(driver).logInAsCommonUser();
//        DevicesMenu devicesMenu = new DevicesMenu(driver);
//
//        String deviceType = "ePort Edge/G9";
//        devicesMenu.viewByType(deviceType);
////        devicesMenu.searchDeviceByTypeAndValue("Serial Number", deviceName);
//        DeviceListPage deviceListPage = new DeviceListPage(driver);
//        deviceListPage.clickAnyDeviceName();
//
//        DeviceSettingsTab deviceSettingsTab = new DeviceSettingsTab(driver);
//
//        deviceSettingsTab.clickDeviceSettingsTab();
//        deviceSettingsTab.clickListAllButton();
//        deviceSettingsTab.clickListDeviceSettingsButton();
//        deviceSettingsTab.clickListCountersHistoryButton();
//        deviceSettingsTab.clickListEventHistoryButton();
//        deviceSettingsTab.clickListEventHistoryButton();
//    }


//    @Test
//    public void fileTransferTab() throws Exception {
//        openDms();
//        new pages.Dms.LogInPage(driver).logInAsCommonUser();
//        DevicesMenu devicesMenu = new DevicesMenu(driver);
//
////        String deviceName = "";
////        devicesMenu.searchDeviceByTypeAndValue("Serial Number", deviceName);
//        String deviceType = "ePort Edge/G9";
//        devicesMenu.viewByType(deviceType);
//        DeviceListPage deviceListPage = new DeviceListPage(driver);
//        deviceListPage.clickAnyDeviceName();
//
//        FileTransferTab fileTransferTab = new FileTransferTab(driver);
//
//        fileTransferTab.clickFileTransferTab();
//        fileTransferTab.clickListButton();
//        fileTransferTab.clickListButton();
//    }

}

