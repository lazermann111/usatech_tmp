package Dms.Devices;

import helper.Core;
import pages.Dms.Devices.DeviceListPage;
import pages.Dms.Devices.DeviceProfileTab;
import pages.Dms.Devices.DeviceSettingsTab;
import pages.Dms.LeftPanel.DevicesMenu;
import pages.UsaLive.Administration.Devices.DeviceProfilePage;

import org.junit.Test;

/**
 * Created by ahachikyan on 3/2/2016.
 */
public class SearchTest extends Core {

    @Test
    public void USAT_14_USAT_15_USAT_16_USAT_17_USAT_18_USAT_19_USAT_20_USAT_21_USAT_22_USAT_23_USAT_24_DMS_Devices_SearchBy_Serial_Number_DeviceName_CustomerName() throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();

        String serialNumber = "VJ000002000";
        String deviceName = "TD002059";
//        String deviceType = "ePort Edge/G9";
        String reportCustomer = "Buggy Bugs Emporium";
        String customerName = "Buggy Bugs Emporium";
        String location = "USAT Deerfield Demo Area";
        String firmware = "2.04.006eC";
        String firmwareInt = "2.04.002aC";
        String diagnostic = "Diagnostic=N/A";
        String pTest = "PTest 1.01.005";
        String bezelMfgr = "VivoTech";
        String bezelAppVersion = "EC5 GR 1.1";
//        String commMethod	= "HSPA - Eseye";
//        String lastActivity = "5/19/2016  5:53:48 AM";
//        String status = "Enabled";

        if(getEnvironment().equalsIgnoreCase("int"))
        {
//            serialNumber = "VJ100000000";
//            deviceName = "TD001160";
//            reportCustomer = "USA Technologies QA";
//            customerName = "Unknown";
//            location = "USA Tech Config Center";
//            firmware = "2.04.001vI";
//            diagnostic = "Diagnostic=N/A";
//            pTest = "PTest 1.01.005";
//            bezelMfgr = "OTI";
//            bezelAppVersion = "040111";
            serialNumber = "VJ100023886";
            deviceName = "TD001415";
            reportCustomer = "The Vending Acropolis";
            customerName = "The Vending Acropolis";
            location = "Acropolis Cheezyville";
            firmware = "2.04.006cI";
            diagnostic = "Diagnostic=N/A";
            pTest = "PTest 1.01.005";
            bezelMfgr = "IdTech";
            bezelAppVersion = "NEO v1.00.075.1A";
        }
//        else if(getEnvironment().equalsIgnoreCase("ecc"))
//        {
////            serialNumber = "VJ100000000";
////            deviceName = "TD001160";
////			reportCustomer = "USA Technologies QA";
////            customerName = "Unknown";
////            location = "USA Tech Config Center";
////            firmware = "2.04.001vI";
////            diagnostic = "Diagnostic=N/A";
////            pTest = "PTest 1.01.005";
////            bezelMfgr = "OTI";
////            bezelAppVersion = "040111";
//            serialNumber = "VJ100023886";
//            deviceName = "TD001394";
//            customerName = "Unknown";
//            location = "USA Tech Config Center";
//            firmware = "2.04.002eI";
//            diagnostic = "Diagnostic=N/A";
//            pTest = "PTest 1.01.005";
//            bezelMfgr = "IdTech";
//            bezelAppVersion = "NEO v1.00.008";
//        }
        DevicesMenu devicesMenu = new DevicesMenu(driver);
        DeviceListPage deviceListPage = new DeviceListPage(driver);
        DeviceProfileTab deviceProfileTab = new DeviceProfileTab(driver);

        devicesMenu.searchDeviceByTypeAndValue("Serial Number", serialNumber);
        deviceProfileTab.verifySerialNumber(serialNumber);

        devicesMenu.searchDeviceByTypeAndValue("Device Name", deviceName);
        deviceProfileTab.verifyDeviceName(deviceName);

        devicesMenu.searchDeviceByTypeAndValue("Reporting Customer", reportCustomer);
        deviceListPage.show2500RowsPerPage();
        deviceListPage.verifyReportingCustomer(reportCustomer);
        deviceListPage.clickDeviceNameLink(deviceName);
        deviceProfileTab.verifyDeviceName(deviceName);

        devicesMenu.searchDeviceByTypeAndValue("DMS Customer Name", customerName);
        deviceListPage.show2500RowsPerPage();
        deviceListPage.verifyCustomerPresence(customerName);
        deviceListPage.clickDeviceNameLink(deviceName);
        deviceProfileTab.verifyDeviceName(deviceName);
//
        devicesMenu.searchDeviceByTypeAndValue("DMS Location Name", location);
        deviceListPage.show2500RowsPerPage();
        deviceListPage.verifyLocationPresence(location);
//        deviceListPage.clickDeviceNameLink(deviceName);
//        deviceProfileTab.verifyDeviceName(deviceName);

        devicesMenu.searchDeviceByTypeAndValue("Firmware Version", firmware);
        deviceListPage.show2500RowsPerPage();

//if(getEnvironment().equalsIgnoreCase("ecc"))
    deviceListPage.verifyFirmwareVersionPresence(firmware);
//        else
//    deviceProfileTab.verifyFirmwareVersion(firmware);
        //for ecc: deviceListPage.verifyFirmwareVersionPresence(firmware);
//        deviceListPage.clickDeviceNameLink(deviceName);
//        deviceProfileTab.verifyDeviceName(deviceName);

        devicesMenu.searchDeviceByTypeAndValue("Diagnostic Version", diagnostic);
        deviceListPage.show2500RowsPerPage();
        deviceListPage.verifyDiagnosticVersionPresence(diagnostic);
        deviceListPage.clickDeviceNameLink(deviceName);
        deviceProfileTab.verifyDeviceName(deviceName);

        devicesMenu.searchDeviceByTypeAndValue("PTest Version", pTest);
        deviceListPage.show2500RowsPerPage();
        deviceListPage.verifyPTestVersionPresence(pTest);
//        deviceListPage.clickDeviceNameLink(deviceName);
//        deviceProfileTab.verifyDeviceName(deviceName);

        devicesMenu.searchDeviceByTypeAndValue("Bezel Manufacturer", bezelMfgr);
        deviceListPage.show2500RowsPerPage();
        deviceListPage.verifyBezelManufacturerPresence(bezelMfgr);
//        deviceListPage.clickDeviceNameLink(deviceName);
//        deviceProfileTab.verifyDeviceName(deviceName);

        devicesMenu.searchDeviceByTypeAndValue("Bezel App Version", bezelAppVersion);
        deviceListPage.show2500RowsPerPage();
        deviceListPage.verifyBezelAppVersionPresence(bezelAppVersion);
//        deviceListPage.clickDeviceNameLink(deviceName);
//        deviceProfileTab.verifyDeviceName(deviceName);
}


    @Test
    public void USAT_207_DMS_Devices_SearchBy_IncludingDisabled () throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();
        DevicesMenu devicesMenu = new DevicesMenu(driver);
        DeviceListPage deviceListPage = new DeviceListPage(driver);
        DeviceProfileTab deviceProfileTab = new DeviceProfileTab(driver);

        String disabledDeviceName = "WS000626";
        if(getEnvironment().equalsIgnoreCase("ecc"))
        {

            devicesMenu.searchDeviceByTypeAndValue("Device Name", disabledDeviceName);
            deviceListPage.verifyZeroRecordsFound();

            devicesMenu.clickIncludeDisabled();
            devicesMenu.searchDeviceByTypeAndValue("Device Name", disabledDeviceName);
            deviceProfileTab.verifyDeviceName(disabledDeviceName);
        }
        else
        {
            disabledDeviceName = "TD001126";
            devicesMenu.searchDeviceByTypeAndValue("Device Name", disabledDeviceName);
            deviceProfileTab.verifyStatus("Enabled");

            devicesMenu.clickIncludeDisabled();
            devicesMenu.searchDeviceByTypeAndValue("Device Name", disabledDeviceName);
            deviceListPage.verifyDeviceNamePresence(disabledDeviceName);
        }




    }
}

