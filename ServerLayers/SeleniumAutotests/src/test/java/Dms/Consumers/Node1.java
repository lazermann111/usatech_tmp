package Dms.Consumers;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Driver;
import org.testng.annotations.Test;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class Node1 {

	
	@Test
	public void testDms() throws MalformedURLException
	{	
		WebDriver driver;
		String nodeUrl;
		
		DesiredCapabilities desiredCapabilities  =  DesiredCapabilities.chrome();
		desiredCapabilities.setBrowserName("chrome"); 
		desiredCapabilities.setPlatform(Platform.WINDOWS);
		nodeUrl= "http://10.0.1.31:4444/wd/hub";
		driver = new RemoteWebDriver(new URL(nodeUrl), desiredCapabilities);
		
		driver.get("https://dms-int.usatech.com:8443/home.i");
		
		driver.quit();
	}
	

}
