package Dms.Consumers;

import helper.Core;
import org.junit.Ignore;
import org.junit.Test;
import pages.Dms.Consumers.ConsumerAccountPage;
import pages.Dms.Consumers.ConsumerSearchPage;
import pages.Dms.LeftPanel.ConsumersMenu;

import java.util.Date;

/**
 * Created by ahachikyan on 3/2/2016.
 */
public class ConsumerTest extends Core {

    @Ignore
    @Test
    public void TempPerformanceConsumerCardSearch() throws Exception {
//        String cardNumber = "6396212001484426430";
//        String modifiedCardNumber = cardNumber.replace(cardNumber.substring(6, 15), "*********");
//        String accountNumber = "";
//        String consumerId = "1120858";
        String startCardID = "311865";
        String endCardID = "311865";
//        String firstName = "Arthur";
//        String lastName = "Royce";
//        String primaryEmail = "aroyce_prepaid1@usatech.com";
//        String consumerAccountType = "Prepaid Account";
//        String consumerAccountSubtype = "Prepaid Account - USAT Serviced Prepaid";


        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();

        ConsumersMenu consumersMenu = new ConsumersMenu(driver);
        ConsumerSearchPage consumerSearchPage = new ConsumerSearchPage(driver);
        ConsumerAccountPage consumerAccountPage = new ConsumerAccountPage(driver);
        //------------------------------------

        //By Card Number
        consumersMenu.consumerCardSearch();
//        consumerSearchPage.setCardNumber(cardNumber);

//        consumerSearchPage.clickSearchButton();

        //        consumerAccountPage.verifyAccountID(accountNumber);
        consumerSearchPage.setStartCardID(startCardID);
        consumerSearchPage.setEndCardID(endCardID);
        Date date = new Date();
        long beforeTime = date.getTime();
        logger.info("Clicking search");
        consumerSearchPage.clickSearchButton();
//        consumerAccountPage.verifyCardNumber(modifiedCardNumber);


        consumerAccountPage.verifyCardID(startCardID);

        consumerAccountPage.verifyCardID(endCardID);
        logger.info("Got result");
        date = new Date();
        long afterTime = date.getTime();
        long deltaTime = afterTime - beforeTime;
        logger.info("Duration = " + deltaTime/1000 + " sec");
    }
    @Test
    //USAT-54 DMS. Consumers. Consumer Search
    public void USAT_54_DMS_Consumers_ConsumerSearch() throws Exception {
//        String cardNumber = "6396212001551837030";
//        String modifiedCardNumber = cardNumber.replace(cardNumber.substring(6,15), "*********");
//        String accountNumber = "";
//        String consumerId = "1120858";
//        String startCardID = "125667";
//        String endCardID = "125667";
//        String firstName = "Arthur";
//        String lastName = "Royce";
//        String primaryEmail = "aroyce_prepaid1@usatech.com";
//        String consumerAccountType = "Prepaid Account";
//        String consumerAccountSubtype = "Prepaid Account - Operator Serviced Prepaid";
        String cardNumber = "6396212001484426430";
        String modifiedCardNumber = cardNumber.replace(cardNumber.substring(6,15), "*********");
        String accountNumber = "";
        String consumerId = "1120858";
        String startCardID = "125795";
        String endCardID = "125795";
        String firstName = "Arthur";
        String lastName = "Royce";
        String primaryEmail = "aroyce_prepaid1@usatech.com";
        String consumerAccountType = "Prepaid Account";
        String consumerAccountSubtype = "Prepaid Account - USAT Serviced Prepaid";
//        String consumerAccountSubtype = "Prepaid Account - Operator Serviced Prepaid";

        if(getEnvironment().equalsIgnoreCase("int"))
        {
//            cardNumber = "6396210011275918513";
//            modifiedCardNumber = cardNumber.replace(cardNumber.substring(6,15), "*********");
//            accountNumber = "";
//            consumerId = "42493";
//            startCardID = "1000353650";
//            endCardID = "1000353650";
//            firstName = "Virtual Consumer";
//            lastName = "USA Tech Demo - USAT Internal Merchant";
//            primaryEmail = "merchant_362@usatech.com";
            cardNumber = "6396212003698938242";
            modifiedCardNumber = cardNumber.replace(cardNumber.substring(6,15), "*********");
            accountNumber = "";
            consumerId = "44386";
            startCardID = "125722";
            endCardID = "125722";
            firstName = "Arthur";
            lastName = "Royce";
            primaryEmail = "aroyce2@usatech.com";
            consumerAccountType = "Prepaid Account";
            consumerAccountSubtype = "Prepaid Account - USAT Serviced Prepaid";
        }

        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();

        ConsumersMenu consumersMenu = new ConsumersMenu(driver);
//        consumersMenu.consumerSearch();
        ConsumerSearchPage consumerSearchPage = new ConsumerSearchPage(driver);
        ConsumerAccountPage consumerAccountPage = new ConsumerAccountPage(driver);

        //------------------------------------
        //By Card Type
        consumersMenu.consumerSearch();

        consumerSearchPage.setConsumerAccountType(consumerAccountType);
        consumerSearchPage.setConsumerAccountSubtype(consumerAccountSubtype);

        consumerSearchPage.clickSearchButton();

        consumerSearchPage.show2500RowsPerPage();

        consumerSearchPage.verifySearchResultsConsumerID(consumerId);
        consumerSearchPage.verifySearchResultsFirstName(firstName);
        consumerSearchPage.verifySearchResultsLastName(lastName);
        consumerSearchPage.verifySearchResultsEmail(primaryEmail);

        consumerSearchPage.clickConsumerId(consumerId);

        consumerSearchPage.verifyConsumerInfoConsumerId(consumerId);
        consumerSearchPage.verifyConsumerInfoFirstName(firstName);
        consumerSearchPage.verifyConsumerInfoLastName(lastName);
        consumerSearchPage.verifyConsumerPrimaryEmail(primaryEmail);

        consumerSearchPage.clickCardCode(modifiedCardNumber);

//        consumerAccountPage.verifyAccountID(accountNumber);
        consumerAccountPage.verifyCardNumber(modifiedCardNumber);
        consumerAccountPage.verifyCardID(startCardID);
        consumerAccountPage.verifyCardID(endCardID);
        consumerAccountPage.verifyConsumerID(consumerId);

        //------------------------------------

        //By Email
        consumersMenu.consumerSearch();

        consumerSearchPage.setPrimaryEmail(primaryEmail);

        consumerSearchPage.clickSearchButton();

        consumerSearchPage.verifySearchResultsConsumerID(consumerId);
        consumerSearchPage.verifySearchResultsFirstName(firstName);
        consumerSearchPage.verifySearchResultsLastName(lastName);
        consumerSearchPage.verifySearchResultsEmail(primaryEmail);

        consumerSearchPage.clickConsumerId(consumerId);

        consumerSearchPage.verifyConsumerInfoConsumerId(consumerId);
        consumerSearchPage.verifyConsumerInfoFirstName(firstName);
        consumerSearchPage.verifyConsumerInfoLastName(lastName);
        consumerSearchPage.verifyConsumerPrimaryEmail(primaryEmail);

        consumerSearchPage.clickCardCode(modifiedCardNumber);

//        consumerAccountPage.verifyAccountID(accountNumber);
        consumerAccountPage.verifyCardNumber(modifiedCardNumber);
        consumerAccountPage.verifyCardID(startCardID);
        consumerAccountPage.verifyCardID(endCardID);
        consumerAccountPage.verifyConsumerID(consumerId);

        //------------------------------------

        //BY first name and last name
        consumersMenu.consumerSearch();

        consumerSearchPage.setFirstName(firstName);
        consumerSearchPage.setLastName(lastName);

        consumerSearchPage.clickSearchButton();

        consumerSearchPage.verifySearchResultsConsumerID(consumerId);
        consumerSearchPage.verifySearchResultsFirstName(firstName);
        consumerSearchPage.verifySearchResultsLastName(lastName);
        consumerSearchPage.verifySearchResultsEmail(primaryEmail);

        consumerSearchPage.clickConsumerId(consumerId);

//        consumerSearchPage.verifyConsumerInfoConsumerId(consumerId);
        consumerSearchPage.verifyConsumerInfoFirstName(firstName);
        consumerSearchPage.verifyConsumerInfoLastName(lastName);
//        consumerSearchPage.verifyConsumerPrimaryEmail(primaryEmail);

        consumerSearchPage.clickCardCode(modifiedCardNumber);

//        consumerAccountPage.verifyAccountID(accountNumber);
//        consumerAccountPage.verifyCardNumber(modifiedCardNumber);
//        consumerAccountPage.verifyCardID(startCardID);
//        consumerAccountPage.verifyCardID(endCardID);
//        consumerAccountPage.verifyConsumerID(consumerId);
        //------------------------------------

        //By Card Number
        consumersMenu.consumerSearch();

        consumerSearchPage.setCardNumber(cardNumber);

        consumerSearchPage.clickSearchButton();

        consumerSearchPage.verifySearchResultsConsumerID(consumerId);
        consumerSearchPage.verifySearchResultsFirstName(firstName);
        consumerSearchPage.verifySearchResultsLastName(lastName);
        consumerSearchPage.verifySearchResultsEmail(primaryEmail);

        consumerSearchPage.clickConsumerId(consumerId);

        consumerSearchPage.verifyConsumerInfoConsumerId(consumerId);
        consumerSearchPage.verifyConsumerInfoFirstName(firstName);
        consumerSearchPage.verifyConsumerInfoLastName(lastName);
        consumerSearchPage.verifyConsumerPrimaryEmail(primaryEmail);

        consumerSearchPage.clickCardCode(modifiedCardNumber);

//        consumerAccountPage.verifyAccountID(accountNumber);
        consumerAccountPage.verifyCardNumber(modifiedCardNumber);
        consumerAccountPage.verifyCardID(startCardID);
        consumerAccountPage.verifyCardID(endCardID);
        consumerAccountPage.verifyConsumerID(consumerId);

        //------------------------------------
//        By start and end Card ID
        consumersMenu.consumerSearch();

        consumerSearchPage.setStartCardID(startCardID);
        consumerSearchPage.setEndCardID(endCardID);

        consumerSearchPage.clickSearchButton();

        consumerSearchPage.verifySearchResultsConsumerID(consumerId);
        consumerSearchPage.verifySearchResultsFirstName(firstName);
        consumerSearchPage.verifySearchResultsLastName(lastName);
        consumerSearchPage.verifySearchResultsEmail(primaryEmail);

        consumerSearchPage.clickConsumerId(consumerId);

        consumerSearchPage.verifyConsumerInfoConsumerId(consumerId);
        consumerSearchPage.verifyConsumerInfoFirstName(firstName);
        consumerSearchPage.verifyConsumerInfoLastName(lastName);
        consumerSearchPage.verifyConsumerPrimaryEmail(primaryEmail);

        consumerSearchPage.clickCardCode(modifiedCardNumber);

//        consumerAccountPage.verifyAccountID(accountNumber);
        consumerAccountPage.verifyCardNumber(modifiedCardNumber);
        consumerAccountPage.verifyCardID(startCardID);
        consumerAccountPage.verifyCardID(endCardID);
        consumerAccountPage.verifyConsumerID(consumerId);

    }

    @Test
    public void USAT55_DMS_Consumer_ConsumerCardSearch() throws Exception {
//        String cardNumber = "6396212001551837030";
//        String modifiedCardNumber = cardNumber.replace(cardNumber.substring(6, 15), "*********");
//        String accountNumber = "";
//        String consumerId = "1120858";
//        String startCardID = "125667";
//        String endCardID = "125667";
//        String firstName = "Arthur";
//        String lastName = "Royce";
//        String primaryEmail = "aroyce_prepaid1@usatech.com";
//        String consumerAccountType = "Prepaid Account";
//        String consumerAccountSubtype = "Prepaid Account - Operator Serviced Prepaid";
        String cardNumber = "6396212001484426430";
        String modifiedCardNumber = cardNumber.replace(cardNumber.substring(6,15), "*********");
        String accountNumber = "";
        String consumerId = "1120858";
        String startCardID = "125795";
        String endCardID = "125795";
        String firstName = "Arthur";
        String lastName = "Royce";
        String primaryEmail = "aroyce_prepaid1@usatech.com";
        String consumerAccountType = "Prepaid Account";
        String consumerAccountSubtype = "Prepaid Account - USAT Serviced Prepaid";

        if (getEnvironment().equalsIgnoreCase("int")) {
//            cardNumber = "6396210011275918513";
//            modifiedCardNumber = cardNumber.replace(cardNumber.substring(6, 15), "*********");
//            accountNumber = "";
//            consumerId = "42493";
//            startCardID = "1000353650";
//            endCardID = "1000353650";
//            firstName = "Virtual Consumer";
//            lastName = "USA Tech Demo - USAT Internal Merchant";
//            primaryEmail = "merchant_362@usatech.com";
            cardNumber = "6396212003698938242";
            modifiedCardNumber = cardNumber.replace(cardNumber.substring(6,15), "*********");
            accountNumber = "";
            consumerId = "44386";
            startCardID = "125722";
            endCardID = "125722";
            firstName = "Arthur";
            lastName = "Royce";
            primaryEmail = "aroyce2@usatech.com";
            consumerAccountType = "Prepaid Account";
            consumerAccountSubtype = "Prepaid Account - USAT Serviced Prepaid";
        }

        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();

        ConsumersMenu consumersMenu = new ConsumersMenu(driver);
        ConsumerSearchPage consumerSearchPage = new ConsumerSearchPage(driver);
        ConsumerAccountPage consumerAccountPage = new ConsumerAccountPage(driver);
        //------------------------------------

        //By Card Number
        consumersMenu.consumerCardSearch();
        consumerSearchPage.setCardNumber(cardNumber);

        consumerSearchPage.clickSearchButton();

        //        consumerAccountPage.verifyAccountID(accountNumber);
        consumerAccountPage.verifyCardNumber(modifiedCardNumber);
        consumerAccountPage.verifyCardID(startCardID);
        consumerAccountPage.verifyCardID(endCardID);
        consumerAccountPage.verifyConsumerID(consumerId);
        consumerAccountPage.verifyConsumerAccountType(consumerAccountType);

        //------------------------------------
        consumersMenu.consumerCardSearch();
        //By Card ID
        consumerSearchPage.setStartCardID(startCardID);
        consumerSearchPage.setEndCardID(endCardID);

        consumerSearchPage.clickSearchButton();

        //        consumerAccountPage.verifyAccountID(accountNumber);
        consumerAccountPage.verifyCardNumber(modifiedCardNumber);
        consumerAccountPage.verifyCardID(startCardID);
        consumerAccountPage.verifyCardID(endCardID);
        consumerAccountPage.verifyConsumerID(consumerId);
        consumerAccountPage.verifyConsumerAccountType(consumerAccountType);

        //------------------------------------
        consumersMenu.consumerCardSearch();
        //By Card Type
        consumerSearchPage.setConsumerAccountType(consumerAccountType);
        consumerSearchPage.setConsumerAccountSubtype(consumerAccountSubtype);

        consumerSearchPage.clickSearchButton();

        consumerSearchPage.show2500RowsPerPage();

        consumerSearchPage.verifySearchResultsCardId(startCardID);

        consumerSearchPage.clickAccountIdByCardId(startCardID);

        //        consumerAccountPage.verifyAccountID(accountNumber);
        consumerAccountPage.verifyCardNumber(modifiedCardNumber);
        consumerAccountPage.verifyCardID(startCardID);
        consumerAccountPage.verifyCardID(endCardID);
        consumerAccountPage.verifyConsumerID(consumerId);
        consumerAccountPage.verifyConsumerAccountType(consumerAccountType);


    }

    }

