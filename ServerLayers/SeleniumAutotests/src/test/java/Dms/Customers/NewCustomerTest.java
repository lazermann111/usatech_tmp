package Dms.Customers;

import helper.Core;

import org.junit.Ignore;
import org.junit.Test;
import pages.Dms.Customers.CustomerListPage;
import pages.Dms.Customers.EditCustomerPage;
import pages.Dms.LeftPanel.CustomersMenu;

import java.util.Random;
//import pages.UsaLive.LogInPage;

/**
 * Created by ahachikyan on 3/2/2016.
 */
public class NewCustomerTest extends Core {

//	@Ignore
    @Test
    public void USAT_50_USAT51_USAT52_USAT53_DMS_Customers_NewCustomer() throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();
        Integer randomPart = new Random().ints(1, 0, 1000000).findFirst().getAsInt();
        String name = "TestCustomer" + randomPart;
        String addressLine1_1 = "Address Line 1_1";
        String addressLine2_1 = "Address Line 2_1";
        String addressLine1_2 = "Address Line 1_2";
        String addressLine2_2 = "Address Line 2_2";
        String type1 = "End User";
        String type2 = "Distributor";
        String postalCode1 = "02108";
        String postalCode2 = "10007";
        String state1 = "Massachusetts: MA, US";
        String state2 = "New York: NY, US";
        String city1 = "Boston";
        String city2 = "New York";
        String userName1 = "usalive.test" + randomPart;
//        String userName2 = "usalive.test2";
        String emailAddress1 = "usalive.test1@gmail.com";
        String emailAddress2 = "usalive.test2@gmail.com";

        CustomersMenu customersMenu = new CustomersMenu(driver);
        customersMenu.newCustomerName(name);
        EditCustomerPage editCustomerPage = new EditCustomerPage(driver);
        editCustomerPage.setType(type1);
        editCustomerPage.setAddressLine1(addressLine1_1);
        editCustomerPage.setAddressLine2(addressLine2_1);
        editCustomerPage.setPostalCode(postalCode1);
        editCustomerPage.setCity(city1);
        editCustomerPage.setState(state1);
        editCustomerPage.setUserName(userName1);
        editCustomerPage.setEmailAddress(emailAddress1);
        editCustomerPage.saveNew();

        customersMenu.searchCustomer(name);

        editCustomerPage.verifyName(name);
        editCustomerPage.verifyType(type1);
        editCustomerPage.verifyAddressLine1(addressLine1_1);
        editCustomerPage.verifyAddressLine2(addressLine2_1);
        editCustomerPage.verifyPostalCode(postalCode1);
        editCustomerPage.verifyCity(city1);
        editCustomerPage.verifyState(state1);
//        editCustomerPage.verifyUserName(userName1);
//        editCustomerPage.verifyEmailAddress(emailAddress1);

        editCustomerPage.setType(type2);
        editCustomerPage.setAddressLine1(addressLine1_2);
        editCustomerPage.setAddressLine2(addressLine2_2);
        editCustomerPage.setPostalCode(postalCode2);
        editCustomerPage.setCity(city2);
        editCustomerPage.setState(state2);
//        editCustomerPage.setUserName(userName2);
//        editCustomerPage.setEmailAddress(emailAddress2);

        editCustomerPage.save();

        customersMenu.searchCustomer(name);

        editCustomerPage.verifyName(name);
        editCustomerPage.verifyType(type2);
        editCustomerPage.verifyAddressLine1(addressLine1_2);
        editCustomerPage.verifyAddressLine2(addressLine2_2);
        editCustomerPage.verifyPostalCode(postalCode2);
        editCustomerPage.verifyCity(city2);
        editCustomerPage.verifyState(state2);
//        editCustomerPage.verifyUserName(userName2);
//        editCustomerPage.verifyEmailAddress(emailAddress2);

        editCustomerPage.delete();

        //TODO: Uncomment when Known Selenium Bug will be fixed (https://bugzilla.mozilla.org/show_bug.cgi?id=1422272 )
//        customersMenu.searchCustomer(name);
//
//        editCustomerPage.verifyName(name);
//        editCustomerPage.verifyType(type2);
//        editCustomerPage.verifyAddressLine1(addressLine1_2);
//        editCustomerPage.verifyAddressLine2(addressLine2_2);
//        editCustomerPage.verifyPostalCode(postalCode2);
//        editCustomerPage.verifyCity(city2);
//        editCustomerPage.verifyState(state2);
////        editCustomerPage.verifyUserName(userName2);
////        editCustomerPage.verifyEmailAddress(emailAddress2);
//
//        editCustomerPage.undelete();
//
//        editCustomerPage.verifyName(name);
//        editCustomerPage.verifyType(type2);
//        editCustomerPage.verifyAddressLine1(addressLine1_2);
//        editCustomerPage.verifyAddressLine2(addressLine2_2);
//        editCustomerPage.verifyPostalCode(postalCode2);
//        editCustomerPage.verifyCity(city2);
//        editCustomerPage.verifyState(state2);
////        editCustomerPage.verifyUserName(userName2);
////        editCustomerPage.verifyEmailAddress(emailAddress2);
//        editCustomerPage.delete();

    }


}