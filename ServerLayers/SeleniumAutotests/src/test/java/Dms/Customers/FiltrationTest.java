package Dms.Customers;

import helper.Core;
import org.junit.Test;
import pages.Dms.Customers.CustomerListPage;
import pages.Dms.LeftPanel.CustomersMenu;
import pages.Dms.LeftPanel.DevicesMenu;

/**
 * Created by ahachikyan on 3/2/2016.
 */
public class FiltrationTest extends Core {

    @Test
    public void USAT48_DMS_Customers_ViewByType() throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();
        
        CustomersMenu customersMenu = new CustomersMenu(driver);
        CustomerListPage customereListPage = new CustomerListPage(driver);
        
        String customerType = "Distributor";
        customersMenu.viewByType(customerType);        
        customereListPage.verifyColumnsHeaders();
        customereListPage.verifyCustomerTypeFiltration(customerType);

        customerType = "End User";
        customersMenu.viewByType(customerType);
        customereListPage.verifyColumnsHeaders();
        customereListPage.verifyCustomerTypeFiltration(customerType);

        customerType = "eSuds Operator";
        customersMenu.viewByType(customerType);
        customereListPage.verifyColumnsHeaders();
        customereListPage.verifyCustomerTypeFiltration(customerType);

        customerType = "Sony PictureStation Operator";
        customersMenu.viewByType(customerType);
        customereListPage.verifyColumnsHeaders();
        customereListPage.verifyCustomerTypeFiltration(customerType);
    }
}

