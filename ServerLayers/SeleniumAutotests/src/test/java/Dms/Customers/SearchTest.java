package Dms.Customers;

import helper.Core;
import org.junit.Test;
import pages.Dms.Customers.CustomerListPage;
import pages.Dms.Customers.EditCustomerPage;
import pages.Dms.Devices.DeviceListPage;
import pages.Dms.LeftPanel.CustomersMenu;
import pages.Dms.LeftPanel.DevicesMenu;
//import pages.UsaLive.LogInPage;

/**
 * Created by ahachikyan on 3/2/2016.
 */
public class SearchTest extends Core {

    @Test
    public void USAT47_DMS_Customers_SearchByName() throws Exception {
        openDms();
        new pages.Dms.LogInPage(driver).logInAsCommonUser();
        String name = "Coca Cola";
        CustomersMenu customersMenu = new CustomersMenu(driver);
        customersMenu.searchCustomer(name);
        CustomerListPage customerListPage = new CustomerListPage(driver);
        customerListPage.verifyColumnsHeaders();
        customerListPage.verifyCustomerNameFiltration(name);

        //only 1 result
        name = "Coca Cola Boston";
        customersMenu.searchCustomer(name);
        EditCustomerPage editCustomerPage = new EditCustomerPage(driver);
        editCustomerPage.verifyName(name);
    }

}