/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.activemq;


import org.apache.activemq.broker.region.MessageReference;
import org.apache.activemq.command.ActiveMQMessage;
import org.apache.activemq.command.MessageId;
import org.apache.activemq.command.ProducerId;
import org.apache.activemq.util.BitArrayBin;

public class ActiveMQMessageAuditWithDestination extends ActiveMQMessageAudit{
	
	protected class ProducerIdWithDestination{
		protected ProducerId producerId;
		protected String destination;
		public ProducerIdWithDestination(ProducerId producerId, String destination) {
			super();
			this.producerId = producerId;
			this.destination = destination;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((destination == null) ? 0 : destination.hashCode());
			result = prime * result + ((producerId == null) ? 0 : producerId.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if(this == obj)
				return true;
			if(obj == null)
				return false;
			if(getClass() != obj.getClass())
				return false;
			final ProducerIdWithDestination other = (ProducerIdWithDestination) obj;
			if(destination == null) {
				if(other.destination != null)
					return false;
			} else if(!destination.equals(other.destination))
				return false;
			if(producerId == null) {
				if(other.producerId != null)
					return false;
			} else if(!producerId.equals(other.producerId))
				return false;
			return true;
		}
		
	}
    public ActiveMQMessageAuditWithDestination() {
        super();
    }
    
    @Override
    public boolean isDuplicate(final MessageReference message){
        MessageId id = message.getMessageId();
		String destinationName=((ActiveMQMessage)message).getDestination().toString();
        return isDuplicate(id, destinationName);
    }
    
    public synchronized boolean isDuplicate(final MessageId id, String destinationName) {
        boolean answer = false;
        
        if (id != null) {
            ProducerId pid = id.getProducerId();
            if (pid != null) {
            	ProducerIdWithDestination pidD=new ProducerIdWithDestination(pid, destinationName);
                BitArrayBin bab = map.get(pidD);
                if (bab == null) {
                    bab = new BitArrayBin(auditDepth);
                    map.put(pid, bab);
                }
                answer = bab.setBit(id.getProducerSequenceId(), true);
            }
        }
        return answer;
    }
    
    /**
     * mark this message as being received
     * 
     * @param message
     */
    public void rollback(final MessageReference message) {
        MessageId id = message.getMessageId();
        String destinationName=((ActiveMQMessage)message).getDestination().toString();
        rollback(id, destinationName);
    }
    
    public synchronized void rollback(final  MessageId id, String destinationName) {
        if (id != null) {
            ProducerId pid = id.getProducerId();
            if (pid != null) {
            	ProducerIdWithDestination pidD=new ProducerIdWithDestination(pid, destinationName);
                BitArrayBin bab = map.get(pidD);
                if (bab != null) {
                    bab.setBit(id.getProducerSequenceId(), false);
                }
            }
        }
    }
}
