/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.activemq;


import org.apache.activemq.broker.region.MessageReference;
import org.apache.activemq.command.MessageId;
import org.apache.activemq.command.ProducerId;
import org.apache.activemq.util.BitArrayBin;

public class ActiveMQMessageAuditWithBrokerSequenceId extends ActiveMQMessageAudit{
	
	protected class ProducerIdWithBrokerSequenceId{
		protected ProducerId producerId;
		protected long brokerSequenceId;
		public ProducerIdWithBrokerSequenceId(ProducerId producerId, long brokerSequenceId) {
			super();
			this.producerId = producerId;
			this.brokerSequenceId = brokerSequenceId;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + (int) (brokerSequenceId ^ (brokerSequenceId >>> 32));
			result = prime * result + ((producerId == null) ? 0 : producerId.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if(this == obj)
				return true;
			if(obj == null)
				return false;
			if(getClass() != obj.getClass())
				return false;
			final ProducerIdWithBrokerSequenceId other = (ProducerIdWithBrokerSequenceId) obj;
			if(brokerSequenceId != other.brokerSequenceId)
				return false;
			if(producerId == null) {
				if(other.producerId != null)
					return false;
			} else if(!producerId.equals(other.producerId))
				return false;
			return true;
		}
		
		
	}
    public ActiveMQMessageAuditWithBrokerSequenceId() {
        super();
    }
    
    public ActiveMQMessageAuditWithBrokerSequenceId(int auditDepth, final int maximumNumberOfProducersToTrack) {
    	super(auditDepth, maximumNumberOfProducersToTrack);
    }
    
    @Override
    public boolean isDuplicate(final MessageReference message){
        MessageId id = message.getMessageId();
        return isDuplicate(id);
    }
    @Override
    public synchronized boolean isDuplicate(final MessageId id) {
        boolean answer = false;
        
        if (id != null) {
            ProducerId pid = id.getProducerId();
            if (pid != null) {
            	ProducerIdWithBrokerSequenceId pidD=new ProducerIdWithBrokerSequenceId(pid, id.getBrokerSequenceId());
                BitArrayBin bab = map.get(pidD);
                if (bab == null) {
                    bab = new BitArrayBin(auditDepth);
                    map.put(pid, bab);
                }
                answer = bab.setBit(id.getProducerSequenceId(), true);
            }
        }
        return answer;
    }
    
    /**
     * mark this message as being received
     * 
     * @param message
     */
    public void rollback(final MessageReference message) {
        MessageId id = message.getMessageId();
        rollback(id);
    }
    
    @Override
    public synchronized void rollback(final  MessageId id) {
        if (id != null) {
            ProducerId pid = id.getProducerId();
            if (pid != null) {
            	ProducerIdWithBrokerSequenceId pidD=new ProducerIdWithBrokerSequenceId(pid, id.getBrokerSequenceId());
                BitArrayBin bab = map.get(pidD);
                if (bab != null) {
                    bab.setBit(id.getProducerSequenceId(), false);
                }
            }
        }
    }
}
