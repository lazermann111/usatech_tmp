/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.activemq.bugs;

import java.net.URI;

import javax.jms.Destination;
import javax.jms.MessageConsumer;
import javax.jms.Topic;

import junit.framework.Test;

import org.apache.activemq.JmsMultipleBrokersTestSupport;
import org.apache.activemq.util.MessageIdList;

public class AMQ2123FourBrokerTopicNetworkTest extends JmsMultipleBrokersTestSupport {
    protected static final int MESSAGE_COUNT = 10;
    public boolean dynamicOnly;


    /**
     * BrokerA <-> BrokerB <-> BrokerC <-> BrokerD
     */
    public void testAllConnectedUsingMulticast() throws Exception {
        // Setup broker networks
        bridgeAllBrokers();

        startAllBrokers();

        // Setup destination
        Destination dest = createDestination("TEST.FOO", true);

        // Setup consumers
        MessageConsumer clientA = createDurableSubscriber("BrokerA", (Topic)dest, "consumerA");
        //MessageConsumer clientA2 = createDurableSubscriber("BrokerA", (Topic)dest, "consumerA2");
        MessageConsumer clientB = createDurableSubscriber("BrokerB", (Topic)dest, "consumerB");
        MessageConsumer clientC = createDurableSubscriber("BrokerA", (Topic)dest, "consumerC");
        MessageConsumer clientD = createDurableSubscriber("BrokerA", (Topic)dest, "consumerD");
        
        
        //let consumers propogate around the network
        Thread.sleep(2000);

        // Send messages
        sendMessages("BrokerA", dest, MESSAGE_COUNT);
        sendMessages("BrokerB", dest, MESSAGE_COUNT);
        sendMessages("BrokerC", dest, MESSAGE_COUNT);

        // Get message count
        MessageIdList msgsA = getConsumerMessages("BrokerA", clientA);
        //MessageIdList msgsA2 = getConsumerMessages("BrokerA", clientA2);
        MessageIdList msgsB = getConsumerMessages("BrokerB", clientB);
        MessageIdList msgsC = getConsumerMessages("BrokerA", clientC);
        MessageIdList msgsD = getConsumerMessages("BrokerA", clientD);
        

        msgsA.waitForMessagesToArrive(MESSAGE_COUNT * 3);
        //msgsA2.waitForMessagesToArrive(MESSAGE_COUNT * 3);
        msgsB.waitForMessagesToArrive(MESSAGE_COUNT * 3);
        msgsC.waitForMessagesToArrive(MESSAGE_COUNT * 3);
        msgsD.waitForMessagesToArrive(MESSAGE_COUNT * 3);
        
        assertEquals(MESSAGE_COUNT * 3, msgsA.getMessageCount());
        //assertEquals(MESSAGE_COUNT * 3, msgsA2.getMessageCount());
        assertEquals(MESSAGE_COUNT * 3, msgsB.getMessageCount());
        assertEquals(MESSAGE_COUNT * 3, msgsC.getMessageCount());
        assertEquals(MESSAGE_COUNT * 3, msgsD.getMessageCount());
    }

    public void setUp() throws Exception {
        super.setAutoFail(true);
        super.setUp();
        createBroker(new URI("broker:(tcp://localhost:61616)/BrokerA?persistent=true&useJmx=false"));
        createBroker(new URI("broker:(tcp://localhost:61617)/BrokerB?persistent=true&useJmx=false"));
        createBroker(new URI("broker:(tcp://localhost:61618)/BrokerC?persistent=true&useJmx=false"));
        createBroker(new URI("broker:(tcp://localhost:61619)/BrokerD?persistent=true&useJmx=false"));
    }
    
    public static Test suite() {
    	return suite(AMQ2123FourBrokerTopicNetworkTest.class);
    }
}
