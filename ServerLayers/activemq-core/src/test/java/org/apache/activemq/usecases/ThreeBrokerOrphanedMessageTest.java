package org.apache.activemq.usecases;

import java.net.URI;
import java.util.Date;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.JmsMultipleBrokersTestSupport;
import org.apache.activemq.util.MessageIdList;

public class ThreeBrokerOrphanedMessageTest extends JmsMultipleBrokersTestSupport {
	
	private int MESSAGE_COUNT = 100;
	
    public void setUp() throws Exception {
        super.setAutoFail(true);
        super.setUp();
        createBroker(new URI("broker:(tcp://localhost:61616)/BrokerA?persistent=true&useJmx=false"));
        createBroker(new URI("broker:(tcp://localhost:61617)/BrokerB?persistent=true&useJmx=false"));
        createBroker(new URI("broker:(tcp://localhost:61618)/BrokerC?persistent=true&useJmx=false"));
        createBroker(new URI("broker:(tcp://localhost:61619)/BrokerD?persistent=true&useJmx=false"));
    }
    
    public void testOrphanedMessage() throws Exception {
        // Setup broker networks
    	bridgeAllBrokers();
        startAllBrokers();

        // Setup destination
        Destination dest = createDestination("TEST.FOO", false);

        // Setup consumers
        MessageConsumer clientA = createConsumer("BrokerA", dest);
        MessageConsumer clientB = createConsumer("BrokerB", dest);
        //MessageConsumer clientC = createConsumer("BrokerC", dest);

        // run producer
        Thread producerThread = new Thread(new Producer(MESSAGE_COUNT));
        producerThread.start();
        Thread.sleep(1000);

        // Get message count
        MessageIdList msgsA = getConsumerMessages("BrokerA", clientA);
        MessageIdList msgsB = getConsumerMessages("BrokerB", clientB);
        //MessageIdList msgsC = getConsumerMessages("BrokerC", clientC);
 
        System.out.println("messages recieved on A: " + msgsA);
        System.out.println("messages recieved on B: " +  msgsB);
        System.out.println("messages recieved on A: " +  msgsA.getMessageCount());
        System.out.println("messages recieved on B: " +  msgsB.getMessageCount());
        //System.out.println("messages recieved on C: " +  msgsC.getMessageCount());

        assertEquals(MESSAGE_COUNT, msgsA.getMessageCount() + msgsB.getMessageCount());
   
    }

    public class Producer implements Runnable {  
	  
	  private int numMessages = 10;
	  
	  Producer(int numMsgs) {
		  numMessages = numMsgs;
	  }
	  public void run() {

		  Connection connection = null;
		  try {
			  // Create the connection.
			  ActiveMQConnectionFactory connectionFactory = 
				  new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_USER,
						  ActiveMQConnection.DEFAULT_PASSWORD, 
						  "failover:(tcp://localhost:61619)");
			  connection = connectionFactory.createConnection();
	          connection.start();

	          // Create the session
	          Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
	          Destination destination = session.createQueue("TEST.FOO");

	          // Create the producer.
	          MessageProducer producer = session.createProducer(destination);
	          producer.setDeliveryMode(DeliveryMode.PERSISTENT);


	          // Start sending messages
	          sendLoop(session, producer);

	      } catch (Exception e) {
	          System.out.println("Caught: " + e);
	          e.printStackTrace();
	      } finally {
	    	  try {
	              connection.close();
	          } catch (Throwable ignore) {
	          }
	      }
	  }

	  protected void sendLoop(Session session, MessageProducer producer) throws Exception {
		  
	      for (int i = 0; i < numMessages || numMessages == 0; i++) {
	    	  TextMessage message = session.createTextMessage(createMessageText(i));

	          if (verbose) {
	              String msg = message.getText();
	              if (msg.length() > 50) {
	                  msg = msg.substring(0, 50) + "...";
	              }
	              System.out.println("Sending message: " + msg);
	          }
	          producer.send(message);
	      }
	  }

	  private String createMessageText(int index) {
	      StringBuffer buffer = new StringBuffer(messageSize);
	      buffer.append("Message: " + index + " sent at: " + new Date());
	      if (buffer.length() > messageSize) {
	          return buffer.substring(0, messageSize);
	          }
	      for (int i = buffer.length(); i < messageSize; i++) {
	          buffer.append(' ');
	      }
	      return buffer.toString();
	    }
	}	

}
