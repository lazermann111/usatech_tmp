package org.apache.activemq.usecases;

import java.net.URI;
import java.util.Date;
import java.util.concurrent.CountDownLatch;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TemporaryQueue;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.JmsMultipleBrokersTestSupport;
import org.apache.activemq.JmsMultipleBrokersTestSupport.BrokerItem;
import org.apache.activemq.util.MessageIdList;

public class ThreeBrokerTempMessageTest extends JmsMultipleBrokersTestSupport {
	
	private int MESSAGE_COUNT = 10;
	public static String tempQueueName;
	
    public void setUp() throws Exception {
        super.setAutoFail(true);
        super.setUp();
        createBroker(new URI("broker:(tcp://localhost:61616)/BrokerA"));
        createBroker(new URI("broker:(tcp://localhost:61617)/BrokerB"));
        createBroker(new URI("broker:(tcp://localhost:61618)/BrokerC"));
        createBroker(new URI("broker:(tcp://localhost:61619)/BrokerD"));
    }
    
    public void testOrphanedMessage() throws Exception {
        // Setup broker networks
    	bridgeAllBrokers();
        
        startAllBrokers();
        // run producer
        Producer p=new Producer(MESSAGE_COUNT);
        Thread producerThread = new Thread(p);
        producerThread.start();
        
        // Setup destination
        //Destination dest = createTempDestination("tempQueue");
        // Setup consumers
        
        BrokerItem brokerItem = brokers.get("BrokerA");
        Connection c = brokerItem.createConnection();
        c.start();
        Session session=c.createSession(true, Session.CLIENT_ACKNOWLEDGE);
        TemporaryQueue tempQ = session.createTemporaryQueue();
    	tempQueueName=tempQ.getQueueName();
    	session.close();
    	
        p.consumerCreated();
        Thread.sleep(10000);
        int i=0;
        
        while(true){
        	Session session2=c.createSession(true, Session.CLIENT_ACKNOWLEDGE);
        	MessageConsumer clientA = session2.createConsumer(tempQ, null);
            System.out.println("#### tempQueueName:"+tempQueueName);
        	Message message = clientA.receive();
        	session2.commit();
        	//message.acknowledge();
        	System.out.println("#### message:"+(++i)+" "+message);
        	//session2.close();
        	if(i==MESSAGE_COUNT){
        		break;
        	}
        }
        // Get message count
        //MessageIdList msgsA = getConsumerMessages("BrokerA", clientA);
        //MessageIdList msgsB = getConsumerMessages("BrokerB", clientB);
        //MessageIdList msgsC = getConsumerMessages("BrokerC", clientC);
 
        //System.out.println("messages recieved on A: " + msgsA);
        //System.out.println("messages recieved on B: " +  msgsB);
        //System.out.println("messages recieved count on A: " +  msgsA.getMessageCount());
        //System.out.println("messages recieved count on B: " +  (msgsB!=null?msgsB.getMessageCount():0));
        //System.out.println("messages recieved on C: " +  msgsC.getMessageCount());

        //assertEquals(MESSAGE_COUNT, msgsA.getMessageCount());
   
    }

    public class Producer implements Runnable {  
	  
	  private int numMessages = 10;
	  CountDownLatch consumerCreated = new CountDownLatch(1);
	  
	  public String getTempQueueName(){
		  return tempQueueName;
	  }
	  
	  Producer(int numMsgs) {
		  numMessages = numMsgs;
	  }
	  
	  public void consumerCreated(){
		  consumerCreated.countDown();
	  }
	  public void run() {

		  Connection connection = null;
		  try {
			  // Create the connection.
			  ActiveMQConnectionFactory connectionFactory = 
				  new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_USER,
						  ActiveMQConnection.DEFAULT_PASSWORD, 
						  "failover:(tcp://localhost:61617)");
			  connection = connectionFactory.createConnection();
	          connection.start();

	          // Create the session
	          Session session = connection.createSession(true, Session.CLIENT_ACKNOWLEDGE);
	          //TemporaryQueue tempQ = session.createTemporaryQueue();
	          consumerCreated.await();
	          //tempQueueName=tempQ.getQueueName();
	          System.out.println("&&&&& tempQueueName:"+tempQueueName);
	          Destination tempQ = createTempDestination(tempQueueName);
	          // Create the producer.
	          MessageProducer producer = session.createProducer(tempQ);
	          //producer.setDeliveryMode(DeliveryMode.PERSISTENT);


	          // Start sending messages
	          sendLoop(session, producer);

	      } catch (Exception e) {
	          System.out.println("Caught: " + e);
	          e.printStackTrace();
	      } finally {
	    	  try {
	              connection.close();
	          } catch (Throwable ignore) {
	          }
	      }
	  }

	  protected void sendLoop(Session session, MessageProducer producer) throws Exception {
		  
	      for (int i = 0; i < numMessages || numMessages == 0; i++) {
	    	  TextMessage message = session.createTextMessage(createMessageText(i));

	          if (verbose) {
	              String msg = message.getText();
	              if (msg.length() > 50) {
	                  msg = msg.substring(0, 50) + "...";
	              }
	              System.out.println("Sending message: " + msg);
	          }
	          producer.send(message);
	          session.commit();
	      }
	  }

	  private String createMessageText(int index) {
	      StringBuffer buffer = new StringBuffer(messageSize);
	      buffer.append("Message: " + index + " sent at: " + new Date());
	      if (buffer.length() > messageSize) {
	          return buffer.substring(0, messageSize);
	          }
	      for (int i = buffer.length(); i < messageSize; i++) {
	          buffer.append(' ');
	      }
	      return buffer.toString();
	    }
	}	

}
