set BASE_DIR=D:/Development/ActiveMQ/actmqtrn
set USAT_CONFIG=aq-usat-slave.xml
set ACTIVEMQ_INSTANCE_NAME=sb
set ACTIVEMQ_JMX_PORT=2616
set USAT_CONF=%BASE_DIR%/conf
set ACTIVEMQ_BASE=%BASE_DIR%
set ACTIVEMQ_BASE=%BASE_DIR%
set ACTIVEMQ_HOME=%BASE_DIR%

set HOSTNAME=localhost
set ACTIVEMQ_BROKER_NUM=01
set ACTIVEMQ_JMX_PORT2=2656
set ACTIVEMQ_JMX_PORT_B=32616
set ACTIVEMQ_JMX_PORT_B2=32656
set USAT_AGENT_JAR=usat-jmx-agent-1.0.3.jar
set USAT_PATCH_JAR=usat-activemq-1.jar

"C:\Program Files (x86)\Java\jdk1.6.0_17\bin\java" -Dapp.servicename=%ACTIVEMQ_INSTANCE_NAME% -Xmx1536M -XX:+AggressiveHeap -XX:+UseParallelGC -XX:+UseParallelOldGC -XX:ParallelGCThreads=2 -XX:LargePageSizeInBytes=4m -Dorg.apache.activemq.UseDedicatedTaskRunner=false -Dapp.hostname=%HOSTNAME% -javaagent:%ACTIVEMQ_BASE%/lib/%USAT_AGENT_JAR% -Dcom.sun.management.jmxremote -Djmx.remote.x.trace=false -Djmx.remote.x.count=2 -Djmx.remote.x.1.rmiRegistryPort=%ACTIVEMQ_JMX_PORT% -Djmx.remote.x.1.rmiServerPort=%ACTIVEMQ_JMX_PORT2% -Djmx.remote.x.1.login.config=JMXControl -Djmx.remote.x.2.rmiRegistryPort=%ACTIVEMQ_JMX_PORT_B% -Djmx.remote.x.2.rmiServerPort=%ACTIVEMQ_JMX_PORT_B2% -Djmx.remote.x.2.login.config=JMXCheck -Djmx.remote.x.access.file=%USAT_CONF%/jmx.access -Djava.security.auth.login.config=%USAT_CONF%/jmx.login.config -Djava.util.logging.config.file=%USAT_CONF%/logging.properties -Djavax.net.ssl.keyStore=%USAT_CONF%/keystore.ks -Djavax.net.ssl.trustStore=%USAT_CONF%/truststore.ts -Djavax.net.ssl.keyStorePassword=usatech -Djavax.net.ssl.trustStorePassword=usatech -Daqinstance-name=%ACTIVEMQ_INSTANCE_NAME% -Dactivemq-broker-number=%ACTIVEMQ_BROKER_NUM% -Dactivemq.classpath=%ACTIVEMQ_BASE%/lib/%USAT_PATCH_JAR% -Dactivemq.home=%ACTIVEMQ_HOME% -Dactivemq.base=%ACTIVEMQ_BASE% -Dorg.apache.activemq.store.kahadb.LOG_SLOW_ACCESS_TIME=1500 -jar %ACTIVEMQ_HOME%/bin/run.jar start xbean:%USAT_CONFIG%


