5.3 trunk download starts on 02032010
update to trunk on 02052010 10:30am
Check status
yes 			https://issues.apache.org/activemq/browse/AMQ-1998 removeQueue in JConsole does not remove cleanly.
yes partial 	https://issues.apache.org/activemq/browse/AMQ-2070 stopGracefully command, stop before start for master and slave, activemq does not quit when stopped due to threading issue.
yes				https://issues.apache.org/activemq/browse/AMQ-2071 masterConnectorURI using failover
no patch test fixed	https://issues.apache.org/activemq/browse/AMQ-2135 network of brokers distributes messages to brokers with 0 consumers with dynamicOnly=true set
no wrong test 	https://issues.apache.org/activemq/browse/AMQ-2183 Master/slave out of sync with multiple consumers of a VirtualTopic on the same broker
no	purge()		https://issues.apache.org/activemq/browse/AMQ-2323 purge infinite loop due to incorrect statistics while loop
https://issues.apache.org/activemq/browse/AMQ-2324 forwared to broker is able to forward message to the original broker if there is no active consumer on the broker.

Change Note:
1.http://issues.apache.org/activemq/browse/AMQ-2530 fixes ConduitBridge.java for the memory leak which I originally put a fix for AMQ-2135
2.MasterConnector.java add onException change by Rob #815321
http://fisheye6.atlassian.com/changelog/activemq/?cs=815321
3. StopGracefully.java is identical
4. AMQ-2070
TransportConnection.java stop issue still not patched, done
XBeanBrokerService.java may not be needed if we don't use embeded jetty
AMQMessageStore.java not patched, done
BrokerService.java most of it is there but only minor change for stop when not fully started done
===================================
03/20/2009 starting patch trunk 756591 revision
04/01/2009 patched trunk 760721 revision
04/03/2009 patched trunk 761597 revision without AMQ-2195 fix
04/13/2009 patched trunk 764403 revision without AMQ-2195 fix
04/16/2009 patched trunk 764403 revision without AMQ-2195 fix and fix for https://issues.apache.org/activemq/browse/AMQ-2210
04/17/2009 patched trunk 765921 revision without AMQ-2195 fix
05/01/2009 patched trunk 765921 revision without AMQ-2195 fix 
05/07/2009 patched trunk 771718 revision without AMQ-2195 fix
05/15/2009 patched trunk 771718 revision without AMQ-2195 fix fix purge queue in jconsole
06/15/2009 patched trunk 771718 revision without AMQ-2195 fix fix purge queue in jconsole, fix message network forwarding to original broker.

Add:
https://issues.apache.org/activemq/browse/AMQ-2201 DemandForwardingBridge shutdown due to java.io.IOException: Unknown data type
https://issues.apache.org/activemq/browse/AMQ-2248 PrefetchSubscription NullPointerException
http://issues.apache.org/activemq/browse/AMQ-2286

USAT patches: 

waiting to be accepted
https://issues.apache.org/activemq/browse/AMQ-1998 removeQueue in JConsole does not remove cleanly.
https://issues.apache.org/activemq/browse/AMQ-2070 stopGracefully command, stop before start for master and slave, activemq does not quit when stopped due to threading issue.
https://issues.apache.org/activemq/browse/AMQ-2071 masterConnectorURI using failover
https://issues.apache.org/activemq/browse/AMQ-2135 network of brokers distributes messages to brokers with 0 consumers with dynamicOnly=true set
https://issues.apache.org/activemq/browse/AMQ-2183 Master/slave out of sync with multiple consumers of a VirtualTopic on the same broker
https://issues.apache.org/activemq/browse/AMQ-2323 purge infinite loop due to incorrect statistics while loop
https://issues.apache.org/activemq/browse/AMQ-2324 forwared to broker is able to forward message to the original broker if there is no active consumer on the broker.

NOTE:
1. https://issues.apache.org/activemq/browse/AMQ-2094
Since the trunk version, the master address config needs to change to make network of broker work
Change from localhost to 0.0.0.0 in all master config xml file.

2. https://issues.apache.org/activemq/browse/AMQ-2195
This changeset should not be included because it will affect the client's side handling
Revision 761301
Our WorkQueue will now throw a WorkQueueException instead of returning null, which depending on the errorAction configured on the AbstractWorkQueueService will either:
CONTINUE - Same thing would happen
STOP_ALL - Service will stop all its threads
Brian Krug [10:24 AM]:
PAUSE - Service will pause all its threads
Brian Krug [10:24 AM]:
FINISH_THREAD - just the thread in which the error occurred will be stopped
Brian Krug [10:25 AM]:
We currently use CONTINUE, so it shouldn't make a difference, but I vote to not include it at this time


http://svn.apache.org/viewvc?view=rev&revision=761301 for https://issues.apache.org/activemq/browse/AMQ-2195
http://svn.apache.org/viewvc?view=rev&revision=761597 for failover more
04/13/2009
http://svn.apache.org/viewvc?view=rev&revision=762464 for AMQ-2149 ack
http://svn.apache.org/viewvc?view=rev&revision=762996 tidy up logging
http://svn.apache.org/viewvc?view=rev&revision=762925 AMQ-2198
http://svn.apache.org/viewvc?view=rev&revision=763565
http://svn.apache.org/viewvc?view=rev&revision=763983 Allow suppression of duplicate queue subscriptions in a cyclic network topology
http://svn.apache.org/viewvc?view=rev&revision=763993
http://svn.apache.org/viewvc?view=rev&revision=764403
04/17/2009
http://svn.apache.org/viewvc?view=rev&revision=765141
http://svn.apache.org/viewvc?view=rev&revision=765212
http://svn.apache.org/viewvc?view=rev&revision=765921

05/07/2009
http://svn.apache.org/viewvc?view=rev&revision=766639
http://svn.apache.org/viewvc?view=rev&revision=768300 plugin to trace message route. https://issues.apache.org/activemq/browse/AMQ-2224
http://svn.apache.org/viewvc?view=rev&revision=771718 ensure default session is created for a duplicate connection request

Latest update:

http://svn.apache.org/viewvc?view=rev&revision=772931 test case for https://issues.apache.org/activemq/browse/AMQ-2195
http://svn.apache.org/viewvc?view=rev&revision=773569 fix for https://issues.apache.org/activemq/browse/AMQ-2245 - broker restart a feature
http://svn.apache.org/viewvc?view=rev&revision=773862 test case for https://issues.apache.org/activemq/browse/AMQ-2233 with prefetch  workaround
http://svn.apache.org/viewvc?view=rev&revision=774731 test fix
http://svn.apache.org/viewvc?view=rev&revision=774738 pom.xml
http://svn.apache.org/viewvc?view=rev&revision=775094 nio transport fix resolve https://issues.apache.org/activemq/browse/AMQ-2253
http://svn.apache.org/viewvc?view=rev&revision=775990 kahadb reduce message to info when iteration takes > 100ms from warn
http://svn.apache.org/viewvc?view=rev&revision=777463
http://svn.apache.org/viewvc?view=rev&revision=778622 test
http://svn.apache.org/viewvc?view=rev&revision=779556 reopen 1936 pom.xml change

http://svn.apache.org/viewvc?view=rev&revision=777716 https://issues.apache.org/activemq/browse/AMQ-2262 and https://issues.apache.org/activemq/browse/AMQ-2265 Inflight message count becomes negative when messages expire
http://svn.apache.org/viewvc?view=rev&revision=774421 https://issues.apache.org/activemq/browse/AMQ-2238 Improve HTTP transport reliability
http://svn.apache.org/viewvc?view=rev&revision=774829 resolve https://issues.apache.org/activemq/browse/AMQ-2252 - deadlock with nio and optimizeDispatch and test case
http://svn.apache.org/viewvc?view=rev&revision=777821 Fix for https://issues.apache.org/activemq/browse/AMQ-1629 wildcards don't work in networkconnector excludeDestinations 
http://svn.apache.org/viewvc?view=rev&revision=777209 Extended the Marshaller interface so that users can determin if uses fixed size records.  Kahadb should be able to make some optimizations on fixed sized objects.
http://svn.apache.org/viewvc?view=rev&revision=774712 * Cannot add a consumer to a session that had not been registered resolve alternative path to https://issues.apache.org/activemq/browse/AMQ-2241 via ra where connection is reused. state tracker was not notified when remove command is suppressed and simulated during transport outage
http://svn.apache.org/viewvc?view=rev&revision=775058 allow access to selector manager instance so that number of nio channels per thread can be configured
http://svn.apache.org/viewvc?view=rev&revision=777666 rolling back change committed in rev 777209
http://svn.apache.org/viewvc?view=rev&revision=776743 * allow configuring the wireformat maxInactivityDurationInitalDelay
http://svn.apache.org/viewvc?view=rev&revision=777806 Fix for https://issues.apache.org/activemq/browse/AMQ-2134 Multicast transport does not work when for a consumer or producer
http://svn.apache.org/viewvc?view=rev&revision=774431 test improve failure reporting in assert
http://svn.apache.org/viewvc?view=rev&revision=773986 * improve error reporting from classcast
http://svn.apache.org/viewvc?view=rev&revision=777804 test INcrease Consumer Count
http://svn.apache.org/viewvc?view=rev&revision=777803 test Added test case for https://issues.apache.org/activemq/browse/AMQ-1936
http://svn.apache.org/viewvc?view=rev&revision=781312 NIO transport resolve: https://issues.apache.org/activemq/browse/AMQ-2277 - patch applied with thanks
http://svn.apache.org/viewvc?view=rev&revision=782037 IBM jdk test fix
http://svn.apache.org/viewvc?view=rev&revision=784062 Added perf test for KahaDB and update XBeans for KahaDB Persistence Adapter
http://svn.apache.org/viewvc?view=rev&revision=784711 logging slave start failure
http://svn.apache.org/viewvc?view=rev&revision=784862 fix for https://issues.apache.org/activemq/browse/AMQ-2291 - jdbc sequence changed to long
http://svn.apache.org/viewvc?view=rev&revision=786451 https://issues.apache.org/activemq/browse/AMQ-2290 - patch applied with thanks
http://svn.apache.org/viewvc?view=rev&revision=788068 test case for https://issues.apache.org/activemq/browse/AMQ-2303 - durable consumers recovery
http://svn.apache.org/viewvc?view=rev&revision=788316 add reduced version of patch from: https://issues.apache.org/activemq/browse/AMQ-2158 - warn on exceeding 80% - leave rest at debug level
http://svn.apache.org/viewvc?view=rev&revision=793892 https://issues.apache.org/activemq/browse/AMQ-1112 expire message removal
http://svn.apache.org/viewvc?view=rev&revision=794315 test fix
http://svn.apache.org/viewvc?view=rev&revision=795069 * fix for https://issues.apache.org/activemq/browse/AMQ-2327 - close proxy consumers
http://svn.apache.org/viewvc?view=rev&revision=795848 * https://issues.apache.org/activemq/browse/AMQ-2216 remove queue check to see with AMQ-1998
http://svn.apache.org/viewvc?view=rev&revision=797685 resolve intermittent hang of JDBCQueueMasterSlaveTest
http://svn.apache.org/viewvc?view=rev&revision=798931 https://issues.apache.org/activemq/browse/AMQ-2158 Usage.java
