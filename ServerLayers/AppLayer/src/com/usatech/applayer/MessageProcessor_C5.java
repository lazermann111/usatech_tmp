package com.usatech.applayer;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.OfflineMessage;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.BooleanType;
import com.usatech.layers.common.constants.CountersResetCode;
import com.usatech.layers.common.constants.EventCodePrefix;
import com.usatech.layers.common.constants.EventDetailType;
import com.usatech.layers.common.constants.EventState;
import com.usatech.layers.common.constants.EventType;
import com.usatech.layers.common.constants.FillEventAuthType;
import com.usatech.layers.common.constants.HostCounterType;
import com.usatech.layers.common.constants.LoadDataAttrEnum;
import com.usatech.layers.common.messagedata.MessageData_C5;
import com.usatech.layers.common.messagedata.MessageData_C5.Format0Content;
import com.usatech.layers.common.messagedata.MessageData_C5.Format1Content;

import simple.app.DatabasePrerequisite;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.Log;

public class MessageProcessor_C5 implements OfflineMessageProcessor {
	protected String loadFillQueueKey = "usat.load.fill";
	
	public void processMessage(MessageChainTaskInfo taskInfo, OfflineMessage message) throws ServiceException {
        Log log = message.getLog();
        MessageData_C5 data = (MessageData_C5)message.getData();
        String deviceName = message.getDeviceName();
        Calendar eventTs = data.getCreationTime();
        long eventUtcTsMs = eventTs.getTimeInMillis();
        long messageStartTimeMs = message.getServerTime();
        if(eventUtcTsMs < messageStartTimeMs - AppLayerUtils.getValidDeviceTimeDays()*24*60*60*1000L || eventUtcTsMs > messageStartTimeMs) {
			log.warn("Received invalid event timestamp: " + eventUtcTsMs + ", using server message start time: " + messageStartTimeMs);
			eventUtcTsMs = messageStartTimeMs;
        }
        int eventUtcOffsetMs = eventTs.get(Calendar.ZONE_OFFSET);
        int eventUtcOffsetMin = eventUtcOffsetMs / (60*1000);
        long eventLocalTsMs = eventUtcTsMs + eventUtcOffsetMs;
        Map<String, Object> params = new HashMap<String, Object>();
        long eventId;
        Connection conn;
        try {
			conn = DataLayerMgr.getConnection("OPER");
		} catch(Exception e) {
			throw new RetrySpecifiedServiceException("Could not get connection for data source OPER", e, WorkRetryType.BLOCKING_RETRY);
		}
		boolean okay = false;
        try {
            eventId = AppLayerUtils.getOrCreateEvent(log, conn, params, EventCodePrefix.APP_LAYER.getValue(), message.getGlobalSessionCode(), deviceName,0 /* Base Host*/, data.getEventType().getValue(), -1, EventState.COMPLETE_FINAL, Long.toString(data.getEventId()), BooleanType.TRUE, BooleanType.TRUE, eventLocalTsMs, eventLocalTsMs);
            AppLayerUtils.getOrCreateEventDetail(log, conn, params, eventId, EventDetailType.EVENT_TYPE_VERSION, "3", -1);
            AppLayerUtils.getOrCreateEventDetail(log, conn, params, eventId, EventDetailType.HOST_EVENT_TIMESTAMP, null, eventLocalTsMs);
            AppLayerUtils.getOrCreateSettlementEvent(log, conn, params, eventId, data.getBatchId(), data.getNewBatchId(), eventUtcTsMs, eventUtcOffsetMin);
            MessageChain loadMessageChain;
            MessageChainStep loadStep;
            if(data.getEventType() == EventType.FILL) {
            	loadMessageChain = new MessageChainV11();
            	loadStep = loadMessageChain.addStep(getLoadFillQueueKey());
            	loadStep.setAttribute(LoadDataAttrEnum.ATTR_EVENT_ID, eventId);
            	loadStep.setAttribute(LoadDataAttrEnum.ATTR_EVENT_TIME, eventLocalTsMs);
				loadStep.setAttribute(LoadDataAttrEnum.ATTR_DEVICE_TYPE, message.getDeviceType());
            } else {
            	loadStep = null;
            	loadMessageChain = null;
            }
            
            switch(data.getContentFormat()) {
            	case FORMAT_0:
	            	Format0Content c0Data=(Format0Content)data.getContentFormatData();
	                AppLayerUtils.getOrCreateHostCounter(log, conn, params, eventId, 0 /* Base Host*/, HostCounterType.I_CURRENCY_TRANSACTION.getValue(), Long.toString(c0Data.getCashItems()));
	                AppLayerUtils.getOrCreateHostCounter(log, conn, params, eventId, 0 /* Base Host*/, HostCounterType.I_CURRENCY_MONEY.getValue(), c0Data.getCashAmount().toPlainString());
	                AppLayerUtils.getOrCreateHostCounter(log, conn, params, eventId, 0 /* Base Host*/, HostCounterType.I_CASHLESS_TRANSACTION.getValue(), Long.toString(c0Data.getCashlessItems()));
	                AppLayerUtils.getOrCreateHostCounter(log, conn, params, eventId, 0 /* Base Host*/, HostCounterType.I_CASHLESS_MONEY.getValue(), c0Data.getCashlessAmount().toPlainString());
	                if(loadStep != null) {
	                	loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTER_CASH_AMOUNT, c0Data.getCashAmount());
	                	loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTER_CASHLESS_AMOUNT, c0Data.getCashlessAmount());
	                	loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTER_CASH_ITEMS, c0Data.getCashItems());
	                	loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTER_CASHLESS_ITEMS, c0Data.getCashlessItems());
	                	loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTERS_REPORTED, true);
	                	loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTERS_RESET_CODE, CountersResetCode.YES);
	                	loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTERS_DISPLAYED, true);
			        }
	                break;
            	case FORMAT_1:
            		Format1Content c1Data=(Format1Content)data.getContentFormatData();
	                AppLayerUtils.getOrCreateHostCounter(log, conn, params, eventId, 0 /* Base Host*/, HostCounterType.I_CURRENCY_TRANSACTION.getValue(), Long.toString(c1Data.getCashItems()));
	                AppLayerUtils.getOrCreateHostCounter(log, conn, params, eventId, 0 /* Base Host*/, HostCounterType.I_CURRENCY_MONEY.getValue(), c1Data.getCashAmount().toPlainString());
	                AppLayerUtils.getOrCreateHostCounter(log, conn, params, eventId, 0 /* Base Host*/, HostCounterType.I_CASHLESS_TRANSACTION.getValue(), Long.toString(c1Data.getCashlessItems()));
	                AppLayerUtils.getOrCreateHostCounter(log, conn, params, eventId, 0 /* Base Host*/, HostCounterType.I_CASHLESS_MONEY.getValue(), c1Data.getCashlessAmount().toPlainString());
	                AppLayerUtils.getOrCreateHostCounter(log, conn, params, eventId, 0 /* Base Host*/, HostCounterType.CURRENCY_TRANSACTION.getValue(), Long.toString(c1Data.getAppCashItems()));
	                AppLayerUtils.getOrCreateHostCounter(log, conn, params, eventId, 0 /* Base Host*/, HostCounterType.CURRENCY_MONEY.getValue(), c1Data.getAppCashAmount().toPlainString());
	                AppLayerUtils.getOrCreateHostCounter(log, conn, params, eventId, 0 /* Base Host*/, HostCounterType.CASHLESS_TRANSACTION.getValue(), Long.toString(c1Data.getAppCashlessItems()));
	                AppLayerUtils.getOrCreateHostCounter(log, conn, params, eventId, 0 /* Base Host*/, HostCounterType.CASHLESS_MONEY.getValue(), c1Data.getAppCashlessAmount().toPlainString());
	                if(loadStep != null) {
	                	loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTER_CASH_AMOUNT, c1Data.getAppCashAmount());
	                	loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTER_CASHLESS_AMOUNT, c1Data.getAppCashlessAmount());
	                	loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTER_CASH_ITEMS, c1Data.getAppCashItems());
	                	loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTER_CASHLESS_ITEMS, c1Data.getAppCashlessItems());
	                	loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTERS_REPORTED, true);
	                	loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTERS_RESET_CODE, CountersResetCode.YES);
	                	loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTERS_DISPLAYED, true);
			        }
	                break;
            	case FORMAT_2:
    	            break;
            	default:
	            	throw new ServiceException("Unsupported content format " + data.getContentFormat());
            }
            conn.commit();
            okay = true;
            log.info("Successfully stored settlement");
            if(loadStep != null) {
            	params.clear();
            	params.put("eventId", eventId);
            	DataLayerMgr.selectInto(conn, "GET_DEVICE_INFO_FOR_EVENT", params);
            	loadStep.setAttribute(LoadDataAttrEnum.ATTR_DEVICE_SERIAL_CD, params.get("deviceSerialCd"));
            	loadStep.setAttribute(LoadDataAttrEnum.ATTR_MINOR_CURRENCY_FACTOR, params.get("minorCurrencyFactor"));
				if(data.getEventType() == EventType.FILL) {
					// TODO: We do not have a way to determine the FillEventAuthType for G9s right now. For now assume NETWORK
					boolean dexEnabled = ConvertUtils.getBoolean(params.get("dexEnabled"), false);
					Boolean createDEX;
					if(!dexEnabled)
						createDEX = Boolean.FALSE;
					else
						createDEX = (AppLayerUtils.getDexCreatedDuringFill(log, conn, params, eventId, FillEventAuthType.NETWORK) == Boolean.FALSE);
					if(createDEX != null)
						loadStep.setAttribute(LoadDataAttrEnum.ATTR_CREATE_DEX, createDEX);
				}
				MessageChainService.publish(loadMessageChain, taskInfo.getPublisher());
            }
        } catch(SQLException e) {
        	throw DatabasePrerequisite.createServiceException(e);
        } catch(Exception e) {
        	throw new ServiceException(e);
        } finally {
        	if(!okay)
        		ProcessingUtils.rollbackDbConnection(log, conn);
        	ProcessingUtils.closeDbConnection(log, conn);
        }
    }

	public String getLoadFillQueueKey() {
		return loadFillQueueKey;
	}

	public void setLoadFillQueueKey(String loadFillQueueKey) {
		this.loadFillQueueKey = loadFillQueueKey;
	}
}
