package com.usatech.applayer;

import simple.app.ServiceException;

import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.EdgeInitMessageProcessor;
import com.usatech.layers.common.Message;
import com.usatech.layers.common.MessageResponse;
import com.usatech.layers.common.messagedata.MessageData_C0;

public class MessageProcessor_C0 extends EdgeInitMessageProcessor implements MessageProcessor {
    public MessageResponse processMessage(MessageChainTaskInfo taskInfo, Message message) throws ServiceException {
    	return AppLayerUtils.processInit(message, (MessageData_C0) message.getData(), message.getLog(), encryptionKeyMinAgeMin, defaultFilePacketSize);
    }
}
