package com.usatech.applayer;

import simple.app.ServiceException;

import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.LegacyUpdateStatusProcessor;
import com.usatech.layers.common.Message;
import com.usatech.layers.common.MessageProcessingUtils;
import com.usatech.layers.common.MessageResponse;
import com.usatech.layers.common.constants.DeviceType;

public class MessageProcessor_5C implements MessageProcessor {
	public MessageResponse processMessage(MessageChainTaskInfo taskInfo, Message message) throws ServiceException {
		if(message.getDeviceInfo().getDeviceType()==DeviceType.ESUDS) {
        	return LegacyUpdateStatusProcessor.processMessage(taskInfo, message, true);
        } else {
        	MessageProcessingUtils.writeLegacy2FGenericAck(message);
        }
		return MessageResponse.CONTINUE_SESSION;
	}
}
