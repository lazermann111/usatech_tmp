package com.usatech.applayer;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import simple.app.DatabasePrerequisite;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;

import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.OfflineMessage;
import com.usatech.layers.common.messagedata.MessageData_021A;

public class MessageProcessor_021A implements OfflineMessageProcessor {
	public void processMessage(MessageChainTaskInfo taskInfo, OfflineMessage message) throws ServiceException {
        Log log = message.getLog();
		MessageData_021A data = (MessageData_021A) message.getData();
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("deviceName", message.getDeviceName());
			params.put("effectiveTime", message.getServerTime());
			Map<String, String> attributes = data.getAttributeMap();
			DataLayerMgr.selectInto("GET_DEVICE_ID", params, true);
			for (String key: attributes.keySet()) {
				params.put("deviceSettingParameterCd", "Device Info: " + key);
				params.put("deviceSettingValue", attributes.get(key));
				DataLayerMgr.executeCall("UPSERT_DEVICE_SETTING", params, true);
			}
			log.info("Updated Device Info for EC2 Client " + data.getSerialNumber());
		} catch(SQLException e) {
			if(DatabasePrerequisite.determineRetryType(e) == WorkRetryType.BLOCKING_RETRY)
				throw new RetrySpecifiedServiceException(e, WorkRetryType.BLOCKING_RETRY);
			log.error("Error processing message", e);
		} catch(DataLayerException e) {
			throw new RetrySpecifiedServiceException(e, WorkRetryType.BLOCKING_RETRY);
		} catch(Exception e) {
			log.error("Error processing message", e);
		}
    }
}
