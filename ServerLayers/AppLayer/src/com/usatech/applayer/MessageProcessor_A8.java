package com.usatech.applayer;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.LegacyUtils;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.OfflineMessage;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.BooleanType;
import com.usatech.layers.common.constants.CountersResetCode;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.EventCodePrefix;
import com.usatech.layers.common.constants.EventDetailType;
import com.usatech.layers.common.constants.EventState;
import com.usatech.layers.common.constants.FillEventAuthType;
import com.usatech.layers.common.constants.LoadDataAttrEnum;
import com.usatech.layers.common.messagedata.MessageData_A8;

import simple.app.DatabasePrerequisite;
import simple.app.Publisher;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.ByteInput;
import simple.io.Log;

public class MessageProcessor_A8 implements OfflineMessageProcessor {
	private static final Log log = Log.getLog();
	protected String loadFillQueueKey = "usat.load.fill";
	protected String prepFillQueueKey = "usat.load.fill.prepare.v2";
	
	public void processMessage(MessageChainTaskInfo taskInfo, OfflineMessage message) throws ServiceException {
		Log log = message.getLog();
		MessageData_A8 data = (MessageData_A8)message.getData();
		String deviceName = message.getDeviceName();
		Map<String,Object> params = new HashMap<String, Object>();
		long eventLocalTsMs = data.getDateAndTime()*1000;
		long eventId;
        Connection conn;
        try {
			conn = DataLayerMgr.getConnection("OPER");
		} catch(Exception e) {
			throw new RetrySpecifiedServiceException("Could not get connection for data source OPER", e, WorkRetryType.BLOCKING_RETRY);
		}
		boolean okay = false;
        try {
        	eventId = AppLayerUtils.getOrCreateEvent(log, conn, params, EventCodePrefix.APP_LAYER.getValue(), message.getGlobalSessionCode(), deviceName,0 /* Base Host*/, data.getReasonCode().getEventTypeId(), data.getReasonCode().getValue(), EventState.COMPLETE_FINAL, Long.toString(data.getCountersId()), BooleanType.TRUE, BooleanType.TRUE, eventLocalTsMs, eventLocalTsMs);
        	long getOrCreateEventTsms=ConvertUtils.getLong(params.get("eventStartTsMs"));
			// if eventLocalTsMs is changed during getOrCreateEvent due to invalid time, use the new time;
			// this will avoid GET_SUBSEQUENT_FILL_INFO to select itself and publish a load fill message
			eventLocalTsMs = getOrCreateEventTsms;
			AppLayerUtils.getOrCreateEventDetail(log, conn, params, eventId, EventDetailType.EVENT_TYPE_VERSION, String.valueOf(data.getReasonCode().getEventVersion()), -1);
			AppLayerUtils.getOrCreateEventDetail(log, conn, params, eventId, EventDetailType.HOST_EVENT_TIMESTAMP, null, eventLocalTsMs);
			
			LegacyUtils.insertCounters(log, conn, params, eventId, 0, data);
			if(data.getReasonCode().getEventTypeId() != 2) {
            	conn.commit();
    			okay = true;
			} else {
				params.put("eventId", eventId);
		    	DataLayerMgr.selectInto(conn, "GET_DEVICE_INFO_FOR_EVENT", params);
		    	String deviceSerialCd = ConvertUtils.getString(params.get("deviceSerialCd"), true);
				int minorCurrencyFactor = ConvertUtils.getInt(params.get("minorCurrencyFactor"), 100);
		    	String localServiceCardMask = ConvertUtils.getString(params.get("localServiceCardMask"), false);
		    	String networkServiceCardRegex = ConvertUtils.getString(params.get("networkServiceCardRegex"), false);
				boolean dexEnabled = ConvertUtils.getBoolean(params.get("dexEnabled"), false);

		    	FillEventAuthType authType;
	        	CountersResetCode countersResetCode;
				boolean countersDisplayed;
				switch(data.getReasonCode()) {
					default:
						if((data.getReasonCode().getValue() & 0x80) == 0x80) { // use bitmap
							authType = data.getReasonCode().getAuthType();
							countersResetCode = data.getReasonCode().getCountersResetCode();
							countersDisplayed = data.getReasonCode().isCountersDisplayed();
							break;
						}
						// fall-through
				/*
				case FILL_BUTTON_PRESS_LOCAL_AUTH:
					countersResetCode = CountersResetCode.NO;
					authType = FillEventAuthType.LOCAL;
					break;
				case FILL_CARD_SWIPE_LOCAL_AUTH:
					authType = FillEventAuthType.LOCAL;
					switch(message.getDeviceType()) {
						case MEI: countersResetCode = CountersResetCode.NO; break;
						case GX: countersResetCode = CountersResetCode.CASHLESS; break;
						default: countersResetCode = CountersResetCode.NO; break;	    					
					}
					break;
				case FILL_CARD_SWIPE_NETWORK_AUTH:
				case FILL_BUTTON_PRESS_NETWORK_AUTH:
					countersResetCode = null;
					authType = FillEventAuthType.NETWORK;
					break;
					*/
					case FILL_CARD_SWIPE:
			        	String card = data.getFillCardMagstripe();
			        	if(card == null || (card=card.trim()).length() == 0) {
				    		countersResetCode = CountersResetCode.NO;
							authType = FillEventAuthType.LOCAL;
						} else {
				    		card = MessageResponseUtils.cleanTrack(card);
				    		if(localServiceCardMask != null && (localServiceCardMask=localServiceCardMask.trim()).length() > 0 && cardMaskMatch(localServiceCardMask, card)) {
								switch(message.getDeviceType()) {
				    				case MEI: countersResetCode = CountersResetCode.NO; break;
				    				case GX: countersResetCode = CountersResetCode.CASHLESS; break;
				    				default: countersResetCode = CountersResetCode.NO; break;	    					
				    			}
				    			authType = FillEventAuthType.LOCAL;
				    		} else if(networkServiceCardRegex != null && (networkServiceCardRegex=networkServiceCardRegex.trim()).length() > 0 
				    				&& Pattern.compile(networkServiceCardRegex, Pattern.DOTALL).matcher(card).matches()) {
				    			countersResetCode = null;
				    			authType = FillEventAuthType.NETWORK;
							} else {
				    			log.warn("Could not determine whether the V2 Fill was local auth or network auth; Defaulting to local auth");
								switch(message.getDeviceType()) {
				    				case MEI: countersResetCode = CountersResetCode.NO; break;
				    				case GX: countersResetCode = CountersResetCode.CASHLESS; break;
				    				default: countersResetCode = CountersResetCode.NO; break;	    					
				    			}
				    			authType = FillEventAuthType.LOCAL;
				    		}			
				    	}
						countersDisplayed = data.getReasonCode().getEventVersion() > 1;
			        	break;

	        	}
	        	
		    	AppLayerUtils.getOrCreateEventDetail(log, conn, params, eventId, EventDetailType.AUTHORIZATION_TYPE, authType.getValue(), -1);
				AppLayerUtils.getOrCreateEventDetail(log, conn, params, eventId, EventDetailType.ENTRY_METHOD, data.getReasonCode().getEntryType().toString(), -1);
		    	conn.commit();
				okay = true;
				Boolean createDEX;
				if(!dexEnabled)
					createDEX = Boolean.FALSE;
				else if((data.getReasonCode().getValue() & 0x80) == 0x80)
					createDEX = !data.getReasonCode().isDexCreated();
				else
					createDEX = (AppLayerUtils.getDexCreatedDuringFill(log, conn, params, eventId, authType) == Boolean.FALSE);
				publishLoadFill(conn, taskInfo.getPublisher(), log, eventId, eventLocalTsMs, deviceSerialCd, minorCurrencyFactor, message.getDeviceType(),
						data.getTotalCurrencyMoneyCounter().getValue(), data.getTotalCashlessMoneyCounter().getValue(), data.getTotalPasscardMoneyCounter().getValue(), 
						data.getTotalCurrencyTransactionCounter().getValue(), data.getTotalCashlessTransactionCounter().getValue(), data.getTotalPasscardTransactionCounter().getValue(), 
						countersResetCode, countersDisplayed, createDEX);
			}
        } catch(SQLException e) {
        	throw DatabasePrerequisite.createServiceException(e);
        } catch(Exception e) {
        	throw new ServiceException(e);
        } finally {
        	if(!okay)
        		ProcessingUtils.rollbackDbConnection(log, conn);
        	ProcessingUtils.closeDbConnection(log, conn);
        }
	}

	protected boolean cardMaskMatch(String localServiceCardMask, String card) {
		for(int i = 0; i < localServiceCardMask.length(); i++) {
			char ch = localServiceCardMask.charAt(i);
			switch(ch) {
				case 'A': case 'a':
					break;
				case 'B': case 'b':
					return false;
				case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9':
					if(card.charAt(i) != ch)
						return false;
					break;
				default:
					log.warn("Invalid character '" + ch + "' in local service card mask");
					return false;
			}
		}
		return true;
	}

	protected void publishLoadFill(Connection conn, Publisher<ByteInput> publisher, Log log, long eventId, long eventLocalTsMs, String deviceSerialCd, int minorCurrencyFactor, DeviceType deviceType, int cashAmount, int cashlessAmount, int passcardAmount, int cashItems, int cashlessItems, int passcardItems, CountersResetCode countersResetCode, boolean countersDisplayed, Boolean createDEX)
			throws ServiceException {
		MessageChain loadMessageChain = new MessageChainV11();
		MessageChainStep loadStep = loadMessageChain.addStep(getLoadFillQueueKey());
    	loadStep.setAttribute(LoadDataAttrEnum.ATTR_EVENT_ID, eventId);
    	loadStep.setAttribute(LoadDataAttrEnum.ATTR_EVENT_TIME, eventLocalTsMs);
    	loadStep.setAttribute(LoadDataAttrEnum.ATTR_DEVICE_TYPE, deviceType);
    	loadStep.setAttribute(LoadDataAttrEnum.ATTR_DEVICE_SERIAL_CD, deviceSerialCd);
    	loadStep.setAttribute(LoadDataAttrEnum.ATTR_MINOR_CURRENCY_FACTOR, minorCurrencyFactor);
    	loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTERS_REPORTED, true);
		loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTERS_DISPLAYED, countersDisplayed);
		if(countersResetCode == null || countersResetCode == CountersResetCode.UNKNOWN) {
    		MessageChainStep prepStep = loadMessageChain.addStep(getPrepFillQueueKey());
    		prepStep.setAttribute(LoadDataAttrEnum.ATTR_EVENT_ID, eventId);
    		prepStep.setNextSteps(0, loadStep);
    		loadMessageChain.setCurrentStep(prepStep);
    		loadStep.setReferenceAttribute(LoadDataAttrEnum.ATTR_COUNTERS_RESET_CODE, prepStep, LoadDataAttrEnum.ATTR_COUNTERS_RESET_CODE);       	
		} else {
			loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTERS_RESET_CODE, countersResetCode);	    	
		}
		loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTER_CASH_AMOUNT, cashAmount);
    	loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTER_CASHLESS_AMOUNT, cashlessAmount);
    	loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTER_PASSCARD_AMOUNT, passcardAmount);
    	loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTER_CASH_ITEMS, cashItems);
    	loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTER_CASHLESS_ITEMS, cashlessItems);
    	loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTER_PASSCARD_ITEMS, passcardItems);
		if(createDEX != null)
			loadStep.setAttribute(LoadDataAttrEnum.ATTR_CREATE_DEX, createDEX);
    	MessageChainService.publish(loadMessageChain, publisher);	
	}
	public String getLoadFillQueueKey() {
		return loadFillQueueKey;
	}

	public void setLoadFillQueueKey(String loadFillQueueKey) {
		this.loadFillQueueKey = loadFillQueueKey;
	}

	public String getPrepFillQueueKey() {
		return prepFillQueueKey;
	}

	public void setPrepFillQueueKey(String prepFillQueueKey) {
		this.prepFillQueueKey = prepFillQueueKey;
	}
}
