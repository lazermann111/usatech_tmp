/**
 * 
 */
package com.usatech.applayer;

import simple.bean.ConvertUtils;

public class PSDAltKey {
	protected final long paymentSubtypeId;
	protected final Long paymentSubtypeKey;
	public PSDAltKey(long paymentSubtypeId, Long paymentSubtypeKey) {
		super();
		this.paymentSubtypeId = paymentSubtypeId;
		this.paymentSubtypeKey = paymentSubtypeKey;
	}
	public long getPaymentSubtypeId() {
		return paymentSubtypeId;
	}
	public Long getPaymentSubtypeKey() {
		return paymentSubtypeKey;
	}
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		PSDAltKey psdak = (PSDAltKey)obj;
		return paymentSubtypeId == psdak.paymentSubtypeId && ConvertUtils.areEqual(paymentSubtypeKey, psdak.paymentSubtypeKey);
	}
	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return paymentSubtypeKey == null ? (int)paymentSubtypeId : ((int)paymentSubtypeId) ^ (31 * paymentSubtypeKey.intValue());
	}
}