package com.usatech.applayer;

import java.util.HashMap;
import java.util.Map;

import simple.app.ServiceException;

import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.LegacyUtils;
import com.usatech.layers.common.Message;
import com.usatech.layers.common.MessageResponse;

public class ESudsRoomStatusProcessor implements MessageProcessor {
	protected static String emailQueueKey="usat.ccs.email";
	protected String emailTo;
	protected static String emailFrom="admin@esuds.net";
	protected String subject="eSuds.net Cycle Complete";
	public static String getEmailQueueKey() {
		return emailQueueKey;
	}
	public static void setEmailQueueKey(String emailQueueKey) {
		ESudsRoomStatusProcessor.emailQueueKey = emailQueueKey;
	}
	public static String getEmailFrom() {
		return emailFrom;
	}
	public static void setEmailFrom(String emailFrom) {
		ESudsRoomStatusProcessor.emailFrom = emailFrom;
	}

	public MessageResponse processMessage(MessageChainTaskInfo taskInfo, Message message) throws ServiceException {
		return LegacyUtils.processESudsRoomStatus(taskInfo, message, getEmailProperties());
	}

	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	public Map<String, String> getEmailProperties(){
		Map<String, String> params= new HashMap<String, String>();
		params.put("queueKey", emailQueueKey);
		params.put("emailFrom", emailFrom);
		params.put("subject", subject);
		return params;
	}
}
