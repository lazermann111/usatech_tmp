package com.usatech.applayer;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertUtils;
import simple.util.concurrent.LockSegmentCache;
import simple.util.concurrent.SegmentCache;

import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.OfflineMessage;

public class OfflineAppLayerTask implements MessageChainTask {
	protected static final String MESSAGE_PROCESSOR_CLASSNAME_PREFIX = MessageProcessor.class.getName() + "_";
	protected final SegmentCache<String, OfflineMessageProcessor, ServiceException> messageProcessorCache = new LockSegmentCache<String, OfflineMessageProcessor, ServiceException>() {
		@Override
		protected OfflineMessageProcessor createValue(String key, Object... additionalInfo)
				throws ServiceException {
			String className = MESSAGE_PROCESSOR_CLASSNAME_PREFIX + key;
			Class<? extends OfflineMessageProcessor> clazz;
			try {
				clazz = Class.forName(className).asSubclass(OfflineMessageProcessor.class);
			} catch(ClassNotFoundException e) {
				throw new ServiceException("Could not find offline message processor for message type '" + key + "'. Attempted to load class '" + className + "'");
			} catch(ClassCastException e) {
				throw new ServiceException("Class '" + className + "' is not a subclass of " + OfflineMessageProcessor.class.getName());
			}
			try {
				return clazz.newInstance();
			} catch(InstantiationException e) {
				throw new ServiceException("Could not instantiate an instance of " + className, e);
			} catch(IllegalAccessException e) {
				throw new ServiceException("Could not instantiate an instance of " + className, e);
			}
		}

		@Override
		protected boolean keyEquals(String key1, String key2) {
			return ConvertUtils.areEqual(key1, key2);
		}

		@Override
		protected boolean valueEquals(OfflineMessageProcessor value1, OfflineMessageProcessor value2) {
			return ConvertUtils.areEqual(value1, value2);
		}
	};

	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		taskInfo.setNextQos(taskInfo.getCurrentQos());
		OfflineMessage message = AppLayerUtils.constructOfflineMessage(taskInfo);
		final OfflineMessageProcessor mp;
		try {
			mp = getMessageProcessor(message.getData().getMessageType().getHex());
		} catch(ServiceException e) {
			throw new RetrySpecifiedServiceException("Could not find message processor for message type " + message.getData().getMessageType(), e, WorkRetryType.NO_RETRY);
		}
		mp.processMessage(taskInfo, message);
		return 0;
	}

	public OfflineMessageProcessor getMessageProcessor(String hexMessageType) throws ServiceException {
		return messageProcessorCache.getOrCreate(hexMessageType);
	}

	public void setMessageProcessor(String hexMessageType, OfflineMessageProcessor messageProcessor) {
		messageProcessorCache.put(hexMessageType, messageProcessor);
	}
}
