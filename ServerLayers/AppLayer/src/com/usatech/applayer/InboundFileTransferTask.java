package com.usatech.applayer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipException;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.CommonProcessing;
import com.usatech.layers.common.DeleteResourceTask;
import com.usatech.layers.common.LegacyUtils;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.ProcessingUtils.PropertyValueHandler;
import com.usatech.layers.common.constants.BooleanType;
import com.usatech.layers.common.constants.CountersResetCode;
import com.usatech.layers.common.constants.DeviceInfoProperty;
import com.usatech.layers.common.constants.DeviceProperty;
import com.usatech.layers.common.constants.DevicePropertyType;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.EventCodePrefix;
import com.usatech.layers.common.constants.EventState;
import com.usatech.layers.common.constants.EventType;
import com.usatech.layers.common.constants.FileType;
import com.usatech.layers.common.constants.LoadDataAttrEnum;
import com.usatech.layers.common.device.DeviceConfigurationUtils;
import com.usatech.layers.common.device.DevicesConstants;
import com.usatech.layers.common.model.ConfigTemplateSetting;

import simple.app.DatabasePrerequisite;
import simple.app.Publisher;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.NotEnoughRowsException;
import simple.io.ByteInput;
import simple.io.HeapBufferStream;
import simple.io.IOUtils;
import simple.io.Log;
import simple.io.resource.ByteArrayResource;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.lang.InvalidIntValueException;
import simple.results.BeanException;
import simple.text.StringUtils;

public class InboundFileTransferTask extends CommonProcessing implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected static final Pattern DEX_FILE_HEADER_REGEX = Pattern.compile("^DEX-(?:[a-zA-Z0-9]{8,20})-([0-9]{6})-([0-9]{6})-");
	protected static final Pattern KIOSK_DEX_FILL_FILE_NAME = Pattern.compile("^DEX-FILL-([0-9]{1,20})(-.*)?(\\..*)?$");
	protected static final Pattern DEX_FILL_FILE_NAME = Pattern.compile("^DEX-(?:[a-zA-Z0-9]{8,30})-([0-9]{6})-([0-9]{6})-FILL");
	protected ResourceFolder resourceFolder;
	protected String emailQueueKey = "usat.email";
	protected String fileImportQueueKey = "usat.inbound.fileimport";
	protected static String loadFillQueueKey = "usat.load.fill";
	protected long minFileCreateTime = 946702800000L;

	protected static class CommStats {
		public Integer rssi;
		public Integer ber;
	}

	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		Map<String, Object> attributes = taskInfo.getStep().getAttributes();
		byte[] resourceContent = ConvertUtils.convertSafely(byte[].class, attributes.get("resourceContent"), null);
		Resource resource;
		boolean delete;
		String resourceKey = null;
		if(resourceContent != null && resourceContent.length > 0) {
			String fileName = ConvertUtils.getStringSafely(attributes.get("fileName"));
			resource = new ByteArrayResource(resourceContent, "in memory", fileName);
			delete = false;
		} else {
			resourceKey = ConvertUtils.getStringSafely(attributes.get("resourceKey"));
			if(resourceKey != null && resourceKey.trim().length() > 0) {
				ResourceFolder rf = getResourceFolder();
				if(rf == null)
					throw new ServiceException("ResourceFolder property is not set on " + this);
				try {
					resource = rf.getResource(resourceKey, ResourceMode.READ);
				} catch(IOException e) {
					if(!rf.isAvailable(ResourceMode.READ)) {
						throw new RetrySpecifiedServiceException("Could not get resource at '" + resourceKey + "' for processing file transfer", e, WorkRetryType.NONBLOCKING_RETRY);
					}
					throw new ServiceException("Could not get resource at '" + resourceKey + "' for processing file transfer", e);
				}
				delete = true;
			} else {
				String fileName = ConvertUtils.getStringSafely(attributes.get("fileName"));
				resource = new ByteArrayResource(IOUtils.EMPTY_BYTES, "in memory", fileName);
				delete = false;
			}
		}
		try {
			StringBuilder sb = new StringBuilder();
			sb.append('\'').append(taskInfo.getMessageChainGuid()).append("': ");
			Log log = ProcessingUtils.getPrefixedLog(InboundFileTransferTask.log, sb.toString());
			Map<String, Object> output = new HashMap<String, Object>();
			int result;
			try {
				result = processFileTransfer(taskInfo, taskInfo.getPublisher(), attributes, resource, true, log, output);
			} catch(ZipException e) {
				log.info("Could not decompress retrying with out decompression", e);
				try {
					result = processFileTransfer(taskInfo, taskInfo.getPublisher(), attributes, resource, false, log, output);
				} catch(ZipException e1) {
					throw new ServiceException("Could not decompress resource at '" + resource.getKey() + "' for processing file transfer", e1);
				}
			}
			switch(result) {
				case 0: case 10: case 20:
					if(delete)
						DeleteResourceTask.requestResourceDeletion(resource, taskInfo.getPublisher());
					break;
				case 30: // file import
					MessageChain mc = new MessageChainV11();
					MessageChainStep step = mc.addStep(fileImportQueueKey);
					step.addStringAttribute("resourceKey", resourceKey);
					step.addByteArrayAttribute("resourceContent", resourceContent);
					step.addLiteralAttribute("deviceName", attributes.get("deviceName"));
					step.setAttribute(DeviceInfoProperty.DEVICE_CHARSET, taskInfo.getStep().getAttributeSafely(DeviceInfoProperty.DEVICE_CHARSET, Object.class, null));
					for(Map.Entry<String, Object> entry : output.entrySet())
						step.addLiteralAttribute(entry.getKey(), entry.getValue());
					MessageChainService.publish(mc, taskInfo.getPublisher());
					break;
				case 40: // forward file
					MessageChain mcForward = new MessageChainV11();
					MessageChainStep stepForward = mcForward.addStep(fileImportQueueKey);
					stepForward.addLiteralAttribute("deviceName", attributes.get("deviceName"));
					stepForward.setAttribute(DeviceInfoProperty.DEVICE_CHARSET, taskInfo.getStep().getAttributeSafely(DeviceInfoProperty.DEVICE_CHARSET, Object.class, null));
					for(Map.Entry<String, Object> entry : output.entrySet())
						stepForward.addLiteralAttribute(entry.getKey(), entry.getValue());
					MessageChainService.publish(mcForward, taskInfo.getPublisher());
					break;
			}
			return result;
		} finally {
			resource.release();
		}
	}

	protected int processFileTransfer(MessageChainTaskInfo taskInfo, Publisher<ByteInput> publisher, Map<String, Object> attributes, Resource resource, boolean allowDecompress, final Log log, Map<String, Object> output) throws ServiceException, ZipException {
		String deviceName = ConvertUtils.getStringSafely(attributes.get("deviceName"));
		DeviceType deviceType;
		String timeZoneGuid;
		Charset deviceCharset;
		FileType fileType;
		Calendar createTime;
		Long eventId;
		boolean isCompressed = false;
		try {
			deviceCharset = taskInfo.getStep().getAttributeDefault(DeviceInfoProperty.DEVICE_CHARSET, Charset.class, ProcessingConstants.US_ASCII_CHARSET);
			fileType = FileType.getByValue(ConvertUtils.getInt(attributes.get("fileType")));
			createTime = ProcessingUtils.bytesToTimestamp(ConvertUtils.convert(byte[].class, attributes.get("createTime")));
			eventId = ConvertUtils.convert(Long.class, attributes.get("eventId"));
			deviceType = taskInfo.getStep().getAttribute(DeviceInfoProperty.DEVICE_TYPE, DeviceType.class, false);
			timeZoneGuid = taskInfo.getStep().getAttribute(DeviceInfoProperty.TIME_ZONE_GUID, String.class, false);
		} catch(AttributeConversionException e) {
			throw new ServiceException("Could not convert data in file transfer message", e);
		} catch(ConvertException e) {
			throw new ServiceException("Could not convert data in file transfer message", e);
		} catch(InvalidIntValueException e) {
			throw new ServiceException("Could not parse data in file transfer message", e);
		}
		long startTime;
		Object startTimeObj = attributes.get("startTime");
		if(startTimeObj == null)
			startTime = System.currentTimeMillis();
		else
			try {
				startTime = ConvertUtils.getLong(startTimeObj);
			} catch(ConvertException e) {
				startTime = System.currentTimeMillis();
			}
		long effectiveDate;
		if(createTime != null && createTime.getTimeInMillis() >= getMinFileCreateTime() && createTime.getTimeInMillis() < startTime + 86400000)
			effectiveDate = createTime.getTimeInMillis();
		else 
			effectiveDate = startTime;
		final Map<String, Object> params = new HashMap<String, Object>();
		params.put("deviceName", deviceName);
		params.put("effectiveTime", effectiveDate);
		try {
			DataLayerMgr.selectInto("GET_DEVICE_ID", params);
		} catch(NotEnoughRowsException e) {
			throw new ServiceException("Could not find device " + deviceName, e);
		} catch(SQLException e) {
			throw new ServiceException("Could not get device id", e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not get device id", e);
		} catch(BeanException e) {
			throw new ServiceException("Could not get device id", e);
		}
		long deviceId;
		String deviceSerialCd;
		try {
			deviceId = ConvertUtils.getLong(params.get("deviceId"));
			deviceSerialCd = ConvertUtils.getString(params.get("deviceSerialCd"), true);
			if(deviceType == null)
				deviceType = ConvertUtils.convertRequired(DeviceType.class, params.get("deviceTypeId"));
			if(timeZoneGuid == null) {
				params.put("serverTime", effectiveDate);
				try {
					DataLayerMgr.selectInto("GET_TIMEZONE_BY_DEVICE_NAME", params);
				} catch(SQLException e) {
					throw new ServiceException("Could not get timezone for device '" + deviceName + "' from the database", e);
				} catch(DataLayerException e) {
					throw new ServiceException("Could not get timezone for device '" + deviceName + "' from the database", e);
				} catch(BeanException e) {
					throw new ServiceException("Could not get timezone for device '" + deviceName + "' from the database", e);
				}
				timeZoneGuid = ConvertUtils.getString(params.get("timeZoneGuid"), true);
			}
		} catch(ConvertException e) {
			throw new ServiceException("Could not get device id or timezone", e);
		}

		String fileName = ConvertUtils.getStringSafely(attributes.get("fileName"), "UNNAMED-" + fileType);
		InputStream in;
		try {
			in = resource.getInputStream();
		} catch(IOException e) {
			throw new ServiceException("Could not read from resource at '" + resource.getKey() + "' for processing file transfer", e);
		}
		//NOTE: we could check crc if it is provided, though NetLayer does this already
		boolean updateDeviceSettings = false;
		boolean overwrite = false;
		if(deviceType == DeviceType.LEGACY_KIOSK || deviceType == DeviceType.KIOSK) { // Device Specific processing - ugh!
			if(allowDecompress && fileName.toLowerCase().endsWith(".gz")) {
				if(fileType == FileType.CONFIGURATION_FILE || fileType == FileType.AUTO_EMAIL || fileName.equals("EportGPRSInfo.gz")) {
					try {
						in = new GZIPInputStream(in);
					} catch(IOException e) {
						throw new ServiceException("Could not read from resource at '" + resource.getKey() + "' for processing file transfer", e);
					}
					isCompressed = true;
				}
			}
			if(fileType == FileType.CONFIGURATION_FILE) {
				fileName = deviceName + "-CFG";
				updateDeviceSettings = true;
			} else if(fileName.equals("EportGPRSInfo") || fileName.equals("EportGPRSInfo.gz")) {
				fileName = deviceName + "-GPRS";
				overwrite = true;
			}
		}

		// blocking_retry true
		final Connection conn = AppLayerUtils.getConnection(true);

		boolean wasRead = false;
		int propertyCount = 0;
		boolean importFile = false;
		boolean forwardFile = false;
		try {
			params.put("globalSessionCode", attributes.get("globalSessionCode"));
			params.put("deviceName", deviceName);
			String globalSessionCd = ConvertUtils.getString(attributes.get("globalSessionCode"), true);
			String globalEventCd;
			String msg = null;
			switch(fileType) {
				case AUTO_EMAIL:
					// email this file out; to addresses are listed on the first line, comma delimited
					// from address is on the second line
					// subject is on the 3rd line
					// the rest is message data
					String[] contents = IOUtils.readFully(in).split("\n", 4);
					wasRead = true;
					String[] toList = StringUtils.split(contents[0], StringUtils.STANDARD_DELIMS, false);
					if(toList == null || toList.length == 0) {
						log.error("Email from device '" + deviceName + "' did not contain any addressees. File is '" + resource.getKey() + "'");
						return 2;
					}
					for(String to : toList) {
						AppLayerUtils.sendEmail(publisher, getEmailQueueKey(), to, contents[1], contents[2], contents[3]);
					}
					return 10;
				case CONFIGURATION_FILE:
					switch(deviceType) {
						case G4: case GX: case MEI:
							/* For devices sending fixed-map CONFIGURATION_FILE:
							 * Handle configuration changes through DMS code
							 * to parse the configuration file and import the contained
							 * parameters into DEVICE_SETTINGS  
							 * 
							 * Currently: just MEI, GX, G4 
							 */				
							{
								msg = readFile(resource, isCompressed);
								wasRead = true;
								LinkedHashMap<String, ConfigTemplateSetting> defaultConfData = DeviceConfigurationUtils.getDefaultConfFileDataByDeviceTypeId(deviceType.getValue(), -1, null, conn);
								LinkedHashMap<String, String> deviceSettingData = DeviceConfigurationUtils.parseMapFileData(defaultConfData, msg, false);
								switch(deviceType) {
									case MEI:
										if(deviceSettingData.containsKey(String.valueOf(DevicesConstants.MEI_MAP_SIM_NUMBER))) {
											String serialNum = deviceSettingData.get(String.valueOf(DevicesConstants.MEI_MAP_SIM_NUMBER));
											if(serialNum != null && LegacyUtils.VALID_CCID_REGEX.matcher(serialNum = serialNum.trim()).matches()) {
												params.put("CCID", serialNum);
												/* This data not currently provided by MEI 
												params.put("CGMI", null); // modem manufacturer 
												params.put("CGSN", null); // serial number 
												params.put("CGMM", null); // model identification 
												params.put("CGMR", null); // revision information 
												params.put("CSQ", null); // signal quality
												 */
												String cnum = deviceSettingData.get(String.valueOf(DevicesConstants.MEI_MAP_GSM_MODEM_PHONE_NUMBER));
												params.put("CNUM", cnum); // Subscriber number
												params.put("deviceId", deviceId);
												params.put("rssiTsMs", ConvertUtils.getLocalTime(effectiveDate, timeZoneGuid));
												params.put("modemInfo", null);
												DataLayerMgr.executeCall(conn, "UPDATE_GPRS_SETTINGS", params);
												// Add to host
												long hostId = AppLayerUtils.upsertHost(conn, params, deviceId, ProcessingConstants.MODEM_HOST_PORT_NUM, 
														0, ProcessingConstants.GPRS_MODEM_HOST_TYPE_ID, serialNum, "Unknown", "Unknown", null, ConvertUtils.getLocalTime(effectiveDate, timeZoneGuid), msg);
												AppLayerUtils.clearHostSetting(conn, params, hostId);
											if(!StringUtils.isBlank(cnum))
												AppLayerUtils.upsertHostSetting(conn, params, hostId, "CNUM", cnum);
											} else {
												log.warn("MEI configuration data contains invalid ICCID; Skipping Update. Params:" + params + "; Data:" + msg);
											}
										} else {
											log.warn(String.format("MEI configuration data for deviceId %1$d, or the configured DMS map does not specify the required fields 'Sim Number', 'Firmware Version' and 'GSM Modem Phone Number'; Data:%2$s", deviceId, msg));
										}
										break;
								}
								// special case for G4,GX,MEI:
								// check if there are pending Pokes for this unit and don't save if there are - if we
								// store the incoming config but we have pokes pending, we will poke back the config
								// data it just transfered
								// for changes to take effect on the unit, the operator must call it in first then make changes
								params.clear();
								params.put("deviceName", deviceName);
								params.put("effectiveTime", attributes.get("startTime"));
								DataLayerMgr.selectInto(conn, "GET_PENDING_POKES_COUNT", params);
								int n = ConvertUtils.getInt(params.get("count"), 0);
								if(n > 0) {
									log.info("Gx Config - Not overwriting file '" + fileName + "' because pokes already exists for current config");
									// no update to database so don't commit
									return 20;
								}
								deviceSettingData = DeviceConfigurationUtils.parseMapFileData(defaultConfData, msg, true);
								DeviceConfigurationUtils.saveDeviceSettingData(deviceId, deviceSettingData, false, conn);
							}														
							overwrite = true;
							break;
						case LEGACY_KIOSK: case KIOSK: case T2:
							overwrite = true;
					}
					break;
				case GX_PEEK_RESPONSE:
					// If peekResponse is provided, process it here, otherwise, for G4 and Gx it has already been processed in InboundFileTransferAck
					// also if means there was no pending command so let command be null
					String peekResponse = ConvertUtils.getStringSafely(attributes.get("peekResponse"));
					if(peekResponse != null && peekResponse.length() > 0) {
						LegacyUtils.processPeekResponse(null, peekResponse, deviceId, deviceType, deviceName, ConvertUtils.getLocalTime(effectiveDate, timeZoneGuid), conn);
					} else if(deviceType != DeviceType.G4 && deviceType != DeviceType.GX) {
						msg = readFile(resource, isCompressed);
						LegacyUtils.parseModemInfo(msg, deviceId, deviceType, deviceName, ConvertUtils.getLocalTime(effectiveDate, timeZoneGuid), conn);
					}
					break;
				case DEX_FILE_FOR_FILL_TO_FILL:
				case DEX_FILE:
					importFile = true;
					EventState eventState = EventState.INCOMPLETE;
					BooleanType eventEndFlag = BooleanType.FALSE;
					BooleanType eventUploadCompleteFlag = BooleanType.FALSE;
					if (fileName.contains("-FILL") || fileType == FileType.DEX_FILE_FOR_FILL_TO_FILL) {
						if(eventId == null) {
							if(deviceType == DeviceType.KIOSK) {
								Matcher matcher = KIOSK_DEX_FILL_FILE_NAME.matcher(fileName);
								if(matcher.matches())
									eventId = Long.valueOf(matcher.group(1));
								else {
									matcher = DEX_FILL_FILE_NAME.matcher(fileName);
									if (matcher.find()) {
										Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
										DateFormat df = new SimpleDateFormat("MMddyyHHmmss");
										df.setCalendar(calendar);
										try {
											Date dexDate = df.parse(matcher.group(2) + matcher.group(1));
											eventId = df.getCalendar().getTimeInMillis() / 1000;
										} catch(ParseException pe) {}
									}
								}
								eventState = EventState.COMPLETE_FINAL;
								eventEndFlag = BooleanType.TRUE;
								eventUploadCompleteFlag = BooleanType.TRUE;
							} else {
								Matcher matcher = Pattern.compile(deviceName + "-DEX-FILL-([a-fA-F0-9]{8})(?:-.*)?").matcher(fileName);
								if(matcher.matches())
								eventId = Long.valueOf(matcher.group(1), 16);
							}
						}
						if(eventId != null && eventId > 0L) {
							String dexFileHeader = new BufferedReader(new InputStreamReader(in, deviceCharset)).readLine();
							wasRead = true;
							long eventLocalTsMs = 0;
							Matcher dexHeaderMatcher = DEX_FILE_HEADER_REGEX.matcher(dexFileHeader);
							if (dexHeaderMatcher.find()) {
								Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
								DateFormat df = new SimpleDateFormat("MMddyyHHmmss");
								df.setCalendar(calendar);
								try {
									Date dexDate = df.parse(dexHeaderMatcher.group(2) + dexHeaderMatcher.group(1));
									eventLocalTsMs = dexDate.getTime();
								} catch(ParseException pe) {}
							}
							if (eventLocalTsMs == 0)
								eventLocalTsMs = ConvertUtils.getLocalTime(effectiveDate, timeZoneGuid);
							else if (deviceSerialCd.startsWith("K3VS"))
								//Interactive devices send timestamps in UTC, we are converting them to device local time 
								eventLocalTsMs = ConvertUtils.getLocalTime(eventLocalTsMs, timeZoneGuid);
	
							try {
								AppLayerUtils.getOrCreateEvent(log, conn, params, EventCodePrefix.APP_LAYER.getValue(), globalSessionCd, deviceName, 0 /* Base Host*/, EventType.FILL.getValue(), EventType.FILL.getHostNumber(), eventState, String.valueOf(eventId), eventEndFlag, eventUploadCompleteFlag, eventLocalTsMs, eventLocalTsMs);
							} catch (Exception e) {
								throw new ServiceException("Error creating fill event for a fill-to-fill DEX file, deviceName: " + deviceName + ", fileName: " + fileName, e);
							}

							if (deviceType == DeviceType.KIOSK) {
								MessageChain loadMessageChain = new MessageChainV11();
								MessageChainStep loadStep = loadMessageChain.addStep(getLoadFillQueueKey());
								loadStep.setAttribute(LoadDataAttrEnum.ATTR_EVENT_ID, eventId);
								loadStep.setAttribute(LoadDataAttrEnum.ATTR_EVENT_TIME, eventLocalTsMs);
								loadStep.setAttribute(LoadDataAttrEnum.ATTR_DEVICE_TYPE, deviceType);
								loadStep.setAttribute(LoadDataAttrEnum.ATTR_DEVICE_SERIAL_CD, deviceSerialCd);
								loadStep.setAttribute(LoadDataAttrEnum.ATTR_MINOR_CURRENCY_FACTOR, 100);
								loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTERS_REPORTED, "N");
								loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTERS_DISPLAYED, "N");
								loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTERS_RESET_CODE, CountersResetCode.NO);
								loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTER_CASH_AMOUNT, null);
								loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTER_CASHLESS_AMOUNT, null);
								loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTER_PASSCARD_AMOUNT, null);
								loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTER_CASH_ITEMS, null);
								loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTER_CASHLESS_ITEMS, null);
								loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTER_PASSCARD_ITEMS, null);
								loadStep.setAttribute(LoadDataAttrEnum.ATTR_CREATE_DEX, "N");
								MessageChainService.publish(loadMessageChain, publisher);
							}
						}
					}
					break;
				case CUSTOM_FILE_UPLOAD:
					forwardFile = true;
					break;
				case PROPERTY_LIST: // Property List
					// Update device settings
					params.clear();
					params.put("deviceId", deviceId);
					final CommStats commStats = new CommStats();
					propertyCount = ProcessingUtils.parsePropertyList(in, deviceCharset, new PropertyValueHandler<SQLException>() {
						public void handleProperty(int propertyIndex, String propertyValue) throws SQLException {
							DeviceProperty dp = DeviceProperty.getByValueSafely(propertyIndex);
							if(dp != null) {
								if(dp.getDevicePropertyType() == DevicePropertyType.DYNAMIC) {
									if(log.isInfoEnabled())
										log.info("Ignoring the value of dynamic property '" + dp + "' sent by the device");
									return; // Early exit
								}
								switch(dp) {
									case COMSTATS_RSSI:
										commStats.rssi = ConvertUtils.convertSafely(Integer.class, propertyValue, null);
										break;
									case COMSTAT_BER:
										commStats.ber = ConvertUtils.convertSafely(Integer.class, propertyValue, null);
										break;
									case ENCRYPTION_KEY:
										return; // Because it is sensitive data, do not record what device sent as the encryption key
								}
							}
							params.put("name", propertyIndex);
							params.put("value", propertyValue);
							try {
								DataLayerMgr.executeCall(conn, "UPDATE_DEVICE_SETTING", params);
							} catch(DataLayerException e) {
								throw new SQLException(e);
							}
						}
					});
					wasRead = true;

					if(commStats.rssi != null && commStats.ber != null) {
						msg = readFile(resource, isCompressed);
						params.clear();
						params.put("deviceId", deviceId);
						params.put("rssi", commStats.rssi);
						params.put("ber", commStats.ber);
						params.put("rssiTs", createTime);
						params.put("modemInfo", msg.length() > 4000 ? msg.substring(0, 4000 - 1) : msg);
						DataLayerMgr.executeCall(conn, "UPDATE_COMM_STATS", params);
						// if the file only has RSSI and BER properties, don't insert it into device.file_transfer
						if(propertyCount == 2) {
							conn.commit();
							return 0;
						}
					}
					break;
				case PROPERTY_LIST_REQUEST: // property value list request
					params.clear();
					params.put("deviceName", deviceName);
					DataLayerMgr.executeCall(conn, "LOCK_DEVICE_PENDING_COMMAND", params); // This is so that two threads don't do this at the same time and the order in which the insert into MACHINE_CMD_PENDING is not consistent with the order that deviceInfo properties are selected
					String propertyListString = IOUtils.readFully(new BufferedReader(new InputStreamReader(in, deviceCharset)));
					wasRead = true;
					Set<Integer> propertyList;
					try {
						propertyList = ProcessingUtils.parsePropertyIndexList(propertyListString);
					} catch(ParseException e) {
						throw new RetrySpecifiedServiceException("Invalid property index list '" + propertyListString + "' from device '" + deviceName + "'", e, WorkRetryType.NO_RETRY);
					}
					HeapBufferStream bufferStream = AppLayerUtils.constructPropertyValueStream(deviceName, deviceCharset, null, propertyList, true, conn);
					Calendar fileCreateTime = Calendar.getInstance();
					String pendingFileName = deviceName + "-CFG-partial-" + ConvertUtils.formatObject(fileCreateTime.getTime(), "DATE:yyyy-MM-dd_HH-mm-ss.SSS");
					AppLayerUtils.addPendingFileTransfer(19, pendingFileName, bufferStream.getInputStream(), bufferStream.size(), deviceName, getDefaultFilePacketSize(), 800, conn);
					conn.commit();
					break;
				case GENERIC_DEVICE_INFORMATION:
					String deviceInformation = IOUtils.readFully(new BufferedReader(new InputStreamReader(in, deviceCharset)));
					wasRead = true;
					AppLayerUtils.parseDeviceInfo(conn, deviceId, deviceType, deviceName, ConvertUtils.getLocalTime(effectiveDate, timeZoneGuid), deviceInformation);
					break;
			}

			if(deviceType == DeviceType.KIOSK) {
				if(fileName.equals(deviceName + "-GPRS")) {
					if(msg == null)
						msg = readFile(resource, isCompressed);
					if(LegacyUtils.parseModemInfo(msg, deviceId, deviceType, deviceName, ConvertUtils.getLocalTime(effectiveDate, timeZoneGuid), conn))
						return 0;
				}

				//no need to do ack, ack is done properly by building message chain in netlayer
				// update device setting
				if(updateDeviceSettings) {
					params.clear();
					params.put("device_id", deviceId);
					DataLayerMgr.executeCall(conn, "DELETE_DEVICE_SETTINGS", params);
					if(msg == null) {
						msg = readFile(resource, isCompressed);
					}
					updateKioskDeviceSettings(params, msg, conn);
				}
			}
			
			// XXX: should we create an event for non-dex fill file types?
			//insert into file_transfer and associate with device in device_file_transfer and with event_file_transfer if applicable
			if(eventId != null) {
				globalEventCd = ProcessingConstants.GLOBAL_EVENT_CODE_PREFIX + ':' + deviceName + ":" + eventId;
			} else {
				globalEventCd = null;
			}
			if(wasRead)
				try {
					if(isCompressed) {
						in = new GZIPInputStream(resource.getInputStream());
					} else {
						in = resource.getInputStream();
					}
				} catch(IOException e) {
					throw new ServiceException("Could not read from resource at '" + resource.getKey() + "' for processing file transfer", e);
				}
			final long contentLength;
			if(!isCompressed)
				contentLength = resource.getLength();
			else {
				// NOTE: The only way to get the uncompressed size is to read the whole file once, because Resource does not allow random access
				// and the ISIZE is at the end of the GZIP stream.
				if(msg != null) {
					contentLength = msg.length();
				} else {
					contentLength = IOUtils.readForLength(new GZIPInputStream(resource.getInputStream()));				
				}			
			}
			if(contentLength == 0) {
				log.warn("File '" + resource.getKey() + "' has 0 bytes");
				return 0;
			}
			Calendar fileTransferTime = Calendar.getInstance();
			fileTransferTime.setTimeInMillis(ConvertUtils.getLongSafely(attributes.get("startTime"), System.currentTimeMillis()));

			params.clear();
			params.put("globalSessionCd", globalSessionCd);
			params.put("deviceName", deviceName);
			params.put("deviceId", deviceId);
			params.put("fileName", fileName);
			params.put("fileType", fileType);
			params.put("overwrite", overwrite ? "Y" : "N");
			params.put("deviceEventCd", eventId);
			params.put("globalEventCd", globalEventCd);
			params.put("fileTransferTs", fileTransferTime);
			DataLayerMgr.executeCall(conn, "RECORD_INBOUND_FILE_TRANSFER", params);
			params.put("fileContent", in);
			DataLayerMgr.executeCall(conn, "UPDATE_FILE_TRANSFER_CONTENT", params);
			conn.commit();
			long fileTransferId = ConvertUtils.getLong(params.get("fileTransferId"));
			log.info("Uploaded file '" + fileName + "' of " + contentLength + " bytes from device " + deviceName + " to OPER database, fileTransferId: " + fileTransferId);

			if(eventId != null && deviceType == DeviceType.EDGE) {
				params.clear();
				params.put("deviceId", deviceId);
				params.put("name", DeviceProperty.MASTER_ID.getValue());
				params.put("value", eventId);
				params.put("onlyGreater", "Y");
				DataLayerMgr.executeCall("UPDATE_DEVICE_SETTING", params, true);
			}
			
			if(importFile) {
				output.put("fileTransferId", fileTransferId);
				output.put("fileTransferTs", fileTransferTime);
				output.put("fileName", fileName);
				output.put("fileType", fileType);
				output.put("deviceSerialCd", deviceSerialCd);
				output.put("timeZoneGuid", timeZoneGuid);
				return 30;
			}
			
			if(forwardFile) {
				output.put("fileTransferId", fileTransferId);
				output.put("fileTransferTs", fileTransferTime);
				output.put("fileName", fileName);
				output.put("fileType", fileType);
				output.put("deviceSerialCd", deviceSerialCd);
				output.put("timeZoneGuid", timeZoneGuid);
				return 40;
			}
			return 0;
		} catch(ZipException e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
			throw e;
		} catch(SQLException e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
			throw DatabasePrerequisite.createServiceException("Could not process file '" + fileName + "' for device '" + deviceName + "'. File is '" + resource.getKey() + "'", e);
		} catch(DataLayerException e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
			throw new ServiceException("Could not process file '" + fileName + "' for device '" + deviceName + "'. File is '" + resource.getKey() + "'", e);
		} catch(BeanException e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
			log.error("Could not process file '" + fileName + "' for device '" + deviceName + "'. File is '" + resource.getKey() + "'", e);
			return 1;
		} catch(IOException e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
			throw new ServiceException("Could not process file '" + fileName + "' for device '" + deviceName + "'. File is '" + resource.getKey() + "'", e);
		} catch(ConvertException e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
			log.error("Could not process file '" + fileName + "' for device '" + deviceName + "'. File is '" + resource.getKey() + "'", e);
			return 1;
		} catch(ParseException e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
			log.error("Could not parse file '" + fileName + "' for device '" + deviceName + "'. File is '" + resource.getKey() + "'", e);
			return 1;
		} finally {
			ProcessingUtils.closeDbConnection(log, conn);
		}
	}

	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}

	public String readFile(Resource resource, boolean isCompressed) throws ServiceException, IOException {
		InputStream in = null;
		try {
			if(isCompressed) {
				in = new GZIPInputStream(resource.getInputStream());
			} else {
				in = resource.getInputStream();
			}
		} catch(IOException e) {
			throw new ServiceException("Could not read from resource at '" + resource.getKey() + "' for processing file transfer", e);
		}
		return IOUtils.readFully(in);
	}

	public static void updateKioskDeviceSettings(Map<String, Object> params, String msg, Connection conn) throws SQLException, DataLayerException {
		String[] configLines = StringUtils.split(msg, "\n");
		for(int i = 0; i < configLines.length; i++) {
			if(configLines[i] != null && !configLines[i].equals("")) {
				String configLine = configLines[i].trim();
				String[] configPair = new String[2];
				int equalIndex = configLine.indexOf("=");
				if(equalIndex > 0) {
					configPair[0] = configLine.substring(0, equalIndex);
					if(equalIndex != configLine.length() - 1) {
						configPair[1] = configLine.substring(equalIndex + 1);
					}
				}
				if(configPair[0] != null && !configPair[0].equals("")) {
					if(configPair[0].length() > 60) {
						configPair[0] = configPair[0].substring(0, 60);
						configPair[0] = configPair[0].replaceAll("^\\s+|\\s+$", "");
					}
					if(configPair[0].length() > 0) {
						if(configPair[1] != null && configPair[1].length() > 200) {
							configPair[1] = configPair[1].substring(0, 200);
							configPair[1] = configPair[1].replaceAll("^\\s+|\\s+$", "");
						}
						params.put("deviceSettingParameterCd", configPair[0]);
						params.put("deviceSettingValue", configPair[1]);
						DataLayerMgr.executeCall(conn, "UPDATE_KIOSK_DEVICE_SETTINGS", params);
					}
				}
			}
		}
	}

	public String getEmailQueueKey() {
		return emailQueueKey;
	}

	public void setEmailQueueKey(String emailQueueKey) {
		this.emailQueueKey = emailQueueKey;
	}

	public String getFileImportQueueKey() {
		return fileImportQueueKey;
	}

	public void setFileImportQueueKey(String fileImportQueueKey) {
		this.fileImportQueueKey = fileImportQueueKey;
	}

	public long getMinFileCreateTime() {
		return minFileCreateTime;
	}

	public void setMinFileCreateTime(long minFileCreateTime) {
		this.minFileCreateTime = minFileCreateTime;
	}

	public static String getLoadFillQueueKey() {
		return loadFillQueueKey;
	}

	public static void setLoadFillQueueKey(String loadFillQueueKey) {
		InboundFileTransferTask.loadFillQueueKey = loadFillQueueKey;
	}
}
