/**
 *
 */
package com.usatech.applayer;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import simple.app.DatabasePrerequisite;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.util.CompositeMap;

import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.task.SQLExceptionHandler;
import com.usatech.layers.common.SessionCache;

/**
 * @author Brian S. Krug
 *
 */
public class InsertMessageProcessedTask implements MessageChainTask {
	protected SQLExceptionHandler sqlExceptionHandler;

	/**
	 * @see com.usatech.app.MessageChainTask#process(com.usatech.app.MessageChainTaskInfo)
	 */
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		Map<String,Object> attributes = taskInfo.getStep().getAttributes();
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("netLayerId", SessionCache.getOrCreateNetLayerId(attributes));
		params.put("checkForDup", taskInfo.isRedelivered() ? "Y" : "N");
		CompositeMap<String, Object> merged = new CompositeMap<String, Object>();
		merged.merge(params);
		merged.merge(attributes);
		try {
			DataLayerMgr.executeCall("INSERT_MESSAGE_PROCESSED", merged, params, true);
		} catch(SQLException e) {
			SQLExceptionHandler handler = getSqlExceptionHandler();
			if(handler != null)
				handler.handleSQLException("INSERT_MESSAGE_PROCESSED", params, e);
			else
				throw DatabasePrerequisite.createServiceException(e);
		} catch(DataLayerException e) {
			throw new RetrySpecifiedServiceException(e, WorkRetryType.BLOCKING_RETRY);
		}

		return 0;
	}

	public SQLExceptionHandler getSqlExceptionHandler() {
		return sqlExceptionHandler;
	}

	public void setSqlExceptionHandler(SQLExceptionHandler sqlExceptionHandler) {
		this.sqlExceptionHandler = sqlExceptionHandler;
	}

}
