package com.usatech.applayer;

import simple.lang.EnumCharValueLookup;
import simple.lang.InvalidValueException;

public enum AccountHandling {
	DENY_ON_NO_ACCOUNT_ID('D'), 
	SKIP_ON_NO_ACCOUNT_ID('S'), 
	REQUIRE_TOKEN_OR_SKIP('T'), 
	CREATE_ACCOUNT_ID('C'), 
	NORMAL('N')
	;
	private final char value;
	protected final static EnumCharValueLookup<AccountHandling> lookup = new EnumCharValueLookup<AccountHandling>(AccountHandling.class);

	private AccountHandling(char value) {
		this.value = value;
	}

	public char getValue() {
		return value;
	}

	public static AccountHandling getByValue(char value) throws InvalidValueException {
		return lookup.getByValue(value);
	}
}
