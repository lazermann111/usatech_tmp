package com.usatech.applayer;

import simple.app.ServiceException;

import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.OfflineMessage;

public interface OfflineMessageProcessor {
	/** Processes the message from the device and returns whether to continue the session or to close it
	 * @param taskInfo TODO
	 * @param message The message from the device
	 * @return Whether the session should continue or not
	 * @throws ServiceException
	 */
	public void processMessage(MessageChainTaskInfo taskInfo, OfflineMessage message) throws ServiceException;

}
