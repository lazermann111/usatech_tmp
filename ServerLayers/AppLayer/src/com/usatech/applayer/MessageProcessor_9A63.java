package com.usatech.applayer;

import java.nio.charset.Charset;
import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;

import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.DeviceInfo;
import com.usatech.layers.common.LegacyUtils;
import com.usatech.layers.common.Message;
import com.usatech.layers.common.MessageProcessingUtils;
import com.usatech.layers.common.MessageResponse;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.BooleanType;
import com.usatech.layers.common.constants.WasherDryerType;
import com.usatech.layers.common.messagedata.MessageData_9A63;
import com.usatech.layers.common.messagedata.MessageData_9A63.ComponentData;
import com.usatech.layers.common.messagedata.MessageData_9A63.ComponentData.SupportedCycleData;
/**
 * Note: diff from the Rerix:
 * 1. does not insert into host_type_host_equipment Need to confirm whether it is needed
 * @author yhe
 *
 */
public class MessageProcessor_9A63 implements MessageProcessor {
    public MessageResponse processMessage(MessageChainTaskInfo taskInfo, Message message) throws ServiceException {
        Log log = message.getLog();
        MessageData_9A63 data = (MessageData_9A63)message.getData();

        MessageResponse msgResponse = MessageResponse.CONTINUE_SESSION;
        Connection conn = null;
        Object[] ret;

        try {
        	DeviceInfo deviceInfo = message.getDeviceInfo();
        	Charset charset = message.getDeviceInfo().getDeviceCharset();
            conn = AppLayerUtils.getConnection(false);

            Map<String, Object> params = new HashMap<String, Object>();

            Map<Integer, String> deviceTypeHostTypes = AppLayerUtils.getDeviceTypeHostTypes(deviceInfo.getDeviceType().getValue());

            Map<String, Map<String, Object>> hosts = new HashMap<String, Map<String, Object>>();

            List<ComponentData> components=data.getComponents();

            for (ComponentData component: components){
            	WasherDryerType unitType=component.getWasherDryerType();
            	if(unitType!=WasherDryerType.NO_WASHER_DRYER_ON_PORT){
            		params.put("deviceTypeHostTypeCd", (char)(component.getWasherDryerType().getValue()));
            		DataLayerMgr.selectInto(conn, "GET_ESUDS_HOST_TYPE_ID", params);
            		int hostTypeId=ConvertUtils.getInt(params.get("hostTypeId"));
            		if(unitType==WasherDryerType.GEN2_STACK_DRYER_DRYER||unitType==WasherDryerType.GEN2_STACK_DRYER_WASHER){
            			// this is a stacked washer/dryer, break it into 2 separate hosts
            			// bottom (0) is the washer
            			// top (1) is the dryer
            			AppLayerUtils.loadHostESuds(log, component.getComponentNumber(), hostTypeId, component, conn, deviceTypeHostTypes, params, hosts, charset, 0);
            			AppLayerUtils.loadHostESuds(log, component.getComponentNumber(), hostTypeId, component, conn, deviceTypeHostTypes, params, hosts, charset, 1);
            			setupSupportedCycle(component, component.getComponentNumber(), 0, hosts);
            			setupSupportedCycle(component, component.getComponentNumber(), 1, hosts);
            		}else if(unitType==WasherDryerType.GEN1_STACKED_DRYER_DRYER_TOP||unitType==WasherDryerType.GEN1_STACKED_DRYER_DRYER_TOP_ALT){
            			//this is a single unit in top position
            			// top (1) is the dryer
            			AppLayerUtils.loadHostESuds(log, component.getComponentNumber(), hostTypeId, component, conn, deviceTypeHostTypes, params, hosts, charset, 1);
            			setupSupportedCycle(component, component.getComponentNumber(), 1, hosts);
            		}else{
            			// assume everything else is a single unit in bottom position
            			AppLayerUtils.loadHostESuds(log, component.getComponentNumber(), hostTypeId, component, conn, deviceTypeHostTypes, params, hosts, charset, 0);
            			setupSupportedCycle(component, component.getComponentNumber(), 0, hosts);
            		}

            	}
            }
            //use hosts.size
            log.info("Received " + hosts.size() + " host info blocks");

            Map<String, Map<String, Object>> dbHosts = new HashMap<String, Map<String, Object>>();

            //Get current Device Id
            params.clear();
            params.put("deviceName", deviceInfo.getDeviceName());
            params.put("effectiveTime", message.getServerTime());
			DataLayerMgr.selectInto("GET_DEVICE_ID", params);
            long deviceId = ConvertUtils.getLong(params.get("deviceId"));
            Results results = DataLayerMgr.executeQuery("GET_DEVICE_HOSTS", params);
            while(results.next()) {
                int hostPortNum = results.getValue("hostPortNum", Integer.class);
                if (hostPortNum < 1)
                	continue;
                int hostPositionNum = results.getValue("hostPositionNum", Integer.class);
                Map<String, Object> dbHost = new HashMap<String, Object>();
                dbHost.put("hostId", results.getValue("hostId", Long.class));
                dbHost.put("hostType", results.getValue("hostTypeId", Integer.class));
                dbHost.put("hostSerialNumber", results.getValue("hostSerialCd", String.class));
                dbHost.put("hostLabel", results.getValue("hostLabelCd", String.class));
                dbHost.put("hostEquipmentId", results.getValue("hostEquipmentId", Long.class));
                dbHost.put("hostRevision", results.getValue("firmwareVersion", String.class));
                dbHosts.put(AppLayerUtils.getHostMapKey(hostPortNum, hostPositionNum), dbHost);
            }

            if (dbHosts.size() > 0)
            {
            	if (hosts.size() < dbHosts.size())
                    log.info("Fewer incoming hosts: " + hosts.size() + " than existing: " + dbHosts.size());
                for (String hostKey : hosts.keySet().toArray(new String[hosts.size()]))
                {
                    if (dbHosts.containsKey(hostKey) == false)
                        log.info("Room Setup Status :" + hostKey + " does not exist");
                    else
                    {
                        Map<String, Object> host = hosts.get(hostKey);
                        Integer hostTypeId = ConvertUtils.convertSafely(Integer.class, host.get("hostType"), -1);
                        Long hostEquipmentId = ConvertUtils.convertSafely(Long.class, host.get("hostEquipmentId"), -1L);
                        String hostSerialNumber = ConvertUtils.convertSafely(String.class, host.get("hostSerialNumber"), "");
                        String hostLabel = ConvertUtils.convertSafely(String.class, host.get("hostLabel"), "");
                        String hostRevision = ConvertUtils.convertSafely(String.class, host.get("hostRevision"), "");

                        Map<String, Object> dbHost = dbHosts.get(hostKey);
                        Integer dbHostTypeId = ConvertUtils.convertSafely(Integer.class, dbHost.get("hostType"), -1);
                        Long dbHostEquipmentId = ConvertUtils.convertSafely(Long.class, dbHost.get("hostEquipmentId"), -1L);
                        String dbHostSerialNumber = ConvertUtils.convertSafely(String.class, dbHost.get("hostSerialNumber"), "");
                        String dbHostLabel = ConvertUtils.convertSafely(String.class, dbHost.get("hostLabel"), "");
                        String dbHostRevision = ConvertUtils.convertSafely(String.class, dbHost.get("hostRevision"), "");

                        if (hostTypeId.equals(dbHostTypeId) && hostEquipmentId.equals(dbHostEquipmentId))
                        {
                            log.info("Room Setup Status : " + hostKey + " has not changed");
                            hosts.remove(hostKey);
							dbHosts.remove(hostKey);
                        } 
                        else
                            log.info("Room Setup Status : " + hostKey + " changed:"
                                    + " dbHostTypeId: " + dbHostTypeId
                                    + ", dbHostEquipmentId: " + dbHostEquipmentId
                                    + ", dbHostSerialNumber: " + dbHostSerialNumber
                                    + ", dbHostLabel: " + dbHostLabel
                                    + ", dbHostRevision: " + dbHostRevision
                                    + " to hostTypeId: " + hostTypeId
                                    + ", hostEquipmentId: " + hostEquipmentId
                                    + ", hostSerialNumber: " + hostSerialNumber
                                    + ", hostLabel: " + hostLabel
                                    + ", hostRevision: " + hostRevision);
                    }
                }                
            }
            else
                log.info("No host records exist in the database");

            Map<Long, Object> cycleMap=new HashMap<Long, Object>();
            for (String hostKey : hosts.keySet().toArray(new String[hosts.size()]))
            {
                Map<String, Object> host = hosts.get(hostKey);
                params.clear();
                params.put("deviceId", deviceId);
                params.put("hostPortNum", ConvertUtils.getInt(host.get("hostPortNum")));
                params.put("hostPositionNum", ConvertUtils.getInt(host.get("hostPositionNum")));
                params.put("hostTypeId", ConvertUtils.convert(Integer.class, host.get("hostType")));
                params.put("hostSerialCd", ConvertUtils.convert(String.class, host.get("hostSerialNumber")));
                params.put("hostLabelCd", ConvertUtils.convert(String.class, host.get("hostLabel")));
                params.put("hostEquipmentId", ConvertUtils.convert(Long.class, host.get("hostEquipmentId")));
                ret = DataLayerMgr.executeCall(conn, "UPSERT_HOST_WITH_EQUIPMENT_ID", params);
                long hostId = ConvertUtils.convert(Long.class, ret[1]);
                BooleanType hostExists = BooleanType.getByValue(ConvertUtils.convert(Byte.class, ret[2]));
                log.info((hostExists == BooleanType.TRUE ? "Existing" : "New") + " host: " + hostId);
                cycleMap.put(hostId, host.get("supportedCycle"));
                String hostRevision = ConvertUtils.convertSafely(String.class, host.get("hostRevision"), "");
                if(hostRevision.length() > 0)
                    AppLayerUtils.upsertHostSetting(conn, params, hostId, "Firmware Version", hostRevision);
                hosts.remove(hostKey);
                dbHosts.remove(hostKey);
            }
            for(String hostKey : dbHosts.keySet()) {
				Map<String, Object> dbHost = dbHosts.get(hostKey);
				DataLayerMgr.executeCall(conn, "DEACTIVATE_HOST", dbHost);
				log.info("Deactivated host " + dbHost.get("hostId"));
			}
            for (Long hostId : cycleMap.keySet())
            {
            	List<?> supportedCycles=(List<?>)cycleMap.get(hostId);
            	for(Object entry : supportedCycles){
            		SupportedCycleData supportedData = (SupportedCycleData) entry;
            		params.clear();
                    params.put("tranLineItemTypeId", supportedData.getCycleType());
                    DataLayerMgr.selectInto("GET_TRAN_LINE_ITEM_DESC", params);
                    String cycleDesc = ConvertUtils.getStringSafely(params.get("tranLineItemDesc"));
                    if(cycleDesc==null){
                    	throw new ServiceException("Failed to lookup tran_line_item_type for tranLineItemTypeId" + supportedData.getCycleType());
                    }
                    //cycle Time
                    int cycleTime = supportedData.getCycleTime();
                    if(cycleTime>0){
                    	boolean isUpdated=LegacyUtils.upsertHostSettingCompare(conn, params, hostId, cycleDesc+" Time", String.valueOf(cycleTime));
                    	if(isUpdated){
                    		params.clear();
                    		params.put("hostId", hostId);
                    		params.put("tranLineItemTypeId", supportedData.getCycleType());
                    		DataLayerMgr.executeCall(conn, "UPDATE_EST_TRAN_COMPLETE_TIME", params);
                    	}
                    }
                    //cycle Price
                    String cyclePrice = LegacyUtils.getCyclePriceString(supportedData.getCyclePrice());
                    LegacyUtils.upsertHostSettingCompare(conn, params, hostId, cycleDesc+" Price", cyclePrice);
            	}

            }
            conn.commit();
            log.info("Successfully processed message");
        } catch(Exception e) {
        	ProcessingUtils.rollbackDbConnection(log, conn);
            log.error("Error processing message", e);
        } finally {
        	ProcessingUtils.closeDbConnection(log, conn);
        }
        MessageProcessingUtils.writeLegacy9A46RoomLayoutAck(message);
        return msgResponse;
    }

    public static void setupSupportedCycle(ComponentData component, int hostPortNum, int hostPositionNum, Map<String, Map<String, Object>> hosts){
    	Map<String, Object> host= hosts.get(AppLayerUtils.getHostMapKey(hostPortNum, hostPositionNum));
    	host.put("supportedCycle", component.getSupportedCycles());
    }

}
