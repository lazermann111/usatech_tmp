package com.usatech.applayer;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.HeapBufferStream;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.results.Results;
import simple.results.Results.Aggregate;
import simple.text.StringUtils;
import simple.util.CompositeMap;

import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.CommonProcessing;
import com.usatech.layers.common.CountingAppendable;
import com.usatech.layers.common.DeviceInfo;
import com.usatech.layers.common.Message;
import com.usatech.layers.common.MessageProcessingUtils;
import com.usatech.layers.common.MessageResponse;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.ActivationStatus;
import com.usatech.layers.common.constants.CRCOutputStream;
import com.usatech.layers.common.constants.CRCType;
import com.usatech.layers.common.constants.DeviceProperty;
import com.usatech.layers.common.constants.GenericRequestType;
import com.usatech.layers.common.constants.GenericResponseResultCode;
import com.usatech.layers.common.constants.GenericResponseServerActionCode;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.ServerActionCode;
import com.usatech.layers.common.messagedata.MessageDataUtils;
import com.usatech.layers.common.messagedata.MessageData_CA;
import com.usatech.layers.common.messagedata.MessageData_CA.FileTransferRequest;
import com.usatech.layers.common.messagedata.MessageData_CA.PropertyListRequest;
import com.usatech.layers.common.messagedata.MessageData_CA.UpdateStatusRequest;

public class MessageProcessor_CA extends CommonProcessing implements MessageProcessor {
	private static final Log log = Log.getLog();
	protected Set<Integer> readyToActivatePropertiesSet;
	protected String readyToActivateProperties;
	protected long encryptionKeyMaxAgeMin;

	public MessageResponse processMessage(MessageChainTaskInfo taskInfo, Message message) throws ServiceException{
		MessageData_CA data = (MessageData_CA)message.getData();
		GenericRequestType requestType;
		try {
			requestType = data.getRequestType();
			String text;
			DeviceInfo deviceInfo;
			try {
				deviceInfo = message.getDeviceInfo();
			} catch(ServiceException e) {
				log.warn("Could not get Device Info", e);
				text = message.getTranslator().translate("client.message.server-error", "Could not get device info");
				MessageProcessingUtils.writeGenericResponseV4_1(message, (requestType == GenericRequestType.UPDATE_STATUS_REQUEST ? GenericResponseResultCode.OKAY : GenericResponseResultCode.APPLAYER_ERROR), text, GenericResponseServerActionCode.NO_ACTION);
				return MessageResponse.CONTINUE_SESSION;
			}
			Map<String, Object> params = new HashMap<String, Object>();

			String deviceName = message.getDeviceName();

			switch(requestType) {
				case UPDATE_STATUS_REQUEST:
					UpdateStatusRequest uRequest=(UpdateStatusRequest)data.getRequestTypeData();
					int statusCodeBitmap = uRequest.getStatusCodeBitmap();
					if(message.getLog().isInfoEnabled()) {
						message.getLog().info("Received messageId: " + data.getMessageId()
								 + ", requestType: " + requestType
								 + ", statusCodeBitmap: " + statusCodeBitmap);
					}
					deviceInfo.refreshSafely();
					// Detect whether we have already done this in this session
					boolean timeUpdateSent = ConvertUtils.getBooleanSafely(message.getSessionAttribute("time-update-sent"), false);
					if(!timeUpdateSent) {
						// send GenericResponse with utc offset in property list
						text = message.getTranslator().translate("client.message.updating-time", "Syncing Date/Time with Server");
						Map<Integer, String> props = AppLayerUtils.constructUSRPropertyList(message, readyToActivatePropertiesSet);

						if(!deviceInfo.isInitOnly()) {
							if(ConvertUtils.getBooleanSafely(message.getSessionAttribute("prevEncKeyUseDetected"), false)) {
								props.put(DeviceProperty.ENCRYPTION_KEY.getValue(), StringUtils.toHex(deviceInfo.getEncryptionKey()));
								log.info(deviceName + ": Detected use of previous encryption key by device, sending current key to device...");
							} else {
								params.put("deviceName", message.getDeviceName());
								long deviceId;
								long encryptionKeyGenTsMs;
								boolean okay = false;
								try {
									DataLayerMgr.selectInto("GET_DEVICE_FOR_KEY_ROTATE", params);
									deviceId = ConvertUtils.getInt(params.get("deviceId"));
									encryptionKeyGenTsMs = ConvertUtils.getLong(params.get("encryptionKeyGenTsMs"));
									if(System.currentTimeMillis() > encryptionKeyGenTsMs + encryptionKeyMaxAgeMin * 60 * 1000) {
										log.info(deviceName + ": Encryption key expired, generating new key...");
										byte[] newEncryptionKey = AppLayerUtils.generateEncryptionKey(log);

										deviceInfo.setPreviousEncryptionKey(deviceInfo.getEncryptionKey());
										deviceInfo.setEncryptionKey(newEncryptionKey);

										params.clear();
										params.put("encryptionKey", newEncryptionKey);
										params.put("deviceId", deviceId);
										DataLayerMgr.executeCall("UPDATE_DEVICE_ENCRYPTION_KEY", params, true);
										long updateTime = ConvertUtils.getLongSafely(params.get("updateTime"), message.getServerTime());
										deviceInfo.commitChanges(updateTime);
										okay = true;
										props.put(DeviceProperty.ENCRYPTION_KEY.getValue(), StringUtils.toHex(newEncryptionKey));
										log.info(deviceName + ": Generated new encryption key, sending to device...");
									}
								} catch(Exception e) {
									log.warn(deviceName + ": Unable to rotate encryption key", e);
								} finally {
									if(!okay)
										deviceInfo.clearChanges();
								}
							}
						}

						MessageProcessingUtils.writeGenericResponseWithPropertyListV4_1(message, GenericResponseResultCode.OKAY, text, props);
						message.setSessionAttribute("time-update-sent", true);
						if(message.getLog().isInfoEnabled())
							message.getLog().info("Sending time update");
						return MessageResponse.CONTINUE_SESSION;
					}
					// see if we have other updates
					int maxExecuteOrder = statusCodeBitmap == 0 ? ProcessingConstants.MAX_COMMAND_EXECUTE_ORDER : 10;
					try {
						AppLayerUtils.processUpdateStatusRequest(message, maxExecuteOrder);
					} catch(BufferOverflowException e) {
						log.warn("Error while trying to process generic request - update status request", e);
						text = message.getTranslator().translate("client.message.server-error", "Error occured on Server");
						MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.OKAY, text, GenericResponseServerActionCode.NO_ACTION);
					}
					return MessageResponse.CONTINUE_SESSION;
				case FILE_TRANSFER_REQUEST:
					FileTransferRequest fRequest=(FileTransferRequest)data.getRequestTypeData();
					int fileTypeId = fRequest.getFileType().getValue();
					String fileName = fRequest.getFileName();
					int filePacketSize = getDefaultFilePacketSize();
					int fileGroupNum = 0; /*b/c half-duplex is enforced, only one file transfer occurs at a time and there is not file Group Num*/

					if(message.getLog().isInfoEnabled()) {
						message.getLog().info("Received messageId: " + data.getMessageId()
								 + ", requestType: " + requestType
								 + ", fileTypeId: " + fileTypeId
								 + ", fileName: " + fileName);
					}

					//Check permission
					switch(fileTypeId) {
						case 1:
							String configFileName = message.getDeviceName() + "-CFG";
							if(fileName == null || (fileName=fileName.trim()).length() == 0) {
								fileName = configFileName;
							} else if(!fileName.equals(configFileName)) {
								log.warn("Device  '" + message.getDeviceName() + "' requested file '" + fileName + "' of type " + fileTypeId + " which is not allowed");
								text = message.getTranslator().translate("client.message.restricted-file", "Requested file is not allowed");
								MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.DENIED, text, GenericResponseServerActionCode.NO_ACTION);
								return MessageResponse.CONTINUE_SESSION;
							}
							break;
						case 35:
							break;
						default:
							log.warn("Device  '" + message.getDeviceName() + "' requested file '" + fileName + "' of type " + fileTypeId + " which is not allowed");
							text = message.getTranslator().translate("client.message.restricted-file-type", "Requested file type is not allowed");
							MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.DENIED, text, GenericResponseServerActionCode.NO_ACTION);
							return MessageResponse.CONTINUE_SESSION;
					}
					//Get the file
					params.clear();
					params.put("deviceName", message.getDeviceName());
					params.put("fileName", fileName);
					params.put("fileTypeId", fileTypeId);
					params.put("dataType", "C8");
					params.put("fileGroupNum", fileGroupNum);
					params.put("filePacketSize", filePacketSize);

					Blob fileContent;
					Calendar fileCreateTime;
					try {
						DataLayerMgr.executeCall("CREATE_OUTBOUND_FILE_TRANSFER", params, true);
						fileContent = ConvertUtils.convert(Blob.class, params.get("fileContent"));
						if(fileContent == null) {
							log.warn("Could not find file '" + fileName + "' of type " + fileTypeId + " for device '" + message.getDeviceName() + "'");
							text = message.getTranslator().translate("client.message.invalid-filename", "Could not find requested file");
							MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.DENIED, text, GenericResponseServerActionCode.NO_ACTION);
						} else {
							fileCreateTime = ConvertUtils.convert(Calendar.class, params.get("fileCreateTime"));
							long commandId = ConvertUtils.getLong(params.get("commandId"));
							long fileTransferId = ConvertUtils.getLong(params.get("fileTransferId"));
							if(fileContent.length() > filePacketSize) {
								MessageProcessingUtils.writeFileTransferStart(message, fileTransferId, commandId, fileGroupNum, fileName, fileTypeId, filePacketSize, fileCreateTime, fileContent, MessageType.FILE_XFER_START_4_1, null);
								if(message.getLog().isInfoEnabled())
									message.getLog().info("Sending file transfer start for command " + commandId);
							} else {
								MessageProcessingUtils.writeShortFileTransfer(message, fileGroupNum, fileName, fileTypeId, fileCreateTime, fileContent);
								if(message.getLog().isInfoEnabled())
									message.getLog().info("Sending short file transfer for command " + commandId);
							}
							message.setNextCommandId(commandId);
						}
					} catch(SQLException e) {
						log.warn("Could not retrieve file '" + fileName + "' of type " + fileTypeId + " for device '" + message.getDeviceName() + "'", e);
						text = message.getTranslator().translate("client.message.server-error-file-transfer", "Could not retrieve file");
						MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.APPLAYER_ERROR, text, GenericResponseServerActionCode.NO_ACTION);
					} catch(DataLayerException e) {
						log.warn("Could not retrieve file '" + fileName + "' of type " + fileTypeId + " for device '" + message.getDeviceName() + "'", e);
						text = message.getTranslator().translate("client.message.server-error-file-transfer", "Could not retrieve file");
						MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.APPLAYER_ERROR, text, GenericResponseServerActionCode.NO_ACTION);
					} catch(ConvertException e) {
						log.warn("Could not retrieve file '" + fileName + "' of type " + fileTypeId + " for device '" + message.getDeviceName() + "'", e);
						text = message.getTranslator().translate("client.message.server-error-file-transfer", "Could not retrieve file");
						MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.APPLAYER_ERROR, text, GenericResponseServerActionCode.NO_ACTION);
					} catch(BufferOverflowException e) {
						log.warn("Could not send file '" + fileName + "' of type " + fileTypeId + " for device '" + message.getDeviceName() + "'", e);
						text = message.getTranslator().translate("client.message.server-error-file-transfer", "Could not retrieve file");
						MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.APPLAYER_ERROR, text, GenericResponseServerActionCode.NO_ACTION);
					} catch(IOException e) {
						log.warn("Could not send file '" + fileName + "' of type " + fileTypeId + " for device '" + message.getDeviceName() + "'", e);
						text = message.getTranslator().translate("client.message.server-error-file-transfer", "Could not retrieve file");
						MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.APPLAYER_ERROR, text, GenericResponseServerActionCode.NO_ACTION);
					} catch(ServiceException e) {
						log.warn("Could not send file '" + fileName + "' of type " + fileTypeId + " for device '" + message.getDeviceName() + "'", e);
						text = message.getTranslator().translate("client.message.server-error-file-transfer", "Could not retrieve file");
						MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.APPLAYER_ERROR, text, GenericResponseServerActionCode.NO_ACTION);
					}
					return MessageResponse.CONTINUE_SESSION;
				case ACTIVATION_REQUEST:
					//TODO: read property value list from device and store somewhere
					if(message.getLog().isInfoEnabled()) {
						message.getLog().info("Received messageId: " + data.getMessageId()
								 + ", requestType: " + requestType);
					}

					deviceInfo.refreshSafely();
					//check and update AppLayerDB (Cluster)
					switch(deviceInfo.getActivationStatus()) {
						case FACTORY: //Factory
						case NOT_ACTIVATED: //Not Activated
							//Return denied if device is in not activated state
							MessageProcessingUtils.writeGenericResponseWithPropertyListV4_1(message, GenericResponseResultCode.DENIED, null, Collections.singletonMap(DeviceProperty.ACTIVATION_STATUS.getValue(), String.valueOf(deviceInfo.getActivationStatus().getValue())));
							if(message.getLog().isInfoEnabled())
								message.getLog().info("Sending Activation Request Denied because activation status is " + deviceInfo.getActivationStatus());
							return MessageResponse.CONTINUE_SESSION;
						case READY_TO_ACTIVATE: //Activable
							deviceInfo.setActivationStatus(ActivationStatus.ACTIVATED);
							deviceInfo.setActionCode(AppLayerUtils.determineActionCode(message));
							text = message.getTranslator().translate("client.message.activated", "Ready to accept sales!");
							prepareActivateMessage(message, deviceInfo, text);
							try {
								deviceInfo.commitChanges(message.getServerTime()); // this will enqueue change to oracle
							} catch(ServiceException e) {
								log.warn("Could not commit activation update for device '" + message.getDeviceName() + "'", e);
								text = message.getTranslator().translate("client.message.server-error", "Could not activate device");
								MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.APPLAYER_ERROR, text, GenericResponseServerActionCode.NO_ACTION);
								return MessageResponse.CONTINUE_SESSION;
							}
							if(message.getLog().isInfoEnabled())
								message.getLog().info("Sending Activation Request Accepted");
							return MessageResponse.CONTINUE_SESSION;
						case ACTIVATED: //Activated
							text = message.getTranslator().translate("client.message.activated-already", "Already accepting sales!");
							prepareActivateMessage(message, deviceInfo, text);
							try {
								deviceInfo.commitChanges(message.getServerTime());
							} catch(ServiceException e) {
								log.warn("Could not commit Action Code update for device '" + message.getDeviceName() + "'", e);
							}
							if(message.getLog().isInfoEnabled())
								message.getLog().info("Sending Activation Request Accepted -- Device was already activated");
							return MessageResponse.CONTINUE_SESSION;
						default: // Invalid
							log.warn("Invalid activation status for device '" + message.getDeviceName() + "': " + deviceInfo.getActivationStatus());
							text = message.getTranslator().translate("client.message.server-error", "Could not activate device");
							MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.APPLAYER_ERROR, text, GenericResponseServerActionCode.NO_ACTION);
							return MessageResponse.CONTINUE_SESSION;
					}
				case PROPERTY_LIST_REQUEST:
					PropertyListRequest pRequest=(PropertyListRequest)data.getRequestTypeData();
					//TODO: Should we not send any when the device is not yet activated?
					String propertyListString=pRequest.toString();
					if(message.getLog().isInfoEnabled()) {
						message.getLog().info("Received messageId: " + data.getMessageId()
								 + ", requestType: " + requestType
								 + ", propertyList: " + propertyListString);
					}

					Set<Integer> propertyList;

					propertyList = pRequest.getPropertyIndexes();

					// Handle dynamic properties (and those not allowed)
					Map<Integer,String> propertyListMap;
					CountingAppendable cAppend=new CountingAppendable();
					try {
						propertyListMap = AppLayerUtils.constructPropertyValue(deviceInfo, message.getData().getMessageId(), propertyList, true);
						MessageDataUtils.appendEfficientPropertyValueList(cAppend, propertyListMap, false);
					} catch(SQLException e) {
						log.warn("Could not retrieve property list for device '" + message.getDeviceName() + "'", e);
						text = message.getTranslator().translate("client.message.server-error", "Could not retrieve property list");
						MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.APPLAYER_ERROR, text, GenericResponseServerActionCode.NO_ACTION);
						return MessageResponse.CONTINUE_SESSION;
					} catch(DataLayerException e) {
						log.warn("Could not retrieve property list for device '" + message.getDeviceName() + "'", e);
						text = message.getTranslator().translate("client.message.server-error", "Could not retrieve property list");
						MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.APPLAYER_ERROR, text, GenericResponseServerActionCode.NO_ACTION);
						return MessageResponse.CONTINUE_SESSION;
					} catch(ConvertException e) {
						log.warn("Could not retrieve property list for device '" + message.getDeviceName() + "'", e);
						text = message.getTranslator().translate("client.message.server-error", "Could not retrieve property list");
						MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.APPLAYER_ERROR, text, GenericResponseServerActionCode.NO_ACTION);
						return MessageResponse.CONTINUE_SESSION;
					} catch(IOException e) {
						log.warn("Could not write property list for device '" + message.getDeviceName() + "'", e);
						text = message.getTranslator().translate("client.message.server-error", "Could not write property list");
						MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.APPLAYER_ERROR, text, GenericResponseServerActionCode.NO_ACTION);
						return MessageResponse.CONTINUE_SESSION;
					}

					filePacketSize = getDefaultFilePacketSize();
					//check length and switch to fileTransfer if necessary
					if(cAppend.getSize() > filePacketSize) {
						fileGroupNum = 0; /*b/c half-duplex is enforced, only one file transfer occurs at a time and there is not file Group Num*/
						fileCreateTime = Calendar.getInstance();
						fileName = message.getDeviceName() + "-CFG-partial-" + ConvertUtils.formatObject(fileCreateTime.getTime(), "DATE:yyyy-MM-dd_HH-mm-ss.SSS");
						fileTypeId = 19;
						CRCType crcType = CRCType.CRC16Server;
						long fileSize;
						try {
							Resource resource = message.addFileTransfer(true, 0, 0, fileGroupNum, fileName, fileTypeId, filePacketSize, cAppend.getSize());
							CRCOutputStream out;
							try {
								out = new CRCOutputStream(resource.getOutputStream(), crcType);
								Writer writer = new OutputStreamWriter(out, deviceInfo.getDeviceCharset());
								MessageDataUtils.appendEfficientPropertyValueList(writer, propertyListMap, false);
								writer.flush();
								fileSize = resource.getLength();
								out.close();
							} catch(IOException e) {
								log.warn("Could not send file '" + fileName + "' of type " + fileTypeId + " for device '" + message.getDeviceName() + "'", e);
								text = message.getTranslator().translate("client.message.server-error-file-transfer", "Could not write file");
								MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.APPLAYER_ERROR, text, GenericResponseServerActionCode.NO_ACTION);
								return MessageResponse.CONTINUE_SESSION;
							} finally {
								resource.release();
							}
							MessageProcessingUtils.writeFileTransferStart(message, fileGroupNum, fileName, fileTypeId, filePacketSize, fileCreateTime, fileSize, crcType, out.getCRCValue(), MessageType.FILE_XFER_START_4_1);
						} catch(ServiceException e) {
							log.warn("Could not create property list", e);
							text = message.getTranslator().translate("client.message.server-error", "Could not create property list");
							MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.APPLAYER_ERROR, text, GenericResponseServerActionCode.NO_ACTION);
							return MessageResponse.CONTINUE_SESSION;
						}
						if(message.getLog().isInfoEnabled())
							message.getLog().info("Sending file transfer start for property value list request of " + propertyList.size() + " properties");
						return MessageResponse.CONTINUE_SESSION;
					}
					text = message.getTranslator().translate("client.message.updating-properties", "Updating properties...");
					try {
						MessageProcessingUtils.writeGenericResponseWithPropertyListV4_1(message, GenericResponseResultCode.OKAY, text, propertyListMap);
					} catch(ServiceException e) {
						log.warn("Could not write property list", e);
						text = message.getTranslator().translate("client.message.server-error", "Could not write property list");
						MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.APPLAYER_ERROR, text, GenericResponseServerActionCode.NO_ACTION);
					}
					if(message.getLog().isInfoEnabled())
						message.getLog().info("Sending generic response for property value list request of " + propertyList.size() + " properties");
					return MessageResponse.CONTINUE_SESSION;
				default:
					if(message.getLog().isWarnEnabled())
						message.getLog().warn("Unsupported Command: " + requestType);
					text = message.getTranslator().translate("client.message.unsupported-command", "Not supported");
					MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.NOT_SUPPORTED, text, GenericResponseServerActionCode.SEND_UPDATE_STATUS_REQUEST_IMMEDIATE);
					return MessageResponse.CONTINUE_SESSION;
			}
		} catch(BufferUnderflowException e) {
			if(message.getLog().isWarnEnabled())
				message.getLog().warn("Invalid message - not enough data" , e);
			String text = message.getTranslator().translate("client.message.data-truncated", "Invalid Message");
			MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.APPLAYER_ERROR, text, GenericResponseServerActionCode.NO_ACTION);//TODO: what should the action be here?
		}

		return MessageResponse.CONTINUE_SESSION;
	}

	/**
	 * @param message
	 * @param deviceInfo
	 * @param text
	 */
	protected void prepareActivateMessage(Message message, DeviceInfo deviceInfo, String text) throws ServiceException{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("deviceName", deviceInfo.getDeviceName());
		Object[] clearCommandIds;
		Connection conn = null;
		try {
			conn = DataLayerMgr.getConnectionForCall("GET_PENDING_PROP_LIST_COMMAND_IDS");
			Results results = DataLayerMgr.executeQuery(conn, "GET_PENDING_PROP_LIST_COMMAND_IDS", params);
			try {
				clearCommandIds = results.getAggregate("commandId", null, Aggregate.SET, Object[].class);
			} finally {
				results.close();
			}

			Map<Integer,String> priorityPropertyValues =new LinkedHashMap<Integer, String>();
			priorityPropertyValues.put(DeviceProperty.ACTIVATION_STATUS.getValue(), String.valueOf(ActivationStatus.ACTIVATED.getValue()));
			CountingAppendable cAppend=new CountingAppendable();
			MessageDataUtils.appendEfficientPropertyValueList(cAppend, priorityPropertyValues, false);

			Map<Integer,String> propertyValues=AppLayerUtils.constructPropertyValue(deviceInfo, message.getData().getMessageId(), null, true, conn);
			HeapBufferStream bufferStream = AppLayerUtils.constructPropertyValueStream(deviceInfo, propertyValues);
			if(cAppend.getSize() + bufferStream.size() > getDefaultFilePacketSize()) {
				Calendar fileCreateTime = Calendar.getInstance();
				String fileName = deviceInfo.getDeviceName() + "-CFG-partial-" + ConvertUtils.formatObject(fileCreateTime.getTime(), "DATE:yyyy-MM-dd_HH-mm-ss.SSS");
				Object commandId = AppLayerUtils.addPendingFileTransfer(19, fileName, bufferStream.getInputStream(), bufferStream.size(), deviceInfo.getDeviceName(), getDefaultFilePacketSize(), 1, conn);
				conn.commit();
				//MessageResponseUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.OKAY, text, GenericResponseServerActionCode.SEND_UPDATE_STATUS_REQUEST_IMMEDIATE);
				MessageProcessingUtils.writeGenericResponseWithPropertyListV4_1(message, GenericResponseResultCode.OKAY, text, priorityPropertyValues);
				message.setSessionAttribute("triggerClearCommandId", commandId);
		    	deviceInfo.setActionCode(ServerActionCode.PERFORM_CALL_IN);
			} else if(bufferStream.size() > 0) {
				CompositeMap<Integer,String> merged=new CompositeMap<Integer,String>();
        		merged.merge(priorityPropertyValues);
        		merged.merge(propertyValues);
        		MessageProcessingUtils.writeGenericResponseWithPropertyListV4_1(message, GenericResponseResultCode.OKAY, null, merged);
        	} else {
        		MessageProcessingUtils.writeGenericResponseWithPropertyListV4_1(message, GenericResponseResultCode.OKAY, text, priorityPropertyValues);
        	}
			if(clearCommandIds != null && clearCommandIds.length > 0) {
				message.setSessionAttribute("clearCommandIds", clearCommandIds);
			}
		} catch(IOException e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
			log.error("Could not write property list", e);
			MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.OKAY, text, GenericResponseServerActionCode.NO_ACTION);
			return;
		} catch(SQLException e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
			log.warn("Could not get pending commands or property values from database", e);
			MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.OKAY, text, GenericResponseServerActionCode.NO_ACTION);
			return;
		} catch(ConvertException e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
			log.warn("Could not get pending commands or property values from database", e);
			MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.OKAY, text, GenericResponseServerActionCode.NO_ACTION);
			return;
		} catch(DataLayerException e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
			log.warn("Could not get pending commands or property values from database", e);
			MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.OKAY, text, GenericResponseServerActionCode.NO_ACTION);
			return;
		} finally {
			ProcessingUtils.closeDbConnection(log, conn);
		}
	}

	public String getReadyToActivatePropertiesSet() {
		return readyToActivateProperties;
	}
	public void setReadyToActivateProperties(String readyToActivateProperties) throws ParseException {
		this.readyToActivateProperties = readyToActivateProperties;
		if(readyToActivateProperties == null || (readyToActivateProperties=readyToActivateProperties.trim()).length() == 0)
			this.readyToActivatePropertiesSet = Collections.emptySet();
		else
			this.readyToActivatePropertiesSet = ProcessingUtils.parsePropertyIndexList(readyToActivateProperties);
	}

	public long getEncryptionKeyMaxAgeMin() {
		return encryptionKeyMaxAgeMin;
	}

	public void setEncryptionKeyMaxAgeMin(long encryptionKeyMaxAgeMin) {
		this.encryptionKeyMaxAgeMin = encryptionKeyMaxAgeMin;
	}
}
