package com.usatech.applayer;

import java.nio.BufferOverflowException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;

import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.DeviceInfo;
import com.usatech.layers.common.Message;
import com.usatech.layers.common.MessageProcessingUtils;
import com.usatech.layers.common.MessageResponse;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.constants.GenericResponseResultCode;
import com.usatech.layers.common.constants.GenericResponseServerActionCode;
import com.usatech.layers.common.messagedata.MessageData_CB;

public class MessageProcessor_CB extends InboundFileTransferBlockAckMessageProcessor implements MessageProcessor {
	private static final Log log = Log.getLog();
	public MessageResponse processMessage(MessageChainTaskInfo taskInfo, Message message) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		Map<String, Object> attributes = step.getAttributes();
		if (attributes.containsKey("fileBlockOffset"))
			return super.processMessage(taskInfo, message);
		
		MessageData_CB data = (MessageData_CB)message.getData();

		DeviceInfo deviceInfo;
		try {
			deviceInfo = message.getDeviceInfo();
		} catch(ServiceException e) {
			log.warn("Could not get Device Info", e);
			String text = message.getTranslator().translate("client.message.server-error", "Could not get device info");
			MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.OKAY, text, GenericResponseServerActionCode.NO_ACTION);
			return MessageResponse.CONTINUE_SESSION;
		}
		
		//Test client's result code
		String text;
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("deviceName", message.getDeviceName());
		int rows;
		boolean checkActionCode = false;
		try {
			switch(data.getResultCode()) {
				case OKAY: //Okay
					Long triggerClearCommandId = ConvertUtils.convertSafely(Long.class, message.getSessionAttribute("triggerClearCommandId"), null);
					Object clearCommandIds = message.getSessionAttribute("clearCommandIds");
					if(message.getLastCommandId() != null) {
						params.put("pendingCommandId", message.getLastCommandId());
						if(triggerClearCommandId != null && triggerClearCommandId.equals(message.getLastCommandId()) && clearCommandIds != null) {
							params.put("clearCommandIds", clearCommandIds);
						}
						params.put("globalSessionCode", message.getGlobalSessionCode());
						rows = DataLayerMgr.executeUpdate("UPDATE_PENDING_COMMAND_SUCCESS", params, true);
						if(triggerClearCommandId != null && triggerClearCommandId.equals(message.getLastCommandId()) && clearCommandIds != null) {
							message.setSessionAttribute("triggerClearCommandId", null);
							message.setSessionAttribute("clearCommandIds", null);
						}
						if(params.get("dataType") == null) {
							log.warn("Had pending command id " + message.getLastCommandId() + " but could not find in database");
						} else {
							checkActionCode = true;
						}
					} else {
						if(triggerClearCommandId == null && clearCommandIds != null) {
							params.put("clearCommandIds", clearCommandIds);
						}
						//Reset any pending commands
						rows = DataLayerMgr.executeUpdate("UPDATE_PENDING_COMMAND_RESET_ANY", params, true);
						if(triggerClearCommandId == null && clearCommandIds != null) {
							message.setSessionAttribute("clearCommandIds", null);
						}
					}
					break;
				case DENIED: case NOT_SUPPORTED:
					// Mark pending command as failed so it is not retried and send CB
					if(message.getLastCommandId() != null) {
						params.put("pendingCommandId", message.getLastCommandId());
						rows = DataLayerMgr.executeUpdate("UPDATE_PENDING_COMMAND_CANCEL", params, true);
						if(rows < 1) {
							log.warn("Had pending command id " + message.getLastCommandId() + " but could not find in database to cancel it");
						}
						break;
					}
					// Reset any pending commands
					log.warn("Received ResultCode " + data.getResultCode() + " but pending command is null");
					rows = DataLayerMgr.executeUpdate("UPDATE_PENDING_COMMAND_RESET_ANY", params, true);
					break;
				default: // Errors
					if(message.getLastCommandId() != null) {
						params.put("pendingCommandId", message.getLastCommandId());
						rows = DataLayerMgr.executeUpdate("UPDATE_PENDING_COMMAND_RESET", params, true);
						if(rows < 1) {
							log.warn("Had pending command id " + message.getLastCommandId() + " but could not find in database");
						}
					} else {
						//Reset any pending commands
						rows = DataLayerMgr.executeUpdate("UPDATE_PENDING_COMMAND_RESET_ANY", params, true);
					}
					break;
			}
			switch(data.getClientActionCode()) {
				case NO_ACTION:
					if(checkActionCode) {
						// Action Code needs to be updated at this point but for now just allow device control
						deviceInfo.setActionCode(AppLayerUtils.determineActionCode(message));
						deviceInfo.commitChanges(message.getServerTime());
					}
					text = message.getTranslator().translate("client.message.allow-device-control", "");
					MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.OKAY, text, deviceInfo.getActionCode().getGenericResponseServerActionCode());
					if(message.getLog().isInfoEnabled())
						message.getLog().info("Sending generic response with no action");
					break;
				case UPDATE_STATUS_REQUEST:
					AppLayerUtils.processUpdateStatusRequest(message, ProcessingConstants.MAX_COMMAND_EXECUTE_ORDER);
					break;
				case STOP_FILE_TRANSFER:
					//This shouldn't get here but code for it anyway
					text = message.getTranslator().translate("client.message.allow-device-control", "");
					MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.OKAY, text, GenericResponseServerActionCode.NO_ACTION);
					if(message.getLog().isInfoEnabled())
						message.getLog().info("Sending generic response with no action");
					break;
				default:
					log.warn("Unrecognized Action Code " + data.getClientActionCode());
					text = message.getTranslator().translate("client.message.server-error", "Error occured on Server");
					MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.NOT_SUPPORTED, text, deviceInfo.getActionCode().getGenericResponseServerActionCode());
					if(message.getLog().isInfoEnabled())
						message.getLog().info("Sending generic response with not supported result");
			}
		} catch(SQLException e) {
			log.warn("Error while trying to process generic response", e);
			//text = message.getTranslator().translate("client.message.server-error", "Error occured on Server");
			//MessageResponseUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.APPLAYER_ERROR, text, deviceInfo.getActionCode().getGenericResponseServerActionCode());
			MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.OKAY, null, deviceInfo.getActionCode().getGenericResponseServerActionCode());
		} catch(DataLayerException e) {
			log.warn("Error while trying to process generic response", e);
			//text = message.getTranslator().translate("client.message.server-error", "Error occured on Server");
			//MessageResponseUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.APPLAYER_ERROR, text, deviceInfo.getActionCode().getGenericResponseServerActionCode());
			MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.OKAY, null, deviceInfo.getActionCode().getGenericResponseServerActionCode());
		} catch(BufferOverflowException e) {
			log.warn("Error while trying to process generic response", e);
			//text = message.getTranslator().translate("client.message.server-error", "Error occured on Server");
			//MessageResponseUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.APPLAYER_ERROR, text, deviceInfo.getActionCode().getGenericResponseServerActionCode());
			MessageProcessingUtils.writeGenericResponseV4_1(message, GenericResponseResultCode.OKAY, null, deviceInfo.getActionCode().getGenericResponseServerActionCode());
		}
		return MessageResponse.CONTINUE_SESSION;
	}
}
