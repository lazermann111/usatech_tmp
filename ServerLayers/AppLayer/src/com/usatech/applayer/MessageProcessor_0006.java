package com.usatech.applayer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import simple.app.ServiceException;
import simple.db.DataLayerMgr;

import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.CommonProcessing;
import com.usatech.layers.common.Message;
import com.usatech.layers.common.MessageProcessingUtils;
import com.usatech.layers.common.MessageResponse;
import com.usatech.layers.common.messagedata.MessageData_0006;

public class MessageProcessor_0006 extends CommonProcessing implements MessageProcessor {
	protected final Callable<?>[] checks = new Callable[16];
	/**
	 *
	 */
	public MessageProcessor_0006() {
		checks[0] = new Callable<Void>() {
			@Override
			public Void call() throws Exception {
				DataLayerMgr.executeSQL("OPER", "SELECT * FROM DUAL", null, null);
				return null;
			}
		};
		checks[1] = new Callable<Void>() {
			@Override
			public Void call() throws Exception {
				DataLayerMgr.executeSQL("EMBED", "VALUES(1)", null, null);
				return null;
			}
		};
	}
	public MessageResponse processMessage(MessageChainTaskInfo taskInfo, Message message) throws ServiceException {
		MessageData_0006 data = (MessageData_0006)message.getData();
		int checked = 0;
		int success = 0;
		List<String> errors = new ArrayList<String>();
		for(int i = 0; i < checks.length && i < 16; i++) {
			int factor = 1 << i;
			if(checks[i] != null && (data.getBitmap() & factor) == factor) {
				checked |= factor;
				try {
					checks[i].call();
					success |= factor;
				} catch(Exception e) {
					errors.add(e.getMessage() == null ? e.toString() : e.getMessage());
				}
			}
		}
		MessageProcessingUtils.writeAppLayerHealthCheckResponse(message, checked, success, errors);
		return MessageResponse.CONTINUE_SESSION;
	}
}
