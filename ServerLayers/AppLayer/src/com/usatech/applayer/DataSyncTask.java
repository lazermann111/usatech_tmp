package com.usatech.applayer;

import java.lang.management.ManagementFactory;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanRegistrationException;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;

import simple.app.DialectResolver;
import simple.app.PrerequisiteManager;
import simple.app.Publisher;
import simple.app.SelfProcessor;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.ByteInput;
import simple.io.Log;
import simple.results.Results;
import simple.security.crypt.EncryptionInfoMapping;
import simple.util.concurrent.CustomThreadFactory;
import simple.util.concurrent.NotifyingBlockingThreadPoolExecutor;
import simple.util.concurrent.NotifyingBlockingWithStatsThreadPoolExecutor;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.DeviceInfo;
import com.usatech.layers.common.DeviceInfoManager.OnMissingPolicy;
import com.usatech.layers.common.constants.DataSyncType;
import com.usatech.layers.common.constants.DeviceInfoProperty;

public class DataSyncTask implements SelfProcessor {
	private static final Log log = Log.getLog();

	protected int appInstance;
	protected int maxItems = 100;
	protected int poolSize = 5;
	protected int queueSize = 10;
	protected String dataSyncQuery = null;
	protected long waitCheckInterval = 100;
	protected String keyReceiverQueueKey;
	protected AppLayerDeviceInfoManager deviceInfoManager;
	protected NotifyingBlockingThreadPoolExecutor threadPoolExecutor = null;
	protected AtomicInteger activeCount = new AtomicInteger(0);
	protected Publisher<ByteInput> publisher;
	protected EncryptionInfoMapping encryptionInfoMapping;
	protected final PrerequisiteManager prerequisiteManager = new PrerequisiteManager();
	protected AtomicInteger blacklistInitialized = new AtomicInteger();

	@Override
	public void process() {
		// ensure prereqs are available
		if(!prerequisiteManager.arePrequisitesAvailable()) {
			log.info("Prerequisites are not available. Skipping this run.");
			return;
		}
		if (dataSyncQuery == null)
			dataSyncQuery = "SELECT * FROM (SELECT DATA_SYNC_ID, DATA_SYNC_TYPE_CD, OBJECT_CD, OPERATION_CD, ITEM_ID, ITEM_CD, CREATED_BY, CREATED_UTC_TS, DBADMIN.TIMESTAMP_TO_MILLIS(CREATED_UTC_TS) CREATED_UTC_TS_MS, DATA_SYNC_VALUE"
					+ " FROM ENGINE.DATA_SYNC_" + appInstance
					+ (!DialectResolver.isOracle()
							? " ORDER BY (CASE DATA_SYNC_TYPE_CD WHEN 'MASTER_ID' THEN 2 ELSE 1 END), CREATED_UTC_TS, DATA_SYNC_ID LIMIT ?::bigint) sq_end WHERE 1 = 1"
							: " ORDER BY DECODE(DATA_SYNC_TYPE_CD, 'MASTER_ID', 2, 1), CREATED_UTC_TS, DATA_SYNC_ID) WHERE ROWNUM <= ?");
		if(threadPoolExecutor == null) {
			threadPoolExecutor = new NotifyingBlockingWithStatsThreadPoolExecutor(poolSize, queueSize, new CustomThreadFactory(Thread.currentThread().getName() + '-', true));
			try {
				ManagementFactory.getPlatformMBeanServer().registerMBean(threadPoolExecutor, new ObjectName("simple.app.jmx:Type=ThreadPoolExecutor,Task=" + getClass().getSimpleName()));
			} catch(InstanceAlreadyExistsException e) {
				log.warn("Could not register DataSyncTask MXBean", e);
			} catch(MBeanRegistrationException e) {
				log.warn("Could not register DataSyncTask MXBean", e);
			} catch(NotCompliantMBeanException e) {
				log.warn("Could not register DataSyncTask MXBean", e);
			} catch(MalformedObjectNameException e) {
				log.warn("Could not register DataSyncTask MXBean", e);
			} catch(NullPointerException e) {
				log.warn("Could not register DataSyncTask MXBean", e);
			}
		}
		Results results;
		// init blacklist
		if(blacklistInitialized.compareAndSet(0, 1)) {
			boolean okay = false;
			try {
				results = DataLayerMgr.executeQuery("GET_BLACKLISTED", null);
				try {
					while(results.next())
						deviceInfoManager.addBlacklisted(results.getValue("globalAccountId", Long.class));
				} finally {
					results.close();
				}
				okay = true;
			} catch(SQLException | DataLayerException | ConvertException e) {
				log.error("Could not load blacklist", e);
			} finally {
				blacklistInitialized.compareAndSet(1, okay ? 2 : 0);
			}
		}
		long dataSyncId = -1;
		try {
			deviceInfoManager.recheckDeviceDetail();
			while (true) {
				results = DataLayerMgr.executeSQL("OPER", dataSyncQuery, new Object[]{maxItems}, null);
				try {
					if(!results.next())
						break;
					
					if(activeCount.get() != 0)
						activeCount.set(0);
					do {
						Map<String, Object> attributes = new HashMap<String, Object>();
						attributes.put("startTimeMs", System.currentTimeMillis());
						attributes.put("appInstance", appInstance);
						attributes.put("dataSyncId", results.get("DATA_SYNC_ID"));
						attributes.put("dataSyncType", results.get("DATA_SYNC_TYPE_CD"));
						attributes.put("objectCd", results.get("OBJECT_CD"));
						attributes.put("operationCd", results.get("OPERATION_CD"));
						attributes.put("itemId", results.get("ITEM_ID"));
						attributes.put("itemCd", results.get("ITEM_CD"));
						attributes.put("createdBy", results.get("CREATED_BY"));
						attributes.put("createdUtcTs", results.get("CREATED_UTC_TS"));
						attributes.put("createdUtcTsMs", results.get("CREATED_UTC_TS_MS"));
						attributes.put("dataSyncValue", results.get("DATA_SYNC_VALUE"));

						dataSyncId = ConvertUtils.getLong(attributes.get("dataSyncId"));
						DataSyncType dataSyncType = DataSyncType.getByValue(String.valueOf(attributes.get("dataSyncType")));
						long itemId = ConvertUtils.getLong(attributes.get("itemId"));
						String itemCd = String.valueOf(attributes.get("itemCd"));

						int subItemCount = 0;
						boolean singleItemDataSync = false;

						switch(dataSyncType) {
							case MASTER_ID:
							case DEVICE_INFO:
							case DEVICE_PTAS:
							case DEVICE:
							case CONSUMER_ACCT:
							case DEVICE_DETAIL:
							case BLACKLIST:
								singleItemDataSync = true;
								enqueueDataSync(new DataSyncItemTask(dataSyncType, deviceInfoManager, attributes, singleItemDataSync));
								break;
							case CUSTOMER_DETAIL:
								subItemCount = updateDeviceDetail(dataSyncType, itemId);
								break;
							case CREDENTIAL:
							case LOCATION:
								subItemCount = updateDevices(dataSyncType, itemId);
								break;
							case AUTHORITY_PAYMENT_MASK:
								subItemCount = updatePaymentSubtypeDetails(dataSyncType, itemId);
								subItemCount += updateDevicePTAs(dataSyncType, itemId, itemCd);
								break;
							case ARAMARK_AUTHORITY:
							case ARAMARK_PAYMENT_TYPE:
							case AUTHORITY:
							case AUTHORITY_SERVICE:
							case BLACKBRD_AUTHORITY:
							case BLACKBRD_TENDER:
							case INTERNAL_AUTHORITY:
							case INTERNAL_PAYMENT_TYPE:
							case MERCHANT:
							case PAYMENT_SUBTYPE:
							case TERMINAL:
								subItemCount = updatePaymentSubtypeDetails(dataSyncType, itemId);
								break;
							case CURRENCY:
								subItemCount = updateDevicePTAs(dataSyncType, itemId, itemCd);
								break;
							default:
								throw new ServiceException("Data sync type " + dataSyncType + " handling has not been implemented");
						}

						if(subItemCount > 0) {
							while(activeCount.get() > 0) {
								if(threadPoolExecutor.await(waitCheckInterval, TimeUnit.MILLISECONDS))
									break;
							}
						}
						if(!singleItemDataSync)
							completeDataSync(attributes);
					} while(results.next());
				} finally {
					results.close();
				}
				
				while (activeCount.get() > 0) {
					if (threadPoolExecutor.await(waitCheckInterval, TimeUnit.MILLISECONDS))
						break;
				}
			}
	    } catch(Exception e) {
	    	log.error("Failed to process data sync, dataSyncId: " + dataSyncId, e);
		}
	}
	
	protected int updateDevices(DataSyncType dataSyncType, long itemId) throws ServiceException {
		Map<String,Object> params = new HashMap<String, Object>();
		Results results;
		try {
			switch(dataSyncType){
				case CREDENTIAL:
					params.put("credentialId", itemId);
					results = DataLayerMgr.executeQuery("GET_CREDENTIAL_DEVICES", params);
					break;
				case LOCATION:
					params.put("locationId", itemId);
					results = DataLayerMgr.executeQuery("GET_LOCATION_DEVICES", params);
					break;				
				default:
					throw new ServiceException("Updating devices for data sync type " + dataSyncType + " has not been implemented");
			}
			int subItemCount = 0;
			while(results.next()) {
				Map<String,Object> attributes = new HashMap<String, Object>();
				attributes.put("itemCd", String.valueOf(results.get("deviceName")));
				if(results.getValue("deviceInfoFlag", Boolean.class) == Boolean.TRUE) {
					enqueueDataSync(new DataSyncItemTask(DataSyncType.DEVICE_INFO, deviceInfoManager, attributes, false));
					subItemCount++;
				}
				if(results.getValue("deviceDetailFlag", Boolean.class) == Boolean.TRUE) {
					enqueueDataSync(new DataSyncItemTask(DataSyncType.DEVICE_DETAIL, deviceInfoManager, attributes, false));
					subItemCount++;
				}
			}
			return subItemCount;
		} catch (Exception e) {
			throw new ServiceException("Error getting devices", e);
		}
	}
	
	protected int updateDeviceDetail(DataSyncType dataSyncType, long itemId) throws ServiceException {
		Map<String, Object> params = new HashMap<String, Object>();
		Results results;
		try {
			switch(dataSyncType) {
				case CUSTOMER_DETAIL:
					params.put("customerId", itemId);
					results = DataLayerMgr.executeQuery("GET_CUSTOMER_DEVICES", params);
					break;
				default:
					throw new ServiceException("Updating device detail for data sync type " + dataSyncType + " has not been implemented");
			}
			int subItemCount = 0;
			while(results.next()) {
				Map<String, Object> attributes = new HashMap<String, Object>();
				attributes.put("itemCd", String.valueOf(results.get("deviceName")));
				enqueueDataSync(new DataSyncItemTask(DataSyncType.DEVICE_DETAIL, deviceInfoManager, attributes, false));
				subItemCount++;
			}
			return subItemCount;
		} catch(Exception e) {
			throw new ServiceException("Error getting devices", e);
		}
	}

	protected int updateDevicePTAs(DataSyncType dataSyncType, long itemId, String itemCd) throws ServiceException {
		Map<String,Object> params = new HashMap<String, Object>();
		Results results;
		try {
			switch(dataSyncType){
				case AUTHORITY_PAYMENT_MASK:
					params.put("authorityPaymentMaskId", itemId);
					results = DataLayerMgr.executeQuery("GET_AUTHORITY_PAYMENT_MASK_DEVICES", params);
					break;
				case CURRENCY:
					params.put("currencyCd", itemCd);
					results = DataLayerMgr.executeQuery("GET_CURRENCY_DEVICES", params);
					break;
				default:
					throw new ServiceException("Updating device PTAs for data sync type " + dataSyncType + " has not been implemented");
			}
			int subItemCount = 0;
			while(results.next()) {
				Map<String,Object> attributes = new HashMap<String, Object>();
				attributes.put("itemCd", String.valueOf(results.get("deviceName")));
				enqueueDataSync(new DataSyncItemTask(DataSyncType.DEVICE_PTAS, deviceInfoManager, attributes, false));
				subItemCount++;
			}
			return subItemCount;
		} catch (Exception e) {
			throw new ServiceException("Error getting devices", e);
		}
	}
	
	protected int updatePaymentSubtypeDetails(DataSyncType dataSyncType, long itemId) throws ServiceException {
		Map<String,Object> params = new HashMap<String, Object>();
		Results results;
		try {
			switch(dataSyncType){
				case ARAMARK_AUTHORITY:
					params.put("aramarkAuthorityId", itemId);
					results = DataLayerMgr.executeQuery("GET_ARAMARK_AUTHORITY_PSD_ALT_KEYS", params);
					break;
				case ARAMARK_PAYMENT_TYPE:
					params.put("aramarkPaymentTypeId", itemId);
					results = DataLayerMgr.executeQuery("GET_ARAMARK_PAYMENT_TYPE_PSD_ALT_KEYS", params);
					break;
				case AUTHORITY:
					params.put("authorityId", itemId);
					results = DataLayerMgr.executeQuery("GET_AUTHORITY_PSD_ALT_KEYS", params);
					break;
				case AUTHORITY_PAYMENT_MASK:
					params.put("authorityPaymentMaskId", itemId);
					results = DataLayerMgr.executeQuery("GET_AUTHORITY_PAYMENT_MASK_PSD_ALT_KEYS", params);
					break;
				case AUTHORITY_SERVICE:
					params.put("authorityServiceId", itemId);
					results = DataLayerMgr.executeQuery("GET_AUTHORITY_SERVICE_PSD_ALT_KEYS", params);
					break;
				case BLACKBRD_AUTHORITY:
					params.put("blackbrdAuthorityId", itemId);
					results = DataLayerMgr.executeQuery("GET_BLACKBRD_AUTHORITY_PSD_ALT_KEYS", params);
					break;
				case BLACKBRD_TENDER:
					params.put("blackbrdTenderId", itemId);
					results = DataLayerMgr.executeQuery("GET_BLACKBRD_TENDER_PSD_ALT_KEYS", params);
					break;
				case INTERNAL_AUTHORITY:
					params.put("internalAuthorityId", itemId);
					results = DataLayerMgr.executeQuery("GET_INTERNAL_AUTHORITY_PSD_ALT_KEYS", params);
					break;
				case INTERNAL_PAYMENT_TYPE:
					params.put("internalPaymentTypeId", itemId);
					results = DataLayerMgr.executeQuery("GET_INTERNAL_PAYMENT_TYPE_PSD_ALT_KEYS", params);
					break;
				case MERCHANT:
					params.put("merchantId", itemId);
					results = DataLayerMgr.executeQuery("GET_MERCHANT_PSD_ALT_KEYS", params);
					break;
				case PAYMENT_SUBTYPE:
					params.put("paymentSubtypeId", itemId);
					results = DataLayerMgr.executeQuery("GET_PAYMENT_SUBTYPE_PSD_ALT_KEYS", params);
					break;
				case TERMINAL:
					params.put("terminalId", itemId);
					results = DataLayerMgr.executeQuery("GET_TERMINAL_PSD_ALT_KEYS", params);
					break;					
				default:
					throw new ServiceException("Updating payment subtype details for data sync type " + dataSyncType + " has not been implemented");
			}
			int subItemCount = 0;
			while(results.next()) {
				Map<String,Object> attributes = new HashMap<String, Object>();
				attributes.put("paymentSubtypeId", results.get("paymentSubtypeId"));
				attributes.put("paymentSubtypeKeyId", results.get("paymentSubtypeKeyId"));
				enqueueDataSync(new DataSyncItemTask(DataSyncType.PAYMENT_SUBTYPE_DETAIL, deviceInfoManager, attributes, false));
				subItemCount++;
			}
			return subItemCount;
		} catch (Exception e) {
			throw new ServiceException("Error getting payment subtype keys", e);
		}
	}
	
	protected void completeDataSync(Map<String,Object> attributes) throws DataLayerException, SQLException {
		long time = System.currentTimeMillis();
		long executionMs = attributes.containsKey("executionTimeMs") ? time - ConvertUtils.getLongSafely(attributes.get("executionTimeMs"), 0) : -1;
		DataLayerMgr.executeCall("DELETE_DATA_SYNC", attributes, true);
		long deletionMs = System.currentTimeMillis() - time;		
		long createdUtcTsMs = ConvertUtils.getLongSafely(attributes.get("createdUtcTsMs"), 0);
		StringBuilder sb = new StringBuilder("Processed data sync, dataSyncId: ").append(attributes.get("dataSyncId"))
			.append(", dataSyncTypeCd: ").append(attributes.get("dataSyncType")).append(", objectCd: ").append(attributes.get("objectCd"))
			.append(", operationCd: ").append(attributes.get("operationCd")).append(", itemId: ").append(attributes.get("itemId"))
			.append(", itemCd: ").append(attributes.get("itemCd"))
			.append(", createdBy: ").append(attributes.get("createdBy")).append(", createdUtcTs: ").append(attributes.get("createdUtcTs"));
		if (attributes.get("dataSyncValue") != null)
			sb.append(", dataSyncValue: ").append(attributes.get("dataSyncValue"));
		sb.append(", time to thread pool: ").append(ConvertUtils.getLongSafely(attributes.get("startTimeMs"), 0) - createdUtcTsMs).append(" ms");
		if (executionMs > -1)
			sb.append(", execution time: ").append(executionMs).append(" ms");
		sb.append(", deletion time: ").append(deletionMs).append(" ms");
		sb.append(", total time: ").append(System.currentTimeMillis() - createdUtcTsMs).append(" ms");
		log.info(sb.toString());
	}
	
	protected class DataSyncItemTask implements Runnable {
		protected DataSyncType dataSyncType;
		protected AppLayerDeviceInfoManager deviceInfoManager;
		protected Map<String,Object> attributes;
		protected boolean singleItemDataSync;
		
		public DataSyncItemTask(DataSyncType dataSyncType, AppLayerDeviceInfoManager deviceInfoManager, Map<String,Object> attributes, boolean singleItemDataSync) {
			this.dataSyncType = dataSyncType;
			this.deviceInfoManager = deviceInfoManager;
			this.attributes = attributes;
			this.singleItemDataSync = singleItemDataSync;
		}
		
		public void run() {
			try {
				long time = System.currentTimeMillis();
				if (singleItemDataSync)
					attributes.put("executionTimeMs", time);
				
				String itemCd = String.valueOf(attributes.get("itemCd"));
				
				DeviceInfo deviceInfo = null;
				switch (dataSyncType) {
					case MASTER_ID:
						long masterId = ConvertUtils.getLongSafely(attributes.get("dataSyncValue"), 0);
						if (masterId > 0) {
							deviceInfo = deviceInfoManager.getDeviceInfo(itemCd, OnMissingPolicy.RETURN_NULL, false);
							if (deviceInfo != null) {
								deviceInfo.updateMasterId(masterId);
								long updateTime;
								try {
									Calendar cal = ConvertUtils.convert(Calendar.class, attributes.get("CREATED_UTC_TS"));
									if(cal != null)
										updateTime = ConvertUtils.getLocalTime(cal.getTimeInMillis(), cal.getTimeZone());
									else
										updateTime = deviceInfo.getTimestamp(DeviceInfoProperty.MASTER_ID);
								} catch(ConvertException e) {
									updateTime = deviceInfo.getTimestamp(DeviceInfoProperty.MASTER_ID);
								}
								deviceInfo.commitChanges(updateTime, true);
							}
						}
						break;
					case DEVICE:
					case DEVICE_INFO:
						MessageChain mc = new MessageChainV11();
						MessageChainStep step = mc.addStep(getKeyReceiverQueueKey());
						step.addStringAttribute("deviceName", itemCd);
						if (dataSyncType == DataSyncType.DEVICE_INFO)
							deviceInfo = deviceInfoManager.getDeviceInfo(itemCd, OnMissingPolicy.RETURN_NULL, false);
						deviceInfo = deviceInfoManager.checkDeviceInfo(deviceInfo, itemCd, dataSyncType == DataSyncType.DEVICE || deviceInfo == null, false);
						if (deviceInfo == null)
							step.addBooleanAttribute("removeDeviceInfo", true);
						else
							deviceInfo.putAllWithTimestamps(step.getAttributes());
						MessageChainService.publish(mc, getPublisher(), getEncryptionInfoMapping());
						break;
					case DEVICE_PTAS:
						deviceInfo = deviceInfoManager.getDeviceInfo(itemCd, OnMissingPolicy.RETURN_NULL, false);
						if (deviceInfo == null)
							deviceInfoManager.checkDeviceInfo(deviceInfo, itemCd, true, false);
						else
							deviceInfoManager.checkDevicePTAs(itemCd, null);
						break;
					case PAYMENT_SUBTYPE_DETAIL:
						PSDAltKey psdKey;
						try {
							psdKey = new PSDAltKey(ConvertUtils.convert(Long.class, attributes.get("paymentSubtypeId")), ConvertUtils.convert(Long.class, attributes.get("paymentSubtypeKeyId")));
						} catch(ConvertException e) {
							throw new ServiceException("Could not get key values for payment subtype detail update", e);
						}	
						deviceInfoManager.checkPaymentSubtypeDetail(psdKey);
						break;
					case CONSUMER_ACCT:
						long itemId = ConvertUtils.getLong(attributes.get("itemId"));
						deviceInfoManager.checkConsumerAcct(itemId);
						break;
					case DEVICE_DETAIL:
						deviceInfoManager.checkDeviceDetail(itemCd);
						break;
					case BLACKLIST:
						itemId = ConvertUtils.getLong(attributes.get("itemId"));
						deviceInfoManager.checkBlacklist(itemId);
						break;
					default:
						throw new ServiceException("Data sync type " + dataSyncType + " handling has not been implemented");
				}				
				
				if (singleItemDataSync)
					completeDataSync(attributes);
				else {
					StringBuilder sb = new StringBuilder("Execution of sub-item data sync ").append(dataSyncType.getValue());
					for (String key: attributes.keySet())
						sb.append(", ").append(key).append(": ").append(attributes.get(key));
					sb.append(" took ").append(System.currentTimeMillis() - time).append(" ms");
					log.info(sb.toString());
				}
			} catch(Exception e) {
				StringBuilder sb = new StringBuilder("Failed to process data sync ").append(dataSyncType.getValue());
				for (String key: attributes.keySet())
					sb.append(", ").append(key).append(": ").append(attributes.get(key));
				log.error(sb.toString(), e);			
			} finally {
				activeCount.decrementAndGet();
			}
		}
	}
	
	protected void enqueueDataSync(DataSyncItemTask dataSyncItemTask) {
		threadPoolExecutor.execute(dataSyncItemTask);
		activeCount.incrementAndGet();
	}

	public int getAppInstance() {
		return appInstance;
	}

	public void setAppInstance(int appInstance) {
		this.appInstance = appInstance;
	}	
	
	public int getMaxItems() {
		return maxItems;
	}

	public void setMaxItems(int maxItems) {
		this.maxItems = maxItems;
	}
	
	public AppLayerDeviceInfoManager getDeviceInfoManager() {
		return deviceInfoManager;
	}

	public void setDeviceInfoManager(AppLayerDeviceInfoManager deviceInfoManager) {
		this.deviceInfoManager = deviceInfoManager;
	}
	
	public int getPoolSize() {
		return poolSize;
	}

	public void setPoolSize(int poolSize) {
		this.poolSize = poolSize;
	}

	public int getQueueSize() {
		return queueSize;
	}

	public void setQueueSize(int queueSize) {
		this.queueSize = queueSize;
	}
	
	public long getWaitCheckInterval() {
		return waitCheckInterval;
	}

	public void setWaitCheckInterval(long waitCheckInterval) {
		this.waitCheckInterval = waitCheckInterval;
	}

	public String getKeyReceiverQueueKey() {
		return keyReceiverQueueKey;
	}

	public void setKeyReceiverQueueKey(String keyReceiverQueueKey) {
		this.keyReceiverQueueKey = keyReceiverQueueKey;
	}
	
	public Publisher<ByteInput> getPublisher() {
		return publisher;
	}
	
	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}
	
	public EncryptionInfoMapping getEncryptionInfoMapping() {
		return encryptionInfoMapping;
	}

	public void setEncryptionInfoMapping(EncryptionInfoMapping encryptionInfoMapping) {
		this.encryptionInfoMapping = encryptionInfoMapping;
	}

	public PrerequisiteManager getPrerequisiteManager() {
		return prerequisiteManager;
	}
}
