package com.usatech.applayer;


import simple.app.ServiceException;

import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.LegacyUpdateStatusProcessor;
import com.usatech.layers.common.Message;
import com.usatech.layers.common.MessageResponse;
/**
 * Update.pm
 * Terminal update status request
 * @author yhe
 *
 */
public class MessageProcessor_92 implements MessageProcessor {

	@Override
	public MessageResponse processMessage(MessageChainTaskInfo taskInfo, Message message) throws ServiceException {
		return LegacyUpdateStatusProcessor.processMessage(taskInfo, message, true);
	}

}


