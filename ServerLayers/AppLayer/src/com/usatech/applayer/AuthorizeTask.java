package com.usatech.applayer;

import java.math.BigDecimal;
import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import com.usatech.app.Attribute;
import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.AppLayerOnlineMessage;
import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.DeviceInfo;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.Message;
import com.usatech.layers.common.MessageProcessingUtils;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.ABPermissionActionCode;
import com.usatech.layers.common.constants.AFActionCode;
import com.usatech.layers.common.constants.AdditionalInfoType;
import com.usatech.layers.common.constants.AuthPermissionActionCode;
import com.usatech.layers.common.constants.AuthResponseType;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAction;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CardType;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.DeniedReason;
import com.usatech.layers.common.constants.DeviceInfoProperty;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.Language;
import com.usatech.layers.common.constants.MessageAttrEnum;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.PaymentActionType;
import com.usatech.layers.common.constants.PaymentMaskBRef;
import com.usatech.layers.common.constants.PosEnvironment;
import com.usatech.layers.common.constants.ServerActionCode;
import com.usatech.layers.common.constants.SessionCloseReason;
import com.usatech.layers.common.messagedata.AuthorizeMessageData;
import com.usatech.layers.common.messagedata.LineItem;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageData_021E;
import com.usatech.layers.common.messagedata.MessageData_0301;
import com.usatech.layers.common.messagedata.MessageData_030B;
import com.usatech.layers.common.messagedata.MessageData_030C;
import com.usatech.layers.common.messagedata.MessageData_030D;
import com.usatech.layers.common.messagedata.MessageData_0310;
import com.usatech.layers.common.messagedata.MessageData_60;
import com.usatech.layers.common.messagedata.MessageData_A1;
import com.usatech.layers.common.messagedata.MessageData_AB;
import com.usatech.layers.common.messagedata.MessageData_AE;
import com.usatech.layers.common.messagedata.MessageData_AF;
import com.usatech.layers.common.messagedata.MessageData_C3;
import com.usatech.layers.common.messagedata.MessageData_C3.AuthorizationV2AuthResponse;
import com.usatech.layers.common.messagedata.MessageData_C3.PermissionAuthResponse;
import com.usatech.layers.common.messagedata.MessageData_C3.PermissionAuthResponse.ConfigurableAction;
import com.usatech.layers.common.messagedata.ReplenishMessageData;
import com.usatech.layers.common.messagedata.Sale;
import com.usatech.layers.common.messagedata.WS2RequestMessageData;
import com.usatech.layers.common.messagedata.WS2TokenizeMessageData;
import com.usatech.layers.common.messagedata.WS2TokenizeMessageData.ExpirationDateContainer;
import com.usatech.layers.common.messagedata.WSRequestMessageData;
import com.usatech.layers.common.util.StringHelper;

import simple.app.DatabasePrerequisite;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.NotEnoughRowsException;
import simple.io.Log;
import simple.lang.InvalidByteValueException;
import simple.lang.InvalidValueException;
import simple.lang.SystemUtils;
import simple.lang.sun.misc.CRC16;
import simple.results.BeanException;
import simple.results.Results;
import simple.security.SecureHash;
import simple.text.StringUtils;
import simple.text.ThreadSafeDateFormat;
import simple.translator.DefaultTranslatorFactory;
import simple.translator.Translator;
import simple.translator.TranslatorFactory;
import simple.util.CollectionUtils;
import simple.util.concurrent.Cache;
import simple.util.concurrent.LockSegmentCache;

/**
 * Adaptation of {@link com.usatech.app.ComponentService} that processes auths
 * from devices
 *
 * @author Brian S. Krug
 *
 */
public class AuthorizeTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected static byte MESSAGE_CHAIN_VERSION = 0x11;
	protected static final AtomicLong traceNumberGenerator = new AtomicLong(new Random().nextLong());
	protected static int traceNumberOffset;
	protected static final Pattern brefCodePattern = Pattern.compile("(\\d+)(?:\\.(\\d+))?");
	protected static final Pattern BIN_NUMBER_PATTERN = Pattern.compile("([0-9]{9})");
	protected static final int[] ZERO_INDEXES = new int[1];
	protected static final int[] NORMAL_RESULT_CODES = { 0, 5 };
	protected static final int[] SUCCESS_RESULT_CODES = { 0 };
	protected static final int[] FAILURE_RESULT_CODES = { 1, 2 };
	protected static Pattern declinedDebitGatewayExpression = Pattern.compile("^(?:authority_iso8583_elavon|authority_iso8583_heartland|tandem)$");
	protected static Pattern denyDebitExpression = Pattern.compile("^;?5[1-5][0-9]{14}.*$");
	protected static Pattern TRACK2_PAN_AND_EXP_DATE_PATTERN = Pattern.compile("^;?([0-9]{1,19})=([0-9]{4})[0-9]*(?:\\?([\\x00-\\xFF])?)?$");
	protected String storeCardQueueKey;
	protected String recordAuthQueueKey;
	protected String recordStatsQueueKey;
	protected String cancelAuthQueueKey;
	protected String createPTAQueueKey;	
	protected String saleQueueKey;
	protected String replenishQueueKey;
	protected String updateAuthQueueKey;
	protected String createTokenQueueKey;
	protected int gatewayTimeout = 15; // seconds
	protected String authorityQueueKeyPrefix;
	protected boolean enableGxSessionRequest = false;
	protected TranslatorFactory translatorFactory;
	protected String translatorContext;
	protected final Map<DeviceType, Boolean> closeAfterAuth = new HashMap<DeviceType, Boolean>();
	protected Attribute[] saleAttributes = new Attribute[] {
			MessageAttrEnum.ATTR_GLOBAL_SESSION_CODE,
			MessageAttrEnum.ATTR_PROTOCOL,
			DeviceInfoProperty.DEVICE_NAME,
			DeviceInfoProperty.DEVICE_TYPE,
			MessageAttrEnum.ATTR_DATA,
			MessageAttrEnum.ATTR_SERVER_TIME,
			MessageAttrEnum.ATTR_DEVICE_INFO_HASH,
			DeviceInfoProperty.TIME_ZONE_GUID,
			DeviceInfoProperty.DEVICE_CHARSET
	};
	protected final Set<Integer> replenishItemIds = new HashSet<Integer>(Arrays.asList(new Integer[] { 550, 551, 552, 553 }));
	protected int cardRetentionDays = 90;
	protected static Pattern googleWalletCardNumberExpression = Pattern.compile("^;?539648.+$");
	protected static Pattern applePayCashCardNumberExpression = Pattern.compile("^;?(65008498|65008499|650009).+$");
	protected final Map<String, Set<String>> allowedGeocodeCountries = new HashMap<>();
	protected long maxGeocodeStaleness = 24 * 60 * 60 * 1000L;
	protected final Set<Integer> allowedGeocodeOEMIntegrities = new HashSet<>(Collections.singleton(0));
	protected final Set<Integer> allowedGeocodeProviders = new HashSet<>(Arrays.asList(new Integer(0), new Integer(3)));
	public static final ThreadSafeDateFormat declineUntilFormat = new ThreadSafeDateFormat(new SimpleDateFormat("MM/dd/yyyy HH:mm:ss z"));
	
	protected static enum CheckState {
		NORMAL, INVALID_TRANSACTION_ID, DUKPT_DECRYPTION_FAILURE, UNPARSABLE_ACCOUNT_DATA
	};

	protected static enum ChainIndex {
		AUTH_STEP(new int[] { 0, 1, 2 }, null),
		REPLENISH_STEP(SUCCESS_RESULT_CODES, null), 
		RESPONSE_STEP(new int[] { 0, 255 }, null), 
		STORE_STEP(SUCCESS_RESULT_CODES, null), 
		SAVE_STEP(SUCCESS_RESULT_CODES, new int[] { 1 }), 
		SALE_STEP(SUCCESS_RESULT_CODES, null), 
		UPDATE_STEP(SUCCESS_RESULT_CODES, null),
		STATS_STEP(SUCCESS_RESULT_CODES, null) ;
		private final int[] successResultCodes;
		private final int[] failureResultCodes;
		private ChainIndex(int[] successResultCodes, int[] failureResultCodes) {
			this.successResultCodes = successResultCodes;
			this.failureResultCodes = failureResultCodes;
		}
		public int[] getSuccessResultCodes() {
			return successResultCodes;
		}
		public int[] getFailureResultCodes() {
			return failureResultCodes;
		}
	}
	protected class ByteBufferPool {
		protected ByteBuffer[] instances = new ByteBuffer[3];
		protected int position = 0;
		public ByteBuffer next() {
			if(position >= instances.length) {
				ByteBuffer[] tmp = new ByteBuffer[position + 1];
				System.arraycopy(instances, 0, tmp, 0, instances.length);
				instances = tmp;
			}
			ByteBuffer bb = instances[position];
			if(bb == null) {
				bb = create();
				instances[position] = bb;
			} else {
				bb.clear();
			}
			position++;
			return bb;
			
		}
		public void reset() {
			position = 0;
		}
		protected ByteBuffer create() {
			ByteBuffer writeBuffer = ByteBuffer.allocate(getMaxResponseSize());
			writeBuffer.order(ByteOrder.BIG_ENDIAN);
			return writeBuffer;
		}
	}
	protected final ThreadLocal<ByteBufferPool> byteBufferLocals = new ThreadLocal<ByteBufferPool>() {
		@Override
		protected ByteBufferPool initialValue() {
			return new ByteBufferPool();
		}
	};
	protected int maxResponseSize = 2048;
	protected AppLayerDeviceInfoManager deviceInfoManager;
	
	protected final static Cache<String, Pattern, PatternSyntaxException> regexCache = new LockSegmentCache<String, Pattern, PatternSyntaxException>() {
		/**
		 * @see simple.util.concurrent.LockSegmentCache#createValue(java.lang.Object,
		 *      java.lang.Object[])
		 */
		@Override
		protected Pattern createValue(String key, Object... additionalInfo) throws PatternSyntaxException {
			return Pattern.compile(key, Pattern.DOTALL);
		}
	};

	protected Translator getTranslator(String locale) {
		TranslatorFactory tf = getTranslatorFactory();
		if(tf != null)
			try {
				return tf.getTranslator(ConvertUtils.convertSafely(Locale.class, locale, Locale.getDefault()), getTranslatorContext());
			} catch(ServiceException e) {
				log.warn("Could not get translator", e);
			}
		return DefaultTranslatorFactory.getTranslatorInstance();
	}

    //TODO: SHORTCUTED FOR May 2017 APPLE DEMO: ONLY SINGLE PASS SUPPORTED IN MESSAGE
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		ProcessingUtils.checkMessageExpiration(taskInfo);
		try {
			final Map<String, Object> resultAttributes = taskInfo.getStep().getResultAttributes();
			AppLayerOnlineMessage message = AppLayerUtils.constructMessage(taskInfo, null, getDeviceInfoManager(), getTranslatorFactory(), getTranslatorContext(), null, null);
			if(message.getDeviceInfo() == null) {
				//device is not longer active - tell Netlayer
				log.info("Device '" + message.getDeviceName() + "' is no longer active. Telling NetLayer to close session and remove the device info from cache");
				resultAttributes.put("endSession", SessionCloseReason.INACTIVE_DEVICE);
				resultAttributes.put("removeDeviceInfo", true);
				return 5;
			} 
			byteBufferLocals.get().reset();
			processMessage(taskInfo, message);
			return message.complete();
		} catch(BufferUnderflowException e) {
			throw new RetrySpecifiedServiceException("Message data is not long enough. Invalid message", e, WorkRetryType.NO_RETRY);
		}
	}
	protected boolean shouldCloseAfterAuth(DeviceInfo deviceInfo, AuthorizeMessageData dataAuth) {
		Boolean result = closeAfterAuth.get(deviceInfo.getDeviceType());
		return result == null || result;
	}
	
	
	private boolean isNeedToAddCardIdToReply(AuthorizeMessageData authData) {
		if (authData.getMessageType() != MessageType.AUTH_REQUEST_4_1) {
			return false;
		}

		if (!authData.getEntryType().isCardIdRequiredInReply()
				&& StringUtils.isBlank(authData.getCardReaderType().getCipherName())) {
			return false;
		}
		
		return true;
	}
	
	private boolean isNeedToReplyOnlyCardId(AuthorizeMessageData authData) {
		if (StringUtils.isBlank(authData.getValidationData())) {
			return false;
		}

		if (!isNeedToAddCardIdToReply(authData)) {
			return false;
		}

		if (authData.getValidationData().startsWith("C|")) {
			return true;
		}

		return false;

	}	

	protected void processMessage(MessageChainTaskInfo taskInfo, AppLayerOnlineMessage message) throws RetrySpecifiedServiceException {
		MessageChainStep step = taskInfo.getStep();
		MessageChainStep[] mcss = step.getNextSteps(0);
		MessageChainStep responseStep = (mcss == null || mcss.length == 0 ? null : mcss[0]);
		Map<String, Object> attributes = step.getAttributes();
		Map<String, Object> resultAttributes = step.getResultAttributes();
		if(message.getLog().isDebugEnabled())
			message.getLog().debug("Processing authorization with attributes " + attributes);
		AuthorizeMessageData dataAuth = (AuthorizeMessageData)message.getData();
		byte[] appSessionKey = null;
		try {
			final DeviceInfo deviceInfo = message.getDeviceInfo();
			if(responseStep != null && shouldCloseAfterAuth(deviceInfo, dataAuth))
				responseStep.setAttribute(CommonAttrEnum.ATTR_END_SESSION, SessionCloseReason.MESSAGE_BASED);
			EntryType entryType = step.getAttributeDefault(AuthorityAttrEnum.ATTR_ENTRY_TYPE, EntryType.class, dataAuth.getEntryType());
			appSessionKey = step.getAttribute(CommonAttrEnum.ATTR_APP_SESSION_KEY, byte[].class, false);
			final long authTime = step.getAttribute(MessageAttrEnum.ATTR_SERVER_TIME, Long.class, true);
			final int cnt = step.getAttributeDefault(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT, Integer.class, 0);
			String[] accountDatas;
			int[] indexes;
			switch(cnt) {
				case 0:
					String accountData = step.getAttribute(AuthorityAttrEnum.ATTR_ACCOUNT_DATA, String.class, false);
					accountDatas = new String[] { accountData };
					indexes = ZERO_INDEXES;
					if(entryType != EntryType.CASH && StringUtils.isBlank(accountData)) {
						message.getLog().error("Encrypted account data was not decrypted prior to processing; Failing the auth 1.");
						findAndProcessMatch(taskInfo, step, responseStep, message, deviceInfo, dataAuth, accountDatas, indexes, authTime, CheckState.DUKPT_DECRYPTION_FAILURE, "Encrypted account data was not decrypted prior to processing; Failing the auth 1", entryType, appSessionKey);
						return;
					}
					break;
				default:
					accountDatas = new String[cnt];
					indexes = new int[cnt];
					boolean good = false;
					for(int i = 0; i < cnt; i++) {
						accountData = step.getIndexedAttribute(AuthorityAttrEnum.ATTR_ACCOUNT_DATA, i + 1, String.class, false);
						if (i == 0 && StringUtils.isBlank(accountData)) {
							// Try without i index
							accountData = step.getAttribute(AuthorityAttrEnum.ATTR_ACCOUNT_DATA, String.class, false);
						}
						accountDatas[i] = accountData;
						if(!StringUtils.isBlank(accountData))
							good = true;
						indexes[i] = i + 1;
					}
					if(!good) {
						message.getLog().error("Encrypted account data was not decrypted prior to processing; Failing the auth 2.");
						findAndProcessMatch(taskInfo, step, responseStep, message, deviceInfo, dataAuth, accountDatas, indexes, authTime, CheckState.DUKPT_DECRYPTION_FAILURE, "Encrypted account data was not decrypted prior to processing; Failing the auth 2", entryType, appSessionKey);
						return;
					}
					break;
			}

			if ((entryType == EntryType.CONTACTLESS || entryType == EntryType.APPLE_PAY || entryType == EntryType.VAS) && applePayCashCardNumberExpression.matcher(accountDatas[0]).matches()) {
				entryType = EntryType.APPLE_PAY_CASH;
			}
			
			if (isNeedToReplyOnlyCardId(dataAuth)) {
				Long globalAccountId = taskInfo.getStep().getOptionallyIndexedAttribute(
						AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, indexes[0], Long.class, false);
				sendResponse(message, responseStep, dataAuth, AuthResultCode.APPROVED, globalAccountId, null, null,
						false, "", false, false, null);
				return;
			}

			if (isNeedToAddCardIdToReply(dataAuth)) {
				Long globalAccountId = taskInfo.getStep().getOptionallyIndexedAttribute(
						AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, indexes[0], Long.class, false);
				if (globalAccountId == null) {
					// Trying to search it in default (not indexed) attribute
					globalAccountId = taskInfo.getStep().getAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, Long.class, false);
				}
				if (globalAccountId != null) {
					responseStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_REPLY_CARD_ID, globalAccountId);
				}
			}			
			
			// Dup check (check eventId against last master id)
			StringBuilder sb;
			switch(dataAuth.getPaymentActionType()) {
				case BALANCE_CHECK:
					break;
				case TOKENIZE:
					// Adjust account data
					sb = new StringBuilder();
					for(int i = 0; i < accountDatas.length; i++) {
						sb.setLength(0);
						String cardNumber;
						String expDate;
						if(dataAuth instanceof ExpirationDateContainer) {
							cardNumber = accountDatas[i];
							expDate = ((ExpirationDateContainer) dataAuth).getExpirationDate();
						} else {
							Matcher matcher = TRACK2_PAN_AND_EXP_DATE_PATTERN.matcher(accountDatas[i]);
							if(!matcher.matches()) {
								List<CardType> cardTypes = CardType.findAll(entryType, PaymentActionType.PURCHASE);
								handleMissingPTA(step, message, deviceInfo, dataAuth, accountDatas, indexes, authTime, (cardTypes == null || cardTypes.isEmpty() ? CardType.ERROR : cardTypes.get(0)).getValue(), System.currentTimeMillis(), CheckState.UNPARSABLE_ACCOUNT_DATA, "Encrypted Track Data is not in a recognizable card format", entryType, appSessionKey);
								return;
							}
							cardNumber = matcher.group(1);
							expDate = matcher.group(2);
						}
						if(!StringUtils.isBlank(cardNumber))
							sb.append(cardNumber);
						sb.append('|');
						if(!StringUtils.isBlank(expDate))
							sb.append(expDate);
						sb.append('|');
						if(dataAuth instanceof MessageData_021E) {
							MessageData_021E dataAuth021E = (MessageData_021E) dataAuth;
							if(!StringUtils.isBlank(dataAuth021E.getSecurityCode()))
								sb.append(dataAuth021E.getSecurityCode());
							sb.append('|');
							if(!StringUtils.isBlank(dataAuth021E.getCardHolder()))
								sb.append(dataAuth021E.getCardHolder());
						} else
							sb.append('|');
						sb.append('|');
						WS2TokenizeMessageData dataTokenize = (WS2TokenizeMessageData) dataAuth;
						if(!StringUtils.isBlank(dataTokenize.getBillingPostalCode()))
							sb.append(dataTokenize.getBillingPostalCode());
						sb.append('|');
						if(!StringUtils.isBlank(dataTokenize.getBillingAddress()))
							sb.append(dataTokenize.getBillingAddress());
						accountDatas[i] = sb.toString();
					}
					// fall-thru
				default:
					boolean valid;
					String invalidTransactionIdMessage;
					switch(deviceInfo.updateMasterId(dataAuth.getTransactionId())) {
						case TOO_HIGH:
							sb = new StringBuilder();
							sb.append("Transaction Id (").append(dataAuth.getTransactionId()).append(") is too high");
							invalidTransactionIdMessage = sb.toString();
							valid = false;
							break;
						case TOO_LOW:
							sb = new StringBuilder();
							sb.append("Transaction Id (").append(dataAuth.getTransactionId()).append(") is NOT ");
							if(deviceInfo.isMasterIdIncrementOnly())
								sb.append("greater than");
							else
								sb.append("different than");
							sb.append(" the previous one (").append(deviceInfo.getMasterId()).append(')');
							invalidTransactionIdMessage = sb.toString();
							valid = false;
							break;
						case NO_CHANGE:
							if(!taskInfo.isRedelivered()) {
								sb = new StringBuilder();
								sb.append("Transaction Id (").append(dataAuth.getTransactionId()).append(") is NOT different than the previous one (").append(deviceInfo.getMasterId()).append(')');
								invalidTransactionIdMessage = sb.toString();
								valid = false;
								break;
							}
							// fall-through
						case UPDATED:
						default:
							valid = true;
							invalidTransactionIdMessage = null;
					}
					if(!valid) { // eventId is less than last master id - reject this auth
						if(message.getLog().isInfoEnabled()) {
							message.getLog().info(invalidTransactionIdMessage);
						}
						if(responseStep != null) {
							responseStep.addLiteralAttribute("authResultCd", AuthResultCode.FAILED);
							sendResponse(message, responseStep, dataAuth, AuthResultCode.FAILED, null, null, null, true, invalidTransactionIdMessage, false, false, appSessionKey);
						}
						// Store Auth
						findAndProcessMatch(taskInfo, step, responseStep, message, deviceInfo, dataAuth, accountDatas, indexes, authTime, CheckState.INVALID_TRANSACTION_ID, invalidTransactionIdMessage, entryType, appSessionKey);
						return;
					}
			}

			taskInfo.setAfterComplete(new Runnable() {
				@Override
				public void run() {
					try {
						deviceInfo.commitChanges(authTime);
					} catch(ServiceException e) {
						log.warn("Could not commit device info changes for " + deviceInfo.getDeviceName(), e);
					} // Make sure master id is propagated
				}
			});
			// finish processing auth
			findAndProcessMatch(taskInfo, step, responseStep, message, deviceInfo, dataAuth, accountDatas, indexes, authTime, CheckState.NORMAL, null, entryType, appSessionKey);
		} catch(ServiceException e) {
			log.error("While processing auth", e);
			// publish failure response
			if(responseStep != null)
				sendFailure(message, responseStep, dataAuth, null, null, appSessionKey, "client.message.service-exception");
			resultAttributes.put("endSession", SessionCloseReason.ERROR);
		} catch(ConvertException e) {
			log.error("While processing auth", e);
			// publish failure response
			if(responseStep != null)
				sendFailure(message, responseStep, dataAuth, null, null, appSessionKey, "client.message.convert-exception");
			resultAttributes.put("endSession", SessionCloseReason.ERROR);
		} catch(AttributeConversionException e) {
			log.error("While processing auth", e);
			// publish failure response
			if(responseStep != null)
				sendFailure(message, responseStep, dataAuth, null, null, appSessionKey, "client.message.convert-exception");
			resultAttributes.put("endSession", SessionCloseReason.ERROR);
		} catch(SQLException e) {
			WorkRetryType retryType = DatabasePrerequisite.determineRetryType(e);
			if(retryType == WorkRetryType.BLOCKING_RETRY)
				throw new RetrySpecifiedServiceException(e, retryType);
			log.error("While processing auth", e);
			// publish failure response
			if(responseStep != null)
				sendFailure(message, responseStep, dataAuth, null, null, appSessionKey, "client.message.sql-exception");
			resultAttributes.put("endSession", SessionCloseReason.ERROR);
		} catch(DataLayerException e) {
			throw new RetrySpecifiedServiceException(e, WorkRetryType.BLOCKING_RETRY);
		} catch(ParseException e) {
			log.error("While processing auth", e);
			// publish failure response
			if(responseStep != null)
				sendFailure(message, responseStep, dataAuth, null, null, appSessionKey, "client.message.parse-exception");
			resultAttributes.put("endSession", SessionCloseReason.ERROR);
		} catch(RuntimeException e) {
			log.error("While processing auth", e);
			// publish failure response
			if(responseStep != null)
				sendFailure(message, responseStep, dataAuth, null, null, appSessionKey, "client.message.runtime-exception");
			resultAttributes.put("endSession", SessionCloseReason.ERROR);
		}
		return;
	}



	protected boolean sendFailure(Message message, MessageChainStep responseStep, AuthorizeMessageData dataAuth, Long globalAccountId, String cardType, byte[] appSessionKey, String reasonKey, Object... reasonParams) {
		String text = message.getTranslator().translate(reasonKey, "Auth Failure", reasonParams);
		return sendResponse(message, responseStep, dataAuth, AuthResultCode.FAILED, globalAccountId, cardType, null, false, text, false, false, appSessionKey);
	}

	protected boolean sendDecline(Message message, MessageChainStep responseStep, AuthorizeMessageData dataAuth, Long globalAccountId, String cardType, byte[] appSessionKey, String reasonKey, Object... reasonParams) {
		String text = message.getTranslator().translate(reasonKey, "Auth Decline", reasonParams);
		return sendResponse(message, responseStep, dataAuth, AuthResultCode.DECLINED, globalAccountId, cardType, null, false, text, false, false, appSessionKey);
	}

	protected boolean sendResponse(Message message, MessageChainStep responseStep, AuthorizeMessageData dataAuth, AuthResultCode authResult, Long globalAccountId, String cardType, BigDecimal approvedAmount, boolean invalidTranId, String responseMessage, boolean freeTransaction, boolean noConvenienceFee, byte[] appSessionKey) {
		try {
			MessageData reply = prepareResponse(message.getDeviceInfo(), dataAuth, authResult, globalAccountId, cardType, approvedAmount, invalidTranId, responseMessage, freeTransaction, noConvenienceFee, appSessionKey);
			sendReply(responseStep, reply);
			return true;
		} catch(ServiceException e) {
			log.error("Could not send message back to device", e);
			return false;
		}
	}

	protected boolean sendResponseTemplate(Message message, MessageChainStep responseStep, AuthorizeMessageData dataAuth, AuthResultCode authResult, Long globalAccountId, String cardType, BigDecimal approvedAmount, boolean invalidTranId, String responseMessage, boolean freeTransaction, boolean noConvenienceFee, byte[] appSessionKey) {
		try {
			MessageData reply = prepareResponse(message.getDeviceInfo(), dataAuth, authResult, globalAccountId, cardType, approvedAmount, invalidTranId, responseMessage, freeTransaction, noConvenienceFee, appSessionKey);
			sendReplyTemplate(responseStep, reply);
			return true;
		} catch(ServiceException e) {
			log.error("Could not send message back to device", e);
			return false;
		}
	}

	protected MessageData prepareResponse(DeviceInfo deviceInfo, AuthorizeMessageData dataAuth, AuthResultCode authResponse, Long globalAccountId, String cardType, BigDecimal approvedAmount, boolean invalidTranId, String responseMessage, boolean freeTransaction, boolean noConvenienceFee, byte[] appSessionKey) {
		return MessageProcessingUtils.prepareAuthResponse(deviceInfo, dataAuth, authResponse, globalAccountId, cardType, approvedAmount, invalidTranId, responseMessage, freeTransaction, noConvenienceFee, appSessionKey);
	}

	protected void sendReply(MessageChainStep responseStep, MessageData reply) throws ServiceException {
		ByteBuffer buffer = byteBufferLocals.get().next();
		try {
			reply.writeData(buffer, false);
		} catch(BufferOverflowException e) {
			throw new ServiceException("Reply Buffer is too small", e);
		} catch(IllegalStateException e) {
			throw new ServiceException("Reply Data is not properly prepared", e);
		}
		buffer.flip();
		responseStep.setAttribute(CommonAttrEnum.ATTR_REPLY, buffer);
	}
	protected void sendReplyTemplate(MessageChainStep responseStep, MessageData reply) throws ServiceException {
		ByteBuffer buffer = byteBufferLocals.get().next();
		try {
			reply.writeData(buffer, false);
		} catch(BufferOverflowException e) {
			throw new ServiceException("Reply Buffer is too small", e);
		} catch(IllegalStateException e) {
			throw new ServiceException("Reply Data is not properly prepared", e);
		}
		buffer.flip();
		responseStep.setAttribute(CommonAttrEnum.ATTR_REPLY_TEMPLATE, buffer);
	}

	protected StringBuilder appendMaskedTrackData(String[] tracks, StringBuilder sb) {
		for(int i = 0; i < tracks.length; i++) {
			if(i > 0)
				sb.append(" / ");
			sb.append(MessageResponseUtils.maskTrackData(tracks[i]));
		}
		return sb;
	}

	protected void addCardInfoToStoreStep(Map<PaymentMaskBRef, Object> parsedCardInfo, MessageChainStep step) {
		int maxStoreIndex = 0;
		for(Map.Entry<PaymentMaskBRef, Object> entry : parsedCardInfo.entrySet()) {
			int storeIndex = entry.getKey().getStoreIndex();
			if(storeIndex <= 0)
				continue;
			String value = cardInfoToString(entry.getValue());
			step.setIndexedSecretAttribute(CommonAttrEnum.ATTR_DECRYPTED_DATA, storeIndex, value);
			if(maxStoreIndex < storeIndex)
				maxStoreIndex = storeIndex;
		}
		step.setAttribute(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT, maxStoreIndex);
	}

	protected void addCardInfoToStep(Map<PaymentMaskBRef, Object> parsedCardInfo, MessageChainStep step, boolean maskData) {
		for(Map.Entry<PaymentMaskBRef, Object> entry : parsedCardInfo.entrySet()) {
			String value = cardInfoToString(entry.getValue());
			if(maskData) {
				step.setAttribute(entry.getKey().attribute, entry.getKey().maskData(value));
			} else {
				step.setSecretAttribute(entry.getKey().attribute, value);
			}
		}
	}

	public static String cardInfoToString(Object cardInfoValue) {
		if(cardInfoValue instanceof String) {
			return (String) cardInfoValue;
		} else if(cardInfoValue instanceof SortedMap<?,?>) {
			StringBuilder sb = new StringBuilder();
			for(String s : CollectionUtils.uncheckedMap((SortedMap<?, ?>) cardInfoValue, Integer.class, String.class).values())
				sb.append(s);
			return sb.toString();
		} else {
			log.warn("Type '" + (cardInfoValue == null ? "NULL" : cardInfoValue.getClass().getName()) + "' for card info is not supported",
					new IllegalArgumentException());
			return cardInfoValue == null ? null : cardInfoValue.toString();
		}
	}
	public static Map<PaymentMaskBRef, Object> parseCardInfo(Matcher cardMatcher, String cardRegexBref) throws ParseException, ConvertException {
		Map<PaymentMaskBRef, Object> parsedCardInfo = new HashMap<PaymentMaskBRef, Object>();
		parsedCardInfo.put(PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER, cardMatcher.group(0));
		if(cardRegexBref != null && (cardRegexBref = cardRegexBref.trim()).length() > 0) {
			Map<String, String> map = new HashMap<String, String>();
			StringUtils.intoMap(map, cardRegexBref, ':', '|', new char[0], true, true);
			for(int i = 1; i <= cardMatcher.groupCount(); i++) {
				String attrValue = cardMatcher.group(i);
				if(attrValue != null && attrValue.length() > 0) {
					String brefCode = map.get(String.valueOf(i));
					if(brefCode != null) {
						Matcher brefMatcher = brefCodePattern.matcher(brefCode);
						if(brefMatcher.matches()) {
							byte brefIndex = ConvertUtils.getByte(brefMatcher.group(1));
							PaymentMaskBRef pmbref;
							try {
								pmbref = PaymentMaskBRef.getByValue(brefIndex);
							} catch(InvalidByteValueException e) {
								log.info("Could not find a bref object for index " + brefIndex + "; ignoring this data");
								continue;
							}
							String piece = brefMatcher.group(2);
							if(piece == null || piece.length() == 0)
								parsedCardInfo.put(pmbref, attrValue);
							else {
								Object prev = parsedCardInfo.get(pmbref);
								if(prev == null) {
									SortedMap<Integer, String> pieces = new TreeMap<Integer, String>();
									pieces.put(Integer.parseInt(piece), attrValue);
									parsedCardInfo.put(pmbref, pieces);
								} else if(prev instanceof SortedMap<?, ?>) {
									CollectionUtils.uncheckedMap((SortedMap<?, ?>) prev, Integer.class, String.class).put(Integer.parseInt(piece), attrValue);
								}
							}
						}
					}
				}
			}
		}
		return parsedCardInfo;
	}

	protected void findAndProcessMatch(MessageChainTaskInfo taskInfo, MessageChainStep currentStep, MessageChainStep responseStep, AppLayerOnlineMessage message, DeviceInfo deviceInfo, AuthorizeMessageData dataAuth, String[] accountDatas, int[] indexes, long authTime, CheckState checkState, String failureMessage, EntryType entryType, byte[] appSessionKey)
			throws ConvertException, ParseException,
			SQLException, DataLayerException,
			ServiceException, AttributeConversionException {
		long startTime = System.currentTimeMillis();
		PaymentActionType effectivePAT;
		switch(dataAuth.getPaymentActionType()) {
			case TOKENIZE:
			case BALANCE_CHECK:
				effectivePAT = PaymentActionType.PURCHASE;
				break;
			default:
				effectivePAT = dataAuth.getPaymentActionType();
		}
		List<CardType> cardTypes = CardType.findAll(entryType, effectivePAT);
		if(message.getLog().isInfoEnabled())
			message.getLog().info("Retrieving PTA's for device " + message.getDeviceName() + " (" + deviceInfo.getActivationStatus() + ") for payment action type " + dataAuth.getPaymentActionType() + " and entry type " + entryType + " (card types: " + cardTypes + ")");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("deviceName", message.getDeviceName());
		params.put("paymentTypes", cardTypes);
		params.put("authTime", authTime);
		Results results;
		boolean original = taskInfo.setInterruptible(false);
		try {
			results = DataLayerMgr.executeQuery("GET_PTA_LIST", params);
		} finally {
			taskInfo.setInterruptible(original);
		}
		while(results.next()) {
			if(checkAndProcessMatch(taskInfo.getStep(), currentStep, responseStep, null, NORMAL_RESULT_CODES, results, message, deviceInfo, dataAuth, authTime, accountDatas, indexes, startTime, checkState, failureMessage, null, entryType, appSessionKey))
				return;
		}
		if(message.getLog().isWarnEnabled()) {
			StringBuilder sb = new StringBuilder();
			sb.append("Did not find a PTA match for card '");
			appendMaskedTrackData(accountDatas, sb);
			sb.append("'; Rejecting auth");
			message.getLog().warn(sb);
		}
		// Create message chain to create missing PTA and then save
		handleMissingPTA(currentStep, message, deviceInfo, dataAuth, accountDatas, indexes, authTime, (cardTypes == null || cardTypes.isEmpty() ? CardType.ERROR : cardTypes.get(0)).getValue(), startTime, checkState, failureMessage, entryType, appSessionKey);
	}
	
	/**
	 * @throws ServiceException
	 * @throws AttributeConversionException
	 * 
	 */
	protected void handleMissingPTA(MessageChainStep currentStep, Message message, DeviceInfo deviceInfo, AuthorizeMessageData dataAuth, String[] accountDatas, int[] indexes, long authTime, char paymentType, long startTime, CheckState checkState, String failureMessage, EntryType entryType, byte[] appSessionKey) throws ServiceException, AttributeConversionException {
		MessageChainStep[] mcss = currentStep.getNextSteps(0);
		boolean doSend;
		MessageChainStep responseStep;
		if(mcss == null || mcss.length == 0) {
			responseStep = currentStep;
			doSend = false;
		} else {
			responseStep = mcss[0];
			doSend = true;
		}
		MessageChainStep createPTAStep = responseStep.addStep(getCreatePTAQueueKey());
		responseStep.setNextSteps(0, createPTAStep);
		responseStep.setNextSteps(255, createPTAStep);

		createPTAStep.addStringAttribute("deviceName", message.getDeviceName());
		createPTAStep.addLongAttribute("authTime", authTime);
		createPTAStep.addStringAttribute("paymentType", String.valueOf(paymentType));
		createPTAStep.addLongAttribute("tranUtcTsMs", authTime);

		String globalEventCd = ProcessingConstants.GLOBAL_EVENT_CODE_PREFIX + ':' + message.getDeviceName() + ":" + dataAuth.getTransactionId();
		long traceNumber = nextTraceNumber();

		MessageChainStep saveStep = responseStep.addStep(getRecordAuthQueueKey());
		MessageChainStep statsStep = responseStep.addStep(getRecordStatsQueueKey());
		createPTAStep.setNextSteps(0, saveStep);
		createPTAStep.setNextSteps(255, saveStep);
		saveStep.setNextSteps(0, statsStep);

		// Add to saveStep
		if(doSend) {
			saveStep.addReferenceAttribute("sentToDevice", responseStep, "sentToDevice");
			statsStep.addReferenceAttribute("responseStartTime", responseStep, "startTime");
			statsStep.addReferenceAttribute("responseEndTime", responseStep, "replyTime");
		} else
			saveStep.addBooleanAttribute("sentToDevice", false);

		saveStep.addStringAttribute("globalSessionCode", message.getGlobalSessionCode());
		saveStep.addLongAttribute("authTime", authTime);
		saveStep.addStringAttribute("timeZoneGuid", deviceInfo.getTimeZoneGuid());
		saveStep.addStringAttribute("deviceName", message.getDeviceName());
		saveStep.addLongAttribute("eventId", dataAuth.getTransactionId());
		saveStep.addStringAttribute("invalidEventId", "N");

		if(accountDatas != null && accountDatas.length > 0 && accountDatas[0] != null)
			saveStep.addStringAttribute("trackData", MessageResponseUtils.maskCardNumber(accountDatas[0]));
		Long globalAccountId;
		if(indexes != null && indexes.length > 0) {
			globalAccountId = currentStep.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, indexes[0], Long.class, false);
			if(globalAccountId != null) {
				saveStep.setAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, globalAccountId);
				saveStep.setAttribute(AuthorityAttrEnum.ATTR_INSTANCE, currentStep.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_INSTANCE, indexes[0], Short.class, false));
			}
			String passIdentifier = currentStep.getOptionallyIndexedAttribute(CommonAttrEnum.ATTR_PASS_IDENTIFIER, indexes[0], String.class, false);
			if (StringHelper.isLong(passIdentifier))
				saveStep.setAttribute(CommonAttrEnum.ATTR_PASS_IDENTIFIER, passIdentifier);
		} else
			globalAccountId = null;

		saveStep.addReferenceAttribute("posPtaId", createPTAStep, "posPtaId");
		saveStep.addStringAttribute("globalEventCd", globalEventCd);
		saveStep.addStringAttribute("entryMethod", String.valueOf(entryType.getValue()));
		saveStep.addStringAttribute("paymentType", String.valueOf(paymentType));
		saveStep.addReferenceAttribute("minorCurrencyFactor", createPTAStep, "minorCurrencyFactor");
		saveStep.addReferenceAttribute("currencyCd", createPTAStep, "currencyCd");
		saveStep.addLongAttribute("traceNumber", traceNumber);
		Number amount = dataAuth.getAmount();
		if(amount == null)
			amount = BigDecimal.ZERO;
		saveStep.addStringAttribute("authAmount", amount.toString());
		saveStep.addStringAttribute("requestedAmount", amount.toString());

		saveStep.addStringAttribute("authResultCd", "N");
		saveStep.addStringAttribute("authorityRespCd", "NO_MATCH");
		saveStep.addStringAttribute("authorityRespDesc", "Account data does not match any PTA records for this device");

		//Add to stats step
		statsStep.addStringAttribute("globalSessionCode", message.getGlobalSessionCode());
		statsStep.addLongAttribute("authTime", authTime);
		statsStep.addLongAttribute("applayerStartTime", startTime);
		statsStep.addStringAttribute("paymentType", String.valueOf(paymentType));
		statsStep.addLongAttribute("authTime", authTime);
		statsStep.addReferenceAttribute("tranId", saveStep, "tranId");
		statsStep.addReferenceAttribute("authId", saveStep, "authId");
		statsStep.addReferenceAttribute("passThru", saveStep, "passThru");
		statsStep.addReferenceAttribute("authResultCd", saveStep, "authResultCd");
		
		Language language = currentStep.getAttributeDefault(CommonAttrEnum.ATTR_LANGUAGE, Language.class, Language.ENGLISH);
		
		switch(checkState) {
			case DUKPT_DECRYPTION_FAILURE: // if(currentStep.getResultAttributes().containsKey("decryptionError")) {
				AuthResultCode authResultCd = currentStep.getAttributeDefault(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.class, AuthResultCode.FAILED);
				String authorityRespCd = currentStep.getAttributeDefault(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, String.class, "DUKPT_DECRYPTION_FAILURE");
				String authorityRespDesc = currentStep.getAttributeDefault(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, String.class, failureMessage);
				saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, authResultCd);
				saveStep.setAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.DUKPT_DECRYPTION_FAILURE);
				saveStep.addStringAttribute("authorityRespCd", authorityRespCd);
				saveStep.addStringAttribute("authorityRespDesc", authorityRespDesc); // requestStep.getResultAttributes().get("decryptionError").toString());
				if(doSend) {
					responseStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, authResultCd);
					String text = message.getTranslator().translate("client.message." + language + ".auth.denied-" + DeniedReason.DUKPT_DECRYPTION_FAILURE, null, authResultCd, authorityRespCd, authorityRespDesc);
					sendResponse(message, responseStep, dataAuth, authResultCd, globalAccountId, null, null, false, text, false, false, appSessionKey);
				}
				currentStep.getResultAttributes().put("endSession", SessionCloseReason.DECRYPTION_FAILURE);
				break;
			case INVALID_TRANSACTION_ID:
				saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.FAILED);
				saveStep.setAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.DATA_FROM_CLIENT);
				saveStep.addStringAttribute("authorityRespCd", "INVALID_TRANSACTION_ID");
				saveStep.addStringAttribute("authorityRespDesc", failureMessage);
				saveStep.addStringAttribute("invalidEventId", "Y");
				break;	
			case NORMAL:
				if(doSend) {
					responseStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.DECLINED);
					responseStep.addLiteralAttribute("paymentType", paymentType);
					String text = message.getTranslator().translate("client.message.no-matching-pta", "Invalid Card");
					sendResponse(message, responseStep, dataAuth, AuthResultCode.DECLINED_PAYMENT_METHOD, globalAccountId, "Unknown", null, false, text, false, false, appSessionKey);
				}
				break;
			case UNPARSABLE_ACCOUNT_DATA:
				saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.DECLINED);
				saveStep.setAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.UNPARSEABLE_CARD_DATA);
				saveStep.addStringAttribute("authorityRespCd", "UNPARSABLE_ACCOUNT_DATA");
				saveStep.addStringAttribute("authorityRespDesc", failureMessage); // currentStep.getResultAttributes().get("decryptionError").toString());
				if(doSend) {
					responseStep.addLiteralAttribute("authResultCd", AuthResultCode.DECLINED);
					sendDecline(message, responseStep, dataAuth, globalAccountId, null, appSessionKey, "client.message." + language + ".auth.denied-" + DeniedReason.UNPARSEABLE_CARD_DATA.toString());
				}
				break;
		}
	}

	protected long determineTargetExpirationTime(Map<PaymentMaskBRef, Object> parsedCardInfo, long startTime, boolean keepEncryptedUntilExpDate) {
		long minTargetTime = startTime + TimeUnit.DAYS.toMillis(cardRetentionDays);
		if(keepEncryptedUntilExpDate) {
			String expDate = cardInfoToString(parsedCardInfo.get(PaymentMaskBRef.EXPIRATION_DATE));
			if(!StringUtils.isBlank(expDate)) {
				return Math.max(minTargetTime, InteractionUtils.expDateToMillis(expDate));
			}
		}
		return minTargetTime;
	}

	/**
	 * Checks if the PTA matches the track data and adds the Save and Stats Steps to the message chain if it does match
	 * 
	 * @param results
	 * @param message
	 * @param authTime
	 * @param startTime
	 * @param addAfterStep
	 * @param eventId
	 * @param entryType
	 * @param tracks
	 * @param authAmount
	 * @param pin
	 * @param validationData
	 * @param lastStep
	 * @return If a match was found
	 * @throws ConvertException
	 * @throws ParseException
	 * @throws ServiceException
	 * @throws AttributeConversionException
	 */
	protected boolean checkAndProcessMatch(MessageChainStep requestStep, MessageChainStep currentStep, MessageChainStep responseStep, MessageChainStep afterStep, int[] resultCodes, Results results, 
			AppLayerOnlineMessage message, DeviceInfo deviceInfo, AuthorizeMessageData dataAuth, long authTime, String[] accountDatas, int[] indexes,
			long startTime, CheckState checkState, String failureMessage,
			MessageChainStep globalAccountIdReferenceStep, EntryType entryType, byte[] appSessionKey) throws ConvertException, ParseException, ServiceException, AttributeConversionException {
		long posPtaId = results.getValue("posPtaId", Long.class);
		String cardRegex = results.getValue("cardRegex", String.class);
		Pattern regex;
		try {
			regex = regexCache.getOrCreate(cardRegex);
		} catch(PatternSyntaxException e) {
			log.warn("Invalid regex '" + cardRegex + "' for POS_PTA_ID " + posPtaId + "; skipping this record", e);
			return false;
		}
		Language language = currentStep.getAttributeDefault(CommonAttrEnum.ATTR_LANGUAGE, Language.class, Language.ENGLISH);
		// do we have the terminalId/paymentSubtypeKeyId
		long paymentSubtypeKeyId = results.getValue("paymentSubtypeKeyId", long.class);

		for(int i = 0; i < accountDatas.length; i++) {
			final Matcher matcher;
			if(entryType == EntryType.CASH)
				matcher = null;
			else {
				if(StringUtils.isBlank(accountDatas[i])) {
					Character autoAuthResultCd = results.getValue("autoAuthResultCd", Character.class);
					if(autoAuthResultCd == null || autoAuthResultCd != 'R')
						continue;
					matcher = null;
				} else {
					matcher = regex.matcher(accountDatas[i]);
					if(!matcher.matches()) {
						if(message.getLog().isDebugEnabled())
							message.getLog().debug("Card did NOT match regex '" + cardRegex + "'");
						continue;
					}
				}
			}
			String authorityName = results.getValue("authorityName", String.class);
			if(message.getLog().isDebugEnabled())
				message.getLog().debug("Card matched regex '" + cardRegex + "' for authority '" + authorityName + "'");
			Long globalAccountId = requestStep.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, indexes[i], Long.class, false);
			if (globalAccountId == null && i == 0) {
				// Trying to search it in default (not indexed) attribute
				globalAccountId = requestStep.getAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, Long.class, false);
			}
			AccountHandling handling = results.getValue("handling", AccountHandling.class);
			String currencyCd = results.getValue("currencyCd", String.class);
			Long consumerAcctId = null;
			String loyaltyToken = null;
			switch(handling) {
				case SKIP_ON_NO_ACCOUNT_ID:
					if(globalAccountId == null)
						continue;
					break;
				case REQUIRE_TOKEN_OR_SKIP:
					if(globalAccountId == null)
						continue;
					Map<String, Object> tokenParams = new HashMap<String, Object>();
					tokenParams.put("globalAccountId", globalAccountId);
					tokenParams.put("currencyCd", currencyCd);
					Results tokenResults;
					try {
						tokenResults = DataLayerMgr.executeQuery("GET_ACCOUNT_TOKEN", tokenParams);
					} catch(SQLException e) {
						log.warn("Could not retrieve account token; skipping...", e);
						continue;
					} catch(DataLayerException e) {
						log.warn("Could not retrieve account token; skipping...", e);
						continue;
					}
					try {
						if(tokenResults.next()) {
							consumerAcctId = tokenResults.getValue("consumerAcctId", Long.class);
							loyaltyToken = StringUtils.toHex(tokenResults.getValue("consumerAcctToken", byte[].class));
							if(tokenResults.next()) {
								log.error("Found more than one account token for global account id = " + globalAccountId + "; continuing anyway");
							}
						} else {
							continue;
						}
					} finally {
						tokenResults.close();
					}
					break;
			}

			// if the requested amount is less then prefLimit (or prefLimit is null) then use the lesser of requested amount and prefAmount, else use requested Amount
			BigDecimal prefLimit = results.getValue("maxAuthAmt", BigDecimal.class);
			BigDecimal prefAmount = results.getValue("prefAuthAmt", BigDecimal.class);

			Number amount = dataAuth.getAmount();
			if(amount == null || dataAuth.getPaymentActionType() == PaymentActionType.BALANCE_CHECK)
				amount = BigDecimal.ZERO;
			BigDecimal requestedAmount = ConvertUtils.convert(BigDecimal.class, amount);
			if(prefLimit == null || prefLimit.compareTo(requestedAmount) > 0) {
				if(prefAmount != null && prefAmount.compareTo(requestedAmount) < 0)
					requestedAmount = prefAmount;
			}
			
			// Parse card
			String bref = results.getValue("cardRegexBref", String.class);
			Map<PaymentMaskBRef, Object> parsedCardInfo;
			if(matcher != null)
				parsedCardInfo = parseCardInfo(matcher, bref);
			else
				parsedCardInfo = new HashMap<PaymentMaskBRef, Object>();

			String globalEventCd = ProcessingConstants.GLOBAL_EVENT_CODE_PREFIX + ':' + message.getDeviceName() + ":" + dataAuth.getTransactionId();
			int minorCurrencyFactor = results.getValue("minorCurrencyFactor", Integer.class);
			String timeZoneGuid = results.getValue("timeZoneGuid", String.class);
			String paymentType = results.getValue("paymentType", String.class);
			long traceNumber = nextTraceNumber();
			String cardType = results.getValue("cardType", String.class);
			String maskedCardNumber = MessageResponseUtils.maskCardNumber(accountDatas[i]);
			final EnumMap<ChainIndex, MessageChainStep> stepChain = new EnumMap<ChainIndex, MessageChainStep>(ChainIndex.class);
			final MessageChainStep saveStep = requestStep.addStep(getRecordAuthQueueKey());
			stepChain.put(ChainIndex.SAVE_STEP, saveStep);
			final MessageChainStep statsStep = requestStep.addStep(getRecordStatsQueueKey());
			stepChain.put(ChainIndex.STATS_STEP, statsStep);
			if(afterStep != null)
				statsStep.setNextSteps(0, afterStep);

			Float longitude;
			Float latitude;
			long geocodeTime;
			Integer oemIntegrity;
			Integer geocodeProvider;
			String consumerEmail=null;
			Integer sproutVendorId=null;
			if(dataAuth instanceof WS2RequestMessageData) {
				Map<String, String> attributes = ((WS2RequestMessageData) dataAuth).getAttributeMap();
				updateCardInfo(parsedCardInfo, attributes, "billingAddress", PaymentMaskBRef.ADDRESS);
				updateCardInfo(parsedCardInfo, attributes, "billingPostalCode", PaymentMaskBRef.ZIP_CODE);
				updateCardInfo(parsedCardInfo, attributes, "securityCode", PaymentMaskBRef.DISCRETIONARY_DATA);
				longitude = ConvertUtils.convertSafely(Float.class, attributes.get("longitude"), null);
				latitude = ConvertUtils.convertSafely(Float.class, attributes.get("latitude"), null);
				geocodeTime = ConvertUtils.getLongSafely(attributes.get("geocodeTime"), 0L);
				if(longitude != null)
					saveStep.setAttribute(AuthorityAttrEnum.ATTR_LONGITUDE, longitude);
				if(latitude != null)
					saveStep.setAttribute(AuthorityAttrEnum.ATTR_LATITUDE, latitude);
				if(geocodeTime > 0)
					saveStep.setAttribute(AuthorityAttrEnum.ATTR_GEOCODE_TIME, geocodeTime);
				oemIntegrity = ConvertUtils.convertSafely(Integer.class, attributes.get("oemIntegrity"), null);
				geocodeProvider = ConvertUtils.convertSafely(Integer.class, attributes.get("geocodeProvider"), null);
				consumerEmail=attributes.get("consumerEmail");
				sproutVendorId=ConvertUtils.convertSafely(Integer.class, attributes.get("sproutVendorId"), null);
			} else {
				longitude = null;
				latitude = null;
				geocodeTime = 0;
				oemIntegrity = null;
				geocodeProvider = null;
			}

			// Save the auth whether we successfully send response to client or not Add common attributes to saveStep
			if(responseStep != null) {
				saveStep.addReferenceAttribute("sentToDevice", responseStep, "sentToDevice");
				statsStep.addReferenceAttribute("responseStartTime", responseStep, "startTime");
				statsStep.addReferenceAttribute("responseEndTime", responseStep, "replyTime");
				responseStep.addLiteralAttribute("paymentType", paymentType);
				responseStep.addIntAttribute("minorCurrencyFactor", minorCurrencyFactor);
				responseStep.addStringAttribute("authAmount", amount.toString());
				stepChain.put(ChainIndex.RESPONSE_STEP, responseStep);
			} else {
				saveStep.addBooleanAttribute("sentToDevice", false);
			}
			saveStep.addStringAttribute("globalSessionCode", message.getGlobalSessionCode());
			saveStep.addLongAttribute("authTime", authTime);
			saveStep.addStringAttribute("timeZoneGuid", timeZoneGuid);
			saveStep.addStringAttribute("deviceName", message.getDeviceName());
			saveStep.addLongAttribute("eventId", dataAuth.getTransactionId());
			saveStep.setAttribute(AuthorityAttrEnum.ATTR_TRACK_DATA, maskedCardNumber);

			saveStep.addLongAttribute("posPtaId", posPtaId);
			saveStep.addStringAttribute("globalEventCd", globalEventCd);
			saveStep.addStringAttribute("entryMethod", String.valueOf(entryType.getValue()));
			saveStep.addStringAttribute("paymentType", paymentType);
			saveStep.addIntAttribute("minorCurrencyFactor", minorCurrencyFactor);
			saveStep.addStringAttribute("currencyCd", currencyCd);

			saveStep.addLongAttribute("traceNumber", traceNumber);
			saveStep.addStringAttribute("authAmount", amount.toString());
			saveStep.addStringAttribute("requestedAmount", requestedAmount.toString());
			saveStep.setAttribute(AuthorityAttrEnum.ATTR_CARD_TYPE, cardType);
			
			saveStep.setAttribute(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_KEY_ID, paymentSubtypeKeyId);
			
			String passIdentifier = requestStep.getOptionallyIndexedAttribute(CommonAttrEnum.ATTR_PASS_IDENTIFIER, indexes[i], String.class, false);
			boolean validPassIdentifier = StringHelper.isLong(passIdentifier);
			if (validPassIdentifier)
				saveStep.setAttribute(CommonAttrEnum.ATTR_PASS_IDENTIFIER, passIdentifier);

			// Add to stats step
			statsStep.addStringAttribute("globalSessionCode", message.getGlobalSessionCode());
			statsStep.addLongAttribute("authTime", authTime);
			statsStep.addLongAttribute("applayerStartTime", startTime);
			statsStep.addStringAttribute("paymentType", paymentType);
			statsStep.addReferenceAttribute("tranId", saveStep, "tranId");
			statsStep.addReferenceAttribute("authId", saveStep, "authId");
			statsStep.addReferenceAttribute("passThru", saveStep, "passThru");
			statsStep.addReferenceAttribute("authResultCd", saveStep, "authResultCd");

			STATE: switch(checkState) {
				case DUKPT_DECRYPTION_FAILURE: // if(requestStep.getResultAttributes().containsKey("decryptionError")) {
					AuthResultCode authResultCd = requestStep.getAttributeDefault(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.class, AuthResultCode.FAILED);
					String authorityRespCd = requestStep.getAttributeDefault(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, String.class, "DUKPT_DECRYPTION_FAILURE");
					String authorityRespDesc = requestStep.getAttributeDefault(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, String.class, failureMessage);
					saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, authResultCd);
					DeniedReason deniedReason = requestStep.getAttributeDefault(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.class, DeniedReason.DUKPT_DECRYPTION_FAILURE);
					saveStep.setAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, deniedReason);
					saveStep.addStringAttribute("authorityRespCd", authorityRespCd);
					saveStep.addStringAttribute("authorityRespDesc", authorityRespDesc); // requestStep.getResultAttributes().get("decryptionError").toString());
					if(responseStep != null) {
						responseStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, authResultCd);
						String text =null;
						if(!StringUtils.isBlank(authorityRespCd)&& authorityRespCd.equals("INVALID_TOKEN")){
							text = message.getTranslator().translate("client.message." + language + ".auth.denied-" + authorityRespCd, null, authResultCd, authorityRespCd, authorityRespDesc);
						}else{
							text = message.getTranslator().translate("client.message." + language + ".auth.denied-" + deniedReason.toString(), null, authResultCd, authorityRespCd, authorityRespDesc);
						}
						sendResponse(message, responseStep, dataAuth, authResultCd, globalAccountId, cardType, null, false, text, false, false, appSessionKey);
					}
					currentStep.getResultAttributes().put("endSession", SessionCloseReason.DECRYPTION_FAILURE);
					break;
				case NORMAL:
					if(globalAccountId != null && getDeviceInfoManager().isBlacklisted(globalAccountId)) {
						saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, "O");
						saveStep.setAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.NOT_ACTIVE);
						saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, "ACCOUNT_BLACKLISTED");
						saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, "Card is blacklisted");
						if(responseStep != null) {
							responseStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.DECLINED_PERMANENT);
							sendDecline(message, responseStep, dataAuth, globalAccountId, cardType, appSessionKey, "client.message." + language + ".auth.denied-" + DeniedReason.NOT_ACTIVE.toString());
						}
						break;
					}
					Date declineUntil = results.getValue("declineUntil", Date.class);
					if(declineUntil != null && declineUntil.getTime() > authTime) {
						saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.DECLINED.getValue());
						saveStep.setAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.DEVICE_TEMP_BLACKLIST);
						saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, "DEVICE TEMPORARILY BLACKLISTED");
						saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, "Device blacklisted until " + declineUntilFormat.format(declineUntil));
						if(responseStep != null) {
							responseStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.DECLINED);
							sendDecline(message, responseStep, dataAuth, globalAccountId, cardType, appSessionKey, "client.message." + language + ".auth.denied-" + DeniedReason.DEVICE_TEMP_BLACKLIST.toString());
						}
						break;
					}
					String gatewayQueueKey = results.getValue("gatewayQueueKey", String.class);
					// if is mobile and authority requires certain countries
					PosEnvironment posEnvironment = results.getValue("posEnvironment", PosEnvironment.class);
					if(posEnvironment == PosEnvironment.MOBILE) {
						Set<String> set = allowedGeocodeCountries.get(gatewayQueueKey);
						if(set != null && !set.isEmpty()) {
							if(longitude == null || latitude == null) {
								saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.DECLINED.getValue());
								saveStep.setAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.NO_GEOLOCATION);
								saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, "NO GEOLOCATION PROVIDED");
								saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, "Longitute and Latitude were not provided");
								if(responseStep != null) {
									responseStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.DECLINED);
									sendDecline(message, responseStep, dataAuth, globalAccountId, cardType, appSessionKey, "client.message." + language + ".auth.denied-" + DeniedReason.NO_GEOLOCATION.toString());
								}
								break;
							}
							if(getMaxGeocodeStaleness() > 0 && authTime > geocodeTime + getMaxGeocodeStaleness()) {
								saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.DECLINED.getValue());
								saveStep.setAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.STALE_GEOLOCATION);
								saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, "GEOLOCATION TOO STALE");
								String intervalText = formatInterval(authTime - geocodeTime);
								saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, "Longitute and Latitude last read " + intervalText + " ago");
								if(responseStep != null) {
									responseStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.DECLINED);
									sendDecline(message, responseStep, dataAuth, globalAccountId, cardType, appSessionKey, "client.message." + language + ".auth.denied-" + DeniedReason.STALE_GEOLOCATION.toString(), intervalText);
								}
								break;
							}
							if(!allowedGeocodeOEMIntegrities.isEmpty() && (oemIntegrity == null || !allowedGeocodeOEMIntegrities.contains(oemIntegrity))) {
								saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.DECLINED.getValue());
								saveStep.setAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.DISALLOWED_GEOLOCATION);
								saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, "OEM INTEGRITY NOT ALLOWED");
								saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, "Device is rooted or jailbroken (" + oemIntegrity + ")");
								if(responseStep != null) {
									responseStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.DECLINED);
									sendDecline(message, responseStep, dataAuth, globalAccountId, cardType, appSessionKey, "client.message." + language + ".auth.denied-ESCALATED_PRIVS");
								}
								break;
							}
							if(!allowedGeocodeProviders.isEmpty() && (geocodeProvider == null || !allowedGeocodeProviders.contains(geocodeProvider))) {
								saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.DECLINED.getValue());
								saveStep.setAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.DISALLOWED_GEOLOCATION);
								saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, "GEOCODE PROVIDER NOT ALLOWED");
								saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, "Source of geocode (" + geocodeProvider + ") is not allowed");
								if(responseStep != null) {
									responseStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.DECLINED);
									sendDecline(message, responseStep, dataAuth, globalAccountId, cardType, appSessionKey, "client.message." + language + ".auth.denied-GEOCODE_SOURCE");
								}
								break;
							}

							Map<String, Object> params = new HashMap<>();
							params.put("longitude", longitude);
							params.put("latitude", latitude);
							try {
								Results countryResults = DataLayerMgr.executeQuery("GET_COUNTRY_BY_COORDS", params, true);
								if(countryResults.isGroupEnding(0)) {
									saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.DECLINED.getValue());
									saveStep.setAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.DISALLOWED_GEOLOCATION);
									saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, "GEOLOCATION IS DISALLOWED");
									saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, "Transactions outside a known country are not allowed");
									if(responseStep != null) {
										responseStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.DECLINED);
										sendDecline(message, responseStep, dataAuth, globalAccountId, cardType, appSessionKey, "client.message." + language + ".auth.denied-" + DeniedReason.DISALLOWED_GEOLOCATION.toString());
									}
									break;
								}
								while(countryResults.next()) {
									String geocodeCountryCd = ConvertUtils.getString(countryResults.getValue("countryCd"), "---");
									String geocodeCountryName = ConvertUtils.getString(countryResults.getValue("countryName"), null);
									if(!set.contains(geocodeCountryCd.toUpperCase())) {
										saveStep.setAttribute(AuthorityAttrEnum.ATTR_GEOCODE_COUNTRY, geocodeCountryName);
										saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.DECLINED.getValue());
										saveStep.setAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.DISALLOWED_GEOLOCATION);
										saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, "GEOLOCATION IS DISALLOWED");
										saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, "Transactions from '" + geocodeCountryName + "' are not allowed");
										if(responseStep != null) {
											responseStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.DECLINED);
											sendDecline(message, responseStep, dataAuth, globalAccountId, cardType, appSessionKey, "client.message.auth.denied-" + DeniedReason.DISALLOWED_GEOLOCATION.toString());
										}
										break STATE;
									} else if(countryResults.getRow() == 1)
										saveStep.setAttribute(AuthorityAttrEnum.ATTR_GEOCODE_COUNTRY, geocodeCountryName);
								}
							} catch(SQLException e) {
								throw DatabasePrerequisite.createServiceException("Could not get geocode country for the auth", e);
							} catch(DataLayerException e) {
								throw new RetrySpecifiedServiceException("Could not get geocode country for the auth", e, WorkRetryType.NONBLOCKING_RETRY);
							}
						}
					}
					Short globalAccountInstance;
					if(entryType != EntryType.CASH && globalAccountId != null)
						globalAccountInstance = requestStep.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_INSTANCE, indexes[i], Short.class, false);
					else
						globalAccountInstance = null;
					int serviceType = results.getValue("serviceType", Integer.class);
					boolean allowPartialAuth;
					String invoiceNumber = null;
					if(dataAuth.getPaymentActionType() == PaymentActionType.REPLENISHMENT) {
						final MessageChainStep replenishStep = saveStep;
						try {
							DataLayerMgr.selectInto("GET_REPLENISH_CONSUMER_ACCT", dataAuth, replenishStep.getAttributes());
						} catch(NotEnoughRowsException e) {
							saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, "N");
							saveStep.setAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.NO_ACCOUNT);
							saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, "NO_REPLENISH_ACCOUNT");
							saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, "Could not find an active replenish account");
							if(responseStep != null) {
								responseStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.DECLINED);
								sendDecline(message, responseStep, dataAuth, globalAccountId, cardType, appSessionKey, "client.message." + language + ".auth.replenish.denied-" + DeniedReason.NO_ACCOUNT.toString());
							}
							break;
						} catch(SQLException e) {
							log.error("While finding replenish card", e);
							saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, "F");
							saveStep.setAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.APP_LAYER_PROCESSING_ERROR);
							saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, DeniedReason.APP_LAYER_PROCESSING_ERROR);
							saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, e.getMessage());
							if(responseStep != null) {
								responseStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.FAILED);
								sendFailure(message, responseStep, dataAuth, globalAccountId, cardType, appSessionKey, "client.message." + language + ".auth.denied-" + DeniedReason.APP_LAYER_PROCESSING_ERROR.toString());
							}
							break;
						} catch(DataLayerException e) {
							log.error("While finding replenish card", e);
							saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, "F");
							saveStep.setAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.APP_LAYER_PROCESSING_ERROR);
							saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, DeniedReason.APP_LAYER_PROCESSING_ERROR);
							saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, e.getMessage());
							if(responseStep != null) {
								responseStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.FAILED);
								sendFailure(message, responseStep, dataAuth, globalAccountId, cardType, appSessionKey, "client.message." + language + ".auth.denied-" + DeniedReason.APP_LAYER_PROCESSING_ERROR.toString());
							}
							break;
						} catch(BeanException e) {
							log.error("While finding replenish card", e);
							saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, "F");
							saveStep.setAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.APP_LAYER_PROCESSING_ERROR);
							saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, DeniedReason.APP_LAYER_PROCESSING_ERROR);
							saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, e.getMessage());
							if(responseStep != null) {
								responseStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.FAILED);
								sendFailure(message, responseStep, dataAuth, globalAccountId, cardType, appSessionKey, "client.message." + language + ".auth.denied-" + DeniedReason.APP_LAYER_PROCESSING_ERROR.toString());
							}
							break;
						}

						// Add other attributes
						ReplenishMessageData dataReplenish = (ReplenishMessageData) dataAuth;
						replenishStep.getAttributes().put("tranDeviceResultTypeCd", dataReplenish.getTransactionResult().getValue());
						replenishStep.getAttributes().put("deviceBatchId", dataReplenish.getBatchId());
						replenishStep.getAttributes().put("receiptResultCd", dataReplenish.getReceiptResult().getValue());
						if(dataReplenish.isAuthOnly())
							replenishStep.getAttributes().put("authOnly", 'Y');

						// replenish step should be combination of save and sale step
						replenishStep.setQueueKey(getReplenishQueueKey());
						stepChain.remove(ChainIndex.SAVE_STEP);
						stepChain.put(ChainIndex.REPLENISH_STEP, replenishStep);
						// Add update auth step to populate card key and possibly global account id (consumer acct id)
						if(entryType != EntryType.CASH) {
							final MessageChainStep updateStep = requestStep.addStep(getUpdateAuthQueueKey());
							stepChain.put(ChainIndex.UPDATE_STEP, updateStep);
							updateStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_TRAN_ID, replenishStep, AuthorityAttrEnum.ATTR_TRAN_ID);
							updateStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_AUTH_ID, replenishStep, AuthorityAttrEnum.ATTR_AUTH_ID);
							updateStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_TRAN_IMPORT_NEEDED, replenishStep, AuthorityAttrEnum.ATTR_TRAN_IMPORT_NEEDED);
							updateStep.setAttribute(AuthorityAttrEnum.ATTR_POS_PTA_ID, posPtaId);
							updateStep.setAttribute(AuthorityAttrEnum.ATTR_CURRENCY_CD, currencyCd);
							updateStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_TIME, authTime);
							updateStep.setAttribute(AuthorityAttrEnum.ATTR_TRACK_DATA, maskedCardNumber);
						}
						allowPartialAuth = true;
					} else if(dataAuth.getPaymentActionType() == PaymentActionType.BALANCE_CHECK) {
						// stepChain.remove(ChainIndex.SAVE_STEP);
						// stepChain.remove(ChainIndex.STATS_STEP);
						if(globalAccountId != null) {
							Map<String, Object> params = new HashMap<>();
							params.put("globalAccountId", globalAccountId);
							params.put("instance", globalAccountInstance);
							params.put("posPtaId", posPtaId);
							params.put("currencyCd", currencyCd);
							params.put("authTime", authTime);
							params.put("trackData", maskedCardNumber);
							try {
								DataLayerMgr.executeCall("GET_CONSUMER_ACCT_FOR_TRAN", params, true);
							} catch(SQLException e) {
								throw DatabasePrerequisite.createServiceException("Could not get consumerAcctId for the auth", e);
							} catch(DataLayerException e) {
								throw new RetrySpecifiedServiceException("Could not get consumerAcctId for the auth", e, WorkRetryType.NONBLOCKING_RETRY);
							}
							// consumerAcctId = ConvertUtils.convertSafely(Long.class, params.get("consumerAcctId"), null);
						}
						allowPartialAuth = false;
					} else if(dataAuth instanceof Sale) {
						final MessageChainStep saleStep = requestStep.addStep(getSaleQueueKey());
						stepChain.put(ChainIndex.SALE_STEP, saleStep);
						// Add attributes
						for(Attribute attr : getSaleAttributes()) {
							Object value = requestStep.getAttributeSafely(attr, Object.class, null);
							if(value != null)
								saleStep.setAttribute(attr, value);
							else if(attr instanceof DeviceInfoProperty) {
								value = ((DeviceInfoProperty) attr).getProperty(deviceInfo);
								if(value != null)
									saleStep.setAttribute(attr, value);
							}
						}

						for(LineItem li : ((Sale) dataAuth).getLineItems()) {
							if(replenishItemIds.contains(li.getItem())) {
								saveStep.addStringAttribute("authResultCd", "F");
								saveStep.setAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.DATA_FROM_CLIENT);
								saveStep.addStringAttribute("authorityRespCd", "REPLENISH_ITEMS_NOT_ALLOWED");
								saveStep.addStringAttribute("authorityRespDesc", "Invalid line items: Replenishment not allowed"); // requestStep.getResultAttributes().get("decryptionError").toString());
								if(responseStep != null) {
									responseStep.addLiteralAttribute("authResultCd", AuthResultCode.FAILED);
									sendFailure(message, responseStep, dataAuth, globalAccountId, cardType, appSessionKey, "client.message." + language + ".auth.denied-" + DeniedReason.DATA_FROM_CLIENT.toString(), "Replenish line items not allowed");
								}
								break STATE;
							} else if(ProcessingConstants.REFERENCE_ITEM_TYPE == li.getItem()) {
								invoiceNumber = li.getDescription();
							}
						}
						allowPartialAuth = false;
					} else {
						saveStep.setNextSteps(0, statsStep);
						if ("N".equalsIgnoreCase(results.getFormattedValue("partialAuthInd")))
							allowPartialAuth = false;
						else
							allowPartialAuth = (dataAuth.getMessageType() != MessageType.AUTH_REQUEST_2_0);
					}

					Character autoAuthResultCd = results.getValue("autoAuthResultCd", Character.class);
					Boolean trackCrcMatch = requestStep.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_TRACK_CRC_MATCH, indexes[i], Boolean.class, false);
					Integer trackCrc = requestStep.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_TRACK_CRC, indexes[i], Integer.class, false);
					byte[] accountCdHash = null;
					MessageChainStep storeStep;
					if(entryType == EntryType.CASH) {
						storeStep = null;
					} else if(globalAccountId != null) {
						saveStep.setAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, globalAccountId);
						saveStep.setAttribute(AuthorityAttrEnum.ATTR_INSTANCE, globalAccountInstance);
						if(trackCrc != null && (trackCrcMatch == null || (handling != AccountHandling.DENY_ON_NO_ACCOUNT_ID && !trackCrcMatch.booleanValue())) && entryType != EntryType.TOKEN) {
							// new track crc - update on successful auth
							storeStep = requestStep.addStep(getStoreCardQueueKey());
							stepChain.put(ChainIndex.STORE_STEP, storeStep);
							storeStep.setAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, globalAccountId);
							storeStep.setAttribute(AuthorityAttrEnum.ATTR_INSTANCE, globalAccountInstance);
							storeStep.setAttribute(AuthorityAttrEnum.ATTR_TRACK_CRC, trackCrc);
						} else {
							storeStep = null; // at least for now
						}
					} else {
						accountCdHash = requestStep.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_ACCOUNT_CD_HASH, indexes[i], byte[].class, false);
						if(accountCdHash != null) {
							storeStep = requestStep.addStep(getStoreCardQueueKey());
							stepChain.put(ChainIndex.STORE_STEP, storeStep);
							storeStep.setAttribute(AuthorityAttrEnum.ATTR_ACCOUNT_CD_HASH, accountCdHash);
							if(trackCrc != null)
								storeStep.setAttribute(AuthorityAttrEnum.ATTR_TRACK_CRC, trackCrc);

							// if this is a pass-through check global_account_id of previous auth step
							if(globalAccountIdReferenceStep != null) {
								storeStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, globalAccountIdReferenceStep, AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID);
								storeStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_INSTANCE, globalAccountIdReferenceStep, AuthorityAttrEnum.ATTR_INSTANCE);
							}
						} else {
							deniedReason = requestStep.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, indexes[i], DeniedReason.class, false);
							if(deniedReason == DeniedReason.KEYMGRS_NOT_LOADED) {
								storeStep = requestStep.addStep(getStoreCardQueueKey());
								stepChain.put(ChainIndex.STORE_STEP, storeStep);
								try {
									storeStep.setAttribute(AuthorityAttrEnum.ATTR_ACCOUNT_CD_HASH, SecureHash.getUnsaltedHash(MessageResponseUtils.getCardNumber(accountDatas[i]).getBytes()));
								} catch(NoSuchAlgorithmException e) {
									log.warn("Could not calculate account cd hash", e);
								}
								CRC16 crc = new CRC16();
								for(byte b : accountDatas[i].getBytes())
									crc.update(b);
								storeStep.setAttribute(AuthorityAttrEnum.ATTR_TRACK_CRC, crc.value);
							} else
								storeStep = null; // at least for now
						}
					}
					if(dataAuth.getPaymentActionType() != PaymentActionType.BALANCE_CHECK && serviceType != 2 && (amount.longValue() < 0 || deviceInfo.getMaxAuthAmount() > 0 && amount.longValue() > deviceInfo.getMaxAuthAmount())) {
						saveStep.addStringAttribute("authResultCd", "F");
						saveStep.setAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.CONFIGURATION_ISSUE);
						saveStep.addStringAttribute("authorityRespCd", "INVALID_AUTH_AMOUNT");
						if(amount.longValue() > deviceInfo.getMaxAuthAmount())
							saveStep.addStringAttribute("authorityRespDesc", new StringBuilder("The auth amount in pennies ").append(amount).append(" is greater than max auth amount ").append(deviceInfo.getMaxAuthAmount()).toString());
						else
							saveStep.addStringAttribute("authorityRespDesc", "The auth amount is invalid:" + amount);
						if(responseStep != null) {
							responseStep.addLiteralAttribute("authResultCd", AuthResultCode.FAILED);
							sendFailure(message, responseStep, dataAuth, globalAccountId, cardType, appSessionKey, "client.message." + language + ".auth.denied-" + DeniedReason.CONFIGURATION_ISSUE.toString());
						}
					} else if(serviceType == 2 && autoAuthResultCd == null && (dataAuth.getMessageType() == MessageType.AUTH_REQUEST_3_2 || dataAuth.getMessageType() == MessageType.AUTH_REQUEST_3_1 || dataAuth.getMessageType() == MessageType.AUTH_REQUEST_3_0 || dataAuth.getMessageType() == MessageType.AUTH_REQUEST_2_0)) {
						saveStep.addStringAttribute("authResultCd", "N");
						saveStep.setAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.CONFIGURATION_ISSUE);
						saveStep.addStringAttribute("authorityRespCd", "PERMISSION REQUESTS ONLY");
						saveStep.addStringAttribute("authorityRespDesc", "This payment configuration entry only supports permission requests");
						if(responseStep != null) {
							responseStep.addLiteralAttribute("authResultCd", AuthResultCode.DECLINED);
							sendDecline(message, responseStep, dataAuth, globalAccountId, cardType, appSessionKey, "client.message." + language + ".auth.denied-" + DeniedReason.CONFIGURATION_ISSUE.toString());
						}
					} else if(handling == AccountHandling.DENY_ON_NO_ACCOUNT_ID && globalAccountId == null) {
						deniedReason = requestStep.getOptionallyIndexedAttributeDefault(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, indexes[i], DeniedReason.class, DeniedReason.NO_ACCOUNT);
						saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, "N");
						saveStep.setAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, deniedReason);
						saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, "PRECHECK");
						if(deniedReason == DeniedReason.NO_ACCOUNT) {
							saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, "Could not find an active account");
						} else {
							saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, StringUtils.capitalizeFirst(deniedReason.toString().replace('_', ' ')));
						}
						if(responseStep != null) {
							responseStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.DECLINED);
							sendDecline(message, responseStep, dataAuth, globalAccountId, cardType, appSessionKey, "client.message.auth.denied-" + deniedReason.toString());
						}
					} else if(handling == AccountHandling.DENY_ON_NO_ACCOUNT_ID && trackCrcMatch != null && !trackCrcMatch.booleanValue() && entryType != EntryType.MANUAL && entryType != EntryType.TOKEN && entryType != EntryType.VAS) {
						saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, "N");
						saveStep.setAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.INVALID_VERIFICATION_DATA);
						saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, "PRECHECK");
						saveStep.setAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, "Invalid card");
						if(responseStep != null) {
							responseStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.DECLINED);
							sendDecline(message, responseStep, dataAuth, globalAccountId, cardType, appSessionKey, "client.message." + language + ".auth.denied-" + DeniedReason.INVALID_VERIFICATION_DATA.toString());
						}
					} else {
						boolean noConvenienceFee = "Y".equalsIgnoreCase(results.getValue("noConvenienceFee", String.class));
						// check auto-response
						if(autoAuthResultCd != null) {
							if(!allowPartialAuth && autoAuthResultCd == 'P')
								autoAuthResultCd = 'N';
							String autoResponseReason = results.getValue("autoAuthResponse", String.class);
							String clientText;
							if(message.getLog().isInfoEnabled())
								message.getLog().info("Using Auto-Auth for '" + globalEventCd + "' to '" + authorityName + "' (pos_pta_id=" + posPtaId + ") with authResultCd = " + autoAuthResultCd + " and authorityRespDesc = '" + autoResponseReason + "'");
							AuthResultCode authResponse;
							try {
								authResponse = AuthResultCode.getByValue(autoAuthResultCd);
							} catch(InvalidValueException e) {
								log.warn("Could not convert '" + autoAuthResultCd + "' to AuthResultCode", e);
								authResponse = AuthResultCode.FAILED;
							}
							deniedReason = null;
							switch(authResponse) {
								case APPROVED:
								case PARTIAL:
									clientText = message.getTranslator().translate("client.message.auth.approved", "{0}", autoResponseReason);
									break;
								case DECLINED_PAYMENT_METHOD:
									deniedReason = DeniedReason.NO_ACCOUNT;
									// fall-thru
								default:
									clientText = message.getTranslator().translate("client.message." + language + ".auth.denied-" + DeniedReason.NO_ACCOUNT.toString(), "{0}", autoResponseReason);
									break;
							}

							final String cardKey = requestStep.getAttributeSafely(AuthorityAttrEnum.ATTR_CARD_KEY, String.class, null);
							BigDecimal approvedAmount;
							MT: switch(dataAuth.getMessageType()) {
								case PERMISSION_REQUEST_3_1: // v3.1 Permission Request
									MessageData_AB replyAB = new MessageData_AB();
									replyAB.setTransactionId(dataAuth.getTransactionId());
									replyAB.setResponseCode(authResponse.getPermissionResponseCodeAB());

									ABPermissionActionCode autoActionCodeAB = results.getValue("autoActionCode", ABPermissionActionCode.class);
									if(autoActionCodeAB != null) {
										replyAB.setActionCode(autoActionCodeAB);
										// TODO: Store in AUTH table (this will require a lookup of the ActionId)
										saveStep.addByteAttribute("authActionCode", autoActionCodeAB.getValue());
									}
									if(replyAB.getActionCodeData() instanceof MessageData_AB.ConfigurableAction) {
										Integer autoActionBitmap = results.getValue("autoActionBitmap", Integer.class);
										if(autoActionBitmap != null) {
											MessageData_AB.ConfigurableAction ca = (MessageData_AB.ConfigurableAction) replyAB.getActionCodeData();
											ca.setActionBitmap(autoActionBitmap);
											saveStep.addLiteralAttribute("authActionBitmap", autoActionBitmap);
										}
									}
									approvedAmount = null;
									if(responseStep != null) {
										responseStep.addLiteralAttribute("authResultCd", autoAuthResultCd);
										sendReply(responseStep, replyAB);
									}
									break MT;
								case AUTH_REQUEST_4_1: // v4.1 Auth
								case CHARGE_REQUEST_4_1: // v4.1 Charge
									switch(serviceType) {
										case 2: // permission request
											MessageData_C3 replyC3 = new MessageData_C3();
											replyC3.setAuthResponseType(AuthResponseType.PERMISSION);
											PermissionAuthResponse par = (PermissionAuthResponse) replyC3.getAuthResponseTypeData();
											par.setResponseCode(authResponse.getPermissionResponseCodeC3());
											AuthPermissionActionCode autoActionCode = results.getValue("autoActionCode", AuthPermissionActionCode.class);
											if(autoActionCode != null) {
												par.setActionCode(autoActionCode);
												// TODO: Store in AUTH table (this will require a lookup of the ActionId)
												saveStep.addByteAttribute("authActionCode", autoActionCode.getValue());
											}
											if(par.getActionCodeData() instanceof ConfigurableAction) {
												Long autoActionBitmap = results.getValue("autoActionBitmap", Long.class);
												if(autoActionBitmap != null) {
													ConfigurableAction ca = (ConfigurableAction) par.getActionCodeData();
													ca.setActionBitmap(autoActionBitmap);
													saveStep.addLiteralAttribute("authActionBitmap", autoActionBitmap);
												}
											}
											approvedAmount = null;
											if(responseStep != null) {
												responseStep.addLiteralAttribute("authResultCd", autoAuthResultCd);
												if(requestStep != currentStep) {
													// pass-through so allow overwrite of response message
													sendReplyTemplate(responseStep, replyC3);
												} else
													sendReply(responseStep, replyC3);
											}

											break MT;
									}
									// Fall-through
								case AUTH_REQUEST_2_0: // v2.0 Auth
								case AUTH_REQUEST_3_0: // v3.0 Auth
								case AUTH_REQUEST_3_1: // v3.1 Auth
								case AUTH_REQUEST_3_2: // v3.2 Auth
									clientText = null; // let device use it's default
								case WS_AUTH_V_3_REQUEST:
								case WS_AUTH_V_3_1_REQUEST:
								case WS_CHARGE_V_3_REQUEST:
								case WS_CHARGE_V_3_1_REQUEST:
								case WS2_AUTH_ENCRYPTED_REQUEST:
								case WS2_AUTH_PLAIN_REQUEST:
								case WS2_CHARGE_ENCRYPTED_REQUEST:
								case WS2_CHARGE_PLAIN_REQUEST:
								case WS2_REPLENISH_CASH_REQUEST:
								case WS2_REPLENISH_ENCRYPTED_REQUEST:
								case WS2_REPLENISH_PLAIN_REQUEST:
									boolean checkReplenish;
									switch(authResponse) {
										case APPROVED:
											approvedAmount = requestedAmount;
											checkReplenish = true;
											break;
										case PARTIAL:
											approvedAmount = BigDecimal.ZERO;
											checkReplenish = true;
											break;
										default:
											approvedAmount = null;
											checkReplenish = false;
									}
									if(responseStep != null) {
										responseStep.addLiteralAttribute("authResultCd", autoAuthResultCd);
										if(checkReplenish && dataAuth.getPaymentActionType() == PaymentActionType.REPLENISHMENT) {
											// must use template
											MessageChainStep replenishStep = stepChain.get(ChainIndex.REPLENISH_STEP);
											responseStep.addReferenceAttribute("replyTemplate.authResultCd", replenishStep, "authResultCd");
											addClientTextAttributes(responseStep, null, replenishStep, "returnMessage", null, 1);
											responseStep.addReferenceAttribute("replyTemplate.replenishBalanceAmount", replenishStep, "replenishBalanceAmount");
											// store card data for reuse if cardkey not provided
											if(entryType != EntryType.CASH && StringUtils.isBlank(cardKey)) {
												// Add to storeStep
												if(storeStep == null) {
													storeStep = requestStep.addStep(getStoreCardQueueKey());
													stepChain.put(ChainIndex.STORE_STEP, storeStep);
												}
												addCardInfoToStoreStep(parsedCardInfo, storeStep);
												storeStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, autoAuthResultCd);
											}
											sendResponseTemplate(message, responseStep, dataAuth, authResponse, globalAccountId, cardType, approvedAmount, false, clientText, false, noConvenienceFee, appSessionKey);
										} else if(requestStep != currentStep) {
											// pass-through so allow overwrite of response message
											if(globalAccountId == null && globalAccountIdReferenceStep != null)
												responseStep.addReferenceAttribute("replyTemplate.cardId", globalAccountIdReferenceStep, AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID.getValue());
											sendResponseTemplate(message, responseStep, dataAuth, authResponse, globalAccountId, cardType, approvedAmount, false, clientText, false, noConvenienceFee, appSessionKey);
										} else
											sendResponse(message, responseStep, dataAuth, authResponse, globalAccountId, cardType, approvedAmount, false, clientText, false, noConvenienceFee, appSessionKey);
									}
									break;
								case WS2_GET_CARD_INFO_PLAIN_REQUEST:
								case WS2_GET_CARD_INFO_ENCRYPTED_REQUEST:
									if(responseStep != null) {
										responseStep.addLiteralAttribute("authResultCd", authResponse.getValue());
										sendResponse(message, responseStep, dataAuth, authResponse, globalAccountId, cardType, null, false, autoResponseReason, false, false, appSessionKey);
									}
									approvedAmount = null;
									break;
								case WS2_TOKENIZE_PLAIN_REQUEST:
								case WS2_TOKENIZE_ENCRYPTED_REQUEST:
									switch(authResponse) {
										case APPROVED:
											approvedAmount = requestedAmount;
											break;
										case PARTIAL:
											approvedAmount = BigDecimal.ZERO;
											break;
										default:
											approvedAmount = null;
									}
									if(responseStep != null) {
										responseStep.addLiteralAttribute("authResultCd", autoAuthResultCd);
										sendResponseTemplate(message, responseStep, dataAuth, authResponse, globalAccountId, cardType, approvedAmount, false, clientText, false, noConvenienceFee, appSessionKey);
									}
									break;
								default:
									throw new IllegalStateException("This should never happen");
							}
							// configure save step
							saveStep.addStringAttribute("authResultCd", autoAuthResultCd.toString());
							switch(authResponse) {
								case APPROVED:
								case PARTIAL:
									saveStep.addBooleanAttribute("authHoldUsed", false);
									if(dataAuth.getPaymentActionType() == PaymentActionType.TOKENIZE) {
										// Need to not do store but do create token instead
										if(storeStep == null) {
											storeStep = requestStep.addStep(getCreateTokenQueueKey());
											stepChain.put(ChainIndex.REPLENISH_STEP, storeStep);
										} else {
											storeStep.setQueueKey(getCreateTokenQueueKey());
											stepChain.remove(ChainIndex.STORE_STEP);
											stepChain.put(ChainIndex.REPLENISH_STEP, storeStep);
										}
										if(accountCdHash == null)
											accountCdHash = requestStep.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_ACCOUNT_CD_HASH, indexes[i], byte[].class, false);
										storeStep.setAttribute(AuthorityAttrEnum.ATTR_ACCOUNT_CD_HASH, accountCdHash); // this is so we always UPSERT account in km
										storeStep.setAttribute(MessageAttrEnum.ATTR_DEVICE_NAME, deviceInfo.getDeviceName());
										storeStep.setAttribute(AuthorityAttrEnum.ATTR_DEVICE_TRAN_CD, dataAuth.getTransactionId());
										if(globalAccountId != null) {
											storeStep.setAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, globalAccountId);
											storeStep.setAttribute(AuthorityAttrEnum.ATTR_INSTANCE, globalAccountInstance);
										} else {
											// if this is a pass-through check global_account_id of previous auth step
											if(globalAccountIdReferenceStep != null) {
												storeStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, globalAccountIdReferenceStep, AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID);
												storeStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_INSTANCE, globalAccountIdReferenceStep, AuthorityAttrEnum.ATTR_INSTANCE);
											}
										}
										addCardInfoToStoreStep(parsedCardInfo, storeStep);
										storeStep.setAttribute(AuthorityAttrEnum.ATTR_STORE_EXPIRATION_TIME, determineTargetExpirationTime(parsedCardInfo, startTime, true));
										storeStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, autoAuthResultCd);
										// add token indicator
										if(responseStep != null) {
											responseStep.addReferenceAttribute("replyTemplate.tokenBytes", storeStep, AuthorityAttrEnum.ATTR_AUTH_TOKEN.getValue());
											if(globalAccountId == null && globalAccountIdReferenceStep == null)
												responseStep.addReferenceAttribute("replyTemplate.cardId", storeStep, AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID.getValue());
										}
									}
									break;
							}
							if(deniedReason != null)
								saveStep.setAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.NO_ACCOUNT);
							if(approvedAmount != null)
								saveStep.addStringAttribute("approvedAmount", approvedAmount.toString());

							// saveStep.addStringAttribute("balanceAmount", null);
							saveStep.addStringAttribute("authorityRespCd", "AUTO");
							saveStep.addStringAttribute("authorityRespDesc", autoResponseReason);
							// saveStep.addStringAttribute("authAuthorityTranCd", null); saveStep.addStringAttribute("authAuthorityRefCd", null);
							// saveStep.addLiteralAttribute("authAuthorityTs", null); saveStep.addStringAttribute("authAuthorityMiscData", null);
							// saveStep.addLiteralAttribute("consumerAcctId", null);

							MessageChainStep updateStep = stepChain.get(ChainIndex.UPDATE_STEP);
							if(storeStep != null) {
								for(Attribute a : new Attribute[] { AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, AuthorityAttrEnum.ATTR_INSTANCE, AuthorityAttrEnum.ATTR_CARD_KEY }) {
									if(saveStep.getAttribute(a, Object.class, false) == null)
										saveStep.setReferenceAttribute(a, storeStep, a);	
								}
								if(updateStep != null) {
									if(globalAccountId != null) {
										updateStep.setAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, globalAccountId);
										updateStep.setAttribute(AuthorityAttrEnum.ATTR_INSTANCE, globalAccountInstance);
									} else {
										updateStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, storeStep, AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID);
										updateStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_INSTANCE, storeStep, AuthorityAttrEnum.ATTR_INSTANCE);
									}
									updateStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_CARD_KEY, storeStep, AuthorityAttrEnum.ATTR_CARD_KEY);
								}
							} else if(updateStep != null && !StringUtils.isBlank(cardKey))
								updateStep.setAttribute(AuthorityAttrEnum.ATTR_CARD_KEY, cardKey);
						} else {
							String creditDebitInd = null;
							String countryCode = null;
							Matcher binMatcher = BIN_NUMBER_PATTERN.matcher(accountDatas[i]);
							if(binMatcher.find()) {
								long binNumber = Long.parseLong(binMatcher.group(1));
								try {
									Results binResults = DataLayerMgr.executeQuery("LOOKUP_BIN_RANGE_CARD_DATA", new Object[] { binNumber, binNumber }, null);
									if(binResults.next()) {
										creditDebitInd = ConvertUtils.getStringSafely(binResults.getValue("creditDebitInd"), null);
										countryCode = ConvertUtils.getStringSafely(binResults.getValue("countryCode"), null);
										saveStep.addStringAttribute("creditDebitInd", creditDebitInd);
										saveStep.addStringAttribute("countryCode", countryCode);
									}
								} catch(Exception e) {
									throw new ServiceException("Error checking BIN range", e);
								}
							}
							String disableDebitDenial = results.getValue("disableDebitDenial", String.class);
							boolean debit = false;
							//allow Google Wallet contactless virtual MC debit card with the unregulated BIN 
							if (entryType != EntryType.CONTACTLESS || !googleWalletCardNumberExpression.matcher(accountDatas[i]).matches()) {
								if(declinedDebitGatewayExpression.matcher(gatewayQueueKey).matches()) {
									if (creditDebitInd != null && creditDebitInd.equals("D")) {
										debit = true;
										cardType = cardType + " Debit";
									}
								}
							}

							if(debit && denyDebitExpression.matcher(accountDatas[i]).matches() && (disableDebitDenial == null || !disableDebitDenial.equalsIgnoreCase("Y"))) {
								saveStep.addStringAttribute("authResultCd", "N");
								saveStep.addStringAttribute("authorityRespCd", "DEBIT NOT ALLOWED");
								saveStep.addStringAttribute("authorityRespDesc", "Debit card declined by USAT server");
								if(responseStep != null) {
									responseStep.addLiteralAttribute("authResultCd", AuthResultCode.DECLINED);
									sendResponse(message, responseStep, dataAuth, AuthResultCode.DECLINED_DEBIT, globalAccountId, cardType, null, false, message.getTranslator().translate("client.message.debit-disabled", "Auth Decline"), false, false, appSessionKey);
								}
							} else {
								boolean allowPassThru = ConvertUtils.getBooleanSafely(results.getValue("allowPassThru"), false);
								// route to gateway with adapted messageChain that responds directly to next step
								final MessageChainStep authStep = requestStep.addStep(getAuthorityQueueKeyPrefix() + gatewayQueueKey);
								stepChain.put(ChainIndex.AUTH_STEP, authStep);
								//check message type for offline attribute
								if(dataAuth instanceof WS2RequestMessageData) {
									Map<String, String> msgAttributes = ((WS2RequestMessageData) dataAuth).getAttributeMap();
									String offlineAttrVal = msgAttributes.get("offline");
									if(offlineAttrVal!=null)
										authStep.setAttribute("offline", offlineAttrVal);
								}
								
								saveStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_TRACK_DATA_OVERRIDE, authStep, AuthorityAttrEnum.ATTR_TRACK_DATA_OVERRIDE);
								if (validPassIdentifier)
									authStep.setAttribute(CommonAttrEnum.ATTR_PASS_IDENTIFIER, passIdentifier);
								
								final long targetExpirationTime = determineTargetExpirationTime(parsedCardInfo, startTime, dataAuth.getPaymentActionType() == PaymentActionType.REPLENISHMENT || dataAuth.getPaymentActionType() == PaymentActionType.TOKENIZE);
								final String cardKey = requestStep.getAttributeSafely(AuthorityAttrEnum.ATTR_CARD_KEY, String.class, null);
								if(!StringUtils.isBlank(cardKey) && requestStep.getAttributeSafely(AuthorityAttrEnum.ATTR_STORE_EXPIRATION_TIME, Long.class, 0L) >= targetExpirationTime) {
									saveStep.setAttribute(AuthorityAttrEnum.ATTR_CARD_KEY, cardKey);
								} else if(handling != AccountHandling.DENY_ON_NO_ACCOUNT_ID) {
									// Add to storeStep
									if(storeStep == null) {
										storeStep = requestStep.addStep(getStoreCardQueueKey());
										stepChain.put(ChainIndex.STORE_STEP, storeStep);
									}
									addCardInfoToStoreStep(parsedCardInfo, storeStep);
									storeStep.setReferenceIndexedAttribute(CommonAttrEnum.ATTR_DECRYPTED_DATA_OVERRIDE, PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER.getStoreIndex(), authStep, AuthorityAttrEnum.ATTR_AUTH_ACCOUNT_DATA);
									storeStep.setAttribute(AuthorityAttrEnum.ATTR_STORE_EXPIRATION_TIME, targetExpirationTime);
								}
								if(storeStep != null)
									storeStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, authStep, AuthorityAttrEnum.ATTR_AUTH_RESULT_CD);

								final MessageChainStep origResponseStep = responseStep;
								if(responseStep != null) {
									if(allowPassThru) {
										responseStep = responseStep.copyStep();
										stepChain.put(ChainIndex.RESPONSE_STEP, responseStep);
										// Update these attributes so they point to new responseStep
										saveStep.addReferenceAttribute("sentToDevice", responseStep, "sentToDevice");
										statsStep.addReferenceAttribute("responseStartTime", responseStep, "startTime");
										statsStep.addReferenceAttribute("responseEndTime", responseStep, "replyTime");
									}

									responseStep.addReferenceAttribute("replyTemplate/0.authResultCd", authStep, "authResultCd");
									responseStep.addReferenceAttribute("authResultCd", authStep, "authResultCd");
									responseStep.addLiteralAttribute("paymentType", paymentType);
									responseStep.addIntAttribute("minorCurrencyFactor", minorCurrencyFactor);
									responseStep.addStringAttribute("authAmount", amount.toString());
									responseStep.addReferenceAttribute("authResultCd", authStep, "authResultCd");

									if(handling == AccountHandling.CREATE_ACCOUNT_ID && globalAccountId == null) { // ISIS
										storeStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, authStep, AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID);
										storeStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_INSTANCE, authStep, AuthorityAttrEnum.ATTR_INSTANCE);
										globalAccountIdReferenceStep = authStep;
									}
									Object additionalInfo = requestStep.getAttribute(AuthorityAttrEnum.ATTR_ADDITIONAL_INFO, Object.class, false);
									if(additionalInfo != null)
										authStep.setAttribute(AuthorityAttrEnum.ATTR_ADDITIONAL_INFO, additionalInfo);

									switch(dataAuth.getMessageType()) {
										case AUTH_REQUEST_4_1: // v4.1 Auth
										case CHARGE_REQUEST_4_1: // v4.1 Charge
											if(additionalInfo == null && !StringUtils.isBlank(dataAuth.getValidationData()))
												switch(entryType) {
													case ISIS:
														authStep.setAttribute(AuthorityAttrEnum.ATTR_ADDITIONAL_INFO_TYPE, AdditionalInfoType.ISIS_SMARTTAP);
														authStep.setAttribute(AuthorityAttrEnum.ATTR_ADDITIONAL_INFO, dataAuth.getValidationData());
														break;
													case EMV_CONTACT:
														authStep.setAttribute(AuthorityAttrEnum.ATTR_ADDITIONAL_INFO_TYPE, AdditionalInfoType.EMV_CONTACT);
														authStep.setAttribute(AuthorityAttrEnum.ATTR_ADDITIONAL_INFO, dataAuth.getValidationData());
														break;
													case EMV_CONTACTLESS:
														authStep.setAttribute(AuthorityAttrEnum.ATTR_ADDITIONAL_INFO_TYPE, AdditionalInfoType.EMV_CONTACTLESS);
														authStep.setAttribute(AuthorityAttrEnum.ATTR_ADDITIONAL_INFO, dataAuth.getValidationData());
														break;
													default:
														break;
												}
											
											MessageData_C3 replyC3 = new MessageData_C3();
											switch(serviceType) {
												case 2: // permission request
													replyC3.setAuthResponseType(AuthResponseType.PERMISSION);
													responseStep.addReferenceAttribute("replyTemplate.authResponseTypeData.actionCode", authStep, "actionCode");
													responseStep.addReferenceAttribute("replyTemplate/1.authResponseTypeData.actionCodeData.actionBitmap", authStep, "actionCodeBitmap");
													break;
												case 1: // auth request (show balance)
												case 4: // auth request (hidden balance)
													if(deviceInfo.getPropertyListVersion() == null || deviceInfo.getPropertyListVersion() <= 11)
														replyC3.setAuthResponseType(AuthResponseType.AUTHORIZATION);
													else
														replyC3.setAuthResponseType(AuthResponseType.AUTHORIZATION_V2);
													responseStep.addReferenceAttribute("replyTemplate.authResponseTypeData.balanceAmount", authStep, "balanceAmount");
													responseStep.addReferenceAttribute("replyTemplate.authResponseTypeData.resultAmount", authStep, "approvedAmount"); // This should be null when authResultCd != P
													responseStep.addReferenceAttribute("replyTemplate.authResponseTypeData.authorizationCode", authStep, "authAuthorityTranCd");
													addClientTextAttributes(responseStep, origResponseStep, authStep, "authResponseTypeData.resultMessage", "authResponseTypeData.overrideResultMessage", 1);
													break;
												case 3: // permission check
													if(deviceInfo.getPropertyListVersion() == null || deviceInfo.getPropertyListVersion() <= 11)
														replyC3.setAuthResponseType(AuthResponseType.AUTHORIZATION);
													else
														replyC3.setAuthResponseType(AuthResponseType.AUTHORIZATION_V2);
													responseStep.addReferenceAttribute("replyTemplate.authResponseTypeData.resultAmount", authStep, "approvedAmount"); // This should be null when authResultCd != P
													responseStep.addReferenceAttribute("replyTemplate.authResponseTypeData.authorizationCode", authStep, "authAuthorityTranCd");
													addClientTextAttributes(responseStep, origResponseStep, authStep, "authResponseTypeData.resultMessage", "authResponseTypeData.overrideResultMessage", 1);
													break;
												default:
													throw new ServiceException("Unsupported Service Type " + serviceType);
											}
											replyC3.setCommandCode(message.getDeviceInfo().getActionCode().getAuthResponseActionCode());
											if(replyC3.getAuthResponseType() == AuthResponseType.AUTHORIZATION_V2) {
												AuthorizationV2AuthResponse aar2 = (AuthorizationV2AuthResponse) replyC3.getAuthResponseTypeData();
												if(noConvenienceFee)
													aar2.setNoConvenienceFee(noConvenienceFee);
												if(appSessionKey != null)
													aar2.setAppSessionKey(appSessionKey);
												responseStep.addReferenceAttribute("replyTemplate.authResponseTypeData.freeTransaction", authStep, "freeTransaction");
											}
											sendReplyTemplate(responseStep, replyC3);
											break;
										case AUTH_REQUEST_3_2: // v3.2 Auth
											MessageData_AF replyAF = new MessageData_AF();
											replyAF.setTransactionId(dataAuth.getTransactionId());
											if(deviceInfo.getDeviceType() == DeviceType.ESUDS) {
												responseStep.addPassThruReferenceAttribute("replyTemplate/1.approvedAmount", authStep, "approvedAmount");
												responseStep.addPassThruReferenceAttribute("replyTemplate/1.approvedAmount", authStep, "balanceAmount");
											} else
												responseStep.addReferenceAttribute("replyTemplate.approvedAmount", authStep, "approvedAmount");
											addClientTextAttributes(responseStep, origResponseStep, authStep, "responseMessage", "overrideResponseMessage", 1);
											MessageData_AE dataAE = (MessageData_AE) dataAuth;
											authStep.addByteAttribute(AuthorityAttrEnum.ATTR_ADDITIONAL_INFO_TYPE.getValue(), dataAE.getAdditionalDataType().getValue());
											authStep.addStringAttribute(AuthorityAttrEnum.ATTR_ADDITIONAL_INFO.getValue(), dataAE.getAdditionalData());
											replyAF.setNoConvenienceFee(noConvenienceFee);
											if(deviceInfo.getActionCode() == ServerActionCode.PERFORM_CALL_IN && deviceInfo.getDeviceType() == DeviceType.GX && !enableGxSessionRequest)
												replyAF.setActionCode(AFActionCode.NO_ACTION);
											else
												replyAF.setActionCode(deviceInfo.getActionCode().getAFActionCode());
											responseStep.addReferenceAttribute("replyTemplate.freeTransaction", authStep, "freeTransaction");
											sendReplyTemplate(responseStep, replyAF);
											break;
										case AUTH_REQUEST_3_1: // v3.1 Auth
										case AUTH_REQUEST_3_0: // v3.0 Auth
											// if(serviceType != 2) {
											MessageData_A1 replyA1 = new MessageData_A1();
											replyA1.setTransactionId(dataAuth.getTransactionId());
											if(deviceInfo.getDeviceType() == DeviceType.ESUDS) {
												responseStep.addPassThruReferenceAttribute("replyTemplate/1.approvedAmount", authStep, "approvedAmount");
												responseStep.addPassThruReferenceAttribute("replyTemplate/1.approvedAmount", authStep, "balanceAmount");
											} else
												responseStep.addReferenceAttribute("replyTemplate.approvedAmount", authStep, "approvedAmount");
											sendReplyTemplate(responseStep, replyA1);
											break;
										// }
										// for serviceType == 2, fall through to PERMISSION_REQUEST
										case PERMISSION_REQUEST_3_1: // v3.1 Permission Request
											MessageData_AB replyAB = new MessageData_AB();
											replyAB.setTransactionId(dataAuth.getTransactionId());
											responseStep.addReferenceAttribute("replyTemplate/0.actionCode", authStep, "actionCode");
											responseStep.addReferenceAttribute("replyTemplate/1.actionCodeData.actionBitmap", authStep, "actionCodeBitmap");
											sendReplyTemplate(responseStep, replyAB);
											break;
										case AUTH_REQUEST_2_0: // v2.0 Auth
											MessageData_60 reply60 = new MessageData_60();
											reply60.setTransactionId(dataAuth.getTransactionId());
											sendReplyTemplate(responseStep, reply60);
											break;
										case WS_AUTH_V_3_REQUEST:
										case WS_AUTH_V_3_1_REQUEST:
											MessageData_0301 reply0301 = (MessageData_0301) ((WSRequestMessageData) dataAuth).createResponse();
											responseStep.addPassThruReferenceAttribute("replyTemplate.approvedAmount", authStep, "approvedAmount");
											responseStep.addPassThruReferenceAttribute("replyTemplate.approvedAmount", authStep, "balanceAmount");
											if(requestedAmount.longValue() < amount.longValue())
												responseStep.addLiteralAttribute("replyTemplate/1.originalAmount", amount);
											addClientTextAttributes(responseStep, origResponseStep, authStep, "returnMessage", "overrideResponseMessage", 1);
											sendReplyTemplate(responseStep, reply0301);
											break;
										case WS_CHARGE_V_3_REQUEST:
										case WS_CHARGE_V_3_1_REQUEST:
											reply0301 = (MessageData_0301) ((WSRequestMessageData) dataAuth).createResponse();
											responseStep.addReferenceAttribute("replyTemplate.approvedAmount", authStep, "approvedAmount");
											responseStep.addLiteralAttribute("replyTemplate/1.originalAmount", amount);
											addClientTextAttributes(responseStep, origResponseStep, authStep, "returnMessage", "overrideResponseMessage", 1);
											sendReplyTemplate(responseStep, reply0301);
											break;
										case WS2_AUTH_ENCRYPTED_REQUEST:
										case WS2_AUTH_PLAIN_REQUEST:
											MessageData_030B reply030B = (MessageData_030B) ((WSRequestMessageData) dataAuth).createResponse();
											if(globalAccountId != null)
												reply030B.setCardId(globalAccountId);
											else if(globalAccountIdReferenceStep != null)
												responseStep.addReferenceAttribute("replyTemplate.cardId", globalAccountIdReferenceStep, AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID.getValue());
											reply030B.setCardType(cardType);
											reply030B.setNoConvenienceFee(noConvenienceFee);

											switch(serviceType) {
												case 2: // permission request
													responseStep.addLiteralAttribute("replyTemplate/0.serviceCard", true);
													responseStep.addReferenceAttribute("replyTemplate/0.actionCode", authStep, "actionCode");
													responseStep.addReferenceAttribute("replyTemplate/0.attributes", authStep, "actionAttributes");
													break;
												case 1: // auth request (show balance)
												case 4: // auth request (hidden balance)
													responseStep.addReferenceAttribute("replyTemplate.approvedAmount", authStep, "approvedAmount");
													responseStep.addReferenceAttribute("replyTemplate.balanceAmount", authStep, "balanceAmount");
													if(requestedAmount.longValue() < amount.longValue())
														responseStep.addLiteralAttribute("replyTemplate/1.originalAmount", amount);
													responseStep.addReferenceAttribute("replyTemplate.freeTransaction", authStep, "freeTransaction");
													break;
												case 3: // permission check
													responseStep.addReferenceAttribute("replyTemplate.approvedAmount", authStep, "approvedAmount");
													if(requestedAmount.longValue() < amount.longValue())
														responseStep.addLiteralAttribute("replyTemplate/1.originalAmount", amount);
													responseStep.addReferenceAttribute("replyTemplate.freeTransaction", authStep, "freeTransaction");
													break;
												default:
													throw new ServiceException("Unsupported Service Type " + serviceType);
											}

											responseStep.addReferenceAttribute("replyTemplate.consumerId", authStep, "consumerId");
											responseStep.addReferenceAttribute("replyTemplate.attribute(firstName)", authStep, "firstName");
											responseStep.addReferenceAttribute("replyTemplate.attribute(lastName)", authStep, "lastName");
											responseStep.addReferenceAttribute("replyTemplate.attribute(email)", authStep, "email");
											responseStep.addReferenceAttribute("replyTemplate.attribute(discountAmount)", authStep, "discountAmount");
											responseStep.addReferenceAttribute("replyTemplate.attribute(discountTotal)", authStep, "discountTotalBefore");
											responseStep.addReferenceAttribute("replyTemplate.attribute(sendReceipt)", authStep, "sendReceipt");
											responseStep.addReferenceAttribute("replyTemplate.attribute(mobileNumber)", authStep, "mobileNumber");
											responseStep.addReferenceAttribute("replyTemplate.attribute(mobileEmail)", authStep, "mobileEmail");
											responseStep.addReferenceAttribute("replyTemplate.attribute(commMethod)", authStep, "commMethod");
											addClientTextAttributes(responseStep, origResponseStep, authStep, "returnMessage", "overrideResponseMessage", 1);
											sendReplyTemplate(responseStep, reply030B);
											break;
										case WS2_CHARGE_ENCRYPTED_REQUEST:
										case WS2_CHARGE_PLAIN_REQUEST:
											reply030B = (MessageData_030B) ((WSRequestMessageData) dataAuth).createResponse();
											if(globalAccountId != null)
												reply030B.setCardId(globalAccountId);
											else if(globalAccountIdReferenceStep != null)
												responseStep.addReferenceAttribute("replyTemplate.cardId", globalAccountIdReferenceStep, AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID.getValue());
											reply030B.setCardType(cardType);
											reply030B.setNoConvenienceFee(noConvenienceFee);
											responseStep.addReferenceAttribute("replyTemplate.approvedAmount", authStep, "approvedAmount");
											responseStep.addReferenceAttribute("replyTemplate.balanceAmount", authStep, "balanceAmount");
											responseStep.addReferenceAttribute("replyTemplate.consumerId", authStep, "consumerId");
											responseStep.addReferenceAttribute("replyTemplate.freeTransaction", authStep, "freeTransaction");
											responseStep.addLiteralAttribute("replyTemplate/1.originalAmount", amount);
											responseStep.addReferenceAttribute("replyTemplate.attribute(firstName)", authStep, "firstName");
											responseStep.addReferenceAttribute("replyTemplate.attribute(lastName)", authStep, "lastName");
											responseStep.addReferenceAttribute("replyTemplate.attribute(email)", authStep, "email");
											responseStep.addReferenceAttribute("replyTemplate.attribute(discountAmount)", authStep, "discountAmount");
											responseStep.addReferenceAttribute("replyTemplate.attribute(discountTotal)", authStep, "discountTotalAfter");
											responseStep.addReferenceAttribute("replyTemplate.attribute(adjustedBalanceAmount)", authStep, "adjustedBalanceAmount");
											responseStep.addReferenceAttribute("replyTemplate.attribute(sendReceipt)", authStep, "sendReceipt");
											responseStep.addReferenceAttribute("replyTemplate.attribute(mobileNumber)", authStep, "mobileNumber");
											responseStep.addReferenceAttribute("replyTemplate.attribute(mobileEmail)", authStep, "mobileEmail");
											responseStep.addReferenceAttribute("replyTemplate.attribute(commMethod)", authStep, "commMethod");
											addClientTextAttributes(responseStep, origResponseStep, authStep, "returnMessage", "overrideResponseMessage", 1);
											sendReplyTemplate(responseStep, reply030B);
											break;
										case WS2_REPLENISH_CASH_REQUEST:
										case WS2_REPLENISH_ENCRYPTED_REQUEST:
										case WS2_REPLENISH_PLAIN_REQUEST:
											MessageData_030C reply030C = (MessageData_030C) ((WSRequestMessageData) dataAuth).createResponse();
											if(globalAccountId != null)
												reply030C.setCardId(globalAccountId);
											else if(globalAccountIdReferenceStep != null)
												responseStep.addReferenceAttribute("replyTemplate.cardId", globalAccountIdReferenceStep, AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID.getValue());
											reply030C.setCardType(cardType);
											responseStep.addReferenceAttribute("replyTemplate.approvedAmount", authStep, "approvedAmount");
											responseStep.addReferenceAttribute("replyTemplate.balanceAmount", authStep, "balanceAmount");
											responseStep.addReferenceAttribute("replyTemplate.consumerId", authStep, "consumerId");
											final MessageChainStep replenishStep = stepChain.get(ChainIndex.REPLENISH_STEP);
											responseStep.addReferenceAttribute("replyTemplate/1.authResultCd", replenishStep, "authResultCd");
											responseStep.addReferenceAttribute("replyTemplate.replenishBalanceAmount", replenishStep, "replenishBalanceAmount");
											responseStep.addLiteralAttribute("replyTemplate/2.originalAmount", amount);
											addClientTextAttributes(responseStep, origResponseStep, authStep, "returnMessage", "overrideResponseMessage", 2);
											addClientTextAttributes(responseStep, origResponseStep, replenishStep, "returnMessage", "overrideResponseMessage", 2);
											sendReplyTemplate(responseStep, reply030C);
											break;
										case WS2_GET_CARD_INFO_PLAIN_REQUEST:
										case WS2_GET_CARD_INFO_ENCRYPTED_REQUEST:
											MessageData_030D reply030D = (MessageData_030D) ((WSRequestMessageData) dataAuth).createResponse();
											if(globalAccountId != null)
												reply030D.setCardId(globalAccountId);
											else if(globalAccountIdReferenceStep != null)
												responseStep.addReferenceAttribute("replyTemplate.cardId", globalAccountIdReferenceStep, AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID.getValue());
											reply030D.setCardType(cardType);
											responseStep.addReferenceAttribute("replyTemplate.balanceAmount", authStep, "balanceAmount");
											responseStep.addReferenceAttribute("replyTemplate.consumerId", authStep, "consumerId");
											responseStep.addReferenceAttribute("replyTemplate.attribute(firstName)", authStep, "firstName");
											responseStep.addReferenceAttribute("replyTemplate.attribute(lastName)", authStep, "lastName");
											responseStep.addReferenceAttribute("replyTemplate.attribute(email)", authStep, "email");
											responseStep.addReferenceAttribute("replyTemplate.attribute(discountTotal)", authStep, "discountTotalBefore");
											responseStep.addReferenceAttribute("replyTemplate.attribute(sendReceipt)", authStep, "sendReceipt");
											responseStep.addReferenceAttribute("replyTemplate.attribute(mobileNumber)", authStep, "mobileNumber");
											responseStep.addReferenceAttribute("replyTemplate.attribute(mobileEmail)", authStep, "mobileEmail");
											responseStep.addReferenceAttribute("replyTemplate.attribute(commMethod)", authStep, "commMethod");
											responseStep.addReferenceAttribute("replyTemplate.attribute(sproutAccountId)", authStep, "sproutAccountId");
											addClientTextAttributes(responseStep, origResponseStep, authStep, "returnMessage", "overrideResponseMessage", 1);
											sendReplyTemplate(responseStep, reply030D);
											break;
										case WS2_TOKENIZE_PLAIN_REQUEST:
										case WS2_TOKENIZE_ENCRYPTED_REQUEST:
											authStep.setAttribute(AuthorityAttrEnum.ATTR_BALANCE_CHECK_ONLY, true);
											MessageData_0310 reply0310 = ((WS2TokenizeMessageData) dataAuth).createResponse();
											if(globalAccountId != null)
												reply0310.setCardId(globalAccountId);
											else if(globalAccountIdReferenceStep != null)
												responseStep.addReferenceAttribute("replyTemplate.cardId", globalAccountIdReferenceStep, AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID.getValue());
											reply0310.setCardType(cardType);
											responseStep.addReferenceAttribute("replyTemplate.consumerId", authStep, "consumerId");
											addClientTextAttributes(responseStep, origResponseStep, authStep, "returnMessage", "overrideResponseMessage", 1);
											sendReplyTemplate(responseStep, reply0310);
											break;
										default:
									}
								}

								switch(deviceInfo.getDeviceType()) {
									case KIOSK:
									case VIRTUAL:
										if(dataAuth instanceof WS2RequestMessageData) {
											authStep.setAttribute(AuthorityAttrEnum.ATTR_ALLOW_CVV_MISMATCH, !ConvertUtils.getBooleanSafely(((WS2RequestMessageData) dataAuth).getAttribute("requireCVVMatch"), true));
											authStep.setAttribute(AuthorityAttrEnum.ATTR_ALLOW_AVS_MISMATCH, !ConvertUtils.getBooleanSafely(((WS2RequestMessageData) dataAuth).getAttribute("requireAVSMatch"), true));
										} else if(dataAuth instanceof WSRequestMessageData) {
											authStep.setAttribute(AuthorityAttrEnum.ATTR_ALLOW_CVV_MISMATCH, false);
											authStep.setAttribute(AuthorityAttrEnum.ATTR_ALLOW_AVS_MISMATCH, false);
										}
										break;
								}

								saveStep.addReferenceAttribute("authActionId", authStep, "actionId");
								saveStep.addReferenceAttribute("authActionBitmap", authStep, "actionCodeBitmap");

								// Add to auth step
								switch(entryType) {
									case MANUAL:
									case TOKEN:
										addCardInfoToStep(parsedCardInfo, authStep, false);
										break;
									default:
										// if(handling != AccountHandling.DENY_ON_NO_ACCOUNT_ID) {
											addCardInfoToStep(parsedCardInfo, authStep, false);
											authStep.setSecretAttribute(AuthorityAttrEnum.ATTR_TRACK_DATA, accountDatas[i]);
										// }
								}

								if (dataAuth.getCardReaderType() != null)
									authStep.setAttribute(CommonAttrEnum.ATTR_CARD_READER_TYPE_ID, dataAuth.getCardReaderType().getValue());

								if(dataAuth.getPin() != null && dataAuth.getPin().length() > 0) {
									authStep.addSecretAttribute("pin", dataAuth.getPin());
								}
								authStep.addStringAttribute("authorityName", authorityName);
								authStep.addStringAttribute("remoteServerAddr", results.getValue("remoteServerAddr", String.class));
								authStep.addStringAttribute("remoteServerAddrAlt", results.getValue("remoteServerAddrAlt", String.class));
								authStep.addStringAttribute("remoteServerPort", results.getValue("remoteServerPort", String.class));

								authStep.addStringAttribute("merchantCd", results.getValue("merchantCd", String.class));
								authStep.addStringAttribute("terminalCd", results.getValue("terminalCd", String.class));
								authStep.addSecretAttribute("terminalEncryptKey", results.getValue("terminalEncryptKey", String.class));
								authStep.addSecretAttribute("terminalEncryptKey2", results.getValue("terminalEncryptKey2", String.class));

								authStep.addStringAttribute("amount", requestedAmount.toString());
								authStep.addStringAttribute("originalAmount", amount.toString());

								authStep.addIntAttribute("minorCurrencyFactor", minorCurrencyFactor);
								authStep.addIntAttribute("serviceType", serviceType);
								authStep.addByteAttribute("entryType", (byte) entryType.getValue());
								authStep.addSecretAttribute("validationData", dataAuth.getValidationData());

								authStep.addStringAttribute("online", "true");
								authStep.addStringAttribute("actionType", AuthorityAction.AUTHORIZATION.toString());
								authStep.addIntAttribute("responseTimeout", getGatewayTimeout());
								authStep.addLongAttribute("traceNumber", traceNumber);
								authStep.addLongAttribute("authTime", authTime);
								authStep.addStringAttribute("timeZoneGuid", timeZoneGuid);
								authStep.addLongAttribute("posPtaId", posPtaId);
								authStep.addStringAttribute("currencyCd", currencyCd);
								authStep.addStringAttribute("globalEventCd", globalEventCd);
								authStep.setAttribute(AuthorityAttrEnum.ATTR_PARTIAL_AUTH_ALLOWED, allowPartialAuth);
								
								authStep.setAttribute(CommonAttrEnum.ATTR_LANGUAGE, language);

								authStep.setAttribute(AuthorityAttrEnum.ATTR_POS_ENVIRONMENT, posEnvironment);
								authStep.setAttribute(AuthorityAttrEnum.ATTR_ENTRY_CAPABILITY, results.getValue("entryCapability", String.class));
								authStep.setAttribute(AuthorityAttrEnum.ATTR_PIN_CAPABILITY, results.getValue("pinCapability", Boolean.class));
								authStep.setAttribute(AuthorityAttrEnum.ATTR_MERCHANT_CATEGORY_CODE, results.getValue("mcc", Short.class));
								authStep.setAttribute(AuthorityAttrEnum.ATTR_DOING_BUSINESS_AS, results.getValue("doingBusinessAs", String.class));
								String address = results.getValue("address", String.class);
								String city = results.getValue("city", String.class);
								String stateCd = results.getValue("stateCd", String.class);
								String postal = results.getValue("postal", String.class);
								String countryCd = results.getValue("countryCd", String.class);
								if(StringUtils.isBlank(city) && StringUtils.isBlank(postal)) {
									address = InteractionUtils.getDefaultTerminalAddress();
									city = InteractionUtils.getDefaultTerminalCity();
									stateCd = InteractionUtils.getDefaultTerminalStateCd();
									postal = InteractionUtils.getDefaultTerminalPostal();
									countryCd = InteractionUtils.getDefaultTerminalCountryCd();
								}
								authStep.setAttribute(AuthorityAttrEnum.ATTR_ADDRESS, address);
								authStep.setAttribute(AuthorityAttrEnum.ATTR_CITY, city);
								authStep.setAttribute(AuthorityAttrEnum.ATTR_STATE_CD, stateCd);
								authStep.setAttribute(AuthorityAttrEnum.ATTR_POSTAL, postal);
								authStep.setAttribute(AuthorityAttrEnum.ATTR_COUNTRY_CD, countryCd);
								authStep.setAttribute(AuthorityAttrEnum.ATTR_CUSTOMER_SERVICE_PHONE, SystemUtils.nvl(results.getValue("csPhone", String.class), InteractionUtils.getDefaultCustomerServicePhone()));
								authStep.setAttribute(AuthorityAttrEnum.ATTR_CUSTOMER_SERVICE_EMAIL, SystemUtils.nvl(results.getValue("csEmail", String.class), InteractionUtils.getDefaultCustomerServiceEmail()));
								authStep.setAttribute(AuthorityAttrEnum.ATTR_CUSTOMER_ID, results.getValue("customerId", String.class));

								authStep.setAttribute(MessageAttrEnum.ATTR_DEVICE_NAME, deviceInfo.getDeviceName());
								authStep.setAttribute(AuthorityAttrEnum.ATTR_DEVICE_SERIAL_CD, deviceInfo.getDeviceSerialCd());
								authStep.setAttribute(AuthorityAttrEnum.ATTR_DEVICE_TRAN_CD, dataAuth.getTransactionId());
								if(!StringUtils.isBlank(invoiceNumber))
									authStep.setAttribute(AuthorityAttrEnum.ATTR_INVOICE_NUMBER, invoiceNumber);
								
								if(dataAuth.getPaymentActionType() == PaymentActionType.BALANCE_CHECK)
									authStep.setAttribute(AuthorityAttrEnum.ATTR_BALANCE_CHECK_ONLY, true);

								if(globalAccountId != null) {
									authStep.setAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, globalAccountId);
									authStep.setAttribute(AuthorityAttrEnum.ATTR_INSTANCE, globalAccountInstance);
									authStep.setAttribute(AuthorityAttrEnum.ATTR_TRACK_CRC_MATCH, trackCrcMatch);
									if(consumerAcctId != null) {
										authStep.setAttribute(AuthorityAttrEnum.ATTR_CONSUMER_ACCT_ID, consumerAcctId);
										saveStep.setAttribute(AuthorityAttrEnum.ATTR_CONSUMER_ACCT_ID, consumerAcctId);
									}
									if(loyaltyToken != null)
										authStep.setAttribute(AuthorityAttrEnum.ATTR_CONSUMER_ACCT_TOKEN, loyaltyToken);
								}
								

								if(longitude != null)
									authStep.setAttribute(AuthorityAttrEnum.ATTR_LONGITUDE, longitude);
								if(latitude != null)
									authStep.setAttribute(AuthorityAttrEnum.ATTR_LATITUDE, latitude);
								
								if(consumerEmail!=null){
									authStep.setAttribute(AuthorityAttrEnum.ATTR_CONSUMER_EMAIL, consumerEmail);
								}
								
								if(sproutVendorId!=null){
									authStep.setAttribute(AuthorityAttrEnum.ATTR_SPROUT_VENDOR_ID, sproutVendorId);
								}

								// Add to saveStep
								saveStep.addReferenceAttribute("authResultCd", authStep, "authResultCd");
								saveStep.addReferenceAttribute("approvedAmount", authStep, "approvedAmount");
								saveStep.addReferenceAttribute("balanceAmount", authStep, "balanceAmount");
								saveStep.addReferenceAttribute("deniedReason", authStep, "deniedReason");
								saveStep.addReferenceAttribute("authorityRespCd", authStep, "authorityRespCd");
								saveStep.addReferenceAttribute("authorityRespDesc", authStep, "authorityRespDesc");
								saveStep.addReferenceAttribute("authAuthorityTranCd", authStep, "authAuthorityTranCd");
								saveStep.addReferenceAttribute("authAuthorityRefCd", authStep, "authAuthorityRefCd");
								saveStep.addReferenceAttribute("authAuthorityTs", authStep, "authAuthorityTs");
								saveStep.addReferenceAttribute("authAuthorityMiscData", authStep, "authAuthorityMiscData");
								saveStep.addReferenceAttribute("consumerAcctId", authStep, "consumerAcctId");
								saveStep.addReferenceAttribute("authHoldUsed", authStep, "authHoldUsed");
								saveStep.addReferenceAttribute("prepaidFlag", authStep, "prepaidFlag");
								saveStep.addStringAttribute("invalidEventId", "N");
								saveStep.addBooleanAttribute("passThru", allowPassThru);
								saveStep.setAttribute(AuthorityAttrEnum.ATTR_CARD_TYPE, cardType);
								saveStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_EMV_PARAMETER_DNLD_REQD_TIMESTAMP, authStep, AuthorityAttrEnum.ATTR_EMV_PARAMETER_DNLD_REQD_TIMESTAMP);
								
								saveStep.addStringAttribute("merchantCd", results.getValue("merchantCd", String.class));
								saveStep.addStringAttribute("terminalCd", results.getValue("terminalCd", String.class));

								// Add to statsStep
								statsStep.addIntAttribute("minorCurrencyFactor", minorCurrencyFactor);
								statsStep.addStringAttribute("authAmount", amount.toString());
								statsStep.addReferenceAttribute("authorityStartTime", authStep, "startTime");
								statsStep.addReferenceAttribute("authorityEndTime", authStep, "endTime");

								if(dataAuth.getPaymentActionType() == PaymentActionType.TOKENIZE) {
									// Need to not do store but do create token instead
									if(storeStep == null) {
										storeStep = requestStep.addStep(getCreateTokenQueueKey());
										stepChain.put(ChainIndex.REPLENISH_STEP, storeStep);
										addCardInfoToStoreStep(parsedCardInfo, storeStep);
										storeStep.setReferenceIndexedAttribute(CommonAttrEnum.ATTR_DECRYPTED_DATA_OVERRIDE, PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER.getStoreIndex(), authStep, AuthorityAttrEnum.ATTR_AUTH_ACCOUNT_DATA);
										storeStep.setAttribute(AuthorityAttrEnum.ATTR_STORE_EXPIRATION_TIME, targetExpirationTime);
										storeStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, authStep, AuthorityAttrEnum.ATTR_AUTH_RESULT_CD);
									} else {
										storeStep.setQueueKey(getCreateTokenQueueKey());
										stepChain.remove(ChainIndex.STORE_STEP);
										stepChain.put(ChainIndex.REPLENISH_STEP, storeStep);
									}
									if(accountCdHash == null)
										accountCdHash = requestStep.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_ACCOUNT_CD_HASH, indexes[i], byte[].class, false);
									storeStep.setAttribute(AuthorityAttrEnum.ATTR_ACCOUNT_CD_HASH, accountCdHash); // this is so we always UPSERT account in km
									storeStep.setAttribute(MessageAttrEnum.ATTR_DEVICE_NAME, deviceInfo.getDeviceName());
									storeStep.setAttribute(AuthorityAttrEnum.ATTR_DEVICE_TRAN_CD, dataAuth.getTransactionId());
									if(globalAccountId != null) {
										storeStep.setAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, globalAccountId);
										storeStep.setAttribute(AuthorityAttrEnum.ATTR_INSTANCE, globalAccountInstance);
									} else {
										// if this is a pass-through check global_account_id of previous auth step
										if(globalAccountIdReferenceStep != null) {
											storeStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, globalAccountIdReferenceStep, AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID);
											storeStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_INSTANCE, globalAccountIdReferenceStep, AuthorityAttrEnum.ATTR_INSTANCE);
										}
									}
									// add token indicator
									if(responseStep != null) {
										responseStep.addReferenceAttribute("replyTemplate.tokenBytes", storeStep, AuthorityAttrEnum.ATTR_AUTH_TOKEN.getValue());
										if(globalAccountId == null && globalAccountIdReferenceStep == null)
											responseStep.addReferenceAttribute("replyTemplate.cardId", storeStep, AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID.getValue());
									}
								}

								if(storeStep != null) {
									storeStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_AUTH_HOLD_USED, authStep, AuthorityAttrEnum.ATTR_AUTH_HOLD_USED);
									for(Attribute a : new Attribute[] {AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, AuthorityAttrEnum.ATTR_INSTANCE, AuthorityAttrEnum.ATTR_CARD_KEY}) {
										if(saveStep.getAttribute(a, Object.class, false) == null)
											saveStep.setReferenceAttribute(a, storeStep, a);	
									}
									MessageChainStep updateStep = stepChain.get(ChainIndex.UPDATE_STEP);	
									if(updateStep != null) {
										if(globalAccountId != null) {
											updateStep.setAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, globalAccountId);
											updateStep.setAttribute(AuthorityAttrEnum.ATTR_INSTANCE, globalAccountInstance);
										} else {
											updateStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, storeStep, AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID);
											updateStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_INSTANCE, storeStep, AuthorityAttrEnum.ATTR_INSTANCE);
										}
										updateStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_CARD_KEY, storeStep, AuthorityAttrEnum.ATTR_CARD_KEY);
									}
								}
								setupChain(stepChain, currentStep, resultCodes);

								if(allowPassThru) {
									if(log.isDebugEnabled())
										log.debug("Looking for next auth for pass thru functionality");

									CHAIN: {
										while(results.next()) {
											if(checkAndProcessMatch(requestStep, authStep, origResponseStep, saveStep, FAILURE_RESULT_CODES, results, message, deviceInfo, dataAuth, authTime, accountDatas, indexes, startTime, checkState, null, globalAccountIdReferenceStep, entryType, appSessionKey)) {
												break CHAIN;
											}
										}
										saveStep.addBooleanAttribute("passThru", false); // no other authorities to try so use passThru = false
									}
								}

								if(message.getLog().isInfoEnabled())
									message.getLog().info("Routing Auth for '" + globalEventCd + "' to " + authorityName + " at '" + authStep.getQueueKey() + "' (pos_pta_id=" + posPtaId + ") with attributes: " + authStep.getAttributes());
								return true;
							}
						}
					}
					break;
				case INVALID_TRANSACTION_ID:
					if(responseStep != null)
						responseStep.addLiteralAttribute("authResultCd", AuthResultCode.FAILED);
					saveStep.addStringAttribute("authResultCd", "F");
					saveStep.setAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.DATA_FROM_CLIENT);
					saveStep.addStringAttribute("authorityRespCd", "INVALID_TRANSACTION_ID");
					saveStep.addStringAttribute("authorityRespDesc", failureMessage);
					saveStep.addStringAttribute("invalidEventId", "Y");
					break;
			}
			setupChain(stepChain, currentStep, resultCodes);
			return true;
		}
		return false;
	}

	/**
	 * Create a string that says the number of days, hours, minutes, seconds, or milliseconds. Show 2 units unless 0
	 * 
	 * @param milliseconds
	 * @return
	 */
	protected static String formatInterval(long milliseconds) {
		return ConvertUtils.formatObject(milliseconds, "simple.text.IntervalFormat");
	}

	protected void updateCardInfo(Map<PaymentMaskBRef, Object> parsedCardInfo, Map<String, String> attributes, String attributeName, PaymentMaskBRef paymentMaskBref) {
		if(parsedCardInfo.get(paymentMaskBref) == null) {
			String value = attributes.get(attributeName);
			if(!StringUtils.isBlank(value))
				parsedCardInfo.put(paymentMaskBref, value);
		}
	}

	protected void setupChain(Map<ChainIndex, MessageChainStep> stepChain, MessageChainStep currentStep, int[] resultCodes) {
		Iterator<Map.Entry<ChainIndex, MessageChainStep>> iter = stepChain.entrySet().iterator(); // NOTE: This iterator returns itself on next() and thus we must copy
		// the key and value
		if(!iter.hasNext())
			return;
		Map.Entry<ChainIndex, MessageChainStep> entry = iter.next();
		ChainIndex prevIndex = entry.getKey();
		MessageChainStep prevStep = entry.getValue();
		for(int rc : resultCodes)
				currentStep.setNextSteps(rc, prevStep);
		int[] pendingCodes = null;
		MessageChainStep pendingStep = null;
		while(iter.hasNext()) {
			entry = iter.next();
			if(pendingStep != null) {
				for(int rc : pendingCodes)
						pendingStep.setNextSteps(rc, entry.getValue());
				pendingStep = null;
				pendingCodes = null;
			}
			for(int rc : prevIndex.getSuccessResultCodes())
					prevStep.setNextSteps(rc, entry.getValue());
			if(iter.hasNext()) {
				if(prevIndex.getFailureResultCodes() != null && prevIndex.getFailureResultCodes().length > 0) {
					// skip to next on failures
					if(iter.hasNext() && entry.getKey().ordinal() - prevIndex.ordinal() == 1) {
						pendingCodes = prevIndex.getFailureResultCodes();
						pendingStep = prevStep;
					} else
						for(int rc : prevIndex.getFailureResultCodes())
								prevStep.setNextSteps(rc, entry.getValue());
				}

				prevIndex = entry.getKey();
				prevStep = entry.getValue();
			}
		}
	}

	protected void addClientTextAttributes(MessageChainStep responseStep, MessageChainStep origResponseStep, MessageChainStep authStep, String attributeName, String overrideAttributeName, int overrideIndex) {
		StringBuilder sb = new StringBuilder((overrideAttributeName == null ? attributeName.length() : overrideAttributeName.length()) + 17);
		sb.append("replyTemplate.~").append(attributeName);
		responseStep.addPassThruReferenceAttribute(sb.toString(), authStep, "~clientText");
		sb.setCharAt(14, '@');
		responseStep.addPassThruReferenceAttribute(sb.toString(), authStep, "@clientText");
		if(overrideAttributeName == null)
			return;
		sb.setLength(13);
		sb.append('/').append(overrideIndex).append(".~");
		int pos = sb.length();
		sb.append(overrideAttributeName);
		responseStep.addPassThruReferenceAttribute(sb.toString(), authStep, "~overrideClientText");
		if(responseStep != origResponseStep && origResponseStep != null)
			origResponseStep.addPassThruReferenceAttribute(sb.toString(), authStep, "~overrideClientText");
		sb.setCharAt(pos - 1, '@');
		responseStep.addPassThruReferenceAttribute(sb.toString(), authStep, "@overrideClientText");
		if(responseStep != origResponseStep && origResponseStep != null)
			origResponseStep.addPassThruReferenceAttribute(sb.toString(), authStep, "@overrideClientText");
	}

	protected static long nextTraceNumber() {
		long base = traceNumberGenerator.getAndAdd(10L);
		base -= (base % 10);
		if(base < 0)
			base -= getTraceNumberOffset();
		else
			base += getTraceNumberOffset();
		return base;
	}

	public static int getTraceNumberOffset() {
		return traceNumberOffset;
	}

	public static void setTraceNumberOffset(int traceNumberOffset) {
		AuthorizeTask.traceNumberOffset = traceNumberOffset;
	}

	public String getRecordAuthQueueKey() {
		return recordAuthQueueKey;
	}

	public void setRecordAuthQueueKey(String recordAuthQueueKey) {
		this.recordAuthQueueKey = recordAuthQueueKey;
	}

	public String getCancelAuthQueueKey() {
		return cancelAuthQueueKey;
	}

	public void setCancelAuthQueueKey(String cancelAuthQueueKey) {
		this.cancelAuthQueueKey = cancelAuthQueueKey;
	}

	public String getCreatePTAQueueKey() {
		return createPTAQueueKey;
	}

	public void setCreatePTAQueueKey(String createPTAQueueKey) {
		this.createPTAQueueKey = createPTAQueueKey;
	}
	
	public static String getDeclinedDebitGatewayExpression() {
		return declinedDebitGatewayExpression.pattern();
	}

	public static void setDeclinedDebitGatewayExpression(String declinedDebitGatewayExpression) {
		AuthorizeTask.declinedDebitGatewayExpression = Pattern.compile(declinedDebitGatewayExpression);
	}

	public int getGatewayTimeout() {
		return gatewayTimeout;
	}

	public void setGatewayTimeout(int gatewayTimeout) {
		this.gatewayTimeout = gatewayTimeout;
	}

	public String getAuthorityQueueKeyPrefix() {
		return authorityQueueKeyPrefix;
	}

	public void setAuthorityQueueKeyPrefix(String authorityQueueKeyPrefix) {
		this.authorityQueueKeyPrefix = authorityQueueKeyPrefix;
	}

	public TranslatorFactory getTranslatorFactory() {
		return translatorFactory;
	}

	public void setTranslatorFactory(TranslatorFactory translatorFactory) {
		this.translatorFactory = translatorFactory;
	}

	public String getTranslatorContext() {
		return translatorContext;
	}

	public void setTranslatorContext(String translatorContext) {
		this.translatorContext = translatorContext;
	}

	public int getMaxResponseSize() {
		return maxResponseSize;
	}

	public void setMaxResponseSize(int maxResponseSize) {
		this.maxResponseSize = maxResponseSize;
	}

	public String getRecordStatsQueueKey() {
		return recordStatsQueueKey;
	}

	public void setRecordStatsQueueKey(String recordStatsQueueKey) {
		this.recordStatsQueueKey = recordStatsQueueKey;
	}

	public AppLayerDeviceInfoManager getDeviceInfoManager() {
		return deviceInfoManager;
	}

	public void setDeviceInfoManager(AppLayerDeviceInfoManager deviceInfoManager) {
		this.deviceInfoManager = deviceInfoManager;
	}
	public Boolean getCloseAfterAuth(int deviceTypeId) throws InvalidByteValueException {
		return closeAfterAuth.get(DeviceType.getByValue((byte)deviceTypeId));
	}
	public void setCloseAfterAuth(int deviceTypeId, Boolean value) throws InvalidByteValueException {
		if(value == null)
			closeAfterAuth.remove(DeviceType.getByValue((byte)deviceTypeId));
		else
			closeAfterAuth.put(DeviceType.getByValue((byte)deviceTypeId), value);
	}

	public String getStoreCardQueueKey() {
		return storeCardQueueKey;
	}

	public void setStoreCardQueueKey(String storeCardQueueKey) {
		this.storeCardQueueKey = storeCardQueueKey;
	}

	public String getSaleQueueKey() {
		return saleQueueKey;
	}

	public void setSaleQueueKey(String saleQueueKey) {
		this.saleQueueKey = saleQueueKey;
	}

	public Attribute[] getSaleAttributes() {
		return saleAttributes;
	}

	public void setSaleAttributes(Attribute[] saleAttributes) {
		this.saleAttributes = saleAttributes;
	}

	public Set<Integer> getReplenishItemIds() {
		return replenishItemIds;
	}

	public int getCardRetentionDays() {
		return cardRetentionDays;
	}

	public void setCardRetentionDays(int cardRetentionDays) {
		this.cardRetentionDays = cardRetentionDays;
	}

	public boolean isEnableGxSessionRequest() {
		return enableGxSessionRequest;
	}

	public void setEnableGxSessionRequest(boolean enableGxSessionRequest) {
		this.enableGxSessionRequest = enableGxSessionRequest;
	}

	public String getReplenishQueueKey() {
		return replenishQueueKey;
	}

	public void setReplenishQueueKey(String replenishQueueKey) {
		this.replenishQueueKey = replenishQueueKey;
	}

	public String getUpdateAuthQueueKey() {
		return updateAuthQueueKey;
	}

	public void setUpdateAuthQueueKey(String updateAuthQueueKey) {
		this.updateAuthQueueKey = updateAuthQueueKey;
	}

	public String getCreateTokenQueueKey() {
		return createTokenQueueKey;
	}

	public void setCreateTokenQueueKey(String createTokenQueueKey) {
		this.createTokenQueueKey = createTokenQueueKey;
	}
	
	public static String getGoogleWalletCardNumberExpression() {
		return googleWalletCardNumberExpression.pattern();
	}

	public static void setGoogleWalletCardNumberExpression(String googleWalletCardNumberExpression) {
		AuthorizeTask.googleWalletCardNumberExpression = Pattern.compile(googleWalletCardNumberExpression);
	}

	public static String getDenyDebitExpression() {
		return denyDebitExpression.pattern();
	}

	public static void setDenyDebitExpression(String denyDebitExpression) {
		AuthorizeTask.denyDebitExpression = Pattern.compile(denyDebitExpression);
	}

	public String[] getAllowedGeocodeCountries(String authorityName) {
		Set<String> set = allowedGeocodeCountries.get(authorityName);
		return set == null ? null : set.toArray(new String[set.size()]);
	}

	public void setAllowedGeocodeCountries(String authorityName, String... countryCds) {
		if(countryCds == null || countryCds.length == 0) {
			allowedGeocodeCountries.remove(authorityName);
			return;
		}
		Set<String> set = allowedGeocodeCountries.get(authorityName);
		if(set == null) {
			set = new LinkedHashSet<String>();
			allowedGeocodeCountries.put(authorityName, set);
		} else
			set.clear();
		for(String cc : countryCds)
			set.add(cc.toUpperCase());
	}

	public long getMaxGeocodeStaleness() {
		return maxGeocodeStaleness;
	}

	public void setMaxGeocodeStaleness(long maxGeocodeStaleness) {
		this.maxGeocodeStaleness = maxGeocodeStaleness;
	}

	public int[] getAllowedGeocodeOEMIntegrities() {
		int[] array = new int[allowedGeocodeOEMIntegrities.size()];
		Iterator<Integer> iter = allowedGeocodeOEMIntegrities.iterator();
		for(int i = 0; i < array.length && iter.hasNext(); i++)
			array[i] = iter.next();
		return array;
	}

	public void setAllowedGeocodeOEMIntegrities(int... allowedGeocodeOEMIntegrity) {
		allowedGeocodeOEMIntegrities.clear();
		if(allowedGeocodeOEMIntegrity != null)
			for(int value : allowedGeocodeOEMIntegrity)
				allowedGeocodeOEMIntegrities.add(value);
	}

	public int[] getAllowedGeocodeProviders() {
		int[] array = new int[allowedGeocodeProviders.size()];
		Iterator<Integer> iter = allowedGeocodeProviders.iterator();
		for(int i = 0; i < array.length && iter.hasNext(); i++)
			array[i] = iter.next();
		return array;
	}

	public void setAllowedGeocodeProviders(int... allowedGeocodeProvider) {
		allowedGeocodeProviders.clear();
		if(allowedGeocodeProvider != null)
			for(int value : allowedGeocodeProvider)
				allowedGeocodeProviders.add(value);
	}
}
