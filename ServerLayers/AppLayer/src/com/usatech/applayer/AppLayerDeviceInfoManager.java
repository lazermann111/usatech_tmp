/**
 *
 */
package com.usatech.applayer;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TimeZone;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;

import simple.app.DatabasePrerequisite;
import simple.app.Publisher;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.NotEnoughRowsException;
import simple.io.ByteInput;
import simple.io.IOUtils;
import simple.io.Log;
import simple.lang.InterruptGuard;
import simple.lang.InvalidValueException;
import simple.lang.SystemUtils;
import simple.results.BeanException;
import simple.results.Results;
import simple.security.crypt.EncryptionInfoMapping;
import simple.text.StringUtils;
import simple.util.DynaBeanMap;
import simple.util.MapBackedSet;
import simple.util.concurrent.AbstractFutureCache;
import simple.util.concurrent.Bottleneck;
import simple.util.concurrent.LockSegmentFutureCache;
import simple.util.concurrent.SingleLockBottleneck;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.AppLayerDeviceInfo;
import com.usatech.layers.common.BasicDeviceInfo;
import com.usatech.layers.common.DeviceInfo;
import com.usatech.layers.common.DeviceInfoManager;
import com.usatech.layers.common.DeviceInfoUpdateListener;
import com.usatech.layers.common.InterruptGuardedDeviceInfo;
import com.usatech.layers.common.NoUpdateDeviceInfo;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.ActivationStatus;
import com.usatech.layers.common.constants.DeviceInfoProperty;
import com.usatech.layers.common.constants.DeviceProperty;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.ServerActionCode;

/**
 * @author Brian S. Krug
 *
 */
public class AppLayerDeviceInfoManager implements DeviceInfoManager {
	private static final Log log = Log.getLog();
	protected String storeQueueKey;
	protected Publisher<ByteInput> publisher;
	protected EncryptionInfoMapping encryptionInfoMapping;
	protected final Set<DeviceInfoProperty> temporalDeviceInfoProperties = new HashSet<DeviceInfoProperty>(); // those which when changed do NOT cause a save to App db
	protected DeviceInfoProperty[] netLayerSensitiveProperties = new DeviceInfoProperty[] {
			DeviceInfoProperty.ENCRYPTION_KEY, DeviceInfoProperty.NEW_PASSWORD_HASH, DeviceInfoProperty.NEW_PASSWORD_SALT, DeviceInfoProperty.NEW_USERNAME,
			DeviceInfoProperty.PASSWORD_HASH, DeviceInfoProperty.PASSWORD_SALT, DeviceInfoProperty.USERNAME
	};
	protected boolean initiallyRefreshedFromSource;
	protected int maxTemporalSaveCount = 0;
	protected String[] keyReceiverQueueKeys;
	protected static final Bottleneck<PSDAltKey> psdBottleneck = new SingleLockBottleneck<PSDAltKey>();
	protected static final Bottleneck<String> ptasBottleneck = new SingleLockBottleneck<String>();
	protected Set<Long> blacklistedGlobalAccountIds = new MapBackedSet<>(new ConcurrentHashMap<Long, Object>(64, 0.75f, 1));
	protected static final Bottleneck<Long> blacklistBottleneck = new SingleLockBottleneck<Long>();
	protected static final Bottleneck<String> deviceDetailBottleneck = new SingleLockBottleneck<String>();// we may need ConcurrentBottleneck
	protected final AtomicLong deviceDetailRecheckTime = new AtomicLong(0);

	//One entry in deviceInfos uses about 1500 bytes of memory, so we can accommodate a whole lot of these
	protected final AbstractFutureCache<String, AppLayerDeviceInfo> deviceInfos = new LockSegmentFutureCache<String, AppLayerDeviceInfo>(0, 1024, 0.75f, 32) {			
		@Override
		protected AppLayerDeviceInfo createValue(String deviceName, Object... additionalInfo) throws Exception {
			AppLayerDeviceInfo deviceInfo = retrieveDeviceInfoFromCall(deviceName, "GET_DEVICE_INFO");
			boolean createNew = (additionalInfo != null && additionalInfo.length > 0 && additionalInfo[0] instanceof Boolean && ((Boolean) additionalInfo[0]));
			boolean refreshFromSource = (additionalInfo != null && additionalInfo.length > 1 && additionalInfo[1] instanceof Boolean && ((Boolean) additionalInfo[1]));
			if(deviceInfo == null) {
				deviceInfo = new InnerDeviceInfo(deviceName);
				refreshFromSource = true;
			}
			if(refreshFromSource || isInitiallyRefreshedFromSource()) {
				try {
					AppLayerDeviceInfo newDeviceInfo = (AppLayerDeviceInfo) checkDeviceInfo(deviceInfo, deviceName, refreshFromSource, refreshFromSource);
					if(!createNew && newDeviceInfo == null)
						throw new NoSuchElementException("Device '" + deviceName + "' is not registered in the OPER database");
					if(newDeviceInfo != null)
						deviceInfo = newDeviceInfo;
				} catch(ServiceException e) {
					if(refreshFromSource)
						throw e;
					log.warn("Could not refresh device info for '" + deviceName + "'", e);
				}
			}
			return deviceInfo;
		}
	};
	protected static final DeviceInfo LOAD_BALANCER_DEVICE_INFO = new NoUpdateDeviceInfo(ProcessingConstants.LOAD_BALANCER_MACHINE_ID, null, null, null, 0, 0, DeviceType.DEFAULT, null, false, ProcessingConstants.US_ASCII_CHARSET, ActivationStatus.NOT_ACTIVATED, TimeZone.getDefault().getID(), ServerActionCode.NO_ACTION, null, null, null, null, 0, null, null, null, null, null, null, 0, null, false);
	protected static final InterruptGuard FAUX_GUARD = new InterruptGuard() {		
		@Override
		public boolean setInterruptible(boolean interruptible) {
			return true;
		}		
		@Override
		public boolean getInterruptible() {
			return true;
		}
	};
	protected class InnerInterruptGuardDeviceInfo extends InterruptGuardedDeviceInfo implements AppLayerDeviceInfo {
		protected final AppLayerDeviceInfo delegate;
		public InnerInterruptGuardDeviceInfo(AppLayerDeviceInfo delegate, InterruptGuard guard) {
			super(delegate, guard);
			this.delegate = delegate;
		}
		public void addDeviceInfoListener(DeviceInfoUpdateListener listener) {
			delegate.addDeviceInfoListener(listener);
		}
		public void removeDeviceInfoListener(DeviceInfoUpdateListener listener) {
			delegate.removeDeviceInfoListener(listener);
		}	
	}
	protected class InnerDeviceInfo extends BasicDeviceInfo implements AppLayerDeviceInfo {
		protected final Set<DeviceInfoUpdateListener> listeners = Collections.newSetFromMap(new WeakHashMap<DeviceInfoUpdateListener, Boolean>());
		protected final ReentrantLock lock = new ReentrantLock();
		protected int saveCount = 0;
		public InnerDeviceInfo(String deviceName,Map<String, Object> attributes, Map<String,Long> timestamps) throws ServiceException {
			super(deviceName);
			acceptChanges(attributes, timestamps);
		}
		/**
		 * @param deviceName
		 */
		public InnerDeviceInfo(String deviceName) {
			super(deviceName);
		}

		public void addDeviceInfoListener(DeviceInfoUpdateListener listener) {
			listeners.add(listener);
		}
		public void removeDeviceInfoListener(DeviceInfoUpdateListener listener) {
			listeners.remove(listener);
		}

		/**
		 * @see com.usatech.layers.common.BasicDeviceInfo#processChanges(java.util.Map, long, boolean)
		 */
		@Override
		protected void processChanges(Map<String, Object> changes, Map<String,Long> timestamps, boolean internalOnly, Connection targetConn) throws ServiceException {
			// Save in local db
			if(saveCount < getMaxTemporalSaveCount() || !isTemporal(changes)) {
				saveChanges(this, targetConn);
				if(saveCount < Integer.MAX_VALUE)
					saveCount++;
			}
			if(!internalOnly) {
				if(!isInitOnly()) {
					if(getDeviceType().getValue() >= DeviceType.EDGE.getValue() && changes.containsKey(DeviceInfoProperty.ACTIVATION_STATUS.getAttributeKey())) {
						// update oracle
						queueSettingsUpdate(getDeviceName(), Collections.singletonMap(String.valueOf(DeviceProperty.ACTIVATION_STATUS.getValue()), String.valueOf(getActivationStatus().getValue())));
					}
					if(isNetLayerSensitive(changes)) {
						sendToNetLayer(this, changes.keySet());
					}
				}
			}
			for(DeviceInfoUpdateListener listener : listeners)
				listener.propertiesUpdated(changes, timestamps);
		}
		@Override
		public void refresh() throws ServiceException {
			checkDeviceInfo(this, deviceName, true, false);
		}
		@Override
		protected void unlock() {
			lock.unlock();
		}
		
		@Override
		protected void lock() {
			lock.lock();
		}
	}
	public AppLayerDeviceInfoManager() {
	}

	protected boolean isNetLayerSensitive(Map<String, Object> changes) {
		for(DeviceInfoProperty dip : netLayerSensitiveProperties)
			if(changes.containsKey(dip.getAttributeKey()))
				return true;
		return false;
	}

	protected boolean isTemporal(Map<String, Object> changes) {
		int size = changes.size();
		for(DeviceInfoProperty dip : temporalDeviceInfoProperties)
			if(changes.containsKey(dip.getAttributeKey()))
				size--;
		return size <= 0;
	}

	protected void sendToNetLayer(DeviceInfo deviceInfo, Set<String> changesSet) throws ServiceException {
		String[] queues = getKeyReceiverQueueKeys();
		if(queues != null && queues.length > 0) {
			MessageChain mc = new MessageChainV11();
			MessageChainStep step = mc.addStep("");
			Map<String, Object> attributes = step.getAttributes();
			step.setTemporary(true);
			attributes.put(DeviceInfoProperty.DEVICE_NAME.getAttributeKey(), deviceInfo.getDeviceName());
			for(String change : changesSet)
				try {
					deviceInfo.putWithTimestamp(DeviceInfoProperty.getByValue(change), attributes);
				} catch(InvalidValueException e) {
					throw new ServiceException(e);
				}
			for(String queue : queues) {
				step.setQueueKey(queue);
				MessageChainService.publish(mc, getPublisher(), getEncryptionInfoMapping());
			}
		}
	}
	/** 
	 * @param attributes
	 * @return
	 * @throws ServiceException
	 */
	protected void updateDeviceInfo(String deviceName, DeviceInfo deviceInfo, Map<String, Object> attributes, Connection targetConn) throws ServiceException {
		long timestamp = ConvertUtils.getLongSafely(attributes.get("timestamp"), 0);
		Map<String,Long> timestamps = new HashMap<String, Long>();
		for(DeviceInfoProperty dip : DeviceInfoProperty.values()) {
			String key = dip.getAttributeKey();
			timestamps.put(key, ConvertUtils.getLongSafely(attributes.get(key + ".timestamp"), timestamp));
			if (attributes.containsKey(key) && attributes.get(key) == null) {
				if(byte[].class.equals(dip.getAttributeType()))
					attributes.put(key, IOUtils.EMPTY_BYTES);
				else if (String.class.equals(dip.getAttributeType()))
					attributes.put(key, "");
			}
		}
		deviceInfo.updateInternal(attributes, timestamps, targetConn);
	}
	
	protected void deleteDeviceInfo(String deviceName, Map<String, Object> attributes) throws ServiceException {
		deviceInfos.remove(deviceName); // NOTE: this will call interrupt on the thread if it is in process of creating device info
		Connection conn = null;
		try {
			conn = DataLayerMgr.getConnection("EMBED", false);
			ptasBottleneck.lock(deviceName);
			try {
				DataLayerMgr.executeUpdate(conn, "DELETE_DEVICE_PTA_FOR_DEVICE", attributes);
			} finally {
				ptasBottleneck.unlock(deviceName);
			}
			DataLayerMgr.executeUpdate(conn, "DELETE_DEVICE_INFO", attributes);
			conn.commit();
		} catch(SQLException e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
			throw new ServiceException("Could not remove device info for '" + deviceName + "'", e);
		} catch(DataLayerException e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
			throw new ServiceException("Could not remove device info for '" + deviceName + "'", e);
		} catch(InterruptedException e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
			throw new ServiceException(e);
		} finally {
			ProcessingUtils.closeDbConnection(log, conn);
		}
		if(log.isDebugEnabled()) {
			log.debug("Removed Device Info for '" + deviceName + "'");
		}
	}
	
	protected void deleteConsumerAcct(long consumerAcctId) throws ServiceException {
		try {
			DataLayerMgr.executeUpdate("DELETE_EMBED_CONSUMER_ACCT", new Object[]{consumerAcctId}, true);
		} catch(Exception e) {
			throw new ServiceException("Error removing consumer account " + consumerAcctId, e);
		}
		if(log.isDebugEnabled()) {
			log.debug("Removed consumer account " + consumerAcctId);
		}
	}
	
	/** This new method is a better means of handling updates
	 * @param attributes
	 * @return
	 * @throws ServiceException
	 */
	public DeviceInfo checkDeviceInfo(DeviceInfo deviceInfo, String deviceName, boolean checkPTAs, boolean allowNew) throws ServiceException {
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("deviceName", deviceName);
		try {
			DataLayerMgr.selectInto("REFRESH_DEVICE_INFO_FROM_SOURCE", params);
		} catch(NotEnoughRowsException e) {
			if(!allowNew)
				deleteDeviceInfo(deviceName, params);
			return null;
		} catch(SQLException e) {
			throw new ServiceException("Could not get device info for '" + deviceName + "'", e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not get device info for '" + deviceName + "'", e);
		} catch(BeanException e) {
			throw new ServiceException("Could not get device info for '" + deviceName + "'", e);
		} 
		if (deviceInfo == null)
			deviceInfo = getDeviceInfo(deviceName, OnMissingPolicy.CREATE_NEW, false);
		Connection targetConn = null;
		boolean success = false;
		try {
			targetConn = DataLayerMgr.getConnection("EMBED", false);
			updateDeviceInfo(deviceName, deviceInfo, params, targetConn);
			if (checkPTAs)
				checkDevicePTAs(deviceName, targetConn);
			targetConn.commit();
			success = true;
			return deviceInfo;
		} catch (Exception e) {
			throw new ServiceException("Error refreshing device info for " + deviceName, e);
		} finally {
			if (!success)
				ProcessingUtils.rollbackDbConnection(log, targetConn);
			ProcessingUtils.closeDbConnection(log, targetConn);
		}
	}

	public void recheckDeviceDetail() throws ServiceException {
		long recheckTime = deviceDetailRecheckTime.get();
		try {
			if(recheckTime == 0L) {
				// get min device detail recheck
				Results results = DataLayerMgr.executeQuery("GET_MIN_DEVICE_DETAIL_RECHECK", null);
				if(results.next()) {
					Long newRecheckTime = results.getValue("recheckDetailTimestamp", Long.class);
					if(newRecheckTime == null)
						recheckTime = Long.MAX_VALUE;
					else
						recheckTime = newRecheckTime.longValue();
					deviceDetailRecheckTime.compareAndSet(0L, recheckTime);
				}
			}
			long time;
			if(recheckTime == Long.MAX_VALUE || recheckTime > (time = System.currentTimeMillis()))
				return;
			// get all devices that need recheck as of now
			Results results = DataLayerMgr.executeQuery("GET_DEVICE_DETAIL_NEEDING_RECHECK", Collections.singletonMap("currentTime", (Object) time));
			while(results.next()) {
				String deviceName = results.getValue("deviceName", String.class);
				checkDeviceDetail(deviceName);
			}
		} catch(SQLException | ConvertException | DataLayerException e) {
			throw new ServiceException("Could not recheck device detail", e);
		}
		deviceDetailRecheckTime.compareAndSet(recheckTime, 0L);
	}

	public void checkDeviceDetail(String deviceName) throws ServiceException {
		Map<String, Object> params = new HashMap<>();
		int rows = -1;
		// lock this device
		if(log.isDebugEnabled())
			log.debug("Obtaining lock on device detail of " + deviceName);
		try {
			deviceDetailBottleneck.lock(deviceName);
		} catch(InterruptedException e) {
			throw new ServiceException(e);
		}
		try {
			// get from source
			params.put("deviceName", deviceName);
			try {
				DataLayerMgr.selectInto("GET_DEVICE_DETAIL_FROM_SOURCE", params);
			} catch(NotEnoughRowsException e) {
				return;
			} catch(SQLException | DataLayerException | BeanException e) {
				throw new ServiceException("Could not retrieve device detail for " + deviceName, e);
			}
			// update target
			try {
				rows = DataLayerMgr.executeUpdate("UPDATE_DEVICE_DETAIL", params, true);
			} catch(SQLException | DataLayerException e) {
				throw new ServiceException("Could not update device detail for " + deviceName, e);
			}
		} finally {
			deviceDetailBottleneck.unlock(deviceName);
		}
		if(rows > 0) {
			log.info("Updated device detail of " + deviceName);
			long newRecheckTime = ConvertUtils.getLongSafely(params.get("recheckDetailTimestamp"), 0L);
			if(newRecheckTime > 0L && newRecheckTime < Long.MAX_VALUE)
				SystemUtils.updateIfLesser(deviceDetailRecheckTime, newRecheckTime);
		} else if(rows == 0)
			log.info("No device detail was found for " + deviceName);

	}

	public void checkBlacklist(long globalAccountId) throws ServiceException {
		// lock this global account id
		if(log.isDebugEnabled())
			log.debug("Obtaining lock on blacklist of " + globalAccountId);
		try {
			blacklistBottleneck.lock(globalAccountId);
		} catch(InterruptedException e) {
			throw new ServiceException(e);
		}
		try {
			// get from source
			Map<String, Object> params = new HashMap<>();
			params.put("globalAccountId", globalAccountId);
			try {
				DataLayerMgr.selectInto("GET_BLACKLIST_FLAG", params);
			} catch(NotEnoughRowsException e) {
				return;
			} catch(SQLException | DataLayerException | BeanException e) {
				throw new ServiceException("Could not retrieve blacklisted flag for " + globalAccountId, e);
			}
			boolean onBlacklist;
			try {
				onBlacklist = ConvertUtils.getBoolean(params.get("blacklistFlag"), false);
			} catch(ConvertException e) {
				throw new ServiceException(e);
			}
			if(onBlacklist) {
				if(addBlacklisted(globalAccountId))
					log.info("Added " + globalAccountId + " to the blacklist");
				else
					log.info("" + globalAccountId + " is already on the blacklist");
			} else {
				if(removeBlacklisted(globalAccountId))
					log.info("Removed " + globalAccountId + " from the blacklist");
				else
					log.info("" + globalAccountId + " is not on the blacklist");
			}
		} finally {
			blacklistBottleneck.unlock(globalAccountId);
		}
	}

	public void checkConsumerAcct(long consumerAcctId) throws ServiceException {
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("consumerAcctId", consumerAcctId);
		try {
			DataLayerMgr.selectInto("REFRESH_CONSUMER_ACCT_FROM_SOURCE", params);
		} catch(NotEnoughRowsException e) {
			deleteConsumerAcct(consumerAcctId);
			return;
		} catch(Exception e) {
			throw new ServiceException("Error refreshing consumer account " + consumerAcctId, e);
		}
		try {
			DataLayerMgr.executeUpdate("UPDATE_EMBED_CONSUMER_ACCT", params, true);
		} catch(Exception e) {
			throw new ServiceException("Error updating consumer account " + consumerAcctId, e);
		}
	}
	protected int crossComparePTAs(Connection targetConn, Results targetResults, Results sourceResults, boolean targetHadNext, boolean sourceHadNext, String deviceName) throws ConvertException, SQLException, DataLayerException {
		int updated = 0;
		if(targetHadNext) {
			if(sourceHadNext) {
				long srcPosPtaId = sourceResults.getValue("POS_PTA_ID", long.class);
				long tgtPosPtaId = targetResults.getValue("POS_PTA_ID", long.class);
				if(srcPosPtaId < tgtPosPtaId) {
					// insert src
					int n = DataLayerMgr.executeUpdate(targetConn, "INSERT_DEVICE_PTA", sourceResults);
					if(n != 1)
						log.warn("Inserted wrong number of rows (" + n + ") when attempting to synchronize device pta for '" + deviceName + "' with values " + new DynaBeanMap(sourceResults).toString());
					else {
						updated++;
						if(log.isDebugEnabled())
							log.debug("Inserted  device pta for '" + deviceName + "' with values " + new DynaBeanMap(sourceResults).toString());
					}
					// next src
					updated += crossComparePTAs(targetConn, targetResults, sourceResults, targetHadNext, sourceResults.next(), deviceName);
				} else if(srcPosPtaId > tgtPosPtaId) {
					// remove tgt
					int n = DataLayerMgr.executeUpdate(targetConn, "DELETE_DEVICE_PTA", targetResults);
					if(n != 1)
						log.warn("Deleted wrong number of rows (" + n + ") when attempting to synchronize device pta for '" + deviceName + "' with values " + new DynaBeanMap(targetResults).toString());
					else {
						updated++;
						if(log.isDebugEnabled())
							log.debug("Deleted device pta for '" + deviceName + "' with values " + new DynaBeanMap(targetResults).toString());
					}
					//next tgt
					updated += crossComparePTAs(targetConn, targetResults, sourceResults, targetResults.next(), sourceHadNext, deviceName);
				} else {//if(c == 0) {
					//check hash & timestamp
					byte[] srcHash = sourceResults.getValue("PTA_HASH", byte[].class);
					byte[] tgtHash = targetResults.getValue("PTA_HASH", byte[].class);
					if(Arrays.equals(srcHash, tgtHash)) {
						if(log.isDebugEnabled())
							log.debug("No change on device pta for '" + deviceName + "' with values " + new DynaBeanMap(sourceResults).toString());
					} else {
						int n = DataLayerMgr.executeUpdate(targetConn, "UPDATE_DEVICE_PTA", sourceResults);
						if(n != 1)
							log.warn("Updated wrong number of rows (" + n + ") when attempting to synchronize device pta for '" + deviceName + "' with values " + new DynaBeanMap(sourceResults).toString());
						else {
							updated++;
							if(log.isDebugEnabled())
								log.debug("Updated device pta for '" + deviceName + "' with values " + new DynaBeanMap(sourceResults).toString());
						}			
					}
					// next both
					updated += crossComparePTAs(targetConn, targetResults, sourceResults, targetResults.next(), sourceResults.next(), deviceName);
				}
			} else {
				// remove all remaining in targetResults
				do {
					int n = DataLayerMgr.executeUpdate(targetConn, "DELETE_DEVICE_PTA", targetResults);
					if(n != 1)
						log.warn("Deleted wrong number of rows (" + n + ") when attempting to synchronize device pta for '" + deviceName + "' with values " + new DynaBeanMap(targetResults).toString());
					else {
						updated++;
						if(log.isDebugEnabled())
							log.debug("Deleted device pta for '" + deviceName + "' with values " + new DynaBeanMap(targetResults).toString());
					}
				} while(targetResults.next()) ;
			}
		} else if(sourceHadNext) {
			//add all in source results
			do {
				int n = DataLayerMgr.executeUpdate(targetConn, "INSERT_DEVICE_PTA", sourceResults);
				if(n != 1)
					log.warn("Inserted wrong number of rows (" + n + ") when attempting to synchronize device pta for '" + deviceName + "' with values " + new DynaBeanMap(sourceResults).toString());
				else {
					updated++;
					if(log.isDebugEnabled())
						log.debug("Inserted device pta for '" + deviceName + "' with values " + new DynaBeanMap(sourceResults).toString());
				}
			} while(sourceResults.next()) ;
		}
		return updated;
	}
	/** This new method is a better means of handling updates
	 * @param attributes
	 * @return
	 * @throws ServiceException
	 */
	public void checkDevicePTAs(String deviceName, Connection targetConn) throws ServiceException {
		//lock on device
		if(log.isDebugEnabled())
			log.debug("Obtaining lock on ptas of " + deviceName);
		try {
			ptasBottleneck.lock(deviceName);
		} catch(InterruptedException e) {
			throw new ServiceException(e);
		} 
		if(log.isDebugEnabled())
			log.debug("Obtained lock on ptas of " + deviceName);
		try {
			//get what we have and compare all against source
			Map<String,Object> params = new HashMap<String, Object>();
			params.put("deviceName", deviceName);
			boolean newConnection = false;
			boolean success = false;
			if (targetConn == null) {
				targetConn = DataLayerMgr.getConnection("EMBED", false);
				newConnection = true;
			}
			try {
				Results targetResults = DataLayerMgr.executeQuery(targetConn, "GET_DEVICE_PTAS_HASH", params);
				Results sourceResults = DataLayerMgr.executeQuery("GET_UPDATED_DEVICE_PTAS", params);
				int updated = crossComparePTAs(targetConn, targetResults, sourceResults, targetResults.next(), sourceResults.next(), deviceName);				
				if (newConnection)
					targetConn.commit();
				success = true;
				if(log.isInfoEnabled()) {
					log.info("Updated " + updated + " pta records for device '" + deviceName + "'");
				}
			} finally {
				if (newConnection) {
					if (!success)
		    			ProcessingUtils.rollbackDbConnection(log, targetConn);    		
		    		ProcessingUtils.closeDbConnection(log, targetConn);
				}
			}
		} catch(SQLException e) {
			throw new ServiceException("Could not update ptas on device '" + deviceName + "'", e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not update ptas on device '" + deviceName + "'", e);
		} catch(ConvertException e) {
			throw new ServiceException("Could not update ptas on device '" + deviceName + "'", e);
		} finally {
			ptasBottleneck.unlock(deviceName);
		}
	}
	/** This new method is a better means of handling updates
	 * @param attributes
	 * @return
	 * @throws ServiceException
	 */
	public int checkPaymentSubtypeDetail(PSDAltKey psdKey) throws ServiceException {
		try {
			psdBottleneck.lock(psdKey);
		} catch(InterruptedException e) {
			throw new ServiceException(e);
		} 
		try {
			Map<String,Object> params = new HashMap<String, Object>();
			try {
				DataLayerMgr.selectInto("GET_ONE_PSD_FROM_SOURCE", psdKey, params);
			} catch(NotEnoughRowsException e) {
				//delete
				try {
					DataLayerMgr.executeUpdate("DELETE_PAYMENT_SUBTYPE_DETAIL", psdKey, null, true);
					log.info("Removed payment subtype detail " + psdKey.getPaymentSubtypeId() + " (" + psdKey.getPaymentSubtypeKey() + ")");
					return 0;
				} catch(SQLException e1) {
					throw new ServiceException("Could not remove payment subtype detail", e1);
				} catch(DataLayerException e1) {
					throw new ServiceException("Could not remove payment subtype detail", e1);
				}
			} catch(SQLException e) {
				throw new ServiceException("Could not get payment subtype detail from source", e);
			} catch(DataLayerException e) {
				throw new ServiceException("Could not get payment subtype detail from source", e);
			} catch(BeanException e) {
				throw new ServiceException("Could not get payment subtype detail from source", e);
			} 
			StringBuilder sb = new StringBuilder("PSD").append(" (psId ").append(psdKey.getPaymentSubtypeId())
				.append(", psKeyId ").append(psdKey.getPaymentSubtypeKey()).append(")");
			upsert("INSERT_PAYMENT_SUBTYPE_DETAIL", "UPDATE_PAYMENT_SUBTYPE_DETAIL", params, sb.toString(), null);
		} finally {
			psdBottleneck.unlock(psdKey);
		}
		return 0;
	}
	
	/**
	 * @see com.usatech.layers.common.DeviceInfoManager#getDeviceInfo(java.lang.String, boolean, boolean)
	 */
	@Override
	public DeviceInfo getDeviceInfo(String deviceName, OnMissingPolicy onMissingPolicy, boolean refreshFromSource) throws ServiceException {
		return getDeviceInfo(deviceName, onMissingPolicy, refreshFromSource, null, false);
	}
	
	/**
	 * @see com.usatech.layers.common.DeviceInfoManager#getDeviceInfo(java.lang.String, boolean, boolean)
	 */
	public DeviceInfo getDeviceInfo(String deviceName, OnMissingPolicy onMissingPolicy, boolean refreshFromSource, InterruptGuard guard, boolean byDeviceSerialCd) throws ServiceException {
		if(LOAD_BALANCER_DEVICE_INFO.getDeviceName().equals(deviceName))
			return LOAD_BALANCER_DEVICE_INFO;
		boolean createNew = (onMissingPolicy == OnMissingPolicy.CREATE_NEW);
		boolean original;
		if(guard != null)
			original = guard.setInterruptible(false);
		else
			original = true;
		AppLayerDeviceInfo deviceInfo;
		try {
			deviceInfo = deviceInfos.getOrCreate(deviceName, createNew, refreshFromSource);
		} catch(ExecutionException e) {
			if(e.getCause() instanceof NoSuchElementException) {
				if(onMissingPolicy == OnMissingPolicy.RETURN_NULL)
					return null;
				throw new ServiceException(e.getCause().getMessage());
			} else if(e.getCause() instanceof ServiceException) {
				throw (ServiceException) e.getCause();
			} else if(e.getCause() != null) {
				throw new ServiceException(e.getCause());
			} else {
				throw new ServiceException(e);
			}
		} finally {
			if(guard != null)
				guard.setInterruptible(original);
		}
		if(guard != null && deviceInfo != null)
			deviceInfo = new InnerInterruptGuardDeviceInfo(deviceInfo, guard);
		return deviceInfo;
	}
	
	protected AppLayerDeviceInfo retrieveDeviceInfoFromCall(String deviceName, String callId) throws ServiceException {
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("deviceName", deviceName);
		try {
			DataLayerMgr.selectInto(callId, params);
			long timestamp = ConvertUtils.getLongSafely(params.get("timestamp"), 0);
			Map<String,Long> timestamps = new HashMap<String, Long>();
			for(String key : BasicDeviceInfo.attributeKeys)
				timestamps.put(key, ConvertUtils.getLongSafely(params.get(key + ".timestamp"), timestamp));
			return new InnerDeviceInfo(deviceName, params, timestamps);
		} catch(NotEnoughRowsException e) {
			return null;
		} catch(SQLException e) {
			throw DatabasePrerequisite.createServiceException("Could not get Device Info", e);
		} catch(DataLayerException e) {
			throw new RetrySpecifiedServiceException("Could not get Device Info", e, WorkRetryType.BLOCKING_RETRY);
		} catch(BeanException e) {
			throw new ServiceException("Could not get Device Info", e);
		}
	}
	
	protected void updateDeviceInfoFromCall(String deviceName, String callId, AppLayerDeviceInfo deviceInfo) throws ServiceException {
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("deviceName", deviceName);
		try {
			DataLayerMgr.selectInto(callId, params);
			updateDeviceInfo(deviceName, deviceInfo, params, null);
		} catch(NotEnoughRowsException e) {
			throw new ServiceException("Could not update Device Info", e);
		} catch(SQLException e) {
			throw new ServiceException("Could not update Device Info", e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not update Device Info", e);
		} catch(BeanException e) {
			throw new ServiceException("Could not update Device Info", e);
		}
	}
	
	/**
	 * @param settings
	 * @throws ServiceException
	 */
	protected void queueSettingsUpdate(String deviceName, Map<String, String> settings) throws ServiceException {
		MessageChain mc = new MessageChainV11();
		MessageChainStep store = mc.addStep(getStoreQueueKey());
		for(Map.Entry<String,String> entry : settings.entrySet())
			store.addStringAttribute(entry.getKey(), entry.getValue());
		store.addStringAttribute("deviceName", deviceName);
		MessageChainService.publish(mc, getPublisher(), getEncryptionInfoMapping());
	}

	protected void saveChanges(DeviceInfo deviceInfo, Connection targetConn) throws ServiceException {
		if (deviceInfo.getDeviceName() != null && deviceInfo.getDeviceName().equalsIgnoreCase(deviceInfo.getDeviceSerialCd()))
			return;
		Map<String,Object> params = new HashMap<String, Object>();
		deviceInfo.putAllWithTimestamps(params);
		params.put("deviceName", deviceInfo.getDeviceName());
		upsert("INSERT_DEVICE_INFO", "UPDATE_DEVICE_INFO", "GET_DEVICE_DETAIL_FROM_SOURCE", params, deviceInfo.getDeviceName() + " device info", targetConn);
	}

	protected void upsert(String insertCall, String updateCall, Object params, String description, Connection targetConn) throws ServiceException {		
		upsert(insertCall, updateCall, null, params, description, targetConn);
	}

	protected void upsert(String insertCall, String updateCall, String inputForInsertCall, Object params, String description, Connection targetConn) throws ServiceException {
		boolean newConnection = false;
		boolean success = false;
		try {
			if (targetConn == null) {
				targetConn = DataLayerMgr.getConnectionForCall(updateCall, false);
				newConnection = true;
			}
			int rows;
			while(true) {
				rows = DataLayerMgr.executeUpdate(targetConn, updateCall, params);
				log.info("Updated " + rows + " " + description + " rows using " + updateCall);
				if(rows > 0)
					break;
				if(!StringUtils.isBlank(inputForInsertCall)) {
					try {
						DataLayerMgr.selectInto(inputForInsertCall, params);
					} catch(NotEnoughRowsException e) {
						log.info("Input Call " + inputForInsertCall + " return zero rows for " + description + "; attempting insert anyway...");
					} catch(SQLException | BeanException e) {
						throw new ServiceException("Could not select for insert of " + description, e);
					}
				}
				try {
					rows = DataLayerMgr.executeUpdate(targetConn, insertCall, params);
					if (rows < 1)
						throw new ServiceException(insertCall + " inserted " + rows + " rows!");
					log.info("Inserted " + rows + " " + description + " rows using " + insertCall);
					long newRecheckTime;
					try {
						newRecheckTime = ConvertUtils.getLongSafely(ReflectionUtils.getProperty(params, "recheckDetailTimestamp"), 0L);
					} catch(IllegalAccessException | InvocationTargetException | IntrospectionException | ParseException e) {
						newRecheckTime = 0L;
					}
					if(newRecheckTime > 0L && newRecheckTime < Long.MAX_VALUE)
						SystemUtils.updateIfLesser(deviceDetailRecheckTime, newRecheckTime);
					break;
				} catch(SQLException e) {
					if("23505".equals(e.getSQLState())  /*SQL-92: INTEGRITY_CONSTRAINT_VIOLATION_NO_SUBCLASS*/) {
						log.info("Integrity Constraint voilation, trying again: " + e.getMessage());
					} else {
						throw e;
					}
				}
			}
			if (newConnection)
				targetConn.commit();
			success = true;
		} catch(SQLException e) {
			throw new ServiceException("Could not upsert " + description, e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not upsert " + description, e);
		}
		finally {
			if (newConnection) {
				if (!success)
					ProcessingUtils.rollbackDbConnection(log, targetConn);
				ProcessingUtils.closeDbConnection(log, targetConn);
			}
		}
	}

	public Publisher<ByteInput> getPublisher() {
		return publisher;
	}
	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}
	public String getStoreQueueKey() {
		return storeQueueKey;
	}
	public void setStoreQueueKey(String storeQueueKey) {
		this.storeQueueKey = storeQueueKey;
	}

	public EncryptionInfoMapping getEncryptionInfoMapping() {
		return encryptionInfoMapping;
	}

	public void setEncryptionInfoMapping(EncryptionInfoMapping encryptionInfoMapping) {
		this.encryptionInfoMapping = encryptionInfoMapping;
	}

	public DeviceInfoProperty[] getTemporalDeviceInfoProperties() {
		return temporalDeviceInfoProperties.toArray(new DeviceInfoProperty[temporalDeviceInfoProperties.size()]);
	}

	public void setTemporalDeviceInfoProperties(DeviceInfoProperty[] temporalDeviceInfoProperties) {
		this.temporalDeviceInfoProperties.clear();
		if(temporalDeviceInfoProperties != null)
			for(DeviceInfoProperty dip : temporalDeviceInfoProperties)
				this.temporalDeviceInfoProperties.add(dip);
	}

	public boolean isInitiallyRefreshedFromSource() {
		return initiallyRefreshedFromSource;
	}

	public void setInitiallyRefreshedFromSource(boolean initiallyRefreshedFromSource) {
		this.initiallyRefreshedFromSource = initiallyRefreshedFromSource;
	}

	public int getMaxTemporalSaveCount() {
		return maxTemporalSaveCount;
	}

	public void setMaxTemporalSaveCount(int maxTemporalSaveCount) {
		this.maxTemporalSaveCount = maxTemporalSaveCount;
	}

	public String[] getKeyReceiverQueueKeys() {
		return keyReceiverQueueKeys;
	}

	public void setKeyReceiverQueueKeys(String[] keyReceiverQueueKeys) {
		this.keyReceiverQueueKeys = keyReceiverQueueKeys;
	}

	public DeviceInfoProperty[] getNetLayerSensitiveProperties() {
		return netLayerSensitiveProperties;
	}

	public void setNetLayerSensitiveProperties(DeviceInfoProperty[] netLayerSensitiveProperties) {
		this.netLayerSensitiveProperties = netLayerSensitiveProperties;
	}

	public boolean isBlacklisted(Long globalAccountId) {
		return blacklistedGlobalAccountIds.contains(globalAccountId);
	}

	public boolean addBlacklisted(long globalAccountId) {
		return blacklistedGlobalAccountIds.add(globalAccountId);
	}

	public boolean removeBlacklisted(long globalAccountId) {
		return blacklistedGlobalAccountIds.remove(globalAccountId);
	}
}
