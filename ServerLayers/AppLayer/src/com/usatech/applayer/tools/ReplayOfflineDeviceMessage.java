package com.usatech.applayer.tools;

import java.io.PrintStream;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Map;

import org.apache.commons.configuration.Configuration;

import simple.app.BaseWithConfig;
import simple.app.Publisher;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.ByteInput;
import simple.results.Results;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.constants.DeviceType;

public class ReplayOfflineDeviceMessage extends BaseWithConfig {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new ReplayOfflineDeviceMessage().run(args);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void execute(Map<String, Object> argMap, Configuration config) {
		PrintStream out = System.out;
		try {
			configureDataSourceFactory(config, null);
		} catch(ServiceException e) {
			out.println("Could not configure data source factory; exiting app.");
			e.printStackTrace(out);
			System.exit(200);
			return;
		}
		try {
			configureDataLayer(config);
		} catch(ServiceException e) {
			out.println("Could not configure data layer; exiting app.");
			e.printStackTrace(out);
			System.exit(200);
			return;
		}
		long[] deviceMessageIds;
		try {
			deviceMessageIds = ConvertUtils.convert(long[].class, argMap.get("deviceMessageIds"));
		} catch(ConvertException e) {
			out.println("Could not convert <deviceMessageIds> argument into an array of numbers; exiting app.");
			e.printStackTrace(out);
			System.exit(200);
			return;
		}
		Results results;
		try {
			results = DataLayerMgr.executeQuery("GET_DEVICE_MESSAGE", Collections.singletonMap("deviceMessageIds", (Object)deviceMessageIds));
		} catch(SQLException e) {
			out.println("Could not retrieve device messages from the database; exiting app.");
			e.printStackTrace(out);
			System.exit(300);
			return;
		} catch(DataLayerException e) {
			out.println("Could not retrieve device messages from the database; exiting app.");
			e.printStackTrace(out);
			System.exit(400);
			return;
		}
		Publisher<ByteInput> publisher;
		try {
			publisher = configure(Publisher.class, config.subset("simple.app.Publisher"));
		} catch(ServiceException e) {
			out.println("Could not configure publisher; exiting app.");
			e.printStackTrace(out);
			System.exit(500);
			return;
		}
		int cnt = 0;
		while(results.next()) {
			try {
				MessageChain mc = new MessageChainV11();
				MessageChainStep step = mc.addStep("usat.inbound.message.offline");
				DeviceType deviceType = results.getValue("deviceType", DeviceType.class);
				int protocol;
				switch(deviceType) {
					case EDGE: protocol = 7; break;
					case ESUDS: protocol = 5; break;
					case G4: protocol = 4; break;
					case GX: protocol = 4; break;
					case KIOSK: protocol = 7; break;
					case MEI: protocol = 6; break;
					default: protocol = -1; break;
				}
				step.addLiteralAttribute("protocol", protocol);
				step.addLiteralAttribute("actionCode", 0);
				step.addLiteralAttribute("deviceName", results.getValue("deviceName", String.class));
				step.addLiteralAttribute("deviceType", deviceType);
				step.addLiteralAttribute("serverTime", results.getValue("serverTime", long.class));
				step.addLiteralAttribute("data", results.getValue("data", byte[].class));
				step.addLiteralAttribute("globalSessionCode", results.getValue("globalSessionCode", String.class));
				step.addLiteralAttribute("sessionTimeout", 40000);
				MessageChainService.publish(mc, publisher);
				out.println("Published message into '" + step.getQueueKey() + "' for device message #" + results.getValue("deviceMessageId"));
				cnt++;
			} catch(ConvertException e) {
				out.println("Invalid data for device message #" + results.getValue("deviceMessageId") + "; could not re-publish message");
				e.printStackTrace(out);
			} catch(ServiceException e) {
				out.println("Error occured trying to publish device message #" + results.getValue("deviceMessageId"));
				e.printStackTrace(out);
			}
		}
		out.println("Published " + cnt + " message(s) into the offline queue for re-processing");
	}

	@Override
	protected void registerDefaultCommandLineArguments() {
		registerCommandLineArgument(false, "deviceMessageIds", "The comma-separated list of ENGINE.DEVICE_MESSAGE.DEVICE_MESSAGE_ID's of the messages to re-play");
	}
}
