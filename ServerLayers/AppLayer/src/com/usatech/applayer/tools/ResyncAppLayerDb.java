package com.usatech.applayer.tools;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.swing.JOptionPane;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.Argument;
import simple.db.BasicDataSourceFactory;
import simple.db.Call;
import simple.db.Cursor;
import simple.db.Parameter;
import simple.db.ParameterException;
import simple.io.Log;
import simple.results.Results;
import simple.sql.SQLType;
import simple.swt.UIUtils;

public class ResyncAppLayerDb {
	private static final Log log = Log.getLog();
	public final static String OPER_DEV = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb011.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=usadev03.world)))";
	public final static String OPER_INT = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=intdb011.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=usadev02.world)))";
	public final static String OPER_ECC = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=eccscan01.usatech.com)(PORT=1525))(ADDRESS=(PROTOCOL=TCP)(HOST=eccscan02.usatech.com)(PORT=1525)))(CONNECT_DATA=(SERVICE_NAME=usaecc_db.world)))";
	public final static String OPER_PROD_31 = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.120)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.121)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.122)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.220)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.221)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.222)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)))";

	protected static enum CheckState {
		OKAY, MISMATCH, MISSING
	}

	public static void main(String[] args) throws Exception {
		String env = (String) JOptionPane.showInputDialog(null, "Which Environment do you wish to re-synchronize?", "Select Environment", JOptionPane.QUESTION_MESSAGE, null, new String[] { "DEV", "INT", "ECC", "USA" }, "DEV");
		String startDeviceName = " ";
		int apps = 4;
		final int maxRows = 1000;
		//----------------------------
		System.setProperty("javax.net.ssl.trustStore", "../LayersCommon/conf/apr/truststore.ts");
		System.setProperty("javax.net.ssl.trustStorePassword", "usatech");
		Call selectCall = new Call();
		selectCall.setSql("SELECT P.DEVICE_NAME, COUNT(P.POS_PTA_ID) CNT, SUM(TO_NUMBER(SUBSTR(P.PTA_HASH, 1, 8), 'XXXXXXXXXXXXXXXX')) HASH_SUM, MAX(P.POS_PTA_ID) MAX_POS_PTA_ID, MAX(D.MAX_DEVICE_NAME) MAX_DEVICE_NAME FROM APP_LAYER.VW_DEVICE_PTA P RIGHT JOIN (SELECT MIN(DEVICE_NAME) MIN_DEVICE_NAME, MAX(DEVICE_NAME) MAX_DEVICE_NAME FROM (SELECT DEVICE_NAME FROM DEVICE.DEVICE WHERE DEVICE_NAME > NVL(?, ' ') AND DEVICE_ACTIVE_YN_FLAG = 'Y' ORDER BY DEVICE_NAME) WHERE ROWNUM <= ?) D ON P.DEVICE_NAME BETWEEN D.MIN_DEVICE_NAME AND D.MAX_DEVICE_NAME GROUP BY P.DEVICE_NAME ORDER BY P.DEVICE_NAME");
		selectCall.setMaxRows(maxRows);
		Cursor cursor = new Cursor();
		cursor.setAllColumns(true);
		selectCall.setArguments(new Argument[] { 
			cursor,
			new Parameter("deviceName", new SQLType(Types.VARCHAR), true, true, false), 
			new Parameter("maxRows", new SQLType(Types.NUMERIC), true, true, false)
		});
    	
		Call syncCall = new Call();
		syncCall.setSql("{call ENGINE.PKG_DATA_SYNC.SP_CREATE_DATA_SYNC('DEVICE_PTAS', 'PSS.POS_PTA', 'U', ?, ?, NULL)}");
		syncCall.setArguments(new Argument[] { 
			new Parameter(null, null, true, false, true), 
			new Parameter("posPtaId", new SQLType(Types.NUMERIC), true, true, false),
			new Parameter("deviceName", new SQLType(Types.VARCHAR), true, true, false)
		});
		Call prepareCall = new Call();
		prepareCall.setSql("CREATE OR REPLACE FUNCTION hex_to_int(hexval character varying)\n  RETURNS bigint AS\n$BODY$\nDECLARE\n    result  bigint;\nBEGIN\n    EXECUTE 'SELECT x''' || hexval || '''::bigint' INTO result;\n    RETURN result;\nEND;\n$BODY$\n  LANGUAGE plpgsql IMMUTABLE STRICT\n  COST 100;\nALTER FUNCTION hex_to_int(character varying)\n  OWNER TO postgres;");
		prepareCall.setArguments(new Argument[] { new Parameter(null, null, true, false, true), });
		final Call checkCall = new Call();
		checkCall.setSql("SELECT P.DEVICE_NAME, COUNT(P.POS_PTA_ID) CNT, SUM(HEX_TO_INT(SUBSTR(ENCODE(P.PTA_HASH, 'HEX'), 1, 8))) HASH_SUM FROM APP_CLUSTER.DEVICE_PTA P WHERE P.DEVICE_NAME > COALESCE(?, ' ') GROUP BY P.DEVICE_NAME ORDER BY P.DEVICE_NAME LIMIT ?");
		checkCall.setMaxRows(maxRows);
		cursor = new Cursor();
		cursor.setAllColumns(true);
		checkCall.setArguments(new Argument[] { 
			cursor,
			new Parameter("deviceName", new SQLType(Types.VARCHAR), true, true, false), 
			new Parameter("maxRows", new SQLType(Types.NUMERIC), true, true, false)
		});
		String oracleDb;
		int servers;
		char h;
		String username;
		switch(env) {
			case "DEV":
				oracleDb = OPER_DEV;
				servers = 4;
				h = '1';
				username = "SYSTEM";
				break;
			case "INT":
				oracleDb = OPER_INT;
				servers = 2;
				h = '1';
				username = "SYSTEM";
				break;
			case "ECC":
				oracleDb = OPER_ECC;
				servers = 2;
				h = '1';
				username = "SYSTEM";
				break;
			case "USA":
				oracleDb = OPER_PROD_31;
				servers = 4;
				h = '3';
				username = System.getProperty("dbUserName", System.getProperty("user.name").toUpperCase());
				break;
			default:
				throw new Exception("Unrecognized env: " + env);
		}

		BasicDataSourceFactory bdsf = new BasicDataSourceFactory();
		String password = UIUtils.promptForPassword("Enter the OPER password for " + username, "Database Login", null);
		bdsf.addDataSource("OPER", "oracle.jdbc.driver.OracleDriver", oracleDb, username, password);
		username = "admin_1";
		password = UIUtils.promptForPassword("Enter the APP password for " + username, "Database Login", null);
		for(int n = 1; n <= apps; n++) {
			StringBuilder sb = new StringBuilder();
			sb.append("jdbc:postgresql://").append(env.toLowerCase()).append("mst").append(h).append((int) Math.ceil(n * servers / (double) apps)).append(':');
			if(apps == servers)
				sb.append(5432);
			else
				sb.append(5432 - ((apps - n) % (apps / servers)));
			sb.append("/app_").append(n).append("?ssl=true&connectTimeout=5&tcpKeepAlive=true");
			bdsf.addDataSource("APP_" + n, "org.postgresql.Driver", sb.toString(), username, password);
		}
		password = null;
		class Checker {
			protected final Connection appConn;
			protected Results checkResults;
			protected String checkDeviceName = null;
			
			public Checker(Connection appConn) {
				this.appConn = appConn;
			}

			public CheckState check(String deviceName, BigInteger count, BigInteger hashSum) throws ConvertException, ParameterException, SQLException {
				if(checkResults == null)
					return CheckState.MISSING;
				while(true) {
					int c = checkDeviceName.compareTo(deviceName);
					if(c < 0) {
						if(checkResults.next()) {
							checkDeviceName = checkResults.getValue("DEVICE_NAME", String.class);
							continue; // keep checking
						} else if(checkResults.getRow() < checkCall.getMaxRows()) {
							checkResults.close();
							checkResults = null;
							return CheckState.MISSING;
						} else {
							Map<String, Object> params = new HashMap<>();
							params.put("deviceName", checkDeviceName);
							params.put("maxRows", maxRows);
							checkResults = checkCall.executeQuery(appConn, params, null);
							if(checkResults.next()) {
								checkDeviceName = checkResults.getValue("DEVICE_NAME", String.class);
								continue; // keep checking
							}
							checkResults.close();
							checkResults = null;
							return CheckState.MISSING;
						}
					} else if(c > 0)
						return CheckState.MISSING;
					else {
						BigInteger checkCount = checkResults.getValue("CNT", BigInteger.class);
						BigInteger checkHashSum = checkResults.getValue("HASH_SUM", BigInteger.class);
						if(checkResults.next())
							checkDeviceName = checkResults.getValue("DEVICE_NAME", String.class);
						else if(checkResults.getRow() < checkCall.getMaxRows()) {
							checkResults.close();
							checkResults = null;
						} else {
							Map<String, Object> params = new HashMap<>();
							params.put("deviceName", checkDeviceName);
							params.put("maxRows", maxRows);
							checkResults = checkCall.executeQuery(appConn, params, null);
							if(checkResults.next()) {
								checkDeviceName = checkResults.getValue("DEVICE_NAME", String.class);
							} else {
								checkResults.close();
								checkResults = null;
							}
						}
						if(!ConvertUtils.areEqual(checkCount, count) || !ConvertUtils.areEqual(checkHashSum, hashSum))
							return CheckState.MISMATCH;
						return CheckState.OKAY;
					}
				}
			}
			
			public void setCheckResults(Results checkResults) throws ConvertException {
				this.checkResults = checkResults;
				if(checkResults.next())
					checkDeviceName = checkResults.getValue("DEVICE_NAME", String.class);
				else 
					this.checkResults = null;
			}

			public void close() throws SQLException {
				if(checkResults != null)
					checkResults.close();
				appConn.close();
			}
		}
		Set<String> resynced = new LinkedHashSet<>();
		Checker[] checkers = new Checker[apps];
		Connection conn = bdsf.getDataSource("OPER").getConnection();
		try {
			Map<String, Object> params = new HashMap<>();
			params.put("deviceName", startDeviceName);
			params.put("maxRows", maxRows);
			for(int i = 0; i < apps; i++) {
				log.info("Getting connection for APP_" + (i + 1));
				Connection appConn = bdsf.getDataSource("APP_" + (i + 1)).getConnection();
				try (ResultSet rs = appConn.getMetaData().getProcedures(null, null, "hex_to_int")) {
					if(!rs.next())
						prepareCall.executeCall(appConn, null, null);
				}
				checkers[i] = new Checker(appConn);
				checkers[i].setCheckResults(checkCall.executeQuery(appConn, params, null));
			}
			
			OUTER: while(true) {
				params.put("deviceName", startDeviceName);
				try (Results results = selectCall.executeQuery(conn, params, null)) {
					while(results.next()) {
						String deviceName = results.getValue("DEVICE_NAME", String.class);
						if(deviceName == null) {
							startDeviceName = results.getValue("MAX_DEVICE_NAME", String.class);
							if(startDeviceName == null)
								break OUTER;
							continue;
						}
						BigInteger count = results.getValue("CNT", BigInteger.class);
						BigInteger hashSum = results.getValue("HASH_SUM", BigInteger.class);
						log.info("Checking PTAs for " + deviceName);
						APPS: for(int i = 0; i < apps; i++) {
							BigInteger posPtaId;
							switch(checkers[i].check(deviceName, count, hashSum)) {
								case MISSING:
									log.info("APP_" + (i + 1) + " is missing " + deviceName);
									/* NOTE: AppLayer will load PTA's if device info is missing
									posPtaId = results.getValue("MAX_POS_PTA_ID", BigInteger.class);
									params.put("posPtaId", posPtaId);
									syncCall.executeCall(conn, params, null);
									conn.commit();
									resynced.add(deviceName);
									log.info("Added re-sync of PTAs of " + deviceName);
									break APPS;
									*/
									break;
								case MISMATCH:
									log.info("PTAs in APP_" + (i + 1) + " do not match for " + deviceName);
									posPtaId = results.getValue("MAX_POS_PTA_ID", BigInteger.class);
									params.put("posPtaId", posPtaId);
									syncCall.executeCall(conn, params, null);
									conn.commit();
									resynced.add(deviceName);
									log.info("Added re-sync of PTAs of " + deviceName);
									break APPS;
								case OKAY:
									break;
							}
						}
						startDeviceName = deviceName;
					}
					if(results.getRowCount() < 1)
						break;
				}
			}
		} finally {
			conn.close();
			for(int i = 0; i < apps; i++)
				if(checkers[i] != null) {
					checkers[i].close();
					checkers[i] = null;
				}
		}
		log.info("Re-sync of PTAs is complete. Found " + resynced.size() + " devices that needed re-syncing:\n" + resynced);
	}

}
