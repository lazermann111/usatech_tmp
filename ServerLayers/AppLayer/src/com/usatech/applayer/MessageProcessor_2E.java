package com.usatech.applayer;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import simple.app.ServiceException;
import simple.db.DataLayerMgr;
import simple.io.Log;

import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.Message;
import com.usatech.layers.common.MessageResponse;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.messagedata.MessageData_2E;

public class MessageProcessor_2E implements MessageProcessor {
	public MessageResponse processMessage(MessageChainTaskInfo taskInfo, Message message) throws ServiceException {
		Log log = message.getLog();
		MessageData_2E data = (MessageData_2E)message.getData();
		Map<String,Object> params = new HashMap<String, Object>();
		byte[] genericLog = data.getGenericData();
		Connection conn = null;
		try {
			conn = AppLayerUtils.getConnection(false);
			params.put("exceptionCodeId", 10000);
			params.put("deviceName", message.getDeviceInfo().getDeviceName());
			DeviceType deviceType=message.getDeviceInfo().getDeviceType();
			if(deviceType==DeviceType.ESUDS){
				ProcessingUtils.parseESudsGenericLog(params, genericLog);
			}else{
				params.put("genericData", new String(genericLog));
		        params.put("objectName", message.getDeviceInfo().getDeviceType().name());
			}
	        DataLayerMgr.executeCall(conn, "LOG_GENERIC", params);
			conn.commit();
		} catch(ServiceException e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
            throw e;
		} catch(Exception e) {
        	ProcessingUtils.rollbackDbConnection(log, conn);
        	throw new ServiceException(e);
        } finally {
        	ProcessingUtils.closeDbConnection(log, conn);
        }
		return MessageResponse.CONTINUE_SESSION;
	}
}
