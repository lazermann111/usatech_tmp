package com.usatech.applayer;

import simple.app.ServiceException;
import simple.io.Log;

import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.OfflineMessage;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.messagedata.AuthorizeMessageData;
import com.usatech.layers.common.messagedata.Sale;
import com.usatech.layers.common.messagedata.WSRequestMessageData;

public class SaleMessageProcessor implements OfflineMessageProcessor {
	public void processMessage(MessageChainTaskInfo taskInfo, OfflineMessage message) throws ServiceException {
        Log log = message.getLog();
        Sale sale = (Sale)message.getData();
		AppLayerUtils.processSale(sale, message, taskInfo.getPublisher(), taskInfo.getStep().getAttributeSafely(AuthorityAttrEnum.ATTR_VOID_ALLOWED, Boolean.class, sale instanceof AuthorizeMessageData||sale instanceof WSRequestMessageData));
        log.info("Successfully processed sale message");
    }
}
