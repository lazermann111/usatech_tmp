package com.usatech.applayer;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import simple.app.DatabasePrerequisite;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.CallNotFoundException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DbUtils;
import simple.db.ParameterException;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.results.DatasetHandler;
import simple.text.FixedWidthParser;
import simple.text.StringUtils.Justification;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.BatchUpdateDatasetHandler;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.CommonAttrEnum;

public class ParseRoutingNumFileTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected ResourceFolder resourceFolder;
	protected final FixedWidthParser parser;
	protected int batchSize = 100;

	protected class RoutingNumHandler extends BatchUpdateDatasetHandler {
		public RoutingNumHandler(Connection conn, Map<String, Object> additionalData) throws ParameterException, CallNotFoundException {
			super(conn, "INSERT_ROUTING_NUM_FRB", getBatchSize(), additionalData);
		}
	}
	public ParseRoutingNumFileTask() {
		parser = new FixedWidthParser();
		/*Format: 
		Field Name	Length	Position Description
		Routing Number	9	1-9	The institution's routing number
		Office Code	1	10	Main office or branch O=main B=branch
		Servicing FRB Number	9	11-19	Servicing Fed's main office routing number
		Record Type Code	1	20	The code indicating the ABA number to be used to route or send ACH items to the RFI; 0 = Institution is a Federal Reserve Bank; 1 = Send items to customer routing number; 2 = Send items to customer using new routing number field
		Change Date	6	21-26	Date of last change to CRF information (MMDDYY)
		New Routing Number	9	27-35	Institution's new routing number resulting from a merger or renumber
		Customer Name	36	36-71	Commonly used abbreviated name
		Address	36	72-107	Delivery address
		City	20	108-127	City name in the delivery address
		State Code	2	128-129	State code of the state in the delivery address
		Zipcode	5	130-134	Zip code in the delivery address
		Zipcode Extension	4	135-138	Zip code extension in the delivery address
		Telephone Area Code	3	139-141	Area code of the CRF contact telephone number
		Telephone Prefix Number	3	142-144	Prefix of the CRF contact telephone number
		Telephone Suffix Number	4	145-148	Suffix of the CRF contact telephone number
		Institution Status Code	1	149	Code is based on the customers receiver code;1=Receives Gov/Comm
		Data View Code	1	150	1=Current view
		Filler	5	151-155	Spaces
		 */
		parser.addColumn("routingNum", 9, Justification.LEFT, ' ', String.class);
		parser.addColumn("officeCode", 1, Justification.LEFT, ' ', String.class);
		parser.addColumn("servicingRoutingNum", 9, Justification.LEFT, ' ', String.class);
		parser.addColumn("routingType", 1, Justification.LEFT, ' ', String.class);
		parser.addColumn("changeDate", 6, Justification.LEFT, ' ', String.class); //MMDDYY
		parser.addColumn("newRoutingNum", 9, Justification.LEFT, ' ', String.class);
		parser.addColumn("bankName", 36, Justification.LEFT, ' ', String.class);
		parser.addColumn("address", 36, Justification.LEFT, ' ', String.class);
		parser.addColumn("city", 20, Justification.LEFT, ' ', String.class);
		parser.addColumn("stateCd", 2, Justification.LEFT, ' ', String.class);
		parser.addColumn("postal", 9, Justification.LEFT, ' ', String.class);
		parser.addColumn("phone", 10, Justification.LEFT, ' ', String.class);
		parser.addColumn("statusCd", 1, Justification.LEFT, ' ', String.class);
		parser.addColumn("dataViewCd", 1, Justification.LEFT, ' ', String.class);
		parser.addColumn("filler", 5, Justification.LEFT, ' ', String.class);		
	}
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		String resourceKey;
		long lastModified;
		try {
			resourceKey = step.getAttribute(CommonAttrEnum.ATTR_RESOURCE, String.class, true);
			lastModified = step.getAttribute(CommonAttrEnum.ATTR_RESOURCE_MODIFIED_TIME, Long.class, true);
		} catch(AttributeConversionException e) {
			throw new RetrySpecifiedServiceException("Could not convert attributes", e, WorkRetryType.NO_RETRY);
		}
		String countryCd = "US";
		Map<String, Object> params = new HashMap<>();
		params.put("lastModified", lastModified);
		params.put("countryCd", countryCd);
		Connection conn;
		try {
			conn = DataLayerMgr.getConnection("OPER");
		} catch(SQLException e) {
			throw DatabasePrerequisite.createServiceException(e);
		} catch(DataLayerException e) {
			throw new ServiceException(e);
		}
		try {
			// TODO: check if lastModified is greater than that of the file we've already processed
			DataLayerMgr.executeCall(conn, "SETUP_ROUTING_NUM_REFRESH", params);
			if(!ConvertUtils.getBoolean(params.get("proceed"))) {
				conn.commit();
				log.warn("Routing Num File is not newer than existing. Skipping");
				return 1;
			}
			Connection batchConn = DataLayerMgr.getConnection("OPER");
			try {
				DatasetHandler<ServiceException> handler = new RoutingNumHandler(conn, params);
				ResourceFolder rf = getResourceFolder();
				if(rf == null)
					throw new ServiceException("ResourceFolder property is not set on " + this);
				Resource resource;
				try {
					resource = rf.getResource(resourceKey, ResourceMode.READ);
				} catch(IOException e) {
					throw new ServiceException("Could not get resource at '" + resourceKey + "' for parsing", e);
				}
				try {
					Reader reader = new InputStreamReader(resource.getInputStream());
					try {
						parser.parse(reader, handler);
					} finally {
						reader.close();
					}
				} catch(IOException e) {
					throw new ServiceException(e);
				} finally {
					resource.release();
				}
				batchConn.commit();
			} finally {
				DbUtils.closeSafely(batchConn);
			}
			DataLayerMgr.executeCall(conn, "FINISH_ROUTING_NUM_REFRESH", params);
			conn.commit();
		} catch(SQLException e) {
			throw DatabasePrerequisite.createServiceException(e);
		} catch(DataLayerException | ConvertException e) {
			throw new ServiceException(e);
		} finally {
			DbUtils.closeSafely(conn);
		}
		return 0;
	}

	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}

	public int getBatchSize() {
		return batchSize;
	}

	public void setBatchSize(int batchSize) {
		this.batchSize = batchSize;
	}

}
