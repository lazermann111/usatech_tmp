package com.usatech.applayer;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import simple.app.DatabasePrerequisite;
import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.db.NotEnoughRowsException;
import simple.io.Log;

import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.LoadDataAttrEnum;

public class CheckForFillEventTask implements MessageChainTask {
	private static final Log log = Log.getLog();

	/* INPUT:  LoadDataAttrEnum.ATTR_EVENT_ID, LoadDataAttrEnum.ATTR_EVENT_TIME, LoadDataAttrEnum.ATTR_DEVICE_TYPE,
	 * 		   LoadDataAttrEnum.ATTR_COUNTER_CASH_AMOUNT, LoadDataAttrEnum.ATTR_COUNTER_CASHLESS_AMOUNT, LoadDataAttrEnum.ATTR_COUNTER_PASSCARD_AMOUNT, 
	 * 		   LoadDataAttrEnum.ATTR_COUNTER_CASH_ITEMS, LoadDataAttrEnum.ATTR_COUNTER_CASHLESS_ITEMS, LoadDataAttrEnum.ATTR_COUNTER_PASSCARD_ITEMS
	 * OUTPUT: LoadDataAttrEnum.ATTR_EVENT_ID, LoadDataAttrEnum.ATTR_EVENT_TIME, 
	 * 		   LoadDataAttrEnum.ATTR_DEVICE_SERIAL_CD, LoadDataAttrEnum.ATTR_MINOR_CURRENCY_FACTOR, 
	 * 		   LoadDataAttrEnum.ATTR_COUNTER_CASH_AMOUNT, LoadDataAttrEnum.ATTR_COUNTER_CASHLESS_AMOUNT, LoadDataAttrEnum.ATTR_COUNTER_PASSCARD_AMOUNT, 
	 * 		   LoadDataAttrEnum.ATTR_COUNTER_CASH_ITEMS, LoadDataAttrEnum.ATTR_COUNTER_CASHLESS_ITEMS, LoadDataAttrEnum.ATTR_COUNTER_PASSCARD_ITEMS
	 */
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		final MessageChainStep step = taskInfo.getStep();
		final Map<String,Object> resultAttributes = step.getResultAttributes();
		final Connection conn = AppLayerUtils.getConnection(true);
		try{
			Map<String, Object> params = new HashMap<String, Object>();
			Object counterEventId = step.getAttribute(LoadDataAttrEnum.ATTR_EVENT_ID, Object.class, true);
			params.put("counterEventId", counterEventId);
			//Find counters for event
			try {
				DataLayerMgr.selectInto(conn, "GET_FILL_EVENT_FOR_BATCH_EVENT", params);
			} catch(NotEnoughRowsException e) {
				try {
					DataLayerMgr.selectInto(conn, "GET_FILL_EVENT_FOR_BATCH_EVENT_BY_SESSION_ID", params);
				} catch(NotEnoughRowsException e2) {
					if(log.isInfoEnabled())
						log.info("Fill Event for Batch Event #" + counterEventId + " not found in database. Ignoring for now.");
					return 0;
				}
			}
	    	Object eventId = params.get("eventId");
			long eventLocalTsMs = ConvertUtils.getLong(params.get("eventTime"));
			resultAttributes.put(LoadDataAttrEnum.ATTR_EVENT_ID.getValue(), eventId);
			resultAttributes.put(LoadDataAttrEnum.ATTR_EVENT_TIME.getValue(), eventLocalTsMs);
	    	
	    	DataLayerMgr.selectInto(conn, "GET_DEVICE_INFO_FOR_EVENT", params);
	    	String deviceSerialCd = ConvertUtils.getString(params.get("deviceSerialCd"), true);
	    	int minorCurrencyFactor = ConvertUtils.getInt(params.get("minorCurrencyFactor"));
	    	resultAttributes.put(LoadDataAttrEnum.ATTR_DEVICE_SERIAL_CD.getValue(), deviceSerialCd);
	    	resultAttributes.put(LoadDataAttrEnum.ATTR_MINOR_CURRENCY_FACTOR.getValue(), minorCurrencyFactor);
			return 5; // do load fill
		} catch(SQLException e) {
        	ProcessingUtils.rollbackDbConnection(log, conn);
            throw DatabasePrerequisite.createServiceException(e);
		} catch(Exception e){
			ProcessingUtils.rollbackDbConnection(log, conn);
            throw new ServiceException("Error while checking for Fill V1 Event", e);
		} finally {
			ProcessingUtils.closeDbConnection(log, conn);
		}
	}
}
