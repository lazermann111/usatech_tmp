package com.usatech.applayer;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.SessionCache;
import com.usatech.layers.common.constants.CardType;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.CredentialResult;
import com.usatech.layers.common.constants.SessionControlAction;

import simple.app.DatabasePrerequisite;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.NotEnoughRowsException;
import simple.io.Log;
import simple.results.BeanException;
import simple.text.StringUtils;
import simple.util.CompositeMap;

public class SessionControlTask implements MessageChainTask {
	private static final Log log = Log.getLog();

	protected Number getNetLayerId(Map<String, Object> attributes) throws ServiceException {
		return SessionCache.getOrCreateNetLayerId(attributes);
	}

	protected void startSession(Map<String, Object> attributes, Number netLayerId) throws ServiceException {
		CompositeMap<String, Object> merged = new CompositeMap<String, Object>();
		Map<String, Object> extra = new HashMap<String, Object>();
		extra.put("netLayerId", netLayerId);
		CredentialResult credentialResult = ConvertUtils.convertSafely(CredentialResult.class, attributes.get(CommonAttrEnum.ATTR_CREDENTIAL_RESULT.getValue()), null);
		if(credentialResult == CredentialResult.NEW_PASSWORD_MATCHED)
			extra.put("comments", "New Password Used");
		else if(credentialResult == CredentialResult.TMP_PASSWORD_MATCHED)
			extra.put("comments", "Temporary Password Used");
		else if(credentialResult == CredentialResult.INTERNAL)
			extra.put("comments", "Internal Session");
		else if(credentialResult == CredentialResult.INVALID_USERNAME)
			extra.put("comments", "Invalid Username");
		merged.merge(extra);
		merged.merge(attributes);
		try {
			DataLayerMgr.executeCall("UPDATE_SESSION_START", attributes, true);
			if (merged.containsKey("username"))
				DataLayerMgr.executeCall("PROCESS_DEVICE_CREDENTIAL", attributes, true);
			DataLayerMgr.executeCall("MAIN_UPDATE_SESSION_START", merged, true);
		} catch(SQLException e) {
			throw DatabasePrerequisite.createServiceException(e);
		} catch(DataLayerException e) {
			throw new ServiceException(e);
		}

	}

	protected void startSession(Map<String, Object> attributes) throws ServiceException {
		startSession(attributes, getNetLayerId(attributes));
	}

	protected void endSession(Map<String, Object> attributes) throws ServiceException {
		try {
			DataLayerMgr.executeCall("UPDATE_SESSION_END", attributes, true);
			DataLayerMgr.executeCall("MAIN_UPDATE_SESSION_END", attributes, true);
			if(!StringUtils.isBlank(ConvertUtils.getStringSafely(attributes.get("deviceSerialCd"), null)))
				DataLayerMgr.executeCall("UPDATE_EPORT", attributes, true);
		} catch(SQLException e) {
			throw DatabasePrerequisite.createServiceException(e);
		} catch(DataLayerException e) {
			throw new ServiceException(e);
		}
	}

	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		Map<String, Object> attributes = taskInfo.getStep().getAttributes();
		SessionControlAction action;
		try {
			action = ConvertUtils.convertRequired(SessionControlAction.class, attributes.get("action"));
		} catch(ConvertException e) {
			log.error("Could not convert action to a SessionControlAction", e);
			return 1;
		}
		String deviceName = ConvertUtils.getStringSafely(attributes.get("deviceName"), null);
		String deviceSerialCd = ConvertUtils.getStringSafely(attributes.get("deviceSerialCd"), null);
		if(!StringUtils.isBlank(deviceName) && StringUtils.isBlank(deviceSerialCd) || StringUtils.isBlank(deviceName) && !StringUtils.isBlank(deviceSerialCd)){
			try{
				DataLayerMgr.selectInto("GET_DEVICE_NAME_OR_SERIAL_CD", attributes, attributes);
			} catch(NotEnoughRowsException e){
				log.warn("Could not find device for deviceName: '" + deviceName + "', deviceSerialCd: '" + deviceSerialCd + "'");
				return 0;
			} catch(SQLException e) {
				throw DatabasePrerequisite.createServiceException(e);
			} catch(DataLayerException e) {
				throw new ServiceException(e);
			}catch(BeanException e) {
				throw new ServiceException(e);
			}
		}
		if (StringUtils.isBlank(ConvertUtils.getStringSafely(attributes.get("deviceName"), null))) {
			log.warn("Invalid deviceName: '" + deviceName + "'");
			return 0;
		}
		String globalSessionCodeAttribute = action == SessionControlAction.ADD_TRAN_TO_DEVICE_SESSION ? "saleGlobalSessionCode" : "globalSessionCode";
		if (StringUtils.isBlank(ConvertUtils.getStringSafely(attributes.get(globalSessionCodeAttribute), null))) {
			log.warn("Invalid " + globalSessionCodeAttribute + ": '" + attributes.get(globalSessionCodeAttribute) + "'");
			return 0;
		}
		switch(action) {
			case START:
				startSession(attributes);
				break;
			case END:
				endSession(attributes);
				break;
			case ADD_TRAN_TO_DEVICE_SESSION:
				try {
					CardType cardType = CardType.getByValue(ConvertUtils.getChar(attributes.get("clientPaymentTypeCd")));
					if (cardType.getPaymentType() == null)
						return 0;
					attributes.put("paymentTypeCd", cardType.getPaymentType().getValue());
					DataLayerMgr.executeCall("ADD_TRAN_TO_DEVICE_SESSION", attributes, true);
				} catch (Exception e) {
					throw new ServiceException(e);
				}
				break;				
		}
		return 0;
	}
}
