package com.usatech.applayer;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.NotEnoughRowsException;
import simple.io.Log;
import simple.lang.InvalidValueException;
import simple.results.BeanException;
import simple.security.SecureHash;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.HashDoesNotMatchException;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.MessageAttrEnum;
import com.usatech.layers.common.constants.PaymentMaskBRef;

public class VerifyAccountTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		Map<String, Object> params = new HashMap<String, Object>();
		long globalAccountId;
		String securityCd;
		try {
			globalAccountId = step.getAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, long.class, true);
			params.put("globalAccountId", globalAccountId);
			params.put("currencyCd", step.getAttribute(AuthorityAttrEnum.ATTR_CURRENCY_CD, String.class, false));
			securityCd = step.getAttribute(PaymentMaskBRef.DISCRETIONARY_DATA.attribute, String.class, true);
		} catch(AttributeConversionException e) {
			log.error("Missing attribute", e);
			step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_CODE, AuthResultCode.FAILED.getAuthResponseCodeEC2());
			step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_MESSAGE, "Error");
			return 1;
		}
		try {
			DataLayerMgr.selectInto("GET_ACCOUNT_VERIFY", params);
		} catch(NotEnoughRowsException e) {
			step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_CODE, AuthResultCode.DECLINED_PERMANENT.getAuthResponseCodeEC2());
			step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_MESSAGE, "Invalid account");
			return 1;
		} catch(SQLException | DataLayerException | BeanException e) {
			log.error("While looking up account verification data", e);
			step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_CODE, AuthResultCode.FAILED.getAuthResponseCodeEC2());
			step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_MESSAGE, "Error. Please try again.");
			return 1;
		}
		String cardType = ConvertUtils.getStringSafely(params.get("cardType"), "Unknown");
		step.setResultAttribute(AuthorityAttrEnum.ATTR_CARD_TYPE, cardType);
		// verify security code
		byte[] securityCdHash = ConvertUtils.convertSafely(byte[].class, params.get("securityCdHash"), null);
		byte[] securityCdSalt = ConvertUtils.convertSafely(byte[].class, params.get("securityCdSalt"), null);
		if(securityCdHash != null && securityCdSalt != null) {
			byte[] hash;
			try {
				hash = SecureHash.getHash(securityCd.getBytes(), securityCdSalt);
			} catch(NoSuchAlgorithmException e) {
				log.error("Error calculating security code hash", e);
				step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_CODE, AuthResultCode.FAILED.getAuthResponseCodeEC2());
				step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_MESSAGE, "Error");
				return 1;
			}
			if(!Arrays.equals(securityCdHash, hash)) {
				log.info("Incorrect security code");
				step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_CODE, AuthResultCode.DECLINED.getAuthResponseCodeEC2());
				step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_MESSAGE, "Confirmation data does not match");
				return 1;
			}
		}
		// verify anything else provided
		String savedPostalCd = ConvertUtils.getStringSafely(params.get("consumerPostalCd"));
		String postalCd = step.getAttributeSafely(PaymentMaskBRef.ZIP_CODE.attribute, String.class, null);
		if(!StringUtils.isBlank(postalCd)) {
			if(!StringUtils.isBlank(savedPostalCd) && !postalCd.trim().equalsIgnoreCase(savedPostalCd.trim())) {
				log.info("Incorrect postal code");
				step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_CODE, AuthResultCode.DECLINED.getAuthResponseCodeEC2());
				step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_MESSAGE, "Confirmation data does not match");
				return 1;
			}
		}
		String savedFirstName = ConvertUtils.getStringSafely(params.get("firstName"));
		String savedLastName = ConvertUtils.getStringSafely(params.get("lastName"));
		String cardHolder = step.getAttributeSafely(PaymentMaskBRef.CARD_HOLDER.attribute, String.class, null);
		if(!StringUtils.isBlank(cardHolder)) {
			if(!StringUtils.isBlank(savedFirstName) && !cardHolder.trim().toUpperCase().contains(savedFirstName.trim().toUpperCase())) {
				log.info("Incorrect card holder first name");
				step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_CODE, AuthResultCode.DECLINED.getAuthResponseCodeEC2());
				step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_MESSAGE, "Confirmation data does not match");
				return 1;
			}
			if(!StringUtils.isBlank(savedLastName) && !cardHolder.trim().toUpperCase().contains(savedLastName.trim().toUpperCase())) {
				log.info("Incorrect card holder last name");
				step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_CODE, AuthResultCode.DECLINED.getAuthResponseCodeEC2());
				step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_MESSAGE, "Confirmation data does not match");
				return 1;
			}
		}
		String savedExpDate = ConvertUtils.getStringSafely(params.get("expirationDate"));
		String expDate = step.getAttributeSafely(PaymentMaskBRef.EXPIRATION_DATE.attribute, String.class, null);
		if(!StringUtils.isBlank(expDate)) {
			if(!StringUtils.isBlank(savedExpDate) && !expDate.trim().equalsIgnoreCase(savedExpDate.trim())) {
				log.info("Incorrect expiration date");
				step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_CODE, AuthResultCode.DECLINED.getAuthResponseCodeEC2());
				step.setResultAttribute(MessageAttrEnum.ATTR_RESPONSE_MESSAGE, "Confirmation data does not match");
				return 1;
			}
		}
		StringBuilder nameBuilder = new StringBuilder();
		if(!StringUtils.isBlank(savedFirstName))
			nameBuilder.append(savedFirstName).append(' ');
		if(!StringUtils.isBlank(savedLastName))
			nameBuilder.append(savedLastName);
		String cardNum = step.getAttributeSafely(PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER.attribute, String.class, null);
		if(StringUtils.isBlank(cardNum)) {
			try {
				String maskedCardNum = ConvertUtils.getString(params.get("maskedCardNum"), true);
				int fmtId = ConvertUtils.getInt(params.get("fmtId"));
				String merchantCd = ConvertUtils.getString(params.get("merchantCd"), true);
				byte[] accountCdHash = ConvertUtils.convertRequired(byte[].class, params.get("accountCdHash"));
				cardNum = findCardNum(maskedCardNum, fmtId, merchantCd, accountCdHash);
			} catch(NoSuchAlgorithmException | HashDoesNotMatchException | InvalidValueException | ConvertException e) {
				log.warn("Could not determine card number for globalAccountId " + globalAccountId + "; using blank as card number", e);
			}
		}
		if(!StringUtils.isBlank(cardNum))
			step.setSecretResultAttribute(PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER.attribute, cardNum);
		if(!StringUtils.isBlank(savedPostalCd))
			step.setSecretResultAttribute(PaymentMaskBRef.ZIP_CODE.attribute, savedPostalCd);
		String savedName = nameBuilder.toString().trim();
		if(!StringUtils.isBlank(savedName))
			step.setSecretResultAttribute(PaymentMaskBRef.CARD_HOLDER.attribute, savedName);
		if(!StringUtils.isBlank(savedExpDate))
			step.setSecretResultAttribute(PaymentMaskBRef.EXPIRATION_DATE.attribute, savedExpDate);
		return 0;
	}

	protected String findCardNum(String maskedCardNum, int fmtId, String merchantCd, byte[] accountCdHash) throws NoSuchAlgorithmException, HashDoesNotMatchException, InvalidValueException {
		byte[] groupIDPadded = StringUtils.pad(String.valueOf(Integer.parseInt(merchantCd.substring(merchantCd.length() - 3))), '0', 3, Justification.RIGHT).getBytes();
		byte[] cardArray = maskedCardNum.getBytes();
		switch(fmtId) {
			case 0:
			case 2:
				if(cardArray[6] == '*')
					cardArray[6] = (byte) ('0' + fmtId);
				for(int i = 7; i < 10; i++)
					if(cardArray[i] == '*')
						cardArray[i] = groupIDPadded[i - 7];
				int start = 10;
				for(; start < cardArray.length; start++)
					if(cardArray[start] == '*')
						break;
				int end = cardArray.length - 1;
				for(; end >= start; end--)
					if(cardArray[end] == '*')
						break;
				int baseChecksum = addChecksum(cardArray, 0, start) + addChecksum(cardArray, end + 1, cardArray.length);
				if(log.isDebugEnabled())
					log.debug("Must guess " + (end - start + 1) + " digits for " + maskedCardNum);
				for(int i = start; i <= end; i++)
					cardArray[i] = '0';
				int max = (int) Math.pow(10, (end - start + 1));
				int n;
				for(n = 1; n < max; n++) {
					int checksum = baseChecksum + addChecksum(cardArray, start, end + 1);
					if((checksum % 10) == 0) {
						byte[] guessHash = SecureHash.getUnsaltedHash(cardArray);
						if(Arrays.equals(accountCdHash, guessHash)) {
							if(log.isDebugEnabled())
								log.debug("Found account cd for " + maskedCardNum);
							return new String(cardArray);
						}
					}
					String s = StringUtils.pad(Integer.toString(n), '0', end - start + 1, Justification.RIGHT);
					getBytesFromString(s, 0, s.length(), cardArray, start);
				}
				throw new HashDoesNotMatchException("Could NOT find account cd after " + n + " guesses for " + maskedCardNum);
			case 1:
				if(cardArray[6] == '*')
					cardArray[6] = (byte) ('0' + fmtId);
				// cardArray[7] = process code and is unknown
				for(int i = 8; i < 11; i++)
					if(cardArray[i] == '*')
						cardArray[i] = groupIDPadded[i - 8];
				start = 11;
				for(; start < cardArray.length; start++)
					if(cardArray[start] == '*')
						break;
				end = cardArray.length - 1;
				for(; end >= start; end--)
					if(cardArray[end] == '*')
						break;
				baseChecksum = addChecksum(cardArray, 0, 7) + addChecksum(cardArray, 8, start) + addChecksum(cardArray, end + 1, cardArray.length);
				if(log.isDebugEnabled())
					log.debug("Must guess " + (end - start + 2) + " digits for " + maskedCardNum);
				cardArray[7] = '0';
				for(int i = start; i <= end; i++)
					cardArray[i] = '0';
				max = (int) Math.pow(10, (end - start + 2));
				for(n = 1; n < max; n++) {
					int checksum = baseChecksum + addChecksum(cardArray, start, end + 1) + addChecksum(cardArray, 7, 8);
					if((checksum % 10) == 0) {
						byte[] guessHash = SecureHash.getUnsaltedHash(cardArray);
						if(Arrays.equals(accountCdHash, guessHash)) {
							if(log.isDebugEnabled())
								log.debug("Found account cd for " + maskedCardNum);
							return new String(cardArray);
						}
					}
					String s = StringUtils.pad(Integer.toString(n), '0', end - start + 2, Justification.RIGHT);
					getBytesFromString(s, 0, 1, cardArray, 7);
					getBytesFromString(s, 1, s.length(), cardArray, start);
				}
				throw new HashDoesNotMatchException("Could NOT find account cd after " + n + " guesses for " + maskedCardNum);
			default:
				throw new InvalidValueException("Unrecognized format id " + fmtId, maskedCardNum);
		}
	}

	@SuppressWarnings("deprecation")
	protected void getBytesFromString(String s, int srcBegin, int srcEnd, byte[] dst, int dstBegin) {
		s.getBytes(srcBegin, srcEnd, dst, dstBegin);
	}

	public int addChecksum(byte[] cardArray, int start, int end) {
		int len = cardArray.length;
		int checksum = 0;
		int tmp;

		for(int i = start + ((len + start) % 2); i < end; i += 2) {
			tmp = Character.getNumericValue(cardArray[i]) * 2;
			while(tmp > 9)
				tmp = (tmp % 10) + (tmp / 10);
			checksum += tmp;
		}
		for(int i = start + ((len + start + 1) % 2); i < end; i += 2) {
			tmp = Character.getNumericValue(cardArray[i]);
			checksum += tmp;
		}

		return checksum;
	}

}
