package com.usatech.applayer;

import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.Message;
import com.usatech.layers.common.MessageResponse;

import simple.app.ServiceException;

public interface MessageProcessor {
	/** Processes the message from the device and returns whether to continue the session or to close it
	 * @param taskInfo TODO
	 * @param message The message from the device
	 * @return Whether the session should continue or not
	 * @throws ServiceException
	 */
	public MessageResponse processMessage(MessageChainTaskInfo taskInfo, Message message) throws ServiceException;

}
