package com.usatech.applayer;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.io.Log;

import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.LegacyUtils;
import com.usatech.layers.common.Message;
import com.usatech.layers.common.MessageProcessingUtils;
import com.usatech.layers.common.MessageResponse;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.messagedata.MessageData_82;

public class MessageProcessor_82 implements MessageProcessor {
	public MessageResponse processMessage(MessageChainTaskInfo taskInfo, Message message) throws ServiceException {
		Log log = message.getLog();
		MessageData_82 data = (MessageData_82)message.getData();
		Map<String,Object> params = new HashMap<String, Object>();
		String deviceName=message.getDeviceInfo().getDeviceName();
		params.put("deviceName", deviceName);
		int requestNumber = data.getRequestNumber();
		switch(requestNumber){
			case 0:
				// eSuds send us 82 for unix time as device EV000000 in a separate session and expects it in eastern time (not local).
				// It uses this to set it's hwclock which then appears to be off by the timezone offset.
				// Then it syncs the system time ('date' command) to the hwclock and java uses "date -u" as the local time
				MessageProcessingUtils.writeLegacy84UnixTime(message, ConvertUtils.getLocalTime(System.currentTimeMillis(), message.getDeviceInfo().getTimeZoneGuid()) / 1000);
				break;
			case 1:
				MessageProcessingUtils.writeLegacy85BCDTime(message, ConvertUtils.getLocalTime(System.currentTimeMillis(), message.getDeviceInfo().getTimeZoneGuid()));
				break;
			case 2:
				Connection conn;
				try {
					conn = AppLayerUtils.getConnection(false);
				} catch(ServiceException e) {
					message.getLog().warn("Could not get connection", e);
					return MessageResponse.CLOSE_SESSION;
				}
				try {
					LegacyUtils.configPoke(params, conn, message.getDeviceInfo().getDeviceName(), message.getDeviceInfo().getDeviceType().getValue());
					conn.commit();
				} catch(ServiceException e) {
					message.getLog().warn("Could not process message", e);
					ProcessingUtils.rollbackDbConnection(log, conn);
					return MessageResponse.CLOSE_SESSION;
				} catch(Exception e) {
					message.getLog().warn("Could not process message", e);
					ProcessingUtils.rollbackDbConnection(log, conn);
					return MessageResponse.CLOSE_SESSION;
		        } finally {
		        	ProcessingUtils.closeDbConnection(log, conn);
		        }
				MessageProcessingUtils.writeLegacy2FGenericAck(message);
				break;
			default:
				log.warn("Unknown requestNumber:" + requestNumber);
				return MessageResponse.CLOSE_SESSION;
		}
		
		return MessageResponse.CONTINUE_SESSION;
	}
	
}
