package com.usatech.applayer;

import simple.app.ServiceException;

import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.LegacyUtils;
import com.usatech.layers.common.OfflineMessage;

public class LocalAuthProcessor implements OfflineMessageProcessor {
	@Override
	public void processMessage(MessageChainTaskInfo taskInfo, OfflineMessage message) throws ServiceException {
		LegacyUtils.processLocalAuth(taskInfo, message, AuthorizeTask.nextTraceNumber());
	}
}
