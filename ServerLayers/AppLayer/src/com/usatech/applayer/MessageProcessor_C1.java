package com.usatech.applayer;

import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import simple.app.DatabasePrerequisite;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;

import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.DeviceInfo;
import com.usatech.layers.common.Message;
import com.usatech.layers.common.MessageProcessingUtils;
import com.usatech.layers.common.MessageResponse;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.BooleanType;
import com.usatech.layers.common.constants.GenericResponseResultCode;
import com.usatech.layers.common.constants.GenericResponseServerActionCode;
import com.usatech.layers.common.messagedata.Component;
import com.usatech.layers.common.messagedata.MessageData_C1;
import com.usatech.layers.common.messagedata.MessageData_C1.ComponentData;

public class MessageProcessor_C1 implements MessageProcessor {
    public MessageResponse processMessage(MessageChainTaskInfo taskInfo, Message message) throws ServiceException {
        Log log = message.getLog();
        MessageData_C1 data = (MessageData_C1)message.getData();

        MessageResponse msgResponse = MessageResponse.CONTINUE_SESSION;
        GenericResponseResultCode resultCode = GenericResponseResultCode.DENIED;
        GenericResponseServerActionCode actionCode = GenericResponseServerActionCode.DISCONNECT;
        Connection conn = null;
        Object[] ret;

		try {
			DeviceInfo deviceInfo = message.getDeviceInfo();
			Charset charset = message.getDeviceInfo().getDeviceCharset();
			conn = AppLayerUtils.getConnection(false);

			Map<String, Object> params = new HashMap<String, Object>();

			Map<Integer, String> deviceTypeHostTypes = AppLayerUtils.getDeviceTypeHostTypes(deviceInfo.getDeviceType().getValue());

			Map<String, Map<String, Object>> hosts = new HashMap<String, Map<String, Object>>();
			AppLayerUtils.loadHost(log, 0, data.getDefaultCurrency().getCurrencyCode(), data.getBaseComponentType().getValue(), data.getBaseComponent(), conn, true, deviceTypeHostTypes, params, hosts, charset);
			String deviceCurrency = data.getDefaultCurrency().getCurrencyCode();

			Component gprsModem = null;
			List<ComponentData> components = data.getComponents();
			int numHostBlocks = components.size();
			log.info("Received " + numHostBlocks + " host info blocks");
			for(Component component : components) {
				//TODO:Edge puts CIMI (International Mobile Subscriber Identity) into the model field 
				// Instead we should expand C1 so that Edge can send both the model and the CIMI and maybe other info
				AppLayerUtils.loadHost(log, component.getComponentNumber(), null, component.getComponentType().getValue(), component, conn, false, deviceTypeHostTypes, params, hosts, charset);
				switch(component.getComponentType()) {
					case GPRS_MODEM:
					case GPRS_ATT_MODEM:
					case GPRS_ESEYE_MODEM:
					case GPRS_ROGERS_MODEM:
						gprsModem = component;
						break;
					default:
						break;
				}
			}

			Map<String, Map<String, Object>> dbHosts = new HashMap<String, Map<String, Object>>();

			// Get current Device Id
			params.clear();
			params.put("deviceName", deviceInfo.getDeviceName());
			params.put("effectiveTime", message.getServerTime());
			DataLayerMgr.selectInto("GET_DEVICE_ID", params);
			long deviceId = ConvertUtils.getLong(params.get("deviceId"));
			Results results = DataLayerMgr.executeQuery("GET_DEVICE_HOSTS", params);
			while(results.next()) {
				int hostNumber = results.getValue("hostPortNum", Integer.class);
				Map<String, Object> dbHost = new HashMap<String, Object>();
				dbHost.put("hostId", results.getValue("hostId", Long.class));
				dbHost.put("hostType", results.getValue("hostTypeId", Integer.class));
				dbHost.put("hostSerialNumber", results.getValue("hostSerialCd", String.class));
				dbHost.put("hostLabel", results.getValue("hostLabelCd", String.class));
				dbHost.put("hostEquipmentId", results.getValue("hostEquipmentId", Long.class));
				dbHost.put("hostRevision", results.getValue("firmwareVersion", String.class));
				dbHosts.put(String.valueOf(hostNumber), dbHost);
			}

			if(dbHosts.size() > 0) {
				if(hosts.size() < dbHosts.size())
					log.info("Fewer incoming hosts: " + hosts.size() + " than existing: " + dbHosts.size());
				for(String hostNumber : hosts.keySet().toArray(new String[hosts.size()])) {
					if(dbHosts.containsKey(hostNumber) == false)
						log.info("Host number " + hostNumber + " does not exist");
					else {
						Map<String, Object> host = hosts.get(hostNumber);
						Integer hostTypeId = ConvertUtils.convertSafely(Integer.class, host.get("hostType"), -1);
						Long hostEquipmentId = ConvertUtils.convertSafely(Long.class, host.get("hostEquipmentId"), -1L);
						String hostSerialNumber = ConvertUtils.convertSafely(String.class, host.get("hostSerialNumber"), "");
						String hostLabel = ConvertUtils.convertSafely(String.class, host.get("hostLabel"), "");
						String hostRevision = ConvertUtils.convertSafely(String.class, host.get("hostRevision"), "");

						Map<String, Object> dbHost = dbHosts.get(hostNumber);
						Integer dbHostTypeId = ConvertUtils.convertSafely(Integer.class, dbHost.get("hostType"), -1);
						Long dbHostEquipmentId = ConvertUtils.convertSafely(Long.class, dbHost.get("hostEquipmentId"), -1L);
						String dbHostSerialNumber = ConvertUtils.convertSafely(String.class, dbHost.get("hostSerialNumber"), "");
						String dbHostLabel = ConvertUtils.convertSafely(String.class, dbHost.get("hostLabel"), "");
						String dbHostRevision = ConvertUtils.convertSafely(String.class, dbHost.get("hostRevision"), "");

						if(hostTypeId.equals(dbHostTypeId) && hostEquipmentId.equals(dbHostEquipmentId) && hostSerialNumber.equals(dbHostSerialNumber) && hostLabel.equals(dbHostLabel)
								&& hostRevision.equals(dbHostRevision)) {
							log.info("Host number " + hostNumber + " has not changed");
							hosts.remove(hostNumber);
							dbHosts.remove(hostNumber);
						} else
							log.info("Host number " + hostNumber + " changed:" + " dbHostTypeId: " + dbHostTypeId + ", dbHostEquipmentId: " + dbHostEquipmentId + ", dbHostSerialNumber: "
									+ dbHostSerialNumber + ", dbHostLabel: " + dbHostLabel + ", dbHostRevision: " + dbHostRevision + " to hostTypeId: " + hostTypeId + ", hostEquipmentId: "
									+ hostEquipmentId + ", hostSerialNumber: " + hostSerialNumber + ", hostLabel: " + hostLabel + ", hostRevision: " + hostRevision);
					}
				}
			} else
				log.info("No host records exist in the database");

			AppLayerUtils.upsertDeviceSetting(conn, params, deviceId, "Default Currency", deviceCurrency);

			for(String hostNumber : hosts.keySet().toArray(new String[hosts.size()])) {
                Map<String, Object> host = hosts.get(hostNumber);
                Integer hostTypeId = ConvertUtils.convert(Integer.class, host.get("hostType"));
                params.clear();
                params.put("deviceId", deviceId);
                params.put("hostPortNum", hostNumber);
                params.put("hostTypeId", hostTypeId);
                params.put("hostSerialCd", ConvertUtils.convert(String.class, host.get("hostSerialNumber")));
                params.put("hostLabelCd", ConvertUtils.convert(String.class, host.get("hostLabel")));
                params.put("hostEquipmentId", ConvertUtils.convert(Long.class, host.get("hostEquipmentId")));
                ret = DataLayerMgr.executeCall(conn, "UPSERT_HOST_WITH_EQUIPMENT_ID", params);
                long hostId = ConvertUtils.convert(Long.class, ret[1]);
                BooleanType hostExists = BooleanType.getByValue(ConvertUtils.convert(Byte.class, ret[2]));
                log.info((hostExists == BooleanType.TRUE ? "Existing" : "New") + " host: " + hostId);

                String hostRevision = ConvertUtils.convertSafely(String.class, host.get("hostRevision"), "");
                if(hostRevision.length() > 0)
                    AppLayerUtils.upsertHostSetting(conn, params, hostId, (hostTypeId == 400 ? "Application Version" : "Firmware Version"), hostRevision);
                hosts.remove(hostNumber);
                dbHosts.remove(hostNumber);
            }
			for(String hostNumber : dbHosts.keySet()) {
				Map<String, Object> dbHost = dbHosts.get(hostNumber);
				DataLayerMgr.executeCall(conn, "DEACTIVATE_HOST", dbHost);
				log.info("Deactivated host " + dbHost.get("hostId"));
			}
            if(gprsModem != null) {
            	//Update GPRS_DEVICE
            	params.clear();
                params.put("deviceId", deviceId);
                params.put("component", gprsModem);
                DataLayerMgr.executeCall(conn, "UPDATE_COMM_INFO", params);
            } else {	
            	//Remove GPRS_DEVICE
            	params.clear();
                params.put("deviceId", deviceId);
                DataLayerMgr.executeCall(conn, "REMOVE_COMM_INFO", params);
            }

			// Mark complete any pending "Send Components" commands
			params.clear();
			params.put("deviceName", deviceInfo.getDeviceName());
			params.put("actionCode", GenericResponseServerActionCode.SEND_COMP_IDENTITY);
			DataLayerMgr.executeCall(conn, "UPDATE_CB_PENDING_COMMANDS_SUCCESS", params);

            resultCode = GenericResponseResultCode.OKAY;
            deviceInfo.refreshSafely();
			actionCode = deviceInfo.getActionCode().getGenericResponseServerActionCode();
            conn.commit();

            log.info("Successfully processed message");
		} catch(SQLException e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
			if(DatabasePrerequisite.determineRetryType(e) == WorkRetryType.BLOCKING_RETRY)
				throw new RetrySpecifiedServiceException(e, WorkRetryType.BLOCKING_RETRY);
			log.error("Error processing message", e);
		} catch(DataLayerException e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
			throw new RetrySpecifiedServiceException(e, WorkRetryType.BLOCKING_RETRY);
		} catch(Exception e) {
        	ProcessingUtils.rollbackDbConnection(log, conn);
            log.error("Error processing message", e);
        } finally {
        	ProcessingUtils.closeDbConnection(log, conn);
        }

        MessageProcessingUtils.writeGenericResponseV4_1(message, resultCode, null, actionCode);
        return msgResponse;
    }
}
