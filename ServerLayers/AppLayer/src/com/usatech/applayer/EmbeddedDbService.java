package com.usatech.applayer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.management.ManagementFactory;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.LockSupport;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import com.usatech.applayer.jmx.IndDbRefreshMXBean;
import com.usatech.layers.common.ProcessingUtils;

import simple.app.AbstractService;
import simple.app.ServiceException;
import simple.app.ServiceStatus;
import simple.app.ServiceStatusInfo;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.Call.BatchUpdate;
import simple.db.CallNotFoundException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.ExtendedDataSource;
import simple.db.ParameterException;
import simple.db.helpers.PostgresHelper;
import simple.event.ProgressEvent;
import simple.event.ProgressListener;
import simple.io.Log;
import simple.io.resource.ResourceFolder;
import simple.results.BeanException;
import simple.results.Results;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;
import simple.util.concurrent.TrivialFuture;

public class EmbeddedDbService extends AbstractService implements IndDbRefreshMXBean{
	private static final Log log = Log.getLog();
	protected final ReentrantLock lock = new ReentrantLock();
	protected int numThreads;
	protected boolean started;
	protected int starts;
	protected int startupStage = 0;
	protected int appInstance;
	protected String binRangeFile = "db/ind_db_ardef.txt";
	protected ResourceFolder resourceFolder;
	protected double refreshDeviceActivityDays = 0.0;

	public static enum BinRangeFileType {
		ELAVON(Pattern.compile("^[0-9]{7} (5[1-5][0-9]{7})([0-9]{9})  .{52}$")), 
		CPT(Pattern.compile("^D\\|([0-9]{6,9})[0-9]{0,10}\\|([0-9]{6,9})[0-9]{0,10}\\|[0-9]{1,2}\\|[^|]{0,60}\\|([^|]{0,3})\\|[A-Z]\\|([^|]*[PEHCSX][^|]*)\\|[^|]{0,2}\\|[^|]{0,6}(?:\\|[^|]*)+$"));
		
		private final Pattern parserPattern;
		private BinRangeFileType(Pattern parserPattern) {
			this.parserPattern = parserPattern;
		}
		public Pattern getParserPattern() {
			return parserPattern;
		}
	}
	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}
	
	@Override
	public void approveBinRangeRefresh() throws ServiceException {
		try {
			DataLayerMgr.executeCall("SWAP_BIN_RANGE_TABLE", null, true);
			File tmpFile = new File(binRangeFile + ".tmp");
			rotateBinRangeFiles(tmpFile, new File(binRangeFile));
			DataLayerMgr.executeUpdate("UPDATE_APP_SETTING_INDDB_APPROVE", null, true);
		} catch(Exception e) {
			throw new ServiceException("Error refreshing new BIN ranges", e);
		}
	}
	
	public static boolean rotateBinRangeFiles(File from, File to) {
		if (!from.exists()) {
			log.error("Rename {0} to {1} failed: {2} does not exist", from.getName(), to.getName(), from.getName());
			return false;
		}
		
		File bak = new File(to + ".bak");
		if (bak.exists()) {
			if (!bak.delete()) {
				log.error("Rename {0} to {1} failed: delete {2} failed", from.getName(), to.getName(), bak.getName());
				return false;
			}
		}
		
		if (to.exists()) {
			if (!to.renameTo(bak)) {
				log.error("Rename {0} to {1} failed", to.getName(), bak.getName());
				return false;
			}
		}
		
		if (!from.renameTo(to)) {
			log.error("Rename {0} to {1} failed", from.getName(), to.getName());
			return false;
		}
		
		return true;
	}
	
	protected void registerJMX(){
		MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
		try{
			mbs.registerMBean(this, new ObjectName("IndDbRefreshMXBean:name=IndDbRefreshMXBeanImpl"));
		}catch(Exception e){
			log.warn("Failed to register IndDbRefreshMXBeanImpl.", e );
		}
	}

	@Override
	public double getLastUploadBinRangeDiffPercentage() throws ServiceException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("appSettingCd", "IND_DB_FILE_LAST_UPLOAD_DIFF_PERCENTAGE");
		double lastUploadBinRangeDiffPercentage;
		try{
			DataLayerMgr.selectInto("GET_APP_SETTING_VALUE", params);
			lastUploadBinRangeDiffPercentage=ConvertUtils.getDouble(params.get("appSettingValue"));
			return lastUploadBinRangeDiffPercentage;
		}catch(SQLException e) {
			throw new ServiceException("Could not get IND_DB_FILE_LAST_UPLOAD_DIFF_PERCENTAGE", e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not get IND_DB_FILE_LAST_UPLOAD_DIFF_PERCENTAGE", e);
		} catch(BeanException e) {
			throw new ServiceException("Could not get IND_DB_FILE_LAST_UPLOAD_DIFF_PERCENTAGE", e);
		}  catch(ConvertException e) {
			throw new ServiceException("Could not get IND_DB_FILE_LAST_UPLOAD_DIFF_PERCENTAGE", e);
		} 
	}
	
	public String getPreapproveIndDbFileHostModificationTime() throws ServiceException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("appSettingCd", "IND_DB_FILE_MOD_TIME_PREAPPROVE");
		String indDbModTime;
		try{
			DataLayerMgr.selectInto("GET_APP_SETTING_VALUE", params);
			indDbModTime=ConvertUtils.getStringSafely(params.get("appSettingValue"));
			return indDbModTime;
		}catch(SQLException e) {
			throw new ServiceException("Could not get IND_DB_FILE_MOD_TIME_PREAPPROVE", e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not get IND_DB_FILE_MOD_TIME_PREAPPROVE", e);
		} catch(BeanException e) {
			throw new ServiceException("Could not get IND_DB_FILE_MOD_TIME_PREAPPROVE", e);
		} 
	}

	public EmbeddedDbService(String serviceName) throws Exception {
		super(serviceName);
		registerJMX();
	}

	@Override
	protected ServiceStatusInfo[] getCurrentServiceStatusInfos() {
		ServiceStatusInfo info = new ServiceStatusInfo();
		info.setIterationCount(starts);
		info.setService(this);
		info.setServiceStatus(started ? ServiceStatus.STARTED : ServiceStatus.STOPPED);
		info.setThread(null);
		return new ServiceStatusInfo[] { info };
	}

	public int getNumThreads() {
		return numThreads;
	}

	public int pauseThreads() throws ServiceException {
		return 0;
	}

	public int restartThreads(long timeout) throws ServiceException {
		lock.lock();
		try {
			if(this.numThreads == 0)
				return 0;
			checkDataSource();
			if(started) {
				internalShutdown();
			}
			internalStartup();
			return this.numThreads;
		} catch(Exception e) {
			throw new ServiceException("Could not pre-load APP database", e);
		} finally {
			lock.unlock();
		}
	}

	public int startThreads(int numThreads) throws ServiceException {
		if(numThreads <= 0)
			return 0;
		lock.lock();
		try {
			this.numThreads += numThreads;
			if(!started) {
				internalStartup();
			}
		} catch(Exception e) {
			throw new ServiceException("Could not pre-load APP database", e);
		} finally {
			lock.unlock();
		}
		return numThreads;
	}

	public int stopAllThreads(boolean force, long timeout) throws ServiceException {
		try {
			return stopAllThreads(force).get(timeout, TimeUnit.MILLISECONDS);
		} catch(InterruptedException e) {
			throw new ServiceException(e);
		} catch(ExecutionException e) {
			if(e.getCause() instanceof ServiceException)
				throw (ServiceException)e.getCause();
			else if(e.getCause() != null)
				throw new ServiceException(e.getCause());
			else
				throw new ServiceException(e);
		} catch(TimeoutException e) {
			throw new ServiceException(e);
		}
	}
	public int stopThreads(int numThreads, boolean force, long timeout) throws ServiceException {
		try {
			return stopThreads(numThreads, force).get(timeout, TimeUnit.MILLISECONDS);
		} catch(InterruptedException e) {
			throw new ServiceException(e);
		} catch(ExecutionException e) {
			if(e.getCause() instanceof ServiceException)
				throw (ServiceException)e.getCause();
			else if(e.getCause() != null)
				throw new ServiceException(e.getCause());
			else
				throw new ServiceException(e);
		} catch(TimeoutException e) {
			throw new ServiceException(e);
		}
	}
	
	public Future<Integer> stopAllThreads(boolean force) throws ServiceException {
		lock.lock();
		try {
			if(started) {
				internalShutdown();
				int n = this.numThreads;
				this.numThreads = 0;
				return new TrivialFuture<Integer>(n);
			}
			return new TrivialFuture<Integer>(0);
		} catch(Exception e) {
			throw new ServiceException("Could not shutdown", e);
		} finally {
			lock.unlock();
		}
	}

	public Future<Integer> stopThreads(int numThreads, boolean force) throws ServiceException {
		if(numThreads <= 0)
			return new TrivialFuture<Integer>(0);
		lock.lock();
		try {
			int origThreads = this.numThreads;
			this.numThreads -= numThreads;
			if(!started) 
				return new TrivialFuture<Integer>(0);
			if(this.numThreads > 0) {
				return new TrivialFuture<Integer>(numThreads);
			}
			this.numThreads = 0;
			internalShutdown();
			return new TrivialFuture<Integer>(origThreads);
		} catch(Exception e) {
			throw new ServiceException("Could not shutdown", e);
		} finally {
			lock.unlock();
		}
	}

	protected void internalShutdown() throws Exception {
		started = false;
		fireServiceStatusChanged(null, ServiceStatus.STOPPED, starts, null);
	}
	protected void internalStartup() throws Exception {
		fireServiceStatusChanged(null, ServiceStatus.STARTING, startupStage++, null);
		checkDataSource();
		fireServiceStatusChanged(null, ServiceStatus.STARTED, ++starts, null);
	}

	/**
	 * @throws SQLException
	 *
	 */
	protected void checkDataSource() throws DataLayerException, SQLException, IOException {
		ExtendedDataSource ds = DataLayerMgr.getDataSourceFactory().getDataSource("EMBED");
		for(int i = 0; i < 20 && !ds.isAvailable(); i++)
			LockSupport.parkNanos(TimeUnit.SECONDS.toNanos(1));
		checkBINRanges();
		Map<String, Object> params = new HashMap<String, Object>();
		int refreshPsd;
		int refreshPta;
		int refreshDevice;

		try {
			DataLayerMgr.selectInto("GET_EMBED_REFRESH_STATUS", params);
			refreshPsd = ConvertUtils.getInt(params.get("refreshPsd"));
			refreshPta = ConvertUtils.getInt(params.get("refreshPta"));
			refreshDevice = ConvertUtils.getInt(params.get("refreshDevice"));
		} catch (Exception e) {
			throw new DataLayerException("Error calling GET_EMBED_REFRESH_STATUS", e);
		}
		
		refreshDataFromSource(refreshPsd > 0, refreshPta > 0, refreshDevice > 0);
	}
	
	protected void refreshDataFromSource(boolean refreshPsds, boolean refreshPtas, boolean refreshDevices) throws SQLException, DataLayerException {
		if(!refreshPsds && !refreshPtas && !refreshDevices) {
			log.info("Data found; Not refreshing data from source");
			return;
		}
		long dataRefreshStartTs = System.currentTimeMillis();
		log.info("Refreshing data from source");
		ProgressListener listener = new ProgressListener() {
			public void progressUpdate(ProgressEvent event) {
				fireServiceStatusChanged(null, ServiceStatus.STARTING, startupStage++, null);
				if(log.isInfoEnabled())
					log.info("Imported " + event.getStatusProgress() + " rows");
			}

			public void progressStart(ProgressEvent event) {
				fireServiceStatusChanged(null, ServiceStatus.STARTING, startupStage++, null);
				if(log.isInfoEnabled())
					log.info("Started importing rows");
			}

			public void progressFinish(ProgressEvent event) {
				fireServiceStatusChanged(null, ServiceStatus.STARTING, startupStage++, null);
				if(log.isInfoEnabled())
					log.info("Finished importing " + event.getStatusProgress() + " rows");
			}
		};

		if(refreshPtas || refreshDevices) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("appInstance", appInstance);
			log.info("Purging data syncs for app instance " + appInstance);
			try {
				DataLayerMgr.executeCall("PURGE_DATA_SYNCS", params, true);
			} catch(SQLException e) {
				if(e.getErrorCode() == 54)
					log.warn("Could not truncate data syncs table; continuing anyway", e);
				else
					throw e;
			}
			log.info("Purged data syncs for app instance " + appInstance);
		}
		Map<String, Object> loadparams = Collections.singletonMap("refreshDate", (Object) (System.currentTimeMillis() - (long) (getRefreshDeviceActivityDays() * 1000L * 60 * 60 * 24)));
		if(refreshPsds) {
			log.info("Loading payment subtype detail from source");
			// importTableByBatch("INSERT_PAYMENT_SUBTYPE_DETAIL", "GET_PAYMENT_SUBTYPE_DETAIL_FROM_SOURCE", null, listener);
			importTableByCopy("APP_CLUSTER.PAYMENT_SUBTYPE_DETAIL", null /*new String[] { "PAYMENT_SUBTYPE_ID", "PAYMENT_SUBTYPE_KEY_ID", "CLIENT_PAYMENT_TYPE_CD", "AUTHORITY_GATEWAY_QUEUE_KEY", "AUTO_AUTH_RESULT_CD", "AUTO_AUTH_RESP_DESC", "AUTO_AUTH_ACTION_CD", "AUTO_AUTH_ACTION_BITMAP", "CARD_REGEX", "CARD_REGEX_BREF", "AUTHORITY_SERVICE_TYPE_ID", "AUTHORITY_NAME", "REMOTE_SERVER_ADDR",
																			"REMOTE_SERVER_ADDR_ALT", "REMOTE_SERVER_PORT", "TERMINAL_ENCRYPT_KEY", "TERMINAL_ENCRYPT_KEY2", "TERMINAL_CD", "MERCHANT_CD", "PSD_TIMESTAMP", "PSD_HASH" }*/, "GET_PAYMENT_SUBTYPE_DETAIL_FROM_SOURCE", null, listener);
		}
		if(refreshDevices) {
			log.info("Loading devices from source");
			// importTableByBatch("INSERT_DEVICE_INFO", "GET_DEVICE_INFO_FROM_SOURCE", loadparams, listener);
			importTableByCopy("APP_CLUSTER.DEVICE_INFO", new String[] { "DEVICE_NAME", "DEVICE_SERIAL_CD", "ENCRYPTION_KEY", "PREVIOUS_ENCRYPTION_KEY", "MASTER_ID", "REJECT_UNTIL_TS", "DEVICE_TYPE_ID", "LOCALE", "INIT_ONLY", "ACTIVATION_STATUS_CD", "TIME_ZONE_GUID", "ACTION_CD", "PROPS_TO_REQUEST", "ENCRYPTION_KEY_TIMESTAMP", "PREV_ENCR_KEY_TIMESTAMP", "ACT_STATUS_TIMESTAMP",
					"TIME_ZONE_TIMESTAMP", "MASTER_ID_TIMESTAMP", "REJECT_UNTIL_TS_TIMESTAMP", "ACTION_CD_TIMESTAMP", "PROPS_TO_REQUEST_TIMESTAMP", "USERNAME", "PASSWORD_HASH", "PASSWORD_SALT", "CREDENTIAL_TIMESTAMP", "PREVIOUS_USERNAME", "PREVIOUS_PASSWORD_HASH", "PREVIOUS_PASSWORD_SALT", "NEW_USERNAME", "NEW_PASSWORD_HASH", "NEW_PASSWORD_SALT", "MAX_AUTH_AMOUNT", "MASTER_ID_ALWAYS_INC",
					"PROP_LIST_VERSION", "PROP_LIST_VERSION_TIMESTAMP", "POS_ENVIRONMENT_CD", "PIN_CAPABILITY", "ENTRY_CAPABILITY_CDS", "MERCHANT_CATEGORY_CODE", "DOING_BUSINESS_AS", "ADDRESS", "CITY", "STATE_CD", "POSTAL", "COUNTRY_CD", "CUSTOMER_SERVICE_PHONE", "CUSTOMER_SERVICE_EMAIL", "CUSTOMER_ID", "RECHECK_DETAIL_TIMESTAMP", "PARTIAL_AUTH_IND" }, "GET_DEVICE_INFO_FROM_SOURCE", loadparams, listener);
		}
		if(refreshPtas) {
			log.info("Loading ptas from source");
			// importTableByBatch("INSERT_DEVICE_PTA", "GET_DEVICE_PTA_FROM_SOURCE", loadparams, listener);
			importTableByCopy("APP_CLUSTER.DEVICE_PTA", null /*new String[] { "POS_PTA_ID", "DEVICE_NAME", "PTA_PRIORITY", "PAYMENT_SUBTYPE_ID", "PAYMENT_SUBTYPE_KEY_ID", "PTA_CARD_REGEX", "PTA_CARD_REGEX_BREF", "PTA_ENCRYPT_KEY", "PTA_ENCRYPT_KEY2", "PTA_ALLOW_PASSTHRU", "PREF_AUTH_AMT", "PREF_AUTH_AMT_MAX", "PTA_DISABLE_DEBIT_DENIAL", "CURRENCY_CD", "MINOR_CURRENCY_FACTOR", "PTA_TERMINAL_CD",
																" PTA_ACTIVE_TS", " PTA_DEACTIVE_TS", " PTA_TIMESTAMP", " PTA_HASH", "NO_CONVENIENCE_FEE", "DECLINE_UNTIL" }*/, "GET_DEVICE_PTA_FROM_SOURCE", loadparams, listener);
		}
		log.info("Data refresh completed in " + (System.currentTimeMillis() - dataRefreshStartTs) + " ms");
	}	
	
	protected void importTableByBatch(String targetInsert, String sourceQuery, Object params, ProgressListener listener) throws SQLException, DataLayerException {
		Results results = DataLayerMgr.executeQuery(sourceQuery, params);
		if(listener != null)
			listener.progressStart(new ProgressEvent(0, 0.0f, "Start inserting rows using '" + targetInsert + "'"));
		int i = 1;
		if(!results.isGroupEnding(0)) {
			Connection targetConn = DataLayerMgr.getConnectionForCall(targetInsert, true);
			try {
				BatchUpdate batch = DataLayerMgr.createBatchUpdate(targetInsert, false, 1.0f);
				for(; results.next(); i++) {
					batch.addBatch(results);
					if((i % 1000) == 0) {
						batch.executeBatch(targetConn);
						if(listener != null)
							listener.progressUpdate(new ProgressEvent(i, null, "Inserted " + i + " rows"));
					}
				}
				batch.executeBatch(targetConn);
			} finally {
				ProcessingUtils.closeDbConnection(log, targetConn);
			}
		}
		if(listener != null)
			listener.progressFinish(new ProgressEvent(i - 1, 1.0f, "Finished inserting " + (i - 1) + " rows using '" + targetInsert + "'"));
	}

	protected void importTableByCopy(String targetTable, String[] targetColumns, String sourceQuery, Object params, ProgressListener listener) throws SQLException, DataLayerException {
		Results results = DataLayerMgr.executeQuery(sourceQuery, params);
		if(log.isInfoEnabled()) {
			StringBuilder sb = new StringBuilder("Copying data from '").append(sourceQuery).append("' to table '").append(targetTable);
			if(targetColumns != null) {
				sb.append("' using column mapping:");
				for(int i = 0; i < targetColumns.length; i++) {
					sb.append("\n\t").append(results.getColumnName(i + 1)).append(" => ").append(targetColumns[i]);
				}
			} else
				sb.append("'");
			log.info(sb.toString());
		}
		if(listener != null)
			listener.progressStart(new ProgressEvent(0, 0.0f, "Start inserting rows using '" + "COPY TO " + targetTable + "'"));
		long rows = 0;
		if(!results.isGroupEnding(0)) {
			Connection targetConn = DataLayerMgr.getConnection("EMBED", false);
			try {
				rows = PostgresHelper.copyTo(results, targetConn, targetTable, targetColumns);
				targetConn.commit();
			} catch(IOException e) {
				if(e.getCause() instanceof SQLException)
					throw (SQLException) e.getCause();
				throw new SQLException(e);
			} finally {
				ProcessingUtils.closeDbConnection(log, targetConn);
			}
		}
		if(listener != null)
			listener.progressFinish(new ProgressEvent((int) rows, 1.0f, "Finished updating " + rows + " rows using '" + "COPY TO " + targetTable + "'"));
	}

	protected void checkBINRanges() throws SQLException, DataLayerException, IOException {
		if (binRangeFile == null || binRangeFile.length() == 0)
			return;
		
		Connection conn = DataLayerMgr.getConnection("EMBED", true);
		try {
			long maxBINRangeStart = -1;
			Results results = DataLayerMgr.executeQuery(conn, "GET_MAX_BIN_RANGE_START", null);
			if (results.next())
				maxBINRangeStart = ConvertUtils.getLongSafely(results.getValue(1), -1);
			if (maxBINRangeStart > 0)
				return;
			
			log.info("Loading all BIN ranges...");
			int binCount;
			long loadStartTs = System.currentTimeMillis();
			InputStream in = new FileInputStream(binRangeFile);
			try {
				binCount = loadBINRanges(in, null, conn, "INSERT_BIN_RANGE", null);
			} finally {
				in.close();
			}
			log.info(new StringBuilder("Completed loading ").append(binCount).append(" BIN ranges in ").append(System.currentTimeMillis() - loadStartTs).append(" ms"));
		} finally {
			ProcessingUtils.closeDbConnection(log, conn);
		}
	}

	public static int loadBINRanges(InputStream in, BinRangeFileType fileType, Connection conn, String callId, PrintWriter copy) throws ParameterException, SQLException, CallNotFoundException, NumberFormatException, IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		String line;
		int binCount = 0;
		long binRangeStart, binRangeEnd;
		String countryCode;
		String cardType;
		Set<Long> uniqueness = new HashSet<>();
		BatchUpdate batch = DataLayerMgr.createBatchUpdate(callId, false, 1.0f);
		while((line = reader.readLine()) != null) {
			if(copy != null)
				copy.println(line);
			if(fileType == null) {
				if(line.trim().length() == 0)
					continue;
				if(line.startsWith("00000000|GLOBALBIN"))
					fileType = BinRangeFileType.CPT;
				else
					fileType = BinRangeFileType.ELAVON;
			}
			Matcher binMatcher = fileType.getParserPattern().matcher(line);
			if(!binMatcher.matches())
				continue;
			binRangeStart = Long.parseLong(StringUtils.pad(binMatcher.group(1), '0', 9, Justification.LEFT));
			if(!uniqueness.add(binRangeStart))
				continue;
			binRangeEnd = Long.parseLong(StringUtils.pad(binMatcher.group(2), '9', 9, Justification.LEFT));
			countryCode = binMatcher.group(3);
			cardType = binMatcher.group(4);
			batch.addBatch(new Object[] { binRangeStart, binRangeEnd, cardType, countryCode });
			binCount++;

			if(binCount % 1000 == 0) {
				batch.executeBatch(conn);
				log.info(new StringBuilder("Loaded ").append(binCount).append(" BIN ranges"));
			}
		}
		batch.executeBatch(conn);
		if(copy != null)
			copy.flush();
		return binCount;
	}

	protected static int executeUpdate(Connection conn, String sql) throws SQLException {
		if((sql=sql.trim()).length() == 0)
			return 0;
		if (log.isDebugEnabled())
			log.debug("Executing SQL: " + sql);
		long start = System.currentTimeMillis();
		Statement st = conn.createStatement();
		try {
			int result = st.executeUpdate(sql);
			if (log.isDebugEnabled())
				log.debug("SQL took " + (System.currentTimeMillis() - start) + " milliseconds");
			return result;
		} finally {
			st.close();
		}
	}
	public int unpauseThreads() throws ServiceException {
		return 0;
	}

	public int getAppInstance() {
		return appInstance;
	}

	public void setAppInstance(int appInstance) {
		this.appInstance = appInstance;
	}

	public String getDebitBINRangeFile() {
		return binRangeFile;
	}

	public void setDebitBINRangeFile(String debitBINRangeFile) {
		this.binRangeFile = debitBINRangeFile;
	}

	public double getRefreshDeviceActivityDays() {
		return refreshDeviceActivityDays;
	}

	public void setRefreshDeviceActivityDays(double refreshDeviceActivityDays) {
		this.refreshDeviceActivityDays = refreshDeviceActivityDays;
	}

}
