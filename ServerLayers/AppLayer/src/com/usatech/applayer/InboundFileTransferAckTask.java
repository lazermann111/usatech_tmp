package com.usatech.applayer;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;

import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.DeviceInfo;
import com.usatech.layers.common.DeviceInfoManager;
import com.usatech.layers.common.LegacyUtils;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.MessageAttrEnum;
import com.usatech.layers.common.constants.ServerActionCode;

public class InboundFileTransferAckTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected DeviceInfoManager deviceInfoManager;

	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		if(deviceInfoManager == null)
			throw new ServiceException("DeviceInfoManager is not set on " + this);
		MessageChainStep step = taskInfo.getStep();
		Map<String, Object> attributes = step.getAttributes();
		Long pendingCommandId = ConvertUtils.getLongSafely(attributes.get("pendingCommandId"), -1);
		if(pendingCommandId == -1) {
			log.warn("PendingCommandId is not set in " + attributes);
			return 1;
		}
		if(ProcessingUtils.isNetlayerWaiting(taskInfo))
			taskInfo.setNextQos(taskInfo.getCurrentQos());
		try {			
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("pendingCommandId", pendingCommandId);
			params.put("globalSessionCode", attributes.get("globalSessionCode"));
			boolean okay = false;
			Connection conn = DataLayerMgr.getConnectionForCall("UPDATE_PENDING_COMMAND_SUCCESS");
			try {
				DataLayerMgr.executeUpdate(conn, "UPDATE_PENDING_COMMAND_SUCCESS", params);
				String command;
				if(params.get("dataType") == null) {
					okay = true;
					log.warn("Had pending command id " + pendingCommandId + " but could not find in database");
					return 1;
				}
				command = ConvertUtils.getStringSafely(params.get("command"));
				long updateTime = ConvertUtils.getLongSafely(params.get("updateTime"), 0);
				taskInfo.getStep().getResultAttributes().put("command", command);
				DeviceInfo deviceInfo = ProcessingUtils.getDeviceInfoSafely(deviceInfoManager, taskInfo, attributes, log);
				ServerActionCode actionCode = AppLayerUtils.determineActionCode(deviceInfo, log);
				deviceInfo.setActionCode(actionCode);
				deviceInfo.commitChanges(updateTime);
				taskInfo.getStep().getResultAttributes().put("responseActionCode", actionCode.getGenericResponseServerActionCode());
				String peekResponse = ConvertUtils.getStringSafely(attributes.get("peekResponse"));
				if(peekResponse != null && peekResponse.length() > 0) {
					long time = System.currentTimeMillis();
					long deviceId = AppLayerUtils.getDeviceId(deviceInfo.getDeviceName(), time);
					boolean callIn = LegacyUtils.processPeekResponse(command, peekResponse, deviceId, deviceInfo.getDeviceType(), deviceInfo.getDeviceName(), ConvertUtils.getLocalTime(step.getAttributeSafely(MessageAttrEnum.ATTR_SERVER_TIME, Long.class, time), deviceInfo.getTimeZoneGuid()), conn);
					if(callIn) {
						deviceInfo.setActionCode(ServerActionCode.PERFORM_CALL_IN);
						deviceInfo.commitChanges(updateTime);
					}
				}
				okay = true;
			} finally {
				try {
					if(okay)
						conn.commit();
					else
						ProcessingUtils.rollbackDbConnection(log, conn);
				} finally {
					ProcessingUtils.closeDbConnection(log, conn);
				}
			}
		} catch(SQLException e) {
			log.warn("Error while trying to process generic response", e);
			return 1;
		} catch(DataLayerException e) {
			log.warn("Error while trying to process generic response", e);
			return 1;
		}
		return 0;
	}

	public DeviceInfoManager getDeviceInfoManager() {
		return deviceInfoManager;
	}

	public void setDeviceInfoManager(DeviceInfoManager deviceInfoManager) {
		this.deviceInfoManager = deviceInfoManager;
	}

}
