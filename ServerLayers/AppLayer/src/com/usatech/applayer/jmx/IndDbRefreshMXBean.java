package com.usatech.applayer.jmx;

import javax.management.MXBean;

import simple.app.ServiceException;


@MXBean(true)
public interface IndDbRefreshMXBean {
	
	public abstract void approveBinRangeRefresh() throws ServiceException;
	
	public abstract double getLastUploadBinRangeDiffPercentage() throws ServiceException;
	
	public abstract String getPreapproveIndDbFileHostModificationTime() throws ServiceException;

}
