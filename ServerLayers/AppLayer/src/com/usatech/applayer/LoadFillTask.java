/**
 *
 */
package com.usatech.applayer;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import simple.app.DatabasePrerequisite;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.util.CompositeMap;

import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.task.SQLExceptionHandler;
import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.ProcessingUtils;

/**
 * @author Brian S. Krug
 *
 */
public class LoadFillTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected SQLExceptionHandler sqlExceptionHandler;

	/**
	 * @see com.usatech.app.MessageChainTask#process(com.usatech.app.MessageChainTaskInfo)
	 */
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		Map<String,Object> attributes = taskInfo.getStep().getAttributes();
		Map<String,Object> params = new HashMap<String, Object>();
		CompositeMap<String, Object> merged = new CompositeMap<String, Object>();
		merged.merge(params);
		merged.merge(attributes);
		String callId = null;
		Connection conn = AppLayerUtils.getConnection(true);
		try {
			callId = "LOAD_FILL_START";
			DataLayerMgr.executeCall(conn, "LOAD_FILL_START", merged, params);
			conn.commit();
			if(taskInfo.isRedelivered() || ConvertUtils.getBoolean(params.get("processInfoFlag"))) {
				callId = "PROCESS_FILL_INFO";
				DataLayerMgr.executeCall(conn, "PROCESS_FILL_INFO", merged, params);
				conn.commit();	
				callId = "UPDATE_FILL_INFO";
				DataLayerMgr.executeCall(conn, "UPDATE_FILL_INFO", merged, params);
				conn.commit();	
			}
			if(taskInfo.isRedelivered() || ConvertUtils.getBoolean(params.get("processAmountFlag"))) {
				callId = "PROCESS_FILL_AMOUNT";
				DataLayerMgr.executeCall(conn, "PROCESS_FILL_AMOUNT", merged, params);
				conn.commit();	
			}
			if(taskInfo.isRedelivered() || ConvertUtils.getBoolean(params.get("processDateFlag"))) {
				callId = "PROCESS_FILL_DATE";
				DataLayerMgr.executeCall(conn, "PROCESS_FILL_DATE", merged, params);
				conn.commit();	
			}
		} catch(SQLException e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
			SQLExceptionHandler handler = getSqlExceptionHandler();
			if(handler != null)
				handler.handleSQLException(callId, params, e);
			else
				throw DatabasePrerequisite.createServiceException(e);
		} catch(DataLayerException e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
			throw new RetrySpecifiedServiceException(e, WorkRetryType.BLOCKING_RETRY);
		} catch(ConvertException e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
			throw new RetrySpecifiedServiceException(e, WorkRetryType.BLOCKING_RETRY);
		} finally {
			ProcessingUtils.closeDbConnection(log, conn);
		}
		return 0;
	}

	public SQLExceptionHandler getSqlExceptionHandler() {
		return sqlExceptionHandler;
	}

	public void setSqlExceptionHandler(SQLExceptionHandler sqlExceptionHandler) {
		this.sqlExceptionHandler = sqlExceptionHandler;
	}
}
