package com.usatech.applayer;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.Log;

import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.DeviceInfo;
import com.usatech.layers.common.LegacyUpdateStatusProcessor;
import com.usatech.layers.common.LegacyUtils;
import com.usatech.layers.common.Message;
import com.usatech.layers.common.MessageProcessingUtils;
import com.usatech.layers.common.MessageResponse;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.messagedata.MessageData_9A6A;
import com.usatech.layers.common.messagedata.MessageData_9A6A.LabelData;
/**
 * Room label handling
 * @author yhe
 *
 */
public class MessageProcessor_9A6A implements MessageProcessor {
    public MessageResponse processMessage(MessageChainTaskInfo taskInfo, Message message) throws ServiceException {
        Log log = message.getLog();
        MessageData_9A6A data = (MessageData_9A6A)message.getData();

        Connection conn = null;
        boolean isRoomLayoutRequested=false;
        try {
            List<LabelData> labelList = data.getLabels();
            if(labelList.size()>99) {
            	log.warn("Data looks corrupt!  number of ports is:"+labelList.size());
            	return MessageResponse.CLOSE_SESSION;
            }
            conn = AppLayerUtils.getConnection(false);
            for(LabelData label: labelList){
            	String labelStr=label.getPortLabel().trim();
            	if(labelStr.equals("")) {
            		continue;
            	} else {
            		Map<String,Object> params = new HashMap<String, Object>();
            		DeviceInfo deviceInfo = message.getDeviceInfo();
        			params.put("deviceName", deviceInfo.getDeviceName());
        			params.put("effectiveTime", message.getServerTime());
        			DataLayerMgr.selectInto("GET_DEVICE_ID", params);
                    params.put("hostPortNum", label.getPortNumber());
                    params.put("hostPositionNum", label.getTopOrBottom().getPosition());
                    params.put("hostLabelCd", labelStr);
                    DataLayerMgr.executeCall(conn, "UPDATE_LABEL", params);
                    int hostExists = ConvertUtils.getInt(params.get("exists"));
                    if(hostExists==0) {
                    	log.info("No host record found for deviceId:"+params.get("deviceId")+", port:"+label.getPortNumber()+", position:"+label.getTopOrBottom().getPosition()+".Request room layout.");
                    	isRoomLayoutRequested=LegacyUtils.requestRoomLayout(conn, deviceInfo.getDeviceName());
                    }
            	}
            }
            conn.commit();
            if(isRoomLayoutRequested) {
        		return LegacyUpdateStatusProcessor.processMessage(taskInfo, message, true);
        	} else {
        		MessageProcessingUtils.writeLegacy2FGenericAck(message);
        	}
            log.info("Successfully processed message");
            return MessageResponse.CONTINUE_SESSION;
        } catch(Exception e) {
        	ProcessingUtils.rollbackDbConnection(log, conn);
            log.error("Error processing message", e);
        } finally {
        	ProcessingUtils.closeDbConnection(log, conn);
        }
        return MessageResponse.CLOSE_SESSION;
    }
}
