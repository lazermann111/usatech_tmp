package com.usatech.applayer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.text.StrSubstitutor;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.db.DbUtils;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.mail.Email;
import simple.mail.Emailer;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.applayer.EmbeddedDbService.BinRangeFileType;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CommonAttrEnum;

/**
 * Based on APP_CLUSTER.BIN_RANGE data difference, refresh the data.
 * Send email alert when the bin range difference is greater than the threadhold.
 * 
 * @author yhe
 * 
 */
public class RefreshIndDBTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected ResourceFolder resourceFolder;
	protected double indDbRefreshThreshholdPercentage = 10;
	protected final Emailer emailer = new Emailer();
	protected String mailTo;
	protected String mailFrom;
	protected String mailSubject;
	protected String mailBody;
	protected final SimpleDateFormat indDbModTimeFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
	protected String binRangeFile = "db/ind_db_ardef.txt";

	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		String fileKey;
		long indDbModTime;
		try {
			fileKey = taskInfo.getStep().getAttribute(AuthorityAttrEnum.ATTR_FILE_KEY, String.class, true);
			indDbModTime = taskInfo.getStep().getAttribute(AuthorityAttrEnum.ATTR_NEW_IND_DB_MOD_TIME, Long.class, true);
		} catch(AttributeConversionException e) {
			throw new RetrySpecifiedServiceException(e, WorkRetryType.NO_RETRY);
		}
		Resource dataSource;
		try {
			dataSource = resourceFolder.getResource(fileKey, ResourceMode.READ);
		} catch(IOException e) {
			throw new ServiceException("Could not get Bin Range File from File Repository", e);
		}
		try {
			String fileTypeDesc = taskInfo.getStep().getAttributeDefault(CommonAttrEnum.ATTR_FILE_TYPE, String.class, "UNKNOWN");
			BinRangeFileType fileType;
			if(fileTypeDesc.startsWith("GBINP.V"))
				fileType = BinRangeFileType.CPT;
			else
				fileType = BinRangeFileType.ELAVON;
			int binCount;
			long loadStartTs;
			boolean okay = false;
			Connection conn = DataLayerMgr.getConnection("EMBED", true);
			try {
				File tmpFile = new File(binRangeFile + ".tmp");
				PrintWriter out = new PrintWriter(new FileWriter(tmpFile), true);
				try {
					DataLayerMgr.executeCall(conn, "TRUNCATE_BIN_RANGE_NEW", null);
					log.info("Loading new debit BIN ranges...");
					loadStartTs = System.currentTimeMillis();
					InputStream in = dataSource.getInputStream();
					try {
						binCount = EmbeddedDbService.loadBINRanges(in, fileType, conn, "INSERT_BIN_RANGE_NEW", out);
					} finally {
						in.close();
					}
				} finally {
					out.close();
				}
				Map<String, Object> params = new HashMap<String, Object>();
				DataLayerMgr.selectInto(conn, "GET_IND_DB_DIFF_PERCENTAGE", params);
				double indDbDiffPercentage = ConvertUtils.getDouble(params.get("indDbDiffPercentage"));
				log.info("RefreshIndDBTask indDbDiffPercentage=" + indDbDiffPercentage + " indDbModTime=" + indDbModTime);
				// we will refresh the bin_range only when the diff is less than the threshhold %
				if(indDbDiffPercentage == 0) {
					log.info("RefreshIndDBTask no refresh. bin_range data is identical.");
					EmbeddedDbService.rotateBinRangeFiles(tmpFile, new File(binRangeFile));
				} else if(indDbDiffPercentage < indDbRefreshThreshholdPercentage) {
					DataLayerMgr.executeCall(conn, "SWAP_BIN_RANGE_TABLE", null);
					log.info("RefreshIndDBTask rename bin_range tables.");
					EmbeddedDbService.rotateBinRangeFiles(tmpFile, new File(binRangeFile));
				} else {
					if(emailer.getMailHost() != null && mailTo != null) {
						HashMap<String, Object> alertParams = new HashMap<String, Object>(params);
						alertParams.put("indDbModTime", indDbModTimeFormat.format(new Date(indDbModTime)));
						alertParams.put("indDbRefreshThreshholdPercentage", indDbRefreshThreshholdPercentage);
						alertParams.put("indDbDiffPercentage", indDbDiffPercentage);
						Email email = emailer.createEmail();
						email.setTo(mailTo);
						email.setFrom(mailFrom);
						email.setSubject(mailSubject);
						email.setBody(StrSubstitutor.replace(mailBody, alertParams));
						email.send();
					} else {
						log.warn("RefreshIndDBTask alert mailHost and mailTo are not configured.");
					}
					DataLayerMgr.executeUpdate("UPDATE_APP_SETTING", new Object[] { indDbModTime, "IND_DB_FILE_MOD_TIME_PREAPPROVE" }, true);
					DataLayerMgr.executeUpdate("UPDATE_APP_SETTING", new Object[] { indDbDiffPercentage, "IND_DB_FILE_LAST_UPLOAD_DIFF_PERCENTAGE" }, true);
				}
				okay = true;
			} finally {
				DbUtils.commitOrRollback(conn, okay, true);
			}
			log.info(new StringBuilder("Completed loading ").append(binCount).append(" new debit BIN ranges in ").append(System.currentTimeMillis() - loadStartTs).append(" ms"));
		} catch(Exception e) {
			throw new ServiceException("Error populating new BIN ranges", e);
		} finally {
			dataSource.release();
		}
		return 0;
	}

	public void setIndDbRefreshThreshholdPercentage(double indDbRefreshThreshholdPercentage) {
		this.indDbRefreshThreshholdPercentage = indDbRefreshThreshholdPercentage;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}

	public void setMailHost(String mailHost) {
		this.emailer.setMailHost(mailHost);
	}

	public void setMailTo(String mailTo) {
		this.mailTo = mailTo;
	}

	public void setMailFrom(String mailFrom) {
		this.mailFrom = mailFrom;
	}

	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}

	public void setMailBody(String mailBody) {
		this.mailBody = mailBody;
	}

}
