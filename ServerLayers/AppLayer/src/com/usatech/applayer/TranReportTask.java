package com.usatech.applayer;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import simple.app.DatabasePrerequisite;
import simple.app.Publisher;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DbUtils;
import simple.io.ByteInput;
import simple.io.Log;
import simple.results.BeanException;
import simple.results.Results;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.constants.ConsumerCommAttrEnum;
import com.usatech.layers.common.constants.ConsumerCommType;

public class TranReportTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected String requestQueuePrefix = "usat.report.request.";
	protected Publisher<ByteInput> reportingPublisher;
	
	protected static String[] notSendPurchaseReceiptAppType;
	
	
	public static String[] getNotSendPurchaseReceiptAppType() {
		return notSendPurchaseReceiptAppType;
	}

	public static void setNotSendPurchaseReceiptAppType(
			String[] notSendPurchaseReceiptAppType) {
		TranReportTask.notSendPurchaseReceiptAppType = notSendPurchaseReceiptAppType;
	}

	public String getRequestQueuePrefix() {
		return requestQueuePrefix;
	}

	public void setRequestQueuePrefix(String requestQueuePrefix) {
		this.requestQueuePrefix = requestQueuePrefix;
	}

	public Publisher<ByteInput> getReportingPublisher() {
		return reportingPublisher;
	}

	public void setReportingPublisher(Publisher<ByteInput> reportingPublisher) {
		this.reportingPublisher = reportingPublisher;
	}

	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		Map<String, Object> attributes = taskInfo.getStep().getAttributes();
		try {
			boolean isReload=ConvertUtils.getBoolean(attributes.get("isReload"), false);
			DataLayerMgr.executeCall("REPORT_TRAN", attributes, true);
			if(isReload==false){
				// Logic to publish request for credit xml as it occurs
				publishCreditReportRequest(attributes);
				publishSingleTranReportRequest(attributes);
				publishPrepaidPurchaseRequest(attributes);
				publishReplenishSuccess(attributes);
				publishSproutAlerts(attributes);
			}
		} catch(SQLException e) {
			throw DatabasePrerequisite.createServiceException("Error reporting transaction: " + attributes.get("reportTranId"), e);
		} catch(DataLayerException e) {
			throw new RetrySpecifiedServiceException("Error reporting transaction: " + attributes.get("reportTranId"), e, WorkRetryType.NONBLOCKING_RETRY);
		} catch(ConvertException e) {
			throw new RetrySpecifiedServiceException("Error reporting transaction: " + attributes.get("reportTranId"), e, WorkRetryType.NONBLOCKING_RETRY);
		} catch(BeanException e) {
			throw new RetrySpecifiedServiceException("Error reporting transaction: " + attributes.get("reportTranId"), e, WorkRetryType.NONBLOCKING_RETRY);
		}
		
		return 0;
	}
	
	public void publishCreditReportRequest(Map<String, Object> params) throws BeanException, SQLException,DataLayerException,ConvertException,ServiceException{
		Results results = DataLayerMgr.executeQuery("GET_CREDIT_TRAN_REPORTS", params, false);
		int creditTranReportCount=0;
		long terminalId=-1;
		int[] userReportIds=null;
		if(results.next()) {
			userReportIds=results.getValue("userReportIds", int[].class);
			terminalId=results.getValue("terminalId", long.class);
			creditTranReportCount=userReportIds.length;
			log.info("creditTranReportCount:"+creditTranReportCount+" terminalId:"+terminalId);
			MessageChain mcCreditXml = new MessageChainV11();
			MessageChainStep requestStep = mcCreditXml.addStep(getRequestQueuePrefix() + 10 /*ReportScheduleType.SINGLE_TXN.getValue()*/);
			requestStep.addLiteralAttribute("userReportIds", userReportIds);
			requestStep.addLiteralAttribute("reportTranId", params.get("reportTranId"));
			MessageChainService.publish(mcCreditXml, reportingPublisher);
		} 
			
	}
	
	public void publishSingleTranReportRequest(Map<String, Object> params) throws BeanException, SQLException,DataLayerException,ConvertException,ServiceException{
		Results results = DataLayerMgr.executeQuery("GET_SINGLE_TRAN_REPORTS", params, false);
		int singleTranReportCount=0;
		long terminalId=-1;
		int[] userReportIds=null;
		if(results.next()) {
			userReportIds=results.getValue("userReportIds", int[].class);
			terminalId=results.getValue("terminalId", long.class);
			singleTranReportCount=userReportIds.length;
			log.info("singleTranReportCount:"+singleTranReportCount+" terminalId:"+terminalId);
			MessageChain mcSingleTran = new MessageChainV11();
			MessageChainStep requestStep = mcSingleTran.addStep(getRequestQueuePrefix() + 10 /*ReportScheduleType.SINGLE_TXN.getValue()*/);
			requestStep.addLiteralAttribute("userReportIds", userReportIds);
			requestStep.addLiteralAttribute("reportTranId", params.get("reportTranId"));
			MessageChainService.publish(mcSingleTran, reportingPublisher);
		} 
			
	}
	
	public void publishPrepaidPurchaseRequest(Map<String, Object> params) throws BeanException, SQLException,DataLayerException,ConvertException,ServiceException{
		params.put("notSendPurchaseReceiptAppType", notSendPurchaseReceiptAppType);
		Results results = DataLayerMgr.executeQuery("GET_PREPAID_PURCHASE_REQUEST", params, false);
		if(results.next()) {
			MessageChain prepaidPurchase = new MessageChainV11();
			MessageChainStep requestStep = prepaidPurchase.addStep("usat.consumer.communicate");
			requestStep.setAttribute(ConsumerCommAttrEnum.ATTR_CONSUMER_ACCT_ID, results.get("consumerAcctId"));
			requestStep.setAttribute(ConsumerCommAttrEnum.ATTR_COMMUNICATION_TYPE, ConsumerCommType.PREPAID_PURCHASE);
			requestStep.setAttribute(ConsumerCommAttrEnum.ATTR_REPORT_TRAN_ID, params.get("reportTranId"));
			MessageChainService.publish(prepaidPurchase, reportingPublisher);
		}
	}
	
	public void publishReplenishSuccess(Map<String, Object> params) throws BeanException, SQLException,DataLayerException,ConvertException,ServiceException{
		Results results = DataLayerMgr.executeQuery("GET_REPLENISH_SUCCESS_REQUEST", params, false);
		if(results.next()) {
			MessageChain replenishSuccess = new MessageChainV11();
			MessageChainStep requestStep = replenishSuccess.addStep("usat.consumer.communicate");
			requestStep.setAttribute(ConsumerCommAttrEnum.ATTR_CONSUMER_ACCT_ID, results.get("consumerAcctId"));
			requestStep.setAttribute(ConsumerCommAttrEnum.ATTR_COMMUNICATION_TYPE, ConsumerCommType.REPLENISH_SUCCESS);
			requestStep.setAttribute(ConsumerCommAttrEnum.ATTR_REPLENISH_CARD_MASKED, results.get("replenishCardMasked"));
			requestStep.setAttribute(ConsumerCommAttrEnum.ATTR_REPLENISH_AMOUNT, results.get("replenishAmount"));
			requestStep.setAttribute(ConsumerCommAttrEnum.ATTR_REPLENISH_CASH_IND, results.get("replenishCashInd"));
			MessageChainService.publish(replenishSuccess, reportingPublisher);
		}
	}
	
	public void publishSproutAlerts(Map<String, Object> params) throws BeanException, SQLException,DataLayerException,ConvertException,ServiceException{
		Results results = DataLayerMgr.executeQuery("GET_SPROUT_ALERT_INFO", params, false);
		if(results.next()) {
			long consumerAcctId = ConvertUtils.getLong(results.get("consumerAcctId"));
			long cashBackParentTranId = ConvertUtils.getLong(results.get("cashBackParentTranId"));
			Connection conn = DataLayerMgr.getConnectionForCall("GET_SPROUT_ALERT_INFO");
			boolean okay = false;
			try {
				InteractionUtils.checkForCashBack(cashBackParentTranId, conn, reportingPublisher, log);
				InteractionUtils.checkForBelowBalance(consumerAcctId, conn, reportingPublisher, log);
				okay = true;
			} finally {
				DbUtils.commitOrRollback(conn, okay, true);
			}
		}
	}
		
}
