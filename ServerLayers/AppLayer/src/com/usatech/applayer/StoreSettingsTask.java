/**
 *
 */
package com.usatech.applayer;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import simple.app.DatabasePrerequisite;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.db.Call.BatchUpdate;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.BeanException;

import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.task.SQLExceptionHandler;
import com.usatech.layers.common.ProcessingUtils;

/**
 * @author Brian S. Krug
 *
 */
public class StoreSettingsTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected SQLExceptionHandler sqlExceptionHandler;
	protected final Set<String> onlyGreaterSettings = new HashSet<String>();

	/**
	 * @see com.usatech.app.MessageChainTask#process(com.usatech.app.MessageChainTaskInfo)
	 */
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		Map<String,Object> attributes = taskInfo.getStep().getAttributes();
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("deviceName", attributes.get("deviceName"));
		params.put("effectiveTime", System.currentTimeMillis()); // we don't have any synchronization control right now so just use the current time
		Connection conn = null;
		try {
			conn = DataLayerMgr.getConnectionForCall("UPDATE_DEVICE_SETTING");
			DataLayerMgr.selectInto(conn, "GET_DEVICE_ID", params);
			BatchUpdate batch = DataLayerMgr.createBatchUpdate("UPDATE_DEVICE_SETTING", false, 0.0f);
			for(Map.Entry<String, Object> entry : attributes.entrySet()) {
				String key = entry.getKey();
				if(!"deviceName".equals(key)) {
					params.put("name", key);
					params.put("value", entry.getValue());
					params.put("onlyGreater", onlyGreaterSettings.contains(key) ? "Y" : "N");
					batch.addBatch(params);
				}					
			}
			batch.executeBatch(conn);
			conn.commit();
		} catch(BeanException e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
			throw new RetrySpecifiedServiceException(e, WorkRetryType.NO_RETRY);
		} catch(SQLException e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
			SQLExceptionHandler handler = getSqlExceptionHandler();
			if(handler != null)
				handler.handleSQLException("UPDATE_DEVICE_SETTING", params, e);
			else
				throw DatabasePrerequisite.createServiceException(e);
		} catch(DataLayerException e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
			throw new RetrySpecifiedServiceException(e, WorkRetryType.BLOCKING_RETRY);
		}
		finally {
			ProcessingUtils.closeDbConnection(log, conn);
		}
		return 0;
	}

	public SQLExceptionHandler getSqlExceptionHandler() {
		return sqlExceptionHandler;
	}

	public void setSqlExceptionHandler(SQLExceptionHandler sqlExceptionHandler) {
		this.sqlExceptionHandler = sqlExceptionHandler;
	}

	public String[] getOnlyGreaterSettings() {
		return onlyGreaterSettings.toArray(new String[onlyGreaterSettings.size()]);
	}

	public void setOnlyGreaterSettings(String[] values) {
		onlyGreaterSettings.clear();
		for(String v : values)
			onlyGreaterSettings.add(v);
	}
}
