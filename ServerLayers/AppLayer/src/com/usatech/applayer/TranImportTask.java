package com.usatech.applayer;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import simple.app.DatabasePrerequisite;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.NotEnoughRowsException;
import simple.io.Log;
import simple.results.BeanException;
import simple.results.Results;
import simple.text.StringUtils;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.ProcessingUtils;

public class TranImportTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	
	protected static String vzm2pLoyaltyQueueKey = null;

	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		long importStartTs = System.currentTimeMillis();
		Map<String, Object> attributes = taskInfo.getStep().getAttributes();
		long tranId;
		try {
			tranId = ConvertUtils.getLong(attributes.get("tranId"));
		} catch (ConvertException e) {
			throw new ServiceException("Could not get tranId", e);
		}
		if (tranId <= 0)
			throw new ServiceException("Received invalid tranId " + tranId + " for import");
    	StringBuilder sb = new StringBuilder("Importing transaction tranId: ").append(tranId).append("...");
		log.info(sb.toString());
		Connection sourceConn;
		Connection targetConn;
        try {
        	sourceConn = DataLayerMgr.getConnection("OPER");
		} catch(SQLException e) {
			throw new RetrySpecifiedServiceException("Could not get connection for data source OPER", e, WorkRetryType.BLOCKING_RETRY);
		} catch(DataLayerException e) {
			throw new RetrySpecifiedServiceException("Could not get connection for data source OPER", e, WorkRetryType.BLOCKING_RETRY);
		}
        try {
			try {
				targetConn = DataLayerMgr.getConnection("REPORT");
			} catch(SQLException e) {
				throw new RetrySpecifiedServiceException("Could not get connection for data source REPORT", e, WorkRetryType.BLOCKING_RETRY);
			} catch(DataLayerException e) {
				throw new RetrySpecifiedServiceException("Could not get connection for data source REPORT", e, WorkRetryType.BLOCKING_RETRY);
			}
			boolean okay = false;
			try {
				Map<String, Object> parameters = new HashMap<String, Object>();
				parameters.put("tranId", tranId);
				try {
					DataLayerMgr.selectInto(sourceConn, "GET_TRAN_DETAILS", parameters);
				} catch(NotEnoughRowsException e) {
					sb.setLength(0);
					sb.append("Transaction tranId: ").append(tranId).append(" does not qualify for import, skipping");
					log.info(sb);
					return 1;
				}
				String tranGlobalTransCd = ConvertUtils.getStringSafely(parameters.get("machineTransNo"));
				int transTypeId = ConvertUtils.getInt(parameters.get("transTypeId"));
				BigDecimal amt = ConvertUtils.convert(BigDecimal.class, parameters.get("totalAmount"));
				if(amt == null || amt.compareTo(BigDecimal.ZERO) == 0) {
					parameters.put("settleStateId", 2);
					parameters.put("settleDate", parameters.get("endDate"));
					parameters.put("apCode", "No Charge");
				} else {
					if(amt.compareTo(BigDecimal.ZERO) < 0) { // a refund!
						switch(transTypeId) {
							case 13:
							case 14:
							case 15:
							case 16:
							case 17:
							case 18:
							case 19:
							case 32:
								transTypeId = 20;
								parameters.put("transTypeId", transTypeId);
								break;
							case 20:
							case 31:
								break;
							case 30:
								transTypeId = 31;
								parameters.put("transTypeId", transTypeId);
								break;
							default:
								log.warn("Amount is negative for transaction " + tranId + " (transTypeId=" + transTypeId + ")");
							
						}
						if(ConvertUtils.getInt(parameters.get("settleStateId")) == 3) {
							DataLayerMgr.selectInto(sourceConn, "GET_REFUND_SETTLEMENT_DATE", parameters);
						}
					} else {
						DataLayerMgr.selectInto(sourceConn, "GET_AUTH_TRAN_CD", parameters);
						if(ConvertUtils.getInt(parameters.get("settleStateId")) == 3){
							DataLayerMgr.selectInto(sourceConn, "GET_SETTLEMENT_DATE", parameters);
						}
					}
				}

				int result = 0;
				Results lineItems = DataLayerMgr.executeQuery(sourceConn, "GET_LINE_ITEM_DETAILS", parameters);
				DataLayerMgr.executeCall(targetConn, "ADD_TRAN", parameters);
				boolean isReload=false;
				if(parameters.get("reloadFlag")!=null&&parameters.get("reloadFlag").equals("Y")){
					isReload=true;
				}
				if(parameters.get("dupFlag")!=null&&parameters.get("dupFlag").equals("Y")){
					log.info("A transaction with machineTransNo="+parameters.get("machineTransNo")+" already exists. reportTranId="+parameters.get("reportTranId"));
					DataLayerMgr.executeCall(sourceConn, "FINALIZE_TRAN_IMPORT", parameters);
					sourceConn.commit();
					result = 2;
				}
				
				if (result == 0) {
					while(lineItems.next()) {
						lineItems.fillBean(parameters);
						DataLayerMgr.executeCall(targetConn, "ADD_LINE_ITEM", parameters);
					}
	
					DataLayerMgr.executeCall(sourceConn, "FINALIZE_TRAN_IMPORT", parameters);
					targetConn.commit();
					sourceConn.commit();
	
					if(log.isInfoEnabled()) {
						sb.setLength(0);
						sb.append("Imported transaction tranId: ").append(tranId).append(", tranGlobalTransCd: ").append(tranGlobalTransCd)
								.append(", import took ").append(System.currentTimeMillis() - importStartTs).append(" ms");
						log.info(sb.toString());
					}
				}
				InteractionUtils.publishTranReport(isReload, tranId, transTypeId, taskInfo.getPublisher(), log, ConvertUtils.getLong(parameters.get("reportTranId")));
				if(!StringUtils.isBlank(vzm2pLoyaltyQueueKey)) {
					switch(transTypeId) {
						case 15:
						case 16:
						case 17:
						case 18:
						case 19:
							Map<String, Object> params = new HashMap<String, Object>();
							params.put("tranId", tranId);
							try {
								DataLayerMgr.selectInto(sourceConn, "GET_LOYALTY_TRAN_DETAILS", params);
								MessageChain mc = new MessageChainV11();
								MessageChainStep step = mc.addStep(vzm2pLoyaltyQueueKey);
								for (String key: params.keySet())
									step.addLiteralAttribute(key, params.get(key));
								MessageChainService.publish(mc, taskInfo.getPublisher());
							} catch(NotEnoughRowsException e) { }
						break;
					}
				}
				okay = true;
				return 0;
			} catch(SQLException e) {
				throw DatabasePrerequisite.createServiceException("Error importing transaction tranId: " + tranId + " into REPORT database", e);
			} catch(DataLayerException e) {
				throw new RetrySpecifiedServiceException("Error importing transaction tranId: " + tranId + " into REPORT database", e, WorkRetryType.NONBLOCKING_RETRY);
			} catch(BeanException e) {
				throw new RetrySpecifiedServiceException("Error importing transaction tranId: " + tranId + " into REPORT database", e, WorkRetryType.NONBLOCKING_RETRY);
			} catch(ConvertException e) {
				throw new RetrySpecifiedServiceException("Error importing transaction tranId: " + tranId + " into REPORT database", e, WorkRetryType.NONBLOCKING_RETRY);
			} finally {
				if(!okay) {
					ProcessingUtils.rollbackDbConnection(log, sourceConn);
					ProcessingUtils.rollbackDbConnection(log, targetConn);
				}
				ProcessingUtils.closeDbConnection(log, targetConn);
			}
        } finally {
        	ProcessingUtils.closeDbConnection(log, sourceConn);
        }
	}

	public static String getVzm2pLoyaltyQueueKey() {
		return TranImportTask.vzm2pLoyaltyQueueKey;
	}

	public static void setVzm2pLoyaltyQueueKey(String vzm2pLoyaltyQueueKey) {
		TranImportTask.vzm2pLoyaltyQueueKey = vzm2pLoyaltyQueueKey;
	}
	
}
