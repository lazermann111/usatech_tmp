/**
 *
 */
package com.usatech.applayer;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import org.apache.commons.configuration.Configuration;

import simple.app.BaseWithConfig;
import simple.app.ServiceException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;

/**
 * @author Brian S. Krug
 *
 */
public class StandAloneAppLayerDB extends BaseWithConfig {
	private static final Log log = Log.getLog();

	/**
	 * @see simple.app.BaseWithConfig#execute(java.util.Map, org.apache.commons.configuration.Configuration)
	 */
	@Override
	protected void execute(Map<String, Object> argMap, Configuration config) {
		log.debug("Configurating DataSourceFactory...");
		try {
			configureDataSourceFactory(config, null);
		} catch(ServiceException e) {
			log.warn("Could not configure DataSourceFactory", e);
			finishAndExit("Could not configure DataSourceFactory", 5, e);
			return;
		}
		log.debug("Testing connection...");
		Connection conn;
		try {
			conn = DataLayerMgr.getConnection("EMBED");
		} catch(SQLException e) {
			log.warn("Could not get connection EMBED", e);
			finishAndExit("Could not get connection EMBED", 6, e);
			return;
		} catch(DataLayerException e) {
			log.warn("Could not get connection EMBED", e);
			finishAndExit("Could not get connection EMBED", 7, e);
			return;
		}
		try {
			conn.close();
		} catch(SQLException e) {
			log.info("Could not close connection. Continuing anyway.", e);
		}
		log.info("Connection is okay. Waiting for exit.");
		synchronized(this) {
			try {
				wait();
			} catch(InterruptedException e) {
				log.info("Interupt received");
			}
		}
		log.info("Exitting");
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new StandAloneAppLayerDB().run(args);
	}

}
