package com.usatech.applayer;


import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.crypto.CipherInputStream;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.InputStreamByteInput;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.results.DatasetUtils;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.BatchUpdateDatasetHandler;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.Cryption;
import com.usatech.layers.common.constants.CommonAttrEnum;

/**
 * Update the consumer_acct_cd_hash
 * 
 * @author bkrug
 * 
 */
public class UpdateAccountHashTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected ResourceFolder resourceFolder;
	protected int maxBatchSize = 100;
	
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		try {
			String resourceKey = step.getAttribute(CommonAttrEnum.ATTR_CRYPTO_RESOURCE, String.class, true);
			byte[] key = step.getAttribute(CommonAttrEnum.ATTR_CRYPTO_ENCRYPTION_KEY, byte[].class, true);
			String cipherName = step.getAttribute(CommonAttrEnum.ATTR_CRYPTO_CIPHER, String.class, true);
			int blockSize = step.getAttribute(CommonAttrEnum.ATTR_CRYPTO_BLOCK_SIZE, Integer.class, true);
			ResourceFolder rf = getResourceFolder();
			if(rf == null)
				throw new RetrySpecifiedServiceException("ResourceFolder is not set", WorkRetryType.BLOCKING_RETRY, true);
			Resource inputResource = rf.getResource(resourceKey, ResourceMode.READ);
			try {
				CipherInputStream cis = Cryption.createDecryptingInputStream(Cryption.DEFAULT_PROVIDER, cipherName, key, blockSize, inputResource.getInputStream());
				try {
					int rows = -1;
					Connection conn = DataLayerMgr.getConnection("OPER", false);
					try {
						rows = DatasetUtils.parseDataset(new InputStreamByteInput(cis), new BatchUpdateDatasetHandler(conn, "UPDATE_MISSING_ACCT_HASH", getMaxBatchSize()));
						log.info("Processed " + rows + " for account hash update");
					} finally {
						if(rows < 0 && !conn.getAutoCommit())
							conn.rollback();
						conn.close();
					}
				} finally {
					cis.close();
				}
				inputResource.delete();
			} finally {
				inputResource.release();
			}
		} catch(DataLayerException e) {
			throw new ServiceException(e);
		} catch(SQLException e) {
			throw new ServiceException(e);
		} catch(AttributeConversionException e) {
			throw new ServiceException(e);
		} catch(GeneralSecurityException e) {
			throw new ServiceException(e);
		} catch(IOException e) {
			throw new ServiceException(e);
		}
		return 0;
	}

	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}

	public int getMaxBatchSize() {
		return maxBatchSize;
	}

	public void setMaxBatchSize(int maxBatchSize) {
		this.maxBatchSize = maxBatchSize;
	}
}
