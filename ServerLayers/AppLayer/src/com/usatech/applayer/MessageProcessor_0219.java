package com.usatech.applayer;


import java.sql.Blob;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import javax.sql.rowset.serial.SerialBlob;

import simple.app.DialectResolver;
import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.ByteArrayUtils;
import simple.io.Log;
import simple.text.StringUtils;

import com.usatech.app.MessageChainTaskInfo;
import com.usatech.ec.ECResponse;
import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.CommonProcessing;
import com.usatech.layers.common.DeviceInfo;
import com.usatech.layers.common.LegacyUtils;
import com.usatech.layers.common.Message;
import com.usatech.layers.common.MessageResponse;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.CredentialResult;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.messagedata.MessageData_0219;
import com.usatech.layers.common.messagedata.MessageData_030E;

/**
 * Process Updates Request
 * 
 * @author bkrug
 * 
 */
public class MessageProcessor_0219 extends CommonProcessing implements MessageProcessor {
	protected long credentialMaxAgeMs;
	protected long newCredentialMaxAgeMs;

	@Override
	public MessageResponse processMessage(MessageChainTaskInfo taskInfo, Message message) throws ServiceException {
		MessageData_0219 message0219 = (MessageData_0219) message.getData();
		MessageData_030E reply030E = message0219.createResponse();
		Log log = message.getLog();
		DeviceInfo deviceInfo = message.getDeviceInfo();
		Connection conn = null;
		Long commandId;
		try {
			conn = AppLayerUtils.getConnection(false);
			Map<String, Object> params = new HashMap<String, Object>();
			deviceInfo.refreshSafely();
			params.put("deviceName", deviceInfo.getDeviceName());
			boolean updatedProtocolVersion = message0219.getProtocolVersion() > 0 && (deviceInfo.getPropertyListVersion() == null || deviceInfo.getPropertyListVersion() != message0219.getProtocolVersion());
			if(!StringUtils.isBlank(message0219.getAppType()) || !StringUtils.isBlank(message0219.getAppVersion()) || updatedProtocolVersion) {
				params.put("appType", message0219.getAppType());
				params.put("appVersion", message0219.getAppVersion());
				params.put("procotolVersion", message0219.getProtocolVersion());
				DataLayerMgr.executeUpdate(conn, "UPDATE_APP_VERSION", params);
				if(updatedProtocolVersion) {
					deviceInfo.setPropertyListVersion(message0219.getProtocolVersion());
					deviceInfo.commitChanges(message.getServerTime());
				}

			}
			params.put("maxExecuteOrder", ProcessingConstants.MAX_COMMAND_EXECUTE_ORDER);
			params.put("sessionAttributes", ConvertUtils.getStringSafely(message.getSessionAttribute(LegacyUtils.PENDING_COMMAND_STATE_ATTR), ""));
			params.put("globalSessionCode", message.getGlobalSessionCode());
			params.put("updateStatus", message0219.getUpdateStatus());
			params.put("v4Messages", 'N');
			DataLayerMgr.executeUpdate(conn, "UPDATE_PENDING_COMMAND_NEXT", params);
			if(log.isInfoEnabled())
				log.info("Got results from UPDATE_PENDING_COMMAND_NEXT: " + params);
			message.setSessionAttribute(LegacyUtils.PENDING_COMMAND_STATE_ATTR, ConvertUtils.getStringSafely(params.get("sessionAttributes"), ""));

			commandId = ConvertUtils.convertSafely(Long.class, params.get("commandId"), null);
			if(commandId == null) {
				reply030E.setReturnCode(ECResponse.RES_OK_NO_UPDATE);
				reply030E.setReturnMessage("No updates");
			} else {
				String commandDataType = ConvertUtils.getString(params.get("dataType"), true);
				MessageType messageType = MessageType.getByValue(ByteArrayUtils.fromHex(commandDataType), 0);
				switch(messageType) {
					case FILE_XFER_START_2_0:
					case FILE_XFER_START_3_0:
						// expect final 7F/A7
						String fileName = ConvertUtils.getString(params.get("fileName"), true);
						int fileTypeId = ConvertUtils.getInt(params.get("fileTypeId"));
						Blob fileContent = null;
						if (!DialectResolver.isOracle()) {
							byte[] filebytes = ConvertUtils.convertSafely(byte[].class,params.get("fileContent"), null);
							fileContent = new SerialBlob(filebytes);
						} else {
							fileContent = ConvertUtils.convert(Blob.class, params.get("fileContent"));
						}
						// Calendar fileCreateTime = ConvertUtils.convert(Calendar.class, params.get("fileCreateTime"));
						int filePacketSize = ConvertUtils.getInt(params.get("filePacketSize"), defaultFilePacketSize);
						int fileGroupNum = message.getData().getMessageNumber();// use messageNumber for group number
						long fileTransferId = ConvertUtils.getLong(params.get("fileTransferId"));
						long fileSize = fileContent.length();
						message.addFileTransfer(false, fileTransferId, commandId, fileGroupNum, fileName, fileTypeId, filePacketSize, fileSize);

						reply030E.setFileName(fileName);
						reply030E.setFileType(fileTypeId);
						reply030E.setFileSize(fileSize);
						reply030E.setReturnCode(ECResponse.RES_OK);
						message.setSessionAttribute(LegacyUtils.LAST_PENDING_COMMAND_SENT, commandId);
						message.getLog().info("Sending " + reply030E + " for command " + commandId);
						break;
					default:
						// nothing else is supported
						params.clear();
						params.put("pendingCommandId", commandId);
						DataLayerMgr.executeUpdate(conn, "UPDATE_PENDING_COMMAND_CANCEL", params);
						reply030E.setReturnCode(ECResponse.RES_OK_NO_UPDATE);
						reply030E.setReturnMessage("No updates");
						message.sendReply(reply030E);
						break;
				}
			}
			CredentialResult credentialResult = taskInfo.getStep().getAttribute(CommonAttrEnum.ATTR_CREDENTIAL_RESULT, CredentialResult.class, false);
			AppLayerUtils.checkDevicePassword(deviceInfo, credentialResult, message0219.getUsername(), message0219.getPassword(), credentialMaxAgeMs, newCredentialMaxAgeMs, conn, reply030E);
			conn.commit();
		} catch(ServiceException e) {
			message.getLog().warn("Could not process message", e);
			ProcessingUtils.rollbackDbConnection(log, conn);
			reply030E.setReturnMessage("Error");
		} catch(Exception e) {
			message.getLog().warn("Could not process message", e);
			ProcessingUtils.rollbackDbConnection(log, conn);
			reply030E.setReturnMessage("Error");
		} finally {
			ProcessingUtils.closeDbConnection(log, conn);
		}
		message.sendReply(reply030E);
		return MessageResponse.CONTINUE_SESSION;
	}

	public long getNewCredentialMaxAgeMs() {
		return newCredentialMaxAgeMs;
	}

	public void setNewCredentialMaxAgeMs(long newCredentialMaxAgeMs) {
		this.newCredentialMaxAgeMs = newCredentialMaxAgeMs;
	}

	public long getCredentialMaxAgeMs() {
		return credentialMaxAgeMs;
	}

	public void setCredentialMaxAgeMs(long credentialMaxAgeMs) {
		this.credentialMaxAgeMs = credentialMaxAgeMs;
	}

}


