package com.usatech.applayer;

import java.sql.Connection;
import java.sql.SQLException;

import simple.app.DatabasePrerequisite;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.BeanException;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.task.ExecuteDbCallTask;
import com.usatech.app.task.SQLExceptionHandler;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.constants.AuthorityAttrEnum;

public class UpdateAuthorizationTask extends ExecuteDbCallTask {
	private static final Log log = Log.getLog();
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		if(step.getAttributeSafely(AuthorityAttrEnum.ATTR_CONSUMER_ACCT_ID, Long.class, null) == null && step.getAttributeSafely(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, Long.class, null) != null) {
			try {
				DataLayerMgr.executeCall("GET_CONSUMER_ACCT_FOR_TRAN", step.getAttributes(), true);
			} catch(SQLException e) {
				SQLExceptionHandler handler = getSqlExceptionHandler();
				if(handler != null)
					handler.handleSQLException("GET_CONSUMER_ACCT_FOR_TRAN", step.getAttributes(), e);
				else
					throw new RetrySpecifiedServiceException("Could not get consumerAcctId for the auth", e, WorkRetryType.NONBLOCKING_RETRY); // TODO: maybe blocking retry would be better for some of these
			} catch(DataLayerException e) {
				throw new RetrySpecifiedServiceException("Could not get consumerAcctId for the auth", e, WorkRetryType.NONBLOCKING_RETRY);
			}
		}
		super.process(taskInfo);
		Long tranId;
		Character tranImportNeeded;
		try {
			tranId = step.getAttribute(AuthorityAttrEnum.ATTR_TRAN_ID, Long.class, false);
			tranImportNeeded = step.getAttribute(AuthorityAttrEnum.ATTR_TRAN_IMPORT_NEEDED, Character.class, false);
		} catch(AttributeConversionException e) {
			throw new RetrySpecifiedServiceException(e, WorkRetryType.NO_RETRY);
		}
		if(tranId != null && tranId > 0 && tranImportNeeded != null && tranImportNeeded == 'Y') {
			InteractionUtils.publishTranImport(tranId, taskInfo.getPublisher(), log);
			try {
				Connection conn = DataLayerMgr.getConnection("OPER");
				try {
					InteractionUtils.checkForReplenishBonuses(tranId, conn, taskInfo.getPublisher(), log);
					InteractionUtils.publishCheckTerminal(tranId, conn, taskInfo.getPublisher(), log);
				} finally {
					conn.close();
				}
			} catch(SQLException e) {
				SQLExceptionHandler handler = getSqlExceptionHandler();
				if(handler != null)
					handler.handleSQLException("", "", e);
				else
					throw DatabasePrerequisite.createServiceException(e);
			} catch(DataLayerException | BeanException e) {
				throw new ServiceException(e);
			}
		}
		return 0;
	}
}
