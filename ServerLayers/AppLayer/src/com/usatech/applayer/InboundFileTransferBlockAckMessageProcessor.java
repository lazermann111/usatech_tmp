package com.usatech.applayer;

import java.sql.Blob;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import simple.app.DatabasePrerequisite;
import simple.app.DialectResolver;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.IOUtils;
import simple.io.Log;
import simple.results.Results;

import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.Message;
import com.usatech.layers.common.MessageResponse;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.messagedata.FileTransferMessageData;
import com.usatech.layers.common.messagedata.MessageData_7E;
import com.usatech.layers.common.messagedata.MessageData_A6;
import com.usatech.layers.common.messagedata.MessageData_C9;

public class InboundFileTransferBlockAckMessageProcessor implements MessageProcessor {
	private static final Log log = Log.getLog();
	
	protected static int fileChunkMaxLength;

	public MessageResponse processMessage(MessageChainTaskInfo taskInfo, Message message) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		Map<String, Object> attributes = step.getAttributes();		
		try {
			long fileTransferId = ConvertUtils.getLong(attributes.get("fileTransferId"));
			long offset = ConvertUtils.getLong(attributes.get("fileBlockOffset"));
			int length = ConvertUtils.getInt(attributes.get("fileBlockLength"));
			Map<String,Object> params = new HashMap<String, Object>();
			byte[] fileChunk;
			params.put("fileTransferId", fileTransferId);
			if (!DialectResolver.isOracle()) {
				Results rs = DataLayerMgr.executeQuery("GET_FILE_CONTENT", params, false);
				if (rs.next()) {
					byte[] fileContent = ConvertUtils.convert(byte[].class, rs.get("fileContent"));
					if (fileContent == null || fileContent.length < 1)
						throw new ServiceException("Unable to read file content for fileTransferId: " + fileTransferId);
					fileChunk = new byte[fileChunkMaxLength];
					int arrayOffset = (int) (offset - 1);
					System.arraycopy(fileContent, arrayOffset, fileChunk, 0,
							Math.min(fileContent.length - arrayOffset, fileChunkMaxLength));
				} else
					throw new ServiceException("Unable to read file chunk for fileTransferId: " + fileTransferId
							+ ", offset: " + offset + ", length: " + fileChunkMaxLength);
			} else {
				DataLayerMgr.executeCall("GET_FILE_CONTENT", params, false);
				Blob fileContent = ConvertUtils.convert(Blob.class, params.get("fileContent"));
				if (fileContent == null || fileContent.length() < 1)
					throw new ServiceException("Unable to read file content for fileTransferId: " + fileTransferId);
				fileChunk = fileContent.getBytes(offset, fileChunkMaxLength);
				if (fileChunk == null || fileChunk.length < 1)
					throw new ServiceException("Unable to read file chunk for fileTransferId: " + fileTransferId
							+ ", offset: " + offset + ", length: " + fileChunkMaxLength);
			}
			byte group = ConvertUtils.getByte(attributes.get("fileGroup"));
			int packetNum = ConvertUtils.getInt(attributes.get("filePacket"));
			FileTransferMessageData replyFT;
			MessageType messageType = message.getData().getMessageType();
			switch(messageType) {
				case FILE_XFER_START_ACK_2_0: //0x7D:
				case FILE_XFER_ACK_2_0: //0x7F:
					MessageData_7E reply7E = new MessageData_7E();
					reply7E.setGroup(group);
					reply7E.setPacketNum(packetNum);
					replyFT = reply7E;
					break;
				case FILE_XFER_START_ACK_3_0: //(byte)0xA5:
				case FILE_XFER_ACK_3_0: //(byte)0xA7:
					MessageData_A6 replyA6 = new MessageData_A6();
					replyA6.setGroup(group);
					replyA6.setPacketNum(packetNum);
					replyFT = replyA6;
					break;
				case GENERIC_RESPONSE_4_1: //(byte)0xCB:
					MessageData_C9 replyC9 = new MessageData_C9();
					replyC9.setPacketNum(packetNum);
					replyFT = replyC9;
					break;
				default:
					throw new ServiceException("Invalid configuration; This class cannot handle message '" + messageType + "'");
			}
			if (length > 0) {
				if (fileChunk.length <= length)
					replyFT.setContent(fileChunk);
				else {
					byte[] fileBlock = new byte[length];
					System.arraycopy(fileChunk, 0, fileBlock, 0, length);
					replyFT.setContent(fileBlock);
				}
			} else
				replyFT.setContent(IOUtils.EMPTY_BYTES);
			message.setResultAttribute("fileTransferId", fileTransferId);
			message.setResultAttribute("fileGroup", group);
			message.setResultAttribute("fileChunk", fileChunk);
			message.setResultAttribute("fileChunkOffset", offset);
			message.sendReply(replyFT);
		} catch(SQLException e) {
			if(DatabasePrerequisite.determineRetryType(e) == WorkRetryType.BLOCKING_RETRY)
				throw new RetrySpecifiedServiceException(e, WorkRetryType.BLOCKING_RETRY);
			log.warn("Error while trying to process file transfer ACK", e);
			return MessageResponse.CLOSE_SESSION;
		} catch(DataLayerException e) {
			throw new RetrySpecifiedServiceException(e, WorkRetryType.BLOCKING_RETRY);
		} catch(Exception e) {
			log.warn("Error while trying to process file transfer ACK", e);
			return MessageResponse.CLOSE_SESSION;
		}
		return MessageResponse.CONTINUE_SESSION;
	}
	
	public static int getFileChunkMaxLength() {
		return fileChunkMaxLength;
	}
	public static void setFileChunkMaxLength(int fileChunkMaxLength) {
		InboundFileTransferBlockAckMessageProcessor.fileChunkMaxLength = fileChunkMaxLength;
	}
}
