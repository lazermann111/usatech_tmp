package com.usatech.applayer;

import simple.app.ServiceException;

import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.LegacyInitMessageProcessor;
import com.usatech.layers.common.LegacyUpdateStatusProcessor;
import com.usatech.layers.common.LegacyUtils;
import com.usatech.layers.common.Message;
import com.usatech.layers.common.MessageResponse;
import com.usatech.layers.common.constants.DeviceType;

public class MessageProcessor_75 extends LegacyInitMessageProcessor implements MessageProcessor {
	
	@Override
	public MessageResponse processMessage(MessageChainTaskInfo taskInfo, Message message) throws ServiceException {
		LegacyUtils.processAckMessage(message);
		// to see if more pending message, if so, send it
		if(message.getDeviceInfo().getDeviceType()==DeviceType.ESUDS){
        	return LegacyUpdateStatusProcessor.processMessage(taskInfo, message, true);
        }
		return MessageResponse.CONTINUE_SESSION;
	}
}
