package com.usatech.applayer;

import java.sql.Blob;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.Log;

import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.DeviceInfo;
import com.usatech.layers.common.Message;
import com.usatech.layers.common.MessageProcessingUtils;
import com.usatech.layers.common.MessageResponse;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.FileType;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.messagedata.MessageData_9B;

public class MessageProcessor_9B implements MessageProcessor {
	public MessageResponse processMessage(MessageChainTaskInfo taskInfo, Message message) throws ServiceException {
		Log log = message.getLog();
		MessageData_9B data = (MessageData_9B)message.getData();
		String fileName=data.getFileName();
		DeviceInfo deviceInfo=message.getDeviceInfo();
		DeviceType deviceType=deviceInfo.getDeviceType();
		Map<String,Object> params = new HashMap<String, Object>();
		
		MessageType fileTransferMessageType;
		int filePacketSize;
		if(deviceType==DeviceType.KIOSK||deviceType==DeviceType.T2){
			fileTransferMessageType=MessageType.FILE_XFER_START_3_0;
			filePacketSize=1024;
		}else{
			fileTransferMessageType=MessageType.FILE_XFER_START_2_0;
			filePacketSize=256;
		}
		int fileTypeId=FileType.CONFIGURATION_FILE.getValue();//only config is allowed for 9B
		Connection conn = null;
		try {
			conn = AppLayerUtils.getConnection(false);
			params.put("deviceName", deviceInfo.getDeviceName());
            params.put("fileName", data.getFileName());
            params.put("dataType", fileTransferMessageType.getHex());
            params.put("fileType", fileTypeId);
            params.put("filePacketSize", filePacketSize);
            DataLayerMgr.executeCall(conn, "GET_DEVICE_FILE_TRANSFER", params);
            int fileNameExists = ConvertUtils.getInt(params.get("fileNameExists"));
			int transferPendingExists = ConvertUtils.getInt(params.get("transferPendingExists"));
			if(fileNameExists>0){
				if(transferPendingExists>0){
					log.info("A pending outgoing file transfer already exists for file name:"+fileName+" device:"+deviceInfo.getDeviceName());
				}else{
					//add pending file transfer and write file transfer start
					byte fileGroupNum=ConvertUtils.getByte(params.get("fileGroupNum"));
					Blob fileContent = ConvertUtils.convert(Blob.class, params.get("fileContent"));
					long fileTransferId = ConvertUtils.getLong(params.get("fileTransferId"));
					long pendingCommandId = ConvertUtils.getLong(params.get("commandId"));
					MessageProcessingUtils.writeFileTransferStart(message, fileTransferId, pendingCommandId, fileGroupNum, fileName, fileTypeId, filePacketSize, null, fileContent, fileTransferMessageType, null);
					message.setNextCommandId(pendingCommandId);
				}
			}else{
				log.info("Invalid file transfer request file name:"+fileName);
			}
			conn.commit();
		} catch(Exception e) {
        	ProcessingUtils.rollbackDbConnection(log, conn);
        	throw new ServiceException(e);
        } finally {
        	ProcessingUtils.closeDbConnection(log, conn);
        }
		return MessageResponse.CONTINUE_SESSION;
	}
}
