package com.usatech.applayer;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;

import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.LegacyUpdateStatusProcessor;
import com.usatech.layers.common.LegacyUtils;
import com.usatech.layers.common.Message;
import com.usatech.layers.common.MessageResponse;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.messagedata.MessageData_9A62;
import com.usatech.layers.common.messagedata.MessageData_9A62.DiagBlockData;
/**
 * washer_dryer_diagnostics
 * @author yhe
 *
 */
public class MessageProcessor_9A62 implements MessageProcessor {
    public MessageResponse processMessage(MessageChainTaskInfo taskInfo, Message message) throws ServiceException {
        Log log = message.getLog();
        MessageData_9A62 data = (MessageData_9A62)message.getData();

        Connection conn = null;
        try {
            conn = AppLayerUtils.getConnection(false);
            //remove any pending diagnostic request messages
            String deviceName=message.getDeviceInfo().getDeviceName();
            Map<String,Object> params = new HashMap<String, Object>();
            LegacyUtils.updatePendingCommandSent(conn, deviceName, "9A61");
            params.put("deviceName", deviceName);
            params.put("effectiveTime", message.getServerTime());
			DataLayerMgr.selectInto("GET_DEVICE_ID", params);
            params.put("hostPortNum", data.getPortNumber());
            Results result= DataLayerMgr.executeQuery(conn, "GET_HOST_BY_PORT", params);
            if(result.next()) {
            	int hostId= ConvertUtils.getInt(result.getValue("hostId"));
            	params.clear();
                params.put("hostId", hostId);
                Results hostDiagResult= DataLayerMgr.executeQuery(conn, "GET_HOST_DIAG_CD", params);
                Map<String, Integer> activeMap=new HashMap<String, Integer>();
                while(hostDiagResult.next()){
                	activeMap.put(ConvertUtils.getStringSafely(hostDiagResult.getValue("hostDiagCd")), ConvertUtils.getInt(hostDiagResult.getValue("hostDiagStatusId")));
                }
                
                List<DiagBlockData> diagList = data.getDiagBlocks();
                for(DiagBlockData diag: diagList) {
                	String hostDiagCd=String.valueOf(diag.getHostDiagCode());
                	if(activeMap.containsKey(hostDiagCd)) {
                		//update
                		params.clear();
                		params.put("hostDiagStatusId", activeMap.get(hostDiagCd));
                		DataLayerMgr.executeCall(conn, "UPDATE_HOST_DIAG_STATUS", params);
                		activeMap.remove(hostDiagCd);
                	} else {
                		//insert
                		params.clear();
                		params.put("hostId", hostId);
                		params.put("hostDiagCd", hostDiagCd);
                		DataLayerMgr.executeCall(conn, "INSERT_HOST_DIAG_STATUS", params);
                		log.info("New Code: Port:"+data.getPortNumber()+" Code:"+hostDiagCd);
                	}
                }
                //anything left in the hash is a cleared code!
                for(Integer hostDiagStatusId :activeMap.values()) {
                	//update
                	params.clear();
            		params.put("hostDiagStatusId", hostDiagStatusId);
            		DataLayerMgr.executeCall(conn, "UPDATE_HOST_DIAG_STATUS_CLEARED", params);
                }
            } else {
            	log.info("No host record found for deviceId:"+params.get("deviceId")+", port:"+data.getPortNumber()+", position:0.Request room layout.");
            	LegacyUtils.requestRoomLayout(conn, deviceName);
            }
            conn.commit();
        	return LegacyUpdateStatusProcessor.processMessage(taskInfo, message, true);
        } catch(Exception e) {
        	ProcessingUtils.rollbackDbConnection(log, conn);
            log.error("Error processing message", e);
        } finally {
        	ProcessingUtils.closeDbConnection(log, conn);
        }
        return MessageResponse.CLOSE_SESSION;
    }
}
