package com.usatech.applayer;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import simple.app.DatabasePrerequisite;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.Log;

import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.LegacyUtils;
import com.usatech.layers.common.OfflineMessage;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.BooleanType;
import com.usatech.layers.common.constants.EventCodePrefix;
import com.usatech.layers.common.constants.EventDetailType;
import com.usatech.layers.common.constants.EventState;
import com.usatech.layers.common.constants.EventType;
import com.usatech.layers.common.messagedata.MessageData_C6;

public class MessageProcessor_C6 implements OfflineMessageProcessor {
	public void processMessage(MessageChainTaskInfo taskInfo, OfflineMessage message) throws ServiceException {
        Log log = message.getLog();
        MessageData_C6 data = (MessageData_C6)message.getData();
        String deviceName = message.getDeviceName();
        Calendar eventTs = data.getEventTime();
        long eventUtcTsMs = eventTs.getTimeInMillis();
        long messageStartTimeMs = message.getServerTime();
        if(eventUtcTsMs < messageStartTimeMs - AppLayerUtils.getValidDeviceTimeDays()*24*60*60*1000L || eventUtcTsMs > messageStartTimeMs) {
			log.warn("Received invalid event timestamp: " + eventUtcTsMs + ", using server message start time: " + messageStartTimeMs);
			eventUtcTsMs = messageStartTimeMs;
        }        
        int eventUtcOffsetMs = eventTs.get(Calendar.ZONE_OFFSET);
        long eventLocalTsMs = eventUtcTsMs + eventUtcOffsetMs;
        Map<String, Object> params = new HashMap<String, Object>();
        long eventId;
        Connection conn;
        try {
			conn = DataLayerMgr.getConnection("OPER");
		} catch(Exception e) {
			throw new RetrySpecifiedServiceException("Could not get connection for data source OPER", e, WorkRetryType.BLOCKING_RETRY);
		}
		boolean okay = false;
        try {
            eventId = AppLayerUtils.getOrCreateEvent(log, conn, params, EventCodePrefix.APP_LAYER.getValue(), message.getGlobalSessionCode(), deviceName, data.getComponentNumber(), data.getEventType().getValue(), -1, EventState.COMPLETE_FINAL, Long.toString(data.getEventId()), BooleanType.TRUE, BooleanType.TRUE, eventLocalTsMs, eventLocalTsMs);
            long hostId = ConvertUtils.getLongSafely(params.get("hostId"), -1);
            AppLayerUtils.getOrCreateEventDetail(log, conn, params, eventId, EventDetailType.HOST_EVENT_TIMESTAMP, null, eventLocalTsMs);
            AppLayerUtils.getOrCreateEventDetail(log, conn, params, eventId, EventDetailType.EVENT_DATA, data.getEventData(), -1);
            if (data.getEventType() == EventType.COMPONENT_DETAILS && hostId > 0) {
				Map<String, String> componentParams = new HashMap<String, String>();
        		LegacyUtils.parseMultitechModemInfoNew(componentParams, data.getEventData());
        		Map<String,Object> subParams = new HashMap<String, Object>(); 
        		for (String key : componentParams.keySet())
					AppLayerUtils.upsertHostSetting(conn, subParams, hostId, key, componentParams.get(key));
            }
			// forward into alerting fabric
			AppLayerUtils.getOrCreateAlertForEvent(log, conn, params, eventId, eventUtcTsMs);
            conn.commit();
            okay = true;
			log.info("Recorded event in processing database");
        } catch(SQLException e) {
            throw DatabasePrerequisite.createServiceException(e);
        } catch(Exception e) {
            throw new ServiceException(e);
        } finally {
        	if(!okay)
        		ProcessingUtils.rollbackDbConnection(log, conn);
        	ProcessingUtils.closeDbConnection(log, conn);
        }
    }
}
