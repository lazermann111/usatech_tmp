package com.usatech.applayer;

import simple.app.ServiceException;

import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.LegacyInitMessageProcessor;
import com.usatech.layers.common.Message;
import com.usatech.layers.common.MessageResponse;
import com.usatech.layers.common.messagedata.InitMessageData;

public class MessageProcessor_AD extends LegacyInitMessageProcessor implements MessageProcessor {
	@Override
	public MessageResponse processMessage(MessageChainTaskInfo taskInfo, Message message) throws ServiceException {
		return AppLayerUtils.processLegacyInit(message, (InitMessageData) message.getData(), message.getLog(), encryptionKeyMinAgeMin, deviceReactivationOnReinit, initEmailEnabled, emailQueueKey, initEmailTo, initEmailFrom);
	}
}
