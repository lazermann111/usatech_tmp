package com.usatech.applayer;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Map;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.io.resource.ResourceFolder;
import simple.translator.TranslatorFactory;
import simple.util.concurrent.LockSegmentCache;
import simple.util.concurrent.SegmentCache;

import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.AppLayerOnlineMessage;
import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.MessageResponse;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.SessionCloseReason;

public class AppLayerTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected static final String MESSAGE_PROCESSOR_CLASSNAME_PREFIX = MessageProcessor.class.getName() + "_";
	protected final SegmentCache<String, MessageProcessor, ServiceException> messageProcessorCache = new LockSegmentCache<String, MessageProcessor, ServiceException>() {
		@Override
		protected MessageProcessor createValue(String key, Object... additionalInfo)
				throws ServiceException {
			String className = MESSAGE_PROCESSOR_CLASSNAME_PREFIX + key;
			Class<? extends MessageProcessor> clazz;
			try {
				clazz = Class.forName(className).asSubclass(MessageProcessor.class);
			} catch(ClassNotFoundException e) {
				throw new ServiceException("Could not find message processor for message type '" + key + "'. Attempted to load class '" + className + "'");
			} catch(ClassCastException e) {
				throw new ServiceException("Class '" + className + "' is not a subclass of " + MessageProcessor.class.getName());
			}
			try {
				return clazz.newInstance();
			} catch(InstantiationException e) {
				throw new ServiceException("Could not instantiate an instance of " + className, e);
			} catch(IllegalAccessException e) {
				throw new ServiceException("Could not instantiate an instance of " + className, e);
			}
		}

		@Override
		protected boolean keyEquals(String key1, String key2) {
			return ConvertUtils.areEqual(key1, key2);
		}

		@Override
		protected boolean valueEquals(MessageProcessor value1, MessageProcessor value2) {
			return ConvertUtils.areEqual(value1, value2);
		}
	};

	protected final ThreadLocal<ByteBuffer> byteBufferLocals = new ThreadLocal<ByteBuffer>() {
		@Override
		protected ByteBuffer initialValue() {
			ByteBuffer writeBuffer = ByteBuffer.allocate(getMaxResponseSize());
			writeBuffer.order(ByteOrder.BIG_ENDIAN);
			return writeBuffer;
		}
	};
	protected int maxResponseSize = 2048;
	protected TranslatorFactory translatorFactory;
	protected String translatorContext;
	protected ResourceFolder resourceFolder;
	protected String outboundFileTransferDirectory;
	protected AppLayerDeviceInfoManager deviceInfoManager;
	protected boolean offline;

	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		taskInfo.setNextQos(taskInfo.getCurrentQos());
		if(ProcessingUtils.isNetlayerWaiting(taskInfo))
			ProcessingUtils.checkMessageExpiration(taskInfo);
		ResourceFolder rf = getResourceFolder();
		if(rf == null)
			throw new ServiceException("ResourceFolder is not set on " + this);
		Map<String, Object> resultAttributes = taskInfo.getStep().getResultAttributes();
		try {
			ByteBuffer buffer = byteBufferLocals.get();
			AppLayerOnlineMessage message = AppLayerUtils.constructMessage(taskInfo, buffer, getDeviceInfoManager(), getTranslatorFactory(), getTranslatorContext(), rf, getOutboundFileTransferDirectory());
			if(message.getDeviceInfo() == null) {
				// device is not longer active - tell Netlayer
				log.info("Device '" + message.getDeviceName() + "' is no longer active. Telling NetLayer to close session and remove the device info from cache");
				resultAttributes.put("endSession", SessionCloseReason.INACTIVE_DEVICE);
				resultAttributes.put("removeDeviceInfo", true);
				return 5;
			}
			final MessageProcessor mp;
			try {
				mp = getMessageProcessor(message.getData().getMessageType().getHex());
			} catch(ServiceException e) {
				throw new RetrySpecifiedServiceException("Could not find message processor for message type " + message.getData().getMessageType(), e, WorkRetryType.NO_RETRY);
			}
			MessageResponse response = mp.processMessage(taskInfo, message);
			switch(response) {
				case CLOSE_SESSION: resultAttributes.put("endSession", SessionCloseReason.ERROR); break;
			}
			return message.complete();
		} catch(BufferUnderflowException e) {
			throw new RetrySpecifiedServiceException("Message data is not long enough. Invalid message", e, WorkRetryType.NO_RETRY);
		}
	}

	public MessageProcessor getMessageProcessor(String hexMessageType) throws ServiceException {
		return messageProcessorCache.getOrCreate(hexMessageType);
	}

	public void setMessageProcessor(String hexMessageType, MessageProcessor messageProcessor) {
		messageProcessorCache.put(hexMessageType, messageProcessor);
	}

	public int getMaxResponseSize() {
		return maxResponseSize;
	}

	public void setMaxResponseSize(int maxResponseSize) {
		this.maxResponseSize = maxResponseSize;
	}

	public TranslatorFactory getTranslatorFactory() {
		return translatorFactory;
	}

	public void setTranslatorFactory(TranslatorFactory translatorFactory) {
		this.translatorFactory = translatorFactory;
	}

	public String getTranslatorContext() {
		return translatorContext;
	}

	public void setTranslatorContext(String translatorContext) {
		this.translatorContext = translatorContext;
	}

	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}

	public String getOutboundFileTransferDirectory() {
		return outboundFileTransferDirectory;
	}

	public void setOutboundFileTransferDirectory(String outboundFileTransferDirectory) {
		this.outboundFileTransferDirectory = outboundFileTransferDirectory;
	}

	public AppLayerDeviceInfoManager getDeviceInfoManager() {
		return deviceInfoManager;
	}

	public void setDeviceInfoManager(AppLayerDeviceInfoManager deviceInfoManager) {
		this.deviceInfoManager = deviceInfoManager;
	}

	public boolean isOffline() {
		return offline;
	}

	public void setOffline(boolean offline) {
		this.offline = offline;
	}
}
