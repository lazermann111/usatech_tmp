package com.usatech.applayer;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import simple.app.ServiceException;
import simple.db.DataLayerMgr;
import simple.io.Log;

import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.LegacyInitMessageProcessor;
import com.usatech.layers.common.LegacyUtils;
import com.usatech.layers.common.Message;
import com.usatech.layers.common.MessageResponse;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.messagedata.MessageData_99;
/**
 * So far only eSuds is using this. Request sent during init.
 * @author yhe
 *
 */
public class MessageProcessor_99 extends LegacyInitMessageProcessor implements MessageProcessor {
	private static final String[] eSudsVersionLayout={
		"eSuds Application Version", 
		"Multiplexor Application Version", 
		"Display Firmware Version", 
		"Root File System Version",
		"Init File System Version",
		"eSuds File System Version", 
		"MAC Address",
		"Multiplexor Bootloader Version"};
	@Override
	public MessageResponse processMessage(MessageChainTaskInfo taskInfo, Message message) throws ServiceException {
		Log log = message.getLog();
		//ack 83 requestNumber 11 so far only eSuds
		LegacyUtils.processAckMessage(message);
		if(message.getDeviceInfo().getDeviceType()==DeviceType.ESUDS){
			//update device settings
			MessageData_99 data = (MessageData_99)message.getData();
			String[] versionString = data.getVersionString();
			Connection conn = null;
			Map<String,Object> params = new HashMap<String, Object>();
			try {
				conn = AppLayerUtils.getConnection(false);
				params.put("deviceName", message.getDeviceInfo().getDeviceName());
				params.put("effectiveTime", message.getServerTime());
				DataLayerMgr.selectInto("GET_DEVICE_ID", params);
				
				for(int i = 0; i < versionString.length && i < eSudsVersionLayout.length; i++) {
					params.put("deviceSettingParameterCd", eSudsVersionLayout[i]);
					params.put("deviceSettingValue", versionString[i]);
					DataLayerMgr.executeCall(conn, "UPSERT_DEVICE_SETTING", params);
				}
				conn.commit();
			} catch(ServiceException e) {
	        	ProcessingUtils.rollbackDbConnection(log, conn);
	            throw e;
	        } catch(Exception e) {
	        	ProcessingUtils.rollbackDbConnection(log, conn);
	        	throw new ServiceException(e);
	        } finally {
	        	ProcessingUtils.closeDbConnection(log, conn);
	        }
		}
		return MessageResponse.CONTINUE_SESSION;
	}
}
