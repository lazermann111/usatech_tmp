/** 
 * USA Technologies, Inc. 2011
 * AbstractDexProcessingStrategy.java by phorsfield, Aug 16, 2011 4:40:02 PM
 */
package com.usatech.applayer;

import java.io.IOException;
import java.io.LineNumberReader;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.NotEnoughRowsException;
import simple.io.Log;
import simple.results.BeanException;

/**
 * Common functionality
 * @author phorsfield
 *
 */
public abstract class AbstractDexProcessingStrategy implements DexProcessingStrategy {

	private static final Log log = Log.getLog();
	protected static final String DEX_HEADER_SEPARATOR = "-";

	@Override
	public abstract State writeWholeDexToDatabase(State state)
			throws SQLException, DataLayerException, BeanException,
			ConvertException;

	@Override
	public abstract State readDatabaseInfo(State state,
			LineNumberReader in) throws SQLException, DataLayerException,
			BeanException, ConvertException, ParseException, IOException;

	@Override
	public abstract LineHandler getLineHandler(State state);

	@Override
	public abstract State cleanupFileContentForDatabase(State state,
			String dirtyFileContent);

	@Override
	public abstract State process(State state, LineHandler handler)
			throws IOException, ParseException, SQLException, DataLayerException;

	@Override
	public abstract State disposition(String fileName, String fileContent, long fileTransferId);

	@Override
	public State readDexFileInfo(State state, LineNumberReader in, Map<String, Object> attributes)
			throws IOException {
		String dexFileName = in.readLine();

		String[] info = dexFileName.split(DEX_HEADER_SEPARATOR);
		state.setHeaders(info);
			
		// Construct file name
		String deviceSerialCd = ConvertUtils.getStringSafely(attributes.get("deviceSerialCd"), info[1]);
		StringBuilder sb = new StringBuilder(dexFileName.length());
		sb.append(info[0]).append(DEX_HEADER_SEPARATOR).append(deviceSerialCd); // Use the deviceSerialCd and not what the device sent
		for(int i = 2; i < 5; i++) {
			sb.append(DEX_HEADER_SEPARATOR);
			if(info.length > i && info[i] != null)
				sb.append(info[i]);
		}
		sb.append(".log");
			    
		state.setEportSerialNum(deviceSerialCd);
		state.setDexFileName(sb.toString());
				
		return state;
	}

	/**
	 * Reads the previous headers, and one additional line
	 * @param state current strategy state
	 * @param in file reader
	 * @param fileTransferId file transfer id attribute
	 * @return one more line read from the reader
	 * @throws ParseException
	 * @throws IOException
	 */
	protected String extractDexDateAndTypeFromHeaders(State state,
			LineNumberReader in) throws ParseException, IOException {
				String[] info = state.getHeaders();        
				Date dexDate;
				
				DateFormat df = new SimpleDateFormat("MMddyy HHmmss");
				try {
					dexDate = df.parse(info[3] + " " + info[2]);
				} catch(ParseException pe) {
					throw new ParseException("Could not parse the date in 'DEX-' header, skipping file.", pe.getErrorOffset());
				} 
				int dexType = 0;
				if(info.length > 4 && info[4] != null) {
					if(info[4].equalsIgnoreCase("SCHEDULED")) dexType = 1;
					else if(info[4].equalsIgnoreCase("INTERVAL")) dexType = 2;
					else if(info[4].equalsIgnoreCase("FILL")) dexType = 3;
					else if(info[4].equalsIgnoreCase("ALARM")) dexType = 4;
					else log.warn("Unknown DEX type: '" + info[4] + "', fileTransferId: " + state.getFileTransferId());
				}
				String line;
				if((line = in.readLine()) == null)
					throw new ParseException("The file " + state.getDexFileName() + " is empty, skipping file.", 2);
				
				state.setDexType(dexType);
				state.setDexDate(dexDate);
				return line;
			}

	/**
	 * @param state
	 * @throws BeanException 
	 * @throws DataLayerException 
	 * @throws SQLException 
	 * @throws ConvertException 
	 */
	public void readEportIdFromDatabaseIntoState(State state) throws SQLException, DataLayerException, BeanException, ConvertException {
    	Connection conn = state.getConnection();
		
    	Map<String, Object> params = state.getDbParams();
    	params.put("eportSerialNum", state.getEportSerialNum());
            	
        try {
        	DataLayerMgr.selectInto(conn, "GET_EPORT", params);
			state.setEportId(ConvertUtils.convert(BigDecimal.class, params.get("eportId")));
			
		} catch (NotEnoughRowsException e) {
			state.setLastError(DexError.NOT_FOUND_IN_DATABASE);
			state.setDisposition(Disposition.ABORT);
			throw e;
		}		
	}

}