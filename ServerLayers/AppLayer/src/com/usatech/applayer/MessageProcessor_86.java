package com.usatech.applayer;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.io.Log;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.LegacyUtils;
import com.usatech.layers.common.OfflineMessage;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.BooleanType;
import com.usatech.layers.common.constants.EventCodePrefix;
import com.usatech.layers.common.constants.EventState;
import com.usatech.layers.common.constants.EventType;
import com.usatech.layers.common.constants.LoadDataAttrEnum;
import com.usatech.layers.common.messagedata.MessageData_86;

public class MessageProcessor_86 implements OfflineMessageProcessor {
	protected String checkFillQueueKey = "usat.load.fill.check.fill";
	protected String loadFillQueueKey = "usat.load.fill";

	public void processMessage(MessageChainTaskInfo taskInfo, OfflineMessage message) throws ServiceException {
		Log log = message.getLog();
		MessageData_86 data = (MessageData_86)message.getData();
		String deviceName = message.getDeviceName();
		Map<String,Object> params = new HashMap<String, Object>();
		long eventLocalTsMs = ConvertUtils.getLocalTime(message.getServerTime(), message.getTimeZoneGuid());
		Connection conn = null;
		try {
			conn = AppLayerUtils.getConnection(false);			
			long eventId = AppLayerUtils.getOrCreateEvent(log, conn, params, EventCodePrefix.APP_LAYER.getValue(), message.getGlobalSessionCode(), deviceName,0 /* Base Host*/, EventType.BATCH.getValue(), EventType.BATCH.getHostNumber(), EventState.COMPLETE_FINAL, null, BooleanType.TRUE, BooleanType.TRUE, eventLocalTsMs, eventLocalTsMs);
			
			LegacyUtils.insertCounters(log, conn, params, eventId, 0, data);
			
			// check for fill v1 and if found send to load fill
			MessageChain loadMessageChain = new MessageChainV11();
			MessageChainStep checkStep = loadMessageChain.addStep(getCheckFillQueueKey());
			checkStep.setAttribute(LoadDataAttrEnum.ATTR_EVENT_ID, eventId);
			
			MessageChainStep loadStep = loadMessageChain.addStep(getLoadFillQueueKey());
	    	loadStep.setReferenceAttribute(LoadDataAttrEnum.ATTR_EVENT_ID, checkStep, LoadDataAttrEnum.ATTR_EVENT_ID);
			loadStep.setReferenceAttribute(LoadDataAttrEnum.ATTR_EVENT_TIME, checkStep, LoadDataAttrEnum.ATTR_EVENT_TIME);
			loadStep.setAttribute(LoadDataAttrEnum.ATTR_DEVICE_TYPE, message.getDeviceType());
	    	loadStep.setReferenceAttribute(LoadDataAttrEnum.ATTR_DEVICE_SERIAL_CD, checkStep, LoadDataAttrEnum.ATTR_DEVICE_SERIAL_CD);
	    	loadStep.setReferenceAttribute(LoadDataAttrEnum.ATTR_MINOR_CURRENCY_FACTOR, checkStep, LoadDataAttrEnum.ATTR_MINOR_CURRENCY_FACTOR);
	    	loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTERS_RESET_CODE, 'N');
	    	loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTERS_REPORTED, true);
	    	loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTERS_DISPLAYED, false);
	    	
	    	loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTER_CASH_ITEMS, data.getTotalCurrencyTransactionCounter().getValue());
	    	loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTER_CASH_AMOUNT, data.getTotalCurrencyMoneyCounter().getValue());
	    	loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTER_CASHLESS_ITEMS, data.getTotalCashlessTransactionCounter().getValue());
	    	loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTER_CASHLESS_AMOUNT, data.getTotalCashlessMoneyCounter().getValue());
	    	loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTER_PASSCARD_ITEMS, data.getTotalPasscardTransactionCounter().getValue());
	    	loadStep.setAttribute(LoadDataAttrEnum.ATTR_COUNTER_PASSCARD_AMOUNT, data.getTotalPasscardMoneyCounter().getValue());        	

        	checkStep.setNextSteps(5, loadStep);
			MessageChainService.publish(loadMessageChain, taskInfo.getPublisher());
        	
			conn.commit();
		} catch(ServiceException e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
            throw e;
		} catch(Exception e) {
        	ProcessingUtils.rollbackDbConnection(log, conn);
        	throw new ServiceException(e);
        } finally {
        	ProcessingUtils.closeDbConnection(log, conn);
        }
	}
	public String getCheckFillQueueKey() {
		return checkFillQueueKey;
	}
	public void setCheckFillQueueKey(String checkFillQueueKey) {
		this.checkFillQueueKey = checkFillQueueKey;
	}
	public String getLoadFillQueueKey() {
		return loadFillQueueKey;
	}
	public void setLoadFillQueueKey(String loadFillQueueKey) {
		this.loadFillQueueKey = loadFillQueueKey;
	}
}
