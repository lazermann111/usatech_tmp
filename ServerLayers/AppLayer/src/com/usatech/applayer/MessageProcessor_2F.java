package com.usatech.applayer;

import java.nio.BufferOverflowException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;

import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.LegacyUpdateStatusProcessor;
import com.usatech.layers.common.LegacyUtils;
import com.usatech.layers.common.Message;
import com.usatech.layers.common.MessageResponse;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.messagedata.MessageData_2F;

public class MessageProcessor_2F implements MessageProcessor {
	public MessageResponse processMessage(MessageChainTaskInfo taskInfo, Message message) throws ServiceException {
		Log log = message.getLog();
		MessageData_2F data = (MessageData_2F)message.getData();
		Map<String,Object> params = new HashMap<String, Object>();
		try {
			int ackedMessageNumber = data.getAckedMessageNumber();
			Long pendingCommandId = ConvertUtils.getLong(message.getSessionAttribute(LegacyUtils.PENDING_COMMAND_PREFIX+ackedMessageNumber),-1);
			if( pendingCommandId>0) {
				params.put("pendingCommandId", pendingCommandId);
				params.put("globalSessionCode", message.getGlobalSessionCode());
				DataLayerMgr.executeUpdate("UPDATE_PENDING_COMMAND_SUCCESS", params, true);
				message.setSessionAttribute(LegacyUtils.PENDING_COMMAND_PREFIX+ackedMessageNumber, null);
				//XXX: Do we need to also do the following?
				//message.setSessionAttribute(LegacyUtils.LAST_PENDING_COMMAND_SENT, null);
				if(params.get("dataType") == null) {
					log.warn("Had pending command that needs ack " + LegacyUtils.PENDING_COMMAND_PREFIX+ackedMessageNumber + " but could not find in database");
				} 
			} 
		} catch(ConvertException e){
			log.warn("Error while trying to process generic response", e);
		} catch(SQLException e) {
			log.warn("Error while trying to process generic response", e);
		} catch(DataLayerException e) {
			log.warn("Error while trying to process generic response", e);
		} catch(BufferOverflowException e) {
			log.warn("Error while trying to process generic response", e);
		}
		// to see if more pending message, if so, send it
		if(message.getDeviceInfo().getDeviceType()==DeviceType.ESUDS){
        	return LegacyUpdateStatusProcessor.processMessage(taskInfo, message, true);
        }
		return MessageResponse.CONTINUE_SESSION;
	}
}
