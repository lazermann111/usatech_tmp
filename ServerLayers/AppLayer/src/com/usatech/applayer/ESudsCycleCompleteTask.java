package com.usatech.applayer;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import simple.app.DatabasePrerequisite;
import simple.app.Publisher;
import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.ByteInput;
import simple.io.Log;

import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.LegacyUtils;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.ESudsRoomStatus;
/**
 * NetworkServices/middleware/esuds_cycle_complete_simulator for src logic
 * @author yhe
 *
 */
public class ESudsCycleCompleteTask extends ESudsRoomStatusProcessor implements MessageChainTask {
	private static final Log log = Log.getLog();
	
	protected Publisher<ByteInput> publisher;
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		if(publisher == null)
			throw new ServiceException("Publisher property is not set for " + this);
		Connection conn = null;
		try{
			long currentTime=System.currentTimeMillis();
			Map<String, Object> attributes = taskInfo.getStep().getAttributes();
			int hostId=ConvertUtils.getInt(attributes.get("hostId"));
			int hostEstCompleteMin=ConvertUtils.getInt(attributes.get("hostEstCompleteMin"));
			long startTime=ConvertUtils.getLong(attributes.get("startTime"));
			Date currentDate=new Date();
			currentDate.setTime(currentTime);
			Date startDate=new Date();
			startDate.setTime(startTime);
			boolean expired=false;
			if(currentTime-startTime>LegacyUtils.getESudsMaxScheduleMin()*60*1000){
				if(currentTime-startTime+LegacyUtils.getESudsScheduleMin()*60*1000-hostEstCompleteMin*60*1000>0){
					expired=true;
				}
			}
			log.info("ESuds cycle complete update CurrentTime:"+currentDate+" StartTime:"+startDate+" hostEstCompleteMin:"+hostEstCompleteMin);
			conn = AppLayerUtils.getConnection(true);
			if(expired){
				log.warn("ESuds cycle complete schedule failed. Time already expired. CurrentTime:"+currentDate+" StartTime:"+startDate	);
			}else{
				Map<String,Object> params = new HashMap<String, Object>();
				params.put("hostId", hostId);
				DataLayerMgr.selectInto(conn, "GET_HOST_DEVICE", params);
				int deviceId=ConvertUtils.getInt(params.get("deviceId"));
				int hostPortNum=ConvertUtils.getInt(params.get("hostPortNum"));
				int hostPositionNum=ConvertUtils.getInt(params.get("hostPositionNum"));
				log.info("ESuds cycle complete update deviceId:"+deviceId+" hostPortNum:"+hostPortNum+" hostPositionNum:"+hostPositionNum+" email:"+getEmailProperties()+" publisher:"+publisher);
				LegacyUtils.roomStatusUpdateHost(log, conn, false, deviceId, hostId, hostPortNum, hostPositionNum, ESudsRoomStatus.EQUIP_STATUS_IDLE_NOT_AVAILABLE, getEmailProperties(), publisher);
			}
			conn.commit();
		} catch(SQLException e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
			throw DatabasePrerequisite.createServiceException("Error while trying to update host cycle complete", e);
		} catch(Exception e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
        	throw new ServiceException("Error while trying to update host cycle complete", e);
		} finally {
        	ProcessingUtils.closeDbConnection(log, conn);
        }
		return 0;
	}
	public Publisher<ByteInput> getPublisher() {
		return publisher;
	}
	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}

}
