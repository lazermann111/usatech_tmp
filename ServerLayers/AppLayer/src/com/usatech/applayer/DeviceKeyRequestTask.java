package com.usatech.applayer;

import java.util.Map;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.text.StringUtils;

import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.DeviceInfo;
import com.usatech.layers.common.DeviceInfoManager.OnMissingPolicy;
import com.usatech.layers.common.ProcessingUtils;

public class DeviceKeyRequestTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected AppLayerDeviceInfoManager deviceInfoManager;
	
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		ProcessingUtils.checkMessageExpiration(taskInfo);
		taskInfo.setNextQos(taskInfo.getCurrentQos());
		String deviceName = ConvertUtils.getStringSafely(taskInfo.getStep().getAttributes().get("deviceName"));
		boolean refreshFromSource = ConvertUtils.getBooleanSafely(taskInfo.getStep().getAttributes().get("refresh"), false);
		Map<String,Object> resultAttributes = taskInfo.getStep().getResultAttributes();
		DeviceInfo deviceInfo;
		try {
			deviceInfo = deviceInfoManager.getDeviceInfo(deviceName, OnMissingPolicy.THROW_EXCEPTION, refreshFromSource, taskInfo, false);
		} catch(RetrySpecifiedServiceException e) {
			if(e.getRetryType() == WorkRetryType.BLOCKING_RETRY)
				throw e;
			log.warn("Could not retrieve device info for '" + deviceName + "'", e);
			resultAttributes.put("deviceName", deviceName);
			resultAttributes.put("exception", e.getMessage());
			return 0;
		} catch(ServiceException e) {
			log.warn("Could not retrieve device info for '" + deviceName + "'", e);
			resultAttributes.put("deviceName", deviceName);
			resultAttributes.put("exception", e.getMessage());
			return 0;
		}
		if(log.isDebugEnabled())
			log.debug("Retrieved encryption key '" + StringUtils.toHex(deviceInfo.getEncryptionKey()) + "' for device " + deviceName);
		deviceInfo.putAllWithTimestamps(resultAttributes);
		if (!deviceName.equalsIgnoreCase(ConvertUtils.getStringSafely(resultAttributes.get("deviceName"))))
			resultAttributes.put("storeDeviceName", true);
		return 0;
	}

	public AppLayerDeviceInfoManager getDeviceInfoManager() {
		return deviceInfoManager;
	}

	public void setDeviceInfoManager(AppLayerDeviceInfoManager deviceInfoManager) {
		this.deviceInfoManager = deviceInfoManager;
	}
}
