package com.usatech.applayer;


import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.Blob;
import java.sql.Connection;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.ByteArrayUtils;
import simple.io.Log;
import simple.security.SecureHash;

import com.usatech.app.MessageChainTaskInfo;
import com.usatech.ec.ECResponse;
import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.CommonProcessing;
import com.usatech.layers.common.DeviceInfo;
import com.usatech.layers.common.LegacyUtils;
import com.usatech.layers.common.Message;
import com.usatech.layers.common.MessageResponse;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.CredentialResult;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.messagedata.MessageData_0206;
import com.usatech.layers.common.messagedata.MessageData_0302;

/**
 * Process Updates Request
 * 
 * @author bkrug
 * 
 */
public class MessageProcessor_0206 extends CommonProcessing implements MessageProcessor {
	protected long credentialMaxAgeMs;
	protected long newCredentialMaxAgeMs;

	@Override
	public MessageResponse processMessage(MessageChainTaskInfo taskInfo, Message message) throws ServiceException {
		MessageData_0206 message0206 = (MessageData_0206) message.getData();
		MessageData_0302 reply0302 = message0206.createResponse();
		Log log = message.getLog();
		DeviceInfo deviceInfo = message.getDeviceInfo();
		Connection conn = null;
		Long commandId;
		try {
			conn = AppLayerUtils.getConnection(false);
			Map<String, Object> params = new HashMap<String, Object>();
			deviceInfo.refreshSafely();
			params.put("deviceName", deviceInfo.getDeviceName());
			params.put("maxExecuteOrder", ProcessingConstants.MAX_COMMAND_EXECUTE_ORDER);
			params.put("sessionAttributes", ConvertUtils.getStringSafely(message.getSessionAttribute(LegacyUtils.PENDING_COMMAND_STATE_ATTR), ""));
			params.put("globalSessionCode", message.getGlobalSessionCode());
			params.put("updateStatus", message0206.getUpdateStatus());
			params.put("v4Messages", 'N');
			DataLayerMgr.executeUpdate(conn, "UPDATE_PENDING_COMMAND_NEXT", params);
			if(log.isInfoEnabled())
				log.info("Got results from UPDATE_PENDING_COMMAND_NEXT: " + params);
			message.setSessionAttribute(LegacyUtils.PENDING_COMMAND_STATE_ATTR, ConvertUtils.getStringSafely(params.get("sessionAttributes"), ""));

			commandId = ConvertUtils.convertSafely(Long.class, params.get("commandId"), null);
			if(commandId == null) {
				reply0302.setReturnCode(ECResponse.RES_OK_NO_UPDATE);
				reply0302.setReturnMessage("No updates");
			} else {
				String commandDataType = ConvertUtils.getString(params.get("dataType"), true);
				MessageType messageType = MessageType.getByValue(ByteArrayUtils.fromHex(commandDataType), 0);
				switch(messageType) {
					case FILE_XFER_START_2_0:
					case FILE_XFER_START_3_0:
						// expect final 7F/A7
						String fileName = ConvertUtils.getString(params.get("fileName"), true);
						int fileTypeId = ConvertUtils.getInt(params.get("fileTypeId"));
						Blob fileContent = ConvertUtils.convert(Blob.class, params.get("fileContent"));
						// Calendar fileCreateTime = ConvertUtils.convert(Calendar.class, params.get("fileCreateTime"));
						int filePacketSize = ConvertUtils.getInt(params.get("filePacketSize"), defaultFilePacketSize);
						int fileGroupNum = message.getData().getMessageNumber();// use messageNumber for group number
						long fileTransferId = ConvertUtils.getLong(params.get("fileTransferId"));
						long fileSize = fileContent.length();
						message.addFileTransfer(false, fileTransferId, commandId, fileGroupNum, fileName, fileTypeId, filePacketSize, fileSize);

						reply0302.setFileName(fileName);
						reply0302.setFileType(fileTypeId);
						reply0302.setFileSize(fileSize);
						reply0302.setReturnCode(ECResponse.RES_OK);
						message.setSessionAttribute(LegacyUtils.LAST_PENDING_COMMAND_SENT, commandId);
						message.getLog().info("Sending " + reply0302 + " for command " + commandId);
						break;
					default:
						// nothing else is supported
						params.clear();
						params.put("pendingCommandId", commandId);
						DataLayerMgr.executeUpdate(conn, "UPDATE_PENDING_COMMAND_CANCEL", params);
						reply0302.setReturnCode(ECResponse.RES_OK_NO_UPDATE);
						reply0302.setReturnMessage("No updates");
						message.sendReply(reply0302);
						break;
				}
			}
			CredentialResult credentialResult = taskInfo.getStep().getAttribute(CommonAttrEnum.ATTR_CREDENTIAL_RESULT, CredentialResult.class, false);
			AppLayerUtils.checkDevicePassword(deviceInfo, credentialResult, message0206.getUsername(), message0206.getPassword(), credentialMaxAgeMs, newCredentialMaxAgeMs, conn, reply0302);
			conn.commit();
		} catch(ServiceException e) {
			message.getLog().warn("Could not process message", e);
			ProcessingUtils.rollbackDbConnection(log, conn);
			reply0302.setReturnMessage("Error");
		} catch(Exception e) {
			message.getLog().warn("Could not process message", e);
			ProcessingUtils.rollbackDbConnection(log, conn);
			reply0302.setReturnMessage("Error");
		} finally {
			ProcessingUtils.closeDbConnection(log, conn);
		}
		message.sendReply(reply0302);
		return MessageResponse.CONTINUE_SESSION;
	}

	protected boolean hashMatches(String password, byte[] hash, byte[] salt) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		byte[] check = SecureHash.getHash(password, salt);
		return Arrays.equals(check, hash);
	}

	public long getNewCredentialMaxAgeMs() {
		return newCredentialMaxAgeMs;
	}

	public void setNewCredentialMaxAgeMs(long newCredentialMaxAgeMs) {
		this.newCredentialMaxAgeMs = newCredentialMaxAgeMs;
	}

	public long getCredentialMaxAgeMs() {
		return credentialMaxAgeMs;
	}

	public void setCredentialMaxAgeMs(long credentialMaxAgeMs) {
		this.credentialMaxAgeMs = credentialMaxAgeMs;
	}

}


