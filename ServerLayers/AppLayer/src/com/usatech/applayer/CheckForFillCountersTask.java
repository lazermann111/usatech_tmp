package com.usatech.applayer;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import simple.app.DatabasePrerequisite;
import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.db.NotEnoughRowsException;
import simple.io.Log;

import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.LoadDataAttrEnum;

public class CheckForFillCountersTask implements MessageChainTask {
	private static final Log log = Log.getLog();

	/* INPUT: LoadDataAttrEnum.ATTR_EVENT_ID, LoadDataAttrEnum.ATTR_EVENT_TIME, LoadDataAttrEnum.ATTR_DEVICE_TYPE
	 * OUTPUT: LoadDataAttrEnum.ATTR_DEVICE_SERIAL_CD, LoadDataAttrEnum.ATTR_MINOR_CURRENCY_FACTOR, LoadDataAttrEnum.ATTR_COUNTERS_REPORTED, 
	 * 		   LoadDataAttrEnum.ATTR_COUNTER_CASH_AMOUNT, LoadDataAttrEnum.ATTR_COUNTER_CASHLESS_AMOUNT, LoadDataAttrEnum.ATTR_COUNTER_PASSCARD_AMOUNT, 
	 * 		   LoadDataAttrEnum.ATTR_COUNTER_CASH_ITEMS, LoadDataAttrEnum.ATTR_COUNTER_CASHLESS_ITEMS, LoadDataAttrEnum.ATTR_COUNTER_PASSCARD_ITEMS
	 */
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		final MessageChainStep step = taskInfo.getStep();
		final Map<String,Object> resultAttributes = step.getResultAttributes();
		final Connection conn = AppLayerUtils.getConnection(true);
		try{
			Map<String, Object> params = new HashMap<String, Object>();
			Object eventId = step.getAttribute(LoadDataAttrEnum.ATTR_EVENT_ID, Object.class, true);
			params.put("eventId", eventId);
			//Find counters for event
			try {
				DataLayerMgr.selectInto(conn, "GET_BATCH_EVENT_FOR_FILL_EVENT", params);
			} catch(NotEnoughRowsException e) {
				try {
					DataLayerMgr.selectInto(conn, "GET_BATCH_EVENT_FOR_FILL_EVENT_BY_SESSION_ID", params);
				} catch(NotEnoughRowsException e2) {
					if(log.isInfoEnabled())
						log.info("Batch Event for Fill Event #" + eventId + " not found in database. Ignoring for now.");
					return 0;
				}
			}
	    	boolean duplicate = ConvertUtils.getBoolean(params.get("duplicateFlag"));
	    	DataLayerMgr.selectInto(conn, "GET_DEVICE_INFO_FOR_EVENT", params);
	    	String deviceSerialCd = ConvertUtils.getString(params.get("deviceSerialCd"), true);
	    	int minorCurrencyFactor = ConvertUtils.getInt(params.get("minorCurrencyFactor"));
	    	resultAttributes.put(LoadDataAttrEnum.ATTR_DEVICE_SERIAL_CD.getValue(), deviceSerialCd);
	    	resultAttributes.put(LoadDataAttrEnum.ATTR_MINOR_CURRENCY_FACTOR.getValue(), minorCurrencyFactor);
	    	if(duplicate) {
	    		resultAttributes.put(LoadDataAttrEnum.ATTR_COUNTERS_REPORTED.getValue(), false); //can't tell which batch counters to use so act like none were provided
	    		if(log.isInfoEnabled())
					log.info("Duplicate Fill v1 Event #" + eventId + " found; adding a fill without any counters");
	    	} else {
	    		DataLayerMgr.selectInto(conn, "GET_COUNTERS_FOR_EVENT", params);
	    		Integer cashAmount = ConvertUtils.convert(Integer.class, params.get("cashAmount"));
	    		Integer cashlessAmount = ConvertUtils.convert(Integer.class, params.get("cashlessAmount"));
	    		Integer passcardAmount = ConvertUtils.convert(Integer.class, params.get("passcardAmount"));
	    		Integer cashItems = ConvertUtils.convert(Integer.class, params.get("cashItems"));
	    		Integer cashlessItems = ConvertUtils.convert(Integer.class, params.get("cashlessItems"));
	    		Integer passcardItems = ConvertUtils.convert(Integer.class, params.get("passcardItems"));
            	int numCounters = ConvertUtils.getInt(params.get("numCounters"), 0);
	    		resultAttributes.put(LoadDataAttrEnum.ATTR_COUNTERS_REPORTED.getValue(), numCounters > 0);
	    		resultAttributes.put(LoadDataAttrEnum.ATTR_COUNTER_CASH_AMOUNT.getValue(), cashAmount);
            	resultAttributes.put(LoadDataAttrEnum.ATTR_COUNTER_CASHLESS_AMOUNT.getValue(), cashlessAmount);
            	resultAttributes.put(LoadDataAttrEnum.ATTR_COUNTER_PASSCARD_AMOUNT.getValue(), passcardAmount);
            	resultAttributes.put(LoadDataAttrEnum.ATTR_COUNTER_CASH_ITEMS.getValue(), cashItems);
            	resultAttributes.put(LoadDataAttrEnum.ATTR_COUNTER_CASHLESS_ITEMS.getValue(), cashlessItems);
            	resultAttributes.put(LoadDataAttrEnum.ATTR_COUNTER_PASSCARD_ITEMS.getValue(), passcardItems);          	
	    	}		
			return 5; // do load fill
		} catch(SQLException e) {
        	ProcessingUtils.rollbackDbConnection(log, conn);
            throw DatabasePrerequisite.createServiceException(e);
		} catch(Exception e){
			ProcessingUtils.rollbackDbConnection(log, conn);
            throw new ServiceException("Error while recording email sent", e);
		} finally {
			ProcessingUtils.closeDbConnection(log, conn);
		}
	}
}
