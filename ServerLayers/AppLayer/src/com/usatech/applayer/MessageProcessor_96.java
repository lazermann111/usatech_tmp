package com.usatech.applayer;

import java.util.List;

import simple.app.ServiceException;
import simple.io.Log;

import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.OfflineMessage;
import com.usatech.layers.common.messagedata.MessageData_96;
import com.usatech.layers.common.messagedata.MessageData_96.CashVendData;
/**
 * Each cash vend in 96 is a sale with one line item.
 * Tran id is transactionId:blockNumber
 * @author yhe
 *
 */
public class MessageProcessor_96 implements OfflineMessageProcessor {
	public void processMessage(MessageChainTaskInfo taskInfo, OfflineMessage message) throws ServiceException {
		Log log = message.getLog();
		MessageData_96 data = (MessageData_96)message.getData();
		List<CashVendData> cashVends = data.getCashVends();
        for(CashVendData cashVend: cashVends) {
			AppLayerUtils.processSale(cashVend, message, taskInfo.getPublisher(), false);
        	log.info("Successfully processed cash vend transaction");
        }
	}
}
