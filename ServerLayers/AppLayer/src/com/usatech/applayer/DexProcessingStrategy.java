/** 
 * USA Technologies, Inc. 2011
 * DexProcessingStrategy.java by phorsfield, Aug 16, 2011 10:25:07 AM
 */
package com.usatech.applayer;

import java.io.IOException;
import java.io.LineNumberReader;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.results.BeanException;

/**
 * Mechanism by which DEX files are handled. 
 * The strategy pattern is used, but State, representing a DEX file kept separate
 * Implementations of this interface must be stateless.
 *
 * Example code follows:
 * <code>
 * strat.disposition(fileName, fileContent, fileTransferId)).getDisposition()

state.getStrategy().readDexFileInfo(state, in, attributes).getDisposition()

// Strategy hits database for EPORT ID, and for pre-existence
state.getStrategy().readDatabaseInfo(state, conn, in).getDisposition()

//remove the DEX file header for parsing and storage
state.getStrategy().cleanupFileContentForDb(state, fileContent);

state.getStrategy().writeDexFileDataToDatabase(state, conn).getDisposition()

LineHandler lh = state.getStrategy().getLineHandler(state);

// last step in the strategy invokes lh for each line
state.getStrategy().process(state, lh);

 * </code>
 * 
 *  @author phorsfield
 *
 */
public interface DexProcessingStrategy {

	/** To which each DEX line is yielded */
	public interface LineHandler {
		/** To which each DEX line is yielded */
		public void handleLine(State state, String line);
	}
			
	public enum DexError { 			INVALID_SIZE(1), /** File has wrong size, abort */	
									DEX_LINE_SEPARATOR(2), /** No line separator found in DEX file */
									NO_DXS_MARKER(3), /** No DXS* marker found in DEX file */
									NO_DXE_MARKER(4), /** No DXE* marker found in DEX file */
									NO_DEXDASH_HEADER(5), /** No 'DEX-' header in DEX file */
									INVALID_DEXDASH_HEADER(6), /** Invalid 'DEX-' header in DEX file */
									NOT_FOUND_IN_DATABASE(7), /** Eport not found in database */
									ALREADY_IN_DATABASE(8), /** DEX already in the database */
									CANNOT_PARSE(9); /** Errors from BVFusion code */
														    		
						    		int code; public int getCode() { return code; }
						    		private DexError(int code) { this.code = code; }
	}
		
	public enum Disposition { 		CORRUPT_FILE, /** File is corrupt, see DexError */
									PASS,    /** Not for this processor */
									PROCESS, /** Continue processing */
									ABORT /** Abort this strategy */
	}
	
	public static class State {
		
		public State(Disposition disposition, String fileContent, long fileTransferId) { 
			this.disposition = disposition;
			this.lastError = null;
			this.fileContent = fileContent;
			this.fileTransferId = fileTransferId;
		}

		public State(DexError error) { 
			this.disposition = Disposition.CORRUPT_FILE;
			this.lastError = error;
		}
		
		protected Disposition disposition;
		public Disposition getDisposition() { return disposition; }
		public void setDisposition(Disposition disposition) { this.disposition = disposition; }
		
		protected DexError lastError;
		public DexError getLastError() { return lastError; }
		public void setLastError(DexError dexError) { this.lastError = dexError; }

		protected DexProcessingStrategy strategy;
		public DexProcessingStrategy getStrategy() {  return strategy;	}
		public void setStrategy(DexProcessingStrategy strategy) {  this.strategy = strategy; }
		
		protected String currentLine;

		public String getCurrentLine() { return currentLine; }
		public void setCurrentLine(String currentLine) { this.currentLine = currentLine; }

		protected LineNumberReader fileReader;
		public LineNumberReader getFileReader() { return fileReader; }
		public void setFileReader(LineNumberReader fileReader) { this.fileReader = fileReader; }
		
		protected String fileContent;
		public String getFileContent() { return fileContent; }
		public void setFileContent(String fileContent) { this.fileContent = fileContent; }
		
		protected Map<String, Object> store = new HashMap<String, Object>();
		public Map<String,Object> getStore() { return store; }
		
		protected String dexFileName;
		public String getDexFileName() { return dexFileName; }
		public void setDexFileName(String dexFileName) { this.dexFileName = dexFileName; }
		
		protected String[] headers;
		public String[] getHeaders() { return headers; }
		public void setHeaders(String[] headers) { this.headers = headers; }
		
		protected String eportSerialNum;
		public String getEportSerialNum() { return eportSerialNum; }
		public void setEportSerialNum(String eportSerialNum) { this.eportSerialNum = eportSerialNum; }

		protected BigDecimal eportId;

		public BigDecimal getEportId() { return eportId; }
		public void setEportId(BigDecimal eportId) { this.eportId = eportId; }
		
		protected final Map<String, Object> dbParams = new HashMap<String, Object>();
		protected Map<String, Object> getDbParams() { return dbParams; }
		
		protected long fileTransferId;
		public long getFileTransferId() { return fileTransferId; }
		public void setFileTransferId(long fileTransferId) { this.fileTransferId = fileTransferId; }

		protected long dexType;
		public long getDexType() { return dexType; }
		public void setDexType(long dexType) { this.dexType = dexType; }

		protected Date dexDate;
		public Date getDexDate() { return dexDate; }
		public void setDexDate(Date dexDate) { this.dexDate = dexDate; }

		protected long dexFileId;
		public long getDexFileId() { return dexFileId; }
		public void setDexFileId(long dexFileId) { this.dexFileId = dexFileId; }

		protected long terminalId;
		public long getTerminalId() { return terminalId; }
		public void setTerminalId(long terminalId) { this.terminalId = terminalId; }

		protected Connection connection = null;
		public Connection getConnection() { return this.connection; }
		public void setConnection(Connection connection) { this.connection = connection; }
	}

	/**
	 * Invoked to process the file in it's entirety
	 * Always called.
	 * @param fileContent the file, untouched
	 * @param fileName the system file name
	 * @param fileTransferId the system file transfer id
	 * @return fieldsRequired are: lastErrorCode, fileContent (as passed in), and 
	 *         disposition (whether this strategy is to be used for this DEX file).
	 */
	public State disposition(String fileName, String fileContent, long fileTransferId);

	/**
	 * Invoked to manipulate parameters and process the file in it's entirety
	 * Always called.
	 * @param state State of the execution of this strategy for a given DEX file.
	 * @param reader a reader for the file. 
	 * @param params standard map of parameters
	 * @params taskAttributes map of parameters from message
	 * @return state, with the dexFileName set, and parameters including eportSerialNum, and info (String[] headers)
	 * @throws IOException 
	 */
	public State readDexFileInfo(State state, LineNumberReader reader, Map<String, Object> taskAttributes) throws IOException;

	/** 
	 * Invoked to load information from the database.
	 * Always called. 
	 * @param state State of the execution of this strategy for a given DEX file.
	 * @param conn REPORT database connection
	 * @param reader a reader for the file. 
	 * @return current state  
	 * @throws BeanException 
	 * @throws DataLayerException 
	 * @throws SQLException 
	 * @throws ConvertException 
	 * @throws ParseException 
	 * @throws IOException 
	 */
	public State readDatabaseInfo(State state, LineNumberReader reader) throws SQLException, DataLayerException, BeanException, ConvertException, ParseException, IOException;

	/** 
	 * Invoked to pre-process the file in it's entirety
	 * Always called. Store your cleaned content in the state with a
	 * call to state.setFileContent(newContent);
	 * This is what writeDexFileDataToDatabase will store as fileContent
	 * 
	 * @param state State of the execution of this strategy for a given DEX file.
	 * @param dirtyFileContent fileContent prior to being tweaked
	 * @return fileContent ready for parsing 
	 */
	public State cleanupFileContentForDatabase(State state, String dirtyFileContent);

	/** 
	 * Invoked to store the entire DEX file to the database
	 * Always called. 
	 * @param state State of the execution of this strategy for a given DEX file.
	 * @param conn REPORT database connection
	 * @return current state  
	 * @throws BeanException 
	 * @throws DataLayerException 
	 * @throws SQLException 
	 * @throws ConvertException 
	 */
	public State writeWholeDexToDatabase(State state) throws SQLException, DataLayerException, BeanException, ConvertException;

	/**
	 * Creates a line handler for parsing. 
	 * If you return null, a default one will be created (stores alerts)
	 * @param state State of the execution of this strategy for a given DEX file. 
	 * @param handler processes individual lines
	 * @return a line handler, or null if, and only if you are DefaultDexProcessing.java
	 */
	public LineHandler getLineHandler(State state);

	/**
	 * Invoked to process the file line by line. 
	 * Caller will provide a line handler, by invoking the API of the same name 
	 * @param state State of the execution of this strategy for a given DEX file. 
	 * @param handler processes individual lines
	 * @return the state, for error checking
	 * @throws IOException Errors during parsing
	 * @throws DataLayerException 
	 * @throws SQLException 
	 */
	public State process(State state, LineHandler handler) throws IOException, ParseException, SQLException, DataLayerException;
}
