
import java.nio.ByteBuffer;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import simple.io.ByteArrayUtils;
import simple.io.Log;
import simple.test.UnitTest;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.ESudsLogType;
import com.usatech.layers.common.messagedata.MessageData_2E;


public class ESudsDataLogTest extends UnitTest {
	
	@Before
	public void setUp() throws Exception {
		setupLog();
	}
	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
	/**
	 *  Esuds data log
	 	select * from app_exec_hist.exception_data where object_name like 'LOG%' order by exception_data_id desc;
		select * from engine.machine_cmd_inbound_hist where machine_id='EV035037' and inbound_command like '2E%'
	 	Wed Oct 8 09:07:13.315 2008
	 * @throws Exception
	 */
	@Test
	public void testESudsDataLog() throws Exception {
		
		String commandHex="00840000011CDBB5BF23";
		ByteBuffer bb=ByteBuffer.allocate(1024);
		bb.put(ByteArrayUtils.fromHex(commandHex));
		bb.flip();
		MessageData_2E data = new MessageData_2E();
		data.readData(bb);
		byte[] genericLog=data.getGenericData();
		short diag=ByteArrayUtils.readShort(genericLog, 0);
		log.info("genericData:"+ESudsLogType.getByValue(diag));
		long time= ByteArrayUtils.readLong(genericLog, 2);
		Calendar returnCalendar = Calendar.getInstance();
		returnCalendar.clear();
		returnCalendar.setTimeInMillis(time);
		log.info("time:"+ProcessingUtils.esudsDateFormat.format(returnCalendar.getTime()));
		Map<String,Object> params = new HashMap<String, Object>();
		ProcessingUtils.parseESudsGenericLog(params, genericLog);
		assert params.get("objectName").equals("LOG_MULTIPLEXOR_COMM_NOT_ESTABLISHED");
		assert params.get("genericData").equals("Wed Oct 8 09:07:13.315 2008");
	}
	
}
