/** 
 * USA Technologies, Inc. 2011
 * FileImportTaskTest.java by phorsfield, Aug 17, 2011 9:10:21 AM
 */
package com.usatech.applayer;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.management.ManagementFactory;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.dex.InboundFileImportTask;

import junit.framework.Assert;
import simple.app.Publisher;
import simple.app.QoS;
import simple.app.ServiceException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.ByteInput;
import simple.io.IOUtils;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.security.crypt.EncryptionInfoMapping;
import simple.test.UnitTest;
import simple.text.StringUtils;

/**
 * Send a DEX import message to the import task
 * 
 * @author phorsfield
 * 
 */
@IntegrationTest
public class InboundFileImportTaskTest extends UnitTest {
	protected static final String DEX_LINE_SEPARATOR = "\r\n";
	protected final static String GLOBAL_SESSION_CODE_PREFIX = ProcessingConstants.GLOBAL_EVENT_CODE_PREFIX
			+ ':'
			+ ManagementFactory.getRuntimeMXBean().getName()
			+ ':'
			+ Long.toHexString(System.currentTimeMillis()).toUpperCase() + ':';
	protected final static AtomicLong sessionIDGenerator = new AtomicLong(
			Double.doubleToLongBits(Math.random()));
	protected final long sessionId = sessionIDGenerator.getAndIncrement();
	protected final String globalSessionCode = GLOBAL_SESSION_CODE_PREFIX
			+ Long.toHexString(sessionId).toUpperCase();;

	private String fileNameNoTotals = "DEX-EE100053087-003600-081211-SCHEDULED.DEX";
	private String fileNameTotalsInDev = "DEX-G5063530-160946-072611-SCHEDULED.DEX";
	private String fileNameAlerts = "DEX-G5063530-160946-102411-SCHEDULED.DEX";
	private HashMap<String,String> resourceKeysByFileName = new HashMap<String, String>(2);
	
	private ByteInput alertMessageSent = null;
	private ResourceFolder resourceFolder;

	@Rule
	public TemporaryFolder folder = new TemporaryFolder();
	private InboundFileImportTask ifit;
	private Connection conn;
	
	@Before
	public void setUp() throws IOException, ServiceException, SQLException, DataLayerException {	
		 
		super.setupDSF("TestAppLayerService-dev.properties");
		
		resourceFolder = new simple.io.resource.FileResourceFolder(folder
				.getRoot().getPath());

		ifit = new InboundFileImportTask();		
		ifit.setResourceFolder(resourceFolder);			
		//ifit.setDeviceInfoManager(new BasicDeviceInfoManager());		
	}
	
	@BeforeClass
	public static void setUpStatics()
	{
		//List<DexProcessingStrategy> strategies = InboundFileImportTask.getStrategies();
		
		//BVFusionDexProcessingStrategy bvStrat = new BVFusionDexProcessingStrategy();
		//bvStrat.setProcessBestVendorDEXOnly(true);
		
		//strategies.add(new DefaultDexProcessingStrategy());
		//strategies.add(new BVFusionDexProcessingStrategy());
	}
	
	@Before
	public void setUpLog() throws Exception {
		setupLog();
	}
	
	/**
	 */
	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
	

	/**
	 * Copies a resource out to the JUnit temporary folder 
	 * @param filename name of the file resource to export
	 * @throws IOException 
	 **/
	private void createLocalCopy(String fileName) throws IOException
	{
		String timeStamp = new SimpleDateFormat("HHssSS").format( new Date() ).substring(0,6);
		String[] fileNameParts = fileName.split("-");
		fileNameParts[2]=timeStamp;		
		Resource resource = resourceFolder.getResource(StringUtils.join(fileNameParts, "-"), ResourceMode.CREATE);
		OutputStream fos = resource.getOutputStream();
		InputStream is = ClassLoader.getSystemResourceAsStream(fileName);
		byte[] b = new byte[13]; is.read(b,0,b.length); /*serial num component*/
		fos.write(b,0,b.length);
		is.read(b, 0, 6); // skip time
		fos.write(timeStamp.getBytes()); // write current time, scaled
		IOUtils.copy(is, fos);
		fos.close();
		is.close();
		String resourceKey = resource.getKey();
		
		resourceKeysByFileName.put(fileName, resourceKey);
	}
	
	private MessageChainStep constructFileTransferStep(MessageChain mc, String fileName) throws SQLException, DataLayerException {
		MessageChainStep step = mc.addStep("usat.inbound.fileimport");
		step.addStringAttribute("deviceName", "EE100053087");
		
		long fileTransferId = 50000+Math.abs(fileName.hashCode() & 0x0000ffff);
		
		while(!isUniqueFTid(fileTransferId))
		{			
			++fileTransferId;
		}
		
		step.addLongAttribute("fileTransferId", fileTransferId); 

		step.addStringAttribute("resourceKey", resourceKeysByFileName.get(fileName));
		step.addIntAttribute("fileType", 0 /* DEX */);
		step.addStringAttribute("globalSessionCode", GLOBAL_SESSION_CODE_PREFIX);
		step.addLongAttribute("startTime", System.currentTimeMillis() - 100);
		step.addLongAttribute("endTime", System.currentTimeMillis());

		return step;
	}

	@Test
	public void testUniqueGoodDex() throws ServiceException, IOException, SQLException, DataLayerException {
		MessageChain mc = new MessageChainV11();

		createLocalCopy(fileNameNoTotals);

		this.constructFileTransferStep(mc, fileNameNoTotals);

		//MessageChainService.publish(mc, publisher);
						
		ifit.process(new DummyMessageChainTaskInfo(mc, testPublisher));
		
	}

	@Test
	public void testDexWithTotals() throws ServiceException, IOException, SQLException, DataLayerException
	{
		MessageChain mc = new MessageChainV11();

		createLocalCopy(fileNameTotalsInDev );

		this.constructFileTransferStep(mc, fileNameTotalsInDev);

		//MessageChainService.publish(mc, publisher);
						
		ifit.process(new DummyMessageChainTaskInfo(mc, testPublisher));
	}

	@Test
	public void testDexWithAccumulation() throws ServiceException, IOException, SQLException, DataLayerException
	{	
		conn = DataLayerMgr.getConnection("REPORT");
		conn.createStatement().execute("DELETE FROM G4OP.DEX_DELTA dt WHERE dt.EPORT_ID = 1000000007506");
		conn.createStatement().execute("DELETE FROM G4OP.DEX_COUNTER dt WHERE dt.EPORT_ID = 1000000007506");
		conn.commit();
		conn.close();
		
		// first will be an insert
		testDexWithTotals();
		// second will be an update
		testDexWithTotals();
		// now assert correct quantity 
		conn = DataLayerMgr.getConnection("REPORT");
		Assert.assertTrue("Accumulation failed - wrong delta", conn.createStatement().executeUpdate("UPDATE G4OP.DEX_DELTA t SET EPORT_ID = EPORT_ID WHERE EXISTS (select t2.QTY from G4OP.DEX_DELTA t1 inner join G4OP.DEX_DELTA t2 on (t2.EPORT_ID = t1.EPORT_ID AND t1.DEX_DELTA_ID < t2.DEX_DELTA_ID) WHERE t2.EPORT_ID = 1000000007506 AND t2.QTY = 0)") > 0);
		conn.commit();
		conn.close();
	}
	
	@Test
	public void testDexSeries() throws ServiceException, IOException, SQLException, DataLayerException
	{	
		String getFilesSql = " select df.file_name, df.file_content, df.dex_file_id " + 
			    " from pss.pos p"+
			    " inner join location.customer c on (p.customer_id = c.customer_id)"+
			    " inner join device.device d on (p.device_id = d.device_id)  "+
			    " inner join device.device_file_transfer dft on ( d.device_id = dft.device_id )"+ 
			    " inner join g4op.dex_file df on  (dft.file_transfer_id = df.file_transfer_id) "+
			    " inner join REPORT.TERMINAL_EPORT te on (te.eport_id = df.EPORT_ID) "+
			    " inner join REPORT.TERMINAL t on (te.TERMINAL_ID = t.TERMINAL_ID)"+
			    " where "+
			     "  df.DEX_DATE between NVL(te.START_DATE, MIN_DATE) and NVL(te.END_DATE, MAX_DATE)"+
			     "  and p.customer_id = 6 and instr(substr(df.file_content,1,50),'DXS*') > -1 " +
			     "  and d.device_name = 'TD000021'" +
			     "  order by df.DEX_DATE";
		String deleteTestFilesSql1 = 
			    " delete from g4op.dex_file df2 where  df2.dex_file_id in ( select df.dex_file_id  " +
			    "  from g4op.dex_file df "+
			    "  inner join REPORT.TERMINAL_EPORT te on (te.eport_id = df.EPORT_ID) "+
			    "  inner join REPORT.TERMINAL t on (te.TERMINAL_ID = t.TERMINAL_ID)"+
			    "  where "+
			     "   df.DEX_DATE between NVL(te.START_DATE, MIN_DATE) and NVL(te.END_DATE, MAX_DATE)"+
			     "   and te.eport_id = '1000000012777'" +
			     " and df.file_name like '%TEST%' " +
			     " and not exists (select d2.file_transfer_id from device.file_transfer d2 where d2.file_transfer_id = df.file_transfer_id)" +
			     ")";
		String deleteTestFilesSql2 = 
			    " delete from g4op.dex_file_line df2 where  df2.dex_file_id in ( select df.dex_file_id  " +
			    "  from g4op.dex_file df "+
			    "  inner join REPORT.TERMINAL_EPORT te on (te.eport_id = df.EPORT_ID) "+
			    "  inner join REPORT.TERMINAL t on (te.TERMINAL_ID = t.TERMINAL_ID)"+
			    "  where "+
			     "   df.DEX_DATE between NVL(te.START_DATE, MIN_DATE) and NVL(te.END_DATE, MAX_DATE)"+
			     "   and te.eport_id = '1000000012777'" +
			     " and df.file_name like '%TEST%' " +
			     " and not exists (select d2.file_transfer_id from device.file_transfer d2 where d2.file_transfer_id = df.file_transfer_id)" +
			     ")";
		
		List<String> resourceKeys = new LinkedList<String>();
		
		conn = DataLayerMgr.getConnection("REPORT");
		Statement queryFile = conn.createStatement();
	    ResultSet files = queryFile.executeQuery(getFilesSql);
	    
	    int idx = 0;
	    while(files.next())
	    {
	    	++idx;
	    	String dexFileName = files.getString(1);

	    	dexFileName = dexFileName.replace(".log", Long.toString(50000+System.currentTimeMillis()).substring(7) + "-" + Integer.toString(idx) + ".log");
	    	dexFileName = dexFileName.replace("-SCHEDULED", "-TEST");
			Resource resource = resourceFolder.getResource(dexFileName, ResourceMode.CREATE);
			OutputStream fos = resource.getOutputStream();
			InputStream is = files.getClob(2).getAsciiStream();
			
			fos.write(dexFileName.substring(0, dexFileName.lastIndexOf('.')).getBytes());
			fos.write(DEX_LINE_SEPARATOR.getBytes());
			IOUtils.copy(is, fos);
			fos.close();
			is.close();
			
			String resourceKey = resource.getKey();
			resourceKeys.add(resourceKey);
	    }
	    files.close();
	    queryFile.close();
	    
		conn.createStatement().execute("DELETE FROM G4OP.DEX_DELTA dt WHERE dt.EPORT_ID = 1000000012777");
		conn.createStatement().execute("DELETE FROM G4OP.DEX_COUNTER dt WHERE dt.EPORT_ID = 1000000012777");
		conn.createStatement().execute(deleteTestFilesSql2);
		conn.createStatement().execute(deleteTestFilesSql1);
		conn.commit();
		conn.close();

		for(String dexFileResource : resourceKeys)
		{
			MessageChain mc = new MessageChainV11();
			MessageChainStep step = mc.addStep("usat.inbound.fileimport");
			step.addStringAttribute("deviceName", "TD000021");
			step.addStringAttribute("fileName", dexFileResource); // happens to be the same for file stores
			long fileTransferId = 50000+Math.abs(dexFileResource.hashCode() & 0x0000ffff);
			
			while(!isUniqueFTid(fileTransferId))
			{			
				++fileTransferId;
			}
			
			step.addLongAttribute("fileTransferId", fileTransferId); 
			step.addStringAttribute("resourceKey", dexFileResource);
			step.addIntAttribute("fileType", 0 /* DEX */);
			step.addStringAttribute("globalSessionCode", GLOBAL_SESSION_CODE_PREFIX);
			step.addLongAttribute("startTime", System.currentTimeMillis() - 100);
			step.addLongAttribute("endTime", System.currentTimeMillis());
			step.addLongAttribute("serverTime", System.currentTimeMillis() - 100);
			
			ifit.process(new DummyMessageChainTaskInfo(mc, testPublisher));
		}

		conn = DataLayerMgr.getConnection("REPORT");
		conn.createStatement().execute(deleteTestFilesSql2);
		conn.createStatement().execute(deleteTestFilesSql1);
		Assert.assertEquals("Accumulation failed - wrong delta", 1, conn.createStatement().executeUpdate("UPDATE G4OP.DEX_DELTA t SET EPORT_ID = EPORT_ID WHERE t.EPORT_ID = 1000000012777 AND t.QTY = 1051"));
		conn.commit();
		conn.close();
	}
	

	private boolean isUniqueFTid(long fileTransferId) throws SQLException, DataLayerException {
		conn = DataLayerMgr.getConnection("REPORT");
		ResultSet r = conn.createStatement().executeQuery("select count(*) from g4op.dex_file where file_transfer_id = " +fileTransferId);
		if(r.next())
		{
			boolean rval = r.getInt(1) == 0;
			conn.close();
			return rval;			
		}
		else
		{
			conn.close();
			return true;
		}				
	}

	@Test
	public void testDexWithAlerts() throws ServiceException, IOException, SQLException, DataLayerException
	{
		
		MessageChain mc = new MessageChainV11();

		createLocalCopy(fileNameAlerts );

		this.constructFileTransferStep(mc, fileNameAlerts);
						
		ifit.process(new DummyMessageChainTaskInfo(mc, testPublisher));
		
		assertTrue(alertMessageSent != null);
	}

	private Publisher<ByteInput> testPublisher = 
			new Publisher<ByteInput>() {
				
				/*@Override
				public void publish(String queueKey, boolean multicast, boolean temporary,
						ByteInput content, QoS qos) throws ServiceException {
					if(queueKey.equals("usat.inbound.alert"))
					{
						alertMessageSent = content;
					}
					return;											
				}*/
				
				@Override
				public void publish(String queueKey, boolean multicast, boolean temporary,
						ByteInput content) throws ServiceException {
					
					if(queueKey.equals("usat.inbound.alert"))
					{
						alertMessageSent = content;
					}
					return;						
				}
				
				@Override
				public void publish(String queueKey, ByteInput content)
						throws ServiceException {
					if(queueKey.equals("usat.inbound.alert"))
					{
						alertMessageSent = content;
					}
					return;						
				}
				
				@Override
				public void close() {
					return;						
				}

				@Override
				public void publish(String queueKey, boolean multicast, boolean temporary, ByteInput content,
						Long correlation, QoS qos, String... blocks) throws ServiceException {
					// TODO Auto-generated method stub
					
				}

				@Override
				public int getConsumerCount(String queueKey, boolean temporary) {
					// TODO Auto-generated method stub
					return 0;
				}

				@Override
				public String[] getSubscriptions(String queueKey) throws ServiceException {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public void block(String... blocks) throws ServiceException {
					// TODO Auto-generated method stub
					
				}
			};

}

class DummyMessageChainTaskInfo implements MessageChainTaskInfo { 
	
	MessageChain mc;
	Publisher<ByteInput> testPublisher;

	public DummyMessageChainTaskInfo(MessageChain mc, Publisher<ByteInput> testPublisher)
	{
		this.testPublisher = testPublisher;
		this.mc = mc;
	}
	
	@Override
	public boolean setInterruptible(boolean interruptible) {
		return false;
	}
	
	@Override
	public boolean getInterruptible() {
		return true;
	}
	
	@Override
	public void setNextQos(QoS qos) {				
	}
	
	@Override
	public void setAfterComplete(Runnable afterComplete) {		
	}
	
	@Override
	public boolean isRedelivered() {
		return false;
	}
	
	@Override
	public MessageChainStep getStep() {
		return mc.getCurrentStep();
	}
	
	@Override
	public String getServiceName() {
		return "";
	}
	
	@Override
	public int getRetryCount() {
		return 0;
	}
	
	@Override
	public Publisher<ByteInput> getPublisher() {
		return testPublisher;
	}
		
	@Override
	public String getMessageChainGuid() {
		return "";
	}
	
	@Override
	public QoS getCurrentQos() {
		return null;
	}

	@Override
	public EncryptionInfoMapping getEncryptionInfoMapping() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getEnqueueTime() {
		// TODO Auto-generated method stub
		return 0;
	}
};
