/** 
 * USA Technologies, Inc. 2011
 * IntegrationTestCategory.java by phorsfield, Aug 17, 2011 9:26:31 AM
 */
package com.usatech.applayer;

/**
 * Define a JUnit 4 Category  
 * for tests that leverage 
 * messaging or other live resources
 * @author phorsfield
 *
 */
public class IntegrationTestCategory {

}
