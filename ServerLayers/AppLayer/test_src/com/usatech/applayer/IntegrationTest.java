/** 
 * USA Technologies, Inc. 2011
 * IntegrationTest.java by phorsfield, Aug 17, 2011 9:23:36 AM
 */
package com.usatech.applayer;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;

import org.junit.experimental.categories.Categories;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

/**
 * Custom Annotation 
 * Define a JUnit 4 Category  
 * for tests that leverage 
 * messaging or other live resources
 * 
 * @author phorsfield
 *
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@RunWith(Categories.class)
@Category(IntegrationTestCategory.class)
public @interface IntegrationTest {
	
}
