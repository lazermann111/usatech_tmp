package com.usatech.applayer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.ExtensionFileFilter;
import simple.io.Log;
import simple.text.StringUtils;
import simple.util.sheet.SheetUtils;
import simple.util.sheet.SheetUtils.RowValuesIterator;

public class LoadMissingCardKeys {
	private static final Log log = Log.getLog();
	
	public static void main(String[] args) throws Exception {
		File dir = new File("C:\\Users\\bkrug\\Documents\\KeyMgrLogs_20140606");
		File missingFile = new File(dir, "AuthsMissingCardKey_h.csv");
		Map<Long,Map<String,Object>> lookup = new HashMap<Long, Map<String,Object>>();
		InputStream in = new FileInputStream(missingFile);
		try {
			RowValuesIterator rvi = SheetUtils.getCVSRowValuesIterator(in);
			while(rvi.hasNext()) {
				Map<String,Object> values = rvi.next();
				long globalAccountId = ConvertUtils.getLong(values.get("GLOBAL_ACCOUNT_ID"));
				lookup.put(globalAccountId, values);
			}
		} finally {
			in.close();
		}
		Writer writer = new BufferedWriter(new FileWriter(new File(dir, "LoadMissingCardKeys_h.sql")));
		try {
			// now parse the log files
			Pattern FULL_PATTERN = Pattern.compile("2014-06-06 10:[12]\\d:\\d\\d,\\d+ \\[Store Thread #\\d+\\] INFO <> com.usatech.app.MessageChainService - FINISHED step.*resultAttributes=\\{.*globalAccountId(?:\\.\\d+)?=(\\d+),.*cardKey=(\\d:\\d+,\\d:\\d+)[,}].*");
			Pattern FIND_PATTERN = Pattern.compile("2014-06-06 10:[12]\\d:\\d\\d,\\d+ \\[(?:LookupAccount|DukptDecrypt) Thread #\\d+\\] INFO <> com.usatech.app.MessageChainService - FINISHED step '([^']+)'.*resultAttributes=\\{.*globalAccountId(?:\\.\\d+)?=(\\d+)[,}].*");
			Pattern LOADER_FIND_PATTERN = Pattern.compile("2014-06-06 10:[12]\\d:\\d\\d,\\d+ \\[RecordAuth Thread #\\d+\\] INFO  com.usatech.app.MessageChainService - FINISHED step '([^']+)'.*attributes=\\{.*globalAccountId(?:\\.\\d+)?=(\\d+)[,}].*");
			//2014-06-06 10:19:17,752 [DukptDecrypt Thread #11] INFO <> com.usatech.app.MessageChainService - FINISHED step '18016@usanet34.trooper.usatech.com:1467167ACB2:144120:2': ["usat.keymanager.decrypt.magtek"]: attributes={encryptedData=[B@31ba9eaa, cipherName=DESede/CBC/NOPADDING, validationRegex=[%;]?([\x20-\x3E\x40-\x7F]+)(?:\?[\x01-\xFF]?)?(?:\x00[\x00-\xFF]*)?, keySerialNum=[B@5306c467, keyId=1222874644089, cardReaderTypeId=1, decryptedDataEncoding=US-ASCII}; resultCode=0; resultAttributes={globalAccountId=1009836271, accountCdHash=[B@5ae4090f, trackCrcMatch=true, decryptedData=*************************************, instance=1, trackCrc=57289} [in 2 ms; published 12 ms ago]
			//2014-06-06 10:27:03,599 [RecordAuth Thread #46] INFO  com.usatech.app.MessageChainService - FINISHED step '9385@usanet32.trooper.usatech.com:146715D6B90:226653:2': ["usat.inbound.auth.record"]<--["usat.keymanager.store#not.4" by 39047@usakls31.trooper.usatech.com in 2ms]<--["usat.keymanager.store#any" by 60064@usakls34.trooper.usatech.com in 2ms]<--["netlayer-usanet32-14107.outbound" by 9385@usanet32.trooper.usatech.com in 10ms]<--["usat.authority.realtime.authority_iso8583_elavon" by 21006@usaapr34.trooper.usatech.com in 585ms]<--["usat.inbound.message.auth" by 6384@usaapr31.trooper.usatech.com in 8ms]<--["usat.keymanager.account.lookup" by 39047@usakls31.trooper.usatech.com in 2ms]: attributes={trackData=426684******0038, currencyCd=USD, cardKey=null, invalidEventId=N, traceNumber=4600884552876886389, deviceName=EV219111, eventId=1402046808, balanceAmount=null, timeZoneGuid=US/Central, minorCurrencyFactor=100, authAuthorityMiscData=elavonAuthorizationData=000000  50APPROVAL^\CPSData=E464157520235320Z74PB 0^\merchantDefinedData=005864^\, approvedAmount=125, passThru=false, instance=4, authActionBitmap=null, authAuthorityTranCd=01106B, globalEventCd=A:EV219111:1402046808, authAuthorityRefCd=415714112342, authAuthorityTs=1402064823000, paymentType=C, requestedAmount=125.0000, sentToDevice=true, authTime=1402064822735, authAmount=500, authHoldUsed=true, globalAccountId=1002140204, authResultCd=Y, authActionId=null, consumerAcctId=null, posPtaId=15738061, authorityRespCd=00, entryMethod=SWIPE, authorityRespDesc=APPROVAL, globalSessionCode=A:9385@usanet32.trooper.usatech.com:146715D6B7E:3FD096D09D3F9195}; resultCode=0; resultAttributes={saleAmount=0, authId=1393862108, saleSessionStartTime=null, sessionUpdateNeeded=N, saleGlobalSessionCode=null, tranLineItemCount=null, tranId=2235025391, tranImportNeeded=N, tranStateCd=6, clientPaymentTypeCd=null} [in 60 ms; published 70 ms ago]

			Pattern RESULT_PATTERN = Pattern.compile("2014-06-06 10:[12]\\d:\\d\\d,\\d+ \\[Store Thread #\\d+\\] INFO <> com.usatech.app.MessageChainService - FINISHED step '([^']+)'.*resultAttributes=\\{.*cardKey=(\\d:\\d+,\\d:\\d+)[,}].*");
			Pattern LAST_RESORT = null; //Pattern.compile("2014-06-06 10:[12]\\d:\\d\\d,\\d+ \\[Store Thread #\\d+\\] INFO <> com.usatech.app.MessageChainService - FINISHED step.*: \\[\\\"usat\\.keymanager\\.store#not\\.\\d\\\"\\].*: attributes=\\{.*globalAccountId(?:\\.\\d+)?=(\\d+),.*cardKey=(\\d:\\d+)[,}].*resultAttributes=\\{\\}.*");
			//"2014-06-06 10:24:35,970 [Store Thread #17] INFO <> com.usatech.app.MessageChainService - FINISHED step '22751@usanet31.trooper.usatech.com:14671565FEA:236427:2': ["usat.keymanager.store#not.3"]<--["usat.keymanager.store#any" by 43283@usakls33.trooper.usatech.com in 3ms]<--["netlayer-usanet31-14107.outbound" by 22751@usanet31.trooper.usatech.com in 10ms]<--["usat.authority.realtime.authority_iso8583_elavon" by 21283@usaapr33.trooper.usatech.com in 768ms]<--["usat.inbound.message.auth" by 17589@usaapr33.trooper.usatech.com in 10ms]<--["usat.keymanager.account.lookup" by 43283@usakls33.trooper.usatech.com in 1ms]: attributes={globalAccountId=1028269884, authResultCd=Y, instance=4, storeExpirationTime=1409840675083, decryptedData.1=****************, trackCrc=16602, decryptedData.2=****, encryptedDataCount=2, authHoldUsed=true, cardKey=3:160787950}; resultCode=0; resultAttributes={} [in 2 ms; published 15 ms ago]");
			Map<String, Map<String,Object>> pairings = new HashMap<String, Map<String,Object>>();
			for(File logFile : dir.listFiles((FilenameFilter)new ExtensionFileFilter("Log File", "log"))) {
				int cnt = 0;
				BufferedReader reader = new BufferedReader(new FileReader(logFile));
				try {
					String line;
					while((line=reader.readLine()) != null) {
						Matcher matcher = FULL_PATTERN.matcher(line);
						if(matcher.matches()) {
							long globalAccountId = ConvertUtils.getLong(matcher.group(1));
							if(handleMatch(lookup, globalAccountId, matcher.group(2), writer))
								cnt++;
						} else if((matcher = FIND_PATTERN.matcher(line)).matches() || (matcher = LOADER_FIND_PATTERN.matcher(line)).matches()){
							long globalAccountId = ConvertUtils.getLong(matcher.group(2));
							String stepGuid = matcher.group(1);
							Map<String,Object> values = pairings.get(stepGuid);
							if(values == null) {
								values = new HashMap<String, Object>();
								values.put("globalAccountId", globalAccountId);
								pairings.put(stepGuid, values);
							} else {
								String cardKey = ConvertUtils.getString(values.get("cardKey"), false);
								if(!StringUtils.isBlank(cardKey) && handleMatch(lookup, globalAccountId, cardKey, writer))
									cnt++;
							}
						} else if((matcher = RESULT_PATTERN.matcher(line)).matches()){
							String cardKey = ConvertUtils.getString(matcher.group(2), false);
							String stepGuid = matcher.group(1);
							Map<String,Object> values = pairings.get(stepGuid);
							if(values == null) {
								values = new HashMap<String, Object>();
								values.put("cardKey", cardKey);
								pairings.put(stepGuid, values);
							} else {
								long globalAccountId = ConvertUtils.getLong(values.get("globalAccountId"));
								if(!StringUtils.isBlank(cardKey) && handleMatch(lookup, globalAccountId, cardKey, writer))
									cnt++;
							}
						} else if(LAST_RESORT != null && (matcher = LAST_RESORT.matcher(line)).matches()) {
							long globalAccountId = ConvertUtils.getLong(matcher.group(1));
							if(handleMatch(lookup, globalAccountId, matcher.group(2), writer)) {
								cnt++;
								writer.write("-- Above is Last Result for ");
								writer.write(String.valueOf(globalAccountId));
								writer.write("\n");
							}
						}
					}
				} finally {
					reader.close();
				}
				writer.flush();
				log.info("Found " + cnt + " lines in file " + logFile.getAbsolutePath());
			}
		} finally {
			writer.close();
		}
		Log.finish();
	}

	protected static boolean handleMatch(Map<Long,Map<String,Object>> lookup, long globalAccountId, String cardKey, Writer writer) throws ConvertException, IOException {
		Map<String,Object> values = lookup.get(globalAccountId);
		if(values != null) {
			long tranId = ConvertUtils.getLong(values.get("TRAN_ID"));
			long authId = ConvertUtils.getLong(values.get("AUTH_ID"));								
			writer.write("UPDATE PSS.AUTH SET CARD_KEY = '");
			writer.write(cardKey);
			writer.write("' WHERE AUTH_ID = ");
			writer.write(String.valueOf(authId));
			writer.write(" AND TRAN_ID = ");
			writer.write(String.valueOf(tranId));
			writer.write(" AND CARD_KEY IS NULL;\n");								
			return true;
		}
		return false;
	}
}
