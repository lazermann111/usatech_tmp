package com.usatech.applayer;


import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.*;
import java.sql.*;

import org.junit.Before;
import org.junit.Test;

import simple.test.UnitTest;

import com.usatech.applayer.EmbeddedDbService.BinRangeFileType;

public class EmbeddedDbServiceTest extends UnitTest {

	@Before
	public void setUp() throws Exception {
		super.setupDSF("AppLayerService.properties");
	}

	@Test
	public void testLoadBINRanges() throws Exception {
		Connection connection = mock(Connection.class);
		CallableStatement callableStatement = mock(CallableStatement.class);
		when(connection.prepareCall(anyString(), anyInt(), anyInt())).thenReturn(callableStatement);
		when(callableStatement.executeBatch()).thenReturn(new int[] {1000});

		FileInputStream in = new FileInputStream("C:\\Development\\Workspace\\connectbot-ssh2\\GBINP.V005.04012015.P");
		BinRangeFileType fileType = BinRangeFileType.CPT;
		String callId = "INSERT_BIN_RANGE";
		StringWriter sw = new StringWriter();
		PrintWriter out = new PrintWriter(sw);
		
		EmbeddedDbService.loadBINRanges(in, fileType, connection, callId, out);
		
		// InputStream in, BinRangeFileType fileType, Connection conn, String callId, PrintWriter copy
		
		
		
	}

}
