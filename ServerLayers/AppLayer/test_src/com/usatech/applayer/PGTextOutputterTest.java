/** 
 * USA Technologies, Inc. 2011
 * FileImportTaskTest.java by phorsfield, Aug 17, 2011 9:10:21 AM
 */
package com.usatech.applayer;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import simple.app.ServiceException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.io.PGTextOutputter;
import simple.io.ResultsPGTextOutputter;
import simple.results.Results;
import simple.test.UnitTest;

/**
 * 
 * 
 * @author bkrug
 * 
 */
@IntegrationTest
public class PGTextOutputterTest extends UnitTest {

	@Before
	public void setUp() throws IOException, ServiceException {
		super.setupDSF("TestAppLayerService-dev.properties");
		setupLog();
	}
	
	/**
	 */
	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
	

	@Test
	public void testToFile() throws IOException, SQLException, DataLayerException {
		Results results = DataLayerMgr.executeQuery("GET_PAYMENT_SUBTYPE_DETAIL_FROM_SOURCE", null);
		final PGTextOutputter outputter = new ResultsPGTextOutputter(results);
		outputter.setWriter(new FileWriter("C:\\Users\\bkrug\\Documents\\PG_Copy_Format_Test.txt"));
		outputter.setColumnNames(new String[] { "PAYMENT_SUBTYPE_ID", "PAYMENT_SUBTYPE_KEY_ID", "CLIENT_PAYMENT_TYPE_CD", "AUTHORITY_GATEWAY_QUEUE_KEY", "AUTO_AUTH_RESULT_CD", "AUTO_AUTH_RESP_DESC", "AUTO_AUTH_ACTION_CD", "AUTO_AUTH_ACTION_BITMAP", "CARD_REGEX", "CARD_REGEX_BREF", "AUTHORITY_SERVICE_TYPE_ID", "AUTHORITY_NAME", "REMOTE_SERVER_ADDR", "REMOTE_SERVER_ADDR_ALT", "REMOTE_SERVER_PORT",
				"TERMINAL_ENCRYPT_KEY", "TERMINAL_ENCRYPT_KEY2", "TERMINAL_CD", "MERCHANT_CD", "PSD_TIMESTAMP", "PSD_HASH" });
		
		outputter.writeData();
		outputter.getWriter().close();
	}

}