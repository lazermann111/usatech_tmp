package com.usatech.applayer;

import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import simple.bean.ConvertException;
import simple.io.Log;
import simple.test.UnitTest;

public class PaymentMaskTest extends UnitTest {

	@Before
	public void setUp() throws Exception {
		setupLog();
	}

	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
	@Test
	public void testPaymentMask() throws ParseException, ConvertException {
		// String maskRegex = "^([0-9]{9,10}|[0-9]{15})(?:=([0-9])(01[2-3]))?$";
		String maskRegex = "^((?:[0-9]{9,10}|[0-9]{15})(?:=([0-9])(01[2-3]))?)$";
		String bref = "1:1|2:10|3:7";
		String[] cards = { "12341234", "322729000009326", "322729000009326=1013", "222222222=0013" };
		for(String card : cards) {
			Matcher matcher = Pattern.compile(maskRegex).matcher(card);
			if(!matcher.matches())
				log.info("NO MATCH for " + card);
			else
				log.info("Result for " + card + "=" + AuthorizeTask.parseCardInfo(matcher, bref));
		}
	}

}
