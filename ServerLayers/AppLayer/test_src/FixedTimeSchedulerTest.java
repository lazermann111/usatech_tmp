
import java.lang.management.ManagementFactory;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import simple.app.BaseWithConfig;
import simple.app.MainWithConfig;
import simple.app.Publisher;
import simple.io.ByteInput;
import simple.io.Log;
import simple.test.UnitTest;
import simple.util.CollectionUtils;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.app.jms.JMSPublisher;
import com.usatech.layers.common.LegacyUtils;
//import com.usatech.applayer.LegacyUtils;



public class FixedTimeSchedulerTest extends UnitTest {
	
	private Publisher<ByteInput> publisher;
	
	/**
	 * @return the publisher
	 */
	public Publisher<ByteInput> getPublisher() {
		return publisher;
	}
	/**
	 * @param publisher the publisher to set
	 */
	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}
	@SuppressWarnings("unchecked")
	@Before
	public void setUp() throws Exception {			
		setupLog();

		Map<String, Object> namedRefs = new HashMap<String,Object>();
        Configuration config = MainWithConfig.loadConfig("TestAppLayerService-dev.properties", getClass(), null);
        BaseWithConfig.configureDataSourceFactory(config, namedRefs);
        BaseWithConfig.configureDataLayer(config);

		// need to do this to follow include
		PropertiesConfiguration cfg = new PropertiesConfiguration(FixedTimeSchedulerTest.class.getResource("TestAppLayerService-dev.properties"));
		Properties properties = new Properties();
		
		java.util.Iterator<String> i = (java.util.Iterator<String>) cfg.getKeys();
		while(i.hasNext())
		{
			String k = i.next();
			properties.put(k, cfg.getProperty(k));			
		}
		
		Properties pubSubProperties = CollectionUtils.getSubProperties(properties, "simple.app.Publisher.");
		Publisher<ByteInput> publisher = (Publisher<ByteInput>)simple.app.Main.configure(Publisher.class, pubSubProperties, properties, namedRefs, true);
		
		this.setPublisher(publisher);
}
	@After
	public void tearDown() throws Exception {
		Log.finish();
	}

	@Test
	public void testFixedTimeScheduler() throws Exception {
		
		MessageChain mc = new MessageChainV11();
		long startTime=System.currentTimeMillis()+30000L;
		MessageChainStep step1 = mc.addStep("usat.fixedtime.scheduler");
		step1.addStringAttribute("time", String.valueOf(startTime));
		
		MessageChainStep step2 = mc.addStep("usat.email");
		step2.addStringAttribute("emailTo", "yhe@usatech.com");
		step2.addStringAttribute("emailFrom", "schedulertest@usatech.com");
		step2.addStringAttribute("subject", "scheduler works");
		Date date=new Date();
		date.setTime(startTime);
		step2.addStringAttribute("body", "scheduled time to run:"+date);
		
		step1.setNextSteps(0, step2);

		MessageChainService.publish(mc, publisher);
		
		//usat.email
	}
	
	@Test
	public void testFixedTimeESudsScheduler() throws Exception {
		long currentTime=System.currentTimeMillis();
		LegacyUtils.addHostCycleCompleteSchedule(LegacyUtils.getEsudsCompleteCycleQueueKey(), 1, currentTime, 1L, 20, new JMSPublisher());
	}
	
	@Test
	public void testProcessId(){
		log.info(ManagementFactory.getRuntimeMXBean().getName());
	}
	
}
