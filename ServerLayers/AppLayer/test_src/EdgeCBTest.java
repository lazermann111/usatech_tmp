
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.usatech.layers.common.constants.GenericResponseResultCode;
import com.usatech.layers.common.constants.GenericResponseServerActionCode;
import com.usatech.layers.common.messagedata.MessageData_CB;
import com.usatech.layers.common.messagedata.MessageDirection;

import simple.io.ByteArrayUtils;
import simple.io.Log;
import simple.test.UnitTest;
import simple.text.StringUtils;


public class EdgeCBTest extends UnitTest {
	
	@Before
	public void setUp() throws Exception {
		setupLog();
	}
	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
	@Test
	public void testEdgeCBWriteRead1() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		MessageData_CB reply=new MessageData_CB();
		reply.setResultCode(GenericResponseResultCode.OKAY);
		reply.setResponseMessage("responseText");
		reply.setServerActionCode(GenericResponseServerActionCode.PROCESS_PROP_LIST);
		reply.setDirection(MessageDirection.SERVER_TO_CLIENT);
		Map<Integer, String> prop=new HashMap<Integer, String>();
		prop.put(70, "3");
		prop.put(20, "5");
		((MessageData_CB.PropertyValueListAction) reply.getServerActionCodeData()).getPropertyValues().putAll(prop);
		reply.writeData(bb, false);
		bb.flip();
		byte[] dataArray=new byte[bb.remaining()];
		bb.get(dataArray);
		System.out.println("hex is:"+StringUtils.toHex(dataArray));
		String commandHex=StringUtils.toHex(dataArray).substring(4);
		ByteBuffer bb2=ByteBuffer.allocate(1024);
		bb2.put(ByteArrayUtils.fromHex(commandHex));
		bb2.flip();
		MessageData_CB data = new MessageData_CB();
		data.setDirection(MessageDirection.SERVER_TO_CLIENT);
		data.readData(bb2);
		log.info("cb data:"+data);
	}
	@Test
	public void testEdgeCBWriteRead2() throws Exception {
		
		ByteBuffer bb=ByteBuffer.allocate(1024);
		MessageData_CB reply=new MessageData_CB();
		reply.setResultCode(GenericResponseResultCode.OKAY);
		reply.setResponseMessage("responseText");
		reply.setServerActionCode(GenericResponseServerActionCode.UPLOAD_PROPERTY_VALUE_LIST);
		reply.setDirection(MessageDirection.SERVER_TO_CLIENT);
		Map<Integer, String> prop=new HashMap<Integer, String>();
		prop.put(70, "3");
		prop.put(20, "5");
		//reply.getPropertyValues().putAll(prop);
		reply.writeData(bb, false);
		bb.flip();
		byte[] dataArray=new byte[bb.remaining()];
		bb.get(dataArray);
		System.out.println("hex is:"+StringUtils.toHex(dataArray));
		String commandHex=StringUtils.toHex(dataArray).substring(4);
		ByteBuffer bb2=ByteBuffer.allocate(1024);
		bb2.put(ByteArrayUtils.fromHex(commandHex));
		bb2.flip();
		MessageData_CB data = new MessageData_CB();
		data.setDirection(MessageDirection.SERVER_TO_CLIENT);
		data.readData(bb2);
		log.info("cb data:"+data);
	}
	
	@Test
	public void testEdgeCBWriteRead3() throws Exception {
		ByteBuffer bb=ByteBuffer.allocate(1024);
		MessageData_CB reply=new MessageData_CB();
		reply.setResultCode(GenericResponseResultCode.OKAY);
		reply.setResponseMessage("responseText");
		reply.setServerActionCode(GenericResponseServerActionCode.REBOOT);
		reply.setDirection(MessageDirection.SERVER_TO_CLIENT);
		((MessageData_CB.ReconnectAction) reply.getServerActionCodeData()).setReconnectTime(300);
		reply.writeData(bb, false);
		bb.flip();
		byte[] dataArray=new byte[bb.remaining()];
		bb.get(dataArray);
		System.out.println("hex is:"+StringUtils.toHex(dataArray));
		String commandHex=StringUtils.toHex(dataArray).substring(4);
		ByteBuffer bb2=ByteBuffer.allocate(1024);
		bb2.put(ByteArrayUtils.fromHex(commandHex));
		bb2.flip();
		MessageData_CB data = new MessageData_CB();
		data.setDirection(MessageDirection.SERVER_TO_CLIENT);
		data.readData(bb2);
		log.info("cb data:"+data);
	}
}
