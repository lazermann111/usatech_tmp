import java.nio.ByteBuffer;
import java.text.ParseException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageDataFactory;
import com.usatech.layers.common.messagedata.MessageDataUtils;
import com.usatech.layers.common.messagedata.MessageData_87;

import simple.app.ServiceException;
import simple.io.ByteArrayUtils;
import simple.io.Log;
import simple.test.UnitTest;
import simple.text.StringUtils;


public class WriteMessageTest extends UnitTest {
	@Before
	public void setUp() throws Exception {
		setupLog();
	}
	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
	public String testAddrLength(String memoryCode, int addr, int leng){
		   String stored=StringUtils.toHex(memoryCode)+StringUtils.toHex(addr)+StringUtils.toHex(leng);
		   log.info("stored:"+stored);
		   return stored;
	}
	public MessageData writeMessageFromBytes(MessageType messageType, byte[] commandBytes) throws ServiceException{
		MessageData reply=MessageDataFactory.createMessageData(messageType);
		if(commandBytes!=null){
			ByteBuffer bb = ByteBuffer.allocate(1024);
			if(messageType.isMessageIdExpected()){
				ProcessingUtils.writeLongInt(bb, System.currentTimeMillis());
			}
			bb.put(commandBytes);
			bb.flip();
			try{
				reply.readData(bb);
			}catch(ParseException e){
				throw new ServiceException("Could not parse commandBytes:"+commandBytes, e);
			}
		}
		return reply;
	}
	@Test
	public void testGxPeek() throws Exception {
		testGxPeekHelper("B", 0, 255);
		testGxPeekHelper("B", 0, 136);
		testGxPeekHelper("B",71, 142);
		testGxPeekHelper("B",178, 156);
	}
	
	public void testGxPeekHelper(String memoryC, int startAddress, int length) throws Exception {
		String stored= testAddrLength(memoryC, startAddress, length);
		String memoryCode=stored.substring(0, 2);
	   String addr=stored.substring(2, 10);
	   String leng=stored.substring(10);
	   log.info("memoryCode:"+new String(ByteArrayUtils.fromHex(memoryCode))+" addr:"+Integer.parseInt(addr, 16)+" leng:"+Integer.parseInt(leng, 16));
	   log.info("from stored:"+new String(ByteArrayUtils.fromHex(stored)));
	   MessageData_87 reply=(MessageData_87)writeMessageFromBytes(MessageType.GX_PEEK, ByteArrayUtils.fromHex(stored));
	   assert reply.getMemoryCode()==(byte)memoryC.charAt(0);
	   assert reply.getStartingAddress()==startAddress;
	   assert reply.getNumberOfBytesToRead()==length;
	}
}
