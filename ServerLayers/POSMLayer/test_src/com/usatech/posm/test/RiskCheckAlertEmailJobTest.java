package com.usatech.posm.test;

import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import java.util.*;

import org.apache.commons.configuration.Configuration;
import org.junit.BeforeClass;
import org.junit.Test;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;

import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.constants.MessageAttrEnum;
import com.usatech.posm.schedule.RiskCheckAlertEmailJob;
import com.usatech.posm.schedule.SubmerchantsUploadJob;
import com.usatech.posm.tasks.RiskCheckTask;

import simple.app.*;
import simple.io.ByteInput;
import simple.io.Log;
import simple.io.resource.ResourceFolder;
import simple.util.LRUMap;

public class RiskCheckAlertEmailJobTest {
	private static final Log log = Log.getLog();
	
	private static Configuration config;
	
	@BeforeClass
	public static void init() throws Exception {
		config = MainWithConfig.loadConfig("POSMLayerService.properties", RiskCheckTask.class, null);
		BaseWithConfig.configureDataSourceFactory(config, null);
		BaseWithConfig.configureDataLayer(config);
//    BaseWithConfig.configureStatics(config, null);
//    MainWithConfig.configureSystem(null, null);
	}
	
	@Test
	public void testRiskCheck() throws Exception{
		try {
			JobExecutionContext mockJobExecutionContext = mock(JobExecutionContext.class, withSettings().verboseLogging());
			JobDataMap jobDataMap = mock(JobDataMap.class, withSettings().verboseLogging());
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("mail.smtp.host", "127.0.0.1");
			map.put("mail.smtp.port", 25);
			
			when(mockJobExecutionContext.getMergedJobDataMap()).thenReturn(jobDataMap);
			when(jobDataMap.getWrappedMap()).thenReturn(map);
			
			//RiskCheckAlertEmailJob job = new RiskCheckAlertEmailJob();
			RiskCheckAlertEmailJob job = BaseWithConfig.configure(RiskCheckAlertEmailJob.class, config);
			job.setReportDays(999);
			
			job.executePostConfigure(mockJobExecutionContext);
		} catch (Throwable e) {
			e.printStackTrace();
			throw e;
		}
	}
}
