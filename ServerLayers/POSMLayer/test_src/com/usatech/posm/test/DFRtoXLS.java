package com.usatech.posm.test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Types;
import java.text.Format;
import java.util.Date;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.Argument;
import simple.db.BasicDataSourceFactory;
import simple.db.Call;
import simple.db.Cursor;
import simple.db.Parameter;
import simple.results.DatasetHandler;
import simple.results.DatasetHandlerSelectorFacade;
import simple.results.Results;
import simple.sql.SQLType;
import simple.swt.UIUtils;
import simple.text.MultiRecordDelimitedParser;
import simple.text.StringUtils;
import simple.util.sheet.WriteExcelDatasetHandler;

import com.usatech.posm.schedule.dfr.DFRSetup;

public class DFRtoXLS {
	protected final MultiRecordDelimitedParser<String> parser;

	public static void main(String[] args) throws Exception {
		processCommandLine("C:\\Users\\bkrug\\Documents\\Chase Paymentech\\0000248258.150624.d.B304.dfr");
		// processDatabase("C:\\Users\\bkrug\\Documents\\Chase Paymentech", "07/11/2015");
	}

	protected static void processCommandLine(String... args) throws Exception {
		if(args == null || args.length == 0) {
			printUsage();
			System.exit(1);
			return;
		}
		File inFile = new File(args[0]);
		if(!inFile.canRead()) {
			logErrorAndExit("Input file '" + inFile.getAbsolutePath() + "' either does not exist or is not readable", 2);
			return;
		}
		File outFile;
		if(args.length > 1 && !StringUtils.isBlank(args[1]))
			outFile = new File(args[1]);
		else
			outFile = new File(inFile.getPath() + ".xls");
		DFRtoXLS dfrtoXls = new DFRtoXLS("\n");
		Reader reader = new FileReader(inFile);
		try {
			dfrtoXls.process(reader, outFile);
		} finally {
			reader.close();
		}
	}

	protected static void processDatabase(String... args) throws Exception {
		File dir;
		if(args.length > 0 && !StringUtils.isBlank(args[0]))
			dir = new File(args[0]);
		else
			dir = new File(".");
		
		Date start;
		if(args.length > 1 && !StringUtils.isBlank(args[1]))
			start = ConvertUtils.convert(Date.class, args[1]);
		else
			start = new Date(0L);
		
		Call call = new Call();
		call.setSql("SELECT FILE_NAME, FILE_CONTENT FROM REPORT.FILE_CACHE WHERE FILE_NAME LIKE '%.d.A___.dfr' AND FILE_MODIFIED_TS >= ?");
		Cursor cursor = new Cursor();
		cursor.setAllColumns(true);
		call.setArguments(new Argument[] { cursor, new Parameter("startDate", new SQLType(Types.TIMESTAMP), true, true, false) });
		BasicDataSourceFactory bdsf = new BasicDataSourceFactory();
		String username = System.getProperty("dbUserName", System.getProperty("user.name").toUpperCase());
		String password = UIUtils.promptForPassword("Enter the OPER password for " + username, "Database Login", null);
		bdsf.addDataSource(
				"OPER",
				"oracle.jdbc.driver.OracleDriver",
				"jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.120)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.121)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.122)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.220)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.221)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.222)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)))",
				username, password);
		try {
			Connection conn = bdsf.getDataSource("OPER").getConnection();
			try {
				Results results = call.executeQuery(conn, new Object[] { start }, null);
				while(results.next()) {
					String fileName = results.getValue(1, String.class);
					InputStream content = results.getValue(2, InputStream.class);
					DFRtoXLS dfrtoXls = new DFRtoXLS("\n");
					dfrtoXls.process(new InputStreamReader(content), new File(dir, fileName + ".xls"));
				}
				results.close();
			} finally {
				conn.close();
			}
		} finally {
			bdsf.close();
		}
	}
	public DFRtoXLS(String lineSeparator) {
		parser = DFRSetup.createFinancialDFRParser(true);
		parser.setLineSeparator(lineSeparator);
	}

	public void process(Reader reader, File outFile) throws Exception {
		if(outFile.exists() && !outFile.canWrite()) {
			logErrorAndExit("Output file '" + outFile.getAbsolutePath() + "' is not writable", 2);
			return;
		}

		WriteExcelDatasetHandler handler;
		OutputStream out = new FileOutputStream(outFile);
		try {
			handler = new WriteExcelDatasetHandler(out);
			handler.setSelectorColumn(parser.getSelectorColumn().getLabel());
			DatasetHandlerSelectorFacade<Exception> selectorHandler = new DatasetHandlerSelectorFacade<>(parser.getSelectorColumn().getLabel(), handler);
			final Format currencyFormat = ConvertUtils.getFormat("CURRENCY");
			// selectorHandler.addHandler("RFIN0011", dafh.feeSummaryHandler);
			selectorHandler.addHandler("RACT0010", new DatasetHandler<ServiceException>() {
				protected BigDecimal amount = BigDecimal.ZERO;
				protected long count = 0;

				@Override
				public void handleValue(String columnName, Object value) throws ServiceException {
					switch(columnName) {
						case "Amount":
							count++;
							try {
								amount = amount.add(ConvertUtils.convert(BigDecimal.class, value));
							} catch(ConvertException e) {
								throw new ServiceException(e);
							}
							break;
					}
				}

				@Override
				public void handleRowStart() throws ServiceException {
					// TODO Auto-generated method stub

				}

				@Override
				public void handleRowEnd() throws ServiceException {
					// TODO Auto-generated method stub

				}

				@Override
				public void handleDatasetStart(String[] columnNames) throws ServiceException {
					// TODO Auto-generated method stub

				}

				@Override
				public void handleDatasetEnd() throws ServiceException {
					System.out.println("File '" + outFile.getAbsolutePath() + "' contained " + count + " Sales/Refunds for " + currencyFormat.format(amount));
				}
			});
			selectorHandler.addHandler("RACT0036", new DatasetHandler<ServiceException>() {
				protected BigDecimal amount = BigDecimal.ZERO;
				protected long count = 0;

				@Override
				public void handleValue(String columnName, Object value) throws ServiceException {
					switch(columnName) {
						case "Amount":
							count++;
							try {
								amount = amount.add(ConvertUtils.convert(BigDecimal.class, value));
							} catch(ConvertException e) {
								throw new ServiceException(e);
							}
							break;
					}
				}

				@Override
				public void handleRowStart() throws ServiceException {
					// TODO Auto-generated method stub

				}

				@Override
				public void handleRowEnd() throws ServiceException {
					// TODO Auto-generated method stub

				}

				@Override
				public void handleDatasetStart(String[] columnNames) throws ServiceException {
					// TODO Auto-generated method stub

				}

				@Override
				public void handleDatasetEnd() throws ServiceException {
					System.out.println("File '" + outFile.getAbsolutePath() + "' contained " + count + " Auths for " + currencyFormat.format(amount));
				}
			});
			// selectorHandler.addHandler("RACT0002", dafh.exceptionHandler);
			// selectorHandler.addHandler("RANS0013", dafh.downgradeHandler);
			parser.parse(reader, selectorHandler);
		} finally {
			out.close();
		}
		System.out.println("Wrote " + handler.getRows() + " rows to '" + outFile.getAbsolutePath() + "'");
	}

	protected static void logErrorAndExit(String message, int exitCode) {
		System.err.println(message);
		printUsage();
		System.exit(exitCode);
	}

	protected static void printUsage() {
		System.err.println("Arguments: <DFR Input File Path> (<Excel Output File Path>)");
	}

}
