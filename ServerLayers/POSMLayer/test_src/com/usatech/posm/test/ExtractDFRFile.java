package com.usatech.posm.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.InflaterInputStream;

import simple.io.IOUtils;

public class ExtractDFRFile {
	public static void main(String args[]) throws Exception {
		File file = new File(args[0]);
		if (!file.getName().endsWith(".z")) {
			File zFile = new File(file.getAbsolutePath() + ".z");
			file.renameTo(zFile);
			file = zFile;
		}
		File result = new File(file.getParentFile(), file.getName().replaceFirst("\\.z$", ""));
		long bytes = IOUtils.readInto(new InflaterInputStream(new FileInputStream(file)), new FileOutputStream(result), 1024);
		System.out.printf("Wrote %s bytes\n", bytes);
	}
}
