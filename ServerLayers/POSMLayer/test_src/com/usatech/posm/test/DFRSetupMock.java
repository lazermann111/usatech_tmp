package com.usatech.posm.test;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Time;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicLong;

import com.usatech.app.AbstractAttributeDatasetHandler;
import com.usatech.app.ParallelBatchUpdateDatasetHandler;
import com.usatech.layers.common.ProcessingUtils;

import simple.app.DatabasePrerequisite;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.db.CallNotFoundException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DbUtils;
import simple.db.ParameterException;
import simple.io.Log;
import simple.lang.SystemUtils;
import simple.results.DatasetHandler;
import simple.results.DatasetHandlerSelectorFacade;
import simple.text.AbstractDelimitedParser.ExtraHandling;
import simple.text.DelimitedColumn;
import simple.text.MultiRecordDelimitedParser;
import simple.text.StringUtils;

public class DFRSetupMock {
	private static final Log log = Log.getLog();
	public static final String SELECTOR_COLUMN = "RecordType";
	protected static int maxBatchSize = 100;
	protected static String alertEmailTo = null;
	protected static String alertEmailFrom = "DFR_Processing@usatech.com";
	protected static String dmsRoot = "http://127.0.0.1:8780/";
	protected static final Object DUMMY = new Object();
	
	public static MultiRecordDelimitedParser<String> createFinancialDFRParser(boolean longNames) {
		MultiRecordDelimitedParser<String> parser = new MultiRecordDelimitedParser<>();
		parser.setColumnSeparator("|");
		parser.setLineSeparator("\n");
		parser.setQuotes('"');
		parser.setSelectorColumn(SELECTOR_COLUMN, String.class);
		parser.setExtraColumnHandling(ExtraHandling.OUTPUT);
		parser.setMissingSelectorHandling(ExtraHandling.OUTPUT);
		parser.setRecordColumns("RACT0012", new DelimitedColumn[] {
			new DelimitedColumn(longNames ? "Entity Type" : "entityType", String.class),
			new DelimitedColumn(longNames ? "Entity #" : "entity", Long.class),
			new DelimitedColumn(longNames ? "Presentment Currency" : "presentmentCurrency", String.class),
			new DelimitedColumn(longNames ? "Submission #" : "submission", String.class),
			new DelimitedColumn(longNames ? "PID" : "pid", Long.class),
			new DelimitedColumn(longNames ? "PID Short Name" : "pidName", String.class),
			new DelimitedColumn(longNames ? "Submission Date" : "submissionDate", Date.class),
			new DelimitedColumn(longNames ? "Submission Time" : "submissionTime", Time.class),
			new DelimitedColumn(longNames ? "Transaction Count" : "tranCount", Long.class),
			new DelimitedColumn(longNames ? "Authorization Count" : "authCount", Long.class),
			new DelimitedColumn(longNames ? "Non-Financial Transaction Count" : "nonFinancialCount", Long.class),
			new DelimitedColumn(longNames ? "Declined Deposit Count" : "declinedDepositCount", Long.class),
			new DelimitedColumn(longNames ? "Rejected Transaction Count" : "rejectedCount", Long.class),
			new DelimitedColumn(longNames ? "Cancelled/On-Hold Deposit Count" : "onHoldCount", Long.class),
			new DelimitedColumn(longNames ? "Cancelled/ OnHold Net Deposit Amount" : "onHoldAmount", BigDecimal.class),
			new DelimitedColumn(longNames ? "Successful Deposit Count" : "successCount", Long.class),
			new DelimitedColumn(longNames ? "Successful Net Deposit Amount" : "successAmount", BigDecimal.class),
			new DelimitedColumn(longNames ? "Status" : "status", String.class),
		});
		parser.setRecordColumns("RACT0010", new DelimitedColumn[] {
			new DelimitedColumn(longNames ? "Submission Date" : "submissionDate", Date.class),
			new DelimitedColumn(longNames ? "PID" : "pid", Long.class),
			new DelimitedColumn(longNames ? "PID Short Name" : "pidName", String.class),
			new DelimitedColumn(longNames ? "Submission #" : "submission", String.class),
			new DelimitedColumn(longNames ? "Record #" : "record", Long.class),
			new DelimitedColumn(longNames ? "Entity Type" : "entityType", String.class),
			new DelimitedColumn(longNames ? "Entity #" : "entity", Long.class),
			new DelimitedColumn(longNames ? "Presentment Currency" : "presentmentCurrency", String.class),
			new DelimitedColumn(longNames ? "Merchant Order #" : "merchantOrder", String.class),
			new DelimitedColumn(longNames ? "RDFI #" : "rdfi", Long.class),
			new DelimitedColumn(longNames ? "Account #" : "account", String.class),
			new DelimitedColumn(longNames ? "Expiration Date" : "expirationDate", String.class),
			new DelimitedColumn(longNames ? "Amount" : "amount", BigDecimal.class),
			new DelimitedColumn(longNames ? "MOP" : "cardType", String.class),
			new DelimitedColumn(longNames ? "Action Code" : "actionCd", String.class),
			new DelimitedColumn(longNames ? "Auth Date" : "authDate", Date.class),
			new DelimitedColumn(longNames ? "Auth Code" : "authCd", String.class),
			new DelimitedColumn(longNames ? "Auth Response Code" : "authResponseCd", String.class),
			new DelimitedColumn(longNames ? "Trace #"  : "trace", Integer.class),
			new DelimitedColumn(longNames ? "Consumer Country Code" : "consumerCountryCd", String.class),
			new DelimitedColumn(longNames ? "Reserved" : "reserved", String.class),
			new DelimitedColumn(longNames ? "MCC" : "mcc", Short.class),
			new DelimitedColumn(longNames ? "Token Indicator" : "tokenIndicator", String.class),			
			new DelimitedColumn(longNames ? "Cash Back Amount" : "cashBackAmount", BigDecimal.class),
			new DelimitedColumn(longNames ? "Surcharge Amount" : "surchargeAmount", BigDecimal.class),
			new DelimitedColumn(longNames ? "Voucher #" : "voucher", String.class),
			new DelimitedColumn(longNames ? "EBT Account Type" : "ebtAccountType", String.class),
			new DelimitedColumn(longNames ? "Interchange Qualification" : "interchange", String.class),
			new DelimitedColumn(longNames ? "Durbin Regulated" : "durbinFlag", String.class),
			new DelimitedColumn(longNames ? "Interchange Unit Fee" : "interchangeUnitFee", BigDecimal.class),
			new DelimitedColumn(longNames ? "Interchange Face Value % Fee" : "interchangePercentFee", BigDecimal.class),
			new DelimitedColumn(longNames ? "Total Interchange Amount" : "interchangeAmount", BigDecimal.class),
			new DelimitedColumn(longNames ? "Total Assessment Amount" : "assessmentAmount", BigDecimal.class),
			new DelimitedColumn(longNames ? "Other Debit Passthrough Fees" : "debitPassthroughFees", BigDecimal.class),	
			new DelimitedColumn(longNames ? "Merchant Information" : "merchantInfo", String.class),
			new DelimitedColumn(longNames ? "Merchant Surcharge Amount" : "merchantSurchargeAmount", BigDecimal.class),
		});
		parser.setRecordColumns("RFIN0011", new DelimitedColumn[] {
			new DelimitedColumn(longNames ? "Category" : "category", String.class),
			new DelimitedColumn(longNames ? "Sub-Category" : "subcategory", String.class),
			new DelimitedColumn(longNames ? "Entity Type" : "entityType", String.class),
			new DelimitedColumn(longNames ? "Entity #" : "entity", Long.class),
			new DelimitedColumn(longNames ? "Funds Transfer Instruction #" : "fti", Long.class),
			new DelimitedColumn(longNames ? "Secure BA #" : "secureBA", Long.class),
			new DelimitedColumn(longNames ? "Settlement Currency" : "settlementCurrency", String.class),
			new DelimitedColumn(longNames ? "Fee Schedule" : "feeSchedule", Long.class),
			new DelimitedColumn(longNames ? "MOP" : "cardType", String.class),
			new DelimitedColumn(longNames ? "Interchange Qualification" : "interchange", String.class),
			new DelimitedColumn(longNames ? "Fee Type Description" : "description", String.class),
			new DelimitedColumn(longNames ? "Action Type" : "actionType", String.class),
			new DelimitedColumn(longNames ? "Unit Quantity" : "quantity", Long.class),
			new DelimitedColumn(longNames ? "Unit Fee" : "fixed", BigDecimal.class),
			new DelimitedColumn(longNames ? "Amount" : "amount", BigDecimal.class),
			new DelimitedColumn(longNames ? "% Rate" : "rate", BigDecimal.class),
			new DelimitedColumn(longNames ? "Total Charge" : "total", BigDecimal.class),
		});
		parser.setRecordColumns("RACT0002", new DelimitedColumn[] {
			new DelimitedColumn(longNames ? "Submission Date" : "submissionDate", Date.class),
			new DelimitedColumn(longNames ? "PID" : "pid", Long.class),
			new DelimitedColumn(longNames ? "PID Short Name" : "pidName", String.class),
			new DelimitedColumn(longNames ? "Submission #" : "submission", String.class),
			new DelimitedColumn(longNames ? "Record #" : "record", Long.class),
			new DelimitedColumn(longNames ? "Entity Type" : "entityType", String.class),
			new DelimitedColumn(longNames ? "Entity #" : "entity", Long.class),
			new DelimitedColumn(longNames ? "Presentment Currency" : "presentmentCurrency", String.class),
			new DelimitedColumn(longNames ? "Merchant Order #" : "merchantOrder", String.class),
			new DelimitedColumn(longNames ? "RDFI #" : "rdfi", Long.class),
			new DelimitedColumn(longNames ? "Account #" : "account", String.class),
			new DelimitedColumn(longNames ? "Expiration Date" : "expirationDate", String.class),
			new DelimitedColumn(longNames ? "Amount" : "amount", BigDecimal.class),
			new DelimitedColumn(longNames ? "MOP" : "cardType", String.class),
			new DelimitedColumn(longNames ? "Action Code" : "actionCd", String.class),
			new DelimitedColumn(longNames ? "Auth Date" : "authDate", Date.class),
			new DelimitedColumn(longNames ? "Auth Code" : "authCd", String.class),
			new DelimitedColumn(longNames ? "Auth Response Code" : "authResponseCd", String.class),
			new DelimitedColumn(longNames ? "Trace #" : "trace", Integer.class),
			new DelimitedColumn(longNames ? "Consumer Country Code" : "consumerCountryCd", String.class),
			new DelimitedColumn(longNames ? "Category" : "category", String.class),
			new DelimitedColumn(longNames ? "MCC" : "mcc", Short.class),
			new DelimitedColumn(longNames ? "Reject Code" : "rejectCd", String.class),
			new DelimitedColumn(longNames ? "Submission Status" : "submissionStatus", String.class),
			new DelimitedColumn(longNames ? "Settlement Currency" : "settlementCurrency", String.class),
		});
		parser.setRecordColumns("RACT0036", new DelimitedColumn[] {
			new DelimitedColumn(longNames ? "Entity Type" : "entityType", String.class),
			new DelimitedColumn(longNames ? "Entity #": "entity", Long.class),
			new DelimitedColumn(longNames ? "Presentment Currency" : "presentmentCurrency", String.class),
			new DelimitedColumn(longNames ? "Submission Date" : "submissionDate", Date.class),
			new DelimitedColumn(longNames ? "PID" : "pid", Long.class),
			new DelimitedColumn(longNames ? "PID Short Name" : "pidName", String.class),
			new DelimitedColumn(longNames ? "Submission #" : "submission", String.class),
			new DelimitedColumn(longNames ? "Record #" : "record", Long.class),
			new DelimitedColumn(longNames ? "Transaction Division #": "transactionDivision", String.class),
			new DelimitedColumn(longNames ? "Merchant Order #": "merchantOrder", String.class),
			new DelimitedColumn(longNames ? "RDFI #": "rdfi", Long.class),
			new DelimitedColumn(longNames ? "Account #" : "account", String.class),
			new DelimitedColumn(longNames ? "Expiration Date" : "expirationDate", String.class),
			new DelimitedColumn(longNames ? "Amount": "amount", BigDecimal.class),
			new DelimitedColumn(longNames ? "Settlement Currency" : "settlementCurrency", String.class),				
			new DelimitedColumn(longNames ? "MOP" : "cardType", String.class),
			new DelimitedColumn(longNames ? "Action Code": "actionCd", String.class),
			new DelimitedColumn(longNames ? "Transaction Type" : "transactionType", String.class),				
			new DelimitedColumn(longNames ? "Auth Date": "authDate", Date.class),
			new DelimitedColumn(longNames ? "Auth Code" : "authCd", String.class),
			new DelimitedColumn(longNames ? "Auth Response Code": "authResponseCd", String.class),
			new DelimitedColumn(longNames ? "Vendor Auth Response":"vendorAuthResponse", String.class),
			new DelimitedColumn(longNames ? "Auth Initiator":"authInitiator", String.class),
			new DelimitedColumn(longNames ? "Auth Source":"authSource", String.class),
			new DelimitedColumn(longNames ? "POS Auth Source":"posAuthSource", String.class),
			new DelimitedColumn(longNames ? "Voice Auth Indicator":"voiceAuthIndicator", String.class),
			new DelimitedColumn(longNames ? "Vendor Line":"vendorLine", String.class),
			new DelimitedColumn(longNames ? "PTI AVS Response":"avsResponse", String.class),
			new DelimitedColumn(longNames ? "Vendor AVS Response":"vendorAvsResponse", String.class),
			new DelimitedColumn(longNames ? "Card Security Value Response ":"cvvResponse", String.class),
			new DelimitedColumn(longNames ? "Consumer Bank Country Code":"consumerBankCountryCd", String.class),
			new DelimitedColumn(longNames ? "Trace #" : "trace", Integer.class),
			new DelimitedColumn(longNames ? "MCC": "mcc", Short.class),
			new DelimitedColumn(longNames ? "Enhanced AVS Response":"enhancedAvsResponse", String.class),
			new DelimitedColumn(longNames ? "Encrypt":"encrypt", String.class),
			new DelimitedColumn(longNames ? "Fraud Score Request":"fraudScoreRequest", String.class),
			new DelimitedColumn(longNames ? "Fraud Score Response":"fraudScoreResponse", String.class),
			new DelimitedColumn(longNames ? "Country of Issuance":"countryOfIssuance", String.class),
			new DelimitedColumn(longNames ? "Durbin Regulated" : "durbinFlag", String.class),
			new DelimitedColumn(longNames ? "Commercial Card":"commercialCard", String.class),
			new DelimitedColumn(longNames ? "Prepaid Card":"prepaidCard", String.class),
			new DelimitedColumn(longNames ? "Payroll Card":"payrollCard", String.class),
			new DelimitedColumn(longNames ? "Healthcard Card":"healthcareCard", String.class),
			new DelimitedColumn(longNames ? "Affluent Card":"affluentCard", String.class),
			new DelimitedColumn(longNames ? "Signature Debit":"signatureDebit", String.class),
			new DelimitedColumn(longNames ? "PINless Debit":"pinlessDebit", String.class),
			new DelimitedColumn(longNames ? "Level III Capable":"level3Capable", String.class),
			new DelimitedColumn(longNames ? "Reserved 1":"reversed1", String.class),
			new DelimitedColumn(longNames ? "Reserved 2":"reversed2", String.class),
			new DelimitedColumn(longNames ? "Pre/Final Authorization":"preFinalAuth", String.class),
			new DelimitedColumn(longNames ? "Token Indicator":"tokenIndicator", String.class),
		});
		parser.setRecordColumns("RANS0013", new DelimitedColumn[] {
			new DelimitedColumn(longNames ? "Entity Type" : "entityType", String.class),
			new DelimitedColumn(longNames ? "Entity #" : "entity", Long.class),
			new DelimitedColumn(longNames ? "PID": "pid", Long.class),				
			new DelimitedColumn(longNames ? "Currency" : "currency", String.class),
			new DelimitedColumn(longNames ? "MOP" : "cardType", String.class),
			new DelimitedColumn(longNames ? "Interchange Qualification" : "interchange", String.class),
			new DelimitedColumn(longNames ? "Downgrade Reason Code" : "downgradeReasonCd", String.class),
			new DelimitedColumn(longNames ? "Downgrade Reason Description" : "downgradeReasonDesc", String.class),
			new DelimitedColumn(longNames ? "Account #" : "account", String.class),
			new DelimitedColumn(longNames ? "Merchant Order #" : "merchantOrder", String.class),				
			new DelimitedColumn(longNames ? "Deposit Date" : "depositDate", Date.class),
			new DelimitedColumn(longNames ? "Amount" : "amount", BigDecimal.class),			
		});
		return parser;
	}
	
	protected static class FeeKey {
		protected String actionType;
		protected String cardType;
		protected String interchange;
		protected Integer entity;

		public String getActionType() {
			return actionType;
		}

		public void setActionType(String actionType) {
			this.actionType = actionType;
		}

		public String getCardType() {
			return cardType;
		}

		public void setCardType(String cardType) {
			this.cardType = cardType;
		}

		public String getInterchange() {
			return interchange;
		}

		public void setInterchange(String interchange) {
			this.interchange = interchange;
		}

		public Integer getEntity() {
			return entity;
		}

		public void setEntity(Integer entity) {
			this.entity = entity;
		}

		@Override
		public int hashCode() {
			return SystemUtils.addHashCodes(0, entity, actionType, cardType, interchange);
		}

		@Override
		public boolean equals(Object obj) {
			if(!(obj instanceof FeeKey))
				return false;
			FeeKey fk = (FeeKey) obj;
			return ConvertUtils.areEqual(entity, fk.entity) && ConvertUtils.areEqual(actionType, fk.actionType) && ConvertUtils.areEqual(cardType, fk.cardType) && ConvertUtils.areEqual(interchange, fk.interchange);
		}

		@Override
		public String toString() {
			return new StringBuilder().append("Entity #=").append(entity).append("; Action Type=").append(actionType).append("; MOP=").append(cardType).append("; Interchange Qualification=").append(interchange).toString();
		}
	}

	protected static class FeeDetail {
		protected String description;
		protected Long quantity;
		protected BigDecimal fixed;
		protected BigDecimal amount;
		protected BigDecimal rate;

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public Long getQuantity() {
			return quantity;
		}

		public void setQuantity(Long quantity) {
			this.quantity = quantity;
		}

		public BigDecimal getFixed() {
			return fixed;
		}

		public void setFixed(BigDecimal fixed) {
			this.fixed = fixed;
		}

		public BigDecimal getAmount() {
			return amount;
		}

		public void setAmount(BigDecimal amount) {
			this.amount = amount;
		}

		public BigDecimal getRate() {
			return rate;
		}

		public void setRate(BigDecimal rate) {
			this.rate = rate;
		}
	}

	public static DatasetHandlerSelectorFacade<ServiceException> createFinancialDFRHandler(final Connection conn,final long fileCacheId, final String fileName, ExecutorService executor) throws ParameterException, CallNotFoundException {
		return createFinancialDFRHandler(conn,fileCacheId, fileName, 0L, executor);
	}

	public static DatasetHandlerSelectorFacade<ServiceException> createFinancialDFRHandler(final Connection conn,final long fileCacheId, final String fileName, final long startLineNumber, ExecutorService executor) throws ParameterException, CallNotFoundException {
		final Map<String, Object> extraData = Collections.singletonMap("fileCacheId", (Object) fileCacheId);
		final int retries = 3;
		DatasetHandlerSelectorFacade<ServiceException> selectorHandler = new DatasetHandlerSelectorFacade<ServiceException>(SELECTOR_COLUMN) {
			@Override
			public void handleDatasetEnd() throws ServiceException {
				super.handleDatasetEnd();
				int i = 0;
				
				while(true) {
					try {
						DataLayerMgr.executeCall("UPDATE_DFR_BATCH_MOCK", extraData, true);
						log.info("Original UPDATE_DFR_BATCH called extraData="+extraData);
					} catch(DataLayerException e) {
						if(i++ < retries) {
							log.info("Could not update DFR batch; retrying...", e);
							continue;
						}
						throw new ServiceException(e);
					} catch(SQLException e) {
						if(DatabasePrerequisite.determineRetryType(e) != WorkRetryType.NO_RETRY && i++ < retries) {
							log.info("Could not update DFR batch; retrying...", e);
							continue;
						}
						throw new ServiceException(e);
					}
					break;
				}
			}

		};
		final DecimalFormat currencyFormat = (DecimalFormat) ConvertUtils.getFormat("CURRENCY");
		currencyFormat.setNegativePrefix(currencyFormat.getNegativePrefix().replace('(', '-'));
		currencyFormat.setNegativeSuffix(currencyFormat.getNegativeSuffix().replace(")", ""));
		final Map<FeeKey, List<FeeDetail>> fees = new LinkedHashMap<>();
		final Map<String, Date> submissionTimestamps = new LinkedHashMap<>();
		final long lineNumberIncrement = getMaxBatchSize() * 100;
		class LineNumberTracker {
			protected final AtomicLong lastLineNumber = new AtomicLong(startLineNumber);
			protected final NavigableMap<Long, Object> pendingLineNumbers = new ConcurrentSkipListMap<>();

			public void pending(long lineNumber) {
				pendingLineNumbers.put(lineNumber, DUMMY);
			}
			public void complete(long lineNumber) {
				if(pendingLineNumbers.remove(lineNumber) == DUMMY) {
					Map.Entry<Long, Object> entry = pendingLineNumbers.firstEntry();
					if(entry == null || entry.getKey() > lineNumber)
						updateLastLineNumber(lineNumber);
					else if(lastLineNumber.get() + lineNumberIncrement < lineNumber && (lineNumber % 100) == 0)
						log.info("Processed line " + lineNumber + " but waiting for line " + entry.getKey());
			}
			}

			protected boolean updateLastLineNumber(long lineNumber) {
				while(true) {
					long l = lastLineNumber.get();
					log.info("updateLastLineNumber l="+l+" lineNumber="+lineNumber);
					if(l + lineNumberIncrement < lineNumber) {
						if(lastLineNumber.compareAndSet(l, lineNumber)) {
							Map<String, Object> params = new HashMap<>();
							params.put("fileCacheId", fileCacheId);
							params.put("lineNumber", lineNumber);
							try {
								
								long startTime=System.currentTimeMillis();
								DataLayerMgr.executeCall(conn,"UPDATE_DFR_LINE_NUMBER", params);
								conn.commit();
								long endTime=System.currentTimeMillis();
								long callSpentTime=endTime-startTime;
								DFRFinancialParserTest.totalUpdateLineCalled++;
								DFRFinancialParserTest.totalUpdateLineCalledTimeSpent=DFRFinancialParserTest.totalUpdateLineCalledTimeSpent+callSpentTime;
								double callSpentTimeAverage=DFRFinancialParserTest.totalUpdateLineCalledTimeSpent/DFRFinancialParserTest.totalUpdateLineCalled;
								log.info("Original UPDATE_DFR_LINE_NUMBER called params="+params+" totalUpdateLineCalled="+DFRFinancialParserTest.totalUpdateLineCalled+" totalUpdateLineCalledTimeSpent="+DFRFinancialParserTest.totalUpdateLineCalledTimeSpent+" callSpentTime="+callSpentTime+" callSpentTimeAverage="+callSpentTimeAverage);
							} catch(SQLException | DataLayerException e) {
								log.warn("Could not update DFR Line Number to " + lineNumber + " for file Id " + fileCacheId, e);
								DbUtils.rollbackSafely(conn);
								return false;
							}
							return true;
						}
					} else
						return false;
				}
		}
		}
		final LineNumberTracker lineNumberTracker = new LineNumberTracker();
		DatasetHandler<ServiceException> submissionListingHandler = new AbstractAttributeDatasetHandler<ServiceException>() {
			public void handleDatasetStart(String[] columnNames) throws ServiceException {
			}

			public void handleRowEnd() throws ServiceException {
				if(log.isDebugEnabled())
					log.debug("ACT0012: " + data);
				try {
					String submissionKey = ConvertUtils.getString(data.get("submission"), true);
					Date submissionDate = ConvertUtils.convertRequired(Date.class, data.get("submissionDate"));
					Time submissionTime = ConvertUtils.convertRequired(Time.class, data.get("submissionTime"));
					LocalTime localTime = submissionTime.toLocalTime();
					Calendar cal = Calendar.getInstance();
					cal.setTime(submissionDate);
					cal.set(Calendar.HOUR_OF_DAY, localTime.getHour());
					cal.set(Calendar.MINUTE, localTime.getMinute());
					cal.set(Calendar.SECOND, localTime.getSecond());
					submissionTimestamps.put(submissionKey, cal.getTime());
				} catch(ConvertException e) {
					throw new ServiceException(e);
				}
			}

			public void handleDatasetEnd() throws ServiceException {
			}
		};
		DatasetHandler<ServiceException> feeSummaryHandler = new AbstractAttributeDatasetHandler<ServiceException>() {
			public void handleDatasetStart(String[] columnNames) throws ServiceException {
			}

			public void handleRowEnd() throws ServiceException {
				if(log.isDebugEnabled())
					log.debug("FIN0011: " + data);
				FeeKey key = new FeeKey();
				FeeDetail detail = new FeeDetail();
				try {
					ReflectionUtils.populateProperties(key, data);
					ReflectionUtils.populateProperties(detail, data);
				} catch(IllegalAccessException | InvocationTargetException | InstantiationException | IntrospectionException | ConvertException | ParseException e) {
					throw new ServiceException(e);
				}
				List<FeeDetail> details = fees.get(key);
				if(details == null) {
					details = new ArrayList<>();
					fees.put(key, details);
				}
				details.add(detail);
			}

			public void handleDatasetEnd() throws ServiceException {
			}
		};
		DatasetHandler<ServiceException> exceptionHandler = new AbstractAttributeDatasetHandler<ServiceException>() {
			protected final StringBuilder message = new StringBuilder();
			public void handleDatasetStart(String[] columnNames) throws ServiceException {
			}

			public void handleRowEnd() throws ServiceException {
				data.put("lineNumber", selectorHandler.getRow());
				if(log.isDebugEnabled())
					log.debug("ACT0002: " + data);
				BigDecimal amount;
				String actionCd;
				try {
					amount = ConvertUtils.convertRequired(BigDecimal.class, data.get("amount"));
					actionCd = ConvertUtils.convertRequired(String.class, data.get("actionCd"));
				} catch(ConvertException e) {
					throw new ServiceException(e);
				}
				if(amount.signum() == 0 || "A".equals(actionCd))
					return;
				// a real rejection; alert!
				message.append("REJECTION: ");
				String merchantOrder = ConvertUtils.getStringSafely(data.get("merchantOrder"));
				if (!StringUtils.isBlank(merchantOrder))
					message.append(dmsRoot).append("tran.i?tran_global_trans_cd=").append(StringUtils.prepareURLPart(merchantOrder)).append(" ");
				message.append(data).append("\n");
			}

			public void handleDatasetEnd() throws ServiceException {
				if(message.length() > 0 && !StringUtils.isBlank(getAlertEmailTo())) {
					try {
						ProcessingUtils.sendEmail(getAlertEmailFrom(), "POSMLayer DFR Processing", getAlertEmailTo(), getAlertEmailTo(), "Rejections found in DFR Report '" + fileName + "'", message.toString(), null);
					} catch(SQLException | DataLayerException e) {
						throw new ServiceException(e);
					}
				}
			}
		};
		DatasetHandler<ServiceException> downgradeHandler = new ParallelBatchUpdateDatasetHandler("UPDATE_DFR_DOWNGRADE_MOCK", getMaxBatchSize(), executor, extraData) {
			public void handleRowEnd() throws ServiceException {
				long lineNumber = selectorHandler.getRow();
				if(lineNumber < startLineNumber) {
					if(log.isDebugEnabled())
						log.debug("Skipping line " + lineNumber + " with ANS0013: " + data);
					data.clear();
					return;
				}
				data.put("lineNumber", lineNumber);
				if(log.isDebugEnabled())
					log.debug("ANS0013: " + data);
				super.handleRowEnd();
			}

			protected final Map<Integer, Long> lineNumbers = new ConcurrentHashMap<>();

			protected void handleBatchPrepare(int batchNumber) {
				long lineNumber = selectorHandler.getRow();
				lineNumbers.put(batchNumber, lineNumber);
				lineNumberTracker.pending(lineNumber);
			}

			protected void handleBatchComplete(int batchNumber) {
				Long lineNumber = lineNumbers.remove(batchNumber);
				if(lineNumber != null)
					log.info("UPDATE_DFR_DOWNGRADE_MOCK handleBatchComplete lineNumber="+lineNumber);
					lineNumberTracker.complete(lineNumber);
			}
		};
		DatasetHandler<ServiceException> saleDetailHandler = new ParallelBatchUpdateDatasetHandler("UPDATE_DFR_SALE_MOCK", getMaxBatchSize(), executor, extraData) {
			public void handleRowEnd() throws ServiceException {
				long lineNumber = selectorHandler.getRow();
				if(lineNumber < startLineNumber) {
					if(log.isDebugEnabled())
						log.debug("Skipping line " + lineNumber + " with ACT0010: " + data);
					data.clear();
					return;
				}
				data.put("lineNumber", lineNumber);
				if(log.isDebugEnabled())
					log.debug("ACT0010: " + data);
				FeeKey key = new FeeKey();
				try {
					ReflectionUtils.populateProperties(key, data);
				} catch(IllegalAccessException | InvocationTargetException | InstantiationException | IntrospectionException | ConvertException | ParseException e) {
					throw new ServiceException(e);
				}
				key.setActionType("S");
				List<FeeDetail> details = fees.get(key);
				if(details != null) {
					BigDecimal amount;
					try {
						amount = ConvertUtils.convertRequired(BigDecimal.class, data.get("amount"));
					} catch(ConvertException e) {
						throw new ServiceException(e);
					}
					StringBuilder feeDesc = new StringBuilder();
					BigDecimal feeTotal = BigDecimal.ZERO;
					for(FeeDetail detail : details) {
						BigDecimal fee = null;
						if(detail.getRate() != null && detail.getRate().signum() != 0)
							fee = detail.getRate().multiply(amount);
						if(detail.getFixed() != null && detail.getFixed().signum() != 0)
							fee = fee == null ? detail.getFixed() : fee.add(detail.getFixed());
						if(fee != null) {
							if(feeDesc.length() > 0)
								feeDesc.append("; ");
							feeDesc.append(detail.getDescription()).append(" (").append(currencyFormat.format(fee)).append(")");
							feeTotal = feeTotal.add(fee);
						}
					}
					data.put("feeTotal", feeTotal);
					data.put("feeDescription", feeDesc);
				}
				String submissionKey;
				try {
					submissionKey = ConvertUtils.getString(data.get("submission"), true);
				} catch(ConvertException e) {
					throw new ServiceException(e);
				}
				Date submissionTimestamp = submissionTimestamps.get(submissionKey);
				if(submissionTimestamp != null)
					data.put("submissionDate", submissionTimestamp);
				super.handleRowEnd();
			}
			
			protected final Map<Integer, Long> lineNumbers = new ConcurrentHashMap<>();

			protected void handleBatchPrepare(int batchNumber) {
				long lineNumber = selectorHandler.getRow();
				lineNumbers.put(batchNumber, lineNumber);
				lineNumberTracker.pending(lineNumber);
			}

			protected void handleBatchComplete(int batchNumber) {
				Long lineNumber = lineNumbers.remove(batchNumber);
				if(lineNumber != null)
					log.info("UPDATE_DFR_SALE_MOCK handleBatchComplete lineNumber="+lineNumber);
					lineNumberTracker.complete(lineNumber);
			}
		};
		DatasetHandler<ServiceException> authDetailHandler = new ParallelBatchUpdateDatasetHandler("UPDATE_DFR_AUTH_MOCK", getMaxBatchSize(), executor, extraData) {
			public void handleRowEnd() throws ServiceException {
				long lineNumber = selectorHandler.getRow();
				if(lineNumber < startLineNumber) {
					if(log.isDebugEnabled())
						log.debug("Skipping line " + lineNumber + " with ACT0036: " + data);
					data.clear();
					return;
				}
				data.put("lineNumber", lineNumber);
				if(log.isDebugEnabled())
					log.debug("ACT0036: " + data);
				FeeKey key = new FeeKey();
				try {
					ReflectionUtils.populateProperties(key, data);
				} catch(IllegalAccessException | InvocationTargetException | InstantiationException | IntrospectionException | ConvertException | ParseException e) {
					throw new ServiceException(e);
				}
				key.setActionType("A");
				List<FeeDetail> details = fees.get(key);
				if(details != null) {
					BigDecimal amount;
					try {
						amount = ConvertUtils.convertRequired(BigDecimal.class, data.get("amount"));
					} catch(ConvertException e) {
						throw new ServiceException(e);
					}
					StringBuilder feeDesc = new StringBuilder();
					BigDecimal feeTotal = BigDecimal.ZERO;
					for(FeeDetail detail : details) {
						BigDecimal fee = null;
						if(detail.getRate() != null && detail.getFixed().signum() != 0)
							fee = detail.getRate().multiply(amount);
						if(detail.getFixed() != null && detail.getFixed().signum() != 0)
							fee = fee == null ? detail.getFixed() : fee.add(detail.getFixed());
						if(fee != null) {
							if(feeDesc.length() > 0)
								feeDesc.append("; ");
							feeDesc.append(detail.getDescription()).append(" (").append(currencyFormat.format(fee)).append(")");
						}
					}
					data.put("feeTotal", feeTotal);
					data.put("feeDescription", feeDesc);
				}
				String submissionKey;
				try {
					submissionKey = ConvertUtils.getString(data.get("submission"), true);
				} catch(ConvertException e) {
					throw new ServiceException(e);
				}
				Date submissionTimestamp = submissionTimestamps.get(submissionKey);
				if(submissionTimestamp != null)
					data.put("submissionDate", submissionTimestamp);
				super.handleRowEnd();
			}

			protected final Map<Integer, Long> lineNumbers = new ConcurrentHashMap<>();

			protected void handleBatchPrepare(int batchNumber) {
				long lineNumber = selectorHandler.getRow();
				lineNumbers.put(batchNumber, lineNumber);
				lineNumberTracker.pending(lineNumber);
			}

			protected void handleBatchComplete(int batchNumber) {
				Long lineNumber = lineNumbers.remove(batchNumber);
				if(lineNumber != null)
					log.info("UPDATE_DFR_AUTH_MOCK handleBatchComplete lineNumber="+lineNumber);
					lineNumberTracker.complete(lineNumber);
			}
		};
		selectorHandler.addHandler("RACT0012", submissionListingHandler);
		selectorHandler.addHandler("RFIN0011", feeSummaryHandler);
		selectorHandler.addHandler("RACT0010", saleDetailHandler);
		selectorHandler.addHandler("RACT0036", authDetailHandler);
		selectorHandler.addHandler("RACT0002", exceptionHandler);
		selectorHandler.addHandler("RANS0013", downgradeHandler);
		return selectorHandler;
	}

	public static MultiRecordDelimitedParser<String> createExceptionDFRParser(boolean longNames) {
		MultiRecordDelimitedParser<String> parser = new MultiRecordDelimitedParser<>();
		parser.setColumnSeparator("|");
		parser.setLineSeparator("\n");
		parser.setQuotes('"');
		parser.setSelectorColumn(SELECTOR_COLUMN, String.class);
		parser.setExtraColumnHandling(ExtraHandling.IGNORE);
		return parser;
	}

	public static int getMaxBatchSize() {
		return maxBatchSize;
	}

	public static void setMaxBatchSize(int maxBatchSize) {
		DFRSetupMock.maxBatchSize = maxBatchSize;
	}

	public static String getAlertEmailTo() {
		return alertEmailTo;
	}

	public static void setAlertEmailTo(String alertEmailTo) {
		DFRSetupMock.alertEmailTo = alertEmailTo;
	}

	public static String getAlertEmailFrom() {
		return alertEmailFrom;
	}

	public static void setAlertEmailFrom(String alertEmailFrom) {
		DFRSetupMock.alertEmailFrom = alertEmailFrom;
	}

	public static String getDmsRoot() {
		return dmsRoot;
	}

	public static void setDmsRoot(String dmsRoot) {
		DFRSetupMock.dmsRoot = dmsRoot;
	}
}
