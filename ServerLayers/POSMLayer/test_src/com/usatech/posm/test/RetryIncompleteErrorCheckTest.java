package com.usatech.posm.test;

import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;

import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.constants.MessageAttrEnum;
import com.usatech.posm.schedule.RetryIncompleteErrorCheck;
import com.usatech.posm.schedule.RiskCheckAlertEmailJob;
import com.usatech.posm.schedule.SubmerchantsUploadJob;
import com.usatech.posm.tasks.RiskCheckTask;

import simple.app.*;
import simple.db.DataLayerException;
import simple.io.ByteInput;
import simple.io.Log;
import simple.io.resource.ResourceFolder;
import simple.test.UnitTest;
import simple.util.LRUMap;

public class RetryIncompleteErrorCheckTest extends UnitTest {
	private static final Log log = Log.getLog();
	
	private static Configuration config;
	
	@BeforeClass
	public static void init() throws Exception {
		setupLog();
	}
	
	@Before
	public void setUp() throws Exception {
		super.setupDSF("POSMLayerService.properties");
	}
	
	@Test
	public void testRiskCheck() throws Exception{
		try {
			JobExecutionContext mockJobExecutionContext = mock(JobExecutionContext.class, withSettings().verboseLogging());
			JobDataMap jobDataMap = mock(JobDataMap.class, withSettings().verboseLogging());
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("mail.smtp.host", "127.0.0.1");
			map.put("mail.smtp.port", 25);
			
			when(mockJobExecutionContext.getMergedJobDataMap()).thenReturn(jobDataMap);
			when(jobDataMap.getWrappedMap()).thenReturn(map);
			
			RetryIncompleteErrorCheck check = new RetryIncompleteErrorCheck();
//			check.setMaxRetries(2);
//			check.setMaxSaleAmount(20);
//			check.setReprocessCountLimit(20);
			check.executePostConfigure(mockJobExecutionContext);
		} catch (Throwable e) {
			e.printStackTrace();
			throw e;
		}
	}
}
