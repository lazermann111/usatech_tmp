package com.usatech.posm.test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.zip.DeflaterInputStream;
import java.util.zip.InflaterInputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.quartz.JobExecutionException;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.Argument;
import simple.db.BasicDataSourceFactory;
import simple.db.Call;
import simple.db.CallNotFoundException;
import simple.db.Cursor;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DataSourceNotFoundException;
import simple.db.Parameter;
import simple.db.ParameterException;
import simple.db.config.ConfigException;
import simple.db.config.ConfigLoader;
import simple.io.ConfigSource;
import simple.io.HeapBufferStream;
import simple.io.IOUtils;
import simple.io.LoadingException;
import simple.io.Log;
import simple.lang.Decision;
import simple.results.DatasetHandlerSelectorFacade;
import simple.results.Results;
import simple.sql.ArraySQLType;
import simple.sql.SQLType;
import simple.swt.UIUtils;
import simple.test.UnitTest;
import simple.text.MultiRecordDelimitedParser;

import com.usatech.app.AbstractAttributeDatasetHandler;
import com.usatech.posm.schedule.dfr.DFRSetup;
import com.usatech.posm.schedule.dfr.ProcessFinancialDFRJob;

public class DFRTests extends UnitTest {
	@Before
	public void prepareLog() {
		System.setProperty("log4j.configuration", "log4j-dev.properties");
		log = Log.getLog();
	}

	@After
	public void finishLog() {
		Log.finish();
	}
	@Test
	public void testFilteringParsing() throws IOException, ParameterException, CallNotFoundException, SQLException, DataSourceNotFoundException{
		String inputFile = "C:\\Users\\bkrug\\Documents\\Chase Paymentech\\0000248258.150624.d.B304.dfr";
		BasicDataSourceFactory bdsf = new BasicDataSourceFactory();
		String username = "SYSTEM"; // System.getProperty("dbUserName", System.getProperty("user.name").toUpperCase());
		String password = UIUtils.promptForPassword("Enter the OPER password for " + username, "Database Login", null);
		bdsf.addDataSource(
				"OPER",
				"oracle.jdbc.driver.OracleDriver",
				"jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=eccdb013.usatech.com)(PORT=1522)))(CONNECT_DATA=(SERVICE_NAME=usadev03)))",
				username, password);
		Connection conn = bdsf.getDataSource("OPER").getConnection();

		MultiRecordDelimitedParser<String> parser = DFRSetup.createFinancialDFRParser(true);
		DatasetHandlerSelectorFacade<ServiceException> selectorHandler = DFRSetup.createFinancialDFRHandler(conn,0L, "0000248258.150624.d.B304.dfr", null);

		final Set<String> filteredSections = new HashSet<>();
		filteredSections.add("RACT0010");
		filteredSections.add("RACT0036");
		Reader reader = new FileReader(inputFile);
		try {
			Reader filtered = parser.newFilter(reader, selectorHandler, new Decision<Object>() {
				@Override
				public boolean decide(Object argument) {
					String value = ConvertUtils.getStringSafely(argument);
					return !filteredSections.contains(value);
				}
			});
			Writer writer = new FileWriter(inputFile + ".filtered");
			try {
				IOUtils.copy(filtered, writer);
				writer.flush();
			} finally {
				writer.close();
			}
		} finally {
			reader.close();
			if(conn!=null){
				conn.close();
			}
		}
	}
	
	@Test
	public void logData() throws IOException, ServiceException {
		String inputFile = "C:\\Users\\bkrug\\Documents\\Chase Paymentech\\0000248258.151129.d.B442-ptiol.dfr";

		MultiRecordDelimitedParser<String> parser = DFRSetup.createFinancialDFRParser(true);
		try (Reader reader = new FileReader(inputFile)) {
			parser.parse(reader, new AbstractAttributeDatasetHandler<ServiceException>() {
				@Override
				public void handleRowEnd() throws ServiceException {
					log.info(data);
				}

				@Override
				public void handleDatasetEnd() throws ServiceException {
					log.info("------------------");
				}

				@Override
				public void handleDatasetStart(String[] columnNames) throws ServiceException {
				}
			});
		}
	}

	@Test
	public void loadFileToDb() throws DataSourceNotFoundException, SQLException, ParameterException, IOException {
		String inputPath = "C:\\Users\\bkrug\\Documents\\Chase Paymentech\\0000248258.151023.d.B440.dfr";
		// String inputPath = "C:\\Users\\bkrug\\Documents\\Chase Paymentech\\test\\0000248258.151030.d.B950.dfr";
		Call call = new Call();
		call.setSql("INSERT INTO REPORT.FILE_CACHE(FILE_NAME, FILE_SOURCE, FILE_CACHE_TYPE_ID, FILE_CONTENT, FILE_COMMENT, FILE_MODIFIED_TS, FILE_CACHE_STATE_ID) SELECT ?, ?, ?, ?, ?, ?, 1 FROM DUAL WHERE NOT EXISTS( SELECT 1 FROM REPORT.FILE_CACHE WHERE FILE_NAME = ? AND NVL(FILE_SOURCE, '-') = NVL(?, '-') AND FILE_MODIFIED_TS >= ?)");
		call.setArguments(new Argument[] { 
			new Parameter(null, null, true, false, true),
			new Parameter("fileName", new SQLType(Types.VARCHAR), true, true, false),
            new Parameter("fileSource", new SQLType(Types.VARCHAR), false, true, false),
            new Parameter("fileType", new SQLType(Types.NUMERIC), true, true, false),
            new Parameter("fileContent", new SQLType(Types.BLOB), true, true, false),
			new Parameter("fileComment", new SQLType(Types.VARCHAR), false, true, false),
            new Parameter("modificationTime", new SQLType(Types.TIMESTAMP), true, true, false),
            new Parameter("fileName", new SQLType(Types.VARCHAR), true, true, false),
            new Parameter("fileSource", new SQLType(Types.VARCHAR), false, true, false),
            new Parameter("modificationTime", new SQLType(Types.TIMESTAMP), true, true, false)
		});
    	BasicDataSourceFactory bdsf = new BasicDataSourceFactory();
		String username = "SYSTEM"; // System.getProperty("dbUserName", System.getProperty("user.name").toUpperCase());
		String password = UIUtils.promptForPassword("Enter the OPER password for " + username, "Database Login", null);
		bdsf.addDataSource(
				"OPER",
				"oracle.jdbc.driver.OracleDriver",
				// "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.120)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.121)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.122)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.220)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.221)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.222)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)))",
				// "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb012.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=USADEV04.WORLD)))",
				"jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb011.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=usadev03.world)))",
				username, password);
		File file = new File(inputPath);
		Connection conn = bdsf.getDataSource("OPER").getConnection();
		try {
			Map<String, Object> params = new HashMap<>();
			InputStream in = new DeflaterInputStream(new FileInputStream(file));
			try {
				params.put("fileContent", in);
				params.put("modificationTime", file.lastModified());
				params.put("fileName", file.getName());
				params.put("fileSource", "206.253.184.37/./prod/data/253145");
				params.put("fileType", 2);
				call.executeCall(conn, params, null);
				conn.commit();
			} finally {
				in.close();
			}
		} finally {
			conn.close();
		}
	}

	@Test
	public void generateFinancialDFR() throws Exception {
		File dir = new File("C:\\Users\\bkrug\\Documents\\Chase Paymentech\\test");
		long DAY = 24 * 60 * 60 * 1000L;
		long curr = System.currentTimeMillis() - DAY * 2;
		long time = (curr / DAY) * DAY;

		Date fileDate = new Date(time);
		String fileName = ConvertUtils.formatObject(new Object[] { fileDate, curr % 1000 }, "MESSAGE:0000248258.{0,DATE,yyMMdd}.d.B{1,NUMBER,000}.dfr");
		String dateStr = ConvertUtils.formatObject(fileDate, "DATE:MM/dd/yyyy");
		String timeStr = ConvertUtils.formatObject(fileDate, "DATE:HH:mm:ss");
		
		HeapBufferStream bufferStream = new HeapBufferStream(false);
		Writer out = new BufferedWriter(new OutputStreamWriter(bufferStream.getOutputStream()));
		out.append("*DFRBEG|PID=253145|FREQ=DAILY|CO=248258\n");
		Call call = new Call();
		Cursor cursor = new Cursor();
		cursor.setAllColumns(true);
		call.setArguments(new Argument[] { 
			cursor,
			new Parameter("startTime", new SQLType(Types.TIMESTAMP), true, true, false),
            new Parameter("endTime", new SQLType(Types.TIMESTAMP), true, true, false),
			new Parameter("startTime", new SQLType(Types.TIMESTAMP), true, true, false),
            new Parameter("endTime", new SQLType(Types.TIMESTAMP), true, true, false)
		});
		
		Map<String, Object> params = new HashMap<>();
		params.put("startTime", time - DAY);
		params.put("endTime", time);
		
		BasicDataSourceFactory bdsf = new BasicDataSourceFactory();
		String username = "SYSTEM"; // System.getProperty("dbUserName", System.getProperty("user.name").toUpperCase());
		String password = UIUtils.promptForPassword("Enter the OPER password for " + username, "Database Login", null);// "usatmanager"; //
		bdsf.addDataSource(
				"OPER",
				"oracle.jdbc.driver.OracleDriver",
				"jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=eccscan01.usatech.com)(PORT=1525))(ADDRESS=(PROTOCOL=TCP)(HOST=eccscan02.usatech.com)(PORT=1525)))(CONNECT_DATA=(SERVICE_NAME=usaecc_db.world)))",
				//"jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb012.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=USADEV04.WORLD)))",
				//"jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=intdb011.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=usadev02.world)))",
				username, password);
		Results results;
		Connection conn = bdsf.getDataSource("OPER").getConnection();
		try {
			out.append("HACT0012|248258|").append(dateStr).append('|').append(dateStr).append('|').append(dateStr).append('|').append(timeStr).append('\n');
			call.setSql("SELECT 'RACT0012|TD|254242|USD|\"' || TO_CHAR(A.AUTH_AUTHORITY_TS, 'YMMDD') || '.'|| D.N || TO_CHAR(A.AUTH_AUTHORITY_TS, 'HH24') ||'Dh\"|253826|\"usatec\"|' || TO_CHAR(A.AUTH_AUTHORITY_TS, 'MM/DD/YYYY') || '|'  || TO_CHAR(MAX(A.AUTH_AUTHORITY_TS) + (1/24/60), 'HH24:MI:SS') || '|'|| COUNT(*) || '|0|0|0|0|0|0|'|| COUNT(*) || '|' || TO_CHAR(SUM(A.AUTH_AMT_APPROVED), 'FM9999990.00') ||'|Accepted'FROM PSS.TRAN X     JOIN (SELECT A.TRAN_ID, A.AUTH_AUTHORITY_TS, A.AUTH_AMT_APPROVED, A.AUTH_AUTHORITY_TRAN_CD, A.AUTH_AUTHORITY_MISC_DATA, 'D' ACTION_CODE, AA.AUTH_AUTHORITY_TS AUTHORIZATION_TS     FROM PSS.AUTH A      LEFT JOIN PSS.AUTH AA ON A.TRAN_ID = AA.TRAN_ID AND AA.AUTH_TYPE_CD = 'N'     WHERE A.AUTH_TYPE_CD = 'U'       AND A.AUTH_STATE_ID IN(2, 5)       AND A.AUTH_AUTHORITY_TS BETWEEN CAST(? AS DATE) AND CAST(? AS DATE)       UNION ALL SELECT R.TRAN_ID, R.REFUND_AUTHORITY_TS, -ABS(R.REFUND_AMT), R.REFUND_AUTHORITY_TRAN_CD, R.REFUND_AUTHORITY_MISC_DATA, 'R', NULL     FROM PSS.REFUND R      WHERE R.REFUND_STATE_ID IN(1)       AND R.REFUND_AUTHORITY_TS BETWEEN CAST(? AS DATE) AND CAST(? AS DATE)       ) A ON X.TRAN_ID = A.TRAN_ID       LEFT JOIN (PSS.TERMINAL T       JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID      ) ON X.PAYMENT_SUBTYPE_KEY_ID = T.TERMINAL_ID     LEFT JOIN (SELECT 'CR' CARD_TYPE,'CR' MOP, 'CQDB' INTERCHANGE, 0 FLAT, 0 PERC, 0 ASSESS FROM DUAL     UNION ALL SELECT 'VI', 'VI', 'VUSI', 0.2, 0.175, 0 FROM DUAL     UNION ALL SELECT 'MC', 'MC','MUSG', 0.15, 0.275, 0 FROM DUAL     UNION ALL SELECT 'AE', 'AX','NONE', 0, 0, 0 FROM DUAL     UNION ALL SELECT 'DS', 'DI','DCES', 0.25, 0.3, 0 FROM DUAL     ) B ON REGEXP_REPLACE(A.AUTH_AUTHORITY_MISC_DATA, '.*CardType=([A-Z][A-Z]).*', '\\1') = B.CARD_TYPE  JOIN (SELECT COLUMN_VALUE N FROM TABLE(NUMBER_TABLE(0,1))) D ON X.TRAN_DEVICE_TRAN_CD LIKE '%2' OR D.N = 0 WHERE X.PAYMENT_SUBTYPE_CLASS = 'Tandem' GROUP BY TO_CHAR(A.AUTH_AUTHORITY_TS, 'YMMDD'), TO_CHAR(A.AUTH_AUTHORITY_TS, 'HH24'), D.N, TO_CHAR(A.AUTH_AUTHORITY_TS, 'MM/DD/YYYY')");
			results = call.executeQuery(conn, params, null);
			while(results.next()) {
				for(int i = 1; i <= results.getColumnCount(); i++) {
					if(i > 1)
						out.append('|');
					out.append(results.getValue(i, String.class));
				}
				out.append('\n');
			}
			
			out.append("HFIN0011|248258|").append(dateStr).append('|').append(dateStr).append('|').append(dateStr).append('|').append(timeStr).append('\n');
			// load section from file
			Reader sectionIn = new InputStreamReader(ConfigSource.createConfigSource("dfr_test_RFIN001.txt").getInputStream());
			try {
				IOUtils.copy(sectionIn, out);
			} finally {
				sectionIn.close();
			}
			out.append("HACT0010|248258|").append(dateStr).append('|').append(dateStr).append('|').append(dateStr).append('|').append(timeStr).append('\n');

			call.setSql("SELECT 'RACT0010|' || TO_CHAR(A.AUTH_AUTHORITY_TS, 'MM/DD/YYYY') || '|253826|\"usatec\"|\"'          || TO_CHAR(A.AUTH_AUTHORITY_TS, 'YMMDD') || '.'|| D.N || TO_CHAR(A.AUTH_AUTHORITY_TS, 'HH24')         ||'Dh\"|' || ROWNUM || '|TD|254242|USD|\"'         || REPLACE(REGEXP_REPLACE(X.TRAN_GLOBAL_TRANS_CD, '^[^:]*:', ''), ':', '-') || '\"||' || LPAD(SUBSTR(X.TRAN_RECEIVED_RAW_ACCT_DATA, -4), LENGTH(X.TRAN_RECEIVED_RAW_ACCT_DATA),'X')         || '|' || TO_CHAR(A.AUTH_AUTHORITY_TS + 30 * MOD(NVL(TO_NUMBER_OR_NULL(SUBSTR(X.TRAN_RECEIVED_RAW_ACCT_DATA, -4)), 12), 36), 'MM/YY')         || '|' || TO_CHAR(A.AUTH_AMT_APPROVED, 'FM9999990.00')         || '|' || B.MOP         || '|' || A.ACTION_CODE         || '|' || TO_CHAR(A.AUTHORIZATION_TS, 'MM/DD/YYYY')         || '|' || A.AUTH_AUTHORITY_TRAN_CD         || '|100||||'|| M.MCC         || '||||||' || B.INTERCHANGE         || '|N|' || REGEXP_REPLACE(TO_CHAR(B.FLAT, 'FM99.99'), '[.]$', '')          || '|'|| REGEXP_REPLACE(TO_CHAR(B.PERC, 'FM99.9999'), '[.]$', '')           || '|' || REGEXP_REPLACE(TO_CHAR(B.FLAT + A.AUTH_AMT_APPROVED * B.PERC, 'FM9999999.99'), '[.]$', '')           || '|'|| REGEXP_REPLACE(TO_CHAR(B.ASSESS, 'FM99.99'), '[.]$', '')  || '||\"\"|0' 		FROM PSS.TRAN X     JOIN (SELECT A.TRAN_ID, A.AUTH_AUTHORITY_TS, A.AUTH_AMT_APPROVED, A.AUTH_AUTHORITY_TRAN_CD, A.AUTH_AUTHORITY_MISC_DATA, 'D' ACTION_CODE, AA.AUTH_AUTHORITY_TS AUTHORIZATION_TS     FROM PSS.AUTH A      LEFT JOIN PSS.AUTH AA ON A.TRAN_ID = AA.TRAN_ID AND AA.AUTH_TYPE_CD = 'N'     WHERE A.AUTH_TYPE_CD = 'U'       AND A.AUTH_STATE_ID IN(2, 5)       AND A.AUTH_AUTHORITY_TS BETWEEN CAST(? AS DATE) AND CAST(? AS DATE)       UNION ALL SELECT R.TRAN_ID, R.REFUND_AUTHORITY_TS, -ABS(R.REFUND_AMT), R.REFUND_AUTHORITY_TRAN_CD, R.REFUND_AUTHORITY_MISC_DATA, 'R', NULL     FROM PSS.REFUND R      WHERE R.REFUND_STATE_ID IN(1)       AND R.REFUND_AUTHORITY_TS BETWEEN CAST(? AS DATE) AND CAST(? AS DATE)       ) A ON X.TRAN_ID = A.TRAN_ID       LEFT JOIN (PSS.TERMINAL T       JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID      ) ON X.PAYMENT_SUBTYPE_KEY_ID = T.TERMINAL_ID     LEFT JOIN (SELECT 'CR' CARD_TYPE,'CR' MOP, 'CQDB' INTERCHANGE, 0 FLAT, 0 PERC, 0 ASSESS FROM DUAL     UNION ALL SELECT 'VI', 'VI', 'VUSI', 0.2, 0.175, 0 FROM DUAL     UNION ALL SELECT 'MC', 'MC','MUSG', 0.15, 0.275, 0 FROM DUAL     UNION ALL SELECT 'AE', 'AX','NONE', 0, 0, 0 FROM DUAL     UNION ALL SELECT 'DS', 'DI','DCES', 0.25, 0.3, 0 FROM DUAL     ) B ON REGEXP_REPLACE(A.AUTH_AUTHORITY_MISC_DATA, '.*CardType=([A-Z][A-Z]).*', '\\1') = B.CARD_TYPE  JOIN (SELECT COLUMN_VALUE N FROM TABLE(NUMBER_TABLE(0,1))) D ON X.TRAN_DEVICE_TRAN_CD LIKE '%2' OR D.N = 0 WHERE X.PAYMENT_SUBTYPE_CLASS = 'Tandem'");
			results = call.executeQuery(conn, params, null);
			while(results.next()) {
				for(int i = 1; i <= results.getColumnCount(); i++) {
					if(i > 1)
						out.append('|');
					out.append(results.getValue(i, String.class));
				}
				out.append('\n');
			}
			out.append("HACT0002|248258|").append(dateStr).append(dateStr).append(dateStr).append(timeStr).append('\n');
			call.setSql("SELECT 'RACT0002|' || TO_CHAR(A.AUTH_AUTHORITY_TS, 'MM/DD/YYYY') || '|253826|\"usatec\"|\"'          || TO_CHAR(A.AUTH_AUTHORITY_TS, 'YMMDD') || '.0' || TO_CHAR(A.AUTH_AUTHORITY_TS, 'HH24')         ||'Dh\"|' || ROWNUM || '|TD|254242|USD|\"'         || REPLACE(REGEXP_REPLACE(X.TRAN_GLOBAL_TRANS_CD, '^[^:]*:', ''), ':', '-') || '\"||' || LPAD(SUBSTR(X.TRAN_RECEIVED_RAW_ACCT_DATA, -4), LENGTH(X.TRAN_RECEIVED_RAW_ACCT_DATA),'X')         || '|' || TO_CHAR(A.AUTH_AUTHORITY_TS + 30 * MOD(NVL(TO_NUMBER_OR_NULL(SUBSTR(X.TRAN_RECEIVED_RAW_ACCT_DATA, -4)), 12), 36), 'MM/YY')         || '|' || TO_CHAR(A.AUTH_AMT_APPROVED, 'FM9999990.00')         || '|' || B.MOP         || '|' || A.ACTION_CODE         || '||||||REJECT|' || M.MCC || '|203|ACCEPT|' FROM PSS.TRAN X     JOIN (SELECT A.TRAN_ID, A.AUTH_AUTHORITY_TS, A.AUTH_AMT_APPROVED, A.AUTH_AUTHORITY_TRAN_CD, A.AUTH_AUTHORITY_MISC_DATA, 'D' ACTION_CODE, AA.AUTH_AUTHORITY_TS AUTHORIZATION_TS     FROM PSS.AUTH A      LEFT JOIN PSS.AUTH AA ON A.TRAN_ID = AA.TRAN_ID AND AA.AUTH_TYPE_CD = 'N'     WHERE A.AUTH_TYPE_CD = 'U'       AND A.AUTH_STATE_ID IN(2, 5)       AND A.AUTH_AUTHORITY_TS BETWEEN CAST(? AS DATE) AND CAST(? AS DATE)       UNION ALL SELECT R.TRAN_ID, R.REFUND_AUTHORITY_TS, R.REFUND_AMT, R.REFUND_AUTHORITY_TRAN_CD, R.REFUND_AUTHORITY_MISC_DATA, 'R', NULL     FROM PSS.REFUND R      WHERE R.REFUND_STATE_ID IN(1)       AND R.REFUND_AUTHORITY_TS BETWEEN CAST(? AS DATE) AND CAST(? AS DATE)       ) A ON X.TRAN_ID = A.TRAN_ID       LEFT JOIN (PSS.TERMINAL T       JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID      ) ON X.PAYMENT_SUBTYPE_KEY_ID = T.TERMINAL_ID     LEFT JOIN (SELECT 'CR' CARD_TYPE,'CR' MOP, 'CQDB' INTERCHANGE, 0 FLAT, 0 PERC, 0 ASSESS FROM DUAL     UNION ALL SELECT 'VI', 'VI', 'VUSI', 0.2, 0.175, 0 FROM DUAL     UNION ALL SELECT 'MC', 'MC','MUSG', 0.15, 0.275, 0 FROM DUAL     UNION ALL SELECT 'AE', 'AX','NONE', 0, 0, 0 FROM DUAL     UNION ALL SELECT 'DS', 'DI','DCES', 0.25, 0.3, 0 FROM DUAL     ) B ON REGEXP_REPLACE(A.AUTH_AUTHORITY_MISC_DATA, '.*CardType=([A-Z][A-Z]).*', '\\1') = B.CARD_TYPE   WHERE X.PAYMENT_SUBTYPE_CLASS = 'Tandem' AND X.TRAN_DEVICE_TRAN_CD LIKE '%0'");
			results = call.executeQuery(conn, params, null);
			while(results.next()) {
				for(int i = 1; i <= results.getColumnCount(); i++) {
					if(i > 1)
						out.append('|');
					out.append(results.getValue(i, String.class));
				}
				out.append('\n');
			}
			
			out.append("HACT0036|248258|").append(dateStr).append('|').append(dateStr).append('|').append(dateStr).append('|').append(timeStr).append('\n');
			call.setSql("SELECT 'RACT0036|TD|254242|USD|' || TO_CHAR(A.AUTH_AUTHORITY_TS, 'MM/DD/YYYY') || '|253826|\"usatec\"|\"'          || TO_CHAR(A.AUTH_AUTHORITY_TS, 'YMMDD') || '.0' || TO_CHAR(A.AUTH_AUTHORITY_TS, 'HH24')         ||'Dh\"|' || ROWNUM || '|998692|\"'         || REPLACE(REGEXP_REPLACE(X.TRAN_GLOBAL_TRANS_CD, '^[^:]*:', ''), ':', '-') || '\"||' || LPAD(SUBSTR(X.TRAN_RECEIVED_RAW_ACCT_DATA, -4), LENGTH(X.TRAN_RECEIVED_RAW_ACCT_DATA),'X')         || '|' || TO_CHAR(A.AUTH_AUTHORITY_TS + 30 * MOD(NVL(TO_NUMBER_OR_NULL(SUBSTR(X.TRAN_RECEIVED_RAW_ACCT_DATA, -4)), 12), 36), 'MM/YY')         || '|' || TO_CHAR(A.AUTH_AMT_APPROVED, 'FM9999990.00')         || '|USD|' || B.MOP         || '|' || A.ACTION_CODE || '|R|' || TO_CHAR(A.AUTH_AUTHORITY_TS, 'FMMM/DD/YYYY HH:MI:SS AM') || '|' || A.AUTH_AUTHORITY_TRAN_CD || '|100|00|Merchant|B|5|N|PNS||||||' || M.MCC || '||||||||||||||||||0' FROM PSS.TRAN X     JOIN (SELECT A.TRAN_ID, A.AUTH_AUTHORITY_TS, A.AUTH_AMT_APPROVED, A.AUTH_AUTHORITY_TRAN_CD, A.AUTH_AUTHORITY_MISC_DATA, CASE WHEN A.AUTH_TYPE_CD IN('N') THEN 'A' ELSE 'D' END ACTION_CODE     FROM PSS.AUTH A    WHERE A.AUTH_STATE_ID IN(2, 5)       AND A.AUTH_AUTHORITY_TS BETWEEN CAST(? AS DATE) AND CAST(? AS DATE)       UNION ALL SELECT R.TRAN_ID, R.REFUND_AUTHORITY_TS, R.REFUND_AMT, R.REFUND_AUTHORITY_TRAN_CD, R.REFUND_AUTHORITY_MISC_DATA, 'R'  FROM PSS.REFUND R      WHERE R.REFUND_STATE_ID IN(1)       AND R.REFUND_AUTHORITY_TS BETWEEN CAST(? AS DATE) AND CAST(? AS DATE)       ) A ON X.TRAN_ID = A.TRAN_ID       LEFT JOIN (PSS.TERMINAL T       JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID      ) ON X.PAYMENT_SUBTYPE_KEY_ID = T.TERMINAL_ID     LEFT JOIN (SELECT 'CR' CARD_TYPE,'CR' MOP, 'CQDB' INTERCHANGE, 0 FLAT, 0 PERC, 0 ASSESS FROM DUAL     UNION ALL SELECT 'VI', 'VI', 'VUSI', 0.2, 0.175, 0 FROM DUAL     UNION ALL SELECT 'MC', 'MC','MUSG', 0.15, 0.275, 0 FROM DUAL     UNION ALL SELECT 'AE', 'AX','NONE', 0, 0, 0 FROM DUAL     UNION ALL SELECT 'DS', 'DI','DCES', 0.25, 0.3, 0 FROM DUAL     ) B ON REGEXP_REPLACE(A.AUTH_AUTHORITY_MISC_DATA, '.*CardType=([A-Z][A-Z]).*', '\\1') = B.CARD_TYPE   WHERE X.PAYMENT_SUBTYPE_CLASS = 'Tandem'");
			results = call.executeQuery(conn, params, null);
			while(results.next()) {
				for(int i = 1; i <= results.getColumnCount(); i++) {
					if(i > 1)
						out.append('|');
					out.append(results.getValue(i, String.class));
				}
				out.append('\n');
			}

			out.append("HANS0013|248258|").append(dateStr).append('|').append(dateStr).append('|').append(dateStr).append('|').append(timeStr).append('\n');
			call.setSql("SELECT 'RANS0013|TD|254242|253826|USD|' || B.MOP || '|' || B.INTERCHANGE || '|630|No PS2000 Data|' || LPAD(SUBSTR(X.TRAN_RECEIVED_RAW_ACCT_DATA, -4), LENGTH(X.TRAN_RECEIVED_RAW_ACCT_DATA),'X') || '|' || REPLACE(REGEXP_REPLACE(X.TRAN_GLOBAL_TRANS_CD, '^[^:]*:', ''), ':', '-') || '|' || TO_CHAR(A.AUTH_AUTHORITY_TS, 'MM/DD/YYYY') || '|' || TO_CHAR(A.AUTH_AMT_APPROVED, 'FM9999990.00') FROM PSS.TRAN X     JOIN (SELECT A.TRAN_ID, A.AUTH_AUTHORITY_TS, A.AUTH_AMT_APPROVED, A.AUTH_AUTHORITY_TRAN_CD, A.AUTH_AUTHORITY_MISC_DATA, 'D' ACTION_CODE, AA.AUTH_AUTHORITY_TS AUTHORIZATION_TS     FROM PSS.AUTH A      LEFT JOIN PSS.AUTH AA ON A.TRAN_ID = AA.TRAN_ID AND AA.AUTH_TYPE_CD = 'N'     WHERE A.AUTH_TYPE_CD = 'U'       AND A.AUTH_STATE_ID IN(2, 5)       AND A.AUTH_AUTHORITY_TS BETWEEN CAST(? AS DATE) AND CAST(? AS DATE)       UNION ALL SELECT R.TRAN_ID, R.REFUND_AUTHORITY_TS, R.REFUND_AMT, R.REFUND_AUTHORITY_TRAN_CD, R.REFUND_AUTHORITY_MISC_DATA, 'R', NULL     FROM PSS.REFUND R      WHERE R.REFUND_STATE_ID IN(1)       AND R.REFUND_AUTHORITY_TS BETWEEN CAST(? AS DATE) AND CAST(? AS DATE)       ) A ON X.TRAN_ID = A.TRAN_ID       LEFT JOIN (PSS.TERMINAL T       JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID      ) ON X.PAYMENT_SUBTYPE_KEY_ID = T.TERMINAL_ID     LEFT JOIN (SELECT 'CR' CARD_TYPE,'CR' MOP, 'CQDB' INTERCHANGE, 0 FLAT, 0 PERC, 0 ASSESS FROM DUAL     UNION ALL SELECT 'VI', 'VI', 'VUSI', 0.2, 0.175, 0 FROM DUAL     UNION ALL SELECT 'MC', 'MC','MUSG', 0.15, 0.275, 0 FROM DUAL     UNION ALL SELECT 'AE', 'AX','NONE', 0, 0, 0 FROM DUAL     UNION ALL SELECT 'DS', 'DI','DCES', 0.25, 0.3, 0 FROM DUAL     ) B ON REGEXP_REPLACE(A.AUTH_AUTHORITY_MISC_DATA, '.*CardType=([A-Z][A-Z]).*', '\\1') = B.CARD_TYPE   WHERE X.PAYMENT_SUBTYPE_CLASS = 'Tandem' AND X.TRAN_DEVICE_TRAN_CD LIKE '%1'");
			results = call.executeQuery(conn, params, null);
			while(results.next()) {
				for(int i = 1; i <= results.getColumnCount(); i++) {
					if(i > 1)
						out.append('|');
					out.append(results.getValue(i, String.class));
				}
				out.append('\n');
			}
			out.append("*DFREND|PID=253145|FREQ=DAILY|CO=248258\n");
			
			out.flush();
			InputStream in = bufferStream.getInputStream();
			try {
				if(dir == null) {
					call.setSql("INSERT INTO REPORT.FILE_CACHE(FILE_NAME, FILE_SOURCE, FILE_CACHE_TYPE_ID, FILE_CONTENT, FILE_COMMENT, FILE_MODIFIED_TS, FILE_CACHE_STATE_ID) SELECT ?, ?, ?, ?, ?, ?, 1 FROM DUAL WHERE NOT EXISTS( SELECT 1 FROM REPORT.FILE_CACHE WHERE FILE_NAME = ? AND NVL(FILE_SOURCE, '-') = NVL(?, '-') AND FILE_MODIFIED_TS >= ?)");
					call.setArguments(new Argument[] { 
						new Parameter(null, null, true, false, true), new Parameter("fileName", new SQLType(Types.VARCHAR), true, true, false), 
						new Parameter("fileSource", new SQLType(Types.VARCHAR), false, true, false), 
						new Parameter("fileType", new SQLType(Types.NUMERIC), true, true, false), 
						new Parameter("fileContent", new SQLType(Types.BLOB), true, true, false),
						new Parameter("fileComment", new SQLType(Types.VARCHAR), false, true, false), 
						new Parameter("modificationTime", new SQLType(Types.TIMESTAMP), true, true, false), 
						new Parameter("fileName", new SQLType(Types.VARCHAR), true, true, false), 
						new Parameter("fileSource", new SQLType(Types.VARCHAR), false, true, false),
						new Parameter("modificationTime", new SQLType(Types.TIMESTAMP), true, true, false) });
					params.clear();
					params.put("fileContent", in);
					params.put("modificationTime", curr);
					params.put("fileName", fileName);
					params.put("fileSource", InetAddress.getLocalHost().getCanonicalHostName());
					params.put("fileType", 1);
					call.executeCall(conn, params, null);
					conn.commit();
				} else {
					OutputStream fileOut = new FileOutputStream(new File(dir, fileName));
					try {
						IOUtils.copy(in, fileOut);
					} finally {
						fileOut.close();
					}
				}
			} finally {
				in.close();
			}
		} finally {
			conn.close();
		}
	}

	@Test
	public void testProcessFinanicialFiles() throws JobExecutionException, ConfigException, IOException, LoadingException {
		BasicDataSourceFactory bdsf = new BasicDataSourceFactory();
		String username = "SYSTEM"; // System.getProperty("dbUserName", System.getProperty("user.name").toUpperCase());
		String password = UIUtils.promptForPassword("Enter the OPER password for " + username, "Database Login", null);
		bdsf.addDataSource("OPER", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb012.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=USADEV04.WORLD)))", username, password);
		DataLayerMgr.setDataSourceFactory(bdsf);
		ConfigLoader.loadConfig("posm-data-layer.xml");
		new ProcessFinancialDFRJob().executePostConfigure(null);
	}

	@Test
	public void compressDFRFiles() throws DataLayerException, SQLException, ConvertException, IOException {
		BasicDataSourceFactory bdsf = new BasicDataSourceFactory();
		String username = "SYSTEM"; // System.getProperty("dbUserName", System.getProperty("user.name").toUpperCase()); //
		String password = UIUtils.promptForPassword("Enter the OPER password for " + username, "Database Login", null);
		// bdsf.addDataSource("OPER", "oracle.jdbc.driver.OracleDriver",
		// "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb012.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=USADEV04.WORLD)))", username,
		// password);
		bdsf.addDataSource("OPER", "oracle.jdbc.driver.OracleDriver",
				// "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.120)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.121)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.122)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.220)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.221)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.222)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)))",
				"jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb012.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=USADEV04.WORLD)))", 
				username, password);
		DataLayerMgr.setDataSourceFactory(bdsf);
		Call call = new Call();
		Cursor cursor = new Cursor();
		cursor.setLazyAccess(true);
		cursor.setAllColumns(true);
		call.setSql("SELECT FILE_CACHE_ID, FILE_CONTENT FROM REPORT.FILE_CACHE WHERE FILE_CACHE_TYPE_ID = 1 ORDER BY FILE_CACHE_ID");
		call.setArguments(new Argument[] { 
			cursor
		});
		// call.setMaxRows(20);
		try (Connection conn = bdsf.getDataSource("OPER").getConnection()) {
			try (Results results = call.executeQuery(conn, null, null)) {
				Map<String, Object> params = new HashMap<>();
				call.setSql("UPDATE REPORT.FILE_CACHE SET FILE_CONTENT = ?, FILE_CACHE_TYPE_ID = 2 WHERE FILE_CACHE_TYPE_ID = 1 AND FILE_CACHE_ID = ?");
				call.setArguments(new Argument[] { 
						new Parameter(null, null, true, false, true), 
						new Parameter("fileContent", new SQLType(Types.BLOB), true, true, false),
						new Parameter("fileCacheId", new SQLType(Types.NUMERIC), true, true, false), 
				});
				while(results.next()) {
					long fileCacheId = results.getValue(1, Long.class);
					InputStream fileContent = results.getValue(2, InputStream.class);
					try (InputStream compressed = new DeflaterInputStream(fileContent)) {
						params.clear();
						params.put("fileContent", compressed);
						params.put("fileCacheId", fileCacheId);
						call.executeCall(conn, params, null);
					}
					conn.commit();
					log.info("Compressed file content of FILE_CACHE_ID=" + fileCacheId);
				}
			}
		}
	}

	@Test
	public void readCompressed() throws IOException {	
		File file = new File("D:\\Development\\Paymentech\\Tandem\\Reports\\Work\\0000248258.161003.d.B309.dfr.z");	
		File result = new File(file.getParentFile(), file.getName().replaceFirst("\\.z$", ""));
		long bytes = IOUtils.readInto(new InflaterInputStream(new FileInputStream(file)), new FileOutputStream(result), 1024);
		log.info("Processed " + bytes + " bytes");
	}

	@Test
	public void extractDFRFilesToEveryoneDrive() throws DataLayerException, SQLException, ConvertException, IOException {
		String dir = "H:\\4 Kristin";
		BasicDataSourceFactory bdsf = new BasicDataSourceFactory();
		// String username = "SYSTEM";
		String username = System.getProperty("dbUserName", System.getProperty("user.name").toUpperCase());
		String password = UIUtils.promptForPassword("Enter the OPER password for " + username, "Database Login", null);
		String url = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.61)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.62)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.120)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.121)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.122)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.220)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.221)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.222)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)))";
		// String url = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb012.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=USADEV04.WORLD)))";
		bdsf.addDataSource("OPER", "oracle.jdbc.driver.OracleDriver", url, username, password);
		DataLayerMgr.setDataSourceFactory(bdsf);
		Call call = new Call();
		Cursor cursor = new Cursor();
		cursor.setLazyAccess(true);
		cursor.setAllColumns(true);
		call.setSql("SELECT FC.FILE_CACHE_ID, FC.FILE_NAME, FC.FILE_CONTENT, FC.FILE_CACHE_TYPE_ID FROM REPORT.FILE_CACHE FC JOIN TABLE(?) D ON FC.FILE_NAME LIKE '0000248258.' || TO_CHAR(D.COLUMN_VALUE, 'YYMMDD') || '.d.B___.dfr' WHERE FC.FILE_CACHE_TYPE_ID IN(1, 2) ORDER BY FC.FILE_CACHE_ID");
		Parameter p = new Parameter("dates", new ArraySQLType(new SQLType(Types.DATE)), true, true, false);
		call.setArguments(new Argument[] { cursor, p });
		Map<String, Object> params = new HashMap<>();
		Date[] dates = new Date[2];
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.DATE, 1);
		dates[0] = cal.getTime();
		cal.set(Calendar.DATE, 2);
		dates[1] = cal.getTime();
		params.put("dates", dates);
		// call.setMaxRows(20);
		try (Connection conn = bdsf.getDataSource("OPER").getConnection()) {
			try (Results results = call.executeQuery(conn, params, null)) {
				while(results.next()) {
					long fileCacheId = results.getValue(1, Long.class);
					String fileName = results.getValue(2, String.class);
					int fileCacheTypeId = results.getValue(4, Integer.class);
					long length;
					File file = new File(dir, fileName);
					try (OutputStream out = new FileOutputStream(file)) {
						InputStream fileContent = results.getValue(3, InputStream.class);
						InputStream in;
						if(fileCacheTypeId == 2)
							in = new InflaterInputStream(fileContent);
						else
							in = fileContent;
						try {
							length = IOUtils.copy(in, out);
						} finally {
							in.close();
						}
					}
					conn.commit();
					log.info("Wrote " + length + " bytes to " + file + " from FILE_CACHE_ID=" + fileCacheId);
				}
			}
		}
	}

}
