package com.usatech.posm.test;

import static org.mockito.Mockito.*;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.quartz.JobExecutionContext;

import simple.app.BaseWithConfig;
import simple.app.MainWithConfig;
import simple.app.Publisher;
import simple.io.ByteInput;
import simple.io.Log;
import simple.test.UnitTest;

import com.usatech.posm.schedule.PayrollDeductTopUpCheck;

public class TopUpCheckTest extends UnitTest {

	@Before
	public void setUp() throws Exception {
		setupLog();
		setupDSF("POSMLayerService.properties");
	}

	@Test
	public void testTopUp() throws Exception {
		try {
			JobExecutionContext mockJobExecutionContext = mock(JobExecutionContext.class, withSettings().verboseLogging());
			Publisher<ByteInput> mockPublisher = mock(Publisher.class, withSettings().verboseLogging());
			
			PayrollDeductTopUpCheck check = new PayrollDeductTopUpCheck();
			check.setPublisher(mockPublisher);
			
			check.executePostConfigure(mockJobExecutionContext);

		} catch (Throwable e) {
			e.printStackTrace();
			throw e;
		}
	}
}
