package com.usatech.posm.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.zip.InflaterInputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.quartz.JobExecutionException;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.db.BasicDataSourceFactory;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DbUtils;
import simple.db.config.ConfigException;
import simple.db.config.ConfigLoader;
import simple.io.LoadingException;
import simple.io.Log;
import simple.results.Results;
import simple.swt.UIUtils;
import simple.test.UnitTest;

public class DFRFinancialParserTest extends UnitTest {
	
	public static long totalUpdateLineCalled=0;
	public static long totalUpdateLineCalledTimeSpent=0;
	
	@Before
	public void prepareLog() {
		System.setProperty("log4j.configuration", "log4j-dev.properties");
		log = Log.getLog();
	}

	@After
	public void finishLog() {
		Log.finish();
	}
	
	public static void copyInputStreamToFile( InputStream in, File file ) {
    try {
        OutputStream out = new FileOutputStream(file);
        byte[] buf = new byte[1024];
        int len;
        while((len=in.read(buf))>0){
            out.write(buf,0,len);
        }
        out.close();
        in.close();
    } catch (Exception e) {
        e.printStackTrace();
    }
}
	
	@Test
	public void extractFromProdDFRByFileCacheIdToLocalDrive() throws DataLayerException, SQLException, ConvertException, IOException {
		String dir = "H:\\4Ying\\DFR";
		BasicDataSourceFactory bdsf = new BasicDataSourceFactory();
		String username = System.getProperty("dbUserName", System.getProperty("user.name").toUpperCase());
		//String username = "yhe";
		String password = UIUtils.promptForPassword("Enter the OPER password for " + username, "Database Login", null);
		int fileCacheIdToExtract = 5994;
		String url = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.61)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.62)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.120)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.121)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.122)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.220)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.221)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.222)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)))";
		//String url = "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb012.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=USADEV04.WORLD)))";
		bdsf.addDataSource("OPER", "oracle.jdbc.driver.OracleDriver", url, username, password);
		DataLayerMgr.setDataSourceFactory(bdsf);
		String sqlToExecute="SELECT FC.FILE_CACHE_ID, FC.FILE_NAME, FC.FILE_CONTENT, FC.FILE_CACHE_TYPE_ID FROM REPORT.FILE_CACHE FC WHERE FILE_CACHE_ID ="+fileCacheIdToExtract;
		try (Connection conn = bdsf.getDataSource("OPER").getConnection()) {
			try (Results results = DataLayerMgr.executeSQL(conn, sqlToExecute, null,null,null)) {
				while(results.next()) {
					long fileCacheId = results.getValue(1, Long.class);
					String fileName = results.getValue(2, String.class);
					int fileCacheTypeId = results.getValue(4, Integer.class);
					long length;
					File file = new File(dir, fileName);
					try (OutputStream out = new FileOutputStream(file)) {
						InputStream fileContent = results.getValue(3, InputStream.class);
						InputStream in;
						if(fileCacheTypeId == 2)
							in = new InflaterInputStream(fileContent);
						else
							in = fileContent;
						try {
							copyInputStreamToFile(in, file);
							length=1;
						} finally {
							out.flush();
							in.close();
							out.close();
						}
					}
					conn.commit();
					log.info("Wrote " + length + " bytes to " + file + " from FILE_CACHE_ID=" + fileCacheId);
				}
			}
		}
	}

	@Test
	public void testProcessFinanicialFiles() throws JobExecutionException, ConfigException, IOException, LoadingException, DataLayerException,SQLException, ServiceException{
		BasicDataSourceFactory bdsf = new BasicDataSourceFactory();
		String username = "SYSTEM"; // System.getProperty("dbUserName", System.getProperty("user.name").toUpperCase());
		String password = UIUtils.promptForPassword("Enter the OPER password for " + username, "Database Login", null);
		bdsf.addDataSource("OPER", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb012.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=USADEV04.WORLD)))", username, password);
		DataLayerMgr.setDataSourceFactory(bdsf);
		ConfigLoader.loadConfig("posm-test-data-layer.xml");
		Connection conn = DataLayerMgr.getConnection("OPER");
		InputStream in = new FileInputStream("H:\\4Ying\\DFR\\0000248258.160706.d.B355.dfr");
		ParseFinancialDFRTaskMock parserMock=new ParseFinancialDFRTaskMock();
		parserMock.setTmpFileUsed(true);
		long fileCacheId=143;
		String fileName="0000248258.160706.d.B355.dfr";
		long lastProcessedLine=0;
		boolean okay=false;
	try{
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(in))) {
			long rows = parserMock.parseFileContent(conn, fileCacheId, fileName, lastProcessedLine, reader);
			log.info("-------------------rows:"+rows);
			okay = true;
			conn.commit();
		}
	} finally {
		try {
			in.close();
		} catch(IOException e) {
			// ignore
		}
		if(!okay)
			DbUtils.rollbackSafely(conn);
		conn.close();
	}
	}


}
