package com.usatech.posm.test;

import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.quartz.CronExpression;
import org.quartz.CronTrigger;
import org.quartz.JobExecutionContext;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.posm.schedule.FanfUploadJob;
import com.usatech.posm.schedule.SubmerchantsUploadJob;

import simple.app.*;
import simple.io.ByteInput;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;

public class FanfUploadJobTest {
	private static final Log log = Log.getLog();
	
	@Test
	public void testCron() throws Exception {
		CronExpression cronExpression = new CronExpression("0 27 11 L-3W * ?");
		Date fromDate = new SimpleDateFormat("MM/dd/yyyy").parse("07/23/2016");
		System.out.println(cronExpression.getNextValidTimeAfter(fromDate));
	}
	
	@Test
	public void testFileGeneration() throws Exception{
		try {
			Properties props = MainWithConfig.loadPropertiesWithConfig("POSMLayerService.properties", SubmerchantsUploadJob.class, null);
			Base.configureDataSourceFactory(props, null);
			Base.configureDataLayer(props);
			
			ResourceFolder resourceFolder = BaseWithConfig.configureFromBase(ResourceFolder.class, "simple.io.resource.ResourceFolder", props, null, true, null);
	
			Publisher<ByteInput> mockPublisher = mock(Publisher.class, withSettings().verboseLogging());
			JobExecutionContext mockJobExecutionContext = mock(JobExecutionContext.class, withSettings().verboseLogging());
			
			FanfUploadJob job = new FanfUploadJob();
			job.setPublisher(mockPublisher);
			job.setResourceFolder(resourceFolder);
			job.setCompanyId(251341);
			job.setSftpUploadQueueKey("usat.file.upload.sftp.inside");
			job.setResourceDeleteQueueKey("usat.file.resource.delete");
			job.setHost("devapr11.usatech.com");
			job.setPort(22);
			job.setUsername("pcowan");
			job.setPassword("test");
			job.setDirectory("/home/pcowan");
			job.setSid(1);
			job.setSidPassword("asdf");
			job.setPid(2);
			job.setPidPassword("asdf");
			job.setDefaultTdId(254242);
			
			job.executePostConfigure(mockJobExecutionContext);
			
			String queueKey = "usat.file.upload.sftp.inside";
			
			ArgumentCaptor<ByteInput> captor = ArgumentCaptor.forClass(ByteInput.class);
			verify(mockPublisher).publish(eq(queueKey), eq(false), eq(false), captor.capture(), any(), any(), any());
			
			ByteInput byteInput = captor.getValue();
			MessageChain messageChain = MessageChainService.deserializeMessageChain_v11(byteInput, queueKey, false);
			MessageChainStep step = messageChain.getStep(1);

			String host = step.getAttribute(CommonAttrEnum.ATTR_HOST, String.class, true);
			int port = step.getAttribute(CommonAttrEnum.ATTR_PORT, Integer.class, true);
			String username = step.getAttribute(CommonAttrEnum.ATTR_USERNAME, String.class, true);
			String password = step.getAttribute(CommonAttrEnum.ATTR_PASSWORD, String.class, true);

			assertEquals("devapr11.usatech.com", host);
			assertEquals(22, port);
			assertEquals("pcowan", username);
			assertEquals("test", password);

			String filePath = step.getAttribute(CommonAttrEnum.ATTR_FILE_PATH, String.class, true);
			String fileRenameReplace = step.getAttribute(CommonAttrEnum.ATTR_FILE_RENAME_REPLACE, String.class, false);
			
			assertNotNull(filePath);
			assertTrue(filePath.startsWith("/home/pcowan/t"));
			
			assertNotNull(fileRenameReplace);
			assertTrue(fileRenameReplace.startsWith("/home/pcowan/p"));

			String resourceKey = step.getAttribute(CommonAttrEnum.ATTR_RESOURCE, String.class, true);

			assertNotNull(resourceKey);

			Resource resource = resourceFolder.getResource(resourceKey, ResourceMode.READ);
			assertNotNull(resource);
			
			InputStream in = resource.getInputStream();
			byte[] buffer = new byte[2048];
			int r;
			while((r = in.read(buffer)) >= 0) {
				System.out.println(new String(buffer, 0, r));
			}
			in.close();
			resource.release();
			resource.delete();
		} catch (Throwable e) {
			e.printStackTrace();
			throw e;
		}
	}

}
