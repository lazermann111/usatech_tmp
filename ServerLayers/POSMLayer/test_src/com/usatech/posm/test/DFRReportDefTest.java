package com.usatech.posm.test;

import java.util.List;
import java.util.Properties;

import org.junit.BeforeClass;
import org.junit.Test;

import simple.app.Base;
import simple.app.MainWithConfig;
import simple.io.Log;

import com.usatech.posm.schedule.SubmerchantsUploadJob;
import com.usatech.posm.schedule.dfr.DFRReportDef;
import com.usatech.posm.schedule.dfr.DFRReportDef.DFRReportFieldDef;

public class DFRReportDefTest {
	private static final Log log = Log.getLog();

	@BeforeClass
	public static void init() throws Exception {
		Properties props = MainWithConfig.loadPropertiesWithConfig("POSMLayerService.properties", SubmerchantsUploadJob.class, null);
		Base.configureDataSourceFactory(props, null);
		Base.configureDataLayer(props);
	}
	
	@Test
	public void testLoad() throws Exception {
		try {
			List<DFRReportDef> reportDefs = DFRReportDef.loadAll();
			for(DFRReportDef rd : reportDefs) {
				System.out.println(rd.getReportName());
				for(DFRReportFieldDef fd : rd.getFields()) {
					System.out.println(" " + fd.getFieldName());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
