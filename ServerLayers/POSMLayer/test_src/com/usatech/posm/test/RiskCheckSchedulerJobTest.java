package com.usatech.posm.test;

import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import org.apache.commons.configuration.Configuration;
import org.junit.BeforeClass;
import org.junit.Test;
import org.quartz.JobExecutionContext;

import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.constants.MessageAttrEnum;
import com.usatech.posm.schedule.RiskCheckSchedulerJob;
import com.usatech.posm.schedule.SubmerchantsUploadJob;
import com.usatech.posm.tasks.RiskCheckTask;

import simple.app.*;
import simple.io.ByteInput;
import simple.io.Log;
import simple.io.resource.ResourceFolder;
import simple.util.LRUMap;

public class RiskCheckSchedulerJobTest {
	
	@Test
	public void testRiskCheck() throws Exception{
		try {
			Configuration config = MainWithConfig.loadConfig("POSMLayerService.properties", RiskCheckTask.class, null);
			BaseWithConfig.configureDataSourceFactory(config, null);
			BaseWithConfig.configureDataLayer(config);
//    BaseWithConfig.configureStatics(config, null);
//    MainWithConfig.configureSystem(null, null);
			
			Publisher<ByteInput> mockPublisher = mock(Publisher.class, withSettings().verboseLogging());
			JobExecutionContext mockJobExecutionContext = mock(JobExecutionContext.class, withSettings().verboseLogging());

			RiskCheckSchedulerJob job = new RiskCheckSchedulerJob();
			job.setPublisher(mockPublisher);
			job.setCheckDays(1);
			
			job.executePostConfigure(mockJobExecutionContext);
			
			verify(mockPublisher).publish(eq("usat.posm.risk.check"), any(ByteInput.class));
		} catch (Throwable e) {
			e.printStackTrace();
			throw e;
		}
	}
}
