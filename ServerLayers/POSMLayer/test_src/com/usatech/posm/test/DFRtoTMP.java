package com.usatech.posm.test;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.Types;
import java.util.Collections;
import java.util.Date;

import simple.bean.ConvertUtils;
import simple.db.Argument;
import simple.db.BasicDataSourceFactory;
import simple.db.Call;
import simple.db.Call.BatchUpdate;
import simple.db.Cursor;
import simple.db.Parameter;
import simple.results.DatasetHandlerSelectorFacade;
import simple.results.Results;
import simple.sql.SQLType;
import simple.swt.UIUtils;
import simple.text.MultiRecordDelimitedParser;

import com.usatech.app.BatchUpdateDatasetHandler;
import com.usatech.posm.schedule.dfr.DFRSetup;

public class DFRtoTMP {
	protected final MultiRecordDelimitedParser<String> parser;

	public static void main(String[] args) throws Exception {
		processDatabase(ConvertUtils.convert(Date.class, "08/13/2015 09:00"));
	}

	protected static void processDatabase(Date start) throws Exception {
		Call call = new Call();
		call.setSql("SELECT FILE_CACHE_ID, FILE_CONTENT FROM REPORT.FILE_CACHE WHERE FILE_NAME LIKE '%.d.B___.dfr' AND FILE_MODIFIED_TS >= ? ORDER BY FILE_MODIFIED_TS");
		Cursor cursor = new Cursor();
		cursor.setLazyAccess(true);
		cursor.setAllColumns(true);
		call.setArguments(new Argument[] { cursor, new Parameter("startDate", new SQLType(Types.TIMESTAMP), true, true, false) });

		Call updateCall = new Call();
		updateCall.setSql("INSERT INTO BKRUG.TMP_DFR_COMPLETION(FILE_CACHE_ID, SUBMISSION_NUM, RECORD_NUM, MERCHANT_ORDER_NUM, AUTH_CD, SUBMISSION_DATE, AUTH_DATE) VALUES(?,?,?,?,?,?,?)");
		updateCall.setArguments(new Argument[] { 
				new Parameter(null, null, true, false, true),
				new Parameter("fileCacheId", new SQLType(Types.NUMERIC), true, true, false),
	            new Parameter("submission", new SQLType(Types.VARCHAR), true, true, false),
	            new Parameter("record", new SQLType(Types.NUMERIC), true, true, false),
	            new Parameter("merchantOrder", new SQLType(Types.VARCHAR), true, true, false),
				new Parameter("authCd", new SQLType(Types.VARCHAR), false, true, false),
	            new Parameter("submissionDate", new SQLType(Types.DATE), true, true, false),
	            new Parameter("authDate", new SQLType(Types.DATE), false, true, false)
		});
		BatchUpdate batchUpdate = updateCall.createBatchUpdate(false, 0.0f);
		BasicDataSourceFactory bdsf = new BasicDataSourceFactory();
		String username = System.getProperty("dbUserName", System.getProperty("user.name").toUpperCase());
		String password = UIUtils.promptForPassword("Enter the OPER password for " + username, "Database Login", null);
		bdsf.addDataSource(
				"OPER",
				"oracle.jdbc.driver.OracleDriver",
				"jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.120)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.121)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.122)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.220)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.221)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.222)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)))",
				username, password);
		try {
			Connection conn = bdsf.getDataSource("OPER").getConnection();
			try {
				Results results = call.executeQuery(conn, new Object[] { start }, null);
				while(results.next()) {
					long fileCacheId = results.getValue(1, Long.class);
					InputStream content = results.getValue(2, InputStream.class);
					DFRtoTMP dfrtoXls = new DFRtoTMP("\n");
					dfrtoXls.process(new InputStreamReader(content), conn, batchUpdate, fileCacheId);
				}
				results.close();
			} finally {
				conn.close();
			}
		} finally {
			bdsf.close();
		}
	}
	public DFRtoTMP(String lineSeparator) {
		parser = DFRSetup.createFinancialDFRParser(false);
		parser.setLineSeparator(lineSeparator);
	}

	public void process(Reader reader, Connection conn, BatchUpdate batchUpdate, long fileCacheId) throws Exception {
		DatasetHandlerSelectorFacade<Exception> selectorHandler = new DatasetHandlerSelectorFacade<>(parser.getSelectorColumn().getLabel());
		selectorHandler.addHandler("RACT0010", new BatchUpdateDatasetHandler(conn, batchUpdate, 1000, Collections.singletonMap("fileCacheId", (Object) fileCacheId)));
		parser.parse(reader, selectorHandler);
		System.out.println("Processed File Cache " + fileCacheId);
	}

	protected static void logErrorAndExit(String message, int exitCode) {
		System.err.println(message);
		printUsage();
		System.exit(exitCode);
	}

	protected static void printUsage() {
		System.err.println("Arguments: <DFR Input File Path> (<Excel Output File Path>)");
	}

}
