package com.usatech.posm.test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.apache.commons.configuration.Configuration;
import org.junit.Test;

import simple.app.BaseWithConfig;
import simple.app.MainWithConfig;
import simple.app.ServiceManagerWithConfig;
import simple.io.Log;
import simple.text.MessageFormat;

import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.MessageAttrEnum;
import com.usatech.posm.tasks.RiskCheckTask;


public class RiskCheckTaskTest {
	private static final Log log = Log.getLog();
	
	@Test
	public void testFormat() throws Exception {
		String f = "Device {0} triggered risk check alarm with average score {1,number,#0.##}: {2}";
		System.out.println(MessageFormat.format(f, "foo", 123.45678, "bar"));
	}
	
	@Test
	public void testRiskCheck() throws Exception{
		Configuration config = MainWithConfig.loadConfig("POSMLayerService.properties", RiskCheckTask.class, null);
		BaseWithConfig.configureDataSourceFactory(config, null);
		BaseWithConfig.configureDataLayer(config);
//  BaseWithConfig.configureStatics(config, null);
//  MainWithConfig.configureSystem(null, null);
		
		try {
			MessageChainTaskInfo mockTaskInfo = mock(MessageChainTaskInfo.class, withSettings().verboseLogging());
			MessageChainStep mockStep = mock(MessageChainStep.class, withSettings().verboseLogging());
			
			when(mockTaskInfo.getStep()).thenReturn(mockStep);
			when(mockStep.getAttribute(MessageAttrEnum.ATTR_DEVICE_NAME, String.class, true)).thenReturn("WS000101");
			
			RiskCheckTask task = new RiskCheckTask();
			task.setMinAvgAlarmScore(0);
			task.setSampleDays(60);
			task.setTranCountMin(40);
			task.setTranAmountMin(50);
			task.setMagnitudeOfDeviationMin(3.5);
			task.setSamplePopulationPercentMin(0.5);
			task.setChargebackCountMin(1);
			task.setChargebackAmountMin(2);
			
			int result = task.process(mockTaskInfo);
			assertEquals(0, result);
			
			verify(mockStep, times(1)).getAttribute(MessageAttrEnum.ATTR_DEVICE_NAME, String.class, true);
			
		} catch (Throwable e) {
			e.printStackTrace();
			throw e;
		}
	}
//	
//    @Test
//    public void testMessaging() throws Exception {
//      Configuration config = MainWithConfig.loadConfig("POSMLayerService.properties", RiskCheckTask.class, null);
//      TestMainWithConfig main = new TestMainWithConfig();
//      ServiceManagerWithConfig serviceManager = main.startServiceManager(config);
//      
//      Thread.sleep(5);
//      
//      Publisher<ByteInput> publisher = (Publisher<ByteInput>) serviceManager.getNamedReference("simple.app.Publisher");
//      
//      InteractionUtils.doRiskChecks(publisher, "WS000152", log);
//      
//      Thread.sleep(9999999);
//    }
//
//	
//  @Test
//  public void testProperties() throws Exception {
//    LRUMap<String, Long> cache = InteractionUtils.getLastRiskCheckCache();
//    assertEquals(2000, cache.getMaxSize());
//  }

    static class TestMainWithConfig extends MainWithConfig {
      public ServiceManagerWithConfig startServiceManager(Configuration config) throws Exception {
        TestServiceManager sm = new TestServiceManager();
        sm.configureServiceManager(config);
        sm.start();
        return sm;
      }
    }
    
    static class TestServiceManager extends ServiceManagerWithConfig {
      public void start() throws Exception {
        super.startup();
      }
    }
}
