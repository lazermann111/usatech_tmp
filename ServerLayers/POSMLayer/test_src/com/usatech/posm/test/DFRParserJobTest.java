package com.usatech.posm.test;

import static org.mockito.Mockito.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;
import java.util.Properties;

import org.junit.Test;
import org.quartz.JobExecutionContext;

import simple.app.Base;
import simple.app.MainWithConfig;
import simple.app.Publisher;
import simple.io.ByteInput;
import simple.io.Log;
import simple.io.ReaderInputStream;

import com.usatech.posm.schedule.SubmerchantsUploadJob;
import com.usatech.posm.schedule.dfr.DFRParser;
import com.usatech.posm.schedule.dfr.DFRParserJob;

public class DFRParserJobTest {
	private static final Log log = Log.getLog();
	
//	@Test
	public void testParserJob() throws Exception{
		try {
			Properties props = MainWithConfig.loadPropertiesWithConfig("POSMLayerService.properties", SubmerchantsUploadJob.class, null);
			Base.configureDataSourceFactory(props, null);
			Base.configureDataLayer(props);
			
			Publisher<ByteInput> mockPublisher = mock(Publisher.class, withSettings().verboseLogging());
			JobExecutionContext mockJobExecutionContext = mock(JobExecutionContext.class, withSettings().verboseLogging());
			
			DFRParserJob job = new DFRParserJob();
			job.executePostConfigure(mockJobExecutionContext);
		} catch (Throwable e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test
	public void testParser() throws Exception{
		try (Reader in = new BufferedReader(new FileReader("c:\\tmp\\0000248258.150821.d.B355.dfr"), 65535)) {
			Properties props = MainWithConfig.loadPropertiesWithConfig("POSMLayerService.properties", SubmerchantsUploadJob.class, null);
			Base.configureDataSourceFactory(props, null);
			Base.configureDataLayer(props);
			DFRParser parser = new DFRParser(86, 1000);
			parser.parse(in);
		} catch (Throwable e) {
			e.printStackTrace();
			throw e;
		}
	}


}
