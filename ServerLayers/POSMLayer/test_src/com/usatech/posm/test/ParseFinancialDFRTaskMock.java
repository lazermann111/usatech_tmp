package com.usatech.posm.test;

import java.io.IOException;
import java.io.Reader;
import java.sql.Connection;
import java.util.concurrent.locks.ReentrantLock;

import simple.app.ServiceException;
import simple.db.DataLayerException;
import simple.results.DatasetHandlerSelectorFacade;
import simple.text.MultiRecordDelimitedParser;
import simple.util.concurrent.CustomThreadFactory;
import simple.util.concurrent.NotifyingBlockingWithStatsThreadPoolExecutor;

import com.usatech.posm.schedule.dfr.AbstractDFRParser;
import com.usatech.posm.schedule.dfr.ParseFinancialDFRTaskMXBean;

public class ParseFinancialDFRTaskMock extends AbstractDFRParser implements ParseFinancialDFRTaskMXBean {
	protected long processingThresholdMillis = 4 * 60 * 60 * 1000L + 50 * 60 * 1000L; // 3 hours 50 minutes
	protected static final MultiRecordDelimitedParser<String> parser = DFRSetupMock.createFinancialDFRParser(false);
	protected int queueSize = 50;
	protected int poolSize = 5;
	protected final ReentrantLock lock = new ReentrantLock();
	protected NotifyingBlockingWithStatsThreadPoolExecutor executor = null;

	public ParseFinancialDFRTaskMock() {
	}

	public long getProcessingThresholdMillis() {
		return processingThresholdMillis;
	}

	public void setProcessingThresholdMillis(long processingThresholdMillis) {
		this.processingThresholdMillis = processingThresholdMillis;
	}

	@Override
	protected long parseFileContent(Connection conn, long fileCacheId, String fileName, long startLineNumber, Reader fileContent) throws DataLayerException, IOException, ServiceException {
		DatasetHandlerSelectorFacade<ServiceException> selectorHandler = DFRSetupMock.createFinancialDFRHandler(conn,fileCacheId, fileName, startLineNumber, getOrCreateExecutor());
		parser.parse(fileContent, selectorHandler);
		return selectorHandler.getRow();
	}

	protected NotifyingBlockingWithStatsThreadPoolExecutor getOrCreateExecutor() {
		lock.lock();
		try {
			if(executor == null)
				executor = new NotifyingBlockingWithStatsThreadPoolExecutor(poolSize, queueSize, new CustomThreadFactory("ParseFinancialDFRThread-", true, Thread.NORM_PRIORITY));
			return executor;
		} finally {
			lock.unlock();
		}
	}

	public int getQueueSize() {
		return queueSize;
	}

	public void setQueueSize(int queueSize) {
		if(executor != null)
			throw new IllegalStateException("Executor is already created; queueSize cannot be changed");
		this.queueSize = queueSize;
	}

	public void setPoolSize(int poolSize) {
		this.poolSize = poolSize;
		if(executor != null)
			executor.setCorePoolSize(poolSize);
	}

	public int getPoolSize() {
		return poolSize;
	}

	public int getActiveCount() {
		return executor == null ? 0 : executor.getActiveCount();
	}

	public long getTaskCount() {
		return executor == null ? 0L : executor.getTaskCount();
	}

	public long getCompletedTaskCount() {
		return executor == null ? 0L : executor.getCompletedTaskCount();
	}

	public long getTotalProcessingCount() {
		return executor == null ? 0L : executor.getTotalProcessingCount();
	}

	public long getTotalLatencyTime() {
		return executor == null ? 0L : executor.getTotalLatencyTime();
	}

	public long getTotalProcessingTime() {
		return executor == null ? 0L : executor.getTotalProcessingTime();
	}

	public int getSampleSize() {
		return executor == null ? 0 : executor.getSampleSize();
	}

	public Double getAvgLatencyTime() {
		return executor == null ? null : executor.getAvgLatencyTime();
	}

	public Double getAvgProcessingTime() {
		return executor == null ? null : executor.getAvgProcessingTime();
	}

	public int getCorePoolSize() {
		return executor == null ? 0 : executor.getCorePoolSize();
	}

	public int getMaximumPoolSize() {
		return executor == null ? 0 : executor.getMaximumPoolSize();
	}

	public int getLargestPoolSize() {
		return executor == null ? 0 : executor.getLargestPoolSize();
	}

}
