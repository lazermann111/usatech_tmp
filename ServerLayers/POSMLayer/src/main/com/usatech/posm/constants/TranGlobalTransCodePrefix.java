package com.usatech.posm.constants;

import java.util.HashMap;
import java.util.Map;

import com.usatech.posm.exceptions.InvalidDataException;

/**
 * This enum defines the transaction prefix for common transaction and refund transaction.
 * The transaction prefix is used as the first part in the 'TranGlobalTransCd' construction.
 * 
 * @author Evan
 * 
 */
public enum TranGlobalTransCodePrefix
{
    TRAN_PREFIX("X"),
    REFUND_PREFIX("RF");

    private final String value;
    private static final Map<String, TranGlobalTransCodePrefix> tranGlobalTransCodePrefix = new HashMap<String, TranGlobalTransCodePrefix>();
    static
    {
        for (TranGlobalTransCodePrefix item : values())
        {
            tranGlobalTransCodePrefix.put(item.getValue(), item);
        }
    }

    /**
     * Internal constructor.
     * @param value
     */
    private TranGlobalTransCodePrefix(String value)
    {
        this.value = value;
    }

    /**
     * Get the intrinsic value of the type.
     * @return a String
     */
    public String getValue()
    {
        return value;
    }

    /**
     * Get the TranGlobalTransCodePrefix by value.
     * @param value a String
     * @return TranGlobalTransCodePrefix
     * @throws InvalidDataException can not find corresponding TranDeviceResultType
     */
    public static TranGlobalTransCodePrefix getByValue(String value) throws InvalidDataException
    {
        if (tranGlobalTransCodePrefix.containsKey(value))
            return tranGlobalTransCodePrefix.get(value);
        else
            throw new InvalidDataException("The value for TranGlobalTransCodePrefix is not valid:" + value);
    }
}
