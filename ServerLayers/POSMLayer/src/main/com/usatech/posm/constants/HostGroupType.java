package com.usatech.posm.constants;

import java.util.HashMap;
import java.util.Map;

import com.usatech.posm.exceptions.InvalidDataException;

/**
 * This enum defines the groups of the hosts.
 * 
 * @author Evan
 * 
 */
public enum HostGroupType
{
    BASE_HOST("BASE_HOST"),
    ESUDS_WASHER("ESUDS_WASHER"),
    ESUDS_DRYER("ESUDS_DRYER"),
    ESUDS_STACKED_DRYER("ESUDS_STACKED_DRYER"),
    ESUDS_STACKED_WASHER_DRYER("ESUDS_STACKED_WASHER_DRYER");

    private final String value;
    private static final Map<String, HostGroupType> hostGroupTypes = new HashMap<String, HostGroupType>();
    static
    {
        for (HostGroupType item : values())
        {
            hostGroupTypes.put(item.getValue(), item);
        }
    }

    /**
     * Internal constructor.
     * @param value
     */
    private HostGroupType(String value)
    {
        this.value = value;
    }

    /**
     * Get the intrinsic value of the type.
     * @return a String
     */
    public String getValue()
    {
        return value;
    }

    /**
     * Get the HostGroupType by value.
     * @param value a String
     * @return HostGroupType
     * @throws InvalidDataException can not find corresponding HostGroupType
     */
    public static HostGroupType getByValue(String value) throws InvalidDataException
    {
        if (hostGroupTypes.containsKey(value))
            return hostGroupTypes.get(value);
        else
            throw new InvalidDataException("The value for HostGroupType is not valid:" + value);
    }
}
