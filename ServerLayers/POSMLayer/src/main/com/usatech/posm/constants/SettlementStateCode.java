package com.usatech.posm.constants;

import java.util.HashMap;
import java.util.Map;

import com.usatech.posm.exceptions.InvalidDataException;

/**
 * This enum defines all the states that are part of the POSM while handling the settlement process.
 * 
 * @author sjillidimudi
 * 
 */
public enum SettlementStateCode
{
    // TODO This state was introduced during the NOP implementation. It was marked as deprecated .
    // Need to Check and update Accordingly.
    SERVER_SETTLEMENT_CANCELLED(5, "(deprecated) indicates reversal of action: no communication to gateway needed or reversal, if required, was sent successfull"),
    SERVER_WAITING_FOR_SETTLEMENT(4, "Waiting for response from the Authority for the Settlement Response"),
    SERVER_SETTLEMENT_DECLINE(2, "Got DECLINED response from Authority for the settlement."),
    SERVER_SETTLEMENT_FAILURE(3, "Got DECLINED_PERMANENT response from Authority for the settlement."),
    SERVER_SETTLEMENT_SUCCESS(1, "Got SUCCESS response from Authority for the settlement. This is the final state of the Settlement");

    private final int value;
    private final String description;

    private static final Map<Integer, SettlementStateCode> settlementStateCodes = new HashMap<Integer, SettlementStateCode>();

    static
    {
        for (SettlementStateCode item : values())
        {
            settlementStateCodes.put(item.getValue(), item);
        }
    }

    /**
     * This Methods returns the SettlementStateCode representation
     * @param value
     * @return
     * @throws InvalidDataException
     */

    public static SettlementStateCode getByValue(int value) throws InvalidDataException
    {
        if (settlementStateCodes.containsKey(value))
            return settlementStateCodes.get(value);
        else
            throw new InvalidDataException("There is No Constant defined with the Value - " + value);
    }

    /**
     * Internal Constructor
     * 
     * @param value
     * @param description
     */
    private SettlementStateCode(int value, String description)
    {
        this.description = description;
        this.value = value;
    }

    /**
     * Get the intrinsic value of the type.
     * 
     * @return char
     */
    public int getValue()
    {
        return value;
    }

    /**
     * Get the description of the Type.
     * @return String
     */
    public String getDescription()
    {
        return description;
    }
}
