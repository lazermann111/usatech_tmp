package com.usatech.posm.constants;

import java.util.HashMap;
import java.util.Map;

import com.usatech.posm.exceptions.InvalidDataException;

/**
 * This enum defines all the possible states of the Refund transactions.
 * 
 * @author Evan
 * 
 */
public enum RefundHist
{
    SERVER_REFUND_SUCCESS(1),
    SERVER_REFUND_DECLINE(2),
    SERVER_REFUND_FAILURE(3),
    WAITING_FOR_RESPONSE(4),
    SERVER_REFUND_DECLINE_PERMANENT(5),
    SERVER_REFUND_PENDING(6);

    private final long value;
    private static final Map<Long, RefundHist> refundHists = new HashMap<Long, RefundHist>();
    static
    {
        for (RefundHist item : values())
        {
            refundHists.put(item.getValue(), item);
        }
    }

    /**
     * Internal constructor.
     * @param value
     */
    private RefundHist(int value)
    {
        this.value = value;
    }

    /**
     * Get the intrinsic value of the type.
     * @return long
     */
    public long getValue()
    {
        return value;
    }

    /**
     * Get the RefundHist by value.
     * @param value int
     * @return RefundHist
     * @throws InvalidDataException can not find corresponding RefundHist
     */
    public static RefundHist getByValue(Long value) throws InvalidDataException
    {
        if (refundHists.containsKey(value))
            return refundHists.get(value);
        else
            throw new InvalidDataException("The value for RefundHist is not valid:" + value);
    }
}
