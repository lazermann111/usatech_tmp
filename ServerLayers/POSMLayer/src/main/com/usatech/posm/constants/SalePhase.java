package com.usatech.posm.constants;

import simple.lang.EnumCharValueLookup;
import simple.lang.InvalidValueException;

public enum SalePhase {
    ACTUAL('A'),
    INTENDED('I'),
    NO_PROCESSING('-'),
    REVERSAL('R'),
    ;

    private final char value;
    private static final EnumCharValueLookup<SalePhase> lookup = new EnumCharValueLookup<SalePhase>(SalePhase.class);
    private SalePhase(char value) {
        this.value = value;
    }

    public char getValue() {
        return value;
    }
    public static SalePhase getByValue(char value) throws InvalidValueException {
        return lookup.getByValue(value);
    }
}
