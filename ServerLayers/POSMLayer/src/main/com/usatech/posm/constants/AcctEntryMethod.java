package com.usatech.posm.constants;

import java.util.HashMap;
import java.util.Map;

import com.usatech.posm.exceptions.InvalidDataException;

/**
 * 
 * This class defines the various constants that represents the various modes of entering the
 * transaction details. For example , <code>MANUAL</code> represents that the entries were entered into the
 * system by typing the card details.
 * 
 * @author Evan,sjillidimudi
 * 
 */
public enum AcctEntryMethod
{
    UNSPECIFIED(1),
    MANUAL(2),
    MAGNETIC_STRIPE(3),
    ICC_READ(4),
    MAGNETIC_STRIPE_ICC_CAPABLE(5),
    CONTACTLESS(6);

    private final int value;
    private static final Map<Integer, AcctEntryMethod> accntEntryMethods = new HashMap<Integer, AcctEntryMethod>();
    static
    {
        for (AcctEntryMethod item : values())
        {
            accntEntryMethods.put(item.getValue(), item);
        }
    }

    /**
     * Internal constructor.
     * @param value
     */
    private AcctEntryMethod(int value)
    {
        this.value = value;
    }

    /**
     * Get the intrinsic value of the type.
     * @return int
     */
    public int getValue()
    {
        return value;
    }

    /**
     * Get the AcctEntryMethod by value.
     * @param value int
     * @return AcctEntryMethod
     * @throws InvalidDataException can not find corresponding AcctEntryMethod
     */
    public static AcctEntryMethod getByValue(Integer value) throws InvalidDataException
    {
        if (accntEntryMethods.containsKey(value))
            return accntEntryMethods.get(value);
        else
            throw new InvalidDataException("The value for AcctEntryMethod is not valid:" + value);
    }
}
