package com.usatech.posm.constants;

import java.util.HashMap;
import java.util.Map;

import com.usatech.posm.exceptions.InvalidDataException;

/**
 * This enum defines types of client payment ways, e.g: CASH, CREDIT_CARD etc.
 * 
 * @author Evan
 * 
 */
public enum ClientPaymentType
{
    CREDIT_CARD('C'),
    SPECIAL_CARD('S'),
    RFID_CREDIT_CARD('R'),
    CASH('M'),
    RFID_SPECIAL_CARD('P');

    private final char value;
    private static final Map<Character, ClientPaymentType> clientPaymentTypes = new HashMap<Character, ClientPaymentType>();
    static
    {
        for (ClientPaymentType item : values())
        {
            clientPaymentTypes.put(item.getValue(), item);
        }
    }

    /**
     * Internal constructor.
     * @param value
     */
    private ClientPaymentType(char value)
    {
        this.value = value;
    }

    /**
     * Get the intrinsic value of the type.
     * @return a char
     */
    public char getValue()
    {
        return value;
    }

    /**
     * Get the ClientPaymentType by value.
     * @param value a char
     * @return ClientPaymentType
     * @throws InvalidDataException can not find corresponding ClientPaymentType
     */
    public static ClientPaymentType getByValue(char value) throws InvalidDataException
    {
        if (clientPaymentTypes.containsKey(value))
            return clientPaymentTypes.get(value);
        else
            throw new InvalidDataException("The value for ClientPaymentType is not valid:" + value);
    }
}
