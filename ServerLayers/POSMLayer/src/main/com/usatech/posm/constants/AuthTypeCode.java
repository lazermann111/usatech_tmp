package com.usatech.posm.constants;

import simple.lang.EnumCharValueLookup;
import simple.lang.InvalidValueException;

import com.usatech.posm.exceptions.InvalidDataException;

/**
 * This class defined all the available Auth Type Codes . Reference : /POSM/USAT/POS/Const.pm #
 * PKG_AUTH_TYPE_GLOBALS__
 * 
 * @author sjillidimudi, Brian S. Krug
 * 
 */

public enum AuthTypeCode {
    NETWORK_AUTH('N', "#auth: live authorization, pre-sale amount not known"),
    LOCAL_AUTH('L', "#auth: a \"virtual\" offline sale--can be processed like a network_auth, a network_sale, or an offline_sale (depends on authority requirements/allowances)"),
    AUTH_ADJUSTMENT('A', "#auth: adjustment (network|local)_auth; applies to most recent auth event"),
    AUTH_CANCEL('C', "#cancellation of an auth (used by retry logic only; applies to all but void)"),
    AUTH_VOID('V', "#sale: void an auth (except cancel)"),
    AUTH_SALE('U', "#sale: post-auth sale, final amount known (applies to network_auth, may apply to local_auth)"),
    NETWORK_SALE('S', "#sale: live sale, pre-settlement amount known"),
    OFFLINE_SALE('O', "#sale: offline sale, pre-settlement amount known"),
    SALE_ADJUSTMENT('D', "sale: adjustment (auth|network|offline)_sale; applies to most recent sale event"),
    SALE_CANCEL('E', "#cancellation of a sale (used by retry logic only; applies to all but void)"),
    SALE_VOID('I', "#sale: void a sale (except cancel)");

    private final char value;
    private final String description;

    private static final EnumCharValueLookup<AuthTypeCode> lookup = new EnumCharValueLookup<AuthTypeCode>(AuthTypeCode.class);

    /**
     * Internal Constructor
     * 
     * @param value
     * @param description
     */
	private AuthTypeCode(char value, String description) {
		this.description = description;
		this.value = value;
	}

	/**
	 * Get the intrinsic value of the type.
	 * 
	 * @return Char
	 */
	public char getValue() {
		return value;
	}

	/**
	 * Get the description of the type.
	 * 
	 * @return String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * This Methods returns the TransactionStateCode representation
	 * 
	 * @param value
	 * @return TransactionStateCode representation of the input Value
	 * @throws InvalidDataException
	 */

	public static AuthTypeCode getByValue(char value) throws InvalidValueException {
		return lookup.getByValue(value);
	}

}
