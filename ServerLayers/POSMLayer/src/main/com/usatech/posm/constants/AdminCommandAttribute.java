package com.usatech.posm.constants;

import com.usatech.app.MessageChain;

/**
 * Attribute names of {@link MessageChain} for the administrative command.
 * 
 * @author Evan
 * 
 */
public enum AdminCommandAttribute
{
    COMMAND("Command"),
    POSM_INSTANCE("POSMInstance"),
    USER_ID("UserID"),
    TERMINAL_BATCH_ID("TerminalBatchID"),
    REASON("Reason"),
    TRANSACTION_ID("TransactionID"),
    FAILED_SETTLEMENTS("FailedSettlements"),
    FAILED_SETTLEMENTS_PREFIX("failedsettlement"),
    RESULT("Result"),

    MAX_PROCESSING_TIME("MaxProcessingTime"),
    MIN_PROCESSING_TIME("MinProcessingTime"),
    AVG_PROCESSING_TIME("AvgProcessingTime"),
    MAX_GW_PROCESSING_TIME("MaxGWProcessingTime"),
    MIN_GW_PROCESSING_TIME("MinGWProcessingTime"),
    AVG_GW_RPOCESSING_TIME("AvgGWProcessingTime"),
    POSM_STATUS("POSMStatus"),
    DB_CONNECTION_STATUS("DBConnectionStatus"),
    POOLED_DB_CONNECTIONS("PooledDBConnections"),
    
    PAYMENT_SUBTYPE_KEY_ID ("paymentSubTypeKeyId"),
    PAYMENT_SUBTYPE_CLASS("paymentSubTypeClass");
    ;

    private String value;

    /**
     * Internal constructor for this enum type.
     */
    private AdminCommandAttribute(String value)
    {
        this.value = value;
    }

    /**
     * Get the value
     * 
     * @return the name of the attribute
     */
    public String getValue()
    {
        return value;
    }

    /**
     * Override the toString() method to return the value.
     * 
     * @return value
     */
    @Override
    public String toString()
    {
        return value;
    }
}
