package com.usatech.posm.constants;

import java.util.HashMap;
import java.util.Map;

import com.usatech.posm.exceptions.InvalidDataException;

/**
 * The enum defines the error types which are supported by POSM.
 * It is used in Error Notification component for indicating the source of error.
 * 
 * @author Evan
 * 
 */
public enum ErrorType
{
    /**
     * POSM internal error
     */
    POSM_ERROR(1, "POSM internal error"),
    /**
     * Database error
     */
    DB_ERROR(2, "Database error");

    private final int value;
    private final String description;

    private static final Map<Integer, ErrorType> errorTypes = new HashMap<Integer, ErrorType>();
    static
    {
        for (ErrorType item : values())
        {
            errorTypes.put(item.getValue(), item);
        }
    }

    /**
     * Internal constructor for the enum type.
     * 
     * @param value
     * @param description
     */
    private ErrorType(int value, String description)
    {
        this.description = description;
        this.value = value;
    }

    /**
     * Get the description of this ErrorType.
     * @return
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Get the int representation of this ErrorType.
     * 
     * @return
     */
    public int getValue()
    {
        return value;
    }

    /**
     * Convert the int value to the corresponding ErrorType. An {@link InvalidDataException} will be
     * thrown if no such ErrorType is found.
     * 
     * @param value
     * @return an ErrorType corresponding to the value
     * @throws InvalidDataException
     */
    public static ErrorType getByValue(int value) throws InvalidDataException
    {
        if (errorTypes.containsKey(value))
            return errorTypes.get(value);
        else
            throw new InvalidDataException("There is No Constant defined with the Value - " + value);
    }
}
