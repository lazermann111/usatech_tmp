package com.usatech.posm.constants;

import java.util.HashMap;
import java.util.Map;

import com.usatech.posm.exceptions.InvalidDataException;

/**
 * This class defined all the available Auth States . Reference : /POSM/USAT/POS/Const.pm #
 * PKG_AUTH_HIST_GLOBALS__
 * 
 * @author sjillidimudi
 * 
 */
public enum AuthStateCode
{

    WAITING_FOR_RESPONSE(1, "Request was sent to Authority. Waiting for its outcome."),
    SUCCESS(2, "Success state from Authority"),
    DECLINE(3, "Declined by Authority . This state can be retried ."),
    FAILURE(4, "Failure from Authority. This case might arrise if the Authority layer fails to communicate with Gateways."),
    SUCCESS_CONDITIONAL(5, ""),
    PENDING_NETWORK_SALE(6, "Authorization is Completed. Pending for the Network sale "),
    DECLINE_PERMANENT(7, "Declined Permanently from the authority. Requires a manual intervention. ");

    private final int value;
    private final String description;

    private static final Map<Integer, AuthStateCode> authStateCodes = new HashMap<Integer, AuthStateCode>();

    static
    {
        for (AuthStateCode item : values())
        {
            authStateCodes.put(item.getValue(), item);
        }
    }

    /**
     * Internal Constructor.
     * @param value
     * @param description
     */
    private AuthStateCode(int value, String description)
    {
        this.description = description;
        this.value = value;
    }

    /**
     * Get the intrinsic value of the type.
     * @return
     */
    public int getValue()
    {
        return value;
    }

    /**
     * Get the description of the entry.
     * @return String
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * This Methods returns the TransactionStateCode representation
     * @param value
     * @return TransactionStateCode representation of the input Value
     * @throws InvalidDataException
     */

    public static AuthStateCode getByValue(int value) throws InvalidDataException
    {
        if (authStateCodes.containsKey(value))
            return authStateCodes.get(value);
        else
            throw new InvalidDataException("There is No Constant defined with the Value - " + value);
    }

}
