package com.usatech.posm.constants;

import java.util.HashMap;
import java.util.Map;

import com.usatech.posm.exceptions.InvalidDataException;

/**
 * This enum defines the Refund types existing in the system.
 * 
 * @author Evan
 * 
 */
public enum RefundType
{
    ACTUAL_BATCH_ADJUSTMENT('A'),
    GENERIC_REFUND('G'),
    CANCEL('C'), // used by retry logic only; applies to all but void
    VOID('V'); // applies to all except cancel

    private final char value;
    private static final Map<Character, RefundType> refundTypes = new HashMap<Character, RefundType>();
    static
    {
        for (RefundType item : values())
        {
            refundTypes.put(item.getValue(), item);
        }
    }

    /**
     * Internal constructor.
     * @param value
     */
    private RefundType(char value)
    {
        this.value = value;
    }

    /**
     * Get the intrinsic value of the type.
     * @return a char
     */
    public char getValue()
    {
        return value;
    }

    /**
     * Get the RefundType by value.
     * @param value a char
     * @return RefundType
     * @throws InvalidDataException can not find corresponding RefundType
     */
    public static RefundType getByValue(char value) throws InvalidDataException
    {
        if (refundTypes.containsKey(value))
            return refundTypes.get(value);
        else
            throw new InvalidDataException("The value for RefundType is not valid:" + value);
    }
}
