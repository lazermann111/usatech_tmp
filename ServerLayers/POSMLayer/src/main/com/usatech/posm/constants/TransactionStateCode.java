package com.usatech.posm.constants;

import simple.lang.EnumCharValueLookup;
import simple.lang.InvalidValueException;

import com.usatech.posm.exceptions.InvalidDataException;

/**
 * This Enum defines all the State Codes that a transaction has to under-go during the complete
 * life-cycle of the Transaction processing. Reference : /POSM/USAT/POS/Const.pm #
 * PKG_TRAN_HIST_GLOBALS__
 *
 * @author sjillidimudi
 *
 */
public enum TransactionStateCode {

    PROCESSED_CLIENT_CANCELLED_VEND('C', "#post batch, final tran: cancelled (user cancelled, vend timeout, etc.)"),
    SERVER_PROCESSING_BATCH('4', "Processing Batch"),
    PROCESSED_SERVER_BATCH('8', "Transactions Ready for processing by POSM"),
    PROCESSING_SERVER_TRAN('A', "Transaction is under processing by POSM"),
    PROCESSED_SERVER_TRAN('T', "SUCCESS from Authority .Settlement due for processing "),
    TRANSACTION_INCOMPLETE_ERROR('J', "DECLINED_PERMANENT from Authority . Need manual intervention "),
    TRANSACTION_INCOMPLETE('I', "DECLINED from Authority . Transaction can be re-tried for processing "),
    PROCESSING_SERVER_SETTLEMENT('S', "Transaction is under settlement processing by POSM"),
    TRANSACTION_COMPLETE('D', "Transaction processing is compeleted by POSM"),
    PROCESSED_SERVER_SETTLEMENT_INCOMPLETE('N', "Settelment DECLINED by Authority. Can be retired "),
    PROCESSED_SERVER_SETTLEMENT_ERROR('R', "Settelment DECLINED_PERMANENT by Authority. Requires Manual Intervention . " + "Or Some Unresolvable Error while procesing the Transaction ."),
    PROCESSING_SERVER_TRAN_RETRY('B', "The transaction is marked for re-processing by POSM "),
    TRANSACTION_COMPLETE_ERROR('E', "This is the final State of the transaction after force closure "),
    DUPLICATE_TRAN('Z', "Represents duplicate of already existing tran; retained for reconciliation purposes;"),
    PROCESSED_SERVER_TRAN_INTENDED('U', "State to which the transaction needs to be marked if the intended sale is processed and actual sale is due to the system."),
    PROCESSED_SERVER_BATCH_INTENDED('9', "Eligible State for processing the Intended Sale for the the transaction.");

    private final char value;
    private final String description;

    private static final EnumCharValueLookup<TransactionStateCode> lookup = new EnumCharValueLookup<TransactionStateCode>(TransactionStateCode.class);

    /**
     * Internal Constructor .
     *
     * @param value
     * @param description
     */
    private TransactionStateCode(char value, String description) {
        this.description = description;
        this.value = value;
    }

	/**
	 *
	 * @return Char
	 */
	public char getValue() {
		return value;
	}

	public String getDescription() {
		return description;
	}

	/**
	 * This Methods returns the TransactionStateCode representation
	 *
	 * @param value
	 * @return TransactionStateCode representation of the input Value
	 * @throws InvalidDataException
	 */

	public static TransactionStateCode getByValue(char value) throws InvalidValueException {
		return lookup.getByValue(value);
	}
}
