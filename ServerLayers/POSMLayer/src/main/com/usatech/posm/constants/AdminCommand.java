package com.usatech.posm.constants;

import java.util.HashMap;
import java.util.Map;

/**
 * This enum defines types of administrative commands.
 *
 * @author Evan
 *
 */
public enum AdminCommand
{

    //PAUSE("pause", new String[] {AdminCommandAttribute.POSM_INSTANCE.getValue()}, "Pause All Operations."),
    //UNPAUSE("unpause", new String[] {AdminCommandAttribute.POSM_INSTANCE.getValue()}, "Resume All Operations."),
    //QUIT("quit", new String[] {AdminCommandAttribute.POSM_INSTANCE.getValue()}, "Shutdown."),
    PAUSE_TRAN("pausetran", new String[] {AdminCommandAttribute.POSM_INSTANCE.getValue()}, "Pause Transaction Processing."),
    RESUME_TRAN("resumetran", new String[] {AdminCommandAttribute.POSM_INSTANCE.getValue()}, "Resume Transaction Processing."),
    SETTLE_ALL("settleall", new String[] {AdminCommandAttribute.POSM_INSTANCE.getValue()}, "Settle all batches now."),
    RETRY_TRAN("retrytran", new String[] {AdminCommandAttribute.POSM_INSTANCE.getValue(), AdminCommandAttribute.TRANSACTION_ID.getValue() }, "Retry a specific transaction now."),
    RETRY_SETTLE("retrysettle", new String[] {AdminCommandAttribute.POSM_INSTANCE.getValue(), AdminCommandAttribute.TERMINAL_BATCH_ID.getValue()}, "Retry a specific settlement now."),
    RETRY_ALL("retryall", new String[] {AdminCommandAttribute.POSM_INSTANCE.getValue()}, "Retry all transactions/settlements now."),
    FORCE_SETTLE("forcesettle", new String[] {AdminCommandAttribute.POSM_INSTANCE.getValue(), AdminCommandAttribute.TERMINAL_BATCH_ID.getValue(), AdminCommandAttribute.REASON.getValue()}, "Force a settlement of a specific batch."),
    FORCE_TRAN("forcetran", new String[] {AdminCommandAttribute.POSM_INSTANCE.getValue(), AdminCommandAttribute.TRANSACTION_ID.getValue(), AdminCommandAttribute.REASON.getValue()}, "Force a settlement of a specific Transaction."),
    ENABLE_DEBUG("enabledebug", new String[] {AdminCommandAttribute.POSM_INSTANCE.getValue()}, "Enable Status Debug Mode."),
    DISABLE_DEBUG("disabledebug", new String[] {AdminCommandAttribute.POSM_INSTANCE.getValue()}, "Disable Status Debug Mode."),
    LIST_FAILED_SETTELEMENTS("listfailedsettlements", new String[] {AdminCommandAttribute.POSM_INSTANCE.getValue()}, "List incomplete settlements."),
    STATUS("posmstatus", new String[] {AdminCommandAttribute.POSM_INSTANCE.getValue()}, "Status information."),
    PING("ping", new String[]{AdminCommandAttribute.POSM_INSTANCE.getValue()}, "Ping the POSM instance, if the POSM instance is running, it will return with the instance name"),
    PROCESS("process" , new String[]{AdminCommandAttribute.POSM_INSTANCE.getValue(), AdminCommandAttribute.PAYMENT_SUBTYPE_KEY_ID.getValue() , AdminCommandAttribute.PAYMENT_SUBTYPE_CLASS.getValue()}, "Posts the Notification message to the check queue for the processing for the specified terminal");

    private String value;
    private String[] arguments;
    private String description;
    private static final Map<String, AdminCommand> cmdTypes = new HashMap<String, AdminCommand>();
    static
    {
        for (AdminCommand item : values())
        {
            cmdTypes.put(item.getValue(), item);
        }
    }

    /**
     * Internal constructor for the enum type.
     *
     * @param value lower case name of the command
     * @param arguments String[] of arguments
     * @param description description of the command
     */
    private AdminCommand(String value, String[] arguments, String description)
    {
        this.value = value;
        this.arguments = arguments;
        this.description = description;
    }

    /**
     * Get the command's name.
     *
     * @return the lower case form of the command name
     */
    public String getValue()
    {
        return value;
    }

    /**
     * Get the arguments of the command.
     *
     * @return String[] of arguments.
     */
    public String[] getArguments()
    {
        return arguments;
    }

    /**
     * Get the description of the command.
     *
     * @return
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Get the corresponding AdminCommand from the lower case command name. A null will be return if
     * no such ErrorType is found.
     *
     * @param value the command's lower case name
     * @return an AdminCommand corresponding to the lower case name
     */
    public static AdminCommand getByValue(String value)
    {
        if (cmdTypes.containsKey(value))
            return cmdTypes.get(value);
        else
            return null;
    }
}
