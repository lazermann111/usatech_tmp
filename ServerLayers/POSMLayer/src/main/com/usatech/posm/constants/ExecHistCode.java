package com.usatech.posm.constants;

import java.util.HashMap;
import java.util.Map;

import com.usatech.posm.exceptions.InvalidDataException;

/**
 * This enum defines the various transaction processing result from POSM.
 * The execution result code will be marked to transaction's status by POSM.
 * 
 * @author Evan
 * 
 */
public enum ExecHistCode
{
    SUCCESSFUL_EXECUTION(0),
    UNSUCCESSFUL_EXECUTION(1),
    DEVICE_TRAN_ID_NOFOUND(11),
    DUPLICATE_TRANSACTION(20),
    TRANSACTION_OR_HOST_NOT_FOUND(-100),
    RETRY_MAX_ATTEMPT_EXCEEDED(50);

    private final int value;
    private static final Map<Integer, ExecHistCode> refundHists = new HashMap<Integer, ExecHistCode>();
    static
    {
        for (ExecHistCode item : values())
        {
            refundHists.put(item.getValue(), item);
        }
    }

    /**
     * Internal constructor.
     * @param value
     */
    private ExecHistCode(int value)
    {
        this.value = value;
    }

    /**
     * Get the intrinsic value of the type.
     * @return int
     */
    public int getValue()
    {
        return value;
    }

    /**
     * Get the ExecHistCode by value.
     * @param value int
     * @return ExecHistCode
     * @throws InvalidDataException can not find corresponding ExecHistCode
     */
    public static ExecHistCode getByValue(Integer value) throws InvalidDataException
    {
        if (refundHists.containsKey(value))
            return refundHists.get(value);
        else
            throw new InvalidDataException("The value for ExecHistCode is not valid:" + value);
    }
}
