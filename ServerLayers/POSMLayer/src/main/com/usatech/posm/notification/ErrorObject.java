package com.usatech.posm.notification;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.Serializable;

import simple.text.StringUtils;
import simple.text.StringUtils.Justification;

import com.usatech.posm.constants.ErrorType;

/**
 * The <code>ErrorObject</code> contains detailed information about 
 * an error that needed to be notified by the {@link ErrorNotificationMgr}.
 * 
 * @author Evan
 *
 */
public class ErrorObject implements Serializable {
	// constants used in formatting the ErrorObject into String.
	public static final String ERROR_TYPE = "Error Type: ";
	public static final String ERROR_MSG = "Error Message: ";
	public static final String ADDITIONAL_INFO = "Additional Info: ";
	
	private static final long serialVersionUID = 4601286542001220007L;
	
	private ErrorType errorType;
	private String errorMessage;
	private Object additionalInfo;
	
	/**
	 * Create an <code>ErrorObject</code>.
	 */
	public ErrorObject() {	
	}
	
	/**
	 * Create an <code>ErrorObject</code> with all properties provided.
	 * 
	 * @param errorType {@link ErrorType}
	 * @param msg	error message
	 * @param additionalInfo the additionalInfo object to set, 
	 * 		  it can be an <code>Exception</code> instance,
	 * 		  in that case, when returning the <code>ErrorObject</code> 
	 * 		  in a String representation, its stack trace will be printed;
	 * 
	 * 		  in other cases, the object's <code>toString()</code> method 
	 * 		  should be implemented, otherwise <code>java.lang.Object.toString()</code> 
	 * 		  will be called when returning the <code>ErrorObject</code> in a String 
	 * 		  representation.
	 */
	public ErrorObject(ErrorType errorType, String msg, Object additionalInfo) {
		this.errorType = errorType;
		this.errorMessage = msg;
		this.additionalInfo = additionalInfo;
	}
	
	/**
	 * Get the error type.
	 * @return the errorType
	 */
	public ErrorType getErrorType() {
		return errorType;
	}
	
	/**
	 * Set the error type.
	 * @param errorType the {@link ErrorType} to set
	 */
	public void setErrorType(ErrorType errorType) {
		this.errorType = errorType;
	}
	
	/**
	 * Get the error message.
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}
	
	/**
	 * Set the error message.
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	/**
	 * Get the additional information object.
	 * @return the additionalInfo
	 */
	public Object getAdditionalInfo() {
		return additionalInfo;
	}
	
	/**
	 * Set additional information object.
	 * 
	 * @param additionalInfo the additionalInfo object to set, 
	 * 		  it can be an <code>Exception</code> instance,
	 * 		  in that case, when returning the <code>ErrorObject</code> 
	 * 		  in a String representation, its stack trace will be printed;
	 * 
	 * 		  in other cases, the object's <code>toString()</code> method 
	 * 		  should be implemented, otherwise <code>java.lang.Object.toString()</code> 
	 * 		  will be called when returning the <code>ErrorObject</code> in a String 
	 * 		  representation.
	 */
	public void setAdditionalInfo(Object additionalInfo) {
		this.additionalInfo = additionalInfo;
	}
	
	/**
	 * Return the <code>ErrorObject</code> in a formatted String representation.
	 * <p>
	 * The format looks like:
	 * <pre>
	 * Error Type:         POSM error / DB error
	 * Error Message:      Some error message
	 * Additional Info:    Additional information
	 * </pre>
	 * @return a formatted String containing information in this ErrorObject
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("\n");
		
		if (errorType != null) {
			sb.append(StringUtils.pad(ERROR_TYPE, ' ', 20, Justification.LEFT));
			sb.append(errorType.getDescription() + "\n");
		}
		
		if (errorMessage != null) {
			sb.append(StringUtils.pad(ERROR_MSG, ' ', 20, Justification.LEFT));
			sb.append(errorMessage + "\n");
		}
		
		if (additionalInfo != null) {
			sb.append(StringUtils.pad(ADDITIONAL_INFO, ' ', 20, Justification.LEFT));
			if (additionalInfo instanceof Throwable) {
				ByteArrayOutputStream bao = new ByteArrayOutputStream();
				PrintStream ps = new PrintStream(bao);
				((Throwable)additionalInfo).printStackTrace(ps);
				sb.append("\n");
				sb.append(bao.toString());
			} else {
				sb.append(additionalInfo.toString());
			}
		}
		return sb.toString();
	}
}
