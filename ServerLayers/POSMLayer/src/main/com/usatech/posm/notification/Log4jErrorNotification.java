package com.usatech.posm.notification;

import org.apache.log4j.Logger;

/**
 * The Log4j implementation of the {@link ErrorNotification} interface.
 *
 * @author Evan
 *
 */
public class Log4jErrorNotification implements ErrorNotification {
	public static final String LOGGER_NAME = "ERROR_NOTIFICATION";

	private static Logger logger = Logger.getLogger(LOGGER_NAME);

	/**
	 * Notify the ErrorObject to an appender named as
	 * {@link Log4jErrorNotification#LOGGER_NAME}.
	 *
	 * @param e the ErrorObject contains detailed information.
	 */
	@Override
	public void notifyError(ErrorObject e) {
		logger.error(e);
	}
}
