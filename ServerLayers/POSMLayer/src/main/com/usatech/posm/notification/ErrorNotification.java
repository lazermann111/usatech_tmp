package com.usatech.posm.notification;


/**
 * The interface for the underlying worker used by {@link ErrorNotificationMgr} to notify the error.
 * The manner of notifying the error will be specified in a concrete implementation.
 *
 * @author Evan
 *
 */
public interface ErrorNotification {
	/**
	 * Notify the error.
	 *
	 * @param e {@link ErrorObject} that contains detailed information
	 */
	public void notifyError(ErrorObject e);
}
