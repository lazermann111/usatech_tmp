package com.usatech.posm.utils;

import static simple.text.MessageFormat.format;

import java.lang.management.ManagementFactory;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.LoadDataAttrEnum;

import simple.app.Publisher;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.ServiceManager;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.ByteInput;
import simple.io.Log;
import simple.results.BeanException;
import simple.results.Results;
import simple.text.StringUtils;

/**
 * Extends the {@link ServiceManager} to provide some customizations for POSM.
 *
 * @author bkrug
 *
 */
public class POSMUtils {
	private static Log log = Log.getLog();
	protected final static String GLOBAL_TOKEN_PREFIX = ManagementFactory.getRuntimeMXBean().getName() + ':' + Long.toHexString(System.currentTimeMillis()).toUpperCase() + ':';
	protected final static AtomicLong tokenCdGenerator = new AtomicLong(Double.doubleToLongBits(Math.random()));
	protected static String settleUpdateQueueKey = "usat.import.tran.settled";

	/**
	 */
	public POSMUtils() {
		super();
	}

	/**
	 * This method generates the Global Token that needs to be updated to the
	 * terminal Table. A unique token has to be generated with a combination of
	 * the POSM instance name + Start up TimeStamp (in Hex) + Incremented Number (in Hex) separated with ":".
	 *
	 * @return
	 */
	public static String generateGlobalToken() {
		return GLOBAL_TOKEN_PREFIX + Long.toHexString(tokenCdGenerator.getAndIncrement()).toUpperCase();
	}

	public static boolean obtainGlobalToken(Connection conn, long paymentSubtypeKeyId, String paymentSubtypeClass, String currentGlobalTokenCode, String newGlobalTokenCode, boolean currentMustMatch) throws ServiceException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(AuthorityAttrEnum.ATTR_GLOBAL_TOKEN_CD.getValue(), currentGlobalTokenCode);
		params.put(AuthorityAttrEnum.ATTR_NEW_GLOBAL_TOKEN_CD.getValue(), newGlobalTokenCode);
		params.put(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_KEY_ID.getValue(), paymentSubtypeKeyId);
		params.put(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_CLASS.getValue(), paymentSubtypeClass);
		params.put("currentMustMatch", currentMustMatch ? 'Y' : 'N');
		int updateCount;
		try {
			DataLayerMgr.executeUpdate(conn, "OBTAIN_GLOBAL_TOKEN", params);
			updateCount = ConvertUtils.getInt(params.get("updateCount"));
		} catch(SQLException e) {
			if(e.getErrorCode() == 20865)
				throw new RetrySpecifiedServiceException("Payment subtype class '" + paymentSubtypeClass + "' does not have terminals", e, WorkRetryType.NO_RETRY);
			throw new ServiceException("Failed to Update the Global Token Code for the paymentSubtypeKeyId :" + paymentSubtypeKeyId + " :: paymentSubtypeClass :" + paymentSubtypeClass, e);
		} catch(DataLayerException e) {
			throw new ServiceException("Failed to Update the Global Token Code for the paymentSubtypeKeyId :" + paymentSubtypeKeyId + " :: paymentSubtypeClass :" + paymentSubtypeClass, e);
		} catch(ConvertException e) {
			throw new ServiceException("Failed to Update the Global Token Code for the paymentSubtypeKeyId :" + paymentSubtypeKeyId + " :: paymentSubtypeClass :" + paymentSubtypeClass, e);
		}
		boolean success = (updateCount == 1);
		if(log.isDebugEnabled()) {
			 if(success)
				 log.debug("Obtained the global token for Terminal " + paymentSubtypeKeyId + "(" + paymentSubtypeClass + ")");
			 else
				 log.debug("Could not obtain the global token for Terminal " + paymentSubtypeKeyId + "(" + paymentSubtypeClass + ")");
		}
		return success;
	}


	/**
	 * This method resets the Global Token to Null for the specified
	 * paymentSubTypeKeyId and paymentSubtypeClass.
	 *
	 * @param conn
	 * @param paymentSubTypeKeyId
	 * @return This method return the record update count .
	 * @throws ServiceException
	 */
	public static boolean releaseGlobalToken(Connection conn, long paymentSubtypeKeyId, String paymentSubtypeClass, String globalTokenCode) throws ServiceException {
		Map<String, Object> input = new HashMap<String, Object>();
		input.put(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_KEY_ID.getValue(), paymentSubtypeKeyId);
		input.put(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_CLASS.getValue(), paymentSubtypeClass);
		input.put(AuthorityAttrEnum.ATTR_GLOBAL_TOKEN_CD.getValue(), globalTokenCode);
		int updateCount = 0;
		try {
			DataLayerMgr.executeUpdate(conn, "RESET_GLOBAL_TOKEN", input);
			updateCount = ConvertUtils.getInt(input.get("updateCount"));
		} catch(SQLException e) {
			throw new ServiceException("Failed to reset the Global Token Code for the paymentSubtypeKeyId :" + paymentSubtypeKeyId + " :: paymentSubtypeClass :" + paymentSubtypeClass, e);
		} catch(DataLayerException e) {
			throw new ServiceException("Failed to reset the Global Token Code for the paymentSubtypeKeyId :" + paymentSubtypeKeyId + " :: paymentSubtypeClass :" + paymentSubtypeClass, e);
		} catch(ConvertException e) {
			throw new ServiceException("Failed to reset the Global Token Code for the paymentSubtypeKeyId :" + paymentSubtypeKeyId + " :: paymentSubtypeClass :" + paymentSubtypeClass, e);
		}
		return updateCount == 1;
	}

	public static void publishSettleUpdate(Publisher<ByteInput> publisher, String globalTransCd, AuthResultCode authResultCode, long processedTime, String authorityTranCd, String sourceSystemCd, char authTypeCd, Number amount, Integer overrideTransTypeId, Long tranId) throws ServiceException {
		MessageChain mc = new MessageChainV11();
		MessageChainStep settleUpdateStep = mc.addStep(settleUpdateQueueKey);
		settleUpdateStep.setAttribute(LoadDataAttrEnum.ATTR_SOURCE_SYSTEM_CD, sourceSystemCd);
		settleUpdateStep.setAttribute(LoadDataAttrEnum.ATTR_SETTLE_DATE, processedTime);
		settleUpdateStep.setIndexedAttribute(LoadDataAttrEnum.ATTR_SETTLE_STATE_ID, 0, getSettleState(authResultCode, authTypeCd));
		settleUpdateStep.setIndexedAttribute(LoadDataAttrEnum.ATTR_TRAN_GLOBAL_TRANS_CD, 0, globalTransCd);
		settleUpdateStep.setIndexedAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD, 0, getApprovalCode(authResultCode, authorityTranCd, authTypeCd));
		settleUpdateStep.setIndexedAttribute(LoadDataAttrEnum.ATTR_TRANS_TYPE_ID, 0, overrideTransTypeId);
		settleUpdateStep.setIndexedAttribute(AuthorityAttrEnum.ATTR_TRAN_ID, 0, tranId);
		if(amount != null)
			switch(authTypeCd) {
				case 'C':
				case 'V':
				case 'E':
				case 'I':
					settleUpdateStep.setIndexedAttribute(AuthorityAttrEnum.ATTR_AMOUNT, 0, amount);
					break;
			}
		settleUpdateStep.setAttribute(LoadDataAttrEnum.ATTR_TRAN_COUNT, 1);
		MessageChainService.publish(mc, publisher);
	}

	public static void publishSettleUpdate(Publisher<ByteInput> publisher, AuthResultCode authResultCode, long processedTime, Results... results) throws ServiceException {
		MessageChain mc = new MessageChainV11();
		MessageChainStep settleUpdateStep = mc.addStep(settleUpdateQueueKey);		
		settleUpdateStep.setAttribute(LoadDataAttrEnum.ATTR_SOURCE_SYSTEM_CD, "PSS");
		settleUpdateStep.setAttribute(LoadDataAttrEnum.ATTR_SETTLE_DATE, processedTime);
		int i = 0;
		for(Results r : results)
			while(r.next()) {
				try {
					char authTypeCd = r.getValue(AuthorityAttrEnum.ATTR_AUTH_TYPE_CD.getValue(), Character.class);
					boolean sale = (r.get(AuthorityAttrEnum.ATTR_SALE_AUTH_ID.getValue()) != null);
					boolean refund = (r.get(AuthorityAttrEnum.ATTR_REFUND_ID.getValue()) != null);
					Object transGlobalTransCd = r.getValue(LoadDataAttrEnum.ATTR_TRAN_GLOBAL_TRANS_CD.getValue());
					if(updateSettleState(sale, refund, authTypeCd, transGlobalTransCd)) {
						settleUpdateStep.setIndexedAttribute(LoadDataAttrEnum.ATTR_SETTLE_STATE_ID, i, getSettleState(authResultCode, authTypeCd));
						settleUpdateStep.setIndexedAttribute(LoadDataAttrEnum.ATTR_TRAN_GLOBAL_TRANS_CD, i, transGlobalTransCd);
						settleUpdateStep.setIndexedAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD, i, getApprovalCode(authResultCode, r.getValue(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD.getValue(), String.class), authTypeCd));
						settleUpdateStep.setIndexedAttribute(LoadDataAttrEnum.ATTR_TRANS_TYPE_ID, i, r.getValue(LoadDataAttrEnum.ATTR_TRANS_TYPE_ID.getValue()));
						Number amount = r.getValue(AuthorityAttrEnum.ATTR_AMOUNT.getValue(), Number.class);
						if(amount != null)
							switch(authTypeCd) {
								case 'C':
								case 'V':
								case 'E':
								case 'I':
									settleUpdateStep.setIndexedAttribute(AuthorityAttrEnum.ATTR_AMOUNT, i, amount);
									break;
							}
						i++;
					}
				} catch(ConvertException e) {
					throw new RetrySpecifiedServiceException(e, WorkRetryType.NO_RETRY);
				}
			}
		settleUpdateStep.setAttribute(LoadDataAttrEnum.ATTR_TRAN_COUNT, i);
		MessageChainService.publish(mc, publisher);
	}

	protected static boolean updateSettleState(boolean sale, boolean refund, char authTypeCd, Object transGlobalTransCd) {
		if(transGlobalTransCd == null || StringUtils.isBlank(transGlobalTransCd.toString()))
			return false;
		return true;
	}

	protected static int getSettleState(AuthResultCode authResultCode) {
		switch(authResultCode) {
			case APPROVED:
				return 3;
			case DECLINED:
				return 4;
			case DECLINED_PERMANENT:
				return 5;
			case DECLINED_PAYMENT_METHOD:
				return 5;
			case FAILED:
				return 4;
			case PARTIAL:
				return 1;
			default:
				return 1;
		}
	}
	
	protected static int getSettleState(AuthResultCode authResultCode, char authTypeCd) {
		switch(authTypeCd) {
			case 'C':
			case 'V':
			case 'E':
			case 'I':
				return 2;
			default:
				return getSettleState(authResultCode);
		}
	}

	protected static String getApprovalCode(AuthResultCode authResultCode, String authorityTranCd) {
		switch(authResultCode) {
			case APPROVED:
				return authorityTranCd == null ? "Approved" : authorityTranCd;
			case DECLINED:
				return "Pending";
			case DECLINED_PERMANENT:
			case DECLINED_PAYMENT_METHOD:
				return "Declined";
			case FAILED:
				return "Pending";
			case PARTIAL:
				return "Pending";
			default:
				return "Pending";
		}
	}
	
	protected static String getApprovalCode(AuthResultCode authResultCode, String authorityTranCd, char authTypeCd) {
		switch(authTypeCd) {
			case 'C':
			case 'V':
			case 'E':
			case 'I':
				return "No Charge";
			default:
				return getApprovalCode(authResultCode, authorityTranCd);
		}
	}
	
	public static <T> T getAppSetting(String key, Class<T> toClass, T defaultValue) throws ServiceException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("appSettingCd", key);
		
		try{
			DataLayerMgr.selectInto("GET_APP_SETTING_VALUE", params);
			return ConvertUtils.convertSafely(toClass, params.get("appSettingValue"), defaultValue);
		} catch(SQLException | DataLayerException | BeanException e) {
			throw new ServiceException(format("Error getting APP_SETTING {0}", key), e);
		}
	}
	
	public static void setAppSetting(String key, Object value) throws ServiceException {
		try{
			DataLayerMgr.executeUpdate("UPDATE_APP_SETTING", new Object[] {value, key}, true);
		} catch(SQLException | DataLayerException  e) {
			throw new ServiceException(format("Error setting APP_SETTING {0}={1}", key, value), e);
		}
	}

}
