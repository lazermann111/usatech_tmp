package com.usatech.posm.tasks;

import java.sql.Connection;

import simple.app.ServiceException;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.posm.utils.POSMUtils;


/**
 * This is the base class for all the tasks that perform operations that are to be performed after
 * the response from the Authority. Any method that can be reused by the update Tasks must go into
 * this class.
 *
 * @author sjillidimudi, Evan, bkrug
 */
public abstract class POSMTask implements MessageChainTask {
	/**
	 * The implementation of the method contains all the required business that
	 * has to be performed by the task.
	 *
	 * @param taskInfo
	 *            taskInfo contains the complete information about the Step
	 * @return int Value representing the status of the execution of the task.
	 */
	protected abstract int processInternal(MessageChainTaskInfo taskInfo) throws ServiceException;

	/**
	 * @see com.usatech.app.MessageChainTask#process(com.usatech.app.MessageChainTaskInfo)
	 */
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		return processInternal(taskInfo);
	}
	protected void checkValidGlobalToken(Connection conn, MessageChainStep step) throws ServiceException {
		String globalTokenCode;
		long paymentSubTypeKeyId;
		String paymentSubTypeClass;
		try {
			globalTokenCode = step.getAttribute(AuthorityAttrEnum.ATTR_GLOBAL_TOKEN_CD, String.class, true);
			paymentSubTypeKeyId = step.getAttribute(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_KEY_ID, Long.class, true);
			paymentSubTypeClass = step.getAttribute(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_CLASS, String.class, true);
		} catch(AttributeConversionException e) {
			throw new ServiceException("Could not convert attributes", e);
		}
		if(!POSMUtils.obtainGlobalToken(conn, paymentSubTypeKeyId, paymentSubTypeClass, globalTokenCode, globalTokenCode, false)) {
			throw new ServiceException("Could not obtain the global token code '" + globalTokenCode + "' for Terminal " + paymentSubTypeKeyId + "(" + paymentSubTypeClass + ")");
		}
	}
}
