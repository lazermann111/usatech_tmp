package com.usatech.posm.tasks;

import static simple.text.MessageFormat.*;

import java.sql.SQLException;
import java.util.*;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.NotEnoughRowsException;
import simple.io.Log;
import simple.results.BeanException;

import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.MessageAttrEnum;

public class RiskCheckTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	
	private double minAvgAlarmScore = 0;
	private int testDays = 1;
	private int sampleDays = 90;
	private double tranCountMin = 10;
	private double tranAmountMin = 20;
	private double magnitudeOfDeviationMin = 3.5;
	private double samplePopulationPercentMin = 0.5;
	private int chargebackCountMin = 1;
	private int chargebackAmountMin = 2;

	private List<RiskCheck> checks = new ArrayList<>();
	
	public synchronized void init() {
		// TODO: adjust weight per query - setting all weights to 1 for now
		if (checks.isEmpty()) {
			checks.add(new MagnitudeOfDeviationFromSampleStandardDeviationQueryRiskCheck("Average transaction count", 1, tranCountMin, "RISK_CHECK_TRAN_COUNT_TO_SAMPLE"));
			checks.add(new MagnitudeOfDeviationFromSampleStandardDeviationQueryRiskCheck("Average transaction amount", 1, tranAmountMin, "RISK_CHECK_TRAN_AMOUNT_TO_SAMPLE"));
			checks.add(new MagnitudeOfDeviationFromExpectedQueryRiskCheck("Average transaction count", 1, tranCountMin, "RISK_CHECK_TRAN_COUNT_TO_EXPECTED"));
			checks.add(new MagnitudeOfDeviationFromExpectedQueryRiskCheck("Average transaction amount", 1, tranAmountMin, "RISK_CHECK_TRAN_AMOUNT_TO_EXPECTED"));
			checks.add(new ChargebackQueryRiskCheck("Chargeback count", 1, chargebackCountMin, "RISK_CHECK_CHARGEBACK_COUNT"));
			checks.add(new ChargebackQueryRiskCheck("Chargeback amount", 1, chargebackAmountMin, "RISK_CHECK_CHARGEBACK_AMOUNT"));
			checks.add(new MagnitudeOfDeviationFromSampleStandardDeviationQueryRiskCheck("Prepaid card auth count", 1, tranCountMin, "RISK_CHECK_PREPAID_AUTH_COUNT_TO_SAMPLE"));
			checks.add(new MagnitudeOfDeviationFromSampleStandardDeviationQueryRiskCheck("Foreign card auth count", 1, tranCountMin, "RISK_CHECK_FOREIGN_AUTH_COUNT_TO_SAMPLE"));
		}
	}

	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		init();
		
		String deviceName = null;
		
		try {
			deviceName = taskInfo.getStep().getAttribute(MessageAttrEnum.ATTR_DEVICE_NAME, String.class, true);
		} catch(Exception e) {
			log.error("Failed to get {0} from taskInfo", MessageAttrEnum.ATTR_DEVICE_NAME, e);
			return 2;
		}
		
		log.debug("Risk checking device {0}", deviceName);
		
		long terminalId = -1;
		try {
			terminalId = getTerminalId(deviceName);
		} catch(Exception e) {
			log.debug("No active terminal for device {0}", deviceName, e);
			return 0;
		}
		
		List<CheckResult> results = new ArrayList<>();
		for(RiskCheck check : checks) {
			CheckResult result = check.check(deviceName, terminalId);
			if (result == null)
				continue;
			
			if (result.isSignifigant()) {
				results.add(result);
			}
		}
		
		if (results.isEmpty()) {
			log.debug("Device {0} has no risk check alarms", deviceName);
			return 0;
		}
		
		Collections.sort(results);
		
		boolean first = true;
		StringBuilder sb = new StringBuilder();
		sb.append(format("{0} alerts: ", results.size()));
		for(CheckResult result : results) {
			if (!first)
				sb.append(", ");
			sb.append(result.getCheck().getMetricName());
			first = false;
		}
		
		sb.append("\n");
		
		double sumScore = 0;
		for(int i=0; i<results.size(); i++) {
			CheckResult result = results.get(i);
			sumScore += result.getScore();
			String s = format("  {0}: {1} (score={2,number,#0.#} weight={3,number,#0.##})\n",
					i+1,
					result.getMessage(),
					result.getScore(),
					result.getCheck().getWeight());
			sb.append(s);
		}
		
		double avgScore = sumScore/results.size();
		if (avgScore < minAvgAlarmScore) {
			return 0;
		}
		
		log.info("Device {0} triggered risk check alarm with average score {1,number,#0.##}: {2}", deviceName, avgScore, sb.toString());
		
		Map<String, Object> insertParams = new HashMap<>();
		insertParams.put("terminalId", terminalId);
		insertParams.put("score", avgScore);
		insertParams.put("alertMessage", sb.toString());
		
		try {
			DataLayerMgr.executeUpdate("INSERT_RISK_ALERT", insertParams, true);
		} catch (SQLException | DataLayerException e) {
			log.error("Failed to insert into RISK_CHECK_ALERT for device {0}", deviceName, e);
			return 2;
		}
		
		return 1;
	}
	
	public double getMinAvgAlarmScore() {
    return minAvgAlarmScore;
  }

  public void setMinAvgAlarmScore(double minAvgAlarmScore) {
    this.minAvgAlarmScore = minAvgAlarmScore;
  }
  
  public int getTestDays() {
		return testDays;
	}
  
  public void setTestDays(int testDays) {
		this.testDays = testDays;
	}

  public int getSampleDays() {
    return sampleDays;
  }

  public void setSampleDays(int sampleDays) {
    this.sampleDays = sampleDays;
  }

  public double getTranCountMin() {
    return tranCountMin;
  }

  public void setTranCountMin(double tranCountMin) {
    this.tranCountMin = tranCountMin;
  }

  public double getTranAmountMin() {
    return tranAmountMin;
  }

  public void setTranAmountMin(double tranAmountMin) {
    this.tranAmountMin = tranAmountMin;
  }
  
  public double getMagnitudeOfDeviationMin() {
		return magnitudeOfDeviationMin;
	}
  
  public void setMagnitudeOfDeviationMin(double magnitudeOfDeviationMin) {
		this.magnitudeOfDeviationMin = magnitudeOfDeviationMin;
	}
  
  public double getSamplePopulationPercentMin() {
		return samplePopulationPercentMin;
	}
  
  public void setSamplePopulationPercentMin(double samplePopulationPercentMin) {
		this.samplePopulationPercentMin = samplePopulationPercentMin;
	}
  
  public int getChargebackCountMin() {
		return chargebackCountMin;
	}
  
  public void setChargebackCountMin(int chargebackCountMin) {
		this.chargebackCountMin = chargebackCountMin;
	}
  
  public int getChargebackAmountMin() {
		return chargebackAmountMin;
	}
  
  public void setChargebackAmountMin(int chargebackAmountMin) {
		this.chargebackAmountMin = chargebackAmountMin;
	}

  private static long getTerminalId(String deviceName) throws DataLayerException, BeanException, SQLException, ConvertException {
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("deviceName", deviceName);
		DataLayerMgr.selectInto("GET_TERMINAL_ID_BY_DEVICE_NAME", params);
		return ConvertUtils.getLong(params.get("terminalId"));
	}
	
	public class CheckResult implements Comparable<CheckResult>
	{
		private RiskCheck check;
		private boolean signifigant = false;
		private double score;
		private String message;
		private double testVal;
		private double expectedVal;
		private double expectedValStddev;
		
		public CheckResult(RiskCheck check) {
			this.check = check;
		}
		
		public boolean isSignifigant() {
			return signifigant;
		}
		
		public void setSignifigant(boolean signifigant) {
			this.signifigant = signifigant;
		}
		
		public double getScore() {
			return score;
		}
		
		public void setScore(double score) {
			this.score = score;
		}
		
		public RiskCheck getCheck() {
			return check;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public double getTestVal() {
			return testVal;
		}

		public void setTestVal(double testVal) {
			this.testVal = testVal;
		}

		public double getExpectedVal() {
			return expectedVal;
		}

		public void setExpectedVal(double expectedAvg) {
			this.expectedVal = expectedAvg;
		}

//		public double getExpectedValStddev() {
//			return expectedValStddev;
//		}
//
//		public void setExpectedValStddev(double expectedValStddev) {
//			this.expectedValStddev = expectedValStddev;
//		}
//		
//		public double getMagnitudeOfDeviation() {
//			return magnitudeOfDeviation;
//		}
//		
//		public void setMagnitudeOfDeviation(double magnitudeOfDeviation) {
//			this.magnitudeOfDeviation = magnitudeOfDeviation;
//		}

		@Override
		public int compareTo(CheckResult o) {
			return Double.compare(this.getScore(), o.getScore());
		}
		
		public boolean isInvalid() {
			return (Double.isNaN(testVal) || Double.isNaN(expectedVal) || Double.isNaN(expectedValStddev) || Double.isNaN(magnitudeOfDeviationMin));
		}
	}
	
	public abstract class RiskCheck {
		protected final String metricName;
		protected final double minTestVal;
		protected final double weight;
		
		public RiskCheck(String metricName, double weight, double minTestVal) {
			this.metricName = metricName;
			this.weight = weight;
			this.minTestVal = minTestVal;
		}
		
		public String getMetricName() {
			return metricName;
		}
		
		public double getWeight() {
			return weight;
		}

		public double getMinTestVal() {
			return minTestVal;
		}
		
		public abstract CheckResult check(String deviceName, long terminalId) throws ServiceException;
	}
	
	public abstract class QueryRiskCheck extends RiskCheck {
		protected String queryId;
		protected Map<String, Object> params;
		
		public QueryRiskCheck(String metricName, double weight, double minTestVal, String queryId) {
			this(metricName, weight, minTestVal, queryId, new HashMap<>());
		}

		public QueryRiskCheck(String metricName, double weight, double minTestVal, String queryId, Map<String, Object> params) {
			super(metricName, weight, minTestVal);
			this.queryId = queryId;
			this.params = params;
		}
		
		public String getQueryId() {
			return queryId;
		}

		public Map<String, Object> getParams() {
			return params;
		}
		
		public void addParam(String name, Object val) {
			params.put(name, val);
		}
		
		public CheckResult check(String deviceName, long terminalId) throws ServiceException {
			Map<String,Object> params = new HashMap<String, Object>();
			params.put("terminalId", terminalId);
			params.put("deviceName", deviceName);
			params.put("testDays", testDays);
			params.putAll(this.params);
			
			try {
				DataLayerMgr.selectInto(queryId, params);
			} catch(NotEnoughRowsException e) {
				return null;
			} catch(Exception e) {
				throw new ServiceException(format("Risk check query {0} failed for terminalId {1}", queryId, terminalId), e);
			}
			
			return processResults(deviceName, params);
		}
		
		protected abstract CheckResult processResults(String deviceName, Map<String, Object> results) throws ServiceException;
	}
	
	public abstract class MagnitudeOfDeviationQueryRiskCheck extends QueryRiskCheck {
		
		public MagnitudeOfDeviationQueryRiskCheck(String metricName, double weight, double minTestVal, String queryId) {
			super(metricName, weight, minTestVal, queryId);
		}

		protected CheckResult processResults(String deviceName, Map<String, Object> results) throws ServiceException {
			CheckResult checkResult = new CheckResult(this);
			
			double expectedValStddev = 0;
			double magnitudeOfDeviation = 0;
			try {
				checkResult.setTestVal(getDouble(results, "testVal"));
				checkResult.setExpectedVal(getDouble(results, "expectedVal"));
//				checkResult.setExpectedValStddev(getDouble(results, "expectedValStddev"));
//				checkResult.setMagnitudeOfDeviation(getDouble(results, "magnitudeOfDeviation"));
				expectedValStddev = getDouble(results, "expectedValStddev");
				magnitudeOfDeviation = getDouble(results, "magnitudeOfDeviation");
			} catch (ConvertException e) {
				throw new ServiceException(e.getMessage(), e);
			}
			
			if (checkResult.isInvalid()) {
				log.debug("Query check {0} for {1} returned invalid results", this.getQueryId(), deviceName);
				return null;
			}
			
			checkResult.setSignifigant(checkResult.getTestVal() > minTestVal && magnitudeOfDeviation > magnitudeOfDeviationMin);
			checkResult.setScore(magnitudeOfDeviation * getWeight());
			checkResult.setMessage(generateMessage(checkResult, expectedValStddev, magnitudeOfDeviation));

			return checkResult;
		}
		
		protected abstract String generateMessage(CheckResult checkResult, double expectedValStddev, double magnitudeOfDeviation);
	}
	
	public class MagnitudeOfDeviationFromSampleStandardDeviationQueryRiskCheck extends MagnitudeOfDeviationQueryRiskCheck {
		
		public MagnitudeOfDeviationFromSampleStandardDeviationQueryRiskCheck(String metricName, double weight, double minTestVal, String queryId) {
			super(metricName, weight, minTestVal, queryId);
			addParam("sampleDays", sampleDays);
			addParam("samplePopulationPercentMin", samplePopulationPercentMin);
		}
		
		protected String generateMessage(CheckResult checkResult, double expectedValStddev, double magnitudeOfDeviation) {
			return format("{0} of {1,number,#0.##} for the prior {2} days is {3,number,#0.##} standard deviations away from the prior {4} day average of {5,number,#0.##}",
					getMetricName(),
					checkResult.getTestVal(),
					getTestDays(),
					magnitudeOfDeviation,
					getSampleDays(),
					checkResult.getExpectedVal());
		}
	}
	
	public class MagnitudeOfDeviationFromExpectedQueryRiskCheck extends MagnitudeOfDeviationQueryRiskCheck {
		
		public MagnitudeOfDeviationFromExpectedQueryRiskCheck(String metricName, double weight, double minTestVal, String queryId) {
			super(metricName, weight, minTestVal, queryId);
		}
		
		protected String generateMessage(CheckResult checkResult, double expectedValStddev, double magnitudeOfDeviation) {
			return format("{0} of {1,number,0.00} is {2,number,0.00} times the expected value of {3,number,0.00}",
				getMetricName(),
				checkResult.getTestVal(),
				magnitudeOfDeviation,
				checkResult.getExpectedVal());
		}
	}
	
	public class ChargebackQueryRiskCheck extends QueryRiskCheck {
		
		public ChargebackQueryRiskCheck(String metricName, double weight, double minTestVal, String queryId) {
			super(metricName, weight, minTestVal, queryId);
		}
		
		protected CheckResult processResults(String deviceName, Map<String, Object> results) throws ServiceException {
			CheckResult checkResult = new CheckResult(this);
			
			try {
				checkResult.setTestVal(getDouble(results, "testVal"));
			} catch (ConvertException e) {
				throw new ServiceException(e.getMessage(), e);
			}
			
			if (checkResult.isInvalid()) {
				log.debug("Query check {0} for {1} returned invalid results", this.getQueryId(), deviceName);
				return null;
			}
			
			checkResult.setSignifigant(checkResult.getTestVal() > getMinTestVal());
			checkResult.setScore(checkResult.getTestVal() * getWeight());
			
			checkResult.setMessage(format("{0} of {1,number,0.00} for the prior {2} days exceeds the threshold of {3}",
				getMetricName(),
				checkResult.getTestVal(),
				getTestDays(),
				getMinTestVal()));

			return checkResult;
		}
	}
	
	public static double getDouble(Map<String,Object> params, String key) throws ConvertException {
		try {
			return ConvertUtils.getDouble(params.get(key));
		} catch (ConvertException e) { 
			//throw new ConvertException("Failed to convert key " + key, Double.class, params, e);
			return Double.NaN;
		}
	}
}
