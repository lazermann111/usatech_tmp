package com.usatech.posm.tasks;

import java.lang.management.ManagementFactory;
import java.math.BigDecimal;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicLong;

import simple.app.AbstractWorkQueue;
import simple.app.BasicQoS;
import simple.app.Publisher;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.ByteInput;
import simple.io.Log;
import simple.mq.app.AnyTwoInstanceWorkQueue;
import simple.translator.Translator;
import simple.translator.TranslatorFactory;
import simple.util.concurrent.WaitForUpdateFuture;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.MessageChainV11;
import com.usatech.app.MixedValue;
import com.usatech.ec.ECResponse;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.MessageProcessingUtils;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.DeviceInfoProperty;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.MessageAttrEnum;
import com.usatech.layers.common.constants.PaymentMaskBRef;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageDataFactory;
import com.usatech.layers.common.messagedata.MessageData_0216;
import com.usatech.layers.common.messagedata.MessageDirection;
import com.usatech.layers.common.messagedata.WS2AuthorizeResponseMessageData;

public class ReplenishPrepaidTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected static final String GLOBAL_SESSION_CODE_PREFIX = "P:" + ManagementFactory.getRuntimeMXBean().getName() + ':' + Long.toHexString(System.currentTimeMillis()).toUpperCase() + ':';
	protected static final AtomicLong sessionIDGenerator = new AtomicLong(Double.doubleToLongBits(Math.random()));
	protected static final byte[] DUMMY_ENCRYPTED_DATA = new byte[1];
	protected int replenishItemId = 552;
	protected String virtualDeviceSerialCd = "V1-1";
	protected String replenishQueueKey = "usat.inbound.message.auth";
	protected String retrieveQueueKey = "usat.keymanager.retrieve";
	protected String lookupQueueKey = "usat.keymanager.account.lookup";
	protected int maxInputSize = 2048;
	protected final ThreadLocal<ByteBuffer> byteBufferLocal = new ThreadLocal<ByteBuffer>() {
		@Override
		protected ByteBuffer initialValue() {
			return ByteBuffer.allocate(maxInputSize);
		}
	};
	protected final Map<Long, WaitForUpdateFuture<MessageData>> sessions = new ConcurrentHashMap<Long, WaitForUpdateFuture<MessageData>>();
	protected AbstractWorkQueue<ByteInput> responseQueue;
	protected final BasicQoS qos = new BasicQoS(15000, BasicQoS.DEFAULT_PRIORITY, true, BasicQoS.EMPTY_MESSAGE_PROPERTIES);
	protected int minorCurrencyFactor = 100;
	protected int maxReplenishDenied = 5;
	protected TranslatorFactory translatorFactory;
	protected int appInstance;

	public int getAppInstance() {
		return appInstance;
	}

	public void setAppInstance(int appInstance) {
		this.appInstance = appInstance;
	}

	protected Translator getTranslator() throws ServiceException {
		TranslatorFactory tf = getTranslatorFactory();
		if(tf == null)
			tf = TranslatorFactory.getDefaultFactory();
		return tf.getTranslator(null, null);
	}

	public void initialize() throws ServiceException {
		if(getResponseQueue() == null)
			throw new ServiceException("responseQueue is not set");
	}
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		final long sessionId = step.getAttributeSafely(CommonAttrEnum.ATTR_SESSION_ID, Long.class, -1L);
		final AuthResultCode authResult = step.getAttributeSafely(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.class, null);
		WaitForUpdateFuture<MessageData> future = sessions.get(sessionId);
		if(future == null) {
			log.warn("Could not find session " + sessionId);
			if(!taskInfo.isRedelivered()) {
				step.setResultAttribute(CommonAttrEnum.ATTR_REPLY_TIME, System.currentTimeMillis());
				step.setResultAttribute(CommonAttrEnum.ATTR_SENT_TO_DEVICE, false);
			}
			if(step.getResultCodes().contains(new Integer(254)) && (authResult == AuthResultCode.APPROVED || authResult == AuthResultCode.PARTIAL || authResult == AuthResultCode.FAILED)) {
				return 254;
			}
			return 255;
		}
		final byte[] data = step.getAttributeSafely(CommonAttrEnum.ATTR_REPLY, byte[].class, null);
		MessageData replyData = null;
		if(data == null || data.length == 0) {
			byte[] replyBytes = step.getAttributeSafely(CommonAttrEnum.ATTR_REPLY_TEMPLATE, byte[].class, null);
			if(replyBytes != null) {
				try {
					replyData = MessageDataFactory.readMessage(ByteBuffer.wrap(replyBytes), MessageDirection.SERVER_TO_CLIENT, DeviceType.VIRTUAL);
					MessageProcessingUtils.populateReplyTemplates(replyData, step.getAttributes(), getTranslator(), DeviceType.VIRTUAL);
				} catch(ParseException e) {
					throw new ServiceException("Invalid reply template", e);
				}
			}
		} else {
			ByteBuffer buffer = ByteBuffer.wrap(data);
			try {
				replyData = MessageDataFactory.readMessage(buffer, MessageDirection.SERVER_TO_CLIENT, DeviceType.VIRTUAL);
			} catch(BufferUnderflowException e) {
				throw new ServiceException("Invalid reply", e);
			}
		}
		future.set(replyData);
		step.setResultAttribute(CommonAttrEnum.ATTR_REPLY_TIME, System.currentTimeMillis());
		step.setResultAttribute(CommonAttrEnum.ATTR_SENT_TO_DEVICE, true);
		if(step.getResultCodes().contains(new Integer(1)) && (authResult == AuthResultCode.APPROVED || authResult == AuthResultCode.PARTIAL || authResult == AuthResultCode.FAILED)) {
			return 1;
		}
		return 0;
	}

	public boolean checkReplenishCard(long consumerAcctId, Connection conn, Map<String, Object> params, Publisher<ByteInput> publisher) throws SQLException, DataLayerException, ConvertException, ServiceException {
		params.clear();
		params.put("consumerAcctId", consumerAcctId);
		params.put("maxDeniedCount", getMaxReplenishDenied());
		log.info("Attempting to replenish prepaid consumer acct " + consumerAcctId);
		DataLayerMgr.executeCall(conn, "REPLENISH_CONSUMER_ACCT", params);
		conn.commit();
		if("Y".equals(ConvertUtils.getString(params.get("replenishFlag"), true))) {
			log.info("Prepaid account balance is less than threshhold for consumer acct " + consumerAcctId + ". Sending replenish charge");
			long replenishDeviceTranCd = ConvertUtils.getLong(params.get("replenishDeviceTranCd"));
			String replenishDeviceSerialCd = ConvertUtils.getString(params.get("replenishDeviceSerialCd"), true);
			String replenishDeviceName = ConvertUtils.getString(params.get("replenishDeviceName"), true);
			BigDecimal replenishAmount = ConvertUtils.convertRequired(BigDecimal.class, params.get("replenishAmount"));
			String replenishCardKey = ConvertUtils.getString(params.get("replenishCardKey"), true);
			long globalAccountId = ConvertUtils.convertRequired(Long.class, params.get("globalAccountId"));
			long consumerId = ConvertUtils.convertRequired(Long.class, params.get("consumerId"));
			AuthResultCode authResult = null;
			try {
				authResult = replenishCard(replenishDeviceTranCd, replenishDeviceName, replenishDeviceSerialCd, globalAccountId, consumerId, replenishCardKey, InteractionUtils.toMinorCurrency(replenishAmount, minorCurrencyFactor), publisher);
			} finally {
				if(authResult == null){
					params.put("authResultCd", '?');
				}else{
					params.put("authResultCd", authResult.getValue());
				}
				DataLayerMgr.executeCall(conn, "UPDATE_PENDING_AUTO_REPLENISH", params);
				conn.commit();
			}
			if(authResult==null){
				log.info("Replenish " + authResult + " for prepaid consumer acct " + consumerAcctId);
				return false;
			}else{
				String replenishCardMasked = ConvertUtils.getString(params.get("replenishCardMasked"), false);
				switch(authResult) {
					case APPROVED:
					case PARTIAL:
					case AVS_MISMATCH:
						log.info("Prepaid consumer acct " + consumerAcctId + " replenished");
						return true;
					case DECLINED:
					case DECLINED_PERMANENT:
					case DECLINED_PAYMENT_METHOD:
					case CVV_MISMATCH:
					case CVV_AND_AVS_MISMATCH:
						InteractionUtils.handleReplenishDeclined(publisher, consumerAcctId, replenishCardMasked, replenishAmount, log);
						int deniedCount = ConvertUtils.getInt(params.get("deniedCount"));
						if(deniedCount >= getMaxReplenishDenied()) {
							// send email
							long replenishId = ConvertUtils.getLong(params.get("replenishId"));
							InteractionUtils.handleReplenishDisabled(publisher, consumerAcctId, replenishId, replenishCardMasked, deniedCount, log);
						}
					default:
						log.info("Replenish " + authResult + " for prepaid consumer acct " + consumerAcctId);
						return false;
				}
			}
		}
		log.info("Prepaid consumer acct " + consumerAcctId + " no longer needs replenishment");
		return false;
	}

	public AuthResultCode replenishCard(long deviceTranCd, String deviceName, String deviceSerialCd, long replenishCardId, long replenishConsumerId, String replenishCardKey, BigDecimal replenishAmount, Publisher<ByteInput> publisher) {
		MessageData_0216 data = new MessageData_0216();
		data.setAmount(replenishAmount.longValue());
		data.setEntryTypeString(String.valueOf(EntryType.MANUAL.getValue()));
		data.setSerialNumber(deviceSerialCd);
		data.setTranId(deviceTranCd);
		data.setReplenishCardId(replenishCardId);
		data.setReplenishConsumerId(replenishConsumerId);
		data.setAttribute("requireCVVMatch", "false");
		data.setAttribute("requireAVSMatch", "false");
		MessageChain messageChain = new MessageChainV11();
		MessageChainStep retrieveStep;
		try {
			retrieveStep = messageChain.addStep(determineRetrieveQueue(replenishCardKey));
		} catch(ServiceException e) {
			log.warn("Could not determine retrieve queue for replenishment", e);
			return AuthResultCode.FAILED;
		}
		retrieveStep.setTemporary(false); // keymgr expects HYBRID distribution
		retrieveStep.setAttribute(AuthorityAttrEnum.ATTR_CARD_KEY, replenishCardKey);
		// <card number>|<card expiration date>|<card security code>|<card holder name>|<card holder zip code>|<card holder address>
		retrieveStep.setAttribute(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT, 4);
		retrieveStep.setAttribute(CommonAttrEnum.ATTR_DECRYPTED_ENCODING, ProcessingConstants.US_ASCII_CHARSET);

		MessageChainStep lookupStep = messageChain.addStep(getLookupQueueKey());
		lookupStep.setTemporary(true); // keymgr expects DIRECT distribution
		retrieveStep.setNextSteps(0, lookupStep);
		retrieveStep.setNextSteps(1, retrieveStep); // if KeyMgr does not have that account

		lookupStep.setReferenceIndexedAttribute(AuthorityAttrEnum.ATTR_ACCOUNT_DATA, retrieveStep, CommonAttrEnum.ATTR_DECRYPTED_DATA, PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER.getStoreIndex());
		MessageChainStep step = messageChain.addStep(getReplenishQueueKey());
		lookupStep.setNextSteps(0, step);
		step.setTemporary(true); // applayer expects DIRECT distribution
		step.setAttribute(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT, 0);
		MixedValue track = step.createMixedAttribute(AuthorityAttrEnum.ATTR_ACCOUNT_DATA);

		track.addIndexedReferencePart(retrieveStep, CommonAttrEnum.ATTR_DECRYPTED_DATA, PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER.getStoreIndex());
		track.addLiteralPart("|");
		track.addIndexedReferencePart(retrieveStep, CommonAttrEnum.ATTR_DECRYPTED_DATA, PaymentMaskBRef.EXPIRATION_DATE.getStoreIndex());
		track.addLiteralPart("|||");
		track.addIndexedReferencePart(retrieveStep, CommonAttrEnum.ATTR_DECRYPTED_DATA, PaymentMaskBRef.ZIP_CODE.getStoreIndex());
		track.addLiteralPart("|");
		track.addIndexedReferencePart(retrieveStep, CommonAttrEnum.ATTR_DECRYPTED_DATA, PaymentMaskBRef.ADDRESS.getStoreIndex());

		step.setAttribute(MessageAttrEnum.ATTR_DEVICE_NAME, deviceName); // required
		step.setAttribute(DeviceInfoProperty.DEVICE_TYPE, 14); // required
		ByteBuffer buffer = byteBufferLocal.get();
		buffer.clear();
		data.writeData(buffer, false);
		buffer.flip();
		step.setAttribute(MessageAttrEnum.ATTR_DATA, buffer); // required
		long sessionStartTime = System.currentTimeMillis();
		step.addLongAttribute("serverTime", sessionStartTime);
		step.addLongAttribute("sessionTimeout", Long.MAX_VALUE - sessionStartTime); // to avoid overflow
		long sessionId = sessionIDGenerator.getAndIncrement();
		String globalSessionCode = GLOBAL_SESSION_CODE_PREFIX + Long.toHexString(sessionId).toUpperCase();
		step.setAttribute(MessageAttrEnum.ATTR_GLOBAL_SESSION_CODE, globalSessionCode); // required
		step.setAttribute(AuthorityAttrEnum.ATTR_CARD_KEY, replenishCardKey); // this will cause chain to skip storing the card info in keymgr all over again.
		step.setReferenceAttribute(AuthorityAttrEnum.ATTR_STORE_EXPIRATION_TIME, retrieveStep, AuthorityAttrEnum.ATTR_STORE_EXPIRATION_TIME); // this will cause chain to skip storing the card info in keymgr all over again.
		
		step.setReferenceAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, lookupStep, AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID);
		step.setReferenceAttribute(AuthorityAttrEnum.ATTR_ACCOUNT_CD_HASH, lookupStep, AuthorityAttrEnum.ATTR_ACCOUNT_CD_HASH);
		step.setReferenceAttribute(AuthorityAttrEnum.ATTR_TRACK_CRC, lookupStep, AuthorityAttrEnum.ATTR_TRACK_CRC);
		step.setReferenceAttribute(AuthorityAttrEnum.ATTR_TRACK_CRC_MATCH, lookupStep, AuthorityAttrEnum.ATTR_TRACK_CRC_MATCH);
		step.setReferenceAttribute(AuthorityAttrEnum.ATTR_INSTANCE, lookupStep, AuthorityAttrEnum.ATTR_INSTANCE);

		MessageChainStep responseStep = messageChain.addStep(getResponseQueue().getQueueKey());
		step.setNextSteps(0, responseStep);
		step.setNextSteps(5, responseStep);
		responseStep.setAttribute(CommonAttrEnum.ATTR_SESSION_ID, sessionId);
		responseStep.addReferenceAttribute("reply", step, "reply");
		responseStep.addReferenceAttribute("replyTemplate", step, "replyTemplate");

		WaitForUpdateFuture<MessageData> future = new WaitForUpdateFuture<MessageData>();
		sessions.put(sessionId, future);
		try {
			MessageChainService.publish(messageChain, publisher, null, qos);
		} catch(ServiceException e) {
			log.warn("Could not publish message chain for replenishment", e);
			return AuthResultCode.FAILED;
		}
		MessageData reply;
		try {
			reply = future.get(getResponseTimeout(), TimeUnit.MILLISECONDS);
		} catch(InterruptedException e) {
			log.warn("Interrupted while waiting for response to replenishment", e);
			return null;
		} catch(ExecutionException e) {
			log.warn("Exception while waiting for response to replenishment", e);
			return null;
		} catch(TimeoutException e) {
			log.warn("Timeout while waiting for response to replenishment", e);
			return null;
		}
		// process reply
		if(reply instanceof WS2AuthorizeResponseMessageData) {
			WS2AuthorizeResponseMessageData replySpec = (WS2AuthorizeResponseMessageData) reply;
			switch(replySpec.getReturnCode()) {
				case ECResponse.RES_APPROVED:
					log.info("Charged auto replenishment card for " + replySpec.getApprovedAmount());
					return AuthResultCode.APPROVED;
				case ECResponse.RES_PARTIALLY_APPROVED:
					log.info("Charged auto replenishment card for " + replySpec.getApprovedAmount());
					return AuthResultCode.PARTIAL;
				case ECResponse.RES_DECLINED:
					log.warn("Auto replenishment was denied: " + replySpec.getReturnMessage());
					return AuthResultCode.DECLINED;
				case ECResponse.RES_FAILED:
					log.warn("Auto replenishment failed: " + replySpec.getReturnMessage());
					return AuthResultCode.FAILED;
				case ECResponse.RES_CVV_MISMATCH:
					log.warn("Auto replenishment didn't match cvv: " + replySpec.getReturnMessage());
					return AuthResultCode.CVV_MISMATCH;
				case ECResponse.RES_AVS_MISMATCH:
					log.warn("Auto replenishment didn't match avs: " + replySpec.getReturnMessage());
					return AuthResultCode.AVS_MISMATCH;
				case ECResponse.RES_CVV_AND_AVS_MISMATCH:
					log.warn("Auto replenishment didn't match cvv and avs: " + replySpec.getReturnMessage());
					return AuthResultCode.CVV_AND_AVS_MISMATCH;
				default:
					log.error("Unexpected auth return code " + replySpec.getReturnCode() + " for auto replenishment");
					return AuthResultCode.FAILED;
			}
		}
		log.error("Unexpected message type returned: " + (reply == null ? null : reply.getMessageType()));
		return AuthResultCode.FAILED;
	}

	protected String determineRetrieveQueue(String cardKey) throws ServiceException {
		if(cardKey.startsWith("A:"))
			return AnyTwoInstanceWorkQueue.constructQueueName(getRetrieveQueueKey(), String.valueOf(getAppInstance()));
		
		Map<String, Long> parsedMap = new HashMap<String, Long>();
		try {
			InteractionUtils.parseCardIdList(cardKey, parsedMap);
		} catch(ParseException e) {
			throw new ServiceException("Could not parse card key '" + cardKey + "'", e);
		}
		switch(parsedMap.size()) {
			case 0: // trouble
				throw new ServiceException("Card key '" + cardKey + "' yield no instances");
			case 1:
				return AnyTwoInstanceWorkQueue.constructQueueName(getRetrieveQueueKey(), parsedMap.keySet().iterator().next());
			default:
				Iterator<String> iter = parsedMap.keySet().iterator();
				return AnyTwoInstanceWorkQueue.constructQueueName(getRetrieveQueueKey(), iter.next(), iter.next());
		}
	}

	public int getReplenishItemId() {
		return replenishItemId;
	}

	public void setReplenishItemId(int replenishItemId) {
		this.replenishItemId = replenishItemId;
	}

	public String getVirtualDeviceSerialCd() {
		return virtualDeviceSerialCd;
	}

	public void setVirtualDeviceSerialCd(String virtualDeviceSerialCd) {
		this.virtualDeviceSerialCd = virtualDeviceSerialCd;
	}

	public String getReplenishQueueKey() {
		return replenishQueueKey;
	}

	public void setReplenishQueueKey(String replenishQueueKey) {
		this.replenishQueueKey = replenishQueueKey;
	}

	public String getRetrieveQueueKey() {
		return retrieveQueueKey;
	}

	public void setRetrieveQueueKey(String retrieveQueueKey) {
		this.retrieveQueueKey = retrieveQueueKey;
	}

	public int getMaxInputSize() {
		return maxInputSize;
	}

	public void setMaxInputSize(int maxInputSize) {
		this.maxInputSize = maxInputSize;
	}

	public long getResponseTimeout() {
		return qos.getTimeToLive();
	}

	public void setResponseTimeout(long responseTimeout) {
		if(responseTimeout <= 0)
			throw new IllegalArgumentException("Response Timeout must be greater than zero");
		qos.setTimeToLive(responseTimeout);
	}

	public AbstractWorkQueue<ByteInput> getResponseQueue() {
		return responseQueue;
	}

	public void setResponseQueue(AbstractWorkQueue<ByteInput> responseQueue) {
		this.responseQueue = responseQueue;
	}

	public int getMinorCurrencyFactor() {
		return minorCurrencyFactor;
	}

	public void setMinorCurrencyFactor(int minorCurrencyFactor) {
		this.minorCurrencyFactor = minorCurrencyFactor;
	}

	public int getMaxReplenishDenied() {
		return maxReplenishDenied;
	}

	public void setMaxReplenishDenied(int maxReplenishDenied) {
		this.maxReplenishDenied = maxReplenishDenied;
	}

	public String getLookupQueueKey() {
		return lookupQueueKey;
	}

	public void setLookupQueueKey(String lookupQueueKey) {
		this.lookupQueueKey = lookupQueueKey;
	}

	public TranslatorFactory getTranslatorFactory() {
		return translatorFactory;
	}

	public void setTranslatorFactory(TranslatorFactory translatorFactory) {
		this.translatorFactory = translatorFactory;
	}
}
