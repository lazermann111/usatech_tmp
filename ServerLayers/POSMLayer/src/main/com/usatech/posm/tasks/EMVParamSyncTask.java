package com.usatech.posm.tasks;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.security.NoSuchAlgorithmException;
import java.sql.Blob;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.constants.FileType;

import simple.app.SelfProcessor;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;
import simple.security.SecureHash;
import simple.text.Hasher;

public class EMVParamSyncTask implements SelfProcessor {
	private static final Log log = Log.getLog();

	private static final String PROCESS_CD = "EMV_PARAM_SYNC";
	private static final String LOCK_PROCESS_TOKEN_CD = ManagementFactory.getRuntimeMXBean().getName() + ':' + Long.toHexString(System.currentTimeMillis()).toUpperCase() + ':' + Long.toHexString(Double.doubleToLongBits(Math.random()));

	private static final String KEY_START = "keyAID=";

	private static final DateFormat df = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS");

	protected final Hasher hasher;

	public EMVParamSyncTask() throws NoSuchAlgorithmException {
		hasher = new Hasher("SHA-256", true);
	}

	@Override
	public void process() throws ServiceException {
		Map<String, Object> params = new HashMap<String, Object>();
		long emvParamSyncId = -1;
		Results results = null;

		try {
			if (!InteractionUtils.lockProcess(PROCESS_CD, LOCK_PROCESS_TOKEN_CD, params)) {
	  		log.info("EMV Param Sync is already locked by instance " + params.get("lockedBy"));
	  		return;
	  	}
		} catch(Exception e) { 
			log.error("Error locking {0}", PROCESS_CD, e);
			return;
		}

		try {
			results = DataLayerMgr.executeQuery("GET_EMV_PARAM_SYNC_ID", params);

			if (results.next()) {
				emvParamSyncId = results.getValue("emvParamSyncId", Long.class);
				if (emvParamSyncId < 0) 
					return;
				// final call so start the process to merge the files
				params = new HashMap<String, Object>();
				DataLayerMgr.executeCall("GET_EMV_AND_INTERAC_FILES_CONTENT", params, false);
				
				Blob emvFileBlob = ConvertUtils.convert(Blob.class, params.get("emvFileContent"));
				if (emvFileBlob == null || emvFileBlob.length() < 1)
					throw new ServiceException("Unable to read EMV Param file content from the database");
				Blob interacFileBlob = ConvertUtils.convert(Blob.class, params.get("interacFileContent"));
				if (interacFileBlob == null || interacFileBlob.length() < 1)
					throw new ServiceException("Unable to read Interac Param file content from the database");

				StringBuilder sb = new StringBuilder();
				appendFromBlob(sb, emvFileBlob);
				appendFromBlob(sb, interacFileBlob);
				
				log.info("CR file: " + sb.toString());
				updateFileTransfer(sb.toString());

				DataLayerMgr.executeUpdate("DELETE_EMV_PARAM_SYNC", new Object[] {emvParamSyncId}, true);
			}
		} catch (DataLayerException | SQLException | ArrayIndexOutOfBoundsException | ConvertException | IOException e) {
			throw new ServiceException(e);
		} finally {
			if (results != null)
				results.close();
			try {
				InteractionUtils.unlockProcess(PROCESS_CD, LOCK_PROCESS_TOKEN_CD, params);
			} catch (Exception e) {
				log.error("Error unlocking {0}", PROCESS_CD, e);
			}
		}
		
		return;
	}
	
	private void appendFromBlob(StringBuilder crFile, Blob fileBlob) throws SQLException {
		int blobLen = (int)fileBlob.length();
		String kvContent = new String(fileBlob.getBytes(1, blobLen));
		byte[] tmp = {'A', '\n', 'B', '\r', '\n', 'C' };
		log.info("ABC: " + (new String(tmp)));
		log.info("File KV: " + kvContent);

		String[] kvLines = kvContent.split("[\\r\\n]");
		for (String kv : kvLines) {
			log.info("KV line: " + kv);
			if (kv == null || kv.trim().isEmpty())
				continue;

			if (kv.startsWith(KEY_START)) {
				if (crFile.length() > 0)
					crFile.append("\r\n");
			} else
				crFile.append('|');
			String[] kvs = kv.split("=", 2);
			if (kvs.length < 2)
				throw new IllegalStateException("Wrong format of EMV Param file line: " + kv);
			crFile.append(kvs[1].trim());
		}
	}

	protected void updateFileTransfer(String content)
			throws SQLException, DataLayerException, IOException, ServiceException {
		String resourceName = "emv-param-dl-" + df.format(new Date());
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("fileTransferName", resourceName + "_CR");
		params.put("fileTransferTypeCd", FileType.CARD_READER_EMV_PARAMETER_FILE);
		params.put("fileContent", content);
		try {
			byte[] contentHash = SecureHash.getUnsaltedHash(content.getBytes());
			params.put("fileContentHash", contentHash);
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException(e);
		}
		params.put("terminalId", 0);
		Object[] result = DataLayerMgr.executeCall("APPLY_FILE_TRANSFER_CONTENT", params, true);
		if (result != null && result.length > 1) {
			log.info("card_reader_emv_param_file_transfer_id: " + result[1]);
		}
		// still keeping the EMV_PARAM_DOWNLOAD_REQD record so there is some history as to what transpired
	}
	
}
