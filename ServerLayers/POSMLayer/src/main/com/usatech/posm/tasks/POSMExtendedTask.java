package com.usatech.posm.tasks;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;

import com.usatech.app.Attribute;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;

/**
 * This is the base class for all the Tasks that are defined in the POSM. Any
 * Task written in POSM has to extend from this Class directly or indirectly.
 * The processInternal(taskInfo) has to be implemented for the business that has
 * to performed by the task. The "enableTaskTiming" property can be configured
 * in the properties file . For example to configure the property for
 * POSMNextTask a property will be defined for the task "POSNNextTask" as below.
 *
 * Eg: simple.app.Service.POSMNextTask.messageChainTask.enableTaskTiming=true
 *
 * @author sjillidimudi
 *
 */
public abstract class POSMExtendedTask extends POSMTask {
	private static final Log log = Log.getLog();
	protected boolean enableTaskTiming = false;
	protected String dataSourceName = "OPER";

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.usatech.app.MessageChainTask#process(com.usatech.app.MessageChainTaskInfo
	 * )
	 */
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		long startTime = System.currentTimeMillis();
		if(enableTaskTiming && log.isDebugEnabled()) {
			log.debug("Starting processing of the Task " + this.getClass().getName());
		}
		// This call executes the "processInternal" method of the implementation
		// class.
		int resultCode = 0;
		resultCode = processInternal(taskInfo);
		logTimings(startTime, System.currentTimeMillis());
		return resultCode;
	}

	// Getters and Setters

	public boolean isEnableTaskTiming() {
		return enableTaskTiming;
	}

	public void setEnableTaskTiming(boolean enableTaskTiming) {
		this.enableTaskTiming = enableTaskTiming;
	}

	/**
	 * This methods logs the time taken to process this task. This method can be
	 * override for custom logging. For example logging to a database.
	 *
	 * @param startTime
	 *            - Start time of the Processing
	 * @param endTime
	 *            - End time of the Processing
	 *
	 */
	protected void logTimings(long startTime, long endTime) {
		if(enableTaskTiming && log.isDebugEnabled()) {
			log.debug("Finised processing of task " + this.getClass().getName() + " in " + (endTime - startTime) + " ms");
		}
	}

	/**
	 * Over-ride this method to provide the necessary behavior when failed to
	 * obtain the connection object
	 *
	 * @param e
	 *            Exception object.
	 */
	protected void notifyConnectionError(Exception e) {
		log.error("Unable to get the Connection from the DataSource " + dataSourceName, e);
	}

	/**
	 * This Method Commits the Operations that are performed by the connection
	 * object (dbConnection).
	 *
	 * @return True in case of Successful Commit.
	 * @throws ServiceException
	 *             Delegates the SQLException in case of any error during the
	 *             Commit process
	 */
	protected boolean commitConnection(Connection dbConnection) throws ServiceException {
		if(dbConnection != null) {
			try {
				dbConnection.commit();
			} catch(SQLException e) {
				throw new ServiceException("Error occurred while committing the transaction. ", e);
			}
		}
		return true;
	}

	/**
	 * This Method RollBacks the Operations that are performed by the connection
	 * Object .
	 *
	 * @return True in case of Successful rollback.
	 * @throws ServiceException
	 *             Delegates the SQLException in case of any error during the
	 *             RollBack process
	 */
	protected boolean rollbackConnection(Connection dbConnection) throws ServiceException {
		if(dbConnection != null) {
			try {
				dbConnection.rollback();
			} catch(SQLException e) {
				log.error("Error occurred while rolling back the transaction. ", e);
				return false;
			}
		}
		return true;
	}

	/**
	 * This Method closes the Connection.
	 *
	 * @return True , if the connection is closed successfully.
	 */
	protected boolean closeConnection(Connection dbConnection) {
		try {
			if(dbConnection != null) {
				dbConnection.close();
			}
		} catch(SQLException e) {
			log.warn("Problem occurred while closing the connection for the DataSource " + getDataSourceName(), e);
		}
		return true;
	}

	/**
	 * This class creates the connection from the Configured DataSource. If
	 * there is no DataSource Configured a connection will be obtained from the
	 * default data source name.
	 *
	 * @throws ServiceException
	 *             When failed to get a connection using the Configured
	 *             DataSource or Default DataSource.
	 */
	protected Connection createConnection() throws ServiceException {
		try {
			return DataLayerMgr.getConnection(getDataSourceName());
		} catch(SQLException e) {
			notifyConnectionError(e);
			throw new ServiceException("Unable to get the Connection from the DataSource " + dataSourceName, e);
		} catch(DataLayerException e) {
			notifyConnectionError(e);
			throw new ServiceException("Unable to get the Connection from the DataSource " + dataSourceName, e);
		}
	}

	// Getters and Setters
	public String getDataSourceName() {
		return dataSourceName;
	}

	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}

	public static <T> T copyAttribute(MessageChainStep step, Map<String, Object> input, Attribute attribute, Class<T> convertTo, boolean required) throws ServiceException {
		return copyAttribute(step, input, attribute, 0, convertTo, required);
	}
	@SuppressWarnings("unchecked")
	public static <T> T copyAttribute(MessageChainStep step, Map<String, Object> input, Attribute attribute, int index, Class<T> convertTo, boolean required) throws ServiceException {
		Object value = input.get(attribute.getValue());
		if(convertTo != null) {
			try {
				value = ConvertUtils.convert(convertTo, value);
			} catch(ConvertException e) {
				throw new ServiceException("Attribute '" + attribute.getValue() + "' could not be converted to " + convertTo.getName(), e);
			}
		}
		if(required && value == null)
			throw new ServiceException("Attribute '" + attribute.getValue() + "' is required but not provided");
		step.setOptionallyIndexedAttribute(attribute, index, value);
		return (T) value;
	}

	public static <T> T copySecretAttribute(MessageChainStep step, Map<String, Object> input, Attribute attribute, boolean required) throws ServiceException {
		return copySecretAttribute(step, input, attribute, 0, required);
	}

	@SuppressWarnings("unchecked")
	public static <T> T copySecretAttribute(MessageChainStep step, Map<String, Object> input, Attribute attribute, int index, boolean required) throws ServiceException {
		Object value = input.get(attribute.getValue());
		String string;
		try {
			string = ConvertUtils.convert(String.class, value);
		} catch(ConvertException e) {
			throw new ServiceException("Attribute '" + attribute.getValue() + "' could not be converted to String", e);
		}
		if(required && value == null)
			throw new ServiceException("Attribute '" + attribute.getValue() + "' is required but not provided");
		step.setOptionallyIndexedSecretAttribute(attribute, index, string);
		return (T) value;
	}
}
