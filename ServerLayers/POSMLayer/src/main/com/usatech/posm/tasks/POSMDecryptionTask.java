package com.usatech.posm.tasks;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.rmi.RemoteException;
import java.util.Map;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.MaskedString;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.keymanager.CryptoException;
import com.usatech.keymanager.KeyManager;
import com.usatech.layers.common.constants.CommonAttrEnum;

/**
 * <p> The MessageChainTask for decryption service. A {@link KeyManager} must be set to this class,
 * it uses the <code>KeyManager</code> to decrypt data. </p>
 * @author Evan, bkrug
 *
 */
public class POSMDecryptionTask extends POSMTask {
    protected KeyManager keyManager;

    /**
     * Process the MessageChainTaskInfo. Uses the KeyManager to decrypt data.
     *
     */
    @Override
    protected int processInternal(MessageChainTaskInfo taskInfo) throws ServiceException {
    	if (keyManager == null) {
            throw new ServiceException("KeyManager property is not set on " + this + "; cannot decrypt data");
        }
    	MessageChainStep step = taskInfo.getStep();
    	int kid;
    	int cnt;
    	String encoding;
    	try {
			kid = step.getAttribute(CommonAttrEnum.ATTR_KEY_ID, Integer.class, true);
			cnt = step.getAttributeDefault(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT, Integer.class, -1);
			encoding = step.getAttribute(CommonAttrEnum.ATTR_DECRYPTED_ENCODING, String.class, false);
		} catch(AttributeConversionException e) {
			throw new ServiceException("Could not convert attributes as required", e);
		}
		Charset charset;
		if(encoding == null || encoding.length() == 0)
			charset = null;
		else
			charset = Charset.forName(encoding);
		Map<String, Object> attributes = step.getAttributes();
		Map<String, Object> results = taskInfo.getStep().getResultAttributes();
        if(cnt > 0) {
			for(int i = 1; i <= cnt; i++) {
				byte[] encrypted;
				try {
					encrypted = ConvertUtils.convertRequired(byte[].class, attributes.get(CommonAttrEnum.ATTR_ENCRYPTED_DATA.getValue() + '.' + i));
				} catch(ConvertException e) {
					throw new ServiceException("Could not convert attributes as required", e);
				}
				byte[] decrypted;
				try {
					decrypted = keyManager.decrypt(kid, encrypted);
				} catch(CryptoException e) {
					throw new ServiceException("Could not decrypt data for kid=" + kid, e);
				} catch(RemoteException e) {
					throw new ServiceException("Could not decrypt data for kid=" + kid, e);
				}
				results.put(CommonAttrEnum.ATTR_DECRYPTED_DATA.getValue() + '.' + i, charset == null ? decrypted : new MaskedString(charset.decode(ByteBuffer.wrap(decrypted)).toString()));
			}
		} else {
			byte[] encrypted;
			try {
				encrypted = ConvertUtils.convertRequired(byte[].class, attributes.get(CommonAttrEnum.ATTR_ENCRYPTED_DATA.getValue()));
			} catch(ConvertException e) {
				throw new ServiceException("Could not convert attributes as required", e);
			}
			byte[] decrypted;
			try {
				decrypted = keyManager.decrypt(kid, encrypted);
			} catch(CryptoException e) {
				throw new ServiceException("Could not decrypt data for kid=" + kid, e);
			} catch(RemoteException e) {
				throw new ServiceException("Could not decrypt data for kid=" + kid, e);
			}
			results.put(CommonAttrEnum.ATTR_DECRYPTED_DATA.getValue(), charset == null ? decrypted : new MaskedString(charset.decode(ByteBuffer.wrap(decrypted)).toString()));
		}
		return 0;
    }

    /**
     * Set the keyManager.
     * @param keyManager <code>KeyManager</code> used to decrypt data
     */
    public void setKeyManager(KeyManager keyManager) {
        this.keyManager = keyManager;
    }
}
