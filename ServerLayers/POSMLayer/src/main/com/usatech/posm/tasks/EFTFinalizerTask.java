/*
 * Copyright USA Technologies Inc. 2018
 */
package com.usatech.posm.tasks;

import java.util.Map;

import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.Log;

public class EFTFinalizerTask implements MessageChainTask {
	private static final Log log = Log.getLog();	

	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		long eftId = -1;
		try {
			Map<String, Object> attributes = taskInfo.getStep().getAttributes();
			eftId = ConvertUtils.convert(long.class, attributes.get("eftId"));
			log.info("Finalizing EFT " + eftId);
			DataLayerMgr.executeUpdate("FINALIZE_EFT", attributes, true);
			log.info("Finished finalizing EFT " + eftId);
		} catch(Exception e) {
			throw new ServiceException("Error finalizing EFT " + eftId, e);
		}
		return 0;
	}
}
