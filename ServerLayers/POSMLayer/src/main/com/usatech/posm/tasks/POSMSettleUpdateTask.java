package com.usatech.posm.tasks;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import java.util.Set;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.LoadDataAttrEnum;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.AbstractDynaBean;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DbUtils;
import simple.io.Log;
import simple.util.CollectionUtils;

public class POSMSettleUpdateTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected static final Set<String> indexedParamNames = CollectionUtils.asSet(LoadDataAttrEnum.ATTR_TRAN_GLOBAL_TRANS_CD.getValue(), AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD.getValue(), LoadDataAttrEnum.ATTR_SETTLE_STATE_ID.getValue(), AuthorityAttrEnum.ATTR_AMOUNT.getValue(), LoadDataAttrEnum.ATTR_TRANS_TYPE_ID.getValue(), AuthorityAttrEnum.ATTR_TRAN_ID.getValue());
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		final MessageChainStep step = taskInfo.getStep();
		class Params extends AbstractDynaBean {
			protected int index;
			protected final Map<String,Object> attributes = step.getAttributes();
			@Override
			protected String getDynaClassName() {
				return getClass().getName();
			}
			@Override
			protected Object getDynaProperty(String name) {
				if(indexedParamNames.contains(name))
					return attributes.get(name + '.' + index);
				return attributes.get(name);
			}
			@Override
			protected boolean hasDynaProperty(String name) {
				if(indexedParamNames.contains(name))
					return attributes.containsKey(name + '.' + index);
				return attributes.containsKey(name);
			}
			@Override
			protected void setDynaProperty(String name, Object value) {
				throw new UnsupportedOperationException("Not modifiable");
			}
		}
		Params params = new Params();
		try {
			int count = step.getAttribute(LoadDataAttrEnum.ATTR_TRAN_COUNT, Integer.class, true);
			Connection conn = DataLayerMgr.getConnectionForCall("UPDATE_SETTLE_STATE");
			boolean okay = false;
			try {
				for(; params.index < count; params.index++) {
					try {
						DataLayerMgr.executeCall(conn, "UPDATE_SETTLE_STATE", params);
					} catch(SQLException e) {
						if(count > 1 && e.getErrorCode() == 20888)
							log.warn("Could not find transaction '" + params.get(LoadDataAttrEnum.ATTR_TRAN_GLOBAL_TRANS_CD.getValue()) + "'; continuing to process the rest of the batch");
						else
							throw e;
					}
					conn.commit(); // smaller transactions is probably better
				}
				okay = true;
			} finally {
				if(!okay)
					DbUtils.rollbackSafely(conn);
				conn.close();
			}
		} catch(SQLException e) {
			if(e.getErrorCode() == 20888)
				throw new RetrySpecifiedServiceException(e, WorkRetryType.SCHEDULED_RETRY);
			throw new ServiceException(e);
		} catch(AttributeConversionException e) {
			throw new RetrySpecifiedServiceException(e, WorkRetryType.NO_RETRY);
		} catch(DataLayerException e) {
			throw new ServiceException(e);
		}
		return 0;
	}

}
