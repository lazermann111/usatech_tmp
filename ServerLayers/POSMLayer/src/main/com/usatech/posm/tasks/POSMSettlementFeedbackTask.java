package com.usatech.posm.tasks;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.DeleteResourceTask;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.LoadDataAttrEnum;
import com.usatech.layers.common.constants.SettlementFeedbackType;
import com.usatech.posm.utils.POSMUtils;

import simple.app.Publisher;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.NotEnoughRowsException;
import simple.io.ByteInput;
import simple.io.InputStreamByteInput;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.results.BeanException;
import simple.results.DatasetHandler;
import simple.results.DatasetUtils;
import simple.results.Results;
import simple.util.CollectionUtils;
import simple.util.CompositeMap;
import simple.util.SimpleHashMap;

public class POSMSettlementFeedbackTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected ResourceFolder resourceFolder;
	protected static final String[] settlementBatchKeys = {
		AuthorityAttrEnum.ATTR_MERCHANT_CD.getValue(),
		AuthorityAttrEnum.ATTR_TERMINAL_CD.getValue(),
		AuthorityAttrEnum.ATTR_TERMINAL_BATCH_NUM.getValue(),
	};
	protected static class SettlementBatchInfo {
		public Object terminalBatchId;
		public Object settlementBatchId;
		public long processedTime;
		public AuthResultCode authResultCode;
	}
	
	protected class BatchFeedbackHandler implements DatasetHandler<SQLException> {
		protected final Map<String,Object> data = new HashMap<String, Object>();
		protected final Connection conn;
		protected final Publisher<ByteInput> publisher;

		public BatchFeedbackHandler(Connection conn, Publisher<ByteInput> publisher) {
			this.conn = conn;
			this.publisher = publisher;
		}

		public void handleRowStart() {
			data.clear();
		}

		public void handleValue(String columnName, Object value) {
			data.put(columnName, value);
		}

		public void handleDatasetEnd() throws SQLException {
			conn.commit();
		}

		public void handleDatasetStart(String[] columnNames) throws SQLException {
			// do nothing
		}

		public void handleRowEnd() throws SQLException {
			if(log.isInfoEnabled())
				log.info("Processing batch feedback with properties=" + data);
			try {
				DataLayerMgr.executeCall(conn, "UPDATE_BATCH_FEEDBACK_BY_REF_CD", data);
				conn.commit(); // no reason we can't commit multiple times
				Long processedTime = ConvertUtils.convert(Long.class, data.get(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS.getValue()));
				if(processedTime == null)
					processedTime = System.currentTimeMillis();
				Object settlementBatchId = data.get(AuthorityAttrEnum.ATTR_SETTLEMENT_BATCH_ID.getValue());
				AuthResultCode authResultCode = ConvertUtils.convertRequired(AuthResultCode.class, data.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
				Results authResults = DataLayerMgr.executeQuery(conn, "GET_AUTHS_IN_BATCH", data);
				data.put("majorCurrencyUsed", true);
				data.put("amount", null);
				Map<String,Object> detailData = new HashMap<String, Object>();
				CompositeMap<String, Object> params = new CompositeMap<String, Object>();
				params.merge(detailData);
				params.merge(data);
				detailData.put("tranTypeCd", '+');
				while(authResults.next()) {
					authResults.fillBean(detailData);
		        	if(log.isInfoEnabled())
						log.info("Updating tran in batch feedback with properties=" + detailData);
					DataLayerMgr.executeCall(conn, "UPDATE_TRAN_FEEDBACK", params, null);
					conn.commit(); // no reason we can't commit multiple times
				}		
				Results refundResults = DataLayerMgr.executeQuery(conn, "GET_REFUNDS_IN_BATCH", data);
		        detailData.clear();
		        detailData.put("tranTypeCd", '-');
				while(refundResults.next()) {
					refundResults.fillBean(detailData);
		        	if(log.isInfoEnabled())
						log.info("Updating tran in batch feedback with properties=" + detailData);
					DataLayerMgr.executeCall(conn, "UPDATE_TRAN_FEEDBACK", params, null);
					conn.commit(); // no reason we can't commit multiple times
				}		
				log.info("Closed settlement batch " + settlementBatchId);
				if(authResultCode != AuthResultCode.PARTIAL) {
					authResults.setRow(0);
					refundResults.setRow(0);
					POSMUtils.publishSettleUpdate(publisher, authResultCode, processedTime, authResults, refundResults);
				}
			} catch(DataLayerException e) {
				throw new SQLException(e);
			} catch(BeanException e) {
				throw new SQLException(e);
			} catch(ServiceException e) {
				throw new SQLException(e);
			} catch(ConvertException e) {
				throw new SQLException(e);
			}
		}
	}
	
	protected class TranFeedbackHandler implements DatasetHandler<SQLException> {
		protected final Map<String,Object> data = new HashMap<String, Object>();
		protected final Map<Map<String,Object>,SettlementBatchInfo> settlementBatchCache = new SimpleHashMap<Map<String,Object>, SettlementBatchInfo>() {
			private static final long serialVersionUID = 897359650361375924L;
			@Override
			protected int hash(Map<String,Object> x) {
				int hc = 1;
				for(String key : settlementBatchKeys) {
					hc = 31 * hc + CollectionUtils.deepHashCode(x.get(key));
				}
				return secondaryHash(hc);
			}
			@Override
			protected boolean keyEquals(Map<String, Object> key1, Map<String, Object> key2) {
				for(String key : settlementBatchKeys) {
					if(!CollectionUtils.deepEquals(key1.get(key), key2.get(key)))
						return false;
				}
				return true;
			}
		};
		protected final Connection conn;
		protected final Publisher<ByteInput> publisher;

		public TranFeedbackHandler(Connection conn, Publisher<ByteInput> publisher) {
			this.conn = conn;
			this.publisher = publisher;
		}

		public void handleRowStart() {
			data.clear();
		}

		public void handleValue(String columnName, Object value) {
			data.put(columnName, value);
		}

		public void handleDatasetEnd() throws SQLException {
			conn.commit();
		}

		public void handleDatasetStart(String[] columnNames) throws SQLException {
			// do nothing
		}

		public void handleRowEnd() throws SQLException {
			if(log.isInfoEnabled())
				log.info("Processing tran feedback with properties=" + data);
			try {
				// Lookup the settlement_batch_id
				SettlementBatchInfo settleBatchInfo = settlementBatchCache.get(data);
				if(settleBatchInfo == null) {
					Map<String, Object> copy = new HashMap<String, Object>(data);
					DataLayerMgr.executeCall(conn, "UPDATE_BATCH_FEEDBACK", data);
					settleBatchInfo = new SettlementBatchInfo();
					settleBatchInfo.terminalBatchId = data.get(AuthorityAttrEnum.ATTR_TERMINAL_BATCH_ID.getValue());
					if(settleBatchInfo.terminalBatchId == null)
						throw new NotEnoughRowsException("Settlement Batch could not be found for " + data);
					settleBatchInfo.settlementBatchId = data.get(AuthorityAttrEnum.ATTR_SETTLEMENT_BATCH_ID.getValue());
					Long processedTime = ConvertUtils.convert(Long.class, data.get(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS.getValue()));
					if(processedTime == null)
						settleBatchInfo.processedTime = System.currentTimeMillis();
					else
						settleBatchInfo.processedTime = processedTime;
					settleBatchInfo.authResultCode = ConvertUtils.convertRequired(AuthResultCode.class, data.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD));
					log.info("Closed settlement batch " + settleBatchInfo.settlementBatchId);
					settlementBatchCache.put(copy, settleBatchInfo);			
				} else {
					data.put(AuthorityAttrEnum.ATTR_TERMINAL_BATCH_ID.getValue(), settleBatchInfo.terminalBatchId);
					data.put(AuthorityAttrEnum.ATTR_SETTLEMENT_BATCH_ID.getValue(), settleBatchInfo.settlementBatchId);
				}
				// Update the tran
				DataLayerMgr.executeCall(conn, "UPDATE_TRAN_FEEDBACK", data);
				boolean settleUpdateNeeded = !"N".equalsIgnoreCase(ConvertUtils.getString(data.get(LoadDataAttrEnum.ATTR_SETTLE_UPDATE_NEEDED.getValue()), false));
				if(settleBatchInfo.authResultCode != AuthResultCode.PARTIAL && settleUpdateNeeded) {
					char authTypeCd = ConvertUtils.getChar(data.get(AuthorityAttrEnum.ATTR_AUTH_TYPE_CD.getValue()));
					String globalTransCd = ConvertUtils.getString(data.get(LoadDataAttrEnum.ATTR_TRAN_GLOBAL_TRANS_CD.getValue()), true);
					String authorityTranCd = ConvertUtils.getString(data.get(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD.getValue()), false);
					Number amount = ConvertUtils.convert(Number.class, data.get(AuthorityAttrEnum.ATTR_AMOUNT.getValue()));
					Integer overrideTransTypeId = ConvertUtils.convert(Integer.class, data.get(LoadDataAttrEnum.ATTR_TRANS_TYPE_ID.getValue()));
					Long tranId = ConvertUtils.convertSafely(Long.class, data.get(AuthorityAttrEnum.ATTR_TRAN_ID.getValue()), null);
					POSMUtils.publishSettleUpdate(publisher, globalTransCd, settleBatchInfo.authResultCode, settleBatchInfo.processedTime, authorityTranCd, "PSS", authTypeCd, amount, overrideTransTypeId, tranId);
				}
			} catch(DataLayerException e) {
				throw new SQLException(e);
			} catch(ConvertException e) {
				throw new SQLException(e);
			} catch(ServiceException e) {
				throw new SQLException(e);
			}
		}
	}

	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		ResourceFolder rf = getResourceFolder();
		if(rf == null)
			throw new RetrySpecifiedServiceException("ResourceFolder is not set", WorkRetryType.NONBLOCKING_RETRY, true);
		MessageChainStep step = taskInfo.getStep();
		String resourceKey;
		SettlementFeedbackType feedbackType;
		try {
			resourceKey = step.getAttribute(AuthorityAttrEnum.ATTR_FEEDBACK_RESOURCE, String.class, true);
			feedbackType = step.getAttribute(AuthorityAttrEnum.ATTR_FEEDBACK_TYPE, SettlementFeedbackType.class, true);
		} catch(AttributeConversionException e) {
			throw new RetrySpecifiedServiceException("Could not convert attribute", e, WorkRetryType.NO_RETRY);
		}
		Resource resource;
		try {
			resource = rf.getResource(resourceKey, ResourceMode.READ);
		} catch(IOException e) {
			throw new RetrySpecifiedServiceException("Could not get feedback resource", e, WorkRetryType.NONBLOCKING_RETRY, true);
		}
		try {
			InputStream in = resource.getInputStream();
			try {
				Connection conn = DataLayerMgr.getConnectionForCall("UPDATE_TRAN_FEEDBACK");
				boolean okay = false;
				try {
					DatasetHandler<SQLException> handler;
					switch(feedbackType) {
						case BATCH:
							handler = new BatchFeedbackHandler(conn, taskInfo.getPublisher());
							break;
						case TRAN:
							handler = new TranFeedbackHandler(conn, taskInfo.getPublisher());
							break;
						default:
							throw new RetrySpecifiedServiceException("Unsupported feedback type '" + feedbackType + "'", WorkRetryType.NO_RETRY);
					}
					DatasetUtils.parseDataset(new InputStreamByteInput(in), handler);
					okay = true;
				} finally {
					if(!okay)
						try {
							conn.rollback();
						} catch(SQLException e) {
							log.warn("Could not rollback connection", e);
						}
					conn.close();
				}
			} catch(SQLException e) {
				if(e.getErrorCode() == 20100) 
					log.info("Settlement batch in feedback file was already processed");
				else
					throw new ServiceException("Could not process feedback file", e);
			} catch(DataLayerException e) {
				throw new ServiceException("Could not process feedback file", e);
			} finally {
				in.close();
			}
			DeleteResourceTask.requestResourceDeletion(resource, taskInfo.getPublisher());
		} catch(IOException e) {
			throw new ServiceException("Could not process feedback file", e);
		} finally {
			resource.release();
		}
		return 0;
	}
	
	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}
}
