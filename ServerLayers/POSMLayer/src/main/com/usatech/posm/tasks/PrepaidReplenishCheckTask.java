package com.usatech.posm.tasks;


import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import simple.app.Publisher;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.ByteInput;
import simple.io.Log;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.AuthorityAttrEnum;

/**
 * Checks if one prepaid account need replenishment and calls replenish
 * 
 * @author bkrug
 * 
 */
public class PrepaidReplenishCheckTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected ReplenishPrepaidTask replenishPrepaidTask;
	protected Publisher<ByteInput> publisher;
	
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		long consumerAcctId;
		try {
			consumerAcctId = step.getAttribute(AuthorityAttrEnum.ATTR_CONSUMER_ACCT_ID, Long.class, true);
		} catch(AttributeConversionException e) {
			throw new RetrySpecifiedServiceException(e, WorkRetryType.NO_RETRY);
		}
		try {
			Connection conn = DataLayerMgr.getConnectionForCall("FIND_REPLENISH_NEEDED");
			try {
				Map<String, Object> params = new HashMap<String, Object>();
				try {
					replenishPrepaidTask.checkReplenishCard(consumerAcctId, conn, params, getPublisher());
				} catch(SQLException e) {
					log.warn("Error while replenishing prepaid card", e);
					try {
						conn.rollback();
					} catch(SQLException e1) {
						log.warn("Could not rollback prepaid card replenishment", e1);
					}
				} catch(ConvertException e) {
					log.warn("Error while replenishing prepaid card", e);
					try {
						conn.rollback();
					} catch(SQLException e1) {
						log.warn("Could not rollback prepaid card replenishment", e1);
					}
				} catch(DataLayerException e) {
					log.warn("Error while replenishing prepaid card", e);
					try {
						conn.rollback();
					} catch(SQLException e1) {
						log.warn("Could not rollback prepaid card replenishment", e1);
					}
				}
				conn.commit();
			} finally {
				conn.close();
			}
		} catch(SQLException e) {
			log.warn("Could not find accounts to replenish", e);
		} catch(DataLayerException e) {
			log.warn("Could not find accounts to replenish", e);
		}
		return 0;
	}

	public ReplenishPrepaidTask getReplenishPrepaidTask() {
		return replenishPrepaidTask;
	}

	public void setReplenishPrepaidTask(ReplenishPrepaidTask replenishPrepaidTask) {
		this.replenishPrepaidTask = replenishPrepaidTask;
	}

	public Publisher<ByteInput> getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}
}
