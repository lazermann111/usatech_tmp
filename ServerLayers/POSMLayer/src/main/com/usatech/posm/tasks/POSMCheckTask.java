package com.usatech.posm.tasks;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.posm.utils.POSMUtils;

/**
 * This class process the messages that are posted by the APP Layer to the
 * "usat.posm.terminal.check" Queue. This task locks the terminal with the
 * information provided in the message , generates the unique GLOBAL TOKEN and
 * updates to the Terminal table. In case , if the terminal already contains a
 * global token ,the lock will be released and completes the message processing
 * successfully.
 *
 * Any error during the complete operation will throw a ServiceException , which
 * rollback the message transaction and the message will be re-delivered for
 * processing.
 *
 * @author bkrug
 *
 */
public class POSMCheckTask extends POSMExtendedTask {
	private static final Log log = Log.getLog();
	protected static final double millisInDay = 24 * 60 * 60 * 1000;
	protected String posmNextQueue;
	protected long settlementRetryIntervalMillis = 60 * 60 * 1000;
	protected long tranRetryIntervalMillis = 60 * 60 * 1000;
	protected int maxSettlements = 50;
	protected int maxTrans = 200;
	protected Map<String, String> posmNextPsQueues = new HashMap<>();

	/**
	 * This method process all the messages that are posted by the APP layer to
	 * the "usat.posm.terminal.check" . The input from the message to this task
	 * is the TerminalId and the paymentSubtypeKey . A lock will be acquired
	 * with the input information and a unique global token is generated and
	 * updated to the GLOBAL_TOKEN_CD column of the Terminal table for the
	 * corresponding TerminalID. Once the token is updated , the lock will be
	 * released.
	 *
	 * @exception ServiceException
	 *                will be thrown
	 *
	 */
	@Override
	protected int processInternal(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		long paymentSubtypeKeyId;
		String paymentSubtypeClass;
		String oldGlobalTokenCode;
		try {
			paymentSubtypeKeyId = step.getAttribute(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_KEY_ID, long.class, true);
			paymentSubtypeClass = step.getAttribute(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_CLASS, String.class, true);
			oldGlobalTokenCode = step.getAttribute(AuthorityAttrEnum.ATTR_GLOBAL_TOKEN_CD, String.class, false);
		} catch(AttributeConversionException e) {
			throw new ServiceException("Could not convert attributes", e);
		}
		//String lockString = paymentSubTypeKeyId + "|" + paymentSubTypeClass;
		Connection conn = createConnection();
		// Get the Lock for the given input information.
		boolean okay = false;
		try {
			boolean again;
			do {
				again = false;
				String newGlobalTokenCode = POSMUtils.generateGlobalToken();
				boolean hasToken = POSMUtils.obtainGlobalToken(conn, paymentSubtypeKeyId, paymentSubtypeClass, oldGlobalTokenCode, newGlobalTokenCode, false);
				if(hasToken) {
					commitConnection(conn);
					if(log.isInfoEnabled()) {
						if(oldGlobalTokenCode == null)
							log.info("Locked Terminal " + paymentSubtypeKeyId + "(" + paymentSubtypeClass + ") with global token code '" + newGlobalTokenCode + "'");
						else
							log.info("Renewed lock on Terminal " + paymentSubtypeKeyId + "(" + paymentSubtypeClass + ") from global token code '" + oldGlobalTokenCode + "' to '" + newGlobalTokenCode + "'");
					}
					// Check if terminal needs processing
					Map<String, Object> params = new HashMap<String, Object>();
					params.put(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_KEY_ID.getValue(), paymentSubtypeKeyId);
					params.put(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_CLASS.getValue(), paymentSubtypeClass);
					params.put("settlementRetryInterval", getSettlementRetryIntervalMillis() / millisInDay);
					params.put("tranRetryInterval", getTranRetryIntervalMillis() / millisInDay);
					params.put("maxSettlements", getMaxSettlements());
					params.put("maxTrans", getMaxTrans());
					try {
						DataLayerMgr.executeCall(conn, "GET_PENDING_ACTIONS", params);
					} catch(SQLException e) {
						throw new ServiceException("Could not check terminal actions in database", e);
					} catch(DataLayerException e) {
						throw new ServiceException("Could not check terminal actions in database", e);
					}
					int terminalStateId;
					long[] pendingTerminalBatchIds;
					long[] pendingTranIds;
					Long terminalBatchId;
					boolean callAgainFlag;
					try {
						terminalStateId = ConvertUtils.getInt(params.get("terminalStateId"));
						pendingTerminalBatchIds = ConvertUtils.convert(long[].class, params.get("pendingTerminalBatchIds"));
						pendingTranIds = ConvertUtils.convert(long[].class, params.get("pendingTranIds"));
						terminalBatchId = ConvertUtils.convert(Long.class, params.get("terminalBatchId"));
						callAgainFlag = ConvertUtils.getBooleanSafely(params.get("callAgainFlag"), false);
					} catch(ConvertException e) {
						throw new ServiceException("Could not convert terminal actions attributes", e);
					}
					boolean sendToNext;
					switch(terminalStateId) {
						case 1: // Active
							if(log.isInfoEnabled())
								log.error("Terminal " + paymentSubtypeKeyId + "(" + paymentSubtypeClass + ") is not locked and cannot be processed. This should not occur.");
							sendToNext = false;
							break;
						case 3:
							if(pendingTerminalBatchIds != null && pendingTerminalBatchIds.length > 0) {
								if(log.isInfoEnabled())
									log.info("Sending Terminal " + paymentSubtypeKeyId + "(" + paymentSubtypeClass + ") to POSM Next Task to process " + pendingTerminalBatchIds.length + " settlements.");
								sendToNext = true;
							} else if(pendingTranIds != null && pendingTranIds.length > 0) {
								if(log.isInfoEnabled())
									log.info("Sending Terminal " + paymentSubtypeKeyId + "(" + paymentSubtypeClass + ") to POSM Next Task to process " + pendingTranIds.length + " transactions.");
								sendToNext = true;
							} else {
								if(log.isInfoEnabled())
									log.info("For Terminal " + paymentSubtypeKeyId + "(" + paymentSubtypeClass + ") no POSM Next Task processing needed at this time.");
								sendToNext = false;
							}
							break;
						case 2: // Inactive
							if(log.isInfoEnabled())
								log.info("Terminal " + paymentSubtypeKeyId + "(" + paymentSubtypeClass + ") is disabled. No POSM Next Task processing can be performed.");
							sendToNext = false;
							break;
						case 4:
							if(log.isInfoEnabled())
								log.info("Terminal " + paymentSubtypeKeyId + "(" + paymentSubtypeClass + ") is busy. No POSM Next Task processing can be performed.");
							sendToNext = false;
							break;
						case 5:
							if(pendingTerminalBatchIds != null && pendingTerminalBatchIds.length > 0) {
								if(log.isInfoEnabled())
									log.info("Sending Terminal " + paymentSubtypeKeyId + "(" + paymentSubtypeClass + ") to POSM Next Task to process " + pendingTerminalBatchIds.length + " settlement retries.");
								sendToNext = true;
							} else {
								if(log.isInfoEnabled())
									log.info("Terminal " + paymentSubtypeKeyId + "(" + paymentSubtypeClass + ") is waiting for a settlement retry that is not yet due. No POSM Next Task processing can be performed.");
								sendToNext = false;
							}
							break;
						default:
							throw new ServiceException("Unknown terminal state " + terminalStateId + " for Terminal " + paymentSubtypeKeyId + "(" + paymentSubtypeClass + ")");
					}
					if(sendToNext) {
						MessageChain messageChain = new MessageChainV11();
						MessageChainStep nextStep = messageChain.addStep(getPosmNextQueue(paymentSubtypeClass));
						nextStep.setAttribute(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_KEY_ID, paymentSubtypeKeyId);
						nextStep.setAttribute(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_CLASS, paymentSubtypeClass);
						nextStep.setAttribute(AuthorityAttrEnum.ATTR_GLOBAL_TOKEN_CD, newGlobalTokenCode);
						nextStep.setAttribute(AuthorityAttrEnum.ATTR_TERMINAL_BATCH_IDS, pendingTerminalBatchIds);
						nextStep.setAttribute(AuthorityAttrEnum.ATTR_TRAN_IDS, pendingTranIds);
						nextStep.setAttribute(AuthorityAttrEnum.ATTR_TERMINAL_BATCH_ID, terminalBatchId);

						// If the NextStep ends up doing nothing (result code 5), we must have it send back to this Step so we can release the global token
						MessageChainStep checkStep = messageChain.addStep(step.getQueueKey());
						checkStep.setAttribute(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_KEY_ID, paymentSubtypeKeyId);
						checkStep.setAttribute(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_CLASS, paymentSubtypeClass);
						checkStep.setAttribute(AuthorityAttrEnum.ATTR_GLOBAL_TOKEN_CD, newGlobalTokenCode);
						nextStep.setNextSteps(5, checkStep);

						MessageChainService.publish(messageChain, taskInfo.getPublisher());
					} else if(callAgainFlag) {
						if(log.isInfoEnabled())
							log.info("More to do for Terminal " + paymentSubtypeKeyId + "(" + paymentSubtypeClass + "); running check again.");
						oldGlobalTokenCode = newGlobalTokenCode;
						again = callAgainFlag;
					} else {
						POSMUtils.releaseGlobalToken(conn, paymentSubtypeKeyId, paymentSubtypeClass, newGlobalTokenCode);
						if(log.isInfoEnabled())
							log.info("Released Terminal " + paymentSubtypeKeyId + "(" + paymentSubtypeClass + ") by resetting the global token.");
					}
				} else {
					if(log.isInfoEnabled())
						log.info("Terminal " + paymentSubtypeKeyId + "(" + paymentSubtypeClass + ") is already in process.");
				}
				commitConnection(conn);
			} while(again);
			okay = true;
		} finally {
			//unlockTerminal(dbConnection, lockHandle, paymentSubTypeKeyId, paymentSubTypeClass);
			if(!okay)
				try {
					rollbackConnection(conn);
				} catch(ServiceException e) {
					//ignore
				}
			closeConnection(conn);
		}

		return 0;
	}

	/**
	 *
	 * @param posmNextTaskQueue
	 *            posmNextTaskQueue to set
	 */
	public void setPosmNextQueue(String posmNextTaskQueue) {
		this.posmNextQueue = posmNextTaskQueue;
	}

	/**
	 *
	 * @return posmNextTaskQueue
	 */
	public String getPosmNextQueue() {
		return posmNextQueue;
	}
	
	public String getPosmNextQueue(String paymentSubtypeClass) {
		return posmNextPsQueues.getOrDefault(paymentSubtypeClass, posmNextQueue);
	}

	public String getPosmNextPsQueue(String paymentSubtypeClass) {
		return posmNextPsQueues.get(paymentSubtypeClass);
	}

	public void setPosmNextPsQueue(String paymentSubtypeClass, String queueName) {
		posmNextPsQueues.put(paymentSubtypeClass, queueName);
	}
	
	public long getSettlementRetryIntervalMillis() {
		return settlementRetryIntervalMillis;
	}

	public void setSettlementRetryIntervalMillis(long settlementRetryIntervalMillis) {
		this.settlementRetryIntervalMillis = settlementRetryIntervalMillis;
	}

	public long getTranRetryIntervalMillis() {
		return tranRetryIntervalMillis;
	}

	public void setTranRetryIntervalMillis(long tranRetryIntervalMillis) {
		this.tranRetryIntervalMillis = tranRetryIntervalMillis;
	}

	public int getMaxSettlements() {
		return maxSettlements;
	}

	public void setMaxSettlements(int maxSettlements) {
		this.maxSettlements = maxSettlements;
	}

	public int getMaxTrans() {
		return maxTrans;
	}

	public void setMaxTrans(int maxTrans) {
		this.maxTrans = maxTrans;
	}
}
