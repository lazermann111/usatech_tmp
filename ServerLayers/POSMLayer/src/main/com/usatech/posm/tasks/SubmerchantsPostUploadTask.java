package com.usatech.posm.tasks;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import simple.app.ServiceException;
import simple.db.DataLayerMgr;
import simple.io.Log;

import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.posm.schedule.SubmerchantsUploadJob;
import com.usatech.posm.schedule.SubmerchantsUploadJob.MatchStatusCode;
import com.usatech.posm.utils.POSMUtils;

public class SubmerchantsPostUploadTask implements MessageChainTask {
	
	private static final Log log = Log.getLog();

	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		int resultCode = step.getPreviousStep().getResultCode();
		
		String fileName = null;
		Date uploadTime = null;
		boolean isFull = false;
		int recordCount = -1;
		
		try {
			fileName = step.getAttribute(AuthorityAttrEnum.ATTR_SUBMERCHANTS_UPLOAD_FILENAME, String.class, true);
			uploadTime = step.getAttribute(AuthorityAttrEnum.ATTR_SUBMERCHANTS_UPLOAD_TS, Date.class, true);
			isFull = step.getAttribute(AuthorityAttrEnum.ATTR_SUBMERCHANTS_UPLOAD_FULL, Boolean.class, true);
			recordCount = step.getAttribute(AuthorityAttrEnum.ATTR_SUBMERCHANTS_UPLOAD_RECORD_COUNT, Integer.class, true);
		} catch (Exception e) {
			log.error("Failed to get required attributes, will not retry...", e);
//		throw new RetrySpecifiedServiceException(e, WorkRetryType.NO_RETRY);
			return 0;
		}

		try {
			if (resultCode != 0) {
				log.info("Failed to upload {0} submerchants file {1} at {2} with result code {3}, will not retry...", (isFull ? "FULL" : "CHANGES-ONLY"), fileName, uploadTime, resultCode);
			} else {
				log.info("Uploaded {0} submerchants file {1} successfully at {2}", (isFull ? "FULL" : "CHANGES-ONLY"), fileName, uploadTime);
				if (isFull) {
					POSMUtils.setAppSetting(SubmerchantsUploadJob.LAST_FULL_UPLOAD_KEY, uploadTime.getTime());
				} else {
					POSMUtils.setAppSetting(SubmerchantsUploadJob.LAST_CHANGES_UPLOAD_KEY, uploadTime.getTime());
				}
			}
			
			Map<String, Object> submerchantsParams = new HashMap<String, Object>();
			submerchantsParams.put("SUBMERCHANTS_FILE_NAME", fileName);
			submerchantsParams.put("FILE_CONTENT_TYPE", (isFull ? "F" : "C"));
			submerchantsParams.put("SEND_RECORD_COUNT", recordCount);
			submerchantsParams.put("SEND_TS", uploadTime);
			submerchantsParams.put("SEND_RESULT_CD", resultCode);
			
			DataLayerMgr.executeUpdate("INSERT_SUBMERCHANTS_FILE", submerchantsParams, true);

			Map<String, Object> matchParams = new HashMap<String, Object>();
			matchParams.put("MATCH_STATUS_CD", MatchStatusCode.SENT.getCode());
			matchParams.put("SENT_TS", uploadTime);
			matchParams.put("SUBMERCHANTS_FILE_NAME", fileName);
			
			DataLayerMgr.executeUpdate("UPDATE_MATCH_STATUS_BY_FILE_NAME", matchParams, true);
		} catch (Exception e) {
			log.info("Failed to update results of successfull upload of {0} submerchants file {1} at {2}, will retry...", (isFull ? "FULL" : "CHANGES-ONLY"), fileName, uploadTime);
			throw new ServiceException(e);
		}

		return 0;
	}
}
