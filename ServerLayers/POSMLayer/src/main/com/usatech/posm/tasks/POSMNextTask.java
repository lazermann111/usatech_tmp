package com.usatech.posm.tasks;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.NotEnoughRowsException;
import simple.io.ByteOutput;
import simple.io.Log;
import simple.io.OutputStreamByteOutput;
import simple.io.PassThroughOutputStream;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.lang.SystemUtils;
import simple.mq.app.AnyTwoInstanceWorkQueue;
import simple.results.BeanException;
import simple.results.DatasetUtils;
import simple.results.Results;
import simple.text.StringUtils;

import com.usatech.app.Attribute;
import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.Cryption;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAction;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.AuthorityServiceTypeCode;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.MessageAttrEnum;
import com.usatech.layers.common.constants.PaymentMaskBRef;
import com.usatech.layers.common.util.Tallier;
import com.usatech.posm.constants.SalePhase;
import com.usatech.posm.utils.POSMUtils;

/**
 * The Task is to validate the Global Token and identify the work that is due
 * for processing for this terminal. Construct the Message Chains and post them
 * to the respective Authority Queues and unlocks the terminal +
 * paymentSubTypeClass lock .
 *
 * In case none of the operation is due for processing , reset the global token
 * and release the lock .
 *
 * @author Brian S. Krug
 *
 */
public class POSMNextTask extends POSMExtendedTask {
	private static final Log log = Log.getLog();
	
	//protected static final String[] sensitiveDecryptColumnNames = { CommonAttrEnum.ATTR_KEY_ID.getValue(), CommonAttrEnum.ATTR_ENCRYPTED_DATA.getValue() + ".1", CommonAttrEnum.ATTR_ENCRYPTED_DATA.getValue() + ".2" };
	protected static final String[] sensitiveRetrieveColumnNames = { AuthorityAttrEnum.ATTR_ENCRYPT_KEY_LIST.getValue() };
	protected String massDecryptQueue;
	protected String massRetrieveQueue;
	protected String singleRetrieveQueue;

	protected String authorityQueueKeyPrefix = "usat.authority.";
	protected ResourceFolder resourceFolder;
	protected final Cryption authorityCryption = new Cryption();
	
	protected String settlementUpdateQueue;
	protected int settlementResponseTimeOut;
	protected int settlementRetryAttempts;
	protected int refundRetryAttempts;
	protected int saleResponseTimeOut; // Response Time Out for the Authority While performing the
	protected String saleUpdateQueue;
	protected int saleRetryAttempts;
	protected int appInstance;
	protected final Map<String, Integer> bulkSaleProcessingLimits = new HashMap<>();
	protected final Set<String> keymgrInstances = new LinkedHashSet<>();

	protected class SaleMessageChain {
		protected final MessageChain messageChain = new MessageChainV11();
		protected final String authorityQueueKey;
		protected MessageChainStep authorityStep;
		protected MessageChainStep retrieveStep;
		protected MessageChainStep saleUpdateStep;
		protected final Tallier<String> retrieveQueues = new Tallier<String>(String.class);

		public SaleMessageChain(String paymentSubTypeClass) {
			this.authorityQueueKey = paymentSubTypeClass.replace("::", "_").toLowerCase();
		}

		public MessageChainStep getOrCreateAuthorityStep() {
			if(authorityStep == null) {
				authorityStep = messageChain.addStep(getAuthorityQueueKeyPrefix() + authorityQueueKey);
				getOrCreateSaleUpdateStep();
				authorityStep.setNextSteps(0, saleUpdateStep);
				authorityStep.setNextSteps(1, saleUpdateStep);
				authorityStep.setNextSteps(2, saleUpdateStep);
				if(retrieveStep == null)
					messageChain.setCurrentStep(authorityStep);
			}
			return authorityStep;
		}

		public MessageChainStep getOrCreateRetrieveStep() {
			if(retrieveStep == null) {
				retrieveStep = messageChain.addStep("");
				retrieveStep.setNextSteps(0, getOrCreateAuthorityStep());
				retrieveStep.setNextSteps(1, retrieveStep);
				messageChain.setCurrentStep(retrieveStep);
			}
			return retrieveStep;
		}

		public MessageChainStep getOrCreateSaleUpdateStep() {
			if(saleUpdateStep == null)
				saleUpdateStep = messageChain.addStep(getSaleUpdateQueue());
			return saleUpdateStep;
		}

		public MessageChain getMessageChain() {
			return messageChain;
		}

		public void voteForRetrieve(Collection<String> instances) {
			retrieveQueues.vote(instances);
		}

		public void voteForRetrieve(String instance) {
			retrieveQueues.vote(instance);
		}

		public void prepareForPublish(int maxIndex, long paymentSubtypeKeyId, String paymentSubtypeClass, String globalTokenCode) {
			if(retrieveStep != null) {
				if(maxIndex > 0)
					retrieveStep.setAttribute(CommonAttrEnum.ATTR_ENTRY_COUNT, maxIndex);
				if(retrieveQueues.getInstanceCount() == 0)
					retrieveStep.setQueueKey(AnyTwoInstanceWorkQueue.constructQueueNameAny(getSingleRetrieveQueue()));
				else
					retrieveStep.setQueueKey(AnyTwoInstanceWorkQueue.constructQueueName(getSingleRetrieveQueue(), retrieveQueues.getWinners(2)));
			}
			if(authorityStep != null) {
				authorityStep.setAttribute(AuthorityAttrEnum.ATTR_RESPONSE_TIME_OUT, getSaleResponseTimeOut());
				if(maxIndex > 0)
					authorityStep.setAttribute(CommonAttrEnum.ATTR_ENTRY_COUNT, maxIndex);
			}
			if(saleUpdateStep != null) {
				if(maxIndex > 0)
					saleUpdateStep.setAttribute(CommonAttrEnum.ATTR_ENTRY_COUNT, maxIndex);
				saleUpdateStep.setAttribute(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_KEY_ID, paymentSubtypeKeyId);
				saleUpdateStep.setAttribute(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_CLASS, paymentSubtypeClass);
				saleUpdateStep.setAttribute(AuthorityAttrEnum.ATTR_GLOBAL_TOKEN_CD, globalTokenCode);
			}
		}
	}
	/**
	 * Step 1 : Check for the settlements that are due for processing and
	 * release the DB Lock in case of any settlements are processed and return.
	 * Step 2 : Check for any transactions ( Sale / Refund ) that are due for
	 * processing and release the DB Lock in case of any transaction is
	 * processed and return . Step 3 : If no work is identified , reset the
	 * global token to NULL and release the database lock.
	 *
	 */
	@Override
	protected int processInternal(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		String oldGlobalTokenCode;
		long paymentSubtypeKeyId;
		String paymentSubtypeClass;
		long[] pendingTerminalBatchIds;
		long[] pendingTranIds;
		Long terminalBatchId;
		try {
			oldGlobalTokenCode = step.getAttribute(AuthorityAttrEnum.ATTR_GLOBAL_TOKEN_CD, String.class, true);
			paymentSubtypeKeyId = step.getAttribute(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_KEY_ID, Long.class, true);
			paymentSubtypeClass = step.getAttribute(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_CLASS, String.class, true);
			pendingTerminalBatchIds = step.getAttribute(AuthorityAttrEnum.ATTR_TERMINAL_BATCH_IDS, long[].class, false);
			pendingTranIds = step.getAttribute(AuthorityAttrEnum.ATTR_TRAN_IDS, long[].class, false);
			terminalBatchId = step.getAttribute(AuthorityAttrEnum.ATTR_TERMINAL_BATCH_ID, Long.class, false);
			log.info("Processing next task for Terminal " + paymentSubtypeKeyId + "(" + paymentSubtypeClass + "), global token code: " + oldGlobalTokenCode + ", pending transactions: " + (pendingTranIds == null ? "none" : pendingTranIds.length) + ", pending batches: " + (pendingTerminalBatchIds == null ? "none" : pendingTerminalBatchIds.length));
		} catch(AttributeConversionException e) {
			throw new ServiceException("Could not convert attributes", e);
		}
		//String lockString = paymentSubTypeKeyId + "|" + paymentSubTypeClass;

		Connection conn = createConnection();
		boolean okay = true;
		try {
			try {
				// 0. Verify global token
				String newGlobalTokenCode = POSMUtils.generateGlobalToken();			
				if(!POSMUtils.obtainGlobalToken(conn, paymentSubtypeKeyId, paymentSubtypeClass, oldGlobalTokenCode, newGlobalTokenCode, false)) {
					throw new ServiceException("The global token code '" + oldGlobalTokenCode + "' for Terminal " + paymentSubtypeKeyId + "(" + paymentSubtypeClass + ") changed. Not able to process this request.");
				}
				MessageChainStep[] nextSteps = taskInfo.getStep().getNextSteps(5);
				if(nextSteps != null && nextSteps.length > 0) {
					MessageChainStep checkStep = nextSteps[0];
					if(checkStep != null)
						checkStep.setAttribute(AuthorityAttrEnum.ATTR_GLOBAL_TOKEN_CD, newGlobalTokenCode); // processSettlement and processSale use this step in building the new message chain, so we
																											// must update the global token cd
				}
				if(log.isInfoEnabled())
					log.info("Renewed lock on Terminal " + paymentSubtypeKeyId + "(" + paymentSubtypeClass + ") from global token code '" + oldGlobalTokenCode + "' to '" + newGlobalTokenCode + "'");
				// 1. Check and process one settlement if found
				if(pendingTerminalBatchIds != null && pendingTerminalBatchIds.length > 0) {
					if(processSettlement(conn, taskInfo, paymentSubtypeKeyId, paymentSubtypeClass, newGlobalTokenCode, pendingTerminalBatchIds))
						return 0;
				}

				// 2. Check and process one sale (or more) if found and transaction processing is enabled
				if(pendingTranIds != null && pendingTranIds.length > 0) {
					int limit = getBulkSaleProcessingLimit(paymentSubtypeClass);
					if(limit > 1) {
						if(processSalesEnmasse(conn, taskInfo, paymentSubtypeKeyId, paymentSubtypeClass, newGlobalTokenCode, pendingTranIds, terminalBatchId, limit))
							return 0;
					} else if(processSale(conn, taskInfo, paymentSubtypeKeyId, paymentSubtypeClass, newGlobalTokenCode, pendingTranIds, terminalBatchId))
						return 0;
				}

				return 5;
			} catch (ServiceException e) {
				okay = false;
				throw e;
			} catch (RuntimeException e) {
				okay = false;
				throw e;
			} catch (Error e) {
				okay = false;
				throw e;
			} finally {
				if(okay)
					commitConnection(conn);
				else
					rollbackConnection(conn);
			}
		} finally {
			closeConnection(conn);
		}
	}


	protected boolean processSale(Connection conn, MessageChainTaskInfo taskInfo, long paymentSubTypeKeyId, String paymentSubTypeClass, String globalTokenCode, long[] tranIds, Long terminalBatchId) throws ServiceException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("terminalBatchId", terminalBatchId);
		for(int i = 0; i < tranIds.length; i++) {
			params.put("tranId", tranIds[i]);
			try {
				DataLayerMgr.executeCall(conn, "PREPARE_PENDING_TRAN", params);
				Number saleAuthId = ConvertUtils.convert(Number.class, params.get("saleAuthId"));
				Number refundId = ConvertUtils.convert(Number.class, params.get("refundId"));
				SalePhase salePhase = ConvertUtils.convertRequired(SalePhase.class, params.get("salePhaseCd"));
				if(salePhase == SalePhase.NO_PROCESSING) {
					//no processing needed
					continue;
				}
				int priorAttempts = ConvertUtils.getInt(params.get("priorAttempts"), 0);
				boolean hasMore;
				long[] tmp;
				if(tranIds.length > i + 1) {
					tmp = new long[tranIds.length - 1 - i];
					System.arraycopy(tranIds, i + 1, tmp, 0, tmp.length);
					hasMore = true;
				} else {
					tmp = null;
					hasMore = false;
				}
				SaleMessageChain smc = new SaleMessageChain(paymentSubTypeClass);
				constructSaleSteps(smc, conn, 0, tranIds[i], paymentSubTypeKeyId, paymentSubTypeClass, globalTokenCode, saleAuthId, refundId, salePhase, priorAttempts);
				smc.prepareForPublish(0, paymentSubTypeKeyId, paymentSubTypeClass, globalTokenCode);
				MessageChainStep[] checkStep = taskInfo.getStep().getNextSteps(5);
				for(int k = 0; k < checkStep.length; k++) {
					if(checkStep[k].getMessageChain() != smc.getMessageChain()) {
						MessageChainStep newStep = smc.getMessageChain().addStep(checkStep[k].getQueueKey(), checkStep[k].isMulticast());
						newStep.setTemporary(checkStep[k].isTemporary());
						newStep.copyAttributes(checkStep[k]);
						checkStep[k] = newStep;
					}
				}
				if(hasMore) {
					MessageChainStep nextStep = smc.getMessageChain().addStep(taskInfo.getStep().getQueueKey());
					nextStep.setAttribute(AuthorityAttrEnum.ATTR_TRAN_IDS, tmp);
					nextStep.setAttribute(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_KEY_ID, paymentSubTypeKeyId);
					nextStep.setAttribute(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_CLASS, paymentSubTypeClass);
					nextStep.setAttribute(AuthorityAttrEnum.ATTR_GLOBAL_TOKEN_CD, globalTokenCode);
					smc.getOrCreateSaleUpdateStep().setNextSteps(0, nextStep);
					nextStep.setNextSteps(5, checkStep);
				} else {
					smc.getOrCreateSaleUpdateStep().setNextSteps(0, checkStep);
				}
				MessageChainService.publish(smc.getMessageChain(), taskInfo.getPublisher(), taskInfo.getEncryptionInfoMapping());
				return true;
			} catch(SQLException e) {
				throw new ServiceException("Could not prepare transaction " + tranIds[i] + " for processing", e);
			} catch(DataLayerException e) {
				throw new ServiceException("Could not prepare transaction " + tranIds[i] + " for processing", e);
			} catch(ConvertException e) {
				throw new ServiceException("Could not prepare transaction " + tranIds[i] + " for processing", e);
			} catch(ServiceException e) {
				throw new ServiceException("Could not prepare transaction " + tranIds[i] + " for processing", e);
			}
		}
		return false;
	}

	protected boolean processSalesEnmasse(Connection conn, MessageChainTaskInfo taskInfo, long paymentSubTypeKeyId, String paymentSubTypeClass, String globalTokenCode, long[] tranIds, Long terminalBatchId, int limit) throws ServiceException {
		SaleMessageChain smc = new SaleMessageChain(paymentSubTypeClass);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("terminalBatchId", terminalBatchId);
		int index = 0;
		if(limit < 1)
			limit = Integer.MAX_VALUE;
		int i = 0;
		try {
			for(; i < tranIds.length && index <= limit; i++) {
				params.put("tranId", tranIds[i]);
				DataLayerMgr.executeCall(conn, "PREPARE_PENDING_TRAN", params);
				Number saleAuthId = ConvertUtils.convert(Number.class, params.get("saleAuthId"));
				Number refundId = ConvertUtils.convert(Number.class, params.get("refundId"));
				SalePhase salePhase = ConvertUtils.convertRequired(SalePhase.class, params.get("salePhaseCd"));
				if(salePhase == SalePhase.NO_PROCESSING) {
					// no processing needed
					continue;
				}
				int priorAttempts = ConvertUtils.getInt(params.get("priorAttempts"), 0);

				constructSaleSteps(smc, conn, ++index, tranIds[i], paymentSubTypeKeyId, paymentSubTypeClass, globalTokenCode, saleAuthId, refundId, salePhase, priorAttempts);
			}
			if(index == 0)
				return false; // nothing to process
			smc.prepareForPublish(index, paymentSubTypeKeyId, paymentSubTypeClass, globalTokenCode);
			MessageChainStep[] checkStep = taskInfo.getStep().getNextSteps(5);
			for(int k = 0; k < checkStep.length; k++) {
				if(checkStep[k].getMessageChain() != smc.getMessageChain()) {
					MessageChainStep newStep = smc.getMessageChain().addStep(checkStep[k].getQueueKey(), checkStep[k].isMulticast());
					newStep.setTemporary(checkStep[k].isTemporary());
					newStep.copyAttributes(checkStep[k]);
					checkStep[k] = newStep;
				}
			}
			boolean hasMore;
			long[] tmp;
			if(tranIds.length > i + 1) {
				tmp = new long[tranIds.length - 1 - i];
				System.arraycopy(tranIds, i + 1, tmp, 0, tmp.length);
				hasMore = true;
			} else {
				tmp = null;
				hasMore = false;
			}

			if(hasMore) {
				MessageChainStep nextStep = smc.getMessageChain().addStep(taskInfo.getStep().getQueueKey());
				nextStep.setAttribute(AuthorityAttrEnum.ATTR_TRAN_IDS, tmp);
				nextStep.setAttribute(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_KEY_ID, paymentSubTypeKeyId);
				nextStep.setAttribute(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_CLASS, paymentSubTypeClass);
				nextStep.setAttribute(AuthorityAttrEnum.ATTR_GLOBAL_TOKEN_CD, globalTokenCode);
				smc.getOrCreateSaleUpdateStep().setNextSteps(0, nextStep);
				nextStep.setNextSteps(5, checkStep);
			} else {
				smc.getOrCreateSaleUpdateStep().setNextSteps(0, checkStep);
			}
			MessageChainService.publish(smc.getMessageChain(), taskInfo.getPublisher(), taskInfo.getEncryptionInfoMapping());
			return true;
		} catch(SQLException | DataLayerException | ConvertException | ServiceException e) {
			throw new ServiceException(i >= tranIds.length ? "Could not publish sale processing message chain" : "Could not prepare transaction " + tranIds[i] + " for processing", e);
		}
	}

	protected boolean constructSaleSteps(SaleMessageChain smc, Connection conn, int index, long tranId, long paymentSubtypeKeyId, String paymentSubtypeClass, String globalTokenCode, Number saleAuthId, Number refundId, SalePhase salePhase, int priorAttempts) throws ServiceException {
		// Gets the Information required for the message chain.
		long time = System.currentTimeMillis();
		log.info("Adding tranId " + tranId + " at index " + index);
		Map<String, Object> authorityInput = new HashMap<String, Object>();
		authorityInput.put("saleAuthId", saleAuthId);
		authorityInput.put("refundId", refundId);
		authorityInput.put("saleTypeCd", salePhase.getValue());
		try {
			DataLayerMgr.selectInto(conn, saleAuthId == null ? "GET_AUTHORITY_REFUND_DETAILS" : "GET_AUTHORITY_SALE_DETAILS", authorityInput);
		} catch(NotEnoughRowsException e) {
			// mark transaction as failed
			// Construction of the SaleUpdateStep.
			MessageChainStep saleUpdateStep = smc.getOrCreateSaleUpdateStep();
			saleUpdateStep.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_TRAN_ID, index, tranId);
			saleUpdateStep.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_SALE_PHASE, index, salePhase);
			saleUpdateStep.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_LAST_ATTEMPT, index, priorAttempts + 1 >= (saleAuthId == null ? getRefundRetryAttempts() : getSaleRetryAttempts()));
			saleUpdateStep.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_SALE_AUTH_ID, index, saleAuthId);
			saleUpdateStep.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_REFUND_ID, index, refundId);
			saleUpdateStep.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_MINOR_CURRENCY_FACTOR, index, 0); // we can do this since approved amount will be null
			saleUpdateStep.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, index, AuthResultCode.FAILED.getValue());
			saleUpdateStep.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, index, "Could not find transaction details in call '" + (saleAuthId == null ? "GET_AUTHORITY_REFUND_DETAILS" : "GET_AUTHORITY_SALE_DETAILS") + "'");
			saleUpdateStep.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS, index, System.currentTimeMillis());
			return false;
		} catch(SQLException e) {
			throw new ServiceException("Unable to get the required attributes to construct the message chain.", e);
		} catch(DataLayerException e) {
			throw new ServiceException("Unable to get the required attributes to construct the message chain.", e);
		} catch(BeanException e) {
			throw new ServiceException("Unable to get the required attributes to construct the message chain.", e);
		}

		// If track data or pan and exp date are not available use the encrypted and send first to decrypt queue
		String authAccountData;
		String cardKey;
		try {
			authAccountData = ConvertUtils.getString(authorityInput.get(AuthorityAttrEnum.ATTR_AUTH_ACCOUNT_DATA.getValue()), false);
			cardKey = ConvertUtils.getString(authorityInput.get(AuthorityAttrEnum.ATTR_CARD_KEY.getValue()), false);
		} catch(ConvertException e) {
			throw new ServiceException("Could not convert attributes for transaction details", e);
		}
		MessageChainStep authorityStep = smc.getOrCreateAuthorityStep();
		if(!StringUtils.isBlank(authAccountData)) {
			authorityStep.setOptionallyIndexedSecretAttribute(AuthorityAttrEnum.ATTR_AUTH_ACCOUNT_DATA, index, authAccountData);
		} else if(!StringUtils.isBlank(cardKey)) {
			if(!cardKey.startsWith("A:")) {
				Map<String, Long> parsedMap = new HashMap<String, Long>();
				try {
					InteractionUtils.parseCardIdList(cardKey, parsedMap);
				} catch(ParseException e) {
					throw new ServiceException("Could not parse card key '" + cardKey + "' for transaction #" + tranId, e);
				}
				Set<String> instances = parsedMap.keySet();
				if(!keymgrInstances.isEmpty())
					instances.retainAll(keymgrInstances);
				smc.voteForRetrieve(instances);
			}
			MessageChainStep retrieveStep = smc.getOrCreateRetrieveStep();
			retrieveStep.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_CARD_KEY, index, cardKey);
			retrieveStep.setOptionallyIndexedAttribute(CommonAttrEnum.ATTR_DECRYPTED_ENCODING, index, ProcessingConstants.US_ASCII_CHARSET.name());
			int encryptedDataCount = copyAttribute(retrieveStep, authorityInput, CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT, index, Integer.class, true);
			for(PaymentMaskBRef pmb : PaymentMaskBRef.values())
				if(pmb.getStoreIndex() > 0 && pmb.getStoreIndex() <= encryptedDataCount)
					authorityStep.setReferenceOptionallyIndexedAttribute(pmb.attribute, index, retrieveStep, pmb.decryptionAttribute, index);
		}

		// Authority Step attributes.
		AuthorityServiceTypeCode serviceType = copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_SERVICE_TYPE, index, AuthorityServiceTypeCode.class, true);
		if(serviceType != AuthorityServiceTypeCode.BALANCE_BASED_ACCOUNT && serviceType != AuthorityServiceTypeCode.HIDDEN_BALANCE_ACCOUNT && serviceType != AuthorityServiceTypeCode.PSEUDO_PRIVILEGES_ACCOUNT) {
			throw new ServiceException("Invalid Authority Service Type Id - " + serviceType.getValue());
		}

		authorityStep.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_SALE_AUTH_ID, index, saleAuthId);
		copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_CARD_TYPE, index, String.class, false);
		copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_MERCHANT_CD, index, String.class, false);
		copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_MINOR_CURRENCY_FACTOR, index, Integer.class, true);
		copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_AMOUNT, index, Long.class, true);
		copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_AUTH_AMOUNT, index, Long.class, false);
		Long originalAmount = copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_ORIGINAL_AMOUNT, index, Long.class, false);
		if(originalAmount != null && originalAmount > 0) {
			copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_ORIGINAL_TRACE_NUMBER, index, Long.class, true);
			authorityStep.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_ACTION_TYPE, index, AuthorityAction.ADJUSTMENT);
			authorityStep.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_ACTION_SUB_TYPE, index, saleAuthId == null ? AuthorityAction.REFUND : AuthorityAction.SALE);
			Long authAuthorityTs = copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS, index, Long.class, false);
			if(authAuthorityTs == null || authAuthorityTs <= 0)
				authorityStep.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS, index, System.currentTimeMillis());
		} else {
			authorityStep.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_ACTION_TYPE, index, saleAuthId == null ? AuthorityAction.REFUND : AuthorityAction.SALE);
			copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS, index, Long.class, false);
			copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_AMT_APPROVED, index, Long.class, false);
		}
		copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_ENTRY_TYPE, index, Byte.class, true);
		copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_AUTH_TYPE_CD, index, Character.class, true);
		copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_REMOTE_SERVER_ADDRESS, index, String.class, false);
		copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_REMOTE_SERVER_ALTERNATIVE, index, String.class, false);
		copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_REMOTE_SERVER_PORT, index, String.class, false);
		copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_TERMINAL_CD, index, String.class, false);
		copyAttribute(authorityStep, authorityInput, MessageAttrEnum.ATTR_DEVICE_NAME, index, String.class, true);
		copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_DEVICE_SERIAL_CD, index, String.class, true);
		copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_DEVICE_TRAN_CD, index, String.class, true);
		copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_TRACE_NUMBER, index, Long.class, true);
		copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_AUTHORITY_NAME, index, String.class, true);
		copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_CURRENCY_CD, index, String.class, true);
		copySecretAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_TERMINAL_ENCRYPTION_KEY, index, false);
		copySecretAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_TERMINAL_ENCRYPTION_KEY2, index, false);
		copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_TIMEZONE_GUID, index, String.class, false);
		copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_AUTH_TIME, index, Long.class, true);
		copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, index, String.class, false);
		copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD, index, String.class, false);
		copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, index, String.class, false);
		authorityStep.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_PRIOR_ATTEMPTS, index, priorAttempts);

		copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_POS_ENVIRONMENT, index, Character.class, false);
		copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_ENTRY_CAPABILITY, index, String.class, false);
		copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_PIN_CAPABILITY, index, Boolean.class, false);
		copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_MERCHANT_CATEGORY_CODE, index, Short.class, false);
		copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_DOING_BUSINESS_AS, index, String.class, false);
		copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_CUSTOMER_ID, index, String.class, false);
		copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_CONSUMER_ACCT_TYPE_ID, index, Integer.class, false);
		try {
			String address = ConvertUtils.convert(String.class, authorityInput.get("address"));
			String city = ConvertUtils.convert(String.class, authorityInput.get("city"));
			String stateCd = ConvertUtils.convert(String.class, authorityInput.get("stateCd"));
			String postal = ConvertUtils.convert(String.class, authorityInput.get("postal"));
			String countryCd = ConvertUtils.convert(String.class, authorityInput.get("countryCd"));
			if(StringUtils.isBlank(city) && StringUtils.isBlank(postal)) {
				address = InteractionUtils.getDefaultTerminalAddress();
				city = InteractionUtils.getDefaultTerminalCity();
				stateCd = InteractionUtils.getDefaultTerminalStateCd();
				postal = InteractionUtils.getDefaultTerminalPostal();
				countryCd = InteractionUtils.getDefaultTerminalCountryCd();
			}
			authorityStep.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_ADDRESS, index, address);
			authorityStep.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_CITY, index, city);
			authorityStep.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_STATE_CD, index, stateCd);
			authorityStep.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_POSTAL, index, postal);
			authorityStep.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_COUNTRY_CD, index, countryCd);
			authorityStep.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_CUSTOMER_SERVICE_PHONE, index, SystemUtils.nvl(ConvertUtils.convert(String.class, authorityInput.get("csPhone")), InteractionUtils.getDefaultCustomerServicePhone()));
			authorityStep.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_CUSTOMER_SERVICE_EMAIL, index, SystemUtils.nvl(ConvertUtils.convert(String.class, authorityInput.get("csEmail")), InteractionUtils.getDefaultCustomerServiceEmail()));
		} catch(ConvertException e) {
			throw new ServiceException("Could not convert attributes for transaction details", e);
		}

		copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_LONGITUDE, index, Float.class, false);
		copyAttribute(authorityStep, authorityInput, AuthorityAttrEnum.ATTR_LATITUDE, index, Float.class, false);

		byte[] consumerAcctToken;
		try {
			consumerAcctToken = ConvertUtils.convert(byte[].class, authorityInput.get(AuthorityAttrEnum.ATTR_CONSUMER_ACCT_TOKEN.getValue()));
		} catch(ConvertException e) {
			throw new ServiceException("Could not get consumer account token", e);
		}
		if(consumerAcctToken != null)
			authorityStep.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_CONSUMER_ACCT_TOKEN, index, StringUtils.toHex(consumerAcctToken));

		// Sales are offline; refunds are online
		authorityStep.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_ONLINE, index, refundId != null);
		authorityStep.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_TRAN_ID, index, tranId); // Internal Authority needs this
		authorityStep.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_START_TIME, index, time);

		if("Y".equals(authorityInput.get("sendLineItemsInd"))) {
			try {
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("tranId", tranId);
				Results results = DataLayerMgr.executeQuery(conn, "GET_MAPPED_LINE_ITEM_DETAILS", params);
				final AtomicInteger itemCount = new AtomicInteger(0);
				Attribute productAttr = new Attribute() {
					public String getValue() {
						return new StringBuilder("product").append(itemCount.get()).toString();
					}
				};
				Attribute priceAttr = new Attribute() {
					public String getValue() {
						return new StringBuilder("price").append(itemCount.get()).toString();
					}
				};
				Attribute quantityAttr = new Attribute() {
					public String getValue() {
						return new StringBuilder("quantity").append(itemCount.get()).toString();
					}
				};
				Attribute tranLineItemTypeIdAttr = new Attribute() {
					public String getValue() {
						return new StringBuilder("tranLineItemTypeId").append(itemCount.get()).toString();
					}
				};
				Attribute codeAttr = new Attribute() {
					public String getValue() {
						return new StringBuilder("code").append(itemCount.get()).toString();
					}
				};
				Attribute descAttr = new Attribute() {
					public String getValue() {
						return new StringBuilder("desc").append(itemCount.get()).toString();
					}
				};
				Attribute productDescAttr = new Attribute() {
					public String getValue() {
						return new StringBuilder("productDesc").append(itemCount.get()).toString();
					}
				};
				while(results.next()) {
					itemCount.incrementAndGet();
					authorityStep.setOptionallyIndexedAttribute(productAttr, index, results.getFormattedValue("lineItem.product"));
					authorityStep.setOptionallyIndexedAttribute(priceAttr, index, results.getFormattedValue("lineItem.price"));
					authorityStep.setOptionallyIndexedAttribute(quantityAttr, index, results.getFormattedValue("lineItem.quantity"));
					authorityStep.setOptionallyIndexedAttribute(tranLineItemTypeIdAttr, index, results.getFormattedValue("lineItem.tranLineItemTypeId"));
					authorityStep.setOptionallyIndexedAttribute(codeAttr, index, results.getFormattedValue("lineItem.code"));
					authorityStep.setOptionallyIndexedAttribute(descAttr, index, results.getFormattedValue("lineItem.desc"));
					authorityStep.setOptionallyIndexedAttribute(productDescAttr, index, results.getFormattedValue("lineItem.productDesc"));
				}
				authorityStep.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_ITEM_COUNT, index, itemCount);
			} catch(Exception e) {
				throw new ServiceException("Error getting line items", e);
			}
		}

		MessageChainStep saleUpdateStep = smc.getOrCreateSaleUpdateStep();

		// Construction of the SaleUpdateStep.
		saleUpdateStep.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_TRAN_ID, index, tranId);
		saleUpdateStep.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_SALE_PHASE, index, salePhase);
		saleUpdateStep.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_LAST_ATTEMPT, index, priorAttempts + 1 >= (saleAuthId == null ? getRefundRetryAttempts() : getSaleRetryAttempts()));
		saleUpdateStep.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_SALE_AUTH_ID, index, saleAuthId);
		saleUpdateStep.setOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_REFUND_ID, index, refundId);

		copyAttribute(saleUpdateStep, authorityInput, AuthorityAttrEnum.ATTR_MINOR_CURRENCY_FACTOR, index, Integer.class, true);
		copyAttribute(saleUpdateStep, authorityInput, AuthorityAttrEnum.ATTR_AUTH_TYPE_CD, index, Character.class, true);

		// Reference Attributes from the Authority Step
		saleUpdateStep.setReferenceOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, index, authorityStep, AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, index);
		saleUpdateStep.setReferenceOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_SETTLED, index, authorityStep, AuthorityAttrEnum.ATTR_SETTLED, index);
		saleUpdateStep.setReferenceOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, index, authorityStep, AuthorityAttrEnum.ATTR_AMT_APPROVED, index);
		saleUpdateStep.setReferenceOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, index, authorityStep, AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, index);
		saleUpdateStep.setReferenceOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, index, authorityStep, AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, index);
		saleUpdateStep.setReferenceOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, index, authorityStep, AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, index);
		saleUpdateStep.setReferenceOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD, index, authorityStep, AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD, index);
		saleUpdateStep.setReferenceOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, index, authorityStep, AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, index);
		saleUpdateStep.setReferenceOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS, index, authorityStep, AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS, index);
		saleUpdateStep.setReferenceOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, index, authorityStep, AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, index);
		saleUpdateStep.setReferenceOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT, index, authorityStep, AuthorityAttrEnum.ATTR_CLIENT_TEXT, index);
		saleUpdateStep.setReferenceOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_START_TIME, index, authorityStep, AuthorityAttrEnum.ATTR_START_TIME, index);
		saleUpdateStep.setReferenceOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_END_TIME, index, authorityStep, AuthorityAttrEnum.ATTR_END_TIME, index);
		saleUpdateStep.setReferenceOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_AUTH_ADJUSTED, index, authorityStep, AuthorityAttrEnum.ATTR_AUTH_ADJUSTED, index);
		saleUpdateStep.setReferenceOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_EMV_PARAMETER_DNLD_REQD_TIMESTAMP, index, authorityStep, AuthorityAttrEnum.ATTR_EMV_PARAMETER_DNLD_REQD_TIMESTAMP);
		
		return true;
	}

	/**
	 *
	 */
	protected boolean processSettlement(Connection conn, MessageChainTaskInfo taskInfo, long paymentSubTypeKeyId, String paymentSubTypeClass, String globalTokenCode, long[] terminalBatchIds) throws ServiceException {
		Map<String, Object> params = new HashMap<String, Object>();
		for(int i = 0; i < terminalBatchIds.length; i++) {
			params.put("terminalBatchId", terminalBatchIds[i]);
			try {
				DataLayerMgr.executeCall(conn, "GET_OR_CREATE_SETTLEMENT", params);
				Long settlementBatchId = ConvertUtils.convert(Long.class, params.get("settlementBatchId"));
				if(settlementBatchId == null)
					continue;// It is rare that it would get to this
				int priorAttempts = ConvertUtils.getInt(params.get("priorAttempts"), 0);
				boolean uploadNeeded = ConvertUtils.getBoolean(params.get("uploadNeededFlag"));
				boolean hasMore;
				long[] tmp;
				if(terminalBatchIds.length > i + 1) {
					tmp = new long[terminalBatchIds.length - 1 - i];
					System.arraycopy(terminalBatchIds, i + 1, tmp, 0, tmp.length);
					hasMore = true;
				} else {
					tmp = null;
					hasMore = false;
				}
				MessageChain mc = new MessageChainV11();
				MessageChainStep lastStep = constructSettlementSteps(mc, conn, settlementBatchId, terminalBatchIds[i], paymentSubTypeKeyId, paymentSubTypeClass, globalTokenCode, priorAttempts, uploadNeeded);
				MessageChainStep[] checkStep = taskInfo.getStep().getNextSteps(5);
				for(int k = 0; k < checkStep.length; k++) {
					if(checkStep[k].getMessageChain() != mc) {
						MessageChainStep newStep = mc.addStep(checkStep[k].getQueueKey(), checkStep[k].isMulticast());
						newStep.setTemporary(checkStep[k].isTemporary());
						newStep.copyAttributes(checkStep[k]);
						checkStep[k] = newStep;
					}
				}
				if(hasMore) {
					MessageChainStep nextStep = mc.addStep(taskInfo.getStep().getQueueKey());
					nextStep.setAttribute(AuthorityAttrEnum.ATTR_TERMINAL_BATCH_IDS, tmp);
					nextStep.setAttribute(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_KEY_ID, paymentSubTypeKeyId);
					nextStep.setAttribute(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_CLASS, paymentSubTypeClass);
					nextStep.setAttribute(AuthorityAttrEnum.ATTR_GLOBAL_TOKEN_CD, globalTokenCode);
					if(lastStep != null)
						lastStep.setNextSteps(0, nextStep);
					nextStep.setNextSteps(5, checkStep);
				} else {
					if(lastStep != null)
						lastStep.setNextSteps(0, checkStep);
				}
				if(lastStep != null || hasMore) {
					MessageChainService.publish(mc, taskInfo.getPublisher(), taskInfo.getEncryptionInfoMapping());
					return true;
				}
			} catch(SQLException e) {
				throw new ServiceException("Could not prepare batch " + terminalBatchIds[i] + " for processing", e);
			} catch(DataLayerException e) {
				throw new ServiceException("Could not prepare batch " + terminalBatchIds[i] + " for processing", e);
			} catch(ConvertException e) {
				throw new ServiceException("Could not prepare batch " + terminalBatchIds[i] + " for processing", e);
			} catch(ServiceException e) {
				throw new ServiceException("Could not prepare batch " + terminalBatchIds[i] + " for processing", e);
			}
		}
		return false;
	}

	protected MessageChainStep constructSettlementSteps(MessageChain mc, Connection conn, long settlementBatchId, long terminalBatchId, long terminalId, String paymentSubTypeClass, String globalTokenCode, int priorAttempts, boolean uploadNeeded) throws ServiceException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("settlementBatchId", settlementBatchId);
		params.put("terminalBatchId", terminalBatchId);
		try {
			DataLayerMgr.selectInto(conn, "GET_AUTHORITY_SETTLEMENT_DETAILS", params);
		} catch(NotEnoughRowsException e) {
			log.error("Could not retrieve settlement details for terminal batch " + terminalBatchId, e);
			return null;
		} catch(SQLException e) {
			throw new ServiceException("Could not get Settlement Details from database", e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not get Settlement Details from database", e);
		} catch(BeanException e) {
			throw new ServiceException("Could not get Settlement Details from database", e);
		}
		try {
			// Add Authority Step.
			MessageChainStep authorityStep = mc.addStep(getAuthorityQueueKeyPrefix() + ConvertUtils.getString(params.get("gatewayQueueKey"), true));

			// Attributes for the Authority Layer
			if(priorAttempts > 0) {
				authorityStep.setAttribute(AuthorityAttrEnum.ATTR_ACTION_TYPE, AuthorityAction.SETTLEMENT_RETRY);
			} else {
				authorityStep.setAttribute(AuthorityAttrEnum.ATTR_ACTION_TYPE, AuthorityAction.SETTLEMENT);
			}
			copyAttribute(authorityStep, params, AuthorityAttrEnum.ATTR_MERCHANT_CD, null, false);
			copyAttribute(authorityStep, params, AuthorityAttrEnum.ATTR_MERCHANT_NAME, null, false);
			copyAttribute(authorityStep, params, AuthorityAttrEnum.ATTR_TERMINAL_CD, null, false);
			authorityStep.setAttribute(AuthorityAttrEnum.ATTR_TRACE_NUMBER, settlementBatchId);
			copyAttribute(authorityStep, params, AuthorityAttrEnum.ATTR_TERMINAL_BATCH_NUM, null, true);
			copyAttribute(authorityStep, params, AuthorityAttrEnum.ATTR_CURRENCY_CD, String.class, true); // default USD
			copySecretAttribute(authorityStep, params, AuthorityAttrEnum.ATTR_TERMINAL_ENCRYPTION_KEY, false);
			copySecretAttribute(authorityStep, params, AuthorityAttrEnum.ATTR_TERMINAL_ENCRYPTION_KEY2, false);
			
			copyAttribute(authorityStep, params, AuthorityAttrEnum.ATTR_REMOTE_SERVER_ADDRESS, null, false);
			copyAttribute(authorityStep, params, AuthorityAttrEnum.ATTR_REMOTE_SERVER_PORT, null, false);
			authorityStep.setAttribute(AuthorityAttrEnum.ATTR_RESPONSE_TIME_OUT, getSettlementResponseTimeOut());
			copyAttribute(authorityStep, params, AuthorityAttrEnum.ATTR_MINOR_CURRENCY_FACTOR, Integer.class, true);
			copyAttribute(authorityStep, params, AuthorityAttrEnum.ATTR_CREDIT_SALE_COUNT, Integer.class, true);
			copyAttribute(authorityStep, params, AuthorityAttrEnum.ATTR_CREDIT_SALE_AMOUNT, Long.class, true);
			copyAttribute(authorityStep, params, AuthorityAttrEnum.ATTR_CREDIT_REFUND_COUNT, Integer.class, true);
			copyAttribute(authorityStep, params, AuthorityAttrEnum.ATTR_CREDIT_REFUND_AMOUNT, Long.class, true);
			copyAttribute(authorityStep, params, AuthorityAttrEnum.ATTR_UNIQUE_SECONDS, Integer.class, false);
			
			// Add Settlement Update Step.
			MessageChainStep settlementUpdateStep = mc.addStep(settlementUpdateQueue);
			settlementUpdateStep.setAttribute(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_KEY_ID, terminalId);
			settlementUpdateStep.setAttribute(AuthorityAttrEnum.ATTR_TERMINAL_BATCH_ID, terminalBatchId);
			settlementUpdateStep.setAttribute(AuthorityAttrEnum.ATTR_SETTLEMENT_BATCH_ID, settlementBatchId);
			settlementUpdateStep.setAttribute(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_CLASS, paymentSubTypeClass);
			settlementUpdateStep.setAttribute(AuthorityAttrEnum.ATTR_GLOBAL_TOKEN_CD, globalTokenCode);
			settlementUpdateStep.setAttribute(AuthorityAttrEnum.ATTR_LAST_ATTEMPT, priorAttempts + 1 >= getSettlementRetryAttempts());

			settlementUpdateStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, authorityStep, AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD);
			settlementUpdateStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, authorityStep, AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD);
			settlementUpdateStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, authorityStep, AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC);
			settlementUpdateStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD, authorityStep, AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD);
			settlementUpdateStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, authorityStep, AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA);
			settlementUpdateStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS, authorityStep, AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS);
			settlementUpdateStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_START_TIME, authorityStep, AuthorityAttrEnum.ATTR_START_TIME);
			settlementUpdateStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_END_TIME, authorityStep, AuthorityAttrEnum.ATTR_END_TIME);
			settlementUpdateStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, authorityStep, AuthorityAttrEnum.ATTR_AUTH_RESULT_CD);

			authorityStep.setNextSteps(0, settlementUpdateStep);
			authorityStep.setNextSteps(1, settlementUpdateStep);
			authorityStep.setNextSteps(2, settlementUpdateStep);
			
			boolean terminalCapture = "Y".equalsIgnoreCase(ConvertUtils.getStringSafely(params.get("terminalCaptureFlag"), "N"));
			if(terminalCapture || uploadNeeded) {
				// Get and serialize transactions into message chain attribute
				params.clear();
				params.put("terminalBatchId", terminalBatchId);
				if(terminalCapture) {
					params.put("authStateId", 6);
				} else {
					params.put("authStateId", 2);
				}
				try {
					Results results = DataLayerMgr.executeQuery(conn, "GET_TERMINAL_CAPTURE_TRAN_DETAILS", params);
					byte[] key = getAuthorityCryption().generateKey();
					authorityStep.setAttribute(AuthorityAttrEnum.ATTR_CAPTURE_DETAILS_ENCRYPTION_KEY, key);	
					authorityStep.setAttribute(AuthorityAttrEnum.ATTR_CAPTURE_DETAILS_CIPHER, getAuthorityCryption().getCipherName());	
					authorityStep.setAttribute(AuthorityAttrEnum.ATTR_CAPTURE_DETAILS_BLOCK_SIZE, getAuthorityCryption().getBlockSize());	
					ResourceFolder rf = getResourceFolder();
					if(rf == null)
						throw new ServiceException("ResourceFolder is not set");
					Resource resource = rf.getResource("CaptureDetails_" + terminalBatchId, ResourceMode.CREATE);
					boolean okay = false;
					try {
						authorityStep.setAttribute(AuthorityAttrEnum.ATTR_CAPTURE_DETAILS_RESOURCE, resource.getKey());	
						Resource sensitiveRetrieveResource = null;
						OutputStream sensitiveRetrieveCos = null;
						ByteOutput sensitiveRetrieveOutput = null;
						OutputStream cos = getAuthorityCryption().createEncryptingOutputStream(key, resource.getOutputStream());
						try {
							if(log.isDebugEnabled()) {
								final ByteArrayOutputStream baos = new ByteArrayOutputStream();
								@SuppressWarnings("resource")
								PassThroughOutputStream ptos = new PassThroughOutputStream() {
									@Override
									public void close() throws IOException {
										super.close();
										log.debug("Wrote the following bytes to the Dataset File:\n" + StringUtils.toHex(baos.toByteArray()));
									}
								};
								ptos.addOutputStream(cos);
								ptos.addOutputStream(baos);							
								cos = ptos;
							}
							String[] columnNames = new String[results.getColumnCount()];
							for(int i = 0; i < columnNames.length; i++)
								columnNames[i] = results.getColumnName(i+1);
							Object[] values = new Object[columnNames.length];
							Object[] sensitiveRetrieveValues = null;
							ByteOutput output = new OutputStreamByteOutput(cos);
							DatasetUtils.writeHeader(output, columnNames);
							String massRetrieveQueue = getMassRetrieveQueue();
							MessageChainStep retrieveStep = null;
							int cnt = 0;
							int retrieveIndex = 0;
							Tallier<String> retrieveQueues = new Tallier<String>(String.class);
							while(results.next()) {
								for(int i = 0; i < values.length; i++)
									values[i] = results.getValue(i+1);
								cnt++;
								String authAccountData = ConvertUtils.getStringSafely(results.getValue(AuthorityAttrEnum.ATTR_AUTH_ACCOUNT_DATA.getValue()));
								if(StringUtils.isBlank(authAccountData)) {
									String cardKey = ConvertUtils.getStringSafely(results.getValue(AuthorityAttrEnum.ATTR_CARD_KEY.getValue()));
									if(!StringUtils.isBlank(cardKey)) {
										if(cardKey.startsWith("A:")) {
											retrieveQueues.vote(String.valueOf(getAppInstance()));
										} else {
											Map<String, Long> parsedMap = new HashMap<String, Long>();
											try {
												InteractionUtils.parseCardIdList(cardKey, parsedMap);
											} catch(ParseException e) {
												throw new ServiceException("Could not parse card key '" + cardKey + "' for transaction #" + results.getValue("tranId"), e);
											}
											switch(parsedMap.size()) {
												case 0: // trouble
													throw new ServiceException("Card key '" + cardKey + "' yield no instances for transaction #" + results.getValue("tranId"));
												default:
													retrieveQueues.vote(parsedMap.keySet());
													break;
											}
										}										
										if(retrieveStep == null) {
											if(!StringUtils.isBlank(massRetrieveQueue)) {
												sensitiveRetrieveResource = rf.getResource("SensitiveRetrieveDetails_" + terminalBatchId, ResourceMode.CREATE);
												retrieveStep = mc.addStep(massRetrieveQueue);
												retrieveStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_RESOURCE, sensitiveRetrieveResource.getKey());
												retrieveStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_ENCRYPTION_KEY, key);
												retrieveStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_CIPHER, getAuthorityCryption().getCipherName());
												retrieveStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_BLOCK_SIZE, getAuthorityCryption().getBlockSize());
												retrieveStep.setAttribute(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT, 2); // We only care about pan and expDate
												retrieveStep.setAttribute(CommonAttrEnum.ATTR_DECRYPTED_ENCODING, ProcessingConstants.US_ASCII_CHARSET.name());
												authorityStep.setReferenceAttribute(CommonAttrEnum.ATTR_CRYPTO_RESOURCE, retrieveStep, CommonAttrEnum.ATTR_CRYPTO_RESOURCE);
												sensitiveRetrieveCos = getAuthorityCryption().createEncryptingOutputStream(key, sensitiveRetrieveResource.getOutputStream());
												sensitiveRetrieveOutput = new OutputStreamByteOutput(sensitiveRetrieveCos);
												DatasetUtils.writeHeader(sensitiveRetrieveOutput, sensitiveRetrieveColumnNames);
												sensitiveRetrieveValues = new Object[1];
											} else {
												retrieveStep = mc.addStep(getSingleRetrieveQueue());
												retrieveStep.setAttribute(CommonAttrEnum.ATTR_DECRYPTED_ENCODING, ProcessingConstants.US_ASCII_CHARSET.name());
												retrieveStep.setAttribute(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT, 2); // We only care about pan and expDate
											}
											mc.setCurrentStep(retrieveStep);
											retrieveStep.setNextSteps(0, authorityStep);
											retrieveStep.setNextSteps(1, retrieveStep);
										}
										retrieveIndex++;
										if(sensitiveRetrieveResource != null) {
											if(retrieveIndex < cnt) {
												sensitiveRetrieveValues[0] = null;
												for(; retrieveIndex < cnt; retrieveIndex++) {
													DatasetUtils.writeRow(sensitiveRetrieveOutput, sensitiveRetrieveValues);
												}
											}
											sensitiveRetrieveValues[0] = cardKey;
											DatasetUtils.writeRow(sensitiveRetrieveOutput, sensitiveRetrieveValues);
										} else {
											retrieveStep.setIndexedAttribute(AuthorityAttrEnum.ATTR_CARD_KEY, retrieveIndex, cardKey);
											authorityStep.setReferenceIndexedAttribute(PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER.attribute, cnt, retrieveStep, PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER.decryptionAttribute, retrieveIndex);
											authorityStep.setReferenceIndexedAttribute(PaymentMaskBRef.EXPIRATION_DATE.attribute, cnt, retrieveStep, PaymentMaskBRef.EXPIRATION_DATE.decryptionAttribute, retrieveIndex);
										}
									} else {
										throw new ServiceException("CardKey is not provided");
									}
								}
								DatasetUtils.writeRow(output, values);
							}
							if(sensitiveRetrieveResource != null) {
								DatasetUtils.writeFooter(sensitiveRetrieveOutput);
								sensitiveRetrieveCos.flush();
							}
							DatasetUtils.writeFooter(output);
							cos.flush();
							authorityStep.setAttribute(AuthorityAttrEnum.ATTR_TRAN_COUNT, cnt);
							if(retrieveStep != null) {
								retrieveStep.setAttribute(CommonAttrEnum.ATTR_ENTRY_COUNT, retrieveIndex);
								retrieveStep.setQueueKey(AnyTwoInstanceWorkQueue.constructQueueName(retrieveStep.getQueueKey(), retrieveQueues.getWinners(2)));
							}
							okay = true;
						} finally {
							if(sensitiveRetrieveResource != null) {
								if(sensitiveRetrieveCos != null)
									sensitiveRetrieveCos.close();
								if(!okay)
									sensitiveRetrieveResource.delete();
								sensitiveRetrieveResource.release();
							}
							cos.close();
						}
					} finally {
						if(!okay)
							resource.delete();
						resource.release();
					}					
				} catch(SQLException e) {
					throw new ServiceException("Could not get Terminal Capture Tran Details from database", e);
				} catch(DataLayerException e) {
					throw new ServiceException("Could not get Terminal Capture Tran Details from database", e);
				} catch(IOException e) {
					throw new ServiceException("Could not write Terminal Capture Tran Details to file repo", e);
				} catch(GeneralSecurityException e) {
					throw new ServiceException("Could not encrypt Terminal Capture Tran Details while writing to file repo", e);
				}	
			}
			return settlementUpdateStep;
		} catch(ConvertException e) {
			throw new ServiceException("Unable to construct the settlement message chain because of invalid data", e);
		}
	}

	// Getters and Setters
	public void setSettlementUpdateQueue(String settlementUpdateQueue) {
		this.settlementUpdateQueue = settlementUpdateQueue;
	}

	public String getSettlementUpdateQueue() {
		return settlementUpdateQueue;
	}

	public void setSettlementResponseTimeOut(int settlementResponseTimeOut) {
		this.settlementResponseTimeOut = settlementResponseTimeOut;
	}

	public int getSettlementResponseTimeOut() {
		return settlementResponseTimeOut;
	}

	public void setSettlementRetryAttempts(int settlementRetryAttempts) {
		this.settlementRetryAttempts = settlementRetryAttempts;
	}

	public int getSettlementRetryAttempts() {
		return settlementRetryAttempts;
	}

	/**
	 * @return the maxRefundRetryAttempts
	 */
	public int getRefundRetryAttempts() {
		return refundRetryAttempts;
	}

	/**
	 * @param maxRefundRetryAttempts
	 *            the maxRefundRetryAttempts to set
	 */
	public void setRefundRetryAttempts(int maxRefundRetryAttempts) {
		this.refundRetryAttempts = maxRefundRetryAttempts;
	}

	public void setSaleResponseTimeOut(int saleResponseTimeOut) {
		this.saleResponseTimeOut = saleResponseTimeOut;
	}

	public int getSaleResponseTimeOut() {
		return saleResponseTimeOut;
	}

	public void setSaleUpdateQueue(String saleUpdateQueue) {
		this.saleUpdateQueue = saleUpdateQueue;
	}

	public String getSaleUpdateQueue() {
		return saleUpdateQueue;
	}

	public void setSaleRetryAttempts(int saleRetryAttempts) {
		this.saleRetryAttempts = saleRetryAttempts;
	}

	public int getSaleRetryAttempts() {
		return saleRetryAttempts;
	}

	public String getAuthorityQueueKeyPrefix() {
		return authorityQueueKeyPrefix;
	}

	public void setAuthorityQueueKeyPrefix(String authorityQueueKeyPrefix) {
		this.authorityQueueKeyPrefix = authorityQueueKeyPrefix;
	}

	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}

	public Cryption getAuthorityCryption() {
		return authorityCryption;
	}

	public String getMassDecryptQueue() {
		return massDecryptQueue;
	}

	public void setMassDecryptQueue(String massDecryptQueue) {
		this.massDecryptQueue = massDecryptQueue;
	}

	public String getSingleRetrieveQueue() {
		return singleRetrieveQueue;
	}

	public void setSingleRetrieveQueue(String retrieveCardQueue) {
		this.singleRetrieveQueue = retrieveCardQueue;
	}

	public String getMassRetrieveQueue() {
		return massRetrieveQueue;
	}

	public void setMassRetrieveQueue(String massRetrieveQueue) {
		this.massRetrieveQueue = massRetrieveQueue;
	}

	public int getAppInstance() {
		return appInstance;
	}

	public void setAppInstance(int appInstance) {
		this.appInstance = appInstance;
	}

	public int getBulkSaleProcessingLimit(String paymentSubtypeClass) {
		return bulkSaleProcessingLimits.getOrDefault(paymentSubtypeClass, Integer.valueOf(0));
	}

	public void setBulkSaleProcessingLimit(String paymentSubtypeClass, int limit) {
		if(limit <= 0)
			bulkSaleProcessingLimits.remove(paymentSubtypeClass);
		else
			bulkSaleProcessingLimits.put(paymentSubtypeClass, limit);
	}

	public String[] getKeymgrInstances() {
		return keymgrInstances.isEmpty() ? null : keymgrInstances.toArray(new String[keymgrInstances.size()]);
	}

	public void setKeymgrInstances(String[] keymgrInstances) {
		this.keymgrInstances.clear();
		if(keymgrInstances != null)
			for(String instance : keymgrInstances)
				if(instance != null && !(instance = instance.trim()).isEmpty())
					this.keymgrInstances.add(instance);
	}
}
