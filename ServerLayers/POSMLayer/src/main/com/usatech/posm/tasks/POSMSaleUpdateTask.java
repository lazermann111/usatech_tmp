package com.usatech.posm.tasks;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.usatech.app.Attribute;
import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.EMVUtils;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.LoadDataAttrEnum;
import com.usatech.posm.utils.POSMUtils;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;

/**
 *
 * This class updates the sale , settlement and tran settlement records based on the authority
 * response.
 * @author  bkrug
 *
 */
public class POSMSaleUpdateTask extends POSMTask {
	private static final Log log = Log.getLog();
	protected boolean sproutEnabled = false;
	protected static final Attribute[] COPY_ATTRIBUTES = new Attribute[] {
		AuthorityAttrEnum.ATTR_SALE_PHASE,
		AuthorityAttrEnum.ATTR_MINOR_CURRENCY_FACTOR,
		AuthorityAttrEnum.ATTR_AMT_APPROVED,
		AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD,
		AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC,
		AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD,
		AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA,
	};

	@Override
  protected int processInternal(MessageChainTaskInfo taskInfo) throws ServiceException {
  	MessageChainStep step = taskInfo.getStep();
    try {
      	Connection conn = DataLayerMgr.getConnectionForCall("UPDATE_PROCESSED_TRAN");
      	boolean okay = false;
      	try {
	        	checkValidGlobalToken(conn, step);
				int cnt = step.getAttributeDefault(CommonAttrEnum.ATTR_ENTRY_COUNT, Integer.class, Integer.valueOf(0));
				if(cnt > 0) {
					for(int index = 1; index <= cnt; index++)
						processSaleUpdate(taskInfo, index, conn);

				} else
					processSaleUpdate(taskInfo, 0, conn);
		        okay = true;
      	} finally {
      		if(!okay){ 
	    			try {
	      			conn.rollback();
	      		} catch(SQLException e) {
	      			log.warn("Could not rollback transaction");
	      		}
      		}
      		conn.close();
      	}
    } catch(AttributeConversionException e) {
    	throw new ServiceException("Could not convert attributes as needed", e);
    } catch(SQLException e) {
   		throw new ServiceException("Could not update processed sale", e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not update processed sale", e);
		}

    return 0;
  }

	protected void processSaleUpdate(MessageChainTaskInfo taskInfo, int index, Connection conn) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		// Update auth result cd if necessary
		Map<String, Object> input = new HashMap<String, Object>();
		boolean settled;
		long processedTime;
		AuthResultCode authResultCode;
		boolean settleUpdateNeeded = true;
		Long saleAuthId;
		Long refundId;
		Long requestStartTimestamp;
		long tranId;
		try {
			for(Attribute a : COPY_ATTRIBUTES)
				input.put(a.getValue(), step.getOptionallyIndexedAttribute(a, index, Object.class, false));
			authResultCode = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, index, AuthResultCode.class, true);
			boolean lastAttempt = step.getOptionallyIndexedAttributeDefault(AuthorityAttrEnum.ATTR_LAST_ATTEMPT, index, Boolean.class, false);
			if(lastAttempt) {
				switch(authResultCode) {
					case DECLINED:
						authResultCode = AuthResultCode.DECLINED_PERMANENT;
						break;
				}
			}
			input.put(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue(), authResultCode);

			requestStartTimestamp = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_EMV_PARAMETER_DNLD_REQD_TIMESTAMP, index, Long.class, false);
			if (requestStartTimestamp!=null && requestStartTimestamp > 0) {
				Long paymentSubtypeKeyId = ConvertUtils.getLong(step.getAttributes().get(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_KEY_ID.getValue()), 0);
				EMVUtils.processEMVParameterDownloadRequiredFlag(paymentSubtypeKeyId);
			}
			
			settled = step.getOptionallyIndexedAttributeDefault(AuthorityAttrEnum.ATTR_SETTLED, index, Boolean.class, false);
			input.put("settledFlag", settled ? "Y" : "N");
			Boolean adjusted = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_AUTH_ADJUSTED, index, Boolean.class, false);
			if(adjusted != null)
				input.put("adjustedFlag", adjusted ? "Y" : "N");
			Long authAuthorityTs = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS, index, Long.class, false);
			if(authAuthorityTs == null || authAuthorityTs <= 0) {
				Long authorityEndTime = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_END_TIME, index, Long.class, false);
				if(authorityEndTime != null && authorityEndTime > 0)
					processedTime = authorityEndTime;
				else
					processedTime = System.currentTimeMillis();
			} else
				processedTime = authAuthorityTs;
			input.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS.getValue(), processedTime);
			input.put("ignoreReprocessedFlag", taskInfo.isRedelivered() ? "Y" : "N");
			input.put("authorityTranCd", step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD, index, Object.class, false));
			tranId = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_TRAN_ID, index, Long.class, true);
			input.put("tranId", tranId);
			saleAuthId = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_SALE_AUTH_ID, index, Long.class, false);
			input.put("saleAuthId", saleAuthId);
			refundId = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_REFUND_ID, index, Long.class, false);
			input.put("refundId", refundId);
			input.put("sproutEnabledFlag", sproutEnabled ? 'Y' : 'N');

			DataLayerMgr.executeCall(conn, "UPDATE_PROCESSED_TRAN", input);
			if (sproutEnabled)
				DataLayerMgr.executeCall(conn, "UPDATE_SPROUT_REFUND_PROCESSED", input);
			conn.commit();
			try {
				settleUpdateNeeded = !"N".equalsIgnoreCase(ConvertUtils.getString(input.get(LoadDataAttrEnum.ATTR_SETTLE_UPDATE_NEEDED.getValue()), false));
			} catch(ConvertException e) {
				throw new ServiceException(e);
			}
			
		} catch(AttributeConversionException e) {
			throw new ServiceException("Could not convert attributes as needed", e);
		} catch(SQLException e) {
			//invalid transaction status code exception thrown from 'PKG_SETTLEMENT.UPDATE_PROCESSED_TRAN'
			if(Math.abs(e.getErrorCode())==20554){
				//ignore and continue processing the rest of the batch
				log.warn(e.getMessage());
				return;
			} else {
				//otherwise re throw the exception
				throw new ServiceException("Could not update processed sale", e);
			}
		} catch(DataLayerException e) {
			throw new ServiceException("Could not update processed sale", e);
		} catch (ConvertException e) {
			throw new ServiceException("Could not update processed sale", e);
		}
		// Record stats
		/* XXX: This ought to be recorded in the database (in a separate step)
		long authorityStartTime;
		try {
			authorityStartTime = step.getAttributeDefault(AuthorityAttrEnum.ATTR_AUTHORITY_START_TIME, Long.class, 0L);
			long authorityEndTime = step.getAttributeDefault(AuthorityAttrEnum.ATTR_AUTHORITY_END_TIME, Long.class, 0L);
		    long authTime = step.getAttribute(AuthorityAttrEnum.ATTR_AUTH_TIME, Long.class, true);
		    if(authorityStartTime != 0 && authorityEndTime != 0) {
		    	//StatusInfoCollector.getInstance().put(StatusUpdate.newGatewayProcessTime(authorityEndTime - authorityStartTime));
		    }
		    long updateTime = System.currentTimeMillis();
		    //StatusInfoCollector.getInstance().put(StatusUpdate.newTranProcessTime(updateTime - authTime));
		} catch(AttributeConversionException e) {
			log.warn("Could not convert times for statistics", e);
		}
		 */
		// Update reporting database if necessary
		if(settled && settleUpdateNeeded) {
			try {
				char authTypeCd = ConvertUtils.getChar(input.get(AuthorityAttrEnum.ATTR_AUTH_TYPE_CD.getValue()));
				String globalTransCd = ConvertUtils.getString(input.get(LoadDataAttrEnum.ATTR_TRAN_GLOBAL_TRANS_CD.getValue()), true);
				String authorityTranCd = ConvertUtils.getString(input.get(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD.getValue()), false);
				String sourceSystemCd = ConvertUtils.getString(input.get("sourceSystemCd"), true);
				BigInteger amountMinor;
				try {
					amountMinor = ConvertUtils.convert(BigInteger.class, input.get(AuthorityAttrEnum.ATTR_AMT_APPROVED.getValue()));
				} catch(ConvertException e) {
					log.warn("Could not convert " + AuthorityAttrEnum.ATTR_AMT_APPROVED.getValue() + "; using null", e);
					amountMinor = null;
				}
				int minorCurrencyFactor = ConvertUtils.getInt(input.get(AuthorityAttrEnum.ATTR_MINOR_CURRENCY_FACTOR.getValue()));
				Integer overrideTransTypeId = ConvertUtils.convert(Integer.class, input.get(LoadDataAttrEnum.ATTR_TRANS_TYPE_ID.getValue()));
				BigDecimal amountMajor;
				if(amountMinor == null)
					amountMajor = null;
				else {
					amountMajor = InteractionUtils.toMajorCurrency(amountMinor, minorCurrencyFactor);
					if(saleAuthId == null && refundId != null && amountMajor.signum() > 0)
						amountMajor = amountMajor.negate();
					else if(saleAuthId != null && amountMajor.signum() < 0)
						amountMajor = amountMajor.abs();
				}
				POSMUtils.publishSettleUpdate(taskInfo.getPublisher(), globalTransCd, authResultCode, processedTime, authorityTranCd, sourceSystemCd, authTypeCd, amountMajor, overrideTransTypeId, tranId);
			} catch(ConvertException e) {
				throw new RetrySpecifiedServiceException(e, WorkRetryType.NO_RETRY);
			}
		}
	}

	public boolean isSproutEnabled() {
		return sproutEnabled;
	}

	public void setSproutEnabled(boolean sproutEnabled) {
		this.sproutEnabled = sproutEnabled;
	}
}
