package com.usatech.posm.tasks;

import java.util.Map;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.constants.CommonAttrEnum;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.Log;

public class EFTProcessorTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected String eftFinalizeQueue = "usat.posm.eft.finalize";

	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		long eftId = -1;
		try {
			Map<String, Object> attributes = taskInfo.getStep().getAttributes();
			eftId = ConvertUtils.convert(long.class, attributes.get("eftId"));
			log.info("Processing EFT " + eftId);
			DataLayerMgr.executeUpdate("PROCESS_EFT", attributes, true);
			DataLayerMgr.executeUpdate("CHECK_EFT_PROCESS_COMPLETION", null, true);
			log.info("Finished processing EFT " + eftId);
			
			MessageChain messageChain = new MessageChainV11();
			MessageChainStep nextStep = messageChain.addStep(eftFinalizeQueue);
			nextStep.setAttribute(CommonAttrEnum.ATTR_EFT_ID, eftId);
			MessageChainService.publish(messageChain, taskInfo.getPublisher());			
		} catch(Exception e) {
			throw new ServiceException("Error processing EFT " + eftId, e);
		}
		return 0;
	}
	
	public String getEftFinalizeQueue() {
		return eftFinalizeQueue;
	}

	public void setEftFinalizeQueue(String eftFinalizeQueue) {
		this.eftFinalizeQueue = eftFinalizeQueue;
	}	
}
