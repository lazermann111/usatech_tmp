package com.usatech.posm.tasks;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.FileType;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.results.Results;
import simple.security.SecureHash;
import simple.text.Hasher;
import simple.text.StringUtils;

public class EMVParameterDownloadTask implements MessageChainTask {

	private static final simple.io.Log log = simple.io.Log.getLog();

	private static final String KEY_START = "keyAID=";

	private static final int MIN_BYTES = 8 + 23 + 1;

	protected final Hasher hasher;

	protected ResourceFolder resourceFolder;

	private String interacFolder;

	public EMVParameterDownloadTask() throws NoSuchAlgorithmException {
		hasher = new Hasher("SHA-256", true);
	}

	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		try {
			log.info("Starting EMV Param ...");
			MessageChainStep step = taskInfo.getStep();
			
			Long paramDnldId = step.getAttribute(AuthorityAttrEnum.ATTR_EMV_PARM_DNDL_ID, Long.class, true);
			String tandemCreatedResourceKey = step.getAttribute(CommonAttrEnum.ATTR_RESOURCE, String.class, true);
			
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("paramDnldId", paramDnldId);
			Results results = DataLayerMgr.executeQuery("GET_EMV_PARAM_DOWNLOAD_BY_PARAM_DOWNLOAD_REQD_ID", params);

			if (results.next()) {
				// final call so start the process to update the devices
				Map<String, Object>updateParams = new HashMap<>();
				updateParams.put("paramDnldId", paramDnldId);
				updateParams.put("completedTimestamp", new Date());
				updateParams.put("resourceKey", tandemCreatedResourceKey);
				Object [] result = DataLayerMgr.executeCall("UPDATE_EMV_PARAMETER_DOWNLOAD_RESOURCE_KEY", updateParams, true);
				
				log.warn("EMVParameterDownloadTask: 2) place record in MACHINE_CMD_PENDING!");
				Long terminalId = results.getValue("terminalId", Long.class);
				updateFileTransfer(terminalId, paramDnldId, tandemCreatedResourceKey);
			} else { // else just ignore it.  should not happen
				log.warn("EMVParameterDownloadTask did not find paramDnldId: " + paramDnldId);
			}
		} catch (AttributeConversionException | DataLayerException | SQLException | ArrayIndexOutOfBoundsException | ConvertException | IOException e) {
			throw new ServiceException(e);
		}
		
		return 0;
	}
	
	protected void updateFileTransfer(Long terminalId, Long paramDnldId, String resourceKey)
			throws SQLException, DataLayerException, IOException, ServiceException {
		Resource resource = null;
		resource = getResourceFolder().getResource(resourceKey, ResourceMode.READ);
		String resourceName = resource.getName();
		InputStream in = resource.getInputStream();
		String[] contents = convertToCardReaderFormat(in);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("fileTransferName", resourceName);
		params.put("fileTransferTypeCd", FileType.EMV_PARAMETERS);
		params.put("fileContent", contents[0]);
		try {
			byte[] contentHash = SecureHash.getUnsaltedHash(contents[0].getBytes());
			params.put("fileContentHash", contentHash);
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException(e);
		}
		params.put("terminalId", terminalId);
		Object[] result = DataLayerMgr.executeCall("APPLY_FILE_TRANSFER_CONTENT", params, true);
		if (result != null && result.length > 1) {
			log.info("emv_param_file_transfer_id: " + result[1] + " for terminalId: " + terminalId);
		}
		params.put("fileTransferName", resourceName + "_CR");
		params.put("fileTransferTypeCd", FileType.CARD_READER_EMV_PARAMETER_FILE);
		params.put("fileContent", contents[1]);
		try {
			byte[] contentHash = SecureHash.getUnsaltedHash(contents[1].getBytes());
			params.put("fileContentHash", contentHash);
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException(e);
		}
		result = DataLayerMgr.executeCall("APPLY_FILE_TRANSFER_CONTENT", params, true);
		resource.delete();
		if (result != null && result.length > 1) {
			log.info("card_reader_emv_param_file_transfer_id: " + result[1] + " for terminalId: " + terminalId);
		}
		// still keeping the EMV_PARAM_DOWNLOAD_REQD record so there is some history as to what transpired
	}
	
	private String[] convertToCardReaderFormat(InputStream in) throws IOException, ServiceException {
		StringBuilder kvFormat = new StringBuilder();
		StringBuilder crFormat = new StringBuilder();
		InputStreamReader isr = new InputStreamReader(in);
		BufferedReader br = new BufferedReader(isr);
		String kv;
		while ((kv = br.readLine()) != null) {
			if (kvFormat.length() > 0)
				kvFormat.append('\n');
			kvFormat.append(kv);
			if (crFormat.length() > 0) {
				if (kv.startsWith(KEY_START))
					crFormat.append("\r\n");
				else
					crFormat.append('|');
			}
			String[] kvs = kv.split("=", 2);
			if (kvs.length < 2)
				throw new IllegalStateException("Wrong format of EMV Parameter line: " + kv);
			crFormat.append(kvs[1]);
		}
		if (crFormat.length() == 0)
			throw new IllegalStateException("Card Reader EMV Parameter File is empty!");
		appendInteracFiles(crFormat);
		return new String[] { kvFormat.toString(), crFormat.toString() };
	}

	private void appendInteracFiles(StringBuilder crFile) throws ServiceException {
		Map<String,Object> params = new HashMap<String, Object>();
		String kvContent = null;
		try {
			DataLayerMgr.executeCall("GET_INTERAC_FILE_CONTENT", params, false);
			Blob fileBlob = ConvertUtils.convert(Blob.class, params.get("fileContent"));
			if (fileBlob == null || fileBlob.length() < 1)
				throw new ServiceException("Unable to read Interac file content from the database");
			int blobLen = (int)fileBlob.length();
			kvContent = new String(fileBlob.getBytes(1, blobLen));
			log.info("Interac file KV: " + kvContent);
		} catch (Exception e) {
			throw new ServiceException("Appending Interac CAPK: ", e);
		}
		
		String[] kvLines = kvContent.split("[\\r\\n]");
		for (String kv : kvLines) {
			//log.info("KV line: " + kv);
			if (kv == null || kv.trim().isEmpty())
				continue;

			if (kv.startsWith(KEY_START))
				crFile.append("\r\n");
			else
				crFile.append('|');
			String[] kvs = kv.split("=", 2);
			if (kvs.length < 2)
				throw new IllegalStateException("Wrong format of Interac file line: " + kv);
			crFile.append(kvs[1]);
		}
	}

	private static final int[] prefix = { 5, 1, 1, 1 };
	private static final int[] suffix = { 23, 3, 20 };

	private void appendInteracFilesFromFolder(StringBuilder crFile) throws IOException, ServiceException {
		if (interacFolder == null || interacFolder.isEmpty())
			throw new ServiceException("Interac folder is not specified!");

		Path interacFolderPath = Paths.get(interacFolder);
		if (!Files.exists(interacFolderPath))
			throw new ServiceException("Interac folder " + interacFolder + " does not exist!");
		if (!Files.isDirectory(interacFolderPath))
			throw new ServiceException("Interac folder " + interacFolder + " is not a directory!");

		Stream<Path> pubFilesStream = Files.find(interacFolderPath, 1,
				(path, basicFileAttributes) -> String.valueOf(path).toLowerCase().endsWith(".pub"));
		List<Path> pubFiles = new ArrayList<Path>();
		pubFilesStream.forEach(pf -> {
			log.info("Found PUB file: " + pf);
			pubFiles.add(pf);
		});
		int cnt = pubFiles.size();
		if (cnt == 0)
			throw new ServiceException("No Interac PUB files found in the folder " + interacFolder + "!");

		for (Path pf : pubFiles) {
			log.info("PUB file: " + pf);
			byte[] pfBytes = Files.readAllBytes(pf);
			int pfLen = pfBytes.length;
			if (pfLen < MIN_BYTES)
				throw new ServiceException("Wrong size " + pfLen + " of Interac PUB file " + pf + '!');

			int offset = 0;
			for (int i = 0; i < prefix.length; offset += prefix[i++]) {
				if (i == 0)
					crFile.append("\r\n");
				else
					crFile.append('|');
				crFile.append(StringUtils.toHex(pfBytes, offset, prefix[i]));
			}

			// key modulus
			crFile.append('|').append(StringUtils.toHex(pfBytes, offset, pfLen - offset - suffix[0]));

			offset = pfLen - suffix[0];
			for (int j = 1; j < suffix.length; offset += suffix[j++]) {
				crFile.append('|');
				crFile.append(StringUtils.toHex(pfBytes, offset, suffix[j]));
			}
		}
	}

	/*private final static char[] hexArray = "0123456789ABCDEF".toCharArray();

	public static String bytesToHex(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		for ( int j = 0; j < bytes.length; j++ ) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}*/

	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}

	public String getInteracFolder() {
		return interacFolder;
	}

	public void setInteracFolder(String interacFolder) {
		this.interacFolder = interacFolder;
	}
}
