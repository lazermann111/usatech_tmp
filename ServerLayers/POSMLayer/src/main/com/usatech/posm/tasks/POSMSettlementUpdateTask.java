package com.usatech.posm.tasks;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.posm.constants.ErrorType;
import com.usatech.posm.notification.ErrorNotification;
import com.usatech.posm.notification.ErrorObject;
import com.usatech.posm.utils.POSMUtils;

/**
 * This MessageChainTask is responsible for listening on "posm.terminal.settlement.update" for
 * Settlement response from the Authority Layer.
 *
 * @author Evan, bkrug
 *
 */
public class POSMSettlementUpdateTask extends POSMTask {
    private static final Log log = Log.getLog();
    protected ErrorNotification errorNotification;
    @Override
	protected int processInternal(MessageChainTaskInfo taskInfo) throws ServiceException {
    	MessageChainStep step = taskInfo.getStep();
        Map<String, Object> input = new HashMap<String, Object>(step.getAttributes());
		AuthResultCode authResultCode;
		long processedTime;
		long terminalBatchId;
		Results authResults = null;
		Results refundResults = null;
		try {
			authResultCode = step.getAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.class, true);
			boolean lastAttempt = step.getAttributeDefault(AuthorityAttrEnum.ATTR_LAST_ATTEMPT, Boolean.class, false);
			if(lastAttempt) {
				switch(authResultCode) {
					case DECLINED:
						authResultCode = AuthResultCode.DECLINED_PERMANENT;
						input.put(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue(), authResultCode);
						break;
				}
			}

			Long authorityEndTime = step.getAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_END_TIME, Long.class, false);
			if(authorityEndTime != null && authorityEndTime > 0) {
				processedTime = authorityEndTime;
				input.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS.getValue(), authorityEndTime);
			} else
				processedTime = step.getAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS, Long.class, true);
			terminalBatchId = step.getAttribute(AuthorityAttrEnum.ATTR_TERMINAL_BATCH_ID, Long.class, true);
		} catch(AttributeConversionException e) {
			throw new RetrySpecifiedServiceException("Could not convert attributes as needed", e, WorkRetryType.NO_RETRY);
		}
        try {
			Connection conn = DataLayerMgr.getConnectionForCall("UPDATE_PROCESSED_BATCH");
        	boolean okay = false;
        	try {
	        	checkValidGlobalToken(conn, step);
				// Update auth result cd if necessary
				try {
					DataLayerMgr.executeCall(conn, "UPDATE_PROCESSED_BATCH", input);
					conn.commit();
			        okay = true;
	        	
			        switch(authResultCode) {
						case DECLINED:
						case FAILED:
						case DECLINED_PERMANENT:
						case DECLINED_PAYMENT_METHOD:
		        			//if it is a failed settlement, notify the error
		        	        ErrorNotification en = getErrorNotification();
		        	        if(en != null) {
			        	        ErrorObject error = new ErrorObject();
			        	        error.setErrorType(ErrorType.POSM_ERROR);
			        	        error.setErrorMessage("Settlement for terminal batch (" + terminalBatchId + ") was unsuccessful");
			        	        error.setAdditionalInfo(input);
			        	        en.notifyError(error);
		        	        }
		        	        if (log.isErrorEnabled())
		        	            log.error("Settlement for terminal batch (" +  terminalBatchId + ") was unsuccessful");
		        	        break;
		        	}
				} catch(SQLException e) {
					if(e.getErrorCode() == 20100)
						log.info("Settlement batch was already processed");
					else
						throw e;
				}
				authResults = DataLayerMgr.executeQuery(conn, "GET_AUTHS_IN_BATCH", input);
				refundResults = DataLayerMgr.executeQuery(conn, "GET_REFUNDS_IN_BATCH", input);
		    } finally {
        		if(!okay) try {
        			conn.rollback();
        		} catch(SQLException e) {
        			log.warn("Could not rollback transaction");
        		}
        		conn.close();
        	}
        } catch(SQLException e) {
			throw new ServiceException("Could not update processed settlement", e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not update processed settlement", e);
		}
		
		if(authResultCode != AuthResultCode.PARTIAL) {
			authResults.setRow(0);
			refundResults.setRow(0);
			POSMUtils.publishSettleUpdate(taskInfo.getPublisher(), authResultCode, processedTime, authResults, refundResults);
		}
		return 0;
	}
	public ErrorNotification getErrorNotification() {
		return errorNotification;
	}
	public void setErrorNotification(ErrorNotification errorNotification) {
		this.errorNotification = errorNotification;
	}
}
