package com.usatech.posm.schedule;


import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;

import com.usatech.layers.common.QuartzCronScheduledWithPublisherJob;
import com.usatech.posm.tasks.ReplenishPrepaidTask;

/**
 * Checks if prepaid accounts need replenish and calls replenish
 * 
 * @author bkrug
 * 
 */
@DisallowConcurrentExecution
public class ReplenishPrepaidCheck extends QuartzCronScheduledWithPublisherJob {
	private static final Log log = Log.getLog();
	protected static ReplenishPrepaidTask replenishPrepaidTask;
	
	@Override
	public void executePostConfigure(JobExecutionContext context) throws JobExecutionException {
		try {
			Connection conn = DataLayerMgr.getConnectionForCall("FIND_REPLENISH_NEEDED");
			try {
				Results results = DataLayerMgr.executeQuery(conn, "FIND_REPLENISH_NEEDED", null);
				Map<String, Object> params = new HashMap<String, Object>();
				int replenished = 0;
				while(results.next()) {
					long consumerAcctId = results.getValue("consumerAcctId", Long.class);
					try {
						if(replenishPrepaidTask.checkReplenishCard(consumerAcctId, conn, params, getPublisher()))
							replenished++;
					} catch(SQLException e) {
						log.warn("Error while replenishing prepaid card", e);
						try {
							conn.rollback();
						} catch(SQLException e1) {
							log.warn("Could not rollback prepaid card replenishment", e1);
						}
					} catch(ConvertException e) {
						log.warn("Error while replenishing prepaid card", e);
						try {
							conn.rollback();
						} catch(SQLException e1) {
							log.warn("Could not rollback prepaid card replenishment", e1);
						}
					} catch(DataLayerException e) {
						log.warn("Error while replenishing prepaid card", e);
						try {
							conn.rollback();
						} catch(SQLException e1) {
							log.warn("Could not rollback prepaid card replenishment", e1);
						}
					} catch(ServiceException e) {
						log.warn("Error while replenishing prepaid card", e);
						try {
							conn.rollback();
						} catch(SQLException e1) {
							log.warn("Could not rollback prepaid card replenishment", e1);
						}
					}
					conn.commit();
				}
				log.info("Found " + results.getRowCount() + " accounts that needed replenishment and replenished " + replenished);
			} finally {
				conn.close();
			}
		} catch(SQLException e) {
			log.warn("Could not find accounts to replenish", e);
		} catch(DataLayerException e) {
			log.warn("Could not find accounts to replenish", e);
		} catch(ConvertException e) {
			log.warn("Could not find accounts to replenish", e);
		}
	}

	public static ReplenishPrepaidTask getReplenishPrepaidTask() {
		return replenishPrepaidTask;
	}

	public static void setReplenishPrepaidTask(ReplenishPrepaidTask replenishPrepaidTask) {
		ReplenishPrepaidCheck.replenishPrepaidTask = replenishPrepaidTask;
	}
}
