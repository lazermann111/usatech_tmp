package com.usatech.posm.schedule;

import java.util.HashMap;
import java.util.Map;

import simple.app.BasicQoS;
import simple.app.Publisher;
import simple.app.SelfProcessor;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.ByteInput;
import simple.io.Log;
import simple.results.Results;

import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.constants.AuthorityAttrEnum;

/**
 * Polls the database for long opened batches and posts them
 * to the POSM Check Task for settlement or transaction processing.
 *
 * @author Evan, sjillidimudi, bkrug
 *
 */
public class POSMBatchTask implements SelfProcessor {
	private static final Log log = Log.getLog();

	protected String posmCheckQueue = "usat.posm.terminal.check";
	protected final BasicQoS posmCheckQueueQos = new BasicQoS(300000, 5, false, null);
	protected Publisher<ByteInput> publisher;
	protected int maxBatches = 1000;
	protected long minInactiveSec = 300;

	/**
	 * This method Queries the DataBase for the Terminals whose global token has not been recently released
	 * and which are active (not currently locked). It forwards any that it finds to the POSM Check Task to
	 * do the "real" processing.
	 *
	 */
	@Override
	public void process() {
		log.info("Looking for terminals that have not been updated for " + getMinInactiveSec() + " seconds or more");
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("minInactiveSec", getMinInactiveSec());
		params.put("maxNum", getMaxBatches());
		Results results;
		try {
			results = DataLayerMgr.executeQuery("FIND_TERMINALS_TO_CHECK", params);
		} catch(Exception e) {
			log.error("Unable to retrieve the Terminals that qualify for settlement", e);
			return;
		}
		while(results.next()) {
			try {
				postToCheckQueue(ConvertUtils.getLong(results.get(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_KEY_ID.getValue())), String.valueOf(results.get(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_CLASS.getValue())));
			} catch(ServiceException e) {
				log.error("Unable to post to the Check Queue for Terminal " + results.get(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_KEY_ID.getValue()) + " ("
						+ results.get(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_CLASS.getValue()) + ')', e);
				break;
			} catch(ConvertException e) {
				log.error("Unable to convert attributes for Check Queue for Terminal " + results.get(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_KEY_ID.getValue()) + " ("
						+ results.get(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_CLASS.getValue()) + ')', e);
				break;
			}
			log.info("Posted to the Check Queue for Terminal " + results.get(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_KEY_ID.getValue()) + " ("
						+ results.get(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_CLASS.getValue()) + ')');
		}
		log.info("Found " + (results.getRow() - 1)  + " terminals to check for transactions or batches to settle");		
	}

	/**
	 * Constructs the Message Chain and Posts it to the check Queue.
	 *
	 * @param terminalId
	 *            - Terminal ID to be processed
	 * @param paymentSubtypeClass
	 *            - paymentSubTypeClass which is a part of the lock identifier.
	 * @throws ServiceException
	 *             When the publisher fails to post the message chain.
	 */
	protected void postToCheckQueue(long paymentSubtypeKeyId, String paymentSubtypeClass) throws ServiceException {
		Publisher<ByteInput> publisher = getPublisher();
		if(publisher == null)
			throw new ServiceException("Publisher not set on " + this);
		String queueKey = getPosmCheckQueue();
		if(queueKey == null || (queueKey=queueKey.trim()).length() == 0)
			throw new ServiceException("PosmCheckQueue not set on " + this);
		MessageChainV11 mc = new MessageChainV11();
		MessageChainStep checkStep = mc.addStep(queueKey);
		checkStep.setAttribute(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_KEY_ID, paymentSubtypeKeyId);
		checkStep.setAttribute(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_CLASS, paymentSubtypeClass);
		MessageChainService.publish(mc, publisher, null, getPosmCheckQueueQos());
	}

	public void setPosmCheckQueue(String posmCheckTaskQueue) {
		this.posmCheckQueue = posmCheckTaskQueue;
	}

	public String getPosmCheckQueue() {
		return posmCheckQueue;
	}

	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}

	public Publisher<ByteInput> getPublisher() {
		return publisher;
	}

	public int getMaxBatches() {
		return maxBatches;
	}

	public void setMaxBatches(int maxBatches) {
		this.maxBatches = maxBatches;
	}

	public long getMinInactiveSec() {
		return minInactiveSec;
	}

	public void setMinInactiveSec(long checkInactiveMillis) {
		this.minInactiveSec = checkInactiveMillis;
	}

	public BasicQoS getPosmCheckQueueQos() {
		return posmCheckQueueQos;
	}
}
