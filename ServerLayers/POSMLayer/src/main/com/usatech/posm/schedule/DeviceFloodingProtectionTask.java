package com.usatech.posm.schedule;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.usatech.layers.common.util.DeviceUtils;
import simple.app.SelfProcessor;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;
import simple.util.concurrent.CustomThreadFactory;
import simple.util.concurrent.NotifyingBlockingThreadPoolExecutor;

import com.usatech.layers.common.InteractionUtils;

public class DeviceFloodingProtectionTask implements SelfProcessor {
	private static final Log log = Log.getLog();
	
	protected String DEVICE_FLOODING_PROTECTION = "DEVICE_FLOODING_PROTECTION";

	protected int appInstance;
	protected int poolSize = 5;
	protected int queueSize = 10;
	protected long waitCheckInterval = 5000;
	protected long lockUpdateInterval = 900000;
	protected NotifyingBlockingThreadPoolExecutor threadPoolExecutor = null;

	@Override
	public void process() {
		if (threadPoolExecutor == null)
			threadPoolExecutor = new NotifyingBlockingThreadPoolExecutor(poolSize, queueSize, new CustomThreadFactory(Thread.currentThread().getName() + '-', true));
					
		int floodMessageCountThreshold, floodRejectionSec;
		Map<String, Object> params = new HashMap<String, Object>();
		try {
			params.put("appSettingCd", "DEVICE_FLOOD_MSG_CNT_THRESHOLD");
			DataLayerMgr.selectInto("GET_APP_SETTING_VALUE", params);
			floodMessageCountThreshold = ConvertUtils.getInt(params.get("appSettingValue"));
			
			params.put("appSettingCd", "DEVICE_FLOOD_REJECTION_SEC");
			DataLayerMgr.selectInto("GET_APP_SETTING_VALUE", params);
			floodRejectionSec = ConvertUtils.getInt(params.get("appSettingValue"));
		} catch (Exception e) {
			log.error("Error getting DEVICE_FLOOD_MSG_CNT_THRESHOLD or DEVICE_FLOOD_REJECTION_SEC app setting", e);
			return;
		}
		
		params.put("floodMessageCountThreshold", floodMessageCountThreshold);		
		long startTs = System.currentTimeMillis();
		int itemCount = 0;
        Results results = null;        
        try {
        	if (!InteractionUtils.lockProcess(DEVICE_FLOODING_PROTECTION, String.valueOf(appInstance), params)) {
        		log.info("Device Flooding Protection is already locked by instance " + params.get("lockedBy"));
        		return;
        	}
        	long lockUpdatedTs = System.currentTimeMillis();
        	
			results = DataLayerMgr.executeQuery("GET_FLOODING_DEVICES", params);			
            while(results.next()) {
            	itemCount++;
            	
            	if (System.currentTimeMillis() - lockUpdatedTs > lockUpdateInterval) {
            		if (!InteractionUtils.lockProcess(DEVICE_FLOODING_PROTECTION, String.valueOf(appInstance), params)) {
                		log.error("Device Flooding Protection got locked by instance " + params.get("lockedBy") + " during processing, exiting");
                		threadPoolExecutor.shutdownNow();
                		return;
            		}
            		lockUpdatedTs = System.currentTimeMillis();
            	}
            	
            	Map<String,Object> attributes = new HashMap<String, Object>();
            	attributes.put("floodRejectionSec", floodRejectionSec);
            	attributes.put("deviceSerialCd", results.get("deviceSerialCd"));                
            	threadPoolExecutor.execute(new ThreadTask(attributes));
            }

			if (itemCount > 0) {
				while (threadPoolExecutor.getActiveCount() > 0)
					threadPoolExecutor.await(waitCheckInterval, TimeUnit.MILLISECONDS);
				log.info("Processed " + itemCount + " flooding device(s) in " + (System.currentTimeMillis() - startTs) + " ms");
			} else
        		log.info("No flooding devices found");
	    } catch(Exception e) {
	    	log.error("Error executing device flooding protection", e);
        } finally {
        	if (results != null)
	    		results.close();
        	try {
        		InteractionUtils.unlockProcess(DEVICE_FLOODING_PROTECTION, String.valueOf(appInstance), params);
        	} catch (Exception e) {
        		log.error("Error unlocking " + DEVICE_FLOODING_PROTECTION, e);
        	}
        }
	}
	
	protected class ThreadTask implements Runnable {
		protected Map<String,Object> attributes;
		
		public ThreadTask(Map<String,Object> attributes) {
			this.attributes = attributes;
		}
		
		public void run() {
			try {
				String deviceSerialCd = (String)attributes.get("deviceSerialCd");
				String activeG9EdgeDeviceName = getG9EdgeDeviceName(deviceSerialCd);
				if (activeG9EdgeDeviceName != null) {
					// This is G9/Edge device
					scheduleG9EdgeDeviceRebootAsap(activeG9EdgeDeviceName);
				}
				DataLayerMgr.executeUpdate("HANDLE_FLOODING_DEVICE", attributes, true);
			} catch(Exception e) {
				log.error("Error handling flooding device " + attributes.get("deviceName"), e);				
			}
		}
	}

	String getG9EdgeDeviceName(String g9EdgeDeviceSerialCd) throws SQLException, DataLayerException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("deviceSerialCd", g9EdgeDeviceSerialCd);
		Results results = DataLayerMgr.executeQuery("GET_ACTIVE_G9EDGE_DEVICE_NAME", params);
		if (results != null && results.next()) {
			return results.getFormattedValue("ACTIVE_G9EDGE_DEVICE_NAME");
		}
		return null;
	}

	void scheduleG9EdgeDeviceRebootAsap(String evNumber) throws SQLException, DataLayerException {
		String dataType = DeviceUtils.EPORT_CMD_GENERIC_RESPONSE_V4_1;
		String cmdRebootAndInitAfter300Seconds = "000005012C";
		String pendingCmdExecuteCode = "P";
		int runAsapOrder = -9;
		DeviceUtils.createPendingCommand(null,
										evNumber,
										dataType,
										cmdRebootAndInitAfter300Seconds,
										pendingCmdExecuteCode,
										runAsapOrder,
										true);
	}

	public int getAppInstance() {
		return appInstance;
	}

	public void setAppInstance(int appInstance) {
		this.appInstance = appInstance;
	}
	
	public int getPoolSize() {
		return poolSize;
	}

	public void setPoolSize(int poolSize) {
		this.poolSize = poolSize;
	}

	public int getQueueSize() {
		return queueSize;
	}

	public void setQueueSize(int queueSize) {
		this.queueSize = queueSize;
	}	

	public long getWaitCheckInterval() {
		return waitCheckInterval;
	}

	public void setWaitCheckInterval(long waitCheckInterval) {
		this.waitCheckInterval = waitCheckInterval;
	}
	
	public long getLockUpdateInterval() {
		return lockUpdateInterval;
	}

	public void setLockUpdateInterval(long lockUpdateInterval) {
		this.lockUpdateInterval = lockUpdateInterval;
	}	
}
