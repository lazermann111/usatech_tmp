package com.usatech.posm.schedule;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.util.StringHelper;

import simple.app.Publisher;
import simple.app.SelfProcessor;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.ByteInput;
import simple.io.Log;
import simple.results.Results;
import simple.util.concurrent.CustomThreadFactory;
import simple.util.concurrent.NotifyingBlockingThreadPoolExecutor;

public class EFTPropagatorTask implements SelfProcessor {
	private static final Log log = Log.getLog();
	
	protected String EFT_PROPAGATION = "EFT_PROPAGATION";
	protected String eftProcessQueue = "usat.posm.eft.process";
	protected Publisher<ByteInput> publisher;

	protected int appInstance;
	protected int poolSize = 5;
	protected int queueSize = 10;
	protected long waitCheckInterval = 5000;
	protected long lockUpdateInterval = 900000;
	protected NotifyingBlockingThreadPoolExecutor threadPoolExecutor = null;
	protected String currencyIds = "1,2";
	protected BigDecimal minEftAmount = new BigDecimal(25);
	protected BigDecimal maxEftAmount = new BigDecimal(1000000);
	protected AtomicInteger errorCount = new AtomicInteger(0);

	@Override
	public void process() {
		if (threadPoolExecutor == null)
			threadPoolExecutor = new NotifyingBlockingThreadPoolExecutor(poolSize, queueSize, new CustomThreadFactory(Thread.currentThread().getName() + '-', true));
				
        Results results = null;
        Map<String,Object> params = new HashMap<String, Object>();
        int itemCount = 0;
        try {
        	if (!InteractionUtils.lockProcess(EFT_PROPAGATION, String.valueOf(appInstance), params)) {
        		log.info("EFT Propagation is already locked by instance " + params.get("lockedBy"));
        		return;
        	}
        	long lockUpdatedTs = System.currentTimeMillis();
        	
        	long startTs = System.currentTimeMillis();
        	log.info("Running Scan for Service Fees");
        	DataLayerMgr.executeUpdate("SCAN_FOR_SERVICE_FEES", null, true);
        	log.info("Scan for Service Fees took " + (System.currentTimeMillis() - startTs) + " ms");
        	
        	if (System.currentTimeMillis() - lockUpdatedTs > lockUpdateInterval) {
        		if (!updateProcessLock(params))
            		return;
        		lockUpdatedTs = System.currentTimeMillis();
        	}
        	
        	startTs = System.currentTimeMillis();
        	DataLayerMgr.selectInto("CHECK_EFT_PROPAGATION", params);
        	if ("Y".equalsIgnoreCase(ConvertUtils.getStringSafely(params.get("startEFTPropagation"))))
        		log.info("Starting EFT Propagation, Scan for Service Fees completed at " + ConvertUtils.getStringSafely(params.get("feeScanLastRunCompleteTs")));
        	else {
        		log.info("EFT Propagation is not needed, last EFT Propagation completed at " + ConvertUtils.getStringSafely(params.get("lastRunCompleteTs"))
        				+ ", last Scan for Service Fees completed at " + ConvertUtils.getStringSafely(params.get("feeScanLastRunCompleteTs")));
        		return;
        	}
        	
        	DataLayerMgr.executeUpdate("START_EFT_PROPAGATION", null, true);
        	
        	Map<String,Object> queryParams = new HashMap<String, Object>();
        	queryParams.put("currencyIds", currencyIds);
			results = DataLayerMgr.executeQuery("GET_UNPROCESSED_EFTS", queryParams);
            while(results.next()) {
            	if (System.currentTimeMillis() - lockUpdatedTs > lockUpdateInterval) {
            		if (!updateProcessLock(params))
                		return;
            		lockUpdatedTs = System.currentTimeMillis();
            	}
            	
            	Map<String,Object> attributes = new HashMap<String, Object>();
            	attributes.put("eftId", results.get("eftId"));	                
            	threadPoolExecutor.execute(new ThreadTask(attributes));
            	itemCount++;
            }

			if (itemCount > 0) {
				while (threadPoolExecutor.getActiveCount() > 0) {
					threadPoolExecutor.await(waitCheckInterval, TimeUnit.MILLISECONDS);					
					if (System.currentTimeMillis() - lockUpdatedTs > lockUpdateInterval) {
	            		if (!updateProcessLock(params))
	                		return;
	            		lockUpdatedTs = System.currentTimeMillis();
	            	}
				}
			}
			
			if(errorCount.get() == 0) {
				DataLayerMgr.executeUpdate("COMPLETE_EFT_PROPAGATION", null, true);
				DataLayerMgr.executeUpdate("CHECK_EFT_PROCESS_COMPLETION", null, true);
			}
			if (itemCount > 0)
				log.info("Propagated " + itemCount + " EFT(s) in " + (System.currentTimeMillis() - startTs) + " ms, errors encountered: " + errorCount.get());
			else
				log.info("No EFTs found for propagation, operation took " + (System.currentTimeMillis() - startTs) + " ms");
	    } catch(Exception e) {
	    	log.error("Error propagating EFTs", e);
        } finally {
        	if (results != null)
	    		results.close();
        	try {
        		InteractionUtils.unlockProcess(EFT_PROPAGATION, String.valueOf(appInstance), params);
        	} catch (Exception e) {
        		log.error("Error unlocking " + EFT_PROPAGATION, e);
        	}        	
        }
	}
	
	protected boolean updateProcessLock(Map<String,Object> params) throws ConvertException, DataLayerException, SQLException {
		if (!InteractionUtils.lockProcess(EFT_PROPAGATION, String.valueOf(appInstance), params)) {
    		log.error("EFT Propagation got locked by instance " + params.get("lockedBy") + " during processing, exiting");
    		threadPoolExecutor.shutdownNow();
    		return false;
		}
		return true;
	}
	
	protected class ThreadTask implements Runnable {
		protected Map<String,Object> attributes;
		
		public ThreadTask(Map<String,Object> attributes) {
			this.attributes = attributes;
		}
		
		public void run() {
			long eftId = -1;
			try {
				eftId = ConvertUtils.convert(long.class, attributes.get("eftId"));
				
				Results results = DataLayerMgr.executeQuery("GET_PENDING_EFT_INFO", attributes);
				if (results.next()) {
					BigDecimal netAmount = results.getValue("netAmount", BigDecimal.class);
					if (netAmount.compareTo(minEftAmount) < 0) {
						log.info("The net amount " + netAmount + " of EFT " + eftId + " is less than " + minEftAmount + ", not processing");
						return;
					}
					if (netAmount.compareTo(maxEftAmount) > 0) {
						log.info("The net amount " + netAmount + " of EFT " + eftId + " is greater than " + maxEftAmount + ", not processing");
						return;
					}
				}else{
					log.info("GET_PENDING_EFT_INFO returns no result for eftId="+eftId+", not processing");
					return;
				}
				
				//publish to EFT Process task
				String queueKey = getEftProcessQueue();
				if(StringHelper.isBlank(queueKey))
					log.warn("eftProcessQueue not set on " + this);
				else {
					DataLayerMgr.executeUpdate("START_EFT_PROCESSING", attributes, true);
					MessageChainV11 mc = new MessageChainV11();
					MessageChainStep eftStep = mc.addStep(queueKey);
					eftStep.setAttribute(CommonAttrEnum.ATTR_EFT_ID, attributes.get("eftId"));
					MessageChainService.publish(mc, publisher);
					log.info("Enqueued EFT " + eftId + " for processing");
				}
			} catch(Exception e) {
				errorCount.incrementAndGet();
				log.error("Error propagating EFT " + eftId, e);				
			}
		}
	}	

	public int getAppInstance() {
		return appInstance;
	}

	public void setAppInstance(int appInstance) {
		this.appInstance = appInstance;
	}	
	
	public int getPoolSize() {
		return poolSize;
	}

	public void setPoolSize(int poolSize) {
		this.poolSize = poolSize;
	}

	public int getQueueSize() {
		return queueSize;
	}

	public void setQueueSize(int queueSize) {
		this.queueSize = queueSize;
	}
	
	public String getEftProcessQueue() {
		return eftProcessQueue;
	}

	public void setEftProcessQueue(String eftProcessQueue) {
		this.eftProcessQueue = eftProcessQueue;
	}
	
	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}

	public Publisher<ByteInput> getPublisher() {
		return publisher;
	}

	public long getWaitCheckInterval() {
		return waitCheckInterval;
	}

	public void setWaitCheckInterval(long waitCheckInterval) {
		this.waitCheckInterval = waitCheckInterval;
	}
	
	public long getLockUpdateInterval() {
		return lockUpdateInterval;
	}

	public void setLockUpdateInterval(long lockUpdateInterval) {
		this.lockUpdateInterval = lockUpdateInterval;
	}

	public String getCurrencyIds() {
		return currencyIds;
	}

	public void setCurrencyIds(String currencyIds) {
		this.currencyIds = currencyIds;
	}

	public BigDecimal getMinEftAmount() {
		return minEftAmount;
	}

	public void setMinEftAmount(BigDecimal minEftAmount) {
		this.minEftAmount = minEftAmount;
	}

	public BigDecimal getMaxEftAmount() {
		return maxEftAmount;
	}

	public void setMaxEftAmount(BigDecimal maxEftAmount) {
		this.maxEftAmount = maxEftAmount;
	}
}
