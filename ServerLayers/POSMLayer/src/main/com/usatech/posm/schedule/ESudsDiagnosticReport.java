/**
 * 
 */
package com.usatech.posm.schedule;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.io.IOUtils;
import simple.io.Log;
import simple.mail.Email;
import simple.mail.Emailer;
import simple.text.StringUtils;

import com.usatech.layers.common.QuartzCronScheduledJob;
import com.usatech.server.esuds.util.OperatorDiagnosticReport;

/**
 * 
 * This implementation will use the ESuds Diagnostic Report code
 * 
 * Configuration specifies the Cron schedule
 * QuartzScheduledService takes care of configuring Quartz
 * 
 * We just react to the 'execute' API call here.
 * 
 * @author phorsfield
 *
 */
@DisallowConcurrentExecution
public class ESudsDiagnosticReport extends QuartzCronScheduledJob {
	private static Log log = Log.getLog();

	protected class ReportMessage
	{
		protected int customerID;
		protected String toEmailString;
		protected String subject;
		protected String textBody;
		protected String htmlBody;
		protected String attachment;
		protected String attachmentName;
		protected boolean success;
		
		protected void send() throws MessagingException
		{
			MessagingException storedException = null;
			if(success)
			{
				// if generation was successful send the message to the operator
				// but let's send each individually 
				for(InternetAddress toAddress : InternetAddress.parse(toEmailString))
				{
					try {
						sendTo(toAddress, bccEmailStr);
					} 
					catch(MessagingException e)
					{
						if(storedException != null)
						{
							storedException = e;
						}
						// attempt to send the next anyway
					}
				}
			}
			else
			{
				for(InternetAddress toAddress : InternetAddress.parse(adminEmailStr))
				{
					try {
						sendTo(toAddress, null);
					}
					catch(MessagingException e)
					{
						if(storedException != null)
						{
							storedException = e;
						}
						// attempt to send the next anyway
					}
				}				
			}
			
			if(storedException != null) throw storedException;
		}

		private void sendTo(InternetAddress toAddress, String bccEmailStr) throws MessagingException {
			Email email = smtp.createEmail();
			email.setTo(toAddress.toString());
			if(bccEmailStr != null) 
			{
				email.setBcc(bccEmailStr);
			}
			email.setSubject(subject);
			email.setBody(textBody);
			email.setBodyHtml(htmlBody);
			email.addAttachment(attachment, "text/csv", attachmentName);
			email.send();			
		}
	}

	private Properties props = new Properties();
	private final static String	LINE_SEPARATOR = System.getProperty("line.separator");

	protected String 			adminEmailStr 	= "";
	protected String 			bccEmailStr 	= "";
	protected List<Integer> 	customerIDs		= new ArrayList<Integer>();
	private final DateFormat fileDateFormat = new SimpleDateFormat("MMddyyyy");
	protected int 				reportDays		= -1;
	protected Emailer		 	smtp;
	private final DateFormat subjectDateFormat = new SimpleDateFormat("EEE, MMM dd, yyyy");
	protected String			templateResource;
	private final DateFormat timeStampDateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG);

	@SuppressWarnings("unchecked")
	@Override
	public void executePostConfigure(JobExecutionContext context)
			throws JobExecutionException {
			
		JobDataMap quartzJobMap = context.getMergedJobDataMap();
		
		props.putAll(quartzJobMap.getWrappedMap()); // unchecked		
		
		try {
			StringBuilder textBody;
			
			log.info("eSuds DiagnosticReportSender is Running @ " + timeStampDateFormat.format(new Date()));
			
			if(!readConfiguration(props))
			{
				throw new simple.app.ServiceException("Invalid eSuds Diagnostic Report configuration in .properties file");
			}
			
			try
			{
				textBody = getTextBody(templateResource, reportDays);
			}
			catch(IOException e)
			{
				throw new simple.app.ServiceException("Invalid eSuds Diagnostic Report template file");
			}
			
			String subjectDateStamp = subjectDateFormat.format(new Date());
			String fileDateStamp = fileDateFormat.format(new Date());
			
			log.debug("Report datestamp: " + subjectDateStamp);
							
			// create a list to store the result of each report generation for sending after all reports have been generated
			List<ReportMessage> messages = new ArrayList<ReportMessage>();
			
			// keep a count of errors encountered so we know how bad the issue is
			int errorCount = 0;
			
			// now iterate through each customer id
			for(int customerID : customerIDs)
			{
				ReportMessage reportMessage = new ReportMessage();
				reportMessage.customerID = customerID;
				
				// load the list of emails to send to for this customer expect abc <email@x.y>[,<email@x.y>]*
				String emailsStr = ConvertUtils.getStringSafely(props.get("customer." + customerID + ".email.addresses"));
				reportMessage.toEmailString = emailsStr;
				
				// generate the report
				try
				{
					log.debug(customerID + ":\t" + "Generating Operator Diagnostic Report...");
					OperatorDiagnosticReport rept = new OperatorDiagnosticReport(customerID, reportDays);
					log.debug(customerID + ":\t" + "Report generated successfully!");
			
					reportMessage.success = true;
					reportMessage.subject = "eSuds Operator Daily Diagnostic Report for " + subjectDateStamp;
					reportMessage.textBody = textBody.toString();
					reportMessage.htmlBody = rept.getResultsAsHTML();
					reportMessage.attachmentName = "esuds-diag-" + fileDateStamp + ".csv";
					reportMessage.attachment = rept.getResultsAsCSV();
					
					messages.add(reportMessage);
				}
				catch(Exception e)
				{
					errorCount++;
					
					// there was a problem generating the report
					
					log.error(customerID + ":\t" + "Report generation failed: " + e.getMessage(), e);
			
					StringBuilder sb = new StringBuilder();
					sb.append("Operator Diagnostic Report Generation Failed for customer " + customerID + "!" + LINE_SEPARATOR);
					sb.append(e.getLocalizedMessage() + LINE_SEPARATOR);
					sb.append(StringUtils.exceptionToString(e));
					
					alertAdmin(sb.toString());
				}
			}
			
			if(errorCount == customerIDs.size())
			{
				// there were no successful reports generated... must be a major problem like a database outage
				// notify administrators only
				
				log.fatal("No reports were generated successfully!  Notifying admin...");
				alertAdmin("No reports were generated successfully!  Please check the logs.");		
			}
			
			log.debug("Sending " + messages.size() + " reports...");
			
			// initialize the SMTP connection; do this after report generation so the connection doesn't get closed due to
			// a timeout while we're querying the database to generate report which can take a while...
			try
			{
				smtp = new Emailer(props);
			}
			catch(Exception e)
			{
				log.fatal("Failed to connect to SMTP server!", e);
							
				alertAdmin("No reports were generated successfully!  Please check the logs.");			
			}
			
			
			for(ReportMessage message : messages)
				message.send();
					
		} catch (ServiceException e) {
			throw new JobExecutionException("Failed to execute eSudsDiagnosticReport", e);
		} catch (MessagingException e) {
			throw new JobExecutionException("Failed to send one or more email reports!", e);
		} 
	}

	private void alertAdmin(String body) throws MessagingException
	{
		if(smtp == null)
		{
			try
			{
				Properties prop = new Properties();
				prop.put("mail.from","SoftwareDevelopmentTeam@usatech.com");
				prop.put("mail.smtp.host", "mailhost");
				smtp = new Emailer(prop); /* Default email properties used */
				
			}
			catch(Exception e)
			{
				log.fatal("Failed to connect to default SMTP server!", e);
			}
		}
		
		Email email = smtp.createEmail();
		email.setTo(adminEmailStr);
		email.setSubject("Fatal Error Generating eSuds Operator Diagnostic Reports!");
		email.setBody(body);
	}

	public String getTemplateResource() {
		return templateResource;
	}

	/**
	 * @param templateResource If null, we read a resource named eSudsDiagnosticEmail.template 
	 * @param reportDays
	 * @return
	 * @throws IOException 
	 */
	private StringBuilder getTextBody(String templateResource, int reportDays) throws IOException {
		
		StringBuilder textBody;
		textBody = new StringBuilder();
	
		if(templateResource != null)
		{
			try 
			{
				textBody.append(IOUtils.readFullyNIO(new FileReader(templateResource)));
			}
			catch(IOException e)
			{
				log.warn("Could not load eSuds Diagnostic Report email template, falling back on internal resource.", e);
			}
		}
	
		if(textBody.length() == 0)
		{
			InputStream is = ClassLoader.getSystemResourceAsStream("eSudsDiagnosticEmail.template");
			InputStreamReader reader = new InputStreamReader(is);
			IOUtils.copy(reader, textBody);			
		}
		
		int rptDaysIdx = textBody.indexOf("${reportDays}");
		
		if(rptDaysIdx != -1)
		{
			textBody.replace(rptDaysIdx, rptDaysIdx+13, Integer.toString(reportDays));
		}
		
		return textBody;		
	}

	/**
	 * TODO - change to DI
	 * @param props Property configuration
	 * @return false if no good configuration could be loaded 
	 */
	private boolean readConfiguration(Properties props) {
	
		// N.B. our smtp server and port and from address are already directly in the Job's properties
		
		reportDays = ConvertUtils.getIntSafely(props.get("report.days"),1);
	
		// load the list of admin emails
		adminEmailStr = ConvertUtils.getStringSafely(props.get("admin.email.addresses"), "");
	
		// load the list of BCC emails  
		bccEmailStr = ConvertUtils.getStringSafely(props.get("bcc.email.addresses"), "");
	
		// CC is now unsupported due to questionable value.
		
		// load the list of customer ids
		String idsStr = ConvertUtils.getStringSafely(props.get("customer.ids"), "");
		Scanner idsScanner = new Scanner(idsStr);
		idsScanner.useDelimiter(",");
		
		customerIDs = new ArrayList<Integer>();
		while(idsScanner.hasNextInt())
		{
			int customerID = idsScanner.nextInt();
			log.debug("Loading customer ID: "+customerID);
			customerIDs.add(customerID);
		}
		
		return customerIDs.size() != 0;
	}

	public void setTemplateResource(String templateResource) {
		this.templateResource = templateResource;
	}

}
