package com.usatech.posm.schedule.dfr;

import simple.util.concurrent.ThreadPoolExecutorMXBean;

public interface ParseFinancialDFRTaskMXBean extends ThreadPoolExecutorMXBean, DFRParsingMXBean {
	public int getQueueSize();
}
