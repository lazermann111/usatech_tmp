package com.usatech.posm.schedule;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.management.ManagementFactory;
import java.rmi.RemoteException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import simple.app.Publisher;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.NotEnoughRowsException;
import simple.io.ByteArrayUtils;
import simple.io.ByteInput;
import simple.io.ByteOutput;
import simple.io.Log;
import simple.io.OutputStreamByteOutput;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.mq.app.AnyTwoInstanceWorkQueue;
import simple.results.DatasetUtils;
import simple.results.Results;
import simple.security.SecureHash;
import simple.security.crypt.EncryptionInfoMapping;
import simple.text.StringUtils;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.Cryption;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.util.Tallier;

@DisallowConcurrentExecution
public class PopulateTranAccountJob implements Job, PopulateTranAccountMXBean {
	private static final Log log = Log.getLog();
	protected static final String LOCK_PROCESS_TOKEN_CD = ManagementFactory.getRuntimeMXBean().getName() + ':' + Long.toHexString(System.currentTimeMillis()).toUpperCase() + ':' + Long.toHexString(Double.doubleToLongBits(Math.random()));
	protected static final String PROCESS_CD = "POPULATE_TRAN_ACCOUNT";
	protected static final char[] MASK_CHARS = "*@".toCharArray();
	protected ResourceFolder resourceFolder;
	protected String massCreateAccountQueueKey;
	protected String mainUpdateQueueKey;
	protected String massUpdateAccountQueueKey;
	protected String massLookupAccountQueueKey;
	protected String replenishUpdateQueueKey;
	protected String massRetrieveQueue="usat.keymanager.retrieve.enmasse";
	protected String massUpdateExpDateQueueKey="usat.load.account.expdate.update";
	protected final Cryption keyMgrCryption = new Cryption();
	protected EncryptionInfoMapping encryptionInfoMapping;
	protected int maxBatchSize = 1000;
	protected int maxBatches = 1000;
	protected Publisher<ByteInput> publisher;
	protected static final String[] OUTPUT_HEADER = new String[] { AuthorityAttrEnum.ATTR_TRAN_ID.getValue(), AuthorityAttrEnum.ATTR_ACCOUNT_CD_HASH.getValue() };
	protected static final String[] sensitiveRetrieveColumnNames = { AuthorityAttrEnum.ATTR_ENCRYPT_KEY_LIST.getValue() };
	public static final ObjectName JMX_OBJECT_NAME;
	static {
		ObjectName on;
		try {
			on = new ObjectName("AppJobs:Name=PopulateTranAccount");
		} catch(MalformedObjectNameException e) {
			log.error(e);
			on = null;
		} catch(NullPointerException e) {
			log.error(e);
			on = null;
		}
		JMX_OBJECT_NAME = on;
	}
	protected static PopulateTranAccountJob instance;
	protected boolean jmxEnabled;

	public static PopulateTranAccountJob getInstance() {
		if(instance == null)
			instance = new PopulateTranAccountJob();
		return instance;
	}
	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {
			processBatches(getMaxBatchSize(), getMaxBatches(), false);
		} catch(IllegalStateException e) {
			throw new JobExecutionException(e);
		} catch(SQLException e) {
			throw new JobExecutionException(e);
		} catch(DataLayerException e) {
			throw new JobExecutionException(e);
		} catch(IOException e) {
			throw new JobExecutionException(e);
		} catch(GeneralSecurityException e) {
			throw new JobExecutionException(e);
		} catch(ConvertException e) {
			throw new JobExecutionException(e);
		} catch(ServiceException e) {
			throw new JobExecutionException(e);
		}
	}
	
	@Override
	public void processMissingExpDate() throws RemoteException{
		try {
			final Results results = DataLayerMgr.executeQuery("GET_ACTIVE_REPLEN_CONSUMER_ACCT", null);
			if(!results.isGroupEnding(0)) {
				if(resourceFolder == null)
					throw new IllegalStateException("ResourceFolder is not set");
				if(publisher == null)
					throw new IllegalStateException("Publisher is not set");
				Resource resource = resourceFolder.getResource("KeyMgrAccountRetrive_" + System.currentTimeMillis(), ResourceMode.CREATE);
				boolean okay = false;
				try {
					byte[] encryptionKey = keyMgrCryption.generateKey();
					MessageChain messageChain = new MessageChainV11();
					MessageChainStep massRetrieveStep = messageChain.addStep(getMassRetrieveQueue());
					massRetrieveStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_RESOURCE, resource.getKey());
					massRetrieveStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_ENCRYPTION_KEY, encryptionKey);
					massRetrieveStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_CIPHER, keyMgrCryption.getCipherName());
					massRetrieveStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_BLOCK_SIZE, keyMgrCryption.getBlockSize());
					massRetrieveStep.setAttribute(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT, 2);
					massRetrieveStep.setAttribute(CommonAttrEnum.ATTR_DECRYPTED_ENCODING, "US-ASCII");
					
					OutputStream sensitiveRetrieveCos = null;
					sensitiveRetrieveCos = keyMgrCryption.createEncryptingOutputStream(encryptionKey, resource.getOutputStream());
					ByteOutput sensitiveRetrieveOutput = new OutputStreamByteOutput(sensitiveRetrieveCos);
					final Tallier<String> picker = new Tallier<String>(String.class);
					DatasetUtils.writeDataset(sensitiveRetrieveOutput, results, new Runnable() {
						public void run() {
							String encryptedIdList;
							try {
								encryptedIdList = results.getValue(AuthorityAttrEnum.ATTR_ENCRYPT_KEY_LIST.getValue(), String.class);
							} catch(ConvertException e) {
								log.warn("Could not get card key from results", e);
								return;
							}
							Map<String, Long> parsedMap = new HashMap<String, Long>();
							try {
								InteractionUtils.parseCardIdList(encryptedIdList, parsedMap);
							} catch(ParseException e) {
								log.warn("Could not parse card key '" + encryptedIdList + "'", e);
								return;
							}
							picker.vote(parsedMap.keySet());
						}
					});
					if(resource != null) {
						sensitiveRetrieveCos.flush();
					}
					sensitiveRetrieveCos.close();
					
					MessageChainStep expDateUpdateStep = messageChain.addStep(getMassUpdateExpDateQueueKey());
					expDateUpdateStep.setReferenceAttribute(CommonAttrEnum.ATTR_CRYPTO_RESOURCE, massRetrieveStep, CommonAttrEnum.ATTR_CRYPTO_RESOURCE);
					expDateUpdateStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_ENCRYPTION_KEY, encryptionKey);
					expDateUpdateStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_CIPHER, keyMgrCryption.getCipherName());
					expDateUpdateStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_BLOCK_SIZE, keyMgrCryption.getBlockSize());
					
					massRetrieveStep.setNextSteps(0, expDateUpdateStep);
					massRetrieveStep.setNextSteps(1, massRetrieveStep);


					MessageChainStep deleteFileStep = messageChain.addStep("usat.file.resource.delete");
					deleteFileStep.setAttribute(CommonAttrEnum.ATTR_RESOURCE, resource.getKey());
					deleteFileStep.setJoined(true);
					deleteFileStep.addWaitFor(expDateUpdateStep);
					expDateUpdateStep.setNextSteps(0, deleteFileStep);
					massRetrieveStep.setQueueKey(AnyTwoInstanceWorkQueue.constructQueueName(massRetrieveStep.getQueueKey(), picker.getWinners(2)));
					resource.release();
					MessageChainService.publish(messageChain, publisher, encryptionInfoMapping);
					okay = true;
				} finally {
					if(!okay) {
						resource.delete();
						resource.release();
					}
					results.close();
				}
			}
		}catch(Exception e){
			log.warn("Could not process missing expiration date", e);
			throw new RemoteException("Error", e);
		} 
	}

	@Override
	public int processMissing(int batchSize) throws RemoteException {
		try {
			return internalProcessMissing(batchSize);
		} catch(Exception e) {
			log.warn("Could not process missing", e);
			throw new RemoteException("Error", e);
		}
	}

	protected int internalProcessMissing(int batchSize) throws SQLException, DataLayerException, IOException, GeneralSecurityException, ServiceException {
			final Map<String, Object> params = new HashMap<String, Object>();
			params.put("maxRows", batchSize);
			final Results results = DataLayerMgr.executeQuery("GET_MISSING_REPLEN_CONSUMER_ACCT", params);
			try {
				if(!results.isGroupEnding(0)) {
					if(resourceFolder == null)
						throw new IllegalStateException("ResourceFolder is not set");
					if(publisher == null)
						throw new IllegalStateException("Publisher is not set");

					Resource resource = resourceFolder.getResource("KeyMgrAccountLookup_" + System.currentTimeMillis(), ResourceMode.CREATE);
					boolean okay = false;
					try {
						byte[] encryptionKey = keyMgrCryption.generateKey();
						MessageChain messageChain = new MessageChainV11();
						MessageChainStep massLookupStep = messageChain.addStep(getMassLookupAccountQueueKey());
						massLookupStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_RESOURCE, resource.getKey());
						massLookupStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_ENCRYPTION_KEY, encryptionKey);
						massLookupStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_CIPHER, keyMgrCryption.getCipherName());
						massLookupStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_BLOCK_SIZE, keyMgrCryption.getBlockSize());

						MessageChainStep replenishUpdateStep = messageChain.addStep(getReplenishUpdateQueueKey());
						replenishUpdateStep.setReferenceAttribute(CommonAttrEnum.ATTR_CRYPTO_RESOURCE, massLookupStep, CommonAttrEnum.ATTR_CRYPTO_RESOURCE);
						replenishUpdateStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_ENCRYPTION_KEY, encryptionKey);
						replenishUpdateStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_CIPHER, keyMgrCryption.getCipherName());
						replenishUpdateStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_BLOCK_SIZE, keyMgrCryption.getBlockSize());

						MessageChainStep massUpdateStep = messageChain.addStep(getMassUpdateAccountQueueKey(), true);
						massUpdateStep.setReferenceAttribute(CommonAttrEnum.ATTR_CRYPTO_RESOURCE, massLookupStep, CommonAttrEnum.ATTR_CRYPTO_RESOURCE);
						massUpdateStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_ENCRYPTION_KEY, encryptionKey);
						massUpdateStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_CIPHER, keyMgrCryption.getCipherName());
						massUpdateStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_BLOCK_SIZE, keyMgrCryption.getBlockSize());
						massUpdateStep.setAttribute(CommonAttrEnum.ATTR_SKIP_ON_SAME_INSTANCE, true);

						massLookupStep.setNextSteps(0, replenishUpdateStep, massUpdateStep);
						massLookupStep.setNextSteps(1, massLookupStep);

						MessageChainStep deleteFileStep = messageChain.addStep("usat.file.resource.delete");
						deleteFileStep.setAttribute(CommonAttrEnum.ATTR_RESOURCE, resource.getKey());
						deleteFileStep.setJoined(true);
						deleteFileStep.addWaitFor(massUpdateStep);
						deleteFileStep.addWaitFor(replenishUpdateStep);
						massUpdateStep.setNextSteps(0, deleteFileStep);
						replenishUpdateStep.setNextSteps(0, deleteFileStep);

						OutputStream out = keyMgrCryption.createEncryptingOutputStream(encryptionKey, resource.getOutputStream());
						ByteOutput output = new OutputStreamByteOutput(out);
						final Tallier<String> picker = new Tallier<String>(String.class);
						DatasetUtils.writeDataset(output, results, new Runnable() {
							public void run() {
								String encryptedIdList;
								try {
									encryptedIdList = results.getValue(AuthorityAttrEnum.ATTR_ENCRYPT_KEY_LIST.getValue(), String.class);
								} catch(ConvertException e) {
									log.warn("Could not get card key from results", e);
									return;
								}
								Map<String, Long> parsedMap = new HashMap<String, Long>();
								try {
									InteractionUtils.parseCardIdList(encryptedIdList, parsedMap);
								} catch(ParseException e) {
									log.warn("Could not parse card key '" + encryptedIdList + "'", e);
									return;
								}
								picker.vote(parsedMap.keySet());
							}
						});
						output.flush();
						out.close();
						resource.release();
						// send message chain
						massLookupStep.setQueueKey(AnyTwoInstanceWorkQueue.constructQueueName(massLookupStep.getQueueKey(), picker.getWinners(2)));
						MessageChainService.publish(messageChain, publisher, encryptionInfoMapping);
						okay = true;
					} finally {
						if(!okay) {
							resource.delete();
							resource.release();
						}
					}
				}
				return results.getRowCount();
			} finally {
				results.close();
			}
	}

	@Override
	public int processBatches(int batchSize, int maxBatches) throws RemoteException {
		try {
			return processBatches(batchSize, maxBatches, true);
		} catch(IllegalStateException e) {
			throw new RemoteException("Error", e);
		} catch(SQLException e) {
			throw new RemoteException("Error", e);
		} catch(DataLayerException e) {
			throw new RemoteException("Error", e);
		} catch(IOException e) {
			throw new RemoteException("Error", e);
		} catch(GeneralSecurityException e) {
			throw new RemoteException("Error", e);
		} catch(ConvertException e) {
			throw new RemoteException("Error", e);
		} catch(ServiceException e) {
			throw new RemoteException("Error", e);
		}
	}

	protected int processBatches(int batchSize, int batches, boolean lock) throws IllegalStateException, SQLException, DataLayerException, IOException, GeneralSecurityException, ConvertException, ServiceException {
		int tot = 0;
		for(int i = 0; batches > 0 && i < batches; i++) {
			int r = process(batchSize, lock);
			if(r > 0)
				tot += r;
			if(r < batchSize)
				return tot;
		}
		return tot;
	}

	protected int process(int batchSize, boolean lock) throws SQLException, DataLayerException, IllegalStateException, IOException, GeneralSecurityException, ConvertException, ServiceException {
		final Map<String, Object> params = new HashMap<String, Object>();
		if(lock && !InteractionUtils.lockProcess(PROCESS_CD, LOCK_PROCESS_TOKEN_CD, params)) {
			log.error("Process is already locked");
			throw new ServiceException("Process is already locked");
		}
		try {
			long lastTranId = 0;
			params.clear();
			params.put("maxRows", batchSize);
			Results results = DataLayerMgr.executeQuery("GET_MISSING_CONSUMER_ACCT", params);
			try {
				if(!results.isGroupEnding(0)) {
					if(resourceFolder == null)
						throw new IllegalStateException("ResourceFolder is not set");
					if(publisher == null)
						throw new IllegalStateException("Publisher is not set");
					Resource resource = resourceFolder.getResource("KeyMgrAccountCreate_" + System.currentTimeMillis(), ResourceMode.CREATE);
					boolean okay = false;
					try {
						byte[] encryptionKey = keyMgrCryption.generateKey();
						MessageChain messageChain = new MessageChainV11();
						MessageChainStep massCreateStep = messageChain.addStep(getMassCreateAccountQueueKey());
						massCreateStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_RESOURCE, resource.getKey());
						massCreateStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_ENCRYPTION_KEY, encryptionKey);
						massCreateStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_CIPHER, keyMgrCryption.getCipherName());
						massCreateStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_BLOCK_SIZE, keyMgrCryption.getBlockSize());

						MessageChainStep mainUpdateStep = messageChain.addStep(getMainUpdateQueueKey());
						mainUpdateStep.setReferenceAttribute(CommonAttrEnum.ATTR_CRYPTO_RESOURCE, massCreateStep, CommonAttrEnum.ATTR_CRYPTO_RESOURCE);
						mainUpdateStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_ENCRYPTION_KEY, encryptionKey);
						mainUpdateStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_CIPHER, keyMgrCryption.getCipherName());
						mainUpdateStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_BLOCK_SIZE, keyMgrCryption.getBlockSize());

						MessageChainStep massUpdateStep = messageChain.addStep(getMassUpdateAccountQueueKey(), true);
						massUpdateStep.setReferenceAttribute(CommonAttrEnum.ATTR_CRYPTO_RESOURCE, massCreateStep, CommonAttrEnum.ATTR_CRYPTO_RESOURCE);
						massUpdateStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_ENCRYPTION_KEY, encryptionKey);
						massUpdateStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_CIPHER, keyMgrCryption.getCipherName());
						massUpdateStep.setAttribute(CommonAttrEnum.ATTR_CRYPTO_BLOCK_SIZE, keyMgrCryption.getBlockSize());
						massUpdateStep.setAttribute(CommonAttrEnum.ATTR_SKIP_ON_SAME_INSTANCE, true);

						massCreateStep.setNextSteps(0, mainUpdateStep, massUpdateStep);

						MessageChainStep deleteFileStep = messageChain.addStep("usat.file.resource.delete");
						deleteFileStep.setAttribute(CommonAttrEnum.ATTR_RESOURCE, resource.getKey());
						deleteFileStep.setJoined(true);
						deleteFileStep.addWaitFor(massUpdateStep);
						deleteFileStep.addWaitFor(mainUpdateStep);
						massUpdateStep.setNextSteps(0, deleteFileStep);
						mainUpdateStep.setNextSteps(0, deleteFileStep);

						OutputStream out = keyMgrCryption.createEncryptingOutputStream(encryptionKey, resource.getOutputStream());
						ByteOutput output = new OutputStreamByteOutput(out);
						DatasetUtils.writeHeader(output, OUTPUT_HEADER);
						Object[] values = new Object[OUTPUT_HEADER.length];
						while(results.next()) {
							long tranId = results.getValue("tranId", Long.class);
							lastTranId = tranId;
							byte[] accountCdHash = ByteArrayUtils.fromHex(results.getValue("accountCdHash", String.class));
							if(accountCdHash == null) {
								String trackData = results.getValue("trackData", String.class);
								String cardNumber;
								if(!StringUtils.isBlank(trackData) && StringUtils.indexOf((cardNumber = MessageResponseUtils.getCardNumber(trackData)), MASK_CHARS) < 0) {
									accountCdHash = SecureHash.getUnsaltedHash(cardNumber.getBytes());
								} else {
									log.error("Account Cd Hash is null for tranId=" + tranId + "; skipping");
									continue;
								}
							}
							values[0] = tranId;
							values[1] = accountCdHash;
							DatasetUtils.writeRow(output, values);
						}
						DatasetUtils.writeFooter(output);
						output.flush();
						out.close();
						resource.release();
						// send message chain
						MessageChainService.publish(messageChain, publisher, encryptionInfoMapping);
						okay = true;
					} finally {
						if(!okay) {
							resource.delete();
							resource.release();
						}
					}
				}
				// update database
				params.clear();
				params.put("lastTranId", lastTranId);
				int rows = DataLayerMgr.executeUpdate("UPDATE_LAST_PROCESSED_MISSING_CONSUMER_ACCT", params, true);
				if(rows < 1)
					throw new NotEnoughRowsException("MISSING_CONSUMER_ACCT_LAST_TRAN app setting NOT updated");
				return results.getRowCount();
			} finally {
				results.close();
			}
		} finally {
			if(lock) {
				params.clear();
				InteractionUtils.unlockProcess(PROCESS_CD, LOCK_PROCESS_TOKEN_CD, params);
			}
		}
	}

	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}

	public String getMassCreateAccountQueueKey() {
		return massCreateAccountQueueKey;
	}

	public void setMassCreateAccountQueueKey(String massUpdateAccountQueueKey) {
		this.massCreateAccountQueueKey = massUpdateAccountQueueKey;
	}

	public EncryptionInfoMapping getEncryptionInfoMapping() {
		return encryptionInfoMapping;
	}

	public void setEncryptionInfoMapping(EncryptionInfoMapping encryptionInfoMapping) {
		this.encryptionInfoMapping = encryptionInfoMapping;
	}

	public Cryption getKeyMgrCryption() {
		return keyMgrCryption;
	}

	public int getMaxBatchSize() {
		return maxBatchSize;
	}

	public void setMaxBatchSize(int maxBatchSize) {
		this.maxBatchSize = maxBatchSize;
	}

	public Publisher<ByteInput> getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}

	public String getMassUpdateAccountQueueKey() {
		return massUpdateAccountQueueKey;
	}

	public void setMassUpdateAccountQueueKey(String massUpdateAccountQueueKey) {
		this.massUpdateAccountQueueKey = massUpdateAccountQueueKey;
	}

	public String getMainUpdateQueueKey() {
		return mainUpdateQueueKey;
	}

	public void setMainUpdateQueueKey(String mainUpdateQueueKey) {
		this.mainUpdateQueueKey = mainUpdateQueueKey;
	}

	public int getMaxBatches() {
		return maxBatches;
	}

	public void setMaxBatches(int maxBatches) {
		this.maxBatches = maxBatches;
	}

	public boolean isJmxEnabled() {
		return jmxEnabled;
	}

	public void setJmxEnabled(boolean jmxEnabled) throws InstanceAlreadyExistsException, MBeanRegistrationException, NotCompliantMBeanException, InstanceNotFoundException {
		if(this.jmxEnabled == jmxEnabled)
			return;
		if(jmxEnabled) {
			MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
			mbs.registerMBean(this, JMX_OBJECT_NAME);
		} else {
			MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
			mbs.unregisterMBean(JMX_OBJECT_NAME);
		}
		this.jmxEnabled = jmxEnabled;
	}

	public String getMassLookupAccountQueueKey() {
		return massLookupAccountQueueKey;
	}

	public void setMassLookupAccountQueueKey(String massLookupAccountQueueKey) {
		this.massLookupAccountQueueKey = massLookupAccountQueueKey;
	}

	public String getReplenishUpdateQueueKey() {
		return replenishUpdateQueueKey;
	}

	public void setReplenishUpdateQueueKey(String replenishUpdateQueueKey) {
		this.replenishUpdateQueueKey = replenishUpdateQueueKey;
	}
	public String getMassRetrieveQueue() {
		return massRetrieveQueue;
	}
	public void setMassRetrieveQueue(String massRetrieveQueue) {
		this.massRetrieveQueue = massRetrieveQueue;
	}
	public String getMassUpdateExpDateQueueKey() {
		return massUpdateExpDateQueueKey;
	}
	public void setMassUpdateExpDateQueueKey(String massUpdateExpDateQueueKey) {
		this.massUpdateExpDateQueueKey = massUpdateExpDateQueueKey;
	}
	
	
}
