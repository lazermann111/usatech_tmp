package com.usatech.posm.schedule.dfr;

import static simple.text.MessageFormat.format;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.Blob;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.InflaterInputStream;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.DatasetHandlerSelectorFacade;
import simple.text.AbstractDelimitedParser.ExtraHandling;
import simple.text.DelimitedColumn;
import simple.text.MultiRecordDelimitedParser;
import simple.text.StringUtils;
import simple.text.ThreadSafeDateFormat;

import com.usatech.app.AbstractAttributeDatasetHandler;
import com.usatech.posm.schedule.dfr.DFRReport.Status;

public class DFRParser {
	
	private static final Log log = Log.getLog();
	
	public static final String SELECTOR_COLUMN = "RecordType";
	public static final String GENERATION_DATE = "GenererationDate";
	public static final String GENERATION_TIME = "GenerationTime";
	public static final String GENERATION_TS = "GenerationTs";
	
	public static final ThreadSafeDateFormat dateTimeFormat = new ThreadSafeDateFormat(new SimpleDateFormat("MM/dd/yyyy HH:mm:ss"));

	private long fileCacheId;
	private long retryMs;
	
	private MultiRecordDelimitedParser<String> parser = new MultiRecordDelimitedParser<>();
	private DatasetHandlerSelectorFacade<ServiceException> selectorHandler = new DatasetHandlerSelectorFacade<>(SELECTOR_COLUMN);
	
	private static DelimitedColumn[] fileHeaderColumns = new DelimitedColumn[3];
	private static DelimitedColumn[] sectionHeaderColumns = new DelimitedColumn[5];
	
	private DFRReport currentReport;
	private String currentDataRecordTypeId;

	static {
		fileHeaderColumns[0] = new DelimitedColumn("PID", String.class);
		fileHeaderColumns[1] = new DelimitedColumn("Frequency", String.class);
		fileHeaderColumns[2] = new DelimitedColumn("EntityId", String.class);

		sectionHeaderColumns[0] = new DelimitedColumn("CompanyID", String.class);
		sectionHeaderColumns[1] = new DelimitedColumn("ReportDateFrom", Date.class);
		sectionHeaderColumns[2] = new DelimitedColumn("ReportDateTo", Date.class);
		sectionHeaderColumns[3] = new DelimitedColumn("GenererationDate", String.class);
		sectionHeaderColumns[4] = new DelimitedColumn("GenerationTime", String.class);
	}
	
	public DFRParser(long fileCacheId, long retryMs) throws ServiceException {
		this.fileCacheId = fileCacheId;
		this.retryMs = retryMs;
		
		parser.setColumnSeparator("|");
		parser.setLineSeparator("\n");
		parser.setQuotes('"');
		parser.setSelectorColumn(SELECTOR_COLUMN, String.class);
		parser.setExtraColumnHandling(ExtraHandling.IGNORE);
		
		List<DFRReportDef> reportDefs = null;
		try {
			reportDefs = DFRReportDef.loadAll();
		} catch (Exception e) {
			throw new ServiceException("Failed to load DFR Report Defs", e);
		}
		
		// TODO: NOTE! there may be a problem if a headerTypeId is use for more than one type of data record
		// such as the case of HPDE0017 with PDE0017S and PDE0017D, but the S suffix records are not 
		// particularly valuable for importing at this time so I'm not dealing with it
		for(DFRReportDef reportDef : reportDefs) {
			if (parser.hasRecordColumns(reportDef.getHeaderRecordTypeId())) {
				log.warn("Handler is already configured for DFR Report header type {0}", reportDef.getHeaderRecordTypeId());
			} else {
				// the HeaderDatasetHandler will add a handler for the data records when encountered
				parser.setRecordColumns(reportDef.getHeaderRecordTypeId(), reportDef.getHeaderRecordTypeId().startsWith("*") ? fileHeaderColumns : sectionHeaderColumns);
				selectorHandler.addHandler(reportDef.getHeaderRecordTypeId(), new HeaderDatasetHandler(reportDef));
			}
		}
	}
	
	public long getFileCacheId() {
		return fileCacheId;
	}
	
	public long getRetryMs() {
		return retryMs;
	}
	
	public void parse() {
		Blob fileContent = null;
		int fileType;
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("FILE_CACHE_ID", fileCacheId);
			DataLayerMgr.executeCall("GET_FILE_CACHE_CONTENT", params, false);
			fileContent = ConvertUtils.convert(Blob.class, params.get("FILE_CONTENT"));
			fileType = ConvertUtils.getInt(params.get("FILE_CACHE_TYPE_ID"));
		} catch (Exception e) {
			log.warn("Failed to parse FILE_CACHE_ID {0}", fileCacheId, e);
			return;
		}
		
		if (fileContent == null) {
			log.warn("Failed to parse FILE_CACHE_ID {0}: empty blob content", fileCacheId);
			return;
		}
		
		InputStream in;
		try {
			in = fileContent.getBinaryStream();
		} catch(SQLException e) {
			log.warn("Failed to parse FILE_CACHE_ID {0}", fileCacheId, e);
			return;
		}
		switch(fileType) {
			case 2:
				in = new InflaterInputStream(in);
		}

		try (BufferedReader reader = new BufferedReader(new InputStreamReader(in))) {
			parse(reader);
		} catch (Exception e) {
			log.warn("Failed to parse FILE_CACHE_ID {0}", fileCacheId, e);
		}
	}
	
	public long parse(Reader reader) throws ServiceException, IOException {
		parser.parse(reader, selectorHandler);
		return selectorHandler.getRow();
	}
	
	public class HeaderDatasetHandler extends AbstractAttributeDatasetHandler<ServiceException> {
		private DFRReportDef reportDef;
		
		public HeaderDatasetHandler(DFRReportDef reportDef) {
			this.reportDef = reportDef;
		}
		
		public DFRReportDef getReportDef() {
			return reportDef;
		}
		
		@Override
		public void handleValue(String columnName, Object value) {
			if (value == null || (value instanceof String && StringUtils.isBlank(((String)value))))
				return;
			if (value instanceof String) {
				String s = (String) value;
				s = s.replaceAll("\r", "");
				s = s.replaceAll("\n", "");
				s = s.replaceAll("\"", "");
				s = s.trim();
				value = s;
			}
			super.handleValue(columnName, value);
		}

		@Override
		public void handleDatasetStart(String[] columnNames) throws ServiceException {
			if (currentReport != null) {
				currentReport.setProcessFinishTs(new Date());
				currentReport.setProcessStatus(Status.S);
				try {
					currentReport.save();
					log.info("Successfully parsed DFR Report Type {0} generated on {1} from FILE_CACHE_ID {2}", currentDataRecordTypeId, dateTimeFormat.format(currentReport.getHeaderGenerationTs()), fileCacheId);
				} catch (Exception e) {
					throw new ServiceException(format("Failed to save DFR Report fileCacheId={0}, reportDefId={1}, generationTs={2}", currentReport.getFileCacheId(), reportDef.getReportDefId(), currentReport.getHeaderGenerationTs()), e);
				}
				
				parser.removeRecordColumns(currentDataRecordTypeId);
				selectorHandler.removeHandler(currentDataRecordTypeId);
				
				currentReport = null;
				currentDataRecordTypeId = null;
			}
		}

		@Override
		public void handleDatasetEnd() throws ServiceException {
		}

		@Override
		public void handleRowEnd() throws ServiceException {
			if (!reportDef.isEnabled())
				return;
			
			Date generationDate = null;
			try {
				String dateStr = getDetailAttribute(GENERATION_DATE, String.class, true);
				String timeStr =  getDetailAttribute(GENERATION_TIME, String.class, true);
				String dateTimeStr = format("{0} {1}", dateStr, timeStr);
				generationDate = dateTimeFormat.parse(dateTimeStr);
			} catch (Exception e) {
				throw new ServiceException("Failed to parse report generation date/time", e);
			}
			
			DFRReport report = null;
			try {
				report = DFRReport.loadByAlternateKey(fileCacheId, reportDef.getReportDefId(), generationDate);
			} catch (Exception e) {
				throw new ServiceException(e);
			}
			
			if (report == null || report.shouldRetry(retryMs)) {
				if (report == null)
					report = new DFRReport();
				
				// this section of the DFR file has not been processed yet, or previously failed - start processing it

				report.setFileCacheId(fileCacheId);
				report.setReportDefId(reportDef.getReportDefId());
				report.setHeaderGenerationTs(generationDate);
				report.setProcessStartTs(new Date());
				report.setProcessStatus(Status.IP);

				try {
					report.save();
				} catch (Exception e) {
					throw new ServiceException(format("Failed to save DFR Report fileCacheId={0}, reportDefId={1}, generationTs={2}", fileCacheId, reportDef.getReportDefId(), dateTimeFormat.format(generationDate)), e);
				}
				
				log.info("Adding handler for DFR Report Type {0} generated on {1} from FILE_CACHE_ID {2}", reportDef.getDataRecordTypeId(), dateTimeFormat.format(generationDate), fileCacheId);

				DelimitedColumn[] columns = reportDef.toDelimitedColumns();
				parser.setRecordColumns(reportDef.getDataRecordTypeId(), columns);
				selectorHandler.addHandler(reportDef.getDataRecordTypeId(), new DFRDatasetHandler(reportDef, report));
				
				currentReport = report;
				currentDataRecordTypeId = reportDef.getDataRecordTypeId();
			} else {
				log.info("DFR Report Type {0} generated on {1} from FILE_CACHE_ID {2} has already been parsed", reportDef.getDataRecordTypeId(), dateTimeFormat.format(generationDate), fileCacheId);
			}
		}
	}
}
