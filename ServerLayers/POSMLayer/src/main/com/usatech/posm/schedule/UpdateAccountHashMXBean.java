package com.usatech.posm.schedule;

import java.rmi.RemoteException;

import javax.management.MXBean;

@MXBean(true)
public interface UpdateAccountHashMXBean {
	public int updateMissingAccountHashes(int batchSize) throws RemoteException;
}
