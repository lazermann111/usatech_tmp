package com.usatech.posm.schedule;

import java.lang.management.ManagementFactory;

import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.ReflectionException;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.db.DataLayerMgr;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.QuartzScheduledService2;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
/**
 * Resume the quartz job by jobId
 * @author yhe
 *
 */
public class ResumeRequestIndDBTask implements MessageChainTask {
	protected static final String[] RESUME_JOB_SIGNATURE = new String[] { "java.lang.String" };
	
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		String jobId;
		long newIndDbModTime;
		try {
			jobId = taskInfo.getStep().getAttribute(AuthorityAttrEnum.ATTR_JOB_ID, String.class, true);
			newIndDbModTime = taskInfo.getStep().getAttributeDefault(AuthorityAttrEnum.ATTR_NEW_IND_DB_MOD_TIME, Long.class, 0L);
		} catch(AttributeConversionException e) {
			throw new RetrySpecifiedServiceException(e, WorkRetryType.NO_RETRY);
		}
		try{
			ManagementFactory.getPlatformMBeanServer().invoke(QuartzScheduledService2.QUARTZ_OBJECT_NAME, "resumeJob", new Object[] { jobId }, RESUME_JOB_SIGNATURE);
			//quartzJMX.resumeJob(jobId);
			if(newIndDbModTime > 0)
				DataLayerMgr.executeUpdate("UPDATE_APP_SETTING", new Object[] { newIndDbModTime, "IND_DB_FILE_MOD_TIME" }, true);
		} catch(ReflectionException e) {
			throw new ServiceException("Failed to resume jobId=" + jobId, e);
		} catch(InstanceNotFoundException e) {
			throw new ServiceException("Failed to resume jobId="+jobId, e);
		} catch(MBeanException e) {
			throw new ServiceException("Failed to resume jobId=" + jobId, e);
		}catch(Exception e){
			throw new ServiceException("Failed to update IND_DB_FILE_MOD_TIME newIndDbModTime=" + newIndDbModTime, e);
		}
		return 0;
	}



}
