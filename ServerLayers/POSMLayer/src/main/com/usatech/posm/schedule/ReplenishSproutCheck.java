package com.usatech.posm.schedule;


import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;

import com.usatech.layers.common.QuartzCronScheduledWithPublisherJob;
import com.usatech.layers.common.sprout.SproutAccountResponse;
import com.usatech.layers.common.sprout.SproutExcpetion;
import com.usatech.layers.common.sprout.SproutUtils;
import com.usatech.posm.tasks.ReplenishPrepaidTask;

@DisallowConcurrentExecution
public class ReplenishSproutCheck extends QuartzCronScheduledWithPublisherJob {
	private static final Log log = Log.getLog();
	protected static ReplenishPrepaidTask replenishPrepaidTask;
	protected static SproutUtils sproutUtils;
	
	public static SproutUtils getSproutUtils() {
		return sproutUtils;
	}

	public static void setSproutUtils(SproutUtils sproutUtils) {
		ReplenishSproutCheck.sproutUtils = sproutUtils;
	}

	@Override
	public void executePostConfigure(JobExecutionContext context) throws JobExecutionException {
		try {
			Connection conn = DataLayerMgr.getConnectionForCall("FIND_SPROUT_AUTO_REPLENISH_NEEDED");
			try {
				Results results = DataLayerMgr.executeQuery(conn, "FIND_SPROUT_AUTO_REPLENISH_NEEDED", null);
				Map<String, Object> params = new HashMap<String, Object>();

				
				int replenished = 0;
				while(results.next()) {
					long consumerAcctId = results.getValue("consumerAcctId", Long.class);
					String sproutAccountId = results.getValue("consumerAcctIdentifier", String.class);
					SproutAccountResponse accountRsp=null;
					try{
						accountRsp=sproutUtils.getAccount(sproutAccountId);
						if(accountRsp.isActive()==false){
							log.info(new StringBuilder("Account is not active.Skip sprout consumerAcctId:").append(consumerAcctId));
							continue;
						}
						char replenishRequestedFlag=results.getValue("replenishRequestedFlag", char.class);
						if(replenishRequestedFlag=='N'){
							BigDecimal balance=accountRsp.getAvailable_balance();
							BigDecimal pendingAmount=results.getValue("pendingAmount", BigDecimal.class);
							BigDecimal replenishThreshhold=results.getValue("replenishThreshhold", BigDecimal.class);
							if(balance.add(pendingAmount).compareTo(replenishThreshhold)<0){
								params.put("consumerAcctBalance", balance);
							}else{
								log.info(new StringBuilder("Account balance is not < replenishThreshhold.Skip sprout consumerAcctId:").append(consumerAcctId).append(" balance:").append(balance).append(" pendingAmount:").append(pendingAmount).append(" replenishThreshhold:").append(replenishThreshhold));
								continue;
							}
						}
					}catch(SproutExcpetion e){
						int statusCode=e.getHttpStatus();
						String errorMsg=e.getMessage()==null?"":e.getMessage();
						log.info(new StringBuilder("Failed to retrieve sprout info consumerAcctId:").append(consumerAcctId).append("statusCode:").append(statusCode).append(" response:").append(errorMsg).toString());
						continue;
					}
					try {
						if(replenishPrepaidTask.checkReplenishCard(consumerAcctId, conn, params, getPublisher()))
							replenished++;
					} catch(SQLException e) {
						log.warn("Error while replenishing prepaid card", e);
						try {
							conn.rollback();
						} catch(SQLException e1) {
							log.warn("Could not rollback prepaid card replenishment", e1);
						}
					} catch(ConvertException e) {
						log.warn("Error while replenishing prepaid card", e);
						try {
							conn.rollback();
						} catch(SQLException e1) {
							log.warn("Could not rollback prepaid card replenishment", e1);
						}
					} catch(DataLayerException e) {
						log.warn("Error while replenishing prepaid card", e);
						try {
							conn.rollback();
						} catch(SQLException e1) {
							log.warn("Could not rollback prepaid card replenishment", e1);
						}
					} catch(ServiceException e) {
						log.warn("Error while replenishing prepaid card", e);
						try {
							conn.rollback();
						} catch(SQLException e1) {
							log.warn("Could not rollback prepaid card replenishment", e1);
						}
					}
					conn.commit();
				}
				log.info("Found " + results.getRowCount() + " sprout accounts that needed replenishment and replenished " + replenished);
			} finally {
				conn.close();
			}
		} catch(SQLException e) {
			log.warn("Could not find accounts to replenish", e);
		} catch(DataLayerException e) {
			log.warn("Could not find accounts to replenish", e);
		} catch(ConvertException e) {
			log.warn("Could not find accounts to replenish", e);
		}
	}

	public static ReplenishPrepaidTask getReplenishPrepaidTask() {
		return replenishPrepaidTask;
	}

	public static void setReplenishPrepaidTask(ReplenishPrepaidTask replenishPrepaidTask) {
		ReplenishSproutCheck.replenishPrepaidTask = replenishPrepaidTask;
	}
}
