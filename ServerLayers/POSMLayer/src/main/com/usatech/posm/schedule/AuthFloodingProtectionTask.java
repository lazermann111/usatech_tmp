package com.usatech.posm.schedule;

import java.util.HashMap;
import java.util.Map;

import simple.app.SelfProcessor;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;

import com.usatech.layers.common.InteractionUtils;

public class AuthFloodingProtectionTask implements SelfProcessor {
	private static final Log log = Log.getLog();

	protected static final String AUTH_FLOODING_PROTECTION = "AUTH_FLOODING_PROTECTION";

	protected int appInstance;

	@Override
	public void process() {
		Map<String, Object> params = new HashMap<String, Object>();
		Results results = null;
		try {
			if (!InteractionUtils.lockProcess(AUTH_FLOODING_PROTECTION, String.valueOf(appInstance), params)) {
				log.info("Auth Flooding Protection is already locked by instance {0}", params.get("lockedBy"));
				return;
			}
			DataLayerMgr.executeCall("AUTH_FLOODING_PROTECTION", params, true);
		} catch (Exception e) {
			log.error("Error executing Auth Flooding Protection", e);
		} finally {
			if (results != null)
				results.close();
			try {
				InteractionUtils.unlockProcess(AUTH_FLOODING_PROTECTION, String.valueOf(appInstance), params);
			} catch (Exception e) {
				log.error("Error unlocking {0}", AUTH_FLOODING_PROTECTION, e);
			}
		}
	}
	
	public int getAppInstance() {
		return appInstance;
	}

	public void setAppInstance(int appInstance) {
		this.appInstance = appInstance;
	}

}
