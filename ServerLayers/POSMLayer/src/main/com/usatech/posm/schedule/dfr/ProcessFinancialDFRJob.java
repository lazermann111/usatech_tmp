package com.usatech.posm.schedule.dfr;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.usatech.layers.common.QuartzCronScheduledJob;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;

@DisallowConcurrentExecution
public class ProcessFinancialDFRJob extends QuartzCronScheduledJob {
	private static final Log log = Log.getLog();
	protected int[] fileTypes = new int[] { 1, 2 };
	protected int checkHours = 7 * 24; //7 days
	protected final ParseFinancialDFRTask parser = new ParseFinancialDFRTask();

	@Override
	public void executePostConfigure(JobExecutionContext context) throws JobExecutionException {
		// get unprocessed
		Map<String, Object> params = new HashMap<>();
		params.put("fileTypes", fileTypes);
		params.put("processingThreshold", getProcessingThresholdMillis());
		params.put("checkHours", getCheckHours());
		try {
			Connection conn = DataLayerMgr.getConnection("OPER");
			try {
				Results results = DataLayerMgr.executeQuery(conn, "GET_UNPROCESSED_FINANCIAL_DFR_FILES", params);
				try {
					while(results.next()) {
						long fileCacheId = ConvertUtils.getLong(results.getValue("fileCacheId"));
						try {
							parser.parseFile(fileCacheId);
						} catch(SQLException | DataLayerException | ConvertException | ServiceException | IOException e) {
							// Skip to next
							log.error("Error parsing DFR file " + fileCacheId, e);
						}
					}
				} finally {
					results.close();
				}
			} finally {
				conn.close();
			}
		} catch(SQLException | DataLayerException | ConvertException e) {
			log.error("While processing files", e);
			throw new JobExecutionException(e);
		}
	}



	public int[] getFileTypes() {
		return fileTypes;
	}

	public void setFileTypes(int[] fileTypes) {
		this.fileTypes = fileTypes;
	}

	public long getProcessingThresholdMillis() {
		return parser.getProcessingThresholdMillis();
	}

	public void setProcessingThresholdMillis(long processingThresholdMillis) {
		parser.setProcessingThresholdMillis(processingThresholdMillis);
	}

	public int getCheckHours() {
		return checkHours;
	}

	public void setCheckHours(int checkHours) {
		this.checkHours = checkHours;
	}
}
