package com.usatech.posm.schedule.dfr;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.text.DelimitedColumn;

public class DFRReportDef implements Iterable<DFRReportDef.DFRReportFieldDef> {
	private long reportDefId;
	private String headerRecordTypeId;
	private String dataRecordTypeId;
	private String reportName;
	private String reportDescription;
	private boolean enabled = false;
	private String tableName;
	private List<DFRReportFieldDef> fields = new ArrayList<>();

	public DFRReportDef() {
		
	}
	
	public DFRReportDef(long reportDefId) {
		this.reportDefId = reportDefId;
	}

	public long getReportDefId() {
		return reportDefId;
	}

	public void setReportDefId(long reportDefId) {
		this.reportDefId = reportDefId;
	}

	public String getHeaderRecordTypeId() {
		return headerRecordTypeId;
	}

	public void setHeaderRecordTypeId(String headerRecordTypeId) {
		this.headerRecordTypeId = headerRecordTypeId;
	}
	
	public String getDataRecordTypeId() {
		return dataRecordTypeId;
	}
	
	public void setDataRecordTypeId(String dataRecordTypeId) {
		this.dataRecordTypeId = dataRecordTypeId;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public String getReportDescription() {
		return reportDescription;
	}
	
	public void setReportDescription(String reportDescription) {
		this.reportDescription = reportDescription;
	}
	
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	
	public DFRReportFieldDef createFieldDef(long fieldDefId) {
		DFRReportFieldDef fd = new DFRReportFieldDef(fieldDefId);
		fields.add(fd);
		return fd;
	}
	
	public List<DFRReportFieldDef> getFields() {
		return fields;
	}
	
	public DelimitedColumn[] toDelimitedColumns() {
		List<DelimitedColumn> columnList = new ArrayList<>();
		
		for(DFRReportFieldDef fieldDef : fields) {
			DelimitedColumn column = new DelimitedColumn(fieldDef.getFieldName(), fieldDef.getDataType());
			columnList.add(column);
		}
		
		DelimitedColumn[] columnArray = new DelimitedColumn[columnList.size()];
		columnList.toArray(columnArray);
		
		
		return columnArray;
	}
	
	public List<String> getColumnNames() {
		List<String> list = new ArrayList<String>();
		for(DFRReportFieldDef fd : this) {
			list.add(fd.getColumnName());
		}
		return list;
	}
	
	@Override
	public Iterator<DFRReportFieldDef> iterator() {
		return getFields().iterator();
	}
	
	public static List<DFRReportDef> loadAll() throws ServiceException, DataLayerException, SQLException, ConvertException {
		List<DFRReportDef> list = new ArrayList<>();
		
		DFRReportDef currentReportDef = null;
		
		Results results = DataLayerMgr.executeQuery("GET_DFR_REPORT_DEFS", new Object[] {});
		while(results.next()) {
			long reportDefId = results.getValue("reportDefId", Long.class);
			
			if (currentReportDef == null || currentReportDef.getReportDefId() != reportDefId) {
				currentReportDef = new DFRReportDef();
				try {
					ReflectionUtils.populateProperties(currentReportDef, results.getValuesMap());
				} catch (Exception e) {
					throw new ServiceException(e);
				}
				list.add(currentReportDef);
			}
			
			long fieldDefId = ConvertUtils.getLongSafely(results.getValue("fieldDefId"), -1L);
			if (fieldDefId > 0) {
				DFRReportFieldDef fieldDef = currentReportDef.createFieldDef(fieldDefId);
				fieldDef.loadFromResults(results);
			}
		}
		
		return list;
	}
	
	@Override
	public String toString() {
		return String.format("DFRReportDef [reportDefId=%s, headerRecordTypeId=%s, dataRecordTypeId=%s, name=%s, description=%s, tableName=%s, fields=%s]", reportDefId, headerRecordTypeId, dataRecordTypeId, reportName, reportDescription, tableName, fields);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataRecordTypeId == null) ? 0 : dataRecordTypeId.hashCode());
		result = prime * result + ((reportDescription == null) ? 0 : reportDescription.hashCode());
		result = prime * result + ((fields == null) ? 0 : fields.hashCode());
		result = prime * result + ((headerRecordTypeId == null) ? 0 : headerRecordTypeId.hashCode());
		result = prime * result + (int)(reportDefId ^ (reportDefId >>> 32));
		result = prime * result + ((reportName == null) ? 0 : reportName.hashCode());
		result = prime * result + ((tableName == null) ? 0 : tableName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		DFRReportDef other = (DFRReportDef)obj;
		if(dataRecordTypeId == null) {
			if(other.dataRecordTypeId != null)
				return false;
		} else if(!dataRecordTypeId.equals(other.dataRecordTypeId))
			return false;
		if(reportDescription == null) {
			if(other.reportDescription != null)
				return false;
		} else if(!reportDescription.equals(other.reportDescription))
			return false;
		if(fields == null) {
			if(other.fields != null)
				return false;
		} else if(!fields.equals(other.fields))
			return false;
		if(headerRecordTypeId == null) {
			if(other.headerRecordTypeId != null)
				return false;
		} else if(!headerRecordTypeId.equals(other.headerRecordTypeId))
			return false;
		if(reportDefId != other.reportDefId)
			return false;
		if(reportName == null) {
			if(other.reportName != null)
				return false;
		} else if(!reportName.equals(other.reportName))
			return false;
		if(tableName == null) {
			if(other.tableName != null)
				return false;
		} else if(!tableName.equals(other.tableName))
			return false;
		return true;
	}

	public class DFRReportFieldDef {
		private long fieldDefId;
		private String fieldName;
		private String fieldDescription;
		private int fieldNumber;
		private int fieldLength;
		private Class<?> dataType;
		private String dataFormat;
		private String columnName;
		private boolean key;

		public DFRReportFieldDef() {
			
		}

		public DFRReportFieldDef(long fieldDefId) {
			this.fieldDefId = fieldDefId;
		}

		public long getFieldDefId() {
			return fieldDefId;
		}

		public void setFieldDefId(long fieldDefId) {
			this.fieldDefId = fieldDefId;
		}

		public String getFieldName() {
			return fieldName;
		}

		public void setFieldName(String fieldName) {
			this.fieldName = fieldName;
		}
		
		public String getFieldDescription() {
			return fieldDescription;
		}
		
		public void setFieldDescription(String fieldDescription) {
			this.fieldDescription = fieldDescription;
		}

		public int getFieldNumber() {
			return fieldNumber;
		}

		public void setFieldNumber(int fieldNumber) {
			this.fieldNumber = fieldNumber;
		}

		public int getFieldLength() {
			return fieldLength;
		}

		public void setFieldLength(int fieldLength) {
			this.fieldLength = fieldLength;
		}

		public Class<?> getDataType() {
			return dataType;
		}
		
		public String getDataFormat() {
			return dataFormat;
		}
		
		public void setDataFormat(String dataFormat) {
			this.dataFormat = dataFormat;
		}

		public void setDataType(Class<?> dataType) {
			this.dataType = dataType;
		}

		public String getColumnName() {
			return columnName;
		}

		public void setColumnName(String columnName) {
			this.columnName = columnName;
		}

		public boolean isKey() {
			return key;
		}

		public void setKey(boolean key) {
			this.key = key;
		}
		
		public void loadFromResults(Results results) throws ServiceException {
			try {
				ReflectionUtils.populateProperties(this, results.getValuesMap());
			} catch (Exception e) {
				throw new ServiceException(e);
			}
		}

		@Override
		public String toString() {
			return String.format("DFRReportFieldDef [fieldDefId=%s, fieldName=%s, fieldDescription=%s, fieldNumber=%s, fieldLength=%s, dataType=%s, dataFormat=%s, columName=%s, key=%s]", fieldDefId, fieldName, fieldDescription, fieldNumber, fieldLength, dataType, dataFormat, columnName, key);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((columnName == null) ? 0 : columnName.hashCode());
			result = prime * result + ((dataFormat == null) ? 0 : dataFormat.hashCode());
			result = prime * result + ((dataType == null) ? 0 : dataType.hashCode());
			result = prime * result + ((fieldDescription == null) ? 0 : fieldDescription.hashCode());
			result = prime * result + fieldLength;
			result = prime * result + fieldNumber;
			result = prime * result + (int)(fieldDefId ^ (fieldDefId >>> 32));
			result = prime * result + (key ? 1231 : 1237);
			result = prime * result + ((fieldName == null) ? 0 : fieldName.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if(this == obj)
				return true;
			if(obj == null)
				return false;
			if(getClass() != obj.getClass())
				return false;
			DFRReportFieldDef other = (DFRReportFieldDef)obj;
			if(!getOuterType().equals(other.getOuterType()))
				return false;
			if(columnName == null) {
				if(other.columnName != null)
					return false;
			} else if(!columnName.equals(other.columnName))
				return false;
			if(dataFormat == null) {
				if(other.dataFormat != null)
					return false;
			} else if(!dataFormat.equals(other.dataFormat))
				return false;
			if(dataType == null) {
				if(other.dataType != null)
					return false;
			} else if(!dataType.equals(other.dataType))
				return false;
			if(fieldDescription == null) {
				if(other.fieldDescription != null)
					return false;
			} else if(!fieldDescription.equals(other.fieldDescription))
				return false;
			if(fieldLength != other.fieldLength)
				return false;
			if(fieldNumber != other.fieldNumber)
				return false;
			if(fieldDefId != other.fieldDefId)
				return false;
			if(key != other.key)
				return false;
			if(fieldName == null) {
				if(other.fieldName != null)
					return false;
			} else if(!fieldName.equals(other.fieldName))
				return false;
			return true;
		}

		private DFRReportDef getOuterType() {
			return DFRReportDef.this;
		}
	}
}
