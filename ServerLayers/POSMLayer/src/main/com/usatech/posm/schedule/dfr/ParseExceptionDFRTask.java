package com.usatech.posm.schedule.dfr;

import java.io.IOException;
import java.io.Reader;
import java.sql.Connection;

import simple.app.ServiceException;
import simple.db.DataLayerException;

public class ParseExceptionDFRTask extends AbstractDFRParser {
	protected long retryMs = 60 * 60 * 1000L;
	@Override
	protected long parseFileContent(Connection conn, long fileCacheId, String fileName, long startLineNumber, Reader fileContent) throws DataLayerException, IOException, ServiceException {
		return new DFRParser(fileCacheId, retryMs).parse(fileContent);
	}

}
