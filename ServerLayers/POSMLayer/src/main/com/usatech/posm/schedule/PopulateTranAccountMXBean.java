package com.usatech.posm.schedule;

import java.rmi.RemoteException;

import javax.management.MXBean;

@MXBean(true)
public interface PopulateTranAccountMXBean {
	public int processBatches(int batchSize, int maxBatches) throws RemoteException;

	public int processMissing(int batchSize) throws RemoteException;
	
	public void processMissingExpDate() throws RemoteException;
}
