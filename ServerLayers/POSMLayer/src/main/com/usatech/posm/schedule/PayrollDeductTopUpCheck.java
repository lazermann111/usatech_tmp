package com.usatech.posm.schedule;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;

import com.usatech.layers.common.QuartzCronScheduledWithPublisherJob;
import com.usatech.layers.common.model.PayrollSchedule;

@DisallowConcurrentExecution
public class PayrollDeductTopUpCheck extends QuartzCronScheduledWithPublisherJob {
	
	private static final Log log = Log.getLog();
	
	@Override
	public void executePostConfigure(JobExecutionContext context) throws JobExecutionException {
		try {
			Results results = DataLayerMgr.executeQuery("GET_PAYROLL_DEDUCT_CUSTOMERS", null);
			while(results.next()) {
				long customerId = results.getValue("customerId", Long.class);
				String customerName = results.getFormattedValue("customerName");
				try {
					LocalDate now = LocalDate.now();
					PayrollSchedule payrollSchedule = PayrollSchedule.loadFrom(results);
					LocalDate lastTopUpDate = results.getValue("lastTopUp", LocalDate.class);
					payrollSchedule.setCurrentDate(lastTopUpDate);
					LocalDate nextTopUpDate = payrollSchedule.getNextPayrollDate();
					if (nextTopUpDate.isBefore(now)) {
						log.info("Payroll deduct customer {0} {1} is due for topping up NOW by {2}", customerId, customerName, DateTimeFormatter.ISO_LOCAL_DATE.format(nextTopUpDate));
						DataLayerMgr.executeCall("TOP_UP_CUSTOMER_ACCOUNTS", new Object[] {customerId}, true);
					} else {
						log.info("Payroll deduct customer {0} {1} is not due for topping up until {2}", customerId, customerName, DateTimeFormatter.ISO_LOCAL_DATE.format(nextTopUpDate));
					}
				} catch(Exception e) {
					log.error("Error while topping up payroll deduct customer", e);
				}
			}
		} catch(Exception e) {
			log.error("Failed to find payroll deduct customers!", e);
		}
	}
}
