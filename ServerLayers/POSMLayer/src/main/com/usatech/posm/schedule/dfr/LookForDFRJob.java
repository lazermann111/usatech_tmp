package com.usatech.posm.schedule.dfr;

import java.sql.SQLException;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;
import simple.security.crypt.EncryptionInfoMapping;
import simple.text.RegexUtils;
import simple.text.StringUtils;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.QuartzCronScheduledWithPublisherJob;
import com.usatech.layers.common.constants.CommonAttrEnum;

@DisallowConcurrentExecution
public class LookForDFRJob extends QuartzCronScheduledWithPublisherJob {
	private static final Log log = Log.getLog();
	protected int searchDays = 5;
	protected String host;
	protected int port;
	protected String username;
	protected String password;
	protected boolean sensitive;
	protected String filePathPrefix;
	protected EncryptionInfoMapping encryptionInfoMapping;

	@Override
	public void executePostConfigure(JobExecutionContext context) throws JobExecutionException {
		// find any missing DFR reports
		try (Results results = DataLayerMgr.executeQuery("GET_UNRETRIEVED_DFR_FILES", this)) {
			while(results.next()) {
				// setup message chain to handle them
				String fileNameRegex = results.getValue("fileNameRegex", String.class);
				String fileVariety = results.getValue("fileVariety", String.class);
				MessageChain mc = new MessageChainV11();
				MessageChainStep downloadStep = mc.addStep("usat.file.download.sftp.inside");
				downloadStep.setAttribute(CommonAttrEnum.ATTR_HOST, getHost());
				downloadStep.setAttribute(CommonAttrEnum.ATTR_PORT, getPort());
				downloadStep.setAttribute(CommonAttrEnum.ATTR_USERNAME, getUsername());
				downloadStep.setSecretAttribute(CommonAttrEnum.ATTR_PASSWORD, getPassword());
				downloadStep.setAttribute(CommonAttrEnum.ATTR_SENSITIVE, isSensitive());
				downloadStep.setAttribute(CommonAttrEnum.ATTR_WILDCARD, true);
				downloadStep.setAttribute(CommonAttrEnum.ATTR_FILE_PATH, StringUtils.isBlank(getFilePathPrefix()) ? fileNameRegex : RegexUtils.escape(getFilePathPrefix()) + fileNameRegex);

				MessageChainStep saveStep = mc.addStep("usat.load.file.oper");
				saveStep.setAttribute(CommonAttrEnum.ATTR_HOST, getHost());
				saveStep.setAttribute(CommonAttrEnum.ATTR_FILE_TYPE, 2);
				saveStep.setAttribute(CommonAttrEnum.ATTR_COMPRESS, true);
				saveStep.setReferenceAttribute(CommonAttrEnum.ATTR_RESOURCE, downloadStep, CommonAttrEnum.ATTR_RESOURCE);
				saveStep.setReferenceAttribute(CommonAttrEnum.ATTR_RESOURCE_MODIFIED_TIME, downloadStep, CommonAttrEnum.ATTR_RESOURCE_MODIFIED_TIME);
				saveStep.setReferenceAttribute(CommonAttrEnum.ATTR_FILE_PATH, downloadStep, CommonAttrEnum.ATTR_FILE_PATH);

				MessageChainStep deleteStep = mc.addStep("usat.file.resource.delete");
				deleteStep.setReferenceAttribute(CommonAttrEnum.ATTR_RESOURCE, downloadStep, CommonAttrEnum.ATTR_RESOURCE);

				downloadStep.setNextSteps(0, saveStep);
				MessageChainStep parseStep = null;
				if(!StringUtils.isBlank(fileVariety)) {
					switch(fileVariety) {
						case "A":
							parseStep = mc.addStep("usat.dfr.parse.exception");
							parseStep.setReferenceAttribute(CommonAttrEnum.ATTR_FILE_ID, saveStep, CommonAttrEnum.ATTR_FILE_ID);
							break;
						case "B":
							parseStep = mc.addStep("usat.dfr.parse.financial");
							parseStep.setReferenceAttribute(CommonAttrEnum.ATTR_FILE_ID, saveStep, CommonAttrEnum.ATTR_FILE_ID);
							break;
					}
				}
				if(parseStep == null) {
					saveStep.setNextSteps(0, deleteStep);
					saveStep.setNextSteps(1, deleteStep);
				} else {
					saveStep.setNextSteps(0, parseStep, deleteStep);
					saveStep.setNextSteps(1, parseStep, deleteStep);
				}
				MessageChainService.publish(mc, getPublisher(), getEncryptionInfoMapping());
			}
		} catch(SQLException | DataLayerException | ConvertException | ServiceException e) {
			log.error("While looking up files", e);
			throw new JobExecutionException(e);
		}
	}

	public int getSearchDays() {
		return searchDays;
	}

	public void setSearchDays(int searchDays) {
		this.searchDays = searchDays;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isSensitive() {
		return sensitive;
	}

	public void setSensitive(boolean sensitive) {
		this.sensitive = sensitive;
	}

	public String getFilePathPrefix() {
		return filePathPrefix;
	}

	public void setFilePathPrefix(String filePathPrefix) {
		this.filePathPrefix = filePathPrefix;
	}

	public EncryptionInfoMapping getEncryptionInfoMapping() {
		return encryptionInfoMapping;
	}

	public void setEncryptionInfoMapping(EncryptionInfoMapping encryptionInfoMapping) {
		this.encryptionInfoMapping = encryptionInfoMapping;
	}

}
