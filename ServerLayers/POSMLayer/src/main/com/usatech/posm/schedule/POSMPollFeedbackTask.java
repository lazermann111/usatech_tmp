package com.usatech.posm.schedule;

import java.lang.management.ManagementFactory;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import simple.app.Publisher;
import simple.app.SelfProcessor;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.ByteInput;
import simple.io.Log;
import simple.results.Results;

import com.usatech.app.Attribute;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CommonAttrEnum;

public class POSMPollFeedbackTask implements SelfProcessor {
	private static final Log log = Log.getLog();
	protected String authorityQueueKeyPrefix = "usat.authority.feedback.";
	protected String pollUnlockQueue = "posm.terminal.settlement.poll.unlock";
	protected String feedbackQueue = "posm.terminal.settlement.feedback";
	protected Publisher<ByteInput> publisher;
	
	@Override
	public void process() {
		try {
			Publisher<ByteInput> publisher = getPublisher();
			if(publisher == null) {
				throw new ServiceException("Publisher is not set");
			}
			Map<String,Object> params = new HashMap<String, Object>();
			// check and lock in db
			String processId = ManagementFactory.getRuntimeMXBean().getName();
			params.put("processId", processId);
			DataLayerMgr.executeCall("LOCK_PARTIAL_SETTLEMENT_POLLING", params, true);
			boolean success = "Y".equals(ConvertUtils.getString(params.get("successFlag"), true));
			if(!success) {
				log.info("Partial Settlement Polling is already locked by '" + params.get("lockedBy") + "'");
			} else {
				boolean okay = false;
				try {
					// find distinct queueKey of all partial settlements
					Results results = DataLayerMgr.executeQuery("GET_PARTIAL_SETTLEMENT_QUEUES", null);
					// send to each queue request for poll of files
					MessageChain mc = new MessageChainV11();
					MessageChainStep prevStep = null;
					while(results.next()) {
						String queueKey = results.getValue("gatewayQueueKey", String.class);
						int count = results.getValue("count", Integer.class);
						log.info("Requesting partial settlement polling for '" + queueKey + "' (" + count + " outstanding settlements)");
						MessageChainStep pollStep = mc.addStep(getAuthorityQueueKeyPrefix() + queueKey);
						copyAttribute(pollStep, results, AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_CLASS, String.class, true);
						pollStep.setAttribute(AuthorityAttrEnum.ATTR_FEEDBACK_PROCESS_QUEUE, getFeedbackQueue());
						if(prevStep != null) {
							prevStep.setNextSteps(0, pollStep);
							prevStep.setNextSteps(1, pollStep);
						}
						prevStep = pollStep;
					}
					MessageChainStep unlockStep = mc.addStep(getPollUnlockQueue());
					unlockStep.setAttribute(CommonAttrEnum.ATTR_PROCESS_ID, processId);
					if(prevStep != null) {
						prevStep.setNextSteps(0, unlockStep);
						prevStep.setNextSteps(1, unlockStep);
					}
					MessageChainService.publish(mc, publisher);
					okay = true;
				} finally {
					if(!okay) 
						try {
							DataLayerMgr.executeCall("UNLOCK_PARTIAL_SETTLEMENT_POLLING", Collections.singletonMap(CommonAttrEnum.ATTR_PROCESS_ID.getValue(), processId), true);
						} catch(SQLException e) {
							log.error("Failed to unlock feedback polling for process id '" + processId + "'. Please do so manually", e); 
						} catch(DataLayerException e) {
							log.error("Failed to unlock feedback polling for process id '" + processId + "'. Please do so manually", e); 
						} 
				}
			}
		} catch(ConvertException e) {
			log.error("Could not convert parameters", e);
		} catch(SQLException e) {
			log.error("Error on database connection", e);
		} catch(DataLayerException e) {
			log.error("Error on database connection", e);
		} catch(ServiceException e) {
			log.error("Could not publish message chain", e); 
		} catch(RuntimeException e) {
			log.error("Could not poll for feedback", e); 
		} catch(Error e) {
			log.error("Could not poll for feedback", e); 
		}
	}

	@SuppressWarnings("unchecked")
	protected static <T> T copyAttribute(MessageChainStep step, Results results, Attribute attribute, Class<T> convertTo, boolean required) throws ServiceException {
		Object value = results.getValue(attribute.getValue());
		if(convertTo != null) {
			try {
				value = ConvertUtils.convert(convertTo, value);
			} catch(ConvertException e) {
				throw new ServiceException("Attribute '" + attribute.getValue() + "' could not be converted to " + convertTo.getName(), e);
			}
		}
		if(required && value == null)
			throw new ServiceException("Attribute '" + attribute.getValue() + "' is required but not provided");
		step.setAttribute(attribute, value);
		return (T)value;
	} 
	
	public String getAuthorityQueueKeyPrefix() {
		return authorityQueueKeyPrefix;
	}

	public void setAuthorityQueueKeyPrefix(String authorityQueueKeyPrefix) {
		this.authorityQueueKeyPrefix = authorityQueueKeyPrefix;
	}

	public String getPollUnlockQueue() {
		return pollUnlockQueue;
	}

	public void setPollUnlockQueue(String pollUnlockQueue) {
		this.pollUnlockQueue = pollUnlockQueue;
	}

	public String getFeedbackQueue() {
		return feedbackQueue;
	}

	public void setFeedbackQueue(String feedbackQueue) {
		this.feedbackQueue = feedbackQueue;
	}

	public Publisher<ByteInput> getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}
}
