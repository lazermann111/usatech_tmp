package com.usatech.posm.schedule.dfr;

import static simple.text.MessageFormat.*;

import java.sql.Connection;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import simple.app.ServiceException;
import simple.db.*;
import simple.io.Log;
import simple.sql.SQLType;
import simple.sql.SQLTypeUtils;
import simple.text.StringUtils;

import com.usatech.app.AbstractAttributeDatasetHandler;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.posm.schedule.dfr.DFRReportDef.DFRReportFieldDef;

public class DFRDatasetHandler extends AbstractAttributeDatasetHandler<ServiceException> {
	public static final Log log = Log.getLog();
	
	private DFRReportDef reportDef;
	private DFRReport report;
	
	private Call insertCall;
	private Call checkExistsCall;
	
	public DFRDatasetHandler(DFRReportDef reportDef, DFRReport report) {
		this.reportDef = reportDef;
		this.report = report;
		
		insertCall = generateInsertCall(reportDef);
		checkExistsCall = generateCheckExistsCall(reportDef);
	}
	
	protected Call generateInsertCall(DFRReportDef reportDef) {
		List<DFRReportFieldDef> fields = reportDef.getFields();
		
		List<Parameter> params = new ArrayList<Parameter>();
		params.add(new Parameter(null, new SQLType(Types.NUMERIC), false, false, true));
		params.add(new Parameter("reportId", new SQLType(Types.NUMERIC), true, true, false));
		
		StringBuilder columnNames = new StringBuilder();
		StringBuilder placeHolders = new StringBuilder();
		
		columnNames.append("DFR_REPORT_ID");
		placeHolders.append("?");
		
		for(int i=0; i<fields.size(); i++) {
			DFRReportFieldDef field = fields.get(i);
			if (field.getColumnName() == null)
				continue;
			
			columnNames.append(",");
			placeHolders.append(",");
			
			columnNames.append(field.getColumnName());
			placeHolders.append("?");
			
			Parameter param = new Parameter(field.getFieldName(), new SQLType(SQLTypeUtils.getTypeCode(field.getDataType())), false, true, false);
			params.add(param);
		}
		
		Parameter[] args = new Parameter[params.size()];
		params.toArray(args);
		
		String sql = format("INSERT INTO {0}({1}) VALUES ({2})", reportDef.getTableName(), columnNames, placeHolders);
		
		Call call = new Call();
		call.setSql(sql);
		call.setArguments(args);
		
		return call;
	}
	
	protected Call generateCheckExistsCall(DFRReportDef reportDef) {
		List<DFRReportFieldDef> fields = reportDef.getFields();
		
		List<Argument> params = new ArrayList<Argument>();
		
		Column column = new Column();
		column.setName("COUNT");
		column.setPropertyName("COUNT");
		column.setIndex(1);
		column.setSqlType(Types.NUMERIC);
		
		Cursor cursor = new Cursor();
		cursor.setAllColumns(false);
		cursor.setUpdateable(false);
		cursor.setChangeable(false);
		cursor.setCaching(false);
		
		cursor.setColumns(new Column[] {column});

		params.add(cursor);
		
		StringBuilder whereClause = new StringBuilder();
		
		boolean first = true;
		for(int i=0; i<fields.size(); i++) {
			DFRReportFieldDef field = fields.get(i);
			if (!field.isKey())
				continue;
			
			if (!first) {
				whereClause.append(" AND ");
			}
			
			whereClause.append(format("{0}=?", field.getColumnName()));

			Parameter param = new Parameter(field.getFieldName(), new SQLType(SQLTypeUtils.getTypeCode(field.getDataType())), false, true, false);
			params.add(param);
			
			first = false;
		}
		
		String sql = format("SELECT COUNT(1) FROM {0} WHERE {1}", reportDef.getTableName(), whereClause.toString());

		Argument[] args = new Argument[params.size()];
		params.toArray(args);
		
		Call call = new Call();
		call.setSql(sql);
		call.setArguments(args);
		
		return call;
	}
	
	@Override
	public void handleValue(String columnName, Object value) {
		if (value == null || (value instanceof String && StringUtils.isBlank(((String)value))))
			return;
		if (value instanceof String) {
			String s = (String) value;
			s = s.replaceAll("\r", "");
			s = s.replaceAll("\n", "");
			s = s.replaceAll("\"", "");
			s = s.trim();
			value = s;
		}
		super.handleValue(columnName, value);
	}

	@Override
	public void handleDatasetStart(String[] columnNames) throws ServiceException {
	}

	@Override
	public void handleDatasetEnd() throws ServiceException {
	}
	
	@Override
	public void handleRowEnd() throws ServiceException {
		Connection conn = null;
		try{
			try {
				conn = DataLayerMgr.getConnection("OPER", true);
			} catch (Exception e) {
				throw new ServiceException(format("Failed to get Connection to OPER"), e);
			}
			
			int count = 0;
			
			try {
				checkExistsCall.selectInto(conn, data, null);
				count = getDetailAttribute("COUNT", Integer.class, true);
			} catch (Exception e) {
				throw new ServiceException(format("Failed to select from \"{0}\" using dynamic SQL \"{1}\"", reportDef.getTableName(), checkExistsCall.getSql()), e);
			}
		
		
			if (count > 0) {
				return;
			}
			
			data.put("reportId", report.getReportId());
			
			try {
				insertCall.executeCall(conn, data, null);
			} catch (Exception e) {
				ProcessingUtils.rollbackDbConnection(log, conn);
				throw new ServiceException(format("Failed to insert into \"{0}\" using dynamic SQL \"{1}\"", reportDef.getTableName(), insertCall.getSql()), e);
			}
		}finally{
			ProcessingUtils.closeDbConnection(log, conn);
		}
	}
	
}
