package com.usatech.posm.schedule;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import simple.db.DataLayerMgr;
import simple.io.Log;

import com.usatech.layers.common.QuartzCronScheduledWithPublisherJob;

@DisallowConcurrentExecution
public class RetryIncompleteErrorCheck extends QuartzCronScheduledWithPublisherJob {
	private static final Log log = Log.getLog();
	@Override
	public void executePostConfigure(JobExecutionContext context) throws JobExecutionException {
		try {
			DataLayerMgr.executeCall("RETRY_INCOMPLETE_ERROR_TRANS", new Object[] {}, true);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}
}
