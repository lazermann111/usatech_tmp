package com.usatech.posm.schedule;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import simple.app.BasicQoS;
import simple.app.Publisher;
import simple.app.SelfProcessor;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.ByteInput;
import simple.io.Log;
import simple.results.Results;
import simple.util.concurrent.CustomThreadFactory;
import simple.util.concurrent.NotifyingBlockingThreadPoolExecutor;

import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.SubmitRefundAttribute;

public class POSMRefundPropagatorTask implements SelfProcessor {
	private static final Log log = Log.getLog();
	
	protected String REFUND_PROPAGATION = "REFUND_PROPAGATION";
	protected String posmCheckQueue = "usat.posm.terminal.check";
	protected final BasicQoS posmCheckQueueQos = new BasicQoS(300000, 5, false, null);
	protected Publisher<ByteInput> publisher;

	protected int appInstance;
	protected int maxItems = 100;
	protected int poolSize = 5;
	protected int queueSize = 10;
	protected long waitCheckInterval = 5000;
	protected long lockUpdateInterval = 900000;
	protected NotifyingBlockingThreadPoolExecutor threadPoolExecutor = null;

	@Override
	public void process() {
		if (threadPoolExecutor == null)
			threadPoolExecutor = new NotifyingBlockingThreadPoolExecutor(poolSize, queueSize, new CustomThreadFactory(Thread.currentThread().getName() + '-', true));
		
		long startTs = System.currentTimeMillis();
		long totalItemCount = 0;
		int itemCount = 0;
        Results results = null;
        Map<String,Object> params = new HashMap<String, Object>();
        params.put("maxItems", maxItems);
        try {
        	if (!InteractionUtils.lockProcess(REFUND_PROPAGATION, String.valueOf(appInstance), params)) {
        		log.info("Refund Propagation is already locked by instance " + params.get("lockedBy"));
        		return;
        	}
        	long lockUpdatedTs = System.currentTimeMillis();
        	
        	while (true) {
				itemCount = 0;
				results = DataLayerMgr.executeQuery("GET_UNPROCESSED_REFUNDS", params);
				
	            while(results.next()) {
	            	itemCount++;
	            	
	            	if (System.currentTimeMillis() - lockUpdatedTs > lockUpdateInterval) {
	            		if (!InteractionUtils.lockProcess(REFUND_PROPAGATION, String.valueOf(appInstance), params)) {
	                		log.error("Refund Propagation got locked by instance " + params.get("lockedBy") + " during processing, exiting");
	                		threadPoolExecutor.shutdownNow();
	                		return;
	            		}
	            		lockUpdatedTs = System.currentTimeMillis();
	            	}
	            	
	            	Map<String,Object> attributes = new HashMap<String, Object>();
	            	attributes.put("processedFlag", results.get("processedFlag"));
	            	attributes.put("refundId", results.get("refundId"));
	            	attributes.put("settleState", results.get("settleState"));
	            	attributes.put("settleDate", results.get("settleDate"));
	            	attributes.put("machineTransNo", results.get("machineTransNo"));
	            	attributes.put("refundAmt", results.get("refundAmt"));
	            	attributes.put("currencyCode", results.get("currencyCode"));
	            	attributes.put("reason", results.get("reason"));
	            	attributes.put("comment", results.get("comment"));
	            	attributes.put("sourceSystem", results.get("sourceSystem"));
	            	attributes.put("refundMachineTransNo", results.get("refundMachineTransNo"));
	            	attributes.put("origUploadTs", results.get("origUploadTs"));
	            	attributes.put("issuerUserName", results.get("issuerUserName"));
	            	attributes.put("origTranAmt", results.get("origTranAmt"));
	            	attributes.put("refundTs", results.get("refundTs"));
	            	attributes.put("prevRefundAmt", results.get("prevRefundAmt"));
	            	attributes.put("origSourceTranId", results.get("origSourceTranId"));
	            	attributes.put("applyToConsumerAcctId", results.get("applyToConsumerAcctId"));
	                
	            	threadPoolExecutor.execute(new ThreadTask(attributes));
	            }
	
				if (itemCount > 0) {
					totalItemCount += itemCount;
					while (threadPoolExecutor.getActiveCount() > 0)
						threadPoolExecutor.await(waitCheckInterval, TimeUnit.MILLISECONDS);
				}
				
				if (itemCount < maxItems)
					break;
        	}
	    } catch(Exception e) {
	    	log.error("Error processing refunds", e);
        } finally {
        	if (results != null)
	    		results.close();
        	try {
        		InteractionUtils.unlockProcess(REFUND_PROPAGATION, String.valueOf(appInstance), params);
        	} catch (Exception e) {
        		log.error("Error unlocking " + REFUND_PROPAGATION, e);
        	}
        	if (totalItemCount > 0)
				log.info("Processed " + totalItemCount + " refund(s) in " + (System.currentTimeMillis() - startTs) + " ms");
        }
	}
	
	protected class ThreadTask implements Runnable {
		protected Map<String,Object> attributes;
		
		public ThreadTask(Map<String,Object> attributes) {
			this.attributes = attributes;
		}
		
		public void run() {
			boolean okay = false;
			Connection sourceConn = null;
			Connection targetConn = null;
			int refundId = -1;
			try {
                refundId = ConvertUtils.convert(int.class, attributes.get("refundId"));
                int settleState = ConvertUtils.convert(int.class, attributes.get("settleState"));
                String sourceSystem = ConvertUtils.convert(String.class, attributes.get("sourceSystem"));
                
                sourceConn = DataLayerMgr.getConnection("REPORT");
                if(!"PSS".equalsIgnoreCase(sourceSystem)) {
                	log.warn("Refund " + refundId + " was not propagated because its source system " + sourceSystem + " is unsupported");
                	attributes.put("processed", "Y");
                	DataLayerMgr.executeUpdate(sourceConn, "SET_REFUND_STATE", attributes);
                	DataLayerMgr.executeUpdate(sourceConn, "SET_SETTLE_STATE_TO_FAILED", attributes);
                	sourceConn.commit();
                	okay = true;
                	return;
                }
                switch(settleState) {
                    case 5: //declined
                        log.warn("Transaction was declined. No refund issued for refund " + refundId);
                        attributes.put("processed", "Y");
                        DataLayerMgr.executeUpdate(sourceConn, "SET_REFUND_STATE", attributes);
                        DataLayerMgr.executeUpdate(sourceConn, "SET_SETTLE_STATE_TO_FAILED", attributes);
                        sourceConn.commit();
                        okay = true;
                        break;
                    case 3:   
                    	String processedFlag = ConvertUtils.convert(String.class, attributes.get("processedFlag"));
                    	if(processedFlag!=null&&processedFlag.equals("I")){
                    		DataLayerMgr.executeUpdate(sourceConn, "REFUND_SPROUT_REPLENISH", attributes);
                    		attributes.put("processed", "S");
                    		DataLayerMgr.executeUpdate(sourceConn, "SET_REFUND_STATE", attributes);
                            sourceConn.commit();
                            okay = true;
                    	}else{
	                    	Date refundTs = ConvertUtils.convert(Date.class, attributes.get("refundTs"));
	                    	Date uploadTs = ConvertUtils.convert(Date.class, attributes.get("origUploadTs"));
	                    	BigDecimal refundAmt = ConvertUtils.convert(BigDecimal.class, attributes.get("refundAmt")).abs();
	                		BigDecimal prevRefundAmt = ConvertUtils.convert(BigDecimal.class, attributes.get("prevRefundAmt"));
	                		BigDecimal origTranAmt = ConvertUtils.convert(BigDecimal.class, attributes.get("origTranAmt"));
	                		BigDecimal generalRefundAmt;
	                		BigDecimal additionalRefundAmt;
	
	                		if(origTranAmt == null) {
	                			generalRefundAmt = refundAmt;
	                			additionalRefundAmt = null;
	                		} else {
	                			BigDecimal unrefunded = (prevRefundAmt == null ? origTranAmt : origTranAmt.subtract(prevRefundAmt.abs()));
	                			if(unrefunded.compareTo(refundAmt) < 0) {
	                				generalRefundAmt = unrefunded;
	                				additionalRefundAmt = refundAmt.subtract(unrefunded);
	                			} else {
	                				generalRefundAmt = refundAmt;
	                				additionalRefundAmt = null;
	                			}
	                		}
	
	        		        attributes.put(SubmitRefundAttribute.ATTR_ADDITIONAL_REFUND_AMOUNT.getValue(), additionalRefundAmt);
	        				attributes.put(SubmitRefundAttribute.ATTR_GENERAL_REFUND_AMOUNT.getValue(), generalRefundAmt);
	        				attributes.put(SubmitRefundAttribute.ATTR_ISSUER.getValue(), attributes.get("issuerUserName"));
	        				attributes.put(SubmitRefundAttribute.ATTR_ORIG_GLOBAL_TRANS_CD.getValue(), attributes.get("machineTransNo"));
	        				attributes.put(SubmitRefundAttribute.ATTR_REFUND_GLOBAL_TRANS_CD.getValue(), attributes.get("refundMachineTransNo"));
	        				attributes.put(SubmitRefundAttribute.ATTR_REFUND_TIME.getValue(), refundTs.getTime());
	        				attributes.put(SubmitRefundAttribute.ATTR_ORIG_IMPORT_TIME.getValue(), uploadTs.getTime());
	        		        
	        		    	Map<String, Object> params = new HashMap<String, Object>();
	        		    	targetConn = DataLayerMgr.getConnectionForCall("CREATE_REFUND");
	    		    		DataLayerMgr.executeCall(targetConn, "CREATE_REFUND", attributes, params);
	    		    		boolean alreadyInserted = "Y".equalsIgnoreCase(ConvertUtils.getString(params.get("alreadyInsertedFlag"), true).trim());
	    		    		BigDecimal minorCurrencyFactor = ConvertUtils.convertRequired(BigDecimal.class, params.get("minorCurrencyFactor"));
	    		    		long paymentSubtypeKeyId = ConvertUtils.getLong(params.get("paymentSubtypeKeyId"));
	    		    		String paymentSubtypeClass = ConvertUtils.getString(params.get("paymentSubtypeClass"), true);
	    		    		if(alreadyInserted)
	    		    			log.info("Refund " + ConvertUtils.getString(attributes.get(SubmitRefundAttribute.ATTR_REFUND_GLOBAL_TRANS_CD.getValue()), true) + " is a duplicate");
	    		    		else {
	    			    		params.put("hostPortNum", 0); // use base host
	    			    		params.put("tliQuantity", 1);
	    			    		params.put("tliDesc", ConvertUtils.getString(attributes.get(SubmitRefundAttribute.ATTR_REASON.getValue()), false));
	    						long refundTime = ConvertUtils.getLong(attributes.get(SubmitRefundAttribute.ATTR_REFUND_TIME.getValue()));
	    			    		params.put("tliUtcTsMs", refundTime);
	    						params.put("tliUtcOffsetMin", TimeZone.getDefault().getOffset(refundTime) / 60000);
	    						if(generalRefundAmt != null && generalRefundAmt.compareTo(BigDecimal.ZERO) != 0) {
	    			    			params.put("tliTypeId", 500);
	    			    			params.put("tliAmount", generalRefundAmt.abs().multiply(minorCurrencyFactor));
	    			    			DataLayerMgr.executeCall(targetConn, "CREATE_TRAN_LINE_ITEM_REFUND", params);
	    			    		}
	    						if(additionalRefundAmt != null && additionalRefundAmt.compareTo(BigDecimal.ZERO) != 0) {
	    			    			params.put("tliTypeId", 501);
	    			    			params.put("tliAmount", additionalRefundAmt.abs().multiply(minorCurrencyFactor));
	    			    			DataLayerMgr.executeCall(targetConn, "CREATE_TRAN_LINE_ITEM_REFUND", params);
	    			    		}
	    		    		}
							attributes.put("processed", "Y");
							long tranId = ConvertUtils.getLong(params.get("tranId"));
							attributes.put("sourceTranId", tranId);
	                        DataLayerMgr.executeUpdate(sourceConn, "SET_REFUND_STATE", attributes);
	                        targetConn.commit();
	                    	sourceConn.commit();
	                    	okay = true;
	                    	log.info("Propagated refund " + refundId);
							
	                    	if (!alreadyInserted) {
	
								//publish to check task
		        				String queueKey = getPosmCheckQueue();
		        				if(queueKey == null || (queueKey=queueKey.trim()).length() == 0)
		        					log.warn("PosmCheckQueue not set on " + this);
		        				else {
		        					MessageChainV11 mc = new MessageChainV11();
		        					MessageChainStep checkStep = mc.addStep(queueKey);
		        					checkStep.setAttribute(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_KEY_ID, paymentSubtypeKeyId);
		        					checkStep.setAttribute(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_CLASS, paymentSubtypeClass);
		        					MessageChainService.publish(mc, publisher, null, getPosmCheckQueueQos());
		        				}
	                    	}
                    	}
                        break;
                    default:
                        log.warn("Transaction of refund " + refundId + " is not yet settled. Refund not propagated yet.");
                }            
			} catch(Exception e) {
				log.error("Error processing refund " + refundId, e);				
			} finally {
				if(!okay) {
					ProcessingUtils.rollbackDbConnection(log, sourceConn);
	        		ProcessingUtils.rollbackDbConnection(log, targetConn);
				}
	        	ProcessingUtils.closeDbConnection(log, sourceConn);
	        	ProcessingUtils.closeDbConnection(log, targetConn);
			}
		}
	}	

	public int getAppInstance() {
		return appInstance;
	}

	public void setAppInstance(int appInstance) {
		this.appInstance = appInstance;
	}
	
	public int getMaxItems() {
		return maxItems;
	}

	public void setMaxItems(int maxItems) {
		this.maxItems = maxItems;
	}	
	
	public int getPoolSize() {
		return poolSize;
	}

	public void setPoolSize(int poolSize) {
		this.poolSize = poolSize;
	}

	public int getQueueSize() {
		return queueSize;
	}

	public void setQueueSize(int queueSize) {
		this.queueSize = queueSize;
	}
	
	public String getPosmCheckQueue() {
		return posmCheckQueue;
	}

	public void setPosmCheckQueue(String posmCheckQueue) {
		this.posmCheckQueue = posmCheckQueue;
	}

	public BasicQoS getPosmCheckQueueQos() {
		return posmCheckQueueQos;
	}
	
	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}

	public Publisher<ByteInput> getPublisher() {
		return publisher;
	}

	public long getWaitCheckInterval() {
		return waitCheckInterval;
	}

	public void setWaitCheckInterval(long waitCheckInterval) {
		this.waitCheckInterval = waitCheckInterval;
	}
	
	public long getLockUpdateInterval() {
		return lockUpdateInterval;
	}

	public void setLockUpdateInterval(long lockUpdateInterval) {
		this.lockUpdateInterval = lockUpdateInterval;
	}	
}
