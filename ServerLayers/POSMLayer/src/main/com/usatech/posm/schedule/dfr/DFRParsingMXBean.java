package com.usatech.posm.schedule.dfr;

import java.io.IOException;
import java.sql.SQLException;

import javax.management.MXBean;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.db.DataLayerException;

@MXBean(true)
public interface DFRParsingMXBean {
	public boolean parseFile(long fileCacheId) throws SQLException, DataLayerException, ServiceException, IOException, ConvertException;

}
