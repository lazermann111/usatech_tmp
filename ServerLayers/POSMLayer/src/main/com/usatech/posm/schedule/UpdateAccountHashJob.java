package com.usatech.posm.schedule;

import java.lang.management.ManagementFactory;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import simple.app.Publisher;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.ByteInput;
import simple.io.Log;
import simple.io.resource.ResourceFolder;
import simple.results.Results;
import simple.security.crypt.EncryptionInfoMapping;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CommonAttrEnum;

@DisallowConcurrentExecution
public class UpdateAccountHashJob implements Job, UpdateAccountHashMXBean {
	private static final Log log = Log.getLog();
	protected static final String LOCK_PROCESS_TOKEN_CD = ManagementFactory.getRuntimeMXBean().getName() + ':' + Long.toHexString(System.currentTimeMillis()).toUpperCase() + ':' + Long.toHexString(Double.doubleToLongBits(Math.random()));
	protected static final String PROCESS_CD = "UPDATE_ACCOUNT_HASH";
	protected ResourceFolder resourceFolder;
	protected String massExtractHashQueueKey;
	protected String massUpdateHashQueueKey;
	protected EncryptionInfoMapping encryptionInfoMapping;
	protected int maxBatchSize = 1000;
	protected Publisher<ByteInput> publisher;
	public static final ObjectName JMX_OBJECT_NAME;
	static {
		ObjectName on;
		try {
			on = new ObjectName("AppJobs:Name=UpdateAccountHash");
		} catch(MalformedObjectNameException e) {
			log.error(e);
			on = null;
		} catch(NullPointerException e) {
			log.error(e);
			on = null;
		}
		JMX_OBJECT_NAME = on;
	}
	protected static UpdateAccountHashJob instance;
	protected boolean jmxEnabled;

	public static UpdateAccountHashJob getInstance() {
		if(instance == null)
			instance = new UpdateAccountHashJob();
		return instance;
	}
	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {
			process(getMaxBatchSize(), false);
		} catch(IllegalStateException e) {
			throw new JobExecutionException(e);
		} catch(SQLException e) {
			throw new JobExecutionException(e);
		} catch(DataLayerException e) {
			throw new JobExecutionException(e);
		} catch(ConvertException e) {
			throw new JobExecutionException(e);
		} catch(ServiceException e) {
			throw new JobExecutionException(e);
		}
	}

	@Override
	public int updateMissingAccountHashes(int batchSize) throws RemoteException {
		try {
			return process(batchSize, true);
		} catch(IllegalStateException e) {
			throw new RemoteException("Error", e);
		} catch(SQLException e) {
			throw new RemoteException("Error", e);
		} catch(DataLayerException e) {
			throw new RemoteException("Error", e);
		} catch(ConvertException e) {
			throw new RemoteException("Error", e);
		} catch(ServiceException e) {
			throw new RemoteException("Error", e);
		}
	}

	protected int process(int batchSize, boolean lock) throws SQLException, DataLayerException, IllegalStateException, ConvertException, ServiceException {
		if(resourceFolder == null)
			throw new IllegalStateException("ResourceFolder is not set");
		if(publisher == null)
			throw new IllegalStateException("Publisher is not set");
		final Map<String, Object> params = new HashMap<String, Object>();
		if(lock && !InteractionUtils.lockProcess(PROCESS_CD, LOCK_PROCESS_TOKEN_CD, params)) {
			log.error("Process is already locked");
			throw new ServiceException("Process is already locked");
		}
		try {
			params.clear();
			params.put("maxRows", batchSize);
			Results results = DataLayerMgr.executeQuery("GET_MISSING_ACCT_HASH", params);
			try {
				StringBuilder sb = new StringBuilder();
				long prevGlobalAccountId = 0;
				boolean inRange = false;
				while(results.next()) {
					long globalAccountId = results.getValue("globalAccountId", Long.class);
					if(sb.length() == 0)
						sb.append(globalAccountId);
					else if(prevGlobalAccountId + (globalAccountId < 1000000000L ? 1 : 10) == globalAccountId) {
						if(!inRange)
							sb.append('-');
						inRange = true;
					} else if(inRange) {
						sb.append(prevGlobalAccountId).append('|').append(globalAccountId);
						inRange = false;
					} else
						sb.append('|').append(globalAccountId);
					prevGlobalAccountId = globalAccountId;
				}
				if(inRange)
					sb.setLength(sb.length() - 1);
				MessageChain messageChain = new MessageChainV11();
				MessageChainStep massExtractStep = messageChain.addStep(getMassExtractHashQueueKey());
				massExtractStep.setAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, sb.toString());

				MessageChainStep massUpdateStep = messageChain.addStep(getMassUpdateHashQueueKey());
				massUpdateStep.setReferenceAttribute(CommonAttrEnum.ATTR_CRYPTO_RESOURCE, massExtractStep, CommonAttrEnum.ATTR_CRYPTO_RESOURCE);
				massUpdateStep.setReferenceAttribute(CommonAttrEnum.ATTR_CRYPTO_ENCRYPTION_KEY, massExtractStep, CommonAttrEnum.ATTR_CRYPTO_ENCRYPTION_KEY);
				massUpdateStep.setReferenceAttribute(CommonAttrEnum.ATTR_CRYPTO_CIPHER, massExtractStep, CommonAttrEnum.ATTR_CRYPTO_CIPHER);
				massUpdateStep.setReferenceAttribute(CommonAttrEnum.ATTR_CRYPTO_BLOCK_SIZE, massExtractStep, CommonAttrEnum.ATTR_CRYPTO_BLOCK_SIZE);

				massExtractStep.setNextSteps(0, massUpdateStep);
				// send message chain
				MessageChainService.publish(messageChain, publisher, encryptionInfoMapping);
				return results.getRowCount();
			} finally {
				results.close();
			}
		} finally {
			if(lock) {
				params.clear();
				InteractionUtils.unlockProcess(PROCESS_CD, LOCK_PROCESS_TOKEN_CD, params);
			}
		}
	}

	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}

	public String getMassExtractHashQueueKey() {
		return massExtractHashQueueKey;
	}

	public void setMassExtractHashQueueKey(String massUpdateAccountQueueKey) {
		this.massExtractHashQueueKey = massUpdateAccountQueueKey;
	}

	public EncryptionInfoMapping getEncryptionInfoMapping() {
		return encryptionInfoMapping;
	}

	public void setEncryptionInfoMapping(EncryptionInfoMapping encryptionInfoMapping) {
		this.encryptionInfoMapping = encryptionInfoMapping;
	}

	public int getMaxBatchSize() {
		return maxBatchSize;
	}

	public void setMaxBatchSize(int maxBatchSize) {
		this.maxBatchSize = maxBatchSize;
	}

	public Publisher<ByteInput> getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}

	public String getMassUpdateHashQueueKey() {
		return massUpdateHashQueueKey;
	}

	public void setMassUpdateHashQueueKey(String mainUpdateQueueKey) {
		this.massUpdateHashQueueKey = mainUpdateQueueKey;
	}

	public boolean isJmxEnabled() {
		return jmxEnabled;
	}

	public void setJmxEnabled(boolean jmxEnabled) throws InstanceAlreadyExistsException, MBeanRegistrationException, NotCompliantMBeanException, InstanceNotFoundException {
		if(this.jmxEnabled == jmxEnabled)
			return;
		if(jmxEnabled) {
			MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
			mbs.registerMBean(this, JMX_OBJECT_NAME);
		} else {
			MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
			mbs.unregisterMBean(JMX_OBJECT_NAME);
		}
		this.jmxEnabled = jmxEnabled;
	}
}
