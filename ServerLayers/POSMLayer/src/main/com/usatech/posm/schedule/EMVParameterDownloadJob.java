package com.usatech.posm.schedule;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.QuartzCronScheduledWithPublisherJob;
import com.usatech.layers.common.constants.AuthorityAction;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.CurrencyCd;

import simple.app.Publisher;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.ByteInput;
import simple.io.Log;
import simple.results.Results;
import simple.security.crypt.EncryptionInfoMapping;

public class EMVParameterDownloadJob  extends QuartzCronScheduledWithPublisherJob {
	private static final Log log = Log.getLog();
	protected Publisher<ByteInput> publisher;
	protected EncryptionInfoMapping encryptionInfoMapping;

	@Override
	public void executePostConfigure(JobExecutionContext context) throws JobExecutionException {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			try (Results results = DataLayerMgr.executeQuery("GET_TERMINALS_REQUIRING_EMV_PARAM_DOWNLOAD", params)) {
				while(results.next()) {
					Long paramDnldId = results.getValue("paramDnldId", Long.class);
					String terminalCd = results.getValue("terminalCd", String.class);
					String merchantCd = results.getValue("merchantCd", String.class);
					Long systemTraceAuditNumber = results.getValue("systemTraceAuditNumber", Long.class);

					MessageChain messageChain = EMVParameterDownloadJob.setupEMVParameterMessageChain(paramDnldId, terminalCd, merchantCd, systemTraceAuditNumber);

					Map<String, Object> updateParams = new HashMap<>();
					updateParams.put("paramDnldId", paramDnldId);
					updateParams.put("requestedTimestamp", new Date());
					DataLayerMgr.executeCall("UPDATE_EMV_PARAMETER_DOWNLOAD_REQUESTED_TS", updateParams, true);

					MessageChainService.publish(messageChain, getPublisher(), getEncryptionInfoMapping());
				}
			}
		} catch(ServiceException | SQLException | DataLayerException | ConvertException e) {
			log.warn("Could not run emv parameter job", e);
		}
	}


	public Publisher<ByteInput> getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}

	public EncryptionInfoMapping getEncryptionInfoMapping() {
		return encryptionInfoMapping;
	}

	public void setEncryptionInfoMapping(EncryptionInfoMapping encryptionInfoMapping) {
		this.encryptionInfoMapping = encryptionInfoMapping;
	}


	public static MessageChain setupEMVParameterMessageChain(Long paramDnldId, String terminalCd, String merchantCd, Long systemTraceAuditNumber) {
		String authorityQueueKey = "usat.authority.tandem";
		String replyQueueKey = "usat.posm.emvparameter.download";
		
		MessageChain messageChain = new MessageChainV11();
		MessageChainStep requestParameterStep = messageChain.addStep(authorityQueueKey);
		
		requestParameterStep.setAttribute(AuthorityAttrEnum.ATTR_RESPONSE_TIME_OUT, 0);
		requestParameterStep.setAttribute(AuthorityAttrEnum.ATTR_ACTION_TYPE, AuthorityAction.EMV_PARAMETER_DOWNLOAD);
	
		requestParameterStep.setAttribute(AuthorityAttrEnum.ATTR_CURRENCY_CD, CurrencyCd.USD.getValue());
	
		requestParameterStep.setAttribute(AuthorityAttrEnum.ATTR_EMV_PARM_DNDL_ID, paramDnldId);
		requestParameterStep.setAttribute(AuthorityAttrEnum.ATTR_TERMINAL_CD, terminalCd);
		requestParameterStep.setAttribute(AuthorityAttrEnum.ATTR_MERCHANT_CD, merchantCd);
		requestParameterStep.setAttribute(AuthorityAttrEnum.ATTR_SYSTEM_TRACE_AUDIT_NUM, systemTraceAuditNumber);
		
		MessageChainStep replyStep = messageChain.addStep(replyQueueKey);
		
		replyStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_EMV_PARM_DNDL_ID, requestParameterStep, AuthorityAttrEnum.ATTR_EMV_PARM_DNDL_ID);
		replyStep.setReferenceAttribute(CommonAttrEnum.ATTR_RESOURCE, requestParameterStep, CommonAttrEnum.ATTR_RESOURCE);
		
		requestParameterStep.setNextSteps(0, replyStep);
		return messageChain;
	}
}
