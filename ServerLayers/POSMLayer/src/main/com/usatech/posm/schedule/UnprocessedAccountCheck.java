package com.usatech.posm.schedule;

import java.io.IOException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.usatech.layers.common.Cryption;
import com.usatech.layers.common.HashDoesNotMatchException;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.util.KeyMgrAccountUpdater;

import simple.app.Publisher;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.Call.BatchUpdate;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.ByteInput;
import simple.io.Log;
import simple.io.resource.ResourceFolder;
import simple.lang.Holder;
import simple.lang.InvalidValueException;
import simple.results.BeanException;
import simple.results.Results;
import simple.security.SecureHash;
import simple.security.crypt.EncryptionInfoMapping;
import simple.lang.sun.misc.CRC16;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;

@DisallowConcurrentExecution
public class UnprocessedAccountCheck implements Job {
	private static final Log log = Log.getLog();
	protected static final char[] MASK_CHARS = "*@".toCharArray();
	protected static final String USAT_IIN = "639621";
	private static final BigInteger bi97 = BigInteger.valueOf(97);
	private static final BigInteger bi98 = BigInteger.valueOf(98);
	private static final BigInteger bi100 = BigInteger.valueOf(100);
	protected ResourceFolder resourceFolder;
	protected String massUpdateAccountQueueKey;
	protected final Cryption keyMgrCryption = new Cryption();
	protected EncryptionInfoMapping encryptionInfoMapping;
	protected int maxBatchSize = 1000;
	protected Publisher<ByteInput> publisher;
	protected boolean truncateCardNumbers = false;
	protected short unprocessedInstanceNum = -1;

	public void execute(JobExecutionContext context) throws JobExecutionException {
		final Set<Long> processedGlobalAccountIds = new HashSet<Long>();
		int batchSize = getMaxBatchSize();
		processAll(batchSize, processedGlobalAccountIds, getUnprocessedInstanceNum());
	}

	protected void processAll(int batchSize, Set<Long> processedGlobalAccountIds, short instance) {
		int r;
		do
			try {
				r = process(batchSize, processedGlobalAccountIds, instance);
			} catch(SQLException e) {
				log.error("While Processing Accounts", e);
				return;
			} catch(IllegalStateException e) {
				log.error("While Processing Accounts", e);
				return;
			} catch(DataLayerException e) {
				log.error("While Processing Accounts", e);
				return;
			} catch(IOException e) {
				log.error("While Processing Accounts", e);
				return;
			} catch(GeneralSecurityException e) {
				log.error("While Processing Accounts", e);
				return;
			} catch(ConvertException e) {
				log.error("While Processing Accounts", e);
				return;
			} catch(ServiceException e) {
				log.error("While Processing Accounts", e);
				return;
			} catch(RuntimeException e) {
				log.error("While Processing Accounts", e);
				return;
			} catch(Error e) {
				log.error("While Processing Accounts", e);
				return;
			}
		while(r >= batchSize);
	}

	protected int process(int batchSize, Set<Long> processedGlobalAccountIds, short instance) throws SQLException, DataLayerException, IllegalStateException, IOException, GeneralSecurityException, ConvertException, ServiceException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("maxRows", batchSize);
		params.put("instance", instance);
		Results results = DataLayerMgr.executeQuery("GET_UNPROCESSED_ACCOUNTS", params);
		try {
			if(!results.isGroupEnding(0)) {
				BatchUpdate update1 = DataLayerMgr.createBatchUpdate("SET_ACCOUNT_PROCESSED", false, 0.0f);
				BatchUpdate update2 = DataLayerMgr.createBatchUpdate("MASK_ACCOUNT_CD", false, 0.0f);
				BatchUpdate update3 = DataLayerMgr.createBatchUpdate("SET_ACCOUNT_FAILED", false, 0.0f);
				BatchUpdate update4 = DataLayerMgr.createBatchUpdate("SET_ACCOUNT_DID_NOT_MATCH", false, 0.0f);
				KeyMgrAccountUpdater updater = new KeyMgrAccountUpdater(getPublisher(), resourceFolder, massUpdateAccountQueueKey, keyMgrCryption, encryptionInfoMapping);
				Holder<byte[]> accountCdHashHolder = new Holder<byte[]>();
				int hashIterations = instance == -3 ? 1000 : 0;
				while(results.next()) {
					long globalAccountId = results.getValue("globalAccountId", Long.class);
					String rawAccountCd = results.getValue("consumerAcctCd", String.class);
					boolean masked = StringUtils.indexOf(rawAccountCd, MASK_CHARS) >= 0;
					if(processedGlobalAccountIds.add(globalAccountId)) {
						Integer fmtId = results.getValue("consumerAcctFmtId", Integer.class);
						byte[] accountCdHash = results.getValue("consumerAcctCdHash", byte[].class);
						String trackData;
						Integer trackCrc;
						if(accountCdHash != null) {
							if(fmtId != null) {
								String merchantCd = results.getValue("merchantCd", String.class);
								String expDate = results.getValue("expDate", String.class);
								String extra = results.getValue("trackExtra", String.class);
								byte[] trackHash = results.getValue("consumerAcctCdRawHash", byte[].class);
								byte[] trackSalt = results.getValue("consumerAcctCdRawSalt", byte[].class);
								String trackAlg = results.getValue("consumerAcctCdRawAlg", String.class);

								try {
									trackData = calculateTrack(rawAccountCd, accountCdHash, fmtId, merchantCd, expDate, extra, trackHash, trackSalt, trackAlg, hashIterations, accountCdHashHolder);
								} catch(InvalidValueException e) {
									log.warn("Could not calculate the track data for globalAccountId " + globalAccountId, e);
									update3.addBatch(results);
									continue;
								} catch(HashDoesNotMatchException e) {
									log.warn("Could not calculate the track data for globalAccountId " + globalAccountId, e);
									(hashIterations == 0 ? update4 : update3).addBatch(results);
									continue;
								}
								if(!StringUtils.isBlank(trackAlg) && trackHash != null && trackHash.length > 0) {
									CRC16 crc = new CRC16();
									for(byte b : trackData.getBytes())
										crc.update(b);
									trackCrc = crc.value;
								} else
									trackCrc = null;
								if(hashIterations > 0)
									accountCdHash = accountCdHashHolder.getValue();
							} else if(!masked) {
								trackData = rawAccountCd;
								String accountCd = MessageResponseUtils.getCardNumber(trackData);
								if(trackData.equals(accountCd)) { // its the same so assume that card has more to it but we only have the card number
									trackCrc = null;
								} else {
									CRC16 crc = new CRC16();
									for(byte b : trackData.getBytes())
										crc.update(b);
									trackCrc = crc.value;
								}
								byte[] guessHash = SecureHash.getUnsaltedHash(accountCd.getBytes());
								if(!Arrays.equals(accountCdHash, guessHash)) {
									log.warn("Consumer Acct Cd Hash does NOT match for globalAccountId " + globalAccountId);
									update3.addBatch(results);
									continue;
								}
							} else {
								trackCrc = null;
							}
						} else if(!masked) {
							// Handle fmt_id is not null but hash is null
							if(fmtId != null && !rawAccountCd.startsWith(USAT_IIN)) {
								String merchantCd = results.getValue("merchantCd", String.class);
								if(merchantCd != null && !rawAccountCd.startsWith(merchantCd)) {
									log.warn("Account Number does not start with merchant cd for globalAccountId " + globalAccountId);
									update3.addBatch(results);
									continue;
								}
								// String expDate = results.getValue("expDate", String.class);
								// String extra = results.getValue("trackExtra", String.class);
								switch(fmtId) {
									case 0:
									case 2:
										byte[] cardArray = new byte[rawAccountCd.length() + 8];
										getBytesFromString(USAT_IIN, 0, 6, cardArray, 0);
										cardArray[6] = (byte) ('0' + fmtId);
										getBytesFromString(rawAccountCd, 0, rawAccountCd.length(), cardArray, 7);
										setCheckDigit(cardArray);
										accountCdHash = SecureHash.getUnsaltedHash(cardArray);
										trackCrc = null;
										break;
									case 1:
										// GET most probable process code
										Map<String, Object> subparams = new HashMap<String, Object>();
										subparams.put("consumerAcctId", results.getValue("consumerAcctId"));
										try {
											DataLayerMgr.selectInto("GET_ACCOUNT_PROCESS_CODE", subparams);
										} catch(BeanException e) {
											log.warn("Could not calculate the track data for globalAccountId " + globalAccountId, e);
											update3.addBatch(results);
											continue;
										}
										int processCode = ConvertUtils.getIntSafely(subparams.get("processCode"), 5);
										cardArray = new byte[rawAccountCd.length() + 9];
										getBytesFromString(USAT_IIN, 0, 6, cardArray, 0);
										cardArray[6] = (byte) ('0' + fmtId);
										getBytesFromString(rawAccountCd, 0, rawAccountCd.length(), cardArray, 8);
										cardArray[7] = (byte) ('0' + processCode);
										setCheckDigit(cardArray);
										accountCdHash = SecureHash.getUnsaltedHash(cardArray);
										trackCrc = null;
										break;
									default:
										log.warn("Could not calculate the track data for globalAccountId " + globalAccountId + " because: Unrecognized format id " + fmtId);
										update3.addBatch(results);
										continue;
								}
							} else {
								trackData = rawAccountCd;
								String accountCd = MessageResponseUtils.getCardNumber(trackData);
								trackCrc = null; // we don't know the full track as it may only be a portion
								accountCdHash = SecureHash.getUnsaltedHash(accountCd.getBytes());
							}
						} else {
							log.warn("No Hash and account code is masked for globalAccountId " + globalAccountId);
							update3.addBatch(results);
							continue;
						}
						updater.addCard(accountCdHash, globalAccountId, trackCrc, (short) 0);
					}
					update1.addBatch(results);
					if(!masked && isTruncateCardNumbers())
						update2.addBatch(results);
				}
				Connection conn = DataLayerMgr.getConnection("OPER");
				try {
					if(update1.getPendingCount() > 0)
						update1.executeBatch(conn, Integer.MAX_VALUE);
					if(update2.getPendingCount() > 0)
						update2.executeBatch(conn, Integer.MAX_VALUE);
					if(update3.getPendingCount() > 0)
						update3.executeBatch(conn, Integer.MAX_VALUE);
					if(update4.getPendingCount() > 0)
						update4.executeBatch(conn, Integer.MAX_VALUE);
					updater.send();
					conn.commit();
				} finally {
					conn.close();
				}
			}
			return results.getRowCount();
		} finally {
			results.close();
		}
	}

	protected String calculateTrack(String cardNumber, byte[] accountCdHash, int fmtId, String merchantCd, String expDate, String extra, byte[] trackHash, byte[] trackSalt, String trackAlg, int hashIterations, Holder<byte[]> hash0Holder) throws InvalidValueException, NoSuchAlgorithmException, HashDoesNotMatchException {
		if(!cardNumber.startsWith(USAT_IIN)) {
			if(accountCdHash != null && cardNumber.length() >= 9 && cardNumber.indexOf('*') < 0) {
				byte[] cardArray;
				switch(fmtId) {
					case 0:
					case 2:
						cardArray = new byte[cardNumber.length() + 8];
						getBytesFromString(USAT_IIN, 0, 6, cardArray, 0);
						cardArray[6] = (byte) ('0' + fmtId);
						getBytesFromString(cardNumber, 0, cardNumber.length(), cardArray, 7);
						setCheckDigit(cardArray);
						byte[] guessHash = SecureHash.getUnsaltedHash(cardArray);
						byte[] guessHashMatch;
						if(hashIterations > 0)
							guessHashMatch = SecureHash.getHash(guessHash, null, SecureHash.HASH_ALGORITHM, hashIterations - 1);
						else
							guessHashMatch = guessHash;
						if(Arrays.equals(accountCdHash, guessHashMatch)) {
							if(log.isDebugEnabled())
								log.debug("Found account cd for " + cardNumber);
							hash0Holder.setValue(guessHash);
							// calc track & crc
							StringBuilder sb = new StringBuilder(40);
							String fullCard = new String(cardArray);
							if(!StringUtils.isBlank(trackAlg) && trackHash != null && trackHash.length > 0) {
								if(testTrack(fullCard, expDate, extra, trackHash, trackSalt, trackAlg, sb)) {
									if(log.isDebugEnabled())
										log.debug("Track Hash matches for " + cardNumber);
									return sb.toString();
								}
								char[] expDateArray = new char[4];
								for(int d = 0; d < 1200; d++) {
									sb.setLength(0);
									int year = 2010 + (d / 12);
									int month = 1 + (d % 12);
									expDateArray[0] = (char) ('0' + ((year % 100) / 10));
									expDateArray[1] = (char) ('0' + (year % 10));
									expDateArray[2] = (char) ('0' + (month / 10));
									expDateArray[3] = (char) ('0' + (month % 10));
									if(testTrack(fullCard, new String(expDateArray), extra, trackHash, trackSalt, trackAlg, sb)) {
										if(log.isDebugEnabled())
											log.debug("Found real exp date: " + new String(expDateArray) + " for " + cardNumber);
										return sb.toString();
									}
								}
								throw new InvalidValueException("Track Hash Does NOT match for " + cardNumber + "; expdate=" + expDate, cardNumber);
							}
							return sb.toString();
						}
						break;
					case 1:
						cardArray = new byte[cardNumber.length() + 9];
						getBytesFromString(USAT_IIN, 0, 6, cardArray, 0);
						cardArray[6] = (byte) ('0' + fmtId);
						getBytesFromString(cardNumber, 0, cardNumber.length(), cardArray, 8);
						for(int pc = 0; pc < 10; pc++) {
							cardArray[7] = (byte) ('0' + pc);
							setCheckDigit(cardArray);
							guessHash = SecureHash.getUnsaltedHash(cardArray);
							if(hashIterations > 0)
								guessHashMatch = SecureHash.getHash(guessHash, null, SecureHash.HASH_ALGORITHM, hashIterations - 1);
							else
								guessHashMatch = guessHash;
							if(Arrays.equals(accountCdHash, guessHashMatch)) {
								if(log.isDebugEnabled())
									log.debug("Found account cd for " + cardNumber);
								hash0Holder.setValue(guessHash);
								// calc track & crc
								StringBuilder sb = new StringBuilder(40);
								String fullCard = new String(cardArray);
								if(!StringUtils.isBlank(trackAlg) && trackHash != null && trackHash.length > 0) {
									if(testTrack(fullCard, expDate, extra, trackHash, trackSalt, trackAlg, sb)) {
										if(log.isDebugEnabled())
											log.debug("Track Hash matches for " + cardNumber);
										return sb.toString();
									}
									char[] expDateArray = new char[4];
									for(int d = 0; d < 1200; d++) {
										sb.setLength(0);
										int year = 2010 + (d / 12);
										int month = 1 + (d % 12);
										expDateArray[0] = (char) ('0' + ((year % 100) / 10));
										expDateArray[1] = (char) ('0' + (year % 10));
										expDateArray[2] = (char) ('0' + (month / 10));
										expDateArray[3] = (char) ('0' + (month % 10));
										if(testTrack(fullCard, new String(expDateArray), extra, trackHash, trackSalt, trackAlg, sb)) {
											if(log.isDebugEnabled())
												log.debug("Found real exp date: " + new String(expDateArray) + " for " + cardNumber);
											return sb.toString();
										}
									}
									throw new InvalidValueException("Track Hash Does NOT match for " + cardNumber + "; expdate=" + expDate, cardNumber);
								}
								return sb.toString();
							}
						}
						break;
					default:
						throw new InvalidValueException("Unrecognized format id " + fmtId, cardNumber);
				}
				throw new HashDoesNotMatchException("Could NOT find account cd from unmasked account innards for " + cardNumber);
			}
			throw new InvalidValueException("Unrecognized card prefix '" + cardNumber.substring(0, 6), cardNumber);
		}
		byte[] groupIDPadded = StringUtils.pad(String.valueOf(Integer.parseInt(merchantCd.substring(merchantCd.length() - 3))), '0', 3, Justification.RIGHT).getBytes();
		byte[] cardArray = cardNumber.getBytes();
		switch(fmtId) {
			case 0:
			case 2:
				if(cardArray[6] == '*')
					cardArray[6] = (byte) ('0' + fmtId);
				for(int i = 7; i < 10; i++)
					if(cardArray[i] == '*')
						cardArray[i] = groupIDPadded[i - 7];
				int start = 10;
				for(; start < cardArray.length; start++)
					if(cardArray[start] == '*')
						break;
				int end = cardArray.length - 1;
				for(; end >= start; end--)
					if(cardArray[end] == '*')
						break;
				int baseChecksum = addChecksum(cardArray, 0, start) + addChecksum(cardArray, end + 1, cardArray.length);
				if(log.isDebugEnabled())
					log.debug("Must guess " + (end - start + 1) + " digits for " + cardNumber);
				for(int i = start; i <= end; i++)
					cardArray[i] = '0';
				int max = (int) Math.pow(10, (end - start + 1));
				int n;
				for(n = 1; n < max; n++) {
					int checksum = baseChecksum + addChecksum(cardArray, start, end + 1);
					if((checksum % 10) == 0) {
						byte[] guessHash = SecureHash.getUnsaltedHash(cardArray);
						byte[] guessHashMatch;
						if(hashIterations > 0)
							guessHashMatch = SecureHash.getHash(guessHash, null, SecureHash.HASH_ALGORITHM, hashIterations - 1);
						else
							guessHashMatch = guessHash;
						if(Arrays.equals(accountCdHash, guessHashMatch)) {
							if(log.isDebugEnabled())
								log.debug("Found account cd for " + cardNumber);
							hash0Holder.setValue(guessHash);
							// calc track & crc
							StringBuilder sb = new StringBuilder(40);
							String fullCard = new String(cardArray);
							if(!StringUtils.isBlank(trackAlg) && trackHash != null && trackHash.length > 0) {
								if(testTrack(fullCard, expDate, extra, trackHash, trackSalt, trackAlg, sb)) {
									if(log.isDebugEnabled())
										log.debug("Track Hash matches for " + cardNumber);
									return sb.toString();
								}
								char[] expDateArray = new char[4];
								for(int d = 0; d < 1200; d++) {
									sb.setLength(0);
									int year = 2010 + (d / 12);
									int month = 1 + (d % 12);
									expDateArray[0] = (char) ('0' + ((year % 100) / 10));
									expDateArray[1] = (char) ('0' + (year % 10));
									expDateArray[2] = (char) ('0' + (month / 10));
									expDateArray[3] = (char) ('0' + (month % 10));
									if(testTrack(fullCard, new String(expDateArray), extra, trackHash, trackSalt, trackAlg, sb)) {
										if(log.isDebugEnabled())
											log.debug("Found real exp date: " + new String(expDateArray) + " for " + cardNumber);
										return sb.toString();
									}
								}
								throw new InvalidValueException("Track Hash Does NOT match for " + cardNumber + "; expdate=" + expDate, cardNumber);
							}
							return sb.toString();
						}
					}
					String s = StringUtils.pad(Integer.toString(n), '0', end - start + 1, Justification.RIGHT);
					getBytesFromString(s, 0, s.length(), cardArray, start);
				}
				throw new HashDoesNotMatchException("Could NOT find account cd after " + n + " guesses for " + cardNumber);
			case 1:
				if(cardArray[6] == '*')
					cardArray[6] = (byte) ('0' + fmtId);
				// cardArray[7] = process code and is unknown
				for(int i = 8; i < 11; i++)
					if(cardArray[i] == '*')
						cardArray[i] = groupIDPadded[i - 8];
				start = 11;
				for(; start < cardArray.length; start++)
					if(cardArray[start] == '*')
						break;
				end = cardArray.length - 1;
				for(; end >= start; end--)
					if(cardArray[end] == '*')
						break;
				baseChecksum = addChecksum(cardArray, 0, 7) + addChecksum(cardArray, 8, start) + addChecksum(cardArray, end + 1, cardArray.length);
				if(log.isDebugEnabled())
					log.debug("Must guess " + (end - start + 2) + " digits for " + cardNumber);
				cardArray[7] = '0';
				for(int i = start; i <= end; i++)
					cardArray[i] = '0';
				max = (int) Math.pow(10, (end - start + 2));
				for(n = 1; n < max; n++) {
					int checksum = baseChecksum + addChecksum(cardArray, start, end + 1) + addChecksum(cardArray, 7, 8);
					if((checksum % 10) == 0) {
						byte[] guessHash = SecureHash.getUnsaltedHash(cardArray);
						byte[] guessHashMatch;
						if(hashIterations > 0)
							guessHashMatch = SecureHash.getHash(guessHash, null, SecureHash.HASH_ALGORITHM, hashIterations - 1);
						else
							guessHashMatch = guessHash;
						if(Arrays.equals(accountCdHash, guessHashMatch)) {
							if(log.isDebugEnabled())
								log.debug("Found account cd for " + cardNumber);
							hash0Holder.setValue(guessHash);
							// calc track
							StringBuilder sb = new StringBuilder(40);
							String fullCard = new String(cardArray);
							if(!StringUtils.isBlank(trackAlg) && trackHash != null && trackHash.length > 0) {
								if(testTrack(fullCard, expDate, extra, trackHash, trackSalt, trackAlg, sb)) {
									if(log.isDebugEnabled())
										log.debug("Track Hash matches for " + cardNumber);
									return sb.toString();
								}
								char[] expDateArray = new char[4];
								for(int d = 0; d < 1200; d++) {
									sb.setLength(0);
									int year = 2010 + (d / 12);
									int month = 1 + (d % 12);
									expDateArray[0] = (char) ('0' + ((year % 100) / 10));
									expDateArray[1] = (char) ('0' + (year % 10));
									expDateArray[2] = (char) ('0' + (month / 10));
									expDateArray[3] = (char) ('0' + (month % 10));
									if(testTrack(fullCard, new String(expDateArray), extra, trackHash, trackSalt, trackAlg, sb)) {
										if(log.isDebugEnabled())
											log.debug("Found real exp date: " + new String(expDateArray) + " for " + cardNumber);
										return sb.toString();
									}
								}
								throw new InvalidValueException("Track Hash Does NOT match for " + cardNumber + "; expdate=" + expDate, cardNumber);
							}
							return sb.toString();
						}
					}
					String s = StringUtils.pad(Integer.toString(n), '0', end - start + 2, Justification.RIGHT);
					getBytesFromString(s, 0, 1, cardArray, 7);
					getBytesFromString(s, 1, s.length(), cardArray, start);
				}
				throw new HashDoesNotMatchException("Could NOT find account cd after " + n + " guesses for " + cardNumber);
			default:
				throw new InvalidValueException("Unrecognized format id " + fmtId, cardNumber);
		}
	}

	@SuppressWarnings("deprecation")
	protected void getBytesFromString(String s, int srcBegin, int srcEnd, byte[] dst, int dstBegin) {
		s.getBytes(srcBegin, srcEnd, dst, dstBegin);
	}

	protected boolean testTrack(String cardNumber, String expDate, String extra, byte[] trackHash, byte[] trackSalt, String trackAlg, StringBuilder sb) throws NoSuchAlgorithmException {
		sb.append(cardNumber).append('=').append(expDate);
		if(extra != null)
			sb.append(extra);
		String track = sb.toString();
		byte[] checkHash = SecureHash.getHash(track.getBytes(), trackSalt, trackAlg);
		if(Arrays.equals(trackHash, checkHash))
			return true;
		int mod97 = mod97(sb.toString().replace('=', '0'));
		sb.append((mod97 / 10) % 10).append(mod97 % 10);
		track = sb.toString();
		checkHash = SecureHash.getHash(track.getBytes(), trackSalt, trackAlg);
		if(!Arrays.equals(trackHash, checkHash))
			return false;

		return true;
	}

	protected String buildTrack(String cardNumber, String expDate, String extra, StringBuilder sb) {
		sb.append(cardNumber).append('=').append(expDate);
		if(extra != null)
			sb.append(extra);
		int mod97 = mod97(sb.toString().replace('=', '0'));
		sb.append((mod97 / 10) % 10).append(mod97 % 10);
		return sb.toString();
	}

	public int calcDigit(String accountCd, int index) {
		int[] a = new int[accountCd.length()];
		for(int i = 0; i < a.length; i++)
			a[i] = Character.getNumericValue(accountCd.charAt(i));

		int len = a.length;
		int checksum = 0;
		int tmp;

		for(int i = (len % 2); i < len; i += 2) {
			if(i == index)
				continue;
			tmp = a[i] * 2;
			while(tmp > 9)
				tmp = (tmp % 10) + (tmp / 10);
			checksum += tmp;
		}
		for(int i = ((len + 1) % 2); i < len; i += 2) {
			if(i == index)
				continue;
			tmp = a[i];
			while(tmp > 9)
				tmp = (tmp % 10) + (tmp / 10);
			checksum += tmp;
		}

		checksum = (10 - (checksum % 10)) % 10;
		if(((len - index) % 2) == 0)
			return (checksum % 2) == 0 ? checksum / 2 : (9 + checksum) / 2;
		return checksum;
	}

	public void setCheckDigit(byte[] cardArray) {
		int checksum = addChecksum(cardArray, 0, cardArray.length - 1);
		checksum = (10 - (checksum % 10)) % 10;
		cardArray[cardArray.length - 1] = (byte) ('0' + checksum);
	}

	public int addChecksum(byte[] cardArray, int start, int end) {
		int len = cardArray.length;
		int checksum = 0;
		int tmp;

		for(int i = start + ((len + start) % 2); i < end; i += 2) {
			tmp = Character.getNumericValue(cardArray[i]) * 2;
			while(tmp > 9)
				tmp = (tmp % 10) + (tmp / 10);
			checksum += tmp;
		}
		for(int i = start + ((len + start + 1) % 2); i < end; i += 2) {
			tmp = Character.getNumericValue(cardArray[i]);
			checksum += tmp;
		}

		return checksum;
	}

	protected int mod97(String s) {
		BigInteger i = new BigInteger(s);
		return bi98.subtract(i.multiply(bi100).mod(bi97)).mod(bi97).intValue();
	}

	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}

	public String getMassUpdateAccountQueueKey() {
		return massUpdateAccountQueueKey;
	}

	public void setMassUpdateAccountQueueKey(String massUpdateAccountQueueKey) {
		this.massUpdateAccountQueueKey = massUpdateAccountQueueKey;
	}

	public EncryptionInfoMapping getEncryptionInfoMapping() {
		return encryptionInfoMapping;
	}

	public void setEncryptionInfoMapping(EncryptionInfoMapping encryptionInfoMapping) {
		this.encryptionInfoMapping = encryptionInfoMapping;
	}

	public Cryption getKeyMgrCryption() {
		return keyMgrCryption;
	}

	public int getMaxBatchSize() {
		return maxBatchSize;
	}

	public void setMaxBatchSize(int maxBatchSize) {
		this.maxBatchSize = maxBatchSize;
	}

	public Publisher<ByteInput> getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}

	public boolean isTruncateCardNumbers() {
		return truncateCardNumbers;
	}

	public void setTruncateCardNumbers(boolean truncateCardNumbers) {
		this.truncateCardNumbers = truncateCardNumbers;
	}

	public short getUnprocessedInstanceNum() {
		return unprocessedInstanceNum;
	}

	public void setUnprocessedInstanceNum(short unprocessedInstanceNum) {
		this.unprocessedInstanceNum = unprocessedInstanceNum;
	}
}
