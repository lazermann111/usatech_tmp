package com.usatech.posm.schedule.dfr;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.lang.management.ManagementFactory;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.InflaterInputStream;

import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.posm.utils.POSMUtils;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DbUtils;
import simple.io.IOUtils;
import simple.io.Log;
import simple.text.StringUtils;

public abstract class AbstractDFRParser implements MessageChainTask, DFRParsingMXBean {
	private static final Log log = Log.getLog();
	protected long processingThresholdMillis = 60 * 60 * 1000L; // 1 hour
	protected int maxErrorRetries = 20;
	protected boolean jmxEnabled = false;
	protected ObjectName jmxObjectName;
	protected boolean tmpFileUsed;

	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		boolean success = true;
		try {
			Long[] fileIds = ConvertUtils.convertToArrayNoParse(Long.class, taskInfo.getStep().getAttribute(CommonAttrEnum.ATTR_FILE_ID, Object.class, true));
			for(Long fileId : fileIds)
				if(!parseFile(fileId))
					success = false;
		} catch(SQLException | DataLayerException | IOException | ConvertException | AttributeConversionException e) {
			throw new ServiceException(e);
		}
		return success ? 0 : 1;
	}

	public boolean parseFile(long fileCacheId) throws SQLException, DataLayerException, ServiceException, IOException, ConvertException {
		int errorRetries = 0;
		Map<String, Object> params = new HashMap<>();
		params.put("fileCacheId", fileCacheId);
		params.put("processingThreshold", getProcessingThresholdMillis());
		params.put("globalTokenCd", POSMUtils.generateGlobalToken());
		while (true) {
			// get unprocessed
			boolean okay = false;
			Connection conn = DataLayerMgr.getConnection("OPER");
			try {
				DataLayerMgr.executeCall(conn, "MARK_FILE_CACHE_PROCESSING", params);
				conn.commit();
				int n = ConvertUtils.getInt(params.get("rows"));
				String fileName = ConvertUtils.getString(params.get("fileName"), true);
				Blob fileContent;
				switch(n) {
					case 0:
						log.warn("File '" + fileName + "' is being processed by another thread - skipping");
						return false;
					case 1:
						fileContent = ConvertUtils.convertRequired(Blob.class, params.get("fileContent"));
						log.info("Parsing file '" + fileName + "' of " + fileContent.length() + " bytes");
						break;
					default:
						log.error("Update for file '" + fileName + "' returned wrong number of affected rows (" + n + ") - skipping");
						return false;
				}
				int fileType = ConvertUtils.getInt(params.get("fileType"));
				long startLineNumber = ConvertUtils.getLong(params.get("lastProcessedLine"), 0L);
				File tmpFile = null;
				InputStream in = fileContent.getBinaryStream();
				long rows;
				try {
					try {
						if(isTmpFileUsed()) {
							// write to tmp file then read from tmp file
							tmpFile = File.createTempFile(fileName, null);
							try (OutputStream out = new FileOutputStream(tmpFile)) {
								IOUtils.copy(in, out);
							}
							in.close();
							in = new FileInputStream(tmpFile);
						}
	
						switch(fileType) {
							case 2:
								in = new InflaterInputStream(in);
						}
						try (BufferedReader reader = new BufferedReader(new InputStreamReader(in))) {
							rows = parseFileContent(conn, fileCacheId, fileName, startLineNumber, reader);
						}
					} finally {
						try {
							in.close();
						} catch(IOException e) {
							// ignore
						}
					}
					n = DataLayerMgr.executeUpdate(conn, "MARK_FILE_CACHE_PROCESSED", params);
					okay = true;
					conn.commit();
				} finally {
					if(tmpFile != null)
						tmpFile.delete();
				}
				switch(n) {
					case 0:
						log.warn("File '" + fileName + "' was processed by another thread");
						return false;
					case 1:
						log.info("Finished processing file '" + fileName + "' with " + rows + " lines");
						return true;
					default:
						log.error("Update to complete for file '" + fileName + "' returned wrong number of affected rows (" + n + ")");
						return false;
				}
			} catch(SQLException | DataLayerException | ServiceException | IOException | ConvertException e) {
				DbUtils.rollbackSafely(conn);				
				log.error("Error processing dfr file cache id " + fileCacheId, e);
				if (errorRetries < maxErrorRetries && e instanceof SQLException) {
					switch (((SQLException) e).getErrorCode()) {
						case 1555:
							//ORA-01555: snapshot too old
							errorRetries++;
							DbUtils.closeSafely(conn);
							continue;
					}
				}
				params.put("fileCacheId", fileCacheId);
				params.put("exception", StringUtils.exceptionToString(e));
				DataLayerMgr.executeUpdate(conn, "MARK_FILE_CACHE_ERROR", params);
				conn.commit();
				throw e;
			} finally {
				if(!okay)
					DbUtils.rollbackSafely(conn);
				DbUtils.closeSafely(conn);
			}
		}
	}
	
	protected abstract long parseFileContent(Connection conn, long fileCacheId, String fileName, long startLineNumber, Reader fileContent) throws DataLayerException, IOException, ServiceException;

	public long getProcessingThresholdMillis() {
		return processingThresholdMillis;
	}

	public void setProcessingThresholdMillis(long processingThresholdMillis) {
		this.processingThresholdMillis = processingThresholdMillis;
	}

	public int getMaxErrorRetries() {
		return maxErrorRetries;
	}

	public void setMaxErrorRetries(int maxErrorRetries) {
		this.maxErrorRetries = maxErrorRetries;
	}

	public boolean isJmxEnabled() {
		return jmxEnabled;
	}

	public void setJmxEnabled(boolean jmxEnabled) throws InstanceAlreadyExistsException, MBeanRegistrationException, NotCompliantMBeanException, InstanceNotFoundException {
		if(this.jmxEnabled == jmxEnabled)
			return;
		if(jmxEnabled) {
			MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
			try {
				jmxObjectName = new ObjectName("AppJobs:Name=" + getClass().getSimpleName());
				mbs.registerMBean(this, jmxObjectName);
			} catch(MalformedObjectNameException e) {
				log.error(e);
				jmxObjectName = null;
			} catch(NullPointerException e) {
				log.error(e);
				jmxObjectName = null;
			}
		} else if(jmxObjectName != null) {
			MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
			mbs.unregisterMBean(jmxObjectName);
			jmxObjectName = null;
		}
		this.jmxEnabled = jmxEnabled;
	}

	public boolean isTmpFileUsed() {
		return tmpFileUsed;
	}

	public void setTmpFileUsed(boolean tmpFileUsed) {
		this.tmpFileUsed = tmpFileUsed;
	}

}
