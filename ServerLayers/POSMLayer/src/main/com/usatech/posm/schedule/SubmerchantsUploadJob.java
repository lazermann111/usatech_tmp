package com.usatech.posm.schedule;

import static simple.text.MessageFormat.*;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.results.Results;
import simple.text.*;
import simple.text.FormattedFile.Row;
import simple.text.FormattedFile.Row.Field;
import simple.text.FormattedFile.ValidationException;

import com.usatech.app.*;
import com.usatech.layers.common.QuartzCronScheduledWithPublisherJob;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.posm.utils.POSMUtils;

@DisallowConcurrentExecution
public class SubmerchantsUploadJob extends QuartzCronScheduledWithPublisherJob {
	
	public static final Log log = Log.getLog();
	
	public static final char SPACE = ' ';
	
	public static final long ONE_HOUR = (60 * 60 * 1000);
	public static final long ONE_DAY = (ONE_HOUR * 24);
	public static final long ONE_WEEK = (7 * ONE_DAY);
	
	public static final String RECORD_TERMINATOR = "\n";
	public static final String FIELD_SEPARATOR = "|";

	public static final String HEADER_INDICATOR = "HEADER";
	public static final String DETAIL_INDICATOR = "DETAIL";
	public static final String TRAILER_INDICATOR = "TRAILER";
	
	public static final String LAST_FULL_UPLOAD_KEY = "SUBMERCHANTS_LAST_FULL_UPLOAD_TS";
	public static final String LAST_CHANGES_UPLOAD_KEY = "SUBMERCHANTS_LAST_CHANGES_UPLOAD_TS";
	public static final String SEQUENCE_NUMBER_KEY = "SUBMERCHANTS_LAST_SEQUENCE_NUMBER";

	public static final String HEADER_FILE_NAME_FORMAT = "SM_FILE_{0}_{1}";
	public static final String RESOURCE_KEY_FORMAT = "SUBMERCHANTS_{0}_{1}";
	
	public static final String QUERY_ID = "GET_SUBMERCHANTS";

	public static final ThreadSafeDateFormat shortDateFormat = new ThreadSafeDateFormat(new SimpleDateFormat("yyMMdd"));
	public static final ThreadSafeDateFormat dateFormat = new ThreadSafeDateFormat(new SimpleDateFormat("yyyyMMdd"));
	public static final ThreadSafeDateFormat dateTimeFormat = new ThreadSafeDateFormat(new SimpleDateFormat("yyyyMMddHHmmss"));

	private String sftpUploadQueueKey;
	private String postUploadQueueKey;
	private String resourceDeleteQueueKey;
	private ResourceFolder resourceFolder;
	private Set<String> supportedCountries;
	private int companyId;
	private String companyName;
	private int sid;
	private String sidPassword;
	private int pid;
	private String pidPassword;
	private long uploadFullIntervalMs = (ONE_WEEK - ONE_DAY);
	private long uploadChangesIntervalMs = (ONE_DAY - ONE_HOUR);
	private boolean includeValidationFailures = false;
//	private int customerDeviceLastActivityDaysToInclude;
	private long retryIntervalMs = ((3 * ONE_DAY) - ONE_HOUR);
	private int existingCustomerSendLimit = 0;
	private String defaultMCC;
	private String host;
	private int port;
	private String username;
	private String password;
	private String directory;
	
	private List<FailureRow> failures = new ArrayList<>();
	
	@Override
	public void executePostConfigure(JobExecutionContext context) throws JobExecutionException {
		validateParams();
		
		long lastFullTs = 0;
		long lastChangesTs = 0;

		try {
			lastFullTs = POSMUtils.getAppSetting(LAST_FULL_UPLOAD_KEY, Long.class, 0L);
			lastChangesTs = POSMUtils.getAppSetting(LAST_CHANGES_UPLOAD_KEY, Long.class, 0L);
		} catch (ServiceException e) {
			throw new JobExecutionException(e);
		}
		
		Date startTime = new Date();
		long startTs = startTime.getTime();
		
		long lastFullEt = (startTs - lastFullTs);
		long lastChangesEt = (startTs - lastChangesTs);
		
		boolean doFull = false;
		
		if (lastFullEt > uploadFullIntervalMs) {
			log.info("Last full upload {0,number,0.00} hours ago: doing full upload", ((double)lastFullEt)/((double)ONE_HOUR));
			doFull = true;
		} else if (lastChangesEt > uploadChangesIntervalMs) {
			log.info("Last changes-only upload {0,number,0.00} hours ago: doing changes-only upload", ((double)lastChangesEt)/((double)ONE_HOUR));
			doFull = false;
		} else {
			log.info("Last full upload {0,number,0.00} hours ago, last changes-only upload {1,number,0.00} hours ago: nothing to do", ((double)lastFullEt)/((double)ONE_HOUR), ((double)lastChangesEt)/((double)ONE_HOUR));
			return;
		}
		
		long lastRunTs = lastFullTs > lastChangesTs ? lastFullTs : lastChangesTs; 
		
		double retryDays = ((double)retryIntervalMs)/((double)ONE_DAY);
		
		Results results =  null;
		try {
			results = DataLayerMgr.executeQuery(QUERY_ID, new Object[] {retryDays});
		} catch (Exception e) {
			throw new JobExecutionException(e);
		}
		
		FormattedFile file = new SubmerchantsInputFile();
		List<MatchStatusUpdate> statusUpdates = new ArrayList<>();
		
		int existingCustomerSendCount = 0;
		
		try {
			while(results.next()) {
				long customerId = results.getValue("IDENTIFIER", Long.class);
				
				String country = results.getFormattedValue("COUNTRY");
				if (!supportedCountries.contains(country)) {
					log.debug("NOT sending customer {0} because country is {1}", customerId, country);
					continue;
				}
				
				String matchCodeStr = results.getValue("MATCH_REASON_CD", String.class);
				MatchReasonCode matchCode = null;
				if (matchCodeStr != null) {
					matchCode = MatchReasonCode.getByCode(matchCodeStr);
				}

				String matchStatusCodeStr = results.getValue("MATCH_STATUS_CD", String.class);
				MatchStatusCode matchStatusCode = null;
				if (matchStatusCodeStr != null) {
					matchStatusCode = MatchStatusCode.getByCode(matchStatusCodeStr);
				}
				
//				Date createdDate = results.getValue("CUSTOMER_CREATED_TS", Date.class);
				
				List<Date> dates = new ArrayList<Date>() {
					@Override
					public boolean add(Date e) {
						if (e == null)
							return false;
						return super.add(e);
					}
				};
				
				dates.add(results.getValue("CUSTOMER_CREATED_TS", Date.class));
				dates.add(results.getValue("CUSTOMER_LAST_UPDATED_TS", Date.class));
				dates.add(results.getValue("USER_CREATED_TS", Date.class));
				dates.add(results.getValue("USER_LAST_UPDATED_TS", Date.class));
				dates.add(results.getValue("CUSTOMER_ADDRESS_CREATED_TS", Date.class));
				dates.add(results.getValue("CUSTOMER_ADDRESS_LAST_UPDATED_TS", Date.class));
				
				long lastModifiedTs = 0;
				if (!dates.isEmpty()) {
					Collections.sort(dates, Collections.reverseOrder());
					lastModifiedTs = dates.get(0).getTime();
				}
				
				Date lastMatchNewSentDate = results.getValue("LAST_MATCH_NEW_TS", Date.class);
				Date lastMatchChangedSentDate = results.getValue("LAST_MATCH_CHANGED_TS", Date.class);
				
				long lastMatchNewSentTs = lastMatchNewSentDate == null ? 0 : lastMatchNewSentDate.getTime();
				long lastMatchChangedSentTs = lastMatchChangedSentDate == null ? 0 : lastMatchChangedSentDate.getTime();
				
				long lastMatchSentTs = lastMatchNewSentTs > lastMatchChangedSentTs ? lastMatchNewSentTs : lastMatchChangedSentTs;
				
				Date customerCreateDate = results.getValue("CUSTOMER_CREATED_TS", Date.class);
				
				// the query will filter out customers that do not need to be sent for this run

				MatchReasonCode codeToSend = null;
				boolean isExistingNewSend = false;
				boolean overLimit = false;
				
				if (matchStatusCode != null && matchStatusCode == MatchStatusCode.PENDING) {
					codeToSend = matchCode; // send the pending code
					log.debug("Sending customer {0} with PENDING status {1}", customerId, codeToSend);
				} else if (matchStatusCode != null && matchStatusCode == MatchStatusCode.SENT) {
					codeToSend = matchCode; // this is a retry, send the sent code
					log.debug("Sending customer {0} with SENT(retry) status {1}", customerId, codeToSend);
				} else if (lastMatchNewSentDate == null) {
					if (customerCreateDate == null || customerCreateDate.getTime() < (startTs - (2 * ONE_WEEK))) {
						// this is an existing customer that's never been checked
						if (existingCustomerSendLimit > 0 && existingCustomerSendCount < existingCustomerSendLimit) {
							codeToSend = MatchReasonCode.NEW;
							isExistingNewSend = true;
							log.debug("Sending never checked existing customer {0} ({1}/{2}) PENDING status {3}", customerId, existingCustomerSendCount, existingCustomerSendLimit, codeToSend);
						} else if (existingCustomerSendLimit > 0) {
							overLimit = true;
							log.debug("NOT sending never checked existing customer {0} because over limit {1}", customerId, existingCustomerSendLimit);
						} else {
							codeToSend = MatchReasonCode.NEW;
							isExistingNewSend = true;
							log.debug("Sending never checked existing customer {0} PENDING status {1}", customerId, codeToSend);
						}
					} else {
						codeToSend = MatchReasonCode.NEW; // new customer that's never been sent
						log.debug("Sending NEW customer {0} with PENDING status {1}", customerId, codeToSend);
					}
				} else if (lastModifiedTs > 0 && lastModifiedTs > lastMatchSentTs) {
					codeToSend = MatchReasonCode.CHANGED; // there's been a change
					log.debug("Sending CHANGED customer {0} with PENDING status {1}", customerId, codeToSend);
				}
				
				if (codeToSend == null && !doFull) {
					if (!overLimit)
						log.debug("NOT sending customer {0} because no change", customerId, codeToSend);
					continue; // if there's no specific code and we're doing changes-only, then don't send this row
				}
				
				MatchStatusUpdate statusUpdate = null;
				if (codeToSend != null) {
					long matchStatusId = ConvertUtils.getLongSafely(results.getValue("MATCH_STATUS_ID"), 0);
					statusUpdate = new MatchStatusUpdate(customerId, codeToSend, matchStatusId);
				}
				
				Row row = file.nextData();
				
				row.set("RECORD_INDICATOR", DETAIL_INDICATOR);
				if (codeToSend != null)
					row.set("REASON_FOR_ADDING", codeToSend.getCode());
		
				for(Field field : row) {
					Object o = results.get(field.getKey().toString());
					if (o == null)
						continue;
					
					String value = StringUtils.trim(ConvertUtils.getStringSafely(o));
					
					int maxLen = field.getDefinition().getSize();
					if (value.length() > maxLen) {
						value = value.substring(0, maxLen);
					}
					
					field.setValue(value);
				}
				
				if (codeToSend == MatchReasonCode.ADD) {
					row.set("DATE_OF_TERMINATION", dateFormat.format(new Date(lastModifiedTs)));
					statusUpdate.setViolationCode(results.getValue("MATCH_VIOLATION_CD", String.class));
				}
				
				if (defaultMCC != null && !row.getField("MCC").isPopulated()) {
					row.set("MCC", defaultMCC);
				}
				
				row.set("PHONE_TYPE", "W");
				
				if (!row.getField("PRINCIPAL_1_STATE").isPopulated())
					row.set("PRINCIPAL_1_STATE", row.getField("STATE").getValue());
				
				if (!row.getField("PRINCIPAL_1_COUNTRY").isPopulated())
					row.set("PRINCIPAL_1_COUNTRY", row.getField("COUNTRY").getValue());

				if (!row.getField("PRINCIPAL_1_POSTAL").isPopulated())
					row.set("PRINCIPAL_1_POSTAL", row.getField("POSTAL").getValue());

				boolean rowAdded = false;
		
				try {
					file.commitData();
					rowAdded = true;
				} catch (ValidationException e) {
					log.debug("Customer {0} row failed validation: {1}: {2}", customerId, e.getMessage(), row.toString());
					
					failures.add(new FailureRow(row, e.getMessage()));
					
					if (includeValidationFailures) {
						try {
							file.commitData(true);
							rowAdded = true;
						} catch (ValidationException e2) {
							// this shouldn't happen
							log.debug("Customer {0} row failed validation(2): {1}: {2}", customerId, e.getMessage(), row.toString());
						}
					}
				}
				
				if (rowAdded && statusUpdate != null) {
					statusUpdates.add(statusUpdate);
					if (isExistingNewSend) {
						existingCustomerSendCount++;
					}
				}
			}
		} catch (Exception e) {
			log.error("Failed to load results due to unexpected exception: {0}", e.getMessage(), e);
			throw new JobExecutionException(e);
		} finally {
			if (results != null)
				results.close();
		}
		
		log.info("{0} rows added ({1}including validation failures) {2} rows failed validation", file.getDataRowCount(), includeValidationFailures ? "" : "not ", failures.size());
		
		if (file.getDataRowCount() == 0) {
			log.info("Nothing to upload!");
			return;
		}
		
		file.header()
			.set("PID", format("PID={0}", pid))
			.set("PID_PASSWORD", pidPassword)
			.set("SID", format("SID={0}", pid))
			.set("SID_PASSWORD", sidPassword)
			.set("START", "START")
			.set("DATE", shortDateFormat.format(startTime))
			.set("VERSION", "3.0.0");
		
		try {
			file.commitHeader();
		} catch (ValidationException e) {
			log.error("Header row failed validation", e);
			throw new JobExecutionException(e);
		}
		
		String recordCount = Integer.toString(file.getTotalRowCount()-1); // record count does not include the fixed-width PID/SID header
		
		String fileName = format(HEADER_FILE_NAME_FORMAT, companyId, dateTimeFormat.format(startTime));

		file.header()
			.set("RECORD_INDICATOR", HEADER_INDICATOR)
			.set("FILE_NAME", fileName)
			.set("COMPANY_NAME", companyName)
			.set("RECORD_COUNT", recordCount); 
		
		try {
			file.commitHeader();
		} catch (ValidationException e) {
			log.error("Header row failed validation", e);
			throw new JobExecutionException(e);
		}
		
		file.footer()
			.set("RECORD_INDICATOR", TRAILER_INDICATOR)
			.set("RECORD_COUNT", recordCount);

		try {
			file.commitFooter();
		} catch (ValidationException e) {
			log.error("Footer row failed validation", e);
			throw new JobExecutionException(e);
		}
		
		String resourceName = format(RESOURCE_KEY_FORMAT, doFull ? "FULL" : "CHANGES", startTs);
		Resource resource = null;
		
		boolean ok = false;
		try {
			resource = resourceFolder.getResource(resourceName, ResourceMode.CREATE);
			OutputStreamWriter out = new OutputStreamWriter(resource.getOutputStream());
			file.appendTo(out);
			out.close();
			ok = true;
		} catch (Exception e) {
			log.error("Failed to write resource: {0}", resourceName, e);
			throw new JobExecutionException(e);
		} finally {
			if(!ok && resource != null) {
				resource.delete();
				resource.release();
			}
		}
		
		int sequenceNumber = 1;
		try {
			sequenceNumber = POSMUtils.getAppSetting(SEQUENCE_NUMBER_KEY, Integer.class, 0) + 1;
		} catch (ServiceException e) {
			throw new JobExecutionException(e);
		}
		 
		if (sequenceNumber >= 1000)
			sequenceNumber = 1;
		
		String uploadPath = String.format("%s/t%s.%03d", directory, pid, sequenceNumber);
		String renamePath = String.format("%s/p%s.%03d", directory, pid, sequenceNumber);
		
		MessageChain mc = new MessageChainV11();
		MessageChainStep uploadStep = mc.addStep(sftpUploadQueueKey);
		uploadStep.setAttribute(CommonAttrEnum.ATTR_RESOURCE, resource.getKey());
		uploadStep.setAttribute(CommonAttrEnum.ATTR_FILE_PATH, uploadPath);
		uploadStep.setAttribute(CommonAttrEnum.ATTR_FILE_RENAME_REPLACE, renamePath);
		uploadStep.setAttribute(CommonAttrEnum.ATTR_HOST, host);
		uploadStep.setAttribute(CommonAttrEnum.ATTR_PORT, port);
		uploadStep.setAttribute(CommonAttrEnum.ATTR_USERNAME, username);
		uploadStep.setSecretAttribute(CommonAttrEnum.ATTR_PASSWORD, password);
		
		MessageChainStep postUploadStep = mc.addStep(postUploadQueueKey);
		postUploadStep.setAttribute(CommonAttrEnum.ATTR_RESOURCE, resource.getKey());
		postUploadStep.setAttribute(AuthorityAttrEnum.ATTR_SUBMERCHANTS_UPLOAD_FULL, doFull);
		postUploadStep.setAttribute(AuthorityAttrEnum.ATTR_SUBMERCHANTS_UPLOAD_TS, startTs);
		postUploadStep.setAttribute(AuthorityAttrEnum.ATTR_SUBMERCHANTS_UPLOAD_FILENAME, fileName);
		postUploadStep.setAttribute(AuthorityAttrEnum.ATTR_SUBMERCHANTS_UPLOAD_RECORD_COUNT, file.getDataRowCount());
		
		MessageChainStep deleteFileStep = mc.addStep(resourceDeleteQueueKey);
		deleteFileStep.setAttribute(CommonAttrEnum.ATTR_RESOURCE, resource.getKey());
		deleteFileStep.setJoined(true);
		deleteFileStep.addWaitFor(postUploadStep);
		
		uploadStep.setNextSteps(0, postUploadStep);
		uploadStep.setNextSteps(1, postUploadStep);
		uploadStep.setNextSteps(2, postUploadStep);
		
		postUploadStep.setNextSteps(0, deleteFileStep);
		postUploadStep.setNextSteps(1, deleteFileStep);
		postUploadStep.setNextSteps(2, deleteFileStep);
		
		try {
			// now update/insert match tracking records
			for(MatchStatusUpdate update : statusUpdates) {
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("CUSTOMER_ID", update.getCustomerId());
				params.put("MATCH_REASON_CD", update.getMatchCode().getCode());
				params.put("MATCH_STATUS_CD", MatchStatusCode.PENDING.getCode());
				params.put("MATCH_VIOLATION_CD", update.getViolationCode());
				params.put("PENDING_TS", startTime);
				params.put("SUBMERCHANTS_FILE_NAME", fileName);
				
				if (update.getMatchStatusId() > 0) {
					params.put("MATCH_STATUS_ID", update.getMatchStatusId());
					DataLayerMgr.executeUpdate("UPDATE_MATCH_STATUS", params, true);
				} else {
					DataLayerMgr.executeUpdate("INSERT_MATCH_STATUS", params, true);
				}
			}
		} catch (Exception e) { 
			throw new JobExecutionException("Failed to update MATCH_STATUS", e);
		}
		
		log.info("Uploading {0} submerchants file {1} with {2} rows to {3}:{4}:{5}", (doFull ? "FULL" : "CHANGES-ONLY"), fileName, file.getDataRowCount(), host, port, renamePath);
		
		try {
			MessageChainService.publish(mc, publisher);
		} catch (Exception e) {
			log.fatal("Failed to publish message chain resource: {0}", resource.getKey(), e);
			throw new JobExecutionException(e);
		} finally {
			resource.release();
			try {
				POSMUtils.setAppSetting(SEQUENCE_NUMBER_KEY, sequenceNumber);
			} catch (ServiceException e) {
				throw new JobExecutionException(e);
			}
		}
		
	}
	
	public String getSftpUploadQueueKey() {
		return sftpUploadQueueKey;
	}

	public void setSftpUploadQueueKey(String sftpUploadQueueKey) {
		this.sftpUploadQueueKey = sftpUploadQueueKey;
	}

	public String getPostUploadQueueKey() {
		return postUploadQueueKey;
	}

	public void setPostUploadQueueKey(String postUploadQueueKey) {
		this.postUploadQueueKey = postUploadQueueKey;
	}

	public String getResourceDeleteQueueKey() {
		return resourceDeleteQueueKey;
	}

	public void setResourceDeleteQueueKey(String resourceDeleteQueueKey) {
		this.resourceDeleteQueueKey = resourceDeleteQueueKey;
	}

	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}
	
	public Set<String> getSupportedCountries() {
		return supportedCountries;
	}

	public void setSupportedCountries(Set<String> supportedCountries) {
		this.supportedCountries = supportedCountries;
	}
	
	public void addSupportedCoutry(String supportedCountry) {
		if (supportedCountries == null) 
			supportedCountries = new HashSet<String>();
		supportedCountries.add(supportedCountry);
	}
	
	public long getUploadChangesIntervalMs() {
		return uploadChangesIntervalMs;
	}

	public void setUploadChangesIntervalMs(long uploadChangesIntervalMs) {
		this.uploadChangesIntervalMs = uploadChangesIntervalMs;
	}

	public long getUploadFullIntervalMs() {
		return uploadFullIntervalMs;
	}

	public void setUploadFullIntervalMs(long uploadFullIntervalMs) {
		this.uploadFullIntervalMs = uploadFullIntervalMs;
	}

	public boolean isIncludeValidationFailures() {
		return includeValidationFailures;
	}

	public void setIncludeValidationFailures(boolean includeValidationFailures) {
		this.includeValidationFailures = includeValidationFailures;
	}

//	public int getCustomerDeviceLastActivityDaysToInclude() {
//		return customerDeviceLastActivityDaysToInclude;
//	}
//
//	public void setCustomerDeviceLastActivityDaysToInclude(int customerDeviceLastActivityDaysToInclude) {
//		this.customerDeviceLastActivityDaysToInclude = customerDeviceLastActivityDaysToInclude;
//	}
//	
	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDirectory() {
		return directory;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}

	public long getRetryIntervalMs() {
		return retryIntervalMs;
	}

	public void setRetryIntervalMs(long retryIntervalMs) {
		this.retryIntervalMs = retryIntervalMs;
	}

	public int getExistingCustomerSendLimit() {
		return existingCustomerSendLimit;
	}

	public void setExistingCustomerSendLimit(int existingCustomerSendLimit) {
		this.existingCustomerSendLimit = existingCustomerSendLimit;
	}

	public String getDefaultMCC() {
		return defaultMCC;
	}

	public void setDefaultMCC(String defaultMCC) {
		this.defaultMCC = defaultMCC;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public int getSid() {
		return sid;
	}

	public void setSid(int sid) {
		this.sid = sid;
	}

	public String getSidPassword() {
		return sidPassword;
	}

	public void setSidPassword(String sidPassword) {
		this.sidPassword = sidPassword;
	}

	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public String getPidPassword() {
		return pidPassword;
	}

	public void setPidPassword(String pidPassword) {
		this.pidPassword = pidPassword;
	}

	public void validateParams() throws JobExecutionException {
		if (sftpUploadQueueKey == null)
			throw new JobExecutionException("required parameter missing: sftpUploadQueueKey");
		if (postUploadQueueKey == null)
			throw new JobExecutionException("required parameter missing: postUploadQueueKey");
		if (resourceDeleteQueueKey == null)
			throw new JobExecutionException("required parameter missing: resourceDeleteQueueKey");
		if (resourceFolder == null)
			throw new JobExecutionException("required parameter missing: resourceFolder");
		if (supportedCountries == null || supportedCountries.isEmpty())
			throw new JobExecutionException("required parameter missing: supportedCountries");
		if (companyId <= 0)
			throw new JobExecutionException("required parameter missing: companyId");
		if (companyName == null)
			throw new JobExecutionException("required parameter missing: companyName");
		if (sid <= 0)
			throw new JobExecutionException("required parameter missing: sid");
		if (sidPassword == null)
			throw new JobExecutionException("required parameter missing: sidPassword");
		if (pid <= 0)
			throw new JobExecutionException("required parameter missing: pid");
		if (pidPassword == null)
			throw new JobExecutionException("required parameter missing: pidPassword");
//		if (customerDeviceLastActivityDaysToInclude <= 0)
//			throw new JobExecutionException("required parameter missing: customerDeviceLastActivityDaysToInclude");
		if (directory == null)
			throw new JobExecutionException("required parameter missing: directory");
		if (host == null)
			throw new JobExecutionException("required parameter missing: host");
		if (port <= 0)
			throw new JobExecutionException("required parameter missing: port");
		if (username == null)
			throw new JobExecutionException("required parameter missing: username");
		if (password == null)
			throw new JobExecutionException("required parameter missing: password");
	}

	@SuppressWarnings("serial")
	public class SubmerchantsInputFile extends FormattedFile
	{
		public SubmerchantsInputFile() {
			super(RowFormat.DELIMITED, FIELD_SEPARATOR, RECORD_TERMINATOR);
			
			defineHeader().fixedWidth();
			defineHeader().field("PID").size(11).required();
			defineHeader().field("PID_PASSWORD").size(9).required();
			defineHeader().field("SID").size(11).required();
			defineHeader().field("SID_PASSWORD").size(9).required();
			defineHeader().field("START").size(7);
			defineHeader().field("DATE").size(7);
			defineHeader().field("VERSION").size(6).required();
			defineHeader().field("RESERVED").size(60);
			
			defineNextHeader();
			
			defineHeader().field("RECORD_INDICATOR").size(6).required();
			defineHeader().field("FILE_NAME").size(50).required();
			defineHeader().field("COMPANY_NAME").size(50).required();
			defineHeader().field("RECORD_COUNT").size(20).numeric().required();
			
			defineData().field("RECORD_INDICATOR").size(6).required();
			defineData().field("IDENTIFIER").size(20).required();
			defineData().field("NAME").size(50).required();
			defineData().field("DBA").size(50);;
			defineData().field("TAX_ID").size(50);
			defineData().field("TAX_ID_TYPE").size(1).requiredIfPopulated("TAX_ID");
			defineData().field("MCC").size(4).required();
			defineData().field("STATUS").size(1).required();
			defineData().field("REASON_FOR_ADDING").size(2);
			
			defineData().field("DATE_OF_CONTRACT").size(15).requiredIfEquals("REASON_FOR_ADDING", "AD");
			defineData().field("ADDRESS_1").size(65).required();
			defineData().field("ADDRESS_2").size(50);
			defineData().field("ADDRESS_3").size(50);
			defineData().field("CITY").size(50).required();
			defineData().field("STATE").size(50).requiredIfEquals("COUNTRY", "USA");
			defineData().field("COUNTRY").size(50).required();
			defineData().field("POSTAL").size(20).requiredIfEquals("REASON_FOR_ADDING", "AD");
			defineData().field("PHONE").size(20).requiredIfEquals("REASON_FOR_ADDING", "AD");
			defineData().field("PHONE_TYPE").size(1);
			defineData().field("EMAIL").size(50);
			defineData().field("WEBSITE").size(130);
			
			defineData().field("PRINCIPAL_1_FIRST_NAME").size(50).required();
			defineData().field("PRINCIPAL_1_LAST_NAME").size(50).required();
			defineData().field("PRINCIPAL_1_MIDDLE_INITIAL").size(50);
			defineData().field("PRINCIPAL_1_ADDRESS_1").size(65);
			defineData().field("PRINCIPAL_1_ADDRESS_2").size(50);
			defineData().field("PRINCIPAL_1_ADDRESS_3").size(50);
			defineData().field("PRINCIPAL_1_CITY").size(50);
			defineData().field("PRINCIPAL_1_STATE").size(50).requiredIfEquals("PRINCIPAL_1_COUNTRY", "USA");
			defineData().field("PRINCIPAL_1_COUNTRY").size(50).required();
			defineData().field("PRINCIPAL_1_POSTAL").size(20).required();
			defineData().field("PRINCIPAL_1_DOB").size(15);
			defineData().field("PRINCIPAL_1_TAX_ID").size(50);
			defineData().field("PRINCIPAL_1_TAX_ID_TYPE").size(1);
			defineData().field("PRINCIPAL_1_PHONE").size(20);
			defineData().field("PRINCIPAL_1_PHONE_TYPE").size(1);

			defineData().field("PRINCIPAL_2_FIRST_NAME").size(50);
			defineData().field("PRINCIPAL_2_LAST_NAME").size(50).requiredIfPopulated("PRINCIPAL_2_FIRST_NAME");
			defineData().field("PRINCIPAL_2_MIDDLE_INITIAL").size(50);
			defineData().field("PRINCIPAL_2_ADDRESS_1").size(65);
			defineData().field("PRINCIPAL_2_ADDRESS_2").size(50);
			defineData().field("PRINCIPAL_2_ADDRESS_3").size(50);
			defineData().field("PRINCIPAL_2_CITY").size(50).requiredIfPopulated("PRINCIPAL_2_FIRST_NAME");
			defineData().field("PRINCIPAL_2_STATE").size(50).requiredIfEquals("PRINCIPAL_2_COUNTRY", "USA");
			defineData().field("PRINCIPAL_2_COUNTRY").size(50).requiredIfPopulated("PRINCIPAL_2_FIRST_NAME");
			defineData().field("PRINCIPAL_2_POSTAL").size(20).requiredIfPopulated("PRINCIPAL_2_FIRST_NAME");
			defineData().field("PRINCIPAL_2_DOB").size(15);
			defineData().field("PRINCIPAL_2_TAX_ID").size(50);
			defineData().field("PRINCIPAL_2_TAX_ID_TYPE").size(1);
			defineData().field("PRINCIPAL_2_PHONE").size(20);
			defineData().field("PRINCIPAL_2_PHONE_TYPE").size(1);

			defineData().field("PRINCIPAL_3_FIRST_NAME").size(50);
			defineData().field("PRINCIPAL_3_LAST_NAME").size(50).requiredIfPopulated("PRINCIPAL_3_LAST_NAME");
			defineData().field("PRINCIPAL_3_MIDDLE_INITIAL").size(50);
			defineData().field("PRINCIPAL_3_ADDRESS_1").size(65);
			defineData().field("PRINCIPAL_3_ADDRESS_2").size(50);
			defineData().field("PRINCIPAL_3_ADDRESS_3").size(50);
			defineData().field("PRINCIPAL_3_CITY").size(50).requiredIfPopulated("PRINCIPAL_3_LAST_NAME");
			defineData().field("PRINCIPAL_3_STATE").size(50).requiredIfEquals("PRINCIPAL_3_COUNTRY", "USA");
			defineData().field("PRINCIPAL_3_COUNTRY").size(50).requiredIfPopulated("PRINCIPAL_3_LAST_NAME");
			defineData().field("PRINCIPAL_3_POSTAL").size(20).requiredIfPopulated("PRINCIPAL_3_LAST_NAME");
			defineData().field("PRINCIPAL_3_DOB").size(15);
			defineData().field("PRINCIPAL_3_TAX_ID").size(50);
			defineData().field("PRINCIPAL_3_TAX_ID_TYPE").size(1);
			defineData().field("PRINCIPAL_3_PHONE").size(20);
			defineData().field("PRINCIPAL_3_PHONE_TYPE").size(1);

			defineData().field("PRINCIPAL_4_FIRST_NAME").size(50);
			defineData().field("PRINCIPAL_4_LAST_NAME").size(50).requiredIfPopulated("PRINCIPAL_4_LAST_NAME");
			defineData().field("PRINCIPAL_4_MIDDLE_INITIAL").size(50);
			defineData().field("PRINCIPAL_4_ADDRESS_1").size(65);
			defineData().field("PRINCIPAL_4_ADDRESS_2").size(50);
			defineData().field("PRINCIPAL_4_ADDRESS_3").size(50);
			defineData().field("PRINCIPAL_4_CITY").size(50).requiredIfPopulated("PRINCIPAL_4_LAST_NAME");
			defineData().field("PRINCIPAL_4_STATE").size(50).requiredIfEquals("PRINCIPAL_4_COUNTRY", "USA");
			defineData().field("PRINCIPAL_4_COUNTRY").size(50).requiredIfPopulated("PRINCIPAL_4_LAST_NAME");
			defineData().field("PRINCIPAL_4_POSTAL").size(20).requiredIfPopulated("PRINCIPAL_4_LAST_NAME");
			defineData().field("PRINCIPAL_4_DOB").size(15);
			defineData().field("PRINCIPAL_4_TAX_ID").size(50);
			defineData().field("PRINCIPAL_4_TAX_ID_TYPE").size(1);
			defineData().field("PRINCIPAL_4_PHONE").size(20);
			defineData().field("PRINCIPAL_4_PHONE_TYPE").size(1);

			defineData().field("PRINCIPAL_5_FIRST_NAME").size(50);
			defineData().field("PRINCIPAL_5_LAST_NAME").size(50).requiredIfPopulated("PRINCIPAL_5_LAST_NAME");
			defineData().field("PRINCIPAL_5_MIDDLE_INITIAL").size(50);
			defineData().field("PRINCIPAL_5_ADDRESS_1").size(65);
			defineData().field("PRINCIPAL_5_ADDRESS_2").size(50);
			defineData().field("PRINCIPAL_5_ADDRESS_3").size(50);
			defineData().field("PRINCIPAL_5_CITY").size(50).requiredIfPopulated("PRINCIPAL_5_LAST_NAME");
			defineData().field("PRINCIPAL_5_STATE").size(50).requiredIfEquals("PRINCIPAL_5_COUNTRY", "USA");
			defineData().field("PRINCIPAL_5_COUNTRY").size(50).requiredIfPopulated("PRINCIPAL_5_LAST_NAME");
			defineData().field("PRINCIPAL_5_POSTAL").requiredIfPopulated("PRINCIPAL_5_LAST_NAME");
			defineData().field("PRINCIPAL_5_DOB").size(15);
			defineData().field("PRINCIPAL_5_TAX_ID").size(50);
			defineData().field("PRINCIPAL_5_TAX_ID_TYPE").size(1);
			defineData().field("PRINCIPAL_5_PHONE").size(20);
			defineData().field("PRINCIPAL_5_PHONE_TYPE").size(1);

			defineData().field("MATCH_VIOLATION_CD").size(2).requiredIfEquals("REASON_FOR_ADDING", "AD");
			defineData().field("DATE_OF_TERMINATION").size(15).requiredIfEquals("REASON_FOR_ADDING", "AD");
			defineData().field("CARDHOLDER_ACTIVATED_TERMINAL_INDICATOR").size(1).requiredIfEquals("REASON_FOR_ADDING", "AD");
			
			defineFooter().field("RECORD_INDICATOR").size(6).required();
			defineFooter().field("RECORD_COUNT").size(20).required();
		}
	}
	
	public static enum MatchReasonCode {
		ADD("AD", "Add to MATCH (blacklist)"),
		CHANGED("CH", "Existing merchant, change in owner, send for inquiry"),
		NEW("IN", "New merchant inquiry");
		
		private String code;
		private String desc;
		
		private MatchReasonCode(String code, String desc) {
			this.code = code;
			this.desc = desc;
		}
		
		public String getCode() {
			return code;
		}
		
		public String getDesc() {
			return desc;
		}
		
		public static MatchReasonCode getByCode(String s) {
			for(MatchReasonCode code : values()) {
				if (code.getCode().equalsIgnoreCase(s))
					return code;
			}
			return null;
		}
	}
	
	public static enum MatchStatusCode {
		PENDING("P", "Pending"),
		SENT("S", "Sent"),
		CONFIRMED("C", "Confirmed");
		
		private String code;
		private String desc;
		
		private MatchStatusCode(String code, String desc) {
			this.code = code;
			this.desc = desc;
		}
		
		public String getCode() {
			return code;
		}
		
		public String getDesc() {
			return desc;
		}
		
		public static MatchStatusCode getByCode(String s) {
			for(MatchStatusCode code : values()) {
				if (code.getCode().equalsIgnoreCase(s))
					return code;
			}
			return null;
		}
	}
	
	protected static class FailureRow
	{
		private Row row;
		private String message;
		
		protected FailureRow(Row row, String message) {
			this.row = row;
			this.message = message;
		}
		
		protected Row getRow() {
			return row;
		}
		
		protected String getMesssage() {
			return message;
		}
	}
	
	public static class MatchStatusUpdate implements Serializable
	{
		private static final long serialVersionUID = 1L;
		
		private long customerId;
		private MatchReasonCode matchCode;
		private long matchStatusId;
		private String violationCode;
		
		public MatchStatusUpdate(long customerId, MatchReasonCode matchCode, long matchStatusId) {
			this.customerId = customerId;
			this.matchCode = matchCode;
			this.matchStatusId = matchStatusId;
		}

		public long getCustomerId() {
			return customerId;
		}

		public void setCustomerId(long customerId) {
			this.customerId = customerId;
		}

		public MatchReasonCode getMatchCode() {
			return matchCode;
		}

		public void setMatchCode(MatchReasonCode matchCode) {
			this.matchCode = matchCode;
		}

		public long getMatchStatusId() {
			return matchStatusId;
		}

		public void setMatchStatusId(long matchStatusId) {
			this.matchStatusId = matchStatusId;
		}

		public String getViolationCode() {
			return violationCode;
		}

		public void setViolationCode(String violationCode) {
			this.violationCode = violationCode;
		}
	}
}
