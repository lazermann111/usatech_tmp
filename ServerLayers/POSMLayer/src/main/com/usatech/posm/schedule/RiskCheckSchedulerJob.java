package com.usatech.posm.schedule;

import static simple.text.MessageFormat.format;

import java.time.Duration;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;

import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.QuartzCronScheduledWithPublisherJob;

@DisallowConcurrentExecution
public class RiskCheckSchedulerJob extends QuartzCronScheduledWithPublisherJob {
	
	public static final Log log = Log.getLog();
	
	private static final long FIVE_MINUTES = 1000 * 60 * 5;
	
	private double checkDays = 1D;
	private long maxDelay = 1000L;
	
	@Override
	public void executePostConfigure(JobExecutionContext context) throws JobExecutionException {
		try (Results results = DataLayerMgr.executeQuery("FIND_DEVICES_THAT_NEED_RISK_CHECKING", new Object[] {checkDays})) {
			long resultCount = results.getRowCount();
			if (resultCount <= 0) {
				log.info("No devices need risk checking at this time");
				return;
			}
			
			Duration duration = Duration.between(LocalTime.now(), LocalTime.MAX);
			long remainingTime = (duration.get(ChronoUnit.SECONDS) * 1000) - FIVE_MINUTES;
			long delay = remainingTime/resultCount;
			
			if (delay > maxDelay) {
				delay = maxDelay;
				remainingTime = (delay * resultCount);
			}
			
			if (resultCount > remainingTime) {
				log.warn("There are {0} devices to risk check with {1} ms left in the day: risk checking may not finish in time!", resultCount, remainingTime);
			}
			
			log.info("Risk checking {0} devices in {1} ms: {2} ms delay", resultCount, remainingTime, delay);
			
			while(results.next()) {
				String deviceName = results.getFormattedValue("deviceName");
				InteractionUtils.doRiskChecks(publisher, deviceName, log);
				
				if (delay > 0) {
					try {
						Thread.sleep(delay);
					} catch (InterruptedException e) {
						break;
					}
				}
			}
			
		} catch (Exception e) {
			log.error("Failed to scheduled risk checks: {0}", e.getMessage(), e);
		}
	}

	public double getCheckDays() {
		return checkDays;
	}
	
	public void setCheckDays(double checkDays) {
		this.checkDays = checkDays;
	}
	
	public long getMaxDelay() {
		return maxDelay;
	}
	
	public void setMaxDelay(long maxDelay) {
		this.maxDelay = maxDelay;
	}
}
