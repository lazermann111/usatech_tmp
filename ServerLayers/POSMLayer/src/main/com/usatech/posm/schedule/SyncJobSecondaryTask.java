package com.usatech.posm.schedule;

import java.util.HashMap;
import java.util.Map;

import simple.app.SelfProcessor;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;

import com.usatech.layers.common.InteractionUtils;

public class SyncJobSecondaryTask implements SelfProcessor {
	private static final Log log = Log.getLog();

	protected static final String PROCESS_SECONDARY_SYNCS = "PROCESS_SECONDARY_SYNCS";

	protected int appInstance;

	@Override
	public void process() {
		Map<String, Object> params = new HashMap<String, Object>();
		Results results = null;
		try {
			if (!InteractionUtils.lockProcess(PROCESS_SECONDARY_SYNCS, String.valueOf(appInstance), params)) {
				log.info("Processing SyncJobSecondary is already locked by instance {0}", params.get("lockedBy"));
				return;
			}
			DataLayerMgr.executeCall("PROCESS_SECONDARY_SYNCS", null, true);
		} catch (Exception e) {
			log.error("Error processing SyncJobSecondary", e);
		} finally {
			if (results != null)
				results.close();
			try {
				InteractionUtils.unlockProcess(PROCESS_SECONDARY_SYNCS, String.valueOf(appInstance), params);
			} catch (Exception e) {
				log.error("Error unlocking {0}", PROCESS_SECONDARY_SYNCS, e);
			}
		}
	}
	
	public int getAppInstance() {
		return appInstance;
	}

	public void setAppInstance(int appInstance) {
		this.appInstance = appInstance;
	}

}
