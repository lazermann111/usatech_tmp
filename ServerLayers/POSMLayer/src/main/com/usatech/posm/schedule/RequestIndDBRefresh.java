package com.usatech.posm.schedule;


import java.lang.management.ManagementFactory;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.ReflectionException;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.BeanException;

import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.QuartzCronScheduledWithPublisherJob;
import com.usatech.layers.common.QuartzScheduledService2;
/**
 * This class initiate a request to refresh ind_db_ardef.txt file
 * @author yhe
 *
 */
@DisallowConcurrentExecution
public class RequestIndDBRefresh extends QuartzCronScheduledWithPublisherJob {
	private static final Log log = Log.getLog();
	protected static final String[] PAUSE_JOB_SIGNATURE = new String[] { "java.lang.String" };

	@Override
	public void executePostConfigure(JobExecutionContext context)
			throws JobExecutionException {
		Properties quartzJobMap = getJobDataMap();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("appSettingCd", "IND_DB_FILE_MOD_TIME");
		String indDbModTime;
		try{
			DataLayerMgr.selectInto("GET_APP_SETTING_VALUE", params);
			indDbModTime=ConvertUtils.getStringSafely(params.get("appSettingValue"));
		}catch(SQLException e) {
			throw new JobExecutionException("Could not get IND_DB_FILE_MOD_TIME", e);
		} catch(DataLayerException e) {
			throw new JobExecutionException("Could not get IND_DB_FILE_MOD_TIME", e);
		} catch(BeanException e) {
			throw new JobExecutionException("Could not get IND_DB_FILE_MOD_TIME", e);
		} 
		
		String jobId=context.getJobDetail().getKey().getName();
		MessageChainV11 messageChain = new MessageChainV11();
		MessageChainStep sftpStep = messageChain.addStep(quartzJobMap.getProperty("requestIndDBRefreshQueueKey"));
		sftpStep.addStringAttribute("currentIndDbModTime", indDbModTime);
		
		MessageChainStep resumeStep = messageChain.addStep(quartzJobMap.getProperty("resumeRequestIndDBRefreshQueueKey"));
		resumeStep.addReferenceAttribute("newIndDbModTime", sftpStep, "newIndDbModTime");
		resumeStep.addStringAttribute("jobId", jobId);
		sftpStep.setNextSteps(0, resumeStep);
		try {
			ManagementFactory.getPlatformMBeanServer().invoke(QuartzScheduledService2.QUARTZ_OBJECT_NAME, "pauseJob", new Object[] { jobId }, PAUSE_JOB_SIGNATURE);
		} catch(InstanceNotFoundException e) {
			throw new JobExecutionException("Failed to pause requestIndDBRefresh.", e);
		} catch(ReflectionException e) {
			throw new JobExecutionException("Failed to pause requestIndDBRefresh.", e);
		} catch(MBeanException e) {
			throw new JobExecutionException("Failed to pause requestIndDBRefresh.", e);
		}
		boolean okay = false;
		try{
			MessageChainService.publish(messageChain, publisher);
			log.info("Requested to refresh ind_db_ardef.txt file." );
			okay = true;
		} catch(ServiceException e) {
			throw new JobExecutionException("Failed to request ind_db_ardef.txt refresh.", e);
		} finally {
			if(!okay)
				try {
					ManagementFactory.getPlatformMBeanServer().invoke(QuartzScheduledService2.QUARTZ_OBJECT_NAME, "resumeJob", new Object[] { jobId }, PAUSE_JOB_SIGNATURE);
				} catch(InstanceNotFoundException e) {
					throw new JobExecutionException("Failed to resume requestIndDBRefresh.", e);
				} catch(ReflectionException e) {
					throw new JobExecutionException("Failed to resume requestIndDBRefresh.", e);
				} catch(MBeanException e) {
					throw new JobExecutionException("Failed to resume requestIndDBRefresh.", e);
				}

		}
	}
}
