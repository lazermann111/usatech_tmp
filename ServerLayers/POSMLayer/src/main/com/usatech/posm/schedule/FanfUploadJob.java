package com.usatech.posm.schedule;

import static simple.text.MessageFormat.*;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.results.Results;
import simple.text.*;
import simple.text.FormattedFile.Row;
import simple.text.FormattedFile.Row.Field;
import simple.text.FormattedFile.ValidationException;

import com.usatech.app.*;
import com.usatech.layers.common.QuartzCronScheduledWithPublisherJob;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.posm.utils.POSMUtils;

@DisallowConcurrentExecution
public class FanfUploadJob extends QuartzCronScheduledWithPublisherJob {
	
	public static final Log log = Log.getLog();
	
	public static final char PADDING = ' ';
	public static final String RECORD_TERMINATOR = "\n";
	public static final String HEADER_INDICATOR = "H";
	public static final String DETAIL_INDICATOR = "D";
	public static final String TRAILER_INDICATOR = "TRAILER";
	
	public static final String SEQUENCE_NUMBER_KEY = "FANF_LAST_SEQUENCE_NUMBER";
	
	public static final String RESOURCE_KEY_FORMAT = "FANF_{0}";

	public static final String QUERY_ID = "GET_CUSTOMERS_FOR_FANF";

	public static final ThreadSafeDateFormat shortDateFormat = new ThreadSafeDateFormat(new SimpleDateFormat("yyMMdd"));
	public static final ThreadSafeDateFormat dateFormat = new ThreadSafeDateFormat(new SimpleDateFormat("MMddyyyy"));
	public static final ThreadSafeDateFormat dateTimeFormat = new ThreadSafeDateFormat(new SimpleDateFormat("yyyyMMddHHmmss"));

	private String sftpUploadQueueKey;
	private String resourceDeleteQueueKey;
	private ResourceFolder resourceFolder;
	private int companyId;
	private int sid;
	private String sidPassword;
	private int pid;
	private String pidPassword;
	private int defaultTdId;
	private boolean includeValidationFailures = false;
	private String host;
	private int port;
	private String username;
	private String password;
	private String directory;
	
	@Override
	public void executePostConfigure(JobExecutionContext context) throws JobExecutionException {
		validateParams();
		
		Date startTime = new Date();
		long startTs = startTime.getTime();
		
		Results results =  null;
		try {
			results = DataLayerMgr.executeQuery(QUERY_ID, new Object[] {});
		} catch (Exception e) {
			throw new JobExecutionException(e);
		}
		
		FormattedFile file = new FanfFile();
		
		int failCount = 0;
		
		try {
			while(results.next()) {
				long customerId = results.getValue("CUSTOMER_ID", Long.class);

				Row row = file.nextData();
				
				row.set("RECORD_INDICATOR", DETAIL_INDICATOR);
		
				for(Field field : row) {
					Object o = results.get(field.getKey().toString());
					if (o == null)
						continue;
					
					String value = StringUtils.trim(ConvertUtils.getStringSafely(o));
					
					int maxLen = field.getDefinition().getSize();
					if (value.length() > maxLen) {
						value = value.substring(0, maxLen);
					}
					
					field.setValue(value);
				}
				
				// TODO: query TDID from customer's devices's MID
				if (!row.getField("TD_ID").isPopulated()) {
					row.set("TD_ID", Integer.toString(defaultTdId));
				}
				
				try {
					file.commitData();
				} catch (ValidationException e) {
					log.debug("Customer {0} row failed validation: {1}: {2}", customerId, e.getMessage(), row.toString());
					failCount++;
					if (includeValidationFailures) {
						try {
							file.commitData(true);
						} catch (ValidationException e2) {
							// this shouldn't happen
							log.debug("Customer {0} row failed validation(2): {1}: {2}", customerId, e.getMessage(), row.toString());
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("Failed to load results due to unexpected exception: {0}", e.getMessage(), e);
			throw new JobExecutionException(e);
		} finally {
			if (results != null)
				results.close();
		}
		
		log.info("{0} rows added ({1}including validation failures) {2} rows failed validation", file.getDataRowCount(), includeValidationFailures ? "" : "not ", failCount);
		
		if (file.getDataRowCount() == 0) {
			log.info("Nothing to upload!");
			return;
		}
		
		file.header()
			.set("PID", format("PID={0}", pid))
			.set("PID_PASSWORD", pidPassword)
			.set("SID", format("SID={0}", pid))
			.set("SID_PASSWORD", sidPassword)
			.set("START", "START")
			.set("DATE", shortDateFormat.format(startTime))
			.set("VERSION", "3.0.0");
		
		try {
			file.commitHeader();
		} catch (ValidationException e) {
			log.error("Header row failed validation", e);
			throw new JobExecutionException(e);
		}
		
		file.header()
			.set("RECORD_INDICATOR", HEADER_INDICATOR)
			.set("PID", Integer.toString(pid))
			.set("VERSION", "1.1.0")
			.set("DATE", dateFormat.format(startTime))
			.set("COMPANY_ID", Integer.toString(companyId));
		
		try {
			file.commitHeader();
		} catch (ValidationException e) {
			log.error("Header row failed validation", e);
			throw new JobExecutionException(e);
		}
		
		file.footer()
			.set("RECORD_INDICATOR", TRAILER_INDICATOR)
			.set("RECORD_COUNT", Integer.toString(file.getDataRowCount())); // per Chase 7/15/15, record count is detail rows only

		try {
			file.commitFooter();
		} catch (ValidationException e) {
			log.error("Footer row failed validation", e);
			throw new JobExecutionException(e);
		}
		
		String resourceName = format(RESOURCE_KEY_FORMAT, startTs);
		Resource resource = null;
		
		boolean ok = false;
		try {
			resource = resourceFolder.getResource(resourceName, ResourceMode.CREATE);
			OutputStreamWriter out = new OutputStreamWriter(resource.getOutputStream());
			file.appendTo(out);
			out.close();
			ok = true;
		} catch (Exception e) {
			log.error("Failed to write resource: {0}", resourceName, e);
			throw new JobExecutionException(e);
		} finally {
			if(!ok && resource != null) {
				resource.delete();
				resource.release();
			}
		}
		
		int sequenceNumber = 1;
		try {
			sequenceNumber = POSMUtils.getAppSetting(SEQUENCE_NUMBER_KEY, Integer.class, 0) + 1;
		} catch (ServiceException e) {
			throw new JobExecutionException(e);
		}
		 
		if (sequenceNumber >= 1000)
			sequenceNumber = 1;
		
		String uploadPath = String.format("%s/t%s.%03d", directory, pid, sequenceNumber);
		String renamePath = String.format("%s/p%s.%03d", directory, pid, sequenceNumber);
		
		MessageChain mc = new MessageChainV11();
		MessageChainStep uploadStep = mc.addStep(sftpUploadQueueKey);
		uploadStep.setAttribute(CommonAttrEnum.ATTR_RESOURCE, resource.getKey());
		uploadStep.setAttribute(CommonAttrEnum.ATTR_FILE_PATH, uploadPath);
		uploadStep.setAttribute(CommonAttrEnum.ATTR_FILE_RENAME_REPLACE, renamePath);
		uploadStep.setAttribute(CommonAttrEnum.ATTR_HOST, host);
		uploadStep.setAttribute(CommonAttrEnum.ATTR_PORT, port);
		uploadStep.setAttribute(CommonAttrEnum.ATTR_USERNAME, username);
		uploadStep.setSecretAttribute(CommonAttrEnum.ATTR_PASSWORD, password);
		
		MessageChainStep deleteFileStep = mc.addStep(resourceDeleteQueueKey);
		deleteFileStep.setAttribute(CommonAttrEnum.ATTR_RESOURCE, resource.getKey());
		
		uploadStep.setNextSteps(0, deleteFileStep);
		uploadStep.setNextSteps(1, deleteFileStep);
		uploadStep.setNextSteps(2, deleteFileStep);

		log.info("Uploading FANF file with {0} rows to {1}:{2}:{3}", file.getDataRowCount(), host, port, renamePath);
		
		try {
			MessageChainService.publish(mc, publisher);
		} catch (Exception e) {
			log.fatal("Failed to publish message chain resource: {0}", resource.getKey(), e);
			throw new JobExecutionException(e);
		} finally {
			resource.release();
			try {
				POSMUtils.setAppSetting(SEQUENCE_NUMBER_KEY, sequenceNumber);
			} catch (ServiceException e) {
				throw new JobExecutionException(e);
			}
		}
		
	}
	
	public String getSftpUploadQueueKey() {
		return sftpUploadQueueKey;
	}

	public void setSftpUploadQueueKey(String sftpUploadQueueKey) {
		this.sftpUploadQueueKey = sftpUploadQueueKey;
	}

	public String getResourceDeleteQueueKey() {
		return resourceDeleteQueueKey;
	}

	public void setResourceDeleteQueueKey(String resourceDeleteQueueKey) {
		this.resourceDeleteQueueKey = resourceDeleteQueueKey;
	}

	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}
	
	public boolean isIncludeValidationFailures() {
		return includeValidationFailures;
	}

	public void setIncludeValidationFailures(boolean includeValidationFailures) {
		this.includeValidationFailures = includeValidationFailures;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDirectory() {
		return directory;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public int getSid() {
		return sid;
	}

	public void setSid(int sid) {
		this.sid = sid;
	}

	public String getSidPassword() {
		return sidPassword;
	}

	public void setSidPassword(String sidPassword) {
		this.sidPassword = sidPassword;
	}

	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public String getPidPassword() {
		return pidPassword;
	}

	public void setPidPassword(String pidPassword) {
		this.pidPassword = pidPassword;
	}

	public int getDefaultTdId() {
		return defaultTdId;
	}

	public void setDefaultTdId(int defaultTdId) {
		this.defaultTdId = defaultTdId;
	}

	public void validateParams() throws JobExecutionException {
		if (sftpUploadQueueKey == null)
			throw new JobExecutionException("required parameter missing: sftpUploadQueueKey");
		if (resourceDeleteQueueKey == null)
			throw new JobExecutionException("required parameter missing: resourceDeleteQueueKey");
		if (resourceFolder == null)
			throw new JobExecutionException("required parameter missing: resourceFolder");
		if (companyId <= 0)
			throw new JobExecutionException("required parameter missing: companyId");
		if (sid <= 0)
			throw new JobExecutionException("required parameter missing: sid");
		if (sidPassword == null)
			throw new JobExecutionException("required parameter missing: sidPassword");
		if (pid <= 0)
			throw new JobExecutionException("required parameter missing: pid");
		if (pidPassword == null)
			throw new JobExecutionException("required parameter missing: pidPassword");
		if (defaultTdId <= 0)
			throw new JobExecutionException("required parameter missing: defaultTdId");
		if (directory == null)
			throw new JobExecutionException("required parameter missing: directory");
		if (host == null)
			throw new JobExecutionException("required parameter missing: host");
		if (port <= 0)
			throw new JobExecutionException("required parameter missing: port");
		if (username == null)
			throw new JobExecutionException("required parameter missing: username");
		if (password == null)
			throw new JobExecutionException("required parameter missing: password");
	}

	@SuppressWarnings("serial")
	public class FanfFile extends FormattedFile
	{
		public FanfFile() {
			super(RowFormat.FIXED_WIDTH, PADDING, RECORD_TERMINATOR);
			
			defineHeader().field("PID").size(11).required();
			defineHeader().field("PID_PASSWORD").size(9).required();
			defineHeader().field("SID").size(11).required();
			defineHeader().field("SID_PASSWORD").size(9).required();
			defineHeader().field("START").size(7);
			defineHeader().field("DATE").size(7).numeric();
			defineHeader().field("VERSION").size(6).required();
			defineHeader().field("RESERVED").size(60);
			
			defineNextHeader();	
			
			defineHeader().field("RECORD_INDICATOR").size(1).required();
			defineHeader().field("PID").size(6).numericRightZero().required();
			defineHeader().field("SPACE").size(3);
			defineHeader().field("VERSION").size(5).required();
			defineHeader().field("DATE").size(8).numeric();
			defineHeader().field("COMPANY_ID").size(10).numericRightZero().required();
			defineHeader().field("RESERVED").size(67);
			
			defineData().field("RECORD_INDICATOR").size(1).required();
			defineData().field("CUSTOMER_ID").size(30).required();
			defineData().field("TAX_ID").size(18).required();
			defineData().field("TD_ID").size(10).numericRightZero().required();
			defineData().field("RESERVED").size(41);

			defineFooter().field("RECORD_INDICATOR").size(1).required();
			defineFooter().field("RECORD_COUNT").size(6).numericRightZero().required();
			defineFooter().field("RESERVED").size(93);
		}
	}
}
