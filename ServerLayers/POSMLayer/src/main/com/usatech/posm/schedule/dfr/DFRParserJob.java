package com.usatech.posm.schedule.dfr;

import java.util.Date;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import simple.app.ServiceException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;

import com.usatech.layers.common.QuartzCronScheduledWithPublisherJob;
import com.usatech.posm.utils.POSMUtils;

@DisallowConcurrentExecution
public class DFRParserJob extends QuartzCronScheduledWithPublisherJob {
	public static final Log log = Log.getLog();
	
	public static final long ONE_HOUR = (60 * 60 * 1000);
	public static final long ONE_DAY = (ONE_HOUR * 24);
	public static final long ONE_WEEK = (7 * ONE_DAY);

	public static final String LAST_RUN_KEY = "DFR_LAST_RUN_TS";
	
	private long retryMs = ONE_HOUR;

	@Override
	public void executePostConfigure(JobExecutionContext context) throws JobExecutionException {
		long lastRunTs = 0;

		try {
			lastRunTs = POSMUtils.getAppSetting(LAST_RUN_KEY, Long.class, 0L);
		} catch (ServiceException e) {
			throw new JobExecutionException(e);
		}
		
		Date startTime = new Date();
		long startTs = startTime.getTime();
		
		if (lastRunTs == 0)
			lastRunTs = startTs - (90 * ONE_DAY);

		long lastRunEt = (startTs - lastRunTs);
		double lastRunDays = ((double)lastRunEt)/((double)ONE_DAY);

		try (Results results = DataLayerMgr.executeQuery("LIST_RECENT_DFR_FILES", new Object[] {lastRunDays})) {
			while(results.next()) {
				long fileCacheId = results.getValue("FILE_CACHE_ID", Long.class);
				String fileName = results.getFormattedValue("FILE_NAME");

				log.info("Parsing FILE_CACHE_ID {0}: {1}", fileCacheId, fileName);
				
				DFRParser parser = new DFRParser(fileCacheId, retryMs);
				parser.parse();
			}
			
			POSMUtils.setAppSetting(LAST_RUN_KEY, startTime.getTime());
		} catch(Exception e) {
			throw new JobExecutionException(e);
		}
	}
	
	public long getRetryMs() {
		return retryMs;
	}
	
	public void setRetryMs(long retryMs) {
		this.retryMs = retryMs;
	}
}
