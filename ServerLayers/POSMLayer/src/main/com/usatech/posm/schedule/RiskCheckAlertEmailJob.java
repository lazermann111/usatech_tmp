package com.usatech.posm.schedule;

import static simple.text.MessageFormat.format;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.quartz.*;

import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.mail.Email;
import simple.mail.Emailer;
import simple.results.Results;
import simple.text.ThreadSafeDateFormat;

import com.usatech.layers.common.QuartzCronScheduledWithPublisherJob;

@DisallowConcurrentExecution
public class RiskCheckAlertEmailJob extends QuartzCronScheduledWithPublisherJob {
	
	public static final Log log = Log.getLog();
	
	private final static String LINE_SEPARATOR = System.getProperty("line.separator");
	
	private double reportDays = 1;
	
	private String subject = "Risk Management Report for {0}";
	private String to = "pcowan@usatech.com";
	private String from = "riskalert@usatech.com";
	private String dmsRiskAlertPagePrefix = "http://127.0.0.1:8780/riskAlert.i?riskAlertId=";
	
	public static final ThreadSafeDateFormat shortDateFormat = new ThreadSafeDateFormat(new SimpleDateFormat("MM/dd/yyyy"));
	public static final ThreadSafeDateFormat shortDateTimeFormat = new ThreadSafeDateFormat(new SimpleDateFormat("MM/dd/yyyy HH:mm:ss"));

	@Override
	public void executePostConfigure(JobExecutionContext context) throws JobExecutionException {
		try {
			StringBuilder msg = new StringBuilder();
			msg.append("<html><body>");
			msg.append(LINE_SEPARATOR);
			msg.append("<table cellspacing=\"0\" cellpadding=\"1\" border=\"1\">");
			msg.append(LINE_SEPARATOR);
			msg.append(" <tr><th nowrap>ALERT ID</th><th nowrap>TIMESTAMP</th><th nowrap>SCORE</th><th nowrap>STATUS</th><th nowrap>ALERT TYPE</th><th nowrap>CUSTOMER</th><th nowrap>DEVICE</th><th nowrap>LOCATION</th><th nowrap>MESSAGE</th></tr>");
			msg.append(LINE_SEPARATOR);
			
			int count = 0;
			Results results = DataLayerMgr.executeQuery("GET_RISK_ALERTS", new Object[] {reportDays});
			while(results.next()) {
				String dmsUrl = dmsRiskAlertPagePrefix + results.getFormattedValue("id");
				msg.append(format(" <tr><td><a href=\"{0}\">{1}</a></td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td><td>{6}</td><td>{7}</td><td>{8}</td><td>{9}</td></tr>",
						dmsUrl,
						results.getFormattedValue("id"),
						shortDateTimeFormat.format(results.getValue("alertTime", Date.class)),
						results.getFormattedValue("score"),
						results.getFormattedValue("statusName"),
						results.getValue("deviceSerialCd") == null ? "Customer Alert" : "Device Alert",
						results.getFormattedValue("customerName"),
						results.getFormattedValue("deviceSerialCd"),
						results.getFormattedValue("locationName"),
						results.getFormattedValue("message", "HTML")));
				msg.append(LINE_SEPARATOR);
				count++;
			}
			
			if (count == 0) {
				log.info("No risk alerts found in last {0} days to report.", reportDays);
				return;
			}
			
			msg.append("</table");
			msg.append(LINE_SEPARATOR);
			msg.append("</body></html>");
			msg.append(LINE_SEPARATOR);
			
			Properties props = new Properties();
			JobDataMap quartzJobMap = context.getMergedJobDataMap();
			props.putAll(quartzJobMap.getWrappedMap());
			
			Emailer emailer = new Emailer(props);
			
			Email email = emailer.createEmail();
			email.setFrom(from);
			email.setTo(to);
			email.setSubject(format(subject, shortDateFormat.format(new Date())));
			email.setBodyHtml(msg.toString());
			
			email.send();
		} catch (Exception e) {
			log.error("Failed to send Rick Check Alert email: {0}", e.getMessage(), e);
		}
	}
	
	public double getReportDays() {
		return reportDays;
	}
	
	public void setReportDays(double reportDays) {
		this.reportDays = reportDays;
	}
	
	public String getTo() {
		return to;
	}
	
	public void setTo(String to) {
		this.to = to;
	}
	
	public String getFrom() {
		return from;
	}
	
	public void setFrom(String from) {
		this.from = from;
	}
	
	public String getDmsRiskAlertPagePrefix() {
		return dmsRiskAlertPagePrefix;
	}
	
	public void setDmsRiskAlertPagePrefix(String dmsRiskAlertPagePrefix) {
		this.dmsRiskAlertPagePrefix = dmsRiskAlertPagePrefix;
	}

}
