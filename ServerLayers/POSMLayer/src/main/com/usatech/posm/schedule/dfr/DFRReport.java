package com.usatech.posm.schedule.dfr;

import java.sql.SQLException;
import java.util.Date;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;

public class DFRReport {
	
	public static enum Status {
		IP("In-Process"), 
		S("Success"), 
		F("Failed");
		
		private String desc;
		
		private Status(String desc) {
			this.desc = desc;
		}
		
		public String getDesc() {
			return desc;
		}
	}
	
	private long reportId;
	private long reportDefId;
	private long fileCacheId;
	private Date headerGenerationTs;
	private Status processStatus;
	private Date processStartTs;
	private Date processFinishTs;
	private String processMessage;
	
	public DFRReport() {
		
	}
	
	public DFRReport(int reportId) {
		this.reportId = reportId;
	}

	public long getReportId() {
		return reportId;
	}

	public void setReportId(long reportId) {
		this.reportId = reportId;
	}

	public long getReportDefId() {
		return reportDefId;
	}

	public void setReportDefId(long reportDefId) {
		this.reportDefId = reportDefId;
	}

	public long getFileCacheId() {
		return fileCacheId;
	}

	public void setFileCacheId(long fileCacheId) {
		this.fileCacheId = fileCacheId;
	}

	public Date getHeaderGenerationTs() {
		return headerGenerationTs;
	}

	public void setHeaderGenerationTs(Date headerGenerationTs) {
		this.headerGenerationTs = headerGenerationTs;
	}

	public Status getProcessStatus() {
		return processStatus;
	}

	public void setProcessStatus(Status processStatus) {
		this.processStatus = processStatus;
	}

	public Date getProcessStartTs() {
		return processStartTs;
	}

	public void setProcessStartTs(Date processStartTs) {
		this.processStartTs = processStartTs;
	}

	public Date getProcessFinishTs() {
		return processFinishTs;
	}

	public void setProcessFinishTs(Date processFinishTs) {
		this.processFinishTs = processFinishTs;
	}

	public String getProcessMessage() {
		return processMessage;
	}

	public void setProcessMessage(String processMessage) {
		this.processMessage = processMessage;
	}
	
	public boolean isNew() {
		return reportId <= 0;
	}
	
	public boolean shouldRetry(long retryMs) {
		if (processStatus == Status.S)
			return false;
		else if (processStatus == Status.F)
			return true;
		else if (processStartTs == null)
			return true;
		else if ((System.currentTimeMillis() - processStartTs.getTime()) < retryMs)
			return false;
		else
			return true;
	}
	
	public static DFRReport loadByAlternateKey(long fileCacheId, long reportDefId, Date generationDate) throws ServiceException, DataLayerException, SQLException {
		Results results = DataLayerMgr.executeQuery("GET_DFR_REPORT", new Object[] { fileCacheId, reportDefId, generationDate });
		if (!results.next())
			return null;
		
		DFRReport report = new DFRReport();
		try {
			ReflectionUtils.populateProperties(report, results.getValuesMap());
		} catch (Exception e) {
			throw new ServiceException(e);
		}
		
		return report;
	}
	

	public void save() throws DataLayerException, SQLException, ConvertException {
		if (isNew()) {
			Object[] results = DataLayerMgr.executeCall("INSERT_DFR_REPORT", this, true);
			if (results == null || results.length != 2)
				throw new DataLayerException("INSERT_DFR_REPORT returned unexpected results: " + results);
			setReportId(ConvertUtils.getLong(results[1]));
		} else {
			DataLayerMgr.executeCall("UPDATE_DFR_REPORT_STATUS", this, true);
		}
	}
}
