package com.usatech.posm.schedule;


import java.lang.management.ManagementFactory;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.ReflectionException;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.QuartzCronScheduledWithPublisherJob;
import com.usatech.layers.common.QuartzScheduledService2;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CommonAttrEnum;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.BeanException;
import simple.text.MessageFormat;
import simple.text.StringUtils;

/**
 * This class initiate a request to refresh bin range file
 * 
 * @author bkrug
 * 
 */
@DisallowConcurrentExecution
public class ManageBinRangeFile extends QuartzCronScheduledWithPublisherJob {
	private static final Log log = Log.getLog();
	protected static final String[] PAUSE_JOB_SIGNATURE = new String[] { "java.lang.String" };
	protected String host;
	protected int port;
	protected String username;
	protected String password;
	protected MessageFormat filePathFormat;
	protected String fileRenameMatch;
	protected String fileRenameReplace;

	@Override
	public void executePostConfigure(JobExecutionContext context)
			throws JobExecutionException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("appSettingCd", "IND_DB_FILE_MOD_TIME");
		long modificationTime;
		try{
			DataLayerMgr.selectInto("GET_APP_SETTING_VALUE", params);
			modificationTime = ConvertUtils.getLongSafely(params.get("appSettingValue"), 0L);
		}catch(SQLException e) {
			throw new JobExecutionException("Could not get IND_DB_FILE_MOD_TIME", e);
		} catch(DataLayerException e) {
			throw new JobExecutionException("Could not get IND_DB_FILE_MOD_TIME", e);
		} catch(BeanException e) {
			throw new JobExecutionException("Could not get IND_DB_FILE_MOD_TIME", e);
		} 
		
		String jobId=context.getJobDetail().getKey().getName();
		MessageChainV11 messageChain = new MessageChainV11();
		MessageChainStep sftpStep = messageChain.addStep("usat.file.download.sftp.inside");
		sftpStep.setAttribute(CommonAttrEnum.ATTR_HOST, getHost());
		sftpStep.setAttribute(CommonAttrEnum.ATTR_PORT, getPort());
		sftpStep.setAttribute(CommonAttrEnum.ATTR_USERNAME, getUsername());
		sftpStep.setSecretAttribute(CommonAttrEnum.ATTR_PASSWORD, getPassword());
		String filePath = filePathFormat.format(new Object[] { new Date() });
		sftpStep.setAttribute(CommonAttrEnum.ATTR_FILE_PATH, filePath);
		sftpStep.setAttribute(CommonAttrEnum.ATTR_RESOURCE_MODIFIED_TIME, modificationTime);

		MessageChainStep updateStep = messageChain.addStep("usat.applayer.inddb", true);
		updateStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_FILE_KEY, sftpStep, CommonAttrEnum.ATTR_RESOURCE);
		updateStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_NEW_IND_DB_MOD_TIME, sftpStep, CommonAttrEnum.ATTR_RESOURCE_MODIFIED_TIME);
		updateStep.setAttribute(CommonAttrEnum.ATTR_FILE_TYPE, "GBINP.V006.P");

		MessageChainStep resumeStep = messageChain.addStep("usat.posm.schedule.resumerequestindbrefresh");
		resumeStep.setAttribute(AuthorityAttrEnum.ATTR_JOB_ID, jobId);
		resumeStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_NEW_IND_DB_MOD_TIME, sftpStep, CommonAttrEnum.ATTR_RESOURCE_MODIFIED_TIME);

		MessageChainStep deleteStep = messageChain.addStep("usat.file.resource.delete");
		deleteStep.setReferenceAttribute(CommonAttrEnum.ATTR_RESOURCE, sftpStep, CommonAttrEnum.ATTR_RESOURCE);

		sftpStep.setNextSteps(0, updateStep);
		updateStep.setNextSteps(0, resumeStep, deleteStep);
		sftpStep.setNextSteps(1, resumeStep);
		sftpStep.setNextSteps(2, resumeStep);
		deleteStep.addWaitFor(updateStep);

		if(!StringUtils.isBlank(getFileRenameMatch())) {
			MessageChainStep renameStep = messageChain.addStep("usat.file.rename.sftp.inside");
			renameStep.setAttribute(CommonAttrEnum.ATTR_HOST, getHost());
			renameStep.setAttribute(CommonAttrEnum.ATTR_PORT, getPort());
			renameStep.setAttribute(CommonAttrEnum.ATTR_USERNAME, getUsername());
			renameStep.setSecretAttribute(CommonAttrEnum.ATTR_PASSWORD, getPassword());
			renameStep.setAttribute(CommonAttrEnum.ATTR_FILE_PATH, filePath);
			renameStep.setAttribute(CommonAttrEnum.ATTR_FILE_RENAME_MATCH, getFileRenameMatch());
			renameStep.setAttribute(CommonAttrEnum.ATTR_FILE_RENAME_REPLACE, getFileRenameReplace());
			sftpStep.setNextSteps(0, updateStep, renameStep);
			sftpStep.setNextSteps(1, resumeStep, renameStep);
		}

		try {
			ManagementFactory.getPlatformMBeanServer().invoke(QuartzScheduledService2.QUARTZ_OBJECT_NAME, "pauseJob", new Object[] { jobId }, PAUSE_JOB_SIGNATURE);
		} catch(InstanceNotFoundException e) {
			throw new JobExecutionException("Failed to pause requestBinRangeRefresh.", e);
		} catch(ReflectionException e) {
			throw new JobExecutionException("Failed to pause requestBinRangeRefresh.", e);
		} catch(MBeanException e) {
			throw new JobExecutionException("Failed to pause requestBinRangeRefresh.", e);
		}
		boolean okay = false;
		try{
			MessageChainService.publish(messageChain, publisher);
			log.info("Requested refresh of the bin range file.");
			okay = true;
		} catch(ServiceException e) {
			throw new JobExecutionException("Failed to refresh of the bin range file.", e);
		} finally {
			if(!okay)
				try {
					ManagementFactory.getPlatformMBeanServer().invoke(QuartzScheduledService2.QUARTZ_OBJECT_NAME, "resumeJob", new Object[] { jobId }, PAUSE_JOB_SIGNATURE);
				} catch(InstanceNotFoundException e) {
					throw new JobExecutionException("Failed to resume requestBinRangeRefresh.", e);
				} catch(ReflectionException e) {
					throw new JobExecutionException("Failed to resume requestBinRangeRefresh.", e);
				} catch(MBeanException e) {
					throw new JobExecutionException("Failed to resume requestBinRangeRefresh.", e);
				}

		}
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFilePathFormat() {
		return filePathFormat == null ? null : filePathFormat.toPattern();
	}

	public void setFilePathFormat(String filePathFormat) {
		this.filePathFormat = new MessageFormat(filePathFormat);
	}

	public String getFileRenameMatch() {
		return fileRenameMatch;
	}

	public void setFileRenameMatch(String fileRenameMatch) {
		this.fileRenameMatch = fileRenameMatch;
	}

	public String getFileRenameReplace() {
		return fileRenameReplace;
	}

	public void setFileRenameReplace(String fileRenameReplace) {
		this.fileRenameReplace = fileRenameReplace;
	}
}
