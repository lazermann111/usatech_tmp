package com.usatech.posm.process;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;
import simple.servlet.RecordRequestFilter.CallInputs;
import simple.text.MessageFormat;
import simple.translator.DefaultTranslatorFactory;
import simple.translator.Translator;
import simple.translator.TranslatorFactory;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.process.ProcessResponse;
import com.usatech.layers.common.process.Processor;

public class MassEftAdjustment implements Processor {

	private static final Log log = Log.getLog();
	
	public static interface Messages {
		public void header(String messageKey, Object... parameters) throws ServiceException;
		public void footer(String messageKey, Object... parameters) throws ServiceException;
	}
	
	protected static Translator getTranslator(CallInputs ci) throws MalformedURLException {
		String host = new URL(ci.getRequestURL()).getHost();
		String lang = ci.getHeaders().get("accept-language");
		if(lang != null)
			lang = lang.split("[,]", 2)[0];
		Locale locale = ConvertUtils.convertSafely(Locale.class, lang, null);
		try {
			return TranslatorFactory.getDefaultFactory().getTranslator(locale, host);
		} catch(ServiceException e) {
			log.warn("Could not get translator", e);
			return DefaultTranslatorFactory.getTranslatorInstance();
		}
	}

	@Override
	public void processRequest(long requestId, long profileId, long userId, CallInputs ci, InputStream content,
			ProcessResponse response, long lastProcessedLine, long updatedCount) throws ServiceException {
		ci.setAppCd("DMS");
		ci.setActionName("Mass EFT Adjustments");
		ci.setObjectTypeCd("terminal");
		final Translator translator;
		try {
			translator = getTranslator(ci);
		} catch(IOException e) {
			throw new ServiceException(e);
		}

		Messages messageHandler = new Messages() {
			protected final StringBuilder responseText = new StringBuilder();

			protected String translateMessage(String messageKey, String defaultText, Object... parameters) {
				String message;
				if(translator != null)
					message = translator.translate(messageKey, defaultText, parameters);
				else {
					if(defaultText != null)
						message = defaultText;
					else
						message = messageKey;
					if(parameters != null && message != null && message.indexOf('{') >= 0) {
						try {
							message = new MessageFormat(message).format(parameters);
						} catch(RuntimeException e) {
							log.warn("Could not create Message Format from '" + message + "'", e);
						}
					}
				}
				return message;
			}
			
			public void header(String messageKey, Object... parameters) throws ServiceException {
				responseText.setLength(0);
				responseText.append("Mass EFT adjustments: ").append(translateMessage(messageKey, null, parameters));
				response.setEmailSubject(responseText.toString());
			}
			
			public void footer(String messageKey, Object... parameters) throws ServiceException {
				String message = translateMessage(messageKey, null, parameters);
				response.setResponseSummary(message);
			}
		};
		
		String filePath = ConvertUtils.getStringSafely(ci.getRealParameters().get("updateFile"));
		messageHandler.header("device.update.header", filePath);
		
		StringBuilder err = new StringBuilder();
		Object[] result;
		Map<String, Object> sqlParams = new HashMap<String, Object>();
		Date terminalDate = new Date();
		sqlParams.put("terminalDate", terminalDate);

		HashMap<String, Object> requestParams = new HashMap<String, Object>();
		requestParams.put("processRequestId", requestId);
		requestParams.put("profileId", profileId);
		
		HashMap<String, Object> contentParams = new HashMap<String, Object>();
		contentParams.put("processRequestId", requestId);
		
		Connection conn = null;
		boolean success = false;
		long currentLine = 0;
		try {
			conn = DataLayerMgr.getConnection("REPORT");
			BufferedReader br = new BufferedReader(new InputStreamReader(content));
			String line;
			while ((line = br.readLine()) != null) {
				currentLine++;
				if (currentLine <= lastProcessedLine)
					continue;
				success = false;
				String[] values = line.split(",", 3);
				if (values.length < 3)
					continue;
				String eportSerialNum = values[0].trim();
				String amount = values[1].trim();
				String reason = values[2].trim();
				if (eportSerialNum.length() == 0 || amount.length() == 0 || reason.length() == 0) {
					err.append("Error adding EFT adjustment for ").append(eportSerialNum)
							.append(": Invalid serial number, amount or explanation<br /><br />");
					log.error(new StringBuilder("Error adding EFT adjustment for ").append(eportSerialNum)
							.append(": Invalid serial number, amount or explanation").toString());
					continue;
				}

				sqlParams.put("eportSerialNums", eportSerialNum);
				Results rs = DataLayerMgr.executeQuery(conn, "GET_BULK_TERMINAL_INFO", sqlParams);
				if (rs.next()) {
					long terminalId = rs.getValue("terminalId", long.class);
					long customerBankId = rs.getValue("customerBankId", long.class);
					long currencyId = rs.getValue("feeCurrencyId", long.class);
					long businessUnitId = rs.getValue("businessUnitId", long.class);
					try {
						result = DataLayerMgr.executeCall(conn, "GET_OR_CREATE_DOC",
								new Object[] { customerBankId, currencyId, businessUnitId });
						long eftId = ConvertUtils.getLongSafely(result[1], 0);
						if (eftId > 0) {
							result = DataLayerMgr.executeCall(conn, "ADJUSTMENT_INS",
									new Object[] { eftId, terminalId, reason, amount, 'Y' });
							if (ConvertUtils.getLongSafely(result[1], 0) > 0)
								updatedCount++;
						}
					} catch (Exception e) {
						ProcessingUtils.rollbackDbConnection(log, conn);
						err.append("Error adding EFT adjustment for ").append(rs.getFormattedValue("eportSerialNum"))
								.append(": ").append(e.getMessage()).append("<br /><br />");
						log.error(new StringBuilder("Error adding EFT adjustment for ")
								.append(rs.getFormattedValue("eportSerialNum")).toString(), e);
						if (e.getMessage().indexOf("Closed Connection") > -1)
							throw new ServiceException(e);
					}
				}
				lastProcessedLine = currentLine;
				requestParams.put("lastProcessedLine", lastProcessedLine);
				requestParams.put("updatedCount", updatedCount);
				DataLayerMgr.executeUpdate(conn, "UPDATE_PROCESS_REQUEST_LINE", requestParams);
				conn.commit();
				success = true;
			}
			String emailFromName = ConvertUtils.getStringSafely(ci.getRealParameters().get("emailFromName"));
			String emailFromAddress = ConvertUtils.getStringSafely(ci.getRealParameters().get("emailFromAddress"));
			response.setEmailFromName(emailFromName);
			response.setEmailFromAddress(emailFromAddress);

			StringBuilder sb = new StringBuilder();
			sb.append(
					"<html lang=\"en\" xml:lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\"><head><meta http-equiv=\"Content-Type\" content=\"text/html charset=UTF-8\" /><title>DMS - Mass EFT adjustments</title><base href=\"");
			sb.append(ci.getRequestURL());
			sb.append(
					"\" /><style type=\"text/css\">body {  width: 100% !important;  font-family: \"Helvetica Neue\", Helvetica, Arial, sans-serif;  font-size: 16px;  -webkit-text-size-adjust: none;  -ms-text-size-adjust: none;  margin: 0;  padding: 0; } img {  height: auto;  line-height: 100%;  outline: none;  text-decoration: none;  display: block;  }h1, h2, h3, h4, h5, h6 {  color: #333333;  line-height: 100% !important;  margin:0;  }  p {  margin: 1em 0;  }a {  color:#0088cc;  text-decoration: none;  }  a:hover,a:focus {  color: #005580;  text-decoration: underline !important;}table td {  border-collapse: collapse;  } </style><meta name=\"robots\" content=\"noindex, nofollow\"/></head><body bgcolor=\"#ffffff\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">");
			sb.append("<h4>");
			response.setEmailHeader(sb.toString());
			response.setEmailSummaryFirst(true);
			response.setEmailFooter("</ul></body></html>");
			String emailMiddle = "</h4><ul>EFT adjustments added: " + updatedCount;
			if (err.length() > 0) {
				emailMiddle += "\n" + "errors: " + err.toString();
			}
			response.setEmailMiddle(emailMiddle);
			messageHandler.footer("eft.adjustments.footer", updatedCount);
		} catch (Exception e){
			throw new ServiceException(e);
		} finally {
			if (!success)
				ProcessingUtils.rollbackDbConnection(log, conn);
			ProcessingUtils.closeDbConnection(log, conn);
		}
	}
}
