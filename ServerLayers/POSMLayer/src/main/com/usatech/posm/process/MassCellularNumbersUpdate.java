package com.usatech.posm.process;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;

import javax.mail.MessagingException;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.lang.InvalidByteValueException;
import simple.results.BeanException;
import simple.results.Results;
import simple.servlet.RecordRequestFilter.CallInputs;
import simple.text.MessageFormat;
import simple.translator.DefaultTranslatorFactory;
import simple.translator.Translator;
import simple.translator.TranslatorFactory;

import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.LegacyUtils;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.InitializationReason;
import com.usatech.layers.common.device.DeviceTypeRegex;
import com.usatech.layers.common.messagedata.MessageData_AD;
import com.usatech.layers.common.messagedata.MessageData_C0;
import com.usatech.layers.common.process.ProcessResponse;
import com.usatech.layers.common.process.Processor;
import com.usatech.layers.common.util.DeviceUtils;
import com.usatech.posm.process.MassEftAdjustment.Messages;

public class MassCellularNumbersUpdate implements Processor {

	private static final Log log = Log.getLog();
	protected long encryptionKeyMinAgeMin = 1440;
	protected boolean deviceReactivationOnReinit = false;
	protected String deviceFirmwareVersion = "DMS";

	protected static Translator getTranslator(CallInputs ci) throws MalformedURLException {
		String host = new URL(ci.getRequestURL()).getHost();
		String lang = ci.getHeaders().get("accept-language");
		if (lang != null)
			lang = lang.split("[,]", 2)[0];
		Locale locale = ConvertUtils.convertSafely(Locale.class, lang, null);
		try {
			return TranslatorFactory.getDefaultFactory().getTranslator(locale, host);
		} catch (ServiceException e) {
			log.warn("Could not get translator", e);
			return DefaultTranslatorFactory.getTranslatorInstance();
		}
	}

	private boolean deviceExists(String serialNumber) throws SQLException, DataLayerException, ConvertException {
		Results results = DataLayerMgr.executeQuery("GET_DEVICE_COUNT_BY_SERIAL_NUMBER", new Object[] { serialNumber });
		if (results.next()) {
			return results.getValue("device_count", int.class) > 0;
		}
		return false;
	}

	private static byte getDeviceTypeIdBySerialNumber(LinkedList<DeviceTypeRegex> deviceTypeRegexList,
			String serialNumber) {
		if (deviceTypeRegexList == null || serialNumber == null)
			return -1;
		for (DeviceTypeRegex deviceTypeRegex : deviceTypeRegexList) {
			if (deviceTypeRegex.getDeviceTypeSerialCdPattern().matcher(serialNumber).matches())
				return deviceTypeRegex.getDeviceTypeId();
		}
		return -1;
	}

	public static LinkedList<DeviceTypeRegex> getDeviceTypeRegexList()
			throws SQLException, DataLayerException, BeanException {
		LinkedList<DeviceTypeRegex> list = new LinkedList<DeviceTypeRegex>();
		Results results = DataLayerMgr.executeQuery("GET_DEVICE_TYPE_REGEX", null);
		while (results.next()) {
			DeviceTypeRegex deviceTypeRegex = new DeviceTypeRegex();
			results.fillBean(deviceTypeRegex);
			list.add(deviceTypeRegex);
		}
		return list;
	}

	private void deviceInit(String eportSerialNum, LinkedList<DeviceTypeRegex> deviceTypeRegexList)
			throws InvalidByteValueException, SQLException, DataLayerException, ConvertException, ServiceException,
			BeanException, MessagingException {
		byte deviceTypeId = getDeviceTypeIdBySerialNumber(deviceTypeRegexList, eportSerialNum);
		if (deviceTypeId < DeviceType.EDGE.getValue()) {
			MessageData_AD messageData = new MessageData_AD();
			messageData.setDeviceType(DeviceType.getByValue(deviceTypeId));
			messageData.setDeviceSerialNum(eportSerialNum);
			AppLayerUtils.processOfflineInit(messageData, log, encryptionKeyMinAgeMin, deviceReactivationOnReinit);
		} else {
			int propertyListVersion = DeviceUtils.getMaxPropertyListVersion(null);
			MessageData_C0 messageData = new MessageData_C0();
			messageData.setDeviceType(DeviceType.getByValue(deviceTypeId));
			messageData.setDeviceSerialNum(eportSerialNum);
			messageData.setDeviceFirmwareVersion(deviceFirmwareVersion);
			messageData.setPropertyListVersion(propertyListVersion);
			messageData.setProtocolComplianceRevision(4001009);
			messageData.setReasonCode(InitializationReason.NEW_DEVICE);
			AppLayerUtils.processOfflineInit(messageData, log, encryptionKeyMinAgeMin, deviceReactivationOnReinit);
		}
	}

	private long getDeviceId(String eportSerialNum)
			throws SQLException, DataLayerException, ConvertException, ServiceException {
		long deviceId = -1;
		Results sourceDeviceInfo = DataLayerMgr.executeQuery("GET_DEVICE_INFO_BY_SERIAL_NUMBER",
				new Object[] { eportSerialNum }, false);
		if (sourceDeviceInfo.next()) {
			deviceId = sourceDeviceInfo.getValue("device_id", long.class);
		}
		return deviceId;
	}

	private void insertHost(long deviceId, int hostTypeId) throws SQLException, DataLayerException, ServiceException {

		String sql = "select d.device_serial_cd," +
					 "dt.def_base_host_equipment_id," +
					 "(select COALESCE(max(host_port_num),-1) from device.host where device_id = d.device_id) max_port_num " +
					 "from device.device d " +
					 "inner join device.device_type dt on d.device_type_id = dt.device_type_id " +
					 "where d.device_id = CAST (? AS NUMERIC) ";

		Results deviceInfo = DataLayerMgr.executeSQL("OPER", sql, new Object[] { deviceId }, null);
		if (deviceInfo == null || !deviceInfo.next()) {
			throw new ServiceException("Device with id=" + deviceId + " not found");
		}

		Object[] params = new Object[] { deviceInfo.getFormattedValue(1), 
				ConvertUtils.getIntSafely(deviceInfo.getValue("max_port_num"), -1) + 1,
				deviceId, 
				hostTypeId,
				deviceInfo.getFormattedValue(2) };

		try {
			DataLayerMgr.executeUpdate("INSERT_HOST", params, true);
		} catch (SQLIntegrityConstraintViolationException e) {
			log.warn(
					"Error while inserting a host with host_port_num 0, device_id " + deviceId + ": " + e.getMessage());
		}
	}

	private void updateHostSerialCD(long hostId, String simOrMeidNum) throws Exception {
		Object[] params = new Object[] { simOrMeidNum, hostId };
		DataLayerMgr.executeUpdate("UPDATE_HOST_SERIAL_CD", params, true);
	}

	private void upsertGPRS(long deviceId, String simOrMeidNum) throws SQLException, DataLayerException {
		Map<String, Object> params = new HashMap<String, Object>();
		if (simOrMeidNum != null
				&& LegacyUtils.VALID_CCID_REGEX.matcher(simOrMeidNum = simOrMeidNum.trim()).matches()) {
			params.put("CCID", simOrMeidNum);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new Date());
			params.put("deviceId", deviceId);
			params.put("rssiTsMs", calendar.getTime());
			params.put("modemInfo", null);
			DataLayerMgr.executeCall("UPDATE_GPRS_SETTINGS", params, true);
		}
	}

	private void upsertPhoneNum(long hostId, String phoneNum) throws SQLException, DataLayerException {
		Object[] params = new Object[] { phoneNum, hostId };
		log.info("before update pnone number: "+phoneNum+", "+hostId);
		DataLayerMgr.executeCall("UPDATE_PHONE_NUMBER", params, true);
		log.info("after update pnone number");
	}

	private void applySimOrMeidNumber(String eportSerialNum, String simOrMeidNum, String phoneNum,
			LinkedList<DeviceTypeRegex> deviceTypeRegexList, int hostTypeId) throws SQLException, DataLayerException,
					ConvertException, InvalidByteValueException, ServiceException, BeanException, MessagingException {

		if (!deviceExists(eportSerialNum)) {
			deviceInit(eportSerialNum, deviceTypeRegexList);
		}

		long deviceId = getDeviceId(eportSerialNum);
		if (deviceId == -1) {
			throw new ServiceException("Serial number " + eportSerialNum + " not found");
		}

		String simInd = null;
		long hostId = -1;
		Results results = DataLayerMgr.executeQuery("GET_HOST_INFO", new Object[] { deviceId, hostTypeId });
		if (results.next()) {
			hostId = results.getValue("host_id", long.class);
			simInd = results.getValue("sim_ind", String.class);
		} else {
			insertHost(deviceId, hostTypeId);
			Results hostInfoResult = DataLayerMgr.executeQuery("GET_HOST_INFO", new Object[] { deviceId, hostTypeId });
			if (hostInfoResult.next()) {
				hostId = hostInfoResult.getValue("host_id", long.class);
				simInd = hostInfoResult.getValue("sim_ind", String.class);
			}
		}

		if (hostId == -1) {
			throw new ServiceException("Host with typeId= " + hostTypeId + " not found for deviceId=" + deviceId);
		}

		try {
			updateHostSerialCD(hostId, simOrMeidNum);
		} catch (Exception e) {
			throw new ServiceException("Error when updating host's serialCD, hostId=" + hostId + ": " + e.getMessage());
		}

		if (simInd.equalsIgnoreCase("Y")) {
			upsertGPRS(deviceId, simOrMeidNum);
		}

		if (phoneNum != null) {
			upsertPhoneNum(hostId, phoneNum);
		}
	}

	@Override
	public void processRequest(long requestId, long profileId, long userId, CallInputs ci, InputStream content,
			ProcessResponse response, long lastProcessedLine, long updatedCount) throws ServiceException {
		ci.setAppCd("DMS");
		ci.setActionName("Cellular Device Support");
		ci.setObjectTypeCd("terminal");
		
		final Translator translator;
		try {
			translator = getTranslator(ci);
		} catch (IOException e) {
			throw new ServiceException(e);
		}

		Messages messageHandler = new Messages() {
			protected final StringBuilder responseText = new StringBuilder();

			protected String translateMessage(String messageKey, String defaultText, Object... parameters) {
				String message;
				if (translator != null)
					message = translator.translate(messageKey, defaultText, parameters);
				else {
					if (defaultText != null)
						message = defaultText;
					else
						message = messageKey;
					if (parameters != null && message != null && message.indexOf('{') >= 0) {
						try {
							message = new MessageFormat(message).format(parameters);
						} catch (RuntimeException e) {
							log.warn("Could not create Message Format from '" + message + "'", e);
						}
					}
				}
				return message;
			}

			public void header(String messageKey, Object... parameters) throws ServiceException {
				responseText.setLength(0);
				responseText.append("Cellular Device Support: ").append(translateMessage(messageKey, null, parameters));
				response.setEmailSubject(responseText.toString());
			}

			public void footer(String messageKey, Object... parameters) throws ServiceException {
				String message = translateMessage(messageKey, null, parameters);
				response.setResponseSummary(message);
			}
		};

		String filePath = ConvertUtils.getStringSafely(ci.getRealParameters().get("updateFile"));
		messageHandler.header("device.update.header", filePath);

		StringBuilder err = new StringBuilder();
		Map<String, Object> sqlParams = new HashMap<String, Object>();
		Date terminalDate = new Date();
		sqlParams.put("terminalDate", terminalDate);

		BufferedReader br = new BufferedReader(new InputStreamReader(content));
		String line;
		long currentLine = 0;
		HashMap<String, Object> requestParams = new HashMap<String, Object>();
		requestParams.put("processRequestId", requestId);
		requestParams.put("profileId", profileId);
		try {
			LinkedList<DeviceTypeRegex> deviceTypeRegexList = getDeviceTypeRegexList();
			int hostTypeId = ConvertUtils.getIntSafely(ci.getRealParameters().get("hostTypeId"), -1);
			while ((line = br.readLine()) != null) {
				currentLine++;
				if (currentLine <= lastProcessedLine)
					continue;
				String[] values = line.split(",", 3);
				if (values.length < 2) {
					continue;
				}
				String eportSerialNum = values[0].trim();
				String simOrMeidNum = values[1].trim();
				if (eportSerialNum.length() == 0 || simOrMeidNum.length() == 0) {
					err.append("Error applying SIM or MEID number for ").append(eportSerialNum)
							.append(": Invalid serial or SIM/MEID number<br /><br />");
					log.error(new StringBuilder("Error applying SIM or MEID number for ").append(eportSerialNum)
							.append(": Invalid serial or SIM/MEID number").toString());
					continue;
				}
				String phoneNum = (values.length > 2 ? values[2].trim() : null);
				if (phoneNum != null && !(phoneNum.matches("\\+1[0-9]{10}") || phoneNum.matches("[0-9]{10}"))) {
					err.append("Error applying phone number for ").append(eportSerialNum)
							.append(": Invalid phone number<br /><br />");
					log.error(new StringBuilder("Error applying phone number for ").append(eportSerialNum)
							.append(": Invalid phone number").toString());
					continue;
				}

				try {
					applySimOrMeidNumber(eportSerialNum, simOrMeidNum, phoneNum, deviceTypeRegexList, hostTypeId);
					++updatedCount;
					lastProcessedLine = currentLine;
					requestParams.put("lastProcessedLine", lastProcessedLine);
					requestParams.put("updatedCount", updatedCount);
					DataLayerMgr.executeUpdate("UPDATE_PROCESS_REQUEST_LINE", requestParams, true);
				} catch (Exception e) {
					err.append("Error applying SIM or MEID number for ").append(eportSerialNum).append(": ")
							.append(e.getMessage()).append("<br /><br />");
					log.error(new StringBuilder("Error applying SIM or MEID number for ").append(eportSerialNum), e);
				}

			}
			String emailFromName = ConvertUtils.getStringSafely(ci.getRealParameters().get("emailFromName"));
			String emailFromAddress = ConvertUtils.getStringSafely(ci.getRealParameters().get("emailFromAddress"));
			response.setEmailFromName(emailFromName);
			response.setEmailFromAddress(emailFromAddress);

			StringBuilder sb = new StringBuilder();
			sb.append(
					"<html lang=\"en\" xml:lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\"><head><meta http-equiv=\"Content-Type\" content=\"text/html charset=UTF-8\" /><title>DMS - Mass EFT adjustments</title><base href=\"");
			sb.append(ci.getRequestURL());
			sb.append(
					"\" /><style type=\"text/css\">body {  width: 100% !important;  font-family: \"Helvetica Neue\", Helvetica, Arial, sans-serif;  font-size: 16px;  -webkit-text-size-adjust: none;  -ms-text-size-adjust: none;  margin: 0;  padding: 0; } img {  height: auto;  line-height: 100%;  outline: none;  text-decoration: none;  display: block;  }h1, h2, h3, h4, h5, h6 {  color: #333333;  line-height: 100% !important;  margin:0;  }  p {  margin: 1em 0;  }a {  color:#0088cc;  text-decoration: none;  }  a:hover,a:focus {  color: #005580;  text-decoration: underline !important;}table td {  border-collapse: collapse;  } </style><meta name=\"robots\" content=\"noindex, nofollow\"/></head><body bgcolor=\"#ffffff\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">");
			sb.append("<h4>");
			response.setEmailHeader(sb.toString());
			response.setEmailSummaryFirst(true);
			response.setEmailFooter("</ul></body></html>");
			String emailMiddle = "</h4><ul>SIM/MEID numbers applyed: " + updatedCount;
			if (err.length() > 0) {
				emailMiddle += "\n" + "errors: " + err.toString();
			}
			response.setEmailMiddle(emailMiddle);
			messageHandler.footer("eft.adjustments.footer", updatedCount);
		} catch (Exception e) {
			throw new ServiceException(e);
		}
	}

}