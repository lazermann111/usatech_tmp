package com.usatech.posm.exceptions;

public class PropagatorException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = -948140901L;

	public PropagatorException(String message) {
        super(message);
    }

    public PropagatorException(String message, Throwable cause) {
        super(message, cause);
    }

    public PropagatorException(Throwable cause) {
        super(cause);
    }
}
