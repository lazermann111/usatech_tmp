package com.usatech.posm.exceptions;

import java.util.Map;

/**
 * @author sjillidimudi
 * 
 *  This class will act as base class for all business exceptions. All
 *  business exceptions are of nature CHECKED w.r.t compiler.
 */
public class POSMBusinessException extends Exception {
	
	private static final long serialVersionUID = 4450086522309529520L;
	
	private String errorCode = null;
	private Map<String, Object> keys = null;

	/**
	 * @return the keys
	 */
	public Map<String, Object> getKeys() {
		return this.keys;
	}

	/**
	 * @param message
	 *            text which needs to be propagated
	 */
	public POSMBusinessException(String message) {
		super(message);
	}

	/**
	 * @param errorCode
	 *            
	 * @param messagetext which needs to be propagated
	 */
	public POSMBusinessException(String errorCode, String message) {
		super(message);
		this.errorCode = errorCode;
	}

	/**
	 * @param message
	 *            text which needs to be propagated
	 * @param cause
	 *            The original exception
	 */
	public POSMBusinessException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param errorCode
	 *            error code
	 * @param message
	 *            text which needs to be propagated
	 * @param cause
	 *            The original exception
	 */
	public POSMBusinessException(String errorCode, String message,
			Throwable cause) {
		super(message, cause);
		this.errorCode = errorCode;
	}

	/**
	 * 
	 * @param errorCode ErrorCode
	 * @param keys - Information for debugging Purpose
	 * @param cause - Root Exception
	 */
	public POSMBusinessException(String errorCode, Map<String, Object> keys,
			Throwable cause) {
		super(cause);
		this.keys = keys;
		this.errorCode = errorCode;
	}

	/**
	 * 
	 * @param errorCode ErrorCode
	 * @param keys Information for debugging Purpose
	 */
	public POSMBusinessException(String errorCode, Map<String, Object> keys) {
		this.keys = keys;
		this.errorCode = errorCode;
	}

	/**
	 * @return Returns the errorCode. Returns error code if not null. If it is
	 *         null, it returns error code of wrapped exception.
	 */
	public String getErrorCode() {
		if (errorCode != null) {
			return errorCode;
		}
		Throwable origException = getOriginalException();

		if (origException != null) {
			if (origException instanceof POSMBusinessException) {
				POSMBusinessException faBiz = (POSMBusinessException) origException;
				return faBiz.getErrorCode();
			}

		}

		return null;
	}

	/**
	 * @param errorCode
	 *            The errorCode
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	/*
	 * This is a convenient method which returns root cause exception.
	 */
	public Throwable getOriginalException() {
		return getCause();
	}
}
