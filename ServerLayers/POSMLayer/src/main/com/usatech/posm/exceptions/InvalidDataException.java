package com.usatech.posm.exceptions;

import java.util.Map;

/**
 * This Exception will be thrown when ever there is no sufficient 
 * information to perform any business operation .
 * 
 * @author sjillidimudi
 *
 */
public class InvalidDataException extends POSMBusinessException {

	
	private static final long serialVersionUID = -6774241112091762479L;

	
	public InvalidDataException(String errorCode, Map<String, Object> keys) {
		super(errorCode, keys);
	
	}

	public InvalidDataException(String errorCode, Map<String, Object> keys,
			Throwable cause) {
		super(errorCode, keys, cause);
	}

	public InvalidDataException(String errorCode, String message,
			Throwable cause) {
		super(errorCode, message, cause);
	}

	public InvalidDataException(String errorCode, String message) {
		super(errorCode, message);
	}

	public InvalidDataException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidDataException(String message) {
		super(message);
	}
    
	
	
}
