package com.usatech.posm.admin;

import java.io.PrintWriter;

/**
 * @author Brian S. Krug
 *
 */

public class ExitCommand extends AbstractPOSMCommand {
	public static final String COMMAND_KEY = "exit";
	public static final String COMMAND_DESC = "Exits the POSM Admin Tool";

	public ExitCommand() {
		super(COMMAND_KEY, COMMAND_DESC, null);
	}

	/**
	 * @see com.usatech.posm.admin.AbstractPOSMCommand#executeCommand(java.util.List, java.io.PrintWriter, java.lang.Object[])
	 */
	@Override
	public boolean executeCommand(Void context, PrintWriter out, Object[] arguments) {
		return false;
	}
}
