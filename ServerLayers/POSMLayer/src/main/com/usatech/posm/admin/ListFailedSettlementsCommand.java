package com.usatech.posm.admin;

import java.io.PrintWriter;
import java.sql.SQLException;

import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;
import simple.text.StringUtils;

/**
 * @author bkrug
 *
 */
public class ListFailedSettlementsCommand extends AbstractPOSMCommand {
	private static Log log = Log.getLog();
	public static final String COMMAND_KEY = "listfailedsettle";
	public static final String COMMAND_DESC = "List incomplete settlements";

	public ListFailedSettlementsCommand() {
		super(COMMAND_KEY, COMMAND_DESC, null);
	}

	/**
	 * @see com.usatech.posm.admin.AbstractPOSMCommand#executeCommand(java.util.List, java.io.PrintWriter, java.lang.Object[])
	 */
	@Override
	public boolean executeCommand(Void context, PrintWriter out, Object[] arguments) {
		try {
			Results results = DataLayerMgr.executeQuery("GET_INCOMPLETE_SETTLEMENTS", null);
			int[] widths = new int[] {24, 24, 24, 24, 24};
			StringUtils.writeFixedWidth(results, widths, null, out, true, true, true);
		} catch(SQLException e) {
			if(log.isWarnEnabled())
				log.warn("Could not retrieve incomplete settlements", e);
			out.print("List Failed Settle Command failed because ");
			out.print(e.getMessage());
			out.println('.');
		} catch(DataLayerException e) {
			if(log.isWarnEnabled())
				log.warn("Could not retrieve incomplete settlements", e);
			out.print("List Failed Settle Command failed because ");
			out.print(e.getMessage());
			out.println('.');
		}
		return true;
	}
}
