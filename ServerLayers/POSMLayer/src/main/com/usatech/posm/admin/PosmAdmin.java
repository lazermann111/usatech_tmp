/**
 *
 */
package com.usatech.posm.admin;

import java.io.BufferedReader;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.configuration.Configuration;

import simple.app.Command;
import simple.app.CommandLineInterfaceWithConfig;
import simple.app.ServiceException;

/**
 * @author Brian S. Krug
 *
 */
public class PosmAdmin extends CommandLineInterfaceWithConfig<Void> {
	public static void main(String[] args) {
		new PosmAdmin().run(args);
	}
	/**
	 * @see simple.app.CommandLineInterfaceWithConfig#createContext(org.apache.commons.configuration.Configuration, java.io.BufferedReader, java.io.PrintStream)
	 */
	@Override
	protected Void createContext(Configuration config, BufferedReader in, PrintStream out) {
		Map<String,Object> namedReferences = new HashMap<String,Object>();
		try {
			configureSystem(config, namedReferences);
		} catch(ServiceException e) {
			finishAndExit("Could not configure system", 505, e);
			return null;
		}
		try {
			configureDataSourceFactory(config, namedReferences);
		} catch(ServiceException e) {
			finishAndExit("Could not configure data source factory", 500, e);
			return null;
		}
		try {
			configureDataLayer(config, 0);
		} catch(ServiceException e) {
			finishAndExit("Could not configure data layer", 501, e);
			return null;
		}
		try {
			configureStatics(config, namedReferences);
		} catch(ServiceException e) {
			finishAndExit("Could not configure statics", 502, e);
			return null;
		}
		return null;
	}
	/**
	 * @see simple.app.CommandLineInterfaceWithConfig#getCommands(org.apache.commons.configuration.Configuration)
	 */
	@Override
	protected Command<Void>[] getCommands(Configuration config) {
		return new AbstractPOSMCommand[] {
				new CheckCommand(),
				new CheckAllCommand(),
				new ForceSettlementCommand(),
				new ForceTransactionCommand(),
				new ErrorTransactionCommand(),
				new ListFailedSettlementsCommand(),
				new PauseSettleCommand(),
				new PauseTranCommand(),
				new PosmStatusCommand(),
				new ResumeSettleCommand(),
				new ResumeTranCommand(),
				new RetryAllCommand(),
				new RetrySettlementCommand(),
				new RetryTransactionCommand(),
				new SettleBatchCommand(),
				new SettleAllCommand(),
				new ExitCommand()
		};
	}
}
