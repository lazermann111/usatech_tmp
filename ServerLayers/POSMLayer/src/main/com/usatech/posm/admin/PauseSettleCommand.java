package com.usatech.posm.admin;

import java.io.PrintWriter;

import simple.app.ServiceException;
import simple.io.Log;

import com.usatech.layers.common.posm.AdminCommandUtil;

/**
 *
 * @author bkrug
 *
 */
public class PauseSettleCommand extends AbstractPOSMCommand {
	private static Log log = Log.getLog();
	public static final String COMMAND_KEY = "pausesettle";
	public static final String COMMAND_DESC = "Pause Settlement Processing";

	public PauseSettleCommand() {
		super(COMMAND_KEY, COMMAND_DESC, null);
	}

	/**
	 * @see com.usatech.posm.admin.AbstractPOSMCommand#executeCommand(java.util.List, java.io.PrintWriter, java.lang.Object[])
	 */
	@Override
	public boolean executeCommand(Void context, PrintWriter out, Object[] arguments) {
		try {
			AdminCommandUtil.disableSettlementProcessing();
			out.println("Settlement Processing is paused.");
		} catch(ServiceException e) {
			if(log.isWarnEnabled())
				log.warn("Could not disable settlement processing", e);
			out.print("Pause Settle Command failed because ");
			out.print(e.getMessage());
			out.println('.');
		}
		return true;
	}
}
