package com.usatech.posm.admin;

import java.io.PrintWriter;

import simple.app.ServiceException;
import simple.io.Log;

import com.usatech.layers.common.posm.AdminCommandType;
import com.usatech.layers.common.posm.AdminCommandUtil;

/**
 * This class implements the {@link AdminCommandType#SETTLE_ALL} command.
 *
 * @author bkrug
 *
 */
public class SettleAllCommand extends AbstractPOSMCommand {
	private static Log log = Log.getLog();
	public static final String COMMAND_KEY = "settleall";
	public static final String COMMAND_DESC = "Settle all open batches now";

	public SettleAllCommand() {
		super(COMMAND_KEY, COMMAND_DESC, null);
	}

	/**
	 * @see com.usatech.posm.admin.AbstractPOSMCommand#executeCommand(java.util.List, java.io.PrintWriter, java.lang.Object[])
	 */
	@Override
	public boolean executeCommand(Void context, PrintWriter out, Object[] arguments) {
		try {
			AdminCommandUtil.settleAll(getUser(), 100);
			out.println("Requested settlement of all pending batches.");
		} catch(ServiceException e) {
			if(log.isWarnEnabled())
				log.warn("Could not settle all", e);
			out.print("Settle all Command failed because ");
			out.print(e.getMessage());
			out.println('.');
		}
		return true;
	}
}
