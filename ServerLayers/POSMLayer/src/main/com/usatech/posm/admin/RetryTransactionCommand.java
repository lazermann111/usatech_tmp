package com.usatech.posm.admin;

import java.io.PrintWriter;

import simple.app.BasicCommandArgument;
import simple.app.CommandArgument;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Log;

import com.usatech.layers.common.posm.AdminCommandUtil;

/**
 * This Command is used to retry a transaction by the admin.
 *
 * @author bkrug
 *
 */
public class RetryTransactionCommand extends AbstractPOSMCommand {
	private static Log log = Log.getLog();
	public static final String COMMAND_KEY = "retrytran";
	public static final String COMMAND_DESC = "Retry a transaction";

	public RetryTransactionCommand() {
		super(COMMAND_KEY, COMMAND_DESC, new CommandArgument[] {
				new BasicCommandArgument("tranId", long.class, "The Tran Id to retry", false)});
	}

	/**
	 * @see com.usatech.posm.admin.AbstractPOSMCommand#executeCommand(java.util.List, java.io.PrintWriter, java.lang.Object[])
	 */
	@Override
	public boolean executeCommand(Void context, PrintWriter out, Object[] arguments) {
		if(arguments == null || arguments.length < 1) {
			out.println("Retry Transaction Command failed because of invalid input.");
			return true;
		}
		long tranId;
		try {
			tranId = ConvertUtils.getLong(arguments[0]);
		} catch(ConvertException e) {
			if(log.isWarnEnabled())
				log.warn("Could not convert arguments", e);
			out.print("Retry Transaction Command failed because of invalid input");
			out.println('.');
			return true;
		}
		try {
			AdminCommandUtil.retryTransaction(tranId, getUser(), 100);
			out.print("Requested retry of transaction ");
			out.print(tranId);
			out.println(".");
		} catch(ServiceException e) {
			if(log.isWarnEnabled())
				log.warn("Could not retry transaction", e);
			out.print("Retry Transaction Command failed because ");
			out.print(e.getMessage());
			out.println('.');
		}
		return true;
	}
}