package com.usatech.posm.admin;

import java.net.Inet4Address;
import java.net.UnknownHostException;

import simple.app.CommandArgument;
import simple.app.AbstractCommandManager.AbstractCommand;

/**
 * <p>
 * Abstract implementation for the <code>Command</code> interface.
 * </p>
 * <p>
 * It holds the reference to the managed <code>POSMServiceManager</code>.
 * </p>
 *
 * @author bkrug
 *
 */
public abstract class AbstractPOSMCommand extends AbstractCommand<Void> {
	protected static final String user;
	static {
		String hostname;
		try {
			hostname = Inet4Address.getLocalHost().getCanonicalHostName();
		} catch(UnknownHostException e) {
			hostname = "localhost";
		}
		user = System.getProperty("user.name", "unknown") + "@" + hostname;
	}

	protected AbstractPOSMCommand(String key, String description, CommandArgument[] commandArguments) {
		super(key, description, commandArguments);
	}

	protected String getUser() {
		return user;
	}
}
