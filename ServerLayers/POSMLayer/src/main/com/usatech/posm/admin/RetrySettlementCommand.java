package com.usatech.posm.admin;

import java.io.PrintWriter;

import simple.app.BasicCommandArgument;
import simple.app.CommandArgument;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Log;

import com.usatech.layers.common.posm.AdminCommandUtil;

/**
 * Implementation for RetrySettlementCommand.
 *
 *
 * @author bkrug
 *
 */
public class RetrySettlementCommand extends AbstractPOSMCommand {
	private static Log log = Log.getLog();
	public static final String COMMAND_KEY = "retrysettle";
	public static final String COMMAND_DESC = "Retry settlement of a specific batch";

	public RetrySettlementCommand() {
		super(COMMAND_KEY, COMMAND_DESC, new CommandArgument[] {
				new BasicCommandArgument("terminalBatchId", long.class, "The Terminal Batch Id to retry", false)});
	}

	/**
	 * @see com.usatech.posm.admin.AbstractPOSMCommand#executeCommand(java.util.List, java.io.PrintWriter, java.lang.Object[])
	 */
	@Override
	public boolean executeCommand(Void context, PrintWriter out, Object[] arguments) {
		if(arguments == null || arguments.length < 1) {
			out.println("Retry Settlement Command failed because of invalid input.");
			return true;
		}
		long terminalBatchId;
		try {
			terminalBatchId = ConvertUtils.getLong(arguments[0]);
		} catch(ConvertException e) {
			if(log.isWarnEnabled())
				log.warn("Could not convert arguments", e);
			out.print("Retry Settlement Command failed because of invalid input");
			out.println('.');
			return true;
		}
		try {
			AdminCommandUtil.retrySettlement(terminalBatchId, getUser(), 100);
			out.print("Requested retry of batch ");
			out.print(terminalBatchId);
			out.println(".");
		} catch(ServiceException e) {
			if(log.isWarnEnabled())
				log.warn("Could not retry settlement", e);
			out.print("Retry Settlement Command failed because ");
			out.print(e.getMessage());
			out.println('.');
		}
		return true;
	}
}