package com.usatech.posm.admin;

import java.io.PrintWriter;

import simple.app.ServiceException;
import simple.io.Log;

import com.usatech.layers.common.posm.AdminCommandUtil;

/**
 * @author Brian S. Krug
 *
 */
public class RetryAllCommand extends AbstractPOSMCommand {
	private static Log log = Log.getLog();
	public static final String COMMAND_KEY = "retryall";
	public static final String COMMAND_DESC = "Retry all failed open batches now";

	public RetryAllCommand() {
		super(COMMAND_KEY, COMMAND_DESC, null);
	}

	/**
	 * @see com.usatech.posm.admin.AbstractPOSMCommand#executeCommand(java.util.List, java.io.PrintWriter, java.lang.Object[])
	 */
	@Override
	public boolean executeCommand(Void context, PrintWriter out, Object[] arguments) {
		try {
			AdminCommandUtil.retryAll(getUser(), 100);
			out.println("Requested retry of all declined batches.");
		} catch(ServiceException e) {
			if(log.isWarnEnabled())
				log.warn("Could not retry all", e);
			out.print("Retry all Command failed because ");
			out.print(e.getMessage());
			out.println('.');
		}
		return true;
	}
}
