package com.usatech.posm.admin;

import java.io.PrintWriter;

import simple.app.BasicCommandArgument;
import simple.app.CommandArgument;
import simple.app.Publisher;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.ByteInput;
import simple.io.Log;

import com.usatech.layers.common.posm.AdminCommandUtil;

/**
 * @author Brian S. Krug
 *
 */

public class CheckCommand extends AbstractPOSMCommand {
	private static Log log = Log.getLog();
	public static final String COMMAND_KEY = "check";
	public static final String COMMAND_DESC = "Checks a specific terminal for pending transactions or settlements";
	
	protected Publisher<ByteInput> publisher;

	public CheckCommand() {
		super(COMMAND_KEY, COMMAND_DESC, new CommandArgument[] {
				new BasicCommandArgument("paymentSubtypeKeyId", long.class, "The Payment Subtype Key Id (Terminal Id) to check", false),
				new BasicCommandArgument("paymentSubtypeClass", String.class, "The Payment Subtype Class to check", false)});
	}

	/**
	 * @see com.usatech.posm.admin.AbstractPOSMCommand#executeCommand(java.util.List, java.io.PrintWriter, java.lang.Object[])
	 */
	@Override
	public boolean executeCommand(Void context, PrintWriter out, Object[] arguments) {
		if(arguments == null || arguments.length < 2) {
			out.println("Check Command failed because of invalid input.");
			return true;
		}
		long paymentSubtypeKeyId;
		String paymentSubtypeClass;
		try {
			paymentSubtypeKeyId = ConvertUtils.getLong(arguments[0]);
			paymentSubtypeClass = ConvertUtils.getString(arguments[1], true);
		} catch(ConvertException e) {
			if(log.isWarnEnabled())
				log.warn("Could not convert arguments", e);
			out.print("Check Command failed because of invalid input");
			out.println('.');
			return true;
		}
		try {
			AdminCommandUtil.checkTerminal(paymentSubtypeKeyId, paymentSubtypeClass);
			out.print("Requested check of Terminal ");
			out.print(paymentSubtypeKeyId);
			out.print(" (");
			out.print(paymentSubtypeClass);
			out.println(").");
		} catch(ServiceException e) {
			if(log.isWarnEnabled())
				log.warn("Could not check", e);
			out.print("Check Command failed because ");
			out.print(e.getMessage());
			out.println('.');
		}
		return true;
	}

	public Publisher<ByteInput> getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}
}
