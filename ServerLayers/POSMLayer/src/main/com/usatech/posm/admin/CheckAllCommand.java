package com.usatech.posm.admin;

import java.io.PrintWriter;

import simple.app.ServiceException;
import simple.io.Log;

import com.usatech.layers.common.posm.AdminCommandUtil;

/**
 * This class implements the Check All command.
 *
 * @author bkrug
 *
 */
public class CheckAllCommand extends AbstractPOSMCommand {
	private static Log log = Log.getLog();
	public static final String COMMAND_KEY = "checkall";
	public static final String COMMAND_DESC = "Checks all terminals for pending transactions or settlements";

	public CheckAllCommand() {
		super(COMMAND_KEY, COMMAND_DESC, null);
	}

	/**
	 * @see com.usatech.posm.admin.AbstractPOSMCommand#executeCommand(java.util.List, java.io.PrintWriter, java.lang.Object[])
	 */
	@Override
	public boolean executeCommand(Void context, PrintWriter out, Object[] arguments) {
		try {
			int n = AdminCommandUtil.checkAll();
			out.println("Requested check of " + n + " terminals.");
		} catch(ServiceException e) {
			if(log.isWarnEnabled())
				log.warn("Could not check all", e);
			out.print("Check all Command failed because ");
			out.print(e.getMessage());
			out.println('.');
		}
		return true;
	}
}
