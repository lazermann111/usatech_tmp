package com.usatech.posm.admin;

import java.io.PrintWriter;
import java.sql.SQLException;

import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;

/**
 * @author bkrug
 *
 */
public class PosmStatusCommand extends AbstractPOSMCommand {
	private static Log log = Log.getLog();
	public static final String COMMAND_KEY = "posmstatus";
	public static final String COMMAND_DESC = "List status information for POSM";

	public PosmStatusCommand() {
		super(COMMAND_KEY, COMMAND_DESC, null);
	}

	/**
	 * @see com.usatech.posm.admin.AbstractPOSMCommand#executeCommand(java.util.List, java.io.PrintWriter, java.lang.Object[])
	 */
	@Override
	public boolean executeCommand(Void context, PrintWriter out, Object[] arguments) {
		try {
			Results results = DataLayerMgr.executeQuery("GET_POSM_STATUS", null);
			out.println("POSM Status:");
			out.println(StringUtils.pad(null, '-', 80, Justification.LEFT));
			while(results.next()) {
				String name = ConvertUtils.getStringSafely(results.getValue("posmStatusLabel"), "???");
				Object value = results.getValue("posmSettingValue");
				out.print(name);
				out.print(':');
				out.print(StringUtils.pad(null, ' ', Math.max(0, 60 - name.length()), Justification.LEFT));
				out.println(value);
			}
			out.println(StringUtils.pad(null, '-', 80, Justification.LEFT));
		} catch(SQLException e) {
			if(log.isWarnEnabled())
				log.warn("Could not retrieve POSM status", e);
			out.print("POSM Status Command failed because ");
			out.print(e.getMessage());
			out.println('.');
		} catch(DataLayerException e) {
			if(log.isWarnEnabled())
				log.warn("Could not retrieve POSM status", e);
			out.print("POSM Status Command failed because ");
			out.print(e.getMessage());
			out.println('.');
		}
		return true;
	}
}
