package com.usatech.posm.admin;

import java.io.PrintWriter;

import simple.app.BasicCommandArgument;
import simple.app.CommandArgument;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Log;

import com.usatech.layers.common.posm.AdminCommandUtil;

/**
 * This class implements the Business logic to force close a Settlement Batch
 * and make the terminal ready for the Transaction processing.
 *
 * @author bkrug
 *
 */
public class ForceSettlementCommand extends AbstractPOSMCommand {
	private static Log log = Log.getLog();
	public static final String COMMAND_KEY = "forcesettle";
	public static final String COMMAND_DESC = "Force settlement of a specific batch";

	public ForceSettlementCommand() {
		super(COMMAND_KEY, COMMAND_DESC, new CommandArgument[] {
				new BasicCommandArgument("terminalBatchId", long.class, "The Terminal Batch Id to force settle", false),
				new BasicCommandArgument("forceSettleReason", String.class, "The explanation for forcing settlement of this Terminal Batch", false)});
	}

	/**
	 * @see com.usatech.posm.admin.AbstractPOSMCommand#executeCommand(java.util.List, java.io.PrintWriter, java.lang.Object[])
	 */
	@Override
	public boolean executeCommand(Void context, PrintWriter out, Object[] arguments) {
		if(arguments == null || arguments.length < 2) {
			out.println("Force Settlement Command failed because of invalid input.");
			return true;
		}
		long terminalBatchId;
		String forceReason;
		try {
			terminalBatchId = ConvertUtils.getLong(arguments[0]);
			forceReason = ConvertUtils.getString(arguments[1], true);
		} catch(ConvertException e) {
			if(log.isWarnEnabled())
				log.warn("Could not convert arguments", e);
			out.print("Force Settlement Command failed because of invalid input");
			out.println('.');
			return true;
		}
		try {
			AdminCommandUtil.forceSettlement(terminalBatchId, forceReason, getUser(), 100);
			out.print("Requested force of batch ");
			out.print(terminalBatchId);
			out.println(".");
		} catch(ServiceException e) {
			if(log.isWarnEnabled())
				log.warn("Could not force settlement", e);
			out.print("Force Settlement Command failed because ");
			out.print(e.getMessage());
			out.println('.');
		}
		return true;
	}
}
