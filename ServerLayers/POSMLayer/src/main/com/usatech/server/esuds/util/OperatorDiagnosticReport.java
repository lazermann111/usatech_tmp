package com.usatech.server.esuds.util;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;

public class OperatorDiagnosticReport {
	private static Log log = Log.getLog();
	private final static String LINE_SEPARATOR = System.getProperty("line.separator");

	private static final String ESUDS_PFX = "ESUDS_";
	private static final String IMPROPER_SETTINGS = ESUDS_PFX + "IMPROPER_SETTINGS";
	private static final String MACHINE_DIAG = ESUDS_PFX + "MACHINE_DIAG";
	private static final String NO_USAGE = ESUDS_PFX + "NO_USAGE";
	private static final String ROOM_OFFLINE = ESUDS_PFX + "ROOM_OFFLINE";

	private static final int NUM_COLUMNS = 5;

	private List<Results> allResults = new ArrayList<Results>();
	private List<String> allTitles = new ArrayList<String>();

	public List<Results> getResults() {
		return allResults;
	}

	public String getResultsAsCSV() throws ConvertException {
		StringBuilder msg = new StringBuilder();

		msg.append("\"SCHOOL\",\"RESIDENCE HALL\",\"LAUNDRY ROOM\",\"MACHINE\",\"DIAGNOSTIC\"");
		msg.append(LINE_SEPARATOR);

		int r = 0;
		for(Results results : allResults) {
			results.setRow(0);
			while(results.next()) {
				for(int i = 0; i < NUM_COLUMNS; i++) {
					if(i > 0)
						msg.append(',');
					msg.append('"');
					msg.append(results.getValue(i + 1, String.class));
					msg.append('"');
				}
				msg.append(LINE_SEPARATOR);
			}
			if(log.isInfoEnabled())
				log.info("For CSV report: " + (results.getRow() - 1) + " row(s) from " + allTitles.get(r));
			r++;
		}

		return msg.toString();
	}

	public String getResultsAsHTML() throws ConvertException {
		StringBuilder msg = new StringBuilder();

		msg.append("<html><body>");
		msg.append(LINE_SEPARATOR);
		msg.append("<table cellspacing=\"0\" cellpadding=\"1\" border=\"1\">");
		msg.append(LINE_SEPARATOR);
		msg.append(" <tr><th nowrap>SCHOOL</th><th nowrap>RESIDENCE HALL</th><th nowrap>LAUNDRY ROOM</th><th nowrap>MACHINE</th><th nowrap>DIAGNOSTIC</th></tr>");
		msg.append(LINE_SEPARATOR);

		int r = 0;
		for(Results results : allResults) {
			results.setRow(0);
			while(results.next()) {
				msg.append(" <tr>");
				for(int i = 0; i < NUM_COLUMNS; i++) {
					msg.append("<td nowrap>");
					msg.append(results.getValue(i + 1, String.class));
					msg.append("</td>");
				}
				msg.append("</tr>");
				msg.append(LINE_SEPARATOR);
			}
			if(log.isInfoEnabled())
				log.info("For HTML report: " + (results.getRow() - 1) + " row(s) from " + allTitles.get(r));
			r++;
		}

		msg.append("</table");
		msg.append(LINE_SEPARATOR);
		msg.append("</body></html>");
		msg.append(LINE_SEPARATOR);

		return msg.toString();

	}

	public OperatorDiagnosticReport(int customerID, int days) throws SQLException, DataLayerException {
		Map<String, Integer> dbParams = new HashMap<String, Integer>(3);
		dbParams.put("days", days);
		dbParams.put("customerId", customerID);

		allResults.add(DataLayerMgr.executeQuery(ROOM_OFFLINE, dbParams));
		allTitles.add("Room Offline Report");
		allResults.add(DataLayerMgr.executeQuery(MACHINE_DIAG, dbParams));
		allTitles.add("Maytag Diagnostics Report");
		allResults.add(DataLayerMgr.executeQuery(IMPROPER_SETTINGS, dbParams));
		allTitles.add("Improper Settings Report");
		allResults.add(DataLayerMgr.executeQuery(NO_USAGE, dbParams));
		allTitles.add("Machine Zero Usage Report");
	}
}
