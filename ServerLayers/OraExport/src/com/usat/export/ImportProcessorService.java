package com.usat.export;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;

import simple.app.AbstractService;
import simple.app.ServiceException;
import simple.app.ServiceStatus;
import simple.io.Log;
import simple.db.DataLayerMgr;

public class ImportProcessorService extends AbstractService {

	private static final Log log = Log.getLog();

	private static final String SERVICE_NAME = "ImportProcessor";

	public static final int MAX_CNT_FILE_FOR_PROCESSING = 200;

	private static BlockingQueue<File> queue = null;
	
	public static final File DUMMY = new File("");

	private int threads;

	private String copyFilesFolderPath;

	private ScheduledExecutorService executorService;

	public ImportProcessorService(String serviceName) {
		super(SERVICE_NAME);
	}

	@Override
	public int startThreads(int numThreads) throws ServiceException {

		File folder = new File((System.getProperty("user.dir") + copyFilesFolderPath));

		queue = new LinkedBlockingQueue<>(MAX_CNT_FILE_FOR_PROCESSING);

		if (executorService == null) {
			executorService = Executors.newScheduledThreadPool(threads + 1);
		}

		executorService.scheduleWithFixedDelay(new FileEnumerationTask(queue, folder), 0, 2, TimeUnit.SECONDS);

		for (int i = 0; i < threads; i++) {
			executorService.scheduleWithFixedDelay(new CopyFileLoader(queue), 0, 2, TimeUnit.SECONDS);
		}

		return threads;
	}

	public class FileEnumerationTask implements Runnable {
		/**
		 * Constructs a FileEnumerationTask.
		 * 
		 * @param queue
		 *            the blocking queue to which the enumerated files are added
		 * @param startingDirectory
		 *            the directory in which to start the enumeration
		 */
		public FileEnumerationTask(BlockingQueue<File> queue, File startingDirectory) {
			this.queue = queue;
			this.startingDirectory = startingDirectory;
		}

		public void run() {
			try {
				enumerate(startingDirectory);
				if (!queue.contains(DUMMY)) {
					queue.put(DUMMY);
				}
			} catch (InterruptedException e) {
			}
		}

		/**
		 * Enumerates all files in a given directory
		 * 
		 * @param directory
		 *            the directory in which to start
		 */
		public void enumerate(File directory) throws InterruptedException {
			File[] files = directory.listFiles();
			for (File file : files) {
				if ((!file.isDirectory()) && (file.getName().endsWith("data.sql"))) {
					log.debug("Reset inactivity watchdog timer");
					fireServiceStatusChanged(Thread.currentThread(), ServiceStatus.STARTED, 0, null);
					if (!queue.contains(file)) { 
						queue.offer(file, 10, TimeUnit.SECONDS);
					}
				};
				if (file.getName().endsWith("importing")) {
					log.debug("Reset inactivity watchdog timer");
					fireServiceStatusChanged(Thread.currentThread(), ServiceStatus.STARTED, 0, null);
				}
			}
		}

		private BlockingQueue<File> queue;
		private File startingDirectory;
	}

	public class CopyFileLoader implements Runnable {

		private BlockingQueue<File> queue;

		public CopyFileLoader(BlockingQueue<File> queue) {
			this.queue = queue;
		}

		public void run() {
			File copyFile = null;
			try {
				 copyFile = GetCopyFile();
	 			if (copyFile == null) {
					log.debug("Queue of sql file for processing is empty. waiting new files");
					return;
				};
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			processFile(copyFile);
		}

		private synchronized File GetCopyFile() throws InterruptedException {
			File copyFile;
			copyFile = queue.poll(1, TimeUnit.MINUTES);
			return copyFile;
		}
	}

	private void processFile(File copyFile) {
		Connection connection = null;
		try {
			if (!copyFile.getName().endsWith(".sql")) {
				return;
			}

			if (!copyFile.exists()) {
				return;
			}

			copyFile = tryToMarkFileAsLoading(copyFile);
			if (copyFile == null) {
				return;
			}

			connection = DataLayerMgr.getConnection("IMPORT");
			BaseConnection baseConnection = connection.unwrap(BaseConnection.class);

			loadCopyFile(copyFile, baseConnection);
			Files.deleteIfExists(copyFile.toPath());
		} catch (Exception e) {
			log.error("Failed to processing file " + copyFile, e);
			markFileAsError(copyFile);
		} finally {
			if (connection != null) {
				safeCloseConnection(connection);
			}
		}
	}

	private void safeCloseConnection(Connection connection) {
		try {
			connection.close();
		} catch (Exception exception) {
			log.warn("Failed to close connection", exception);
		}
	}

	private synchronized File tryToMarkFileAsLoading(File copyFile) {
		try {
			if (!copyFile.exists()) {
				return null;
			}
			Path path = Files.move(FileSystems.getDefault().getPath(copyFile.getAbsolutePath()),
					FileSystems.getDefault().getPath(copyFile.getAbsolutePath() + ".importing"),
					StandardCopyOption.REPLACE_EXISTING);
			return path.toFile();
		} catch (IOException ioException) {
			log.warn("Failed to mark file " + copyFile + " as importing", ioException);
			return null;
		}
	}

	private void loadCopyFile(File copyFile, BaseConnection baseConnection) throws SQLException, IOException {
		InputStream in = null;
		try {
			long startTime = System.currentTimeMillis();
			log.info("Loading copy file:" + copyFile);
			String tableName = getTableName(copyFile.getName());
			in = new FileInputStream(copyFile);
			CopyManager copyManager = new CopyManager(baseConnection);
			copyManager.copyIn("COPY " + tableName + " FROM STDIN WITH CSV", in);
			baseConnection.commit();
			log.info("Finished load copy file: " + copyFile + "; Filesize: " + copyFile.length() + "; Time spent: "
					+ TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - startTime));
		} finally {
			if (in != null) {
				in.close();
			}
		}
	}

	private String getTableName(String name) {
		return name.substring(0, name.lastIndexOf("_"));
	}

	private void markFileAsError(File copyFile) {
		try {
			if (!copyFile.exists()) {
				return;
			}

			Files.move(FileSystems.getDefault().getPath(copyFile.getAbsolutePath()),
					FileSystems.getDefault().getPath(copyFile.getAbsolutePath() + ".error"),
					StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException ioException) {
			log.warn("Failed to mark file " + copyFile + " as error", ioException);
		}
	}

	@Override
	public int getNumThreads() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int restartThreads(long timeout) throws ServiceException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int pauseThreads() throws ServiceException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int unpauseThreads() throws ServiceException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int stopAllThreads(boolean force, long timeout) throws ServiceException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int stopThreads(int numThreads, boolean force, long timeout) throws ServiceException {
		// TODO Auto-generated method stub
		return 0;
	}

	public String getCopyFilesFolderPath() {
		return copyFilesFolderPath;
	}

	public void setCopyFilesFolderPath(String copyFilesFolderPath) {
		this.copyFilesFolderPath = copyFilesFolderPath;
	}

	public int getThreads() {
		return threads;
	}

	public void setThreads(int threads) {
		this.threads = threads;
	}

}
