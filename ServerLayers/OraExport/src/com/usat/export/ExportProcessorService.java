package com.usat.export;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringEscapeUtils;

import com.usatech.layers.common.ProcessingUtils;

import simple.app.AbstractService;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.ServiceStatus;
import simple.app.WorkRetryType;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.sql.ColumnInfo;
import simple.sql.DatabaseInfo;
import simple.sql.TableInfo;

public class ExportProcessorService extends AbstractService {

	private static final Log log = Log.getLog();

	private static final String SERVICE_NAME = "ExportProcessor";

	private static final List<Integer> CHARACTER_FIELDS = Arrays.asList(Types.CHAR, Types.CLOB, Types.VARCHAR);

	private static final List<String> EXCLUDE_FIELDS = Arrays.asList("G4OP.DEX_FILE.FILE_CONTENT");

	private static final String NUL_BYTE = "\0";

	private static final String EMPTY_CHAR = "";

	private int threads;

	private String copyFilesFolderPath;

	private boolean findNulBytes;

	private ScheduledExecutorService executorService;

	private ConcurrentHashMap<String, String> TableList;

	public ExportProcessorService(String serviceName) {
		super(SERVICE_NAME);
	}

	final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

	public static String bytesToHex(byte[] bytes) {
		if (bytes == null)
			return "";
		char[] hexChars = new char[bytes.length * 2];
		for (int j = 0; j < bytes.length; j++) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}

	@Override
	public int startThreads(int numThreads) throws ServiceException {
		if (executorService == null) {
			executorService = Executors.newScheduledThreadPool(threads);
		}

		File folder = new File((System.getProperty("user.dir") + copyFilesFolderPath));
		boolean success = false;
		if (!folder.exists())
			success = folder.mkdirs();
		if (success)
			log.info("Directories: " + (System.getProperty("user.dir") + copyFilesFolderPath + " created"));

		TableList = new ConcurrentHashMap<String, String>();
	/*	try {
			Results result = DataLayerMgr.executeQuery("TABLES_TO_EXPORT", null);
			while (result.next()) {
				TableList.put(result.getValue("tablename").toString(), result.getValue("filename").toString());
			}
		} catch (Exception e) {
			log.error("Failed to init Export ", e);
		}  */
		for (int i = 0; i < threads; i++) {
			executorService.scheduleWithFixedDelay(new OraExportToCSV(TableList.entrySet().size()), 0, 2,
					TimeUnit.SECONDS);
		}

		return threads;
	}

	public class OraExportToCSV implements Runnable {

		private int tablesCount;

		private HashSet<String> updates;

		public OraExportToCSV(int size) {
			this.tablesCount = size;
			this.updates = new HashSet<String>();

		}

		public void run() {

			Entry<String, String> entry = null;
			try {
				synchronized (TableList) {

					if (TableList.entrySet().iterator().hasNext())
						entry = TableList.entrySet().iterator().next();
					else {
						throw new InterruptedException();
					}
					log.info(String.format("Progress - %d / %d, current - %s ", TableList.entrySet().size(),
							tablesCount, entry.getKey().toString()));
					TableList.remove(entry.getKey());
				}

				generateCsvFile(entry.getKey().toString(), entry.getValue().toString());
				log.info(String.format("End - %s", entry.getKey().toString()));
			} catch (InterruptedException e) {
				executorService.shutdown();
			} catch (Exception e) {
				log.error("Failed to export " + entry.getKey().toString(), e);
			}

		}

		private void generateCsvFile(String table, String filename) throws RetrySpecifiedServiceException {

			Connection conn = null;
			try {
				conn = DataLayerMgr.getConnection("EXPORT");
				Statement stmt = conn.createStatement();
				String sql = null;
				Map<Object, Object> params = new HashMap<Object, Object>();
				sql = getSql(table, params);
				ResultSet rset = stmt.executeQuery(sql);
				if (!rset.next()) {
					return;
				}
				ResultSetMetaData rsmd = rset.getMetaData();
				if (!findNulBytes) {
					filename = tryToMarkFile(System.getProperty("user.dir") + copyFilesFolderPath + "/" + filename)
							.getPath();
				} else
					filename = null;

				// Writer fname = null;
				// fname = new OutputStreamWriter(new FileOutputStream(filename),
				// StandardCharsets.UTF_8);

				StringBuilder sb = new StringBuilder();

				InputStream in = null;
				do {
					if (rset.isFirst())
						addToFile(filename, sb.toString());
					sb.setLength(0);

					for (int i = 1; i <= rsmd.getColumnCount(); i++) {
						try {
							rset.getObject(i);
							if (rset.wasNull())
								continue;

							if (rsmd.getColumnType(i) == Types.BLOB || rsmd.getColumnType(i) == Types.VARBINARY) {
								in = rset.getBinaryStream(i);
								int bytesRead;
								byte[] bytes = new byte[1024];
								if (in != null) {
									sb.append("\\x");
									addToFile(filename, sb.toString());
									while ((bytesRead = in.read(bytes)) > 0) {
										addToFile(filename, bytesToHex(Arrays.copyOf(bytes, bytesRead)));
									}
									// fname.flush();
									sb.setLength(0);
								}

							} else if (rsmd.getColumnType(i) == Types.TIMESTAMP
									|| rsmd.getColumnType(i) == Types.DATE) {
								if (rset.getTimestamp(i) == null)
									continue;

								Calendar cal = GregorianCalendar.getInstance();
								cal.setTime(rset.getTimestamp(i));
								if (cal.get(Calendar.ERA) == GregorianCalendar.AD)
									sb.append(StringEscapeUtils.escapeCsv(rset.getTimestamp(i).toString()));
								else
									sb.append(StringEscapeUtils.escapeCsv("4712-01-01 00:00:00.0 BC"));
							} else {
								String val = rset.getString(i);
								if (val.trim().isEmpty()) {
									val = "\"" + val + "\"";
								}
								if (findNulBytes && CHARACTER_FIELDS.contains(rsmd.getColumnType(i))
										&& (!EXCLUDE_FIELDS.contains(getFullFieldName(table, rsmd.getColumnName(i))))
										&& val.indexOf(NUL_BYTE) >= 0) {
									for (Iterator<Entry<Object, Object>> iterator = params.entrySet()
											.iterator(); iterator.hasNext();) {
										Entry<Object, Object> en = iterator.next();
										int fieldType = ((ColumnInfo) en.getKey()).getSqlType().getTypeCode();
										String fieldName = ((ColumnInfo) en.getKey()).getColumnName();
										String value = rset.getString(fieldName);
										if (fieldType == Types.VARCHAR || fieldType == Types.CHAR)
											value = "\'" + value + "\'";
										en.setValue(value);
									}
									addToUpdates(rsmd.getColumnName(i), table, params);

								}
								sb.append(StringEscapeUtils.escapeCsv(val));
							}
						} finally {
							if (i != rsmd.getColumnCount())
								sb.append(",");
						}
					}
					addToFile(filename, sb.toString().replaceAll(NUL_BYTE, EMPTY_CHAR));
					addToFile(filename, System.getProperty("line.separator"));
					fireServiceStatusChanged(Thread.currentThread(), ServiceStatus.STARTED, 0, null);
				} while (rset.next());

				stmt.close();
				tryToMarkFile(filename);

			} catch (Exception e) {
				throw new RetrySpecifiedServiceException("Failed to export ", e, WorkRetryType.BLOCKING_RETRY);
			} finally {
				ProcessingUtils.closeDbConnection(log, conn);
			}
		}

		private String getFullFieldName(String table, String field) throws SQLException {
			StringBuilder sb = new StringBuilder();
			int endIndex = table.indexOf(" PARTITION");
			sb = sb.append(table.substring(0, (endIndex >= 0 ? endIndex : table.length()))).append(".").append(field);
			return sb.toString();
		}

		private void addToFile(String filename, String string) {
			if (filename == null)
				return;
			FileOp.appendContents(filename, string);
		}

		private synchronized void addToUpdates(String fieldName, String table, Map<Object, Object> params) {

			StringBuilder sb = new StringBuilder();
			if (params.isEmpty()) {
				sb = sb.append("INSTRB(").append(fieldName).append(", CHR(0)) > 0");
			} else {
				for (Iterator<Entry<Object, Object>> iterator = params.entrySet().iterator(); iterator.hasNext();) {
					Entry<Object, Object> en = iterator.next();
					sb = sb.append(sb.length() > 0 ? " AND " : "").append(((ColumnInfo) en.getKey()).getColumnName())
							.append(" = ").append((String) en.getValue());
				}
			}

			StringBuilder sql = new StringBuilder("UPDATE ").append(table).append(" SET ").append(fieldName)
					.append(" = REPLACE(").append(fieldName).append(", CHR(0), '') WHERE ").append(sb.toString())
					.append(";").append(System.getProperty("line.separator"));

			if (updates.add(sql.toString())) {
				FileOp.appendContents(System.getProperty("user.dir") + copyFilesFolderPath + "/update_nul.sql",
						sql.toString());
				log.warn(sql);
			}
		}

		/**
		 * @param table
		 * @throws SQLException
		 * @throws DataLayerException
		 */
		private String getSql(String table, Map<Object, Object> params) throws SQLException, DataLayerException {

			String tableName = table;
			if (table.contains("PARTITION"))
				tableName = table.substring(0, table.indexOf("PARTITION") - 1);
			StringBuilder sql = new StringBuilder("SELECT ");
			TableInfo ti = new DatabaseInfo().getTableInfo("EXPORT", tableName.substring(0, tableName.indexOf(".")),
					tableName.substring(tableName.indexOf(".") + 1));
			for (ColumnInfo col : ti.getColumnInfos()) {
				if (col.isPrimaryKey())
					params.put(col, null);
				if (tableName.equalsIgnoreCase("device.device")
						&& col.getColumnName().equalsIgnoreCase("encryption_key"))
					sql = sql.append("UTL_RAW.CAST_TO_RAW(encryption_key)").append(",");
				else if (tableName.equalsIgnoreCase("report.campaign_blast")
						&& col.getColumnName().equalsIgnoreCase("send_time")) {
					sql = sql.append("to_char(send_time, 'YYYY-MM-DD HH24:MI:SS.FF3 TZR')").append(",");
				} else
					sql = sql.append(col.getColumnName()).append(",");
			}
			sql.setLength(sql.length() - 1);
			return sql.append(" FROM ").append(table).toString();
		}

		private synchronized File tryToMarkFile(String filename) {
			if (filename == null)
				return null;
			File copyFile = new File(filename);
			Path path = null;
			try {
				if (!copyFile.exists() && !copyFile.createNewFile()) {
					return null;
				}
				if (copyFile.getName().contains(".exporting")) {
					path = Files.move(FileSystems.getDefault().getPath(copyFile.getAbsolutePath()),
							FileSystems.getDefault().getPath(copyFile.getAbsolutePath().replaceAll(".exporting", "")),
							StandardCopyOption.REPLACE_EXISTING);
				} else {
					path = Files.move(FileSystems.getDefault().getPath(copyFile.getAbsolutePath()),
							FileSystems.getDefault().getPath(copyFile.getAbsolutePath() + ".exporting"),
							StandardCopyOption.REPLACE_EXISTING);
				}
				return path.toFile();
			} catch (IOException ioException) {
				log.warn("Failed to mark file " + copyFile + " as exporting", ioException);
				return null;
			}
		}

	}

	@Override
	public int getNumThreads() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int restartThreads(long timeout) throws ServiceException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int pauseThreads() throws ServiceException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int unpauseThreads() throws ServiceException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int stopAllThreads(boolean force, long timeout) throws ServiceException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int stopThreads(int numThreads, boolean force, long timeout) throws ServiceException {
		// TODO Auto-generated method stub
		return 0;
	}

	public String getCopyFilesFolderPath() {
		return copyFilesFolderPath;
	}

	public void setCopyFilesFolderPath(String copyFilesFolderPath) {
		this.copyFilesFolderPath = copyFilesFolderPath;
	}

	public int getThreads() {
		return threads;
	}

	public void setThreads(int threads) {
		this.threads = threads;
	}

	public boolean isFindNulBytes() {
		return findNulBytes;
	}

	public void setFindNulBytes(boolean findNulBytes) {
		this.findNulBytes = findNulBytes;
	}

}
