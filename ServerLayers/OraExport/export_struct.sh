#!/bin/bash

schemas="APP_EXEC_HIST APP_LAYER APP_LOG APP_USER AUTHORITY CORP DBADMIN DEVICE ENGINE FOLIO_CONF FRONT G4OP LOCATION PSS QUARTZ RECON REPORT UPDATER WEB_CONTENT"

ftable="./create_tables.sql"
findex="./create_indexes.sql"
fconst="./create_constraints.sql"

rm ./*.sql

printf "\n\set ON_ERROR_STOP ON\n">>$ftable
printf "\n\set ON_ERROR_STOP ON\n">>$findex
printf "\n\set ON_ERROR_STOP ON\n">>$fconst

for schema in $schemas
do
  echo $schema
  ora2pg -o table.sql -b ./  -n $schema -c ./ora_tables.conf
  
  printf "CREATE SCHEMA IF NOT EXISTS $schema AUTHORIZATION admin_1;\n\n">>$ftable
  
  if [ "$schema" = "REPORT" ]; then 
   printf "CREATE TYPE REPORT.t_sync_msg AS (sql_to_run varchar(4000));\n">>$ftable
  fi

  cat table.sql>>$ftable
  cat INDEXES_table.sql>>$findex
  cat CONSTRAINTS_table.sql>>$fconst
  
  printf "\n\n">>$ftable
  printf "\n\n">>$findex
  printf "\n\n">>$fconst

done

rm ./*table.sql
