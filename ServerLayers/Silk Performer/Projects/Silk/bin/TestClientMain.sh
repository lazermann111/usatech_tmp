#!/bin/sh
# This script starts the TestClientMain
# Resolve links - $0 may be a softlink
PRG="$0"

while [ -h "$PRG" ]; do
  ls=`ls -ld "$PRG"`
  link=`expr "$ls" : '.*-> \(.*\)$'`
  if expr "$link" : '/.*' > /dev/null; then
    PRG="$link"
  else
    PRG=`dirname "$PRG"`/"$link"
  fi
done

# Get standard environment variables
PRGDIR=`dirname "$PRG"`
			
# setenv.sh can set JAVA_OPTIONS JAVA_HOME and APP_OPTS
if [ -f $PRGDIR/setenv.sh ];
then
. $PRGDIR/setenv.sh
else
echo "No setenv.sh file."
fi		
if  [ "$1" != "start" ];
then
	JAVA_OPTS="$JAVA_OPTS -Dcom.sun.management.jmxremote=false"
fi
echo "USING JAVA_OPTS: $JAVA_OPTS"
echo "USING JAVA_HOME: $JAVA_HOME"
echo "USING APP_OPTS: $APP_OPTS"

if [ -z "$JAVA_HOME" ]
then
    JAVA=`which java`
    if [ ! -x "$JAVA" ]; then
      echo "Cannot find java command. Please set the JAVA_HOME environment variable."
      exit 1
    fi
else
    JAVA=$JAVA_HOME/bin/java
fi
export JAVA

cd $PRGDIR/..

HOSTNAME=`uname -n`			

# Run the service
echo "[`date`] TestClientMain ..."
$JAVA  $JAVA_OPTS -cp "classes:lib/simple-selected-messaging.jar:lib/usat-unified-layer.jar:lib/layers-common.jar:lib/testclient.jar:lib/common-2007-08-09.jar:lib/commons-beanutils-1.6.jar:lib/commons-collections-3.1.jar:lib/commons-configuration-1.5.jar:lib/commons-dbcp-1.2.1.jar:lib/commons-lang-2.4.jar:lib/commons-logging-1.0.4.jar:lib/commons-pool-1.2.jar:lib/crypto-2008-10-09.jar:lib/log4j-1.2.11.jar:lib/mail-1.4.jar:lib/ojdbc14_g-10.0.2.0.3.jar" com.usatech.test.ClientMain $APP_OPTS $@
echo "[`date`] TestClientMain Finished"
		
