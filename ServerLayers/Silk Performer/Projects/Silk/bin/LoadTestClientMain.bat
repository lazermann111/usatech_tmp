@echo off
REM This script starts the LoadTestClientMain
REM JAVA_HOME JAVA_OPTS APP_OPTS
if exist "%~dp0\setenv.bat" call "%~dp0\setenv.bat"
echo USING JAVA_HOME: %JAVA_HOME%
echo USING JAVA_OPTS: %JAVA_OPTS%
echo USING APP_OPTS: %APP_OPTS%
if not "%JAVA_HOME%" == "" set JAVA=%JAVA_HOME%\bin\java
if not "%JAVA%" == "" goto gotJava
set JAVA=java
:gotJava
cd "%~dp0/.."

echo [%DATE% %TIME%] LoadTestClientMain ...
%JAVA%  %JAVA_OPTS% -Dfile.encoding=ISO8859-1 -cp "classes;lib/simple-selected-messaging.jar;lib/usat-unified-layer.jar;lib/layers-common.jar;lib/testclient.jar;lib/common-2007-08-09.jar;lib/commons-beanutils-1.6.jar;lib/commons-collections-3.1.jar;lib/commons-configuration-1.5.jar;lib/commons-dbcp-1.2.1.jar;lib/commons-lang-2.4.jar;lib/commons-logging-1.0.4.jar;lib/commons-pool-1.2.jar;lib/crypto-2008-10-09.jar;lib/log4j-1.2.11.jar;lib/mail-1.4.jar;lib/ojdbc14_g-10.0.2.0.3.jar" com.usatech.perftest.LoadTestMain %APP_OPTS% %*
echo [%DATE% %TIME%] LoadTestClientMain Finished
echo on
			
