/**
 *
 */
package com.usatech.authoritylayer;

import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.commons.configuration.Configuration;

import simple.bean.ReflectionUtils;

import com.usatech.iso8583.transaction.ISO8583TransactionManager;

/**
 * @author Brian S. Krug
 *
 */
public class InterchangeGlobalsFactory {
	protected static final Class<?>[] txnMgrConstructorParamTypes = new Class<?>[] {ThreadPoolExecutor.class, Configuration.class};
	protected ThreadPoolExecutor threadPoolExecutor;
	protected Configuration configuration;
	protected Class<? extends ISO8583TransactionManager> transactionManagerClass;

	public Class<? extends ISO8583TransactionManager> getTransactionManagerClass() {
		return transactionManagerClass;
	}
	public void setTransactionManagerClass(
			Class<? extends ISO8583TransactionManager> transactionManagerClass) {
		this.transactionManagerClass = transactionManagerClass;
	}
	public ThreadPoolExecutor getThreadPoolExecutor() {
		return threadPoolExecutor;
	}
	public void setThreadPoolExecutor(ThreadPoolExecutor threadPoolExecutor) {
		this.threadPoolExecutor = threadPoolExecutor;
	}
	public Configuration getConfiguration() {
		return configuration;
	}
	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}
	public ISO8583TransactionManager createTransactionManager() throws IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException {
		Class<? extends ISO8583TransactionManager> cls = getTransactionManagerClass();
		if(cls == null)
			throw new IllegalArgumentException("TransactionManagerClass property is not set on " + this);
		if(ReflectionUtils.hasConstructor(cls, txnMgrConstructorParamTypes)) {
			return ReflectionUtils.createObject(cls, getThreadPoolExecutor(), getConfiguration());
		} else if(ReflectionUtils.hasConstructor(cls)) {
			return ReflectionUtils.createObject(cls);
		} else {
			throw new IllegalArgumentException("Can not find constructor to use");
		}
	}
}
