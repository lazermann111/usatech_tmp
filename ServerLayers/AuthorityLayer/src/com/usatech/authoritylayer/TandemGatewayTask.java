package com.usatech.authoritylayer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Currency;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Level;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOHeader;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISORequest;
import org.jpos.iso.ISORequestListener;
import org.jpos.iso.ISOSource;
import org.jpos.iso.header.BaseHeader;
import org.jpos.util.LogSource;

import com.acmetech.cc.CreditCardInfo;
import com.neovisionaries.i18n.CountryCode;
import com.usatech.app.Attribute;
import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.iso8583.ISO8583Response;
import com.usatech.iso8583.interchange.tandem.Bitmap192x8Field;
import com.usatech.iso8583.interchange.tandem.TandemMUX;
import com.usatech.iso8583.interchange.tandem.TandemMsg;
import com.usatech.iso8583.interchange.tandem.TandemPackager;
import com.usatech.iso8583.interchange.tandem.TandemTCPChannel;
import com.usatech.iso8583.jpos.SeparateLineLog4JListener;
import com.usatech.iso8583.jpos.USATChannelPool;
import com.usatech.iso8583.jpos.USATISOMUX;
import com.usatech.layers.common.EMVUtils;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAction;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.DeniedReason;
import com.usatech.layers.common.constants.EntryCapability;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.MessageAttrEnum;
import com.usatech.layers.common.constants.MessageAttribute;
import com.usatech.layers.common.constants.PaymentMaskBRef;
import com.usatech.layers.common.constants.PosEnvironment;
import com.usatech.layers.common.constants.TLVTag;
import com.usatech.layers.common.util.EMVIdtechDecryptValueHandler;
import com.usatech.layers.common.util.VendXParsing;

import simple.app.Prerequisite;
import simple.app.RetryPolicy;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ScheduledRetryServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ValueException;
import simple.bean.ValueException.ValueInvalidException;
import simple.bean.ValueException.ValueMissingException;
import simple.io.IOUtils;
import simple.io.Log;
import simple.io.TLVParser;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.text.IntervalFormat;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;

public class TandemGatewayTask implements MessageChainTask, Prerequisite {
	private static final Log log = Log.getLog();
	protected static final Set<String> usTerritories = new HashSet<String>(Arrays.asList(new String[] { "AS", "FM", "GU", "PR", "PW", "MH", "MP", "VI" }));
	protected static final String REMOVE_REGEX_AN = "[^A-Za-z0-9 ./,$@&-]";
	protected static final String REMOVE_REGEX_EMAIL = "[^A-Za-z0-9_ ./,$@&-]";
	protected static final String REMOVE_REGEX_PHONE = "[^0-9]";
	protected static final String REMOVE_REGEX_POSTAL = "[^A-Za-z0-9]";
	protected static final ISOHeader OUTGOING_TANDEM_HEADER = new BaseHeader("L.PTIISOYN          ".getBytes());
	protected static final RetryPolicy IMMEDIATE_RETRY_POLICY = new RetryPolicy(20, 0, 0.0f);
	protected static final DateFormat EXP_DATE_FORMAT = (DateFormat) ConvertUtils.getFormat("DATE", "yyMM");
	protected static final NumberFormat GEOLOCATION_FORMAT = (NumberFormat) ConvertUtils.getFormat("NUMBER", "+000.00000;-000.00000");
	protected static final NumberFormat TIMESPAN_FORMAT = new IntervalFormat();
	protected static final Map<String, Pattern> CARD_TYPE_PATTERNS;
	static {
		Map<String, Pattern> ctp = new LinkedHashMap<>();
		ctp.put("Visa".toUpperCase(), Pattern.compile("4[0-9]{15}"));
		ctp.put("MasterCard".toUpperCase(), Pattern.compile("(?:5[1-5]|2[2-7])[0-9]{14}"));
		ctp.put("Discover".toUpperCase(), Pattern.compile("(?:30[0-5][0-9]|3095|35[2-8][0-9]|36|3[8-9][0-9]{2}|6011|622[1-9]|62[4-6][0-9]|628[2-8]|64[4-9][0-9]|65[0-9]{2})[0-9]{12}"));
		ctp.put("American Express".toUpperCase(), Pattern.compile("3[47][0-9]{13}"));
		CARD_TYPE_PATTERNS = Collections.unmodifiableMap(ctp);
	}
	protected int responseTimeout = 15000;
	protected boolean persistent = true;
	protected int reconnectDelay = 60000;
	protected int inactivityTimeout = 200000;
	protected int heartbeatInterval = 180000;
	protected Map<String, AuthResultCode> responseMessageResultCd = new HashMap<String, AuthResultCode>();
	protected Map<String, AuthResultCode> responseCodeResultCd = new HashMap<String, AuthResultCode>();
	protected Map<String, DeniedReason> responseMessageDeniedReason = new HashMap<String, DeniedReason>();
	protected Map<String, DeniedReason> responseCodeDeniedReason = new HashMap<String, DeniedReason>();
	protected String declineAvsResults = "ABCN";
	protected String declineCvvResults = "N";
	protected final Map<String, String> avsResultDescriptions = new HashMap<String, String>();
	protected final Map<String, String> cvvResultDescriptions = new HashMap<String, String>();
	protected Map<String, String> cardTypeDescriptions = new HashMap<String, String>();
	protected String vendorIdentifier = "00E5";
	protected String softwareIdentifier = "016A";
	protected USATISOMUX mux;
	protected InetSocketAddress[] urls;
	protected final ThreadPoolExecutor threadPool = (ThreadPoolExecutor) Executors.newCachedThreadPool();
	protected int maxAuthStaleness = 30 * 1000;
	protected Set<String> supportedCurrencies;
	protected float bulkAdditionalTimeoutFactor = 0.0f;
	protected final AuthAdjustmentSetting authPartiallyReversed = new AuthAdjustmentSetting();
	protected Long defaultCustomerId = null;
	protected long maxAuthReversalAge = 60 * 60 * 1000L;
	protected String doingBusinessAsPrefix = "USA*";
	protected final EnumSet<PosEnvironment> rejectPOSEnvironmentSet = EnumSet.of(PosEnvironment.MOBILE);
	protected final Set<Short> rejectMCCSet = new LinkedHashSet<>();
	protected int maxTimeoutRetries = 10;
	protected int maxParallelRequests = 1;
	protected long maxDuplicateAge = 60 * 60 * 1000L;
	protected long requestStartTimestamp = 0;	

	protected static final TLVParser TLV_PARSER = new TLVParser();

	protected ResourceFolder resourceFolder;

	protected class OptionallyIndexedAccountData {
		protected final MessageChainStep step;
		protected final int index;
		protected final boolean useDecryptionAttribute;
		protected String trackData;
		protected String[] partData;

		public OptionallyIndexedAccountData(MessageChainStep step, int index) {
			this(step, index, false);
		}

		public OptionallyIndexedAccountData(MessageChainStep step, int index, boolean useDecryptionAttribute) {
			this.step = step;
			this.index = index;
			this.useDecryptionAttribute = useDecryptionAttribute;
		}

		public String getTrackData() {
			if(trackData == null) {
				try {
					trackData = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_TRACK_DATA, index, String.class, false);
				} catch(AttributeConversionException e) {
					trackData = null;
				}
				if(StringUtils.isBlank(trackData))
					try {
						trackData = step.getOptionallyIndexedAttributeDefault(AuthorityAttrEnum.ATTR_AUTH_ACCOUNT_DATA, index, String.class, "");
					} catch(AttributeConversionException e) {
						trackData = "";
					}
			}
			return trackData.length() == 0 ? null : trackData;
		}

		public String getPartData(PaymentMaskBRef part) {
			if(partData == null)
				partData = new String[PaymentMaskBRef.values().length];
			final int partIndex = part.ordinal();
			if(partData[partIndex] == null)
				try {
					partData[partIndex] = step.getOptionallyIndexedAttributeDefault(useDecryptionAttribute && part.getStoreIndex() > 0 ? part.decryptionAttribute : part.attribute, index, String.class, "");
				} catch(AttributeConversionException e) {
					partData[partIndex] = "";
				}
			else if(partData[partIndex].length() == 0)
				return null;
			return partData[partIndex];
		}
	}
	protected class UnmatchedResponseHandler implements ISORequestListener {
		public boolean process(ISOSource source, ISOMsg isoMsg) {
			if(isoMsg instanceof TandemMsg) {
				try {
					TandemMsg msg = (TandemMsg) isoMsg;
					if(!msg.hasField(0)) {
						log.debug("Heartbeat IN : " + msg);
					}
				} catch(Exception e) {
					log.error("Failed to process Unmatched Response: " + isoMsg + ": " + e.getMessage(), e);
				}
			} else {
				log.error("Received unmatched message from Tandem Host: " + isoMsg);
			}

			return true;
		}
	}
	protected interface RequestProcessor {
		public void prepare(int timeout);

		public AuthResultCode getResult(int timeout) throws TimeoutException, ServiceException;
	}

	protected class TrivialRequestProcessor implements RequestProcessor {
		protected final AuthResultCode result;

		public TrivialRequestProcessor(AuthResultCode result) {
			this.result = result;
		}

		@Override
		public void prepare(int timeout) {
		}

		@Override
		public AuthResultCode getResult(int timeout) {
			return result;
		}
	}

	protected final TrivialRequestProcessor APPROVED_REQUEST_PROCESSOR = new TrivialRequestProcessor(AuthResultCode.APPROVED);
	protected final TrivialRequestProcessor FAILED_REQUEST_PROCESSOR = new TrivialRequestProcessor(AuthResultCode.FAILED);
	protected final TrivialRequestProcessor DECLINED_PERMANENT_REQUEST_PROCESSOR = new TrivialRequestProcessor(AuthResultCode.DECLINED_PERMANENT);

	public TandemGatewayTask() {
	}

	public void initialize() {
		mux = loadMUX();
		sendEchoTest();
	}

	protected void sendEchoTest() {
		TandemMsg request = new TandemMsg();
		request.setHeader(OUTGOING_TANDEM_HEADER);
		// populate request
		try {
			request.setMTI("1800");
			Date localTranDate = new Date();
			request.setTransactionLocalTime(formatDate(localTranDate, "HHmmss"));
			request.setTransactionLocalDate(formatDate(localTranDate, "MMddyyyy"));
			request.setRetrievalReferenceNumber("0");
			request.setNetworkManagementCode("301");
		} catch(ISOException e) {
			log.error("Could not send echo test message", e);
			return;
		}
		final ISORequest isoRequest = new ISORequest(request);
		mux.queue(isoRequest);
		log.info("Sent echo test message");
	}

	protected String[] getPreActionLogAttributes() {
		return new String[] { MessageAttribute.ACTION_TYPE.getValue(), MessageAttribute.AUTHORITY_NAME.getValue(), MessageAttribute.TRACE_NUMBER.getValue(), MessageAttribute.POS_PTA_ID.getValue(), MessageAttribute.AMOUNT.getValue(), MessageAttribute.CURRENCY_CD.getValue(), MessageAttribute.TIME_ZONE_GUID.getValue(), MessageAttribute.ONLINE.getValue() };
	}

	protected String[] getPostActionLogAttributes() {
		return new String[] { MessageAttribute.ACTION_TYPE.getValue(), MessageAttribute.AUTHORITY_NAME.getValue(), MessageAttribute.TRACE_NUMBER.getValue() };
	}

	protected AuthResultCode processInternal(MessageChainTaskInfo taskInfo) throws ServiceException {
		requestStartTimestamp = System.currentTimeMillis();
		MessageChainStep step = taskInfo.getStep();
		Map<String, Object> attributes = step.getAttributes();
		int cnt;
		int hostResponseTimeout;
		try {
			cnt = step.getAttributeDefault(CommonAttrEnum.ATTR_ENTRY_COUNT, Integer.class, Integer.valueOf(0));
			hostResponseTimeout = step.getAttribute(AuthorityAttrEnum.ATTR_RESPONSE_TIME_OUT, Integer.class, true) * 1000;
		} catch(AttributeConversionException e) {
			log.error("Could not convert '" + e.getAttributeKey() + "' for request", e);
			return populateFailedResults(null, step, 0, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Data Conversion Error of '" + e.getAttributeKey() + "' in " + getClass().getSimpleName());
		}
		if(hostResponseTimeout < 1)
			hostResponseTimeout = 15000;
		AuthResultCode authResultCd;
		if(cnt > 0) {
			if(getBulkAdditionalTimeoutFactor() > 0.0f)
				hostResponseTimeout += (cnt - 1) * getBulkAdditionalTimeoutFactor() * hostResponseTimeout;
			authResultCd = null;
			RequestProcessor[] rps = new RequestProcessor[cnt];
			int chunk = getMaxParallelRequests();
			if(chunk < 1 || chunk > cnt)
				chunk = cnt;
			for(int start = 1; start <= cnt; start += chunk) {
				int end = Math.min(start + chunk, cnt + 1);
				for(int index = start; index < end; index++) {
					ProcessingUtils.logAttributes(log, "Starting ", attributes, index, getPreActionLogAttributes(), null);
					rps[index - 1] = processInternal(taskInfo, index);
				}
				// first pass for auth adjustments
				long expiration = System.currentTimeMillis() + hostResponseTimeout;
				for(int index = start; index < end; index++) {
					if(rps[index - 1] == null)
						continue;
					int localTimeout = (int) (expiration - System.currentTimeMillis());
					rps[index - 1].prepare(localTimeout < 1 ? 1 : localTimeout); // must do a timeout of at least 1 or it will wait forever
				}
				// second pass for completions
				expiration = System.currentTimeMillis() + hostResponseTimeout;
				for(int index = start; index < end; index++) {
					if(rps[index - 1] == null)
						continue;
					step.removeAttribute("trackData." + index);
					int localTimeout = (int) (expiration - System.currentTimeMillis());
					AuthResultCode tmp;
					try {
						tmp = rps[index - 1].getResult(localTimeout < 1 ? 1 : localTimeout); // must do a timeout of at least 1 or it will wait forever
					} catch(TimeoutException e) {
						if(taskInfo.getRetryCount() < getMaxTimeoutRetries() || getMaxTimeoutRetries() < 0)
							throw new ScheduledRetryServiceException(e, IMMEDIATE_RETRY_POLICY, false, true);
						tmp = populateFailedResults(null, step, index, DeniedReason.PROCESSOR_TIMEOUT, "Processor Timeout");
					}
					if(authResultCd == null)
						authResultCd = tmp;
					else if(authResultCd != tmp && authResultCd != AuthResultCode.FAILED) {
						switch(authResultCd) {
							case APPROVED:
							case PARTIAL:
								switch(tmp) {
									case PARTIAL:
									case FAILED:
										authResultCd = tmp;
										break;
									default:
										authResultCd = AuthResultCode.PARTIAL;
										break;
								}
								break;
							default:
								switch(tmp) {
									case PARTIAL:
									case APPROVED:
										authResultCd = AuthResultCode.PARTIAL;
										break;
									case FAILED:
										authResultCd = tmp;
										break;
									default:
										authResultCd = AuthResultCode.DECLINED;
										break;
								}
								break;
						}
					}
					ProcessingUtils.logAttributes(log, "Finished, result=" + tmp.getDescription() + "; ", attributes, index, getPostActionLogAttributes(), null);
				}
			}
			if(authResultCd == null) {
				log.error("No indexes were processed");
				authResultCd = AuthResultCode.FAILED;
			}
		} else
			try {
				ProcessingUtils.logAttributes(log, "Starting ", attributes, 0, getPreActionLogAttributes(), null);
				RequestProcessor rp = processInternal(taskInfo, 0);
				rp.prepare(hostResponseTimeout);
				step.removeAttribute("trackData");
				authResultCd = rp.getResult(hostResponseTimeout);
				ProcessingUtils.logAttributes(log, "Finished, result=" + authResultCd.getDescription() + "; ", attributes, 0, getPostActionLogAttributes(), null);
			} catch(TimeoutException e) {
				if(taskInfo.getRetryCount() < getMaxTimeoutRetries() || getMaxTimeoutRetries() < 0 || step.getAttributeSafely(AuthorityAttrEnum.ATTR_ACTION_TYPE, AuthorityAction.class, null) == AuthorityAction.AUTHORIZATION)
					throw new ScheduledRetryServiceException(e, IMMEDIATE_RETRY_POLICY, false, true);
				authResultCd = populateFailedResults(null, step, 0, DeniedReason.PROCESSOR_TIMEOUT, "Processor Timeout");
			}

		return authResultCd;
	}

	protected RequestProcessor processInternal(final MessageChainTaskInfo taskInfo, final int index) throws ServiceException {
		final MessageChainStep step = taskInfo.getStep();
		final AuthorityAction action;
		final String cardType;
		final OptionallyIndexedAccountData accountData;
		try {
			action = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_ACTION_TYPE, index, AuthorityAction.class, index == 0);
			if(action == null) {
				log.info("Action for index " + index + " not found; skipping");
				return null;
			}
			String currencyCd = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_CURRENCY_CD, index, String.class, true);
			if(supportedCurrencies != null && !supportedCurrencies.contains(currencyCd)) {
				log.info("Currency '" + currencyCd + " is not supported by " + this);
				populateFailedResults(null, step, index, DeniedReason.UNSUPPORTED_CURRENCY, "Currency not supported");
				return FAILED_REQUEST_PROCESSOR;
			}
			accountData = new OptionallyIndexedAccountData(step, index);
			cardType = confirmCardType(step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_CARD_TYPE, index, String.class, false), accountData.getPartData(PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER));
			switch(action) {
				case AUTHORIZATION: // check staleness
					boolean sendReversal = false;
					int maxStaleness = getMaxAuthStaleness();
					if(maxStaleness > 0) {
						long authTime = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_AUTH_TIME, index, Long.class, true);
						if(authTime + maxStaleness < System.currentTimeMillis()) {
							log.warn("Auth request against " + taskInfo.getServiceName() + " initiated at " + authTime + " has become stale");
							if(!taskInfo.isRedelivered()) {
								log.warn("Authorization is stale before it was even attempted");
								populateFailedResults(AuthorityAction.AUTHORIZATION, step, index, DeniedReason.INTERNAL_TIMEOUT, "Auth Expired Before Attempted");
								return FAILED_REQUEST_PROCESSOR;
							}
							sendReversal = true;
						}
					}					
					if(!sendReversal && taskInfo.getRetryCount() > 0) {
						sendReversal = true;
						log.warn("Auth retryCount " + taskInfo.getRetryCount() + " is greater than 0, sending reversal");
					}
					if(sendReversal) {
						TandemMsg request;
						try {
							request = createRequest(AuthorityAction.REVERSAL, AuthorityAction.AUTHORIZATION, taskInfo, index, cardType, accountData);
						} catch(ValueMissingException e) {
							log.warn("Missing value in attributes: " + step.getAttributes(), e);
							populateDeclinePermanentResults(action, step, index, DeniedReason.MISSING_INPUT, e.getMessage());
							return DECLINED_PERMANENT_REQUEST_PROCESSOR;
						} catch(ValueInvalidException e) {
							log.warn("Invalid value in attributes: " + step.getAttributes(), e);
							populateDeclinePermanentResults(action, step, index, DeniedReason.INVALID_INPUT, e.getMessage());
							return DECLINED_PERMANENT_REQUEST_PROCESSOR;
						} catch(ISOException | ServiceException e) {
							log.warn("Could not create request", e);
							populateFailedResults(action, step, index, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, e.getMessage());
							return FAILED_REQUEST_PROCESSOR;
						} catch(AttributeConversionException e) {
							log.warn("Could not create request", e);
							populateFailedResults(null, step, index, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Data Conversion Error of '" + e.getAttributeKey() + "' in " + getClass().getSimpleName());
							return FAILED_REQUEST_PROCESSOR;
						}
						final ISORequest isoRequest = new ISORequest(request);
						mux.queue(isoRequest);
						return new RequestProcessor() {
							public void prepare(int timeout) {
							}
	
							public AuthResultCode getResult(int timeout) throws TimeoutException, ServiceException {
								AuthResultCode authResultCd = retrieveResult(isoRequest, AuthorityAction.REVERSAL, AuthorityAction.AUTHORIZATION, taskInfo, index, timeout);
								switch(authResultCd) {
									case FAILED:
										throw new ServiceException("Error occurred while reversing the expired authorization");
								}
								return populateFailedResults(AuthorityAction.AUTHORIZATION, step, index, DeniedReason.INTERNAL_TIMEOUT, "Auth Timeout");
							}
						};
					}
					if(!rejectPOSEnvironmentSet.isEmpty()) {
						PosEnvironment posEnv = step.getOptionallyIndexedAttributeDefault(AuthorityAttrEnum.ATTR_POS_ENVIRONMENT, index, PosEnvironment.class, PosEnvironment.UNSPECIFIED);
						if(rejectPOSEnvironmentSet.contains(posEnv)) {
							log.warn("POS Environment " + posEnv + " is not suppported by this processor; Denying authorization");
							populateDeclinePermanentResults(action, step, index, DeniedReason.CONFIGURATION_ISSUE, posEnv + " devices are not suppported by this processor");
							return DECLINED_PERMANENT_REQUEST_PROCESSOR;
						}
					}
					if(!rejectMCCSet.isEmpty()) {
						Short mcc = step.getOptionallyIndexedAttributeDefault(AuthorityAttrEnum.ATTR_MERCHANT_CATEGORY_CODE, index, Short.class, (short) 5999);
						if(rejectMCCSet.contains(mcc)) {
							log.warn("MCC  " + mcc + " is not suppported by this processor; Denying authorization");
							populateDeclinePermanentResults(action, step, index, DeniedReason.CONFIGURATION_ISSUE, "Devices with MCC of " + mcc + " are not suppported by this processor");
							return DECLINED_PERMANENT_REQUEST_PROCESSOR;
						}
					}
					break;
				case SALE:
					final int amount = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_AMOUNT, index, Integer.class, true);
					final int approvedAmount = step.getOptionallyIndexedAttributeDefault(AuthorityAttrEnum.ATTR_AMT_APPROVED, index, Integer.class, 0);
					long postedTime = step.getOptionallyIndexedAttributeDefault(AuthorityAttrEnum.ATTR_START_TIME, index, Long.class, 0L);

					if(amount == 0 && approvedAmount == 0) {
						step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, index, AuthResultCode.APPROVED.getValue());
						step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, index, "  ");
						step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, index, "Zero Sale - nothing to do");
						return APPROVED_REQUEST_PROCESSOR;
					} else if(amount == 0) {
						if(getMaxAuthReversalAge() <= 0L) {
							log.warn("Auth reversal is not configured");
							populateDeclinePermanentResults(action, step, index, DeniedReason.INTERNAL_TIMEOUT, "Auth reversal is not configured");
							return DECLINED_PERMANENT_REQUEST_PROCESSOR;
						}
						long authTime = step.getOptionallyIndexedAttributeDefault(AuthorityAttrEnum.ATTR_AUTH_TIME, index, Long.class, 0L);
						long currentTime = System.currentTimeMillis();
						if(authTime + getMaxAuthReversalAge() >= currentTime) {
							TandemMsg request;
							try {
								request = createRequest(AuthorityAction.REVERSAL, AuthorityAction.AUTHORIZATION, taskInfo, index, cardType, accountData);
							} catch(ValueMissingException e) {
								log.warn("Missing value in attributes: " + step.getAttributes(), e);
								populateDeclinePermanentResults(action, step, index, DeniedReason.MISSING_INPUT, e.getMessage());
								return DECLINED_PERMANENT_REQUEST_PROCESSOR;
							} catch(ValueInvalidException e) {
								log.warn("Invalid value in attributes: " + step.getAttributes(), e);
								populateDeclinePermanentResults(action, step, index, DeniedReason.INVALID_INPUT, e.getMessage());
								return DECLINED_PERMANENT_REQUEST_PROCESSOR;
							} catch(ISOException | ServiceException e) {
								log.warn("Could not create request", e);
								populateFailedResults(action, step, index, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, e.getMessage());
								return FAILED_REQUEST_PROCESSOR;
							} catch(AttributeConversionException e) {
								log.warn("Could not create request", e);
								populateFailedResults(null, step, index, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Data Conversion Error of '" + e.getAttributeKey() + "' in " + getClass().getSimpleName());
								return FAILED_REQUEST_PROCESSOR;
							}
							final ISORequest isoRequest = new ISORequest(request);
							mux.queue(isoRequest);
							return new RequestProcessor() {
								public void prepare(int timeout) {
								}

								public AuthResultCode getResult(int timeout) throws TimeoutException {
									return retrieveResult(isoRequest, AuthorityAction.REVERSAL, AuthorityAction.AUTHORIZATION, taskInfo, index, timeout);
								}
							};
						}
						String msg = "Auth reversal is outside of max reversal age by " + TIMESPAN_FORMAT.format(currentTime - authTime - getMaxAuthReversalAge());
						log.warn(msg);
						populateDeclinePermanentResults(action, step, index, DeniedReason.INTERNAL_TIMEOUT, msg);
						return DECLINED_PERMANENT_REQUEST_PROCESSOR;
						// TODO: when a sale is redelivered and is within getMaxAuthReversalAge() > 0L then send a reversal and then a sale
					} else if(amount > 0 && getMaxDuplicateAge() > 0L && postedTime > 0L && postedTime + getMaxDuplicateAge() < System.currentTimeMillis()) {
						// Must fail this because duplicate detection only works up to 90 minutes
						log.warn("Sale was posted more than " + (getMaxDuplicateAge() / 60000.0) + " minutes ago; Duplicate detection won't work - failing sale");
						populateFailedResults(null, step, index, DeniedReason.INTERNAL_TIMEOUT, "Manual duplicate detection needed - Sale was posted more than " + (getMaxDuplicateAge() / 60000.0) + " minutes ago");
						return FAILED_REQUEST_PROCESSOR;
					} else if(shouldAdjustAuthorization(step, index, cardType, amount, approvedAmount)) {
						TandemMsg adjustRequest;
						try {
							adjustRequest = createRequest(AuthorityAction.ADJUSTMENT, AuthorityAction.AUTHORIZATION, taskInfo, index, cardType, accountData);
						} catch(ValueMissingException e) {
							log.warn("Missing value in attributes: " + step.getAttributes(), e);
							populateDeclinePermanentResults(action, step, index, DeniedReason.MISSING_INPUT, e.getMessage());
							return DECLINED_PERMANENT_REQUEST_PROCESSOR;
						} catch(ValueInvalidException e) {
							log.warn("Invalid value in attributes: " + step.getAttributes(), e);
							populateDeclinePermanentResults(action, step, index, DeniedReason.INVALID_INPUT, e.getMessage());
							return DECLINED_PERMANENT_REQUEST_PROCESSOR;
						} catch(ISOException | ServiceException e) {
							log.warn("Could not create request", e);
							populateFailedResults(action, step, index, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, e.getMessage());
							return FAILED_REQUEST_PROCESSOR;
						} catch(AttributeConversionException e) {
							log.warn("Could not create request", e);
							populateFailedResults(null, step, index, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Data Conversion Error of '" + e.getAttributeKey() + "' in " + getClass().getSimpleName());
							return FAILED_REQUEST_PROCESSOR;
						}
						final ISORequest isoAdjustRequest = new ISORequest(adjustRequest);
						mux.queue(isoAdjustRequest);
						return new RequestProcessor() {
							protected AuthResultCode saleResultCd;
							protected ISORequest isoSaleRequest;
							protected TimeoutException timeoutException;

							@Override
							public void prepare(int timeout) {
								AuthResultCode adjustResultCd;
								try {
									// In most cases a failed adjustment should not block the sale
									adjustResultCd = retrieveResult(isoAdjustRequest, AuthorityAction.ADJUSTMENT, action, taskInfo, index, timeout);
								} catch(TimeoutException e) {
									log.warn("Auth adjustment request timed out");
									adjustResultCd = AuthResultCode.FAILED;
								}
								switch(adjustResultCd) {
									case APPROVED:
										step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTH_ADJUSTED, index, true);
										break;
									case DECLINED:
									case DECLINED_PERMANENT:
										step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTH_ADJUSTED, index, false);
										break;
									default:
										step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTH_ADJUSTED, index, false);
										log.warn("Auth adjustment request failed with result code {0}, but continuing to process the sale...", adjustResultCd);
										break;
								}
								TandemMsg saleRequest;
								try {
									saleRequest = createRequest(action, null, taskInfo, index, cardType, accountData);
									isoSaleRequest = new ISORequest(saleRequest);
									mux.queue(isoSaleRequest);
								} catch(ValueMissingException e) {
									log.warn("Missing value in attributes: " + step.getAttributes(), e);
									saleResultCd = populateDeclinePermanentResults(action, step, index, DeniedReason.MISSING_INPUT, e.getMessage());
								} catch(ValueInvalidException e) {
									log.warn("Invalid value in attributes: " + step.getAttributes(), e);
									saleResultCd = populateDeclinePermanentResults(action, step, index, DeniedReason.INVALID_INPUT, e.getMessage());
								} catch(ISOException | ServiceException e) {
									log.warn("Could not create request", e);
									saleResultCd = populateFailedResults(action, step, index, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, e.getMessage());
								} catch(AttributeConversionException e) {
									log.warn("Could not create request", e);
									saleResultCd = populateFailedResults(null, step, index, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Data Conversion Error of '" + e.getAttributeKey() + "' in " + getClass().getSimpleName());
								}
							}

							@Override
							public AuthResultCode getResult(int timeout) throws TimeoutException, ServiceException {
								// TODO: Figure out how to handle when an Auth Adjustment succeeds but a sale fails.
								if(timeoutException != null)
									throw timeoutException;
								if(saleResultCd == null)
									saleResultCd = retrieveResult(isoSaleRequest, action, null, taskInfo, index, timeout);
								return saleResultCd;
							}
						};
					}
					break;
				case REFUND:
					if(getMaxDuplicateAge() > 0L && taskInfo.getEnqueueTime() > 0L && taskInfo.getEnqueueTime() + getMaxDuplicateAge() < System.currentTimeMillis()) {
						// Must fail this because duplicate detection only works up to 90 minutes
						log.warn("Refund was posted more than " + (getMaxDuplicateAge() / 60000.0) + " minutes ago; Duplicate detection won't work - failing refund");
						populateFailedResults(null, step, index, DeniedReason.INTERNAL_TIMEOUT, "Manual duplicate detection needed - Refund was posted more than " + (getMaxDuplicateAge() / 60000.0) + " minutes ago");
						return FAILED_REQUEST_PROCESSOR;
					}
					break;
				case EMV_PARAMETER_DOWNLOAD:
					return emvParameterDownloadProcess(taskInfo, action, index);
				default:
					throw new RetrySpecifiedServiceException("Action type '" + action + "' NOT implemented", WorkRetryType.NO_RETRY);
			}
		} catch(AttributeConversionException e) {
			log.error("Could not convert '" + e.getAttributeKey() + "' for request", e);
			populateFailedResults(null, step, index, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Data Conversion Error of '" + e.getAttributeKey() + "' in " + getClass().getSimpleName());
			return FAILED_REQUEST_PROCESSOR;
		}
		TandemMsg request;
		try {
			request = createRequest(action, null, taskInfo, index, cardType, accountData);
		} catch(ValueMissingException e) {
			log.warn("Missing value in attributes: " + step.getAttributes(), e);
			populateDeclinePermanentResults(action, step, index, DeniedReason.MISSING_INPUT, e.getMessage());
			return DECLINED_PERMANENT_REQUEST_PROCESSOR;
		} catch(ValueInvalidException e) {
			log.warn("Invalid value in attributes: " + step.getAttributes(), e);
			populateDeclinePermanentResults(action, step, index, DeniedReason.INVALID_INPUT, e.getMessage());
			return DECLINED_PERMANENT_REQUEST_PROCESSOR;
		} catch(ISOException | ServiceException e) {
			log.warn("Could not create request", e);
			populateFailedResults(action, step, index, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, e.getMessage());
			return FAILED_REQUEST_PROCESSOR;
		} catch(AttributeConversionException e) {
			log.warn("Could not create request", e);
			populateFailedResults(null, step, index, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Data Conversion Error of '" + e.getAttributeKey() + "' in " + getClass().getSimpleName());
			return FAILED_REQUEST_PROCESSOR;
		}
		final ISORequest isoRequest = new ISORequest(request);
		mux.queue(isoRequest);
		return new RequestProcessor() {
			public void prepare(int timeout) {
			}

			public AuthResultCode getResult(int timeout) throws TimeoutException {
				return retrieveResult(isoRequest, action, null, taskInfo, index, timeout);
			}
		};
	}

	protected RequestProcessor emvParameterDownloadProcess(MessageChainTaskInfo taskInfo, AuthorityAction action, int index) {
		RequestProcessor requestProcessor = FAILED_REQUEST_PROCESSOR;
		Resource resource = null;
		OutputStream out = null;
		try {
			MessageChainStep step = taskInfo.getStep();
			TandemMsg request;
			ISORequest isoRequest;
			AuthResultCode arc = AuthResultCode.FAILED;
			
			resource = resourceFolder.getResource(EMVUtils.emvResourceName(), ResourceMode.CREATE);
			out = resource.getOutputStream();
			
			EMVUtils.DownloadRequestStatus drs = EMVUtils.DownloadRequestStatus.INITIAL;
			String retrievalReferenceNumber = EMVUtils.initialRetrievalReferenceNumber();

			int hostResponseTimeout = step.getAttribute(AuthorityAttrEnum.ATTR_RESPONSE_TIME_OUT, Integer.class, true) * 1000;
			boolean doneRequestingParameters = false;
			do {
				request = createEMVParameterDownloadRequest(step, retrievalReferenceNumber, drs.getValue(), index);
				isoRequest = new ISORequest(request);
				mux.queue(isoRequest);
				TandemMsg response = retrieveResponse(isoRequest, hostResponseTimeout);
				arc = processResponse(request, response, action, null, taskInfo, index);
				if (arc == AuthResultCode.APPROVED) {
					String responseStatusInd = processEMVParameterDownloadResponse(response, out);
					if (responseStatusInd.equals(EMVUtils.ResponseStatusInd.MORE_TO_FOLLOW.getValue())) {
						retrievalReferenceNumber = response.getRetrievalReferenceNumber();
						drs = EMVUtils.DownloadRequestStatus.SUBSEQUENT;
					} else if (responseStatusInd.equals(EMVUtils.ResponseStatusInd.FINAL_BUFFER.getValue())) {
						if (EMVUtils.DownloadRequestStatus.DOWNLOAD_SUCCESSFUL.equals(drs)) {
							step.setResultAttribute(AuthorityAttrEnum.ATTR_EMV_PARM_DNDL_ID, step.getAttribute(AuthorityAttrEnum.ATTR_EMV_PARM_DNDL_ID, Long.class, true));
							step.setResultAttribute(CommonAttrEnum.ATTR_RESOURCE, resource.getKey());
							// finished requesting parameters.  end loop and return all good.
							doneRequestingParameters = true;
							requestProcessor = APPROVED_REQUEST_PROCESSOR;
						} else {
							retrievalReferenceNumber = response.getRetrievalReferenceNumber();
							drs = EMVUtils.DownloadRequestStatus.DOWNLOAD_SUCCESSFUL;
						}
					}
				} else {
					// just stop as the response code is not good!
					doneRequestingParameters = true;
				}
			} while (!doneRequestingParameters);
			log.warn("EMV_PARAMETER_DOWNLOAD finished processing loop");

		} catch (ISOException | IOException | AttributeConversionException | TimeoutException e) {
			log.warn("Unable to download emv parameters: " + e);
		} finally {
			if (out != null) {
				try {
					out.flush();
					out.close();
				} catch (IOException e) {
				}
			}
			if (!requestProcessor.equals(APPROVED_REQUEST_PROCESSOR) && resource != null) {
				// throw it away if there was a failure
				resource.delete();
			} else if (resource != null) {
				resource.release();
			}
		}
		return requestProcessor;
	}
	
	protected AuthResultCode populateFailedResults(AuthorityAction action, MessageChainStep step, int index, DeniedReason deniedReason, String errorText, Object... params) {
		AuthResultCode authResultCode = AuthResultCode.FAILED;
		step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, index, authResultCode.getValue());
		if(deniedReason != null)
			step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, index, deniedReason);
		step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, index, deniedReason == null ? "--" : deniedReason.toString());
		step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, index, errorText);
		setClientText(step, index, authResultCode, deniedReason, params);
		return authResultCode;
	}

	protected AuthResultCode populateDeclinePermanentResults(AuthorityAction action, MessageChainStep step, int index, DeniedReason deniedReason, String errorText, Object... params) {
		AuthResultCode authResultCode = AuthResultCode.DECLINED_PERMANENT;
		step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, index, authResultCode.getValue());
		if(deniedReason != null)
			step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, index, deniedReason);
		step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, index, deniedReason == null ? "--" : deniedReason.toString());
		step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, index, errorText);
		setClientText(step, index, authResultCode, deniedReason, params);
		return authResultCode;
	}
	
	protected AuthResultCode populateDeclineResults(AuthorityAction action, MessageChainStep step, int index, DeniedReason deniedReason, String errorText, Object... params) {
		AuthResultCode authResultCode = AuthResultCode.DECLINED;
		step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, index, authResultCode.getValue());
		if(deniedReason != null)
			step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, index, deniedReason);
		step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, index, deniedReason == null ? "--" : deniedReason.toString());
		step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, index, errorText);
		setClientText(step, index, authResultCode, deniedReason, params);
		return authResultCode;
	}

	protected String confirmCardType(String cardType, String pan) {
		if(StringUtils.isBlank(pan))
			return cardType;
		if(!StringUtils.isBlank(cardType)) {
			Pattern p = CARD_TYPE_PATTERNS.get(cardType.toUpperCase());
			if(p != null && p.matcher(pan).matches())
				return cardType;
			cardType = "Unknown";
		}
		for(Map.Entry<String, Pattern> entry : CARD_TYPE_PATTERNS.entrySet())
			if(entry.getValue().matcher(pan).matches())
				return entry.getKey();
		return cardType;
	}

	protected static final Pattern CARD_TYPE_PATTERN = Pattern.compile("; CardType=(\\w+)(?:;|$)");
	protected boolean shouldAdjustAuthorization(MessageChainStep step, int index, String cardType, int amount, int approvedAmount) throws AttributeConversionException {
		if(approvedAmount <= 0)
			return false;
		if(approvedAmount == amount)
			return false;
		String merchantCd = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_MERCHANT_CD, index, String.class, true);
		if(approvedAmount < amount && !authPartiallyReversed.doWhenGreater(merchantCd))
			return false;
		if(approvedAmount > amount && !authPartiallyReversed.doWhenLess(merchantCd))
			return false;
		
		String miscData = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, index, String.class, false);
		Matcher matcher;
		String cardTypeCd;
		if(!StringUtils.isBlank(miscData) && (matcher = CARD_TYPE_PATTERN.matcher(miscData)).find())
			cardTypeCd = matcher.group(1);
		else if(!StringUtils.isBlank(cardType))
			cardTypeCd = findCardTypeCdFromDescription(cardType);
		else
			cardTypeCd = "";

		if(!authPartiallyReversed.isCardTypeSupported(cardTypeCd))
			return false;

		// Not supported in Canada or the US Territories
		if(authPartiallyReversed.getSupportedCountryCount() > 0) {
			String countryCd = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_COUNTRY_CD, index, String.class, false);
			if(StringUtils.isBlank(countryCd) || !authPartiallyReversed.isSupportedCountry(countryCd))
				return false;
			if("US".equalsIgnoreCase(countryCd)) {
				String stateCd = trim(step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_STATE_CD, index, String.class, false), 3, REMOVE_REGEX_AN);
				if(!StringUtils.isBlank(stateCd) && usTerritories.contains(stateCd) && !authPartiallyReversed.isSupportedCountry(stateCd))
					return false;
			}
		}
		// TODO: we may need to not do this for certain Brand & Industry combinations. See PNS Spec p.151
		return true;
	}

	protected String findCardTypeCdFromDescription(String cardType) {
		for(Map.Entry<String, String> entry : cardTypeDescriptions.entrySet())
			if(entry.getValue().equalsIgnoreCase(cardType))
				return entry.getKey();
		return "";
	}

	private void validateExpirationDate(String expDateStr, long authTime) throws ValueInvalidException {
		Date expDate;
		try {
			expDate = EXP_DATE_FORMAT.parse(expDateStr);
		} catch(ParseException e) {
			throw new ValueException.ValueInvalidException("Expiration Date is not in a valid format", PaymentMaskBRef.EXPIRATION_DATE.attribute.getValue(), expDateStr, e);
		}
		Calendar cal = EXP_DATE_FORMAT.getCalendar();
		cal.setTime(expDate);
		cal.add(Calendar.MONTH, 1);
		if(cal.getTimeInMillis() < authTime) {
			Locale locale = Locale.getDefault();
			cal.add(Calendar.MONTH, -1);
			throw new ValueException.ValueInvalidException("Card has expired as of " + cal.getDisplayName(Calendar.MONTH, Calendar.LONG, locale) + ' ' + cal.get(Calendar.YEAR), PaymentMaskBRef.EXPIRATION_DATE.attribute.getValue(), expDate);
		}
	}
	
	/**
	 * createEMVParameterDownloadRequest
	 * 
	 * @param cardAcceptorTerminalId - comes from step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_TERMINAL_CD, index, String.class, true);
	 * Does not need the split since this came from when the download bit was set.
	 * @param cardAcquirerId - comes from request.setCardAcceptorAcquirerId(step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_MERCHANT_CD, index, String.class, true));
	 * @param systemsTraceAuditNumber
	 * @param retrievalReferenceNumber
	 * @param downloadRequestStatus
	 * @return
	 * @throws ISOException
	 * @throws AttributeConversionException 
	 * 
	 * 
	 * jms notes: MID = merchant id, TID = terminal id, DID = device id
	 * 
	 * Retreive Merchant Code and Terminal Id
	 * 
	 * 		// Control MID, TID and DID as required
		request.setCardAcceptorAcquirerId(step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_MERCHANT_CD, index, String.class, true));
		String terminalCd = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_TERMINAL_CD, index, String.class, true);
		String[] terminalCdParts = StringUtils.split(terminalCd, '|', 1);
		if(terminalCdParts.length == 1) {
			request.setCardAcceptorTerminalId("001");
			request.setDeviceId(terminalCdParts[0]);
		} else {
			request.setCardAcceptorTerminalId(terminalCdParts[0]);
			request.setDeviceId(terminalCdParts[1]);
		}
	 * 
	 */
	// downloadRequestStatus = I = initial download, S = subsequent download request, Y = download successful, N = download failed
	protected TandemMsg createEMVParameterDownloadRequest(MessageChainStep step, String retrievalReferenceValue, String downloadRequestStatus, int index) throws ISOException, AttributeConversionException {
		TandemMsg request = new TandemMsg();
		request.setHeader(OUTGOING_TANDEM_HEADER);
		// bit 0
		request.setMTI("1340");																			
		
		/*
			pg. 179
			Positions 1 - 2, Transaction Type 		57
			Positions 3 - 4, Account Type (From) 	00
			Positions 5 - 6, Account Type (To) 		00 
 		 */
		// bit 3
		request.setProcessingCode("570000");
		
		Long systemTraceAuditNum = step.getAttribute(AuthorityAttrEnum.ATTR_SYSTEM_TRACE_AUDIT_NUM, Long.class, true);
		// bit 11 - remember the spreadsheet
		String systemTraceAuditNumStr = formatSystemTraceAuditNumber(systemTraceAuditNum.intValue());
		request.setSystemTraceAuditNumber(systemTraceAuditNumStr);
		
		// bit 37
		//String retrievalReferenceValue = step.getAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, String.class, true);
		request.setRetrievalReferenceNumber(retrievalReferenceValue);
		
		// bit 41
		request.setCardAcceptorTerminalId(step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_TERMINAL_CD, index, String.class, true));														

		// bit 42
		request.setCardAcceptorAcquirerId(step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_MERCHANT_CD, index, String.class, true));														
		
		// bit 48.C7	remember the spreadsheet
//		String downloadRequestStatus = step.getAttribute(CommonAttrEnum.ATTR_EMV_PARAMETER_DWNLD_REQUEST_STATUS, String.class, true); 
		request.setEMVPOSParameterDownloadRequestStatus(downloadRequestStatus);

		// These are the unspecified values since across multiple devices so may be too complex
		// to specify per device
		// bit 60.A1 - Chase Paymentech Defined Point of Service
		request.setAttendedIndicator("01");
		request.setLocationIndicator("00");
		request.setCardholderAttendence("00");
		request.setCardPresentIndicator("0");
		request.setCATIndicator("02");

		request.setEntryCapability("07");
		
		return request;
	}
	
	protected TandemMsg createRequest(AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo, int index, String cardType, OptionallyIndexedAccountData accountData) throws ValueMissingException, ISOException, ValueInvalidException, ServiceException, AttributeConversionException {
		MessageChainStep step = taskInfo.getStep();
		TandemMsg request = new TandemMsg();
		request.setHeader(OUTGOING_TANDEM_HEADER);
		// populate request
		String pan = accountData.getPartData(PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER);
		if(StringUtils.isBlank(pan))
			throw new ValueException.ValueMissingException("Primary Account Number not provided", PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER.attribute.getValue());
		if(!CreditCardInfo.validateChecksum(pan))
			throw new ValueException.ValueInvalidException("Primary Account Number does not pass MOD10 check", PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER.attribute.getValue(), pan);
		request.setPrimaryAccountNumber(pan);
		String approvalCode = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD, index, String.class, false);
		PosEnvironment posEnv = step.getOptionallyIndexedAttributeDefault(AuthorityAttrEnum.ATTR_POS_ENVIRONMENT, index, PosEnvironment.class, PosEnvironment.UNSPECIFIED);
		String entryCapability = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_ENTRY_CAPABILITY, index, String.class, false);
		int amount = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_AMOUNT, index, Integer.class, true);
		EntryType entryType;
		boolean masterCard = "MasterCard".equalsIgnoreCase(cardType);
		boolean americanExpress = "American Express".equalsIgnoreCase(cardType);
		switch(action) {
			case AUTHORIZATION:
				request.setMTI("1100");
				request.setProcessingCode("009100");
				entryType = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_ENTRY_TYPE, index, EntryType.class, true);
				boolean allowPartialAuth = step.getOptionallyIndexedAttributeDefault(AuthorityAttrEnum.ATTR_PARTIAL_AUTH_ALLOWED, index, Boolean.class, false);
				if(allowPartialAuth)
					request.setEnhancedAuthRequestIndicator("11");
				else
					request.setEnhancedAuthRequestIndicator("01");
				if(masterCard)
					request.setPreAuthIndicator("P");
				break;
			case SALE:
				request.setMTI("1200");
				request.setProcessingCode("179100");
				entryType = EntryType.MANUAL;
				break;
			case REFUND:
				request.setMTI("1200");
				request.setProcessingCode("209100");
				entryType = EntryType.MANUAL;
				posEnv = PosEnvironment.ATTENDED;
				entryCapability = "M";
				break;
			case REVERSAL:
				request.setMTI("1420");
				switch(subAction) {
					case AUTHORIZATION:
						request.setProcessingCode("009100");
						entryType = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_ENTRY_TYPE, index, EntryType.class, true);
						approvalCode = null;
						if(amount == 0) {
							amount = step.getOptionallyIndexedAttributeDefault(AuthorityAttrEnum.ATTR_AUTH_AMOUNT, index, Integer.class, amount);
							if(amount == 0)
								amount = step.getOptionallyIndexedAttributeDefault(AuthorityAttrEnum.ATTR_AMT_APPROVED, index, Integer.class, amount);
						}
						request.setOriginalTransactionData("1100        000000000000                      ");
						break;
					default:
						throw new ServiceException("Reversal of action " + subAction + " is not supported by " + this);
				}
				break;
			case ADJUSTMENT:
				switch(subAction) {
					case AUTHORIZATION:
						request.setMTI("1100");
						request.setProcessingCode("009100");
						entryType = EntryType.MANUAL;
						request.setAuthorizationType("R");
						break;
					default:
						throw new ServiceException("Adjustment of action " + subAction + " is not supported by " + this);
				}
				break;
			default:
				throw new ServiceException("Action " + action + " is not supported by " + this);
		}
		long authTime = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_AUTH_TIME, index, Long.class, true);
		String expDateStr = accountData.getPartData(PaymentMaskBRef.EXPIRATION_DATE);
		if(action == AuthorityAction.AUTHORIZATION && !StringUtils.isBlank(expDateStr))
			validateExpirationDate(expDateStr, authTime);
		//USAT-1244 Reversals must be submitted with only the account number and expiration date 
		if(!StringUtils.isBlank(accountData.getTrackData()) && action != AuthorityAction.REVERSAL)
			request.setTrack2Data(accountData.getTrackData().length() > 37 ? accountData.getTrackData().substring(0, 37) : accountData.getTrackData());
		else if(!StringUtils.isBlank(expDateStr))
			request.setExpirationDate(expDateStr);

		if(!StringUtils.isBlank(getDeclineCvvResults())) {
			String cvv2 = trim(accountData.getPartData(PaymentMaskBRef.DISCRETIONARY_DATA), 4, REMOVE_REGEX_POSTAL);
			if(!StringUtils.isBlank(cvv2)) {
				if(cvv2.length() < 3)
					throw new ValueException.ValueInvalidException("CVV must be at least 3 digits", PaymentMaskBRef.DISCRETIONARY_DATA.attribute.getValue(), MessageResponseUtils.maskFully(cvv2));
				request.setCvvData(cvv2);
			}
		}
		String avsPostal = trim(accountData.getPartData(PaymentMaskBRef.ZIP_CODE), 9, REMOVE_REGEX_POSTAL);
		if(!StringUtils.isBlank(avsPostal))
			request.setAvsPostal(avsPostal);
		String avsAddress = trim(accountData.getPartData(PaymentMaskBRef.ADDRESS), 20, REMOVE_REGEX_AN);
		if(!StringUtils.isBlank(avsAddress))
			request.setAvsAddress(avsAddress);
		request.setTransactionAmount(String.valueOf(amount));

		String timeZoneGuid = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_TIMEZONE_GUID, index, String.class, true);
		Date localTranDate = ConvertUtils.getDateUTC(ConvertUtils.getLocalTime(authTime, timeZoneGuid));
		request.setTransactionLocalTime(formatDate(localTranDate, "HHmmss"));
		request.setTransactionLocalDate(formatDate(localTranDate, "MMddyyyy"));
		String posEntryMode;
		if (entryType == EntryType.MANUAL || entryType == EntryType.TOKEN) {
				posEntryMode = "01";
				if(entryCapability == null)
					request.setDataEntrySource("02");
				else if(EntryCapability.isCapable(entryCapability, EntryCapability.EMV_CONTACT, EntryCapability.PROXIMITY))
					request.setDataEntrySource("46");
				else if(EntryCapability.isCapable(entryCapability, EntryCapability.EMV_CONTACT, EntryCapability.EMV_PROXIMITY))
					request.setDataEntrySource("46");
				else if(EntryCapability.isCapable(entryCapability, EntryCapability.EMV_CONTACT))
					request.setDataEntrySource("54");
				else if(EntryCapability.isCapable(entryCapability, EntryCapability.PROXIMITY))
					request.setDataEntrySource("35");
				else if(EntryCapability.isCapable(entryCapability, EntryCapability.EMV_PROXIMITY))
					request.setDataEntrySource("35");
				else if(EntryCapability.isCapable(entryCapability, EntryCapability.SWIPE))
					request.setDataEntrySource("02"); // There is no code for has Mag Stripe reader but manually entered
				else
					request.setDataEntrySource("02");
		} else if (entryType == EntryType.EMV_CONTACT) {
			posEntryMode = "05";
			if(entryCapability == null)
				request.setDataEntrySource("48");
			else if(EntryCapability.isCapable(entryCapability, EntryCapability.PROXIMITY))
				request.setDataEntrySource("36");
			else if(EntryCapability.isCapable(entryCapability, EntryCapability.EMV_PROXIMITY))
				request.setDataEntrySource("36");
			else
				request.setDataEntrySource("48");
			addEmvData(request, step, index, action);
		} else if (entryType == EntryType.EMV_CONTACTLESS) {
			posEntryMode = "07";
			/*if(entryCapability == null)
				request.setDataEntrySource("41");
			else if(EntryCapability.isCapable(entryCapability, EntryCapability.EMV_CONTACT))
				request.setDataEntrySource("41");
			else
				request.setDataEntrySource("41");*/
			request.setDataEntrySource("41");
			addEmvData(request, step, index, action);
		} else if (entryType == EntryType.CONTACTLESS || entryType == EntryType.ISIS || entryType.getFallbackEntryType() == EntryType.CONTACTLESS) {
				posEntryMode = "91";
				if(entryCapability == null)
					request.setDataEntrySource("32");
				else if(EntryCapability.isCapable(entryCapability, EntryCapability.EMV_CONTACT))
					request.setDataEntrySource("43");
				else
					request.setDataEntrySource("32");
		} else if (entryType == EntryType.SWIPE) {
				posEntryMode = "90";
				if(entryCapability == null)
					request.setDataEntrySource("03");
				else if(EntryCapability.isCapable(entryCapability, EntryCapability.EMV_CONTACT, EntryCapability.PROXIMITY))
					request.setDataEntrySource("45");
				else if(EntryCapability.isCapable(entryCapability, EntryCapability.EMV_CONTACT, EntryCapability.EMV_PROXIMITY))
					request.setDataEntrySource("45");
				else if(EntryCapability.isCapable(entryCapability, EntryCapability.EMV_CONTACT))
					request.setDataEntrySource("53");
				else if(EntryCapability.isCapable(entryCapability, EntryCapability.PROXIMITY))
					request.setDataEntrySource("34");
				else if(EntryCapability.isCapable(entryCapability, EntryCapability.EMV_PROXIMITY))
					request.setDataEntrySource("34");
				else
					request.setDataEntrySource("03");
		} else {
				posEntryMode = "90";
				if(entryCapability == null)
					request.setDataEntrySource("03");
				else if(EntryCapability.isCapable(entryCapability, EntryCapability.EMV_CONTACT, EntryCapability.PROXIMITY))
					request.setDataEntrySource("45");
				else if(EntryCapability.isCapable(entryCapability, EntryCapability.EMV_CONTACT, EntryCapability.EMV_PROXIMITY))
					request.setDataEntrySource("45");
				else if(EntryCapability.isCapable(entryCapability, EntryCapability.EMV_CONTACT))
					request.setDataEntrySource("53");
				else if(EntryCapability.isCapable(entryCapability, EntryCapability.PROXIMITY))
					request.setDataEntrySource("34");
				else if(EntryCapability.isCapable(entryCapability, EntryCapability.EMV_PROXIMITY))
					request.setDataEntrySource("34");
				else
					request.setDataEntrySource("03");
		}
		
		Boolean pinEntryCapability = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_PIN_CAPABILITY, index, Boolean.class, false);
		char pinInd;
		if(pinEntryCapability == null)
			pinInd = '0';
		else if(pinEntryCapability)
			pinInd = '1';
		else
			pinInd = '2';
		
		if (action == AuthorityAction.REVERSAL) {
			//USAT-1244 Reversals are non-original transactions that must include specific DES values (Bit 48-D1) and where also Bit 22 must be set to an equivalent value as the DES
			posEntryMode = "01";
			int des = ConvertUtils.getIntSafely(request.getDataEntrySource(), -1);
			switch(des) {
				case 1:	
				case 3:
				case 4:				
					request.setDataEntrySource("02");
					break;
				case 31:
				case 32:
				case 33:
				case 34:
					request.setDataEntrySource("35");
					break;
			}
		}		
		
		// First two digits: 01 - Manual Entry; 91 - Contactless; 90 - Swipe, Third digit: 0 - Unknown; 1 - PIN capable; 2 - Not PIN Capable; Pin pad is down
		request.setPOSEntryMode(posEntryMode + pinInd);
		long traceNumber = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_TRACE_NUMBER, index, Long.class, true);		
		
		// POSConditionCode: 00 - Normal; 01 - Customer not present (manual only); 02 - Unattended Terminal; 08 - Mail Order (bit 63.M1 required); 52 - Recurring payment (Manual Only); 59 - Ecommerce (bit 63.E1 required)
		// AttendedIndicator: 00 - Attended; 01 - Unattended; 02 - No Terminal (for ecommerce)
		// LocationIndicator: 00 - On premises of card acceptor; 01 - On premises of cardholder (Home PC); 02 - No Terminal Used
		// CardholderAttendence: 00 - Cardholder present; 01 - Cardholder not present, MO/TO, Electronic Commerce; 02 - Cardholder not present, recurring payment; 03 - Cardholder not present, VRU; 04 - Cardholder not present, hotel (lodging) / auto rental authorization; 05 - Cardholder not present, PINless Debit from a VRU; 06 - Cardholder not present, PINless Debit via website; 07 - Cardholder not present, PINless Debit via call center;	08 - Cardholder not present, PINless Debit recurring payment;
		// CardPresentIndicator: 0 - Card was present; 1 - Card not present
		// CATIndicator: 00 - Not a CAT device; 01 - CAT level 1, Automated dispensing machine with online/offline PIN (MC 	Only); 02 - CAT level 2, Self service terminal, used for automated fueling transactions and unattended terminals. ; 03 - CAT level 3, limited amount terminal; 06 - CAT level 6, electronic commerce transaction (Attended Terminal Data must not be "00" if CAT level 6 is used) 
		switch(posEnv) {
			case UNATTENDED:
				request.setPOSConditionCode("02");
				request.setAttendedIndicator("01");
				request.setLocationIndicator("00");
				request.setCardholderAttendence("00");
				request.setCardPresentIndicator("0");
				request.setCATIndicator("02");
				break;
			case MOBILE:
				Float longitude = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_LONGITUDE, index, Float.class, false);
				Float latitude = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_LATITUDE, index, Float.class, false);
				if(longitude != null && latitude != null) {
					request.setGeolocationType("01");
					request.setGeolocationLongitude(GEOLOCATION_FORMAT.format(longitude));
					request.setGeolocationLatitude(GEOLOCATION_FORMAT.format(latitude));
				}
				// fall-thru
			case ATTENDED:
				request.setPOSConditionCode("00");
				request.setAttendedIndicator("00");
				request.setLocationIndicator("00");
				request.setCardholderAttendence("00");
				request.setCardPresentIndicator("0");
				request.setCATIndicator("00");
				break;
			case ECOMMERCE:
				request.setPOSConditionCode("59");
				request.setAttendedIndicator("01");
				request.setLocationIndicator("01");
				request.setCardholderAttendence("01");
				request.setCardPresentIndicator("1");
				request.setCATIndicator("06");
				if(action != AuthorityAction.REVERSAL) {
					String invoiceNumber = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_INVOICE_NUMBER, index, String.class, false);
					if(StringUtils.isBlank(invoiceNumber) || StringUtils.isBlank((invoiceNumber = invoiceNumber.replaceAll("[^A-Za-z0-9./,$@&-]", ""))))
						invoiceNumber = String.valueOf(traceNumber);
					if(invoiceNumber.length() > 16)
						invoiceNumber = invoiceNumber.substring(invoiceNumber.length() - 16, invoiceNumber.length());
					request.setEcommerceOrderNumber(invoiceNumber);
					request.setEcommerceSecurityIndicator("07");
					// request.setEcommerceGoodsIndicator("P"); // NOTE: We don't know whether there were physical goods (P) or digital goods (D) sold
				}
				break;
			case MOTO:
				request.setPOSConditionCode("08");
				request.setAttendedIndicator("02");
				request.setLocationIndicator("02");
				request.setCardholderAttendence("01");
				request.setCardPresentIndicator("1");
				request.setCATIndicator("00");
				if(action != AuthorityAction.REVERSAL) {
					String invoiceNumber = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_INVOICE_NUMBER, index, String.class, false);
					if(StringUtils.isBlank(invoiceNumber) || StringUtils.isBlank((invoiceNumber = invoiceNumber.replaceAll("[^A-Za-z0-9./,$@&-]", ""))))
						invoiceNumber = String.valueOf(traceNumber);
					if(invoiceNumber.length() > 9)
						invoiceNumber = invoiceNumber.substring(invoiceNumber.length() - 9, invoiceNumber.length());
					request.setMotoOrderNumber(invoiceNumber);
					request.setMotoTypeIndicator("1"); // 1 = Single purchase; 2 = Recurring Billing Transaction; 3= Installment Transaction
				}
				break;
			case UNSPECIFIED:
				request.setPOSConditionCode("02");
				request.setAttendedIndicator("01");
				request.setLocationIndicator("00");
				request.setCardholderAttendence("00");
				request.setCardPresentIndicator("0");
				request.setCATIndicator("02");
				break;
		}

		switch(action) {
			case ADJUSTMENT:
				if(subAction == AuthorityAction.AUTHORIZATION) {
					request.setPOSConditionCode("01");
					request.setAttendedIndicator("02"); // no terminal?
					request.setLocationIndicator("02");
					request.setCardholderAttendence("00");
					request.setCardPresentIndicator("0");
					request.setCATIndicator("00"); // or 06
					
					// Per Chase: Partial Reversals with EDS 35, change POS Condition Code to 00, 9/26/16, pwc
					if(EntryCapability.isCapable(entryCapability, EntryCapability.PROXIMITY) || EntryCapability.isCapable(entryCapability, EntryCapability.EMV_PROXIMITY))
						request.setPOSConditionCode("00");
				}
		}

		// EntryCapability: 00 - Magnetic stripe reader and key entry; 01 - Magnetic stripe reader, key entry and chip reader (Data Entry Source	Values = 47-54, 57); 02 - Magnetic stripe reader only (e.g. CAT); 03 - Magnetic stripe reader and chip reader only; 04 - Optical character recognition; 05 - Key entry only (electronic commerce, Direct Marketing/MOTO); 06 - Contactless Magnetic Stripe entry (Required for Amex Express Pay qualification).  (Data Entry Source Values = 31-35, 55);	07 - Magnetic stripe reader, key entry, chip reader and RFID reader (Data Entry Source Values = 36-46, 57)
		switch(posEnv) {
			case ECOMMERCE: case MOTO:
				request.setEntryCapability("05");
				break;
			case UNATTENDED:
				// keyed is not allowed
				if(EntryCapability.isCapable(entryCapability, EntryCapability.EMV_CONTACT, EntryCapability.SWIPE))
					request.setEntryCapability("03");
				else if (entryType == EntryType.EMV_CONTACTLESS)
					request.setEntryCapability("07");
				else if (entryType == EntryType.EMV_CONTACT) {
					if ("48".equalsIgnoreCase(request.getDataEntrySource()))
						request.setEntryCapability("01");
					else
						request.setEntryCapability("07");
				} else if(EntryCapability.isCapable(entryCapability, EntryCapability.PROXIMITY) || entryType == EntryType.CONTACTLESS || entryType == EntryType.ISIS || entryType.getFallbackEntryType() == EntryType.CONTACTLESS)
					request.setEntryCapability("06");
				else if(EntryCapability.isCapable(entryCapability, EntryCapability.SWIPE) || entryType == EntryType.SWIPE)
					request.setEntryCapability("02");
				else if(EntryCapability.isCapable(entryCapability, EntryCapability.KEYED))
					throw new AttributeConversionException(AuthorityAttrEnum.ATTR_ENTRY_CAPABILITY, new ConvertException("Unattended POS must have more than KEYED Entry Capability", EntryCapability.class, entryCapability));
				else
					throw new AttributeConversionException(AuthorityAttrEnum.ATTR_ENTRY_CAPABILITY, new ConvertException("Entry Capabilities not provided", EntryCapability.class, entryCapability));
				break;
			default:
				if(EntryCapability.isCapable(entryCapability, EntryCapability.EMV_CONTACT, EntryCapability.PROXIMITY, EntryCapability.SWIPE, EntryCapability.KEYED) || entryType == EntryType.EMV_CONTACTLESS)
					request.setEntryCapability("07");
				else if (entryType == EntryType.EMV_CONTACT) {
					if ("48".equalsIgnoreCase(request.getDataEntrySource()))
						request.setEntryCapability("01");
					else
						request.setEntryCapability("07");
				} else if(EntryCapability.isCapable(entryCapability, EntryCapability.EMV_CONTACT, EntryCapability.SWIPE, EntryCapability.KEYED))
					request.setEntryCapability("01");
				else if(EntryCapability.isCapable(entryCapability, EntryCapability.EMV_CONTACT, EntryCapability.SWIPE))
					request.setEntryCapability("03");
				else if(EntryCapability.isCapable(entryCapability, EntryCapability.PROXIMITY) || entryType == EntryType.CONTACTLESS || entryType == EntryType.ISIS || entryType.getFallbackEntryType() == EntryType.CONTACTLESS)
					request.setEntryCapability("06");
				else if(EntryCapability.isCapable(entryCapability, EntryCapability.SWIPE, EntryCapability.KEYED))
					request.setEntryCapability("00");
				else if(EntryCapability.isCapable(entryCapability, EntryCapability.SWIPE) || entryType == EntryType.SWIPE)
					request.setEntryCapability("02");
				else if(EntryCapability.isCapable(entryCapability, EntryCapability.KEYED) || entryType == EntryType.MANUAL || entryType == EntryType.TOKEN)
					request.setEntryCapability("05");
				else
					throw new AttributeConversionException(AuthorityAttrEnum.ATTR_ENTRY_CAPABILITY, new ConvertException("Entry Capabilities not provided", EntryCapability.class, entryCapability));
		}
		
		request.setRetrievalReferenceNumber(formatRetrievalReferenceNumber(traceNumber));
		//Authorization ID Response must be space filled for transactions NOT processed online (i.e. return transactions)
		if(!StringUtils.isBlank(approvalCode))
			request.setApprovalCode(approvalCode);
		else if (action == AuthorityAction.REFUND)
			request.setApprovalCode("      ");
		// Control MID, TID and DID as required
		request.setCardAcceptorAcquirerId(step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_MERCHANT_CD, index, String.class, true));
		String deviceName = step.getOptionallyIndexedAttribute(MessageAttrEnum.ATTR_DEVICE_NAME, index, String.class, true);
		String terminalCd = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_TERMINAL_CD, index, String.class, true);
		String authorityName = step.getOptionallyIndexedAttributeDefault(AuthorityAttrEnum.ATTR_AUTHORITY_NAME, index, String.class, "Tandem");
		boolean operatorMOR = authorityName.endsWith("Operator MOR");
		String[] terminalCdParts = StringUtils.split(terminalCd, '|', 1);
		String terminalId = terminalCdParts[0];
		request.setCardAcceptorTerminalId(terminalId);
		if(terminalCdParts.length == 1)
			request.setDeviceId(deviceName.substring(deviceName.length() - 4, deviceName.length()));
		else
			request.setDeviceId(terminalCdParts[1]);
		request.setMerchantCategoryCode(StringUtils.pad(String.valueOf(step.getOptionallyIndexedAttributeDefault(AuthorityAttrEnum.ATTR_MERCHANT_CATEGORY_CODE, index, Short.class, (short) 5999)), '0', 4, Justification.RIGHT));
		int priorAttempts = step.getOptionallyIndexedAttributeDefault(AuthorityAttrEnum.ATTR_PRIOR_ATTEMPTS, index, Integer.class, 0);
		if (action == AuthorityAction.AUTHORIZATION || action == AuthorityAction.SALE) {
			// 00 except when retry, then 02		
			if(priorAttempts > 0 || taskInfo.isRedelivered() || taskInfo.getRetryCount() > 0)
				request.setDuplicateCheckingIndicator("02");
			else
				request.setDuplicateCheckingIndicator("00");
			if (operatorMOR) {
				String softDescriptor = trim(step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_DOING_BUSINESS_AS, index, String.class, true), 25, REMOVE_REGEX_AN);
				request.setSoftDescriptor(softDescriptor);
			}
		}

		StringBuilder globalTranCd = new StringBuilder(30);
		globalTranCd.append(deviceName).append('-').append(step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_DEVICE_TRAN_CD, index, String.class, true).replaceAll(REMOVE_REGEX_AN, "-"));
		request.setCustomerDefinedData(globalTranCd.toString());
				
		//USAT-1244 Results of Canadian Retail Regression testing. Retail 63.R2 cannot be in the message with Ecommerce 63.E1 or MOTO 63.M1, causes ERR PROC FIELD.
		if (action != AuthorityAction.REVERSAL && posEnv != PosEnvironment.ECOMMERCE && posEnv != PosEnvironment.MOTO) {
			String invoiceNumber = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_INVOICE_NUMBER, index, String.class, false);
			if(invoiceNumber != null)
				invoiceNumber = invoiceNumber.replaceAll("[^0-9]", "");
			if(StringUtils.isBlank(invoiceNumber))
				invoiceNumber = String.valueOf(traceNumber);
			if(invoiceNumber.length() > 6)
				invoiceNumber = invoiceNumber.substring(invoiceNumber.length() - 6, invoiceNumber.length());
			else if (invoiceNumber.length() < 6)
				invoiceNumber = StringUtils.pad(invoiceNumber, '0', 6, Justification.RIGHT);
			request.setRetailIndustryDataInvoiceNumber(invoiceNumber);
			String tranInformation = globalTranCd.toString();
			if(tranInformation.length() > 20)
				tranInformation = tranInformation.substring(tranInformation.length() - 20, tranInformation.length());
			else if (tranInformation.length() < 20)
				tranInformation = StringUtils.pad(tranInformation, ' ', 20, Justification.RIGHT);
			request.setRetailIndustryDataTranInformation(tranInformation);
		}

		String currencyCd = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_CURRENCY_CD, index, String.class, true);
		if (!"CAD".equalsIgnoreCase(currencyCd) && !operatorMOR) {
			//Chase doesn't support Bit 62.AG, Sub Merchant Data, for Canadian MIDs yet, and it cannot be sent for Submitter MIDs
			String dba;
			if(StringUtils.isBlank(getDoingBusinessAsPrefix()))
				dba = trim(step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_DOING_BUSINESS_AS, index, String.class, true), 25, REMOVE_REGEX_AN);
			else if(getDoingBusinessAsPrefix().length() >= 25)
				dba = trim(getDoingBusinessAsPrefix(), 25, null);
			else
				dba = getDoingBusinessAsPrefix() + trim(step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_DOING_BUSINESS_AS, index, String.class, true), 25 - getDoingBusinessAsPrefix().length(), REMOVE_REGEX_AN);
			request.setDoingBusinessAs(dba);
			Long customerId = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_CUSTOMER_ID, index, Long.class, false);
			if (masterCard) {
				// USAT-797 Set Sub Merchant ID to TID for MasterCard
				request.setCustomerId(terminalId);
			} else if(customerId != null)
				request.setCustomerId(String.valueOf(customerId));
			else if(getDefaultCustomerId() != null)
				request.setCustomerId(String.valueOf(getDefaultCustomerId()));
			String street = trim(step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_ADDRESS, index, String.class, false), 25, REMOVE_REGEX_AN);
			if(!StringUtils.isBlank(street))
				request.setStreet(street);
			String city = trim(step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_CITY, index, String.class, false), 15, REMOVE_REGEX_AN);
			if(!StringUtils.isBlank(city))
				request.setCity(city);
			// Puerto Rico should use PR as state and 630 for country code
			String stateCd = trim(step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_STATE_CD, index, String.class, false), 3, REMOVE_REGEX_AN);
			if(!StringUtils.isBlank(stateCd))
				request.setStateCd(stateCd);
			String postal = trim(step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_POSTAL, index, String.class, false), 9, REMOVE_REGEX_POSTAL);
			if(!StringUtils.isBlank(postal))
				request.setPostal(postal);
			String countryCd = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_COUNTRY_CD, index, String.class, false);
			if(!StringUtils.isBlank(countryCd)) {
				if("US".equalsIgnoreCase(countryCd) && usTerritories.contains(stateCd))
					countryCd = stateCd;
				CountryCode cc = CountryCode.getByCode(countryCd);
				if(cc == null)
					throw new AttributeConversionException(AuthorityAttrEnum.ATTR_COUNTRY_CD, new ConvertException("Country Code '" + countryCd + "' is not recognized", CountryCode.class, countryCd));
				Currency currency = cc.getCurrency();
				if(currency == null || !currency.getCurrencyCode().equalsIgnoreCase(currencyCd.trim()))
					throw new ValueException.ValueInvalidException("The country of this device's address does not match the currency of the transaction", AuthorityAttrEnum.ATTR_COUNTRY_CD.getValue(), countryCd);
				request.setCountryNum(String.valueOf(cc.getNumeric()));
			}
			String csEmail = trim(step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_CUSTOMER_SERVICE_EMAIL, index, String.class, false), 19, REMOVE_REGEX_EMAIL);
			if(!StringUtils.isBlank(csEmail))
				request.setEmail(csEmail);
			String csPhone = trim(step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_CUSTOMER_SERVICE_PHONE, index, String.class, false), 10, REMOVE_REGEX_PHONE);
			if(!StringUtils.isBlank(csPhone))
				request.setPhone(csPhone);
			// USAT-797 Set Seller Id for MasterCard and American Express transactions
			if(masterCard || americanExpress)
				request.setSellerId(customerId == null ? "0" : String.valueOf(customerId));
		}

		request.setVendorIdentifier(getVendorIdentifier());
		request.setSoftwareIdentifier(getSoftwareIdentifier());
		request.setHardwareSerialNumber(deviceName);

		// POS/VAR Capabilities 2, USAT-1244 should indicate all the features that the payment application/gateway supports
		Bitmap192x8Field posVarCap2 = new Bitmap192x8Field();
		posVarCap2.setBit(2, 4, true);
		posVarCap2.setBit(3, 8, true);
		posVarCap2.setBit(7, 6, true);
		posVarCap2.setBit(9, 2, true); // Retail
		posVarCap2.setBit(11, 3, true);
		
		posVarCap2.setBit(9, 6, true); // E-commerce
		posVarCap2.setBit(9, 7, true); // MOTO
		posVarCap2.setBit(13, 7, true); // Mobile

		posVarCap2.setBit(4, 8, true);	// Visa or ChaseNet EMV
		posVarCap2.setBit(4, 6, true);	// MasterCard Credit EMV
		posVarCap2.setBit(4, 4, true);	// American Express EMV
		posVarCap2.setBit(4, 1, true);	// Discover EMV			

		request.setPOSCapabilities2(posVarCap2.getValue());

		log.info("Sending request: " + request);
		return request;
	}

	protected void addTerminalCdValues(TandemMsg request, MessageChainStep step, int index) throws AttributeConversionException, ISOException {
		String terminalCd = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_TERMINAL_CD, index, String.class, true);
		String[] terminalCdParts = StringUtils.split(terminalCd, '|', 1);
		if(terminalCdParts.length == 1) {
			request.setCardAcceptorTerminalId("001");
			request.setDeviceId(terminalCdParts[0]);
		} else {
			request.setCardAcceptorTerminalId(terminalCdParts[0]);
			request.setDeviceId(terminalCdParts[1]);
		}
		
	}
	
	protected void addEmvData(TandemMsg request, MessageChainStep step, int index, AuthorityAction action) throws AttributeConversionException, ISOException {
		byte [] additionalData = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_ADDITIONAL_INFO, index, byte[].class, true);
		Map<String, byte[]> tlvMap = new HashMap<String, byte[]>();
		ByteBuffer byteBuffer = ByteBuffer.wrap(additionalData);
		CardReaderType cardReaderType = step.getOptionallyIndexedAttributeDefault(CommonAttrEnum.ATTR_CARD_READER_TYPE_ID, index, CardReaderType.class, CardReaderType.IDTECH_VENDX_UNPARSED);
		EMVIdtechDecryptValueHandler valueHandler = null;
		switch (cardReaderType) {
			case IDTECH_VENDX_UNPARSED:
				valueHandler = new EMVIdtechDecryptValueHandler(byteBuffer, null, tlvMap);
				break;
			default:
				throw new ISOException("Unsupported EMV card reader: " + cardReaderType);
		}
		try {
			TLV_PARSER.parse(additionalData, 0, additionalData.length, valueHandler);
			String aid = StringUtils.toHex(tlvMap.get(TLVTag.DEDICATED_FILE_NAME.getHexValue()));
			if (!StringUtils.isBlank(aid))
				request.setEMVAID(aid);
			String panSeqNumber = StringUtils.toHex(tlvMap.get(TLVTag.PAN_SEQUENCE_NUMBER.getHexValue()));
			if(!StringUtils.isBlank(panSeqNumber))
				request.setPanSequenceNumber(panSeqNumber);
			String chipCardData = VendXParsing.extractChaseChipCardData(tlvMap, action);
			if (!StringUtils.isBlank(chipCardData))
				request.setChipCardData(chipCardData);
		} catch (IOException e) {
			throw new ISOException("Error parsing EMV data", e);
		}
	}
	
	protected String trim(String value, int maxLength, String removeRegex) {
		if(value == null)
			return null;
		value = value.trim();
		if(!StringUtils.isBlank(removeRegex))
			value = value.replaceAll(removeRegex, "");
		if(value.length() > maxLength)
			return value.substring(0, maxLength);
		return value;
	}

	protected AuthResultCode retrieveResult(ISORequest isoRequest, AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo, int index, int timeout) throws TimeoutException {
		TandemMsg response = retrieveResponse(isoRequest, timeout);
		return processResponse((TandemMsg) isoRequest.getRequest(), response, action, subAction, taskInfo, index);
	}

	protected TandemMsg retrieveResponse(ISORequest isoRequest, int timeout) throws TimeoutException {
		TandemMsg response = (TandemMsg) isoRequest.getResponse(timeout);
		if(response == null && isoRequest.isExpired())
			throw new TimeoutException("Did not receive a response in " + timeout + " ms");
		if(log.isDebugEnabled() && isoRequest.getRequest().hasField(0)) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			PrintStream ps = new PrintStream(baos);
			isoRequest.getRequest().dump(ps, "");
			ps.flush();
			log.debug(baos.toString());
		} 
		if(log.isInfoEnabled()) {
			log.info("Received response: " + response);
		}
		return response;
	}

	protected AuthResultCode processResponse(TandemMsg request, TandemMsg response, AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo, int index) throws TimeoutException {
		MessageChainStep step = taskInfo.getStep();
		// process result
		try {
			String responseCode = response.getResponseCode();
			String responseMessage = response.getHostResponseData();
			AuthResultCode authResultCode;
			DeniedReason deniedReason;
			if(responseCode == null || responseCode.length() == 0) {
				authResultCode = AuthResultCode.FAILED;
				deniedReason = DeniedReason.INVALID_RESPONSE;
			} else if(responseCode.equals(ISO8583Response.SUCCESS)) {
				authResultCode = AuthResultCode.APPROVED;
				deniedReason = null;
			} else if(responseCode.equals("99")) {
				throw new TimeoutException("Response indicates timeout; try again");
			} else if((action == AuthorityAction.SALE || action == AuthorityAction.REVERSAL) && responseCode.startsWith("GE")) {
				// return DECLINED for failed sale to put transaction in retryable TRANSACTION_INCOMPLETE state
				authResultCode = AuthResultCode.DECLINED;
				deniedReason = DeniedReason.PROCESSOR_RESPONSE;
			} else if(action == AuthorityAction.REFUND && responseCode.equals(ISO8583Response.ERROR_HOST_CONNECTION_FAILURE)) {
				// return DECLINED for refund on connection failure to put transaction in retryable TRANSACTION_INCOMPLETE state
				authResultCode = AuthResultCode.DECLINED;
				deniedReason = DeniedReason.PROCESSOR_TIMEOUT;
			} else {
				authResultCode = getAuthResultCode(responseCode, responseMessage);
				switch(authResultCode) {
					case APPROVED:
					case PARTIAL:
						deniedReason = null;
						break;
					default:
						deniedReason = getDeniedReason(responseCode, responseMessage);
				}
			}
			int approvedAmount;
			switch(authResultCode) {
				case PARTIAL:
					boolean allowPartialAuth;
					try {
						allowPartialAuth = step.getOptionallyIndexedAttributeDefault(AuthorityAttrEnum.ATTR_PARTIAL_AUTH_ALLOWED, index, Boolean.class, false);
					} catch(AttributeConversionException e) {
						log.warn("Could not convert " + AuthorityAttrEnum.ATTR_PARTIAL_AUTH_ALLOWED.toString(), e);
						allowPartialAuth = false;
					}
					if(!allowPartialAuth) {
						authResultCode = AuthResultCode.DECLINED;
						deniedReason = DeniedReason.INSUFFICIENT_FUNDS;
						approvedAmount = 0;
						break;
					}
					// fall-through
				case APPROVED:
					if(action == AuthorityAction.REVERSAL) {
						approvedAmount = 0;
						step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, index, approvedAmount);
					} else if (action == AuthorityAction.EMV_PARAMETER_DOWNLOAD) {
						// do nothing here
						approvedAmount = 0;
					} else {
						int requestedAmount = ConvertUtils.getInt(request.getTransactionAmount());
						if(!StringUtils.isBlank(response.getTransactionAmount())) {
							approvedAmount = ConvertUtils.getInt(response.getTransactionAmount());
							if(approvedAmount < requestedAmount)
								authResultCode = AuthResultCode.PARTIAL;
						} else
							approvedAmount = requestedAmount;
						step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, index, approvedAmount);
					}
					break;
				default:
					approvedAmount = 0;
			}

			// Is EMV parameter download bit set begin
			if (action == AuthorityAction.AUTHORIZATION || action == AuthorityAction.SALE) {
				String emvParameterDownloadReqd = response.getEMVParameterDownloadRequired();
				if (emvParameterDownloadReqd != null && emvParameterDownloadReqd.equals("5") || true) {
					step.setResultAttribute(AuthorityAttrEnum.ATTR_EMV_PARAMETER_DNLD_REQD_TIMESTAMP, requestStartTimestamp);
				}
			}
			// Is EMV parameter download bit set end
			
			String clientText = null;
			if(action == AuthorityAction.AUTHORIZATION) {
				StringBuilder clientTextBuilder = new StringBuilder();
				String cvvResponse = response.getCvvResponse();
				if(!StringUtils.isBlank(cvvResponse)) {
					responseCode = new StringBuilder().append(responseCode).append(";CVV:").append(cvvResponse).toString();
					if(!StringUtils.isBlank(getDeclineCvvResults()) && getDeclineCvvResults().contains(cvvResponse)) {
						switch(authResultCode) {
							case APPROVED:
							case PARTIAL:
								try {
									if(step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_ALLOW_CVV_MISMATCH, index, Boolean.class, Boolean.FALSE)) {
										authResultCode = AuthResultCode.CVV_MISMATCH;
									} else {
										authResultCode = AuthResultCode.DECLINED;
									}
								} catch(AttributeConversionException e) {
									authResultCode = AuthResultCode.DECLINED;
								}
								deniedReason = DeniedReason.CVV_MISMATCH;
						}

						clientTextBuilder.append("; CVV Mis-match");
						String desc = getCvvResultDescription(cvvResponse);
						if(!StringUtils.isBlank(desc))
							clientTextBuilder.append(": ").append(desc);
					}
				}
				String avsResponse = response.getAvsResponse();
				if(!StringUtils.isBlank(avsResponse)) {
					responseCode = new StringBuilder().append(responseCode).append(";AVS:").append(avsResponse).toString();
					if(!StringUtils.isBlank(getDeclineAvsResults()) && getDeclineAvsResults().contains(avsResponse)) {
						switch(authResultCode) {
							case APPROVED:
							case PARTIAL:
							case CVV_MISMATCH:
								try {
									if(step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_ALLOW_AVS_MISMATCH, index, Boolean.class, Boolean.FALSE)) {
										if(authResultCode == AuthResultCode.CVV_MISMATCH)
											authResultCode = AuthResultCode.CVV_AND_AVS_MISMATCH;
										else
											authResultCode = AuthResultCode.AVS_MISMATCH;
									} else {
										authResultCode = AuthResultCode.DECLINED;
									}
								} catch(AttributeConversionException e) {
									authResultCode = AuthResultCode.DECLINED;
								}
								if(deniedReason == DeniedReason.CVV_MISMATCH)
									deniedReason = DeniedReason.CVV_AND_AVS_MISMATCH;
								else
									deniedReason = DeniedReason.AVS_MISMATCH;
						}
						clientTextBuilder.append("; AVS Mis-match");
						String desc = getAvsResultDescription(avsResponse);
						if(!StringUtils.isBlank(desc))
							clientTextBuilder.append(": ").append(desc);
					}
				}
				if(clientTextBuilder.length() > 0) {
					clientText = clientTextBuilder.substring(2);
					if(!StringUtils.isBlank(responseMessage))
						responseMessage = clientTextBuilder.insert(0, responseMessage).toString();
					else if("00".equals(responseCode))
						responseMessage = clientTextBuilder.insert(0, "OK").toString();
					else
						responseMessage = clientTextBuilder.insert(0, "--").toString();
				}
			} else if(authResultCode == AuthResultCode.APPROVED)
				step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_SETTLED, index, true);

			if(action == AuthorityAction.SALE) { // check if previous request had info
				responseCode = appendAdjustmentResult(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, index, responseCode, step.getResultAttributes());
				responseMessage = appendAdjustmentResult(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, index, responseMessage, step.getResultAttributes());
			}

			step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, index, authResultCode.getValue());
			if(deniedReason != null)
				step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, index, deniedReason);
			step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, index, responseCode);
			step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, index, responseMessage);
			step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD, index, response.getApprovalCode());
			step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, index, response.getRetrievalReferenceNumber());
			step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS, index, parseDate(response.getTransactionUTCTimestamp(), "MMddyyyyHHmmss"));
			step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_PREPAID_FLAG, index, response.getEnhancedAuthResponseCardType());
			
			StringBuilder sb = new StringBuilder();
			if(!StringUtils.isBlank(response.getACI()))
				sb.append("ACI=").append(response.getACI()).append("; ");
			if(!StringUtils.isBlank(response.getTransactionId()))
				sb.append("TransactionId=").append(response.getTransactionId()).append("; ");
			if(!StringUtils.isBlank(response.getMerchantCategoryCode()))
				sb.append("MCC=").append(response.getMerchantCategoryCode()).append("; ");
			sb.append("ResponseInfo=").append(response.getAuthResponseInfo());
			if(!StringUtils.isBlank(response.getDowngradeInfo()))
				sb.append("; DowngradeInfo=").append(response.getDowngradeInfo());
			if(!StringUtils.isBlank(response.getHostErrorNumber()))
				sb.append("; HostErrorNumber=").append(response.getHostErrorNumber());
			if(!StringUtils.isBlank(response.getMCInterchangeData()))
				sb.append("; MCInterchangeData=").append(response.getMCInterchangeData());
			if(!StringUtils.isBlank(response.getEnhancedAuthResponseCardType()))
				sb.append("; Prepaid=").append(response.getEnhancedAuthResponseCardType());
			if(!StringUtils.isBlank(response.getMessageFormatErrorInformation()))
				sb.append("; FormatErrorInfo=").append(response.getMessageFormatErrorInformation().trim());

			String cardTypeCd = response.getCardType();
			if(!StringUtils.isBlank(cardTypeCd)) {
				sb.append("; CardType=").append(cardTypeCd);
				String cardTypeDesc = getCardTypeDescription(cardTypeCd);
				if(StringUtils.isBlank(cardTypeDesc))
					cardTypeDesc = "[" + cardTypeCd + "]";
				step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_CARD_TYPE, index, cardTypeDesc);
			}

			// There may be other attributes of interest, but we'll start with those above
			step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, index, sb.toString());

			//balance
			Long availableBalance;
			String tmp = response.getAccountBalanceAmount1();
			if(!StringUtils.isBlank(tmp))
				availableBalance = ConvertUtils.getLong(tmp);
			else if(!StringUtils.isBlank(tmp = response.getAccountBalanceAmount2()))
				availableBalance = ConvertUtils.getLong(tmp);
			else
				availableBalance = null;
			if(availableBalance != null) {
				if(authResultCode == AuthResultCode.PARTIAL)
					step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, index, approvedAmount);
				else
					step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, index, availableBalance.longValue() + approvedAmount);
			}
			if(action == AuthorityAction.AUTHORIZATION) {
				switch(authResultCode) {
					case APPROVED:
					case PARTIAL:
						if(approvedAmount > 0)
							step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTH_HOLD_USED, index, true);
						setClientText(step, index, authResultCode, null, null, availableBalance, null, responseMessage);
						break;
					case CVV_MISMATCH:
					case AVS_MISMATCH:
					case CVV_AND_AVS_MISMATCH:
						if(approvedAmount > 0)
							step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTH_HOLD_USED, index, true);
						setClientText(step, index, authResultCode, deniedReason, responseMessage, clientText);
						break;
					case DECLINED:
						if(deniedReason != null && approvedAmount > 0)
							switch(deniedReason) {
								case CVV_MISMATCH:
								case AVS_MISMATCH:
								case CVV_AND_AVS_MISMATCH:
									step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTH_HOLD_USED, index, true);
									break;
							}
						// fall-through
					default:
						setClientText(step, index, authResultCode, deniedReason, responseMessage);
				}
			}
			return authResultCode;
		} catch(ISOException e) {
			log.warn("Could not process response", e);
			return populateFailedResults(action, taskInfo.getStep(), index, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, e.getMessage());
		} catch(ConvertException e) {
			log.warn("Could not process response", e);
			return populateFailedResults(action, taskInfo.getStep(), index, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, e.getMessage());
		} catch(ParseException e) {
			log.warn("Could not process response", e);
			return populateFailedResults(action, taskInfo.getStep(), index, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, e.getMessage());
		}
	}


	protected String appendAdjustmentResult(AuthorityAttrEnum attribute, int index, String originalValue, Map<String, Object> resultAttributes) {
		StringBuilder sb = new StringBuilder(attribute.getValue().length() + 5);
		sb.append(attribute.getValue());
		if(index > 0)
			sb.append('.').append(index);
		String prevValue = ConvertUtils.getStringSafely(resultAttributes.get(sb.toString()));
		if(StringUtils.isBlank(prevValue))
			return originalValue;
		else {
			sb.setLength(0);
			if(originalValue != null)
				sb.append(originalValue);
			sb.append("; ").append(prevValue).append(" (adj)");
			return sb.toString();
		}
	}

	protected void writeEMVParameter(String key, String value, OutputStream out) throws IOException {
		if (value != null) {
			out.write(key.getBytes());
			out.write("=".getBytes());
			out.write(value.getBytes());
			out.write("\n".getBytes());
		}
	}
	
	protected String processEMVParameterDownloadResponse(TandemMsg response, OutputStream out) throws IOException, ISOException {
		String responseStatusIndicator = null;
		// parse out the emv parameters and place in resource file
		try {
			responseStatusIndicator = response.getEMVParameterDownloadResponseStatusIndicator();
			writeEMVParameter("keyAID", response.getEMVPublicKeyAID(), out);
			writeEMVParameter("keyIndex", response.getEMVPublicKeyIndex(), out);
			writeEMVParameter("keyModulus", response.getEMVPublicKeyModulus(), out);
			writeEMVParameter("keyExponent", response.getEMVPublicKeyExponent(), out);
			writeEMVParameter("keyChecksum", response.getEMVPublicKeyChecksum(), out);

			writeEMVParameter("fallbackIndicatorAID", response.getEMVFallbackIndicatorAID(), out);
			writeEMVParameter("fallbackIndicator", response.getEMVFallbackIndicator(), out);
			
			writeEMVParameter("offlineFloorLimitAID", response.getEMVOfflineFloorLimitAID(), out);
			writeEMVParameter("offlineFloorLimitBiasedRandomSelectionLimit", response.getEMVOffliineFloorLimitBiasedRandomSelectionLimit(), out);
			writeEMVParameter("offlineFloorLimit", response.getEMVOfflineFloorLimit(), out);
			
			out.flush();
		} finally {
		}
		return responseStatusIndicator;
	}
	
	protected void setClientText(MessageChainStep step, int index, AuthResultCode authResultCode, DeniedReason deniedReason, Object... params) {
		String prefix = getClientTextKeyPrefix();
		switch(authResultCode) {
			case APPROVED:
			case PARTIAL:
				step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, index, "client.message.auth." + prefix + ".approved");
				step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, index, params == null ? new Object[0] : params);
				break;
			default:
				step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, index, "client.message.auth." + prefix + ".denied-" + (deniedReason == null ? "UNKNOWN" : deniedReason.toString()));
				step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, index, params == null ? new Object[0] : params);
		}
	}

	protected String getOptionallyIndexedAttributeValue(Attribute attribute, int index) {
		if(index > 0)
			return attribute.getValue() + '.' + index;
		return attribute.getValue();
	}

	protected String getClientTextKeyPrefix() {
		return "tandem";
	}

	protected String formatSystemTraceAuditNumber(int systemTraceAuditNumber) {
		return StringUtils.pad(Integer.toString(systemTraceAuditNumber), '0', 6, Justification.RIGHT);
	}
	
	protected String formatRetrievalReferenceNumber(long traceNumber) {
		byte[] bytes = new byte[6];
		bytes[0] = (byte) (traceNumber >>> 40);
		bytes[1] = (byte) (traceNumber >>> 32);
		bytes[2] = (byte) (traceNumber >>> 24);
		bytes[3] = (byte) (traceNumber >>> 16);
		bytes[4] = (byte) (traceNumber >>> 8);
		bytes[5] = (byte) (traceNumber >>> 0);
		return StringUtils.toHex(bytes, 0, bytes.length);
	}

	protected Date parseDate(String dateStr, String format) throws ParseException, IllegalArgumentException {
		if(StringUtils.isBlank(dateStr))
			return null;
		return ((DateFormat) ConvertUtils.getFormat("DATE", format)).parse(dateStr);
	}

	protected String formatDate(Date date, String format) throws IllegalArgumentException {
		if(date == null)
			return null;
		return ((DateFormat) ConvertUtils.getFormat("DATE", format)).format(date);
	}

	protected AuthResultCode getAuthResultCode(String responseCode, String responseMessage) {
		AuthResultCode arcText = responseMessageResultCd.get(responseMessage);
		AuthResultCode arcCode = responseCodeResultCd.get(responseCode);
		if(arcText != null) {
			if(arcCode == null || arcCode == arcText)
				return arcText;
			// Return the combo of both
			switch(arcText) {
				case APPROVED:
					switch(arcCode) {
						case PARTIAL:
							return arcCode;
						default:
							return arcText;
					}
				case AVS_MISMATCH:
					switch(arcCode) {
						case CVV_MISMATCH:
						case CVV_AND_AVS_MISMATCH:
							return AuthResultCode.CVV_AND_AVS_MISMATCH;
						default:
							return arcText;
					}
				case CVV_MISMATCH:
					switch(arcCode) {
						case AVS_MISMATCH:
						case CVV_AND_AVS_MISMATCH:
							return AuthResultCode.CVV_AND_AVS_MISMATCH;
						default:
							return arcText;
					}
				case FAILED:
					return arcCode;
				default:
					return arcText;
			}
		} else if(arcCode != null)
			return arcCode;
		if(responseCode.startsWith("GE"))
			return AuthResultCode.FAILED;
		return AuthResultCode.DECLINED;
	}

	protected DeniedReason getDeniedReason(String responseCode, String responseMessage) {
		DeniedReason deniedReason = responseMessageDeniedReason.get(responseMessage);
		if(deniedReason != null)
			return deniedReason;
		deniedReason = responseCodeDeniedReason.get(responseCode);
		if(deniedReason != null)
			return deniedReason;
		return DeniedReason.PROCESSOR_RESPONSE;
	}

	protected boolean canSend() {
		if(mux == null)
			return false;
		if(mux.getISOChannel() == null)
			return false;
		if(mux.isTerminating())
			return false;
		if(!mux.isConnected()) {
			try {
				mux.getISOChannel().connect();
			} catch(IOException e) {
				log.warn("Could not connect mux", e);
			}
			if(!mux.isConnected())
				return false;
		}

		return true;
	}

	protected TandemMsg send(TandemMsg request, int hostResponseTimeout) throws TimeoutException {
		ISORequest isoRequest = new ISORequest(request);
		mux.queue(isoRequest);
		final int timeout = hostResponseTimeout > 0 ? hostResponseTimeout : getResponseTimeout();
		TandemMsg response = (TandemMsg) isoRequest.getResponse(timeout);
		if(response == null && isoRequest.isExpired())
			throw new TimeoutException("Did not receive a response in " + timeout + " ms");
		if(log.isDebugEnabled() && request.hasField(0)) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			PrintStream ps = new PrintStream(baos);
			request.dump(ps, "");
			ps.flush();
			log.debug(baos.toString());
		}
		return response;
	}

	protected void configLogger(LogSource logSource) {
		String name = logSource.getClass().getName();
		org.jpos.util.Logger jposLogger = new org.jpos.util.Logger();
		jposLogger.addListener(new SeparateLineLog4JListener(Level.DEBUG.toInt()));
		logSource.setLogger(jposLogger, name);
	}

	protected USATISOMUX loadMUX() {
		TandemPackager packager = new TandemPackager();
		configLogger(packager);

		USATChannelPool channelPool = new USATChannelPool();

		for(InetSocketAddress host : urls) {
			try {
				TandemTCPChannel channel = new TandemTCPChannel(host.getAddress().getHostAddress(), host.getPort(), packager);
				// set the socket read timeout to a value slightly larger than the inactivity time
				channel.setTimeout(getInactivityTimeout() * 2);
				channelPool.addChannel(channel);
				configLogger(channel);
				log.info("Loaded new TandemFrameRelayChannel for " + host);
			} catch(Exception e) {
				log.error("Failed to load Tandem host: " + host + ": " + e.getMessage(), e);
			}
		}

		TandemMUX mux = new TandemMUX(channelPool, threadPool, false);
		mux.setISORequestListener(new UnmatchedResponseHandler());
		mux.setReconnectDelay(getReconnectDelay());
		mux.setInactivityTimeout(getInactivityTimeout());
		mux.setHeartbeatInterval(getHeartbeatInterval());
		mux.start();
		return mux;
	}


	public int getResponseTimeout() {
		return responseTimeout;
	}

	public void setResponseTimeout(int responseTimeout) {
		this.responseTimeout = responseTimeout;
	}

	public InetSocketAddress[] getUrls() {
		return urls;
	}

	public void setUrls(InetSocketAddress[] urls) {
		this.urls = urls;
	}

	public void setUrls(String... urls) throws UnknownHostException, ConvertException {
		if(urls == null || urls.length == 0) {
			this.urls = null;
			return;
		}

		InetSocketAddress[] tmp = new InetSocketAddress[urls.length];
		for(int i = 0; i < urls.length; i++)
			tmp[i] = IOUtils.parseInetSocketAddress(urls[i]);
		this.urls = tmp;
	}

	public String getAvsResultDescription(String result) {
		return avsResultDescriptions.get(result);
	}

	public void setAvsResultDescription(String result, String description) {
		avsResultDescriptions.put(result, description);
	}

	public String getCvvResultDescription(String result) {
		return cvvResultDescriptions.get(result);
	}

	public void setCvvResultDescription(String result, String description) {
		cvvResultDescriptions.put(result, description);
	}

	public boolean isPersistent() {
		return persistent;
	}

	public void setPersistent(boolean persistent) {
		this.persistent = persistent;
	}

	public int getReconnectDelay() {
		return reconnectDelay;
	}

	public void setReconnectDelay(int reconnectDelay) {
		this.reconnectDelay = reconnectDelay;
	}

	public String getDeclineAvsResults() {
		return declineAvsResults;
	}

	public void setDeclineAvsResults(String declineAvsResults) {
		this.declineAvsResults = declineAvsResults;
	}

	public String getDeclineCvvResults() {
		return declineCvvResults;
	}

	public void setDeclineCvvResults(String declineCvvResults) {
		this.declineCvvResults = declineCvvResults;
	}

	public String getCardTypeDescription(String cardTypeCd) {
		return cardTypeDescriptions.get(cardTypeCd);
	}

	public void setCardTypeDescription(String cardTypeCd, String description) {
		cardTypeDescriptions.put(cardTypeCd, description);
	}

	public String getVendorIdentifier() {
		return vendorIdentifier;
	}

	public void setVendorIdentifier(String vendorIdentifier) {
		this.vendorIdentifier = vendorIdentifier;
	}

	public String getSoftwareIdentifier() {
		return softwareIdentifier;
	}

	public void setSoftwareIdentifier(String softwareIdentifier) {
		this.softwareIdentifier = softwareIdentifier;
	}

	public int getInactivityTimeout() {
		return inactivityTimeout;
	}

	public void setInactivityTimeout(int inactivityTimeout) {
		this.inactivityTimeout = inactivityTimeout;
	}

	public int getHeartbeatInterval() {
		return heartbeatInterval;
	}

	public void setHeartbeatInterval(int heartbeatInterval) {
		this.heartbeatInterval = heartbeatInterval;
	}

	public int getMaxAuthStaleness() {
		return maxAuthStaleness;
	}

	public void setMaxAuthStaleness(int maxAuthStaleness) {
		this.maxAuthStaleness = maxAuthStaleness;
	}

	public Set<String> getSupportedCurrencies() {
		return supportedCurrencies;
	}

	public void setSupportedCurrencies(Set<String> supportedCurrencies) {
		this.supportedCurrencies = supportedCurrencies;
	}

	public float getBulkAdditionalTimeoutFactor() {
		return bulkAdditionalTimeoutFactor;
	}

	public void setBulkAdditionalTimeoutFactor(float bulkAdditionalTimeoutFactor) {
		this.bulkAdditionalTimeoutFactor = bulkAdditionalTimeoutFactor;
	}

	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		AuthResultCode arc = processInternal(taskInfo);
		return determineResultInt(arc);
	}

	protected int determineResultInt(AuthResultCode result) {
		switch(result) {
			case APPROVED:
			case PARTIAL:
				return 0;
			case DECLINED:
			case DECLINED_PERMANENT:
			case DECLINED_PAYMENT_METHOD:
				return 1;
			case FAILED:
				return 2;
			default:
				return 1;
		}
	}

	@Override
	public boolean isAvailable() {
		if(mux == null)
			return false;
		if(mux.getISOChannel() == null)
			return false;
		if(mux.isTerminating())
			return false;
		return mux.isConnected() && mux.getISOChannel().getLastReceive() > 0L;
	}

	public AuthAdjustmentSetting getAuthPartiallyReversed() {
		return authPartiallyReversed;
	}

	public Long getDefaultCustomerId() {
		return defaultCustomerId;
	}

	public void setDefaultCustomerId(Long defaultCustomerId) {
		this.defaultCustomerId = defaultCustomerId;
	}

	public long getMaxAuthReversalAge() {
		return maxAuthReversalAge;
	}

	public void setMaxAuthReversalAge(long maxAuthReversalAge) {
		this.maxAuthReversalAge = maxAuthReversalAge;
	}

	public String getDoingBusinessAsPrefix() {
		return doingBusinessAsPrefix;
	}

	public void setDoingBusinessAsPrefix(String doingBusinessAsPrefix) {
		this.doingBusinessAsPrefix = doingBusinessAsPrefix;
	}

	public PosEnvironment[] getRejectPOSEnvironmentSet() {
		return rejectPOSEnvironmentSet.toArray(new PosEnvironment[rejectPOSEnvironmentSet.size()]);
	}

	public void setRejectPOSEnvironments(PosEnvironment[] rejectPOSEnvironments) {
		this.rejectPOSEnvironmentSet.clear();
		if(rejectPOSEnvironments != null)
			for(PosEnvironment pe : rejectPOSEnvironments)
				this.rejectPOSEnvironmentSet.add(pe);
	}

	public short[] getRejectMCCs() {
		short[] array = new short[rejectMCCSet.size()];
		Iterator<Short> iter = rejectMCCSet.iterator();
		for(int i = 0; i < array.length && iter.hasNext(); i++)
			array[i] = iter.next();
		return array;
	}

	public void setRejectMCCs(short[] rejectMCCs) {
		this.rejectMCCSet.clear();
		if(rejectMCCs != null)
			for(short s : rejectMCCs)
				if(s > 0)
					this.rejectMCCSet.add(s);
	}

	public int getMaxTimeoutRetries() {
		return maxTimeoutRetries;
	}

	public void setMaxTimeoutRetries(int maxTimeoutRetries) {
		this.maxTimeoutRetries = maxTimeoutRetries;
	}

	public int getMaxParallelRequests() {
		return maxParallelRequests;
	}

	public void setMaxParallelRequests(int maxParallelRequests) {
		this.maxParallelRequests = maxParallelRequests;
	}

	public long getMaxDuplicateAge() {
		return maxDuplicateAge;
	}

	public void setMaxDuplicateAge(long maxDuplicateAge) {
		this.maxDuplicateAge = maxDuplicateAge;
	}
	
	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}

}
