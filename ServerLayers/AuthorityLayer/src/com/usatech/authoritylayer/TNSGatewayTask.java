package com.usatech.authoritylayer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeoutException;

import org.apache.log4j.Level;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISORequest;
import org.jpos.iso.ISORequestListener;
import org.jpos.iso.ISOSource;
import org.jpos.util.LogSource;

import com.usatech.app.Attribute;
import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.iso8583.ISO8583Response;
import com.usatech.iso8583.interchange.tns.TNSChannel;
import com.usatech.iso8583.interchange.tns.TNSICCData;
import com.usatech.iso8583.interchange.tns.TNSMUX;
import com.usatech.iso8583.interchange.tns.TNSMsg;
import com.usatech.iso8583.interchange.tns.TNSPackager;
import com.usatech.iso8583.interchange.tns.TNSStructuredData;
import com.usatech.iso8583.jpos.SeparateLineLog4JListener;
import com.usatech.iso8583.jpos.USATChannelPool;
import com.usatech.iso8583.jpos.USATISOMUX;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAction;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.DeniedReason;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.MessageAttrEnum;
import com.usatech.layers.common.constants.MessageAttribute;
import com.usatech.layers.common.constants.PaymentMaskBRef;
import com.usatech.layers.common.constants.TLVTag;
import com.usatech.layers.common.util.EMVIdtechDecryptValueHandler;

import simple.app.Prerequisite;
import simple.app.RetryPolicy;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ScheduledRetryServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ValueException;
import simple.bean.ValueException.ValueInvalidException;
import simple.bean.ValueException.ValueMissingException;
import simple.io.IOUtils;
import simple.io.Log;
import simple.io.TLVParser;
import simple.text.IntervalFormat;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;

public class TNSGatewayTask implements MessageChainTask, Prerequisite {
	private static final Log log = Log.getLog();
	protected static final RetryPolicy IMMEDIATE_RETRY_POLICY = new RetryPolicy(20, 0, 0.0f);
	protected static final DateFormat EXP_DATE_FORMAT = (DateFormat) ConvertUtils.getFormat("DATE", "yyMM");
	protected static final NumberFormat TIMESPAN_FORMAT = new IntervalFormat();
	protected int responseTimeout = 15000;
	protected boolean persistent = true;
	protected int reconnectDelay = 60000;
	protected int inactivityTimeout = 200000;
	protected int heartbeatInterval = 180000;
	protected Map<String, AuthResultCode> responseCodeResultCd = new HashMap<String, AuthResultCode>();
	protected Map<String, String> responseCodeResponseMessage = new HashMap<String, String>();
	protected USATISOMUX mux;
	protected InetSocketAddress[] urls;
	protected final ThreadPoolExecutor threadPool = (ThreadPoolExecutor) Executors.newCachedThreadPool();
	protected int maxAuthStaleness = 30 * 1000;
	protected float bulkAdditionalTimeoutFactor = 0.0f;
	protected final AuthAdjustmentSetting authPartiallyReversed = new AuthAdjustmentSetting();
	protected Long defaultCustomerId = null;
	protected int maxTimeoutRetries = 10;
	protected int maxParallelRequests = 1;
	protected long maxDuplicateAge = 60 * 60 * 1000L;
	protected long requestStartTimestamp = 0;
	
	protected static final TLVParser TLV_PARSER = new TLVParser();

	protected class OptionallyIndexedAccountData {
		protected final MessageChainStep step;
		protected final int index;
		protected final boolean useDecryptionAttribute;
		protected String trackData;
		protected String[] partData;

		public OptionallyIndexedAccountData(MessageChainStep step, int index) {
			this(step, index, false);
		}

		public OptionallyIndexedAccountData(MessageChainStep step, int index, boolean useDecryptionAttribute) {
			this.step = step;
			this.index = index;
			this.useDecryptionAttribute = useDecryptionAttribute;
		}

		public String getTrackData() {
			if(trackData == null) {
				try {
					trackData = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_TRACK_DATA, index, String.class, false);
				} catch(AttributeConversionException e) {
					trackData = null;
				}
				if(StringUtils.isBlank(trackData))
					try {
						trackData = step.getOptionallyIndexedAttributeDefault(AuthorityAttrEnum.ATTR_AUTH_ACCOUNT_DATA, index, String.class, "");
					} catch(AttributeConversionException e) {
						trackData = "";
					}
			}
			return trackData.length() == 0 ? null : trackData;
		}

		public String getPartData(PaymentMaskBRef part) {
			if(partData == null)
				partData = new String[PaymentMaskBRef.values().length];
			final int partIndex = part.ordinal();
			if(partData[partIndex] == null)
				try {
					partData[partIndex] = step.getOptionallyIndexedAttributeDefault(useDecryptionAttribute && part.getStoreIndex() > 0 ? part.decryptionAttribute : part.attribute, index, String.class, "");
				} catch(AttributeConversionException e) {
					partData[partIndex] = "";
				}
			else if(partData[partIndex].length() == 0)
				return null;
			return partData[partIndex];
		}
	}
	protected class UnmatchedResponseHandler implements ISORequestListener {
		public boolean process(ISOSource source, ISOMsg isoMsg) {
			if(isoMsg instanceof TNSMsg) {
				try {
					TNSMsg msg = (TNSMsg) isoMsg;
					if(!msg.hasField(0)) {
						log.warn("Received invalid message IN : " + msg);
					}
				} catch(Exception e) {
					log.error("Failed to process Unmatched Response: " + isoMsg + ": " + e.getMessage(), e);
				}
			} else {
				log.error("Received unmatched message from TNS Host: " + isoMsg);
			}

			return true;
		}
	}
	protected interface RequestProcessor {
		public void prepare(int timeout);

		public AuthResultCode getResult(int timeout) throws TimeoutException, ServiceException;
	}

	protected class TrivialRequestProcessor implements RequestProcessor {
		protected final AuthResultCode result;

		public TrivialRequestProcessor(AuthResultCode result) {
			this.result = result;
		}

		@Override
		public void prepare(int timeout) {
		}

		@Override
		public AuthResultCode getResult(int timeout) {
			return result;
		}
	}

	protected final TrivialRequestProcessor APPROVED_REQUEST_PROCESSOR = new TrivialRequestProcessor(AuthResultCode.APPROVED);
	protected final TrivialRequestProcessor FAILED_REQUEST_PROCESSOR = new TrivialRequestProcessor(AuthResultCode.FAILED);
	protected final TrivialRequestProcessor DECLINED_PERMANENT_REQUEST_PROCESSOR = new TrivialRequestProcessor(AuthResultCode.DECLINED_PERMANENT);

	public TNSGatewayTask() {
	}

	public void initialize() {
		mux = loadMUX();
	}

	protected String[] getPreActionLogAttributes() {
		return new String[] { MessageAttribute.ACTION_TYPE.getValue(), MessageAttribute.AUTHORITY_NAME.getValue(), MessageAttribute.TRACE_NUMBER.getValue(), MessageAttrEnum.ATTR_DEVICE_NAME.getValue(), AuthorityAttrEnum.ATTR_AUTH_TIME.getValue() };
	}

	protected String[] getPostActionLogAttributes() {
		return new String[] { MessageAttribute.ACTION_TYPE.getValue(), MessageAttribute.AUTHORITY_NAME.getValue(), MessageAttribute.TRACE_NUMBER.getValue() };
	}

	protected AuthResultCode processInternal(MessageChainTaskInfo taskInfo) throws ServiceException {
		requestStartTimestamp = System.currentTimeMillis();
		MessageChainStep step = taskInfo.getStep();
		Map<String, Object> attributes = step.getAttributes();
		int hostResponseTimeout;
		try {
			hostResponseTimeout = step.getAttribute(AuthorityAttrEnum.ATTR_RESPONSE_TIME_OUT, Integer.class, true) * 1000;
		} catch(AttributeConversionException e) {
			log.error("Could not convert '" + e.getAttributeKey() + "' for request", e);
			return populateFailedResults(null, step, 0, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Data Conversion Error of '" + e.getAttributeKey() + "' in " + getClass().getSimpleName());
		}
		if(hostResponseTimeout < 1)
			hostResponseTimeout = 15000;
		AuthResultCode authResultCd;

		try {
			ProcessingUtils.logAttributes(log, "Starting ", attributes, 0, getPreActionLogAttributes(), null);
			RequestProcessor rp = processInternal(taskInfo, 0);
			rp.prepare(hostResponseTimeout);
			step.removeAttribute("trackData");
			authResultCd = rp.getResult(hostResponseTimeout);
			ProcessingUtils.logAttributes(log, "Finished, result=" + authResultCd.getDescription() + "; ", attributes, 0, getPostActionLogAttributes(), null);
		} catch(TimeoutException e) {
			if(taskInfo.getRetryCount() < getMaxTimeoutRetries() || getMaxTimeoutRetries() < 0 || step.getAttributeSafely(AuthorityAttrEnum.ATTR_ACTION_TYPE, AuthorityAction.class, null) == AuthorityAction.AUTHORIZATION)
				throw new ScheduledRetryServiceException(e, IMMEDIATE_RETRY_POLICY, false, true);
			authResultCd = populateFailedResults(null, step, 0, DeniedReason.PROCESSOR_TIMEOUT, "Processor Timeout");
		}

		return authResultCd;
	}

	protected RequestProcessor processInternal(final MessageChainTaskInfo taskInfo, final int index) throws ServiceException {
		final MessageChainStep step = taskInfo.getStep();
		final AuthorityAction action;
		final OptionallyIndexedAccountData accountData;
		try {
			action = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_ACTION_TYPE, index, AuthorityAction.class, index == 0);
			if(action == null) {
				log.info("Action for index " + index + " not found; skipping");
				return null;
			}
			accountData = new OptionallyIndexedAccountData(step, index);
			switch(action) {
				case AUTHORIZATION: // check staleness
					int maxStaleness = getMaxAuthStaleness();
					if(maxStaleness > 0) {
						long authTime = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_AUTH_TIME, index, Long.class, true);
						if(authTime + maxStaleness < System.currentTimeMillis()) {
							log.warn("Auth request against " + taskInfo.getServiceName() + " initiated at " + authTime + " has become stale");
							if(!taskInfo.isRedelivered()) {
								log.warn("Authorization is stale before it was even attempted");
								populateFailedResults(AuthorityAction.AUTHORIZATION, step, index, DeniedReason.INTERNAL_TIMEOUT, "Auth Expired Before Attempted");
								return FAILED_REQUEST_PROCESSOR;
							}
							TNSMsg request;
							try {
								request = createRequest(AuthorityAction.REVERSAL, AuthorityAction.AUTHORIZATION, taskInfo, index, accountData);
							} catch(ValueMissingException e) {
								log.warn("Missing value in attributes: " + step.getAttributes(), e);
								populateDeclinePermanentResults(action, step, index, DeniedReason.MISSING_INPUT, e.getMessage());
								return DECLINED_PERMANENT_REQUEST_PROCESSOR;
							} catch(ValueInvalidException e) {
								log.warn("Invalid value in attributes: " + step.getAttributes(), e);
								populateDeclinePermanentResults(action, step, index, DeniedReason.INVALID_INPUT, e.getMessage());
								return DECLINED_PERMANENT_REQUEST_PROCESSOR;
							} catch(ISOException | ServiceException e) {
								log.warn("Could not create request", e);
								populateFailedResults(action, step, index, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, e.getMessage());
								return FAILED_REQUEST_PROCESSOR;
							} catch(AttributeConversionException e) {
								log.warn("Could not create request", e);
								populateFailedResults(null, step, index, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Data Conversion Error of '" + e.getAttributeKey() + "' in " + getClass().getSimpleName());
								return FAILED_REQUEST_PROCESSOR;
							}
							final ISORequest isoRequest = new ISORequest(request);
							mux.queue(isoRequest);
							return new RequestProcessor() {
								public void prepare(int timeout) {
								}

								public AuthResultCode getResult(int timeout) throws TimeoutException, ServiceException {
									AuthResultCode authResultCd = retrieveResult(isoRequest, AuthorityAction.REVERSAL, AuthorityAction.AUTHORIZATION, taskInfo, index, timeout);
									switch(authResultCd) {
										case FAILED:
											throw new ServiceException("Error occurred while reversing the expired authorization");
									}
									return populateFailedResults(AuthorityAction.AUTHORIZATION, step, index, DeniedReason.INTERNAL_TIMEOUT, "Auth Timeout");
								}
							};
						}
					}
					break;
				case SALE:
					final int amount = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_AMOUNT, index, Integer.class, true);

					if(amount == 0) {
						TNSMsg request;
						try {
							request = createRequest(AuthorityAction.SALE, null, taskInfo, index, accountData);
						} catch(ValueMissingException e) {
							log.warn("Missing value in attributes: " + step.getAttributes(), e);
							populateDeclinePermanentResults(action, step, index, DeniedReason.MISSING_INPUT, e.getMessage());
							return DECLINED_PERMANENT_REQUEST_PROCESSOR;
						} catch(ValueInvalidException e) {
							log.warn("Invalid value in attributes: " + step.getAttributes(), e);
							populateDeclinePermanentResults(action, step, index, DeniedReason.INVALID_INPUT, e.getMessage());
							return DECLINED_PERMANENT_REQUEST_PROCESSOR;
						} catch(ISOException | ServiceException e) {
							log.warn("Could not create request", e);
							populateFailedResults(action, step, index, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, e.getMessage());
							return FAILED_REQUEST_PROCESSOR;
						} catch(AttributeConversionException e) {
							log.warn("Could not create request", e);
							populateFailedResults(null, step, index, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Data Conversion Error of '" + e.getAttributeKey() + "' in " + getClass().getSimpleName());
							return FAILED_REQUEST_PROCESSOR;
						}
						final ISORequest isoRequest = new ISORequest(request);
						mux.queue(isoRequest);
						return new RequestProcessor() {
							public void prepare(int timeout) {
							}

							public AuthResultCode getResult(int timeout) throws TimeoutException {
								return retrieveResult(isoRequest, AuthorityAction.SALE, null, taskInfo, index, timeout);
							}
						};
					}
					break;
				case TERMINAL_ADD:
				case TERMINAL_UPDATE:
				case TERMINAL_DELETE:
					break;
				default:
					throw new RetrySpecifiedServiceException("Action type '" + action + "' NOT implemented", WorkRetryType.NO_RETRY);
			}
		} catch(AttributeConversionException e) {
			log.error("Could not convert '" + e.getAttributeKey() + "' for request", e);
			populateFailedResults(null, step, index, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Data Conversion Error of '" + e.getAttributeKey() + "' in " + getClass().getSimpleName());
			return FAILED_REQUEST_PROCESSOR;
		}
		TNSMsg request;
		try {
			request = createRequest(action, null, taskInfo, index, accountData);
		} catch(ValueMissingException e) {
			log.warn("Missing value in attributes: " + step.getAttributes(), e);
			populateDeclinePermanentResults(action, step, index, DeniedReason.MISSING_INPUT, e.getMessage());
			return DECLINED_PERMANENT_REQUEST_PROCESSOR;
		} catch(ValueInvalidException e) {
			log.warn("Invalid value in attributes: " + step.getAttributes(), e);
			populateDeclinePermanentResults(action, step, index, DeniedReason.INVALID_INPUT, e.getMessage());
			return DECLINED_PERMANENT_REQUEST_PROCESSOR;
		} catch(ISOException | ServiceException e) {
			log.warn("Could not create request", e);
			populateFailedResults(action, step, index, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, e.getMessage());
			return FAILED_REQUEST_PROCESSOR;
		} catch(AttributeConversionException e) {
			log.warn("Could not create request", e);
			populateFailedResults(null, step, index, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Data Conversion Error of '" + e.getAttributeKey() + "' in " + getClass().getSimpleName());
			return FAILED_REQUEST_PROCESSOR;
		}
		final ISORequest isoRequest = new ISORequest(request);
		mux.queue(isoRequest);
		return new RequestProcessor() {
			public void prepare(int timeout) {
			}

			public AuthResultCode getResult(int timeout) throws TimeoutException {
				return retrieveResult(isoRequest, action, null, taskInfo, index, timeout);
			}
		};
	}
	
	protected AuthResultCode populateFailedResults(AuthorityAction action, MessageChainStep step, int index, DeniedReason deniedReason, String errorText, Object... params) {
		AuthResultCode authResultCode = AuthResultCode.FAILED;
		step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, index, authResultCode.getValue());
		if(deniedReason != null)
			step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, index, deniedReason);
		step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, index, deniedReason == null ? "--" : deniedReason.toString());
		step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, index, errorText);
		setClientText(step, index, authResultCode, deniedReason, params);
		return authResultCode;
	}

	protected AuthResultCode populateDeclinePermanentResults(AuthorityAction action, MessageChainStep step, int index, DeniedReason deniedReason, String errorText, Object... params) {
		AuthResultCode authResultCode = AuthResultCode.DECLINED_PERMANENT;
		step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, index, authResultCode.getValue());
		if(deniedReason != null)
			step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, index, deniedReason);
		step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, index, deniedReason == null ? "--" : deniedReason.toString());
		step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, index, errorText);
		setClientText(step, index, authResultCode, deniedReason, params);
		return authResultCode;
	}
	
	protected AuthResultCode populateDeclineResults(AuthorityAction action, MessageChainStep step, int index, DeniedReason deniedReason, String errorText, Object... params) {
		AuthResultCode authResultCode = AuthResultCode.DECLINED;
		step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, index, authResultCode.getValue());
		if(deniedReason != null)
			step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, index, deniedReason);
		step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, index, deniedReason == null ? "--" : deniedReason.toString());
		step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, index, errorText);
		setClientText(step, index, authResultCode, deniedReason, params);
		return authResultCode;
	}

	private void validateExpirationDate(String expDateStr, long authTime) throws ValueInvalidException {
		Date expDate;
		try {
			expDate = EXP_DATE_FORMAT.parse(expDateStr);
		} catch(ParseException e) {
			throw new ValueException.ValueInvalidException("Expiration Date is not in a valid format", PaymentMaskBRef.EXPIRATION_DATE.attribute.getValue(), expDateStr, e);
		}
	}
	
	protected TNSMsg createRequest(AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo, int index, OptionallyIndexedAccountData accountData) throws ValueMissingException, ISOException, ValueInvalidException, ServiceException, AttributeConversionException {
		MessageChainStep step = taskInfo.getStep();
		TNSMsg request = new TNSMsg();

		/*
		 	Values are set based on "TNS Smart Networks – OSI PostBridge Replacement" BRD_TNS_PBInteracFlash-v1.0.pdf spec.
			Field definition is in "PostBridge Interface Specification" if_postbridge.pdf spec.

			Fields ignored by TNS:
		 	F-32 Acquiring Institution ID Code
			F-40 Service Restriction Code
			F-56 Message Reason Code
			F-59 Echo Data
			F-127.2 Switch Key
			F-127.3  Routing Information
			F-127.4  POS Data
			F-127.12 Terminal Owner
			F-127.13 POS Geographic Data
			F-127.14 Sponsor Bank
			F-127.20 Originator / Authorizer date settlement
		*/
		
		long traceNumber = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_TRACE_NUMBER, index, Long.class, true);
		String formattedTraceNumber = String.valueOf(sanitizeTraceNumber(traceNumber));
		request.setSystemTraceNumber(formattedTraceNumber);
		request.setRetrievalReferenceNumber(formatRetrievalReferenceNumber(traceNumber));
		request.setCurrencyCode("124");

		String timeZoneGuid = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_TIMEZONE_GUID, index, String.class, true);
		long authTime = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_AUTH_TIME, index, Long.class, true);
		Date localTranDate = ConvertUtils.getDateUTC(ConvertUtils.getLocalTime(authTime, timeZoneGuid));		
		String transmissionDateTime = formatDate(localTranDate, "MMddHHmmss");
		request.setTransmissionDateTime(transmissionDateTime);
		request.setTransactionLocalTime(formatDate(localTranDate, "HHmmss"));
		String formattedDate = formatDate(localTranDate, "MMdd");
		request.setTransactionLocalDate(formattedDate);
		request.setSettlementDate(formattedDate);

		String acquiringInstitutionIDCode = "00000000000";
		int amount = 0;

		switch (action) {
			case TERMINAL_ADD:
			case TERMINAL_UPDATE:
			case TERMINAL_DELETE:
				request.setReceivingInstitutionIDCode("00000000000");
				break;
			default:
				amount = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_AMOUNT, index, Integer.class, true);
				request.setAcquiringInstitutionIDCode(acquiringInstitutionIDCode);
		}

		switch(action) {
			case AUTHORIZATION:
				request.setMTI("0100");
				request.setExtendedTransactionType("9002");
				EntryType entryType = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_ENTRY_TYPE, index, EntryType.class, true);
				if (entryType != EntryType.INTERAC_FLASH)
						throw new ServiceException("Entry type " + entryType + " is not supported by " + this);
				addEmvData(request, step, index);
				break;
			case SALE:
				if (amount > 0)
					request.setMTI("0200");
				else {
					//send reversal for canceled sales
					request.setMTI("0400");
					amount = step.getOptionallyIndexedAttributeDefault(AuthorityAttrEnum.ATTR_AUTH_AMOUNT, index, Integer.class, amount);
					if(amount == 0)
						amount = step.getOptionallyIndexedAttributeDefault(AuthorityAttrEnum.ATTR_AMT_APPROVED, index, Integer.class, amount);
					request.setMessageReasonCode("4000"); //Customer cancellation
				}
				request.setExtendedTransactionType("9002");
				String approvalCode = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD, index, String.class, false);
				if (!StringUtils.isBlank(approvalCode)) {
					TNSStructuredData structuredData = new TNSStructuredData();
					structuredData.setInteracAuthIdRsp(approvalCode);
					request.setStructuredData(structuredData.generateStructuredData());
				}
				break;
			case REVERSAL:
				request.setMTI("0400");
				request.setExtendedTransactionType("9002");
				switch(subAction) {
					case AUTHORIZATION:
						if(amount == 0) {
							amount = step.getOptionallyIndexedAttributeDefault(AuthorityAttrEnum.ATTR_AUTH_AMOUNT, index, Integer.class, amount);
							if(amount == 0)
								amount = step.getOptionallyIndexedAttributeDefault(AuthorityAttrEnum.ATTR_AMT_APPROVED, index, Integer.class, amount);
						}
						StringBuilder sb = new StringBuilder();
						sb.append("0100"); //Original message type (positions 1 - 4)
						sb.append(formattedTraceNumber); //Original systems trace audit number (positions 5 - 10)
						sb.append(transmissionDateTime); //Original transmission date and time (positions 11 - 20)
						sb.append(acquiringInstitutionIDCode); //Original acquirer institution ID code (position 21 - 31)
						sb.append("00000000000"); //Original forwarding institution ID code (position 32 - 42)
						request.setOriginalDataElements(sb.toString());

						sb.setLength(0);
						sb.append("000000000000"); //Actual amount, transaction (positions 1 - 12)
						sb.append("000000000000"); //Actual amount, settlement (positions 13 - 24)
						sb.append("C00000000"); //Actual amount, transaction fee (positions 25 - 33)
						sb.append("C00000000"); //Actual amount, settlement fee (positions 34 - 42)
						request.setOriginalAmounts(sb.toString());

						addEmvData(request, step, index);
						break;
					default:
						throw new ServiceException("Reversal of action " + subAction + " is not supported by " + this);
				}
				break;
			case TERMINAL_ADD:
				request.setMTI("0600");
				request.setExtendedTransactionType("5200");
				break;
			case TERMINAL_UPDATE:
				request.setMTI("0600");
				request.setExtendedTransactionType("5203");
				break;
			case TERMINAL_DELETE:
				request.setMTI("0600");
				request.setExtendedTransactionType("5201");
				break;
			default:
				throw new ServiceException("Action " + action + " is not supported by " + this);
		}

		StringBuilder posEntryMode = new StringBuilder();
		posEntryMode.append("07"); //Auto entry via contactless integrated circuit card (ICC)
		posEntryMode.append("2"); //Terminal cannot accept PINs
		request.setPointOfServiceEntryMode(posEntryMode.toString());
		request.setPointOfServiceConditionCode("27"); //Unattended terminal - card can not be retained

		//Ignored by TNS
		/*StringBuilder serviceRestrictionCode = new StringBuilder("2"); //International card - integrated circuit facilities
		serviceRestrictionCode.append("2"); //Online authorization mandatory
		serviceRestrictionCode.append("1"); //No restrictions - normal cardholder verification
		request.setServiceRestrictionCode(serviceRestrictionCode.toString());*/

		//Some values in the spec may seem like a better match but TNS instructed to send the same values as Coke to be safe
		StringBuilder posDataCode = new StringBuilder();
		posDataCode.append("A"); //1: Contactless integrated circuit card (ICC)
		posDataCode.append("0"); //2: No electronic authentication 
		posDataCode.append("0"); //3: No card capture capability
		posDataCode.append("4"); //4: Off premises of card acceptor, unattended
		posDataCode.append("0"); //5: Cardholder present
		posDataCode.append("1"); //6: Card present
		posDataCode.append("8"); //7: Contactless magnetic stripe
		posDataCode.append("0"); //8: No electronic authentication
		posDataCode.append("0"); //9: No electronic authentication
		posDataCode.append("0"); //10: Unknown
		posDataCode.append("0"); //11: Unknown 
		posDataCode.append("0"); //12: No PIN capture capability
		posDataCode.append("0"); //13: Customer operated
		posDataCode.append("01"); //14-15: POS terminal
		request.setPOSDataCode(posDataCode.toString());

		request.setCardAcceptorIDCode(step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_MERCHANT_CD, index, String.class, true));
		request.setMerchantType(step.getOptionallyIndexedAttributeDefault(AuthorityAttrEnum.ATTR_MERCHANT_CATEGORY_CODE, index, String.class, "5814"));
		request.setCardAcceptorTerminalID(step.getOptionallyIndexedAttribute(MessageAttrEnum.ATTR_DEVICE_NAME, index, String.class, true));

		switch (action) {
			case TERMINAL_ADD:
			case TERMINAL_UPDATE:
			case TERMINAL_DELETE:
				request.setProcessingCode("910000");

				TNSStructuredData structuredData = new TNSStructuredData();
				String address = step.getOptionallyIndexedAttributeDefault(AuthorityAttrEnum.ATTR_ADDRESS, index, String.class, "");
				structuredData.setTermAddress(address);
				String city = step.getOptionallyIndexedAttributeDefault(AuthorityAttrEnum.ATTR_CITY, index, String.class, "Malvern");
				structuredData.setTermCity(city);
				String stateCd = step.getOptionallyIndexedAttributeDefault(AuthorityAttrEnum.ATTR_STATE_CD, index, String.class, "PA");
				structuredData.setTermState(stateCd);
				String postal = step.getOptionallyIndexedAttributeDefault(AuthorityAttrEnum.ATTR_POSTAL, index, String.class, "19355");
				structuredData.setTermZipCode(postal);
				String countryCd = step.getOptionallyIndexedAttributeDefault(AuthorityAttrEnum.ATTR_COUNTRY_CD, index, String.class, "CA");
				request.setStructuredData(structuredData.generateStructuredData());

				String locationName = step.getOptionallyIndexedAttributeDefault(AuthorityAttrEnum.ATTR_LOCATION_NAME, index, String.class, "USA Technologies");
				if (locationName.length() > 23)
					locationName = locationName.substring(0, 23);
				StringBuilder sb = new StringBuilder();
				sb.append(StringUtils.pad(locationName, ' ', 23, Justification.LEFT));
				sb.append(StringUtils.pad(city, ' ', 13, Justification.LEFT));
				sb.append(StringUtils.pad(stateCd, ' ', 2, Justification.LEFT));
				sb.append(StringUtils.pad(countryCd, ' ', 2, Justification.LEFT));
				request.setCardAcceptorNameLocation(sb.toString());
				break;
			default:
				request.setProcessingCode("000000");
				request.setTransactionFee("C00000000");
				request.setTranProcessingFee("C00000000");
				request.setTransactionAmount(String.valueOf(amount));

				String pan = accountData.getPartData(PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER);
				if(StringUtils.isBlank(pan))
					throw new ValueException.ValueMissingException("Primary Account Number not provided", PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER.attribute.getValue());
				request.setPrimaryAccountNumber(pan);
				String expDateStr = accountData.getPartData(PaymentMaskBRef.EXPIRATION_DATE);
				if(!StringUtils.isBlank(expDateStr)) {
					validateExpirationDate(expDateStr, authTime);
					request.setExpirationDate(expDateStr);
				}
				if(!StringUtils.isBlank(accountData.getTrackData()))
					request.setTrack2Data(accountData.getTrackData().length() > 37 ? accountData.getTrackData().substring(0, 37) : accountData.getTrackData());

				//TNS ignores this field and populates it from boarded terminal data
				request.setCardAcceptorNameLocation("USA Technologies       Malvern      PACA");
		}

		log.info(new StringBuilder("Sending request: ").append(request).toString());
		return request;
	}

	protected AuthResultCode retrieveResult(ISORequest isoRequest, AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo, int index, int timeout) throws TimeoutException {
		TNSMsg response = retrieveResponse(isoRequest, timeout);
		return processResponse((TNSMsg) isoRequest.getRequest(), response, action, subAction, taskInfo, index);
	}

	protected TNSMsg retrieveResponse(ISORequest isoRequest, int timeout) throws TimeoutException {
		TNSMsg response = (TNSMsg) isoRequest.getResponse(timeout);
		if(response == null && isoRequest.isExpired())
			throw new TimeoutException("Did not receive a response in " + timeout + " ms");
		if(log.isDebugEnabled() && isoRequest.getRequest().hasField(0)) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			PrintStream ps = new PrintStream(baos);
			isoRequest.getRequest().dump(ps, "");
			ps.flush();
			log.debug(baos.toString());
		} 
		if(log.isInfoEnabled()) {
			log.info("Received response: " + response);
		}
		return response;
	}

	protected AuthResultCode processResponse(TNSMsg request, TNSMsg response, AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo, int index) throws TimeoutException {
		MessageChainStep step = taskInfo.getStep();
		// process result
		try {
			String responseCode = response.getResponseCode();
			String responseMessage = responseCodeResponseMessage.get(responseCode);
			if (responseMessage == null)
				responseMessage = responseCode;
			AuthResultCode authResultCode;
			DeniedReason deniedReason;
			if(responseCode == null || responseCode.length() == 0) {
				authResultCode = AuthResultCode.FAILED;
				deniedReason = DeniedReason.INVALID_RESPONSE;
			} else if(responseCode.equals(ISO8583Response.SUCCESS)) {
				authResultCode = AuthResultCode.APPROVED;
				deniedReason = null;
			} else if((action == AuthorityAction.SALE || action == AuthorityAction.REVERSAL) && responseCode.startsWith("GE")) {
				// return DECLINED for failed sale to put transaction in retryable TRANSACTION_INCOMPLETE state
				authResultCode = AuthResultCode.DECLINED;
				deniedReason = DeniedReason.PROCESSOR_RESPONSE;
			} else if(action == AuthorityAction.REFUND && responseCode.equals(ISO8583Response.ERROR_HOST_CONNECTION_FAILURE)) {
				// return DECLINED for refund on connection failure to put transaction in retryable TRANSACTION_INCOMPLETE state
				authResultCode = AuthResultCode.DECLINED;
				deniedReason = DeniedReason.PROCESSOR_TIMEOUT;
			} else {
				authResultCode = getAuthResultCode(responseCode);
				switch(authResultCode) {
					case APPROVED:
					case PARTIAL:
						deniedReason = null;
						break;
					default:
						deniedReason = DeniedReason.PROCESSOR_RESPONSE;
				}
			}
			switch (action) {
				case TERMINAL_ADD:
				case TERMINAL_UPDATE:
				case TERMINAL_DELETE:
					return authResultCode;
			}
			int approvedAmount;
			switch(authResultCode) {
				case PARTIAL:
					boolean allowPartialAuth;
					try {
						allowPartialAuth = step.getOptionallyIndexedAttributeDefault(AuthorityAttrEnum.ATTR_PARTIAL_AUTH_ALLOWED, index, Boolean.class, false);
					} catch(AttributeConversionException e) {
						log.warn("Could not convert " + AuthorityAttrEnum.ATTR_PARTIAL_AUTH_ALLOWED.toString(), e);
						allowPartialAuth = false;
					}
					if(!allowPartialAuth) {
						authResultCode = AuthResultCode.DECLINED;
						deniedReason = DeniedReason.INSUFFICIENT_FUNDS;
						approvedAmount = 0;
						break;
					}
					// fall-through
				case APPROVED:
					if(action == AuthorityAction.REVERSAL) {
						approvedAmount = 0;
						step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, index, approvedAmount);
					} else {
						int requestedAmount = ConvertUtils.getInt(request.getTransactionAmount());
						if(!StringUtils.isBlank(response.getTransactionAmount())) {
							approvedAmount = ConvertUtils.getInt(response.getTransactionAmount());
							if(approvedAmount < requestedAmount)
								authResultCode = AuthResultCode.PARTIAL;
						} else
							approvedAmount = requestedAmount;
						step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, index, approvedAmount);
					}
					break;
				default:
					approvedAmount = 0;
			}

			String clientText = null;
			if(action != AuthorityAction.AUTHORIZATION && authResultCode == AuthResultCode.APPROVED)
				step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_SETTLED, index, true);

			step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, index, authResultCode.getValue());
			if(deniedReason != null)
				step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, index, deniedReason);
			step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, index, responseCode);
			step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, index, responseMessage);
			step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD, index, response.getAuthorizationIDResponse());
			step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, index, response.getRetrievalReferenceNumber());
			step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS, index, formatDate(new Date(), "MMddyyyyHHmmss"));

			if(authResultCode == AuthResultCode.PARTIAL)
				step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, index, approvedAmount);
			if(action == AuthorityAction.AUTHORIZATION) {
				switch(authResultCode) {
					case APPROVED:
					case PARTIAL:
						if(approvedAmount > 0)
							step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTH_HOLD_USED, index, true);
						setClientText(step, index, authResultCode, null, null, approvedAmount, null, responseMessage);
						break;
					case CVV_MISMATCH:
					case AVS_MISMATCH:
					case CVV_AND_AVS_MISMATCH:
						if(approvedAmount > 0)
							step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTH_HOLD_USED, index, true);
						setClientText(step, index, authResultCode, deniedReason, responseMessage, clientText);
						break;
					case DECLINED:
						if(deniedReason != null && approvedAmount > 0)
							switch(deniedReason) {
								case CVV_MISMATCH:
								case AVS_MISMATCH:
								case CVV_AND_AVS_MISMATCH:
									step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_AUTH_HOLD_USED, index, true);
									break;
							}
						// fall-through
					default:
						setClientText(step, index, authResultCode, deniedReason, responseMessage);
				}
			}
			return authResultCode;
		} catch(ISOException e) {
			log.warn("Could not process response", e);
			return populateFailedResults(action, taskInfo.getStep(), index, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, e.getMessage());
		} catch(ConvertException e) {
			log.warn("Could not process response", e);
			return populateFailedResults(action, taskInfo.getStep(), index, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, e.getMessage());
		}
	}
	
	protected void setClientText(MessageChainStep step, int index, AuthResultCode authResultCode, DeniedReason deniedReason, Object... params) {
		String prefix = getClientTextKeyPrefix();
		switch(authResultCode) {
			case APPROVED:
			case PARTIAL:
				step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, index, "client.message.auth." + prefix + ".approved");
				step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, index, params == null ? new Object[0] : params);
				break;
			default:
				step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, index, "client.message.auth." + prefix + ".denied-" + (deniedReason == null ? "UNKNOWN" : deniedReason.toString()));
				step.setOptionallyIndexedResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, index, params == null ? new Object[0] : params);
		}
	}
	
	protected void addEmvData(TNSMsg request, MessageChainStep step, int index) throws AttributeConversionException, ISOException {
		byte [] additionalData = step.getOptionallyIndexedAttribute(AuthorityAttrEnum.ATTR_ADDITIONAL_INFO, index, byte[].class, false);
		if (additionalData == null) {
			log.warn("Received null additionalInfo attribute, not adding EMV data...");
			return;
		}
		Map<String, byte[]> tlvMap = new HashMap<String, byte[]>();
		ByteBuffer byteBuffer = ByteBuffer.wrap(additionalData);
		CardReaderType cardReaderType = step.getOptionallyIndexedAttributeDefault(CommonAttrEnum.ATTR_CARD_READER_TYPE_ID, index, CardReaderType.class, CardReaderType.IDTECH_VENDX_UNPARSED);
		EMVIdtechDecryptValueHandler valueHandler = null;
		switch (cardReaderType) {
			case IDTECH_VENDX_UNPARSED:
				valueHandler = new EMVIdtechDecryptValueHandler(byteBuffer, null, tlvMap);
				break;
			default:
				throw new ISOException("Unsupported EMV card reader: " + cardReaderType);
		}
		try {
			TLV_PARSER.parse(additionalData, 0, additionalData.length, valueHandler);
		} catch (IOException e) {
			throw new ISOException("Error parsing EMV data", e);
		}

		TNSICCData iccData = new TNSICCData();
		byte[] rawValue;
		String value;
		for (TLVTag tag : TLVTag.TNS_EMV_TAGS) {
			rawValue = tlvMap.get(tag.getHexValue());
			value = StringUtils.toHex(rawValue);
			if (StringUtils.isBlank(value))
				continue;
			switch (tag) {
				case TRANSACTION_AMOUNT_AUTHORIZED:
					iccData.setAmountAuthorized(value);
					break;
				case TRANSACTION_AMOUNT_OTHER:
					iccData.setAmountOther(value);
					break;
				case APPLICATION_CRYPTOGRAM:
					iccData.setCryptogram(value);
					break;
				case APPLICATION_INTERCHANGE_PROFILE:
					iccData.setApplicationInterchangeProfile(value);
					break;
				case APPLICATION_TRANSACTION_COUNTER:
					iccData.setApplicationTransactionCounter(value);
					break;
				case CRYPTOGRAM_INFORMATION_DATA:
					iccData.setCryptogramInformationData(value);
					break;
				case CVM_RESULTS:
					iccData.setCvmResults(value);
					break;
				case ISSUER_APPLICATION_DATA:
					iccData.setIssuerApplicationData(value);
					break;
				case PAN_SEQUENCE_NUMBER:
					//Must match Application Primary Account Number (PAN) Sequence Number (PSN)
					if (rawValue.length == 1)
						value = String.valueOf(rawValue[0] & 0xFF);
					request.setCardSequenceNumber(StringUtils.pad(value, '0', 3, Justification.RIGHT));
					break;
				case TERMINAL_CAPABILITIES:
					iccData.setTerminalCapabilities(value);
					break;
				case TERMINAL_COUNTRY_CODE:
					iccData.setTerminalCountryCode(Integer.valueOf(value).toString());
					break;
				case TERMINAL_TYPE:
					iccData.setTerminalType(value);
					break;
				case TERMINAL_VERIFICATION_RESULTS:
					iccData.setTerminalVerificationResult(value);
					break;
				case TRANSACTION_CURRENCY_CODE:
					iccData.setTransactionCurrencyCode(Integer.valueOf(value).toString());
					break;
				case TRANSACTION_DATE:
					iccData.setTransactionDate(value);
					break;
				case TRANSACTION_TYPE:
					iccData.setTransactionType(value);
					break;
				case UNPREDICTABLE_NUMBER:
					iccData.setUnpredictableNumber(value);
					break;
			}
		}

		request.setICCData(TNSICCData.toXML(iccData));
	}

	protected String getOptionallyIndexedAttributeValue(Attribute attribute, int index) {
		if(index > 0)
			return attribute.getValue() + '.' + index;
		return attribute.getValue();
	}

	protected String getClientTextKeyPrefix() {
		return "tns";
	}

	protected static int sanitizeTraceNumber(long traceNumber) {
		return (int)(Math.abs(traceNumber) % 1000000);
	}
	
	protected String formatRetrievalReferenceNumber(long traceNumber) {
		String number = String.valueOf(Math.abs(traceNumber));
		int length = number.length();
		if (length > 12)
			return number.substring(length - 12, length);
		else if (length < 12)
			return StringUtils.pad(number, '0', 12, Justification.RIGHT);
		else
			return number;
	}

	protected String formatDate(Date date, String format) throws IllegalArgumentException {
		if(date == null)
			return null;
		return ((DateFormat) ConvertUtils.getFormat("DATE", format)).format(date);
	}

	protected AuthResultCode getAuthResultCode(String responseCode) {
		AuthResultCode arcCode = responseCodeResultCd.get(responseCode);
		if(arcCode != null)
			return arcCode;
		if(responseCode.startsWith("GE"))
			return AuthResultCode.FAILED;
		return AuthResultCode.DECLINED;
	}

	protected boolean canSend() {
		if(mux == null)
			return false;
		if(mux.getISOChannel() == null)
			return false;
		if(mux.isTerminating())
			return false;
		if(!mux.isConnected()) {
			try {
				mux.getISOChannel().connect();
			} catch(IOException e) {
				log.warn("Could not connect mux", e);
			}
			if(!mux.isConnected())
				return false;
		}

		return true;
	}

	protected TNSMsg send(TNSMsg request, int hostResponseTimeout) throws TimeoutException {
		ISORequest isoRequest = new ISORequest(request);
		mux.queue(isoRequest);
		final int timeout = hostResponseTimeout > 0 ? hostResponseTimeout : getResponseTimeout();
		TNSMsg response = (TNSMsg) isoRequest.getResponse(timeout);
		if(response == null && isoRequest.isExpired())
			throw new TimeoutException("Did not receive a response in " + timeout + " ms");
		if(log.isDebugEnabled() && request.hasField(0)) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			PrintStream ps = new PrintStream(baos);
			request.dump(ps, "");
			ps.flush();
			log.debug(baos.toString());
		}
		return response;
	}

	protected void configLogger(LogSource logSource) {
		String name = logSource.getClass().getName();
		org.jpos.util.Logger jposLogger = new org.jpos.util.Logger();
		jposLogger.addListener(new SeparateLineLog4JListener(Level.DEBUG.toInt()));
		logSource.setLogger(jposLogger, name);
	}

	protected USATISOMUX loadMUX() {
		TNSPackager packager = new TNSPackager();
		configLogger(packager);

		USATChannelPool channelPool = new USATChannelPool();

		for(InetSocketAddress host : urls) {
			try {
				TNSChannel channel = new TNSChannel(host.getAddress().getHostAddress(), host.getPort(), packager);
				// set the socket read timeout to a value slightly larger than the inactivity time
				channel.setTimeout(getInactivityTimeout() * 2);
				channelPool.addChannel(channel);
				configLogger(channel);
				log.info("Loaded new TNSChannel for " + host);
			} catch(Exception e) {
				log.error("Failed to load TNS host: " + host + ": " + e.getMessage(), e);
			}
		}

		TNSMUX mux = new TNSMUX(channelPool, threadPool, false);
		mux.setISORequestListener(new UnmatchedResponseHandler());
		mux.setReconnectDelay(getReconnectDelay());
		mux.setInactivityTimeout(getInactivityTimeout());
		mux.setHeartbeatInterval(getHeartbeatInterval());
		mux.start();
		return mux;
	}


	public int getResponseTimeout() {
		return responseTimeout;
	}

	public void setResponseTimeout(int responseTimeout) {
		this.responseTimeout = responseTimeout;
	}

	public InetSocketAddress[] getUrls() {
		return urls;
	}

	public void setUrls(InetSocketAddress[] urls) {
		this.urls = urls;
	}

	public void setUrls(String... urls) throws UnknownHostException, ConvertException {
		if(urls == null || urls.length == 0) {
			this.urls = null;
			return;
		}

		InetSocketAddress[] tmp = new InetSocketAddress[urls.length];
		for(int i = 0; i < urls.length; i++)
			tmp[i] = IOUtils.parseInetSocketAddress(urls[i]);
		this.urls = tmp;
	}

	public boolean isPersistent() {
		return persistent;
	}

	public void setPersistent(boolean persistent) {
		this.persistent = persistent;
	}

	public int getReconnectDelay() {
		return reconnectDelay;
	}

	public void setReconnectDelay(int reconnectDelay) {
		this.reconnectDelay = reconnectDelay;
	}

	public int getInactivityTimeout() {
		return inactivityTimeout;
	}

	public void setInactivityTimeout(int inactivityTimeout) {
		this.inactivityTimeout = inactivityTimeout;
	}

	public int getHeartbeatInterval() {
		return heartbeatInterval;
	}

	public void setHeartbeatInterval(int heartbeatInterval) {
		this.heartbeatInterval = heartbeatInterval;
	}

	public int getMaxAuthStaleness() {
		return maxAuthStaleness;
	}

	public void setMaxAuthStaleness(int maxAuthStaleness) {
		this.maxAuthStaleness = maxAuthStaleness;
	}

	public float getBulkAdditionalTimeoutFactor() {
		return bulkAdditionalTimeoutFactor;
	}

	public void setBulkAdditionalTimeoutFactor(float bulkAdditionalTimeoutFactor) {
		this.bulkAdditionalTimeoutFactor = bulkAdditionalTimeoutFactor;
	}

	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		AuthResultCode arc = processInternal(taskInfo);
		return determineResultInt(arc);
	}

	protected int determineResultInt(AuthResultCode result) {
		switch(result) {
			case APPROVED:
			case PARTIAL:
				return 0;
			case DECLINED:
			case DECLINED_PERMANENT:
			case DECLINED_PAYMENT_METHOD:
				return 1;
			case FAILED:
				return 2;
			default:
				return 1;
		}
	}

	@Override
	public boolean isAvailable() {
		if(mux == null)
			return false;
		if(mux.getISOChannel() == null)
			return false;
		if(mux.isTerminating())
			return false;
		return mux.isConnected() && mux.getISOChannel().getLastReceive() > 0L;
	}

	public AuthAdjustmentSetting getAuthPartiallyReversed() {
		return authPartiallyReversed;
	}

	public Long getDefaultCustomerId() {
		return defaultCustomerId;
	}

	public void setDefaultCustomerId(Long defaultCustomerId) {
		this.defaultCustomerId = defaultCustomerId;
	}

	public int getMaxTimeoutRetries() {
		return maxTimeoutRetries;
	}

	public void setMaxTimeoutRetries(int maxTimeoutRetries) {
		this.maxTimeoutRetries = maxTimeoutRetries;
	}

	public int getMaxParallelRequests() {
		return maxParallelRequests;
	}

	public void setMaxParallelRequests(int maxParallelRequests) {
		this.maxParallelRequests = maxParallelRequests;
	}

	public long getMaxDuplicateAge() {
		return maxDuplicateAge;
	}

	public void setMaxDuplicateAge(long maxDuplicateAge) {
		this.maxDuplicateAge = maxDuplicateAge;
	}

	public String getResponseCodeResponseMessage(String responseCd) {
		return responseCodeResponseMessage.get(responseCd);
	}

	public void setResponseCodeResponseMessage(String responseCd, String responseMessage) {
		responseCodeResponseMessage.put(responseCd, responseMessage);
	}
	
	public AuthResultCode getResponseCodeResultCd(String responseCode) {
		return responseCodeResultCd.get(responseCode);
	}

	public void setResponseCodeResultCd(String responseCode, AuthResultCode resultCd) {
		responseCodeResultCd.put(responseCode, resultCd);
	}
}
