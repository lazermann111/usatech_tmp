/**
 *
 */
package com.usatech.authoritylayer;

import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicReference;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertUtils;
import simple.bean.MaskedString;
import simple.io.ByteArrayUtils;
import simple.io.Log;
import simple.text.StringUtils;
import simple.util.concurrent.LockSegmentCache;

import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.blackboard.BlackboardException;
import com.usatech.blackboard.BlackboardProxy;
import com.usatech.blackboard.BlackboardProxyTransactionInterface;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAction;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.MessageAttribute;
import com.usatech.layers.common.constants.PaymentMaskBRef;

/**
 * @author Brian S. Krug
 *
 */
public class BlackboardGatewayTask extends AbstractAuthorityTask {
	private static final Log log = Log.getLog();
	protected int tiaProtocolVersion;
	protected String vendorNumber = "2548";	//USA Tech's unique ID assigned by Blackboard

	/** Key to lookup BlackboardProxy objects. Right now this asumes the "name" is a unique representation of the other properties.
	 * @author Brian S. Krug
	 *
	 */
	protected class BlackboardProxyKey {
		protected final String name;
		protected final String preferredHost;
		protected final String alternateHost;
		protected final int port;
		protected final int tiaProtocolVersion;

		public BlackboardProxyKey(String name, String preferredHost, String alternateHost, int port, int tiaProtocolVersion) {
			this.name = name;
			this.preferredHost = preferredHost;
			this.alternateHost = alternateHost;
			this.port = port;
			this.tiaProtocolVersion = tiaProtocolVersion;
		}
		public String getName() {
			return name;
		}
		public String getPreferredHost() {
			return preferredHost;
		}
		public String getAlternateHost() {
			return alternateHost;
		}
		public int getPort() {
			return port;
		}
		public int getTiaProtocolVersion() {
			return tiaProtocolVersion;
		}
		/**
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			return getName().hashCode();
		}
		/**
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if(obj instanceof BlackboardProxyKey) {
				return getName().equals(((BlackboardProxyKey)obj).getName());
			}
			return false;
		}
	}
	protected final LockSegmentCache<String, AtomicReference<BlackboardProxy>, RuntimeException> proxies = new LockSegmentCache<String, AtomicReference<BlackboardProxy>, RuntimeException>() {
		/**
		 * @see simple.util.concurrent.LockSegmentCache#createValue(java.lang.Object, java.lang.Object[])
		 */
		@Override
		protected AtomicReference<BlackboardProxy> createValue(String key, Object... additionalInfo) {
			return new AtomicReference<BlackboardProxy>();
		}
	};

	public BlackboardGatewayTask() {
		super("blackboard");
	}

	@Override
	protected AuthResultCode processRequest(AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		String authorityName;
		String remoteServerAddr;
		String remoteServerAddrAlt;
		int remoteServerPort;
		int tenderNumber;
		int terminalNumber;
		String cardNumber;
		String pin;
		byte[] encryptionKey;
		int amount;
		boolean online;
		long transactionTime;
		String timeZoneGuid;
		boolean manuallyEntered;
		int sequenceNumber;
		try {
			authorityName = getAttribute(step, AuthorityAttrEnum.ATTR_AUTHORITY_NAME, String.class, true);
			remoteServerAddr = getAttribute(step, AuthorityAttrEnum.ATTR_REMOTE_SERVER_ADDRESS, String.class, true);
			remoteServerAddrAlt = getAttribute(step, AuthorityAttrEnum.ATTR_REMOTE_SERVER_ALTERNATIVE, String.class, false);
			remoteServerPort = getAttributeDefault(step, AuthorityAttrEnum.ATTR_REMOTE_SERVER_PORT, Integer.class, 9003);

			tenderNumber = getAttribute(step, AuthorityAttrEnum.ATTR_MERCHANT_CD, Integer.class, true);
			terminalNumber = getAttribute(step, AuthorityAttrEnum.ATTR_TERMINAL_CD, Integer.class, true);
			AccountData accountData = new AccountData(step.getAttributes());
			cardNumber = accountData.getTrackData();
			if(StringUtils.isBlank(cardNumber)) {
				cardNumber = accountData.getPartData(PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER);
				if(StringUtils.isBlank(cardNumber))
					return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), null, "Account Number not provided");
			}

			pin = getAttributeDefault(step, AuthorityAttrEnum.ATTR_PIN, String.class, "0");
			encryptionKey = ByteArrayUtils.fromHex(getAttribute(step, AuthorityAttrEnum.ATTR_TERMINAL_ENCRYPTION_KEY, String.class, false));
			amount = getAttribute(step, AuthorityAttrEnum.ATTR_AMOUNT, Integer.class, true);
			online = true; //POSM always used online=true for blackboard: getAttribute(step, AuthorityAttrEnum.ATTR_ONLINE, Boolean.class, true);
			transactionTime = getAttribute(step, AuthorityAttrEnum.ATTR_AUTH_TIME, Long.class, true);
			timeZoneGuid = getAttribute(step, AuthorityAttrEnum.ATTR_TIMEZONE_GUID, String.class, false);
			byte entryType = getAttribute(step, AuthorityAttrEnum.ATTR_ENTRY_TYPE, Byte.class, true);
			manuallyEntered = (entryType == (byte)'M');
			sequenceNumber = sanitizeTraceNumber(getAttribute(step, AuthorityAttrEnum.ATTR_TRACE_NUMBER, Long.class, true), action); // we may need to change this for blackboard
		} catch(IllegalArgumentException e) {
			log.error("Could not convert hex strings to bytes as needed", e);
			return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), null, "Data Conversion Error");
		} catch(AuthorityAttributeConversionException e) {
			return AuthResultCode.FAILED;
		}

		BlackboardProxyTransactionInterface proxy;
		try {
			proxy = getBlackboardProxy(authorityName, remoteServerAddr, remoteServerAddrAlt, remoteServerPort, getTiaProtocolVersion());
		} catch(BlackboardException e) {
			log.error("Could not create BlackboardProxy for '" + authorityName + "'", e);
			return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), null, "Blackboard Proxy Error");
		}
		Map<String, Object> resultAttributes = taskInfo.getStep().getResultAttributes();
		/*NOTE: in the future do: 
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(timeZoneGuid));
		cal.setTimeInMillis(transactionTime);
		
		Then in TransactionRequestMessage do:
		SimpleDateFormat dateFormatter = new SimpleDateFormat ("yyyyMMddHHmmss");
		dateFormatter.setCalendar(cal);
		 */
		TimeZone timeZone = TimeZone.getTimeZone(timeZoneGuid);
        switch(action) {
			case AUTHORIZATION:
				int approvedAmount;
				try {
					approvedAmount = proxy.authorize(terminalNumber, sequenceNumber, tenderNumber, amount, manuallyEntered, cardNumber, pin, online, transactionTime, timeZone, encryptionKey);
				} catch(BlackboardException e) {
					log.warn("Could not process an authorization against '" + authorityName + "'", e);
					return populateExceptionResults(action, taskInfo.getStep().getResultAttributes(), e);
				}
				boolean allowPartialAuth = step.getAttributeSafely(AuthorityAttrEnum.ATTR_PARTIAL_AUTH_ALLOWED, Boolean.class, true);
				return populateAuthResults(approvedAmount, cardNumber, amount, taskInfo.getStep().getResultAttributes(), allowPartialAuth);
			case SALE:
				try {
					proxy.debit(terminalNumber, sequenceNumber, tenderNumber, amount,  manuallyEntered, cardNumber, pin, online, transactionTime, timeZone, encryptionKey);
				} catch(BlackboardException e) {
					log.warn("Could not process a sale against '" + authorityName + "'", e);
					return populateExceptionResults(action, taskInfo.getStep().getResultAttributes(), e);
				}
				return populateResults(amount, taskInfo.getStep().getResultAttributes());
			case REFUND:
				try {
					proxy.refund(terminalNumber, sequenceNumber, tenderNumber, amount,  manuallyEntered, cardNumber, pin, online, transactionTime, timeZone, encryptionKey);
				} catch(BlackboardException e) {
					log.warn("Could not process a refund against '" + authorityName + "'", e);
					return populateExceptionResults(action, taskInfo.getStep().getResultAttributes(), e);
				}
				return populateResults(amount, taskInfo.getStep().getResultAttributes());
			case ADJUSTMENT:
				int originalAmount;
				try {
					originalAmount = getAttribute(step, AuthorityAttrEnum.ATTR_ORIGINAL_AMOUNT, Integer.class, true);
				} catch(AuthorityAttributeConversionException e) {
					return AuthResultCode.FAILED;
				}
				switch(subAction) {
					case SALE:
						int adjustAmount = amount - originalAmount;
						switch(Integer.signum(adjustAmount)) {
							case -1: // needs credit
								try {
									proxy.refund(terminalNumber, sequenceNumber, tenderNumber, -adjustAmount,  manuallyEntered, cardNumber, pin, online, transactionTime, timeZone, encryptionKey);
								} catch(BlackboardException e) {
									log.warn("Could not process a refund against '" + authorityName + "'", e);
									return populateExceptionResults(action, taskInfo.getStep().getResultAttributes(), e);
								}
								return populateResults(adjustAmount, taskInfo.getStep().getResultAttributes());
							case 0: // nothing to do
								resultAttributes.put("authResultCd", 'Y');
								resultAttributes.put("authorityRespCd", 0);
								String text = "No change to account";
								resultAttributes.put("authorityRespDesc", text);
								resultAttributes.put("settled", true);
								return AuthResultCode.APPROVED;
							case 1: // needs debit
								try {
									proxy.debit(terminalNumber, sequenceNumber, tenderNumber, adjustAmount,  manuallyEntered, cardNumber, pin, online, transactionTime, timeZone, encryptionKey);
								} catch(BlackboardException e) {
									log.warn("Could not process a sale against '" + authorityName + "'", e);
									return populateExceptionResults(action, taskInfo.getStep().getResultAttributes(), e);
								}
								return populateResults(adjustAmount, taskInfo.getStep().getResultAttributes());
							default:
								throw new RetrySpecifiedServiceException("Illegal State: Integer.signum() returned " + Integer.signum(adjustAmount),WorkRetryType.NO_RETRY);
						}
					case REFUND:
						adjustAmount = amount - originalAmount;
						switch(Integer.signum(adjustAmount)) {
							case -1: // needs debit
								try {
									proxy.debit(terminalNumber, sequenceNumber, tenderNumber, -adjustAmount,  manuallyEntered, cardNumber, pin, online, transactionTime, timeZone, encryptionKey);
								} catch(BlackboardException e) {
									log.warn("Could not process a sale against '" + authorityName + "'", e);
									return populateExceptionResults(action, taskInfo.getStep().getResultAttributes(), e);
								}
								return populateResults(adjustAmount, taskInfo.getStep().getResultAttributes());
							case 0: // nothing to do
								resultAttributes.put("authResultCd", 'Y');
								resultAttributes.put("authorityRespCd", 0);
								String text = "No change to account";
								resultAttributes.put("authorityRespDesc", text);
								resultAttributes.put("settled", true);
								return AuthResultCode.APPROVED;
							case 1: // needs credit
								try {
									proxy.refund(terminalNumber, sequenceNumber, tenderNumber, adjustAmount,  manuallyEntered, cardNumber, pin, online, transactionTime, timeZone, encryptionKey);
								} catch(BlackboardException e) {
									log.warn("Could not process a refund against '" + authorityName + "'", e);
									return populateExceptionResults(action, taskInfo.getStep().getResultAttributes(), e);
								}
								return populateResults(adjustAmount, taskInfo.getStep().getResultAttributes());
							default:
								throw new RetrySpecifiedServiceException("Illegal State: Integer.signum() returned " + Integer.signum(adjustAmount),WorkRetryType.NO_RETRY);
						}
					default:
						throw new RetrySpecifiedServiceException("Sub-action '" + subAction + "' for Action '" + action + "' NOT implemented",WorkRetryType.NO_RETRY);
				}
			default:
				throw new RetrySpecifiedServiceException("Action '" + action + "' NOT implemented", WorkRetryType.NO_RETRY);
		}
	}

	/**
	 * @see com.usatech.authoritylayer.AbstractAuthorityTask#processReversal(com.usatech.layers.common.constants.AuthorityAction, com.usatech.app.MessageChainTaskInfo)
	 */
	@Override
	protected AuthResultCode processReversal(AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo) throws ServiceException {
		Map<String, Object> resultAttributes = taskInfo.getStep().getResultAttributes();
		switch(action) {
			case AUTHORIZATION:
				// nothing to do b/c auth is a balance check
				resultAttributes.put("authResultCd", 'Y');
				resultAttributes.put("authorityRespCd", 0);
				String text = "No change to account";
				resultAttributes.put("authorityRespDesc", text);
				return AuthResultCode.APPROVED;
			default:
				text = "Reversal of Action '" + action + "' NOT implemented"; 
				log.warn(text);
				resultAttributes.put("authorityRespDesc", text);
				return AuthResultCode.FAILED;
		}
	}

	protected BlackboardProxyTransactionInterface getBlackboardProxy(String name, String preferredHost, String alternateHost, int port, int tiaProtocolVersion) throws BlackboardException {
		AtomicReference<BlackboardProxy> proxyRef = proxies.getOrCreate(name);
		while(true) {
			BlackboardProxy proxy = proxyRef.get();
			if(proxy == null) {
				BlackboardProxy newProxy = new BlackboardProxy(name, preferredHost, alternateHost, port, tiaProtocolVersion, getVendorNumber());
				if(proxyRef.compareAndSet(proxy, newProxy))
					return newProxy;
			} else if(!ConvertUtils.areEqual(proxy.getPreferredHost(), preferredHost)
					|| !ConvertUtils.areEqual(proxy.getAlternateHost(), alternateHost)
					|| proxy.getPort() != port
					|| proxy.getBlackboardProtocolVersion() != tiaProtocolVersion) {
				BlackboardProxy newProxy = new BlackboardProxy(name, preferredHost, alternateHost, port, tiaProtocolVersion, getVendorNumber());
				if(proxyRef.compareAndSet(proxy, newProxy)) {
					proxy.shutdown();
					return newProxy;
				}
			} else {
				return proxy;
			}
		}
	}

	public int getTiaProtocolVersion() {
		return tiaProtocolVersion;
	}

	public void setTiaProtocolVersion(int tiaProtocolVersion) {
		this.tiaProtocolVersion = tiaProtocolVersion;
	}

	protected AuthResultCode populateExceptionResults(AuthorityAction action, Map<String, Object> resultAttributes, BlackboardException e) {
		if(e.getResponseCode() > -1) {
			resultAttributes.put("authResultCd", 'N');
			resultAttributes.put("authorityRespCd", e.getResponseCode());
			StringBuilder sb = new StringBuilder();
			if(e.getResponseText() != null) sb.append(e.getResponseText());
			if(e.getDescription() != null) sb.append(": ").append(e.getDescription());
			resultAttributes.put("authorityRespDesc", sb.toString());
			return AuthResultCode.DECLINED;
		}
		return populateFailedResults(action, resultAttributes, null, e.getDescription());
	}
	protected AuthResultCode populateResults(int approvedAmount, Map<String, Object> resultAttributes) {
		resultAttributes.put("authResultCd", 'Y');
		resultAttributes.put("approvedAmount", approvedAmount);
		resultAttributes.put("settled", true);
		return AuthResultCode.APPROVED;
	}

	protected AuthResultCode populateAuthResults(int approvedAmount, String cardNumber, int requestedAmount, Map<String, Object> resultAttributes, boolean allowPartialAuth) {
		AuthResultCode authResultCode;
		if(approvedAmount >= requestedAmount) {
			resultAttributes.put("authResultCd", 'Y');
			resultAttributes.put("approvedAmount", requestedAmount);
			authResultCode = AuthResultCode.APPROVED;
		} else if(approvedAmount > 0 && allowPartialAuth) {
			resultAttributes.put("authResultCd", 'P');
			resultAttributes.put("approvedAmount", approvedAmount);
			authResultCode = AuthResultCode.PARTIAL;
		} else {
			resultAttributes.put("authResultCd", 'N');
			resultAttributes.put("authorityRespDesc", "Insufficient funds: " + approvedAmount);
			authResultCode = AuthResultCode.DECLINED;
		}
		resultAttributes.put("authHoldUsed", false);
		resultAttributes.put("authAccountData", new MaskedString(cardNumber));
		resultAttributes.put("balanceAmount", approvedAmount);
		resultAttributes.put("authorityRespCd", 0);
		//resultAttributes.put("authorityRespDesc", response.getResponseMessage());
		//resultAttributes.put("authAuthorityTranCd", response.getApprovalCode());
		//resultAttributes.put("authAuthorityRefCd", response.getRetrievalReferenceNumber());
		//resultAttributes.put("authAuthorityTs", response.getEffectiveDate());
		//resultAttributes.put("authAuthorityMiscData", sb.toString());
		return authResultCode;
	}
	protected static int sanitizeTraceNumber(long traceNumber, AuthorityAction action) {
		return 2 + action.ordinal() + 10 * (int)(Math.abs(traceNumber) % 100000); //1 - 999999
	}

	public String getVendorNumber() {
		return vendorNumber;
	}

	public void setVendorNumber(String vendorNumber) {
		this.vendorNumber = vendorNumber;
	}

	@Override
	protected String[] getAdditionalPreActionLogAttributes() {
		return new String[] {
			MessageAttribute.REMOTE_SERVER_ADDR.getValue(),
			MessageAttribute.REMOTE_SERVER_ADDR_ALT.getValue(),
			MessageAttribute.REMOTE_SERVER_PORT.getValue(),
			MessageAttribute.MERCHANT_CD.getValue(),
			MessageAttribute.TERMINAL_CD.getValue()
		};
	}
}
