/**
 *
 */
package com.usatech.authoritylayer;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import simple.app.AbstractService;
import simple.app.ServiceException;
import simple.app.ServiceStatus;
import simple.app.ServiceStatusInfo;
import simple.app.ServiceStatusListener;
import simple.io.Log;
import simple.util.MapBackedSet;
import simple.util.concurrent.TrivialFuture;

import com.usatech.iso8583.interchange.ISO8583Interchange;
import com.usatech.iso8583.transaction.ISO8583Transaction;
import com.usatech.iso8583.transaction.ISO8583TransactionManager;

/**
 * @author Brian S. Krug
 *
 */
public class InterchangeRecoveryService extends AbstractService implements Runnable {
	private static final Log log = Log.getLog();
	protected final Set<ServiceStatusListener> serviceStatusListeners = new MapBackedSet<ServiceStatusListener>(new ConcurrentHashMap<ServiceStatusListener, Object>()); //new LinkedHashSet<ServiceStatusListener>();
	protected final static int STATUS_STOPPED = 0;
	protected final static int STATUS_STARTING = 1;
	protected final static int STATUS_STARTED = 2;
	protected final static int STATUS_STOPPING = 3;
	protected final AtomicInteger status = new AtomicInteger(STATUS_STOPPED);
	protected final ScheduledExecutorService executor;
	protected long interval = 30000;
	protected volatile ScheduledFuture<?> canceller;
	protected InterchangeGatewayTask[] gatewayComponents;
	protected final Set<ISO8583Transaction> transactionsToRecover = new MapBackedSet<ISO8583Transaction>(new ConcurrentHashMap<ISO8583Transaction, Object>()); //new LinkedHashSet<ServiceStatusListener>();
	protected final AtomicBoolean loadedPending = new AtomicBoolean(false);
	protected int iteration;

	public InterchangeRecoveryService(String serviceName) throws Exception {
		super(serviceName);
		this.executor = new ScheduledThreadPoolExecutor(1);
	}

	@Override
	protected ServiceStatusInfo[] getCurrentServiceStatusInfos() {
		ServiceStatusInfo info = new ServiceStatusInfo();
		info.setIterationCount(iteration);
		info.setService(this);
		info.setServiceStatus(status.intValue() == STATUS_STARTED ? ServiceStatus.STARTED : ServiceStatus.STOPPED);
		info.setThread(null);
		return new ServiceStatusInfo[] { info };
	}

	public InterchangeGatewayTask[] getGatewayComponents() {
		return gatewayComponents;
	}

	public void setGatewayComponents(InterchangeGatewayTask[] gatewayComponents) {
		this.gatewayComponents = gatewayComponents;
	}

	/**
	 * @see simple.app.Service#getNumThreads()
	 */
	public int getNumThreads() {
		return status.get() == STATUS_STARTED ? 1 : 0;
	}

	/**
	 * @see simple.app.Service#pauseThreads()
	 */
	public int pauseThreads() throws ServiceException {
		return stopAllThreads(false, 0);
	}

	/**
	 * @see simple.app.Service#restartThreads(long)
	 */
	public int restartThreads(long timeout) throws ServiceException {
		stopAllThreads(false, timeout);
		return startThreads(1);
	}

	/**
	 * @see simple.app.Service#startThreads(int)
	 */
	public int startThreads(int numThreads) throws ServiceException {
		if(numThreads > 0) {
			if(status.compareAndSet(STATUS_STOPPED, STATUS_STARTING)) {
				if(loadedPending.compareAndSet(false, true)) {
					findPendingTransactions(); //Should this only happen once per jvm
				}
				canceller = executor.scheduleAtFixedRate(this, 0, getInterval(), TimeUnit.MILLISECONDS);
				status.compareAndSet(STATUS_STARTING, STATUS_STARTED);
				fireServiceStatusChanged(null, ServiceStatus.STARTED, ++iteration, null);
				return 1;
			}
		}
		return 0;
	}

	/**
	 *
	 */
	protected void findPendingTransactions() {
		InterchangeGatewayTask[] comps = getGatewayComponents();
		if(comps != null) {
			for(InterchangeGatewayTask comp : comps) {
				List<ISO8583Transaction> pendingList = comp.getTransactionManager().getPending(comp.getInterchange().getName());
				log.info("There are " + pendingList.size() + " pending transactions for interchange " + comp.getInterchange().getName());
				for (ISO8583Transaction pending : pendingList) {
					pending.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
					transactionsToRecover.add(pending);
				}
			}
		}
	}

	/**
	 * @see simple.app.Service#stopAllThreads(boolean, long)
	 */
	public int stopAllThreads(boolean force, long timeout) throws ServiceException {
		if(status.compareAndSet(STATUS_STARTED, STATUS_STOPPING)) {
			canceller.cancel(force);
			canceller = null;
			status.compareAndSet(STATUS_STOPPING, STATUS_STOPPED);
			fireServiceStatusChanged(null, ServiceStatus.STOPPED, iteration, null);
			return 1;
		}
		return 0;
	}

	@Override
	public Future<Integer> stopAllThreads(boolean force) throws ServiceException {
		return new TrivialFuture<Integer>(stopAllThreads(force, 0));
	}
	
	/**
	 * @see simple.app.Service#stopThreads(int, boolean, long)
	 */
	public int stopThreads(int numThreads, boolean force, long timeout) throws ServiceException {
		if(numThreads > 0) {
			return stopAllThreads(force, timeout);
		}
		return 0;
	}
	@Override
	public Future<Integer> stopThreads(int numThreads, boolean force) throws ServiceException {
		return new TrivialFuture<Integer>(stopThreads(numThreads, force, 0));
	}
	
	/**
	 * @see simple.app.Service#unpauseThreads()
	 */
	public int unpauseThreads() throws ServiceException {
		return startThreads(1);
	}
	public long getInterval() {
		return interval;
	}
	public void setInterval(long interval) {
		this.interval = interval;
	}
	/**
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		log.debug("Scanning for transactions that need recovery...");

		InterchangeGatewayTask[] comps = getGatewayComponents();
		if(comps != null) {
			for(InterchangeGatewayTask comp : comps) {
				ISO8583Interchange interchange = comp.getInterchange();
				ISO8583TransactionManager txnManager = comp.getTransactionManager();
				if(!interchange.isConnected()) {
					log.info("Skipping recovery: Interchange " + interchange.getName() + " is not connected");
					return;
				}
				List<ISO8583Transaction> recoverableList = txnManager.getNeedsRecovery(interchange.getName());
				transactionsToRecover.addAll(recoverableList);
				for(Iterator<ISO8583Transaction> iter = transactionsToRecover.iterator(); iter.hasNext(); ) {
					ISO8583Transaction transaction = iter.next();
					log.info("RECOVER: " + transaction);
					if(interchange.recover(transaction)) {
						txnManager.end(transaction);
						iter.remove();
						log.info("RECOVER SUCCESS: " + transaction);
					} else {
						log.error("RECOVER FAILURE: " + transaction);
					}
				}
			}
		}
	}
}
