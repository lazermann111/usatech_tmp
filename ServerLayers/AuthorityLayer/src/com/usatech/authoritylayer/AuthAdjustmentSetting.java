package com.usatech.authoritylayer;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import simple.text.StringUtils;

public class AuthAdjustmentSetting {
	protected boolean whenLess = true;
	protected boolean whenGreater = false;
	protected final Set<String> supportedCountries = new LinkedHashSet<>(Collections.singleton("US"));
	protected final Set<String> notSupportedCardTypes = new LinkedHashSet<>(Collections.singleton("AE"));
	protected final Map<String, Boolean> whenLessMerchantCds = new LinkedHashMap<>();
	protected final Map<String, Boolean> whenGreaterMerchantCds = new LinkedHashMap<>();

	public boolean isWhenLess() {
		return whenLess;
	}

	public void setWhenLess(boolean whenLess) {
		this.whenLess = whenLess;
	}

	public Boolean getWhenLessByMerchant(String merchantCd) {
		return whenLessMerchantCds.get(merchantCd);
	}

	public void setWhenLessByMerchant(String merchantCd, Boolean whenLess) {
		if(whenLess != null)
			whenLessMerchantCds.put(merchantCd, whenLess);
		else
			whenLessMerchantCds.remove(merchantCd);
	}

	public boolean doWhenLess(String merchantCd) {
		Boolean b = whenLessMerchantCds.get(merchantCd);
		return b != null ? b.booleanValue() : whenLess;
	}

	public boolean isWhenGreater() {
		return whenGreater;
	}

	public void setWhenGreater(boolean whenGreater) {
		this.whenGreater = whenGreater;
	}

	public Boolean getWhenGreaterByMerchant(String merchantCd) {
		return whenGreaterMerchantCds.get(merchantCd);
	}

	public void setWhenGreaterByMerchant(String merchantCd, Boolean whenGreater) {
		if(whenGreater != null)
			whenGreaterMerchantCds.put(merchantCd, whenGreater);
		else
			whenGreaterMerchantCds.remove(merchantCd);
	}

	public boolean doWhenGreater(String merchantCd) {
		Boolean b = whenGreaterMerchantCds.get(merchantCd);
		return b != null ? b.booleanValue() : whenGreater;
	}

	public boolean isWhenAmEx() {
		return isCardTypeSupported("AE");
	}

	public void setWhenAmEx(boolean whenAmEx) {
		if(whenAmEx)
			notSupportedCardTypes.remove("AE");
		else
			notSupportedCardTypes.add("AE");
	}

	public String[] getSupportedCountries() {
		return supportedCountries.toArray(new String[supportedCountries.size()]);
	}

	public void setSupportedCountries(String[] countryCds) {
		supportedCountries.clear();
		if(countryCds != null)
			for(String countryCd : countryCds)
				if(!StringUtils.isBlank(countryCd))
					supportedCountries.add(countryCd.trim().toUpperCase());
	}

	public boolean isSupportedCountry(String countryCd) {
		return !StringUtils.isBlank(countryCd) && supportedCountries.contains(countryCd.toUpperCase());
	}

	public int getSupportedCountryCount() {
		return supportedCountries.size();
	}

	public String[] getNotSupportedCardTypes() {
		return notSupportedCardTypes.toArray(new String[notSupportedCardTypes.size()]);
	}

	public void setNotSupportedCardTypes(String[] cardTypes) {
		notSupportedCardTypes.clear();
		if(cardTypes != null)
			for(String cardType : cardTypes)
				if(!StringUtils.isBlank(cardType))
					notSupportedCardTypes.add(cardType.trim().toUpperCase());
	}

	public boolean isCardTypeSupported(String cardType) {
		return !notSupportedCardTypes.contains(cardType == null ? "" : cardType);
	}
}
