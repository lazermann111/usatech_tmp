package com.usatech.authoritylayer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import simple.app.Publisher;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.ftp.SimpleFtpsBean;
import simple.io.ByteInput;
import simple.io.ByteOutput;
import simple.io.CopyInputStream;
import simple.io.Log;
import simple.io.OutputStreamByteOutput;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.results.DatasetHandler;
import simple.results.DatasetUtils;
import simple.text.DelimitedColumn;
import simple.text.MultiRecordDelimitedParser;
import simple.text.StringUtils;
import simple.text.ThreadSafeDateFormat;
import simple.text.StringUtils.Justification;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.SettlementFeedbackType;

import ftp.FtpException;
import ftp.FtpListResult;

public class ElavonFeedbackFileParserTask extends FtpSite implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected static final String[] tranFeedbackFileColumns = {
		AuthorityAttrEnum.ATTR_MERCHANT_CD.getValue(),
		AuthorityAttrEnum.ATTR_TERMINAL_CD.getValue(),
		AuthorityAttrEnum.ATTR_TERMINAL_BATCH_NUM.getValue(),
		AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS.getValue(),
		AuthorityAttrEnum.ATTR_SALE_AUTH_ID.getValue(),
		AuthorityAttrEnum.ATTR_REFUND_ID.getValue(),
		AuthorityAttrEnum.ATTR_TRAN_TYPE_CD.getValue(),
		AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue(),
		AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD.getValue(),
		AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD.getValue(),
		AuthorityAttrEnum.ATTR_AMT_APPROVED.getValue(),
		AuthorityAttrEnum.ATTR_AMOUNT.getValue(),
		AuthorityAttrEnum.ATTR_AUTH_TIME.getValue(),
		AuthorityAttrEnum.ATTR_IN_MAJOR_CURRENCY.getValue(),
	};
	protected static final String[] batchFeedbackFileColumns = {
		AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_CLASS.getValue(),
		AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS.getValue(),
		AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue(),
		AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD.getValue(),
		AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD.getValue(),
		AuthorityAttrEnum.ATTR_CREDIT_SALE_COUNT.getValue(),
		AuthorityAttrEnum.ATTR_CREDIT_SALE_AMOUNT.getValue(),
		AuthorityAttrEnum.ATTR_CREDIT_REFUND_COUNT.getValue(),
		AuthorityAttrEnum.ATTR_CREDIT_REFUND_AMOUNT.getValue(),
		AuthorityAttrEnum.ATTR_AMOUNT.getValue(),
		AuthorityAttrEnum.ATTR_IN_MAJOR_CURRENCY.getValue(),
		AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC.getValue(),	
	};
	protected Pattern acsFilenamePattern = Pattern.compile(".*\\.EDC");
	protected Pattern cnfFilenamePattern = Pattern.compile(".*\\.cnf");
	protected static final Pattern confirmDatePrefixPattern = Pattern.compile("(Confirmation|Email) date and time\\:", Pattern.CASE_INSENSITIVE);
	protected static final Pattern CNF_DATA_PATTERN = Pattern.compile("([\\w ]+)\\.+ (.+)");
	protected static final Pattern CNF_FILE_PATTERN = Pattern.compile("usatech_(\\w+)-(\\d{4})_P\\.(\\d{6})_(\\d{6}) file received\\.");
	protected static final Pattern ERROR_FILE_PATTERN = Pattern.compile("Fatal error for file usatech_(\\w+)-(\\d{4})_P\\.(\\d{6})_(\\d{6})\\.");
	protected static final Pattern ERROR_INDICATOR_PATTERN = Pattern.compile("(ERROR\\s+){2,}.*");
	protected static final NumberFormat cnfValueParser = new DecimalFormat("0");
	protected static final NumberFormat cnfNetAmountParser = new DecimalFormat("0 '+';0 '-'");
    protected String localCopyDirectory;
    protected final MultiRecordDelimitedParser<String> parser;
    protected ResourceFolder resourceFolder;
	protected Publisher<ByteInput> publisher;
	protected String paymentSubtypeClass = "Authority::ISO8583::Elavon";
	protected final ThreadSafeDateFormat settledDateFormat = new ThreadSafeDateFormat(new SimpleDateFormat("yyyyMMddHHmm"));
	protected final ThreadSafeDateFormat authDateFormat = new ThreadSafeDateFormat(new SimpleDateFormat("yyMMddHHmmss"));
    protected static enum FileType { ACS, CNF, UNKNOWN }
    
    protected class ACSFileHandler implements DatasetHandler<IOException> {
    	protected final ByteOutput output;
    	protected final Map<String,Object> data = new HashMap<String, Object>();
    	protected final Map<String,Object> batchData = new HashMap<String, Object>();
    	
    	public ACSFileHandler(ByteOutput output) {
			this.output = output;
		}
		public void handleDatasetEnd() throws IOException {
			DatasetUtils.writeFooter(output);
		}
		public void handleDatasetStart(String[] columnNames) throws IOException {
			DatasetUtils.writeHeader(output, tranFeedbackFileColumns);
		}
		public void handleRowEnd() throws IOException {
			if(log.isDebugEnabled()) {
				log.debug("Handling ACS Data: " + data.toString());
			}
			String recordType = (String)data.get("recordType");
			if("BH".equals(recordType)) {
				// set batch info
				batchData.clear();
				String settledDateText = getDetailAttribute("settlementDate", String.class, true) + getDetailAttribute("settlementTime", String.class, true); 
				long settledTime;
				try {
					settledTime = settledDateFormat.parse(settledDateText).getTime();
					batchData.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS.getValue(), settledTime);
				} catch(ParseException e) {
					log.error("Could not parse settled date '" + settledDateText + "'");
				}
		    	String terminalNumber = getDetailAttribute("terminalNumber", String.class, true);
				if(terminalNumber.length() > 16) {
					String merchantCd = StringUtils.unpad(terminalNumber.substring(0, terminalNumber.length() - 16), Justification.RIGHT, '0');
			    	String terminalCd = StringUtils.unpad(terminalNumber.substring(terminalNumber.length() - 16), Justification.RIGHT, '0');
			    	batchData.put(AuthorityAttrEnum.ATTR_TERMINAL_CD.getValue(), terminalCd);
			    	batchData.put(AuthorityAttrEnum.ATTR_MERCHANT_CD.getValue(), merchantCd);			    	
				}
		    	String batchNumber = getDetailAttribute("batchControlNumber", String.class, true); // XXX: I'm hoping this is the Batch number we send in the BHR record of ECF
		    	batchData.put(AuthorityAttrEnum.ATTR_TERMINAL_BATCH_NUM.getValue(), batchNumber);
			} else if("SI".equals(recordType)) {
				// write data to feedback file
				String merchantCd = (String)batchData.get(AuthorityAttrEnum.ATTR_MERCHANT_CD.getValue());
				String terminalCd = (String)batchData.get(AuthorityAttrEnum.ATTR_TERMINAL_CD.getValue());
				String batchNumber = (String)batchData.get(AuthorityAttrEnum.ATTR_TERMINAL_BATCH_NUM.getValue());
				Long settledTime = (Long)batchData.get(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS.getValue());
				String authTranCd = getDetailAttribute("authCode", String.class, true);
				//String authReferenceCd = StringUtils.pad(getDetailAttribute("referenceNumber", String.class, true), '0', 12, Justification.RIGHT);
				String authReferenceCd = null;
				Long referenceNumber = getDetailAttribute("referenceNumber", Long.class, true);
				BigDecimal amount = getDetailAttribute("transAmount", BigDecimal.class, true);
				BigDecimal origAmount = getDetailAttribute("origAuthAmount", BigDecimal.class, true);
				String authDateText = getDetailAttribute("authDate", String.class, true) + getDetailAttribute("authTime", String.class, true); 
				Long authTime;
				try {
					authTime = authDateFormat.parse(authDateText).getTime();
				} catch(ParseException e) {
					log.error("Could not parse auth date '" + authDateText + "'");
					authTime = null;
				}
				int tranCode = getDetailAttribute("tranCode", int.class, true);
		    	char authResultCd = 'Y';
		    	char tranTypeCd;
		    	switch(tranCode) {
		    		case 1: // RETAIL SALE
		    			tranTypeCd = '+';
		    			break;
		    		case 2: // RETAIL SALE WITH CASH BACK
		    			tranTypeCd = '+';
		    			break;
		    		case 3: // CASH ADVANCE
		    			tranTypeCd = '+';
		    			break;
		    		case 4: // MAIL ORDER
		    			tranTypeCd = '+';
		    			break;
		    		case 5: // TELEPHONE ORDER
		    			tranTypeCd = '+';
		    			break;
		    		case 6: // WIRE TRANSFER
		    			tranTypeCd = '+';
		    			break;
		    		case 7: // AVS ONLY TRANSACTION
		    			tranTypeCd = '?';
		    			break;
		    		case 8: // AUDIO RESPONSE UNIT (ARU)
		    			tranTypeCd = '?';
		    			break;
		    		case 10: // CHECK VERIFICATION/GUARANTEE
		    			tranTypeCd = '?';
		    			break;
		    		case 20: // PURCHASE RETURN
		    			if(origAmount.signum() == 0)
		    				tranTypeCd = '+';
		    			else
		    				tranTypeCd = '-';
		    			break;
		    		case 34: // PURCHASE/CONVERSION
		    			tranTypeCd = '?';
		    			break;
		    		case 35: // PURCHASE/CONVERSION/VERIFICATION
		    			tranTypeCd = '?';
		    			break;
		    		case 36: // PURCHASE/CONVERSION/GUARANTEE
		    			tranTypeCd = '?';
		    			break;
		    		case 37: // VOID
		    			tranTypeCd = '+';
		    			break;
		    		case 50: // MERCHANT SUSPICIOUS (AUTOMATIC REFERRAL)
		    			tranTypeCd = '?';
		    			break;
		    		case 60: // BALANCE INQUIRY
		    			tranTypeCd = '+';
		    			break;
		    		case 61: // PRE-AUTHORIZATION
		    			tranTypeCd = '+';
		    			break;
		    		case 62: // PRE-AUTHORIZATION COMPLETION
		    			tranTypeCd = '+';
		    			break;
		    		case 63: // RESUBMITTAL
		    			tranTypeCd = '+';
		    			break;
		    		case 80: // HOST-BASED TRANSACTION INQUIRY
		    			tranTypeCd = '?';
		    			break;
		    		case 81: // HOST-BASED BALANCE INQUIRY
		    			tranTypeCd = '?';
		    			break;
		    		case 82: // HOST-BASED ITEM CORRECTION
		    			tranTypeCd = '?';
		    			break;
		    		case 83: // HOST-BASED FORCE
		    			tranTypeCd = '?';
		    			break;
		    		case 84: // HOST-BASED CLEAR BATCH
		    			tranTypeCd = '?';
		    			break;
		    		case 90: // HYBRID SETTLE BATCH
		    			tranTypeCd = '?';
		    			break;
		    		case 99: // HYBRID CLEAR (PURGE) BATCH
		    			tranTypeCd = '?';
		    			break;
		    		default:
		    			tranTypeCd = '?';
		    	}
				DatasetUtils.writeRow(output, new Object[] {
						merchantCd,
						terminalCd,
						batchNumber,
						settledTime,
						tranTypeCd == '+' ? referenceNumber : null,
						tranTypeCd == '-' ? referenceNumber : null,
						tranTypeCd,
						authResultCd,
						authTranCd,
						authReferenceCd,
						amount,
						origAmount,
						authTime,
						true // amounts are in major currency
				});
			} 
			data.clear();
		}
		public void handleRowStart() throws IOException {
			// do nothing
		}
		public void handleValue(String columnName, Object value) throws IOException {
			data.put(columnName, value);
		}	
		protected <T> T getDetailAttribute(String attributeName, Class<T> convertTo, boolean required) throws IOException {
			try {
				if(required)
					return ConvertUtils.convertRequired(convertTo, data.get(attributeName));
				else
					return ConvertUtils.convert(convertTo, data.get(attributeName));
			} catch(ConvertException e) {
				throw new IOException(attributeName, e);
			}
		}
    }
    public ElavonFeedbackFileParserTask() {
		this.parser = new MultiRecordDelimitedParser<String>();
		parser.setColumnSeparator("^");
		parser.setLineSeparator("\r\n");
		parser.setIgnoringExtraColumns(true);
		parser.setSelectorColumn("recordType", String.class);
		parser.setRecordColumns("BH", new DelimitedColumn[] {
				new DelimitedColumn("settlementDate", String.class),//What format is this in???
				new DelimitedColumn("terminalNumber", String.class),
				new DelimitedColumn("batchNumber", int.class),
				new DelimitedColumn("fI#", int.class),
				new DelimitedColumn("agent#", int.class),
				new DelimitedColumn("merchantNumber", String.class),
				new DelimitedColumn("batchControlNumber", String.class),
				new DelimitedColumn("settlementTime", String.class),
				new DelimitedColumn("settlementType", String.class),
				new DelimitedColumn("autoCloseInd", String.class),
				new DelimitedColumn("applicationId", String.class),
				new DelimitedColumn("netAmount", BigDecimal.class), // 12.2 - float
				new DelimitedColumn("items", int.class),
				new DelimitedColumn("chain", int.class),
				new DelimitedColumn("sicCat", String.class),
				new DelimitedColumn("batchStatus", String.class),		
		});
			
		parser.setRecordColumns("SI", new DelimitedColumn[] {
				new DelimitedColumn("settlementDate", String.class),
				new DelimitedColumn("terminalNumber", String.class),
				new DelimitedColumn("batchNumber", int.class),
				new DelimitedColumn("itemNumber", int.class),
				new DelimitedColumn("tranCode", int.class),
				new DelimitedColumn("accountNumber", String.class),
				new DelimitedColumn("cardType", String.class),
				new DelimitedColumn("transAmount", BigDecimal.class), //8.2 - float
				new DelimitedColumn("authCode", String.class),
				new DelimitedColumn("authDate", String.class),
				new DelimitedColumn("authTime", String.class),
				new DelimitedColumn("customData", String.class),
				new DelimitedColumn("inputMethod", String.class),
				new DelimitedColumn("authSourceCode", String.class),
				new DelimitedColumn("voidInd", String.class),
				new DelimitedColumn("vps2KData", String.class),
				new DelimitedColumn("avsResult", String.class),
				new DelimitedColumn("origAuthAmount", BigDecimal.class), //8.2 - float
				new DelimitedColumn("referenceNumber", String.class), // use string since we'll pad with zeros
		});
	}

	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		ResourceFolder rf = getResourceFolder();
		if(rf == null) {
			log.error("ResourceFolder is not set", new ServiceException());
			return 1;
		}
		Publisher<ByteInput> publisher = getPublisher();
		if(publisher == null) {
			log.error("Publisher is not set", new ServiceException());
			return 1;
		}
		SimpleFtpsBean ftpsBean;
		try {
			ftpsBean = connect();
		} catch(IOException e) {
			throw new ServiceException(e);
		} catch(FtpException e) {
			throw new ServiceException(e);
		}
        File localCopyDir;
        String lcd = getLocalCopyDirectory();
        if(lcd != null && (lcd=lcd.trim()).length() > 0) {
        	localCopyDir = new File(lcd);
        	if(!localCopyDir.exists())
        		localCopyDir.mkdirs();
        } else
        	localCopyDir = null;
        // Make a client connection
        try {
        	String processQueue = taskInfo.getStep().getAttribute(AuthorityAttrEnum.ATTR_FEEDBACK_PROCESS_QUEUE, String.class, true);
            ftpsBean.setPort(port);
	        ftpsBean.setSocketTimeout(getSocketTimeout());
	        ftpsBean.ftpConnect(host, username, password);
	        try {	        	
	            // Change directory
	            if(remoteDir != null && remoteDir.trim().length() > 0) 
	            	ftpsBean.setDirectory(remoteDir);
	            FtpListResult dirList = ftpsBean.getDirectoryContent();
	            while(dirList.next()) {
	            	String filename = dirList.getName();
	            	if(dirList.getSize() > 0) {
		            	FileType fileType;
		            	if(acsFilenamePattern != null && acsFilenamePattern.matcher(filename).matches()) {
		            		fileType = FileType.ACS;
		            	} else if(cnfFilenamePattern != null && cnfFilenamePattern.matcher(filename).matches()) {
		            		fileType = FileType.CNF;
		            	} else {
		            		if(log.isDebugEnabled())
			            		log.debug("Ignoring unmatched file '" + filename +"'");
		            		continue;
		            	}
	            		log.info("Parsing settlement file '" + filename +"'");
	            		InputStream in = ftpsBean.getFileStream(filename, 0L);
	            		try {
		            		OutputStream copyOut;
		            		if(localCopyDir != null) {
		            			copyOut = new FileOutputStream(new File(localCopyDir, filename));
		            			in = new CopyInputStream(in, copyOut);
		            		} else {
		            			copyOut = null;
		            		}
		            		Resource resource = rf.getResource("SettlementFeedback-" + filename, ResourceMode.CREATE);	            		
		            		try {
		            			MessageChain mc = new MessageChainV11();
		            			MessageChainStep processStep = mc.addStep(processQueue);
		            			OutputStream out = resource.getOutputStream();
		            			try {
		            				switch(fileType) {
		            					case ACS:
		            						processACSFile(in, new OutputStreamByteOutput(out));
		            						processStep.setAttribute(AuthorityAttrEnum.ATTR_FEEDBACK_TYPE, SettlementFeedbackType.TRAN);
		            						break;
		            					case CNF:
		            						processCNFFile(in, new OutputStreamByteOutput(out), "ftps: " + ftpsBean.getUserName() + "@" + ftpsBean.getServerName() + ":" + ftpsBean.getPort() +
		            					            (remoteDir != null && remoteDir.trim().length() > 0 ? "/" + remoteDir : "") + "/" + filename); 
		            						processStep.setAttribute(AuthorityAttrEnum.ATTR_FEEDBACK_TYPE, SettlementFeedbackType.BATCH);
			            					break;
		            					default:
		            						throw new IllegalStateException("This should not happen");
		            				}
				            		if(localCopyDir != null) {
					            		copyOut.flush();
					            		copyOut.close();
				            		}
				            		out.flush();
		            			} finally {
		            				out.close();
		            			}
		            			processStep.setAttribute(AuthorityAttrEnum.ATTR_FEEDBACK_RESOURCE, resource.getKey());
		            			MessageChainService.publish(mc, publisher);///NOTE: We can not use taskInfo.getPublisher() or else it will not be committed until we return from this method
		            		} catch(IOException e) {
		                    	log.error("Could not process ACS file " + filename + "; continuing to process others", e); 
		                    	resource.delete();
		                    	continue; // don't delete
		                    } finally {
		                    	resource.release();
		                    }
	            		} finally {
	            			in.close();
	            		}
	                    if(log.isInfoEnabled())
	                    	log.info("Successfully parsed and posted for processing file '" + filename +"'. Deleting file.");
	            		try {
	            			ftpsBean.fileDelete(filename);
	            		} catch(FtpException e) {
	                    	log.warn("Could not delete ACS file " + filename + "; continuing to process others", e);   	
	                    } catch(IOException e) {
	                    	log.warn("Could not delete ACS file " + filename + "; continuing to process others", e);   	  	
	                    }
	            	} else if(log.isDebugEnabled()) {
	            		log.debug("Ignoring empty file '" + filename +"'");
	            	}
	            }
	        } finally {
	        	try {
	        		ftpsBean.close();
	        	} catch (NullPointerException e) {
					log.debug("FTP Connection was not open",e);
	            } catch (FtpException e) {
					log.warn("Could not close FTP Connection",e);
	            } catch(IOException ioe) {
	                log.warn("Could not close FTP Connection", ioe);
	            }
	        }
        } catch(FtpException e) {
        	log.error("Could not find and parse ACS files", e);
        	return 1;
        } catch(IOException e) {
        	log.error("Could not find and parse ACS files", e);   	
        	return 1;
        } catch(AttributeConversionException e) {
        	log.error("Could not convert attribute", e);   	
        	return 1;
		} 
        return 0;
	}

	protected void processCNFFile(InputStream in, ByteOutput output, String cnfFileName) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		String line;
		while((line=reader.readLine()) != null) {
			Matcher matcher = confirmDatePrefixPattern.matcher(line);
			if(matcher.find()) {
				Calendar cal;
				try {
					cal = ConvertUtils.convert(Calendar.class, line.substring(matcher.end()).trim());
				} catch(ConvertException e) {
					throw new IOException("Could not convert confirmation date and time", e);
				}
				String ecfFileName = null;
				int purchaseCount = -1;
				BigDecimal purchaseAmount = null;
				int returnCount = -1;
				BigDecimal returnAmount = null;
				//int totalCount = -1;
				BigDecimal netAmount = null;
				Character authResultCd = null;
				
				while((line=reader.readLine()) != null) {
					line = line.trim();
					Matcher errorMatcher = ERROR_INDICATOR_PATTERN.matcher(line);
					if(errorMatcher.matches()) {//file has an error
						authResultCd = 'F';
						while((line=reader.readLine()) != null) {
							line = line.trim();
							Matcher fileMatcher = ERROR_FILE_PATTERN.matcher(line);
							if(fileMatcher.matches()) {//file has an error
								ecfFileName = "P" + fileMatcher.group(2) + fileMatcher.group(3) + fileMatcher.group(4) + '.' + fileMatcher.group(1);
								break;
							}
						}
						break;
					} else {
					Matcher dataMatcher = CNF_DATA_PATTERN.matcher(line);
					if(dataMatcher.matches()) {
						String name = dataMatcher.group(1);
						String value = dataMatcher.group(2);
						if(name != null && value != null && (value=value.trim()).length() > 0) {
							try {
							if("Purchases Count".equalsIgnoreCase(name))
								purchaseCount = cnfValueParser.parse(value).intValue();
							else if("Purchases Amount".equalsIgnoreCase(name))
								purchaseAmount = ConvertUtils.convert(BigDecimal.class, cnfValueParser.parse(value));
							else if("Return Count".equalsIgnoreCase(name))
								returnCount = cnfValueParser.parse(value).intValue();
							else if("Return Amount".equalsIgnoreCase(name))
								returnAmount = ConvertUtils.convert(BigDecimal.class, cnfValueParser.parse(value));
							else if("Total Count".equalsIgnoreCase(name)) {
								//totalCount = cnfValueParser.parse(value).intValue();
							} else if("Net Amount".equalsIgnoreCase(name))
								netAmount = ConvertUtils.convert(BigDecimal.class, cnfNetAmountParser.parse(value));
							} catch(ParseException e) {
								throw new IOException("Could not parse value '" + value + "' for '" + name + "' in file", e);
							} catch(ConvertException e) {
								throw new IOException("Could not convert value '" + value + "' for '" + name + "' in file", e);
							}
						}
					} else if(line.length() > 0) {
						Matcher fileMatcher = CNF_FILE_PATTERN.matcher(line);
						if(fileMatcher.matches()) {
							authResultCd = 'Y';
							ecfFileName = "P" + fileMatcher.group(2) + fileMatcher.group(3) + fileMatcher.group(4) + '.' + fileMatcher.group(1);
							break;
						}
					}
					}
				}
				if(ecfFileName == null)
					throw new IOException("File does not contain the original file name");
				DatasetUtils.writeHeader(output, batchFeedbackFileColumns);			
				DatasetUtils.writeRow(output, new Object[] {
						paymentSubtypeClass, // ATTR_PAYMENT_SUBTYPE_CLASS
						cal.getTimeInMillis(), // ATTR_AUTH_AUTHORITY_TS
						authResultCd, // ATTR_AUTH_RESULT_CD
						null, // ATTR_AUTH_AUTHORITY_TRAN_CD
						ecfFileName,// ATTR_AUTH_AUTHORITY_REF_CD
						purchaseCount,// ATTR_CREDIT_SALE_COUNT
						purchaseAmount, // ATTR_CREDIT_SALE_AMOUNT
						returnCount, // ATTR_CREDIT_REFUND_COUNT
						returnAmount, // ATTR_CREDIT_REFUND_AMOUNT
						netAmount, // ATTR_AMOUNT
						true, // ATTR_IN_MAJOR_CURRENCY - amounts are in major currency
						cnfFileName
				});
				
				DatasetUtils.writeFooter(output);	
				return;
			}
		}
		throw new IOException("Never found '" + confirmDatePrefixPattern.pattern() + "' in the file");
	}

	protected void processACSFile(InputStream fileStream, ByteOutput output) throws IOException {
		parser.parse(new InputStreamReader(fileStream), new ACSFileHandler(output));
	}

	public String getAcsFilenamePattern() {
		return (acsFilenamePattern == null ? null : acsFilenamePattern.pattern());
	}

	public void setAcsFilenamePattern(String acsFilenamePattern) {
		if(acsFilenamePattern == null || (acsFilenamePattern=acsFilenamePattern.trim()).length() == 0)
			this.acsFilenamePattern = null;
		else
			this.acsFilenamePattern = Pattern.compile(acsFilenamePattern);
	}

	public String getCnfFilenamePattern() {
		return (cnfFilenamePattern == null ? null : cnfFilenamePattern.pattern());
	}

	public void setCnfFilenamePattern(String cnfFilenamePattern) {
		if(cnfFilenamePattern == null || (cnfFilenamePattern=cnfFilenamePattern.trim()).length() == 0)
			this.cnfFilenamePattern = null;
		else
			this.cnfFilenamePattern = Pattern.compile(cnfFilenamePattern);
	}

	public String getLocalCopyDirectory() {
		return localCopyDirectory;
	}

	public void setLocalCopyDirectory(String localCopyDirectory) {
		this.localCopyDirectory = localCopyDirectory;
	}

	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}

	public Publisher<ByteInput> getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}

	public String getPaymentSubtypeClass() {
		return paymentSubtypeClass;
	}

	public void setPaymentSubtypeClass(String paymentSubtypeClass) {
		this.paymentSubtypeClass = paymentSubtypeClass;
	}
}
