/**
 *
 */
package com.usatech.authoritylayer;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.security.GeneralSecurityException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ThreadPoolExecutor;

import javax.crypto.CipherInputStream;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.configuration.Configuration;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import simple.app.Prerequisite;
import simple.app.ResetAvailabilityListener;
import simple.app.ResetAvailabilityTrigger;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.io.InputStreamByteInput;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.lang.InvalidValueException;
import simple.results.DatasetHandler;
import simple.results.DatasetUtils;
import simple.text.StringUtils;
import simple.xml.ObjectBuilder;

import com.usatech.app.AbstractAttributeDatasetHandler;
import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.iso8583.BatchTotals;
import com.usatech.iso8583.ISO8583Message;
import com.usatech.iso8583.ISO8583Request;
import com.usatech.iso8583.ISO8583Response;
import com.usatech.iso8583.PS2000;
import com.usatech.iso8583.PanData;
import com.usatech.iso8583.TrackData;
import com.usatech.iso8583.interchange.ISO8583Interchange;
import com.usatech.iso8583.interchange.elavon.ElavonInterchange;
import com.usatech.iso8583.interchange.fhms.FHMSInterchange;
import com.usatech.iso8583.transaction.ISO8583Transaction;
import com.usatech.iso8583.transaction.ISO8583TransactionManager;
import com.usatech.layers.common.Cryption;
import com.usatech.layers.common.DeleteResourceTask;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAction;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.DeniedReason;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.MessageAttrEnum;
import com.usatech.layers.common.constants.MessageAttribute;
import com.usatech.layers.common.constants.PaymentMaskBRef;

/**
 * @author Brian S. Krug
 *
 */
public class InterchangeGatewayTask extends AbstractAuthorityTask implements Prerequisite, ResetAvailabilityTrigger {
	private static final Log log = Log.getLog();
	private static final Log transactionLog = Log.getLog("TRANSACTION_LOG");
	protected ISO8583Interchange interchange;
	protected Class<? extends ISO8583Interchange> interchangeClass;
	protected InterchangeGlobalsFactory globalsFactory;
	protected ISO8583TransactionManager transactionManager;
	protected Map<String, AuthResultCode> responseMessageResultCd = new HashMap<String, AuthResultCode>();
	protected Map<String, AuthResultCode> responseCodeResultCd = new HashMap<String, AuthResultCode>();
	protected Map<String, DeniedReason> responseMessageDeniedReason = new HashMap<String, DeniedReason>();
	protected Map<String, DeniedReason> responseCodeDeniedReason = new HashMap<String, DeniedReason>();
	protected boolean authHoldUsed = true;
	protected boolean saleReversalSupported = true;
	protected boolean authReversalSupported = false;
	protected boolean reuploadSupported = false;
	protected boolean redeliverySupported = true;
	protected boolean partialAuthAllowed = false;
	protected String declineAvsResults = "ABCN";
	protected String declineCvvResults = "N";
	protected boolean allowAvsMismatch = true;
	protected boolean allowCvvMismatch = true;
	protected ResourceFolder resourceFolder;
	protected final Set<ResetAvailabilityListener> availabilityListeners = new HashSet<ResetAvailabilityListener>();
	protected int minimumAuthAmount = 0;
	protected final Map<String, String> avsResultDescriptions = new HashMap<String, String>();
	protected final Map<String, String> cvvResultDescriptions = new HashMap<String, String>();

	protected class UploadGenerator extends AbstractAttributeDatasetHandler<AttributeConversionException> {
		protected final List<ISO8583Request> list;	
		protected final MessageChainStep step;
		public UploadGenerator(List<ISO8583Request> list, MessageChainStep step) {
			super();
			this.list = list;
			this.step = step;
		}
		@Override
		public void handleDatasetStart(String[] columnNames) throws AttributeConversionException {
		}
		@Override
		public void handleDatasetEnd() throws AttributeConversionException {
		}
		@Override
		public void handleRowEnd() throws AttributeConversionException {
			ISO8583Request request = new ISO8583Request();
			request.setAcquirerID(step.getAttribute(AuthorityAttrEnum.ATTR_MERCHANT_CD, String.class, true));
			request.setAmount(getDetailAttribute(AuthorityAttrEnum.ATTR_AMOUNT, Integer.class, true));
			request.setApprovalCode(getDetailAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD, String.class, false));
			request.setBatchNumber(step.getAttribute(AuthorityAttrEnum.ATTR_TERMINAL_BATCH_NUM, Integer.class, true));
			request.setEntryMode(translateEntryMethod(getDetailAttribute(AuthorityAttrEnum.ATTR_ENTRY_TYPE, Byte.class, true)));
			String validationData = getDetailAttribute(AuthorityAttrEnum.ATTR_VALIDATION_DATA, String.class, false);
			if(validationData != null && (validationData=validationData.trim()).length() > 0) {
				Map<String,String> validationMap = new HashMap<String, String>();
				try {
					StringUtils.intoMap(validationMap, validationData, '=', '|', StringUtils.STANDARD_QUOTES, true, false);
				} catch(ParseException e) {
					throw new AttributeConversionException(AuthorityAttrEnum.ATTR_VALIDATION_DATA, new ConvertException("", Map.class, validationData, e));
				}
				request.setAvsAddress(validationMap.get("address"));
				request.setAvsZip(validationMap.get("zip"));
				if(!StringUtils.isBlank(getDeclineCvvResults()))
					request.setCvv2(validationMap.get("cvv2"));
			}
			AccountData accountData = new AccountData(data, true);
			if(!StringUtils.isBlank(accountData.getTrackData())) {
				request.setTrackData(new TrackData(accountData.getTrackData(), null, null));
			} else {
				String pan = accountData.getPartData(PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER);
				if(!StringUtils.isBlank(pan))
					request.setPanData(new PanData(accountData.getPartData(PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER), accountData.getPartData(PaymentMaskBRef.EXPIRATION_DATE)));
				if(!StringUtils.isBlank(getDeclineCvvResults())) {
					String cvv2 = accountData.getPartData(PaymentMaskBRef.DISCRETIONARY_DATA);
					if(cvv2 != null && cvv2.length() > 0)
						request.setCvv2(cvv2);
				}
				String postal = accountData.getPartData(PaymentMaskBRef.ZIP_CODE);
				if(postal != null && postal.length() > 0)
					request.setAvsZip(postal);
				String address = accountData.getPartData(PaymentMaskBRef.ADDRESS);
				if(address != null && address.length() > 0)
					request.setAvsAddress(address);
			}
			request.setEffectiveDate(getDetailAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS, Date.class, false));
			request.setHostResponseTimeout(step.getAttribute(AuthorityAttrEnum.ATTR_RESPONSE_TIME_OUT, Integer.class, true));
			request.setOnline(true);
			String referenceCd = getDetailAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, String.class, false); 
			request.setOriginalTraceNumber(sanitizeTraceNumber(ConvertUtils.getIntSafely(referenceCd, 0)));
			
			//request.setPinEntryCapability(pinEntryCapability); //XXX: not sure what this is
			String posEnv = getDetailAttribute(AuthorityAttrEnum.ATTR_POS_ENVIRONMENT, String.class, false);
			if(posEnv == null)
				posEnv = "A";
			request.setPosEnvironment(posEnv);

			String authAuthorityMiscData = getDetailAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, String.class, false);
			if(authAuthorityMiscData != null && authAuthorityMiscData.length() > 0) {
				if(interchange instanceof FHMSInterchange) {
					ObjectBuilder builder;
					try {
						builder = new ObjectBuilder(new InputSource(new StringReader(authAuthorityMiscData)), null);
					} catch(IOException e) {
						throw new AttributeConversionException(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, new ConvertException("", PS2000.class, authAuthorityMiscData, e));
					} catch(SAXException e) {
						throw new AttributeConversionException(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, new ConvertException("", PS2000.class, authAuthorityMiscData, e));
					} catch(ParserConfigurationException e) {
						throw new AttributeConversionException(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, new ConvertException("", PS2000.class, authAuthorityMiscData, e));
					}
					request.setPS2000(new PS2000(builder.getAttrValue("indicator"),
							builder.getAttrValue("identifier"), builder.getAttrValue("validationCode") ,
							builder.getAttrValue("responseCode"), builder.getAttrValue("entryMode") ));
				} else
					request.setMiscData(authAuthorityMiscData);
			}
			request.setRetrievalReferenceNumber(referenceCd);
			request.setTerminalID(step.getAttribute(AuthorityAttrEnum.ATTR_TERMINAL_CD, String.class, true));
			request.setTraceNumber(sanitizeTraceNumber(getDetailAttribute(AuthorityAttrEnum.ATTR_TRACE_NUMBER, Long.class, true))); //TODO: we may need to get this from somewhere else
			
			Number saleAuthId = getDetailAttribute(AuthorityAttrEnum.ATTR_SALE_AUTH_ID, Number.class, false);
			//Number refundId = getDetailAttribute(AuthorityAttrEnum.ATTR_REFUND_ID, Number.class, false);
			
			if(saleAuthId != null)
				request.setTransactionSubType(AuthorityAction.SALE.toString());
			else
				request.setTransactionSubType(AuthorityAction.REFUND.toString());
			request.setTransactionType("UPLOAD");	
			list.add(request);
		}
	}

	public InterchangeGatewayTask() {
		super("interchange");
	}

	protected void populateStubResultAttributes(Map<String,Object> resultAttributes, int amount, String authorityRespDesc) {
		resultAttributes.put("approvedAmount", amount);
		resultAttributes.put("authResultCd", AuthResultCode.APPROVED.getValue());
		resultAttributes.put("authorityRespCd", "-");
		resultAttributes.put("authorityRespDesc", authorityRespDesc);
		resultAttributes.put("authAuthorityTs", new Date());
		resultAttributes.put("settled", true);	
	}
	@Override
	protected AuthResultCode processRequest(AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		if(!isSaleReversalSupported()) {
			switch(action) {
				case SALE: case REFUND: // check reversal
					int amount;
					try {
						amount = getAttribute(step, AuthorityAttrEnum.ATTR_AMOUNT, Integer.class, true);
					} catch(AuthorityAttributeConversionException e) {
						return AuthResultCode.FAILED;
					}
					if(amount == 0) {
						populateStubResultAttributes(step.getResultAttributes(), amount, "Reversals not supported");
						return AuthResultCode.APPROVED;
					}
					break;
			}
		}
		
		ISO8583Interchange interchange = getInterchange();
		ISO8583TransactionManager txnManager = getTransactionManager();
		ISO8583Request request;
		try {
			request = createRequest(action, subAction, taskInfo);
		} catch(AuthorityAttributeConversionException e) {
			log.error("Could not convert data for request", e);
			return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Could not convert data for request");
		} catch(ParseException e) {
			log.error("Could not convert data for request", e);
			return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Could not convert data for request");
		} catch(IOException e) {
			log.error("Could not convert data for request", e);
			return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Could not convert data for request");
		} catch(SAXException e) {
			log.error("Could not convert data for request", e);
			return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Could not convert data for request");
		} catch(ParserConfigurationException e) {
			log.error("Could not convert data for request", e);
			return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Could not convert data for request");
		} catch(GeneralSecurityException e) {
			log.error("Could not convert data for request", e);
			return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Could not convert data for request");
		} catch(AttributeConversionException e) {
			log.error("Could not convert data for request", e);
			return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Could not convert data for request");
		} 
		if(request.getTransactionType() == null) {
			log.error("Request denied: Missing Transaction Type");
			//return new ISO8583Response(ISO8583Response.ERROR_EMPTY_TRANSACTION_TYPE, "Missing Transaction Type");
			return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Request denied: Missing Transaction Type");
		}

		ISO8583Transaction transaction = txnManager.start(request, interchange.getName());
		if(transaction == null) {
			log.error("Failed to start a new transaction, aborting processing and returing error response!");
			//return new ISO8583Response(ISO8583Response.ERROR_INTERNAL_ERROR, "Failed to start a new transaction!");
			return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Failed to start a new transaction, aborting processing and returing error response!");
		}

		log.info("REQUEST: " + transaction + ": " + request);
		transactionLog.info(interchange.getName() + "," + transaction.getTransactionID() + ",REQ," + request);

		ISO8583Response response = interchange.process(transaction);
		if(ISO8583Response.ERROR_HOST_CONNECTION_FAILURE.equals(response.getResponseCode())) {
			fireResetAvailability();
		}
		if(transaction.getState() != ISO8583Transaction.STATE_NEEDS_RECOVERY) {
			txnManager.end(transaction);
		} else {
			log.warn(transaction + " will be queued for recovery");
			taskInfo.getStep().getResultAttributes().put("authHoldUsed", true);
		}
		log.info("RESPONSE: " + transaction + ": " + response);
		transactionLog.info(interchange.getName() + "," + transaction.getTransactionID() + ",RES," + response);

		return populateResults(request.getAmount(), action, response, taskInfo.getStep().getResultAttributes(), step);
	}

	@Override
	protected AuthResultCode processReversal(AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo) throws ServiceException {
		switch(action) {
			case AUTHORIZATION:
				if(isAuthReversalSupported()) {
					return processRequest(AuthorityAction.REVERSAL, action, taskInfo);
				}
				if(isSaleReversalSupported())
					taskInfo.getStep().getResultAttributes().put("authHoldUsed", true); // this is so that a 0$ sale can be sent back with the batch
				return AuthResultCode.FAILED;
			default:
				if(isRedeliverySupported())
					return AuthResultCode.APPROVED;
				log.warn("Reversal of Action '" + action + "' NOT implemented");
				return AuthResultCode.FAILED;
		}		
	}
	protected ISO8583Request createRequest(AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo) throws ParseException, IOException, SAXException, ParserConfigurationException, AuthorityAttributeConversionException, GeneralSecurityException, AttributeConversionException, ServiceException {
		MessageChainStep step = taskInfo.getStep();
		ISO8583Request request = new ISO8583Request();
		request.setAcquirerID(getAttribute(step, AuthorityAttrEnum.ATTR_MERCHANT_CD, String.class, true));
		switch(action) {
			case SETTLEMENT_RETRY:				
				if(isReuploadSupported()) {
					String resourceKey = getAttribute(step, AuthorityAttrEnum.ATTR_CAPTURE_DETAILS_RESOURCE, String.class, false);
					if(resourceKey != null && (resourceKey=resourceKey.trim()).length() > 0) {
						String sensitiveResourceKey = getAttribute(step, CommonAttrEnum.ATTR_CRYPTO_RESOURCE, String.class, false);		
				        byte[] key = getAttribute(step, AuthorityAttrEnum.ATTR_CAPTURE_DETAILS_ENCRYPTION_KEY, byte[].class, true);
				        String cipherName = getAttribute(step, AuthorityAttrEnum.ATTR_CAPTURE_DETAILS_CIPHER, String.class, true);	
				        int blockSize = getAttribute(step, AuthorityAttrEnum.ATTR_CAPTURE_DETAILS_BLOCK_SIZE, Integer.class, true);
						ResourceFolder rf = getResourceFolder();
						if(rf == null)
							throw new IOException("ResourceFolder is not set");
						Resource resource = null;
						InputStream cis = null;
						Resource sensitiveResource = null;
			        	CipherInputStream sensitiveCis = null;			        	
						boolean okay = false;
						try {
							resource = rf.getResource(resourceKey, ResourceMode.READ);
							cis = Cryption.createDecryptingInputStream(Cryption.DEFAULT_PROVIDER, cipherName, key, blockSize, resource.getInputStream());
				    		if(sensitiveResourceKey != null && sensitiveResourceKey.trim().length() > 0) {
				    			sensitiveResource = rf.getResource(sensitiveResourceKey, ResourceMode.READ);
					    		sensitiveCis = Cryption.createDecryptingInputStream(Cryption.DEFAULT_PROVIDER, cipherName, key, blockSize, sensitiveResource.getInputStream());
				    		}
			            	// create sub-requests
				    		List<ISO8583Request> transactionList = new ArrayList<ISO8583Request>();
				    		DatasetHandler<AttributeConversionException> uploadGenerator = new UploadGenerator(transactionList, step);
				    		if(sensitiveCis == null) 
			            		DatasetUtils.parseDataset(new InputStreamByteInput(cis), uploadGenerator);
			            	else
			            		DatasetUtils.parseAndMergeDatasets(uploadGenerator, new InputStreamByteInput(cis), new InputStreamByteInput(sensitiveCis));
				    		request.setTransactionList(transactionList);
				    		okay = true;
			    		} finally {
			    			if(cis != null)
			    				cis.close();
			    			if(resource != null) {
								if(okay)
									DeleteResourceTask.requestResourceDeletion(resource, taskInfo.getPublisher());
								resource.release();
				        	}
			    			if(sensitiveCis != null)
								sensitiveCis.close();
							if(sensitiveResource != null) {
								if(okay)
									DeleteResourceTask.requestResourceDeletion(sensitiveResource, taskInfo.getPublisher());
					        	sensitiveResource.release();
				        	}
						}
					}
				}
				// fall-thru
			case SETTLEMENT: 	
				request.setBatchNumber(getAttribute(step, AuthorityAttrEnum.ATTR_TERMINAL_BATCH_NUM, Integer.class, true));
				request.setBatchTotals(new BatchTotals(
						getAttribute(step, AuthorityAttrEnum.ATTR_CREDIT_SALE_COUNT, Integer.class, true),
						getAttribute(step, AuthorityAttrEnum.ATTR_CREDIT_SALE_AMOUNT, Integer.class, true),
						getAttribute(step, AuthorityAttrEnum.ATTR_CREDIT_REFUND_COUNT, Integer.class, true),
						getAttribute(step, AuthorityAttrEnum.ATTR_CREDIT_REFUND_AMOUNT, Integer.class, true)));
				break;
			case ADJUSTMENT:
				Integer originalAmount = getAttribute(step, AuthorityAttrEnum.ATTR_ORIGINAL_AMOUNT, Integer.class, false);
				if(originalAmount != null)
					request.setOriginalAmount(originalAmount);
				Integer otn = getAttribute(step, AuthorityAttrEnum.ATTR_ORIGINAL_TRACE_NUMBER, Integer.class, false);
				if(otn != null)
					request.setOriginalTraceNumber(sanitizeTraceNumber(otn));
				// Fall-through also
			default:
				int amount = getAttribute(step, AuthorityAttrEnum.ATTR_AMOUNT, Integer.class, true);
				// Elavon has serious issues with auth amounts <= $1 so the kludge below is for them
				if(action == AuthorityAction.AUTHORIZATION) {
					if(amount < getMinimumAuthAmount())
						amount = getMinimumAuthAmount();
					else if(interchange instanceof ElavonInterchange && amount < 101)
						amount = 120;
				}
				request.setAmount(amount);
				if(action == AuthorityAction.AUTHORIZATION && isPartialAuthAllowed()) {
					boolean allowPartialAuth = getAttributeDefault(step, AuthorityAttrEnum.ATTR_PARTIAL_AUTH_ALLOWED, Boolean.class, false);
					request.setPartialAuthAllowed(allowPartialAuth);
				}

				request.setApprovalCode(getAttribute(step, AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD, String.class, action == AuthorityAction.ADJUSTMENT));
				request.setEntryMode(translateEntryMethod(getAttribute(step, AuthorityAttrEnum.ATTR_ENTRY_TYPE, Byte.class, true)));
				String validationData = getAttribute(step, AuthorityAttrEnum.ATTR_VALIDATION_DATA, String.class, false);
				if(validationData != null && (validationData=validationData.trim()).length() > 0) {
					Map<String,String> validationMap = new HashMap<String, String>();
					StringUtils.intoMap(validationMap, validationData, '=', '|', StringUtils.STANDARD_QUOTES, true, false);
					request.setAvsAddress(validationMap.get("address"));
					request.setAvsZip(validationMap.get("zip"));
					if(!StringUtils.isBlank(getDeclineCvvResults()))
						request.setCvv2(validationMap.get("cvv2"));
				}
				AccountData accountData = new AccountData(step.getAttributes());
				if(!StringUtils.isBlank(accountData.getTrackData())) {
					request.setTrackData(new TrackData(accountData.getTrackData(), null, null));
				} else {
					String pan = accountData.getPartData(PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER);
					if(!StringUtils.isBlank(pan))
						request.setPanData(new PanData(pan, accountData.getPartData(PaymentMaskBRef.EXPIRATION_DATE)));
				}
				if(!StringUtils.isBlank(getDeclineCvvResults())) {
					String cvv2 = accountData.getPartData(PaymentMaskBRef.DISCRETIONARY_DATA);
					if(cvv2 != null && cvv2.length() > 0)
						request.setCvv2(cvv2);
				}
				String postal = accountData.getPartData(PaymentMaskBRef.ZIP_CODE);
				if(postal != null && postal.length() > 0)
					request.setAvsZip(postal);
				String address = accountData.getPartData(PaymentMaskBRef.ADDRESS);
				if(address != null && address.length() > 0)
					request.setAvsAddress(address);
		}
		request.setEffectiveDate(getAttribute(step, AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS, Date.class, action == AuthorityAction.ADJUSTMENT));
		request.setHostResponseTimeout(getAttribute(step, AuthorityAttrEnum.ATTR_RESPONSE_TIME_OUT, Integer.class, true));
		Boolean online = getAttribute(step, AuthorityAttrEnum.ATTR_ONLINE, Boolean.class, action == AuthorityAction.SALE || action == AuthorityAction.REFUND);
		if(online != null) {
			if(!online && request.hasTrackData())
				online = true;
			request.setOnline(online);
		}

		String posEnv = getAttributeDefault(step, AuthorityAttrEnum.ATTR_POS_ENVIRONMENT, String.class, ISO8583Message.POS_ENVIRONMENT_UNSPECIFIED);
		request.setPosEnvironment(posEnv);

		Boolean pinEntryCapability = getAttribute(step, AuthorityAttrEnum.ATTR_PIN_CAPABILITY, Boolean.class, false);
		if(pinEntryCapability == null)
			request.setPinEntryCapability(ISO8583Message.PIN_ENTRY_UNSPECIFIED);
		else if(pinEntryCapability)
			request.setPinEntryCapability(ISO8583Message.PIN_ENTRY_CAPABILITY);
		else
			request.setPinEntryCapability(ISO8583Message.PIN_ENTRY_NO_CAPABILITY);

		String entryCapability = getAttribute(step, AuthorityAttrEnum.ATTR_ENTRY_CAPABILITY, String.class, false);
		if(!StringUtils.isBlank(entryCapability))
			request.setPosEntryCapability(entryCapability);

		String deviceName = getAttribute(step, MessageAttrEnum.ATTR_DEVICE_NAME, String.class, false);
		if(!StringUtils.isBlank(deviceName))
			request.setPosIdentifier(deviceName);

		String invoiceNumber = getAttribute(step, AuthorityAttrEnum.ATTR_INVOICE_NUMBER, String.class, false);
		if(!StringUtils.isBlank(invoiceNumber))
			request.setInvoiceNumber(invoiceNumber);

		String doingBusinessAs = getAttribute(step, AuthorityAttrEnum.ATTR_DOING_BUSINESS_AS, String.class, false);
		if(!StringUtils.isBlank(doingBusinessAs))
			request.setDoingBusinessAs(doingBusinessAs);

		String street = getAttribute(step, AuthorityAttrEnum.ATTR_ADDRESS, String.class, false);
		if(!StringUtils.isBlank(street))
			request.setStreet(street);

		String city = getAttribute(step, AuthorityAttrEnum.ATTR_CITY, String.class, false);
		if(!StringUtils.isBlank(city))
			request.setCity(city);

		String stateCd = getAttribute(step, AuthorityAttrEnum.ATTR_STATE_CD, String.class, false);
		if(!StringUtils.isBlank(stateCd))
			request.setState(stateCd);

		String postal = getAttribute(step, AuthorityAttrEnum.ATTR_POSTAL, String.class, false);
		if(!StringUtils.isBlank(postal))
			request.setPostal(postal);

		String countryCd = getAttribute(step, AuthorityAttrEnum.ATTR_COUNTRY_CD, String.class, false);
		if(!StringUtils.isBlank(countryCd))
			request.setCountryCode(countryCd);

		String phone = getAttribute(step, AuthorityAttrEnum.ATTR_CUSTOMER_SERVICE_PHONE, String.class, false);
		if(!StringUtils.isBlank(phone))
			request.setPhone(phone);

		String email = getAttribute(step, AuthorityAttrEnum.ATTR_CUSTOMER_SERVICE_EMAIL, String.class, false);
		if(!StringUtils.isBlank(email))
			request.setEmail(email);

		Long customerId = getAttribute(step, AuthorityAttrEnum.ATTR_CUSTOMER_ID, Long.class, false);
		if(customerId != null)
			request.setCustomerId(customerId);

		String authAuthorityMiscData = getAttribute(step, AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, String.class, false);
		if(authAuthorityMiscData != null && authAuthorityMiscData.length() > 0) {
			if(interchange instanceof FHMSInterchange) {
				ObjectBuilder builder = new ObjectBuilder(new InputSource(new StringReader(authAuthorityMiscData)), null);
				request.setPS2000(new PS2000(builder.getAttrValue("indicator"),
						builder.getAttrValue("identifier"), builder.getAttrValue("validationCode") ,
						builder.getAttrValue("responseCode"), builder.getAttrValue("entryMode") ));
			} else
				request.setMiscData(authAuthorityMiscData);
		}
		request.setRetrievalReferenceNumber(getAttribute(step, AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, String.class, false));
		request.setTerminalID(getAttribute(step, AuthorityAttrEnum.ATTR_TERMINAL_CD, String.class, true));
		request.setTraceNumber(sanitizeTraceNumber(getAttribute(step, AuthorityAttrEnum.ATTR_TRACE_NUMBER, Long.class, true))); //TODO: we may need to get this from somewhere else
		if(subAction != null)
			request.setTransactionSubType(subAction.toString());
		request.setTransactionType(action.toString());
		String terminalEncryptKey = getAttribute(step, AuthorityAttrEnum.ATTR_TERMINAL_ENCRYPTION_KEY, String.class, false);
		if (terminalEncryptKey != null)
			request.setTerminalEncryptKey(terminalEncryptKey);

		return request;
	}

	protected static int sanitizeTraceNumber(long traceNumber) {
		return (int)(Math.abs(traceNumber) % 1000000);
	}

	protected AuthResultCode populateResults(int requestedAmount, AuthorityAction action, ISO8583Response response, Map<String, Object> resultAttributes, MessageChainStep step) {
		AuthResultCode authResultCode;
		DeniedReason deniedReason;
		if(response.getResponseCode().equals(ISO8583Response.SUCCESS)) {
			authResultCode = AuthResultCode.APPROVED;
			deniedReason = null;
			
			resultAttributes.put("approvedAmount", response.hasAmount() ? response.getAmount() : requestedAmount);
		} else if(response.getResponseCode() == null || response.getResponseCode().length() == 0) {
			authResultCode =  AuthResultCode.FAILED;
			deniedReason = DeniedReason.INVALID_RESPONSE;
		} else if(action == AuthorityAction.SALE && response.getResponseCode().startsWith("GE")) {
			//return DECLINED for failed sale to put transaction in retryable TRANSACTION_INCOMPLETE state
			authResultCode = AuthResultCode.DECLINED;
			deniedReason = DeniedReason.PROCESSOR_RESPONSE;
		} else if(action == AuthorityAction.REFUND && response.getResponseCode().equals(ISO8583Response.ERROR_HOST_CONNECTION_FAILURE)) {
			//return DECLINED for refund on connection failure to put transaction in retryable TRANSACTION_INCOMPLETE state
			authResultCode = AuthResultCode.DECLINED;
			deniedReason = DeniedReason.PROCESSOR_TIMEOUT;
		} else {
			authResultCode = getAuthResultCode(response.getResponseCode(), response.getResponseMessage());
			switch(authResultCode) {
				case APPROVED:
				case PARTIAL:
					resultAttributes.put("approvedAmount", response.hasAmount() ? response.getAmount() : requestedAmount);
					deniedReason = null;
					break;
				default:
					deniedReason = getDeniedReason(response.getResponseCode(), response.getResponseMessage());
			}
		}
		String responseCode = response.getResponseCode();
		String responseMessage = response.getResponseMessage();
		String clientText = null;
		if(action == AuthorityAction.AUTHORIZATION) {
			StringBuilder clientTextBuilder = new StringBuilder();
			if(response.hasCvv2Result()) {
				responseCode = new StringBuilder().append(responseCode).append(";CVV:").append(response.getCvv2Result()).toString();
				if(!StringUtils.isBlank(getDeclineCvvResults()) && getDeclineCvvResults().contains(response.getCvv2Result())) {
					switch(authResultCode) {
						case APPROVED:
						case PARTIAL:
							if(isAllowCvvMismatch() && step.getAttributeSafely(AuthorityAttrEnum.ATTR_ALLOW_CVV_MISMATCH, Boolean.class, Boolean.FALSE)) {
								authResultCode = AuthResultCode.CVV_MISMATCH;
							} else {
								authResultCode = AuthResultCode.DECLINED;
							}
							deniedReason = DeniedReason.CVV_MISMATCH;
					}

					clientTextBuilder.append("; CVV Mis-match");
					String desc = getCvvResultDescription(response.getCvv2Result());
					if(!StringUtils.isBlank(desc))
						clientTextBuilder.append(": ").append(desc);
				}
			}
			if(response.hasAvsResult()) {
				responseCode = new StringBuilder().append(responseCode).append(";AVS:").append(response.getAvsResult()).toString();
				if(!StringUtils.isBlank(getDeclineAvsResults()) && getDeclineAvsResults().contains(response.getAvsResult())) {
					switch(authResultCode) {
						case APPROVED:
						case PARTIAL:
						case CVV_MISMATCH:
							if(isAllowAvsMismatch() && step.getAttributeSafely(AuthorityAttrEnum.ATTR_ALLOW_AVS_MISMATCH, Boolean.class, Boolean.FALSE)) {
								if(authResultCode == AuthResultCode.CVV_MISMATCH)
									authResultCode = AuthResultCode.CVV_AND_AVS_MISMATCH;
								else
									authResultCode = AuthResultCode.AVS_MISMATCH;
							} else {
								authResultCode = AuthResultCode.DECLINED;
							}
							if(deniedReason == DeniedReason.CVV_MISMATCH)
								deniedReason = DeniedReason.CVV_AND_AVS_MISMATCH;
							else
								deniedReason = DeniedReason.AVS_MISMATCH;
					}
					clientTextBuilder.append("; AVS Mis-match");
					String desc = getAvsResultDescription(response.getAvsResult());
					if(!StringUtils.isBlank(desc))
						clientTextBuilder.append(": ").append(desc);
				}
			}
			if(clientTextBuilder.length() > 0) {
				clientText = clientTextBuilder.substring(2);
				responseMessage = clientTextBuilder.insert(0, responseMessage).toString();
			}
		}

		resultAttributes.put("authResultCd", authResultCode.getValue());
		if(deniedReason != null)
			resultAttributes.put(AuthorityAttrEnum.ATTR_DENIED_REASON.getValue(), deniedReason);
		resultAttributes.put("authorityRespCd", responseCode);
		resultAttributes.put("authorityRespDesc", responseMessage);
		resultAttributes.put("authAuthorityTranCd", response.getApprovalCode());
		resultAttributes.put("authAuthorityRefCd", response.getRetrievalReferenceNumber());
		resultAttributes.put("authAuthorityTs", response.getEffectiveDate());
		if (response.hasMiscData()) {
			resultAttributes.put("authAuthorityMiscData", response.getMiscData());
		} else if (response.hasPS2000()) {
			PS2000 ps2000 = response.getPS2000();
			StringBuilder sb = new StringBuilder();
			//<opt entryMode="90" identifier="088288754967539" indicator="E" responseCode="00" validationCode="PTBD" />
			sb.append("<opt entryMode=\"").append(StringUtils.prepareCDATA(ps2000.getEntryMode()))
				.append("\" identifier=\"").append(StringUtils.prepareCDATA(ps2000.getIdentifier()))
				.append("\" indicator=\"").append(StringUtils.prepareCDATA(ps2000.getIndicator()))
				.append("\" responseCode=\"").append(StringUtils.prepareCDATA(ps2000.getResponseCode()))
				.append("\" validationCode=\"").append(StringUtils.prepareCDATA(ps2000.getValidationCode()))
				.append("\"/>");
			resultAttributes.put("authAuthorityMiscData", sb.toString());
		}
		if(action == AuthorityAction.AUTHORIZATION) {
			switch(authResultCode) {
				case APPROVED: case PARTIAL:
					resultAttributes.put("authHoldUsed", true);
					setClientText(resultAttributes, authResultCode, null, null, response.getAmount(), null, response.getResponseMessage());
					break;
				case CVV_MISMATCH:
				case AVS_MISMATCH:
				case CVV_AND_AVS_MISMATCH:
					resultAttributes.put("authHoldUsed", true);
					setClientText(resultAttributes, authResultCode, deniedReason, response.getResponseMessage(), clientText);
					break;
				case DECLINED:
					if(deniedReason != null)
						switch(deniedReason) {
							case CVV_MISMATCH:
							case AVS_MISMATCH:
							case CVV_AND_AVS_MISMATCH:
								resultAttributes.put("authHoldUsed", true);
								break;
						}
					// fall-through
				default:
					setClientText(resultAttributes, authResultCode, deniedReason, response.getResponseMessage());
			}
		}
		return authResultCode;
	}

	protected AuthResultCode getAuthResultCode(String responseCode, String responseMessage) {
		AuthResultCode arcText = responseMessageResultCd.get(responseMessage);
		AuthResultCode arcCode = responseCodeResultCd.get(responseCode);
		if(arcText != null) {
			if(arcCode == null || arcCode == arcText)
				return arcText;
			// Return the combo of both
			switch(arcText) {
				case APPROVED:
					switch(arcCode) {
						case PARTIAL:
							return arcCode;
						default:
							return arcText;
					}
				case AVS_MISMATCH:
					switch(arcCode) {
						case CVV_MISMATCH:
						case CVV_AND_AVS_MISMATCH:
							return AuthResultCode.CVV_AND_AVS_MISMATCH;
						default:
							return arcText;
					}
				case CVV_MISMATCH:
					switch(arcCode) {
						case AVS_MISMATCH:
						case CVV_AND_AVS_MISMATCH:
							return AuthResultCode.CVV_AND_AVS_MISMATCH;
						default:
							return arcText;
					}
				case FAILED:
					return arcCode;
				default:
					return arcText;
			}
		} else if(arcCode != null)
			return arcCode;
		if(responseCode.startsWith("GE"))
			return AuthResultCode.FAILED;
		return AuthResultCode.DECLINED;
	}

	protected DeniedReason getDeniedReason(String responseCode, String responseMessage) {
		DeniedReason deniedReason = responseMessageDeniedReason.get(responseMessage);
		if(deniedReason != null)
			return deniedReason;
		deniedReason = responseCodeDeniedReason.get(responseCode);
		if(deniedReason != null)
			return deniedReason;
		return DeniedReason.PROCESSOR_RESPONSE;
	}
	protected static String translateEntryMethod(byte entryTypeInt) {
		EntryType entryType;
		try {
			entryType = EntryType.getByValue(entryTypeInt);
		} catch(InvalidValueException e) {
			entryType = EntryType.UNSPECIFIED;
		}
		switch(entryType) {
			case SWIPE:
				return "S"; //Swipe
			case BAR_CODE:
				return "U";// Bar Code - but gateway has no such concept so use unspecified
			case CONTACTLESS:
			case ISIS:
				return "R"; // Contactless
			case UNSPECIFIED:
				return "U"; // Unspecified
			case MANUAL:
			case TOKEN:
			case PHONE:
			case CARD_ID:
			case CARD_ID_CREDENTIALS:
			case CASH:
			case BLUETOOTH:
				return "M"; // Manual
			default:
				if (entryType.getFallbackEntryType() == EntryType.CONTACTLESS)
					return "R"; // Contactless
				return "U";
		}
	}

	public InterchangeGlobalsFactory getGlobalsFactory() {
		return globalsFactory;
	}

	public void setGlobalsFactory(InterchangeGlobalsFactory globalsFactory) {
		this.globalsFactory = globalsFactory;
	}

	public ThreadPoolExecutor getThreadPoolExecutor() {
		InterchangeGlobalsFactory gf = getGlobalsFactory();
		return gf == null ? null : gf.getThreadPoolExecutor();
	}

	public Configuration getConfiguration() {
		InterchangeGlobalsFactory gf = getGlobalsFactory();
		return gf == null ? null : gf.getConfiguration();

	}

	public Class<? extends ISO8583Interchange> getInterchangeClass() {
		return interchangeClass;
	}

	public void setInterchangeClass(Class<? extends ISO8583Interchange> interchangeClass) {
		this.interchangeClass = interchangeClass;
	}

	public void initialize() throws IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException {
		InterchangeGlobalsFactory gf = getGlobalsFactory();
		if(gf == null)
			throw new IllegalArgumentException("GlobalsFactory property is not set on " + this);
		transactionManager = gf.createTransactionManager();
		Class<? extends ISO8583Interchange> cls2 = getInterchangeClass();
		if(cls2 == null)
			throw new IllegalArgumentException("InterchangeClass property is not set on " + this);
		interchange = ReflectionUtils.createObject(cls2, getThreadPoolExecutor(), transactionManager, getConfiguration());
		interchange.start();
	}


	public ISO8583Interchange getInterchange() {
		return interchange;
	}


	public ISO8583TransactionManager getTransactionManager() {
		return transactionManager;
	}

	public AuthResultCode getResponseMessageResultCd(String responseMessage) {
		return responseMessageResultCd.get(responseMessage);
	}

	public void setResponseMessageResultCd(String responseMessage, AuthResultCode resultCd) {
		responseMessageResultCd.put(responseMessage, resultCd);
	}

	public AuthResultCode getResponseCodeResultCd(String responseCode) {
		return responseCodeResultCd.get(responseCode);
	}

	public void setResponseCodeResultCd(String responseCode, AuthResultCode resultCd) {
		responseCodeResultCd.put(responseCode, resultCd);
	}

	public DeniedReason getResponseMessageDeniedReason(String responseMessage) {
		return responseMessageDeniedReason.get(responseMessage);
	}

	public void setResponseMessageDeniedReason(String responseMessage, DeniedReason deniedReason) {
		responseMessageDeniedReason.put(responseMessage, deniedReason);
	}

	public DeniedReason getResponseCodeDeniedReason(String responseCode) {
		return responseCodeDeniedReason.get(responseCode);
	}

	public void setResponseCodeDeniedReason(String responseCode, DeniedReason deniedReason) {
		responseCodeDeniedReason.put(responseCode, deniedReason);
	}

	@Override
	protected String[] getAdditionalPreActionLogAttributes() {
		return new String[] {
			MessageAttribute.MERCHANT_CD.getValue(),
			MessageAttribute.TERMINAL_CD.getValue()
		};
	}

	public boolean isAuthHoldUsed() {
		return authHoldUsed;
	}

	public void setAuthHoldUsed(boolean authHoldUsed) {
		this.authHoldUsed = authHoldUsed;
	}

	public boolean isSaleReversalSupported() {
		return saleReversalSupported;
	}

	public void setSaleReversalSupported(boolean saleReversalSupported) {
		this.saleReversalSupported = saleReversalSupported;
	}

	public boolean isAuthReversalSupported() {
		return authReversalSupported;
	}

	public void setAuthReversalSupported(boolean authReversalSupported) {
		this.authReversalSupported = authReversalSupported;
	}

	public boolean isReuploadSupported() {
		return reuploadSupported;
	}

	public void setReuploadSupported(boolean reuploadSupported) {
		this.reuploadSupported = reuploadSupported;
	}
	
	public boolean isRedeliverySupported() {
		return redeliverySupported;
	}

	public void setRedeliverySupported(boolean redeliverySupported) {
		this.redeliverySupported = redeliverySupported;
	}

	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}

	@Override
	public boolean isAvailable() {
		return interchange != null && interchange.isConnected();
	}

	@Override
	public void registerListener(ResetAvailabilityListener listener) {
		availabilityListeners.add(listener);
	}

	@Override
	public void deregisterListener(ResetAvailabilityListener listener) {
		availabilityListeners.remove(listener);
	}

	protected void fireResetAvailability() {
		for(ResetAvailabilityListener listener : availabilityListeners)
			listener.resetAvailability(this);
	}

	public boolean isPartialAuthAllowed() {
		return partialAuthAllowed;
	}

	public void setPartialAuthAllowed(boolean partialAuthAllowed) {
		this.partialAuthAllowed = partialAuthAllowed;
	}

	public int getMinimumAuthAmount() {
		return minimumAuthAmount;
	}

	public void setMinimumAuthAmount(int minimumAuthAmount) {
		this.minimumAuthAmount = minimumAuthAmount;
	}

	public String getDeclineAvsResults() {
		return declineAvsResults;
	}

	public void setDeclineAvsResults(String declineAvsResults) {
		this.declineAvsResults = declineAvsResults;
	}

	public String getDeclineCvvResults() {
		return declineCvvResults;
	}

	public void setDeclineCvvResults(String declineCvvResults) {
		this.declineCvvResults = declineCvvResults;
	}

	public boolean isAllowAvsMismatch() {
		return allowAvsMismatch;
	}

	public void setAllowAvsMismatch(boolean allowAvsMismatch) {
		this.allowAvsMismatch = allowAvsMismatch;
	}

	public boolean isAllowCvvMismatch() {
		return allowCvvMismatch;
	}

	public void setAllowCvvMismatch(boolean allowCvvMismatch) {
		this.allowCvvMismatch = allowCvvMismatch;
	}

	public String getAvsResultDescription(String result) {
		return avsResultDescriptions.get(result);
	}

	public void setAvsResultDescription(String result, String description) {
		avsResultDescriptions.put(result, description);
	}

	public String getCvvResultDescription(String result) {
		return cvvResultDescriptions.get(result);
	}

	public void setCvvResultDescription(String result, String description) {
		cvvResultDescriptions.put(result, description);
	}
}
