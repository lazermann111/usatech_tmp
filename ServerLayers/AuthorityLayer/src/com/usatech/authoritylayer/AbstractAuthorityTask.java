/**
 *
 */
package com.usatech.authoritylayer;

import java.util.Map;
import java.util.Set;

import com.usatech.app.Attribute;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAction;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.DebugLevel;
import com.usatech.layers.common.constants.DeniedReason;
import com.usatech.layers.common.constants.Language;
import com.usatech.layers.common.constants.MessageAttribute;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.text.StringUtils;

/**
 * @author Brian S. Krug
 *
 */
public abstract class AbstractAuthorityTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected int maxAuthStaleness = 30 * 1000;
	protected DebugLevel debugLevel;
	protected Set<String> supportedCurrencies;
	protected static final String EMPTY_STRING = "";
	protected final String clientTextKeyPrefix;
	protected Language language = null;

	public AbstractAuthorityTask(String clientTextKeyPrefix) {
		this.clientTextKeyPrefix = clientTextKeyPrefix;
	}

	/**
	 * @see com.usatech.app.MessageChainTask#process(com.usatech.app.MessageChainTaskInfo)
	 */
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		AuthResultCode arc = processInternal(taskInfo);
		taskInfo.getStep().removeAttribute("trackData");
		return determineResultInt(arc);
	}

	protected int determineResultInt(AuthResultCode result) {
		switch(result) {
			case APPROVED:
			case PARTIAL:
				return 0;
			case DECLINED:
			case DECLINED_PERMANENT:
			case DECLINED_PAYMENT_METHOD:
				return 1;
			case FAILED:
				return 2;
			default:
				return 1;
		}
	}

	protected void setClientText(Map<String, Object> resultAttributes, AuthResultCode authResultCode, DeniedReason deniedReason, Object... params) {
		String prefix = getClientTextKeyPrefix();
		String messageLanguage;
		if (language == null) {messageLanguage = "";} else {messageLanguage = language.toString() + ".";}
		switch(authResultCode) {
			case APPROVED:
			case PARTIAL:
				resultAttributes.put(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY.getValue(), "client.message."+ messageLanguage +"auth." + prefix + ".approved");
				resultAttributes.put(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS.getValue(), params == null ? new Object[0] : params);
				break;
			default:
				resultAttributes.put(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY.getValue(), "client.message."+ messageLanguage +"auth." + prefix + ".denied-" + (deniedReason == null ? "UNKNOWN" : deniedReason.toString()));
				resultAttributes.put(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS.getValue(), params == null ? new Object[0] : params);
		}
	}

	protected AuthResultCode processInternal(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		Map<String, Object> attributes = taskInfo.getStep().getAttributes();
		ProcessingUtils.logAttributes(log, "Starting ", attributes, getPreActionLogAttributes(), getAdditionalPreActionLogAttributes());
		AuthorityAction action;
		String currencyCd;
		AuthorityAction subAction;
		try {
			action = getAttribute(step, AuthorityAttrEnum.ATTR_ACTION_TYPE, AuthorityAction.class, true);
			currencyCd = getAttribute(step, AuthorityAttrEnum.ATTR_CURRENCY_CD, String.class, true);
		} catch(AuthorityAttributeConversionException e) {
			return AuthResultCode.FAILED;
		}
		if(supportedCurrencies != null && !supportedCurrencies.contains(currencyCd)) {
			log.info("Currency '" + currencyCd + " is not supported by " + this);
			return populateFailedResults(null, step.getResultAttributes(), DeniedReason.UNSUPPORTED_CURRENCY, "Currency not supported");
		}
		switch(action) {
			case AUTHORIZATION: // check staleness
				if(taskInfo.isRedelivered()) {
					AuthResultCode reversalResult = processReversal(action, null, taskInfo);
					switch(reversalResult) {
						case APPROVED:
							taskInfo.getStep().getResultAttributes().clear();
							break;
						default:
							log.info("{0} reversal did not succeed: {1}, retryCount={2}", action, reversalResult.getDescription(),  taskInfo.getRetryCount());
							return populateFailedResults(null, step.getResultAttributes(), DeniedReason.PROCESSOR_ERROR, action + " reversal: " + reversalResult);
					}
				}
				int maxStaleness = getMaxAuthStaleness();
				if(maxStaleness > 0) {
					long authTime;
					try {
						authTime = getAttribute(step, AuthorityAttrEnum.ATTR_AUTH_TIME, Long.class, true);
					} catch(AuthorityAttributeConversionException e) {
						return AuthResultCode.FAILED;
					}
					if(authTime + maxStaleness < System.currentTimeMillis()) {
						log.info("Auth request against " + taskInfo.getServiceName() + " initiated at " + authTime + " has become stale");
						processAuthExiration(taskInfo);
						return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.INTERNAL_TIMEOUT, "Auth Timeout");
					}
				}
				subAction = null;
				break;
			case SALE: case REFUND: case SETTLEMENT: case SETTLEMENT_RETRY: // check redelivery and reverse if true
				if(taskInfo.isRedelivered()) {
					AuthResultCode reversalResult = processReversal(action, null, taskInfo);
					switch(reversalResult) {
						case APPROVED:
							taskInfo.getStep().getResultAttributes().clear();
							break;
						default:
							log.info("{0} reversal did not succeed: {1}, retryCount={2}", action, reversalResult.getDescription(),  taskInfo.getRetryCount());
							return populateFailedResults(null, step.getResultAttributes(), DeniedReason.PROCESSOR_ERROR, action + " reversal: " + reversalResult);
					}
				}
				subAction = null;
				break;
			case ADJUSTMENT:
				try {
					subAction = getAttribute(step, AuthorityAttrEnum.ATTR_ACTION_SUB_TYPE, AuthorityAction.class, true);
				} catch(AuthorityAttributeConversionException e) {
					return AuthResultCode.FAILED;
				}
				switch(subAction) {
					case SALE: case REFUND: // okay
						break;
					default:
						throw new RetrySpecifiedServiceException("Sub-action type '" + subAction + "' for action type '" + action + "' NOT implemented",WorkRetryType.NO_RETRY);
				}
				if(taskInfo.isRedelivered()) {
					AuthResultCode reversalResult = processReversal(action, subAction, taskInfo);
					switch(reversalResult) {
						case APPROVED:
							taskInfo.getStep().getResultAttributes().clear();
							break;
						default:
							log.info("{0} reversal did not succeed: {1}, retryCount={2}", action, reversalResult.getDescription(),  taskInfo.getRetryCount());
							return populateFailedResults(null, step.getResultAttributes(), DeniedReason.PROCESSOR_ERROR, action + " reversal: " + reversalResult);
					}
				}
				break;
			default:
				throw new RetrySpecifiedServiceException("Action type '" + action + "' NOT implemented",WorkRetryType.NO_RETRY);
		}
		AuthResultCode result = processRequest(action, subAction, taskInfo);
		ProcessingUtils.logAttributes(log, "Finished, result=" + result.getDescription() + "; ", attributes, getPostActionLogAttributes(), getAdditionalPostActionLogAttributes());
		return result;
	}

	/**
	 * @throws ServiceException
	 */
	protected void processAuthExiration(MessageChainTaskInfo taskInfo) throws ServiceException {

	}

	protected <T> T getAttribute(MessageChainStep step, Attribute attributeKey, Class<T> convertTo, boolean required) throws AuthorityAttributeConversionException {
		try {
			if(required)
				return ConvertUtils.convertRequired(convertTo, step.getAttributes().get(attributeKey.getValue()));
			return ConvertUtils.convert(convertTo, step.getAttributes().get(attributeKey.getValue()));
		} catch(ConvertException e) {
			log.error("Could not convert '" + attributeKey.getValue() + "' for request", e);
			populateFailedResults(null, step.getResultAttributes(), DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Data Conversion Error of '" + attributeKey.getValue() + "' in " + getClass().getSimpleName());
			throw new AuthorityAttributeConversionException(attributeKey, e);
		}
	}

	protected <T> T getAttributeDefault(MessageChainStep step, Attribute attributeKey, Class<T> convertTo, T defaultValue) throws AuthorityAttributeConversionException {
		try {
			T value = ConvertUtils.convert(convertTo, step.getAttributes().get(attributeKey.getValue()));
			if(value == null)
				return defaultValue;
			return value;
		} catch(ConvertException e) {
			log.error("Could not convert '" + attributeKey.getValue() + "' for request", e);
			populateFailedResults(null, step.getResultAttributes(), DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Data Conversion Error of '" + attributeKey.getValue() + "' in " + getClass().getSimpleName());
			throw new AuthorityAttributeConversionException(attributeKey, e);
		}
	}
	/**
	 * @param action
	 * @param taskInfo
	 * @return
	 */
	protected abstract AuthResultCode processRequest(AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo) throws ServiceException ;

	/** Reverses the action. It may or may not have previously succeeded. 
	 * @param action
	 * @param taskInfo
	 * @return
	 * @throws ServiceException
	 */
	protected abstract AuthResultCode processReversal(AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo) throws ServiceException ;

	protected String[] getPreActionLogAttributes() {
		return new String[] {
			MessageAttribute.ACTION_TYPE.getValue(),
			MessageAttribute.AUTHORITY_NAME.getValue(),
			MessageAttribute.TRACE_NUMBER.getValue(),
			MessageAttribute.POS_PTA_ID.getValue(),
			MessageAttribute.AMOUNT.getValue(),
			MessageAttribute.CURRENCY_CD.getValue(),
			MessageAttribute.TIME_ZONE_GUID.getValue(),
			MessageAttribute.ONLINE.getValue()
		};
	}

	protected String[] getPostActionLogAttributes() {
		return new String[] {
			MessageAttribute.ACTION_TYPE.getValue(),
			MessageAttribute.AUTHORITY_NAME.getValue(),
			MessageAttribute.TRACE_NUMBER.getValue()
		};
	}

	protected String[] getAdditionalPreActionLogAttributes()
	{
		return null;
	}

	protected String[] getAdditionalPostActionLogAttributes()
	{
		return null;
	}

	public int getMaxAuthStaleness() {
		return maxAuthStaleness;
	}

	public void setMaxAuthStaleness(int maxAuthStaleness) {
		this.maxAuthStaleness = maxAuthStaleness;
	}

	protected AuthResultCode populateDeclinedResults(AuthorityAction action, Map<String, Object> resultAttributes, DeniedReason deniedReason, String errorText) {
		return populateResults(action, AuthResultCode.DECLINED, resultAttributes, deniedReason, errorText, errorText);
	}
	
	protected AuthResultCode populateFailedResults(AuthorityAction action, Map<String, Object> resultAttributes, DeniedReason deniedReason, String errorText) {
		return populateResults(action, AuthResultCode.FAILED, resultAttributes, deniedReason, errorText);
	}

	protected AuthResultCode populateResults(AuthorityAction action, AuthResultCode authResultCode, Map<String, Object> resultAttributes, DeniedReason deniedReason, String errorText) {
		return populateResults(action, authResultCode, resultAttributes, deniedReason, errorText, errorText);
	}

	protected AuthResultCode populateResults(AuthorityAction action, AuthResultCode authResultCode, Map<String, Object> resultAttributes, DeniedReason deniedReason, String errorText, Object... params) {
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue(), authResultCode.getValue());
		if(deniedReason != null)
			resultAttributes.put(AuthorityAttrEnum.ATTR_DENIED_REASON.getValue(), deniedReason);
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue(), deniedReason == null ? "--" : deniedReason.toString());
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC.getValue(), errorText);
		setClientText(resultAttributes, authResultCode, deniedReason, params);
		return authResultCode;
	}

	public DebugLevel getDebugLevel() {
		return debugLevel;
	}

	public void setDebugLevel(DebugLevel debugLevel) {
		this.debugLevel = debugLevel;
	}

	public Set<String> getSupportedCurrencies() {
		return supportedCurrencies;
	}

	public void setSupportedCurrencies(Set<String> supportedCurrencies) {
		this.supportedCurrencies = supportedCurrencies;
	}

	public String getClientTextKeyPrefix() {
		return clientTextKeyPrefix;
	}
}
