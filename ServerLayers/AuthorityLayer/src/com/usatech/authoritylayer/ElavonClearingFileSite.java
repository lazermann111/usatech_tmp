package com.usatech.authoritylayer;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.locks.ReentrantLock;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.text.StringUtils;
import simple.text.ThreadSafeDateFormat;
import simple.text.StringUtils.Justification;

public class ElavonClearingFileSite extends FtpSite {
	private static final Log log = Log.getLog();
	protected int destinationId = 8009;
    protected final ThreadSafeDateFormat fileNameDateFormat = new ThreadSafeDateFormat(new SimpleDateFormat("yyMMddHHmmss"));
    protected final ReentrantLock fileIdLock = new ReentrantLock();
    protected int lastFileId;
    protected int fileIdMod = 4;
    protected int fileIdOffset = 0;
    protected String fileExtension;
    protected RandomAccessFile lastFileIdFileAccess;
    
    public static class FileSiteEntry {
    	protected final String fileName;
    	protected final String tmpFileName;
    	protected final Date fileDate;
    	protected final int destinationId;
    	
		protected FileSiteEntry(String fileName, String tmpFileName, Date fileDate,int destinationId) {
			this.fileName = fileName;
			this.tmpFileName = tmpFileName;
			this.fileDate = fileDate;
			this.destinationId = destinationId;
		}
		public String getFileName() {
			return fileName;
		}
		public String getTmpFileName() {
			return tmpFileName;
		}
		public Date getFileDate() {
			return fileDate;
		}
		public int getDestinationId() {
			return destinationId;
		}	
    }
    
    public void initialize() throws ServiceException, IOException, ConvertException {
    	fileIdLock.lock();
    	try {
    		File lastFileIdFile = new File("elavon-last-file-id"); 
    		// read last fileId from local file
	    	log.info("Reading last elavon file id from '" + lastFileIdFile.getCanonicalPath() + "'");
	    	int fileId;
	    	if(lastFileIdFile.exists()) {
	    		lastFileIdFileAccess = new RandomAccessFile(lastFileIdFile, "rwd");
		    	fileId = ConvertUtils.getInt(lastFileIdFileAccess.readUTF().trim());
	    		fileId = fileId - (fileId % getFileIdMod()) + getFileIdOffset(); // ensure that stored value is offset
	    	} else {
	    		lastFileIdFile.createNewFile();
	    		lastFileIdFileAccess = new RandomAccessFile(lastFileIdFile, "rwd");
		    	fileId = fileIdOffset;
		    	lastFileIdFileAccess.writeUTF(Integer.toString(fileId));
	    	}
	    	lastFileId = fileId;
    	} finally {
    		fileIdLock.unlock();
    	}
    }
    
    public FileSiteEntry createFileSiteEntry(Integer uniqueSeconds) throws IOException {
    	StringBuilder sb = new StringBuilder();
        sb.append('T');
        int destId = getDestinationId();
        StringUtils.appendPadded(sb, Integer.toString(destId), '0', 4, Justification.RIGHT);
        int fileId;
        if(uniqueSeconds != null && uniqueSeconds > 0) {
        	fileId = uniqueSeconds;
        	setNextFileId(fileId);
        } else {
        	fileId = getNextFileId();
        }
        Date fileDate = new Date(fileId*1000L);
        sb.append(getFileNameDateFormat().format(fileDate));
        sb.append('.');
        sb.append(getFileExtension());
        String tmpFileName = sb.toString();
        sb.setCharAt(0, 'P');
        String fileName = sb.toString();
        return new FileSiteEntry(fileName, tmpFileName, fileDate, destId);
    }
	
	protected int getNextFileId() throws IOException {
		int mod = getFileIdMod();
		int timeId = (int)(System.currentTimeMillis() / 1000);
		timeId = timeId - (timeId % mod) + (getFileIdOffset() % mod);
		fileIdLock.lock();
		try {
			if(lastFileIdFileAccess == null)
				throw new IOException("This class was not initialized. Please call 'initialize()' method first");
			lastFileId = Math.max(timeId, lastFileId + mod);
			lastFileIdFileAccess.seek(0L);
			lastFileIdFileAccess.writeUTF(Integer.toString(lastFileId));
			return lastFileId;
		} finally {
    		fileIdLock.unlock();
    	}
	}

	protected void setNextFileId(int fileId) throws IOException {
		int mod = getFileIdMod();
		fileIdLock.lock();
		try {
			if(lastFileIdFileAccess == null)
				throw new IOException("This class was not initialized. Please call 'initialize()' method first");
			lastFileId = Math.max(lastFileId, fileId + (-fileId % mod));
			lastFileIdFileAccess.seek(0L);
			lastFileIdFileAccess.writeUTF(Integer.toString(lastFileId));
		} finally {
    		fileIdLock.unlock();
    	}
	}
	public int getDestinationId() {
		return destinationId;
	}

	public void setDestinationId(int destinationId) {
		this.destinationId = destinationId;
	}

	public ThreadSafeDateFormat getFileNameDateFormat() {
		return fileNameDateFormat;
	}

	public int getFileIdMod() {
		return fileIdMod;
	}

	public void setFileIdMod(int fileIdMod) {
		this.fileIdMod = fileIdMod;
	}

	public int getFileIdOffset() {
		return fileIdOffset;
	}

	public void setFileIdOffset(int fileIdOffset) {
		this.fileIdOffset = fileIdOffset;
	}
	
	public String getFileExtension() {
		return fileExtension;
	}

	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}
	@Override
	public String toString() {
		String rd = getRemoteDir();
		return "ftps: " + getUsername() + "@" + getHost() + ":" + getPort() + (rd != null && rd.trim().length() > 0 ? "/" + rd : "");
	}
}
