/**
 *
 */
package com.usatech.authoritylayer;

import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.Map;
import java.util.TimeZone;

import org.apache.axis.client.Stub;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.io.Log;
import simple.text.StringUtils;

import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAction;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.DeniedReason;
import com.usatech.layers.common.constants.MessageAttribute;
import com.usatech.layers.common.constants.PaymentMaskBRef;
import com.usatech.pos.webservice.AuthorizeResponse;
import com.usatech.pos.webservice.POSException;
import com.usatech.pos.webservice.POSRequestHandler;
import com.usatech.pos.webservice.POSRequestHandlerServiceLocator;
import com.usatech.pos.webservice.RefundResponse;
import com.usatech.pos.webservice.SettleResponse;

/**
 * @author Brian S. Krug
 *
 */
public class POSGatewayTask extends AbstractAuthorityTask {
	private static final Log log = Log.getLog();
	protected static final POSRequestHandlerServiceLocator locator = new POSRequestHandlerServiceLocator();

	public POSGatewayTask() {
		super("posgateway");
	}

	@Override
	protected AuthResultCode processRequest(AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		String remoteServerAddr;
		String currencyCd;
		String terminalCd;
		String cardNumber;
		BigInteger amount;
		Calendar transactionTime;
		int transactionID;
		String approvalCode;
		AccountData accountData = new AccountData(step.getAttributes());
		cardNumber = accountData.getTrackData();
		if(StringUtils.isBlank(cardNumber)) {
			cardNumber = accountData.getPartData(PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER);
			if(StringUtils.isBlank(cardNumber))
				return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), null, "Account Number not provided");
		}
		try {
			remoteServerAddr = getAttribute(step, AuthorityAttrEnum.ATTR_REMOTE_SERVER_ADDRESS, String.class, true);
			terminalCd = getAttribute(step, AuthorityAttrEnum.ATTR_TERMINAL_CD, String.class, true);
			amount = getAttribute(step, AuthorityAttrEnum.ATTR_AMOUNT, BigInteger.class, true);
			transactionTime = getAttribute(step, AuthorityAttrEnum.ATTR_AUTH_TIME, Calendar.class, true);
			String timeZoneGuid = getAttribute(step, AuthorityAttrEnum.ATTR_TIMEZONE_GUID, String.class, false);
			TimeZone tz;
			if(timeZoneGuid != null && timeZoneGuid.length() > 0 && (tz=TimeZone.getTimeZone(timeZoneGuid)) != null)
				transactionTime.setTimeZone(tz);
			transactionID = sanitizeTraceNumber(getAttribute(step, AuthorityAttrEnum.ATTR_TRACE_NUMBER, Long.class, true));
			currencyCd = getAttribute(step, AuthorityAttrEnum.ATTR_CURRENCY_CD, String.class, true);
			approvalCode = getAttribute(step, AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD, String.class, false);
		} catch(AuthorityAttributeConversionException e) {
			return AuthResultCode.FAILED;
		}

		POSRequestHandler handler;
		try {
			handler = locator.getposinterface(new URL(remoteServerAddr));
		} catch(javax.xml.rpc.ServiceException e) {
			log.warn("While connecting to POS Interface", e);
			return populateDeclinedResults(action, taskInfo.getStep().getResultAttributes(), null, e.getMessage());
		} catch(MalformedURLException e) {
			log.warn("While connecting to POS Interface", e);
			return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), null, "Could not connect to '" + remoteServerAddr + "'");
		}
		
	    Stub stub = (Stub) handler;
	    //set timeout to 0 for configuration properties to take effect
	    stub.setTimeout(0);
		
		Map<String, Object> resultAttributes = taskInfo.getStep().getResultAttributes();
		switch(action) {
			case AUTHORIZATION:
				AuthorizeResponse aresp;
				try {
		            log.info("Authorize Request" +
	                    ": remoteServerAddress=" + remoteServerAddr +
	                    ", transactionID=" + transactionID +
	                    ", transactionDateTime=" + transactionTime.getTime() +
	                    ", terminalName=" + terminalCd +
	                    ", cardNumber=" + MessageResponseUtils.maskCard(cardNumber) +
	                    ", currency=" + currencyCd +
	                    ", amount=" + amount);
		            
					aresp = handler.authorize(transactionID, transactionTime, terminalCd, cardNumber, currencyCd, amount.intValue());
		            
					log.info("Authorize Response" +
	                    ": approvalCode=" + aresp.getApprovalCode() +
	                    ", approvedAmount=" + aresp.getApprovedAmount() +
	                    ", responseMessage=" + aresp.getResponseMessage() +
	                    ", statusCode=" + aresp.getStatusCode() +
	                    ", transactionID=" + aresp.getTransactionID());
				} catch(POSException e) {
					log.warn("While connecting to POS Interface", e);
					return populateExceptionResults(action, taskInfo.getStep().getResultAttributes(), e);
				} catch(RemoteException e) {
					log.warn("While connecting to POS Interface", e);
					return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), null, e.getMessage());
				}
				boolean allowPartialAuth = step.getAttributeSafely(AuthorityAttrEnum.ATTR_PARTIAL_AUTH_ALLOWED, Boolean.class, false);
				return populateAuthResults(aresp, amount, transactionID, taskInfo.getStep().getResultAttributes(), allowPartialAuth);
			case SALE:
				SettleResponse sresp;
				try {
		            log.info("Settle Request" +
	                    ": remoteServerAddress=" + remoteServerAddr +
	                    ", transactionID=" + transactionID +
	                    ", transactionDateTime=" + transactionTime.getTime() +
	                    ", terminalName=" + terminalCd +
	                    ", cardNumber=" + MessageResponseUtils.maskCard(cardNumber) +
	                    ", currency=" + currencyCd +
	                    ", amount=" + amount +
	                    ", approvalCode=" + approvalCode);
		            
					sresp = handler.settle(transactionID, transactionTime, terminalCd, cardNumber, currencyCd, amount.intValue(), approvalCode);
		            
					log.info("Settle Response" +
	                    ": responseMessage=" + sresp.getResponseMessage() +
	                    ", statusCode=" + sresp.getStatusCode() +
	                    ", transactionID=" + sresp.getTransactionID());
				} catch(POSException e) {
					log.warn("While connecting to POS Interface", e);
					return populateExceptionResults(action, taskInfo.getStep().getResultAttributes(), e);
				} catch(RemoteException e) {
					log.warn("While connecting to POS Interface", e);
					return populateDeclinedResults(action, taskInfo.getStep().getResultAttributes(), null, e.getMessage());
				}
				return populateResults(sresp, amount, transactionID, taskInfo.getStep().getResultAttributes());
			case REFUND:
				RefundResponse rresp;
				try {
		            log.info("Refund Request" +
	                    ": remoteServerAddress=" + remoteServerAddr +
	                    ", transactionID=" + transactionID +
	                    ", transactionDateTime=" + transactionTime.getTime() +
	                    ", terminalName=" + terminalCd +
	                    ", cardNumber=" + MessageResponseUtils.maskCard(cardNumber) +
	                    ", currency=" + currencyCd +
	                    ", amount=" + amount);
					
					rresp = handler.refund(transactionID, transactionTime, terminalCd, cardNumber, currencyCd, amount.intValue());
					
		            log.info("Refund Response" +
	                    ": responseMessage=" + rresp.getResponseMessage() +
	                    ", statusCode=" + rresp.getStatusCode() +
	                    ", transactionID=" + rresp.getTransactionID());					
				} catch(POSException e) {
					log.warn("While connecting to POS Interface", e);
					return populateExceptionResults(action, taskInfo.getStep().getResultAttributes(), e);
				} catch(RemoteException e) {
					log.warn("While connecting to POS Interface", e);
					return populateDeclinedResults(action, taskInfo.getStep().getResultAttributes(), null, e.getMessage());
				}
				return populateResults(rresp, amount, transactionID, taskInfo.getStep().getResultAttributes());
			case ADJUSTMENT:
				BigInteger originalAmount;
				try {
					originalAmount = getAttribute(step, AuthorityAttrEnum.ATTR_ORIGINAL_AMOUNT, BigInteger.class, true);
				} catch(AuthorityAttributeConversionException e) {
					return AuthResultCode.FAILED;
				}
				switch(subAction) {
					case SALE:
						BigInteger adjustAmount = amount.subtract(originalAmount);
						switch(adjustAmount.signum()) {
							case -1: // needs credit
								try {
									rresp = handler.refund(transactionID, transactionTime, terminalCd, cardNumber, currencyCd, adjustAmount.abs().intValue());
								} catch(POSException e) {
									log.warn("While connecting to POS Interface", e);
									return populateExceptionResults(action, taskInfo.getStep().getResultAttributes(), e);
								} catch(RemoteException e) {
									log.warn("While connecting to POS Interface", e);
									return populateDeclinedResults(action, taskInfo.getStep().getResultAttributes(), null, e.getMessage());
								}
								return populateResults(rresp, adjustAmount, transactionID, taskInfo.getStep().getResultAttributes());
							case 0: // nothing to do
								resultAttributes.put("authResultCd", 'Y');
								resultAttributes.put("authorityRespCd", 0);
								String text = "No change to account";
								resultAttributes.put("authorityRespDesc", text);
								resultAttributes.put("settled", true);
								return AuthResultCode.APPROVED;
							case 1: // needs debit
								try {
									sresp = handler.settle(transactionID, transactionTime, terminalCd, cardNumber, currencyCd, adjustAmount.intValue(), approvalCode);
								} catch(POSException e) {
									log.warn("While connecting to POS Interface", e);
									return populateExceptionResults(action, taskInfo.getStep().getResultAttributes(), e);
								} catch(RemoteException e) {
									log.warn("While connecting to POS Interface", e);
									return populateDeclinedResults(action, taskInfo.getStep().getResultAttributes(), null, e.getMessage());
								}
								return populateResults(sresp, adjustAmount, transactionID, taskInfo.getStep().getResultAttributes());
							default:
								throw new RetrySpecifiedServiceException("Illegal State: BigInteger.signum() returned " + adjustAmount.signum(),WorkRetryType.NO_RETRY);
						}
					case REFUND:
						adjustAmount = amount.subtract(originalAmount);
						switch(adjustAmount.signum()) {
							case -1: // needs debit
								try {
									sresp = handler.settle(transactionID, transactionTime, terminalCd, cardNumber, currencyCd, adjustAmount.abs().intValue(), approvalCode);
								} catch(POSException e) {
									log.warn("While connecting to POS Interface", e);
									return populateExceptionResults(action, taskInfo.getStep().getResultAttributes(), e);
								} catch(RemoteException e) {
									log.warn("While connecting to POS Interface", e);
									return populateDeclinedResults(action, taskInfo.getStep().getResultAttributes(), null, e.getMessage());
								}
								return populateResults(sresp, adjustAmount, transactionID, taskInfo.getStep().getResultAttributes());
							case 0: // nothing to do
								resultAttributes.put("authResultCd", 'Y');
								resultAttributes.put("authorityRespCd", 0);
								String text = "No change to account";
								resultAttributes.put("authorityRespDesc", text);
								resultAttributes.put("settled", true);
								return AuthResultCode.APPROVED;
							case 1: // needs credit
								try {
									rresp = handler.refund(transactionID, transactionTime, terminalCd, cardNumber, currencyCd, adjustAmount.intValue());
								} catch(POSException e) {
									log.warn("While connecting to POS Interface", e);
									return populateExceptionResults(action, taskInfo.getStep().getResultAttributes(), e);
								} catch(RemoteException e) {
									log.warn("While connecting to POS Interface", e);
									return populateDeclinedResults(action, taskInfo.getStep().getResultAttributes(), null, e.getMessage());
								}
								return populateResults(rresp, adjustAmount, transactionID, taskInfo.getStep().getResultAttributes());
							default:
								throw new RetrySpecifiedServiceException("Illegal State: BigInteger.signum() returned " + adjustAmount.signum(),WorkRetryType.NO_RETRY);
						}
					default:
						throw new RetrySpecifiedServiceException("Sub-action '" + subAction + "' for Action '" + action + "' NOT implemented",WorkRetryType.NO_RETRY);
				}
			default:
				throw new RetrySpecifiedServiceException("Action '" + action + "' NOT implemented",WorkRetryType.NO_RETRY);
		}
	}

	/**
	 * @see com.usatech.authoritylayer.AbstractAuthorityTask#processReversal(com.usatech.layers.common.constants.AuthorityAction, com.usatech.app.MessageChainTaskInfo)
	 */
	@Override
	protected AuthResultCode processReversal(AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		String remoteServerAddr;
		String currencyCd;
		String terminalCd;
		String cardNumber;
		Calendar transactionTime;
		int transactionID;
		String approvalCode;
		AccountData accountData = new AccountData(step.getAttributes());
		cardNumber = accountData.getPartData(PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER);
		if(StringUtils.isBlank(cardNumber)) {
			cardNumber = accountData.getTrackData();
			if(StringUtils.isBlank(cardNumber))
				return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), null, "Account Number not provided");
		}

		try {
			remoteServerAddr = getAttribute(step, AuthorityAttrEnum.ATTR_REMOTE_SERVER_ADDRESS, String.class, true);
			terminalCd = getAttribute(step, AuthorityAttrEnum.ATTR_TERMINAL_CD, String.class, true);
			transactionTime = getAttribute(step, AuthorityAttrEnum.ATTR_AUTH_TIME, Calendar.class, true);
			String timeZoneGuid = getAttribute(step, AuthorityAttrEnum.ATTR_TIMEZONE_GUID, String.class, false);
			TimeZone tz;
			if(timeZoneGuid != null && timeZoneGuid.length() > 0 && (tz=TimeZone.getTimeZone(timeZoneGuid)) != null)
				transactionTime.setTimeZone(tz);
			transactionID = sanitizeTraceNumber(getAttribute(step, AuthorityAttrEnum.ATTR_TRACE_NUMBER, Long.class, true));
			currencyCd = getAttribute(step, AuthorityAttrEnum.ATTR_CURRENCY_CD, String.class, true);
			approvalCode = getAttribute(step, AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD, String.class, false);
		} catch(AuthorityAttributeConversionException e) {
			return AuthResultCode.FAILED;
		}

		POSRequestHandler handler;
		try {
			handler = locator.getposinterface(new URL(remoteServerAddr));
		} catch(javax.xml.rpc.ServiceException e) {
			log.warn("While connecting to POS Interface", e);
			return populateDeclinedResults(action, taskInfo.getStep().getResultAttributes(), null, e.getMessage());
		} catch(MalformedURLException e) {
			log.warn("While connecting to POS Interface", e);
			return populateDeclinedResults(action, taskInfo.getStep().getResultAttributes(), null, "Could not connect to '" + remoteServerAddr + "'");
		}
		
	    Stub stub = (Stub) handler;
	    //set timeout to 0 for configuration properties to take effect
	    stub.setTimeout(0);
		
		Map<String, Object> resultAttributes = taskInfo.getStep().getResultAttributes();
		switch(action) {
			case AUTHORIZATION:
				// XXX: Is this right: send a zero amount sale?
				SettleResponse sresp;
				try {
					sresp = handler.settle(transactionID, transactionTime, terminalCd, cardNumber, currencyCd, 0, approvalCode);
				} catch(POSException e) {
					log.warn("While connecting to POS Interface", e);
					return populateExceptionResults(action, taskInfo.getStep().getResultAttributes(), e);
				} catch(RemoteException e) {
					log.warn("While connecting to POS Interface", e);
					return populateDeclinedResults(action, taskInfo.getStep().getResultAttributes(), null, e.getMessage());
				}
				return populateResults(sresp, null, transactionID, taskInfo.getStep().getResultAttributes());
			default:
				String text = "Reversal of Action '" + action + "' NOT implemented"; 
				log.warn(text);
				resultAttributes.put("authorityRespDesc", text);
				return AuthResultCode.FAILED;
		}
	}
	protected AuthResultCode populateExceptionResults(AuthorityAction action, Map<String, Object> resultAttributes, POSException e) {
		if(e.getStatusCode() < 0 || e.getStatusCode() > 3) {
			return populateFailedResults(action, resultAttributes, null, e.getDescription());
		}
		resultAttributes.put("authResultCd", 'N');
		resultAttributes.put("authorityRespCd", e.getStatusCode());
		resultAttributes.put("authorityRespDesc", e.getResponseMessage());
		return AuthResultCode.DECLINED;
	}

	protected AuthResultCode populateResults(SettleResponse response, BigInteger requestedAmount, int transactionID, Map<String, Object> resultAttributes) {
		resultAttributes.put("authorityRespCd", response.getStatusCode());
		if(response.getTransactionID() != transactionID) { // mis-matched tran id - approval fails
			resultAttributes.put("authResultCd", 'F');
			resultAttributes.put("authorityRespDesc", "Mis-matched transactionID: sent '" + transactionID + "', received '" + response.getTransactionID() + "'");
			return AuthResultCode.FAILED;
		}
		switch(response.getStatusCode()) {
			case 0: // Approved
				resultAttributes.put("authResultCd", 'Y');
				resultAttributes.put("authorityRespDesc", response.getResponseMessage());
				resultAttributes.put("settled", true);
				resultAttributes.put("approvedAmount", requestedAmount);
				return AuthResultCode.APPROVED;
			case 1: // Declined
				resultAttributes.put("authResultCd", 'N');
				resultAttributes.put("authorityRespDesc", response.getResponseMessage());
				return AuthResultCode.DECLINED;
			case 2: // Failed
				resultAttributes.put("authResultCd", 'F');
				resultAttributes.put("authorityRespDesc", response.getResponseMessage());
				return AuthResultCode.FAILED;
			case 3: // Conditionally Approved - XXX: What should we do here?
				resultAttributes.put("authResultCd", 'P');
				resultAttributes.put("authorityRespDesc", response.getResponseMessage());
				resultAttributes.put("settled", true);
				return AuthResultCode.PARTIAL;
			default:
				resultAttributes.put("authResultCd", 'N');
				String text = "Unknown statusCode " + response.getStatusCode() + " in response";
				resultAttributes.put("authorityRespDesc", text);
				return AuthResultCode.DECLINED;
		}
	}

	protected AuthResultCode populateResults(RefundResponse response, BigInteger requestedAmount, int transactionID, Map<String, Object> resultAttributes) {
		resultAttributes.put("authorityRespCd", response.getStatusCode());
		if(response.getTransactionID() != transactionID) { // mis-matched tran id - approval fails
			resultAttributes.put("authResultCd", 'F');
			resultAttributes.put("authorityRespDesc", "Mis-matched transactionID: sent '" + transactionID + "', received '" + response.getTransactionID() + "'");
			return AuthResultCode.FAILED;
		}
		switch(response.getStatusCode()) {
			case 0: // Approved
				resultAttributes.put("authResultCd", 'Y');
				resultAttributes.put("authorityRespDesc", response.getResponseMessage());
				resultAttributes.put("settled", true);
				resultAttributes.put("approvedAmount", requestedAmount);
				return AuthResultCode.APPROVED;
			case 1: // Declined
				resultAttributes.put("authResultCd", 'N');
				resultAttributes.put("authorityRespDesc", response.getResponseMessage());
				return AuthResultCode.DECLINED;
			case 2: // Failed
				resultAttributes.put("authResultCd", 'F');
				resultAttributes.put("authorityRespDesc", response.getResponseMessage());
				return AuthResultCode.FAILED;
			case 3: // Conditionally Approved - XXX: What should we do here?
				resultAttributes.put("authResultCd", 'P');
				resultAttributes.put("authorityRespDesc", response.getResponseMessage());
				resultAttributes.put("settled", true);
				return AuthResultCode.PARTIAL;
			default:
				resultAttributes.put("authResultCd", 'N');
				String text = "Unknown statusCode " + response.getStatusCode() + " in response";
				resultAttributes.put("authorityRespDesc", text);
				return AuthResultCode.DECLINED;
		}
	}

	protected AuthResultCode populateAuthResults(AuthorizeResponse response, BigInteger requestedAmount, int transactionID, Map<String, Object> resultAttributes, boolean allowPartialAuth) {
		resultAttributes.put("authorityRespCd", response.getStatusCode());
		AuthResultCode authResultCode;
		if(response.getTransactionID() != transactionID) { // mis-matched tran id - approval fails
			resultAttributes.put("authResultCd", 'F');
			resultAttributes.put("authorityRespDesc", "Mis-matched transactionID: sent '" + transactionID + "', received '" + response.getTransactionID() + "'");
			authResultCode = AuthResultCode.FAILED;
			setClientText(resultAttributes, authResultCode, DeniedReason.INVALID_RESPONSE, "Mis-matched transaction id");
		} else
			switch(response.getStatusCode()) {
				case 0: // Approved
					resultAttributes.put("authResultCd", 'Y');
					resultAttributes.put("authorityRespDesc", response.getResponseMessage());
					resultAttributes.put("authAuthorityTranCd", response.getApprovalCode());
					resultAttributes.put("authAuthorityRefCd", response.getApprovalCode());
					resultAttributes.put("approvedAmount", response.getApprovedAmount());
					resultAttributes.put("authHoldUsed", true);
					authResultCode = AuthResultCode.APPROVED;
					setClientText(resultAttributes, authResultCode, null, null, response.getApprovedAmount(), null, response.getResponseMessage());
					break;
				case 1: // Declined
					resultAttributes.put("authResultCd", 'N');
					resultAttributes.put("authorityRespDesc", response.getResponseMessage());
					resultAttributes.put("authAuthorityTranCd", response.getApprovalCode());
					resultAttributes.put("authAuthorityRefCd", response.getApprovalCode());
					authResultCode = AuthResultCode.DECLINED;
					setClientText(resultAttributes, authResultCode, null, response.getResponseMessage());
					break;
				case 2: // Failed
					resultAttributes.put("authResultCd", 'F');
					resultAttributes.put("authorityRespDesc", response.getResponseMessage());
					resultAttributes.put("authAuthorityTranCd", response.getApprovalCode());
					resultAttributes.put("authAuthorityRefCd", response.getApprovalCode());
					authResultCode = AuthResultCode.FAILED;
					setClientText(resultAttributes, authResultCode, null, response.getResponseMessage());
					break;
				case 3: // Conditionally Approved
					if(allowPartialAuth && response.getApprovedAmount() > 0) {
						resultAttributes.put("authResultCd", 'P');
						resultAttributes.put("approvedAmount", response.getApprovedAmount());
						authResultCode = AuthResultCode.PARTIAL;
						setClientText(resultAttributes, authResultCode, null, null, response.getApprovedAmount(), null, response.getResponseMessage());
					} else {
						resultAttributes.put("authResultCd", 'N');
						authResultCode = AuthResultCode.DECLINED;
						setClientText(resultAttributes, authResultCode, DeniedReason.INSUFFICIENT_FUNDS, response.getResponseMessage(), response.getApprovedAmount(), null);
					}
					resultAttributes.put("authorityRespDesc", response.getResponseMessage());
					resultAttributes.put("authAuthorityTranCd", response.getApprovalCode());
					resultAttributes.put("authAuthorityRefCd", response.getApprovalCode());
					resultAttributes.put("authHoldUsed", true); // XXX: not sure what to do here
					break;
				default:
					resultAttributes.put("authResultCd", 'N');
					String text = "Unknown statusCode " + response.getStatusCode() + " in response";
					resultAttributes.put("authorityRespDesc", text);
					authResultCode = AuthResultCode.DECLINED;
					setClientText(resultAttributes, authResultCode, null, response.getResponseMessage());
			}
		return authResultCode;
	}

	protected static int sanitizeTraceNumber(long traceNumber) {
		return Math.abs((int) traceNumber);
	}

	@Override
	protected String[] getAdditionalPreActionLogAttributes() {
		return new String[] {
			MessageAttribute.REMOTE_SERVER_ADDR.getValue()
		};
	}
}
