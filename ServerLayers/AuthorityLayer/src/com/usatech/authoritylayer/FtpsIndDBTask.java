package com.usatech.authoritylayer;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Map;

import simple.app.Publisher;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.ByteInput;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;

import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.MessageChainV11;
import com.usatech.ftps.FtpsBean;

import ftp.FtpException;
import ftp.FtpListResult;
/**
 * The task handles the ftps of the file and decide whether it should refresh the ind_db_ardef.txt
 * @author yhe
 *
 */
public class FtpsIndDBTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected ResourceFolder resourceFolder;
	protected Publisher<ByteInput> publisher;
	protected String refreshIndDBQueueKey;
	protected FtpSite indDbFileSite;
	protected String indDbFileName="ind_db_ardef.txt";
	protected final DateFormat[] indDbFileDateFormatters = new DateFormat[] {
			(DateFormat)ConvertUtils.getFormat("DATE", "MM-dd-yy hh:mma"),
			(DateFormat)ConvertUtils.getFormat("DATE", "MMM dd yyyy"),			
	};
	protected int maxRetryAllowed=5;

	protected Date parseFileDate(String dateStr) throws ConvertException {
		for(DateFormat df : indDbFileDateFormatters) {
			try {
				return df.parse(dateStr);
			} catch(ParseException e) {
				// ignore
			}
		}
		return ConvertUtils.convertRequired(Date.class, dateStr);
	}
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		Map<String, Object> attributes = taskInfo.getStep().getAttributes();
		long currentIndDbModTime=ConvertUtils.getLongSafely(attributes.get("currentIndDbModTime"), 0);
		try{
			try{
				FtpsBean ftpsBean=indDbFileSite.connect();
				FtpListResult ftpListResult=ftpsBean.getFileInfo(indDbFileName);
				Long newIndDbModTime=null;
				if(ftpListResult==null){
					throw new ServiceException("Failed to get ind_db_ardef.txt fileInfo.");
				} else if(ftpListResult.next()) {
					newIndDbModTime = parseFileDate(ftpListResult.getDate()).getTime();
				} else {
					log.warn("ind_db_ardef.txt not found. No refresh.");
					return 0;
				}
				Resource resource;
				log.info("newIndDbModTime="+newIndDbModTime);
				if(currentIndDbModTime==newIndDbModTime){
					log.info("ind_db_ardef.txt not changed. No refresh.");
				}else{
					resource=resourceFolder.getResource(indDbFileName, ResourceMode.CREATE);
					try{
						ftpsBean.getFile(indDbFileName, 0, resource.getOutputStream());
					}finally{
						resource.release();
					}
					MessageChainV11 messageChain = new MessageChainV11();
					MessageChainStep refreshStep = messageChain.addStep(refreshIndDBQueueKey, true);
					refreshStep.addLongAttribute("newIndDbModTime", newIndDbModTime);
					refreshStep.addStringAttribute("file.fileKey", resource.getKey());
					MessageChainService.publish(messageChain, publisher);
					log.info("FtpsIndDBTask done. Refresh request to applayer sent."); 
				}
				Map<String, Object> resultAttributes=taskInfo.getStep().getResultAttributes();
				resultAttributes.put("newIndDbModTime", newIndDbModTime);
				return 0;
			} catch(ConvertException e) {
				throw new ServiceException("Failed to do FtpsIndDBTask.", e);
			}catch(FtpException e){
				throw new ServiceException("Failed to do FtpsIndDBTask.", e);
			}catch(IOException e){
				throw new ServiceException("Failed to do FtpsIndDBTask.", e);
			}
		}catch(ServiceException e){
			if(taskInfo.getRetryCount() >= maxRetryAllowed){
				log.info("Reach maxRetryAllowed="+maxRetryAllowed, e);
				Map<String, Object> resultAttributes=taskInfo.getStep().getResultAttributes();
				resultAttributes.put("newIndDbModTime", currentIndDbModTime);
				return 0;
			}else{
				throw e;
			}
		}
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}

	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}

	public void setRefreshIndDBQueueKey(String refreshIndDBQueueKey) {
		this.refreshIndDBQueueKey = refreshIndDBQueueKey;
	}

	public void setIndDbFileSite(FtpSite indDbFileSite) {
		this.indDbFileSite = indDbFileSite;
	}

	public void setIndDbFileName(String indDbFileName) {
		this.indDbFileName = indDbFileName;
	}

	public int getMaxRetryAllowed() {
		return maxRetryAllowed;
	}

	public void setMaxRetryAllowed(int maxRetryAllowed) {
		this.maxRetryAllowed = maxRetryAllowed;
	}
	
	
}
