package com.usatech.authoritylayer;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;

import org.apache.http.HttpException;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.text.StringUtils;

import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAction;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.DeniedReason;
import com.usatech.layers.common.constants.MessageAttribute;
import com.usatech.layers.common.constants.PaymentMaskBRef;
import com.usatech.layers.common.sprout.SproutAccountResponse;
import com.usatech.layers.common.sprout.SproutExcpetion;
import com.usatech.layers.common.sprout.SproutLoadOrRefundResponse;
import com.usatech.layers.common.sprout.SproutPurchaseItem;
import com.usatech.layers.common.sprout.SproutPurchaseResponse;
import com.usatech.layers.common.sprout.SproutReversalResponse;
import com.usatech.layers.common.sprout.SproutUtils;

public class SproutGatewayTask extends AbstractAuthorityTask {
	public SproutGatewayTask() {
		super("sprout");
		SproutUtils.setSproutUrl(sproutUrl);
		SproutUtils.setSproutAuthorizationToken(sproutAuthorizationToken);
		if(hostNameVerifier!=null){
			SproutUtils.setHostNameVerifier(hostNameVerifier);
		}
		sproutUtils=new SproutUtils();
		sproutUtils.setConnectionTimeout(connectionTimeout);
		sproutUtils.setSocketTimeout(socketTimeout);
		sproutUtils.setMaxHostConnections(maxHostConnections);
		sproutUtils.setMaxTotalConnections(maxTotalConnections);
	}

	private static final Log log = Log.getLog();
	 
	protected static String sproutAuthorizationToken = "";
	protected static String sproutUrl;
	
	protected static HostnameVerifier hostNameVerifier;
	protected static int connectionTimeout;
	protected static int socketTimeout;
	protected static int maxHostConnections;
	protected static int maxTotalConnections;
	protected SproutUtils sproutUtils;
	protected static String moreProgramCode="default";
	protected static String loadMerchantId="7est7ing";
	

	@Override
	protected AuthResultCode processRequest(AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		String terminalCd;
		String cardNumber;
		String oanLast4Digit;
		BigDecimal amount;
		BigDecimal minorCurrencyFactor;
		String consumerEmail;
		Integer vendorId;
		Integer customerId;
		StringBuilder url=new StringBuilder();
		String remoteServerAddr;
		try {
			remoteServerAddr = getAttribute(step, AuthorityAttrEnum.ATTR_REMOTE_SERVER_ADDRESS, String.class, true);
			url = new StringBuilder(remoteServerAddr);
			if (!remoteServerAddr.endsWith("/"))
				url.append("/");
			SproutUtils.setSproutUrl(url.toString());
			terminalCd = getAttribute(step, AuthorityAttrEnum.ATTR_TERMINAL_CD, String.class, true);
			AccountData accountData = new AccountData(step.getAttributes());
			cardNumber = accountData.getPartData(PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER);
			if(StringUtils.isBlank(cardNumber)) {
				cardNumber = accountData.getTrackData();
			}
			minorCurrencyFactor = getAttribute(step, AuthorityAttrEnum.ATTR_MINOR_CURRENCY_FACTOR, BigDecimal.class, true);
			amount = getAttribute(step, AuthorityAttrEnum.ATTR_AMOUNT, BigDecimal.class, true).divide(minorCurrencyFactor);
			oanLast4Digit = accountData.getPartData(PaymentMaskBRef.DISCRETIONARY_DATA);
			consumerEmail=getAttribute(step, AuthorityAttrEnum.ATTR_CONSUMER_EMAIL, String.class, false);
			vendorId=getAttribute(step, AuthorityAttrEnum.ATTR_SPROUT_VENDOR_ID, Integer.class, false);
			customerId=getAttribute(step, AuthorityAttrEnum.ATTR_CUSTOMER_ID, Integer.class, false);
		} catch(AuthorityAttributeConversionException e) {
			return AuthResultCode.FAILED;
		}				
		
		if (action == AuthorityAction.AUTHORIZATION) {
			try {
				log.info(new StringBuilder("Authorize Request, cardNumber: ").append(MessageResponseUtils.maskCard(cardNumber))
						.append(", amount: ").append(amount).toString());
	    		return populateAuthResults(cardNumber, oanLast4Digit, customerId,consumerEmail, vendorId, amount, minorCurrencyFactor,taskInfo.getStep().getResultAttributes());
			} catch(Exception e) {
				log.error("Error processing Sprout request", e);
				return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), null, e.getMessage());
			} 
		} else if(action == AuthorityAction.SALE) {
			try {
				String authTypeCd = getAttribute(step, AuthorityAttrEnum.ATTR_AUTH_TYPE_CD, String.class, true);
				if (taskInfo.isRedelivered()&&!authTypeCd.equals("O")) {
					//let POSMLayer make another purchase API call with a new saleAuthId
					return populateDeclinedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.INTERNAL_TIMEOUT, "Sprout Purchase Timeout");
				}
			
				String tranId = getAttribute(step, AuthorityAttrEnum.ATTR_TRAN_ID, String.class, true);
				String saleAuthId = getAttribute(step, AuthorityAttrEnum.ATTR_SALE_AUTH_ID, String.class, true);
				
				if(authTypeCd.equals("O")){//reveral for load credit
					int statusCode;
					Map<String,Object> resultAttributes=taskInfo.getStep().getResultAttributes();
					try{
						String originalExternalReferenceId = getAttribute(step, AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, String.class, true);
						SproutReversalResponse reveralResp=sproutUtils.reversal("reversal-"+saleAuthId, originalExternalReferenceId, "load", terminalCd);
						resultAttributes.put("authAuthorityRefCd", reveralResp.getTransaction_id());
						statusCode=200;
						resultAttributes.put("authResultCd", 'Y');
						resultAttributes.put("settled", true);
						resultAttributes.put("authorityRespCd", statusCode);
						resultAttributes.put("authorityRespDesc", "Successful reversal for "+originalExternalReferenceId);
						return AuthResultCode.APPROVED;
					}catch(SproutExcpetion e){
						statusCode=e.getHttpStatus();
						String errorMsg=e.getMessage()==null?"":e.getMessage();
						log.info(new StringBuilder("Reversal Response, statusCode: ").append(statusCode).append(" response:").append(errorMsg).toString());
						if(e.getCode()==40||e.getCode()==41){// for already refunded or reversed, let it pass
							resultAttributes.put("authResultCd", 'Y');
							resultAttributes.put("settled", true);
							resultAttributes.put("authorityRespDesc", errorMsg);
							return AuthResultCode.APPROVED;
						}else{
							resultAttributes.put("authResultCd", 'F');
							resultAttributes.put("authorityRespDesc", errorMsg);
							return AuthResultCode.FAILED;
						}
					}
				}else{
				int itemCount = getAttribute(step, AuthorityAttrEnum.ATTR_ITEM_COUNT, int.class, true);
				
				log.info(new StringBuilder("Purchase Request, tranId: ").append(tranId).append(", saleAuthId: ").append(saleAuthId)
						.append(", cardNumber: ").append(MessageResponseUtils.maskCard(cardNumber))
						.append(", terminalCd: ").append(terminalCd)
						.append(", amount: ").append(amount)
						.append(", itemCount: ").append(itemCount).toString());
				
				if (amount.compareTo(BigDecimal.ZERO) <= 0)
					throw new ServiceException("Sprout sale amount must be greater than 0");
				
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("tranId", tranId);
				
				boolean massagePrices = false;
				List<SproutPurchaseItem> items = new ArrayList<SproutPurchaseItem>();
				if (itemCount > 0) {					
					Map<String, Object> attributes = step.getAttributes();
					for (int i = 1; i <= itemCount; i++) {						
						String product = ConvertUtils.convertRequired(String.class, attributes.get(new StringBuilder("product").append(i).toString()));
						BigDecimal price = ConvertUtils.convertRequired(BigDecimal.class, attributes.get(new StringBuilder("price").append(i).toString()));
						BigDecimal quantity = ConvertUtils.convertRequired(BigDecimal.class, attributes.get(new StringBuilder("quantity").append(i).toString()));
						BigDecimal sproutPrice = price.multiply(quantity);
						int compareResult = sproutPrice.compareTo(BigDecimal.ZERO);
						if (compareResult > 0) {
							SproutPurchaseItem item = new SproutPurchaseItem();
							item.setCoil_name(product);
							item.setPrice(sproutPrice);
							items.add(item);
						} else if (compareResult < 0)
							massagePrices = true;
					}
				}
				int sproutItemCount = items.size();
				if (sproutItemCount < 1) {
					SproutPurchaseItem item = new SproutPurchaseItem();
					item.setPrice(amount);
					items.add(item);
				} else if (massagePrices) {
					/* Sprout doesn't support negative prices so if there happens to be a line item with a negative amount
					we have to be creative and massage the price amounts so the sale would go through, ugh...*/
					if (sproutItemCount == 1)
						items.get(0).setPrice(amount);
					else {
						BigDecimal averagePrice = amount.divide(new BigDecimal(sproutItemCount)).setScale(2, RoundingMode.HALF_UP);
						for (int i = 0; i < sproutItemCount - 1; i++)
							items.get(i).setPrice(averagePrice);
						BigDecimal lastItemPrice = amount.subtract(averagePrice.multiply(new BigDecimal(sproutItemCount - 1)));
						if (lastItemPrice.compareTo(BigDecimal.ZERO) > 0)
							items.get(sproutItemCount - 1).setPrice(lastItemPrice);
						else
							items.get(sproutItemCount - 1).setPrice(new BigDecimal("0.01"));
					}
				}
				
				return populateResults(cardNumber, saleAuthId, terminalCd, items, taskInfo.getStep().getResultAttributes());
				}
			} catch(IOException|HttpException e) {
				log.error("IOException while processing a Sprout purchase", e);
				throw new RetrySpecifiedServiceException("Sprout Purchase IOException", WorkRetryType.NONBLOCKING_RETRY);
			} catch(Exception e) {
				log.error("Error processing Sprout request", e);
				return populateDeclinedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.AUTH_LAYER_PROCESSING_ERROR, e.getMessage());
			} 
		}else if (action == AuthorityAction.REFUND) {
			try{
				return processRefund(step,cardNumber, amount, terminalCd);
			} catch(Exception e) {
				log.error("Error processing Sprout request", e);
				return populateDeclinedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.AUTH_LAYER_PROCESSING_ERROR, e.getMessage());
			} 
		} else
			throw new RetrySpecifiedServiceException("Action '" + action + "' NOT implemented", WorkRetryType.NO_RETRY);
	}
	
	protected AuthResultCode processRefund(MessageChainStep step,String cardNumber, BigDecimal amount, String vampirePOSId) throws AuthorityAttributeConversionException, ConvertException{
		Map<String, Object> resultAttributes=step.getResultAttributes();
		String refundId = getAttribute(step, AuthorityAttrEnum.ATTR_TRACE_NUMBER, String.class, true);
		String refundTypeCd = getAttribute(step, AuthorityAttrEnum.ATTR_AUTH_TYPE_CD, String.class, true);
		Integer sproutTransactionId = getAttribute(step, AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, Integer.class, false);
		int statusCode;
		try{
			SproutAccountResponse accountRsp=null;
			try{
				accountRsp=sproutUtils.getAccount(cardNumber);
				if(accountRsp.isActive()==false){
					log.info(new StringBuilder("Auth response:").append(MessageResponseUtils.maskAnyCardNumbers(accountRsp.toString())).toString());
					resultAttributes.put("authResultCd", 'F');
					resultAttributes.put("authorityRespDesc", "Sprout card is not active any more");
					return AuthResultCode.FAILED;
				}
			}catch(SproutExcpetion e){
				statusCode=e.getHttpStatus();
				String errorMsg=e.getMessage()==null?"":e.getMessage();
				log.info(new StringBuilder("Auth Response, statusCode: ").append(statusCode).append(" response:").append(errorMsg).toString());
				resultAttributes.put("authResultCd", 'F');
				resultAttributes.put("authorityRespDesc", errorMsg);
				return AuthResultCode.FAILED;
			}
			String external_reference_id=null;
			if(refundTypeCd.equals("S")){//replenish case
				int itemCount = getAttribute(step, AuthorityAttrEnum.ATTR_ITEM_COUNT, int.class, true);
				int tranLineItemTypeId=556;
				if (itemCount > 0) {					
					Map<String, Object> attributes = step.getAttributes();
					for (int i = 1; i <= itemCount; i++) {						
						tranLineItemTypeId = ConvertUtils.convertRequired(int.class, attributes.get(new StringBuilder("tranLineItemTypeId").append(i).toString()));
					}
				}
				String loadType=null;
				String refundPrefix=null;
				if(tranLineItemTypeId==556||tranLineItemTypeId==560){
					loadType="card";
					refundPrefix="replenish-";
				}else{
					loadType="cash";
					refundPrefix="bonuscash-";
				}
				external_reference_id=new StringBuilder(refundPrefix).append(refundId).toString();
				SproutLoadOrRefundResponse loadResp=sproutUtils.load(accountRsp.getAccount_id(), amount, external_reference_id, loadMerchantId, vampirePOSId, loadType);
				resultAttributes.put("authAuthorityRefCd", loadResp.getTransaction_id());
				log.info(new StringBuilder("Replenish success response: ").append(MessageResponseUtils.maskAnyCardNumbers(loadResp.toString())).toString());
			}else if(refundTypeCd.equals("G")){//refund sprout card purchase
				external_reference_id=new StringBuilder("refund-").append(refundId).toString();
				SproutLoadOrRefundResponse resp =sproutUtils.refund(external_reference_id, sproutTransactionId, "Sprout Purchase Refund");
				log.info(new StringBuilder("Refund purchase success response: ").append(MessageResponseUtils.maskAnyCardNumbers(resp.toString())).toString());
			}
			statusCode=200;
			resultAttributes.put("authResultCd", 'Y');
			resultAttributes.put("settled", true);
			resultAttributes.put("authorityRespCd", statusCode);
			resultAttributes.put("authorityRespDesc", "Successful "+external_reference_id);
			return AuthResultCode.APPROVED;
		}catch(SproutExcpetion e){
			statusCode=e.getHttpStatus();
			String errorMsg=e.getMessage()==null?"":e.getMessage();
			log.info(new StringBuilder("Auth Response, statusCode: ").append(statusCode).append(" response:").append(errorMsg).toString());
			if(e.getCode()==103||e.getCode()==40||e.getCode()==41){//dup of external_reference_id or already refunded/reversed, let it pass
				resultAttributes.put("authResultCd", 'Y');
				resultAttributes.put("settled", true);
				resultAttributes.put("authorityRespDesc", errorMsg);
				return AuthResultCode.APPROVED;
			}else{
				resultAttributes.put("authResultCd", 'F');
				resultAttributes.put("authorityRespDesc", errorMsg);
				return AuthResultCode.FAILED;
			}
		}
	}

	/**
	 * @see com.usatech.authoritylayer.AbstractAuthorityTask#processReversal(com.usatech.layers.common.constants.AuthorityAction, com.usatech.app.MessageChainTaskInfo)
	 */
	@Override
	protected AuthResultCode processReversal(AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		Map<String, Object> resultAttributes = taskInfo.getStep().getResultAttributes();
		if (action == AuthorityAction.SALE) {
			try {
				String authTypeCd = getAttribute(step, AuthorityAttrEnum.ATTR_AUTH_TYPE_CD, String.class, true);
				String terminalCd = getAttribute(step, AuthorityAttrEnum.ATTR_TERMINAL_CD, String.class, true);
				String saleAuthId = getAttribute(step, AuthorityAttrEnum.ATTR_SALE_AUTH_ID, String.class, true);
				if(authTypeCd.equals("O")){
					log.info(new StringBuilder("Sprout replenish refund. No need to reverse for redelivery:").append(saleAuthId));
					resultAttributes.put("authResultCd", 'Y');
					return AuthResultCode.APPROVED;
				}
				
				log.info(new StringBuilder("Reversal Request, saleAuthId: ").append(saleAuthId)
						.append(", terminalCd: ").append(terminalCd).toString());
				String external_reference_id=new StringBuilder(saleAuthId).append(System.currentTimeMillis()).toString();
				log.info(new StringBuilder("Reversal Request, request: ").append("external_reference_id:").append(external_reference_id).append("original_external_reference_id:").append(saleAuthId).append("vampire_pos_id:").append(terminalCd).toString());
				return populateReversalResults(external_reference_id, saleAuthId, terminalCd, taskInfo.getStep().getResultAttributes());
			} catch (RetrySpecifiedServiceException e) {
				throw e;
			} catch(Exception e) {
				log.error("Error processing Sprout reversal", e);
				throw new RetrySpecifiedServiceException("Sprout Reversal Error", WorkRetryType.NONBLOCKING_RETRY);
			} 
		} else {
			String text = "Reversal of Action '" + action + "' NOT implemented"; 
			log.warn(text);
			resultAttributes.put("authorityRespDesc", text);
			return AuthResultCode.FAILED;
		}
	}
	
	protected AuthResultCode populateAuthResults(String cardNumber, String oanLast4Digit, Integer customerId, String consumerEmail, Integer vendorId, BigDecimal requestedAmount, BigDecimal minorCurrencyFactor,Map<String, Object> resultAttributes) {
		SproutAccountResponse accountRsp=null;
		int statusCode;
		resultAttributes.put("authHoldUsed", false);
		try{
			accountRsp=sproutUtils.getAccount(cardNumber);
			statusCode=200;
			resultAttributes.put("authorityRespCd", statusCode);
			resultAttributes.put("sproutAccountId", accountRsp.getAccount_id());
			BigInteger balanceAmount = accountRsp.getAvailable_balance().multiply(minorCurrencyFactor).toBigInteger();
			resultAttributes.put("balanceAmount", balanceAmount);
			log.info(new StringBuilder("Auth Response, statusCode: ").append(statusCode).append(", response: ").append(MessageResponseUtils.maskAnyCardNumbers(accountRsp.toString())).toString());
			resultAttributes.put("authorityRespDesc", "Available Balance: " + accountRsp.getAvailable_balance());
			if(StringUtils.isBlank(consumerEmail)){
				if (accountRsp.getAvailable_balance().compareTo(requestedAmount) >= 0) {
					resultAttributes.put("authResultCd", 'Y');
					return AuthResultCode.APPROVED;
				} else {
					resultAttributes.put("authResultCd", 'N');
					return AuthResultCode.DECLINED;
				}
			}else{
				// for the getCardInfoPlain from Sprout prepaid registration
				if(customerId!=null&&consumerEmail.equalsIgnoreCase("merchant_"+customerId+"@usatech.com")){
					resultAttributes.put("authResultCd", 'Y');
					return AuthResultCode.APPROVED;
				}else{
					List<String> aliases= accountRsp.getAliases();
					if(aliases==null||aliases.size()==0){
						resultAttributes.put("authResultCd", 'N');
						return AuthResultCode.DECLINED;
					}else{
						boolean isOANMatch=false;
						for(String alias:aliases){
							if(alias!=null&&!alias.equals(cardNumber)&&alias.endsWith(oanLast4Digit)){
								isOANMatch=true;
								break;
							}
						}
						if(isOANMatch){
							resultAttributes.put("authResultCd", 'Y');
							return AuthResultCode.APPROVED;
						}else{
							log.info("Auth Response mismatch oanLast4Digit="+oanLast4Digit);
							resultAttributes.put("authResultCd", 'N');
							resultAttributes.put("authorityRespDesc", "Auth Response OAN last 4 digit mismatch");
							return AuthResultCode.DECLINED;
						}
					}
				}
			}
		}catch(SproutExcpetion e){
			statusCode=e.getHttpStatus();
			resultAttributes.put("authorityRespCd", statusCode);
			String errorMsg=e.getMessage()==null?"":e.getMessage();
			log.info(new StringBuilder("Auth Response, statusCode: ").append(statusCode).append(" response:").append(errorMsg).toString());
			if(statusCode==400||statusCode==401){
				resultAttributes.put("authResultCd", 'F');
				resultAttributes.put("authorityRespDesc", errorMsg);
				return AuthResultCode.FAILED;
			}else{
				if(StringUtils.isBlank(consumerEmail)){
					resultAttributes.put("authResultCd", 'N');
					resultAttributes.put("authorityRespDesc", errorMsg);
					return AuthResultCode.DECLINED;
				}else{// for the getCardInfoPlain from Sprout prepaid registration
					if(statusCode==404){
						
						try{
							if(vendorId==null){
								resultAttributes.put("authResultCd", 'N');
								resultAttributes.put("authorityRespDesc", "Vendor Id is not provided");
								return AuthResultCode.DECLINED;
							}
							sproutUtils.createAccout(cardNumber, moreProgramCode, vendorId, consumerEmail);
							accountRsp=sproutUtils.getAccount(cardNumber);
							resultAttributes.put("authorityRespCd", 200);
							resultAttributes.put("sproutAccountId", accountRsp.getAccount_id());
							resultAttributes.put("authorityRespDesc", "Available Balance: " + accountRsp.getAvailable_balance());
							// for the getCardInfoPlain from Sprout prepaid registration
							resultAttributes.put("authResultCd", 'Y');
							return AuthResultCode.APPROVED;
						}catch(SproutExcpetion se){
							int httpStatus=e.getHttpStatus();
							log.info("Sprout create account:"+httpStatus+" "+e.getMessage());
							resultAttributes.put("authResultCd", 'N');
							resultAttributes.put("authorityRespDesc", errorMsg);
							return AuthResultCode.DECLINED;
						}
						
					}else{
						resultAttributes.put("authResultCd", 'N');
						resultAttributes.put("authorityRespDesc", errorMsg);
						return AuthResultCode.DECLINED;
					}
				}
			}
			
		}
	}

	protected AuthResultCode populateResults(String account_info,String external_reference_id, String vampire_pos_id, List<SproutPurchaseItem> items, Map<String, Object> resultAttributes) throws HttpException, IOException{
		SproutPurchaseResponse purchaseRsp=null;
		int statusCode;
		try{
			log.info(new StringBuilder("Purchase Request, request: ").append(MessageResponseUtils.maskAnyCardNumbers(account_info)).append("external_reference_id:").append(external_reference_id).append("vampire_pos_id:").append(vampire_pos_id).append("items:").append(items).toString());
			purchaseRsp=sproutUtils.purchase(account_info, external_reference_id, vampire_pos_id, items);
			statusCode=201;
			resultAttributes.put("authorityRespCd", statusCode);
			if (purchaseRsp == null) {
				resultAttributes.put("authResultCd", 'F');
				resultAttributes.put("authorityRespDesc", "Received empty response");
				return AuthResultCode.FAILED;
			}else{
				log.info(new StringBuilder("SproutPurchase").append(" Response, statusCode: ").append(statusCode).append(", response: ").append(MessageResponseUtils.maskAnyCardNumbers(purchaseRsp.toString())).toString());
				int transactionId = purchaseRsp.getTransaction_id();
				resultAttributes.put("authorityRespDesc", new StringBuilder("New Available Balance: ").append(purchaseRsp.getNew_available_balance()));
				resultAttributes.put("authAuthorityRefCd", transactionId);
				if (transactionId>0) {
					resultAttributes.put("authResultCd", 'Y');
					resultAttributes.put("settled", true);
					return AuthResultCode.APPROVED;
				} else {
					resultAttributes.put("authResultCd", 'N');
					return AuthResultCode.DECLINED;
				}
			}
		}catch(SproutExcpetion e){
			statusCode=e.getHttpStatus();
			resultAttributes.put("authorityRespCd", statusCode);
			String errorMsg=e.getMessage()==null?"":e.getMessage();
			log.info(new StringBuilder("Purchase Response, statusCode: ").append(statusCode).append(" response:").append(errorMsg).toString());
			resultAttributes.put("authResultCd", 'N');
			resultAttributes.put("authorityRespDesc", errorMsg);
			return AuthResultCode.DECLINED;
		}catch(org.apache.http.HttpException|IOException e){
			throw e;
		}
	}
	
	protected AuthResultCode populateReversalResults(String external_reference_id, String original_external_reference_id, String vampire_pos_id, Map<String, Object> resultAttributes) throws RetrySpecifiedServiceException {
		SproutReversalResponse reversalRsp=null;
		int statusCode;
		try{
			reversalRsp=sproutUtils.reversal(external_reference_id, original_external_reference_id, "purchase", vampire_pos_id);
			statusCode=201;
			log.info(new StringBuilder("Reversal Response, statusCode: ").append(statusCode).append(", response: ").append(MessageResponseUtils.maskAnyCardNumbers(reversalRsp.toString())).toString());
			resultAttributes.put("authResultCd", 'Y');
			return AuthResultCode.APPROVED;
		}catch(SproutExcpetion e){
			statusCode=e.getHttpStatus();
			String errorMsg=e.getMessage()==null?"":e.getMessage();
			log.info(new StringBuilder("Reversal Response, statusCode: ").append(statusCode).append(", response: ").append(errorMsg).toString());
			if(statusCode==422||statusCode==404){//unprocessable entity, purchase has already been reversed
				resultAttributes.put("authResultCd", 'Y');
				return AuthResultCode.APPROVED;
			}else{
				throw new RetrySpecifiedServiceException("Sprout Reversal Error", WorkRetryType.NONBLOCKING_RETRY);
			}
		}
	}	
	

	@Override
	protected String[] getAdditionalPreActionLogAttributes() {
		return new String[] {
			MessageAttribute.REMOTE_SERVER_ADDR.getValue()
		};
	}

	public static void setSproutConnectionTimeout(int sproutConnectionTimeout) {
		SproutGatewayTask.connectionTimeout=sproutConnectionTimeout;
	}

	public static void setSproutSocketTimeout(int sproutSocketTimeout) {
		SproutGatewayTask.socketTimeout=sproutSocketTimeout;
	}
	
	public static void setSproutMaxTotalConnections(int maxTotalConnections) {
		SproutGatewayTask.maxTotalConnections=maxTotalConnections;
	}
	
	public static void setSproutMaxHostConnections(int maxHostConnections) {
		SproutGatewayTask.maxHostConnections=maxHostConnections;
	}

	public static String getSproutAuthorizationToken() {
		return sproutAuthorizationToken;
	}

	public static void setSproutAuthorizationToken(String sproutAuthorizationToken) {
		SproutGatewayTask.sproutAuthorizationToken = sproutAuthorizationToken;
	}

	public static HostnameVerifier getHostNameVerifier() {
		return hostNameVerifier;
	}

	public static void setHostNameVerifier(HostnameVerifier hostNameVerifier) {
		SproutGatewayTask.hostNameVerifier = hostNameVerifier;
	}

	public static String getSproutUrl() {
		return sproutUrl;
	}

	public static void setSproutUrl(String sproutUrl) {
		SproutGatewayTask.sproutUrl = sproutUrl;
	}

	public static String getMoreProgramCode() {
		return moreProgramCode;
	}

	public static void setMoreProgramCode(String moreProgramCode) {
		SproutGatewayTask.moreProgramCode = moreProgramCode;
	}

	public static String getLoadMerchantId() {
		return loadMerchantId;
	}

	public static void setLoadMerchantId(String loadMerchantId) {
		SproutGatewayTask.loadMerchantId = loadMerchantId;
	}
	
}
