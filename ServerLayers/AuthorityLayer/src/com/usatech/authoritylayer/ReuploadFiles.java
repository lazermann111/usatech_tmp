package com.usatech.authoritylayer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

import org.apache.commons.configuration.Configuration;

import simple.app.BaseWithConfig;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.IOUtils;
import simple.io.Log;

import com.usatech.ftps.FtpsBean;

import ftp.FtpException;

public class ReuploadFiles extends BaseWithConfig {
	private static final Log log = Log.getLog();
	
	public static void main(String[] args) {
		new ReuploadFiles().run(args);
	}
	@Override
	protected void registerDefaultCommandLineArguments() {
		super.registerDefaultCommandLineArguments();
		registerCommandLineArgument(false, "files", "A comma-separated list of files to upload");
	}
	@Override
	protected void execute(Map<String, Object> argMap, Configuration config) {
		ElavonClearingFileSite fileSite;
		try {
			fileSite = configure(ElavonClearingFileSite.class, config.subset("com.usatech.authoritylayer.ElavonClearingFileSite"));
		} catch(ServiceException e) {
			finishAndExit("Could not configure remote file site", 206, true, e);
			return;
		}
        if(fileSite == null) {
        	finishAndExit("Property 'fileSite' is not configured from configuration file", 201, true, null);
        	return;
        }
        String[] filePaths;
		try {
			filePaths = ConvertUtils.convert(String[].class, argMap.get("files"));
		} catch(ConvertException e) {
			finishAndExit("Could not convert 'files' argument to an array of Strings", 204, true, e);
			return;
		}
        // Make a client connection
    	FtpsBean ftpsBean;
    	try {
			ftpsBean = fileSite.connect();
		} catch (FtpException e) {
			finishAndExit("Could not connect to " + fileSite.toString(), 202, false, e);
			return;
		} catch(IOException e) {
			finishAndExit("Could not connect to " + fileSite.toString(), 202, false, e);
			return;
		} catch(ServiceException e) {
			finishAndExit("Could not connect to " + fileSite.toString(), 202, false, e);
			return;
		}
		int cnt = 0;
		try {
			log.info("Connected to '" + fileSite + "'");
			ftpsBean.setDirectory("../outbox");
			for(String filePath : filePaths) {
				File file = new File(filePath);
				if(!file.exists()) {
					log.info("Cannot find file '" + filePath + "' in the local filesystem; skipping this");
				} else {
					FileInputStream in = new FileInputStream(file);
					try {
						OutputStream out = ftpsBean.putFileStream(file.getName() + ".tmp", -1);
			            try {
			            	log.info("Writing file '" + filePath + "' to '" + fileSite + "/../outbox'");
			            	IOUtils.copy(in, out);
			            } catch(IOException e) {
			            	log.warn("Could not upload file '" + filePath + "' to '" + fileSite + "/../outbox'", e);
			    		} finally {
			            	try {
			            		out.close();
			            	} catch(IOException e) {
			            		log.info("Could not close file stream: " + e.getMessage());
			            	}
			            }
					} finally {
						in.close();
					}
		            log.info("Renaming file '" + file.getName() + ".tmp" + "' to '" + file.getName() + "'");            	
		            ftpsBean.fileRename(file.getName() + ".tmp", file.getName());
		            cnt++;
				}
			}
		} catch (FtpException e) {
			finishAndExit("Could not upload files '" + fileSite + "/../outbox'", 207, false, e);
			return;
		} catch(IOException e) {
			finishAndExit("Could not upload files '" + fileSite + "/../outbox'", 207, false, e);
			return;
        } finally {
        	try {
        		ftpsBean.close();
        	} catch (NullPointerException e) {
				log.debug("FTP Connection was not open",e);
            } catch (FtpException e) {
				log.warn("Could not close FTP Connection",e);
            } catch(IOException ioe) {
                log.warn("Could not close FTP Connection", ioe);
            }
        }
        log.info("Uploaded " + cnt + " files to '" + fileSite + "/../outbox'");
	}

}
