/**
 *
 */
package com.usatech.authoritylayer;

import simple.app.Publisher;
import simple.app.ServiceException;
import simple.io.ByteInput;
import simple.io.Log;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.constants.AuthorityAttrEnum;

/**
 * @author Brian S. Krug
 *
 */
public class PrepaidAuthorityTask extends InternalAuthorityTask {
	private static final Log log = Log.getLog();
	protected String checkReplenishPrepaidQueueKey;
	protected String consumerCommQueueKey;

	public PrepaidAuthorityTask() {
		super("prepaid");
	}

	protected void handleLowBalance(Publisher<ByteInput> publisher, long consumerAcctId) {
		log.info("Prepaid account balance is less than threshhold for " + consumerAcctId + ". Enqueuing replenish check");
		MessageChain mc = new MessageChainV11();
		MessageChainStep step = mc.addStep(getCheckReplenishPrepaidQueueKey());
		step.setAttribute(AuthorityAttrEnum.ATTR_CONSUMER_ACCT_ID, consumerAcctId);
		try {
			MessageChainService.publish(mc, publisher);
		} catch(ServiceException e) {
			log.warn("Could not publish message to check replenish on prepaid account " + consumerAcctId, e);
		}
	}

	protected boolean isDiscountInAuthResponse() {
		return true;
	}

	public String getCheckReplenishPrepaidQueueKey() {
		return checkReplenishPrepaidQueueKey;
	}

	public void setCheckReplenishPrepaidQueueKey(String checkReplenishPrepaidQueueKey) {
		this.checkReplenishPrepaidQueueKey = checkReplenishPrepaidQueueKey;
	}


	public String getConsumerCommQueueKey() {
		return consumerCommQueueKey;
	}

	public void setConsumerCommQueueKey(String notifyPrepaidQueueKey) {
		this.consumerCommQueueKey = notifyPrepaidQueueKey;
	}
}
