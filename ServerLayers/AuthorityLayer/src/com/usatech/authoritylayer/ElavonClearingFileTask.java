package com.usatech.authoritylayer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.CipherInputStream;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.io.CopyInputStream;
import simple.io.InputStreamByteInput;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.lang.EnumStringValueLookup;
import simple.lang.InvalidValueException;
import simple.results.DatasetHandler;
import simple.results.DatasetUtils;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;
import simple.text.ThreadSafeDateFormat;

import com.usatech.app.AbstractAttributeDatasetHandler;
import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.authoritylayer.ElavonClearingFileSite.FileSiteEntry;
import com.usatech.ftps.FtpsBean;
import com.usatech.iso8583.PanData;
import com.usatech.layers.common.Cryption;
import com.usatech.layers.common.DeleteResourceTask;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAction;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.DebugLevel;
import com.usatech.layers.common.constants.EntryCapability;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.MessageAttribute;
import com.usatech.layers.common.constants.PaymentMaskBRef;
import com.usatech.layers.common.constants.PosEnvironment;

import ftp.FtpException;
import ftp.FtpListResult;

public class ElavonClearingFileTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected static final String RECORD_TERMINATOR = "\r\n";
	protected static final char FIELD_SEPARATOR = 0x1C;
	protected static final Pattern nonAlpaNumPattern = Pattern.compile("[^0-9A-Za-z]+");
	protected static final Pattern RESPONSE_CODE_PATTERN = Pattern.compile("([^;]*)(?:;CVV:([^;]*))?(?:;AVS:([^;]*))?");
	protected DebugLevel debugLevel;
	protected Set<String> supportedCurrencies;
	protected ResourceFolder resourceFolder;
	protected final Set<String> ignoreAttributes = new HashSet<String>(Arrays.asList(new String[] {
			AuthorityAttrEnum.ATTR_CAPTURE_DETAILS_ENCRYPTION_KEY.getValue(), 
			AuthorityAttrEnum.ATTR_CAPTURE_DETAILS_CIPHER.getValue(),
			AuthorityAttrEnum.ATTR_CAPTURE_DETAILS_BLOCK_SIZE.getValue(),
			AuthorityAttrEnum.ATTR_CAPTURE_DETAILS_RESOURCE.getValue(),
	}));
	protected ElavonClearingFileSite fileSite;
	protected final ThreadSafeDateFormat standardDateFormat = new ThreadSafeDateFormat(new SimpleDateFormat("yyyyMMddHHmmss"));
	protected final ThreadSafeDateFormat batchDateFormat = new ThreadSafeDateFormat(new SimpleDateFormat("yyyyMMdd"));
    protected int maxRetryAllowed = 5;
    protected final Map<String,CardType> cardTypeMapping = new HashMap<String, CardType>();
    protected String merchantCity;
    protected String merchantState;
    protected String merchantZip;
	protected Pattern partialAuthReversalTerminalPattern = null;
	protected Pattern addressAddendumTerminalPattern = null;
	protected int specificationVersion = 0; // this means the old way of doing things
	protected final Set<String> validCountryCds = new HashSet<String>(Collections.singleton("US"));
	protected final Set<String> usTerritories = new HashSet<String>(Arrays.asList(new String[] { "AS", "FM", "GU", "PR", "PW", "MH", "MP", "VI" }));
	protected final Set<CardType> addressAddendumCardTypes = new HashSet<CardType>(Arrays.asList(CardType.values()));

    public static enum CardType {
    	AMERICAN_EXPRESS("AX", ' '),
    	DINERS_CLUB("DC", ' '),
    	DISCOVER_CARD("DI", ' '),
    	MASTERCARD("MC", '2'),
    	VISA("VI", '3'),
    	
    	DEBIT_CARD("DB", ' '),
    	ELECTRONIC_BENEFITS_TRANSFER("EB", ' '), 
    	ELECTRONIC_CHECK_SERVICE ("EC", ' '), 
    	JCB("JB", ' '),
    	PRIVATE_LABEL_1("P1", ' '), 
    	PRIVATE_LABEL_2("P2", ' '), 
    	PRIVATE_LABEL_3("P3", ' '),
    	;
        private final String value;
        private final char cardholderActivatedTerminal;
        
        protected final static EnumStringValueLookup<CardType> lookup = new EnumStringValueLookup<CardType>(CardType.class);
        private CardType(String value, char cardholderActivatedTerminal) {
			this.value = value;
			this.cardholderActivatedTerminal = cardholderActivatedTerminal;
		}
        public static CardType getByValue(String value) throws InvalidValueException {
        	return lookup.getByValue(value);
        }
		public String getValue() {
			return value;
		}
		public char getCardholderActivatedTerminal() {
			return cardholderActivatedTerminal;
		}
    }
    
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		int result = processInternal(taskInfo);
		deleteCaptureDetailsResource(taskInfo);
		return result;
	}
	
	protected int processInternal(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		Map<String, Object> attributes = taskInfo.getStep().getAttributes();
		ProcessingUtils.logAttributes(log, "Starting ", attributes, getPreActionLogAttributes(), null);
		AuthorityAction action;
		String currencyCd;
		try {
			action = step.getAttribute(AuthorityAttrEnum.ATTR_ACTION_TYPE, AuthorityAction.class, true);
			currencyCd = step.getAttribute(AuthorityAttrEnum.ATTR_CURRENCY_CD, String.class, true);
		} catch(AttributeConversionException e) {
			return 2; // FAILED
		}
		if(supportedCurrencies != null && !supportedCurrencies.contains(currencyCd)) {
			log.info("Currency '" + currencyCd + " is not supported by " + this);
			populateFailedResults(null, step.getResultAttributes(), "Currency not supported");
			return 2; // FAILED
		}
		switch(action) {
			case SETTLEMENT: case SETTLEMENT_RETRY:
				if(taskInfo.isRedelivered()) {
					Integer uniqueSeconds;
					try {
						uniqueSeconds = taskInfo.getStep().getAttribute(AuthorityAttrEnum.ATTR_UNIQUE_SECONDS, Integer.class, false);
					} catch(AttributeConversionException e) {
						uniqueSeconds = null;
					}
					if(uniqueSeconds == null || uniqueSeconds <= 0) {
						String text = "Reversal of Action '" + action + "' NOT implemented"; 
						log.warn(text);
						populateFailedResults(null, step.getResultAttributes(), text);
						return 2; // FAILED
					}
				}
				AuthResultCode arc = processSettlement(taskInfo, action == AuthorityAction.SETTLEMENT_RETRY);
				switch(arc) {
					case APPROVED:
						return 0;
					case PARTIAL:
						return 0;
					case DECLINED:
					case DECLINED_PERMANENT:
						return 1;
					case FAILED:
						return 2;
					default:
						return 1;
				}
			default:
				throw new RetrySpecifiedServiceException("Action type '" + action + "' NOT implemented", WorkRetryType.NO_RETRY);
		}
	}

	protected void deleteCaptureDetailsResource(MessageChainTaskInfo taskInfo) throws ServiceException {
		//attempt to delete resource
		String resourceKey, sensitiveResourceKey;
        try {
			resourceKey = taskInfo.getStep().getAttribute(AuthorityAttrEnum.ATTR_CAPTURE_DETAILS_RESOURCE, String.class, true);
			sensitiveResourceKey = taskInfo.getStep().getAttribute(CommonAttrEnum.ATTR_CRYPTO_RESOURCE, String.class, false);
		} catch(AttributeConversionException e) {
			log.warn("Could not get Capture Details attribute to delete resource", e);
			return;
        }	
        DeleteResourceTask.requestResourceDeletion(resourceKey, taskInfo.getPublisher());
		if(sensitiveResourceKey != null) {
			DeleteResourceTask.requestResourceDeletion(sensitiveResourceKey, taskInfo.getPublisher());
		}
	}

	protected AuthResultCode processSettlement(MessageChainTaskInfo taskInfo, boolean retry) throws ServiceException {
		AuthResultCode result;
		MessageChainStep step = taskInfo.getStep();
		ElavonClearingFileSite fileSite = getFileSite();
        if(fileSite == null)
        	return handleException(taskInfo, "Property 'fileSite' is not configured", null);
        String resourceKey;
        String sensitiveResourceKey;
        byte[] key;
        String cipherName;
        int blockSize;
        Integer uniqueSeconds;
		try {
			resourceKey = step.getAttribute(AuthorityAttrEnum.ATTR_CAPTURE_DETAILS_RESOURCE, String.class, true);
			key = step.getAttribute(AuthorityAttrEnum.ATTR_CAPTURE_DETAILS_ENCRYPTION_KEY, byte[].class, true);	
			cipherName = step.getAttribute(AuthorityAttrEnum.ATTR_CAPTURE_DETAILS_CIPHER, String.class, true);	
			blockSize = step.getAttribute(AuthorityAttrEnum.ATTR_CAPTURE_DETAILS_BLOCK_SIZE, Integer.class, true);
			sensitiveResourceKey = step.getAttribute(CommonAttrEnum.ATTR_CRYPTO_RESOURCE, String.class, false);
			uniqueSeconds = step.getAttribute(AuthorityAttrEnum.ATTR_UNIQUE_SECONDS, Integer.class, false);
		} catch(AttributeConversionException e) {
			log.warn("Could not get Capture Details attribute", e);
        	return populateFailedResults(null, step.getResultAttributes(), "Could not get Capture Details attribute: " + e.getMessage());
		}	
		FileSiteEntry fileSiteEntry;
		try {
			fileSiteEntry = fileSite.createFileSiteEntry(uniqueSeconds);
		} catch(IOException e) {
			return handleException(taskInfo, "Could not get file site entry", e);
		}
		// Make a client connection
    	FtpsBean ftpsBean;
    	try {
			ftpsBean = fileSite.connect();
		} catch (FtpException e) {
        	return handleException(taskInfo, "Could not write ECF file", e);
		} catch(IOException e) {
			return handleException(taskInfo, "Could not write ECF file", e);
		}
		try {
			log.info("Connected to '" + fileSite + "'");
			if(cleanupFiles(ftpsBean, fileSiteEntry)) {
				if(taskInfo.isRedelivered() || retry)
					log.info("File '" + fileSiteEntry.getFileName() + "' already exists on remote site");
				else { // This should not happen
					String text = "File '" + fileSiteEntry.getFileName() + "' already exists on remote site but this message was not redelivered"; 
					log.warn(text);
					populateFailedResults(null, step.getResultAttributes(), text);
					return AuthResultCode.FAILED;
				}
			} else {				
		        ResourceFolder rf = getResourceFolder();
				if(rf == null)
					throw new RetrySpecifiedServiceException("ResourceFolder is not set", WorkRetryType.NONBLOCKING_RETRY, true);
				Resource resource;
				try {
					resource = rf.getResource(resourceKey, ResourceMode.READ);
				} catch(IOException e) {
					return handleException(taskInfo, "Could not get Capture Details resource", e);
				}
				try {
		    		InputStream cis;
		            try {
		    			cis = Cryption.createDecryptingInputStream(Cryption.DEFAULT_PROVIDER, cipherName, key, blockSize, resource.getInputStream());
		    		} catch(GeneralSecurityException e) {
		    			return handleException(taskInfo, "Could not decrypt Capture Details", e);
		            } catch(IOException e) {
		            	return handleException(taskInfo, "Could not decrypt Capture Details", e);
		            } 
		            Resource sensitiveResource = null;
		        	CipherInputStream sensitiveCis = null;
		        	ByteArrayOutputStream baos = null;
		        	try {   
		            	if(log.isDebugEnabled()) {
		            		baos = new ByteArrayOutputStream();
							cis = new CopyInputStream(cis, baos);
						}			
		            	if(sensitiveResourceKey != null && sensitiveResourceKey.trim().length() > 0) {
			    			try {
				    			sensitiveResource = rf.getResource(sensitiveResourceKey, ResourceMode.READ);
				    		} catch(IOException e) {
				    			return handleException(taskInfo, "Could not get Sensitive Details resource", e);
				    		}
				    		try {
				            	sensitiveCis = Cryption.createDecryptingInputStream(Cryption.DEFAULT_PROVIDER, cipherName, key, blockSize, sensitiveResource.getInputStream());
				    		} catch(GeneralSecurityException e) {
				    			return handleException(taskInfo, "Could not decrypt Sensitive Details", e);
				            } catch(IOException e) {
				            	return handleException(taskInfo, "Could not decrypt Sensitive Details", e);
				            }
			    		}
						ftpsBean.enableCompression();
		            	boolean okay = false;
	            	    OutputStream out = ftpsBean.putFileStream(fileSiteEntry.getTmpFileName(), -1);
			            try {
			            	log.info("Writing file '" + fileSiteEntry.getTmpFileName() + "' to '" + fileSite + "'");
							AbstractAttributeDatasetHandler<IOException> captureFileGenerator;
							if(specificationVersion >= 358)
								captureFileGenerator = new CaptureFileGenerator358(fileSiteEntry.getFileName(), fileSiteEntry.getFileDate(), fileSiteEntry.getDestinationId(), step, new OutputStreamWriter(out));
							else
								captureFileGenerator = new CaptureFileGenerator(fileSiteEntry.getFileName(), fileSiteEntry.getFileDate(), fileSiteEntry.getDestinationId(), step, new OutputStreamWriter(out));

			            	if(sensitiveCis == null) 
								DatasetUtils.parseDataset(new InputStreamByteInput(cis), captureFileGenerator);
			            	else
								DatasetUtils.parseAndMergeDatasets(captureFileGenerator, new InputStreamByteInput(cis), new InputStreamByteInput(sensitiveCis));
			            	out.flush();
			            	okay = true;
			            } catch(IOException e) {
			            	return handleException(taskInfo, "Could not write ECF file", e);
			            } finally {
			            	try {
			            		out.close();
			            	} catch(IOException e) {
			            		if(okay)
			            			throw e;
								log.info("Could not close ECF file stream: " + e.getMessage());
			            	}
			            }
			            log.info("Renaming file '" + fileSiteEntry.getTmpFileName() + "' to '" + fileSiteEntry.getFileName() + "'");            	
			            ftpsBean.fileRename(fileSiteEntry.getTmpFileName(), fileSiteEntry.getFileName());
		        	} finally {
		    			if(log.isDebugEnabled() && baos != null) {
			            	log.debug("Read the following bytes from the Dataset File:\n" + StringUtils.toHex(baos.toByteArray()));
			            }
						try {
							cis.close();
						} catch(IOException e) {
							log.warn("Could not close input stream", e);
						}
						if(sensitiveCis != null)
							try {
								sensitiveCis.close();
							} catch(IOException e) {
								log.warn("Could not close input stream", e);
							}
						if(sensitiveResource != null) {
							sensitiveResource.release();
						}
					}
		    	} finally {
		    		resource.release();	
				}
			}
		} catch (FtpException e) {
        	return handleException(taskInfo, "Could not write ECF file", e);
		} catch(IOException e) {
			return handleException(taskInfo, "Could not write ECF file", e);
        } finally {
        	try {
        		ftpsBean.close();
        	} catch (NullPointerException e) {
				log.debug("FTP Connection was not open",e);
            } catch (FtpException e) {
				log.warn("Could not close FTP Connection",e);
            } catch(IOException ioe) {
                log.warn("Could not close FTP Connection", ioe);
            }
        }
		
        Map<String,Object> resultAttributes = step.getResultAttributes();       
        resultAttributes.put("authResultCd", 'P');
		resultAttributes.put("authorityRespCd", 108);
		resultAttributes.put("authorityRespDesc", fileSite + "/" + fileSiteEntry.getFileName());
		
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD.getValue(), fileSiteEntry.getFileName());
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS.getValue(), System.currentTimeMillis());
		
		result = AuthResultCode.PARTIAL;
		
		ProcessingUtils.logAttributes(log, "Finished, result=" + result.getDescription() + "; ", step.getAttributes(), getPostActionLogAttributes(), null);
		return result;
	}

	protected boolean cleanupFiles(FtpsBean ftpsBean, FileSiteEntry fileSiteEntry) throws IOException, FtpException {
		boolean fileExists = false;
		FtpListResult dirList = ftpsBean.getFileInfo(fileSiteEntry.getFileName());
		if(dirList.next() && dirList.getName().equals(fileSiteEntry.getFileName()) && dirList.getSize() > 0) {
    		fileExists = true;
        }
		dirList = ftpsBean.getFileInfo(fileSiteEntry.getTmpFileName());
        while(dirList.next()) {
        	String filename = dirList.getName();
        	if(filename.equals(fileSiteEntry.getTmpFileName())) {
        		try {
        			ftpsBean.fileDelete(dirList.getName());
        		} catch(FtpException e) {
                	log.warn("Could not delete temporary ECF file " + filename + "; continuing to process others", e);   	
                } catch(IOException e) {
                	log.warn("Could not delete temporary ECF file " + filename + "; continuing to process others", e);   	  	
                }
            }
        }
        return fileExists;
	}

	protected AuthResultCode handleException(MessageChainTaskInfo taskInfo, String message, Throwable exception) throws RetrySpecifiedServiceException {
		if(taskInfo.getRetryCount() >= getMaxRetryAllowed()) {
			log.warn(message, exception);
			return populateDeclinedResults(null, taskInfo.getStep().getResultAttributes(), message + (exception == null ? "" : ": " + exception.getMessage()));
		}
		throw new RetrySpecifiedServiceException(message, exception, WorkRetryType.NONBLOCKING_RETRY, true);
	}

	// NOTE: this sub-class is not thread safe (b/c of the data field)
	/**
	 * @author bkrug
	 *
	 */
	protected class CaptureFileGenerator extends AbstractAttributeDatasetHandler<IOException> {
		protected final String fileName;
		protected final int destinationId;
		protected final MessageChainStep step;
		protected final Date fileCreateDate;
		protected final Writer writer;
		protected int recordCount = 0;
		protected int transactionIndex = 0;
		protected BigInteger cancelledAmount;
		protected int partialReversalCount = 0;
				
		public CaptureFileGenerator(String fileName, Date fileCreateDate, int destinationId, MessageChainStep step, Writer writer) {
			this.fileName = fileName;
			this.destinationId = destinationId;
			this.step = step;
			this.fileCreateDate = fileCreateDate;
			this.writer = writer;
		}
				
		@Override
		public void handleRowEnd() throws IOException {
			try {
				writeDetailRecord();
			} catch(AttributeConversionException e) {
				StringBuilder sb = new StringBuilder();
				sb.append("Could not parse row #").append(recordCount + 1).append(" for ");
				Number saleAuthId;
				try {
					saleAuthId = getDetailAttribute(AuthorityAttrEnum.ATTR_SALE_AUTH_ID, Number.class, false);
				} catch(AttributeConversionException e1) {
					saleAuthId = null;
				}
				if(saleAuthId != null)
					sb.append("sale auth id ").append(saleAuthId);
				else {
					Number refundId;
					try {
						refundId = getDetailAttribute(AuthorityAttrEnum.ATTR_REFUND_ID, Number.class, false);
					} catch(AttributeConversionException e1) {
						refundId = null;
					}
					if(refundId != null)
						sb.append("refund id ").append(refundId);
					else
						sb.append("UNKNOWN");
				}
				throw new IOException(sb.toString(), e);
			}
			data.clear();
		}
		
		public void handleDatasetStart(String[] columnNames) throws IOException {
			writeFileHeader();
			try {
				writeBatchHeader();
			} catch(AttributeConversionException e) {
				throw new IOException(e);
			}
		}
		public void handleDatasetEnd() throws IOException {
			try {
				writeBatchTrailer();
				writeFileTrailer();
			} catch(AttributeConversionException e) {
				throw new IOException(e);
			}
			writer.flush();
		}
		
		protected String getMerchantId(MessageChainStep step) throws AttributeConversionException {
			String terminalCd = step.getAttribute(AuthorityAttrEnum.ATTR_TERMINAL_CD, String.class, true);
			return terminalCd.substring(3, terminalCd.length() - 3);		
		}

		protected boolean isPartialAuthReversalEnabled() throws AttributeConversionException {
			Pattern p = partialAuthReversalTerminalPattern;
			if(p == null)
				return false;
			String terminalCd = step.getAttribute(AuthorityAttrEnum.ATTR_TERMINAL_CD, String.class, true);
			return p.matcher(terminalCd).matches();
		}

		protected boolean isAddressAddendumEnabled() throws AttributeConversionException {
			Pattern p = addressAddendumTerminalPattern;
			if(p == null)
				return false;
			String terminalCd = step.getAttribute(AuthorityAttrEnum.ATTR_TERMINAL_CD, String.class, true);
			return p.matcher(terminalCd).matches();
		}
		
		/**
		 * Fields:
		 * Record Type 1-3 3 AN Constant DTR "DTR"
		 * Transaction Code 4-4 1 AN Transaction Type Identifier
		 * A = Authorization Only
		 * B = Cash Benefit Purchase (EBT)
		 * C = Cash Advance
		 * F = Food Stamp Purchase (EBT)
		 * P = MC Payment Transaction
		 * R = Return Voucher
		 * S = Sales Draft
		 * W = Food Stamp Return (EBT)
		 * X = Sales Draft Reversal
		 * Y = Cash Advance Reversal
		 * Z = Return Voucher Reversal "S" - Sales
		 * "X" - Reversals
		 * Card Type 5-6 2 AN Card Type Identifier
		 * AX = American Express
		 * DB = Debit Card
		 * DC = Diners Club / Carte Blanche
		 * DI = Discover
		 * EB = Electronic Benefits Transfer (EBT)
		 * EC = Electronic Check Service (ECS)
		 * JB = JCB
		 * VI = Visa
		 * MC = MasterCard
		 * P1 = Private Label 1
		 * P2 = Private Label 2
		 * P3 = Private Label 3 As per transaction
		 * Card Number 7-26 20 AN LJ - Trailing Spaces
		 * Expiration Date 27-30 4 N MMYY
		 * Settlement Amount 31-40 10 N Amount to be deposited in 9(8)v99 format
		 * Authorization Date 41-48 8 N YYYYMMDD
		 * Authorization Time 49-54 6 N HHMMSS
		 * Approval code 55-60 6 AN Left-justified with trailing spaces
		 * POS Entry Mode 61-62 2 AN Credit / Debit / EBT POS Entry Mode Identifier
		 * 00 = Terminal not used
		 * 01 = Key entered
		 * 02 = Mag stripe read - not full stripe
		 * 03 = Bar code read
		 * 04 = OCR read
		 * 06 = Track 1 read
		 * 81 = MasterCard Internet
		 * 90 = Complete mag stripe read and transmitted
		 * 91 = Proximity mag stripe read and transmitted
		 * 
		 * Electronic Check POS Entry Mode Identifier
		 * 01 = Key Entered MICR Data
		 * 02 = System Parsed MICR Data
		 * 84 = Electronic Check MICR Read
		 * 02 - Mag stripe read - not dull stripe
		 * 91 - Proximity mag read and transmitted
		 * Auth Source code 63-63 1 AN Code indicating authorization source
		 * Visa - Valid values are 0,1,2,3,4,5
		 * E = Force
		 * 9 = Return Value from pre-auth response
		 * Cardholder ID Method 64-64 1 AN Method of identification used for transaction
		 * Space = Not specified
		 * 1 = Signature
		 * 2 = PIN
		 * 3 = Unattended terminal
		 * 4 = Mail / telephone order "3"
		 * Card Present Indicator 65-65 1 AN Credit / Debit / EBT
		 * Y = Card present
		 * N = Card not present
		 * 
		 * Electronic Check
		 * C = Check Present, Consumer Present (POP)
		 * P = Check Present, Consumer Not Present (ARC)
		 * R = Check Not Present (PPD) "Y"
		 * Reference Number 66-76 11 N Transaction reference number From response message, Right justified, leading zeros filled.
		 * Visa ACI 77-77 1 AN Authorization Characteristics Indicator From Pre-Auth response
		 * Visa Transaction ID
		 * Or
		 * MasterCard Banknet
		 * Reference Number
		 * Or
		 * Amex Transaction ID 78-92 15 AN Visa
		 * The CPS Transaction ID returned on the original authorization response; numeric RJ, 15 digits
		 * MasterCard
		 * The Banknet Reference Number returned on the original authorization response; alphanumeric LJ, 9 characters with trailing spaces
		 * Amex
		 * The Amex Transaction Identifier returned on the original authorization response; numeric RJ, 15 digits From Pre-Auth response
		 * Visa Validation Code
		 * Or
		 * MasterCard Banknet
		 * Reference Date 93-96 4 AN Visa
		 * The CPS Validation Code returned on the original authorization response; alphanumeric LJ with trailing spaces
		 * MasterCard
		 * The Banknet Reference Date returned on the original authorization response; numeric, format is MMDD From Pre-Auth response
		 * Mail/Phone/Electronic
		 * Commerce Indicator 97-97 1 AN Visa / MasterCard
		 * Space = not applicable
		 * 1 = Single transaction
		 * 2 = Recurring transaction
		 * 3 = Installment payment
		 * 4 = Unknown classification
		 * 5 = Secure EC with CH certificate
		 * 6 = Non secure EC with merchant certificate
		 * 7 = Non secure EC without merch certificate
		 * 8 = Non secure transaction
		 * Electronic Check
		 * V = Verify
		 * E = Edit Only (Parse + Edit) space
		 * Cardholder Activated
		 * Terminal 98-98 1 AN Visa
		 * Space = not applicable
		 * 1 = Limited amount terminal
		 * 2 = Automated dispensing machine
		 * 3 = Self service terminal
		 * MasterCard
		 * 0 = Not a CAT transaction
		 * 1 = Automated dispensing machine
		 * 2 = Self service terminal
		 * 3 = Limited amount terminal
		 * 4 = In-flight commerce
		 * 6 = Internet "2" - Visa
		 * "1" - MasterCard
		 * POS Capability 99-99 1 AN Capability of the POS to electronically read cards
		 * Space = not specified
		 * 0 = Unknown
		 * 1 = No terminal
		 * 2 = Magnetic stripe reader
		 * 3 = Bar code reader
		 * 4 = OCR reader
		 * 8 = Proximity reader
		 * 9 = Key entry only, no electronic reading capability "2" - Mag Stripe Reader
		 * "8" - Proximity Reader
		 * 
		 * Response Code 100-101 2 AN 2 digit issuer response code from original authorization transaction. (00 = AA on NOVA Network) "00"
		 * Authorization Currency Type 102-104 3 AN 3 character currency type for this transaction, e.g. USD, CAD, etc. USD - US or CAD - Canada
		 * Original Authorization Amount
		 * Or
		 * Cashback Amount for Debit 105-112 8 N Original amount authorized for this transaction in 9(6)v99 format, or the amount of cashback for debit card transaction Original Auth Amount
		 * AVS Result 113-113 1 AN Address Verification Service Result Code Space filled
		 * Purchase Identifier Format
		 * Code 114-114 1 AN Type of Purchase Identifier used on the transaction
		 * Space = free text description or not used
		 * 0 = free text description or not used
		 * 1 = Order Number / Customer Code
		 * 3 = Rental Agreement Number
		 * 4 = Hotel Folio Number Space filled
		 * Debit Network ID 115-116 2 AN Network ID for Debit cards
		 * MA = MAC
		 * IL = Interlink
		 * ML = Magicline
		 * NY = NYCE
		 * SE = Honor
		 * ST = Star
		 * MS = Money Station
		 * MR = Maestro
		 * PS = Pulse
		 * CS = Cash Station
		 * TY = Tyme
		 * EX = Accel
		 * AO = Alaska Option
		 * EC = Electronic Check Space filled
		 * Debit Settlement Date 117-120 4 AN Switch Settlement date for Debit cards in MMDD format Space filled
		 * Merchant Category Code 121-124 4 N Transaction SIC code Space filled
		 * Item Number 125-128 4 AN Item Number Space filled
		 * MSDI Indicator 129-129 1 AN Space = not specified
		 * B = Bill Payment
		 * G = MasterCard Payment Gateway Payment Space filled
		 * UCAF Indicator 130-130 1 AN MasterCard Only
		 * 0 = UCAF data collection is not supported by merchant.
		 * 1 = UCAF data collection is supported by merchant, but not populated
		 * 2 = UCAF data collection is supported by merchant, and UCAF data must be present. "0" - MasterCard transactions,
		 * SPACE for all other card types.
		 * Debit Surcharge Amount 131-138 8 N Debit Surcharge amount for the transaction in 9(6)v99 format Zero filled
		 * Waybill Number 139-151 13 N Air Canada Cargo Waybill Number Zero filled
		 * Card Acceptor ID 152-166 15 AN Merchant ID Merchant number
		 * ECS Charge Type 167-170 4 AN Charge Type Indicator Space filled
		 * Convenience Fee Amount 171-178 8 AN The Convenience Fee amount for the transaction in 9(6)v99 format Zero filled
		 * 
		 * CVV2 / CVC2 / CID Result 179-179 1 AN The CVV2, CVC2, or CID Result Code. Space filled
		 * Account Level Processing 180-181 2 AN The ALP response code returned from authorization From Pre-Auth response
		 * Reserved 182-200 19 AN Reserved for future use Space filled
		 * 
		 * @throws IOException
		 * @throws AttributeConversionException
		 */
		protected void writeDetailRecord() throws IOException, AttributeConversionException {
			transactionIndex++;
			Number saleAuthId = getDetailAttribute(AuthorityAttrEnum.ATTR_SALE_AUTH_ID, Number.class, false);
			Number refundId = getDetailAttribute(AuthorityAttrEnum.ATTR_REFUND_ID, Number.class, false);
			BigInteger settleAmount = getDetailAttribute(AuthorityAttrEnum.ATTR_AMOUNT, BigInteger.class, true);
			BigInteger authedAmount = getDetailAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, BigInteger.class, true);
			BigInteger partialReversalAmount = null;
			char transactionCode;
			String referenceNumber;
			if(saleAuthId != null) {
				referenceNumber = saleAuthId.toString();
				switch(settleAmount.signum()) {
					case 1:
						transactionCode = 'S';
						if(isPartialAuthReversalEnabled() && settleAmount.compareTo(authedAmount) < 0) {
							partialReversalAmount = authedAmount.subtract(settleAmount);
							if(cancelledAmount == null)
								cancelledAmount = partialReversalAmount;
							else
								cancelledAmount = cancelledAmount.add(partialReversalAmount);
							partialReversalCount++;
						}
						break;
					case 0:
						transactionCode = 'X';
						settleAmount = authedAmount;
						if(cancelledAmount == null) 
							cancelledAmount = authedAmount;
						else
							cancelledAmount = cancelledAmount.add(authedAmount);
						break;
					case -1:
						throw new IOException("Settle Amount is negative for a sale");
					default:
						throw new IOException("Invalid signum() for Settle Amount: " + settleAmount.signum());
				}
				if(authedAmount.signum() < 0)
					throw new IOException("Authed Amount is negative for a sale");				
			} else {
				transactionCode = 'R';
				referenceNumber = refundId.toString();
				if(settleAmount.signum() <= 0)
					throw new IOException("Settle Amount is not positive for a refund");
				if(authedAmount.signum() <= 0)
					throw new IOException("Authed Amount is not positive for a refund");
			}
			String pan = getDetailAttribute(PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER.attribute, String.class, false);
			if(StringUtils.isBlank(pan)) {
				pan = step.getIndexedAttribute(PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER.attribute, transactionIndex, String.class, false);
				if(StringUtils.isBlank(pan))
					pan = getDetailIndexedAttribute(CommonAttrEnum.ATTR_DECRYPTED_DATA, 1, String.class, true);
			}
			String expDate = getDetailAttribute(PaymentMaskBRef.EXPIRATION_DATE.attribute, String.class, false);
			if(StringUtils.isBlank(expDate)) {
				expDate = step.getIndexedAttribute(PaymentMaskBRef.EXPIRATION_DATE.attribute, transactionIndex, String.class, false);
				if(StringUtils.isBlank(expDate))
					expDate = getDetailIndexedAttribute(CommonAttrEnum.ATTR_DECRYPTED_DATA, 2, String.class, true);
			}
			if(expDate.length() < 4)
				throw new AttributeConversionException(PaymentMaskBRef.EXPIRATION_DATE.attribute, new ConvertException("Expiration Date is not long enough. 4 characters required", String.class, expDate));
			CardType cardType = getCardType(getDetailAttribute(AuthorityAttrEnum.ATTR_CARD_TYPE, String.class, false));
			if(cardType == null) {
				PanData panData = new PanData(pan, expDate);
				switch(panData.getCardType()) {
					case PanData.AMERICAN_EXPRESS:
						cardType = CardType.AMERICAN_EXPRESS;
						break;
					case PanData.DISCOVER:
						cardType = CardType.DISCOVER_CARD;
						break;
					case PanData.MASTERCARD:
						cardType = CardType.MASTERCARD;
						break;
					case PanData.VISA:
						cardType = CardType.VISA;
						break;
					case PanData.UNKNOWN: default:
						cardType = CardType.PRIVATE_LABEL_1;
						break;	
				}
			}
			Date authTime = getDetailAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS, Date.class, false);
			if(authTime == null)
				authTime = getDetailAttribute(AuthorityAttrEnum.ATTR_AUTH_TIME, Date.class, true);
			EntryType entryType;
			try {
				entryType = EntryType.getByValue(getDetailAttribute(AuthorityAttrEnum.ATTR_ENTRY_TYPE, Integer.class, true));
			} catch(InvalidValueException e) {
				throw new AttributeConversionException(AuthorityAttrEnum.ATTR_ENTRY_TYPE, new ConvertException(EntryType.class, e.getValue()));
			}
			String cpsData;
			char authSourceCode;
			if(saleAuthId != null) {				
				String miscData = getDetailAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, String.class, false);
				if(miscData == null) {
					cpsData = StringUtils.pad(null, ' ', 23, Justification.LEFT);
					authSourceCode = 'E'; // force
				} else {
					Map<String, String> map = new HashMap<String, String>();
					try {
						StringUtils.intoMap(map, miscData, '=', FIELD_SEPARATOR, new char[0], false, false);
					} catch(ParseException e) {
						log.error("Could not parse misc data", e);
					}
					String elavonAuthData = StringUtils.pad(map.get("elavonAuthorizationData"), ' ', 26, Justification.LEFT);
					authSourceCode = elavonAuthData.charAt(8);
					cpsData = StringUtils.pad(map.get("CPSData"), ' ', 23, Justification.LEFT);
				}
			} else {
				cpsData = StringUtils.pad(null, ' ', 23, Justification.LEFT);
				authSourceCode = '9';
			}
			String currencyCd = step.getAttribute(AuthorityAttrEnum.ATTR_CURRENCY_CD, String.class, true);
			String authAuthorityTranCd = getDetailAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD, String.class, transactionCode == 'S');
			writeDetailRecord(transactionCode, referenceNumber, authedAmount, settleAmount, pan, expDate, cardType, authTime, entryType, cpsData, authSourceCode, currencyCd, authAuthorityTranCd);
			if(isAddressAddendumEnabled() && addressAddendumCardTypes.contains(cardType)) {
				writeMAARecord();
			}
			if(partialReversalAmount != null) {
				writeDetailRecord('X', referenceNumber, authedAmount, partialReversalAmount, pan, expDate, cardType, authTime, entryType, cpsData, authSourceCode, currencyCd, authAuthorityTranCd);
				if(isAddressAddendumEnabled() && addressAddendumCardTypes.contains(cardType)) {
					writeMAARecord();
				}
			}
		}

		protected void writeDetailRecord(char transactionCode, String referenceNumber, BigInteger authedAmount, BigInteger settleAmount, String pan, String expDate, CardType cardType, Date authTime, EntryType entryType, String cpsData, char authSourceCode, String currencyCd, String authAuthorityTranCd) throws IOException, AttributeConversionException {
			writer.append("DTR");
			writer.append(transactionCode);
			writer.append(cardType.getValue());
			StringUtils.appendPadded(writer, pan, ' ', 20, Justification.LEFT); // Card Number
			writer.append(expDate, expDate.length() - 2, expDate.length()); // MM of ExpDate
			writer.append(expDate, expDate.length() - 4, expDate.length() - 2); // YY of ExpDate
			StringUtils.appendPadded(writer, settleAmount.toString(), '0', 10, Justification.RIGHT);
			writer.append(standardDateFormat.format(authTime)); // Authorization Date/Time
			StringUtils.appendPadded(writer, authAuthorityTranCd, ' ', 6, Justification.LEFT);
			if (entryType == EntryType.BAR_CODE)
					writer.append("03");
			else if (entryType == EntryType.CONTACTLESS || entryType.getFallbackEntryType() == EntryType.CONTACTLESS)
					writer.append("91");
			else if (entryType == EntryType.MANUAL)
					writer.append("01");
			else if (entryType == EntryType.SWIPE)
					writer.append("90");
			else
					writer.append("00");
			writer.append(authSourceCode); // Authorization Source Code = 9th character of "Elavon Authorization Data" from auth response
			writer.append('3'); // Card holder Id method
			writer.append('Y'); // Card present indicator
			StringUtils.appendPadded(writer, referenceNumber, '0', 11, Justification.RIGHT); // Reference Number
			writer.append(cpsData, 0, 1);// Visa ACI = 1st character of "CPS Data" from auth response
			writer.append(cpsData, 1, 16); // Visa/Amex TranId or MC Banknet Ref Number
			writer.append(cpsData, 16, 20); // Visa Validation Code or MasterCard Banknet Reference Date
			writer.append(' '); // Mail/Phone/Electronic Commerce Indicator
			writer.append(cardType.getCardholderActivatedTerminal()); // Cardholder Activated Terminal
			 // POS Capability - NOTE: we are faking this for now b/c we don't really know if we have contactless capability or not on each device
			if (entryType == EntryType.BAR_CODE)
					writer.append('3');
			else if (entryType == EntryType.CONTACTLESS || entryType.getFallbackEntryType() == EntryType.CONTACTLESS)
					writer.append('8');
			else if (entryType == EntryType.MANUAL)
					writer.append('9');
			else if (entryType == EntryType.SWIPE)
					writer.append('2');
			else
					writer.append('0');
			writer.append("00");// Response Code
			StringUtils.appendPadded(writer, currencyCd, ' ', 3, Justification.LEFT);// Authorization Currency Type
			StringUtils.appendPadded(writer, authedAmount.toString(), '0', 8, Justification.RIGHT); // Original Authorization Amount
			StringUtils.appendPadded(writer, null, ' ', 17, Justification.LEFT); // AVS Result (1), Purchase Identifier Format Code (1), Debit Network ID (2), Debit Settlement Date (4), Merchant
																					// Category Code (4), Item Number (4), MSDI Indicator (1)
			writer.append(cardType == CardType.MASTERCARD ? '0' : ' '); // UCAF Indicator
			StringUtils.appendPadded(writer, null, '0', 8, Justification.RIGHT); // Debit Surcharge Amount
			StringUtils.appendPadded(writer, null, '0', 13, Justification.RIGHT); // Waybill Number
			StringUtils.appendPadded(writer, getMerchantId(step), ' ', 15, Justification.LEFT); // Card Acceptor ID
			StringUtils.appendPadded(writer, null, ' ', 4, Justification.LEFT); // ECS Charge Type
			StringUtils.appendPadded(writer, null, '0', 8, Justification.RIGHT); // Convenience Fee Amount
			writer.append(' '); // CVV2 / CVC2 / CID Result
			writer.append(cpsData, 20, 22); // Account Level Processing (from CPS)
			StringUtils.appendPadded(writer, null, ' ', 19, Justification.LEFT); // Reserved
			writer.append(RECORD_TERMINATOR);

			recordCount++;
		}

		protected void writeMAARecord() throws AttributeConversionException, IOException {
			String countryCd = getDetailAttribute(AuthorityAttrEnum.ATTR_COUNTRY_CD, String.class, false);
			if(!validCountryCds.contains(countryCd))
				return;
			String doingBusinessAs = getDetailAttribute(AuthorityAttrEnum.ATTR_DOING_BUSINESS_AS, String.class, false);
			if (doingBusinessAs == null)
				doingBusinessAs = step.getAttribute(AuthorityAttrEnum.ATTR_MERCHANT_NAME, String.class, true);
			String city = getDetailAttribute(AuthorityAttrEnum.ATTR_CITY, String.class, false);
			String stateCd = getDetailAttribute(AuthorityAttrEnum.ATTR_STATE_CD, String.class, false);
			String postal = getDetailAttribute(AuthorityAttrEnum.ATTR_POSTAL, String.class, false);
			String address = getDetailAttribute(AuthorityAttrEnum.ATTR_ADDRESS, String.class, false);
			if(!StringUtils.isBlank(city) && !StringUtils.isBlank(stateCd) && !StringUtils.isBlank(postal)) {
				if("US".equalsIgnoreCase(countryCd) && usTerritories.contains(stateCd)) {
					countryCd = stateCd;
					stateCd = null;
				}
				writeMAARecord(doingBusinessAs, city, stateCd, postal, countryCd, address);
			}
		}
		
		/**
		 * Fields:
		 * Record Type 1-3 3 AN Constant MAA "MAA"
		 * Merchant DBA Name 4-28 25 AN The Merchant's "Doing Business As" name New Value, Left-justified, trailing spaces
		 * Merchant City 29-41 13 AN The Merchant's City Name New Value, Left-justified, trailing spaces
		 * Merchant State 42-43 2 AN The Merchant's State New Value
		 * Merchant Postal Code 44-52 9 AN The Merchant's postal or zip code New Value, Left-justified, trailing spaces
		 * Merchant Country Code 53-55 3 AN The Merchant's Country Code USA or CAN
		 * Merchant Street Address 56-85 30 AN The Merchant's Street Address New Value, Left-justified, trailing spaces
		 * Reserved 86-200 115 AN Reserved for Future USe Space filled
		 * 
		 * @param doingBusinessAs
		 * @param city
		 * @param stateCd
		 * @param postal
		 * @param countryCd
		 * @param address
		 * @throws IOException
		 * @throws AttributeConversionException
		 */
		protected void writeMAARecord(String doingBusinessAs, String city, String stateCd, String postal, String countryCd, String address) throws IOException, AttributeConversionException {
			if(!StringUtils.isBlank(postal))
				postal = nonAlpaNumPattern.matcher(postal).replaceAll("");
			if(!StringUtils.isBlank(countryCd) && (countryCd = countryCd.trim()).length() == 2)
				try {
					countryCd = new Locale("", countryCd).getISO3Country();
				} catch(MissingResourceException e) {
					// ignore
				}

			writer.append("MAAUSA*");
			StringUtils.appendPadded(writer, doingBusinessAs, ' ', 21, Justification.LEFT);
			StringUtils.appendPadded(writer, city, ' ', 13, Justification.LEFT);
			StringUtils.appendPadded(writer, stateCd, ' ', 2, Justification.LEFT);
			StringUtils.appendPadded(writer, postal, ' ', 9, Justification.LEFT); // strip punctuation
			StringUtils.appendPadded(writer, countryCd, ' ', 3, Justification.LEFT); // Convert to 3-digit
			StringUtils.appendPadded(writer, address, ' ', 30, Justification.LEFT);
			StringUtils.appendPadded(writer, null, ' ', 115, Justification.LEFT);
			
			writer.append(RECORD_TERMINATOR);
			recordCount++;
		}
		
		/**
		 * Fields:
		 * Record Type 1-3 3 AN Constant BHR "BHR"
		 * Merchant ID 4-19 16 AN Left-justified with trailing spaces Elavon Merchant ID, Left-justified, trailing spaces
		 * Merchant DBA Name 20-44 25 AN The name the merchant is "Doing Business As" Same as what is listed at Elavon, left-justified, trailing spaces
		 * Merchant City 45-57 13 AN The city the merchant is doing business in Same as what is listed at Elavon, left justified, trailing spaces
		 * Merchant State 58-59 2 AN The 2 digit State code or Canadian Province Same as what is listed at Elavon
		 * Merchant Zip 60-68 9 AN Left-justified with trailing spaces Same as what is listed at Elavon
		 * Merchant Country 69-71 3 AN 3 character country code for this location
		 * For example :g. USA, CAN, etc. USA (or CAN for Canadian merchants)
		 * Reserved 72-75 4 AN Reserved for Future Use Space filled
		 * Settlement Date 76-83 8 N Date batch settled - YYYYMMDD Current date
		 * 
		 * Batch number 84-94 11 N Elavon 11 digit GBOK batch number, OR the batch number assigned by the user Meaningful batch number.
		 * Network Identifier 95-98 4 AN Network the batch was captured on.
		 * NOVA = NOVA Information Systems "NOVA"
		 * Reference Number 99-109 11 AN Beginning Transaction reference number User defined
		 * Client Group 110-113 4 AN Client Group assigned by MPS Space filled
		 * MPS Number 114-125 12 AN Left-justified with trailing spaces Space filled
		 * Batch Response Code 126-127 2 AN Left-justified with trailing spaces Space filled
		 * Batch Type 128-128 1 AN The ECS Batch Type.
		 * A = Account Receivable Conversion (ARC)
		 * P = Point-of-Purchase (POP) D = Pre-Authorized Payment and Deposit (PPD)
		 * S = Point-Of-Sale (POS) "S"
		 * Reserved 129-200 72 AN Reserved for future use Space filled
		 * 
		 * @throws IOException
		 * @throws AttributeConversionException
		 */
		protected void writeBatchHeader() throws IOException, AttributeConversionException {
			writer.append("BHR");
			StringUtils.appendPadded(writer, getMerchantId(step), ' ', 16, Justification.LEFT);
			StringUtils.appendPadded(writer, step.getAttribute(AuthorityAttrEnum.ATTR_MERCHANT_NAME, String.class, true), ' ', 25, Justification.LEFT);
			// We will try to send blanks for the merchant address - since we don't have that info in our database
			StringUtils.appendPadded(writer, getMerchantCity(), ' ', 13, Justification.LEFT); // City
			StringUtils.appendPadded(writer, getMerchantState(), ' ', 2, Justification.LEFT); // State
			StringUtils.appendPadded(writer, getMerchantZip(), ' ', 9, Justification.LEFT); // Zip
			String currencyCd = step.getAttribute(AuthorityAttrEnum.ATTR_CURRENCY_CD, String.class, true);
			String country3 = "";
			if(currencyCd.length() > 1)
				try {
					country3 = new Locale("", currencyCd.substring(0, 2)).getISO3Country();
				} catch(MissingResourceException e) {
					log.info("Could not find country for currency '" + currencyCd + "'");
				}
			StringUtils.appendPadded(writer, country3, ' ', 3, Justification.LEFT); // Country
			StringUtils.appendPadded(writer, null, ' ', 4, Justification.LEFT); // Reserved
			StringUtils.appendPadded(writer, batchDateFormat.format(fileCreateDate), ' ', 8, Justification.LEFT); // Settlement Date
			StringUtils.appendPadded(writer, step.getAttribute(AuthorityAttrEnum.ATTR_TERMINAL_BATCH_NUM, BigInteger.class, true).toString(), '0', 11, Justification.RIGHT); // Batch number
			writer.append("NOVA");
			StringUtils.appendPadded(writer, step.getAttribute(AuthorityAttrEnum.ATTR_TRACE_NUMBER, BigInteger.class, true).toString(), '0', 11, Justification.RIGHT); // Reference number
			StringUtils.appendPadded(writer, null, ' ', 4 + 12 + 2, Justification.LEFT); // Client Group, MPS Number, Batch Response Code
			writer.append('S'); // Batch Type
			StringUtils.appendPadded(writer, null, ' ', 72, Justification.LEFT); // Reserved
			writer.append(RECORD_TERMINATOR);
			recordCount++;
		}
		
		/**
		 * Fields:
		 * Record Type 1-3 3 AN Constant BTR "BTR"
		 * Batch Purchases Count 4-9 6 N The count of purchase transactions for the batch Count of payments in batch, pad with leading zeros.
		 * Batch Purchases Amount 10-20 11 N The total dollar amount of purchase transactions for the batch in 9(9)v99 format Sum of payments in batch, pad with leading zeros
		 * Batch Return Count 21-26 6 N The count of return transactions for the batch Count of returns in batch, pad with leading zeros
		 * Batch Return Amount 27-37 11 N The total dollar amount of return transactions for the batch in 9(9)v99 format Sum of returns in batch, pad with leading zeros
		 * Batch Total Count 38-43 6 N The total count of all transactions in the batch Count of all trnx in batch, pad with leading zeros.
		 * Batch Net Amount 44-54 11 N Purchase Amount minus Return Amount (no sign) 9(9)v99 Sum of sales minus returns in batch, pad with leading zeros.
		 * Batch Net Sign 55-55 1 AN The "+" or "-" character depending on sign of Batch Net Amount field "+" or "-"
		 * Batch Number 56-66 11 N NOVA 11 digit batch number OR the batch number assigned by the user Same as Batch Number assigned in the Batch Header Record.
		 * Reserved 67-200 134 AN Reserved for future use Space filled
		 * 
		 * @throws IOException
		 * @throws AttributeConversionException
		 */
		protected void writeBatchTrailer() throws IOException, AttributeConversionException {
			writer.append("BTR");
			BigInteger saleCount = step.getAttribute(AuthorityAttrEnum.ATTR_CREDIT_SALE_COUNT, BigInteger.class, true);
			BigInteger saleAmount = step.getAttribute(AuthorityAttrEnum.ATTR_CREDIT_SALE_AMOUNT, BigInteger.class, true);
			BigInteger refundCount = step.getAttribute(AuthorityAttrEnum.ATTR_CREDIT_REFUND_COUNT, BigInteger.class, true);
			BigInteger refundAmount = step.getAttribute(AuthorityAttrEnum.ATTR_CREDIT_REFUND_AMOUNT, BigInteger.class, true);
			if(cancelledAmount != null)
				saleAmount = saleAmount.add(cancelledAmount);
			// Is this correct?
			if(partialReversalCount > 0)
				saleCount = saleCount.add(BigInteger.valueOf(partialReversalCount));

			StringUtils.appendPadded(writer, saleCount.toString(), '0', 6, Justification.RIGHT);
			StringUtils.appendPadded(writer, saleAmount.toString(), '0', 11, Justification.RIGHT);
			StringUtils.appendPadded(writer, refundCount.toString(), '0', 6, Justification.RIGHT);
			StringUtils.appendPadded(writer, refundAmount.toString(), '0', 11, Justification.RIGHT);
			StringUtils.appendPadded(writer, saleCount.add(refundCount).toString(), '0', 6, Justification.RIGHT);
			StringUtils.appendPadded(writer, saleAmount.subtract(refundAmount).abs().toString(), '0', 11, Justification.RIGHT);
			if(saleAmount.compareTo(refundAmount) < 0)
				writer.append('-');
			else
				writer.append('+');
			StringUtils.appendPadded(writer, step.getAttribute(AuthorityAttrEnum.ATTR_TERMINAL_BATCH_NUM, BigInteger.class, true).toString(), '0', 11, Justification.RIGHT); // Batch number
			StringUtils.appendPadded(writer, null, ' ', 134, Justification.LEFT);
			writer.append(RECORD_TERMINATOR);
			recordCount++;
		}
		
		/**
		 * Fields:
		 * Record Type 1-3 3 AN Constant FHR "FHR"
		 * File Create Date 4-11 8 N YYYYMMDD
		 * File Create Time 12-17 6 N HHMMSS
		 * Version number 18-19 2 N Version of this specification - constant '35' "35"
		 * NOVA File Number 20-23 4 N Destination ID provided by Elavon "8009"
		 * Sending Institution Name 24-48 25 AN User assigned "USA Technologies" with added spaces to 25 positions
		 * Originating File Name 49-80 32 AN The name of original submitted file P8009YYYYMMDDHHMMSS.UAT (File extension will change going to production,)
		 * Reserved 81-200 120 AN Reserved for future use Space filled
		 * 
		 * @throws IOException
		 */
		protected void writeFileHeader() throws IOException {
			writer.append("FHR");
			writer.append(standardDateFormat.format(fileCreateDate));
			writer.append("35");
			StringUtils.appendPadded(writer, Integer.toString(destinationId), '0', 4, Justification.RIGHT);
			StringUtils.appendPadded(writer, "USA Technologies", ' ', 25, Justification.LEFT);
			StringUtils.appendPadded(writer, fileName, ' ', 32, Justification.LEFT);
			StringUtils.appendPadded(writer, null, ' ', 120, Justification.LEFT);
			writer.append(RECORD_TERMINATOR);
			recordCount++;
		}
		
		/**
		 * Fields:
		 * Record Type 1-3 3 AN Constant FTR "FTR"
		 * File Purchases Count 4-9 6 N The count of purchase transactions in the file Number of payments included in file, pad with leading zeros.
		 * File Purchases Amount 10-20 11 N The total dollar amount of the purchase transactions for the file in 9(9)v99 format Sum of payments included in file, pad with leading zeros.
		 * File Return Count 21-26 6 N The count of return transactions in the file Count of returns in batch, pad with leading zeros
		 * File Return Amount 27-37 11 N The total dollar amount of return transactions for the file in 9(9)v99 format Sum of returns in batch, pad with leading zeros
		 * File Total Count 38-43 6 N The total count of all transactions in the file Number of payments included in file, pad with leading zeros.
		 * File Net Amount 44-54 11 N Purchase Amount minus Return Amount (no sign) in 9(9)v99 format Sum of sales minus returns in batch, pad with leading zeros.
		 * File Net Sign 55-55 1 AN The '+' or '-' character depending on sign of File Net Amount field '+' or '-'
		 * File Record Count 56-61 6 N Number of physical records in the file Count of records in this file, including this trailer record, pad with leading zeros.
		 * File Create Date 62-69 8 N YYYYMMDD (same as file header)
		 * File Create Time 70-75 6 N HHMMSS (same as file header)
		 * Reserved 76-200 125 AN Reserved for future use Space filled
		 */
		protected void writeFileTrailer() throws IOException, AttributeConversionException {
			writer.append("FTR");		
			BigInteger saleCount = step.getAttribute(AuthorityAttrEnum.ATTR_CREDIT_SALE_COUNT, BigInteger.class, true);
			BigInteger saleAmount = step.getAttribute(AuthorityAttrEnum.ATTR_CREDIT_SALE_AMOUNT, BigInteger.class, true);
			BigInteger refundCount = step.getAttribute(AuthorityAttrEnum.ATTR_CREDIT_REFUND_COUNT, BigInteger.class, true);
			BigInteger refundAmount = step.getAttribute(AuthorityAttrEnum.ATTR_CREDIT_REFUND_AMOUNT, BigInteger.class, true);
			if(cancelledAmount != null)
				saleAmount = saleAmount.add(cancelledAmount);
			StringUtils.appendPadded(writer, saleCount.toString(), '0', 6, Justification.RIGHT);
			StringUtils.appendPadded(writer, saleAmount.toString(), '0', 11, Justification.RIGHT);
			StringUtils.appendPadded(writer, refundCount.toString(), '0', 6, Justification.RIGHT);
			StringUtils.appendPadded(writer, refundAmount.toString(), '0', 11, Justification.RIGHT);
			StringUtils.appendPadded(writer, saleCount.add(refundCount).toString(), '0', 6, Justification.RIGHT);
			StringUtils.appendPadded(writer, saleAmount.subtract(refundAmount).abs().toString(), '0', 11, Justification.RIGHT);
			if(saleAmount.compareTo(refundAmount) < 0)
				writer.append('-');
			else
				writer.append('+');
			StringUtils.appendPadded(writer, Integer.toString(++recordCount), '0', 6, Justification.RIGHT);
			writer.append(standardDateFormat.format(fileCreateDate));
			StringUtils.appendPadded(writer, null, ' ', 125, Justification.LEFT);
			writer.append(RECORD_TERMINATOR);
		}
	}
	
	// NOTE: this sub-class is not thread safe (b/c of the data field)
	/**
	 * @author bkrug
	 * 
	 */
	protected class CaptureFileGenerator358 extends AbstractAttributeDatasetHandler<IOException> {
		protected final String fileName;
		protected final int destinationId;
		protected final MessageChainStep step;
		protected final Date fileCreateDate;
		protected final Writer writer;
		protected int recordCount = 0;
		protected int transactionIndex = 0;
		protected BigInteger cancelledAmount;
		protected int partialReversalCount = 0;

		public CaptureFileGenerator358(String fileName, Date fileCreateDate, int destinationId, MessageChainStep step, Writer writer) {
			this.fileName = fileName;
			this.destinationId = destinationId;
			this.step = step;
			this.fileCreateDate = fileCreateDate;
			this.writer = writer;
		}

		@Override
		public void handleRowEnd() throws IOException {
			try {
				writeDetailRecord();
			} catch(AttributeConversionException e) {
				StringBuilder sb = new StringBuilder();
				sb.append("Could not parse row #").append(recordCount + 1).append(" for ");
				Number saleAuthId;
				try {
					saleAuthId = getDetailAttribute(AuthorityAttrEnum.ATTR_SALE_AUTH_ID, Number.class, false);
				} catch(AttributeConversionException e1) {
					saleAuthId = null;
				}
				if(saleAuthId != null)
					sb.append("sale auth id ").append(saleAuthId);
				else {
					Number refundId;
					try {
						refundId = getDetailAttribute(AuthorityAttrEnum.ATTR_REFUND_ID, Number.class, false);
					} catch(AttributeConversionException e1) {
						refundId = null;
					}
					if(refundId != null)
						sb.append("refund id ").append(refundId);
					else
						sb.append("UNKNOWN");
				}
				throw new IOException(sb.toString(), e);
			}
			data.clear();
		}

		public void handleDatasetStart(String[] columnNames) throws IOException {
			writeFileHeader();
			try {
				writeBatchHeader();
			} catch(AttributeConversionException e) {
				throw new IOException(e);
			}
		}

		public void handleDatasetEnd() throws IOException {
			try {
				writeBatchTrailer();
				writeFileTrailer();
			} catch(AttributeConversionException e) {
				throw new IOException(e);
			}
			writer.flush();
		}

		protected String getMerchantId(MessageChainStep step) throws AttributeConversionException {
			String terminalCd = step.getAttribute(AuthorityAttrEnum.ATTR_TERMINAL_CD, String.class, true);
			return terminalCd.substring(3, terminalCd.length() - 3);
		}

		protected boolean isPartialAuthReversalEnabled() throws AttributeConversionException {
			Pattern p = partialAuthReversalTerminalPattern;
			if(p == null)
				return false;
			String terminalCd = step.getAttribute(AuthorityAttrEnum.ATTR_TERMINAL_CD, String.class, true);
			return p.matcher(terminalCd).matches();
		}

		protected boolean isAddressAddendumEnabled() throws AttributeConversionException {
			Pattern p = addressAddendumTerminalPattern;
			if(p == null)
				return false;
			String terminalCd = step.getAttribute(AuthorityAttrEnum.ATTR_TERMINAL_CD, String.class, true);
			return p.matcher(terminalCd).matches();
		}

		/**
		 * Fields:
		 * Record Type 1-3 3 AN Constant DTR "DTR"
		 * Transaction Code 4-4 1 AN Transaction Type Identifier
		 * A = Authorization Only
		 * B = Cash Benefit Purchase (EBT)
		 * C = Cash Advance
		 * F = Food Stamp Purchase (EBT)
		 * P = MC Payment Transaction
		 * R = Return Voucher
		 * S = Sales Draft
		 * W = Food Stamp Return (EBT)
		 * X = Sales Draft Reversal
		 * Y = Cash Advance Reversal
		 * Z = Return Voucher Reversal "S" - Sales
		 * "X" - Reversals
		 * Card Type 5-6 2 AN Card Type Identifier
		 * AX = American Express
		 * DB = Debit Card
		 * DC = Diners Club / Carte Blanche
		 * DI = Discover
		 * EB = Electronic Benefits Transfer (EBT)
		 * EC = Electronic Check Service (ECS)
		 * JB = JCB
		 * VI = Visa
		 * MC = MasterCard
		 * P1 = Private Label 1
		 * P2 = Private Label 2
		 * P3 = Private Label 3 As per transaction
		 * Card Number 7-26 20 AN LJ - Trailing Spaces
		 * Expiration Date 27-30 4 N MMYY
		 * Settlement Amount 31-40 10 N Amount to be deposited in 9(8)v99 format
		 * Authorization Date 41-48 8 N YYYYMMDD
		 * Authorization Time 49-54 6 N HHMMSS
		 * Approval code 55-60 6 AN Left-justified with trailing spaces
		 * POS Entry Mode 61-62 2 AN Credit / Debit / EBT POS Entry Mode Identifier
		 * 00 = Terminal not used
		 * 01 = Key entered
		 * 02 = Mag stripe read - not full stripe
		 * 03 = Bar code read
		 * 04 = OCR read
		 * 06 = Track 1 read
		 * 81 = MasterCard Internet
		 * 90 = Complete mag stripe read and transmitted
		 * 91 = Proximity mag stripe read and transmitted
		 * 
		 * Electronic Check POS Entry Mode Identifier
		 * 01 = Key Entered MICR Data
		 * 02 = System Parsed MICR Data
		 * 84 = Electronic Check MICR Read
		 * 02 - Mag stripe read - not dull stripe
		 * 91 - Proximity mag read and transmitted
		 * Auth Source code 63-63 1 AN Code indicating authorization source
		 * Visa - Valid values are 0,1,2,3,4,5
		 * E = Force
		 * 9 = Return Value from pre-auth response
		 * Cardholder ID Method 64-64 1 AN Method of identification used for transaction
		 * Space = Not specified
		 * 1 = Signature
		 * 2 = PIN
		 * 3 = Unattended terminal
		 * 4 = Mail / telephone order "3"
		 * Card Present Indicator 65-65 1 AN Credit / Debit / EBT
		 * Y = Card present
		 * N = Card not present
		 * 
		 * Electronic Check
		 * C = Check Present, Consumer Present (POP)
		 * P = Check Present, Consumer Not Present (ARC)
		 * R = Check Not Present (PPD) "Y"
		 * Reference Number 66-76 11 N Transaction reference number From response message, Right justified, leading zeros filled.
		 * Visa ACI 77-77 1 AN Authorization Characteristics Indicator From Pre-Auth response
		 * Visa Transaction ID
		 * Or
		 * MasterCard Banknet
		 * Reference Number
		 * Or
		 * Amex Transaction ID 78-92 15 AN Visa
		 * The CPS Transaction ID returned on the original authorization response; numeric RJ, 15 digits
		 * MasterCard
		 * The Banknet Reference Number returned on the original authorization response; alphanumeric LJ, 9 characters with trailing spaces
		 * Amex
		 * The Amex Transaction Identifier returned on the original authorization response; numeric RJ, 15 digits From Pre-Auth response
		 * Visa Validation Code
		 * Or
		 * MasterCard Banknet
		 * Reference Date 93-96 4 AN Visa
		 * The CPS Validation Code returned on the original authorization response; alphanumeric LJ with trailing spaces
		 * MasterCard
		 * The Banknet Reference Date returned on the original authorization response; numeric, format is MMDD From Pre-Auth response
		 * Mail/Phone/Electronic
		 * Commerce Indicator 97-97 1 AN Visa / MasterCard
		 * Space = not applicable
		 * 1 = Single transaction
		 * 2 = Recurring transaction
		 * 3 = Installment payment
		 * 4 = Unknown classification
		 * 5 = Secure EC with CH certificate
		 * 6 = Non secure EC with merchant certificate
		 * 7 = Non secure EC without merch certificate
		 * 8 = Non secure transaction
		 * Electronic Check
		 * V = Verify
		 * E = Edit Only (Parse + Edit) space
		 * Cardholder Activated
		 * Terminal 98-98 1 AN Visa
		 * Space = not applicable
		 * 1 = Limited amount terminal
		 * 2 = Automated dispensing machine
		 * 3 = Self service terminal
		 * MasterCard
		 * 0 = Not a CAT transaction
		 * 1 = Automated dispensing machine
		 * 2 = Self service terminal
		 * 3 = Limited amount terminal
		 * 4 = In-flight commerce
		 * 6 = Internet "2" - Visa
		 * "1" - MasterCard
		 * POS Capability 99-99 1 AN Capability of the POS to electronically read cards
		 * Space = not specified
		 * 0 = Unknown
		 * 1 = No terminal
		 * 2 = Magnetic stripe reader
		 * 3 = Bar code reader
		 * 4 = OCR reader
		 * 8 = Proximity reader
		 * 9 = Key entry only, no electronic reading capability "2" - Mag Stripe Reader
		 * "8" - Proximity Reader
		 * 
		 * Response Code 100-101 2 AN 2 digit issuer response code from original authorization transaction. (00 = AA on NOVA Network) "00"
		 * Authorization Currency Type 102-104 3 AN 3 character currency type for this transaction, e.g. USD, CAD, etc. USD - US or CAD - Canada
		 * Original Authorization Amount
		 * Or
		 * Cashback Amount for Debit 105-112 8 N Original amount authorized for this transaction in 9(6)v99 format, or the amount of cashback for debit card transaction Original Auth Amount
		 * AVS Result 113-113 1 AN Address Verification Service Result Code Space filled
		 * Purchase Identifier Format
		 * Code 114-114 1 AN Type of Purchase Identifier used on the transaction
		 * Space = free text description or not used
		 * 0 = free text description or not used
		 * 1 = Order Number / Customer Code
		 * 3 = Rental Agreement Number
		 * 4 = Hotel Folio Number Space filled
		 * Debit Network ID 115-116 2 AN Network ID for Debit cards
		 * MA = MAC
		 * IL = Interlink
		 * ML = Magicline
		 * NY = NYCE
		 * SE = Honor
		 * ST = Star
		 * MS = Money Station
		 * MR = Maestro
		 * PS = Pulse
		 * CS = Cash Station
		 * TY = Tyme
		 * EX = Accel
		 * AO = Alaska Option
		 * EC = Electronic Check Space filled
		 * Debit Settlement Date 117-120 4 AN Switch Settlement date for Debit cards in MMDD format Space filled
		 * Merchant Category Code 121-124 4 N Transaction SIC code Space filled
		 * Item Number 125-128 4 AN Item Number Space filled
		 * MSDI Indicator 129-129 1 AN Space = not specified
		 * B = Bill Payment
		 * G = MasterCard Payment Gateway Payment Space filled
		 * UCAF Indicator 130-130 1 AN MasterCard Only
		 * 0 = UCAF data collection is not supported by merchant.
		 * 1 = UCAF data collection is supported by merchant, but not populated
		 * 2 = UCAF data collection is supported by merchant, and UCAF data must be present. "0" - MasterCard transactions,
		 * SPACE for all other card types.
		 * Debit Surcharge Amount 131-138 8 N Debit Surcharge amount for the transaction in 9(6)v99 format Zero filled
		 * Waybill Number 139-151 13 N Air Canada Cargo Waybill Number Zero filled
		 * Card Acceptor ID 152-166 15 AN Merchant ID Merchant number
		 * ECS Charge Type 167-170 4 AN Charge Type Indicator Space filled
		 * Convenience Fee Amount 171-178 8 AN The Convenience Fee amount for the transaction in 9(6)v99 format Zero filled
		 * 
		 * CVV2 / CVC2 / CID Result 179-179 1 AN The CVV2, CVC2, or CID Result Code. Space filled
		 * Account Level Processing 180-181 2 AN The ALP response code returned from authorization From Pre-Auth response
		 * Reserved 182-200 19 AN Reserved for future use Space filled
		 * 
		 * @throws IOException
		 * @throws AttributeConversionException
		 */
		protected void writeDetailRecord() throws IOException, AttributeConversionException {
			transactionIndex++;
			Number saleAuthId = getDetailAttribute(AuthorityAttrEnum.ATTR_SALE_AUTH_ID, Number.class, false);
			Number refundId = getDetailAttribute(AuthorityAttrEnum.ATTR_REFUND_ID, Number.class, false);
			BigInteger settleAmount = getDetailAttribute(AuthorityAttrEnum.ATTR_AMOUNT, BigInteger.class, true);
			BigInteger authedAmount = getDetailAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, BigInteger.class, true);
			BigInteger partialReversalAmount = null;
			char transactionCode;
			String referenceNumber;
			if(saleAuthId != null) {
				referenceNumber = saleAuthId.toString();
				switch(settleAmount.signum()) {
					case 1:
						transactionCode = 'S';
						if(isPartialAuthReversalEnabled() && settleAmount.compareTo(authedAmount) < 0) {
							partialReversalAmount = authedAmount.subtract(settleAmount);
							if(cancelledAmount == null)
								cancelledAmount = partialReversalAmount;
							else
								cancelledAmount = cancelledAmount.add(partialReversalAmount);
							partialReversalCount++;
						}
						break;
					case 0:
						transactionCode = 'X';
						settleAmount = authedAmount;
						if(cancelledAmount == null)
							cancelledAmount = authedAmount;
						else
							cancelledAmount = cancelledAmount.add(authedAmount);
						break;
					case -1:
						throw new IOException("Settle Amount is negative for a sale");
					default:
						throw new IOException("Invalid signum() for Settle Amount: " + settleAmount.signum());
				}
				if(authedAmount.signum() < 0)
					throw new IOException("Authed Amount is negative for a sale");
			} else {
				transactionCode = 'R';
				referenceNumber = refundId.toString();
				if(settleAmount.signum() <= 0)
					throw new IOException("Settle Amount is not positive for a refund");
				if(authedAmount.signum() <= 0)
					throw new IOException("Authed Amount is not positive for a refund");
			}
			String pan = getDetailAttribute(PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER.attribute, String.class, false);
			if(StringUtils.isBlank(pan)) {
				pan = step.getIndexedAttribute(PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER.attribute, transactionIndex, String.class, false);
				if(StringUtils.isBlank(pan))
					pan = getDetailIndexedAttribute(CommonAttrEnum.ATTR_DECRYPTED_DATA, 1, String.class, true);
			}
			String expDate = getDetailAttribute(PaymentMaskBRef.EXPIRATION_DATE.attribute, String.class, false);
			if(StringUtils.isBlank(expDate)) {
				expDate = step.getIndexedAttribute(PaymentMaskBRef.EXPIRATION_DATE.attribute, transactionIndex, String.class, false);
				if(StringUtils.isBlank(expDate))
					expDate = getDetailIndexedAttribute(CommonAttrEnum.ATTR_DECRYPTED_DATA, 2, String.class, true);
			}
			if(expDate.length() < 4)
				throw new AttributeConversionException(PaymentMaskBRef.EXPIRATION_DATE.attribute, new ConvertException("Expiration Date is not long enough. 4 characters required", String.class, expDate));
			CardType cardType = getCardType(getDetailAttribute(AuthorityAttrEnum.ATTR_CARD_TYPE, String.class, false));
			if(cardType == null) {
				PanData panData = new PanData(pan, expDate);
				switch(panData.getCardType()) {
					case PanData.AMERICAN_EXPRESS:
						cardType = CardType.AMERICAN_EXPRESS;
						break;
					case PanData.DISCOVER:
						cardType = CardType.DISCOVER_CARD;
						break;
					case PanData.MASTERCARD:
						cardType = CardType.MASTERCARD;
						break;
					case PanData.VISA:
						cardType = CardType.VISA;
						break;
					case PanData.UNKNOWN:
					default:
						cardType = CardType.PRIVATE_LABEL_1;
						break;
				}
			}
			Date authTime = getDetailAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS, Date.class, false);
			if(authTime == null)
				authTime = getDetailAttribute(AuthorityAttrEnum.ATTR_AUTH_TIME, Date.class, true);
			EntryType entryType;
			try {
				entryType = EntryType.getByValue(getDetailAttribute(AuthorityAttrEnum.ATTR_ENTRY_TYPE, Integer.class, true));
			} catch(InvalidValueException e) {
				throw new AttributeConversionException(AuthorityAttrEnum.ATTR_ENTRY_TYPE, new ConvertException(EntryType.class, e.getValue()));
			}
			String cpsData;
			char authSourceCode;
			if(saleAuthId != null) {
				String miscData = getDetailAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, String.class, false);
				if(miscData == null) {
					cpsData = StringUtils.pad(null, ' ', 23, Justification.LEFT);
					authSourceCode = 'E'; // force
				} else {
					Map<String, String> map = new HashMap<String, String>();
					try {
						StringUtils.intoMap(map, miscData, '=', FIELD_SEPARATOR, new char[0], false, false);
					} catch(ParseException e) {
						log.error("Could not parse misc data", e);
					}
					String elavonAuthData = StringUtils.pad(map.get("elavonAuthorizationData"), ' ', 26, Justification.LEFT);
					authSourceCode = elavonAuthData.charAt(8);
					cpsData = StringUtils.pad(map.get("CPSData"), ' ', 23, Justification.LEFT);
				}
			} else {
				cpsData = StringUtils.pad(null, ' ', 23, Justification.LEFT);
				authSourceCode = '9';
			}
			String currencyCd = step.getAttribute(AuthorityAttrEnum.ATTR_CURRENCY_CD, String.class, true);
			String authAuthorityTranCd = getDetailAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD, String.class, transactionCode == 'S');
			PosEnvironment posEnv = getDetailAttributeDefault(AuthorityAttrEnum.ATTR_POS_ENVIRONMENT, PosEnvironment.class, PosEnvironment.UNSPECIFIED);
			String entryCapability = getDetailAttribute(AuthorityAttrEnum.ATTR_ENTRY_CAPABILITY, String.class, false);
			String authResponseCode = getDetailAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, String.class, false);
			String invoiceNumber = getDetailAttribute(AuthorityAttrEnum.ATTR_INVOICE_NUMBER, String.class, false);
			String csPhone = getDetailAttribute(AuthorityAttrEnum.ATTR_CUSTOMER_SERVICE_PHONE, String.class, false);
			Long saleUploadTime = getDetailAttribute(AuthorityAttrEnum.ATTR_SALE_UPLOAD_TIME, Long.class, false);
			if(saleUploadTime == null)
				saleUploadTime = getDetailAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS, Long.class, true);
			String responseCode;
			String avsResult;
			String cvvResult;
			Matcher matcher;
			if(authResponseCode == null || !(matcher = RESPONSE_CODE_PATTERN.matcher(authResponseCode)).matches()) {
				responseCode = "00";
				avsResult = " ";
				cvvResult = " ";
			} else {
				responseCode = matcher.group(1);
				cvvResult = matcher.group(2);
				avsResult = matcher.group(3);
			}
			writeDetailRecord(transactionCode, referenceNumber, authedAmount, settleAmount, pan, expDate, cardType, authTime, entryType, cpsData, authSourceCode, currencyCd, authAuthorityTranCd, posEnv, entryCapability, responseCode, avsResult, cvvResult);
			if(isAddressAddendumEnabled() && addressAddendumCardTypes.contains(cardType)) {
				writeMAARecord();
			}
			switch(posEnv) {
				case ECOMMERCE:
				case MOTO:
				case MOBILE:
					if(StringUtils.isBlank(invoiceNumber))
						invoiceNumber = '#' + referenceNumber;
					writeDMARecord(invoiceNumber, avsResult, authedAmount, csPhone, saleUploadTime);
			}
			if(partialReversalAmount != null) {
				writeDetailRecord('X', referenceNumber, authedAmount, partialReversalAmount, pan, expDate, cardType, authTime, entryType, cpsData, authSourceCode, currencyCd, authAuthorityTranCd, posEnv, entryCapability, responseCode, avsResult, cvvResult);
				if(isAddressAddendumEnabled() && addressAddendumCardTypes.contains(cardType)) {
					writeMAARecord();
				}
			}
		}

		protected void writeDetailRecord(char transactionCode, String referenceNumber, BigInteger authedAmount, BigInteger settleAmount, String pan, String expDate, CardType cardType, Date authTime, EntryType entryType, String cpsData, char authSourceCode, String currencyCd, String authAuthorityTranCd, PosEnvironment posEnv, String entryCapability, String responseCode, String avsResult,
				String cvvResult) throws IOException, AttributeConversionException {
			writer.append("DTR");
			writer.append(transactionCode);
			writer.append(cardType.getValue());
			StringUtils.appendPadded(writer, pan, ' ', 20, Justification.LEFT); // Card Number
			writer.append(expDate, expDate.length() - 2, expDate.length()); // MM of ExpDate
			writer.append(expDate, expDate.length() - 4, expDate.length() - 2); // YY of ExpDate
			StringUtils.appendPadded(writer, settleAmount.toString(), '0', 10, Justification.RIGHT);
			writer.append(standardDateFormat.format(authTime)); // Authorization Date/Time
			StringUtils.appendPadded(writer, authAuthorityTranCd, ' ', 6, Justification.LEFT);

			switch(posEnv) {
				case ECOMMERCE:
					switch(cardType) {
						case MASTERCARD:
							writer.append("81");// POS Entry Mode
							break;
						default:
							writer.append("01");// POS Entry Mode
							break;
					}
					break;
				case MOTO:
					switch(entryType) {
						case MANUAL:
							writer.append("01");// POS Entry Mode
							break;
						case UNSPECIFIED:
						default:
							writer.append("00");// POS Entry Mode
							break;
					}
					break;
				default:
					if (entryType == EntryType.BAR_CODE)
							writer.append("03");// POS Entry Mode
					else if (entryType == EntryType.CONTACTLESS || entryType.getFallbackEntryType() == EntryType.CONTACTLESS)
							writer.append("91");// POS Entry Mode
					else if (entryType == EntryType.MANUAL)
							writer.append("01");// POS Entry Mode
					else if (entryType == EntryType.SWIPE)
							writer.append("90");// POS Entry Mode
					else
							writer.append("00");// POS Entry Mode
			}

			writer.append(authSourceCode); // Authorization Source Code = 9th character of "Elavon Authorization Data" from auth response

			switch(posEnv) {
				case ECOMMERCE:
					writer.append('4'); // Card holder Id method
					writer.append('N'); // Card present indicator
					break;
				case MOTO:
					writer.append('4'); // Card holder Id method
					writer.append('N'); // Card present indicator
					break;
				case ATTENDED: case MOBILE:
					writer.append('1'); // Card holder Id method
					writer.append('Y'); // Card present indicator
					break;
				default:
					writer.append('3'); // Card holder Id method
					writer.append('Y'); // Card present indicator
					
			}
			StringUtils.appendPadded(writer, referenceNumber, '0', 11, Justification.RIGHT); // Reference Number
			writer.append(cpsData, 0, 1);// Visa ACI = 1st character of "CPS Data" from auth response
			writer.append(cpsData, 1, 16); // Visa/Amex TranId or MC Banknet Ref Number
			writer.append(cpsData, 16, 20); // Visa Validation Code or MasterCard Banknet Reference Date

			switch(posEnv) {
				case ECOMMERCE:
					writer.append('7'); // Mail/Phone/Electronic Commerce Indicator
					break;
				case MOTO:
					writer.append('1'); // Mail/Phone/Electronic Commerce Indicator
					break;
				default:
					writer.append(' '); // Mail/Phone/Electronic Commerce Indicator
			}

			switch(posEnv) {
				case ATTENDED:
				case MOBILE:
				case MOTO:
					switch(cardType) {
						case MASTERCARD:
							writer.append('0'); // Cardholder Activated Terminal
							break;
						default:
							writer.append(' '); // Cardholder Activated Terminal
					}
					break;
				case ECOMMERCE:
					switch(cardType) {
						case MASTERCARD:
							writer.append('6'); // Cardholder Activated Terminal
							break;
						default:
							writer.append(' '); // Cardholder Activated Terminal
					}
					break;
				default:
					writer.append(cardType.getCardholderActivatedTerminal()); // Cardholder Activated Terminal
			}

			switch(posEnv) {
				case ECOMMERCE:
					writer.append(' '); // POS Capability
					break;
				default:
					if(EntryCapability.isCapable(entryCapability, EntryCapability.PROXIMITY))
						writer.append('8'); // POS Capability
					else if(EntryCapability.isCapable(entryCapability, EntryCapability.SWIPE))
						writer.append('2'); // POS Capability
					else if(EntryCapability.isCapable(entryCapability, EntryCapability.KEYED))
						writer.append('9'); // POS Capability
					else if(StringUtils.isBlank(entryCapability))
						writer.append('1'); // POS Capability
					else
						writer.append('0'); // POS Capability
			}

			StringUtils.appendPadded(writer, responseCode, '0', 2, Justification.RIGHT);// Response Code

			StringUtils.appendPadded(writer, currencyCd, ' ', 3, Justification.LEFT);// Authorization Currency Type
			StringUtils.appendPadded(writer, authedAmount.toString(), '0', 8, Justification.RIGHT); // Original Authorization Amount (approved amount)
			StringUtils.appendPadded(writer, avsResult, ' ', 1, Justification.LEFT); // AVS Result
			switch(posEnv) {
				case ECOMMERCE:
				case MOTO:
				case MOBILE:
					writer.append('1'); // Purchase Identifier Format Code
					break;
				default:
					writer.append(' '); // Purchase Identifier Format Code
			}

			StringUtils.appendPadded(writer, null, ' ', 15, Justification.LEFT); // Debit Network ID (2), Debit Settlement Date (4), Merchant
																					// Category Code (4), Item Number (4), MSDI Indicator (1)
			writer.append(cardType == CardType.MASTERCARD ? '0' : ' '); // UCAF Indicator
			StringUtils.appendPadded(writer, null, '0', 8, Justification.RIGHT); // Debit Surcharge Amount
			StringUtils.appendPadded(writer, null, '0', 13, Justification.RIGHT); // Waybill Number
			StringUtils.appendPadded(writer, step.getAttribute(AuthorityAttrEnum.ATTR_TERMINAL_CD, String.class, true), ' ', 15, Justification.RIGHT); // Card Acceptor ID
			StringUtils.appendPadded(writer, null, ' ', 4, Justification.LEFT); // ECS Charge Type
			StringUtils.appendPadded(writer, null, '0', 8, Justification.RIGHT); // Convenience Fee Amount
			StringUtils.appendPadded(writer, cvvResult, ' ', 1, Justification.LEFT); // CVV2 / CVC2 / CID Result
			writer.append(cpsData, 20, 22); // Account Level Processing (from CPS)
			StringUtils.appendPadded(writer, null, ' ', 19, Justification.LEFT); // Reserved
			writer.append(RECORD_TERMINATOR);

			recordCount++;
		}

		protected void writeMAARecord() throws AttributeConversionException, IOException {
			String countryCd = getDetailAttribute(AuthorityAttrEnum.ATTR_COUNTRY_CD, String.class, false);
			if(!validCountryCds.contains(countryCd))
				return;
			String doingBusinessAs = getDetailAttribute(AuthorityAttrEnum.ATTR_DOING_BUSINESS_AS, String.class, false);
			if(doingBusinessAs == null)
				doingBusinessAs = step.getAttribute(AuthorityAttrEnum.ATTR_MERCHANT_NAME, String.class, true);
			String city = getDetailAttribute(AuthorityAttrEnum.ATTR_CITY, String.class, false);
			String stateCd = getDetailAttribute(AuthorityAttrEnum.ATTR_STATE_CD, String.class, false);
			String postal = getDetailAttribute(AuthorityAttrEnum.ATTR_POSTAL, String.class, false);
			String address = getDetailAttribute(AuthorityAttrEnum.ATTR_ADDRESS, String.class, false);
			if(!StringUtils.isBlank(city) && !StringUtils.isBlank(stateCd) && !StringUtils.isBlank(postal)) {
				if("US".equalsIgnoreCase(countryCd) && usTerritories.contains(stateCd)) {
					countryCd = stateCd;
					stateCd = null;
				}
				writeMAARecord(doingBusinessAs, city, stateCd, postal, countryCd, address);
			}
		}

		/**
		 * Fields:
		 * Record Type 1-3 3 AN Constant MAA "MAA"
		 * Merchant DBA Name 4-28 25 AN The Merchant's "Doing Business As" name New Value, Left-justified, trailing spaces
		 * Merchant City 29-41 13 AN The Merchant's City Name New Value, Left-justified, trailing spaces
		 * Merchant State 42-43 2 AN The Merchant's State New Value
		 * Merchant Postal Code 44-52 9 AN The Merchant's postal or zip code New Value, Left-justified, trailing spaces
		 * Merchant Country Code 53-55 3 AN The Merchant's Country Code USA or CAN
		 * Merchant Street Address 56-85 30 AN The Merchant's Street Address New Value, Left-justified, trailing spaces
		 * Reserved 86-200 115 AN Reserved for Future USe Space filled
		 * 
		 * @param doingBusinessAs
		 * @param city
		 * @param stateCd
		 * @param postal
		 * @param countryCd
		 * @param address
		 * @throws IOException
		 * @throws AttributeConversionException
		 */
		protected void writeMAARecord(String doingBusinessAs, String city, String stateCd, String postal, String countryCd, String address) throws IOException, AttributeConversionException {
			if(!StringUtils.isBlank(postal))
				postal = nonAlpaNumPattern.matcher(postal).replaceAll("");
			if(!StringUtils.isBlank(countryCd) && (countryCd = countryCd.trim()).length() == 2)
				try {
					countryCd = new Locale("", countryCd).getISO3Country();
				} catch(MissingResourceException e) {
					// ignore
				}

			writer.append("MAAUSA*");
			StringUtils.appendPadded(writer, doingBusinessAs, ' ', 21, Justification.LEFT);
			StringUtils.appendPadded(writer, city, ' ', 13, Justification.LEFT);
			StringUtils.appendPadded(writer, stateCd, ' ', 2, Justification.LEFT);
			StringUtils.appendPadded(writer, postal, ' ', 9, Justification.LEFT); // strip punctuation
			StringUtils.appendPadded(writer, countryCd, ' ', 3, Justification.LEFT); // Convert to 3-digit
			StringUtils.appendPadded(writer, address, ' ', 30, Justification.LEFT);
			StringUtils.appendPadded(writer, null, ' ', 115, Justification.LEFT);

			writer.append(RECORD_TERMINATOR);
			recordCount++;
		}

		protected void writeDMARecord(String invoiceNumber, String avsResult, BigInteger authedAmount, String csPhone, long saleUploadTime) throws IOException {
			if(csPhone != null)
				csPhone = csPhone.replaceAll("[^0-9]", "");
			writer.append("DMA");
			StringUtils.appendPadded(writer, invoiceNumber, ' ', 25, Justification.LEFT); // Purchase Identifier
			StringUtils.appendPadded(writer, avsResult, ' ', 1, Justification.LEFT); // AVS Result
			StringUtils.appendPadded(writer, null, '0', 4, Justification.RIGHT); // Installment Sequence (2), Installment Count (2)
			StringUtils.appendPadded(writer, authedAmount.toString(), '0', 10, Justification.RIGHT); // Original Authorization Amount (approved amount)
			StringUtils.appendPadded(writer, csPhone, ' ', 10, Justification.LEFT); // Customer Service number
			StringUtils.appendPadded(writer, batchDateFormat.format(new Date(saleUploadTime)), ' ', 8, Justification.LEFT); // Ship Date
			StringUtils.appendPadded(writer, null, ' ', 139, Justification.LEFT); // Reserved

			writer.append(RECORD_TERMINATOR);
			recordCount++;
		}

		/**
		 * Fields:
		 * Record Type 1-3 3 AN Constant BHR "BHR"
		 * Merchant ID 4-19 16 AN Left-justified with trailing spaces Elavon Merchant ID, Left-justified, trailing spaces
		 * Merchant DBA Name 20-44 25 AN The name the merchant is "Doing Business As" Same as what is listed at Elavon, left-justified, trailing spaces
		 * Merchant City 45-57 13 AN The city the merchant is doing business in Same as what is listed at Elavon, left justified, trailing spaces
		 * Merchant State 58-59 2 AN The 2 digit State code or Canadian Province Same as what is listed at Elavon
		 * Merchant Zip 60-68 9 AN Left-justified with trailing spaces Same as what is listed at Elavon
		 * Merchant Country 69-71 3 AN 3 character country code for this location
		 * For example :g. USA, CAN, etc. USA (or CAN for Canadian merchants)
		 * Reserved 72-75 4 AN Reserved for Future Use Space filled
		 * Settlement Date 76-83 8 N Date batch settled - YYYYMMDD Current date
		 * 
		 * Batch number 84-94 11 N Elavon 11 digit GBOK batch number, OR the batch number assigned by the user Meaningful batch number.
		 * Network Identifier 95-98 4 AN Network the batch was captured on.
		 * NOVA = NOVA Information Systems "NOVA"
		 * Reference Number 99-109 11 AN Beginning Transaction reference number User defined
		 * Client Group 110-113 4 AN Client Group assigned by MPS Space filled
		 * MPS Number 114-125 12 AN Left-justified with trailing spaces Space filled
		 * Batch Response Code 126-127 2 AN Left-justified with trailing spaces Space filled
		 * Batch Type 128-128 1 AN The ECS Batch Type.
		 * A = Account Receivable Conversion (ARC)
		 * P = Point-of-Purchase (POP) D = Pre-Authorized Payment and Deposit (PPD)
		 * S = Point-Of-Sale (POS) "S"
		 * Reserved 129-200 72 AN Reserved for future use Space filled
		 * 
		 * @throws IOException
		 * @throws AttributeConversionException
		 */
		protected void writeBatchHeader() throws IOException, AttributeConversionException {
			writer.append("BHR");
			StringUtils.appendPadded(writer, getMerchantId(step), ' ', 16, Justification.LEFT);
			StringUtils.appendPadded(writer, step.getAttribute(AuthorityAttrEnum.ATTR_MERCHANT_NAME, String.class, true), ' ', 25, Justification.LEFT);
			// We will try to send blanks for the merchant address - since we don't have that info in our database
			StringUtils.appendPadded(writer, getMerchantCity(), ' ', 13, Justification.LEFT); // City
			StringUtils.appendPadded(writer, getMerchantState(), ' ', 2, Justification.LEFT); // State
			StringUtils.appendPadded(writer, getMerchantZip(), ' ', 9, Justification.LEFT); // Zip
			String currencyCd = step.getAttribute(AuthorityAttrEnum.ATTR_CURRENCY_CD, String.class, true);
			String country3 = "";
			if(currencyCd.length() > 1)
				try {
					country3 = new Locale("", currencyCd.substring(0, 2)).getISO3Country();
				} catch(MissingResourceException e) {
					log.info("Could not find country for currency '" + currencyCd + "'");
				}
			StringUtils.appendPadded(writer, country3, ' ', 3, Justification.LEFT); // Country
			StringUtils.appendPadded(writer, null, ' ', 4, Justification.LEFT); // Reserved
			StringUtils.appendPadded(writer, batchDateFormat.format(fileCreateDate), ' ', 8, Justification.LEFT); // Settlement Date
			StringUtils.appendPadded(writer, step.getAttribute(AuthorityAttrEnum.ATTR_TERMINAL_BATCH_NUM, BigInteger.class, true).toString(), '0', 11, Justification.RIGHT); // Batch number
			writer.append("NOVA");
			StringUtils.appendPadded(writer, step.getAttribute(AuthorityAttrEnum.ATTR_TRACE_NUMBER, BigInteger.class, true).toString(), '0', 11, Justification.RIGHT); // Reference number
			StringUtils.appendPadded(writer, null, ' ', 4 + 12 + 2, Justification.LEFT); // Client Group, MPS Number, Batch Response Code
			writer.append('S'); // Batch Type
			StringUtils.appendPadded(writer, null, ' ', 72, Justification.LEFT); // Reserved
			writer.append(RECORD_TERMINATOR);
			recordCount++;
		}

		/**
		 * Fields:
		 * Record Type 1-3 3 AN Constant BTR "BTR"
		 * Batch Purchases Count 4-9 6 N The count of purchase transactions for the batch Count of payments in batch, pad with leading zeros.
		 * Batch Purchases Amount 10-20 11 N The total dollar amount of purchase transactions for the batch in 9(9)v99 format Sum of payments in batch, pad with leading zeros
		 * Batch Return Count 21-26 6 N The count of return transactions for the batch Count of returns in batch, pad with leading zeros
		 * Batch Return Amount 27-37 11 N The total dollar amount of return transactions for the batch in 9(9)v99 format Sum of returns in batch, pad with leading zeros
		 * Batch Total Count 38-43 6 N The total count of all transactions in the batch Count of all trnx in batch, pad with leading zeros.
		 * Batch Net Amount 44-54 11 N Purchase Amount minus Return Amount (no sign) 9(9)v99 Sum of sales minus returns in batch, pad with leading zeros.
		 * Batch Net Sign 55-55 1 AN The "+" or "-" character depending on sign of Batch Net Amount field "+" or "-"
		 * Batch Number 56-66 11 N NOVA 11 digit batch number OR the batch number assigned by the user Same as Batch Number assigned in the Batch Header Record.
		 * Reserved 67-200 134 AN Reserved for future use Space filled
		 * 
		 * @throws IOException
		 * @throws AttributeConversionException
		 */
		protected void writeBatchTrailer() throws IOException, AttributeConversionException {
			writer.append("BTR");
			BigInteger saleCount = step.getAttribute(AuthorityAttrEnum.ATTR_CREDIT_SALE_COUNT, BigInteger.class, true);
			BigInteger saleAmount = step.getAttribute(AuthorityAttrEnum.ATTR_CREDIT_SALE_AMOUNT, BigInteger.class, true);
			BigInteger refundCount = step.getAttribute(AuthorityAttrEnum.ATTR_CREDIT_REFUND_COUNT, BigInteger.class, true);
			BigInteger refundAmount = step.getAttribute(AuthorityAttrEnum.ATTR_CREDIT_REFUND_AMOUNT, BigInteger.class, true);
			if(cancelledAmount != null)
				saleAmount = saleAmount.add(cancelledAmount);
			// Is this correct?
			if(partialReversalCount > 0)
				saleCount = saleCount.add(BigInteger.valueOf(partialReversalCount));

			StringUtils.appendPadded(writer, saleCount.toString(), '0', 6, Justification.RIGHT);
			StringUtils.appendPadded(writer, saleAmount.toString(), '0', 11, Justification.RIGHT);
			StringUtils.appendPadded(writer, refundCount.toString(), '0', 6, Justification.RIGHT);
			StringUtils.appendPadded(writer, refundAmount.toString(), '0', 11, Justification.RIGHT);
			StringUtils.appendPadded(writer, saleCount.add(refundCount).toString(), '0', 6, Justification.RIGHT);
			StringUtils.appendPadded(writer, saleAmount.subtract(refundAmount).abs().toString(), '0', 11, Justification.RIGHT);
			if(saleAmount.compareTo(refundAmount) < 0)
				writer.append('-');
			else
				writer.append('+');
			StringUtils.appendPadded(writer, step.getAttribute(AuthorityAttrEnum.ATTR_TERMINAL_BATCH_NUM, BigInteger.class, true).toString(), '0', 11, Justification.RIGHT); // Batch number
			StringUtils.appendPadded(writer, null, ' ', 134, Justification.LEFT);
			writer.append(RECORD_TERMINATOR);
			recordCount++;
		}

		/**
		 * Fields:
		 * Record Type 1-3 3 AN Constant FHR "FHR"
		 * File Create Date 4-11 8 N YYYYMMDD
		 * File Create Time 12-17 6 N HHMMSS
		 * Version number 18-19 2 N Version of this specification - constant '35' "35"
		 * NOVA File Number 20-23 4 N Destination ID provided by Elavon "8009"
		 * Sending Institution Name 24-48 25 AN User assigned "USA Technologies" with added spaces to 25 positions
		 * Originating File Name 49-80 32 AN The name of original submitted file P8009YYYYMMDDHHMMSS.UAT (File extension will change going to production,)
		 * Reserved 81-200 120 AN Reserved for future use Space filled
		 * 
		 * @throws IOException
		 */
		protected void writeFileHeader() throws IOException {
			writer.append("FHR");
			writer.append(standardDateFormat.format(fileCreateDate));
			writer.append("35");
			StringUtils.appendPadded(writer, Integer.toString(destinationId), '0', 4, Justification.RIGHT);
			StringUtils.appendPadded(writer, "USA Technologies", ' ', 25, Justification.LEFT);
			StringUtils.appendPadded(writer, fileName, ' ', 32, Justification.LEFT);
			StringUtils.appendPadded(writer, null, ' ', 120, Justification.LEFT);
			writer.append(RECORD_TERMINATOR);
			recordCount++;
		}

		/**
		 * Fields:
		 * Record Type 1-3 3 AN Constant FTR "FTR"
		 * File Purchases Count 4-9 6 N The count of purchase transactions in the file Number of payments included in file, pad with leading zeros.
		 * File Purchases Amount 10-20 11 N The total dollar amount of the purchase transactions for the file in 9(9)v99 format Sum of payments included in file, pad with leading zeros.
		 * File Return Count 21-26 6 N The count of return transactions in the file Count of returns in batch, pad with leading zeros
		 * File Return Amount 27-37 11 N The total dollar amount of return transactions for the file in 9(9)v99 format Sum of returns in batch, pad with leading zeros
		 * File Total Count 38-43 6 N The total count of all transactions in the file Number of payments included in file, pad with leading zeros.
		 * File Net Amount 44-54 11 N Purchase Amount minus Return Amount (no sign) in 9(9)v99 format Sum of sales minus returns in batch, pad with leading zeros.
		 * File Net Sign 55-55 1 AN The '+' or '-' character depending on sign of File Net Amount field '+' or '-'
		 * File Record Count 56-61 6 N Number of physical records in the file Count of records in this file, including this trailer record, pad with leading zeros.
		 * File Create Date 62-69 8 N YYYYMMDD (same as file header)
		 * File Create Time 70-75 6 N HHMMSS (same as file header)
		 * Reserved 76-200 125 AN Reserved for future use Space filled
		 */
		protected void writeFileTrailer() throws IOException, AttributeConversionException {
			writer.append("FTR");
			BigInteger saleCount = step.getAttribute(AuthorityAttrEnum.ATTR_CREDIT_SALE_COUNT, BigInteger.class, true);
			BigInteger saleAmount = step.getAttribute(AuthorityAttrEnum.ATTR_CREDIT_SALE_AMOUNT, BigInteger.class, true);
			BigInteger refundCount = step.getAttribute(AuthorityAttrEnum.ATTR_CREDIT_REFUND_COUNT, BigInteger.class, true);
			BigInteger refundAmount = step.getAttribute(AuthorityAttrEnum.ATTR_CREDIT_REFUND_AMOUNT, BigInteger.class, true);
			if(cancelledAmount != null)
				saleAmount = saleAmount.add(cancelledAmount);
			StringUtils.appendPadded(writer, saleCount.toString(), '0', 6, Justification.RIGHT);
			StringUtils.appendPadded(writer, saleAmount.toString(), '0', 11, Justification.RIGHT);
			StringUtils.appendPadded(writer, refundCount.toString(), '0', 6, Justification.RIGHT);
			StringUtils.appendPadded(writer, refundAmount.toString(), '0', 11, Justification.RIGHT);
			StringUtils.appendPadded(writer, saleCount.add(refundCount).toString(), '0', 6, Justification.RIGHT);
			StringUtils.appendPadded(writer, saleAmount.subtract(refundAmount).abs().toString(), '0', 11, Justification.RIGHT);
			if(saleAmount.compareTo(refundAmount) < 0)
				writer.append('-');
			else
				writer.append('+');
			StringUtils.appendPadded(writer, Integer.toString(++recordCount), '0', 6, Justification.RIGHT);
			writer.append(standardDateFormat.format(fileCreateDate));
			StringUtils.appendPadded(writer, null, ' ', 125, Justification.LEFT);
			writer.append(RECORD_TERMINATOR);
		}
	}
	/** Decrypts and parses the capture details file.
	 * @param resource
	 * @param step
	 * @param handler
	 * @throws ServiceException 
	 * @throws AttributeConversionException 
	 */
	protected <I extends Exception> void parseCaptureDetails(Resource resource, MessageChainStep step, DatasetHandler<I> handler) throws ServiceException, AttributeConversionException, I {
		byte[] key = step.getAttribute(AuthorityAttrEnum.ATTR_CAPTURE_DETAILS_ENCRYPTION_KEY, byte[].class, true);	
		String cipherName = step.getAttribute(AuthorityAttrEnum.ATTR_CAPTURE_DETAILS_CIPHER, String.class, true);	
		int blockSize = step.getAttribute(AuthorityAttrEnum.ATTR_CAPTURE_DETAILS_BLOCK_SIZE, Integer.class, true);
		try {
			CipherInputStream cis = Cryption.createDecryptingInputStream(Cryption.DEFAULT_PROVIDER, cipherName, key, blockSize, resource.getInputStream());
			try {
				DatasetUtils.parseDataset(new InputStreamByteInput(cis), handler);
			} finally {
				cis.close();
			}
		} catch(GeneralSecurityException e) {
			throw new ServiceException("Could not parse Capture Details", e);
		} catch(IOException e) {
			throw new ServiceException("Could not parse Capture Details", e);
		} 
	}
	
	private String[] getPreActionLogAttributes() {
		return new String[] {
			MessageAttribute.ACTION_TYPE.getValue(),
			MessageAttribute.AUTHORITY_NAME.getValue(),
			MessageAttribute.TRACE_NUMBER.getValue(),
			MessageAttribute.POS_PTA_ID.getValue(),
			MessageAttribute.AMOUNT.getValue(),
			MessageAttribute.CURRENCY_CD.getValue(),
			MessageAttribute.TIME_ZONE_GUID.getValue(),
			MessageAttribute.ONLINE.getValue()
		};
	}

	private String[] getPostActionLogAttributes() {
		return new String[] {
			MessageAttribute.ACTION_TYPE.getValue(),
			MessageAttribute.AUTHORITY_NAME.getValue(),
			MessageAttribute.TRACE_NUMBER.getValue()
		};
	}

	protected AuthResultCode populateFailedResults(AuthorityAction action, Map<String, Object> resultAttributes, String errorText) {
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue(), 'F');
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue(), "--");
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC.getValue(), errorText);
		if(debugLevel == DebugLevel.DEBUG)
			resultAttributes.put("clientText", (errorText != null && errorText.length() > 255 ? errorText.substring(0, 255) : errorText));
		return AuthResultCode.FAILED;
	}

	protected AuthResultCode populateDeclinedResults(AuthorityAction action, Map<String, Object> resultAttributes, String errorText) {
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue(), 'N');
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue(), "--");
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC.getValue(), errorText);
		if(debugLevel == DebugLevel.DEBUG)
			resultAttributes.put("clientText", (errorText != null && errorText.length() > 255 ? errorText.substring(0, 255) : errorText));
		return AuthResultCode.DECLINED;
	}
	
	public DebugLevel getDebugLevel() {
		return debugLevel;
	}

	public void setDebugLevel(DebugLevel debugLevel) {
		this.debugLevel = debugLevel;
	}

	public Set<String> getSupportedCurrencies() {
		return supportedCurrencies;
	}

	public void setSupportedCurrencies(Set<String> supportedCurrencies) {
		this.supportedCurrencies = supportedCurrencies;
	}

	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}

	public int getMaxRetryAllowed() {
		return maxRetryAllowed;
	}

	public void setMaxRetryAllowed(int maxRetryAllowed) {
		this.maxRetryAllowed = maxRetryAllowed;
	}
	public CardType getCardType(String cardTypeText) {
		return cardTypeMapping.get(cardTypeText == null ? null : cardTypeText.trim().toUpperCase());
	}
	public void setCardType(String cardTypeText, CardType cardTypeCode) {
		cardTypeMapping.put(cardTypeText == null ? null : cardTypeText.trim().toUpperCase(), cardTypeCode);
	}

	public ElavonClearingFileSite getFileSite() {
		return fileSite;
	}

	public void setFileSite(ElavonClearingFileSite fileSite) {
		this.fileSite = fileSite;
	}
    
    public String getMerchantCity() {
		return merchantCity;
	}

	public void setMerchantCity(String merchantCity) {
		this.merchantCity = merchantCity;
	}

	public String getMerchantState() {
		return merchantState;
	}

	public void setMerchantState(String merchantState) {
		this.merchantState = merchantState;
	}

	public String getMerchantZip() {
		return merchantZip;
	}

	public void setMerchantZip(String merchantZip) {
		this.merchantZip = merchantZip;
	}

	public String getPartialAuthReversalTerminalPattern() {
		return partialAuthReversalTerminalPattern.pattern();
	}

	public void setPartialAuthReversalTerminalPattern(String partialAuthReversalTerminalPattern) {
		setPartialAuthReversalTerminalPattern(Pattern.compile(partialAuthReversalTerminalPattern));
	}

	protected void setPartialAuthReversalTerminalPattern(Pattern partialAuthReversalTerminalPattern) {
		this.partialAuthReversalTerminalPattern = partialAuthReversalTerminalPattern;
	}

	public String getAddressAddendumTerminalPattern() {
		return addressAddendumTerminalPattern.pattern();
	}

	public void setAddressAddendumTerminalPattern(String addressAddendumTerminalPattern) {
		setAddressAddendumTerminalPattern(Pattern.compile(addressAddendumTerminalPattern));
	}

	protected void setAddressAddendumTerminalPattern(Pattern addressAddendumTerminalPattern) {
		this.addressAddendumTerminalPattern = addressAddendumTerminalPattern;
	}

	public CardType[] getAddressAddendumCardTypes() {
		return addressAddendumCardTypes.toArray(new CardType[addressAddendumCardTypes.size()]);
	}

	public void setAddressAddendumCardTypes(CardType[] cardTypes) {
		addressAddendumCardTypes.clear();
		if(cardTypes == null)
			cardTypes = CardType.values();
		addressAddendumCardTypes.addAll(Arrays.asList(cardTypes));
	}

	public String[] getValidCountryCds() {
		return validCountryCds.toArray(new String[validCountryCds.size()]);
	}

	public void setValidCountryCds(String[] countryCds) {
		validCountryCds.clear();
		if(countryCds != null)
			for(String cc : countryCds)
				validCountryCds.add(cc);
	}

	public String[] getUsTerritories() {
		return usTerritories.toArray(new String[usTerritories.size()]);
	}

	public void setUsTerritories(String[] stateCds) {
		usTerritories.clear();
		if(stateCds != null)
			for(String sc : stateCds)
				usTerritories.add(sc);
	}

	public int getSpecificationVersion() {
		return specificationVersion;
	}

	public void setSpecificationVersion(int specificationVersion) {
		this.specificationVersion = specificationVersion;
	}
}
