package com.usatech.authoritylayer;

import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import simple.app.Publisher;
import simple.app.SelfProcessor;
import simple.db.DataLayerMgr;
import simple.io.ByteInput;
import simple.io.Log;
import simple.results.Results;
import simple.security.crypt.EncryptionInfoMapping;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.constants.AuthorityAction;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.MessageAttrEnum;
import com.usatech.layers.common.constants.MessageAttribute;

public class AuthoritySyncTask implements SelfProcessor {
	private static final Log log = Log.getLog();
	
	protected static final String AUTHORITY_SYNC = "AUTHORITY_SYNC";
	
	protected int appInstance;
	protected Publisher<ByteInput> publisher;
	protected EncryptionInfoMapping encryptionInfoMapping;
	protected int gatewayTimeout = 30; // seconds
	protected static final AtomicLong traceNumberGenerator = new AtomicLong(new SecureRandom().nextLong());
	protected static int traceNumberOffset;

	@Override
	public void process() {
		Results results = null;
		long authoritySyncId = -1;
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			if (!InteractionUtils.lockProcess(AUTHORITY_SYNC, String.valueOf(appInstance), params)) {
	  		log.info("Authority Sync is already locked by instance " + params.get("lockedBy"));
	  		return;
	  	}
		} catch(Exception e) { 
			log.error("Error locking {0}", AUTHORITY_SYNC, e);
			return;
		}

		try {
			results = DataLayerMgr.executeQuery("GET_AUTHORITY_SYNCS", null);
			while(results.next()) {
				long executionMs = System.currentTimeMillis();
				char operationCd = results.getValue("operationCd", char.class);
				AuthorityAction actionType = null;
				switch (operationCd) {
					case 'I':
					case 'U':
						actionType = AuthorityAction.TERMINAL_UPDATE;
						break;
					case 'D':
						actionType = AuthorityAction.TERMINAL_DELETE;
						break;
					default:
						log.error("Invalid operationCd '" + operationCd + "' for authoritySyncId: " + authoritySyncId + ", exiting...");
						return;
				}

				authoritySyncId = results.getValue("authoritySyncId", Long.class);
				long createdUtcTsMs = results.getValue("createdUtcTsMs", Long.class);

				MessageChain mc = new MessageChainV11();
				String queueKey = new StringBuilder("usat.authority.").append(results.getValue("paymentSubtypeClass", String.class).toLowerCase()).toString();
				MessageChainStep step = mc.addStep(queueKey);
				step.setAttribute(AuthorityAttrEnum.ATTR_ACTION_TYPE, actionType);
				step.setAttribute(AuthorityAttrEnum.ATTR_RESPONSE_TIME_OUT, gatewayTimeout);
				step.setAttribute(MessageAttrEnum.ATTR_DEVICE_NAME, results.getValue("itemCd", String.class));
				step.setAttribute(AuthorityAttrEnum.ATTR_ADDRESS, results.getValue("address1", String.class));
				step.setAttribute(AuthorityAttrEnum.ATTR_CITY, results.getValue("city", String.class));
				step.setAttribute(AuthorityAttrEnum.ATTR_STATE_CD, results.getValue("state", String.class));
				step.setAttribute(AuthorityAttrEnum.ATTR_POSTAL, results.getValue("zip", String.class));
				step.setAttribute(AuthorityAttrEnum.ATTR_COUNTRY_CD, results.getValue("countryCd", String.class));
				step.setAttribute(AuthorityAttrEnum.ATTR_LOCATION_NAME, results.getValue("locationName", String.class));
				step.setAttribute(AuthorityAttrEnum.ATTR_AUTH_TIME, executionMs);
				step.setAttribute(AuthorityAttrEnum.ATTR_TIMEZONE_GUID, results.getValue("timeZoneGuid", String.class));
				step.setAttribute(AuthorityAttrEnum.ATTR_TRACE_NUMBER, nextTraceNumber());
				step.setAttribute(AuthorityAttrEnum.ATTR_MERCHANT_CD, results.getValue("merchantCd", String.class));
				step.setAttribute(AuthorityAttrEnum.ATTR_MERCHANT_CATEGORY_CODE, results.getValue("mcc", String.class));
				step.setAttribute(MessageAttribute.AUTHORITY_NAME, results.getValue("authorityName", String.class));
				MessageChainService.publish(mc, getPublisher(), getEncryptionInfoMapping());

				long deletionMs = System.currentTimeMillis();
				DataLayerMgr.executeUpdate("DELETE_AUTHORITY_SYNC", new Object[] {authoritySyncId}, true);

				long time = System.currentTimeMillis();
				StringBuilder sb = new StringBuilder("Processed authority sync, authoritySyncId: ").append(results.getValue("authoritySyncId", Long.class))
					.append(", authoritySyncTypeCd: ").append(results.getValue("authoritySyncTypeCd", String.class)).append(", objectCd: ").append(results.getValue("objectCd", String.class))
					.append(", operationCd: ").append(results.getValue("operationCd", String.class)).append(", itemId: ").append(results.getValue("itemId", Long.class))
					.append(", deviceName: ").append(results.getValue("itemCd", String.class)).append(", terminalNbr: ").append(results.getValue("terminalNbr", String.class))
					.append(", createdBy: ").append(results.getValue("createdBy", String.class)).append(", createdUtcTs: ").append(results.getValue("createdUtcTs", String.class));
					sb.append(", execution time: ").append(time - executionMs).append(" ms");
				sb.append(", deletion time: ").append(time - deletionMs).append(" ms");
				sb.append(", total time: ").append(time - createdUtcTsMs).append(" ms");
				log.info(sb.toString());
			};
		} catch(Exception e) {
	    	log.error("Failed to process authority sync, authoritySyncId: " + authoritySyncId + ", exiting...", e);
		} finally {
			if (results != null)
				results.close();
			try {
				InteractionUtils.unlockProcess(AUTHORITY_SYNC, String.valueOf(appInstance), params);
			} catch (Exception e) {
				log.error("Error unlocking {0}", AUTHORITY_SYNC, e);
			}
		}
	}
	
	protected static long nextTraceNumber() {
		long base = traceNumberGenerator.getAndAdd(100L);
		base -= (base % 100);
		if(base < 0)
			base -= getTraceNumberOffset();
		else
			base += getTraceNumberOffset();
		return base;
	}

	public int getAppInstance() {
		return appInstance;
	}

	public void setAppInstance(int appInstance) {
		this.appInstance = appInstance;
	}

	public Publisher<ByteInput> getPublisher() {
		return publisher;
	}
	
	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}
	
	public EncryptionInfoMapping getEncryptionInfoMapping() {
		return encryptionInfoMapping;
	}

	public void setEncryptionInfoMapping(EncryptionInfoMapping encryptionInfoMapping) {
		this.encryptionInfoMapping = encryptionInfoMapping;
	}

	public int getGatewayTimeout() {
		return gatewayTimeout;
	}

	public void setGatewayTimeout(int gatewayTimeout) {
		this.gatewayTimeout = gatewayTimeout;
	}

	public static int getTraceNumberOffset() {
		return traceNumberOffset;
	}

	public static void setTraceNumberOffset(int traceNumberOffset) {
		AuthoritySyncTask.traceNumberOffset = traceNumberOffset;
	}	
}
