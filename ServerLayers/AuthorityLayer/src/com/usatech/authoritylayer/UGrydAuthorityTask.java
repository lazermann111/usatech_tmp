package com.usatech.authoritylayer;

import static simple.text.MessageFormat.*;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.net.ssl.HostnameVerifier;

import org.apache.http.HttpException;
import org.apache.http.NoHttpResponseException;

import com.google.gson.JsonSyntaxException;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertUtils;
import simple.bean.MaskedString;
import simple.io.Log;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;

import com.usatech.app.Attribute;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.HttpRequester;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAction;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.DeniedReason;
import com.usatech.layers.common.constants.PaymentMaskBRef;

/**
 * @author Paul Cowan
 *
 */
public class UGrydAuthorityTask extends AbstractAuthorityTask {
	private static final Log log = Log.getLog();

	private static ThreadLocal<NumberFormat> amountFormatLocal = new ThreadLocal<NumberFormat>() {
		@Override
		protected NumberFormat initialValue() {
			return new DecimalFormat("#0.00");
		}
	};

	private static ThreadLocal<Format> currencyFormatLocal = new ThreadLocal<Format>() {
		@Override
		protected Format initialValue() {
			return ConvertUtils.getFormat("CURRENCY", "#0.00");
		}
	};

	private static ThreadLocal<NumberFormat> responseCodeFormatLocal = new ThreadLocal<NumberFormat>() {
		@Override
		protected NumberFormat initialValue() {
			return new DecimalFormat("#.##");
		}
	};
	
	private static ThreadLocal<DateFormat> authorityTimestampFormat = new ThreadLocal<DateFormat>() {
		@Override
		protected DateFormat initialValue() {
			return new SimpleDateFormat("yyyyMMddHHmmss");
		}
	};

	public static enum UGryTransactionType implements Attribute {
		ACK("ack", new BigDecimal("1900")),
		BALANCE_INQUIRY("balance_inquiry", new BigDecimal("1000")),
		REFUND("refund", new BigDecimal("1300")),
		SALE("sale", new BigDecimal("1100")),
		VOID("void", new BigDecimal("1200")),
		VEND_SALE("vend_sale", new BigDecimal("1150")),
		VEND_INQUIRY("vend_inquiry", new BigDecimal("1050"));

		private String value;
		private BigDecimal responseCode;

		private UGryTransactionType(String value, BigDecimal responseCode) {
			this.value = value;
			this.responseCode = responseCode;
		}

		public String getValue() {
			return value;
		}

		public BigDecimal getResponseCode() {
			return responseCode;
		}

		public String getResponseString() {
			return responseCodeFormatLocal.get().format(responseCode);
		}

		public boolean isSuccess(BigDecimal responseCode) {
			if (responseCode == null)
				return false;
			if (responseCode.compareTo(this.responseCode) != 0)
				return false;
			return true;
		}

		public static UGryTransactionType lookup(String transactionType) {
			return UGryTransactionType.valueOf(transactionType.toUpperCase());
		}
	}

	public static final int MAX_SEQUENCE_ID = 32767;

	protected int seqIdCounter = -1;
	protected int instance = -1;
	protected int instanceCount = -1;
	protected int seqMin;
	protected int seqMax;
	protected String[] sslProtocols;
	protected HostnameVerifier hostNameVerifier;
	protected int connectionTimeout = (int) TimeUnit.SECONDS.toMillis(5);
	protected int maxHostConnections = -1;
	protected int maxTotalConnections = -1;
	protected int socketTimeout = (int) TimeUnit.SECONDS.toMillis(15);
	protected String ugrydUrl;
	protected String ugrydUsername;
	protected String ugrydPassword;
	protected BigDecimal minimumBalanceAmount = new BigDecimal("1.00");
	protected boolean padTerminalCd = true;
	protected Set<String> doNotPadTerminalCds = new HashSet<>(Arrays.asList("ACHGROSSSTORE"));
	protected boolean useDeviceSerialCdForTerminalCd = true;
	
	protected HttpRequester httpRequester;

	public UGrydAuthorityTask() {
		super("CBORD_UGryd");
	}

	public void initialize() throws ServiceException {
		initialize(null);
	}

	public void initialize(HttpRequester customHttpRequester) throws ServiceException {
		if (instance <= 0)
			throw new ServiceException("Illegal instance value: " + instance);
		if (instanceCount <= 0)
			throw new ServiceException("Illegal instanceCount value: " + instance);
		if (instance > instanceCount)
			throw new ServiceException(format("Illegal instance/instanceCount values: {0}/{1}", instance, instanceCount));

		// ability to set custom HttpRequester makes testing easier
		if (customHttpRequester == null) {
			httpRequester = new HttpRequester(sslProtocols);
			httpRequester.setIncludeJsonVersion(false);
			if (hostNameVerifier != null)
				httpRequester.setHostNameVerifier(hostNameVerifier);
			if (connectionTimeout >= 0)
				httpRequester.setConnectionTimeout(connectionTimeout);
			if (maxHostConnections >= 0)
				httpRequester.setMaxHostConnections(maxHostConnections);
			if (maxTotalConnections >= 0)
				httpRequester.setMaxTotalConnections(maxTotalConnections);
			if (socketTimeout >= 0)
				httpRequester.setSocketTimeout(socketTimeout);
		} else {
			httpRequester = customHttpRequester;
		}
	}

	@Override
	protected AuthResultCode processRequest(AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo) throws ServiceException {
		if (httpRequester == null) {
			log.warn("Not initialized");
			return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.CONFIGURATION_ISSUE, "Not initialized");
		}
		
		if (taskInfo.isRedelivered() && action == AuthorityAction.AUTHORIZATION) {
			return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.PROCESSOR_ERROR, "Vend Hold Cleared");
		} else if  (taskInfo.isRedelivered() && action != AuthorityAction.REVERSAL && action != AuthorityAction.AUTHORIZATION) {
			// We should only get here after successfully clearing the hold on a vend_inquiry, I think...
			// TODO: is redelivery where we haven't processed the prior message possible?
			log.warn("Unexpected redelivered {0} with retryCount={1}", action, taskInfo.getRetryCount());
			throw new RetrySpecifiedServiceException(format("Unexpected redelivered {0} with retryCount={1}", action, taskInfo.getRetryCount()), WorkRetryType.NO_RETRY);
		}

		MessageChainStep step = taskInfo.getStep();
		String deviceSerialCd;
		String terminalCd;
		AccountData accountData;
		BigInteger amount;
		int minorCurrencyFactor;
		boolean allowPartialAuth;
		String authorityName;
		String authCode;
		try {
			deviceSerialCd = getAttribute(step, AuthorityAttrEnum.ATTR_DEVICE_SERIAL_CD, String.class, true);
			terminalCd = getAttribute(step, AuthorityAttrEnum.ATTR_TERMINAL_CD, String.class, false);
			authorityName = getAttribute(step, AuthorityAttrEnum.ATTR_AUTHORITY_NAME, String.class, true);
			accountData = new AccountData(step.getAttributes());
			amount = getAttribute(step, AuthorityAttrEnum.ATTR_AMOUNT, BigInteger.class, true);
			minorCurrencyFactor = getAttribute(step, AuthorityAttrEnum.ATTR_MINOR_CURRENCY_FACTOR, Integer.class, true);
			allowPartialAuth = step.getAttributeSafely(AuthorityAttrEnum.ATTR_PARTIAL_AUTH_ALLOWED, Boolean.class, true);
			authCode = getAttribute(step, AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD, String.class, false);
		} catch (IllegalArgumentException e) {
			log.warn("Data conversion error", e);
			return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.INVALID_INPUT, "Data conversion error");
		} catch (AuthorityAttributeConversionException e) {
			return AuthResultCode.FAILED;
		}

		String cardNumber = accountData.getTrackData();
		if (StringUtils.isBlank(cardNumber)) {
			cardNumber = accountData.getPartData(PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER);
			if (StringUtils.isBlank(cardNumber))
				return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.INVALID_INPUT, "Account Number not provided");
		}

		// this is sort of a hack - perhaps we should add a field to the database to hold arbitrary authority attributes
		boolean vendSupported = authorityName.toLowerCase().contains("vending");
		BigDecimal amountMajor = InteractionUtils.toMajorCurrency(amount, minorCurrencyFactor);
		
		if (terminalCd == null || useDeviceSerialCdForTerminalCd) {
			terminalCd = deviceSerialCd;
		}

		if (padTerminalCd && !doNotPadTerminalCds.contains(terminalCd)) {
			terminalCd = StringUtils.pad(terminalCd, '0', 15, Justification.RIGHT);
		}
		
		int sequenceId = getNextSequenceId();

		Map<String, Object> payload = new LinkedHashMap<>();
		payload.put("terminal", terminalCd);
		payload.put("seqid", Integer.toString(sequenceId));
		payload.put("account_number", cardNumber);
		payload.put("username", ugrydUsername);
		payload.put("password", ugrydPassword);

		Map<String, Object> resultAttributes = step.getResultAttributes();

		switch (action) {
			case AUTHORIZATION:
				if (vendSupported) {
					return sendVendInquiry(payload, resultAttributes, amountMajor, allowPartialAuth, minorCurrencyFactor);
				} else { 
					return sendBalanceInquiry(payload, resultAttributes, amountMajor, allowPartialAuth, minorCurrencyFactor);
				}
			case SALE:
				if (vendSupported) { 
					// TODO: possibly convert this to a regular sale if the vend_inquiry hold has expired, see below.
					// vend_sale is designed to be paired with a prior vend_inquiry, but the hold is only good for 1 minute (by default, set by UGryd per school)
					// However I've tested vend_sale alone and it appears to work fine even without a valid prior vend_inquiry.
					// Therefore I'm sending the vend_sale here regardless of it's age.
					return sendVendSale(payload, resultAttributes, amountMajor, minorCurrencyFactor);
				} else if (amountMajor.compareTo(BigDecimal.ZERO) > 0) {
					return sendSale(payload, resultAttributes, amountMajor, minorCurrencyFactor);
				} else {
					// This has to be a $0 non-vend sale, which is a reversal
					log.info("No need to reverse balance_inquiry authorization");
					return AuthResultCode.APPROVED;
				}
			case REFUND:
				return sendRefund(payload, resultAttributes, amountMajor, minorCurrencyFactor);
			case REVERSAL:
				log.info("Processing {0} REVERSAL with retryCount={1}", subAction, taskInfo.getRetryCount());
				switch (subAction) {
					case AUTHORIZATION:
						if (vendSupported) {
							// send a $0 vending sale to cancel the last vend auth
							return sendVendSale(payload, resultAttributes, BigDecimal.ZERO, minorCurrencyFactor);
						} else {
							log.info("No need to reverse balance_inquiry authorization");
							return AuthResultCode.APPROVED;
						}
					case SALE:
					case REFUND:
						// TODO: void is only valid until the tran is settled, according to the spec, but it doesn't say when this is
						return sendVoid(payload, resultAttributes, authCode, minorCurrencyFactor);
					default:
						throw new RetrySpecifiedServiceException(format("Reversal of action {0} is NOT implemented", subAction), WorkRetryType.NO_RETRY);
				}
			case ADJUSTMENT:
				// TODO: does this ever happen?
			default:
				throw new RetrySpecifiedServiceException(format("Action {0} is NOT implemented", subAction), WorkRetryType.NO_RETRY);
		}
	}

	@Override
	protected AuthResultCode processReversal(AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo) throws ServiceException {
		if(taskInfo.getRetryCount() > 1)
			return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Failed to reverse transaction");
		
		if (action == AuthorityAction.AUTHORIZATION) {
			return processRequest(AuthorityAction.REVERSAL, action, taskInfo);
		} else {
			throw new RetrySpecifiedServiceException(format("Reversal of action {0} is NOT implemented", subAction), WorkRetryType.NO_RETRY);
		}
	}

	// Balance Inquiry is used to request an account balance without processing any transaction against the account. This is not required during normal
	// transaction processing, as the patron's balance will be checked via standard transaction calls such as Sale and Refund.
	protected AuthResultCode sendBalanceInquiry(Map<String, Object> authorityRequest, Map<String, Object> resultAttributes, BigDecimal amount, boolean allowPartialAuth, int minorCurrencyFactor) throws ServiceException {
		authorityRequest.put("transaction_type", UGryTransactionType.BALANCE_INQUIRY.getValue());

		log.info("Sending {0} {1} to {2}", UGryTransactionType.BALANCE_INQUIRY, maskMap(authorityRequest), ugrydUrl);
		Map<String, Object> authorityResponse;
		try {
			authorityResponse = httpRequester.jsonPost(ugrydUrl, authorityRequest, Map.class, Map.class, null);
		} catch (Exception e) {
			return handleNonAckPostException(AuthorityAction.AUTHORIZATION, UGryTransactionType.BALANCE_INQUIRY, resultAttributes, e);
		}

		log.info("Received {0} from {1}", maskMap(authorityResponse), ugrydUrl);
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA.getValue(), maskMap(authorityResponse).toString());

		BigDecimal responseCode = ConvertUtils.convertSafely(BigDecimal.class, authorityResponse.get("response_code"), null);
		String responseMessage = ConvertUtils.getStringSafely(authorityResponse.get("response_msg"), null);
		if (responseCode == null) {
			log.info("No response code found in response: {0}", maskMap(authorityResponse));
			return populateFailedResults(AuthorityAction.AUTHORIZATION, resultAttributes, DeniedReason.PROCESSOR_ERROR, "No response code");
		}

		if (!UGryTransactionType.BALANCE_INQUIRY.isSuccess(responseCode)) {
			log.info("Received non-success response code: {0}", maskMap(authorityResponse));
			DeniedReason deniedReason = getDeniedReason(responseCode, responseMessage);
			if (deniedReason == DeniedReason.INSUFFICIENT_FUNDS)
				responseMessage = null; // if the denied reason was LOW BALANCE, don't send that message because it's redundant
			return populateDeclinedResults(AuthorityAction.AUTHORIZATION, resultAttributes, deniedReason, responseMessage);
		}

		BigDecimal balance = ConvertUtils.convertSafely(BigDecimal.class, authorityResponse.get("balance"), null);
		if (balance == null) {
			return populateFailedResults(AuthorityAction.AUTHORIZATION, resultAttributes, DeniedReason.PROCESSOR_ERROR, "No balance");
		}

		String transactionId = ConvertUtils.getStringSafely(authorityResponse.get("id"), null);

		BigDecimal approvedFundsMinor = InteractionUtils.toMinorCurrency(amount, minorCurrencyFactor);
		BigDecimal balanceMinor = InteractionUtils.toMinorCurrency(balance, minorCurrencyFactor);

		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_HOLD_USED.getValue(), false);
		resultAttributes.put(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT.getValue(), balanceMinor.longValueExact());
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue(), responseCode.toString());
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC.getValue(), responseMessage);
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD.getValue(), transactionId);
		
		if (balance.compareTo(amount) >= 0) {
			setClientText(resultAttributes, AuthResultCode.APPROVED, null, null, balance, null, responseMessage);
			resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue(), AuthResultCode.APPROVED.getValue());
			resultAttributes.put(AuthorityAttrEnum.ATTR_AMT_APPROVED.getValue(), approvedFundsMinor.longValueExact());
			return AuthResultCode.APPROVED;
		} else if (balance.compareTo(minimumBalanceAmount) < 0) {
			return populateDeclinedResults(AuthorityAction.AUTHORIZATION, resultAttributes, DeniedReason.INSUFFICIENT_FUNDS, currencyFormatLocal.get().format(balance));
		} else if (balance.compareTo(BigDecimal.ZERO) > 0 && allowPartialAuth) {
			setClientText(resultAttributes, AuthResultCode.PARTIAL, null, null, balance, null, responseMessage);
			resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue(), AuthResultCode.PARTIAL.getValue());
			resultAttributes.put(AuthorityAttrEnum.ATTR_AMT_APPROVED.getValue(), balanceMinor.longValueExact());
			return AuthResultCode.PARTIAL;
		} else {
			return populateDeclinedResults(AuthorityAction.AUTHORIZATION, resultAttributes, DeniedReason.INSUFFICIENT_FUNDS, currencyFormatLocal.get().format(balance));
		}
	}

	// Vend Inquiry verifies that a patron has funds available, up to a requested amount, and places a hold on those funds. This is not required prior to
	// processing a sale request, but could be an important step in certain scenarios, such as with vending machines.
	// If the account is not found or funds are unavailable, a 1050.99 reponse code will be returned and the response message field will contain details other than "OK".
	protected AuthResultCode sendVendInquiry(Map<String, Object> authorityRequest, Map<String, Object> resultAttributes, BigDecimal amount, boolean allowPartialAuth, int minorCurrencyFactor) throws ServiceException {
		authorityRequest.put("transaction_type", UGryTransactionType.VEND_INQUIRY.getValue());
		authorityRequest.put("max_vend_amount", amountFormatLocal.get().format(amount));

		log.info("Sending {0} {1} to {2}", UGryTransactionType.VEND_INQUIRY, maskMap(authorityRequest), ugrydUrl);
		Map<String, Object> authorityResponse;
		try {
			authorityResponse = httpRequester.jsonPost(ugrydUrl, authorityRequest, Map.class, Map.class, null);
		} catch (Exception e) {
			return handleNonAckPostException(AuthorityAction.AUTHORIZATION, UGryTransactionType.VEND_INQUIRY, resultAttributes, e);
		}

		log.info("Received {0} from {1}", maskMap(authorityResponse), ugrydUrl);
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA.getValue(), maskMap(authorityResponse).toString());

		BigDecimal responseCode = ConvertUtils.convertSafely(BigDecimal.class, authorityResponse.get("response_code"), null);
		String responseMessage = ConvertUtils.getStringSafely(authorityResponse.get("response_msg"), null);
		if (responseCode == null) {
			log.info("No response code found in response: {0}", maskMap(authorityResponse));
			return populateFailedResults(AuthorityAction.AUTHORIZATION, resultAttributes, DeniedReason.PROCESSOR_ERROR, "No response code");
		}

		if (!UGryTransactionType.VEND_INQUIRY.isSuccess(responseCode)) {
			log.info("Received non-success response code: {0}", maskMap(authorityResponse));
			DeniedReason deniedReason = getDeniedReason(responseCode, responseMessage);
			if (deniedReason == DeniedReason.INSUFFICIENT_FUNDS)
				responseMessage = null; // if the denied reason was LOW BALANCE, don't send that message because it's redundant
			return populateDeclinedResults(AuthorityAction.AUTHORIZATION, resultAttributes, deniedReason, responseMessage);
		}

		BigDecimal approvedFunds = ConvertUtils.convertSafely(BigDecimal.class, authorityResponse.get("approved_funds"), null);
		String transactionId = ConvertUtils.getStringSafely(authorityResponse.get("id"), null);

		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_HOLD_USED.getValue(), true);
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue(), responseCode.toString());
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC.getValue(), responseMessage);
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD.getValue(), transactionId);

		BigDecimal approvedFundsMinor = InteractionUtils.toMinorCurrency(approvedFunds, minorCurrencyFactor);
		
		// unfortunately vend_inquiry doesn't return an account balance

		// approved_funds is optional. If we receive a success result code and no approved_funds then assume it's approved
		if (approvedFunds == null || approvedFunds.compareTo(amount) >= 0) {
			setClientText(resultAttributes, AuthResultCode.APPROVED, null, null, null, null, responseMessage);
			resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue(), AuthResultCode.APPROVED.getValue());
			resultAttributes.put(AuthorityAttrEnum.ATTR_AMT_APPROVED.getValue(), approvedFundsMinor.longValueExact());
			return AuthResultCode.APPROVED;
		} else if (approvedFunds.compareTo(BigDecimal.ZERO) > 0 && allowPartialAuth) {
			// if partial auth, then approvedFunds is the balance
			setClientText(resultAttributes, AuthResultCode.PARTIAL, null, null, approvedFunds, null, responseMessage);
			resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue(), AuthResultCode.PARTIAL.getValue());
			resultAttributes.put(AuthorityAttrEnum.ATTR_AMT_APPROVED.getValue(), approvedFundsMinor.longValueExact());
			resultAttributes.put(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT.getValue(), approvedFundsMinor.longValueExact());
			return AuthResultCode.PARTIAL;
		} else {
			return populateDeclinedResults(AuthorityAction.AUTHORIZATION, resultAttributes, DeniedReason.NO_ACCOUNT, responseMessage != null ? responseMessage : "No account or insufficient funds");
		}
	}

	// Sale is used to process a sale transaction. It must be followed by an ack call or the sale will be reversed.
	protected AuthResultCode sendSale(Map<String, Object> authorityRequest, Map<String, Object> resultAttributes, BigDecimal amount, int minorCurrencyFactor) throws ServiceException {
		authorityRequest.put("transaction_type", UGryTransactionType.SALE.getValue());
		authorityRequest.put("amount", amountFormatLocal.get().format(amount));

		log.info("Sending {0} {1} to {2}", UGryTransactionType.SALE, maskMap(authorityRequest), ugrydUrl);
		Map<String, Object> authorityResponse;
		try {
			authorityResponse = httpRequester.jsonPost(ugrydUrl, authorityRequest, Map.class, Map.class, null);
		} catch (Exception e) {
			return handleNonAckPostException(AuthorityAction.SALE, UGryTransactionType.SALE, resultAttributes, e);
		}

		log.info("Received {0} from {1}", maskMap(authorityResponse), ugrydUrl);
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA.getValue(), maskMap(authorityResponse).toString());

		BigDecimal responseCode = ConvertUtils.convertSafely(BigDecimal.class, authorityResponse.get("response_code"), null);
		String responseMessage = ConvertUtils.getStringSafely(authorityResponse.get("response_msg"), null);
		if (responseCode == null) {
			log.info("No response code found in response: {0}", maskMap(authorityResponse));
			return populateFailedResults(AuthorityAction.SALE, resultAttributes, DeniedReason.PROCESSOR_ERROR, "No response code");
		}

		if (!UGryTransactionType.SALE.isSuccess(responseCode)) {
			log.info("Received non-success response code: {0}", maskMap(authorityResponse));
			return populateDeclinedResults(AuthorityAction.SALE, resultAttributes, getDeniedReason(responseCode, responseMessage), responseMessage);
		}

		BigDecimal balance = ConvertUtils.convertSafely(BigDecimal.class, authorityResponse.get("balance"), null); // optional
		String transactionId = ConvertUtils.getStringSafely(authorityResponse.get("id"), null); // optional
		String authCode = ConvertUtils.getStringSafely(authorityResponse.get("authcode"), null); // optional but needed for ACK
		String terminalTime = ConvertUtils.getStringSafely(authorityResponse.get("terminal_time"), null); // optional
		
		Date responseTs = null;
		if (!StringUtils.isBlank(terminalTime)) {
			try {
				responseTs = authorityTimestampFormat.get().parse(terminalTime);
			} catch (Exception e) {
				log.warn("Received invalid terminal_time: {0}", terminalTime, e);
			}
		}
		
		BigDecimal approvedFundsMinor = InteractionUtils.toMinorCurrency(amount, minorCurrencyFactor);
		BigDecimal balanceMinor = balance == null ? null : InteractionUtils.toMinorCurrency(balance, minorCurrencyFactor);

		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD.getValue(), authCode);
		resultAttributes.put(AuthorityAttrEnum.ATTR_AMT_APPROVED.getValue(), approvedFundsMinor.longValueExact());
		resultAttributes.put(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT.getValue(), balanceMinor.longValueExact());
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue(), responseCode.toString());
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC.getValue(), responseMessage);
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD.getValue(), transactionId);
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS.getValue(), responseTs);

		AuthResultCode ackResult = sendAck(AuthorityAction.SALE, UGryTransactionType.SALE, authorityRequest, resultAttributes, authCode);
		if (ackResult != AuthResultCode.APPROVED)
			return ackResult;
		
		resultAttributes.put(AuthorityAttrEnum.ATTR_SETTLED.getValue(), true);
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue(), ackResult.getValue());

		return AuthResultCode.APPROVED;
	}

	// Vend sale is used to process a sale transaction after vend inquiry has been used. This processes the sale and removes the hold on the funds. It
	// must be followed by an ack call or the sale will be reversed.
	// If a patron cancels a sale where vend inquiry was previously called, vend sale should be called with a $0 amount in order to clear the hold on the funds.
	// For attended point-of-sale implementations, Sale should probably be used instead of Vend Sale.
	protected AuthResultCode sendVendSale(Map<String, Object> authorityRequest, Map<String, Object> resultAttributes, BigDecimal amount, int minorCurrencyFactor) throws ServiceException {
		authorityRequest.put("transaction_type", UGryTransactionType.VEND_SALE.getValue());
		authorityRequest.put("amount", amountFormatLocal.get().format(amount));
		
//		if (amount.compareTo(BigDecimal.ZERO) == 0) {
//			log.info("Reversing vend_inquiry");
//		}

		log.info("Sending {0} {1} to {2}", UGryTransactionType.VEND_SALE, maskMap(authorityRequest), ugrydUrl);
		Map<String, Object> authorityResponse;
		try {
			authorityResponse = httpRequester.jsonPost(ugrydUrl, authorityRequest, Map.class, Map.class, null);
		} catch (Exception e) {
			return handleNonAckPostException(AuthorityAction.SALE, UGryTransactionType.VEND_SALE, resultAttributes, e);
		}

		log.info("Received {0} from {1}", maskMap(authorityResponse), ugrydUrl);
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA.getValue(), maskMap(authorityResponse).toString());

		BigDecimal responseCode = ConvertUtils.convertSafely(BigDecimal.class, authorityResponse.get("response_code"), null);
		String responseMessage = ConvertUtils.getStringSafely(authorityResponse.get("response_msg"), null);
		if (responseCode == null) {
			log.info("No response code found in response: {0}", maskMap(authorityResponse));
			return populateFailedResults(AuthorityAction.SALE, resultAttributes, DeniedReason.PROCESSOR_ERROR, "No response code");
		}

		if (!UGryTransactionType.VEND_SALE.isSuccess(responseCode)) {
			log.info("Received non-success response code: {0}", maskMap(authorityResponse));
			return populateDeclinedResults(AuthorityAction.SALE, resultAttributes, getDeniedReason(responseCode, responseMessage), responseMessage);
		}

		BigDecimal balance = ConvertUtils.convertSafely(BigDecimal.class, authorityResponse.get("balance"), null); // optional
		String transactionId = ConvertUtils.getStringSafely(authorityResponse.get("id"), null); // optional
		String authCode = ConvertUtils.getStringSafely(authorityResponse.get("authcode"), null); // optional but needed for ACK
		String terminalTime = ConvertUtils.getStringSafely(authorityResponse.get("terminal_time"), null); // optional
		
		Date responseTs = null;
		if (!StringUtils.isBlank(terminalTime)) {
			try {
				responseTs = authorityTimestampFormat.get().parse(terminalTime);
			} catch (Exception e) {
				log.warn("Received invalid terminal_time: {0}", terminalTime, e);
			}
		}
		
		BigDecimal approvedFundsMinor = InteractionUtils.toMinorCurrency(amount, minorCurrencyFactor);
		BigDecimal balanceMinor = balance == null ? null : InteractionUtils.toMinorCurrency(balance, minorCurrencyFactor);

		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD.getValue(), authCode);
		resultAttributes.put(AuthorityAttrEnum.ATTR_AMT_APPROVED.getValue(), approvedFundsMinor.longValueExact());
		resultAttributes.put(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT.getValue(), balanceMinor.longValueExact());
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue(), responseCode.toString());
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC.getValue(), responseMessage);
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD.getValue(), transactionId);
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS.getValue(), responseTs);

		AuthResultCode ackResult = sendAck(AuthorityAction.SALE, UGryTransactionType.VEND_SALE, authorityRequest, resultAttributes, authCode);
		if (ackResult != AuthResultCode.APPROVED)
			return ackResult;
		
		resultAttributes.put(AuthorityAttrEnum.ATTR_SETTLED.getValue(), true);
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue(), ackResult.getValue());

		return AuthResultCode.APPROVED;
	}

	// Refund is used to move funds from a merchant to a patron's cashless account. This can also be used to reverse a sale, and is required instead of
	// a void if the original transaction was performed on the prior fiscal day, and therefore already settled. Refunds must be acknowledged with an ack
	// call or they will be reversed automatically.
	protected AuthResultCode sendRefund(Map<String, Object> authorityRequest, Map<String, Object> resultAttributes, BigDecimal amount, int minorCurrencyFactor) throws ServiceException {
		authorityRequest.put("transaction_type", UGryTransactionType.REFUND.getValue());
		authorityRequest.put("amount", amountFormatLocal.get().format(amount));

		log.info("Sending {0} {1} to {2}", UGryTransactionType.REFUND, maskMap(authorityRequest), ugrydUrl);
		Map<String, Object> authorityResponse;
		try {
			authorityResponse = httpRequester.jsonPost(ugrydUrl, authorityRequest, Map.class, Map.class, null);
		} catch (Exception e) {
			return handleNonAckPostException(AuthorityAction.REFUND, UGryTransactionType.REFUND, resultAttributes, e);
		}

		log.info("Received {0} from {1}", maskMap(authorityResponse), ugrydUrl);
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA.getValue(), maskMap(authorityResponse).toString());

		BigDecimal responseCode = ConvertUtils.convertSafely(BigDecimal.class, authorityResponse.get("response_code"), null);
		String responseMessage = ConvertUtils.getStringSafely(authorityResponse.get("response_msg"), null);
		if (responseCode == null) {
			log.info("No response code found in response: {0}", maskMap(authorityResponse));
			return populateFailedResults(AuthorityAction.REFUND, resultAttributes, DeniedReason.PROCESSOR_ERROR, "No response code");
		}

		if (!UGryTransactionType.REFUND.isSuccess(responseCode)) {
			log.info("Received non-success response code: {0}", maskMap(authorityResponse));
			return populateDeclinedResults(AuthorityAction.REFUND, resultAttributes, getDeniedReason(responseCode, responseMessage), responseMessage);
		}

		BigDecimal balance = ConvertUtils.convertSafely(BigDecimal.class, authorityResponse.get("balance"), null); // optional
		String transactionId = ConvertUtils.getStringSafely(authorityResponse.get("id"), null); // optional
		String authCode = ConvertUtils.getStringSafely(authorityResponse.get("authcode"), null); // optional but needed for ACK
		String terminalTime = ConvertUtils.getStringSafely(authorityResponse.get("terminal_time"), null); // optional
		
		Date responseTs = null;
		if (!StringUtils.isBlank(terminalTime)) {
			try {
				responseTs = authorityTimestampFormat.get().parse(terminalTime);
			} catch (Exception e) {
				log.warn("Received invalid terminal_time: {0}", terminalTime, e);
			}
		}
		
		BigDecimal approvedFundsMinor = InteractionUtils.toMinorCurrency(amount, minorCurrencyFactor);
		BigDecimal balanceMinor = balance == null ? null : InteractionUtils.toMinorCurrency(balance, minorCurrencyFactor);

		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD.getValue(), authCode);
		resultAttributes.put(AuthorityAttrEnum.ATTR_AMT_APPROVED.getValue(), approvedFundsMinor.longValueExact());
		resultAttributes.put(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT.getValue(), balanceMinor.longValueExact());
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue(), responseCode.toString());
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC.getValue(), responseMessage);
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD.getValue(), transactionId);
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS.getValue(), responseTs);

		AuthResultCode ackResult = sendAck(AuthorityAction.REFUND, UGryTransactionType.REFUND, authorityRequest, resultAttributes, authCode);
		if (ackResult != AuthResultCode.APPROVED)
			return ackResult;
		
		resultAttributes.put(AuthorityAttrEnum.ATTR_SETTLED.getValue(), true);
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue(), ackResult.getValue());
		
		return AuthResultCode.APPROVED;
	}

	// Void issues a reversal of a sale, refund or tip adjust transaction. It is the preferred method for reversals, but can only be used within the same
	// fiscal day. Once a transaction has been settled, it can no longer be voided. In those situations, use sale or refund to issue the reverse operation.
	// Voids are immediate and do not require acknowledgement, though an ack can be sent and will be replied to.
	// TODO: decide when/if to send a refund instead of a void
	protected AuthResultCode sendVoid(Map<String, Object> authorityRequest, Map<String, Object> resultAttributes, String authCode, int minorCurrencyFactor) throws ServiceException {
		authorityRequest.put("transaction_type", UGryTransactionType.VOID.getValue());
		authorityRequest.put("authcode", authCode);

		log.info("Sending {0} {1} to {2}", UGryTransactionType.VOID, maskMap(authorityRequest), ugrydUrl);
		Map<String, Object> authorityResponse;
		try {
			authorityResponse = httpRequester.jsonPost(ugrydUrl, authorityRequest, Map.class, Map.class, null);
		} catch (Exception e) {
			return handleNonAckPostException(AuthorityAction.REVERSAL, UGryTransactionType.VOID, resultAttributes, e);
		}

		log.info("Received {0} from {1}", maskMap(authorityResponse), ugrydUrl);
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA.getValue(), maskMap(authorityResponse).toString());

		BigDecimal responseCode = ConvertUtils.convertSafely(BigDecimal.class, authorityResponse.get("response_code"), null);
		String responseMessage = ConvertUtils.getStringSafely(authorityResponse.get("response_msg"), null);
		if (responseCode == null) {
			log.info("No response code found in response: {0}", maskMap(authorityResponse));
			return populateFailedResults(AuthorityAction.REVERSAL, resultAttributes, DeniedReason.PROCESSOR_ERROR, "No response code");
		}

		if (!UGryTransactionType.VOID.isSuccess(responseCode)) {
			log.info("Received non-success response code: {0}", maskMap(authorityResponse));
			return populateDeclinedResults(AuthorityAction.REVERSAL, resultAttributes, getDeniedReason(responseCode, responseMessage), responseCode + ":" + responseMessage);
		}

		BigDecimal balance = ConvertUtils.convertSafely(BigDecimal.class, authorityResponse.get("balance"), null); // optional
		BigDecimal balanceMinor = balance == null ? null : InteractionUtils.toMinorCurrency(balance, minorCurrencyFactor);
		
		String transactionId = ConvertUtils.getStringSafely(authorityResponse.get("id"), null); // optional

		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD.getValue(), authCode);
		resultAttributes.put(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT.getValue(), balanceMinor.longValueExact());
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue(), responseCode.toString());
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC.getValue(), responseMessage);
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD.getValue(), transactionId);

		// ack is not required for void
		return AuthResultCode.APPROVED;
	}

	// Ack is used to acknowledge that a transaction response was received by the caller. If you perform a transaction (sale, refund, tip adjust), and do
	// not call ack with the authcode for that transaction, it will be reversed automatically by the UGryd backend.
	protected AuthResultCode sendAck(AuthorityAction action, UGryTransactionType transactionType, Map<String, Object> authorityRequest, Map<String, Object> resultAttributes, String authCode) throws ServiceException {
		Map<String, Object> ackRequest = new LinkedHashMap<>();
		ackRequest.put("transaction_type", UGryTransactionType.ACK.getValue());
		ackRequest.put("terminal", authorityRequest.get("terminal"));
		ackRequest.put("username", authorityRequest.get("username"));
		ackRequest.put("password", authorityRequest.get("password"));
		ackRequest.put("authcode", authCode);
		
		log.info("Sending {0} {1} to {2}", UGryTransactionType.ACK, maskMap(ackRequest), ugrydUrl);
		Map<String, Object> authorityResponse;
		try {
			authorityResponse = httpRequester.jsonPost(ugrydUrl, ackRequest, Map.class, Map.class, null);
		} catch (Exception e) {
			return handleAckPostException(action, transactionType, resultAttributes, e);
		}

		log.info("Received {0} from {1}", maskMap(authorityResponse), ugrydUrl);

		BigDecimal responseCode = ConvertUtils.convertSafely(BigDecimal.class, authorityResponse.get("response_code"), null);
		String responseMessage = ConvertUtils.getStringSafely(authorityResponse.get("response_msg"), null);
		if (responseCode == null) {
			log.info("No response code found in response: {0}", maskMap(authorityResponse));
			return populateFailedResults(AuthorityAction.SALE, resultAttributes, DeniedReason.PROCESSOR_ERROR, "ACK FAILED: No response code");
		}

		if (!UGryTransactionType.ACK.isSuccess(responseCode)) {
			log.info("Received non-success response code: {0}", maskMap(authorityResponse));
			return populateFailedResults(AuthorityAction.SALE, resultAttributes, DeniedReason.PROCESSOR_ERROR, format("ACK FAILED: {0}:{1}", responseCode, responseMessage));
		}

		return AuthResultCode.APPROVED;
	}
	
	protected AuthResultCode handleNonAckPostException(AuthorityAction action, UGryTransactionType transactionType, Map<String, Object> resultAttributes, Exception e) throws ServiceException {
		log.warn("{0} POST failed: {1}", transactionType, e.getMessage(), e);
		
		// UGryd automatically reverses transactions that did not receive an ack, so no reversals should be done here except for clearing the vending hold
		
		if (action == AuthorityAction.REVERSAL) {
			throw new RetrySpecifiedServiceException(e.getMessage(), WorkRetryType.SCHEDULED_RETRY);
		}
		
		DeniedReason deniedReason = getDeniedReason(e);

		if (transactionType != UGryTransactionType.VEND_INQUIRY) {
			if (action == AuthorityAction.AUTHORIZATION) {
				return populateFailedResults(action, resultAttributes, deniedReason, e.getMessage());
			} else {
				// return declined on a failed sale/refund rather than failed to make POSM do retries
				return populateDeclinedResults(action, resultAttributes, deniedReason, e.getMessage());
			} 
		}
		
		// failed vend inquiries should send a $0 sale to clear the hold if possible
		
		Throwable cause = e;
		if (e instanceof HttpException) {
			Throwable httpCause = ((HttpException)e).getCause();
			if (httpCause != null)
				cause = httpCause;
		}
		
		// categorize exceptions based on the possibility that the server received it, so we can decide
		if (cause instanceof ConnectException || cause instanceof SocketException) {
			log.error("Caught connection exception: clearing auth hold will not be attempted.", e);
		} else if (cause instanceof SocketTimeoutException || cause instanceof NoHttpResponseException || cause instanceof IOException || cause instanceof JsonSyntaxException) {
			throw new RetrySpecifiedServiceException(e.getMessage(), WorkRetryType.IMMEDIATE_RETRY);
		} else {
			log.error("Caught unhandled exception: clearing auth hold will not be attempted. Handling should be added for this exception!", e);
		}
		
		return populateFailedResults(action, resultAttributes, deniedReason, e.getMessage());
	}
	
	// this idea here is to map these to something that is in translations.properties so the user gets some meaningful feedback
	protected DeniedReason getDeniedReason(Exception e) {
		if (e instanceof ConnectException || e instanceof SocketException) {
			return DeniedReason.PROCESSOR_ERROR;
		} else if (e instanceof SocketTimeoutException || e instanceof InterruptedIOException || e instanceof TimeoutException) {
			return DeniedReason.PROCESSOR_TIMEOUT;
		} else if (e instanceof NoHttpResponseException || e instanceof IOException || e instanceof JsonSyntaxException) {
			// there's no translations for PROCESSOR_RESPONSE
			//return DeniedReason.PROCESSOR_RESPONSE;
			return DeniedReason.PROCESSOR_ERROR;
		} else {
			return DeniedReason.AUTH_LAYER_PROCESSING_ERROR;
		}
	}
	
	// this idea here is to map these to something that is in translations.properties so the user gets some meaningful feedback
	protected DeniedReason getDeniedReason(BigDecimal responseCode, String responseMessage) {
		if (responseMessage != null) {
			String s = responseMessage.toUpperCase();
			if (s.contains("TERMINAL UNKNOWN"))
				return DeniedReason.CONFIGURATION_ISSUE;
			if (s.contains("LOW BALANCE"))
				return DeniedReason.INSUFFICIENT_FUNDS;
			if (s.contains("NOT ON SYSTEM"))
				return DeniedReason.NO_ACCOUNT;
			if (s.contains("PATRON DATA NOT FOUND"))
				return DeniedReason.NO_ACCOUNT;
			if (s.contains("CARD LOST"))
				return DeniedReason.NO_ACCOUNT;
			if (s.contains("CONN ERROR"))
				return DeniedReason.PROCESSOR_ERROR;
			if (s.contains("INVALID SEARCH PATH"))
				return DeniedReason.PROCESSOR_ERROR;
			if (s.contains("INVALID MESSAGE CONFIGURATION"))
				return DeniedReason.CONFIGURATION_ISSUE;
			if (s.contains("ACCOUNT EXPIRED"))
				return DeniedReason.NOT_ACTIVE;
			if (s.contains("LOST CARD"))
				return DeniedReason.NO_ACCOUNT;
			
			log.warn("Received unhandled response message: {0}. Handling should be added for this message!");
		}
		return DeniedReason.PROCESSOR_RESPONSE; // processor response should return a generic decline message
	}
	
	protected AuthResultCode handleAckPostException(AuthorityAction action, UGryTransactionType transactionType, Map<String, Object> resultAttributes, Throwable e) throws ServiceException {
		log.warn("ACK of {0} POST failed: {1}", action, e.getMessage(), e);

		// IMPORTANT: UGryd automatically reverses transactions that did not receive an ack. This makes handling somewhat easier since then we don't have to
		// deal with reversals if no ack has been sent.  However if we get to this stage and send the ack, but that fails, then we should do reversals 
		// because it's possible the transaction went through but we didn't get a response.  However, at this point I'm not doing reversals, I'm just going
		// to return failed so that sales goes into an INCOMPLETE_ERROR state in POSM, for manual intervention. I'm hoping the chance of sending a successfull
		// sale but then getting a failed ACK is very low.
		
		return populateFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Unhandled HTTP POST case: " + e.getMessage());
	}

	// This should generate sequential numbers partitioned between the number of instances and unique to this instance
	// Start with a random number between restarts
	protected synchronized int getNextSequenceId() {
		if (seqIdCounter < 0) {
			int seqCount = (int)Math.floor(MAX_SEQUENCE_ID / instanceCount);
			seqMin = seqCount * (instance - 1) + 1;
			seqMax = (seqMin + seqCount) - 1;
			seqIdCounter = new Random().nextInt(seqCount + 1) + seqMin;
			log.info("Starting with sequenceId {0}", seqIdCounter);
		} else if (++seqIdCounter > seqMax) {
			seqIdCounter = seqMin;
			log.info("Rolled over to sequenceId {0}", seqIdCounter);
		}
		return seqIdCounter;
	}

	protected static Map<String, Object> maskMap(Map<String, Object> payload) {
		String[] toMask = {"account_number", "username", "password", "first_name", "middle_name", "last_name"};
		Map<String, Object> masked = new HashMap<>(payload);
		for(String key : toMask) {
			String value = ConvertUtils.getStringSafely(masked.get(key), null);
			if (value != null) {
				masked.put(key, new MaskedString(value).toString());
			}
		}
		return masked;
	}

	public int getSeqIdCounter() {
		return seqIdCounter;
	}

	public int getSeqMin() {
		return seqMin;
	}

	public int getSeqMax() {
		return seqMax;
	}

	public HttpRequester getHttpRequester() {
		return httpRequester;
	}

	public void setHttpRequester(HttpRequester httpRequester) {
		this.httpRequester = httpRequester;
	}

	public int getInstance() {
		return instance;
	}

	public void setInstance(int instance) {
		this.instance = instance;
	}

	public int getInstanceCount() {
		return instanceCount;
	}

	public void setInstanceCount(int instanceCount) {
		this.instanceCount = instanceCount;
	}

	public String[] getSslProtocols() {
		return sslProtocols;
	}

	public void setSslProtocols(String[] sslProtocols) {
		this.sslProtocols = sslProtocols;
	}

	public HostnameVerifier getHostNameVerifier() {
		return hostNameVerifier;
	}

	public void setHostNameVerifier(HostnameVerifier hostNameVerifier) {
		this.hostNameVerifier = hostNameVerifier;
	}

	public int getConnectionTimeout() {
		return connectionTimeout;
	}

	public void setConnectionTimeout(int connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}

	public int getMaxHostConnections() {
		return maxHostConnections;
	}

	public void setMaxHostConnections(int maxHostConnections) {
		this.maxHostConnections = maxHostConnections;
	}

	public int getMaxTotalConnections() {
		return maxTotalConnections;
	}

	public void setMaxTotalConnections(int maxTotalConnections) {
		this.maxTotalConnections = maxTotalConnections;
	}

	public int getSocketTimeout() {
		return socketTimeout;
	}

	public void setSocketTimeout(int socketTimeout) {
		this.socketTimeout = socketTimeout;
	}

	public String getUgrydUrl() {
		return ugrydUrl;
	}

	public void setUgrydUrl(String ugrydUrl) {
		this.ugrydUrl = ugrydUrl;
	}

	public String getUgrydUsername() {
		return ugrydUsername;
	}

	public void setUgrydUsername(String ugrydUsername) {
		this.ugrydUsername = ugrydUsername;
	}

	public String getUgrydPassword() {
		return ugrydPassword;
	}

	public void setUgrydPassword(String ugrydPassword) {
		this.ugrydPassword = ugrydPassword;
	}

	public BigDecimal getMinimumBalanceAmount() {
		return minimumBalanceAmount;
	}

	public void setMinimumBalanceAmount(BigDecimal minimumBalanceAmount) {
		this.minimumBalanceAmount = minimumBalanceAmount;
	}
	
	public boolean isPadTerminalCd() {
		return padTerminalCd;
	}
	
	public void setPadTerminalCd(boolean padTerminalCd) {
		this.padTerminalCd = padTerminalCd;
	}
	
	public String[] getDoNotPadTerminalCds() {
		return doNotPadTerminalCds.stream().toArray(s -> new String[s]);
	}
	
	public void setDoNotPadTerminalCds(String[] doNotPadTerminalCds) {
		this.doNotPadTerminalCds = new HashSet<>(Arrays.asList(doNotPadTerminalCds));
	}
	
	public boolean isUseDeviceSerialCdForTerminalCd() {
		return useDeviceSerialCdForTerminalCd;
	}
	
	public void setUseDeviceSerialCdForTerminalCd(boolean useDeviceSerialCdForTerminalCd) {
		this.useDeviceSerialCdForTerminalCd = useDeviceSerialCdForTerminalCd;
	}

}
