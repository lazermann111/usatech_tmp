package com.usatech.authoritylayer;

import java.io.IOException;

import simple.app.ServiceException;
import simple.ftp.SimpleFtpsBean;

import com.usatech.ftps.EncryptType;

import ftp.FtpException;

public class FtpSite {
	protected String host;
	protected String username;
	protected String password;
	protected int port = 21;
    protected int socketTimeout = 30*1000;
	protected int readTimeout = 30 * 1000;
    protected EncryptType encryptType = EncryptType.SSL_REQUIRE;
    protected String remoteDir;
       
    public SimpleFtpsBean connect() throws ServiceException, IOException, FtpException {
    	String host = getHost();
        int port = getPort();
        String username = getUsername();
        String password = getPassword();
        if(host == null)
        	throw new ServiceException("Property 'host' is not configured");
        // Make a client connection
        SimpleFtpsBean ftpsBean = new SimpleFtpsBean();
		ftpsBean.setEncryptType(getEncryptType());
		ftpsBean.setHostVerifying(true);
		ftpsBean.setPort(port);
        ftpsBean.setSocketTimeout(getSocketTimeout());
		ftpsBean.setReadTimeout(getReadTimeout());
        ftpsBean.ftpConnect(host, username, password);   
        String rd = getRemoteDir();
        if(rd != null && (rd=rd.trim()).length() > 0)
        	ftpsBean.setDirectory(rd);
        return ftpsBean;
    }
	
	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRemoteDir() {
		return remoteDir;
	}

	public void setRemoteDir(String remoteDir) {
		this.remoteDir = remoteDir;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getSocketTimeout() {
		return socketTimeout;
	}

	public void setSocketTimeout(int socketTimeout) {
		this.socketTimeout = socketTimeout;
	}

	public EncryptType getEncryptType() {
		return encryptType;
	}

	public void setEncryptType(EncryptType encryptType) {
		this.encryptType = encryptType;
	}

	public int getReadTimeout() {
		return readTimeout;
	}

	public void setReadTimeout(int readTimeout) {
		this.readTimeout = readTimeout;
	}
}
