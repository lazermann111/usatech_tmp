package com.usatech.authoritylayer;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Map;

import org.apache.axis2.transport.http.HTTPConstants;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.text.StringUtils;

import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.vzm2p.M2P_USAT_Payment_ProcessingStub;
import com.usatech.vzm2p.M2P_USAT_Payment_ProcessingStub.M2P_CreditCard_ProxyType_type1;
import com.usatech.vzm2p.M2P_USAT_Payment_ProcessingStub.M2P_Transaction_Activity_type1;
import com.usatech.vzm2p.M2P_USAT_Payment_ProcessingStub.M2P_Transaction_Record;
import com.usatech.vzm2p.M2P_USAT_Payment_ProcessingStub.M2P_Transaction_Request;
import com.usatech.vzm2p.M2P_USAT_Payment_ProcessingStub.M2P_Transaction_Response;
import com.usatech.vzm2p.M2P_USAT_Payment_ProcessingStub.M2P_Transaction_ResponseE;
/**
 * The task handles uploading transactions to Verizon M2P service for loyalty functionality
 *
 */
public class VZM2PTask implements MessageChainTask {
	private static final Log log = Log.getLog();
			
	protected static String vzm2pLoyaltyUrl = null;
	protected static String vzm2pVersion = "0.0.1";
	protected static int vzm2pConnectionTimeout = 3000;
	protected static int vzm2pSocketTimeout = 30000;

	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		if (StringUtils.isBlank(vzm2pLoyaltyUrl)) {
			log.warn("vzm2pLoyaltyUrl is not configured, exiting");
		}
		try{
			Map<String, Object> attributes = taskInfo.getStep().getAttributes();
			Calendar authTs = Calendar.getInstance();
			authTs.setTimeInMillis(ConvertUtils.getLong(attributes.get("authTs")));
			attributes.put("authTs", authTs);
			log.info(StringUtils.objectToString(attributes, "Sending request to M2P server: "));
			M2P_Transaction_Request requestMessage = new M2P_Transaction_Request();
			M2P_Transaction_Record request = new M2P_Transaction_Record();
			request.setM2P_Transaction_Activity(M2P_Transaction_Activity_type1.Force);
			request.setM2P_Consumer_Identifier(ConvertUtils.getStringSafely(attributes.get("consumerAcctToken"), ""));
			request.setM2P_CreditCard_ProxyId("*" + ConvertUtils.getString(attributes.get("lastFourDigits"), true));
			request.setM2P_CreditCard_ProxyType(M2P_CreditCard_ProxyType_type1.Last4Digits);
			request.setM2P_Transaction_Currency(ConvertUtils.getString(attributes.get("currencyCd"), true));
			request.setM2P_Transaction_Identifier(ConvertUtils.getString(attributes.get("reportTranId"), true));
			request.setM2P_Transaction_Source(ConvertUtils.getString(attributes.get("eportSerialNum"), true));
			request.setM2P_Transaction_Timestamp(authTs);
			request.setM2P_Transaction_Value(new BigDecimal(ConvertUtils.getString(attributes.get("saleAmount"), true)));
			request.setM2P_Version(vzm2pVersion);
			requestMessage.setM2P_Transaction_Request(request);
			M2P_USAT_Payment_ProcessingStub vzm2p = new M2P_USAT_Payment_ProcessingStub(vzm2pLoyaltyUrl);
			vzm2p._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT, vzm2pConnectionTimeout);
    		vzm2p._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, vzm2pSocketTimeout);
			M2P_Transaction_ResponseE responseMessage = vzm2p.m2P_Transaction_Request(requestMessage);
			M2P_Transaction_Response response = responseMessage.getM2P_Transaction_Response();
			log.info(StringUtils.objectToString(response, "Received response from M2P server: "));
			return 0;
		}catch(Exception e){
			throw new ServiceException("Error uploading sale to M2P server", e);
		}
	}

	public static String getVzm2pLoyaltyUrl() {
		return vzm2pLoyaltyUrl;
	}

	public static void setVzm2pLoyaltyUrl(String vzm2pLoyaltyUrl) {
		VZM2PTask.vzm2pLoyaltyUrl = vzm2pLoyaltyUrl;
	}

	public static String getVzm2pVersion() {
		return vzm2pVersion;
	}

	public static void setVzm2pVersion(String vzm2pVersion) {
		VZM2PTask.vzm2pVersion = vzm2pVersion;
	}

	public static int getVzm2pConnectionTimeout() {
		return vzm2pConnectionTimeout;
	}

	public static void setVzm2pConnectionTimeout(int vzm2pConnectionTimeout) {
		VZM2PTask.vzm2pConnectionTimeout = vzm2pConnectionTimeout;
	}

	public static int getVzm2pSocketTimeout() {
		return vzm2pSocketTimeout;
	}

	public static void setVzm2pSocketTimeout(int vzm2pSocketTimeout) {
		VZM2PTask.vzm2pSocketTimeout = vzm2pSocketTimeout;
	}
}
