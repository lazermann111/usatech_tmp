/**
 *
 */
package com.usatech.authoritylayer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.Format;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.AuthInsertTask;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAction;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.DeniedReason;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.Language;
import com.usatech.layers.common.constants.PaymentMaskBRef;
import com.usatech.layers.common.util.StringHelper;

import simple.app.Publisher;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DbUtils;
import simple.db.NotEnoughRowsException;
import simple.io.ByteInput;
import simple.io.Log;
import simple.results.BeanException;
import simple.results.Results;
import simple.security.SecureHash;
import simple.text.StringUtils;
import simple.util.CollectionUtils;

/**
 * @author Brian S. Krug
 *
 */
public class InternalAuthorityTask extends AbstractAuthorityTask {
	private static final Log log = Log.getLog();
	protected static final MathContext TRUNC = new MathContext(0,RoundingMode.DOWN);
	protected static final int SERVICE_TYPE_BALANCE_BASED = 1;
	protected static final int SERVICE_TYPE_PSEUDO_PRIVILEGE_BASED = 3;
	protected static final int SERVICE_TYPE_HIDDEN_BALANCE_BASED = 4;

	protected float authHoldDays = 30.0f;
	protected AuthInsertTask authInsertTask;
	protected String authInsertQueueKey;
	protected String authStoreQueueKey;
	protected int[] consumerAcctTypeIds;
	protected String registrationUrl = "prepaid.usatech.com";
	protected String updateAccountQueueKey;
	protected short[] instances;
	protected String authorityTask = getClass().getSimpleName().replace("AuthorityTask", "");
	
	public static final int CONSUMER_ACCT_TYPE_DRIVER = 1;
	public static final int CONSUMER_ACCT_TYPE_GIFT = 2;
	public static final int CONSUMER_ACCT_TYPE_EXTERNAL = 4;

	protected static class InternalActionResult {
		protected final AuthResultCode result;
		protected final String message;
		protected final BigDecimal appliedAmount;

		public InternalActionResult(AuthResultCode result, String message, BigDecimal appliedAmount) {
			super();
			this.result = result;
			this.message = message;
			this.appliedAmount = appliedAmount;
		}

		public AuthResultCode getResult() {
			return result;
		}

		public String getMessage() {
			return message;
		}

		public BigDecimal getAppliedAmount() {
			return appliedAmount;
		}
	}

	public InternalAuthorityTask() {
		super("internal");
	}

	protected InternalAuthorityTask(String clientTextKeyPrefix) {
		super(clientTextKeyPrefix);
	}

	@Override
	protected AuthResultCode processRequest(AuthorityAction action, AuthorityAction subAction, final MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		Map<String, Object> resultAttributes = step.getResultAttributes();
		BigInteger amount;
		String currencyCd;
		int serviceType;
		int minorCurrencyFactor;
		EntryType entryType;
		int consumerAcctTypeId = 0;
		try {
			amount = getAttribute(step, AuthorityAttrEnum.ATTR_AMOUNT, BigInteger.class, true);
			currencyCd = getAttribute(step, AuthorityAttrEnum.ATTR_CURRENCY_CD, String.class, true);
			serviceType = getAttribute(step, AuthorityAttrEnum.ATTR_SERVICE_TYPE, Integer.class, true);
			minorCurrencyFactor = getAttribute(step, AuthorityAttrEnum.ATTR_MINOR_CURRENCY_FACTOR, Integer.class, true);
			entryType = getAttribute(step, AuthorityAttrEnum.ATTR_ENTRY_TYPE, EntryType.class, true);
		} catch(AuthorityAttributeConversionException e) {
			return AuthResultCode.FAILED;
		}
		switch(action) {
			case AUTHORIZATION:
				AccountData accountData = new AccountData(step.getAttributes());
				long posPtaId;
				long authTime;
				long globalAccountId;
				Boolean trackCrcMatches;
				boolean balanceCheckOnly;
				boolean allowPartialAuth;
				try {
					posPtaId = getAttribute(step, AuthorityAttrEnum.ATTR_POS_PTA_ID, Long.class, true);
					authTime = getAttribute(step, AuthorityAttrEnum.ATTR_AUTH_TIME, Long.class, true);
					globalAccountId = getAttributeDefault(step, AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, Long.class, 0L);
					trackCrcMatches = getAttribute(step, AuthorityAttrEnum.ATTR_TRACK_CRC_MATCH, Boolean.class, false);
					balanceCheckOnly = getAttributeDefault(step, AuthorityAttrEnum.ATTR_BALANCE_CHECK_ONLY, Boolean.class, false);
					allowPartialAuth = getAttributeDefault(step, AuthorityAttrEnum.ATTR_PARTIAL_AUTH_ALLOWED, Boolean.class, true);
				} catch(AuthorityAttributeConversionException e) {
					return AuthResultCode.FAILED;
				}
				return authorize(action, balanceCheckOnly, serviceType, posPtaId, globalAccountId, trackCrcMatches, accountData, authTime, amount, currencyCd, minorCurrencyFactor, taskInfo, resultAttributes, entryType, allowPartialAuth);
			case SALE:
				Number tranId;
				try {
					tranId = getAttribute(step, AuthorityAttrEnum.ATTR_TRAN_ID, Number.class, true);
					consumerAcctTypeId = getAttributeDefault(step, AuthorityAttrEnum.ATTR_CONSUMER_ACCT_TYPE_ID, Integer.class, 0);
				} catch(AuthorityAttributeConversionException e) {
					return AuthResultCode.FAILED;
				}
				if(amount.signum() == 0)
					return removeAuthHold(action, tranId, resultAttributes);
				return debit(action, serviceType, tranId, amount, currencyCd, minorCurrencyFactor, taskInfo.isRedelivered(), resultAttributes, taskInfo.getPublisher(), consumerAcctTypeId);
			case REFUND:
				try {
					tranId = getAttribute(step, AuthorityAttrEnum.ATTR_TRAN_ID, Number.class, true);
				} catch(AuthorityAttributeConversionException e) {
					return AuthResultCode.FAILED;
				}
				if(amount.signum() == 0)
					return removeAuthHold(action, tranId, resultAttributes);
				return credit(action, serviceType, tranId, amount, currencyCd, minorCurrencyFactor, taskInfo.isRedelivered(), resultAttributes);
			case ADJUSTMENT:
				BigInteger originalAmount;
				try {
					tranId = getAttribute(step, AuthorityAttrEnum.ATTR_TRAN_ID, Number.class, true);
					originalAmount = getAttribute(step, AuthorityAttrEnum.ATTR_ORIGINAL_AMOUNT, BigInteger.class, true);
				} catch(AuthorityAttributeConversionException e) {
					return AuthResultCode.FAILED;
				}
				switch(subAction) {
					case SALE:
						BigInteger adjustAmount = amount.subtract(originalAmount);
						switch(adjustAmount.signum()) {
							case -1: // needs credit
								return credit(action, serviceType, tranId, adjustAmount.abs(), currencyCd, minorCurrencyFactor, taskInfo.isRedelivered(), resultAttributes);
							case 0: // nothing to do
								resultAttributes.put("authResultCd", 'Y');
								resultAttributes.put("authorityRespCd", 0);
								String text = "No change to account";
								resultAttributes.put("authorityRespDesc", text);
								resultAttributes.put("settled", true);
								return AuthResultCode.APPROVED;
							case 1: // needs debit
								return debit(action, serviceType, tranId, adjustAmount, currencyCd, minorCurrencyFactor, taskInfo.isRedelivered(), resultAttributes, taskInfo.getPublisher(), consumerAcctTypeId);
							default:
								throw new RetrySpecifiedServiceException("Illegal State: BigInteger.signum() returned " + adjustAmount.signum(),WorkRetryType.NO_RETRY);
						}
					case REFUND:
						adjustAmount = amount.subtract(originalAmount);
						switch(adjustAmount.signum()) {
							case -1: // needs debit
								return debit(action, serviceType, tranId, adjustAmount.abs(), currencyCd, minorCurrencyFactor, taskInfo.isRedelivered(), resultAttributes, taskInfo.getPublisher(), consumerAcctTypeId);
							case 0: // nothing to do
								resultAttributes.put("authResultCd", 'Y');
								resultAttributes.put("authorityRespCd", 0);
								String text = "No change to account";
								resultAttributes.put("authorityRespDesc", text);
								resultAttributes.put("settled", true);
								return AuthResultCode.APPROVED;
							case 1: // needs credit
								return credit(action, serviceType, tranId, adjustAmount, currencyCd, minorCurrencyFactor, taskInfo.isRedelivered(), resultAttributes);
							default:
								throw new RetrySpecifiedServiceException("Illegal State: BigInteger.signum() returned " + adjustAmount.signum(),WorkRetryType.NO_RETRY);
						}
					default:
						throw new RetrySpecifiedServiceException("Sub-action '" + subAction + "' for Action '" + action + "' NOT implemented",WorkRetryType.NO_RETRY);
				}
			default:
				throw new RetrySpecifiedServiceException("Action '" + action + "' NOT implemented",WorkRetryType.NO_RETRY);
		}
	}

	@Override
	protected AuthResultCode processReversal(AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		int serviceType;
		try {
			serviceType = getAttribute(step, AuthorityAttrEnum.ATTR_SERVICE_TYPE, Integer.class, true);
		} catch(AuthorityAttributeConversionException e) {
			log.warn("Reversal of Action '" + action + "' failed", e);
			return AuthResultCode.FAILED;
		}
		switch(serviceType) {
			case SERVICE_TYPE_BALANCE_BASED:
			case SERVICE_TYPE_HIDDEN_BALANCE_BASED:
			case SERVICE_TYPE_PSEUDO_PRIVILEGE_BASED:
				switch(action) {
					case AUTHORIZATION:
						try {
							step.getAttributes().put("consumerAcctTypeIds", getConsumerAcctTypeIds());
							Results results = DataLayerMgr.executeQuery("FIND_AUTH_HOLD", step.getAttributes());
							if(results.next()) {
								log.info("Found authId to ignore during re-attempting");
								step.getAttributes().put("ignoreAuthId", results.getValue("ignoreAuthId"));
							} else
								log.info("Did not find authId to ignore during re-attempting");
							return AuthResultCode.APPROVED;
						} catch(SQLException e) {
							log.warn("Error while finding auth hold on consumer acct from database", e);
							return AuthResultCode.FAILED;
						} catch(DataLayerException e) {
							log.warn("Error while finding auth hold on consumer acct from database", e);
							return AuthResultCode.FAILED;
						}
					case SALE: // we handle reentry on sales now
						log.info("Allowing retry of sale");
						return AuthResultCode.APPROVED;
					default:
						log.warn("Reversal of Action '" + action + "' NOT implemented");
						return AuthResultCode.FAILED;
				}
			default:
				log.error("Invalid Authority Service Type " + serviceType);
				populateFailedResults(action, step.getResultAttributes(), DeniedReason.CONFIGURATION_ISSUE, "Invalid Authority Service Type " + serviceType);
				return AuthResultCode.FAILED;
		}
	}

	protected AuthResultCode authorize(AuthorityAction action, boolean balanceCheckOnly, int serviceType, long posPtaId, long globalAccountId, Boolean trackCrcMatches, AccountData accountData, long authTime, BigInteger amount, String currencyCd, int minorCurrencyFactor, MessageChainTaskInfo taskInfo, Map<String, Object> resultAttributes, EntryType entryType, boolean allowPartialAuth) {
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("posPtaId", posPtaId);
		params.put("globalAccountId", globalAccountId);
		params.put("authTime", authTime);
		params.put("currencyCd", currencyCd);
		params.put("authorityTask", authorityTask);
		if(accountData != null && CollectionUtils.iterativeSearch(consumerAcctTypeIds, 4) >= 0) {
			String pan = accountData.getPartData(PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER);
			if(!StringUtils.isBlank(pan) && !pan.equals(MessageResponseUtils.getCardNumber(accountData.getTrackData()))) {
				params.put("consumerAcctCd", pan);
			}
		}
		
		try {
			Language deviceLanguage = getAttribute(taskInfo.getStep(), CommonAttrEnum.ATTR_LANGUAGE, Language.class, false);
			if (deviceLanguage.toString() == null) {deviceLanguage = Language.ENGLISH;}
			language = deviceLanguage;
		} catch (AuthorityAttributeConversionException e2) {
			language = Language.ENGLISH;
		}
				
		AuthResultCode authResultCode;		
		switch(serviceType) {
			case SERVICE_TYPE_BALANCE_BASED: case SERVICE_TYPE_HIDDEN_BALANCE_BASED: case SERVICE_TYPE_PSEUDO_PRIVILEGE_BASED:
				// Get consumerAcctId and available balance
				try {
					DataLayerMgr.selectInto("GET_CONSUMER_ACCT_DETAILS", params);
				} catch(NotEnoughRowsException e) {
					return populateDeclinedResults(action, resultAttributes, DeniedReason.NO_ACCOUNT, "Invalid Account"); // "Could not find active account or account is inaccessible by this device";
				} catch(SQLException e) {
					log.warn("Error while getting consumer acct from database", e);
					populateFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Database Error: " + e.getMessage());
					return AuthResultCode.FAILED;
				} catch(DataLayerException e) {
					log.warn("Error while getting consumer acct from database", e);
					populateFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "DataLayer Error: " + e.getMessage());
					return AuthResultCode.FAILED;
				} catch(BeanException e) {
					log.warn("Error while getting consumer acct from database", e);
					populateFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Bean Error: " + e.getMessage());
					return AuthResultCode.FAILED;
				}
				long consumerAcctId;
				int consumerAcctTypeId;
				long realGlobalAccountId;
				try {
					consumerAcctId = ConvertUtils.getLong(params.get("consumerAcctId"));
					consumerAcctTypeId = ConvertUtils.getInt(params.get("consumerAcctTypeId"));
					realGlobalAccountId = ConvertUtils.getLong(params.get("globalAccountId"));
				} catch(ConvertException e) {
					log.warn("Error while getting consumer acct from database", e);
					return populateFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Conversion Error: " + e.getMessage());
				}
				resultAttributes.put("consumerAcctId", consumerAcctId);
				resultAttributes.put("consumerId", params.get("consumerId"));
				resultAttributes.put("consumerAcctTypeLabel", params.get("consumerAcctTypeLabel"));
				if(consumerAcctTypeId == 4) {
					if(realGlobalAccountId != globalAccountId)
						resultAttributes.put("globalAccountId", realGlobalAccountId);
					trackCrcMatches = null; // skip track match check
				}
				switch(entryType) {
					case TOKEN:
					case VAS:
						break; // if we got this far then we validated the token and that's enough
					case MANUAL:
					case CARD_ID_CREDENTIALS:
						String expirationDate = accountData.getPartData(PaymentMaskBRef.EXPIRATION_DATE);
						String securityCd = accountData.getPartData(PaymentMaskBRef.DISCRETIONARY_DATA);
						String postal = accountData.getPartData(PaymentMaskBRef.ZIP_CODE);
						if(StringUtils.isBlank(expirationDate) || StringUtils.isBlank(securityCd) || StringUtils.isBlank(postal)) {
							log.warn("Empty expiration date, security code or postal code");
							return populateFailedResults(action, resultAttributes, DeniedReason.MISSING_INPUT, "Empty expiration date, security code or postal code");
						}
						if(!expirationDate.equals(params.get("expirationDate"))) {
							log.info("Incorrect expiration date");
							return populateDeclinedResults(action, resultAttributes, DeniedReason.INVALID_VERIFICATION_DATA, "Incorrect expiration date");
						}
						if(!postal.equals(params.get("consumerPostalCd"))) {
							log.info("Incorrect postal code");
							return populateDeclinedResults(action, resultAttributes, DeniedReason.AVS_MISMATCH, "Incorrect postal code");
						}
						byte[] securityCdHash = ConvertUtils.convertSafely(byte[].class, params.get("securityCdHash"), null);
						byte[] securityCdSalt = ConvertUtils.convertSafely(byte[].class, params.get("securityCdSalt"), null);
						if(securityCdHash != null && securityCdSalt != null) {
							byte[] hash;
							try {
								hash = SecureHash.getHash(securityCd.getBytes(), securityCdSalt);
							} catch(NoSuchAlgorithmException e) {
								log.error("Error calculating security code hash", e);
								return populateFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Error calculating security code hash: " + e.getMessage());
							}
							if(!Arrays.equals(securityCdHash, hash)) {
								log.info("Incorrect security code");
								return populateDeclinedResults(action, resultAttributes, DeniedReason.CVV_MISMATCH, "Incorrect security code");
							}
						}
						break;
					default:
						if(trackCrcMatches == null || trackCrcMatches.booleanValue()) {
							String trackData = accountData.getTrackData();
							if(!StringHelper.isBlank(trackData)) {
								switch(consumerAcctTypeId) {
									case 1:
									case 2:
									case 3:
									case 7:
										byte[] consumerAcctRawHash = ConvertUtils.convertSafely(byte[].class, params.get("consumerAcctRawHash"), null);
										byte[] consumerAcctRawSalt = ConvertUtils.convertSafely(byte[].class, params.get("consumerAcctRawSalt"), null);
										boolean alreadyHashed = consumerAcctRawHash != null && consumerAcctRawSalt != null;
										if(!alreadyHashed) {
											try {
												consumerAcctRawSalt = SecureHash.getSalt();
											} catch(NoSuchAlgorithmException e) {
												return populateFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Error generating salt: " + e.getMessage());
											} catch(NoSuchProviderException e) {
												return populateFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Error generating salt: " + e.getMessage());
											}
										}
										byte[] rawHash;
									try {
											rawHash = SecureHash.getHash(trackData.getBytes(), consumerAcctRawSalt);
									} catch(NoSuchAlgorithmException e) {
											log.error("Error calculating raw hash", e);
											return populateFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Error calculating raw hash: " + e.getMessage());
									}
										if(alreadyHashed) {
											if(!Arrays.equals(consumerAcctRawHash, rawHash)) {
												log.info("Invalid track data, raw hash data did not match");
												return populateDeclinedResults(action, resultAttributes, DeniedReason.INVALID_VERIFICATION_DATA, "Invalid track data, raw hash data did not match");
											}
										} else {
											Map<String, Object> updateParams = new HashMap<String, Object>();
											updateParams.put("consumerAcctRawHash", rawHash);
											updateParams.put("consumerAcctRawSalt", consumerAcctRawSalt);
											updateParams.put("consumerAcctRawAlg", SecureHash.HASH_ALGORITHM_ITERS);
											updateParams.put("consumerAcctId", consumerAcctId);
											try {
												DataLayerMgr.executeUpdate("UPDATE_CONSUMER_ACCT_RAW_HASH", updateParams, true);
											} catch(SQLException e) {
												return populateFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Error updating raw hash: " + e.getMessage());
											} catch(DataLayerException e) {
												return populateFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Error updating raw hash: " + e.getMessage());
											}
									}
										String baseQueueName = getUpdateAccountQueueKey();
										if(baseQueueName != null) {
											baseQueueName += '.';
											MessageChain mc = new MessageChainV11();
											MessageChainStep broadcastStep = mc.addStep(baseQueueName);
											broadcastStep.setAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, globalAccountId);
											broadcastStep.setAttribute(AuthorityAttrEnum.ATTR_TRACK_CRC, InteractionUtils.getCRC16(trackData));
											if(trackCrcMatches == null)
												broadcastStep.setAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, AuthResultCode.APPROVED); // To cause update of track CRC
											for(short i : getInstances()) {
												broadcastStep.setQueueKey(baseQueueName + i);
												try {
													MessageChainService.publish(broadcastStep.getMessageChain(), taskInfo.getPublisher(), taskInfo.getEncryptionInfoMapping(), null);
												} catch(ServiceException e) {
													log.warn("Could not broadcast new trackCrc to the keymgrs", e);
												}
										}
									}
										break;
								}
							} else {
								// what to do here?

							}
						} else {
							return populateDeclinedResults(action, resultAttributes, DeniedReason.INVALID_VERIFICATION_DATA, "Invalid card");

					}
				}

				if(balanceCheckOnly) {
					BigDecimal consumerBalance;
					BigDecimal onHoldAmount;
					try {
						DataLayerMgr.selectInto("GET_AUTH_HOLD_TOTAL", params);
						consumerBalance = ConvertUtils.convertRequired(BigDecimal.class, params.get("consumerAcctBalance"));
						onHoldAmount = ConvertUtils.convertRequired(BigDecimal.class, params.get("onHoldAmount"));
					} catch(SQLException e) {
						log.warn("Error while getting auth hold info from database", e);
						populateAuthFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Database Error: " + e.getMessage());
						return AuthResultCode.FAILED;
					} catch(DataLayerException e) {
						log.warn("Error while getting auth hold info from database", e);
						populateAuthFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Database Error: " + e.getMessage());
						return AuthResultCode.FAILED;
					} catch(BeanException e) {
						log.warn("Error while getting auth hold info from database", e);
						populateAuthFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Database Error: " + e.getMessage());
						return AuthResultCode.FAILED;
					} catch(ConvertException e) {
						log.warn("Error while converting balance to BigDecimal", e);
						populateAuthFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Data Conversion Error");
						return AuthResultCode.FAILED;
					}
					Format format = getNumberFormat();
					BigDecimal availableBalance = consumerBalance.subtract(onHoldAmount);
					resultAttributes.put("authResultCd", 'Y');
					resultAttributes.put("authorityRespCd", 0);
					String text = "Available balance: " + format.format(availableBalance) + ", auth hold: " + format.format(onHoldAmount);
					resultAttributes.put("authorityRespDesc", text);
					setClientText(resultAttributes, AuthResultCode.APPROVED, null);
					BigInteger availableBalanceMinor = availableBalance.multiply(BigDecimal.valueOf(minorCurrencyFactor), TRUNC).toBigInteger();
					resultAttributes.put("balanceAmount", availableBalanceMinor);
					resultAttributes.put("firstName", ConvertUtils.getStringSafely(params.get("firstName"), ""));
					resultAttributes.put("lastName", ConvertUtils.getStringSafely(params.get("lastName"), ""));
					resultAttributes.put("email", ConvertUtils.getStringSafely(params.get("email"), ""));
					resultAttributes.put("sendReceipt", ConvertUtils.getStringSafely(params.get("sendReceipt"), ""));
					resultAttributes.put("mobileNumber", ConvertUtils.getStringSafely(params.get("mobileNumber"), ""));
					resultAttributes.put("mobileEmail", ConvertUtils.getStringSafely(params.get("mobileEmail"), ""));
					resultAttributes.put("commMethod", ConvertUtils.getStringSafely(params.get("commMethod"), ""));
					try{
						BigDecimal discountTotalBefore=ConvertUtils.convertRequired(BigDecimal.class, params.get("discountTotalBefore"));
						resultAttributes.put("discountTotalBefore", discountTotalBefore.multiply(BigDecimal.valueOf(minorCurrencyFactor), TRUNC).toBigInteger());
					}catch(ConvertException e){
						log.error("Could not convert attributes as needed", e);
					}
					authResultCode = AuthResultCode.APPROVED;
				} else if(serviceType == SERVICE_TYPE_PSEUDO_PRIVILEGE_BASED) {
					resultAttributes.put("authResultCd", 'Y');
					resultAttributes.put("authorityRespCd", 0);
					String text = "Purchase allowed";
					resultAttributes.put("authorityRespDesc", text);
					setClientText(resultAttributes, AuthResultCode.APPROVED, null);
					resultAttributes.put("approvedAmount", amount);
					authResultCode = AuthResultCode.APPROVED;
				} else {
					BigDecimal requestedAmount = InteractionUtils.toMajorCurrency(amount, minorCurrencyFactor);
					boolean lowBalance = false;
					try {
						Connection conn = DataLayerMgr.getConnectionForCall("LOCK_AUTH_HOLD");
						try {
							DataLayerMgr.executeCall(conn, "LOCK_AUTH_HOLD", params);
							BigDecimal consumerBalance = ConvertUtils.convertRequired(BigDecimal.class, params.get("consumerAcctBalance"));
							BigDecimal onHoldAmount = ConvertUtils.convertRequired(BigDecimal.class, params.get("onHoldAmount"));
							boolean allowNegativeBalance = "Y".equalsIgnoreCase(ConvertUtils.convert(String.class, params.get("allowNegativeBalance")));
							boolean vendAllowNegBalOfflineOnly = "Y".equalsIgnoreCase(ConvertUtils.convert(String.class, params.get("vendAllowNegBalOfflineOnly")));
							
							Format format = getNumberFormat();
							BigDecimal availableBalance = consumerBalance.subtract(onHoldAmount);
							boolean addAuthHold;
							
							resultAttributes.put("firstName", ConvertUtils.getStringSafely(params.get("firstName"), ""));
							resultAttributes.put("lastName", ConvertUtils.getStringSafely(params.get("lastName"), ""));
							resultAttributes.put("email", ConvertUtils.getStringSafely(params.get("email"), ""));
							resultAttributes.put("sendReceipt", ConvertUtils.getStringSafely(params.get("sendReceipt"), ""));
							resultAttributes.put("mobileNumber", ConvertUtils.getStringSafely(params.get("mobileNumber"), ""));
							resultAttributes.put("mobileEmail", ConvertUtils.getStringSafely(params.get("mobileEmail"), ""));
							resultAttributes.put("commMethod", ConvertUtils.getStringSafely(params.get("commMethod"), ""));
							BigDecimal discountTotalBefore=ConvertUtils.convertRequired(BigDecimal.class, params.get("discountTotalBefore"));
							resultAttributes.put("discountTotalBefore", discountTotalBefore.multiply(BigDecimal.valueOf(minorCurrencyFactor), TRUNC).toBigInteger());
							BigDecimal discountAmount=new BigDecimal("0");
							BigInteger adjustedBalanceAmount=new BigInteger("0");
							
							//USAT-449 check corporate customer negative balance restrictions 
							if(availableBalance.compareTo(requestedAmount) < 0 && allowNegativeBalance && vendAllowNegBalOfflineOnly){
								try {
									String offline = getAttribute(taskInfo.getStep(), AuthorityAttrEnum.ATTR_OFFLINE, String.class, false);
									//if not offline then set allow negative balance flag to false for this transaction
									if (offline == null || !"true".equalsIgnoreCase(offline)){
										allowNegativeBalance = false;
										log.info("Customer offline only flag has overridden the allow negative balance setting for consumer acct " + consumerAcctId);
									}	
								} catch(AuthorityAttributeConversionException e) {
									log.warn("Could not determine customer machine offline status", e);
									allowNegativeBalance = false;
								}								
							}
							
							if (allowNegativeBalance||availableBalance.compareTo(requestedAmount) >= 0) {
								authResultCode = AuthResultCode.APPROVED;
								resultAttributes.put("authResultCd", 'Y');
								resultAttributes.put("authorityRespCd", 0);
								String text = "Available balance: " + format.format(availableBalance) + ", auth hold: " + format.format(onHoldAmount);
								resultAttributes.put("authorityRespDesc", text);
								BigDecimal discount;
								if(isDiscountInAuthResponse()) {
									try {
										params.put("tranStartTs", ConvertUtils.getLocalTime(ConvertUtils.getMillisUTC(authTime), taskInfo.getStep().getAttribute(AuthorityAttrEnum.ATTR_TIMEZONE_GUID, String.class, true)));
										DataLayerMgr.selectInto(conn, "GET_LOYALTY_DISCOUNT", params);
										discount = ConvertUtils.convert(BigDecimal.class, params.get("discountPercent"));
									} catch(BeanException e) {
										log.warn("Could not get loyalty discount", e);
										discount = null;
									} catch(AttributeConversionException e) {
										log.warn("Could not get loyalty discount", e);
										discount = null;
									}
								} else
									discount = null;
								setClientText(resultAttributes, authResultCode, null, null, availableBalance, onHoldAmount, discount);
								resultAttributes.put("approvedAmount", amount);
								BigInteger availableBalanceMinor = availableBalance.multiply(BigDecimal.valueOf(minorCurrencyFactor), TRUNC).toBigInteger();
								resultAttributes.put("balanceAmount", availableBalanceMinor);
								addAuthHold = true;
								if(discount!=null&&amount!=null){
									discountAmount=discount.multiply(new BigDecimal(amount));
									discountAmount=discountAmount.round(new MathContext(discountAmount.toBigInteger().toString().length(),RoundingMode.HALF_UP));
								}
								adjustedBalanceAmount=availableBalance.subtract(requestedAmount).multiply(BigDecimal.valueOf(minorCurrencyFactor), TRUNC).add(discountAmount).toBigInteger();
								resultAttributes.put("adjustedBalanceAmount", adjustedBalanceAmount);
							} else if(availableBalance.compareTo(BigDecimal.ZERO) > 0 && allowPartialAuth) {
								authResultCode = AuthResultCode.PARTIAL;
								resultAttributes.put("authResultCd", 'P');
								resultAttributes.put("authorityRespCd", 108);
								String text = "Available balance: " + format.format(availableBalance) + ", auth hold: " + format.format(onHoldAmount);
								resultAttributes.put("authorityRespDesc", text);
								BigDecimal discount;
								
								if(isDiscountInAuthResponse()) {
									try {
										params.put("tranStartTs", ConvertUtils.getLocalTime(authTime, taskInfo.getStep().getAttribute(AuthorityAttrEnum.ATTR_TIMEZONE_GUID, String.class, true)));
										DataLayerMgr.selectInto(conn, "GET_LOYALTY_DISCOUNT", params);
										discount = ConvertUtils.convert(BigDecimal.class, params.get("discountPercent"));
									} catch(BeanException e) {
										log.warn("Could not get loyalty discount", e);
										discount = null;
									} catch(AttributeConversionException e) {
										log.warn("Could not get loyalty discount", e);
										discount = null;
									}
								} else
									discount = null;
								setClientText(resultAttributes, authResultCode, null, null, availableBalance, onHoldAmount, discount);
								BigInteger availableBalanceMinor = availableBalance.multiply(BigDecimal.valueOf(minorCurrencyFactor), TRUNC).toBigInteger();
								resultAttributes.put("approvedAmount", availableBalanceMinor);
								resultAttributes.put("balanceAmount", availableBalanceMinor);
								lowBalance = true;
								addAuthHold = true;
								if(discount!=null&&amount!=null){
									discountAmount=discount.multiply(new BigDecimal(amount));
									discountAmount=discountAmount.round(new MathContext(discountAmount.toBigInteger().toString().length(),RoundingMode.HALF_UP));
								}
								adjustedBalanceAmount=availableBalance.subtract(requestedAmount).multiply(BigDecimal.valueOf(minorCurrencyFactor), TRUNC).add(discountAmount).toBigInteger();
								resultAttributes.put("adjustedBalanceAmount", adjustedBalanceAmount);
							} else {
								String text = "Available balance: " + format.format(availableBalance) + ", auth hold: " + format.format(onHoldAmount);
								authResultCode = populateResults(action, AuthResultCode.DECLINED, resultAttributes, DeniedReason.INSUFFICIENT_FUNDS, text, null, availableBalance, onHoldAmount);
								addAuthHold = false;
								lowBalance = true;
							}
							BigDecimal discountTotalAfter=discountTotalBefore.multiply(BigDecimal.valueOf(minorCurrencyFactor), TRUNC).add(discountAmount);
							resultAttributes.put("discountAmount", discountAmount);
							resultAttributes.put("discountTotalAfter", discountTotalAfter);
							
							if(addAuthHold) {
								AuthInsertTask ait = getAuthInsertTask();
								String qk = getAuthInsertQueueKey();
								if(ait == null) {
									try { conn.rollback(); } catch(SQLException e1) { log.warn("Could not rollback failed auth insert", e1); }
									log.warn("AuthInsertTask property is not set on " + this);
									populateAuthFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Server configuration issue");
									return AuthResultCode.FAILED;
								} else if(qk == null || qk.trim().length() == 0) {
									try { conn.rollback(); } catch(SQLException e1) { log.warn("Could not rollback failed auth insert", e1); }
									log.warn("AuthInsertQueueKey property is not set on " + this);
									populateAuthFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Server configuration issue");
									return AuthResultCode.FAILED;
								}
	
								log.info("Adding auth hold to consumer acct " + consumerAcctId);
								// find save step
								final MessageChainStep saveStep = findStepByQueueName(taskInfo.getStep(), qk, 0);
								if(saveStep == null) {
									//we did not find the save step, return a failed result
									log.warn("Could not find AuthInsert Step in Message Chain");
									try { conn.rollback(); } catch(SQLException e1) { log.warn("Could not rollback failed auth insert", e1); }
									populateAuthFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Server Runtime Error");
									return AuthResultCode.FAILED;
								}
								// run save step
								log.info("AuthInsert step obtained; executing...");
								saveStep.setAttribute(AuthorityAttrEnum.ATTR_ADD_AUTH_HOLD_DAYS, getAuthHoldDays());
								saveStep.addBooleanAttribute("authHoldUsed", true);
								AuthResultCode result;
								try {
									result = ait.process(conn, saveStep.getAttributes(), saveStep.getResultAttributes(), taskInfo.isRedelivered(), taskInfo.getPublisher());
								} catch(ServiceException e) {
									log.warn("Could not execute AuthInsert Step", e);
									try { conn.rollback(); } catch(SQLException e1) { log.warn("Could not rollback failed auth insert", e1); }
									saveStep.addBooleanAttribute("addAuthHold", false);
									populateAuthFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Server Error");
									return AuthResultCode.FAILED;
								}
								int rc = determineResultInt(result);
								// remove save step from all step's
								log.info("Removing AuthInsert step from message chain");
								removeStep(taskInfo.getStep(), saveStep, saveStep.getNextSteps(rc));
								// remove store step
								final MessageChainStep storeStep = findStepByQueueName(taskInfo.getStep(), getAuthStoreQueueKey(), 0);
								if(storeStep != null) {
									log.info("Removing AuthStore step from message chain");
									removeStep(taskInfo.getStep(), storeStep, storeStep.getNextSteps(rc));
								}
							} else {
								resultAttributes.put("authHoldUsed", false);
							}
							conn.commit();
						} catch(SQLException e) {
							log.warn("Error while getting auth hold info from database", e);
							try { conn.rollback(); } catch(SQLException e1) { log.warn("Could not rollback failed auth insert", e1); }
							populateAuthFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Database Error: " + e.getMessage());
							return AuthResultCode.FAILED;
						} catch(DataLayerException e) {
							log.warn("Error while getting auth hold info from database", e);
							try { conn.rollback(); } catch(SQLException e1) { log.warn("Could not rollback failed auth insert", e1); }
							populateAuthFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "DataLayer Error: " + e.getMessage());
							return AuthResultCode.FAILED;
						} catch(ConvertException e) {
							log.error("Could not convert attributes as needed", e);
							try { conn.rollback(); } catch(SQLException e1) { log.warn("Could not rollback failed auth insert", e1); }
							populateAuthFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Data Conversion Error");
							return AuthResultCode.FAILED;
						} finally {
							conn.close();
						}
						if(lowBalance) {
							handleLowBalance(taskInfo.getPublisher(), consumerAcctId);
						}
					} catch(SQLException e) {
						log.warn("Error accessing database connection", e);
						populateAuthFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Database Error");
						return AuthResultCode.FAILED;
					} catch(DataLayerException e) {
						log.warn("Error accessing database connection", e);
						populateAuthFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Database Error");
						return AuthResultCode.FAILED;
					}
				}
				break;
			default:
				log.error("Invalid Authority Service Type " + serviceType);
				populateFailedResults(action, resultAttributes, DeniedReason.CONFIGURATION_ISSUE, "Invalid Authority Service Type " + serviceType);
				return AuthResultCode.FAILED;
		}
		return authResultCode;
	}

	protected boolean isDiscountInAuthResponse() {
		return false;
	}

	protected void handleLowBalance(Publisher<ByteInput> publisher, long consumerAcctId) {
		// do nothing
	}

	protected MessageChainStep findStepByQueueName(MessageChainStep initialStep, String queueName, int resultCode) {
		MessageChainStep[] nextSteps = initialStep.getNextSteps(resultCode);
		if(nextSteps == null || nextSteps.length == 0)
			return null;
		for(MessageChainStep nextStep : nextSteps) {
			if(queueName.equals(nextStep.getQueueKey()))
				return nextStep;					
		}
		for(MessageChainStep nextStep : nextSteps) {
			MessageChainStep foundStep = findStepByQueueName(nextStep, queueName, resultCode);	
			if(foundStep != null)
				return foundStep;
		}
		return null;
	}

	protected AuthResultCode populateAuthFailedResults(AuthorityAction action, Map<String, Object> resultAttributes, DeniedReason deniedReason, String errorText) {
		resultAttributes.remove("approvedAmount");
		resultAttributes.remove("balanceAmount");
		return populateFailedResults(action, resultAttributes, deniedReason, errorText);
	}

	/**
	 * @param publisher
	 * @param tranId
	 * @param params
	 * @throws ServiceException
	 */
	protected void afterDebit(Publisher<ByteInput> publisher, Number tranId, Map<String, Object> params, int consumerAcctTypeId) throws ServiceException {
		try {
			if (consumerAcctTypeId == CONSUMER_ACCT_TYPE_DRIVER
					|| consumerAcctTypeId == CONSUMER_ACCT_TYPE_GIFT
					|| consumerAcctTypeId == CONSUMER_ACCT_TYPE_EXTERNAL)
				return;
			
			long consumerAcctId = ConvertUtils.getLong(params.get("consumerAcctId"));
			if("Y".equals(ConvertUtils.getString(params.get("replenishFlag"), true))) {
				handleLowBalance(publisher, consumerAcctId);
			}
			Connection conn = DataLayerMgr.getConnectionForCall("GET_CASH_BACKS");
			boolean okay = false;
			try {
				InteractionUtils.checkForCashBack(tranId.longValue(), conn, publisher, log);
				InteractionUtils.checkForBelowBalance(consumerAcctId, conn, publisher, log);
				okay = true;
			} finally {
				DbUtils.commitOrRollback(conn, okay, true);
			}
		} catch(ConvertException e) {
			throw new ServiceException(e);
		} catch(SQLException e) {
			throw new ServiceException(e);
		} catch(DataLayerException e) {
			throw new ServiceException(e);
		}
	}

	protected AuthResultCode debit(AuthorityAction action, int serviceType, Number tranId, BigInteger amount, String currencyCd, int minorCurrencyFactor, boolean redelivered, Map<String, Object> resultAttributes, Publisher<ByteInput> publisher, int consumerAcctTypeId) throws ServiceException {
		BigDecimal requestedAmount = InteractionUtils.toMajorCurrency(amount, minorCurrencyFactor);
		Map<String, Object> params = new HashMap<String, Object>();
		switch(serviceType) {
			case SERVICE_TYPE_BALANCE_BASED: case SERVICE_TYPE_HIDDEN_BALANCE_BASED:
				// update account
				params.put("tranId", tranId);
				params.put("amount", requestedAmount);
				if(redelivered)
					params.put("redeliveryFlag", 'Y');
				try {
					DataLayerMgr.executeCall("DEBIT_CONSUMER_ACCT", params, true);
				} catch(SQLException e) {
					switch(e.getErrorCode()) {
						case 20111: // Auth hold not found
							return populateResults(action, AuthResultCode.DECLINED_PERMANENT, resultAttributes, null, "Auth Hold Not Found");
						case 20112: // Insufficient Funds
							return populateResults(action, AuthResultCode.DECLINED, resultAttributes, null, "Insufficient Funds");
						case 20113: // Sale Amount Greater than Auth Amount
							return populateResults(action, AuthResultCode.DECLINED, resultAttributes, null, "Sale Amount Is Greater Than Auth Amount");
						case 20114: // Auth hold Cleared
							return populateResults(action, AuthResultCode.DECLINED_PERMANENT, resultAttributes, null, "Auth Hold Was Cleared");
						case 20115: // Auth hold Expired
							return populateResults(action, AuthResultCode.DECLINED_PERMANENT, resultAttributes, null, "Auth Hold Has Expired");
						case 20116: // Auth hold Inconsistent State
							return populateResults(action, AuthResultCode.DECLINED, resultAttributes, null, "Auth Hold Inconsistent State");
					}
					log.error("Could not debit consumer acct", e);
					return populateResults(action, AuthResultCode.DECLINED, resultAttributes, null, "Database Error: " + e.getMessage());
				} catch(DataLayerException e) {
					return populateResults(action, AuthResultCode.DECLINED, resultAttributes, null, "Database Error: " + e.getMessage());
				}
				Format format = getNumberFormat();
				AuthResultCode authResultCd;
				BigDecimal debittedAmount;
				try {
					authResultCd = ConvertUtils.convertRequired(AuthResultCode.class, params.get("authResultCd"));
					debittedAmount = ConvertUtils.convertRequired(BigDecimal.class, params.get("debittedAmount"));
				} catch(ConvertException e) {
					log.error("Could not get authResultCd or debittedAmount from params", e);
					return populateResults(action, AuthResultCode.FAILED, resultAttributes, null, "Data Conversion Error");
				}				
				afterDebit(publisher, tranId, params, consumerAcctTypeId);
				if(debittedAmount.compareTo(requestedAmount) >= 0) {
					resultAttributes.put("authorityRespCd", 0);
					resultAttributes.put("authorityRespDesc", "Debited " + format.format(debittedAmount) + " from the account");
					resultAttributes.put("approvedAmount", InteractionUtils.toMinorCurrency(debittedAmount, minorCurrencyFactor).longValueExact());
					resultAttributes.put("settled", true);
				} else if(debittedAmount.compareTo(BigDecimal.ZERO) == 0) {
					resultAttributes.put("authorityRespCd", DeniedReason.INSUFFICIENT_FUNDS.toString());
					resultAttributes.put("authorityRespDesc", "Could not debit from the account");
					authResultCd = AuthResultCode.DECLINED;
				} else {
					resultAttributes.put("authorityRespCd", DeniedReason.INSUFFICIENT_FUNDS.toString());
					resultAttributes.put("approvedAmount", InteractionUtils.toMinorCurrency(debittedAmount, minorCurrencyFactor).longValueExact());
					resultAttributes.put("authorityRespDesc", "Could not debit from the account (debited " + format.format(debittedAmount) + ")");
					authResultCd = AuthResultCode.DECLINED;
				}
				resultAttributes.put("authResultCd", authResultCd.getValue());
				return authResultCd;
			case SERVICE_TYPE_PSEUDO_PRIVILEGE_BASED:
				// remove auth hold
				params.put("tranId", tranId);
				try {
					DataLayerMgr.executeCall("REMOVE_AUTH_HOLD", params, true);
				} catch(SQLException e) {
					log.warn("Error while removing auth hold on consumer acct from database", e);
					return populateDeclinedResults(action, resultAttributes, null, "Database Error: " + e.getMessage());
				} catch(DataLayerException e) {
					log.warn("Error while removing auth hold on consumer acct from database", e);
					return populateDeclinedResults(action, resultAttributes, null, "Database Error: " + e.getMessage());
				}
				resultAttributes.put("authResultCd", AuthResultCode.APPROVED.getValue());
				resultAttributes.put("authorityRespCd", 0);
				resultAttributes.put("authorityRespDesc", "Removed auth hold from the account");
				resultAttributes.put("settled", true);
				return AuthResultCode.APPROVED;
			default:
				log.error("Invalid Authority Service Type " + serviceType);
				return populateFailedResults(action, resultAttributes, null, "Invalid Authority Service Type " + serviceType);
		}
	}

	protected AuthResultCode removeAuthHold(AuthorityAction action, Number tranId, Map<String,Object> resultAttributes) {
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("tranId", tranId);
		// remove auth hold
		try {
			DataLayerMgr.executeCall("REMOVE_AUTH_HOLD", params, true);
		} catch(SQLException e) {
			log.warn("Error while removing auth hold on consumer acct from database", e);
			return populateDeclinedResults(action, resultAttributes, null, "Database Error: " + e.getMessage());
		} catch(DataLayerException e) {
			log.warn("Error while removing auth hold on consumer acct from database", e);
			return populateDeclinedResults(action, resultAttributes, null, "Database Error: " + e.getMessage());
		}
		resultAttributes.put("authResultCd", 'Y');
		resultAttributes.put("authorityRespCd", 0);
		String text = "Removed auth hold from the account";
		resultAttributes.put("authorityRespDesc", text);
		resultAttributes.put("settled", true);
		return AuthResultCode.APPROVED;
	}

	protected AuthResultCode credit(AuthorityAction action, int serviceType, Number tranId, BigInteger amount, String currencyCd, int minorCurrencyFactor, boolean redelivered, Map<String, Object> resultAttributes) {
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("tranId", tranId);
		BigDecimal requestedAmount = InteractionUtils.toMajorCurrency(amount, minorCurrencyFactor);
		params.put("amount", requestedAmount);
		if(redelivered)
			params.put("redeliveryFlag", 'Y');

		switch(serviceType) {
			case SERVICE_TYPE_BALANCE_BASED: case SERVICE_TYPE_HIDDEN_BALANCE_BASED:
				// update account
				try {
					DataLayerMgr.executeCall("CREDIT_CONSUMER_ACCT", params, true);
				} catch(SQLException e) {
					switch(e.getErrorCode()) {
						case 20111: // Refund not found
							return populateResults(action, AuthResultCode.DECLINED_PERMANENT, resultAttributes, null, "Refund Not Found");
						case 20116: // Auth hold Inconsistent State
							return populateResults(action, AuthResultCode.DECLINED, resultAttributes, null, "Refund Inconsistent State");
					}
					log.warn("Error while adjusting balance on consumer acct from database", e);
					return populateDeclinedResults(action, resultAttributes, null, "Database Error: " + e.getMessage());
				} catch(DataLayerException e) {
					log.warn("Error while adjusting balance on consumer acct from database", e);
					return populateDeclinedResults(action, resultAttributes, null, "Database Error: " + e.getMessage());
				}
				AuthResultCode authResultCd;
				BigDecimal creditedAmount;
				try {
					authResultCd = ConvertUtils.convertRequired(AuthResultCode.class, params.get("authResultCd"));
					creditedAmount = ConvertUtils.convertRequired(BigDecimal.class, params.get("credittedAmount"));
				} catch(ConvertException e) {
					return populateResults(action, AuthResultCode.FAILED, resultAttributes, null, "Data Conversion Error");
				}
				Format format = getNumberFormat();
				if(creditedAmount.compareTo(requestedAmount) >= 0) {
					resultAttributes.put("authorityRespCd", 0);
					resultAttributes.put("authorityRespDesc", "Credited " + format.format(creditedAmount) + " to the account");
					resultAttributes.put("approvedAmount", InteractionUtils.toMinorCurrency(creditedAmount, minorCurrencyFactor).longValueExact());
					resultAttributes.put("settled", true);
				} else if(creditedAmount.compareTo(BigDecimal.ZERO) == 0) {
					resultAttributes.put("authorityRespCd", DeniedReason.INSUFFICIENT_FUNDS.toString());
					resultAttributes.put("authorityRespDesc", "Could not credit to the account");
					authResultCd = AuthResultCode.DECLINED;
				} else {
					resultAttributes.put("authorityRespCd", DeniedReason.INSUFFICIENT_FUNDS.toString());
					resultAttributes.put("authorityRespDesc", "Could not credit from the account (credited " + format.format(creditedAmount) + ")");
					resultAttributes.put("approvedAmount", InteractionUtils.toMinorCurrency(creditedAmount, minorCurrencyFactor).longValueExact());
					authResultCd = AuthResultCode.DECLINED;
				}
				resultAttributes.put("authResultCd", authResultCd.getValue());
				return authResultCd;
			case SERVICE_TYPE_PSEUDO_PRIVILEGE_BASED:
				//do nothing
				resultAttributes.put("authResultCd", AuthResultCode.APPROVED.getValue());
				resultAttributes.put("authorityRespCd", 0);
				resultAttributes.put("authorityRespDesc", "No change to account");
				resultAttributes.put("settled", true);
				return AuthResultCode.APPROVED;
			default:
				log.error("Invalid Authority Service Type " + serviceType);
				return populateFailedResults(action, resultAttributes, null, "Invalid Authority Service Type " + serviceType);
		}
	}
	/**
	 * @param step
	 * @param saveStep
	 * @param afterStep
	 */
	protected void removeStep(MessageChainStep startStep, MessageChainStep removeStep, MessageChainStep[] afterSteps) {
		Set<Integer> rcs = startStep.getResultCodes();
		Integer[] rcArray = rcs.toArray(new Integer[rcs.size()]);
		for(int rc : rcArray) {
			MessageChainStep[] nextSteps = startStep.getNextSteps(rc);
			for(MessageChainStep nextStep : nextSteps) {
				if(nextStep.equals(removeStep)) {
					startStep.setNextSteps(rc, afterSteps);
				} else if(nextStep != startStep) { // recursion protection
					removeStep(nextStep, removeStep, afterSteps);
				}
			}
		}
	}
	
	protected static Format getNumberFormat() {
		return ConvertUtils.getFormat("NUMBER");
	}

	public float getAuthHoldDays() {
		return authHoldDays;
	}

	public void setAuthHoldDays(float authHoldDays) {
		this.authHoldDays = authHoldDays;
	}

	public AuthInsertTask getAuthInsertTask() {
		return authInsertTask;
	}

	public void setAuthInsertTask(AuthInsertTask authInsertTask) {
		this.authInsertTask = authInsertTask;
	}

	public String getAuthInsertQueueKey() {
		return authInsertQueueKey;
	}

	public void setAuthInsertQueueKey(String authInsertQueueKey) {
		this.authInsertQueueKey = authInsertQueueKey;
	}

	public String getAuthStoreQueueKey() {
		return authStoreQueueKey;
	}

	public void setAuthStoreQueueKey(String authStoreQueueKey) {
		this.authStoreQueueKey = authStoreQueueKey;
	}
	
	public int[] getConsumerAcctTypeIds() {
		return consumerAcctTypeIds;
	}

	public void setConsumerAcctTypeIds(int[] consumerAcctTypeIds) {
		this.consumerAcctTypeIds = consumerAcctTypeIds;
	}

	public String getRegistrationUrl() {
		return registrationUrl;
	}

	public void setRegistrationUrl(String registrationUrl) {
		this.registrationUrl = registrationUrl;
	}

	public String getUpdateAccountQueueKey() {
		return updateAccountQueueKey;
	}

	public void setUpdateAccountQueueKey(String updateAccountQueueKey) {
		this.updateAccountQueueKey = updateAccountQueueKey;
	}

	public short[] getInstances() {
		return instances;
	}

	public void setInstances(short[] instances) {
		this.instances = instances;
	}
}