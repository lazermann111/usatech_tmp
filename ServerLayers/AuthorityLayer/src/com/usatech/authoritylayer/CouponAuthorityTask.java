/**
 *
 */
package com.usatech.authoritylayer;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;

import org.apache.http.HttpException;
import org.apache.http.client.methods.HttpRequestBase;

import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.HttpRequester;
import com.usatech.layers.common.HttpRequester.Authenticator;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAction;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.DeniedReason;
import com.usatech.layers.common.constants.PaymentMaskBRef;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Base64;
import simple.io.Log;
import simple.text.StringUtils;

/**
 * @author Brian S. Krug
 *
 */
public class CouponAuthorityTask extends AbstractAuthorityTask {
	private static final Log log = Log.getLog();
	protected final HttpRequester requester = new HttpRequester();
	protected final Authenticator authenticator = new Authenticator() {
		@Override
		public void addAuthentication(HttpRequestBase method) throws HttpException {
			method.addHeader("Authorization", new StringBuilder("Basic ").append(Base64.encodeString(username + ":" + password, true)).toString());
		}
	};

	protected String host;
	protected int port;
	protected String username;
	protected String password;

	public CouponAuthorityTask() {
		super("Coupon");
		requester.setConnectionTimeout(5000);
		requester.setSocketTimeout(15000);
	}

	@Override
	protected AuthResultCode processRequest(AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo) throws ServiceException {
		String path;
		switch(action) {
			case AUTHORIZATION:
				path = "2.0/coupon/verify";
				break;
			case SALE:
				path = "2.0/coupon/redeem";
				break;
			default:
				return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Action '" + action + "' is not supported");
		}
		MessageChainStep step = taskInfo.getStep();
		String deviceSerialCd;
		AccountData accountData;
		BigInteger amount;
		int minorCurrencyFactor;
		boolean allowPartialAuth;
		try {
			deviceSerialCd = getAttribute(step, AuthorityAttrEnum.ATTR_DEVICE_SERIAL_CD, String.class, true);
			accountData = new AccountData(step.getAttributes());
			amount = getAttribute(step, AuthorityAttrEnum.ATTR_AMOUNT, BigInteger.class, true);
			minorCurrencyFactor = getAttribute(step, AuthorityAttrEnum.ATTR_MINOR_CURRENCY_FACTOR, Integer.class, true);
			allowPartialAuth = ConvertUtils.getBoolean(getAttribute(step, AuthorityAttrEnum.ATTR_PARTIAL_AUTH_ALLOWED, Object.class, false), false);
		} catch(ConvertException e) {
			log.error("Could not convert attributes", e);
			return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), null, "Data Conversion Error");
		} catch(AuthorityAttributeConversionException e) {
			return AuthResultCode.FAILED;
		}
		String couponCode = accountData.getPartData(PaymentMaskBRef.COUPON_CODE);
		if(StringUtils.isBlank(couponCode)) {
			return populateDeclinedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.MISSING_INPUT, "Coupon code is missing");
		}
		BigDecimal amountMajor = InteractionUtils.toMajorCurrency(amount, minorCurrencyFactor);
		String url = new StringBuilder().append("https://").append(getHost()).append(':').append(port).append('/').append(path).toString();
		Map<String, Object> payload = new LinkedHashMap<>();
		payload.put("device_serial", deviceSerialCd);
		payload.put("code", couponCode);
		payload.put("credit", amountMajor);
		log.info("Sending payload " + payload + " to " + url);
		Map<String, Object> result;
		try {
			result = requester.jsonPost(url, payload, Map.class, Map.class, authenticator);
		} catch(HttpException | IOException e) {
			return populateDeclinedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.AUTH_LAYER_PROCESSING_ERROR, e.getMessage());
		}
		log.info("Received result " + result + " from " + url);
		String couponType;
		boolean approved;
		Integer usesRemaining;
		BigDecimal creditRemaining;
		String status;
		try {
			couponType = ConvertUtils.getString(result.get("couponType"), "");
			approved = ConvertUtils.getBoolean(result.get("approved"), false);
			usesRemaining = ConvertUtils.convert(Integer.class, result.get("usesRemaining"));
			creditRemaining = ConvertUtils.convert(BigDecimal.class, result.get("creditRemaining"));
			status = ConvertUtils.getString(result.get("status"), "");
		} catch(ConvertException e) {
			log.error("Could not convert result attributes", e);
			return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), null, "Data Conversion Error");
		}

		step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, approved);
		step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, status);
		// step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD, approvalCode);
		if(couponType!=null){
			step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, StringUtils.capitalizeFirst(couponType));
		}
		// step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS, responseTs);
		step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, result.toString());
		step.setResultAttribute(AuthorityAttrEnum.ATTR_CARD_TYPE, (StringUtils.capitalizeFirst(couponType) + " Coupon").trim());

		AuthResultCode authResultCode;
		if(!approved) {
			authResultCode = AuthResultCode.DECLINED;
			if(usesRemaining == null && creditRemaining == null) {
				step.setResultAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.INVALID_VERIFICATION_DATA);
				setClientText(step.getResultAttributes(), authResultCode, DeniedReason.INVALID_VERIFICATION_DATA, status);
			} else {
				step.setResultAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.INSUFFICIENT_FUNDS);
				setClientText(step.getResultAttributes(), authResultCode, DeniedReason.INSUFFICIENT_FUNDS, status, creditRemaining);
			}
		} else if(action != AuthorityAction.AUTHORIZATION || creditRemaining == null || creditRemaining.compareTo(amountMajor) >= 0) {
			authResultCode = AuthResultCode.APPROVED;
			step.setResultAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, amount.longValueExact());
			step.setResultAttribute(AuthorityAttrEnum.ATTR_SETTLED, true);
			setClientText(step.getResultAttributes(), authResultCode, null, null, null, null, status);
		} else if(allowPartialAuth && creditRemaining.compareTo(BigDecimal.ZERO) > 0) {
			authResultCode = AuthResultCode.PARTIAL;
			step.setResultAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, InteractionUtils.toMinorCurrency(creditRemaining, minorCurrencyFactor).longValueExact());
			step.setResultAttribute(AuthorityAttrEnum.ATTR_SETTLED, true);
			setClientText(step.getResultAttributes(), authResultCode, null, null, creditRemaining, null, status);
		} else {
			authResultCode = AuthResultCode.DECLINED;
			step.setResultAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.INSUFFICIENT_FUNDS);
			step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, status + "; Insufficient funds: " + creditRemaining);
			setClientText(step.getResultAttributes(), authResultCode, DeniedReason.INSUFFICIENT_FUNDS, status, creditRemaining);
		}
		step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTH_HOLD_USED, false);
		step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, authResultCode);
		return authResultCode;
	}

	/**
	 * @see com.usatech.authoritylayer.AbstractAuthorityTask#processReversal(com.usatech.layers.common.constants.AuthorityAction, com.usatech.app.MessageChainTaskInfo)
	 */
	@Override
	protected AuthResultCode processReversal(AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo) throws ServiceException {
		Map<String, Object> resultAttributes = taskInfo.getStep().getResultAttributes();
		switch(action) {
			case AUTHORIZATION:
				// nothing to do b/c auth is a balance check
				return AuthResultCode.APPROVED;
			default:
				return populateFailedResults(subAction, resultAttributes, DeniedReason.CONFIGURATION_ISSUE, "Reversal of Action '" + action + "' NOT implemented");
		}
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getMaxTotalConnections() {
		return requester.getMaxTotalConnections();
	}

	public void setMaxTotalConnections(int maxTotalConnections) {
		requester.setMaxTotalConnections(maxTotalConnections);
	}

	public int getMaxHostConnections() {
		return requester.getMaxHostConnections();
	}

	public void setMaxHostConnections(int maxHostConnections) {
		requester.setMaxHostConnections(maxHostConnections);
	}

	public int getConnectionTimeout() {
		return requester.getConnectionTimeout();
	}

	public void setConnectionTimeout(int connectionTimeout) {
		requester.setConnectionTimeout(connectionTimeout);
	}

	public int getSocketTimeout() {
		return requester.getSocketTimeout();
	}

	public void setSocketTimeout(int socketTimeout) {
		requester.setSocketTimeout(socketTimeout);
	}

	public HostnameVerifier getHostNameVerifier() {
		return requester.getHostNameVerifier();
	}

	public void setHostNameVerifier(HostnameVerifier hostNameVerifier) {
		requester.setHostNameVerifier(hostNameVerifier);
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
