package com.usatech.authoritylayer;

import java.util.Map;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.io.Log;

import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAction;

public class DemoAuthorityTask extends AbstractAuthorityTask {
	private static final Log log = Log.getLog();

	public DemoAuthorityTask() {
		super("demo");
	}
	@Override
	protected AuthResultCode processRequest(AuthorityAction action, AuthorityAction subAction, final MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		Map<String, Object> resultAttributes = step.getResultAttributes();
		try {
			switch(action) {
				case SALE:
					resultAttributes.put("authResultCd", 'Y');
					resultAttributes.put("authorityRespCd", "AUTO");
					resultAttributes.put("authorityRespDesc", "Settled by USAT Demo");
					resultAttributes.put("settled", true);
					return AuthResultCode.APPROVED;
				case REFUND:
					resultAttributes.put("authResultCd", 'Y');
					resultAttributes.put("authorityRespCd", "AUTO");
					resultAttributes.put("authorityRespDesc", "Refunded by USAT Demo");
					resultAttributes.put("settled", true);
					return AuthResultCode.APPROVED;
				default:
					throw new RetrySpecifiedServiceException("Action '" + action + "' NOT implemented", WorkRetryType.NO_RETRY);
			}
		} catch(Exception e) {
			log.error("Error processing demo transaction", e);
			resultAttributes.put("authResultCd", 'F');
			resultAttributes.put("authorityRespCd", 'F');
			resultAttributes.put("authorityRespDesc", "Error: " + e.getMessage());
			return AuthResultCode.FAILED;
		}
	}

	@Override
	protected AuthResultCode processReversal(AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		Map<String, Object> resultAttributes = step.getResultAttributes();
		try {
			switch(action) {
				case AUTHORIZATION:
					resultAttributes.put("authResultCd", 'Y');
					resultAttributes.put("authorityRespCd", "AUTO");
					resultAttributes.put("authorityRespDesc", "Demo auth reversal");
					return AuthResultCode.APPROVED;
				default:
					log.warn("Reversal of Action '" + action + "' NOT implemented");
					return AuthResultCode.FAILED;
			}
		} catch(Exception e) {
			log.error("Error processing demo transaction", e);
			resultAttributes.put("authResultCd", 'F');
			resultAttributes.put("authorityRespCd", 'F');
			resultAttributes.put("authorityRespDesc", "Error: " + e.getMessage());
			return AuthResultCode.FAILED;
		}
	}
}
