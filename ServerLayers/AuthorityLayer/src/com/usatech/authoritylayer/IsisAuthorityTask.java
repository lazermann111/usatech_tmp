package com.usatech.authoritylayer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.text.StringUtils;

import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAction;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.PaymentMaskBRef;

public class IsisAuthorityTask extends AbstractAuthorityTask {
	private static final Log log = Log.getLog();
	
	protected BigInteger approvedAuthAmount = BigInteger.ZERO;

	public IsisAuthorityTask() {
		super("isis");
	}

	@Override
	protected AuthResultCode processRequest(AuthorityAction action, AuthorityAction subAction, final MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		Map<String, Object> resultAttributes = step.getResultAttributes();
		try {
			String currencyCd = getAttribute(step, AuthorityAttrEnum.ATTR_CURRENCY_CD, String.class, true);
			Map<String,Object> params = new HashMap<String, Object>();
			Number tranId;
			switch(action) {
				case AUTHORIZATION:
					BigInteger originalAmount = getAttribute(step, AuthorityAttrEnum.ATTR_ORIGINAL_AMOUNT, BigInteger.class, true);
					Long globalAccountId = step.getAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, Long.class, false);
					if(globalAccountId != null) {
						params.put("globalAccountId", globalAccountId);
						params.put("instance", step.getAttribute(AuthorityAttrEnum.ATTR_INSTANCE, Integer.class, true));
					}
					String consumerAcctCd = step.getAttribute(PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER.attribute, String.class, false);
					if(consumerAcctCd == null) {
						consumerAcctCd = step.getAttribute(PaymentMaskBRef.PARTIAL_PRIMARY_ACCOUNT_NUMBER.attribute, String.class, true);
						if(consumerAcctCd == null)
							consumerAcctCd = MessageResponseUtils.getCardNumber(step.getAttribute(AuthorityAttrEnum.ATTR_TRACK_DATA, String.class, true));
					}
					params.put("consumerAcctCd", MessageResponseUtils.maskCardNumber(consumerAcctCd));
					byte additionalDataType = step.getAttributeSafely(AuthorityAttrEnum.ATTR_ADDITIONAL_INFO_TYPE, byte.class, (byte) 0);
					String additionalData = step.getAttributeSafely(AuthorityAttrEnum.ATTR_ADDITIONAL_INFO, String.class, "");
					switch (additionalDataType) {
						case 1: //ISIS_SMARTTAP
							if (StringUtils.isBlank(additionalData))
								log.warn("Softcard SmartTap has no additional data");
							else {
								StringBuilder miscData = new StringBuilder().append(StringUtils.FS);
								// Isis SmartTap Consumer ID tag is DF21
								byte[] consumerIdentifier = ProcessingUtils.parseTLV(additionalData.getBytes(), (byte)(0xDF & 0xFF), (byte)(0x21 & 0xFF));
								if (consumerIdentifier == null || consumerIdentifier.length == 0)
									log.warn("No Consumer ID in Softcard SmartTap data");
								else {
									String isisConsumerId = StringUtils.toHex(consumerIdentifier);
									params.put("consumerIdentifier", isisConsumerId);
									miscData.append("ConsumerID=").append(isisConsumerId).append(StringUtils.FS);
								}
								// Isis SmartTap Isis Offer ID tag is DF51
								byte[] offerId = ProcessingUtils.parseTLV(additionalData.getBytes(), (byte)(0xDF & 0xFF), (byte)(0x51 & 0xFF));
								if (offerId == null || offerId.length == 0)
									log.info("No Offer ID in Softcard SmartTap data");
								else
									miscData.append("OfferID=").append(StringUtils.toHex(offerId)).append(StringUtils.FS);
								if (miscData.length() > 1)
									resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA.getValue(), miscData.toString());
							}
							break;
					}
					params.put("currencyCd", currencyCd);
					DataLayerMgr.executeCall("AUTHORIZE_ISIS_PROMO", params, true);					
					long consumerAcctId = ConvertUtils.getLongSafely(params.get("consumerAcctId"), -1);
					int transToFreeTran = ConvertUtils.getIntSafely(params.get("transToFreeTran"), 4);
					if(consumerAcctId > 0) {
						resultAttributes.put("consumerAcctId", consumerAcctId);
						if(globalAccountId == null) {
							globalAccountId = ConvertUtils.convertSafely(Long.class, params.get("globalAccountId"), null);
							if(globalAccountId != null) { // Created it - let Store task know
								resultAttributes.put("globalAccountId", globalAccountId);
								resultAttributes.put("instance", 0);
							}
						}
					}
					if ("Y".equals(params.get("authResultCd"))) {						
						int minorCurrencyFactor = getAttribute(step, AuthorityAttrEnum.ATTR_MINOR_CURRENCY_FACTOR, Integer.class, true);
						BigDecimal approvedAmountMajor = new BigDecimal(approvedAuthAmount).divide(BigDecimal.valueOf(minorCurrencyFactor), 2, RoundingMode.FLOOR);
						resultAttributes.put("authorityRespCd", "SOFTCARD_PROMO");
						resultAttributes.put("authAuthorityTranCd", "Softcard Promo");
						setClientText(resultAttributes, AuthResultCode.APPROVED, null, null, approvedAmountMajor);
						resultAttributes.put("authorityRespDesc", "Free purchase up to $" + ConvertUtils.formatObject(approvedAmountMajor, "NUMBER:#,##0.##"));
						resultAttributes.put("freeTransaction", true);
						if(approvedAuthAmount.compareTo(BigInteger.ZERO) == 1 && originalAmount.compareTo(approvedAuthAmount) == 1) {
							resultAttributes.put("approvedAmount", approvedAuthAmount);
							resultAttributes.put("authResultCd", 'P');
							return AuthResultCode.PARTIAL;
						}
						resultAttributes.put("authResultCd", 'Y');
						return AuthResultCode.APPROVED;
					}
					resultAttributes.put("authResultCd", 'N');
					resultAttributes.put("authorityRespCd", 'N');
					resultAttributes.put("authorityRespDesc", transToFreeTran + " More Tap(s) for Free Purchase");
					resultAttributes.put(AuthorityAttrEnum.ATTR_OVERRIDE_CLIENT_TEXT_KEY.getValue(), "client.message.auth.isis.denied.not-yet-free");
					resultAttributes.put(AuthorityAttrEnum.ATTR_OVERRIDE_CLIENT_TEXT_PARAMS.getValue(), new Object[] { transToFreeTran });
					return AuthResultCode.DECLINED;
				case SALE:
					resultAttributes.put("authResultCd", 'Y');
					resultAttributes.put("authorityRespCd", "SOFTCARD_PROMO");
					resultAttributes.put("authorityRespDesc", "Settled by USAT");
					resultAttributes.put("settled", true);
					return AuthResultCode.APPROVED;
				case REFUND:
					tranId = getAttribute(step, AuthorityAttrEnum.ATTR_TRAN_ID, Number.class, true);
					params.put("tranId", tranId);
					DataLayerMgr.executeCall("REFUND_ISIS_PROMO", params, true);
					resultAttributes.put("authResultCd", 'Y');
					resultAttributes.put("authorityRespCd", "SOFTCARD_PROMO");
					resultAttributes.put("authorityRespDesc", "Refunded by USAT");
					resultAttributes.put("settled", true);
					return AuthResultCode.APPROVED;
				default:
					throw new RetrySpecifiedServiceException("Action '" + action + "' NOT implemented", WorkRetryType.NO_RETRY);
			}
		} catch(Exception e) {
			log.error("Error processing Softcard transaction", e);
			resultAttributes.put("authResultCd", 'F');
			resultAttributes.put("authorityRespCd", 'F');
			resultAttributes.put("authorityRespDesc", "Error: " + e.getMessage());
			return AuthResultCode.FAILED;
		}
	}

	@Override
	protected AuthResultCode processReversal(AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		Map<String, Object> resultAttributes = step.getResultAttributes();
		try {
			switch(action) {
				case AUTHORIZATION:					
					resultAttributes.put("authResultCd", 'Y');
					resultAttributes.put("authorityRespCd", '0');
					resultAttributes.put("authorityRespDesc", "No change to account");
					return AuthResultCode.APPROVED;
				default:
					log.warn("Reversal of Action '" + action + "' NOT implemented");
					return AuthResultCode.FAILED;
			}
		} catch(Exception e) {
			log.error("Error processing Softcard transaction", e);
			resultAttributes.put("authResultCd", 'F');
			resultAttributes.put("authorityRespCd", 'F');
			resultAttributes.put("authorityRespDesc", "Error: " + e.getMessage());
			return AuthResultCode.FAILED;
		}
	}

	public BigInteger getApprovedAuthAmount() {
		return approvedAuthAmount;
	}

	public void setApprovedAuthAmount(BigInteger approvedAuthAmount) {
		this.approvedAuthAmount = approvedAuthAmount;
	}
}
