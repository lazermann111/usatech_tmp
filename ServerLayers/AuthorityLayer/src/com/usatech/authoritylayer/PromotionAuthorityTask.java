package com.usatech.authoritylayer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.Log;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAction;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.Language;
import com.usatech.layers.common.constants.PaymentMaskBRef;

public class PromotionAuthorityTask extends AbstractAuthorityTask {
	private static final Log log = Log.getLog();
	
	public PromotionAuthorityTask() {
		super("promotion");
	}

	@Override
	protected AuthResultCode processRequest(AuthorityAction action, AuthorityAction subAction, final MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		Map<String, Object> resultAttributes = step.getResultAttributes();
		try {
			language = taskInfo.getStep().getAttributeDefault(CommonAttrEnum.ATTR_LANGUAGE, Language.class, Language.ENGLISH);
		} catch (AttributeConversionException e1) {
			language = Language.ENGLISH;
		}
		try {
			String currencyCd = getAttribute(step, AuthorityAttrEnum.ATTR_CURRENCY_CD, String.class, true);
			Map<String,Object> params = new HashMap<String, Object>();
			Number tranId;
			switch(action) {
				case AUTHORIZATION:
					long posPtaId = getAttribute(step, AuthorityAttrEnum.ATTR_POS_PTA_ID, Long.class, true);
					long authTime = getAttribute(step, AuthorityAttrEnum.ATTR_AUTH_TIME, Long.class, true);
					params.put("posPtaId",posPtaId);
					params.put("tranStartTime", ConvertUtils.getLocalTime(authTime, taskInfo.getStep().getAttribute(AuthorityAttrEnum.ATTR_TIMEZONE_GUID, String.class, true)));
					
					BigInteger originalAmount = getAttribute(step, AuthorityAttrEnum.ATTR_ORIGINAL_AMOUNT, BigInteger.class, true);
					Long globalAccountId = step.getAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, Long.class, false);
					if(globalAccountId != null) {
						params.put("globalAccountId", globalAccountId);
						params.put("instance", step.getAttribute(AuthorityAttrEnum.ATTR_INSTANCE, Integer.class, true));
					}
					String consumerAcctCd = step.getAttribute(PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER.attribute, String.class, false);
					if(consumerAcctCd == null) {
						consumerAcctCd = step.getAttribute(PaymentMaskBRef.PARTIAL_PRIMARY_ACCOUNT_NUMBER.attribute, String.class, true);
						if(consumerAcctCd == null)
							consumerAcctCd = MessageResponseUtils.getCardNumber(step.getAttribute(AuthorityAttrEnum.ATTR_TRACK_DATA, String.class, true));
					}
					params.put("consumerAcctCd", MessageResponseUtils.maskCardNumber(consumerAcctCd));
					params.put("currencyCd", currencyCd);
					boolean allowPartialAuth = step.getAttributeSafely(AuthorityAttrEnum.ATTR_PARTIAL_AUTH_ALLOWED, Boolean.class, true);
					params.put("allowPartialAuth", allowPartialAuth?"Y":"N");
					params.put("requestedAmount", originalAmount);
					Long consumerPassId = step.getAttribute(CommonAttrEnum.ATTR_PASS_IDENTIFIER, Long.class, false); 
					params.put("consumerPassIdentifier", consumerPassId);
					DataLayerMgr.executeQuery("AUTHORIZE_PROMO", params, params);
					if(params.get("campaignId")==null||params.get("transToFreeTran")==null){
						resultAttributes.put("authResultCd", 'N');
						resultAttributes.put("authorityRespCd", 'N');
						if(params.get("campaignId")==null){
							resultAttributes.put("authorityRespDesc","No promotion campaign is assigned to the device");
						}else{
							resultAttributes.put("authorityRespDesc","No promotion campaign is associated with the card");
						}
						return AuthResultCode.DECLINED;
					}else{
						int nthVendFreeNum = ConvertUtils.getIntSafely(params.get("nthVendFreeNum"), -1);
						int transToFreeTran = ConvertUtils.getIntSafely(params.get("transToFreeTran"), nthVendFreeNum-1);
						long consumerAcctId = ConvertUtils.getLongSafely(params.get("consumerAcctId"), -1);
						resultAttributes.put("authAuthorityRefCd", params.get("campaignId"));//save campaignId to authAuthorityRefCd for refund
						if(consumerAcctId > 0) {
							resultAttributes.put("consumerAcctId", consumerAcctId);
							if(globalAccountId == null) {
								globalAccountId = ConvertUtils.convertSafely(Long.class, params.get("globalAccountId"), null);
								if(globalAccountId != null) { // Created it - let Store task know
									resultAttributes.put("globalAccountId", globalAccountId);
									resultAttributes.put("instance", 0);
								}
							}
						}
						if ("R".equals(params.get("authResultCd"))) {//partial auth declined
							resultAttributes.put("authResultCd", 'N');
							resultAttributes.put("authorityRespCd", 'N');
							resultAttributes.put("authorityRespDesc", "Partial auth not allowed. Insufficient Funds.");
							return AuthResultCode.DECLINED;
						}else if ("Y".equals(params.get("authResultCd"))) {
							int minorCurrencyFactor = getAttribute(step, AuthorityAttrEnum.ATTR_MINOR_CURRENCY_FACTOR, Integer.class, true);
							BigDecimal approvedAmountMajor = ConvertUtils.convertSafely(BigDecimal.class, params.get("approvedAuthAmount"), null);
							if(approvedAmountMajor==null){
								resultAttributes.put("authResultCd", 'N');
								resultAttributes.put("authorityRespCd", 'N');
								resultAttributes.put("authorityRespDesc", "Approved amount is not configured properly");
								return AuthResultCode.DECLINED;
							}
							resultAttributes.put("authorityRespCd", "Promotion free item for every "+nthVendFreeNum+" purchase");
							resultAttributes.put("authAuthorityTranCd", "Promotion");
							setClientText(resultAttributes, AuthResultCode.APPROVED, null, null, approvedAmountMajor);
							resultAttributes.put("authorityRespDesc", "Free purchase up to $" + ConvertUtils.formatObject(approvedAmountMajor, "NUMBER:#,##0.##"));
							resultAttributes.put("freeTransaction", true);
							BigInteger approvedAuthAmount=approvedAmountMajor.multiply(new BigDecimal(minorCurrencyFactor)).toBigInteger();
							if(approvedAuthAmount.compareTo(BigInteger.ZERO) == 1 && originalAmount.compareTo(approvedAuthAmount) == 1) {
								resultAttributes.put("approvedAmount", approvedAuthAmount);
								resultAttributes.put("authResultCd", 'P');
								return AuthResultCode.PARTIAL;
							}
							resultAttributes.put("authResultCd", 'Y');
							return AuthResultCode.APPROVED;
						}else{
							resultAttributes.put("authResultCd", 'N');
							resultAttributes.put("authorityRespCd", 'N');
							if (transToFreeTran > -1){
								resultAttributes.put("authorityRespDesc", transToFreeTran + " More Purchase for Free Purchase");
								resultAttributes.put(AuthorityAttrEnum.ATTR_OVERRIDE_CLIENT_TEXT_KEY.getValue(), "client.message." + language + ".auth.promotion.denied.not-yet-free");
								resultAttributes.put(AuthorityAttrEnum.ATTR_OVERRIDE_CLIENT_TEXT_PARAMS.getValue(), new Object[] { transToFreeTran });
							}else{
								resultAttributes.put("authorityRespDesc", " You have already reached maximum free Purchase count.");
								resultAttributes.put(AuthorityAttrEnum.ATTR_OVERRIDE_CLIENT_TEXT_KEY.getValue(), "client.message." + language + ".auth.promotion.denied.reached-max-count");
							}
							
							return AuthResultCode.DECLINED;
						}
					}
				case SALE:
					resultAttributes.put("authResultCd", 'Y');
					resultAttributes.put("authorityRespCd", "PROMOTION");
					resultAttributes.put("authorityRespDesc", "Settled by USAT");
					resultAttributes.put("settled", true);
					return AuthResultCode.APPROVED;
				case REFUND:
					tranId = getAttribute(step, AuthorityAttrEnum.ATTR_TRAN_ID, Number.class, true);
					params.put("tranId", tranId);
					DataLayerMgr.executeCall("REFUND_PROMO", params, true);
					resultAttributes.put("authResultCd", 'Y');
					resultAttributes.put("authorityRespCd", "PROMOTION");
					resultAttributes.put("authorityRespDesc", "Refunded by USAT");
					resultAttributes.put("settled", true);
					return AuthResultCode.APPROVED;
				default:
					throw new RetrySpecifiedServiceException("Action '" + action + "' NOT implemented", WorkRetryType.NO_RETRY);
			}
		} catch(Exception e) {
			log.error("Error processing promotion transaction", e);
			resultAttributes.put("authResultCd", 'F');
			resultAttributes.put("authorityRespCd", 'F');
			resultAttributes.put("authorityRespDesc", "Error: " + e.getMessage());
			return AuthResultCode.FAILED;
		}
	}

	@Override
	protected AuthResultCode processReversal(AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		Map<String, Object> resultAttributes = step.getResultAttributes();
		try {
			switch(action) {
				case AUTHORIZATION:					
					resultAttributes.put("authResultCd", 'Y');
					resultAttributes.put("authorityRespCd", '0');
					resultAttributes.put("authorityRespDesc", "No change to account");
					return AuthResultCode.APPROVED;
				default:
					log.warn("Reversal of Action '" + action + "' NOT implemented");
					return AuthResultCode.FAILED;
			}
		} catch(Exception e) {
			log.error("Error processing promotion transaction", e);
			resultAttributes.put("authResultCd", 'F');
			resultAttributes.put("authorityRespCd", 'F');
			resultAttributes.put("authorityRespDesc", "Error: " + e.getMessage());
			return AuthResultCode.FAILED;
		}
	}

}
