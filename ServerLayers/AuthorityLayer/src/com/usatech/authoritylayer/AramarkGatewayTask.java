/**
 *
 */
package com.usatech.authoritylayer;

import java.lang.reflect.UndeclaredThrowableException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;

import com.educationaramark.webservices.relayservice.POSResponse;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertUtils;
import simple.bean.MaskedString;
import simple.io.ByteArrayUtils;
import simple.io.Log;
import simple.text.StringUtils;

import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.aramark.AramarkException;
import com.usatech.aramark.WSClient;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAction;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.DeniedReason;
import com.usatech.layers.common.constants.MessageAttribute;
import com.usatech.layers.common.constants.PaymentMaskBRef;

/**
 * @author Brian S. Krug
 *
 */
public class AramarkGatewayTask extends AbstractAuthorityTask {
	private static final Log log = Log.getLog();
	//WSClient is NOT thread-safe so use ThreadLocal to avoid concurrent use problems
	protected final ThreadLocal<WSClient> wsClientLocal = new ThreadLocal<WSClient>() {
		@Override
		protected WSClient initialValue() {
			try {
				return new WSClient();
			} catch(AramarkException e) {
				throw new UndeclaredThrowableException(e);
			}
		}
	};
	protected static final String TIMESTAMP_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSSZ"; // yyyy-MM-dd'T'HH:mm:ss.SSSSSSS-04:00
	protected static final MathContext TRUNC = new MathContext(0,RoundingMode.DOWN);

	public AramarkGatewayTask() {
		super("aramark");
	}
	@Override
	protected AuthResultCode processRequest(AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		String remoteServerAddr;
		String operatorID;
		String account;
		String cardNumber;
		byte[] encryptionKey;
		byte[] encryptionKey2;
		BigInteger amount;
		int minorCurrencyFactor;
		boolean online;
		long transactionTime;
		String timeZoneGuid;
		String transactionID;
		try {
			remoteServerAddr = getAttribute(step, AuthorityAttrEnum.ATTR_REMOTE_SERVER_ADDRESS, String.class, true);
			operatorID = getAttribute(step, AuthorityAttrEnum.ATTR_MERCHANT_CD, String.class, false); // this should be null
			account = getAttribute(step, AuthorityAttrEnum.ATTR_TERMINAL_CD, String.class, true);
			AccountData accountData = new AccountData(step.getAttributes());
			cardNumber = accountData.getTrackData();
			if(StringUtils.isBlank(cardNumber)) {
				cardNumber = accountData.getPartData(PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER);
				if(StringUtils.isBlank(cardNumber))
					return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), null, "Account Number not provided");
			}
			encryptionKey = ByteArrayUtils.fromHex(getAttribute(step, AuthorityAttrEnum.ATTR_TERMINAL_ENCRYPTION_KEY, String.class, false));
			encryptionKey2 = ByteArrayUtils.fromHex(getAttribute(step, AuthorityAttrEnum.ATTR_TERMINAL_ENCRYPTION_KEY2, String.class, false));
			amount = getAttribute(step, AuthorityAttrEnum.ATTR_AMOUNT, BigInteger.class, true);
			minorCurrencyFactor  = getAttribute(step, AuthorityAttrEnum.ATTR_MINOR_CURRENCY_FACTOR, Integer.class, true);
			//online = getAttribute(step, AuthorityAttrEnum.ATTR_ONLINE, Boolean.class, true);
			//per Aramark request on 05/12/2016, all transactions should be sent as online transactions, with offline=FALSE 
			online = true;
			transactionTime = getAttribute(step, AuthorityAttrEnum.ATTR_AUTH_TIME, Long.class, true);
			timeZoneGuid = getAttribute(step, AuthorityAttrEnum.ATTR_TIMEZONE_GUID, String.class, false);
			transactionID = sanitizeTraceNumber(getAttribute(step, AuthorityAttrEnum.ATTR_TRACE_NUMBER, Long.class, true));
		} catch(IllegalArgumentException e) {
			log.error("Could not convert hex strings to bytes as needed", e);
			return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), null, "Data Conversion Error");
		} catch(AuthorityAttributeConversionException e) {
			return AuthResultCode.FAILED;
		}
		DateFormat dateFormat = (DateFormat)ConvertUtils.getFormat("DATE", TIMESTAMP_FORMAT);
		TimeZone tz;
		if(timeZoneGuid != null && timeZoneGuid.length() > 0 && (tz = TimeZone.getTimeZone(timeZoneGuid)) != null) {
			dateFormat.getCalendar().setTimeZone(tz);
		}
		String resultDate = dateFormat.format(new Date(transactionTime));
		String revenueCenterID = null;
		boolean enableEncryption = (encryptionKey != null && encryptionKey.length > 0 && encryptionKey2 != null && encryptionKey2.length > 0);
		POSResponse resp;
		Map<String, Object> resultAttributes = taskInfo.getStep().getResultAttributes();
		switch(action) {
			case AUTHORIZATION:
				try {
					resp = wsClientLocal.get().inquiry(remoteServerAddr, account, cardNumber, resultDate, operatorID, transactionID, revenueCenterID, String.valueOf(!online).toUpperCase(), String.valueOf(enableEncryption).toUpperCase(), encryptionKey == null ? null : new String(encryptionKey), encryptionKey2 == null ? null : new String(encryptionKey2));
				} catch(AramarkException e) {
					return populateExceptionResults(action, taskInfo.getStep().getResultAttributes(), e);
				} catch(UndeclaredThrowableException e) {
					if(e.getCause() instanceof AramarkException)
						return populateExceptionResults(action, taskInfo.getStep().getResultAttributes(), (AramarkException)e.getCause());
					return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), null, "Runtime Error: " + e.getMessage());
				}
				boolean allowPartialAuth = step.getAttributeSafely(AuthorityAttrEnum.ATTR_PARTIAL_AUTH_ALLOWED, Boolean.class, true);
				return populateAuthResults(resp, cardNumber, amount, minorCurrencyFactor, taskInfo.getStep().getResultAttributes(), allowPartialAuth);
			case SALE:
				try {
					// amount is in major currency
					BigDecimal majorAmount = new BigDecimal(amount).divide(BigDecimal.valueOf(minorCurrencyFactor), (int)Math.ceil(Math.log10(minorCurrencyFactor)), RoundingMode.HALF_UP);
					resp = wsClientLocal.get().decliningBalance(remoteServerAddr, account, cardNumber, resultDate, operatorID, transactionID, revenueCenterID, String.valueOf(!online).toUpperCase(), majorAmount.toPlainString(), String.valueOf(enableEncryption).toUpperCase(), new String(encryptionKey), new String(encryptionKey2));
				} catch(AramarkException e) {
					return populateExceptionResults(action, taskInfo.getStep().getResultAttributes(), e);
				} catch(UndeclaredThrowableException e) {
					if(e.getCause() instanceof AramarkException)
						return populateExceptionResults(action, taskInfo.getStep().getResultAttributes(), (AramarkException)e.getCause());
					return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), null, "Runtime Error: " + e.getMessage());
				}
				return populateResults(resp, amount, minorCurrencyFactor, taskInfo.getStep().getResultAttributes());
			case REFUND:
				try {
					// amount is in major currency
					BigDecimal majorAmount = new BigDecimal(amount).divide(BigDecimal.valueOf(minorCurrencyFactor), (int)Math.ceil(Math.log10(minorCurrencyFactor)), RoundingMode.HALF_UP);
					resp = wsClientLocal.get().credit(remoteServerAddr, account, cardNumber, resultDate, operatorID, transactionID, revenueCenterID, String.valueOf(!online).toUpperCase(), majorAmount.toPlainString(), String.valueOf(enableEncryption).toUpperCase(), new String(encryptionKey), new String(encryptionKey2));
				} catch(AramarkException e) {
					return populateExceptionResults(action, taskInfo.getStep().getResultAttributes(), e);
				} catch(UndeclaredThrowableException e) {
					if(e.getCause() instanceof AramarkException)
						return populateExceptionResults(action, taskInfo.getStep().getResultAttributes(), (AramarkException)e.getCause());
					return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), null, "Runtime Error: " + e.getMessage());
				}
				return populateResults(resp, amount, minorCurrencyFactor, taskInfo.getStep().getResultAttributes());
			case ADJUSTMENT:
				BigInteger originalAmount;
				try {
					originalAmount = getAttribute(step, AuthorityAttrEnum.ATTR_ORIGINAL_AMOUNT, BigInteger.class, true);
				} catch(AuthorityAttributeConversionException e) {
					return AuthResultCode.FAILED;
				}
				switch(subAction) {
					case SALE:
						BigInteger adjustAmount = amount.subtract(originalAmount);
						switch(adjustAmount.signum()) {
							case -1: // needs credit
								try {
									// amount is in major currency
									BigDecimal majorAmount = new BigDecimal(adjustAmount.abs()).divide(BigDecimal.valueOf(minorCurrencyFactor), (int)Math.ceil(Math.log10(minorCurrencyFactor)), RoundingMode.HALF_UP);
									resp = wsClientLocal.get().credit(remoteServerAddr, account, cardNumber, resultDate, operatorID, transactionID, revenueCenterID, String.valueOf(!online).toUpperCase(), majorAmount.toPlainString(), String.valueOf(enableEncryption).toUpperCase(), new String(encryptionKey), new String(encryptionKey2));
								} catch(AramarkException e) {
									return populateExceptionResults(action, taskInfo.getStep().getResultAttributes(), e);
								} catch(UndeclaredThrowableException e) {
									if(e.getCause() instanceof AramarkException)
										return populateExceptionResults(action, taskInfo.getStep().getResultAttributes(), (AramarkException)e.getCause());
									return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), null, "Runtime Error: " + e.getMessage());
								}
								return populateResults(resp, amount, minorCurrencyFactor, taskInfo.getStep().getResultAttributes());
							case 0: // nothing to do
								resultAttributes.put("authResultCd", 'Y');
								resultAttributes.put("authorityRespCd", 0);
								String text = "No change to account";
								resultAttributes.put("authorityRespDesc", text);
								resultAttributes.put("settled", true);
								return AuthResultCode.APPROVED;
							case 1: // needs debit
								try {
									// amount is in major currency
									BigDecimal majorAmount = new BigDecimal(adjustAmount).divide(BigDecimal.valueOf(minorCurrencyFactor), (int)Math.ceil(Math.log10(minorCurrencyFactor)), RoundingMode.HALF_UP);
									resp = wsClientLocal.get().decliningBalance(remoteServerAddr, account, cardNumber, resultDate, operatorID, transactionID, revenueCenterID, String.valueOf(!online).toUpperCase(), majorAmount.toPlainString(), String.valueOf(enableEncryption).toUpperCase(), new String(encryptionKey), new String(encryptionKey2));
								} catch(AramarkException e) {
									return populateExceptionResults(action, taskInfo.getStep().getResultAttributes(), e);
								} catch(UndeclaredThrowableException e) {
									if(e.getCause() instanceof AramarkException)
										return populateExceptionResults(action, taskInfo.getStep().getResultAttributes(), (AramarkException)e.getCause());
									return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), null, "Runtime Error: " + e.getMessage());
								}
								return populateResults(resp, amount, minorCurrencyFactor, taskInfo.getStep().getResultAttributes());
							default:
								throw new RetrySpecifiedServiceException("Illegal State: BigInteger.signum() returned " + adjustAmount.signum(),WorkRetryType.NO_RETRY);
						}
					case REFUND:
						adjustAmount = amount.subtract(originalAmount);
						switch(adjustAmount.signum()) {
							case -1: // needs debit
								try {
									// amount is in major currency
									BigDecimal majorAmount = new BigDecimal(adjustAmount.abs()).divide(BigDecimal.valueOf(minorCurrencyFactor), (int)Math.ceil(Math.log10(minorCurrencyFactor)), RoundingMode.HALF_UP);
									resp = wsClientLocal.get().decliningBalance(remoteServerAddr, account, cardNumber, resultDate, operatorID, transactionID, revenueCenterID, String.valueOf(!online).toUpperCase(), majorAmount.toPlainString(), String.valueOf(enableEncryption).toUpperCase(), new String(encryptionKey), new String(encryptionKey2));
								} catch(AramarkException e) {
									return populateExceptionResults(action, taskInfo.getStep().getResultAttributes(), e);
								} catch(UndeclaredThrowableException e) {
									if(e.getCause() instanceof AramarkException)
										return populateExceptionResults(action, taskInfo.getStep().getResultAttributes(), (AramarkException)e.getCause());
									return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), null, "Runtime Error: " + e.getMessage());
								}
								return populateResults(resp, amount, minorCurrencyFactor, taskInfo.getStep().getResultAttributes());
							case 0: // nothing to do
								resultAttributes.put("authResultCd", 'Y');
								resultAttributes.put("authorityRespCd", 0);
								String text = "No change to account";
								resultAttributes.put("authorityRespDesc", text);
								resultAttributes.put("settled", true);
								return AuthResultCode.APPROVED;
							case 1: // needs credit
								try {
									// amount is in major currency
									BigDecimal majorAmount = new BigDecimal(adjustAmount).divide(BigDecimal.valueOf(minorCurrencyFactor), (int)Math.ceil(Math.log10(minorCurrencyFactor)), RoundingMode.HALF_UP);
									resp = wsClientLocal.get().credit(remoteServerAddr, account, cardNumber, resultDate, operatorID, transactionID, revenueCenterID, String.valueOf(!online).toUpperCase(), majorAmount.toPlainString(), String.valueOf(enableEncryption).toUpperCase(), new String(encryptionKey), new String(encryptionKey2));
								} catch(AramarkException e) {
									return populateExceptionResults(action, taskInfo.getStep().getResultAttributes(), e);
								} catch(UndeclaredThrowableException e) {
									if(e.getCause() instanceof AramarkException)
										return populateExceptionResults(action, taskInfo.getStep().getResultAttributes(), (AramarkException)e.getCause());
									return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), null, "Runtime Error: " + e.getMessage());
								}
								return populateResults(resp, amount, minorCurrencyFactor, taskInfo.getStep().getResultAttributes());
							default:
								throw new RetrySpecifiedServiceException("Illegal State: BigInteger.signum() returned " + adjustAmount.signum(),WorkRetryType.NO_RETRY);
						}
					default:
						throw new RetrySpecifiedServiceException("Sub-action '" + subAction + "' for Action '" + action + "' NOT implemented",WorkRetryType.NO_RETRY);
				}
			default:
				throw new RetrySpecifiedServiceException("Action '" + action + "' NOT implemented",WorkRetryType.NO_RETRY);
		}
	}

	/**
	 * @see com.usatech.authoritylayer.AbstractAuthorityTask#processReversal(com.usatech.layers.common.constants.AuthorityAction, com.usatech.app.MessageChainTaskInfo)
	 */
	@Override
	protected AuthResultCode processReversal(AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo) throws ServiceException {
		Map<String, Object> resultAttributes = taskInfo.getStep().getResultAttributes();
		switch(action) {
			case AUTHORIZATION:
				// nothing to do b/c auth is a balance check
				resultAttributes.put("authResultCd", 'Y');
				resultAttributes.put("authorityRespCd", 0);
				String text = "No change to account";
				resultAttributes.put("authorityRespDesc", text);
				return AuthResultCode.APPROVED;
			default:
				text = "Reversal of Action '" + action + "' NOT implemented"; 
				log.warn(text);
				resultAttributes.put("authorityRespDesc", text);
				return AuthResultCode.FAILED;
		}
	}
	protected AuthResultCode populateExceptionResults(AuthorityAction action, Map<String, Object> resultAttributes, AramarkException e) {
		if(e.getStatusCode() == null || e.getStatusCode().trim().length() == 0) {
			return populateFailedResults(action, resultAttributes, null, e.getDescription());
		}
		resultAttributes.put("authResultCd", 'N');
		resultAttributes.put("authorityRespCd", e.getStatusCode());
		resultAttributes.put("authorityRespDesc", e.getResponseText());
		// XXX: how about e.getResponseMessage(), e.getStatus() ???
		return AuthResultCode.DECLINED;
	}
	protected AuthResultCode populateResults(POSResponse response, BigInteger amount, int minorCurrencyFactor, Map<String, Object> resultAttributes) {
		AuthResultCode authResultCode;
		String statusCode = response.getTransaction().getStatusCode();
		BigInteger balanceAmount;
		BigDecimal bd = ConvertUtils.convertSafely(BigDecimal.class, response.getTransaction().getCustomer().getAmount(), null);
		//I believe approvedAmount is in major currency (not minor like other gateways)
		if(bd == null)
			balanceAmount = BigInteger.ZERO;
		else
			balanceAmount = bd.multiply(BigDecimal.valueOf(minorCurrencyFactor), TRUNC).toBigInteger();
		if("0".equals(statusCode) || "100".equals(statusCode)) {
			resultAttributes.put("authResultCd", 'Y');
			resultAttributes.put("approvedAmount", amount);
			resultAttributes.put("balanceAmount", balanceAmount);
			resultAttributes.put("settled", true);
			authResultCode = AuthResultCode.APPROVED;
		} else if("-6".equals(statusCode)) { // original POSM code made a distinction on this status Code (insufficent funds, but I don't know why)
			resultAttributes.put("authResultCd", 'N');
			authResultCode = AuthResultCode.DECLINED;
		} else {
			resultAttributes.put("authResultCd", 'N');
			authResultCode = AuthResultCode.DECLINED;
		}
		resultAttributes.put("authorityRespCd", statusCode);
		resultAttributes.put("authorityRespDesc", response.getTransaction().getResponseText());
		return authResultCode;
	}

	protected AuthResultCode populateAuthResults(POSResponse response, String cardNumber, BigInteger requestedAmount, int minorCurrencyFactor, Map<String, Object> resultAttributes, boolean allowPartialAuth) {
		AuthResultCode authResultCode;
		String statusCode = response.getTransaction().getStatusCode();
		BigInteger balanceAmount;
		BigDecimal bd = ConvertUtils.convertSafely(BigDecimal.class, response.getTransaction().getCustomer().getAmount(), null);
		//I believe approvedAmount is in major currency (not minor like other gateways)
		if(bd == null)
			balanceAmount = BigInteger.ZERO;
		else
			balanceAmount = bd.multiply(BigDecimal.valueOf(minorCurrencyFactor), TRUNC).toBigInteger();
		if("0".equals(statusCode) || "100".equals(statusCode)) {
			if(balanceAmount.compareTo(requestedAmount) >= 0) {
				resultAttributes.put("authResultCd", 'Y');
				resultAttributes.put("approvedAmount", requestedAmount);
				resultAttributes.put("balanceAmount", balanceAmount);
				authResultCode = AuthResultCode.APPROVED;
				setClientText(resultAttributes, authResultCode, null, null, bd, null, response.getTransaction().getResponseText());
			} else if(balanceAmount.compareTo(BigInteger.ZERO) > 0 && allowPartialAuth) {
				resultAttributes.put("authResultCd", 'P');
				resultAttributes.put("approvedAmount", balanceAmount);
				resultAttributes.put("balanceAmount", balanceAmount);
				authResultCode = AuthResultCode.PARTIAL;
				setClientText(resultAttributes, authResultCode, null, null, bd, null, response.getTransaction().getResponseText());
			} else {
				resultAttributes.put("authResultCd", 'N');
				resultAttributes.put("authorityRespDesc", "Account balance is " + bd);
				authResultCode = AuthResultCode.DECLINED;
				setClientText(resultAttributes, authResultCode, DeniedReason.INSUFFICIENT_FUNDS, response.getTransaction().getResponseText(), bd);
			}
			resultAttributes.put("authAccountData", new MaskedString(cardNumber));
		} else if("-6".equals(statusCode)) { // original POSM code made a distinction on this status Code (insufficent funds), but I don't know why)
			resultAttributes.put("authResultCd", 'N');
			authResultCode = AuthResultCode.DECLINED;
			setClientText(resultAttributes, authResultCode, DeniedReason.INSUFFICIENT_FUNDS, response.getTransaction().getResponseText());
		} else {
			resultAttributes.put("authResultCd", 'N');
			authResultCode = AuthResultCode.DECLINED;
			setClientText(resultAttributes, authResultCode, null, response.getTransaction().getResponseText());
		}
		resultAttributes.put("authHoldUsed", false);
		resultAttributes.put("authorityRespCd", statusCode);
		String responseText = response.getTransaction().getResponseText();
		if(responseText != null && (responseText=responseText.trim()).length() > 0)
			resultAttributes.put("authorityRespDesc", responseText);
		return authResultCode;
	}

	protected static String sanitizeTraceNumber(long traceNumber) {
		return String.valueOf(1 + (Math.abs(traceNumber) % 999999)); //1 - 999999
	}

	@Override
	protected String[] getAdditionalPreActionLogAttributes() {
		return new String[] {
			MessageAttribute.REMOTE_SERVER_ADDR.getValue()
		};
	}
}
