package com.usatech.authoritylayer;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import com.usatech.iso8583.ISO8583Request;
import com.usatech.iso8583.transaction.ISO8583Transaction;
import com.usatech.iso8583.transaction.ISO8583TransactionManager;

/** This transaction manager doesn't persist any data, but just provides the necessary functionality to work
 * @author Brian S. Krug
 *
 */
public class UnbackedTransactionManager implements ISO8583TransactionManager {
	protected static final AtomicInteger transactionIdGenerator = new AtomicInteger();
	@Override
	public void end(ISO8583Transaction transaction) {
		//do nothing
	}

	@Override
	public List<ISO8583Transaction> getNeedsRecovery(String interchangeName) {
		return Collections.emptyList();
	}

	@Override
	public List<ISO8583Transaction> getPending(String interchangeName) {
		return Collections.emptyList();
	}

	@Override
	public ISO8583Transaction start(final ISO8583Request request, final String interchangeName) {
		final int transactionId = transactionIdGenerator.incrementAndGet();
		return new ISO8583Transaction() {
			protected int state;
			@Override
			public void setState(int state) {
				this.state = state;
			}
			
			@Override
			public int getTransactionID() {
				return transactionId;
			}
			
			@Override
			public int getState() {
				return state;
			}
			
			@Override
			public ISO8583Request getRequest() {
				return request;
			}
			
			@Override
			public String getInterchangeName() {
				return interchangeName;
			}
			
			public String toString()
			{
				StringBuilder sb = new StringBuilder();
				sb.append("Transaction[id=");
				sb.append(transactionId);
				sb.append(" state=");
				sb.append(ISO8583Transaction.STATE_DESC[state]);
				sb.append(" interchange=");
				sb.append(interchangeName);
				//sb.append(" request=");
				//sb.append(request);
				//sb.append(" response=");
				//sb.append(response);
				sb.append("]");
				return sb.toString();
			}			
		};
	}
}
