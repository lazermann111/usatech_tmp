/**
 *
 */
package com.usatech.authoritylayer;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.UndeclaredThrowableException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;

import org.xml.sax.SAXException;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.IOUtils;
import simple.io.Log;
import simple.io.PassThroughWriter;
import simple.text.StringUtils;
import simple.util.CompositeMap;
import simple.xml.MapXMLLoader;
import simple.xml.MapXMLLoader.ParentMap;

import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAction;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.DeniedReason;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.MessageAttrEnum;
import com.usatech.layers.common.constants.MessageAttribute;
import com.usatech.layers.common.constants.PaymentMaskBRef;

/**
 * @author Brian S. Krug
 *
 */
public class AprivaAuthorityTask extends AbstractAuthorityTask {
	private static final Log log = Log.getLog();
	
	protected static final Pattern EXP_DATE_PATTERN = Pattern.compile("(\\d\\d)(\\d\\d)");
	protected static final DateFormat DATE_FORMAT = (DateFormat) ConvertUtils.getFormat("DATE", "MM/dd");
	protected static final DateFormat TIME_FORMAT = (DateFormat) ConvertUtils.getFormat("DATE", "HH:mm:ss");
	protected static final DateFormat DATETIME_FORMAT = (DateFormat) ConvertUtils.getFormat("DATE", "MM/dd HH:mm:ss");

	protected Charset charset = Charset.forName("UTF-8");
	protected SocketFactory socketFactory = SSLSocketFactory.getDefault();

	protected String host;
	protected int port;
	protected int connectTimeout = 5000;
	protected int connectRetries = 2;
	protected int responseTimeout = 15000;
	protected int maxRetryAttempts = 5;
	protected boolean socketReused = false;
	protected BigDecimal defaultMinimumBalanceAmount = new BigDecimal("1.00");
	protected boolean returnFailedOnFailedReversal = false;
	protected final Map<String, BigDecimal> minimumBalanceAmountByMerchant = new LinkedHashMap<String, BigDecimal>();

	//SSLSocket is NOT thread-safe so use ThreadLocal to avoid concurrent use problems
	protected final ThreadLocal<Socket> socketLocal = new ThreadLocal<Socket>() {
		@Override
		protected Socket initialValue() {
			try {
				return socketFactory.createSocket();
			} catch(IOException e) {
				throw new UndeclaredThrowableException(e);
			}
		}
	};

	public AprivaAuthorityTask() {
		super("Apriva");
	}

	protected Socket obtainSocket() throws IOException {
		int retries = getConnectRetries();
		return obtainSocket(retries);
	}

	protected Socket obtainSocket(int retries) throws IOException {
		Socket socket;
		try {
			socket = socketLocal.get();
		} catch(UndeclaredThrowableException e) {
			if(e.getCause() instanceof IOException)
				throw (IOException) e.getCause();
			throw new IOException(e);
		}

		try {
			if(!socket.isConnected()) {
				if(!socket.isBound() || isSocketReused())
					socket.connect(new InetSocketAddress(getHost(), getPort()), getConnectTimeout());
				else {
					socket.close();
					socketLocal.remove();
					return obtainSocket();
				}
			} else if(socket.isClosed()) {
				socketLocal.remove();
				return obtainSocket();
			} else if(socket.getInetAddress() == null || socket.getPort() != getPort() || !IOUtils.isInetAddressSame(getHost(), socket.getInetAddress(), false)) {
				socket.close();
				socketLocal.remove();
				return obtainSocket();
			} else
					socket.setSoTimeout(getResponseTimeout());
		} catch(IOException e) {
			if(retries < 1)
				throw e;
			socketLocal.remove();
			return obtainSocket(--retries);
		}

		return socket;
	}

	protected void writeElement(Writer writer, String name, String value) throws IOException {
		writer.write('<');
		writer.write(name);
		writer.write('>');
		if(!StringUtils.isBlank(value))
			writer.write(value);
		writer.write('<');
		writer.write('/');
		writer.write(name);
		writer.write('>');
	}

	public static <T> T getResponseValue(ParentMap response, Class<T> type, String... names) throws ConvertException {
		return ConvertUtils.convert(type, getResponseValue(response, names));
	}

	public static Object getResponseValue(ParentMap response, String... names) {
		if(names == null || names.length == 0)
			return response.getText();
		if(names.length == 1) {
			ParentMap[] nodes = response.getChildren(names[0]);
			switch(nodes.length) {
				case 0:
					return null;
				case 1:
					return nodes[0].getText();
				default:
					String[] values = new String[nodes.length];
					for(int i = 0; i < nodes.length; i++)
						values[i] = nodes[i].getText();
					return values;
			}
		}
		ParentMap node = response;
		for(int i = 0; i < names.length; i++) {
			ParentMap[] nodes = node.getChildren(names[i]);
			switch(nodes.length) {
				case 0:
					return null;
				case 1:
					node = nodes[0];
					break;
				default:
					List<String> values = new ArrayList<String>();
					addResponseValues(nodes, values, names, i + 1);
					return values.toArray(new String[values.size()]);
			}
		}
		return node.getText();
	}

	public static void addResponseValues(ParentMap[] nodes, List<String> values, String[] names, int offset) {
		for(int i = 0; i < nodes.length; i++) {
			if(names.length <= offset)
				values.add(nodes[i].getText());
			else
				addResponseValues(nodes[i].getChildren(names[offset]), values, names, offset + 1);
		}
	}

	// Copy this because we are changing how processReversal works
	protected AuthResultCode processInternal(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		Map<String, Object> attributes = taskInfo.getStep().getAttributes();
		ProcessingUtils.logAttributes(log, "Starting ", attributes, getPreActionLogAttributes(), getAdditionalPreActionLogAttributes());
		AuthorityAction action = null;
		String currencyCd = null;
		AuthorityAction subAction;
		try {
			action = getAttribute(step, AuthorityAttrEnum.ATTR_ACTION_TYPE, AuthorityAction.class, true);
			currencyCd = getAttribute(step, AuthorityAttrEnum.ATTR_CURRENCY_CD, String.class, true);
		} catch(AuthorityAttributeConversionException e) {
			return populateFailedResults(action, step.getResultAttributes(), DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Internal Error");
		}
		if(supportedCurrencies != null && !supportedCurrencies.contains(currencyCd)) {
			log.info("Currency '" + currencyCd + " is not supported by " + this);
			return populateFailedResults(null, step.getResultAttributes(), DeniedReason.UNSUPPORTED_CURRENCY, "Currency not supported");
		}
		switch(action) {
			case AUTHORIZATION:
				// auth is a balance check only, there's no need to reverse auth for Apriva
				// check staleness
				int maxStaleness = getMaxAuthStaleness();
				if(maxStaleness > 0) {
					try {
						long authTime = getAttribute(step, AuthorityAttrEnum.ATTR_AUTH_TIME, Long.class, true);
						if(authTime + maxStaleness < System.currentTimeMillis()) {
							log.warn("Auth request against " + taskInfo.getServiceName() + " initiated at " + authTime + " has become stale");
							return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.INTERNAL_TIMEOUT, "Auth Expired");
						}
					} catch (AuthorityAttributeConversionException e) {
						log.warn("Unable to check staleness of message, failed to get ATTR_AUTH_TIME", e);
						return populateFailedResults(action, step.getResultAttributes(), DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Internal Error");
					}
				}
				subAction = null;
				break;
			case SALE:
			case REFUND:
			case SETTLEMENT:
			case SETTLEMENT_RETRY: // check redelivery and reverse if true
				if(taskInfo.isRedelivered()) {
					AuthResultCode reversalResult = processReversal(action, null, taskInfo);
					// reversal sale reversal should always return approved or declined?
					if(reversalResult != null)
						// what if failure?
						return reversalResult;
				}
				subAction = null;
				break;
			case ADJUSTMENT:
				try {
					subAction = getAttribute(step, AuthorityAttrEnum.ATTR_ACTION_SUB_TYPE, AuthorityAction.class, true);
				} catch(AuthorityAttributeConversionException e) {
					return AuthResultCode.FAILED;
				}
				switch(subAction) {
					case SALE:
					case REFUND: // okay
						break;
					default:
						throw new RetrySpecifiedServiceException("Sub-action type '" + subAction + "' for action type '" + action + "' NOT implemented", WorkRetryType.NO_RETRY);
				}
				if(taskInfo.isRedelivered()) {
					AuthResultCode reversalResult = processReversal(action, subAction, taskInfo);
					if(reversalResult != null)
						return reversalResult;
				}
				break;
			default:
				throw new RetrySpecifiedServiceException("Action type '" + action + "' NOT implemented", WorkRetryType.NO_RETRY);
		}
		AuthResultCode result = processRequest(action, subAction, taskInfo);
		CompositeMap<String, Object> allAttributes = new CompositeMap<>(taskInfo.getStep().getAttributes(), taskInfo.getStep().getResultAttributes());
		ProcessingUtils.logAttributes(log, "Finished, result=" + result.getDescription() + "; ", allAttributes, getPostActionLogAttributes(), getAdditionalPostActionLogAttributes());
		return result;
	}

	@SuppressWarnings("resource")
	@Override
	protected AuthResultCode processRequest(AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		AccountData accountData;
		EntryType entryType;
		Date transactionTs;
		String timeZoneGuid;
		BigInteger amount;
		int minorCurrencyFactor;
		String deviceAddress;
		int requestStan;
		String transactionType;
		String processingCode;
		boolean authHoldUsed;
		int priorAttempts;
		String deviceTranCd;
		String globalTranCd;
		String merchantCd;
		boolean allowPartialAuth = false;
		try {
			accountData = new AccountData(step.getAttributes());
			entryType = getAttribute(step, AuthorityAttrEnum.ATTR_ENTRY_TYPE, EntryType.class, true);
			amount = getAttribute(step, AuthorityAttrEnum.ATTR_AMOUNT, BigInteger.class, true);
			minorCurrencyFactor  = getAttribute(step, AuthorityAttrEnum.ATTR_MINOR_CURRENCY_FACTOR, Integer.class, true);
			transactionTs = new Date(getAttribute(step, AuthorityAttrEnum.ATTR_AUTH_TIME, Long.class, true));
			timeZoneGuid = getAttribute(step, AuthorityAttrEnum.ATTR_TIMEZONE_GUID, String.class, false);
			deviceAddress = getAttribute(step, MessageAttrEnum.ATTR_DEVICE_NAME, String.class, true);
			priorAttempts = ConvertUtils.getInt(getAttribute(step, AuthorityAttrEnum.ATTR_PRIOR_ATTEMPTS, Object.class, false), 0);
			deviceTranCd = getAttribute(step, AuthorityAttrEnum.ATTR_DEVICE_TRAN_CD, String.class, true);
			merchantCd = getAttribute(step, AuthorityAttrEnum.ATTR_MERCHANT_CD, String.class, false);
			/*
			 * for credit:
			 int serviceType = getAttribute(step, AuthorityAttrEnum.ATTR_SERVICE_TYPE, Integer.class, true);
			 switch(serviceType) {
					case 1:// balance-based account
						out.write("StoredValue");
						break;
					case 4: // hidden balance account
						out.write("Credit");
						break;
					default:
						return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.CONFIGURATION_ISSUE, "Service Type " + serviceType + " is not supported");
				}
				
			 transactionType = "Credit";
			 authHoldUsed = true;
			 switch(action) {
				case AUTHORIZATION:
					processingCode = "PreAuthorization";
					break;
				case SALE:
					processingCode = "CompletionAuthorization";
					break;
				case REVERSAL:
					processingCode = "Reversal";
					break;
				case SETTLEMENT:
				case SETTLEMENT_RETRY:
					processingCode = "HostCapture";					
					break;
				default:
					return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Action '" + action + "' is not supported");
			}
			 * 
			 */
			transactionType = "StoredValue";
			authHoldUsed = false;
			switch(action) {
				case AUTHORIZATION:
					processingCode = "InquireCurrentBalance";
					allowPartialAuth = step.getAttributeSafely(AuthorityAttrEnum.ATTR_PARTIAL_AUTH_ALLOWED, Boolean.class, true);
					break;
				case SALE:
					processingCode = "Redemption";
					break;
				case SETTLEMENT:
				case SETTLEMENT_RETRY:
					processingCode = "HostCapture";
					break;
				case REFUND:
					processingCode = "AddValue";
					break;
				case REVERSAL:
					processingCode = "SaleReversal";
					transactionType = "Credit";
					break;
				default:
					return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Action '" + action + "' is not supported");
			}
			requestStan = sanitizeDeviceTranCd(deviceTranCd, action, authHoldUsed, priorAttempts);
			globalTranCd = new StringBuilder(3 + deviceAddress.length() + deviceTranCd.length()).append("A-").append(deviceAddress).append('-').append(deviceTranCd.replace(':', '-')).toString();
		} catch(IllegalArgumentException e) {
			log.error("Could not convert hex strings to bytes as needed", e);
			return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), null, "Data Conversion Error");
		} catch(ConvertException | AuthorityAttributeConversionException e) {
			log.error("Could not convert device tran cd", e);
			return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), null, "Data Conversion Error");
		}
		
		Socket socket;
		try {
			socket = obtainSocket();
		} catch(IOException e) {
			log.warn("Could not connect to " + getHost() + ":" + getPort(), e);
			if(action == AuthorityAction.AUTHORIZATION) {
				// We could throw RetrySpecifiedServiceException to retry the auth, but obtainSocket already does reconnect so it should be pointless
				return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.PROCESSOR_ERROR, "Could not connect to Apriva network");
			} else if (action == AuthorityAction.REVERSAL) {
				// Continue to attempt the reversal since that means a sale was sent with the possiblity Apriva received it
				if(taskInfo.getRetryCount() >= getMaxRetryAttempts()) {
					if(action == AuthorityAction.REVERSAL && returnFailedOnFailedReversal) {
						// This will result in a sale in the TRANSACTION_INCOMPLETE_ERROR state and require manual intervention
						return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.PROCESSOR_ERROR, "Could not connect to Apriva network");
					} else { 
						// Returning declined here could lead to duplicate charges because we have not reversed a potential sale and POSM will retry declined sales.
						// However the risk is low and TRANSACTION_INCOMPLETE_ERROR transactions are a nuisance, approved by Denis 6/15/16
						return populateDeclinedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.PROCESSOR_ERROR, "Could not connect to Apriva network");
					}
				} else {
					throw new RetrySpecifiedServiceException("Could not connect to Apriva network", e, WorkRetryType.NONBLOCKING_RETRY);
				}
			} else {
				// there's no reason to retry/reverse this, as a connect error would mean there's no possibility Apriva received it
				return populateDeclinedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.PROCESSOR_ERROR, "Could not connect to Apriva network");
			}
		}
		
		BigDecimal amountMajor = InteractionUtils.toMajorCurrency(amount, minorCurrencyFactor);
		boolean okay = false;
		ParentMap[] responses;
		Writer out;
		StringWriter copy = null;
		try {
			try {
				out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), getCharset()));
				if(log.isDebugEnabled()) {
					copy = new StringWriter(1024);
					out = new PassThroughWriter(out, copy);
				}
				
				out.write("<AprivaPosXml DeviceAddress=\"");
				out.write(deviceAddress);
				out.write("\"><");
				out.write(transactionType);
				out.write(" MessageType=\"Request\" Version=\"5.0\" ProcessingCode=\"");
				out.write(processingCode);
				out.write("\">");
				writeElement(out, "Stan", String.valueOf(requestStan));
				out.write("<ReportingData>");
				writeElement(out, "RDetail1", globalTranCd);
				out.write("</ReportingData>");
				
				if(action != AuthorityAction.REVERSAL) {
					if (entryType == EntryType.CONTACTLESS || entryType == EntryType.ISIS || entryType.getFallbackEntryType() == EntryType.CONTACTLESS) {
							if(!StringUtils.isBlank(accountData.getTrackData()))
								writeElement(out, "Track2", accountData.getTrackData());
							else if(!StringUtils.isBlank(accountData.getPartData(PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER)))
								writeElement(out, "Track2", accountData.getPartData(PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER));
							else
								return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.INVALID_INPUT, "No account data provided");
							writeElement(out, "EntryMode", "Track2");
							writeElement(out, "EntryModeType", "Contactless");
					} else if (entryType == EntryType.MANUAL || entryType == EntryType.TOKEN) {
							if(!StringUtils.isBlank(accountData.getPartData(PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER)))
								writeElement(out, "AccountNumber", MessageResponseUtils.getCardNumber(accountData.getPartData(PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER)));
							else if(!StringUtils.isBlank(accountData.getTrackData()))
								writeElement(out, "AccountNumber", MessageResponseUtils.getCardNumber(accountData.getTrackData()));
							else
								return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.INVALID_INPUT, "No account data provided");
							String expDate = accountData.getPartData(PaymentMaskBRef.EXPIRATION_DATE);
							if(!StringUtils.isBlank(expDate)) {
								out.write("<ExpireDate>");
								Matcher matcher = EXP_DATE_PATTERN.matcher(expDate.trim());
								if(matcher.matches()) {
									out.write(matcher.group(1));
									out.write('/');
									out.write(matcher.group(2));
								}
								out.write("</ExpireDate>");
							}
							writeElement(out, "EntryMode", "Manual");
							writeElement(out, "EntryModeType", "Standard");
					} else {
							if(!StringUtils.isBlank(accountData.getTrackData()))
								writeElement(out, "Track2", accountData.getTrackData());
							else if(!StringUtils.isBlank(accountData.getPartData(PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER)))
								writeElement(out, "Track2", accountData.getPartData(PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER));
							else
								return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.INVALID_INPUT, "No account data provided");
							writeElement(out, "EntryMode", "Track2");
							writeElement(out, "EntryModeType", "Standard");
					}
					if(!processingCode.equals("InquireCurrentBalance"))
						writeElement(out, "Amount", amountMajor.toPlainString());
					TimeZone tz;
					if(timeZoneGuid != null && timeZoneGuid.length() > 0 && (tz = TimeZone.getTimeZone(timeZoneGuid)) != null) {
						DATE_FORMAT.getCalendar().setTimeZone(tz);
						TIME_FORMAT.getCalendar().setTimeZone(tz);
					}
					writeElement(out, "TransactionDate", DATE_FORMAT.format(transactionTs));
					writeElement(out, "TransactionTime", TIME_FORMAT.format(transactionTs));
					// XXX: we may need BatchNumber
				}
				out.write("</");
				out.write(transactionType);
				out.write("></AprivaPosXml>");
				out.flush();
			} catch(IOException e) {
				log.warn("Could not write to the socket", e);
				if(taskInfo.getRetryCount() >= getMaxRetryAttempts()) {
					if(action == AuthorityAction.AUTHORIZATION) {
						// This should have been failed by the stale auth check but fail it here just in case
						return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.PROCESSOR_ERROR, "Could not write to the socket");
					} else if (action == AuthorityAction.REVERSAL && returnFailedOnFailedReversal) {
						// This will result in a sale in the TRANSACTION_INCOMPLETE_ERROR state and require manual intervention
						return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.PROCESSOR_ERROR, "Could not write to the socket");
					} else { 
						// Returning declined here could lead to duplicate charges because we have not reversed a potential sale and POSM will retry declined sales.
						// However the risk is low and TRANSACTION_INCOMPLETE_ERROR transactions are a nuisance, approved by Denis 6/15/16
						return populateDeclinedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.PROCESSOR_ERROR, "Could not write to the socket");
					}
				} else {
					// Retry auth, sale, or reversal (for this exception case). Auths will fail when they become stale.
					throw new RetrySpecifiedServiceException("Could not write to the socket", e, WorkRetryType.NONBLOCKING_RETRY);
				}
			}
			
			if(log.isDebugEnabled() && copy != null) {
				log.debug("Sent request: " + copy.toString());
			}
			
			try {
				MapXMLLoader xmlLoader = new MapXMLLoader();
				InputStream in = socket.getInputStream();
				if(log.isDebugEnabled()) {
					ByteArrayOutputStream baos = new ByteArrayOutputStream(1024);
					IOUtils.copy(in, baos);
					byte[] buffer = baos.toByteArray();
					in = new ByteArrayInputStream(buffer);
					log.debug("Received response of " + buffer.length + " bytes: " + new String(buffer));
				}
				ParentMap result = xmlLoader.load(in);
				// result.getTag() == "AprivaPosXml"
				responses = result.getChildren();
				okay = true;
			} catch(IOException e) {
				log.warn("Could not read from the socket", e);
				if(taskInfo.getRetryCount() >= getMaxRetryAttempts()) {
					if(action == AuthorityAction.AUTHORIZATION) {
						// This should have been failed by the stale auth check but fail it here just in case
						return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.PROCESSOR_ERROR, "Could not read from the socket");
					} else if (action == AuthorityAction.REVERSAL && returnFailedOnFailedReversal) {
						// This will result in a sale in the TRANSACTION_INCOMPLETE_ERROR state and require manual intervention
						return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.PROCESSOR_ERROR, "Could not read from the socket");
					} else { 
						// Returning declined here could lead to duplicate charges because we have not reversed a potential sale and POSM will retry declined sales.
						// However the risk is low and TRANSACTION_INCOMPLETE_ERROR transactions are a nuisance, approved by Denis 6/15/16
						return populateDeclinedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.PROCESSOR_ERROR, "Could not read from the socket");
					}
				} else {
					// Retry auth, sale, or reversal (for this exception case). Auths will fail when they become stale.
					throw new RetrySpecifiedServiceException("Could not connect to Apriva network", e, WorkRetryType.NONBLOCKING_RETRY);
				}
			} catch(SAXException e) {
				log.warn("Could not parse response", e);
				//return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.INVALID_RESPONSE, "Could not parse response");
				if(taskInfo.getRetryCount() >= getMaxRetryAttempts()) {
					if(action == AuthorityAction.AUTHORIZATION) {
						// This should have been failed by the stale auth check but fail it here just in case
						return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.PROCESSOR_ERROR, "Could not parse response");
					} else if (action == AuthorityAction.REVERSAL && returnFailedOnFailedReversal) {
						// This will result in a sale in the TRANSACTION_INCOMPLETE_ERROR state and require manual intervention
						return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.PROCESSOR_ERROR, "Could not parse response");
					} else { 
						// Returning declined here could lead to duplicate charges because we have not reversed a potential sale and POSM will retry declined sales.
						// However the risk is low and TRANSACTION_INCOMPLETE_ERROR transactions are a nuisance, approved by Denis 6/15/16
						return populateDeclinedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.PROCESSOR_ERROR, "Could not parse response");
					}
				} else {
					// Retry auth, sale, or reversal (for this exception case). Auths will fail when they become stale.
					throw new RetrySpecifiedServiceException("Could not parse response", e, WorkRetryType.NONBLOCKING_RETRY);
				}
			}
		} catch(RetrySpecifiedServiceException e) {
			throw e;
		} catch(Throwable e) {
			log.error("Caught unexpected exception while communicating with Apriva", e);
			throw new ServiceException(e);
		} finally {
			if(!okay)
				try {
					socket.close();
				} catch(IOException e) {
					// ignore
				}
		}
		
		step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTH_HOLD_USED, authHoldUsed);
		if(responses == null || responses.length != 1) {
			return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Response did not contain expected content");
		}
		
		// double-check stan
		Integer responseStan;
		String responseCode;
		String responseText;
		String referenceNum;
		String approvalCode;
		String responseDate;
		String responseTime;
		StringBuilder responseMiscData = new StringBuilder();
		Date responseTs;
		BigDecimal balanceAmountMajor;
		String responseCardType;
		try {
			responseStan = getResponseValue(responses[0], Integer.class, "Stan");
			responseCode = getResponseValue(responses[0], String.class, "ResponseCode");
			responseText = getResponseValue(responses[0], String.class, "ResponseText");
			referenceNum = getResponseValue(responses[0], String.class, "RRN");
			approvalCode = getResponseValue(responses[0], String.class, "AuthId");
			responseDate = getResponseValue(responses[0], String.class, "TransactionDate");
			responseTime = getResponseValue(responses[0], String.class, "TransactionTime");
			if(!StringUtils.isBlank(responseDate) && !StringUtils.isBlank(responseTime))
				responseTs = DATETIME_FORMAT.parse(responseDate + ' ' + responseTime);
			else
				responseTs = null;
			// TODO: we may need to capture other fields and add them to misc data
			// responseMiscData = getResponseValue(responses[0], "DetailedResponseData", String.class);
			// approvedAmountMajor = getResponseValue(responses[0], BigDecimal.class, "StoredValueResponse", "ApprovedAmount"); //XXX: this is not correct, do not use it for StoredValue
			balanceAmountMajor = getResponseValue(responses[0], BigDecimal.class, "StoredValueResponse", "NewBalance");
			String ps2000 = getResponseValue(responses[0], String.class, "TransactionResult", "PS2000");
			if(!StringUtils.isBlank(ps2000))
				responseMiscData.append("PS2000=").append(ps2000).append("\n");
			String transactionIdentifier = getResponseValue(responses[0], String.class, "	", "TransactionIdentifier");
			if(!StringUtils.isBlank(transactionIdentifier))
				responseMiscData.append("TransactionIdentifier=").append(transactionIdentifier).append("\n");
			String validationCode = getResponseValue(responses[0], String.class, "TransactionResult", "ValidationCode");
			if(!StringUtils.isBlank(validationCode))
				responseMiscData.append("ValidationCode=").append(validationCode).append("\n");
			String aprivaToken = getResponseValue(responses[0], String.class, "AprivaToken");
			if(!StringUtils.isBlank(aprivaToken))
				responseMiscData.append("AprivaToken=").append(aprivaToken).append("\n");
			responseCardType = getResponseValue(responses[0], String.class, "CardType");
		} catch(ConvertException | ParseException e) {
			log.warn("Could not convert response", e);
			return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.INVALID_RESPONSE, "Could not convert response: " + e.getMessage());
		}
		
		if(action == AuthorityAction.REVERSAL) {
			if(responseCode.equals("59")) {
				if(taskInfo.getRetryCount() >= getMaxRetryAttempts())
					return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.PROCESSOR_ERROR, "Transaction still in-process after " + getMaxRetryAttempts() + " re-tries");
				throw new RetrySpecifiedServiceException("Transaction still in-process; try again", WorkRetryType.NONBLOCKING_RETRY); // ScheduledRetryServiceException
			}
		}
		
		if(responseStan != null && responseStan.intValue() != requestStan) // is response stan required?
			return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Response Stan value " + responseStan + " did not match request Stan value " + requestStan);

		step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, responseCode);
		step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, responseText);
		step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD, approvalCode);
		step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, referenceNum);
		step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS, responseTs);
		step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, responseMiscData.toString());
		step.setResultAttribute(AuthorityAttrEnum.ATTR_CARD_TYPE, responseCardType);

		BigDecimal balanceAmountMinor = null;
		if(balanceAmountMajor != null) {
			balanceAmountMinor = InteractionUtils.toMinorCurrency(balanceAmountMajor, minorCurrencyFactor);
			step.setResultAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, balanceAmountMinor.longValueExact());
		}
		
		AuthResultCode authResultCode;
		if(responseCode.equals("00") || responseCode.equals("0")) { // success!
			BigDecimal minimumBalanceAmount = getMinimumBalanceAmount(merchantCd);
			if(action != AuthorityAction.AUTHORIZATION) {
				authResultCode = AuthResultCode.APPROVED;
				step.setResultAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, amount.longValueExact());
				step.setResultAttribute(AuthorityAttrEnum.ATTR_SETTLED, true);
				setClientText(step.getResultAttributes(), authResultCode, null, null, null, null, responseText);
			} else if(balanceAmountMajor == null) {
				authResultCode = AuthResultCode.DECLINED;
				step.setResultAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.INVALID_RESPONSE);
				step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, "Response is missing Balance Amount");
				setClientText(step.getResultAttributes(), authResultCode, DeniedReason.INVALID_RESPONSE, "Response is missing Balance Amount");
			} else if(minimumBalanceAmount != null && balanceAmountMajor.compareTo(minimumBalanceAmount) < 0) {
				authResultCode = AuthResultCode.DECLINED;
				step.setResultAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.INSUFFICIENT_FUNDS);
				step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, responseText + "; Insufficient funds: " + balanceAmountMajor + "; At least " + minimumBalanceAmount + " is required");
				setClientText(step.getResultAttributes(), authResultCode, DeniedReason.INSUFFICIENT_FUNDS, responseText, balanceAmountMajor);
			} else if(balanceAmountMajor.compareTo(amountMajor) >= 0) {
				authResultCode = AuthResultCode.APPROVED;
				step.setResultAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, amount.longValueExact());
				step.setResultAttribute(AuthorityAttrEnum.ATTR_SETTLED, true);
				setClientText(step.getResultAttributes(), authResultCode, null, null, null, null, responseText);
			} else if(allowPartialAuth && balanceAmountMajor.compareTo(BigDecimal.ZERO) > 0) {
				authResultCode = AuthResultCode.PARTIAL;
				step.setResultAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, balanceAmountMinor.longValueExact());
				step.setResultAttribute(AuthorityAttrEnum.ATTR_SETTLED, true);
				setClientText(step.getResultAttributes(), authResultCode, null, null, balanceAmountMajor, null, responseText);
			} else {
				authResultCode = AuthResultCode.DECLINED;
				step.setResultAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.INSUFFICIENT_FUNDS);
				step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, responseText + "; Insufficient funds: " + balanceAmountMajor);
				setClientText(step.getResultAttributes(), authResultCode, DeniedReason.INSUFFICIENT_FUNDS, responseText, balanceAmountMajor);
			}
		} else {
			authResultCode = AuthResultCode.DECLINED;
			step.setResultAttribute(AuthorityAttrEnum.ATTR_DENIED_REASON, DeniedReason.PROCESSOR_RESPONSE);
			step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTH_HOLD_USED, false);
			setClientText(step.getResultAttributes(), authResultCode, DeniedReason.PROCESSOR_RESPONSE, responseText);
		}
		
		step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD, authResultCode);
		return authResultCode;
	}

	/**
	 * @see com.usatech.authoritylayer.AbstractAuthorityTask#processReversal(com.usatech.layers.common.constants.AuthorityAction, com.usatech.app.MessageChainTaskInfo)
	 */
	@Override
	protected AuthResultCode processReversal(AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo) throws ServiceException {
		Map<String, Object> resultAttributes = taskInfo.getStep().getResultAttributes();
		switch(action) {
			case AUTHORIZATION:
				// nothing to do b/c auth is a balance check
				return null;
			case SALE:
				// do reversal
				AuthResultCode arc = processRequest(AuthorityAction.REVERSAL, action, taskInfo);
				switch(arc) {
					case DECLINED:
						// Reversal was not necessary - Apriva did not have the transaction
						// return declined to have POSM resubmit the sale
						//return populateDeclinedResults(action, resultAttributes, DeniedReason.PROCESSOR_ERROR, "Sale failed. Reversal declined.");
						return arc;
					case APPROVED:
						// Reversal was successfull - Apriva reversed the sale
						// return declined to have POSM resubmit the sale
						return populateDeclinedResults(action, resultAttributes, DeniedReason.PROCESSOR_ERROR, "Sale reversed");
					case FAILED:
						// This should only happen due to an invalid request or unexpected issue that results in a state that requires manual intervention.
						return arc;
				}
				return arc;
			default:
				return populateFailedResults(subAction, resultAttributes, DeniedReason.CONFIGURATION_ISSUE, "Reversal of Action '" + action + "' NOT implemented");
		}
	}

	protected static final Pattern DEVICE_TRAN_CD_PATTERN = Pattern.compile("(\\d+)(?:\\:R(\\d+))?");

	public static int sanitizeDeviceTranCd(String deviceTranCd, AuthorityAction action, boolean authHoldUsed, int priorAttempts) throws ConvertException {
		Matcher matcher = DEVICE_TRAN_CD_PATTERN.matcher(deviceTranCd);
		if(!matcher.matches())
			throw new ConvertException("DeviceTranCd is not in a recognizable form", Integer.class, deviceTranCd);

		long base = ConvertUtils.getLong(matcher.group(1));
		if(!StringUtils.isBlank(matcher.group(2))) {
			int n = ConvertUtils.getInt(matcher.group(2));
			// for now we will use the base device tran cd + R number and hope that device tran cds are not sequential
			base += n;
			return (int) (700000 + (Math.abs(base) % 150000)); // 700000 to 849999
		}
		if(action == AuthorityAction.SALE && priorAttempts > 0) {
			return (int) (850000 + (Math.abs(base + priorAttempts) % 149999)); // 850000 to 999998
		}
		if(authHoldUsed)
			return (int) (1 + (Math.abs(base) % 699999)); // 1 to 699999
		if(action == AuthorityAction.AUTHORIZATION)
			return (int) (1 + (Math.abs(base) % 349999)); // 1 to 349999
		return (int) (350000 + (Math.abs(base) % 350000)); // 350000 to 699999
	}

	@Override
	protected String[] getAdditionalPreActionLogAttributes() {
		return new String[] {
			MessageAttribute.REMOTE_SERVER_ADDR.getValue()
		};
	}
	
	@Override
	protected String[] getAdditionalPostActionLogAttributes() {
		return new String[] {
				AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue(),
				AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue(),
				AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC.getValue()
		};
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getConnectTimeout() {
		return connectTimeout;
	}

	public void setConnectTimeout(int connectTimeout) {
		this.connectTimeout = connectTimeout;
	}

	public int getConnectRetries() {
		return connectRetries;
	}

	public void setConnectRetries(int connectRetries) {
		this.connectRetries = connectRetries;
	}

	public Charset getCharset() {
		return charset;
	}

	public void setCharset(Charset charset) {
		this.charset = charset;
	}

	public int getResponseTimeout() {
		return responseTimeout;
	}

	public void setResponseTimeout(int responseTimeout) {
		this.responseTimeout = responseTimeout;
	}

	public int getMaxRetryAttempts() {
		return maxRetryAttempts;
	}

	public void setMaxRetryAttempts(int maxRetryAttempts) {
		this.maxRetryAttempts = maxRetryAttempts;
	}

	public boolean isSocketReused() {
		return socketReused;
	}

	public void setSocketReused(boolean socketReused) {
		this.socketReused = socketReused;
	}

	protected BigDecimal getMinimumBalanceAmount(String merchantCd) {
		BigDecimal minimumBalanceAmount = getMinimumBalanceAmountByMerchant(merchantCd);
		return minimumBalanceAmount != null ? minimumBalanceAmount : getDefaultMinimumBalanceAmount();
	}

	public BigDecimal getMinimumBalanceAmountByMerchant(String merchantCd) {
		return minimumBalanceAmountByMerchant.get(merchantCd);
	}

	public void setMinimumBalanceAmountByMerchant(String merchantCd, BigDecimal minimumBalanceAmount) {
		if(minimumBalanceAmount == null || minimumBalanceAmount.signum() == 0)
			minimumBalanceAmountByMerchant.remove(merchantCd);
		else
			minimumBalanceAmountByMerchant.put(merchantCd, minimumBalanceAmount);
	}

	public BigDecimal getDefaultMinimumBalanceAmount() {
		return defaultMinimumBalanceAmount;
	}

	public void setDefaultMinimumBalanceAmount(BigDecimal defaultMinimumBalanceAmount) {
		this.defaultMinimumBalanceAmount = defaultMinimumBalanceAmount;
	}

	public SocketFactory getSocketFactory() {
		return socketFactory;
	}
	
	public void setSocketFactory(SocketFactory socketFactory) {
		this.socketFactory = socketFactory;
	}

	public boolean isReturnFailedOnFailedReversal() {
		return returnFailedOnFailedReversal;
	}

	public void setReturnFailedOnFailedReversal(boolean returnFailedOnFailedReversal) {
		this.returnFailedOnFailedReversal = returnFailedOnFailedReversal;
	}
	
}
