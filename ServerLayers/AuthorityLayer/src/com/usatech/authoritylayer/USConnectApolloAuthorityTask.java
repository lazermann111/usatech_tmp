package com.usatech.authoritylayer;

import static simple.text.MessageFormat.format;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URLEncoder;
import java.text.Format;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.net.ssl.HostnameVerifier;

import org.apache.http.HttpException;
import org.apache.http.NoHttpResponseException;
import org.apache.http.client.methods.HttpRequestBase;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.SerializedName;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.HttpRequester;
import com.usatech.layers.common.HttpRequester.Authenticator;
import com.usatech.layers.common.HttpRequester.HttpResponseData;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAction;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.DeniedReason;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.PaymentMaskBRef;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.MaskedString;
import simple.io.Base64;
import simple.io.ByteArrayUtils;
import simple.io.Log;
import simple.text.StringUtils;

/**
 * Apollo (v1)
 * http://dev.apollodocs.usconnect.blueworldinc.com/apigility/documentation/Apollo-v1
 * 
 * @author Paul Cowan
 *
 */
public class USConnectApolloAuthorityTask extends AbstractAuthorityTask {
	private static final Log log = Log.getLog();

	private static ThreadLocal<Format> currencyFormatLocal = new ThreadLocal<Format>() {
		@Override
		protected Format initialValue() {
			return ConvertUtils.getFormat("CURRENCY", "#0.00");
		}
	};
	
	private static Pattern hexPattern = Pattern.compile("^[0-9A-F]{4}$");
	private static Pattern numberPattern = Pattern.compile("^[0-9]+$");
	
	protected static Gson GSON = new Gson();
	
	protected String[] sslProtocols;
	protected HostnameVerifier hostNameVerifier;
	protected int connectionTimeout = (int) TimeUnit.SECONDS.toMillis(5);
	protected int maxHostConnections = -1;
	protected int maxTotalConnections = -1;
	protected int socketTimeout = (int) TimeUnit.SECONDS.toMillis(15);
	protected String baseUrl;
	protected String token;
	protected BigDecimal minimumBalanceAmount = new BigDecimal("1.00");
	protected String createAccountQueueKey = "usat.keymanager.account.create";
	protected String recordAuthQueueKey = "usat.inbound.auth.record";
	
	protected Authenticator tokenAuthenticator;
	protected HttpRequester httpRequester;
	
	public static enum ApolloCallType {
		ACCOUNT_DETAILS("AccountDetails", "/migrate/account/{0}/{1}/{2}"),
		ACCOUNT_DETAILS_APPLE_VAS("AccountDetailsAppleVAS", "/migrate/account/apple_pay/{1}/{2}/?vasData={0}"),
		PURCHASE("Purchase", "/migrate/purchase"),
		REVERSAL("Reversal", "/migrate/reversal");

		private String name;
		private String uri;

		private ApolloCallType(String name, String uri) {
			this.name = name;
			this.uri = uri;
		}
		
		public String getName() {
			return name;
		}
		
		public String getUri() {
			return uri;
		}
	}

	public USConnectApolloAuthorityTask() {
		super("USConnect_Apollo");
	}

	public void initialize() throws ServiceException {
		initialize(null);
	}

	public void initialize(HttpRequester customHttpRequester) throws ServiceException {
		Objects.requireNonNull(baseUrl, "Missing required parameter: url");
		Objects.requireNonNull(token, "Missing required parameter: token");
		
		if (baseUrl.contains("${"))
			throw new NullPointerException("Unconfigured required parameter: url");
		if (token.contains("${"))
			throw new NullPointerException("Unconfigured required parameter: token");

		// ability to set custom HttpRequester makes testing easier
		if (customHttpRequester == null) {
			httpRequester = new HttpRequester(sslProtocols);
			httpRequester.setIncludeJsonVersion(false);
			if (hostNameVerifier != null)
				httpRequester.setHostNameVerifier(hostNameVerifier);
			if (connectionTimeout >= 0)
				httpRequester.setConnectionTimeout(connectionTimeout);
			if (maxHostConnections >= 0)
				httpRequester.setMaxHostConnections(maxHostConnections);
			if (maxTotalConnections >= 0)
				httpRequester.setMaxTotalConnections(maxTotalConnections);
			if (socketTimeout >= 0)
				httpRequester.setSocketTimeout(socketTimeout);
		} else {
			httpRequester = customHttpRequester;
		}
		
		tokenAuthenticator = new Authenticator() {
			public void addAuthentication(HttpRequestBase method) throws HttpException {
				method.addHeader("Authorization", token);
			}
		};
	}

	@Override
	protected AuthResultCode processRequest(AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo) throws ServiceException {
		if (httpRequester == null) {
			log.warn("Not initialized");
			return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.CONFIGURATION_ISSUE, "Not initialized");
		}
		
		MessageChainStep step = taskInfo.getStep();
		String deviceSerialCd;
		Long tranId;
		String deviceTranCd;
		AccountData accountData;
		BigInteger requestedAmount;
		Integer minorCurrencyFactor;
		boolean allowPartialAuth;
		String authorityTranCd;
		EntryType entryType;
		String currencyCd;
		try {
			deviceSerialCd = getAttribute(step, AuthorityAttrEnum.ATTR_DEVICE_SERIAL_CD, String.class, true);
			deviceTranCd = getAttribute(step, AuthorityAttrEnum.ATTR_DEVICE_TRAN_CD, String.class, true);
			tranId = getAttribute(step, AuthorityAttrEnum.ATTR_TRAN_ID, Long.class, action == AuthorityAction.SALE ? true : false);
			accountData = new AccountData(step.getAttributes());
			requestedAmount = getAttribute(step, AuthorityAttrEnum.ATTR_AMOUNT, BigInteger.class, true);
			minorCurrencyFactor = getAttribute(step, AuthorityAttrEnum.ATTR_MINOR_CURRENCY_FACTOR, Integer.class, true);
			allowPartialAuth = step.getAttributeSafely(AuthorityAttrEnum.ATTR_PARTIAL_AUTH_ALLOWED, Boolean.class, true);
			authorityTranCd = getAttribute(step, AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD, String.class, false);
			entryType = getAttribute(step, AuthorityAttrEnum.ATTR_ENTRY_TYPE, EntryType.class, false);
			currencyCd = getAttribute(step, AuthorityAttrEnum.ATTR_CURRENCY_CD, String.class, true);
		} catch (IllegalArgumentException e) {
			log.warn("Data conversion error", e);
			return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.INVALID_INPUT, "Data conversion error");
		} catch (AuthorityAttributeConversionException e) {
			return AuthResultCode.FAILED;
		}
		String countryCd = "CAD".equalsIgnoreCase(currencyCd) ? "CAN" : "USA";

		String trackData = accountData.getTrackData();
		String cardNumber;
		if (trackData != null && trackData.length() > 40)
			try {
				//USConnect expects encrypted Apple VAS data in Base64 format
				cardNumber = URLEncoder.encode(Base64.encodeBytes(ByteArrayUtils.fromHex(trackData), true), "UTF-8");
			} catch (Exception e) {
				log.warn("Error converting data to URL encoded Base64", e);
				cardNumber = trackData;
			}
		else
			cardNumber = accountData.getPartData(PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER);
		if (StringUtils.isBlank(cardNumber))
			return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), DeniedReason.INVALID_INPUT, "Account Number not provided");

		BigDecimal requestedAmountMajor = InteractionUtils.toMajorCurrency(requestedAmount, minorCurrencyFactor);
		Map<String, Object> resultAttributes = step.getResultAttributes();

		switch (action) {
			case AUTHORIZATION:
				ApolloCallType callType = cardNumber.length() > 40 ? ApolloCallType.ACCOUNT_DETAILS_APPLE_VAS : ApolloCallType.ACCOUNT_DETAILS;				
				return getAccountDetails(step, callType, cardNumber, allowPartialAuth, requestedAmountMajor, countryCd, currencyCd, minorCurrencyFactor, resultAttributes);
			case SALE:
				if (requestedAmountMajor.compareTo(BigDecimal.ZERO) > 0) {
					return sendPurchase(step, entryType, deviceSerialCd, cardNumber, tranId, requestedAmountMajor, countryCd, currencyCd, minorCurrencyFactor, resultAttributes, taskInfo.getRetryCount());
				} else {
					// This is an auth reversal.  Apollo does not support auth hold
					log.info("No need to reverse AccountDetails authorization");
					return AuthResultCode.APPROVED;
				}
			case REFUND:
				return populateFailedResults(AuthorityAction.REFUND, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Refund is not supported; USConnect processes refunds directly."); 
			case REVERSAL:
				// we shouldn't get here
				log.error("Received unexpected reversal request!");
				return AuthResultCode.APPROVED;
			default:
				throw new RetrySpecifiedServiceException(format("Action {0} is NOT implemented", subAction), WorkRetryType.NO_RETRY);
		}
	}

	@Override
	protected AuthResultCode processReversal(AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo) throws ServiceException {
		if (action == AuthorityAction.SALE) {
			// Apollo has duplicate detection, so just return approved on a reversal and retry the Purchase.  If it was a dupe, we'll receve
			return AuthResultCode.APPROVED; 
		} else if (action == AuthorityAction.AUTHORIZATION) {
			// Authorization is only a balance inquiry, so as long as the auth is not stale, just retry it
			return AuthResultCode.APPROVED; 
		} else {
			return AuthResultCode.FAILED;
		}
	}

	// Returns detailed information including balances for the specified account or card. The current balance is the amount of money that is currently in the account, 
	// but may not all be usable. The available balance is the amount of money that the account has to spend at this time. The available balance takes into account spend 
	// limits and credit limits if either is enabled for the account. account-info can be the account ID or any account alias.
	protected AuthResultCode getAccountDetails(MessageChainStep step, ApolloCallType callType, String cardNumber, boolean allowPartialAuth, BigDecimal requestedAmount, String countryCd, String currencyCd, int minorCurrencyFactor, Map<String, Object> resultAttributes) throws ServiceException {
		String url = baseUrl + format(callType.getUri(), cardNumber, countryCd, currencyCd); // may need to encode card number here
		String maskedUrl = baseUrl + format(callType.getUri(), MessageResponseUtils.maskTrackData(cardNumber), countryCd, currencyCd);
		
		log.info("Sending {0} to {1}", callType, maskedUrl);
		HttpResponseData httpResponse = null;
		try {
			httpResponse = httpRequester.jsonGet(url, tokenAuthenticator);
		} catch (Exception e) {
			return handleException(AuthorityAction.AUTHORIZATION, callType, resultAttributes, e);
		}
		
		log.info("Received {0} from {1}", httpResponse.getHttpStatusCode(), maskedUrl);
		if (log.isDebugEnabled() && httpResponse.hasResponse())
			log.debug(httpResponse.getRawResponse());
		
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA.getValue(), httpResponse.toString());

		if (httpResponse.hasException()) {
			return handleException(AuthorityAction.AUTHORIZATION, callType, resultAttributes, httpResponse.getException());
		}
		
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue(), httpResponse.getHttpStatusCode());

		if (!httpResponse.isHttpSuccess()) {
			return handleNonSuccessResponse(AuthorityAction.AUTHORIZATION, callType, resultAttributes, httpResponse);
		}
		
		if (!httpResponse.hasJsonResponse()) {
			return populateFailedResults(AuthorityAction.AUTHORIZATION, resultAttributes, DeniedReason.INVALID_RESPONSE, "Empty JSON response!");
		}
		
		Map<String, Object> authorityResponse = httpResponse.getJsonResponse();
		
		String maskedMap = maskMap(authorityResponse);
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA.getValue(), maskedMap);
		log.info(maskedMap);

		BigDecimal balance = ConvertUtils.convertSafely(BigDecimal.class, authorityResponse.get("available_balance"), null);
		if (balance == null) {
			return populateFailedResults(AuthorityAction.AUTHORIZATION, resultAttributes, DeniedReason.INVALID_RESPONSE, "No 'available_balance' field in response!");
		}

		BigDecimal requestedAmountMinor = InteractionUtils.toMinorCurrency(requestedAmount, minorCurrencyFactor);
		BigDecimal balanceMinor = InteractionUtils.toMinorCurrency(balance, minorCurrencyFactor);
		
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_HOLD_USED.getValue(), false);
		resultAttributes.put(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT.getValue(), balanceMinor.longValueExact());
		
		if (callType == ApolloCallType.ACCOUNT_DETAILS_APPLE_VAS) {
			//Extract account number from aliases 
			String[] aliases = ConvertUtils.convertSafely(String[].class, authorityResponse.get("aliases"), null);
			if (aliases != null) {
				BigDecimal account = null;
				for (String alias: aliases) {
					account = ConvertUtils.convertSafely(BigDecimal.class, alias, null);
					if (account != null) {
						String accountData = account.toPlainString();
						step.setResultAttribute(AuthorityAttrEnum.ATTR_AUTH_ACCOUNT_DATA, accountData);
						step.setResultAttribute(AuthorityAttrEnum.ATTR_TRACK_DATA_OVERRIDE, MessageResponseUtils.maskCardNumber(accountData));
						//Get global account ID
						MessageChainStep accountStep = step.addStep(createAccountQueueKey);
						Set<Integer> resultCodes = new HashSet<Integer>(step.getResultCodes());
						for (int resultCode: resultCodes) {
							accountStep.setNextSteps(resultCode, step.getNextSteps(resultCode));
							step.setNextSteps(resultCode, accountStep);
						}
						accountStep.setSecretAttribute(AuthorityAttrEnum.ATTR_ACCOUNT_DATA, accountData);
						MessageChain chain = step.getMessageChain();
						for (int i = 1; i <= chain.getStepCount(); i++) {
							if (chain.getStep(i).getQueueKey().equalsIgnoreCase(recordAuthQueueKey)) {
								//Add global account ID to saveStep
								chain.getStep(i).setReferenceAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, accountStep, AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID);
								break;
							}
						}
						break;
					}
				}
			}
		}
		
		if (balance.compareTo(requestedAmount) >= 0) {
			setClientText(resultAttributes, AuthResultCode.APPROVED, null, null, balance);
			resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue(), AuthResultCode.APPROVED.getValue());
			resultAttributes.put(AuthorityAttrEnum.ATTR_AMT_APPROVED.getValue(), requestedAmountMinor.longValueExact());
			return AuthResultCode.APPROVED;
		} else if (balance.compareTo(minimumBalanceAmount) < 0 && balance.compareTo(BigDecimal.ZERO) > 0) {
			resultAttributes.put(AuthorityAttrEnum.ATTR_AMT_APPROVED.getValue(), 0L);
			return populateDeclinedResults(AuthorityAction.AUTHORIZATION, resultAttributes, DeniedReason.INSUFFICIENT_FUNDS, currencyFormatLocal.get().format(balance));
		} else if (balance.compareTo(BigDecimal.ZERO) > 0 && allowPartialAuth) {
			setClientText(resultAttributes, AuthResultCode.PARTIAL, null, null, balance);
			resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue(), AuthResultCode.PARTIAL.getValue());
			resultAttributes.put(AuthorityAttrEnum.ATTR_AMT_APPROVED.getValue(), balanceMinor.longValueExact());
			return AuthResultCode.PARTIAL;
		} else {
			resultAttributes.put(AuthorityAttrEnum.ATTR_AMT_APPROVED.getValue(), 0L);
			return populateDeclinedResults(AuthorityAction.AUTHORIZATION, resultAttributes, DeniedReason.INSUFFICIENT_FUNDS, currencyFormatLocal.get().format(balance));
		}
	}

	// When a consumers make purchase of products at connections run by affiliates, it will need to send API request to approve/reject the transaction and recording it if approved.
	//deviceSerialCd, cardNumber, deviceTranCd, amountMajor, minorCurrencyFactor, resultAttributes);
	protected AuthResultCode sendPurchase(MessageChainStep step, EntryType entryType, String deviceSerialCd, String cardNumber, long tranId, BigDecimal requestedAmount, String countryCd, String currencyCd, int minorCurrencyFactor, Map<String, Object> resultAttributes, int retryCount) throws ServiceException {
		if (requestedAmount.compareTo(BigDecimal.ZERO) <= 0)
			throw new ServiceException("Sale amount must be greater than 0");
		
		Purchase purchase = new Purchase();
		purchase.setCardNumber(cardNumber);
		purchase.setDeviceSerialNumber(deviceSerialCd);
		purchase.setExternalReferenceId(Long.toString(tranId));
		purchase.setMethod(entryType == EntryType.VAS ? "apple_pay" : "regular");
		purchase.setCountryCode(countryCd);
		purchase.setCurrencyType(currencyCd);
		
		try {
			addLineItems(purchase, requestedAmount, step);
		} catch (Exception e) {
			log.warn("addLineItems failed!", e);
			return populateFailedResults(AuthorityAction.SALE, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, e.getMessage());
		}
		
		String url = baseUrl + ApolloCallType.PURCHASE.getUri();
		
		log.info("Sending {0} to {1}", ApolloCallType.PURCHASE, url);
		log.info(purchase.toString()); // toString masks card number
		
		if (log.isDebugEnabled())
			log.debug(GSON.toJson(purchase));
		
		HttpResponseData httpResponse = null;
		try {
			httpResponse = httpRequester.jsonPost(url, purchase, tokenAuthenticator);
		} catch (Exception e) {
			return handleException(AuthorityAction.SALE, ApolloCallType.PURCHASE, resultAttributes, e);
		}

		log.info("Received {0} from {1}", httpResponse.getHttpStatusCode(), url);
		if (log.isDebugEnabled() && httpResponse.hasResponse())
			log.debug(httpResponse.getRawResponse());
		
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA.getValue(), httpResponse.toString());

		if (httpResponse.hasException()) {
			return handleException(AuthorityAction.SALE, ApolloCallType.PURCHASE, resultAttributes, httpResponse.getException());
		}
		
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue(), httpResponse.getHttpStatusCode());
		
		if (!httpResponse.isHttpSuccess()) {
			AuthResultCode resultCode = handleNonSuccessResponse(AuthorityAction.SALE, ApolloCallType.PURCHASE, resultAttributes, httpResponse);
			if (resultCode != AuthResultCode.APPROVED) {
				return resultCode;
			} else if (retryCount > 0) {
				// this was a sale retry and a duplicate was detected by Apollo
				// we don't have all the original response data, but let's fill in what we can here and return success
				log.info("Duplicate detected by Apollo on Purchase retry {0}, returning success response", retryCount);
				BigDecimal requestedAmountMinor = InteractionUtils.toMinorCurrency(requestedAmount, minorCurrencyFactor);
				resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue(), AuthResultCode.APPROVED.getValue());
				resultAttributes.put(AuthorityAttrEnum.ATTR_AMT_APPROVED.getValue(), requestedAmountMinor.longValueExact());
				resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA.getValue(), maskMap(httpResponse.getJsonResponse()));
				resultAttributes.put(AuthorityAttrEnum.ATTR_SETTLED.getValue(), true);
				return resultCode;
			} else {
				// in this case Apollo detected a duplicate but it was not a retry...  POSM did a retry?
				// TODO: check this case
				// we don't have all the original response data, but let's fill in what we can here and return success
				log.info("Duplicate detected by Apollo on first Purchase attempt, returning success response");
				BigDecimal requestedAmountMinor = InteractionUtils.toMinorCurrency(requestedAmount, minorCurrencyFactor);
				resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue(), AuthResultCode.APPROVED.getValue());
				resultAttributes.put(AuthorityAttrEnum.ATTR_AMT_APPROVED.getValue(), requestedAmountMinor.longValueExact());
				resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA.getValue(), maskMap(httpResponse.getJsonResponse()));
				resultAttributes.put(AuthorityAttrEnum.ATTR_SETTLED.getValue(), true);
				return resultCode;
			}
		}
		
		if (!httpResponse.hasJsonResponse()) {
			return populateFailedResults(AuthorityAction.SALE, resultAttributes, DeniedReason.INVALID_RESPONSE, "Empty JSON response!");
		}
		
		Map<String, Object> authorityResponse = httpResponse.getJsonResponse();
		
		String maskedMap = maskMap(authorityResponse);
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA.getValue(), maskedMap);
		log.info(maskedMap);

		BigDecimal netTransAmt = ConvertUtils.convertSafely(BigDecimal.class, authorityResponse.get("net_transaction_amount"), null);
		BigDecimal newBalance = ConvertUtils.convertSafely(BigDecimal.class, authorityResponse.get("new_available_balance"), null);
		if (newBalance != null)
			newBalance.setScale(2, RoundingMode.HALF_UP);
		BigDecimal transactionId = ConvertUtils.convertSafely(BigDecimal.class, authorityResponse.get("transaction_id"), null);
		BigDecimal created = ConvertUtils.convertSafely(BigDecimal.class, authorityResponse.get("created"), null);
			
		Date responseTs = null;
		if (created != null) {
			responseTs = new Date(created.longValueExact()*1000);
		}
		
		BigDecimal approvedFundsMinor = InteractionUtils.toMinorCurrency(netTransAmt, minorCurrencyFactor);
		BigDecimal balanceMinor = newBalance == null ? null : InteractionUtils.toMinorCurrency(newBalance, minorCurrencyFactor);
		
		// can netTransAmt be less than requestedAmount?

		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue(), AuthResultCode.APPROVED.getValue());
		resultAttributes.put(AuthorityAttrEnum.ATTR_AMT_APPROVED.getValue(), approvedFundsMinor.longValueExact());
		resultAttributes.put(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT.getValue(), balanceMinor.longValue());
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD.getValue(), transactionId.longValueExact());
		resultAttributes.put(AuthorityAttrEnum.ATTR_SETTLED.getValue(), true);
		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS.getValue(), responseTs);

		return AuthResultCode.APPROVED;
	}
	
	protected void addLineItems(Purchase purchase, BigDecimal requestedAmount, MessageChainStep step) throws ConvertException, AuthorityAttributeConversionException {
		int lineItemCount = getAttribute(step, AuthorityAttrEnum.ATTR_ITEM_COUNT, int.class, true);
		Map<String, Object> attributes = step.getAttributes();
		
		if (lineItemCount > 0) {
			for (int i=1; i<=lineItemCount; i++) {
				BigDecimal quantity = ConvertUtils.convertRequired(BigDecimal.class, attributes.get("quantity" + i));
				BigDecimal price = ConvertUtils.convertRequired(BigDecimal.class, attributes.get("price" + i));
				String barCodeStr = ConvertUtils.convertRequired(String.class, attributes.get("product" + i));

				Item item = new Item();
				String barCode;
				if (numberPattern.matcher(barCodeStr).matches()) 
					barCode = barCodeStr;
				else if (hexPattern.matcher(barCodeStr).matches()) {
					// send the hex as an integer
					barCode = Integer.toString(Integer.parseInt(barCodeStr, 16));
				} else {
					// convert characters to numbers... how?
					barCode = Integer.toString(Math.abs(barCodeStr.hashCode()));
				}
				item.setBarcode(barCode);
				item.setQuantity(quantity);
				item.setPrice(price);
				item.setDescription(ConvertUtils.convertRequired(String.class, attributes.get("productDesc" + i)));
				int compareResult = item.getTotalPrice().compareTo(BigDecimal.ZERO);
				if (compareResult == 0) {
					log.info("Skipping zero value line item: {0}", item);
					continue;
				} else if (compareResult < 0) {
					log.info("Skipping negative value line item: {0}", item);
				} else {
					purchase.addItem(item);
				}
			}
		}
		
		if (!purchase.hasItems()) {
			Item item = new Item();
			item.setPrice(requestedAmount);
			purchase.addItem(item);
		}
		
		if (purchase.getTotalPrice().compareTo(requestedAmount) != 0) {
			// line items totals didn't match the requested Purchase amount
			List<Item> items = purchase.getItems();
			if (items.size() == 1) {
				// just set the line item's price to the Purchase amount
				items.get(0).setPrice(requestedAmount);
			} else {
				// this mess was copied from Sprout; it averages out the item prices to match the requested amount
				// TODO: test this
				int itemCount = items.size();
				BigDecimal averagePrice = requestedAmount.divide(new BigDecimal(itemCount), 2, RoundingMode.HALF_UP).setScale(2, RoundingMode.HALF_UP);
				for (int i = 0; i < itemCount - 1; i++)
					items.get(i).setPrice(averagePrice);
				BigDecimal lastItemPrice = requestedAmount.subtract(averagePrice.multiply(new BigDecimal(itemCount - 1)));
				if (lastItemPrice.compareTo(BigDecimal.ZERO) > 0)
					items.get(itemCount - 1).setPrice(lastItemPrice);
				else 
					items.get(itemCount - 1).setPrice(new BigDecimal("0.01"));
			}
		}
	}

	protected AuthResultCode handleException(AuthorityAction action, ApolloCallType callType, Map<String, Object> resultAttributes, Exception e) throws ServiceException {
		log.warn("{0} failed: {1}", callType, e.getMessage(), e);
		
		DeniedReason deniedReason = getDeniedReason(e);
		
		if (action == AuthorityAction.AUTHORIZATION) {
			return populateFailedResults(action, resultAttributes, deniedReason, e.getMessage());
		}
		
		boolean possiblyReceived = false;
		Throwable cause = e;
		if (e instanceof HttpException) {
			Throwable httpCause = ((HttpException)e).getCause();
			if (httpCause != null)
				cause = httpCause;
		}
		
		// categorize exceptions based on the possibility that the server received it, so we can decide 
		if (cause instanceof ConnectException || cause instanceof SocketException) {
			possiblyReceived = false;
		} else if (cause instanceof SocketTimeoutException || cause instanceof NoHttpResponseException || cause instanceof IOException || cause instanceof JsonSyntaxException) {
			possiblyReceived = true;
		} else {
			log.error("Caught unhandled exception: handling should be added for this exception.", e);
		}
		
		if (possiblyReceived) {
			//throw new RetrySpecifiedServiceException(e.getMessage(), WorkRetryType.SCHEDULED_RETRY);
			
			// Apollo supports duplicate detection, and will return a specific code if the transaction was retried and was a duplicate
			// So reversals should be unncessary because we can just retry and handle the duplicate notification
			// code 30 in handleNonSuccessResponse below is a duplicate, and should return success on a sale, so the settlement
			// gets marked as successfull and retries stop
			// Therefore I'm returning declined on this error case, so do retires...
			// However if Apollo duplicate checking is not reliable, then we need to throw RetrySpecifiedServiceException here to
			// handle a retry, and send a reversal on a sale retry
			
			return populateDeclinedResults(action, resultAttributes, deniedReason, e.getMessage());
		}
		
		// return declined on a failed sale rather than failed to make POSM do retries
		return populateDeclinedResults(action, resultAttributes, deniedReason, e.getMessage());
	}

	protected AuthResultCode handleNonSuccessResponse(AuthorityAction action, ApolloCallType callType, Map<String, Object> resultAttributes, HttpResponseData httpResponse) throws ServiceException {
		int httpStatusCode = httpResponse.getHttpStatusCode();
		log.info("{0} returned non-success response: {1}", callType, httpStatusCode);
		
		// HTTP 422 is their JSON response on a decline or normal error
		// Any other HTTP status code is unusual
		if (httpResponse.getHttpStatusCode() != 422) {
			return populateFailedResults(action, resultAttributes, DeniedReason.INVALID_RESPONSE, format("Unexpected HTTP status: {0}", httpStatusCode));
		}
		
		if (!httpResponse.hasJsonResponse()) {
			return populateFailedResults(action, resultAttributes, DeniedReason.INVALID_RESPONSE, "Unexpected non-JSON 422 response!");
		}
		
		Map<String, Object> jsonResponse = httpResponse.getJsonResponse();

		resultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA.getValue(), maskMap(jsonResponse));
		
		int code = ConvertUtils.getIntSafely(jsonResponse.get("code"), -1);
		if (code == -1) {
			return populateFailedResults(action, resultAttributes, DeniedReason.INVALID_RESPONSE, "Error code not found in 422 response!");
		}
		
		String message = ConvertUtils.getStringSafely(jsonResponse.get("message"), null);
		
		if (code == 51)
			return populateDeclinedResults(action, resultAttributes, DeniedReason.INSUFFICIENT_FUNDS, message);
		else if (code == 14) {
			// If it's a Sale and there's no account, that means the account existed for the auth but does not now...
			// That should be an unusual case, we may want to return failed for a Sale here, because retries are not likely work 
			return populateDeclinedResults(action, resultAttributes, DeniedReason.NO_ACCOUNT, message);
		} else if (code == 30) { // this is a duplicate
			if (action == AuthorityAction.SALE) {
				// this sale was a duplicate, it already went through
				return AuthResultCode.APPROVED;
			}
			return populateFailedResults(action, resultAttributes, DeniedReason.PROCESSOR_RESPONSE, message);
		} else if (code == 105) // Item(s) array required to make a purchase
			return populateFailedResults(action, resultAttributes, DeniedReason.INVALID_INPUT, message);
		// handle others here
		
		return populateFailedResults(action, resultAttributes, DeniedReason.PROCESSOR_RESPONSE, message);
	}
	
	// this idea here is to map these to something that is in translations.properties so the user gets some meaningful feedback
	protected DeniedReason getDeniedReason(Exception e) {
		if (e instanceof ConnectException || e instanceof SocketException) {
			return DeniedReason.PROCESSOR_ERROR;
		} else if (e instanceof SocketTimeoutException || e instanceof InterruptedIOException || e instanceof TimeoutException) {
			return DeniedReason.PROCESSOR_TIMEOUT;
		} else if (e instanceof NoHttpResponseException || e instanceof IOException || e instanceof JsonSyntaxException) {
			// there's no translations for PROCESSOR_RESPONSE
			//return DeniedReason.PROCESSOR_RESPONSE;
			return DeniedReason.PROCESSOR_ERROR;
		} else {
			return DeniedReason.AUTH_LAYER_PROCESSING_ERROR;
		}
	}
	
	protected static String maskMap(Map<String, Object> payload) {
		if(payload == null) {
			log.warn("Attempted to mask null map!");
			return "";
		}
		
		Map<String, Object> masked = new HashMap<>(payload);
		
		String[] toMask = {"account", "account_number", "username", "password", "first_name", "middle_name", "last_name", "account_id", "aliases", "url", "account_url"};
		for(String key : toMask) {
			String value = ConvertUtils.getStringSafely(masked.get(key), null);
			if (value != null) {
				masked.put(key, new MaskedString(value).toString());
			}
		}
		
		// format BigDecimals so they don't show up as scientific notation
		List<String> numericKeys = masked.entrySet().stream().filter(e -> e.getValue() instanceof Number).map(e -> e.getKey()).collect(Collectors.toList());
		for(String key : numericKeys) {
			try {
				BigDecimal bd = ConvertUtils.convert(BigDecimal.class, masked.get(key));
				masked.put(key, bd.toPlainString());
			} catch (Exception e) {
				log.debug(e);
			}
		}
		
		return masked.toString();
	}

	public HttpRequester getHttpRequester() {
		return httpRequester;
	}

	public void setHttpRequester(HttpRequester httpRequester) {
		this.httpRequester = httpRequester;
	}

	public String[] getSslProtocols() {
		return sslProtocols;
	}

	public void setSslProtocols(String[] sslProtocols) {
		this.sslProtocols = sslProtocols;
	}

	public HostnameVerifier getHostNameVerifier() {
		return hostNameVerifier;
	}

	public void setHostNameVerifier(HostnameVerifier hostNameVerifier) {
		this.hostNameVerifier = hostNameVerifier;
	}

	public int getConnectionTimeout() {
		return connectionTimeout;
	}

	public void setConnectionTimeout(int connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}

	public int getMaxHostConnections() {
		return maxHostConnections;
	}

	public void setMaxHostConnections(int maxHostConnections) {
		this.maxHostConnections = maxHostConnections;
	}

	public int getMaxTotalConnections() {
		return maxTotalConnections;
	}

	public void setMaxTotalConnections(int maxTotalConnections) {
		this.maxTotalConnections = maxTotalConnections;
	}

	public int getSocketTimeout() {
		return socketTimeout;
	}

	public void setSocketTimeout(int socketTimeout) {
		this.socketTimeout = socketTimeout;
	}

	public BigDecimal getMinimumBalanceAmount() {
		return minimumBalanceAmount;
	}

	public void setMinimumBalanceAmount(BigDecimal minimumBalanceAmount) {
		this.minimumBalanceAmount = minimumBalanceAmount;
	}
	
	public String getCreateAccountQueueKey() {
		return createAccountQueueKey;
	}

	public void setCreateAccountQueueKey(String createAccountQueueKey) {
		this.createAccountQueueKey = createAccountQueueKey;
	}

	public String getRecordAuthQueueKey() {
		return recordAuthQueueKey;
	}

	public void setRecordAuthQueueKey(String recordAuthQueueKey) {
		this.recordAuthQueueKey = recordAuthQueueKey;
	}

	public String getUrl() {
		return baseUrl;
	}
	
	public void setUrl(String url) {
		this.baseUrl = url;
	}
	
	public String getToken() {
		return token;
	}
	
	public void setToken(String token) {
		this.token = token;
	}
	
	public static class Purchase implements Serializable {
		private static final long serialVersionUID = 1L;
		
		@SerializedName("device_serial_number")
		private String deviceSerialNumber;
		
		@SerializedName("card_number")
		private String cardNumber;
		
		@SerializedName("external_reference_id")
		private String externalReferenceId;
		
		@SerializedName("items")
		private List<Item> items = new ArrayList<>();
		
		@SerializedName("method")
		private String method;
		
		@SerializedName("country_code")
		private String countryCode;
		
		@SerializedName("currency_type")
		private String currencyType;

		public String getDeviceSerialNumber() {
			return deviceSerialNumber;
		}

		public void setDeviceSerialNumber(String deviceSerialNumber) {
			this.deviceSerialNumber = deviceSerialNumber;
		}

		public String getCardNumber() {
			return cardNumber;
		}

		public void setCardNumber(String cardNumber) {
			this.cardNumber = cardNumber;
		}

		public String getExternalReferenceId() {
			return externalReferenceId;
		}

		public void setExternalReferenceId(String externalReferenceId) {
			this.externalReferenceId = externalReferenceId;
		}

		public List<Item> getItems() {
			return items;
		}

		public void addItem(Item item) {
			items.add(item);
		}
		
		public BigDecimal getTotalPrice() {
			return items.stream().map(i -> i.getTotalPrice()).reduce(BigDecimal.ZERO, BigDecimal::add);
			//return items.stream().map(i -> i.getPrice()).reduce(BigDecimal.ZERO, BigDecimal::add);
		}
		
		public boolean hasItems() {
			return !items.isEmpty();
		}
		
		public int getItemsSize() {
			return items.size();
		}
		
		public String getMethod() {
			return method;
		}

		public void setMethod(String method) {
			this.method = method;
		}

		public String getCountryCode() {
			return countryCode;
		}

		public void setCountryCode(String countryCode) {
			this.countryCode = countryCode;
		}

		public String getCurrencyType() {
			return currencyType;
		}

		public void setCurrencyType(String currencyType) {
			this.currencyType = currencyType;
		}

		@Override
		public String toString() {
			//return GSON.toJson(this);
			return String.format("Purchase[deviceSerialNumber=%s, cardNumber=%s, externalReferenceId=%s, items=%s, method=%s, countryCode=%s, currencyType=%s]", deviceSerialNumber, MessageResponseUtils.maskTrackData(cardNumber), externalReferenceId, items, method, countryCode, currencyType);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((cardNumber == null) ? 0 : cardNumber.hashCode());
			result = prime * result + ((deviceSerialNumber == null) ? 0 : deviceSerialNumber.hashCode());
			result = prime * result + ((externalReferenceId == null) ? 0 : externalReferenceId.hashCode());
			result = prime * result + ((items == null) ? 0 : items.hashCode());
			result = prime * result + ((method == null) ? 0 : method.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Purchase other = (Purchase)obj;
			if (cardNumber == null) {
				if (other.cardNumber != null)
					return false;
			} else if (!cardNumber.equals(other.cardNumber))
				return false;
			if (deviceSerialNumber == null) {
				if (other.deviceSerialNumber != null)
					return false;
			} else if (!deviceSerialNumber.equals(other.deviceSerialNumber))
				return false;
			if (externalReferenceId == null) {
				if (other.externalReferenceId != null)
					return false;
			} else if (!externalReferenceId.equals(other.externalReferenceId))
				return false;
			if (items == null) {
				if (other.items != null)
					return false;
			} else if (!items.equals(other.items))
				return false;
			if (method == null) {
				if (other.method != null)
					return false;
			} else if (!method.equals(other.method))
				return false;
			return true;
		}
	}
	
	public static class Item implements Serializable {
		private static final long serialVersionUID = 1L;

		@SerializedName("barcode")
		private String barcode;
		
		@SerializedName("description")
		private String description;
		
		@SerializedName("quantity")
		private BigDecimal quantity;
		
		@SerializedName("price")
		private BigDecimal price;

		public String getBarcode() {
			return barcode;
		}

		public void setBarcode(String barcode) {
			this.barcode = barcode;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public BigDecimal getQuantity() {
			return quantity;
		}

		public void setQuantity(BigDecimal quantity) {
			this.quantity = quantity;
		}

		public BigDecimal getPrice() {
			return price;
		}

		public void setPrice(BigDecimal price) {
			this.price = price;
		}
		
		public BigDecimal getTotalPrice() {
			return price.multiply(quantity);
		}

		@Override
		public String toString() {
			//return GSON.toJson(this);
			return String.format("Item[barcode=%s, description=%s, quantity=%s, price=%s]", barcode, description, quantity, price);
			//return String.format("Item[description=%s, price=%s]", description, price);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((barcode == null) ? 0 : barcode.hashCode());
			result = prime * result + ((description == null) ? 0 : description.hashCode());
			result = prime * result + ((price == null) ? 0 : price.hashCode());
			result = prime * result + ((quantity == null) ? 0 : quantity.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Item other = (Item)obj;
			if (barcode == null) {
				if (other.barcode != null)
					return false;
			} else if (!barcode.equals(other.barcode))
				return false;
			if (description == null) {
				if (other.description != null)
					return false;
			} else if (!description.equals(other.description))
				return false;
			if (price == null) {
				if (other.price != null)
					return false;
			} else if (!price.equals(other.price))
				return false;
			if (quantity == null) {
				if (other.quantity != null)
					return false;
			} else if (!quantity.equals(other.quantity))
				return false;
			return true;
		}
	}
	

}
