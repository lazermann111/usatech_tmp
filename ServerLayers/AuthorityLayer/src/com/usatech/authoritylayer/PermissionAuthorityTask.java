/**
 *
 */
package com.usatech.authoritylayer;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAction;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.DeniedReason;
import com.usatech.layers.common.constants.PaymentMaskBRef;
import com.usatech.layers.common.util.StringHelper;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.security.SecureHash;
import simple.text.StringUtils;

/**
 * @author Brian S. Krug
 *
 */
public class PermissionAuthorityTask extends AbstractAuthorityTask {
	private static final Log log = Log.getLog();
	protected static final int SERVICE_TYPE_PRIVILEGE_BASED = 2;

	protected int[] consumerAcctTypeIds;

	public PermissionAuthorityTask() {
		super("permission");
	}

	@Override
	protected AuthResultCode processRequest(AuthorityAction action, AuthorityAction subAction, final MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		Map<String, Object> resultAttributes = step.getResultAttributes();
		int serviceType;
		long posPtaId;
		long authTime;
		long globalAccountId;
		Boolean trackCrcMatches;
		byte entryType;
		try {
			serviceType = getAttribute(step, AuthorityAttrEnum.ATTR_SERVICE_TYPE, Integer.class, true);
			posPtaId = getAttribute(step, AuthorityAttrEnum.ATTR_POS_PTA_ID, Long.class, true);
			authTime = getAttribute(step, AuthorityAttrEnum.ATTR_AUTH_TIME, Long.class, true);
			globalAccountId = getAttribute(step, AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, Long.class, true);
			trackCrcMatches = getAttribute(step, AuthorityAttrEnum.ATTR_TRACK_CRC_MATCH, Boolean.class, false);
			entryType = getAttribute(step, AuthorityAttrEnum.ATTR_ENTRY_TYPE, Byte.class, true);
		} catch(AuthorityAttributeConversionException e) {
			return AuthResultCode.FAILED;
		}
		switch(action) {
			case AUTHORIZATION:
				AccountData accountData = new AccountData(step.getAttributes());
				return permit(action, serviceType, posPtaId, globalAccountId, authTime, trackCrcMatches, entryType, accountData, resultAttributes);
			default:
				throw new RetrySpecifiedServiceException("Action '" + action + "' NOT implemented",WorkRetryType.NO_RETRY);
		}
	}

	@Override
	protected AuthResultCode processReversal(AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		switch(action) {
			case AUTHORIZATION:
				int serviceType;
				try {
					serviceType = getAttribute(step, AuthorityAttrEnum.ATTR_SERVICE_TYPE, Integer.class, true);
				} catch(AuthorityAttributeConversionException e) {
					log.warn("Reversal of Action '" + action + "' failed", e);
					return AuthResultCode.FAILED;
				}
				switch(serviceType) {
					case SERVICE_TYPE_PRIVILEGE_BASED:
						log.info("Privilege based service type needs no reversal");
						return AuthResultCode.APPROVED;
					default:
						log.error("Invalid Authority Service Type " + serviceType);
						populateFailedResults(action, step.getResultAttributes(), null, "Invalid Authority Service Type " + serviceType);
						return AuthResultCode.FAILED;
				}
			default:
				log.warn("Reversal of Action '" + action + "' NOT implemented");
				return AuthResultCode.FAILED;
		}
	}

	protected AuthResultCode permit(AuthorityAction action, int serviceType, long posPtaId, long globalAccountId, long authTime, Boolean trackCrcMatches, byte entryType, AccountData accountData, Map<String, Object> resultAttributes) {
		boolean manuallyEntered = entryType == 'N' || entryType == 'T';
		Map<String,Object> params = new HashMap<String, Object>();
		AuthResultCode authResultCode;		
		switch(serviceType) {
			case SERVICE_TYPE_PRIVILEGE_BASED:
				// Get consumerAcctId and available balance
				params.put("posPtaId", posPtaId);
				params.put("globalAccountId", globalAccountId);
				params.put("authTime", authTime);
				params.put("consumerAcctTypeIds", getConsumerAcctTypeIds());
				try {
					DataLayerMgr.executeCall("PERMIT_CONSUMER_ACCT", params, params, true);
				} catch(SQLException e) {
					log.warn("Error while getting consumer acct from database", e);
					populateFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Database Error: " + e.getMessage());
					resultAttributes.put("actionCode", 0);
					return AuthResultCode.FAILED;
				} catch(DataLayerException e) {
					log.warn("Error while getting consumer acct from database", e);
					populateFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "DataLayer Error: " + e.getMessage());
					resultAttributes.put("actionCode", 0);
					return AuthResultCode.FAILED;
				}
				Object consumerAcctId;
				int consumerAcctTypeId;
				try {
					consumerAcctId = params.get("consumerAcctId");
					consumerAcctTypeId = ConvertUtils.getInt(params.get("consumerAcctTypeId"), 0);
				} catch(ConvertException e) {
					log.warn("Error while getting consumer acct from database", e);
					return populateFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Conversion Error: " + e.getMessage());
				}
				if(consumerAcctId == null) {
					resultAttributes.put("authResultCd", 'N');
					resultAttributes.put("authorityRespCd", 9);
					String text = "Could not find active account or account is inaccessible by this device";
					resultAttributes.put("authorityRespDesc", text);
					setClientText(resultAttributes, AuthResultCode.DECLINED_PERMANENT, DeniedReason.NO_ACCOUNT, "Invalid Account");
					resultAttributes.put("actionCode", 0);
					return AuthResultCode.DECLINED;
				}
				resultAttributes.put("consumerAcctId", consumerAcctId);
				resultAttributes.put("consumerId", params.get("consumerId"));
				resultAttributes.put("consumerAcctTypeLabel", params.get("consumerAcctTypeLabel"));

				if(manuallyEntered) {
					String expirationDate = accountData.getPartData(PaymentMaskBRef.EXPIRATION_DATE);
					String securityCd = accountData.getPartData(PaymentMaskBRef.DISCRETIONARY_DATA);
					String postal = accountData.getPartData(PaymentMaskBRef.ZIP_CODE);
					if(StringUtils.isBlank(expirationDate) || StringUtils.isBlank(securityCd) || StringUtils.isBlank(postal)) {
						log.warn("Empty expiration date, security code or postal code");
						return populateFailedResults(action, resultAttributes, DeniedReason.MISSING_INPUT, "Empty expiration date, security code or postal code");
					}
					if(!expirationDate.equals(params.get("expirationDate"))) {
						log.info("Incorrect expiration date");
						return populateDeclinedResults(action, resultAttributes, DeniedReason.INVALID_VERIFICATION_DATA, "Incorrect expiration date");
					}
					if(!postal.equals(params.get("consumerPostalCd"))) {
						log.info("Incorrect postal code");
						return populateDeclinedResults(action, resultAttributes, DeniedReason.AVS_MISMATCH, "Incorrect postal code");
					}
					byte[] securityCdHash = ConvertUtils.convertSafely(byte[].class, params.get("securityCdHash"), null);
					byte[] securityCdSalt = ConvertUtils.convertSafely(byte[].class, params.get("securityCdSalt"), null);
					if(securityCdHash != null && securityCdSalt != null) {
						byte[] hash;
						try {
							hash = SecureHash.getHash(securityCd.getBytes(), securityCdSalt);
						} catch(NoSuchAlgorithmException e) {
							log.error("Error calculating security code hash", e);
							return populateFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Error calculating security code hash: " + e.getMessage());
						}
						if(!Arrays.equals(securityCdHash, hash)) {
							log.info("Incorrect security code");
							return populateDeclinedResults(action, resultAttributes, DeniedReason.CVV_MISMATCH, "Incorrect security code");
						}
					}
				} else if(trackCrcMatches == null) {
					String trackData = accountData.getTrackData();
					if(!StringHelper.isBlank(trackData)) {
						switch(consumerAcctTypeId) {
							case 1:
							case 2:
							case 3:
								byte[] consumerAcctRawHash = ConvertUtils.convertSafely(byte[].class, params.get("consumerAcctRawHash"), null);
								byte[] consumerAcctRawSalt = ConvertUtils.convertSafely(byte[].class, params.get("consumerAcctRawSalt"), null);
								boolean alreadyHashed = consumerAcctRawHash != null && consumerAcctRawSalt != null;
								if(!alreadyHashed) {
									try {
										consumerAcctRawSalt = SecureHash.getSalt();
									} catch(NoSuchAlgorithmException e) {
										return populateFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Error generating salt: " + e.getMessage());
									} catch(NoSuchProviderException e) {
										return populateFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Error generating salt: " + e.getMessage());
									}
								}
								byte[] rawHash;
								try {
									rawHash = SecureHash.getHash(trackData.getBytes(), consumerAcctRawSalt);
								} catch(NoSuchAlgorithmException e) {
									log.error("Error calculating raw hash", e);
									return populateFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Error calculating raw hash: " + e.getMessage());
								}
								if(alreadyHashed) {
									if(!Arrays.equals(consumerAcctRawHash, rawHash)) {
										log.info("Invalid track data, raw hash data did not match");
										return populateDeclinedResults(action, resultAttributes, DeniedReason.INVALID_VERIFICATION_DATA, "Invalid track data, raw hash data did not match");
									}
								} else {
									Map<String, Object> updateParams = new HashMap<String, Object>();
									updateParams.put("consumerAcctRawHash", rawHash);
									updateParams.put("consumerAcctRawSalt", consumerAcctRawSalt);
									updateParams.put("consumerAcctRawAlg", SecureHash.HASH_ALGORITHM_ITERS);
									updateParams.put("consumerAcctId", consumerAcctId);
									try {
										DataLayerMgr.executeUpdate("UPDATE_CONSUMER_ACCT_RAW_HASH", updateParams, true);
									} catch(SQLException e) {
										return populateFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Error updating raw hash: " + e.getMessage());
									} catch(DataLayerException e) {
										return populateFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Error updating raw hash: " + e.getMessage());
									}
								}
								break;
						}
					} else {
						// what to do here?

					}
				} else if(!trackCrcMatches.booleanValue()) {
					return populateDeclinedResults(action, resultAttributes, DeniedReason.INVALID_VERIFICATION_DATA, "Invalid card");

				}

				Integer privilegeActionId;
				Integer privilegeActionCode;
				long privilegeActionBitmap;
				String privilegeActionAttributes;
				try {
					privilegeActionId = ConvertUtils.convert(Integer.class, params.get("privilegeActionId"));
					privilegeActionCode = ConvertUtils.convert(Integer.class, params.get("privilegeActionCode"));
					privilegeActionBitmap = ConvertUtils.getLong(params.get("privilegeActionBitmap"), 0);
					privilegeActionAttributes = ConvertUtils.getString(params.get("privilegeActionAttributes"), false);
				} catch(ConvertException e) {
					log.error("Could not convert attributes as needed", e);
					populateFailedResults(action, resultAttributes, DeniedReason.AUTH_LAYER_PROCESSING_ERROR, "Data Conversion Error");
					return AuthResultCode.FAILED;
				}
				resultAttributes.put("actionId", privilegeActionId);
				resultAttributes.put("actionCode", privilegeActionCode);
				resultAttributes.put("actionAttributes", privilegeActionAttributes);
				if(privilegeActionCode == null) {
					resultAttributes.put("authResultCd", 'N');
					resultAttributes.put("authorityRespCd", 401);
					String text = "No privilege action is configured for this account";
					resultAttributes.put("authorityRespDesc", text);
					setClientText(resultAttributes, AuthResultCode.DECLINED, DeniedReason.CONFIGURATION_ISSUE, "No action configured", null, null, null);
					authResultCode = AuthResultCode.DECLINED;
				} else {
					resultAttributes.put("authResultCd", 'Y');
					resultAttributes.put("authorityRespCd", 0);
					resultAttributes.put("actionCodeBitmap", privilegeActionBitmap);
					authResultCode = AuthResultCode.APPROVED;
				}
				break;
			default:
				log.error("Invalid Authority Service Type " + serviceType);
				populateFailedResults(action, resultAttributes, DeniedReason.CONFIGURATION_ISSUE, "Invalid Authority Service Type " + serviceType);
				return AuthResultCode.FAILED;
		}
		return authResultCode;
	}

	public int[] getConsumerAcctTypeIds() {
		return consumerAcctTypeIds;
	}

	public void setConsumerAcctTypeIds(int[] consumerAcctTypeIds) {
		this.consumerAcctTypeIds = consumerAcctTypeIds;
	}
}
