/**
 *
 */
package com.usatech.authoritylayer;

import simple.bean.ConvertException;

import com.usatech.app.Attribute;

/**
 * @author Brian S. Krug
 *
 */
public class AuthorityAttributeConversionException extends Exception {
	private static final long serialVersionUID = 2319194478720083521L;

	protected final String attributeKey;
	/**
	 * @param message
	 * @param cause
	 */
	public AuthorityAttributeConversionException(Attribute attribute, ConvertException cause) {
		this(attribute.getValue(), cause);
	}

	/**
	 * @param attribute
	 * @param cause
	 */
	public AuthorityAttributeConversionException(String attributeName, ConvertException cause) {
		super("Data Conversion Error on '" + attributeName + "'", cause);
		this.attributeKey = attributeName;
	}

	public String getAttributeKey() {
		return attributeKey;
	}
}
