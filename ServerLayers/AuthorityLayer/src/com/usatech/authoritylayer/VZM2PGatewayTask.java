package com.usatech.authoritylayer;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Map;

import org.apache.axis2.transport.http.HTTPConstants;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.io.Log;
import simple.text.StringUtils;

import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAction;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.DeniedReason;
import com.usatech.layers.common.constants.MessageAttribute;
import com.usatech.pos.webservice.POSException;
import com.usatech.vzm2p.M2P_Value_TransactionStub;
import com.usatech.vzm2p.M2P_Value_TransactionStub.M2P_Value_Transaction_Request;
import com.usatech.vzm2p.M2P_Value_TransactionStub.M2P_Value_Transaction_RequestE;
import com.usatech.vzm2p.M2P_Value_TransactionStub.M2P_Value_Transaction_Response;
import com.usatech.vzm2p.M2P_Value_TransactionStub.M2P_Value_Transaction_ResponseE;

public class VZM2PGatewayTask extends AbstractAuthorityTask {
	private static final Log log = Log.getLog();
	 
	protected static int vzm2pAuthConnectionTimeout = 2000;
	protected static int vzm2pAuthSocketTimeout = 5000;
	protected static int vzm2pConnectionTimeout = 3000;
	protected static int vzm2pSocketTimeout = 20000;

	public VZM2PGatewayTask() {
		super("vzm2p");
	}
	@Override
	protected AuthResultCode processRequest(AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		String remoteServerAddr;
		String currencyCd;
		String consumerAcctToken;
		BigDecimal amount;
		BigDecimal minorCurrencyFactor;
		String transactionID;
		String approvalCode;
		try {
			remoteServerAddr = getAttribute(step, AuthorityAttrEnum.ATTR_REMOTE_SERVER_ADDRESS, String.class, true);
			consumerAcctToken = getAttribute(step, AuthorityAttrEnum.ATTR_CONSUMER_ACCT_TOKEN, String.class, true);
			minorCurrencyFactor = getAttribute(step, AuthorityAttrEnum.ATTR_MINOR_CURRENCY_FACTOR, BigDecimal.class, true);
			amount = getAttribute(step, AuthorityAttrEnum.ATTR_AMOUNT, BigDecimal.class, true).divide(minorCurrencyFactor);
			transactionID = getAttribute(step, AuthorityAttrEnum.ATTR_TRACE_NUMBER, String.class, true);
			currencyCd = getAttribute(step, AuthorityAttrEnum.ATTR_CURRENCY_CD, String.class, true);
			approvalCode = getAttribute(step, AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD, String.class, false);
		} catch(AuthorityAttributeConversionException e) {
			return AuthResultCode.FAILED;
		}
		
		switch(action) {
			case AUTHORIZATION:
				try {
		            M2P_Value_Transaction_RequestE requestMessage = new M2P_Value_Transaction_RequestE();
		    		M2P_Value_Transaction_Request request = new M2P_Value_Transaction_Request();
		    		request.setValue_Transaction_Type("Auth");
		    		request.setValue_Transaction_Amount(amount);
					request.setValue_Transaction_Consumer_Identifier(consumerAcctToken);
		    		request.setValue_Transaction_Currency(currencyCd);
		    		request.setValue_Transaction_Reference_Id(transactionID);
		    		requestMessage.setM2P_Value_Transaction_Request(request);
		    		log.info(StringUtils.objectToString(request, "Sending Auth request to M2P server: "));
		    		M2P_Value_TransactionStub vzm2p = new M2P_Value_TransactionStub(remoteServerAddr);
		    		vzm2p._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT, vzm2pAuthConnectionTimeout);
		    		vzm2p._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, vzm2pAuthSocketTimeout);
		    		M2P_Value_Transaction_ResponseE responseMessage = vzm2p.m2P_Value_Transaction_Request(requestMessage);
		    		M2P_Value_Transaction_Response response = responseMessage.getM2P_Value_Transaction_Response();
		    		log.info(StringUtils.objectToString(response, "Received Auth response from M2P server: "));
		    		AuthResultCode result = populateAuthResults(response, amount, minorCurrencyFactor, transactionID, taskInfo.getStep().getResultAttributes());
		    		if (result != AuthResultCode.APPROVED)
		    			taskInfo.setNextQos(taskInfo.getCurrentQos()); // fallback auth doesn't need to be durable until it hits AuthorityLayer or NetLayer
		    		return result;
				} catch(RemoteException e) {
					taskInfo.setNextQos(taskInfo.getCurrentQos()); // fallback auth doesn't need to be durable until it hits AuthorityLayer or NetLayer
					log.warn("Error communicating with VZM2P server", e);
					return populateFailedResults(action, taskInfo.getStep().getResultAttributes(), null, e.getMessage());
				}
			case SALE:
				try {
					M2P_Value_Transaction_RequestE requestMessage = new M2P_Value_Transaction_RequestE();
					M2P_Value_Transaction_Request request = new M2P_Value_Transaction_Request();
					request.setValue_Transaction_Type("Capture");
					request.setValue_Transaction_Amount(amount);
					request.setValue_Transaction_Consumer_Identifier(consumerAcctToken);
					request.setValue_Transaction_Currency(currencyCd);
					request.setValue_Transaction_Reference_Id(transactionID);
					request.setValue_Transaction_Details(approvalCode);
					requestMessage.setM2P_Value_Transaction_Request(request);
					log.info(StringUtils.objectToString(request, "Sending Capture request to M2P server: "));
					M2P_Value_TransactionStub vzm2p = new M2P_Value_TransactionStub(remoteServerAddr);
		    		vzm2p._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT, vzm2pConnectionTimeout);
		    		vzm2p._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, vzm2pSocketTimeout);
					M2P_Value_Transaction_ResponseE responseMessage = vzm2p.m2P_Value_Transaction_Request(requestMessage);
					M2P_Value_Transaction_Response response = responseMessage.getM2P_Value_Transaction_Response();
					log.info(StringUtils.objectToString(response, "Received Capture response from M2P server: "));
					return populateResults(response, amount, minorCurrencyFactor, transactionID, taskInfo.getStep().getResultAttributes());
				} catch(RemoteException e) {
					log.warn("Error communicating with VZM2P server", e);
					return populateDeclinedResults(action, taskInfo.getStep().getResultAttributes(), null, e.getMessage());
				}
			case REFUND:
				Map<String, Object> resultAttributes = taskInfo.getStep().getResultAttributes();
				resultAttributes.put("authResultCd", 'Y');
				resultAttributes.put("authorityRespDesc", "VZM2P refund has not been implemented");			
				resultAttributes.put("settled", true);
				resultAttributes.put("approvedAmount", amount.multiply(minorCurrencyFactor));
				return AuthResultCode.APPROVED;
			default:
				throw new RetrySpecifiedServiceException("Action '" + action + "' NOT implemented", WorkRetryType.NO_RETRY);
		}
	}

	/**
	 * @see com.usatech.authoritylayer.AbstractAuthorityTask#processReversal(com.usatech.layers.common.constants.AuthorityAction, com.usatech.app.MessageChainTaskInfo)
	 */
	@Override
	protected AuthResultCode processReversal(AuthorityAction action, AuthorityAction subAction, MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		String remoteServerAddr;
		String currencyCd;
		String consumerAcctToken;
		BigDecimal amount;
		BigDecimal minorCurrencyFactor;
		String transactionID;
		String approvalCode;
		try {
			remoteServerAddr = getAttribute(step, AuthorityAttrEnum.ATTR_REMOTE_SERVER_ADDRESS, String.class, true);
			consumerAcctToken = getAttribute(step, AuthorityAttrEnum.ATTR_CONSUMER_ACCT_TOKEN, String.class, true);
			minorCurrencyFactor = getAttribute(step, AuthorityAttrEnum.ATTR_MINOR_CURRENCY_FACTOR, BigDecimal.class, true);
			amount = getAttribute(step, AuthorityAttrEnum.ATTR_AMOUNT, BigDecimal.class, true).divide(minorCurrencyFactor);
			transactionID = getAttribute(step, AuthorityAttrEnum.ATTR_TRACE_NUMBER, String.class, true);
			currencyCd = getAttribute(step, AuthorityAttrEnum.ATTR_CURRENCY_CD, String.class, true);
			approvalCode = getAttribute(step, AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TRAN_CD, String.class, false);
		} catch(AuthorityAttributeConversionException e) {
			return AuthResultCode.FAILED;
		}
		
		Map<String, Object> resultAttributes = taskInfo.getStep().getResultAttributes();
		switch(action) {
			case AUTHORIZATION:
				try {
					M2P_Value_Transaction_RequestE requestMessage = new M2P_Value_Transaction_RequestE();
					M2P_Value_Transaction_Request request = new M2P_Value_Transaction_Request();
					request.setValue_Transaction_Type("Capture");
					request.setValue_Transaction_Amount(BigDecimal.ZERO);
					request.setValue_Transaction_Consumer_Identifier(consumerAcctToken);
					request.setValue_Transaction_Currency(currencyCd);
					request.setValue_Transaction_Reference_Id(transactionID);
					request.setValue_Transaction_Details(approvalCode);
					requestMessage.setM2P_Value_Transaction_Request(request);
					log.info(StringUtils.objectToString(request, "Sending Capture request for auth reversal to M2P server: "));
					M2P_Value_TransactionStub vzm2p = new M2P_Value_TransactionStub(remoteServerAddr);
					vzm2p._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT, vzm2pConnectionTimeout);
		    		vzm2p._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, vzm2pSocketTimeout);
					M2P_Value_Transaction_ResponseE responseMessage = vzm2p.m2P_Value_Transaction_Request(requestMessage);
					M2P_Value_Transaction_Response response = responseMessage.getM2P_Value_Transaction_Response();
					log.info(StringUtils.objectToString(response, "Received Capture response from M2P server: "));
					return populateResults(response, amount, minorCurrencyFactor, transactionID, taskInfo.getStep().getResultAttributes());
				} catch(RemoteException e) {
					log.warn("Error communicating with VZM2P server", e);
					return populateDeclinedResults(action, taskInfo.getStep().getResultAttributes(), null, e.getMessage());
				}
			default:
				String text = "Reversal of Action '" + action + "' NOT implemented"; 
				log.warn(text);
				resultAttributes.put("authorityRespDesc", text);
				return AuthResultCode.FAILED;
		}
	}
	protected AuthResultCode populateExceptionResults(AuthorityAction action, Map<String, Object> resultAttributes, POSException e) {
		if(e.getStatusCode() < 0 || e.getStatusCode() > 3) {
			return populateFailedResults(action, resultAttributes, null, e.getDescription());
		}
		resultAttributes.put("authResultCd", 'N');
		resultAttributes.put("authorityRespCd", e.getStatusCode());
		resultAttributes.put("authorityRespDesc", e.getResponseMessage());
		return AuthResultCode.DECLINED;
	}

	protected AuthResultCode populateResults(M2P_Value_Transaction_Response response, BigDecimal requestedAmount, BigDecimal minorCurrencyFactor, String transactionId, Map<String, Object> resultAttributes) {
		String processingStatus = response.getM2P_Processing_Status();
		if (processingStatus == null)
			processingStatus = "";
		String processingDetails = response.getM2P_Processing_Details();
		if (processingDetails == null)
			processingDetails = "";
		resultAttributes.put("authorityRespCd", processingStatus);
		if(!transactionId.equals(response.getM2P_Transaction_Identifier())) { // mis-matched tran id - approval fails
			resultAttributes.put("authResultCd", 'F');
			resultAttributes.put("authorityRespDesc", "Mis-matched transactionID: sent '" + transactionId + "', received '" + response.getM2P_Transaction_Identifier() + "'");
			return AuthResultCode.FAILED;
		}
		if ("Complete".equals(processingStatus)) {
			// Approved
			resultAttributes.put("authResultCd", 'Y');
			resultAttributes.put("authorityRespDesc", processingDetails);			
			resultAttributes.put("settled", true);
			resultAttributes.put("approvedAmount", requestedAmount.multiply(minorCurrencyFactor));
			return AuthResultCode.APPROVED;
		} else if (processingDetails.contains("Insufficient Funds")) {
			// Declined
			resultAttributes.put("authResultCd", 'N');
			resultAttributes.put("authorityRespDesc", processingDetails);
			return AuthResultCode.DECLINED;
		} else {
			// Failed
			resultAttributes.put("authResultCd", 'F');
			resultAttributes.put("authorityRespDesc", processingDetails);
			return AuthResultCode.FAILED;
		}
	}

	protected AuthResultCode populateAuthResults(M2P_Value_Transaction_Response response, BigDecimal requestedAmount, BigDecimal minorCurrencyFactor, String transactionId, Map<String, Object> resultAttributes) {
		String processingStatus = response.getM2P_Processing_Status();
		if (processingStatus == null)
			processingStatus = "";
		String processingDetails = response.getM2P_Processing_Details();
		if (processingDetails == null)
			processingDetails = "";
		resultAttributes.put("authorityRespCd", processingStatus);
		AuthResultCode authResultCode;
		if(!transactionId.equals(response.getM2P_Transaction_Identifier())) { // mis-matched tran id - approval fails
			resultAttributes.put("authResultCd", 'F');
			resultAttributes.put("authorityRespDesc", "Mis-matched transactionID: sent '" + transactionId + "', received '" + response.getM2P_Transaction_Identifier() + "'");
			authResultCode = AuthResultCode.FAILED;
			setClientText(resultAttributes, authResultCode, DeniedReason.INVALID_RESPONSE, "Mis-matched transaction id");
		} else if("Complete".equals(processingStatus) && processingDetails.startsWith("AuthKey=")) {
			// Approved
			resultAttributes.put("authResultCd", 'Y');
			resultAttributes.put("authorityRespDesc", processingDetails);
			int approvalCodeEnd = processingDetails.indexOf(";", 8);
			String approvalCode;
			if (approvalCodeEnd > -1)
				approvalCode = processingDetails.substring(8, approvalCodeEnd);
			else
				approvalCode = processingDetails.substring(8);
			resultAttributes.put("authAuthorityTranCd", approvalCode);
			//resultAttributes.put("authAuthorityRefCd", approvalCode);
			BigDecimal approvedAmount = requestedAmount.multiply(minorCurrencyFactor);
			resultAttributes.put("approvedAmount", approvedAmount);
			resultAttributes.put("authHoldUsed", true);
			authResultCode = AuthResultCode.APPROVED;
			setClientText(resultAttributes, authResultCode, null);
		} else if (processingDetails.contains("Insufficient Funds")) {
			// Declined
			resultAttributes.put("authResultCd", 'N');
			resultAttributes.put("authorityRespDesc", processingDetails);
			authResultCode = AuthResultCode.DECLINED;
			setClientText(resultAttributes, authResultCode, DeniedReason.INSUFFICIENT_FUNDS, processingDetails);
		} else {
			// Failed
			resultAttributes.put("authResultCd", 'F');
			resultAttributes.put("authorityRespDesc", processingDetails);
			authResultCode = AuthResultCode.FAILED;
			setClientText(resultAttributes, authResultCode, null, processingDetails);
		}
		return authResultCode;
	}

	@Override
	protected String[] getAdditionalPreActionLogAttributes() {
		return new String[] {
			MessageAttribute.REMOTE_SERVER_ADDR.getValue()
		};
	}

	public static int getVzm2pAuthConnectionTimeout() {
		return vzm2pAuthConnectionTimeout;
	}

	public static void setVzm2pAuthConnectionTimeout(int vzm2pAuthConnectionTimeout) {
		VZM2PGatewayTask.vzm2pAuthConnectionTimeout = vzm2pAuthConnectionTimeout;
	}

	public static int getVzm2pAuthSocketTimeout() {
		return vzm2pAuthSocketTimeout;
	}

	public static void setVzm2pAuthSocketTimeout(int vzm2pAuthSocketTimeout) {
		VZM2PGatewayTask.vzm2pAuthSocketTimeout = vzm2pAuthSocketTimeout;
	}

	public static int getVzm2pConnectionTimeout() {
		return vzm2pConnectionTimeout;
	}

	public static void setVzm2pConnectionTimeout(int vzm2pConnectionTimeout) {
		VZM2PGatewayTask.vzm2pConnectionTimeout = vzm2pConnectionTimeout;
	}

	public static int getVzm2pSocketTimeout() {
		return vzm2pSocketTimeout;
	}

	public static void setVzm2pSocketTimeout(int vzm2pSocketTimeout) {
		VZM2PGatewayTask.vzm2pSocketTimeout = vzm2pSocketTimeout;
	}
}
