package com.usatech.authoritylayer.submerchants;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SubMerchant {

	private ActionCode actionCode;
	private MatchReson matchReson;
	private Company company;
	private List<Principal> principals = new ArrayList<>();

	public ActionCode getActionCode() {
		return actionCode;
	}

	public void setActionCode(ActionCode actionCode) {
		this.actionCode = actionCode;
	}

	public MatchReson getMatchReson() {
		return matchReson;
	}

	public void setMatchReson(MatchReson matchReson) {
		this.matchReson = matchReson;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public List<Principal> getPrincipals() {
		return principals;
	}

	public void setPrincipals(List<Principal> principals) {
		this.principals = principals;
	}
	
	public void addPrincipal(Principal principal){
		principals.add(principal);
	}
	
	@Override
	public String toString() {
		return String.format("SubMerchant[actionCode=%s, matchReson=%s, company=%s, principals=%s]", actionCode, matchReson, company, principals);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((actionCode == null) ? 0 : actionCode.hashCode());
		result = prime * result + ((company == null) ? 0 : company.hashCode());
		result = prime * result + ((matchReson == null) ? 0 : matchReson.hashCode());
		result = prime * result + ((principals == null) ? 0 : principals.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		SubMerchant other = (SubMerchant)obj;
		if(actionCode != other.actionCode)
			return false;
		if(company == null) {
			if(other.company != null)
				return false;
		} else if(!company.equals(other.company))
			return false;
		if(matchReson != other.matchReson)
			return false;
		if(principals == null) {
			if(other.principals != null)
				return false;
		} else if(!principals.equals(other.principals))
			return false;
		return true;
	}

	public static enum CompanyTaxIdType {
		CANADIAN_BUSINESS('H', "Canadian business number"),
		CANADIAN_INSURANCE('I', "Canadian insurance number"),
		UNKNOWN('U', "Unknown type"),
		SSN('S', "USA Social Security Number"),
		TAX_ID('T', "USA tax ID"),
		VAT('V', "VAT");

		private final char code;
		private final String description;

		private CompanyTaxIdType(char code, String description) {
			this.code = code;
			this.description = description;
		}

		public char getCode() {
			return code;
		}

		public String getDescription() {
			return description;
		}
	}

	public static enum PrincipalTaxIdType {
		SSN('S', "SSN"),
		EIN('E', "EIN"),
		FOREIGN('F', "Foreign");

		private final char code;
		private final String description;

		private PrincipalTaxIdType(char code, String description) {
			this.code = code;
			this.description = description;
		}

		public char getCode() {
			return code;
		}

		public String getDescription() {
			return description;
		}
	}

	public static enum ActionCode {
		ADD("AD", "Add to MATCH Repository"),
		CHANGE("CH", "Existing merchant, change in owner, send for inquiry"),
		NEW("IN", "New merchant inquiry");

		private final String code;
		private final String description;

		private ActionCode(String code, String description) {
			this.code = code;
			this.description = description;
		}

		public String getCode() {
			return code;
		}

		public String getDescription() {
			return description;
		}
	}

	public static enum PhoneType {
		CELL('C', "Cell"),
		HOME('H', "Home"),
		WORK('W', "Work");

		private final char code;
		private final String description;

		private PhoneType(char code, String description) {
			this.code = code;
			this.description = description;
		}

		public char getCode() {
			return code;
		}

		public String getDescription() {
			return description;
		}
	}

	public static enum MatchReson {
		DATA_COMPROMISE(1, "Account Data Compromise"),
		COMMON_POINT_OF_PURCHASE(2, "Common Point of Purchase"),
		LAUNDERING(3, "Laundering"),
		EXCESSIVE_CHARGEBACKS(4, "Excessive Chargebacks"),
		EXCESSIVE_FRAUD(5, "Excessive Fraud"),
		RESERVED_6(6, "Reserved for Future Use"),
		FRAUD_CONVICTION(7, "Fraud Conviction"),
		RESERVED_8(8, "Reserved for Future Use"),
		BANKRUPTCY(9, "Bankruptcy/Liquidation/insolvency"),
		VIOLATION_OF_STANDARDS(10, "Violation of MasterCard Standards"),
		MERCHANT_COLLUSION(11, "Merchant Collusion"),
		PCIDSS_NONCOMPLIANCE(12, "PCI Data Security Standard Noncompliance"),
		ILLEGAL_TRANSACTIONS(13, "Illegal Transactions"),
		IDENTITY_THEFT(14, "Identity Theft");

		private final int code;
		private final String description;

		private MatchReson(int code, String description) {
			this.code = code;
			this.description = description;
		}

		public int getCode() {
			return code;
		}

		public String getDescription() {
			return description;
		}
	}

	public static class Location {

		private String streetAddress1;
		private String streetAddress2;
		private String streetAddress3;
		private String city;
		private String state;
		private String countryCode;
		private String postalCode;
		private String phone;
		private PhoneType phoneType = PhoneType.WORK;

		public String getStreetAddress1() {
			return streetAddress1;
		}

		public void setStreetAddress1(String streetAddress1) {
			this.streetAddress1 = streetAddress1;
		}

		public String getStreetAddress2() {
			return streetAddress2;
		}

		public void setStreetAddress2(String streetAddress2) {
			this.streetAddress2 = streetAddress2;
		}

		public String getStreetAddress3() {
			return streetAddress3;
		}

		public void setStreetAddress3(String streetAddress3) {
			this.streetAddress3 = streetAddress3;
		}

		public String getCity() {
			return city;
		}

		public void setCity(String city) {
			this.city = city;
		}

		public String getState() {
			return state;
		}

		public void setState(String state) {
			this.state = state;
		}

		public String getCountryCode() {
			return countryCode;
		}

		public void setCountryCode(String countryCode) {
			this.countryCode = countryCode;
		}

		public String getPostalCode() {
			return postalCode;
		}

		public void setPostalCode(String postalCode) {
			this.postalCode = postalCode;
		}

		public String getPhone() {
			return phone;
		}

		public void setPhone(String phone) {
			this.phone = phone;
		}

		public PhoneType getPhoneType() {
			return phoneType;
		}

		public void setPhoneType(PhoneType phoneType) {
			this.phoneType = phoneType;
		}

		@Override
		public String toString() {
			return String.format("Location[streetAddress1=%s, streetAddress2=%s, streetAddress3=%s, city=%s, state=%s, countryCode=%s, postalCode=%s, phone=%s, phoneType=%s]", streetAddress1, streetAddress2, streetAddress3, city, state, countryCode, postalCode, phone, phoneType);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((city == null) ? 0 : city.hashCode());
			result = prime * result + ((countryCode == null) ? 0 : countryCode.hashCode());
			result = prime * result + ((phone == null) ? 0 : phone.hashCode());
			result = prime * result + ((phoneType == null) ? 0 : phoneType.hashCode());
			result = prime * result + ((postalCode == null) ? 0 : postalCode.hashCode());
			result = prime * result + ((state == null) ? 0 : state.hashCode());
			result = prime * result + ((streetAddress1 == null) ? 0 : streetAddress1.hashCode());
			result = prime * result + ((streetAddress2 == null) ? 0 : streetAddress2.hashCode());
			result = prime * result + ((streetAddress3 == null) ? 0 : streetAddress3.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if(this == obj)
				return true;
			if(obj == null)
				return false;
			if(getClass() != obj.getClass())
				return false;
			Location other = (Location)obj;
			if(city == null) {
				if(other.city != null)
					return false;
			} else if(!city.equals(other.city))
				return false;
			if(countryCode == null) {
				if(other.countryCode != null)
					return false;
			} else if(!countryCode.equals(other.countryCode))
				return false;
			if(phone == null) {
				if(other.phone != null)
					return false;
			} else if(!phone.equals(other.phone))
				return false;
			if(phoneType != other.phoneType)
				return false;
			if(postalCode == null) {
				if(other.postalCode != null)
					return false;
			} else if(!postalCode.equals(other.postalCode))
				return false;
			if(state == null) {
				if(other.state != null)
					return false;
			} else if(!state.equals(other.state))
				return false;
			if(streetAddress1 == null) {
				if(other.streetAddress1 != null)
					return false;
			} else if(!streetAddress1.equals(other.streetAddress1))
				return false;
			if(streetAddress2 == null) {
				if(other.streetAddress2 != null)
					return false;
			} else if(!streetAddress2.equals(other.streetAddress2))
				return false;
			if(streetAddress3 == null) {
				if(other.streetAddress3 != null)
					return false;
			} else if(!streetAddress3.equals(other.streetAddress3))
				return false;
			return true;
		}

	}

	public static class Company {

		private int identifier;
		private String name;
		private String dba;
		private String taxId;
		private CompanyTaxIdType taxIdType = CompanyTaxIdType.TAX_ID;
		private String mcc;
		private Date dateOfAgreement;
		private Location location;
		private String email;
		private String website;

		public int getIdentifier() {
			return identifier;
		}

		public void setIdentifier(int identifier) {
			this.identifier = identifier;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getDba() {
			return dba;
		}

		public void setDba(String dba) {
			this.dba = dba;
		}

		public String getTaxId() {
			return taxId;
		}

		public void setTaxId(String taxId) {
			this.taxId = taxId;
		}

		public CompanyTaxIdType getTaxIdType() {
			return taxIdType;
		}

		public void setTaxIdType(CompanyTaxIdType taxIdType) {
			this.taxIdType = taxIdType;
		}

		public String getMcc() {
			return mcc;
		}

		public void setMcc(String mcc) {
			this.mcc = mcc;
		}

		public Date getDateOfAgreement() {
			return dateOfAgreement;
		}

		public void setDateOfAgreement(Date dateOfAgreement) {
			this.dateOfAgreement = dateOfAgreement;
		}

		public Location getLocation() {
			return location;
		}

		public void setLocation(Location location) {
			this.location = location;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getWebsite() {
			return website;
		}

		public void setWebsite(String website) {
			this.website = website;
		}

		@Override
		public String toString() {
			return String.format("Company[identifier=%s, name=%s, dba=%s, taxId=%s, taxIdType=%s, mcc=%s, dateOfAgreement=%s, location=%s, email=%s, website=%s]", identifier, name, dba, taxId, taxIdType, mcc, dateOfAgreement, location, email, website);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((dateOfAgreement == null) ? 0 : dateOfAgreement.hashCode());
			result = prime * result + ((dba == null) ? 0 : dba.hashCode());
			result = prime * result + ((email == null) ? 0 : email.hashCode());
			result = prime * result + identifier;
			result = prime * result + ((location == null) ? 0 : location.hashCode());
			result = prime * result + ((mcc == null) ? 0 : mcc.hashCode());
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			result = prime * result + ((taxId == null) ? 0 : taxId.hashCode());
			result = prime * result + ((taxIdType == null) ? 0 : taxIdType.hashCode());
			result = prime * result + ((website == null) ? 0 : website.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if(this == obj)
				return true;
			if(obj == null)
				return false;
			if(getClass() != obj.getClass())
				return false;
			Company other = (Company)obj;
			if(dateOfAgreement == null) {
				if(other.dateOfAgreement != null)
					return false;
			} else if(!dateOfAgreement.equals(other.dateOfAgreement))
				return false;
			if(dba == null) {
				if(other.dba != null)
					return false;
			} else if(!dba.equals(other.dba))
				return false;
			if(email == null) {
				if(other.email != null)
					return false;
			} else if(!email.equals(other.email))
				return false;
			if(identifier != other.identifier)
				return false;
			if(location == null) {
				if(other.location != null)
					return false;
			} else if(!location.equals(other.location))
				return false;
			if(mcc == null) {
				if(other.mcc != null)
					return false;
			} else if(!mcc.equals(other.mcc))
				return false;
			if(name == null) {
				if(other.name != null)
					return false;
			} else if(!name.equals(other.name))
				return false;
			if(taxId == null) {
				if(other.taxId != null)
					return false;
			} else if(!taxId.equals(other.taxId))
				return false;
			if(taxIdType != other.taxIdType)
				return false;
			if(website == null) {
				if(other.website != null)
					return false;
			} else if(!website.equals(other.website))
				return false;
			return true;
		}
	}

	public static class Principal {

		private String firstName;
		private String lastName;
		private String middleInitial;
		private Location location;
		private Date dob;
		private String taxId;
		private PrincipalTaxIdType taxIdType;

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getMiddleInitial() {
			return middleInitial;
		}

		public void setMiddleInitial(String middleInitial) {
			this.middleInitial = middleInitial;
		}

		public Location getLocation() {
			return location;
		}

		public void setLocation(Location location) {
			this.location = location;
		}

		public Date getDob() {
			return dob;
		}

		public void setDob(Date dob) {
			this.dob = dob;
		}

		public String getTaxId() {
			return taxId;
		}

		public void setTaxId(String taxId) {
			this.taxId = taxId;
		}

		public PrincipalTaxIdType getTaxIdType() {
			return taxIdType;
		}

		public void setTaxIdType(PrincipalTaxIdType taxIdType) {
			this.taxIdType = taxIdType;
		}

		@Override
		public String toString() {
			return String.format("Principal [firstName=%s, lastName=%s, middleInitial=%s, location=%s, dob=%s, taxId=%s, taxIdType=%s]", firstName, lastName, middleInitial, location, dob, taxId, taxIdType);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((dob == null) ? 0 : dob.hashCode());
			result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
			result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
			result = prime * result + ((location == null) ? 0 : location.hashCode());
			result = prime * result + ((middleInitial == null) ? 0 : middleInitial.hashCode());
			result = prime * result + ((taxId == null) ? 0 : taxId.hashCode());
			result = prime * result + ((taxIdType == null) ? 0 : taxIdType.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if(this == obj)
				return true;
			if(obj == null)
				return false;
			if(getClass() != obj.getClass())
				return false;
			Principal other = (Principal)obj;
			if(dob == null) {
				if(other.dob != null)
					return false;
			} else if(!dob.equals(other.dob))
				return false;
			if(firstName == null) {
				if(other.firstName != null)
					return false;
			} else if(!firstName.equals(other.firstName))
				return false;
			if(lastName == null) {
				if(other.lastName != null)
					return false;
			} else if(!lastName.equals(other.lastName))
				return false;
			if(location == null) {
				if(other.location != null)
					return false;
			} else if(!location.equals(other.location))
				return false;
			if(middleInitial == null) {
				if(other.middleInitial != null)
					return false;
			} else if(!middleInitial.equals(other.middleInitial))
				return false;
			if(taxId == null) {
				if(other.taxId != null)
					return false;
			} else if(!taxId.equals(other.taxId))
				return false;
			if(taxIdType != other.taxIdType)
				return false;
			return true;
		}
	}
}
