package com.usatech.authoritylayer;

import java.util.Map;

import simple.bean.ConvertUtils;
import simple.text.StringUtils;

import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.PaymentMaskBRef;

public class AccountData {
	protected final Map<String, Object> attributes;
	protected final boolean useDecryptionAttribute;
	protected String trackData;
	protected String[] partData;

	public AccountData(Map<String, Object> attributes) {
		this(attributes, false);
	}

	public AccountData(Map<String, Object> attributes, boolean useDecryptionAttribute) {
		this.attributes = attributes;
		this.useDecryptionAttribute = useDecryptionAttribute;
	}

	public String getTrackData() {
		if(trackData == null) {
			trackData = ConvertUtils.getStringSafely(attributes.get(AuthorityAttrEnum.ATTR_TRACK_DATA.getValue()), null);
			if(StringUtils.isBlank(trackData))
				trackData = ConvertUtils.getStringSafely(attributes.get(AuthorityAttrEnum.ATTR_AUTH_ACCOUNT_DATA.getValue()), AbstractAuthorityTask.EMPTY_STRING);
		}
		return trackData.length() == 0 ? null : trackData;
	}

	public String getPartData(PaymentMaskBRef part) {
		if(partData == null)
			partData = new String[PaymentMaskBRef.values().length];
		final int index = part.ordinal();
		if(partData[index] == null) {
			partData[index] = ConvertUtils.getStringSafely(attributes.get((useDecryptionAttribute && part.getStoreIndex() > 0 ? part.decryptionAttribute : part.attribute).getValue()), AbstractAuthorityTask.EMPTY_STRING);
		} else if(partData[index].length() == 0)
			return null;
		return partData[index];
	}
}