package com.usatech.sprout;

import java.math.BigDecimal;

public class SproutPurchaseResponse {
	protected String account_id;
	protected String account_url;
	protected BigDecimal net_transaction_amount;
	protected BigDecimal net_transaction_points;
	protected BigDecimal new_available_balance;
	protected BigDecimal new_points_balance;
	protected BigDecimal reward_amount;
	protected BigDecimal reward_points;
	protected String reward_url;
	protected BigDecimal transaction_id;
	protected String url;
	
	public String getAccount_id() {
		return account_id;
	}
	public void setAccount_id(String account_id) {
		this.account_id = account_id;
	}
	public String getAccount_url() {
		return account_url;
	}
	public void setAccount_url(String account_url) {
		this.account_url = account_url;
	}
	public BigDecimal getNet_transaction_amount() {
		return net_transaction_amount;
	}
	public void setNet_transaction_amount(BigDecimal net_transaction_amount) {
		this.net_transaction_amount = net_transaction_amount;
	}
	public BigDecimal getNet_transaction_points() {
		return net_transaction_points;
	}
	public void setNet_transaction_points(BigDecimal net_transaction_points) {
		this.net_transaction_points = net_transaction_points;
	}
	public BigDecimal getNew_available_balance() {
		return new_available_balance;
	}
	public void setNew_available_balance(BigDecimal new_available_balance) {
		this.new_available_balance = new_available_balance;
	}
	public BigDecimal getNew_points_balance() {
		return new_points_balance;
	}
	public void setNew_points_balance(BigDecimal new_points_balance) {
		this.new_points_balance = new_points_balance;
	}
	public BigDecimal getReward_amount() {
		return reward_amount;
	}
	public void setReward_amount(BigDecimal reward_amount) {
		this.reward_amount = reward_amount;
	}
	public BigDecimal getReward_points() {
		return reward_points;
	}
	public void setReward_points(BigDecimal reward_points) {
		this.reward_points = reward_points;
	}
	public String getReward_url() {
		return reward_url;
	}
	public void setReward_url(String reward_url) {
		this.reward_url = reward_url;
	}
	public BigDecimal getTransaction_id() {
		return transaction_id;
	}
	public void setTransaction_id(BigDecimal transaction_id) {
		this.transaction_id = transaction_id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
}
