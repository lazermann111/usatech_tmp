package com.usatech.sprout;

public class SproutReversal {
	protected String external_reference_id;
	protected String purchase_external_reference_id;
	protected String vampire_pos_id;
	
	public String getExternal_reference_id() {
		return external_reference_id;
	}
	public void setExternal_reference_id(String external_reference_id) {
		this.external_reference_id = external_reference_id;
	}
	public String getPurchase_external_reference_id() {
		return purchase_external_reference_id;
	}
	public void setPurchase_external_reference_id(String purchase_external_reference_id) {
		this.purchase_external_reference_id = purchase_external_reference_id;
	}
	public String getVampire_pos_id() {
		return vampire_pos_id;
	}
	public void setVampire_pos_id(String vampire_pos_id) {
		this.vampire_pos_id = vampire_pos_id;
	}
}
