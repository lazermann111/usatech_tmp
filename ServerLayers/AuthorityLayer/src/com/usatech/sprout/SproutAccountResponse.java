package com.usatech.sprout;

import java.math.BigDecimal;

public class SproutAccountResponse {
	protected String account_id;
	protected boolean active;
	protected BigDecimal available_balance;
	protected BigDecimal current_balance;
	protected BigDecimal points_balance;
	protected String program_url;
	protected String url;
	
	public String getAccount_id() {
		return account_id;
	}

	public void setAccount_id(String account_id) {
		this.account_id = account_id;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public BigDecimal getAvailable_balance() {
		return available_balance;
	}

	public void setAvailable_balance(BigDecimal available_balance) {
		this.available_balance = available_balance;
	}

	public BigDecimal getCurrent_balance() {
		return current_balance;
	}

	public void setCurrent_balance(BigDecimal current_balance) {
		this.current_balance = current_balance;
	}

	public BigDecimal getPoints_balance() {
		return points_balance;
	}

	public void setPoints_balance(BigDecimal points_balance) {
		this.points_balance = points_balance;
	}

	public String getProgram_url() {
		return program_url;
	}

	public void setProgram_url(String program_url) {
		this.program_url = program_url;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
