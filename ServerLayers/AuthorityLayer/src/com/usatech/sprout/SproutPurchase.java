package com.usatech.sprout;

import java.util.List;

public class SproutPurchase {
	protected String card_number;
	protected String external_reference_id;
	protected String vampire_pos_id ;
	protected List<SproutPurchaseItem> items;
	
	public String getCard_number() {
		return card_number;
	}
	public void setCard_number(String card_number) {
		this.card_number = card_number;
	}
	public String getExternal_reference_id() {
		return external_reference_id;
	}
	public void setExternal_reference_id(String external_reference_id) {
		this.external_reference_id = external_reference_id;
	}
	public String getVampire_pos_id() {
		return vampire_pos_id;
	}
	public void setVampire_pos_id(String vampire_pos_id) {
		this.vampire_pos_id = vampire_pos_id;
	}
	public List<SproutPurchaseItem> getItems() {
		return items;
	}
	public void setItems(List<SproutPurchaseItem> items) {
		this.items = items;
	}
}
