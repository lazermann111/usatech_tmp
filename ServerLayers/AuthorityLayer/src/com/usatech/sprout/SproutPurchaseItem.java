package com.usatech.sprout;

import java.math.BigDecimal;

public class SproutPurchaseItem {
	protected BigDecimal price;
	protected String coil_name;

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getCoil_name() {
		return coil_name;
	}

	public void setCoil_name(String coil_name) {
		this.coil_name = coil_name;
	}
}
