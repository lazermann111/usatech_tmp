#!/bin/sh
# This script runs Reupload Files
# Resolve links - $0 may be a softlink
PRG="$0"

while [ -h "$PRG" ]; do
  ls=`ls -ld "$PRG"`
  link=`expr "$ls" : '.*-> \(.*\)$'`
  if expr "$link" : '/.*' > /dev/null; then
    PRG="$link"
  else
    PRG=`dirname "$PRG"`/"$link"
  fi
done

# Get standard environment variables
PRGDIR=`dirname "$PRG"`
cd $PRGDIR/..
STDOUT_LOG="logs/ReuploadFiles.stdout.log"
			
# setenv.sh can set JAVA_OPTIONS JAVA_HOME and APP_OPTS
if [ -f bin/setenv.sh ];
then
	. bin/setenv.sh 2>&1 >> $STDOUT_LOG
else
	echo "[`date`] No setenv.sh file." >> $STDOUT_LOG
fi

if [ -z "$JAVA_HOME" ]
then
    JAVA=`which java`
    if [ ! -x "$JAVA" ]; then
      echo "[`date`] Cannot find java command. Please set the JAVA_HOME environment variable." >> $STDOUT_LOG
      exit 1
    fi
else
    JAVA=$JAVA_HOME/bin/java
fi
export JAVA
			
CLASSPATH="classes:lib/simple-selected.jar:lib/usat-messaging.jar:lib/layers-common.jar:lib/authoritylayer.jar:lib/pos-gateway.jar:lib/aramark-gateway.jar:lib/blackboard-gateway.jar:lib/interchange-gateway.jar:lib/CreditCardUtils-2006-06-01.jar:lib/activation-1.1.jar:lib/activemq-core-5.3.1.jar:lib/axis-1.4.0.jar:lib/common-2005-04-06.jar:lib/commons-beanutils-1.6.jar:lib/commons-codec-1.3.jar:lib/commons-collections-3.1.jar:lib/commons-configuration-1.5.jar:lib/commons-dbcp-1.2.2.jar:lib/commons-discovery-0.4.jar:lib/commons-httpclient-3.1.jar:lib/commons-lang-2.4.jar:lib/commons-logging-1.1.1.jar:lib/commons-pool-1.4.jar:lib/crypto-2004-04-28.jar:lib/ftpsbean-1.4.5-usat-5.jar:lib/hsqldb-1.8.0.jar:lib/ibizptech-2008-07-18.jar:lib/j2ee-management-1.0.jar:lib/jaxrpc-1.1.0.jar:lib/jms-1.1.jar:lib/jpos-1.4.8.jar:lib/log4j-1.2.15.jar:lib/mail-1.4.2.jar:lib/ojdbc6_g-11.2.0.1.0.jar:lib/picocontainer-1.1.jar:lib/postgresql-8.4-701.jdbc4-usat-1.jar:lib/saaj-1.2.0.jar:lib/usat-jmx-agent-1.0.3.jar:lib/wsdl4j-1.5.1.jar:lib/xstream-1.1.2.jar"
echo "[`date`] USING JAVA_HOME: $JAVA_HOME" >> $STDOUT_LOG
			
HOSTNAME=`uname -n`			

echo "[`date`] Reupload Files ($1)..."
$JAVA "-Dapp.servicename=ReuploadFiles" -Dfile.encoding=ISO8859-1 -Dlog4j.configuration=log4j-console.properties -Dlog4j.configuratorClass=simple.io.logging.Log4JWatchingConfigurator -Dapp.hostname=$HOSTNAME -Djavax.net.ssl.trustStore=../conf/truststore.ts -cp $CLASSPATH com.usatech.authoritylayer.ReuploadFiles -p OutsideAuthorityLayerService.properties $1 2>&1 
echo "[`date`] Reupload Files Finished"
			
