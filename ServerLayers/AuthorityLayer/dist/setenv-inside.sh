#!/bin/sh
echo "SET ENV OPTIONS..."
JAVA_OPTS="-Xmx256m -Dcom.sun.management.jmxremote -Djmx.remote.x.rmiRegistryPort=7771 -Djmx.remote.x.rmiServerPort=7772 -Djmx.remote.x.login.config=JMXControl -Djmx.remote.x.access.file=../conf/jmx.access -Djava.security.auth.login.config=../conf/jmx.login.config -Djavax.net.ssl.keyStore=../conf/keystore.ks -Djavax.net.ssl.keyStorePassword=usatech -Djavax.net.ssl.trustStore=../conf/truststore.ts -Djavax.net.ssl.trustStorePassword=usatech"
APP_OPTS="-i 7770"
JAVA_HOME=/usr/jdk/latest
export JAVA_OPTS
export APP_OPTS
export JAVA_HOME

