package com.educationaramark.webservices.relayservice;


public class RelayServiceSoapStub extends org.apache.axis.client.Stub implements com.educationaramark.webservices.relayservice.RelayServiceSoap {
    private final java.util.Vector cachedSerClasses = new java.util.Vector();
    private final java.util.Vector cachedSerQNames = new java.util.Vector();
    private final java.util.Vector cachedSerFactories = new java.util.Vector();
    private final java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[1];
        org.apache.axis.description.OperationDesc oper;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("POS_Transaction");
        oper.addParameter(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "POSTransaction"), new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "POSLOG"), com.educationaramark.webservices.relayservice.POSLOG.class, org.apache.axis.description.ParameterDesc.IN, false, false);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "POSResponse"));
        oper.setReturnClass(com.educationaramark.webservices.relayservice.POSResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "POS_TransactionResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

    }

    public RelayServiceSoapStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public RelayServiceSoapStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public RelayServiceSoapStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "SaleReturnLineItem");
            cachedSerQNames.add(qName);
            cls = com.educationaramark.webservices.relayservice.SaleReturnLineItem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "POSResponse");
            cachedSerQNames.add(qName);
            cls = com.educationaramark.webservices.relayservice.POSResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "Board");
            cachedSerQNames.add(qName);
            cls = com.educationaramark.webservices.relayservice.Board.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "POSLOG");
            cachedSerQNames.add(qName);
            cls = com.educationaramark.webservices.relayservice.POSLOG.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "RetailTransactionLineItem");
            cachedSerQNames.add(qName);
            cls = com.educationaramark.webservices.relayservice.RetailTransactionLineItem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "ArrayOfTenderChange");
            cachedSerQNames.add(qName);
            cls = com.educationaramark.webservices.relayservice.ArrayOfTenderChange.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "DiscountLineItem");
            cachedSerQNames.add(qName);
            cls = com.educationaramark.webservices.relayservice.DiscountLineItem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "POSResponse_Transaction");
            cachedSerQNames.add(qName);
            cls = com.educationaramark.webservices.relayservice.POSResponse_Transaction.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "ArrayOfSaleReturnLineItem");
            cachedSerQNames.add(qName);
            cls = com.educationaramark.webservices.relayservice.ArrayOfSaleReturnLineItem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "Inquiry");
            cachedSerQNames.add(qName);
            cls = com.educationaramark.webservices.relayservice.Inquiry.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "POSResponse_Customer");
            cachedSerQNames.add(qName);
            cls = com.educationaramark.webservices.relayservice.POSResponse_Customer.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "TenderChange");
            cachedSerQNames.add(qName);
            cls = com.educationaramark.webservices.relayservice.TenderChange.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "CashEquivalency");
            cachedSerQNames.add(qName);
            cls = com.educationaramark.webservices.relayservice.CashEquivalency.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "RetailTransaction");
            cachedSerQNames.add(qName);
            cls = com.educationaramark.webservices.relayservice.RetailTransaction.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "DecliningBalance");
            cachedSerQNames.add(qName);
            cls = com.educationaramark.webservices.relayservice.DecliningBalance.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "Credit");
            cachedSerQNames.add(qName);
            cls = com.educationaramark.webservices.relayservice.Credit.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "TaxLines");
            cachedSerQNames.add(qName);
            cls = com.educationaramark.webservices.relayservice.TaxLines.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "Transaction");
            cachedSerQNames.add(qName);
            cls = com.educationaramark.webservices.relayservice.Transaction.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "TaxLineItem");
            cachedSerQNames.add(qName);
            cls = com.educationaramark.webservices.relayservice.TaxLineItem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "ArrayOfTaxLineItem");
            cachedSerQNames.add(qName);
            cls = com.educationaramark.webservices.relayservice.ArrayOfTaxLineItem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "ArrayOfTenderLineItem");
            cachedSerQNames.add(qName);
            cls = com.educationaramark.webservices.relayservice.ArrayOfTenderLineItem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "TenderLineItem");
            cachedSerQNames.add(qName);
            cls = com.educationaramark.webservices.relayservice.TenderLineItem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "ArrayOfTaxLines");
            cachedSerQNames.add(qName);
            cls = com.educationaramark.webservices.relayservice.ArrayOfTaxLines.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    private org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call =
                    (org.apache.axis.client.Call) super.service.createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                        java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                        _call.registerTypeMapping(cls, qName, sf, df, false);
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", t);
        }
    }

    public com.educationaramark.webservices.relayservice.POSResponse POS_Transaction(com.educationaramark.webservices.relayservice.POSLOG POSTransaction) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://webservices.educationaramark.com/relayservice/POS_Transaction");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "POS_Transaction"));

        setRequestHeaders(_call);
        setAttachments(_call);
        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {POSTransaction});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.educationaramark.webservices.relayservice.POSResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.educationaramark.webservices.relayservice.POSResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.educationaramark.webservices.relayservice.POSResponse.class);
            }
        }
    }

}
