/*jadclipse*/package ibizptech;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;

import XcoreXibizptechX50X3119.c;
import XcoreXibizptechX50X3119.d;

public class IBizPtechV5LicenseInstaller extends d {
	public IBizPtechV5LicenseInstaller() {
		/*
		StringBuilder sb = new StringBuilder("Loading Installer from ");
		ClassLoader classLoader = getClass().getClassLoader();
		sb.append("Loading Installer from ");
		sb.append(getClass().getName());
    	if(classLoader instanceof URLClassLoader) {
    		sb.append(" with urls=");
    		URL[] urls = ((URLClassLoader)classLoader).getURLs();
    		sb.append(Arrays.toString(urls));
    	}
    	System.out.println(sb.toString());
		*/
		Class class1 = getClass();
		String s1 = String.valueOf(String.valueOf((new StringBuffer("/")).append(class1.getName().replace('.', '/')).append(".class")));
		URL url = class1.getResource(s1);
		String s2 = url.getFile();
		byte byte0 = 0;
		if(s2.startsWith("file:"))
			byte0 = 5;
		int i1 = s2.indexOf("!");
		if(i1 > -1) {
			s2 = s2.substring(byte0, i1);
			s2 = s2.replaceAll("ibizptech.jar", "ibizptech.lic");
			s2 = s2.replaceAll("deploy.jar", "ibizptech.lic");
		} else {
			s2 = s2.substring(byte0);
			s2 = s2.replaceAll(s1, "/ibizptech.lic");
		}
		File file = new File(URLDecoder.decode(s2));

		System.out.println("+++ Looking for License at '" + file.getAbsolutePath() + "'");
		a = new BufferedReader(new InputStreamReader(System.in));
	}

	public static void main(String args[]) {
		(new IBizPtechV5LicenseInstaller()).install();
	}

	public void install() {
        Object obj = null;
        Object obj1 = null;
        try {
            File file;
            try {
                URLDecoder urldecoder = new URLDecoder();
                Class class1 = ibizptech.IBizPtechV5LicenseInstaller.class;
                String s2 = String.valueOf(String.valueOf((new StringBuffer("/")).append(class1.getName().replace('.', '/')).append(".class")));
                URL url = class1.getResource(s2);
                String s4 = url.getFile();
                byte byte0 = 0;
                if(s4.startsWith("file:"))
                    byte0 = 5;
                int i = s4.indexOf("!");
                if(i > -1) {
                    s4 = s4.substring(byte0, i);
                    s4 = s4.replaceAll("ibizptech.jar", "ibizptech.lic");
                } else {
                    s4 = s4.substring(byte0);
                    s4 = s4.replaceAll(s2, "/ibizptech.lic");
                }
                file = new File(URLDecoder.decode(s4));
            }
            catch(Exception exception) {
                System.out.println("Could not find ibizptech.jar.");
                return;
            }
            String s = "BTJ5V";
            String s1 = "http:";
            try {
                String s3 = c.a();
                s1 = String.valueOf(s1) + String.valueOf(a("nodeid", s3));
                System.out.println("Please enter your name and email address:");
                String s5 = b("Name");
                String s6 = b("Email Address");
                System.out.println();
                System.out.println("Please enter your serial number and key:");
                System.out.println("  (you may use \"TRIAL\" as the serial number to activate a trial license)");
                String s8 = b("Serial Number");
                s1 = String.valueOf(s1) + String.valueOf(a("setup", "true"));
                s1 = String.valueOf(s1) + String.valueOf(a("name", s5));
                s1 = String.valueOf(s1) + String.valueOf(a("email", s6));
                if(s8.equalsIgnoreCase("TRIAL")) {
                    s1 = String.valueOf(s1) + String.valueOf(a("a", "itrial"));
                } else {
                    s1 = String.valueOf(s1) + String.valueOf(a("sno", s8));
                    String s9 = b("Key");
                    s1 = String.valueOf(s1) + String.valueOf(a("key", s9));
                    byte abyte0[] = String.valueOf(String.valueOf(s9)).concat("\0").getBytes();
                    byte abyte1[] = "Ajf5b5pech5ps53u\0".getBytes();
                    c.a(abyte1, (byte)s.charAt(2), (byte)s.charAt(4));
                    byte abyte2[] = String.valueOf(String.valueOf(s8)).concat("\0").getBytes();
                    c.c(abyte2, 0);
                    s8 = new String(abyte2);
                    boolean flag = false;
                    if(s8.indexOf("1SUBR") > -1) {
                        byte abyte3[] = "Aredcarpetsub045\0".getBytes();
                        c.a(abyte3, (byte)85, (byte)82);
                        if(c.a(abyte2, new byte[0], abyte0, abyte3) == 0)
                            flag = true;
                    }
                    if(!flag) {
                        int j = c.a(abyte2, new byte[0], abyte0, abyte1);
                        if(j != 0) {
                            char c1 = (char)(65 + j);
                            throw new Exception(String.valueOf(String.valueOf((new StringBuffer("Invalid serial number and key [code: ")).append(c1).append(" nodeid: ").append(s3).append("]."))));
                        }
                    }
                    s1 = String.valueOf(s1) + String.valueOf(a("a", flag ? "rcset" : "vset"));
                }
            }
            catch(Exception exception1) {
                System.out.println("Error validating user input: ".concat(String.valueOf(String.valueOf(exception1.getMessage()))));
                return;
            }
            BufferedReader bufferedreader = null;
            PrintWriter printwriter = null;
            try {
                System.out.println();
                System.out.println("Downloading license data...");
                URL url1 = new URL(s1);
                bufferedreader = new BufferedReader(new InputStreamReader(url1.openStream()));
                printwriter = new PrintWriter(new FileWriter(file));
                for(String s7 = null; (s7 = bufferedreader.readLine()) != null;)
                    printwriter.println(s7);
                System.out.println("Verifying license data...");
            }
            catch(Exception exception2) {
                System.out.println("Error downloading license: ".concat(String.valueOf(String.valueOf(exception2.getMessage()))));
                System.out.println("\r\nPlease try to activate manually by going to: ");
                System.out.println(s1);
            }
            finally {
                if(bufferedreader != null)
                    try {
                        bufferedreader.close();
                    }
                    catch(IOException ioexception) { }
                if(printwriter != null) {
                    printwriter.flush();
                    printwriter.close();
                }
            }
            int ai[] = new int[1];
            try {
                d.a(0, null, ai, new String[1], false);
                System.out.println("License installation succeeded.");
            }
            catch(Exception exception4) {
                char c2 = (char)(65 + ai[0]);
                System.out.println(String.valueOf(String.valueOf((new StringBuffer("License verification failed [code: ")).append(c2).append("]. Please try again."))));
                if(file.exists())
                    file.delete();
            }
        }
        finally {
            try {
                System.out.println();
                System.out.println("Press any key to exit...");
                a.read();
            }
            catch(IOException ioexception1) { }
        }
    }

	private String b(String s) throws IOException {
		System.out.print(String.valueOf(String.valueOf(s)).concat(": "));
		String s1 = a.readLine();
		return s1;
	}

	private String a(String s, String s1) throws IOException {
		return String.valueOf(String.valueOf((new StringBuffer("&")).append(s).append("=").append(URLEncoder.encode(s1, "UTF-8"))));
	}

	public BufferedReader a;
}

/*
	DECOMPILATION REPORT

	Decompiled from: E:\Java Projects\ThirdPartyJavaLibraries\lib\ibizptech-2008-07-18.jar
	Total time: 31 ms
	Jad reported messages/errors:
#classes: 1, #methods: 6, #fields: 2, elapsed time: 0.016s
	Exit status: 0
	Caught exceptions:
*/
