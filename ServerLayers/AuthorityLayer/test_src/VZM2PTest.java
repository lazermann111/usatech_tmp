import java.math.BigDecimal;

import org.apache.axis2.AxisFault;
import org.junit.Test;

import simple.io.Log;
import simple.text.StringUtils;

import com.usatech.vzm2p.M2P_Value_TransactionStub;
import com.usatech.vzm2p.M2P_Value_TransactionStub.M2P_Value_Transaction_Request;
import com.usatech.vzm2p.M2P_Value_TransactionStub.M2P_Value_Transaction_RequestE;
import com.usatech.vzm2p.M2P_Value_TransactionStub.M2P_Value_Transaction_Response;
import com.usatech.vzm2p.M2P_Value_TransactionStub.M2P_Value_Transaction_ResponseE;

public class VZM2PTest {
	static {
		System.setProperty("app.servicename", "VZM2PTest");
		System.setProperty("log4j.configuration", "log4j-dev.properties");
		System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
		System.setProperty("org.apache.commons.logging.simplelog.showdatetime", "true");
		System.setProperty("org.apache.commons.logging.simplelog.log.httpclient.wire", "debug");
		System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.commons.httpclient", "debug");
	}	
	
	private static final Log log = Log.getLog();
	
	public static final String VZM2P_VALUE_TRAN_URL = "http://204.51.112.180:8080/active-bpel/services/soap12/m2p_Value_Transaction";
	public static final String CONSUMER_ACCT_TOKEN = "08BED7C5C58654C052EC88E04B18AFA81E2DC511023719FD61B3B5126B68EC78";
	public static final BigDecimal AUTH_AMOUNT = new BigDecimal("1");
	public static final BigDecimal CAPTURE_AMOUNT = new BigDecimal("0.50");
	public static final String CURRENCY_CD = "USD";
	public static final String TRAN_REFERENCE_ID = "4596696701920786975";
	public static final String APPROVAL_CD = "H75BGG766UC8YU3YJENLWCX8KTCTCT0B";
	
	protected static M2P_Value_TransactionStub m2pTran = null;
	
	protected void init() throws AxisFault {
		if (m2pTran == null)
			m2pTran = new M2P_Value_TransactionStub(VZM2P_VALUE_TRAN_URL);
	}

	@Test
	public void testAuth() throws Exception {		
		init();
		M2P_Value_Transaction_RequestE requestMessage = new M2P_Value_Transaction_RequestE();
		M2P_Value_Transaction_Request request = new M2P_Value_Transaction_Request();
		request.setValue_Transaction_Type("Auth");
		request.setValue_Transaction_Amount(AUTH_AMOUNT);
		request.setValue_Transaction_Consumer_Identifier(CONSUMER_ACCT_TOKEN);
		request.setValue_Transaction_Currency(CURRENCY_CD);
		request.setValue_Transaction_Reference_Id(TRAN_REFERENCE_ID);
		requestMessage.setM2P_Value_Transaction_Request(request);
		log.info(StringUtils.objectToString(request, "Sending Auth request to M2P server: "));
		M2P_Value_Transaction_ResponseE responseMessage = m2pTran.m2P_Value_Transaction_Request(requestMessage);
		M2P_Value_Transaction_Response response = responseMessage.getM2P_Value_Transaction_Response();
		log.info(StringUtils.objectToString(response, "Received Auth response from M2P server: "));
	}
	
	@Test
	public void testCapture() throws Exception {		
		init();
		M2P_Value_Transaction_RequestE requestMessage = new M2P_Value_Transaction_RequestE();
		M2P_Value_Transaction_Request request = new M2P_Value_Transaction_Request();
		request.setValue_Transaction_Type("Capture");
		request.setValue_Transaction_Amount(CAPTURE_AMOUNT);
		request.setValue_Transaction_Consumer_Identifier(CONSUMER_ACCT_TOKEN);
		request.setValue_Transaction_Currency(CURRENCY_CD);
		request.setValue_Transaction_Reference_Id(TRAN_REFERENCE_ID);
		request.setValue_Transaction_Details(APPROVAL_CD);
		requestMessage.setM2P_Value_Transaction_Request(request);
		log.info(StringUtils.objectToString(request, "Sending Capture request to M2P server: "));
		M2P_Value_Transaction_ResponseE responseMessage = m2pTran.m2P_Value_Transaction_Request(requestMessage);
		M2P_Value_Transaction_Response response = responseMessage.getM2P_Value_Transaction_Response();
		log.info(StringUtils.objectToString(response, "Received Capture response from M2P server: "));
	}
}
