import org.jpos.iso.ISOException;

import simple.bean.ConvertUtils;

import com.usatech.authoritylayer.ElavonClearingFileTask.CardType;
import com.usatech.iso8583.interchange.tandem.Bitmap192x8Field;


public class Test {

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		CardType[] cts = ConvertUtils.convert(CardType[].class, "VI,MC");
		System.out.println(cts);
	}

	@org.junit.Test
	public void testBitmapField() throws ISOException {
		Bitmap192x8Field posVarCap2 = new Bitmap192x8Field();

		posVarCap2.setBit(2, 4, true);
		posVarCap2.setBit(3, 8, true);
		posVarCap2.setBit(7, 6, true);
		posVarCap2.setBit(11, 3, true);
		String s = posVarCap2.getValue();
		System.out.println("Value = " + s);

		Bitmap192x8Field copy = new Bitmap192x8Field();
		copy.setValue(s);
		System.out.println("Copy Value = " + copy.getValue());

	}
}
