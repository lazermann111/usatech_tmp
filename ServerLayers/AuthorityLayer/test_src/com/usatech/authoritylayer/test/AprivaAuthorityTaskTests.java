package com.usatech.authoritylayer.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.Socket;
import java.net.SocketException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import javax.net.SocketFactory;

import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.usatech.app.Attribute;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.authoritylayer.AprivaAuthorityTask;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAction;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.DebugLevel;
import com.usatech.layers.common.constants.DeniedReason;
import com.usatech.layers.common.constants.EntryType;

import simple.app.QoS;
import simple.app.RetrySpecifiedServiceException;
import simple.text.MessageFormat;
import simple.xml.MapXMLLoader;
import simple.xml.MapXMLLoader.ParentMap;

public class AprivaAuthorityTaskTests {
	
	static {
		System.setProperty("app.servicename", "AprivaTest");
		System.setProperty("log4j.configuration", "log4j-dev.properties");
	}
	
	@Test
	public void testAuthApproved() throws Exception {
		AprivaTestHolder test = new AprivaTestHolder(AuthorityAction.AUTHORIZATION);
		
		int resultCode = test.aprivaTask.process(test.taskInfo);
		assertEquals(0, resultCode);
		
		verify(test.socket, times(1)).connect(any(), anyInt());
		verify(test.socket, times(1)).getInputStream();
		verify(test.socket, times(1)).getOutputStream();
		
		assertEquals(AuthResultCode.APPROVED, test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
	}
	
	@Test
	public void testSaleApproved() throws Exception {
		AprivaTestHolder test = new AprivaTestHolder(AuthorityAction.SALE);
		
		int resultCode = test.aprivaTask.process(test.taskInfo);
		assertEquals(0, resultCode);
		
		verify(test.socket, times(1)).connect(any(), anyInt());
		verify(test.socket, times(1)).getInputStream();
		verify(test.socket, times(1)).getOutputStream();
		
		assertEquals(AuthResultCode.APPROVED, test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
	}
	
	@Test
	public void testAuthStale() throws Exception {
		AprivaTestHolder test = new AprivaTestHolder(AuthorityAction.AUTHORIZATION);
		
		long authTime = System.currentTimeMillis() - TimeUnit.SECONDS.toMillis(19);
		test.requestAttributes.put("authTime", authTime);
		
		int resultCode = test.aprivaTask.process(test.taskInfo);
		assertEquals(2, resultCode);
		
		verify(test.socket, never()).connect(any(), anyInt());
		
		assertEquals(AuthResultCode.FAILED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertEquals(DeniedReason.INTERNAL_TIMEOUT.toString(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue()));
	}
	
	@Test
	public void testSaleStale() throws Exception {
		AprivaTestHolder test = new AprivaTestHolder(AuthorityAction.SALE);
		
		long authTime = System.currentTimeMillis() - TimeUnit.SECONDS.toMillis(56);
		test.requestAttributes.put("authTime", authTime);
		
		int resultCode = test.aprivaTask.process(test.taskInfo);
		assertEquals(0, resultCode);
		
		verify(test.socket, times(1)).connect(any(), anyInt());
		verify(test.socket, times(1)).getInputStream();
		verify(test.socket, times(1)).getOutputStream();
		
		assertEquals(AuthResultCode.APPROVED, test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
	}
	
	@Test
	public void testAuthConnectFailure() throws Exception {
		AprivaTestHolder test = new AprivaTestHolder(AuthorityAction.AUTHORIZATION);
		
		doThrow(new SocketException("TEST")).when(test.socket).connect(any(), anyInt());

		int resultCode = test.aprivaTask.process(test.taskInfo);
		assertEquals(2, resultCode);
		
		verify(test.socket, times(3)).connect(any(), anyInt());
		verify(test.socket, never()).getInputStream();
		verify(test.socket, never()).getOutputStream();
		
		assertEquals(AuthResultCode.FAILED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertEquals(DeniedReason.PROCESSOR_ERROR.toString(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue()));
	}
	
	public void testSaleConnectFailure() throws Exception {
		AprivaTestHolder test = new AprivaTestHolder(AuthorityAction.SALE);
		
		doThrow(new SocketException("TEST")).when(test.socket).connect(any(), anyInt());

		int resultCode = test.aprivaTask.process(test.taskInfo);
		assertEquals(1, resultCode);
		
		verify(test.socket, times(3)).connect(any(), anyInt());
		verify(test.socket, never()).getInputStream();
		verify(test.socket, never()).getOutputStream();
		
		assertEquals(AuthResultCode.DECLINED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertEquals(DeniedReason.PROCESSOR_ERROR.toString(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue()));
	}
	
	@Test(expected=RetrySpecifiedServiceException.class)
	public void testAuthReadFailure() throws Exception {
		AprivaTestHolder test = new AprivaTestHolder(AuthorityAction.AUTHORIZATION);
		
		InputStream mockInputStream = mock(InputStream.class);
		when(mockInputStream.read()).thenThrow(new IOException("TEST"));
		test.aprivaServer.in = mockInputStream;

		try {
			test.aprivaTask.process(test.taskInfo);
		} finally {
			verify(test.socket, times(1)).close();
		}
	}
	
	@Test(expected=RetrySpecifiedServiceException.class)
	public void testSaleReadFailureNoDebug() throws Exception {
		AprivaTestHolder test = new AprivaTestHolder(AuthorityAction.SALE);
		test.aprivaTask.setDebugLevel(DebugLevel.OFF);
		
		InputStream mockInputStream = mock(InputStream.class);
		when(mockInputStream.read()).thenThrow(new IOException("TEST"));
		test.aprivaServer.in = mockInputStream;

		try {
			test.aprivaTask.process(test.taskInfo);
		} finally {
			verify(test.socket, times(1)).close();
		}
	}

	@Test(expected=RetrySpecifiedServiceException.class)
	public void testAuthReadFailureNoDebug() throws Exception {
		AprivaTestHolder test = new AprivaTestHolder(AuthorityAction.AUTHORIZATION);
		test.aprivaTask.setDebugLevel(DebugLevel.OFF);
		
		InputStream mockInputStream = mock(InputStream.class);
		when(mockInputStream.read()).thenThrow(new IOException("TEST"));
		test.aprivaServer.in = mockInputStream;

		try {
			test.aprivaTask.process(test.taskInfo);
		} finally {
			verify(test.socket, times(1)).close();
		}
	}

	@Test(expected=RetrySpecifiedServiceException.class)
	public void testSaleWriteFailure() throws Exception {
		AprivaTestHolder test = new AprivaTestHolder(AuthorityAction.SALE);
		
		OutputStream mockOutputStream = mock(OutputStream.class);
		doThrow(new IOException("TEST")).when(mockOutputStream).write(anyInt());
		test.aprivaServer.out = mockOutputStream;

		try {
			test.aprivaTask.process(test.taskInfo);
		} finally {
			verify(test.socket, times(1)).close();
		}
	}

	@Test(expected=RetrySpecifiedServiceException.class)
	public void testSaleWriteFailureNoDebug() throws Exception {
		AprivaTestHolder test = new AprivaTestHolder(AuthorityAction.SALE);
		test.aprivaTask.setDebugLevel(DebugLevel.OFF);
		
		OutputStream mockOutputStream = mock(OutputStream.class);
		doThrow(new IOException("TEST")).when(mockOutputStream).write(anyInt());
		test.aprivaServer.out = mockOutputStream;

		try {
			test.aprivaTask.process(test.taskInfo);
		} finally {
			verify(test.socket, times(1)).close();
		}
	}
	
	@Test(expected=RetrySpecifiedServiceException.class)
	public void testAuthWriteFailure() throws Exception {
		AprivaTestHolder test = new AprivaTestHolder(AuthorityAction.AUTHORIZATION);
		
		OutputStream mockOutputStream = mock(OutputStream.class);
		doThrow(new IOException("TEST")).when(mockOutputStream).write(anyInt());
		test.aprivaServer.out = mockOutputStream;

		try {
			test.aprivaTask.process(test.taskInfo);
		} finally {
			verify(test.socket, times(1)).close();
		}
	}
	
	@Test(expected=RetrySpecifiedServiceException.class)
	public void testReversalWriteFailure() throws Exception {
		AprivaTestHolder test = new AprivaTestHolder(AuthorityAction.SALE);
		test.aprivaTask.setMaxRetryAttempts(5);
		when(test.taskInfo.getRetryCount()).thenReturn(1);
		when(test.taskInfo.isRedelivered()).thenReturn(true);
		
		OutputStream mockOutputStream = mock(OutputStream.class);
		doThrow(new IOException("TEST")).when(mockOutputStream).write(anyInt());
		test.aprivaServer.out = mockOutputStream;

		try {
			test.aprivaTask.process(test.taskInfo);
		} finally {
			verify(test.socket, times(1)).close();
		}
	}
	
	@Test(expected=RetrySpecifiedServiceException.class)
	public void testReversalConnectFailure() throws Exception {
		AprivaTestHolder test = new AprivaTestHolder(AuthorityAction.SALE);
		test.aprivaTask.setMaxRetryAttempts(5);
		when(test.taskInfo.getRetryCount()).thenReturn(1);
		when(test.taskInfo.isRedelivered()).thenReturn(true);
		
		doThrow(new SocketException("TEST")).when(test.socket).connect(any(), anyInt());

		test.aprivaTask.process(test.taskInfo);
	}
	
	@Test
	public void testSaleRedelivered() throws Exception {
		AprivaTestHolder test = new AprivaTestHolder(AuthorityAction.SALE);
		test.aprivaTask.setMaxRetryAttempts(5);
		when(test.taskInfo.getRetryCount()).thenReturn(1);
		when(test.taskInfo.isRedelivered()).thenReturn(true);
		
		int resultCode = test.aprivaTask.process(test.taskInfo);
		assertEquals(1, resultCode);
		
		verify(test.socket, times(1)).connect(any(), anyInt());
		verify(test.socket, times(1)).getInputStream();
		verify(test.socket, times(1)).getOutputStream();
		
		assertEquals(AuthResultCode.DECLINED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
	}
	
	@Test(expected=RetrySpecifiedServiceException.class)
	public void testSaleRedeliveredConnectFailure() throws Exception {
		AprivaTestHolder test = new AprivaTestHolder(AuthorityAction.SALE);
		test.aprivaTask.setMaxRetryAttempts(5);
		when(test.taskInfo.getRetryCount()).thenReturn(1);
		when(test.taskInfo.isRedelivered()).thenReturn(true);
		
		doThrow(new SocketException("TEST")).when(test.socket).connect(any(), anyInt());
		
		test.aprivaTask.process(test.taskInfo);
	}

	@Test(expected=RetrySpecifiedServiceException.class)
	public void testSaleRedeliveredReadFailure() throws Exception {
		AprivaTestHolder test = new AprivaTestHolder(AuthorityAction.SALE);
		test.aprivaTask.setMaxRetryAttempts(5);
		when(test.taskInfo.getRetryCount()).thenReturn(1);
		when(test.taskInfo.isRedelivered()).thenReturn(true);
		
		doThrow(new SocketException("TEST")).when(test.socket).connect(any(), anyInt());
		
		test.aprivaTask.process(test.taskInfo);
	}

	@Test
	public void testAuthRedelivered() throws Exception {
		AprivaTestHolder test = new AprivaTestHolder(AuthorityAction.AUTHORIZATION);
		test.aprivaTask.setMaxRetryAttempts(5);
		when(test.taskInfo.getRetryCount()).thenReturn(1);
		when(test.taskInfo.isRedelivered()).thenReturn(true);
		
		int resultCode = test.aprivaTask.process(test.taskInfo);
		assertEquals(0, resultCode);
		
		verify(test.socket, times(1)).connect(any(), anyInt());
		verify(test.socket, times(1)).getInputStream();
		verify(test.socket, times(1)).getOutputStream();
		
		assertEquals(AuthResultCode.APPROVED, test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
	}

	@Test
	public void testDeclinedOnFailedReversal() throws Exception {
		AprivaTestHolder test = new AprivaTestHolder(AuthorityAction.SALE);
		test.aprivaTask.setMaxRetryAttempts(5);
		test.aprivaTask.setReturnFailedOnFailedReversal(false);
		when(test.taskInfo.getRetryCount()).thenReturn(5);
		when(test.taskInfo.isRedelivered()).thenReturn(true);
		
		doThrow(new SocketException("TEST")).when(test.socket).connect(any(), anyInt());

		int resultCode = test.aprivaTask.process(test.taskInfo);
		assertEquals(1, resultCode);
		
		verify(test.socket, times(3)).connect(any(), anyInt());
		verify(test.socket, never()).getInputStream();
		verify(test.socket, never()).getOutputStream();
		
		assertEquals(AuthResultCode.DECLINED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
	}

	@Test
	public void testFailedOnFailedReversal() throws Exception {
		AprivaTestHolder test = new AprivaTestHolder(AuthorityAction.SALE);
		test.aprivaTask.setMaxRetryAttempts(5);
		test.aprivaTask.setReturnFailedOnFailedReversal(true);
		when(test.taskInfo.getRetryCount()).thenReturn(5);
		when(test.taskInfo.isRedelivered()).thenReturn(true);
		
		doThrow(new SocketException("TEST")).when(test.socket).connect(any(), anyInt());

		int resultCode = test.aprivaTask.process(test.taskInfo);
		assertEquals(2, resultCode);
		
		verify(test.socket, times(3)).connect(any(), anyInt());
		verify(test.socket, never()).getInputStream();
		verify(test.socket, never()).getOutputStream();
		
		assertEquals(AuthResultCode.FAILED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
	}
	
	@Test(expected=RetrySpecifiedServiceException.class)
	public void testReadZeroLengthSale() throws Exception {
		AprivaTestHolder test = new AprivaTestHolder(AuthorityAction.SALE);
		test.aprivaTask.setDebugLevel(DebugLevel.OFF);
		
		InputStream mockInputStream = mock(InputStream.class);
		when(mockInputStream.read()).thenReturn(-1);
		test.aprivaServer.in = mockInputStream;
		
		try {
			test.aprivaTask.process(test.taskInfo);
		} finally {
			verify(test.socket, times(1)).close();
		}
	}
	
	@Test(expected=RetrySpecifiedServiceException.class)
	public void testReadZeroLengthAuth() throws Exception {
		AprivaTestHolder test = new AprivaTestHolder(AuthorityAction.AUTHORIZATION);
		test.aprivaTask.setDebugLevel(DebugLevel.OFF);
		
		InputStream mockInputStream = mock(InputStream.class);
		when(mockInputStream.read()).thenReturn(-1);
		test.aprivaServer.in = mockInputStream;
		
		try {
			test.aprivaTask.process(test.taskInfo);
		} finally {
			verify(test.socket, times(1)).close();
		}
	}

	@Test
	public void testReadZeroLengthReturnFailedOnFailedReversal() throws Exception {
		AprivaTestHolder test = new AprivaTestHolder(AuthorityAction.AUTHORIZATION);
		test.aprivaTask.setDebugLevel(DebugLevel.OFF);
		test.aprivaTask.setMaxRetryAttempts(5);
		test.aprivaTask.setReturnFailedOnFailedReversal(true);
		when(test.taskInfo.getRetryCount()).thenReturn(5);
		when(test.taskInfo.isRedelivered()).thenReturn(true);
		
		InputStream mockInputStream = mock(InputStream.class);
		when(mockInputStream.read()).thenReturn(-1);
		test.aprivaServer.in = mockInputStream;
		
		int resultCode = test.aprivaTask.process(test.taskInfo);
		assertEquals(2, resultCode);
		
		verify(test.socket, times(1)).connect(any(), anyInt());
		verify(test.socket, times(1)).getInputStream();
		verify(test.socket, times(1)).getOutputStream();
		
		assertEquals(AuthResultCode.FAILED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
	}

	// This is just a class to setup and hold everything that will be used in more than one test case
	protected static class AprivaTestHolder {
		AprivaAuthorityTask aprivaTask;
		MockAprivaServer aprivaServer;
		SocketFactory socketFactory;
		Socket socket;
		MessageChainTaskInfo taskInfo;
		MessageChainStep step;
		QoS qos;
		long startTime;
		long traceNumber;
		int posPtaId;
		Map<String, Object> requestAttributes;
		Map<String, Object> resultAttributes;
		
		
		public AprivaTestHolder(AuthorityAction action) throws Exception {
			aprivaTask = new AprivaAuthorityTask();
			aprivaTask.setDebugLevel(DebugLevel.DEBUG);
			aprivaTask.setHost("127.0.0.1");
			aprivaTask.setPort(11098);
			aprivaTask.setConnectRetries(2);
			aprivaTask.setMaxRetryAttempts(5);
			aprivaTask.setMaxAuthStaleness(15 * 1000);

			socketFactory = mock(SocketFactory.class);
			aprivaTask.setSocketFactory(socketFactory);
			
			socket = mock(Socket.class);
			when(socketFactory.createSocket()).thenReturn(socket);
			when(socket.isConnected()).thenReturn(false);
			when(socket.isBound()).thenReturn(false);
			
			aprivaServer = new MockAprivaServer();
			when(socket.getInputStream()).thenReturn(aprivaServer.getInputStream());
			when(socket.getOutputStream()).thenReturn(aprivaServer.getOutputStream());
			
			taskInfo = mock(MessageChainTaskInfo.class);
			when(taskInfo.isRedelivered()).thenReturn(false);
			
			step = mock(MessageChainStep.class);
			when(taskInfo.getStep()).thenReturn(step);
			
			Random random = new Random();
			
			startTime = System.currentTimeMillis()-10;
			traceNumber = random.nextLong();
			posPtaId = random.nextInt(1000000) + 100000;
			
			requestAttributes = new HashMap<String, Object>();
			requestAttributes.put("actionType", action);
			requestAttributes.put("online", action == AuthorityAction.AUTHORIZATION ? true : false);
			requestAttributes.put("serviceType", 1);
			requestAttributes.put("csPhone", "888-561-4748");
			requestAttributes.put("city", "Frazer");
			requestAttributes.put("countryCd", "US");
			requestAttributes.put("csEmail", "support@usatech.com");
			requestAttributes.put("mcc", "5814");
			requestAttributes.put("deviceSerialCd", "VJ200017088");
			requestAttributes.put("doingBusinessAs", "Canteen");
			requestAttributes.put("authorityName", "Apriva");
			requestAttributes.put("traceNumber", traceNumber);
			requestAttributes.put("posPtaId", posPtaId);
			requestAttributes.put("currencyCd", "USD");
			requestAttributes.put("authTime", startTime);
			requestAttributes.put("entryType", EntryType.SWIPE);
			requestAttributes.put("amount", BigInteger.valueOf(100));
			requestAttributes.put("minorCurrencyFactor", 100);
			requestAttributes.put("timeZoneGuid", TimeZone.getDefault().getID());
			requestAttributes.put("deviceName", "TD213716");
			requestAttributes.put("terminalCd", "TD255061");
			requestAttributes.put("priorAttempts", 0);
			requestAttributes.put("deviceTranCd", Long.toString(startTime));
			requestAttributes.put("merchantCd", "12345");
			requestAttributes.put("allowPartialAuth", true);
			requestAttributes.put("trackData", "9999901501=200112345");
			
			when(step.getAttributes()).thenReturn(requestAttributes);
			when(step.getAttributeSafely(any(), any(), any())).thenAnswer(new Answer<Object>() {
				public Object answer(InvocationOnMock invocation) {
					Attribute attribute = invocation.getArgumentAt(0, Attribute.class);
					Class<?> convertTo = invocation.getArgumentAt(1, Class.class);
					boolean required = invocation.getArgumentAt(2, Boolean.class);
					return requestAttributes.get(attribute.getValue());
				}
			});
			
			resultAttributes = new HashMap<>();
			when(step.getResultAttributes()).thenReturn(resultAttributes);

			doAnswer(new Answer<Void>() {
				public Void answer(InvocationOnMock invocation) {
					Attribute attribute = invocation.getArgumentAt(0, Attribute.class);
					Object value = invocation.getArgumentAt(1, Object.class);
					resultAttributes.put(attribute.getValue(), value);
					return null;
				}
			}).when(step).setResultAttribute((Attribute) any(), any());
			
			qos = mock(QoS.class);
			when(taskInfo.getCurrentQos()).thenReturn(qos);
			when(qos.isPersistent()).thenReturn(true);
			when(qos.getTimeToLive()).thenReturn(20000L);
		}
	}

	protected static class MockAprivaServer extends InputStream {
		OutputStream out = new ByteArrayOutputStream();
		InputStream in;
		
		String responseCode = "0";
		String responseMessage = "OK";
		String balance = "100";
		
		// Warning: these responses may not match what Apriva actually sends, I did not have any recent actual sample responses to work from.
		// They only satisfy the expectations of our response parser.
		private String balanceInquiryTemplate = "<AprivaPosXml><StoredValue MessageType=\"Response\" Version=\"5.0\" ProcessingCode=\"InquireCurrentBalance\"><Stan>{0}</Stan><ResponseCode>{1}</ResponseCode><ResponseText>{2}</ResponseText><StoredValueResponse><NewBalance>{3}</NewBalance></StoredValueResponse></StoredValue></AprivaPosXml>"; 
		private String redemptionTemplate = "<AprivaPosXml><StoredValue MessageType=\"Response\" Version=\"5.0\" ProcessingCode=\"Redemption\"><Stan>{0}</Stan><ResponseCode>{1}</ResponseCode><ResponseText>{2}</ResponseText><StoredValueResponse><NewBalance>{3}</NewBalance></StoredValueResponse></StoredValue></AprivaPosXml>";
		private String reversalTemplate = "<AprivaPosXml><Credit MessageType=\"Response\" Version=\"5.0\" ProcessingCode=\"SaleReversal\"><Stan>{0}</Stan><ResponseCode>{1}</ResponseCode><ResponseText>{2}</ResponseText></Credit></AprivaPosXml>";
		
		public OutputStream getOutputStream() {
			//return tempOut;
			return out;
		}
		
		public InputStream getInputStream() {
			return this;
		}

		@Override
		public int read() throws IOException {
			if (in == null) {
				processRequest();
			}
			return in.read();
		}
		
		private void processRequest() throws IOException {
			try {
				MapXMLLoader xmlLoader = new MapXMLLoader();
				ParentMap request = xmlLoader.load(new ByteArrayInputStream(((ByteArrayOutputStream)out).toByteArray()));
				ParentMap requests[] = request.getChildren();
				String processingCode = requests[0].get("ProcessingCode");
				int stan = AprivaAuthorityTask.getResponseValue(requests[0], Integer.class, "Stan");
				String response = null;
				if (processingCode.equals("InquireCurrentBalance"))
					response = MessageFormat.format(balanceInquiryTemplate, stan, responseCode, responseMessage, balance);
				else if (processingCode.equals("Redemption"))
					response = MessageFormat.format(redemptionTemplate, stan, responseCode, responseMessage, balance);
				else if (processingCode.equals("SaleReversal"))
					response = MessageFormat.format(reversalTemplate, stan, responseCode, responseMessage);
				else
					throw new IllegalArgumentException("Unexpected ProcessingCode: " + processingCode);
				
				in = new ByteArrayInputStream(response.getBytes());
			} catch (Exception e) {
				throw new IOException(e);
			}
		}
	}
}

