package com.usatech.authoritylayer.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.InputStreamReader;
import java.io.StringReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.google.gson.Gson;
import com.usatech.app.Attribute;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.authoritylayer.USConnectApolloAuthorityTask;
import com.usatech.authoritylayer.USConnectApolloAuthorityTask.Item;
import com.usatech.authoritylayer.USConnectApolloAuthorityTask.Purchase;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAction;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.DebugLevel;
import com.usatech.layers.common.constants.DeniedReason;
import com.usatech.layers.common.constants.EntryType;

import simple.app.QoS;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.ReaderInputStream;

public class USConnectApolloAuthorityTaskTests {
	
	static {
		System.setProperty("app.servicename", "USConnectApolloTests");
		System.setProperty("log4j.configuration", "log4j-dev.properties");
	}

	@Test
	public void testSerialization() throws Exception {
		Purchase purchase = new Purchase();
		purchase.setCardNumber("6277229032283510");
		purchase.setDeviceSerialNumber("VJ000000336");
		purchase.setExternalReferenceId("1125366");
		
		Item item = new Item();
		item.setBarcode("02C3");
		item.setDescription("ePort Vend 02C3");
		item.setPrice(new BigDecimal(".11"));
		item.setQuantity(new BigDecimal("1"));
		
		purchase.addItem(item);
		
		String json = new Gson().toJson(purchase);
		Map<String, Object> map = new Gson().fromJson(json, Map.class);
		System.out.println(map);
		
		assertEquals("6277229032283510", map.get("card_number"));
		assertEquals("VJ000000336", map.get("device_serial_number"));
		assertEquals("1125366", map.get("external_reference_id"));
		
		assertEquals(4, map.size());

		List<Map<String, Object>> items = (List<Map<String, Object>>) map.get("items");
		Map<String, Object> itemMap = items.get(0);
		
		assertEquals("02C3", itemMap.get("barcode"));
		assertEquals("ePort Vend 02C3", itemMap.get("description"));
		assertEquals(1D, itemMap.get("quantity"));
		assertEquals(.11D, itemMap.get("price"));
		
		assertEquals(4, itemMap.size()); // number of fields in item
	}

	@Test
	public void testAuthorizationApproved() throws Exception {
		ApolloTestHolder test = new ApolloTestHolder(AuthorityAction.AUTHORIZATION, "12345", "100");
		int resultCode = test.execute();
		
		verify(test.httpClient, times(1)).execute(any());
		assertEquals(0, resultCode);
		assertEquals(AuthResultCode.APPROVED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertTrue(test.server.requests.get(0).get("request_url").toString().endsWith("/account/12345"));
		assertEquals("200", test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, String.class));
		assertEquals(false, test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_HOLD_USED, boolean.class));
		assertNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, Object.class));
		assertTrue(new BigDecimal("168911").compareTo(test.getResultAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, BigDecimal.class)) == 0);
		assertTrue(new BigDecimal("100").compareTo(test.getResultAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, BigDecimal.class))==0);
		assertEquals("client.message.auth.USConnect_Apollo.approved", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, String.class));
		assertEquals("200", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, String[].class)[0]);
	}

	@Test
	public void testSaleApproved() throws Exception {
		ApolloTestHolder test = new ApolloTestHolder(AuthorityAction.SALE, "12345", "200");
		int resultCode = test.execute();
		
		verify(test.httpClient, times(1)).execute(any());
		assertEquals(0, resultCode);
		assertEquals(AuthResultCode.APPROVED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertTrue(test.server.requests.get(0).get("request_url").toString().endsWith("/purchase"));
		assertEquals("200", test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, String.class));
		assertEquals(true, test.getResultAttribute(AuthorityAttrEnum.ATTR_SETTLED, boolean.class));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, Object.class));
		assertTrue(new BigDecimal("168911").subtract(new BigDecimal("200")).compareTo(test.getResultAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, BigDecimal.class)) == 0);
		assertTrue(new BigDecimal("200").compareTo(test.getResultAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, BigDecimal.class))==0);
	}
	
	@Test
	public void testAuthorityTimestamp() throws Exception {
		ApolloTestHolder test = new ApolloTestHolder(AuthorityAction.SALE, "12345", "100");
		int resultCode = test.execute();
		
		verify(test.httpClient, times(1)).execute(any());
		assertEquals(0, resultCode);
		assertEquals(AuthResultCode.APPROVED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertTrue(test.server.requests.get(0).get("request_url").toString().endsWith("/purchase"));
		assertEquals("200", test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, String.class));
		
		Date authorityTimestamp = test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS, Date.class);
		System.out.println(authorityTimestamp);
		assertTrue("timestamp too old", authorityTimestamp.getTime() > (System.currentTimeMillis() - TimeUnit.SECONDS.toMillis(5)));
				
	}

	@Test
	public void testAuthorizationNoAccount() throws Exception {
		ApolloTestHolder test = new ApolloTestHolder(AuthorityAction.AUTHORIZATION, "12345", "100");
		test.errorCode = 14;
		test.errorMessage = "Declined: Invalid card or account number";
		
		int resultCode = test.execute();
		
		verify(test.httpClient, times(1)).execute(any());
		assertEquals(1, resultCode);
		assertEquals(AuthResultCode.DECLINED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertTrue(test.server.requests.get(0).get("request_url").toString().endsWith("/account/12345"));
		assertEquals("NO_ACCOUNT", test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, String.class));
		assertEquals("Declined: Invalid card or account number", test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, String.class));
		assertNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_HOLD_USED, boolean.class));
		assertNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, Object.class));
		assertNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, BigDecimal.class));
		assertNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, BigDecimal.class));
		assertEquals("client.message.auth.USConnect_Apollo.denied-NO_ACCOUNT", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, String.class));
		assertEquals("Declined: Invalid card or account number", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, String[].class)[0]);
	}

	@Test
	public void testAuthorizationZeroBalance() throws Exception {
		ApolloTestHolder test = new ApolloTestHolder(AuthorityAction.AUTHORIZATION, "12345", "100");
		test.balance = 0;
		
		int resultCode = test.execute();
		
		verify(test.httpClient, times(1)).execute(any());
		assertEquals(1, resultCode);
		assertEquals(AuthResultCode.DECLINED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertTrue(test.server.requests.get(0).get("request_url").toString().endsWith("/account/12345"));
		assertEquals("INSUFFICIENT_FUNDS", test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, String.class));
		assertEquals("Insufficient funds: 0.00", test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, String.class));
		assertFalse(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_HOLD_USED, boolean.class));
		assertNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, Object.class));
		assertEquals(BigDecimal.ZERO, test.getResultAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, BigDecimal.class));
		assertEquals(BigDecimal.ZERO, test.getResultAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, BigDecimal.class));
		assertEquals("client.message.auth.USConnect_Apollo.denied-INSUFFICIENT_FUNDS", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, String.class));
		assertEquals("Insufficient funds: 0.00", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, String[].class)[0]);
	}

	@Test
	public void testAuthorizationLowBalanceWithMinBalance() throws Exception {
		ApolloTestHolder test = new ApolloTestHolder(AuthorityAction.AUTHORIZATION, "12345", "100");
		test.balance = .5D;
		
		int resultCode = test.execute();
		
		verify(test.httpClient, times(1)).execute(any());
		assertEquals(1, resultCode);
		assertEquals(AuthResultCode.DECLINED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertTrue(test.server.requests.get(0).get("request_url").toString().endsWith("/account/12345"));
		assertEquals("INSUFFICIENT_FUNDS", test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, String.class));
		assertEquals("Insufficient funds: 0.50; At least 1.00 is required", test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, String.class));
		assertFalse(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_HOLD_USED, boolean.class));
		assertNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, Object.class));
		assertEquals(new BigDecimal("50"), test.getResultAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, BigDecimal.class));
		assertEquals(BigDecimal.ZERO, test.getResultAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, BigDecimal.class));
		assertEquals("client.message.auth.USConnect_Apollo.denied-INSUFFICIENT_FUNDS", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, String.class));
		assertEquals("Insufficient funds: 0.50; At least 1.00 is required", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, String[].class)[0]);
	}

	@Test
	public void testAuthorizationPartialLowBalanceNoMinBalance() throws Exception {
		ApolloTestHolder test = new ApolloTestHolder(AuthorityAction.AUTHORIZATION, "12345", "100");
		test.task.setMinimumBalanceAmount(BigDecimal.ZERO);
		test.balance = .5D;
		
		int resultCode = test.execute();
		
		verify(test.httpClient, times(1)).execute(any());
		assertEquals(0, resultCode);
		assertEquals(AuthResultCode.PARTIAL.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertTrue(test.server.requests.get(0).get("request_url").toString().endsWith("/account/12345"));
		
		assertEquals("200", test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, String.class));
		assertEquals(false, test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_HOLD_USED, boolean.class));
		assertNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, Object.class));
		assertEquals(new BigDecimal("50"), test.getResultAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, BigDecimal.class));
		assertEquals(new BigDecimal("50"), test.getResultAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, BigDecimal.class));
		assertEquals("client.message.auth.USConnect_Apollo.approved", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, String.class));
		assertEquals("200", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, String[].class)[0]);
	}
	
	@Test
	public void testAuthorizationPartialLowBalanceMinBalance() throws Exception {
		ApolloTestHolder test = new ApolloTestHolder(AuthorityAction.AUTHORIZATION, "12345", "300");
		test.task.setMinimumBalanceAmount(new BigDecimal("2.00"));
		test.balance = 2.50D;
		
		int resultCode = test.execute();
		
		verify(test.httpClient, times(1)).execute(any());
		assertEquals(0, resultCode);
		assertEquals(AuthResultCode.PARTIAL.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertTrue(test.server.requests.get(0).get("request_url").toString().endsWith("/account/12345"));
		
		assertEquals("200", test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, String.class));
		assertEquals(false, test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_HOLD_USED, boolean.class));
		assertNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, Object.class));
		assertEquals(new BigDecimal("250"), test.getResultAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, BigDecimal.class));
		assertEquals(new BigDecimal("250"), test.getResultAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, BigDecimal.class));
		assertEquals("client.message.auth.USConnect_Apollo.approved", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, String.class));
		assertEquals("200", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, String[].class)[0]);
	}

	@Test
	public void testAuthorizationApprovedLowBalanceMinBalance() throws Exception {
		ApolloTestHolder test = new ApolloTestHolder(AuthorityAction.AUTHORIZATION, "12345", "200");
		test.task.setMinimumBalanceAmount(new BigDecimal("2.00"));
		test.balance = 2.50D;
		
		int resultCode = test.execute();
		
		verify(test.httpClient, times(1)).execute(any());
		assertEquals(0, resultCode);
		assertEquals(AuthResultCode.APPROVED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertTrue(test.server.requests.get(0).get("request_url").toString().endsWith("/account/12345"));
		
		assertEquals("200", test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, String.class));
		assertEquals(false, test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_HOLD_USED, boolean.class));
		assertNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, Object.class));
		assertEquals(new BigDecimal("250"), test.getResultAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, BigDecimal.class));
		assertEquals(new BigDecimal("200"), test.getResultAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, BigDecimal.class));
		assertEquals("client.message.auth.USConnect_Apollo.approved", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, String.class));
		assertEquals("200", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, String[].class)[0]);
	}

	@Test
	public void testAuthorizationApprovedMinBalanceEqualsBalance() throws Exception {
		ApolloTestHolder test = new ApolloTestHolder(AuthorityAction.AUTHORIZATION, "12345", "200");
		test.task.setMinimumBalanceAmount(new BigDecimal("2.00"));
		test.balance = 2.00D;
		
		int resultCode = test.execute();
		
		verify(test.httpClient, times(1)).execute(any());
		assertEquals(0, resultCode);
		assertEquals(AuthResultCode.APPROVED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertTrue(test.server.requests.get(0).get("request_url").toString().endsWith("/account/12345"));
		
		assertEquals("200", test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, String.class));
		assertEquals(false, test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_HOLD_USED, boolean.class));
		assertNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, Object.class));
		assertEquals(new BigDecimal("200"), test.getResultAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, BigDecimal.class));
		assertEquals(new BigDecimal("200"), test.getResultAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, BigDecimal.class));
		assertEquals("client.message.auth.USConnect_Apollo.approved", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, String.class));
		assertEquals("200", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, String[].class)[0]);
	}

	@Test
	public void testAuthorizationStale() throws Exception {
		ApolloTestHolder test = new ApolloTestHolder(AuthorityAction.AUTHORIZATION, "12345", "200");
		
		long authTime = System.currentTimeMillis() - TimeUnit.SECONDS.toMillis(19);
		test.requestAttributes.put("authTime", authTime);
		
		int resultCode = test.execute();
		assertEquals(2, resultCode);
		verify(test.httpClient, never()).execute(any());
		assertEquals(AuthResultCode.FAILED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertEquals(DeniedReason.INTERNAL_TIMEOUT.toString(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue()));
		assertEquals("client.message.auth.USConnect_Apollo.denied-INTERNAL_TIMEOUT", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, String.class));
		assertEquals("Auth Timeout", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, String[].class)[0]);
	}

	@Test
	public void testSaleStale() throws Exception {
		ApolloTestHolder test = new ApolloTestHolder(AuthorityAction.SALE, "12345", "200");
		
		long authTime = System.currentTimeMillis() - TimeUnit.SECONDS.toMillis(56);
		test.requestAttributes.put("authTime", authTime);

		int resultCode = test.execute();

		verify(test.httpClient, times(1)).execute(any());
		assertEquals(0, resultCode);
		assertEquals(AuthResultCode.APPROVED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertTrue(test.server.requests.get(0).get("request_url").toString().endsWith("/purchase"));
		assertEquals("200", test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, String.class));
		assertEquals(true, test.getResultAttribute(AuthorityAttrEnum.ATTR_SETTLED, boolean.class));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, Object.class));
		assertTrue(new BigDecimal("168911").subtract(new BigDecimal("200")).compareTo(test.getResultAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, BigDecimal.class)) == 0);
		assertTrue(new BigDecimal("200").compareTo(test.getResultAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, BigDecimal.class))==0);
	}
	
	@Test
	public void testSaleDeclinedInsufficientFunds() throws Exception {
		ApolloTestHolder test = new ApolloTestHolder(AuthorityAction.SALE, "12345", "200");
		test.errorCode = 51;
		test.errorMessage = "DECLINED: Insufficient Funds";
		
		int resultCode = test.execute();

		verify(test.httpClient, times(1)).execute(any());
		assertEquals(1, resultCode);
		assertEquals(AuthResultCode.DECLINED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertTrue(test.server.requests.get(0).get("request_url").toString().endsWith("/purchase"));
		assertEquals("INSUFFICIENT_FUNDS", test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, String.class));
		assertEquals("DECLINED: Insufficient Funds", test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, String.class));
		//assertEquals(false, test.getResultAttribute(AuthorityAttrEnum.ATTR_SETTLED, boolean.class));
		assertNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, Object.class));
		assertNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, BigDecimal.class));
		assertNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, BigDecimal.class));
	}

	@Test
	public void testSaleDeclinedNoAccount() throws Exception {
		ApolloTestHolder test = new ApolloTestHolder(AuthorityAction.SALE, "12345", "200");
		test.errorCode = 14;
		test.errorMessage = "Declined: Invalid card or account number";
		
		int resultCode = test.execute();

		verify(test.httpClient, times(1)).execute(any());
		assertEquals(1, resultCode);
		assertEquals(AuthResultCode.DECLINED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertTrue(test.server.requests.get(0).get("request_url").toString().endsWith("/purchase"));
		assertEquals("NO_ACCOUNT", test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, String.class));
		assertEquals("Declined: Invalid card or account number", test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, String.class));
		//assertEquals(false, test.getResultAttribute(AuthorityAttrEnum.ATTR_SETTLED, boolean.class));
		assertNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, Object.class));
		assertNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, BigDecimal.class));
		assertNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, BigDecimal.class));
	}

	@Test
	public void testRefund() throws Exception {
		ApolloTestHolder test = new ApolloTestHolder(AuthorityAction.REFUND, "12345", "100");
		int resultCode = test.execute();
		assertEquals(2, resultCode);
		verify(test.httpClient, never()).execute(any());
		assertEquals(AuthResultCode.FAILED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertEquals(DeniedReason.AUTH_LAYER_PROCESSING_ERROR.toString(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue()));
	}

	@Test
	public void testAuthorizationConnectFailure() throws Exception {
		ApolloTestHolder test = new ApolloTestHolder(AuthorityAction.AUTHORIZATION, "12345", "200");
		doThrow(new HttpHostConnectException(new SocketException("TEST"), HttpHost.create(test.task.getUrl()))).when(test.httpClient).execute(any());

		int resultCode = test.execute();
		
		assertEquals(2, resultCode);
		verify(test.httpClient, times(1)).execute(any());
		assertEquals(AuthResultCode.FAILED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertEquals(DeniedReason.PROCESSOR_ERROR.toString(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue()));
		
		assertEquals("client.message.auth.USConnect_Apollo.denied-PROCESSOR_ERROR", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, String.class));
		assertEquals("Connect to dev.apollo.usconnect.blueworldinc.com failed: TEST", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, String[].class)[0]);
	}

	@Test
	public void testSaleConnectFailure() throws Exception {
		ApolloTestHolder test = new ApolloTestHolder(AuthorityAction.SALE, "12345", "200");
		doThrow(new HttpHostConnectException(new SocketException("TEST"), HttpHost.create(test.task.getUrl()))).when(test.httpClient).execute(any());

		int resultCode = test.execute();
		
		assertEquals(1, resultCode); // declined on a failed sale connection error
		verify(test.httpClient, times(1)).execute(any());
		assertEquals(AuthResultCode.DECLINED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertEquals(DeniedReason.PROCESSOR_ERROR.toString(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue()));
	}
	
	@Test
	public void testAuthorizationReadFailure() throws Exception {
		ApolloTestHolder test = new ApolloTestHolder(AuthorityAction.AUTHORIZATION, "12345", "200");
		doThrow(new SocketTimeoutException("Read timed out")).when(test.httpClient).execute(any());

		int resultCode = test.execute();
		
		assertEquals(2, resultCode);
		verify(test.httpClient, times(1)).execute(any());
		assertEquals(AuthResultCode.FAILED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertEquals(DeniedReason.PROCESSOR_TIMEOUT.toString(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue()));
		
		assertEquals("client.message.auth.USConnect_Apollo.denied-PROCESSOR_TIMEOUT", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, String.class));
		assertEquals("Read timed out", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, String[].class)[0]);
	}

	@Test
	public void testSaleReadFailure() throws Exception {
		ApolloTestHolder test = new ApolloTestHolder(AuthorityAction.SALE, "12345", "200");
		doThrow(new SocketTimeoutException("Read timed out")).when(test.httpClient).execute(any());

		int resultCode = test.execute();
		
		assertEquals(1, resultCode); // declined on a failed sale connection error
		verify(test.httpClient, times(1)).execute(any());
		assertEquals(AuthResultCode.DECLINED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertEquals(DeniedReason.PROCESSOR_TIMEOUT.toString(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue()));
	}
	
	@Test
	public void testAuthorizationRetry() throws Exception {
		ApolloTestHolder test = new ApolloTestHolder(AuthorityAction.AUTHORIZATION, "12345", "100");
		when(test.taskInfo.getRetryCount()).thenReturn(1);
		when(test.taskInfo.isRedelivered()).thenReturn(true);
		
		int resultCode = test.execute();

		verify(test.httpClient, times(1)).execute(any());
		assertEquals(0, resultCode);
		assertEquals(AuthResultCode.APPROVED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertTrue(test.server.requests.get(0).get("request_url").toString().endsWith("/account/12345"));
		assertEquals("200", test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, String.class));
		assertEquals(false, test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_HOLD_USED, boolean.class));
		assertNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, Object.class));
		assertTrue(new BigDecimal("168911").compareTo(test.getResultAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, BigDecimal.class)) == 0);
		assertTrue(new BigDecimal("100").compareTo(test.getResultAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, BigDecimal.class))==0);
		assertEquals("client.message.auth.USConnect_Apollo.approved", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, String.class));
		assertEquals("200", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, String[].class)[0]);
	}

	@Test
	public void testSaleRetryNoDuplicate() throws Exception {
		ApolloTestHolder test = new ApolloTestHolder(AuthorityAction.SALE, "12345", "100");
		when(test.taskInfo.getRetryCount()).thenReturn(1);
		when(test.taskInfo.isRedelivered()).thenReturn(true);
		
		int resultCode = test.execute();

		verify(test.httpClient, times(1)).execute(any());
		assertEquals(0, resultCode);
		assertEquals(AuthResultCode.APPROVED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertTrue(test.server.requests.get(0).get("request_url").toString().endsWith("/purchase"));
		assertEquals("200", test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, String.class));
		assertEquals(true, test.getResultAttribute(AuthorityAttrEnum.ATTR_SETTLED, boolean.class));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, Object.class));
		assertTrue(new BigDecimal("168911").subtract(new BigDecimal("100")).compareTo(test.getResultAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, BigDecimal.class)) == 0);
		assertTrue(new BigDecimal("100").compareTo(test.getResultAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, BigDecimal.class))==0);
	}

	@Test
	public void testSaleRetryDuplicate() throws Exception {
		ApolloTestHolder test = new ApolloTestHolder(AuthorityAction.SALE, "12345", "100");
		when(test.taskInfo.getRetryCount()).thenReturn(1);
		when(test.taskInfo.isRedelivered()).thenReturn(true);
		test.errorCode = 30;
		test.errorMessage = "DECLINED: Transaction entered is a duplicate.";
		
		int resultCode = test.execute();

		verify(test.httpClient, times(1)).execute(any());
		assertEquals(0, resultCode);
		assertEquals(AuthResultCode.APPROVED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertTrue(test.server.requests.get(0).get("request_url").toString().endsWith("/purchase"));
		// 422 is technically an error code from Apollo but we're handling this case as success
		assertEquals("422", test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, String.class));
		assertEquals(true, test.getResultAttribute(AuthorityAttrEnum.ATTR_SETTLED, boolean.class));
		// we don't have this value since it was a dupe
		assertNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, Object.class));
		assertNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, BigDecimal.class));
		assertTrue(new BigDecimal("100").compareTo(test.getResultAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, BigDecimal.class))==0);
		assertTrue(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, String.class).contains("DECLINED: Transaction entered is a duplicate"));
	}
	
	@Test
	public void testSaleRetryInsufficientFunds() throws Exception {
		ApolloTestHolder test = new ApolloTestHolder(AuthorityAction.SALE, "12345", "100");
		when(test.taskInfo.getRetryCount()).thenReturn(1);
		when(test.taskInfo.isRedelivered()).thenReturn(true);
		test.errorCode = 51;
		test.errorMessage = "DECLINED: Insufficient Funds";
		
		int resultCode = test.execute();

		verify(test.httpClient, times(1)).execute(any());
		assertEquals(1, resultCode);
		assertEquals(AuthResultCode.DECLINED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertTrue(test.server.requests.get(0).get("request_url").toString().endsWith("/purchase"));
		assertEquals("INSUFFICIENT_FUNDS", test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, String.class));
		assertEquals("DECLINED: Insufficient Funds", test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, String.class));
		//assertEquals(false, test.getResultAttribute(AuthorityAttrEnum.ATTR_SETTLED, boolean.class));
		assertNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, Object.class));
		assertNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, BigDecimal.class));
		assertNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, BigDecimal.class));
		assertTrue(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, String.class).contains("DECLINED: Insufficient Funds"));
	}
	
	@Test
	public void testSaleDuplicate() throws Exception {
		// duplicate code on a non-retry
		ApolloTestHolder test = new ApolloTestHolder(AuthorityAction.SALE, "12345", "100");
		test.errorCode = 30;
		test.errorMessage = "DECLINED: Transaction entered is a duplicate.";
		
		int resultCode = test.execute();

		verify(test.httpClient, times(1)).execute(any());
		assertEquals(0, resultCode);
		assertEquals(AuthResultCode.APPROVED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertTrue(test.server.requests.get(0).get("request_url").toString().endsWith("/purchase"));
		// 422 is technically an error code from Apollo but we're handling this case as success
		assertEquals("422", test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, String.class));
		assertEquals(true, test.getResultAttribute(AuthorityAttrEnum.ATTR_SETTLED, boolean.class));
		// we don't have this value since it was a dupe
		assertNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, Object.class));
		assertNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, BigDecimal.class));
		assertTrue(new BigDecimal("100").compareTo(test.getResultAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, BigDecimal.class))==0);
		assertTrue(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, String.class).contains("DECLINED: Transaction entered is a duplicate"));
	}

	@Test
	public void testHighBalance() throws Exception {
		// must be careful about scientific notation formatting in BigDecimal
		ApolloTestHolder test = new ApolloTestHolder(AuthorityAction.AUTHORIZATION, "12345", "100");
		test.balance = 5000;
		int resultCode = test.execute();
		
		verify(test.httpClient, times(1)).execute(any());
		assertEquals(0, resultCode);
		assertEquals(AuthResultCode.APPROVED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		
		BigDecimal balance = test.getResultAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, BigDecimal.class);
		assertEquals("500000", balance.toString());
	}

	
	// This is just a class to setup and hold everything that will be used in more than one test case
	protected static class ApolloTestHolder {
		USConnectApolloAuthorityTask task;
		MockApolloServer server;
		MessageChainTaskInfo taskInfo;
		MessageChainStep step;
		QoS qos;
		long startTime;
		long traceNumber;
		int posPtaId;
		Map<String, Object> requestAttributes;
		Map<String, Object> resultAttributes;
		HttpClient httpClient;
		double balance = 1689.11;
		int errorCode = 0;
		String errorMessage = "TEST";
		
		public ApolloTestHolder(AuthorityAction action, String trackData, String amountMinor) throws Exception {
			task = new USConnectApolloAuthorityTask();
			task.setDebugLevel(DebugLevel.DEBUG);
			task.setUrl("http://dev.apollo.usconnect.blueworldinc.com");
			task.setToken("0x5IDOgt3uCfHCqBX5n73aK5Vy3hQVa9");
			task.setMaxAuthStaleness((int)TimeUnit.SECONDS.toMillis(15));
			task.initialize();
			
			httpClient = mock(HttpClient.class);
			task.getHttpRequester().setHttpClient(httpClient);

			server = new MockApolloServer();
			when(httpClient.execute(any())).thenAnswer(new Answer<Object>() {
				public Object answer(InvocationOnMock invocation) {
					HttpRequest request = invocation.getArgumentAt(0, HttpEntityEnclosingRequestBase.class);
					return server.execute(request);
				}
			});
			
			taskInfo = mock(MessageChainTaskInfo.class);
			when(taskInfo.isRedelivered()).thenReturn(false);
			
			step = mock(MessageChainStep.class);
			when(taskInfo.getStep()).thenReturn(step);
			
			Random random = new Random();
			
			startTime = System.currentTimeMillis()-10;
			traceNumber = random.nextLong();
			posPtaId = random.nextInt(1000000) + 100000;
			
			requestAttributes = new HashMap<String, Object>();
			requestAttributes.put("actionType", action);
			requestAttributes.put("online", action == AuthorityAction.AUTHORIZATION ? true : false);
			requestAttributes.put("serviceType", 1);
			requestAttributes.put("csPhone", "888-561-4748");
			requestAttributes.put("city", "Frazer");
			requestAttributes.put("countryCd", "US");
			requestAttributes.put("csEmail", "support@usatech.com");
			requestAttributes.put("mcc", "7994");
			requestAttributes.put("deviceSerialCd", "VJ000000130");
			requestAttributes.put("doingBusinessAs", "Elavon Test Bank Numb");
			requestAttributes.put("authorityName", "USConnect Apollo");
			requestAttributes.put("traceNumber", traceNumber);
			requestAttributes.put("posPtaId", posPtaId);
			requestAttributes.put("currencyCd", "USD");
			requestAttributes.put("authTime", startTime);
			requestAttributes.put("entryType", EntryType.SWIPE);
			requestAttributes.put("amount", new BigInteger(amountMinor));
			requestAttributes.put("minorCurrencyFactor", 100);
			requestAttributes.put("timeZoneGuid", TimeZone.getDefault().getID());
			requestAttributes.put("deviceName", "TD001086");
			requestAttributes.put("terminalCd", "0");
			requestAttributes.put("priorAttempts", 0);
			requestAttributes.put("deviceTranCd", Long.toString(startTime));
			requestAttributes.put("merchantCd", "0");
			requestAttributes.put("allowPartialAuth", true);
			requestAttributes.put("trackData", trackData);
			requestAttributes.put("tranId", 12345678L);
			
			if (trackData.contains("="))
				requestAttributes.put("pan", trackData.substring(0, trackData.indexOf('=')));
			else
				requestAttributes.put("pan", trackData);
			
			requestAttributes.put("itemCount", 1);
			requestAttributes.put("price1", new BigInteger(amountMinor).divide(new BigInteger("200")).toString());
			requestAttributes.put("quantity1", 2);
			requestAttributes.put("code1", "03CF");
			requestAttributes.put("desc1", "ePortVend 03CF");
			
			
			when(step.getAttributes()).thenReturn(requestAttributes);
			when(step.getAttributeSafely(any(), any(), any())).thenAnswer(new Answer<Object>() {
				public Object answer(InvocationOnMock invocation) {
					Attribute attribute = invocation.getArgumentAt(0, Attribute.class);
					Class<?> convertTo = invocation.getArgumentAt(1, Class.class);
					boolean required = invocation.getArgumentAt(2, Boolean.class);
					return requestAttributes.get(attribute.getValue());
				}
			});
			
			resultAttributes = new HashMap<>();
			when(step.getResultAttributes()).thenReturn(resultAttributes);

			doAnswer(new Answer<Void>() {
				public Void answer(InvocationOnMock invocation) {
					Attribute attribute = invocation.getArgumentAt(0, Attribute.class);
					Object value = invocation.getArgumentAt(1, Object.class);
					resultAttributes.put(attribute.getValue(), value);
					return null;
				}
			}).when(step).setResultAttribute((Attribute) any(), any());
			
			qos = mock(QoS.class);
			when(taskInfo.getCurrentQos()).thenReturn(qos);
			when(qos.isPersistent()).thenReturn(true);
			when(qos.getTimeToLive()).thenReturn(20000L);
		}
		
		public int execute() throws ServiceException {
			return task.process(taskInfo);
		}

		public <T> T getResultAttribute(String attributeName, Class<T> convertTo) throws ConvertException {
			return ConvertUtils.convert(convertTo, resultAttributes.get(attributeName));
		}

		public <T> T getResultAttribute(Attribute attribute, Class<T> convertTo) throws ConvertException {
			return ConvertUtils.convert(convertTo, resultAttributes.get(attribute.getValue()));
		}
		
		public class MockApolloServer {
			public List<Map<String, Object>> requests = new ArrayList<>();
			public List<Map<String, Object>> responses = new ArrayList<>();
			private Pattern accountPattern = Pattern.compile(".*\\/account\\/([0-9]+)\\/?");
			private Pattern purchasePattern = Pattern.compile(".*\\/purchase\\/?");
			private Gson gson = new Gson();
			
			public HttpResponse execute(HttpRequest request) {
				try {
					Map<String, Object> responseAttributes = new LinkedHashMap<>();
					responses.add(responseAttributes);
					
					Map<String, Object> requestAttributes = new HashMap<>();
					requests.add(requestAttributes);

					String requestUrl = request.getRequestLine().getUri();
					requestAttributes.put("request_url", requestUrl);
					
					Matcher accountMatcher = accountPattern.matcher(requestUrl);
					Matcher purchaseMatcher = purchasePattern.matcher(requestUrl);

					if (errorCode > 0) {
						responseAttributes.put("code", errorCode);
						responseAttributes.put("message", errorMessage);
					} else if (purchaseMatcher.matches()) {
						HttpPost post = (HttpPost) request;
						HttpEntity entity = post.getEntity();
						
						Map<String, Object> jsonRequest = gson.fromJson(new InputStreamReader(entity.getContent(), HTTP.DEF_CONTENT_CHARSET), Map.class);
						requestAttributes.putAll(jsonRequest);
						
						//{"account_url":"/account/68046","reward_url":"/reward/18713030","url":"/purchase/18713030","account_id":68046,"transaction_id":18713030,"net_transaction_amount":1.25,"net_transaction_points":6,"new_available_balance":251.76,"new_points_balance":36,"reward_amount":0,"reward_points":6,"created":1483637519}

						List<Map<String, Object>> items = (List<Map<String, Object>>) requestAttributes.get("items");
						double totalPrice = items.stream().mapToDouble(m -> {try {return ConvertUtils.getDouble(m.get("price"), 0);} catch(Exception e) { throw new RuntimeException(e);}}).sum();
						long tranId = System.currentTimeMillis();
						int accountId = 68046;
						
						responseAttributes.put("account_url", "/account/" + accountId);
						responseAttributes.put("reward_url", "/reward/" + tranId);
						responseAttributes.put("url", "/purchase/" + tranId);
						responseAttributes.put("account_id", accountId);
						responseAttributes.put("transaction_id", tranId);
						responseAttributes.put("net_transaction_amount", totalPrice);
						responseAttributes.put("net_transaction_points", 6);
						responseAttributes.put("new_available_balance", balance-totalPrice);
						responseAttributes.put("new_points_balance", 36);
						responseAttributes.put("reward_amount", 0);
						responseAttributes.put("reward_points", 0);
						responseAttributes.put("created", System.currentTimeMillis()/1000);
					} else if (accountMatcher.matches()) {
						// {"account_id":"12345","active":true,"aliases":[21200000081607,6277229032316070],"available_balance":"0.56","current_balance":"0.56","daily_spend_limit":null,"eula_url":"\/affiliate\/41\/eula","points_balance":"24","program_url":"\/program\/27","recurring_load_url":null,"suggestion_url":null,"vendor_id":"569","url":"\/account\/12345"}
						String accountId = accountMatcher.group(1);
						responseAttributes.put("account_id", accountId);
						responseAttributes.put("active", true);
						responseAttributes.put("aliases", new String[] {"21200000081607","6277229032316070"});
						responseAttributes.put("available_balance", balance);
						responseAttributes.put("current_balance", balance);
						responseAttributes.put("eula_url", "/affiliate/41/eula");
						responseAttributes.put("points_balance", "24");
						responseAttributes.put("program_url", "/program/27");
						responseAttributes.put("recurring_load_url", null);
						responseAttributes.put("suggestion_url", null);
						responseAttributes.put("vendor_id", "569");
						responseAttributes.put("url", "/account/"+accountId);
					}
						
					String responseString = gson.toJson(responseAttributes);
					
					HttpResponse mockResponse = mock(HttpResponse.class);

					StatusLine mockStatus = mock(StatusLine.class);
					when(mockResponse.getStatusLine()).thenReturn(mockStatus);
					
					HttpEntity mockEntity = mock(HttpEntity.class);
					when(mockResponse.getEntity()).thenReturn(mockEntity);
					
					when(mockEntity.getContent()).thenReturn(new ReaderInputStream(new StringReader(responseString), HTTP.DEF_CONTENT_CHARSET, 2048));
					when(mockEntity.getContentLength()).thenReturn((long)responseString.length()+1);
					
					if (errorCode > 0) {
						when(mockStatus.getStatusCode()).thenReturn(422);
						when(mockEntity.getContentType()).thenReturn(new BasicHeader("Content-Type", "application/problem+json"));
					} else {
						when(mockStatus.getStatusCode()).thenReturn(200);
						when(mockEntity.getContentType()).thenReturn(new BasicHeader("Content-Type", "application/json; charset=utf-8"));
					}
					
					return mockResponse;
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		}
	}
}



