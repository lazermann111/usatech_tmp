package com.usatech.authoritylayer.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.InputStreamReader;
import java.io.StringReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.protocol.HTTP;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.google.gson.Gson;
import com.usatech.app.Attribute;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.authoritylayer.UGrydAuthorityTask;
import com.usatech.authoritylayer.UGrydAuthorityTask.UGryTransactionType;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAction;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.DebugLevel;
import com.usatech.layers.common.constants.DeniedReason;
import com.usatech.layers.common.constants.EntryType;

import simple.app.QoS;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.ReaderInputStream;

public class UGrydAuthorityTaskTests {
	
	static {
		System.setProperty("app.servicename", "UGrydTest");
		System.setProperty("log4j.configuration", "log4j-dev.properties");
	}
	
	private static void checkCommon(UGrydTestHolder test, int requestNum) throws Exception {
		UGryTransactionType transactonType = UGryTransactionType.lookup((String)test.server.requests.get(requestNum).get("transaction_type"));
		if (transactonType != UGryTransactionType.ACK) {
			int seqId = ConvertUtils.getInt(test.server.requests.get(requestNum).get("seqid"));
			assertEquals(seqId, test.task.getSeqIdCounter());
			assertTrue(seqId >= 0);
			assertTrue(seqId <= 32767);
			assertTrue(seqId >= test.task.getSeqMin());
			assertTrue(seqId <= test.task.getSeqMax());
			assertEquals("123456789", test.server.requests.get(requestNum).get("account_number"));
		}
		assertEquals("usatech", test.server.requests.get(requestNum).get("username"));
		assertEquals("!us4t3cH", test.server.requests.get(0).get("password"));
		assertEquals("0000VJ000000130", test.server.requests.get(requestNum).get("terminal"));
	}
	
	// === normal approved
	
	@Test
	public void testBalanceInquiryApproved() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.AUTHORIZATION, "1000");
		
		int resultCode = test.execute();
		assertEquals(0, resultCode);
		assertEquals(AuthResultCode.APPROVED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		checkCommon(test, 0);
		
		verify(test.httpClient, times(1)).execute(any());
		
		assertEquals("balance_inquiry", test.server.requests.get(0).get("transaction_type"));
		
		assertTrue(UGryTransactionType.BALANCE_INQUIRY.isSuccess(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, BigDecimal.class)));
		assertEquals(false, test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_HOLD_USED, boolean.class));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, Object.class));
		assertTrue(new BigDecimal("168911").compareTo(test.getResultAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, BigDecimal.class)) == 0);
		assertTrue(new BigDecimal("1000").compareTo(test.getResultAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, BigDecimal.class))==0);
		
		assertEquals("client.message.auth.CBORD_UGryd.approved", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, String.class));
		assertEquals("OK", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, String[].class)[0]);
	}
	
	@Test
	public void testVendInquiryApproved() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.AUTHORIZATION, "1000");
		test.requestAttributes.put("authorityName", "CBORD UGryd with Vending Support");
		
		int resultCode = test.execute();
		assertEquals(0, resultCode);
		assertEquals(AuthResultCode.APPROVED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		checkCommon(test, 0);
		
		verify(test.httpClient, times(1)).execute(any());
		
		assertEquals("vend_inquiry", test.server.requests.get(0).get("transaction_type"));
		assertTrue(UGryTransactionType.VEND_INQUIRY.isSuccess(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, BigDecimal.class)));
		assertEquals(true, test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_HOLD_USED, boolean.class));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, Object.class));
		//assertTrue(new BigDecimal("168911").compareTo(test.getResultAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, BigDecimal.class)) == 0);
		assertTrue(new BigDecimal("1000").compareTo(test.getResultAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, BigDecimal.class))==0);
		
		assertEquals("client.message.auth.CBORD_UGryd.approved", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, String.class));
		assertEquals("OK", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, String[].class)[0]);
	}

	@Test
	public void testSaleApproved() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.SALE, "1000");
		
		int resultCode = test.execute();
		assertEquals(0, resultCode);
		assertEquals(AuthResultCode.APPROVED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		checkCommon(test, 1);
		
		verify(test.httpClient, times(2)).execute(any());
		assertEquals("sale", test.server.requests.get(0).get("transaction_type"));
		assertEquals("ack", test.server.requests.get(1).get("transaction_type"));
		
		assertTrue(UGryTransactionType.SALE.isSuccess(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, BigDecimal.class)));
		assertNull(test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_HOLD_USED.getValue()));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, Object.class));
		assertTrue(new BigDecimal("168911").compareTo(test.getResultAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, BigDecimal.class)) == 0);
		assertTrue(new BigDecimal("1000").compareTo(test.getResultAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, BigDecimal.class))==0);
		assertEquals(new Date(1399048200000L), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS.getValue()));
	}
	
	@Test
	public void testVendSaleApproved() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.SALE, "1000");
		test.requestAttributes.put("authorityName", "CBORD UGryd with Vending Support");
		
		int resultCode = test.execute();
		assertEquals(0, resultCode);
		assertEquals(AuthResultCode.APPROVED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		checkCommon(test, 1);
		
		verify(test.httpClient, times(2)).execute(any());
		assertEquals("vend_sale", test.server.requests.get(0).get("transaction_type"));
		assertEquals("ack", test.server.requests.get(1).get("transaction_type"));
		
		assertTrue(UGryTransactionType.VEND_SALE.isSuccess(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, BigDecimal.class)));
		assertNull(test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_HOLD_USED.getValue()));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, Object.class));
		assertTrue(new BigDecimal("168911").compareTo(test.getResultAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, BigDecimal.class)) == 0);
		assertTrue(new BigDecimal("1000").compareTo(test.getResultAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, BigDecimal.class))==0);
		assertEquals(new Date(1399048200000L), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS.getValue()));
	}
	
	@Test
	public void testRefundApproved() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.REFUND, "1000");
		
		int resultCode = test.execute();
		assertEquals(0, resultCode);
		assertEquals(AuthResultCode.APPROVED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		checkCommon(test, 1);
		
		verify(test.httpClient, times(2)).execute(any());
		assertEquals("refund", test.server.requests.get(0).get("transaction_type"));
		assertEquals("ack", test.server.requests.get(1).get("transaction_type"));
		
		assertTrue(UGryTransactionType.REFUND.isSuccess(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, BigDecimal.class)));
		assertNull(test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_HOLD_USED.getValue()));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, Object.class));
		assertTrue(new BigDecimal("168911").compareTo(test.getResultAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, BigDecimal.class)) == 0);
		assertTrue(new BigDecimal("1000").compareTo(test.getResultAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, BigDecimal.class))==0);
		assertEquals(new Date(1399048200000L), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_TS.getValue()));
	}

	// === normal declined
	
	@Test
	public void testBalanceInquiryDeclined() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.AUTHORIZATION, "1000");
		test.server.decline = true;
		
		int resultCode = test.execute();
		assertEquals(1, resultCode);
		assertEquals(AuthResultCode.DECLINED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertEquals(DeniedReason.PROCESSOR_RESPONSE, test.resultAttributes.get(AuthorityAttrEnum.ATTR_DENIED_REASON.getValue()));
		checkCommon(test, 0);
		
		verify(test.httpClient, times(1)).execute(any());
		assertEquals("balance_inquiry", test.server.requests.get(0).get("transaction_type"));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, Object.class));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, Object.class));
		assertEquals("client.message.auth.CBORD_UGryd.denied-PROCESSOR_RESPONSE", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, String.class));
		assertEquals("TEST", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, String[].class)[0]);
	}
	
	@Test
	public void testBalanceInquiryDeclinedInsufficientFunds() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.AUTHORIZATION, "1000");
		test.task.setMinimumBalanceAmount(BigDecimal.ONE);
		test.balance = "0";
		
		int resultCode = test.execute();
		assertEquals(1, resultCode);
		assertEquals(AuthResultCode.DECLINED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertEquals(DeniedReason.INSUFFICIENT_FUNDS, test.resultAttributes.get(AuthorityAttrEnum.ATTR_DENIED_REASON.getValue()));
		checkCommon(test, 0);
		
		verify(test.httpClient, times(1)).execute(any());
		assertEquals("balance_inquiry", test.server.requests.get(0).get("transaction_type"));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, Object.class));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, Object.class));
		assertEquals("client.message.auth.CBORD_UGryd.denied-INSUFFICIENT_FUNDS", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, String.class));
		assertEquals("Insufficient funds: 0.00; At least 1.00 is required", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, String[].class)[0]);
	}

	@Test
	public void testBalanceInquiryDeclinedInsufficientFundsNoMinBalance() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.AUTHORIZATION, "1000");
		test.task.setMinimumBalanceAmount(BigDecimal.ZERO);
		test.balance = "0";
		
		int resultCode = test.execute();
		assertEquals(1, resultCode);
		assertEquals(AuthResultCode.DECLINED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertEquals(DeniedReason.INSUFFICIENT_FUNDS, test.resultAttributes.get(AuthorityAttrEnum.ATTR_DENIED_REASON.getValue()));
		checkCommon(test, 0);
		
		verify(test.httpClient, times(1)).execute(any());
		assertEquals("balance_inquiry", test.server.requests.get(0).get("transaction_type"));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, Object.class));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, Object.class));
		assertEquals("client.message.auth.CBORD_UGryd.denied-INSUFFICIENT_FUNDS", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, String.class));
		assertEquals("Insufficient funds: 0.00", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, String[].class)[0]);
	}

	@Test
	public void testVendInquiryDeclined() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.AUTHORIZATION, "1000");
		test.server.decline = true;
		test.requestAttributes.put("authorityName", "CBORD UGryd with Vending Support");
		
		int resultCode = test.execute();
		assertEquals(1, resultCode);
		assertEquals(AuthResultCode.DECLINED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertEquals(DeniedReason.PROCESSOR_RESPONSE, test.resultAttributes.get(AuthorityAttrEnum.ATTR_DENIED_REASON.getValue()));
		checkCommon(test, 0);
		
		verify(test.httpClient, times(1)).execute(any());
		assertEquals("vend_inquiry", test.server.requests.get(0).get("transaction_type"));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, Object.class));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, Object.class));
		assertEquals("client.message.auth.CBORD_UGryd.denied-PROCESSOR_RESPONSE", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, String.class));
		assertEquals("TEST", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, String[].class)[0]);
	}

	@Test
	public void testSaleDeclined() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.SALE, "1000");
		test.server.decline = true;

		int resultCode = test.execute();
		assertEquals(1, resultCode);
		assertEquals(AuthResultCode.DECLINED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		checkCommon(test, 0);
		
		verify(test.httpClient, times(1)).execute(any());
		assertEquals("sale", test.server.requests.get(0).get("transaction_type"));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, Object.class));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, Object.class));
	}
	
	@Test
	public void testVendSaleDeclined() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.SALE, "1000");
		test.server.decline = true;
		test.requestAttributes.put("authorityName", "CBORD UGryd with Vending Support");
		
		int resultCode = test.execute();
		assertEquals(1, resultCode);
		assertEquals(AuthResultCode.DECLINED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		checkCommon(test, 0);
		
		verify(test.httpClient, times(1)).execute(any());
		assertEquals("vend_sale", test.server.requests.get(0).get("transaction_type"));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, Object.class));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, Object.class));
	}
	
	@Test
	public void testRefundDeclined() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.REFUND, "1000");
		test.server.decline = true;

		int resultCode = test.execute();
		assertEquals(1, resultCode);
		assertEquals(AuthResultCode.DECLINED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		checkCommon(test, 0);
		
		verify(test.httpClient, times(1)).execute(any());
		assertEquals("refund", test.server.requests.get(0).get("transaction_type"));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, Object.class));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, Object.class));
	}
	
	@Test
	public void testAuthStale() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.AUTHORIZATION, "1000");
		long authTime = System.currentTimeMillis() - TimeUnit.SECONDS.toMillis(19);
		test.requestAttributes.put("authTime", authTime);
		
		int resultCode = test.execute();
		assertEquals(2, resultCode);
		
		verify(test.httpClient, never()).execute(any());
		
		assertEquals(AuthResultCode.FAILED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertEquals(DeniedReason.INTERNAL_TIMEOUT.toString(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue()));
	}
	
	@Test
	public void testSaleStale() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.SALE, "1000");
		long authTime = System.currentTimeMillis() - TimeUnit.SECONDS.toMillis(56);
		test.requestAttributes.put("authTime", authTime);
		
		int resultCode = test.execute();
		assertEquals(0, resultCode);
		
		verify(test.httpClient, times(2)).execute(any());
		assertEquals("sale", test.server.requests.get(0).get("transaction_type"));
		assertEquals("ack", test.server.requests.get(1).get("transaction_type"));
		
		assertTrue(UGryTransactionType.SALE.isSuccess(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, BigDecimal.class)));
		assertNull(test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_HOLD_USED.getValue()));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_REF_CD, Object.class));
		assertTrue(new BigDecimal("168911").compareTo(test.getResultAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, BigDecimal.class)) == 0);
		assertTrue(new BigDecimal("1000").compareTo(test.getResultAttribute(AuthorityAttrEnum.ATTR_AMT_APPROVED, BigDecimal.class))==0);
	}
	
	@Test
	public void testBalanceInquiryConnectFailure() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.AUTHORIZATION, "1000");
		
		doThrow(new HttpHostConnectException(new SocketException("TEST"), HttpHost.create(test.task.getUgrydUrl()))).when(test.httpClient).execute(any());

		int resultCode = test.execute();
		assertEquals(2, resultCode);
		
		verify(test.httpClient, times(1)).execute(any());
		
		assertEquals(AuthResultCode.FAILED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertEquals(DeniedReason.PROCESSOR_ERROR.toString(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue()));
		
		assertEquals("client.message.auth.CBORD_UGryd.denied-PROCESSOR_ERROR", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, String.class));
		assertEquals("Connect to nm.uat.ugryd.com/services.php failed: TEST", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, String[].class)[0]);
	}
	
	@Test
	public void testVendInquiryConnectFailure() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.AUTHORIZATION, "1000");
		test.requestAttributes.put("authorityName", "CBORD UGryd with Vending Support");
		
		doThrow(new HttpHostConnectException(new SocketException("TEST"), HttpHost.create(test.task.getUgrydUrl()))).when(test.httpClient).execute(any());

		int resultCode = test.execute();
		assertEquals(2, resultCode);
		
		verify(test.httpClient, times(1)).execute(any());
		
		assertEquals(AuthResultCode.FAILED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertEquals(DeniedReason.PROCESSOR_ERROR.toString(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue()));
	}

	@Test
	public void testSaleConnectFailure() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.SALE, "1000");
		doThrow(new HttpHostConnectException(new ConnectException("Connection Refused"), HttpHost.create(test.task.getUgrydUrl()))).when(test.httpClient).execute(any());

		int resultCode = test.execute();
		assertEquals(1, resultCode); // declined on a failed sale connection error
		
		verify(test.httpClient, times(1)).execute(any());
		
		assertEquals(AuthResultCode.DECLINED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertEquals(DeniedReason.PROCESSOR_ERROR.toString(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue()));
	}

	@Test
	public void testBalanceInquiryReadFailure() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.AUTHORIZATION, "1000");
		doThrow(new SocketTimeoutException("Read timed out")).when(test.httpClient).execute(any());

		int resultCode = test.execute();
		assertEquals(2, resultCode);

		verify(test.httpClient, times(1)).execute(any());
		
		assertEquals(AuthResultCode.FAILED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertEquals(DeniedReason.PROCESSOR_TIMEOUT.toString(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue()));
	}
	
	@Test(expected=RetrySpecifiedServiceException.class)
	public void testVendInquiryReadFailure() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.AUTHORIZATION, "1000");
		test.requestAttributes.put("authorityName", "CBORD UGryd with Vending Support");
		
		doThrow(new SocketTimeoutException("Read timed out")).when(test.httpClient).execute(any());

		try {
			test.execute();
		} finally {
			verify(test.httpClient, times(1)).execute(any());
		}
	}
	
	@Test
	public void testSaleReadFailure() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.SALE, "1000");
		doThrow(new SocketTimeoutException("Read timed out")).when(test.httpClient).execute(any());

		int resultCode = test.execute();
		assertEquals(1, resultCode); // declined on a failed sale read error - no ack so it will be auto reversed
		
		verify(test.httpClient, times(1)).execute(any());
		
		assertEquals(AuthResultCode.DECLINED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertEquals(DeniedReason.PROCESSOR_TIMEOUT.toString(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue()));
	}
	
	@Test
	public void testVendInquiryRetrySuccess() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.AUTHORIZATION, "1000");
		test.requestAttributes.put("authorityName", "CBORD UGryd with Vending Support");
		when(test.taskInfo.getRetryCount()).thenReturn(1);
		when(test.taskInfo.isRedelivered()).thenReturn(true);
		
		int resultCode = test.execute();
		assertEquals(2, resultCode); // failed on a successfull reversal of a failed auth
		
		verify(test.httpClient, times(2)).execute(any());
		
		assertEquals(AuthResultCode.FAILED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertEquals(DeniedReason.PROCESSOR_ERROR.toString(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue()));
	}

	@Test
	public void testVendInquiryRetryFailure() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.AUTHORIZATION, "1000");
		test.task.setMaxAuthStaleness((int)TimeUnit.HOURS.toMillis(1)); // for manual debugging
		test.requestAttributes.put("authorityName", "CBORD UGryd with Vending Support");

		when(test.taskInfo.getRetryCount()).thenReturn(1);
		when(test.taskInfo.isRedelivered()).thenReturn(true);
		
		doThrow(new SocketTimeoutException("Read timed out")).when(test.httpClient).execute(any());
		
		int resultCode = test.execute();
		assertEquals(2, resultCode); // failed on a successfull reversal of a failed auth
		
		verify(test.httpClient, times(1)).execute(any());
		
		assertEquals(AuthResultCode.FAILED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertEquals(DeniedReason.PROCESSOR_ERROR.toString(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue()));
	}
	
	@Test(expected=RetrySpecifiedServiceException.class)
	public void testSaleRedelivered() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.SALE, "1000");
		when(test.taskInfo.getRetryCount()).thenReturn(1);
		when(test.taskInfo.isRedelivered()).thenReturn(true);
		
		// is this right?
		
		test.execute();
	}
	
	@Test
	public void testSeqId() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.AUTHORIZATION, "1000");
		
		test.execute();
		assertEquals(AuthResultCode.APPROVED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		verify(test.httpClient, times(1)).execute(any());
		Map<String, Object> misc1 = test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, Map.class);
		int seqId1 = ConvertUtils.getInt(misc1.get("seqid"));
		
		test.execute();
		assertEquals(AuthResultCode.APPROVED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		verify(test.httpClient, times(2)).execute(any());
		Map<String, Object> misc2 = test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, Map.class);
		int seqId2 = ConvertUtils.getInt(misc2.get("seqid"));
		assertEquals(seqId1+1, seqId2);
		
		test.execute();
		assertEquals(AuthResultCode.APPROVED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		verify(test.httpClient, times(3)).execute(any());
		Map<String, Object> misc3 = test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, Map.class);
		int seqId3 = ConvertUtils.getInt(misc3.get("seqid"));
		assertEquals(seqId2+1, seqId3);
	}

	@Test
	public void testHighBalanceFormat() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.AUTHORIZATION, "1000");
		test.balance = "5000.00";
		
		int resultCode = test.execute();
		assertEquals(0, resultCode);
		assertEquals(AuthResultCode.APPROVED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		checkCommon(test, 0);
		
		verify(test.httpClient, times(1)).execute(any());
		
		assertEquals("balance_inquiry", test.server.requests.get(0).get("transaction_type"));
		
		assertTrue(UGryTransactionType.BALANCE_INQUIRY.isSuccess(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, BigDecimal.class)));
		
		BigDecimal balance = test.getResultAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, BigDecimal.class);
		assertEquals("500000", balance.toString());
	}

	@Test
	public void testMinBalance() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.AUTHORIZATION, "1000");
		test.task.setMinimumBalanceAmount(new BigDecimal("3.00"));
		test.balance = "1.25";
		
		int resultCode = test.execute();
		assertEquals(1, resultCode);
		assertEquals(AuthResultCode.DECLINED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		checkCommon(test, 0);
		
		verify(test.httpClient, times(1)).execute(any());
		assertEquals("balance_inquiry", test.server.requests.get(0).get("transaction_type"));
		assertEquals("Insufficient funds: 1.25; At least 3.00 is required", test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, String.class));
		
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, Object.class));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, Object.class));
		
		BigDecimal balance = test.getResultAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, BigDecimal.class);
		assertEquals("125", balance.toString());
	}

	@Test
	public void testNoMinBalance() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.AUTHORIZATION, "1000");
		test.task.setMinimumBalanceAmount(new BigDecimal("0"));
		test.balance = "0.00";
		
		int resultCode = test.execute();
		assertEquals(1, resultCode);
		assertEquals(AuthResultCode.DECLINED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		checkCommon(test, 0);
		
		verify(test.httpClient, times(1)).execute(any());
		assertEquals("balance_inquiry", test.server.requests.get(0).get("transaction_type"));
		assertEquals("Insufficient funds: 0.00", test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, String.class));
		
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, Object.class));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, Object.class));
		
		BigDecimal balance = test.getResultAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, BigDecimal.class);
		assertEquals("0", balance.toString());
	}

	@Test
	public void testNoMinBalanceNoPartialAuth() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.AUTHORIZATION, "1000");
		test.requestAttributes.put(AuthorityAttrEnum.ATTR_PARTIAL_AUTH_ALLOWED.getValue(), false);
		test.task.setMinimumBalanceAmount(new BigDecimal("0"));
		test.balance = "3.00";

		int resultCode = test.execute();
		assertEquals(1, resultCode);
		assertEquals(AuthResultCode.DECLINED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		checkCommon(test, 0);
		
		verify(test.httpClient, times(1)).execute(any());
		assertEquals("balance_inquiry", test.server.requests.get(0).get("transaction_type"));
		assertEquals("Insufficient funds: 3.00", test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC, String.class));
		
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, Object.class));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, Object.class));
		
		BigDecimal balance = test.getResultAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, BigDecimal.class);
		assertEquals("300", balance.toString());
	}
	
	@Test
	public void testNoMinBalanceWithPartialAuth() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.AUTHORIZATION, "1000");
		test.task.setMinimumBalanceAmount(new BigDecimal("0"));
		test.balance = "3.00";

		int resultCode = test.execute();
		assertEquals(0, resultCode);
		assertEquals(AuthResultCode.PARTIAL.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		checkCommon(test, 0);
		
		verify(test.httpClient, times(1)).execute(any());
		
		assertEquals("balance_inquiry", test.server.requests.get(0).get("transaction_type"));
		
		assertTrue(UGryTransactionType.BALANCE_INQUIRY.isSuccess(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, BigDecimal.class)));
		
		BigDecimal balance = test.getResultAttribute(AuthorityAttrEnum.ATTR_BALANCE_AMOUNT, BigDecimal.class);
		assertEquals("300", balance.toString());
	}

	@Test
	public void testUsePaddingAndSerialNumber() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.AUTHORIZATION, "1000");
		test.task.setUseDeviceSerialCdForTerminalCd(true);
		
		int resultCode = test.execute();
		assertEquals(0, resultCode);
		assertEquals(AuthResultCode.APPROVED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		
		verify(test.httpClient, times(1)).execute(any());
		
		assertEquals("0000VJ000000130", test.server.requests.get(0).get("terminal"));
	}
	
	@Test
	public void testDoNotUseSerialNumber() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.AUTHORIZATION, "1000");
		test.task.setUseDeviceSerialCdForTerminalCd(false);
		
		int resultCode = test.execute();
		assertEquals(0, resultCode);
		assertEquals(AuthResultCode.APPROVED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		
		verify(test.httpClient, times(1)).execute(any());
		
		assertEquals("ACHGROSSSTORE", test.server.requests.get(0).get("terminal"));
	}

	@Test
	public void testDoNotPadSerialNumber() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.AUTHORIZATION, "1000");
		test.task.setUseDeviceSerialCdForTerminalCd(true);
		test.task.setDoNotPadTerminalCds(new String[] {"VJ000000130"});
		
		int resultCode = test.execute();
		assertEquals(0, resultCode);
		assertEquals(AuthResultCode.APPROVED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		
		verify(test.httpClient, times(1)).execute(any());
		
		assertEquals("VJ000000130", test.server.requests.get(0).get("terminal"));
	}
	
	@Test
	public void testDoNotUseSerialNumberAndNoTerminalCd() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.AUTHORIZATION, "1000");
		test.task.setUseDeviceSerialCdForTerminalCd(false);
		test.task.setPadTerminalCd(true);
		test.requestAttributes.remove("terminalCd");
		
		int resultCode = test.execute();
		assertEquals(0, resultCode);
		assertEquals(AuthResultCode.APPROVED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		
		verify(test.httpClient, times(1)).execute(any());
		
		assertEquals("0000VJ000000130", test.server.requests.get(0).get("terminal"));
	}
	
	@Test
	public void testDeniedUnknownTerminal() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.AUTHORIZATION, "1000");
		test.server.decline = true;
		test.server.responseCode = "1000.34";
		test.server.responseMessage = "UTERMINAL UNKNOWN";
		
		int resultCode = test.execute();
		assertEquals(1, resultCode);
		assertEquals(AuthResultCode.DECLINED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertEquals(DeniedReason.CONFIGURATION_ISSUE, test.resultAttributes.get(AuthorityAttrEnum.ATTR_DENIED_REASON.getValue()));
		checkCommon(test, 0);
		
		verify(test.httpClient, times(1)).execute(any());
		assertEquals("balance_inquiry", test.server.requests.get(0).get("transaction_type"));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, Object.class));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, Object.class));
		assertEquals("client.message.auth.CBORD_UGryd.denied-CONFIGURATION_ISSUE", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, String.class));
		assertEquals("UTERMINAL UNKNOWN", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, String[].class)[0]);
	}

	@Test
	public void testDeniedLowBalance() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.AUTHORIZATION, "1000");
		test.server.decline = true;
		test.server.responseCode = "1000.99";
		test.server.responseMessage = "LOW BALANCE";
		
		int resultCode = test.execute();
		assertEquals(1, resultCode);
		assertEquals(AuthResultCode.DECLINED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertEquals(DeniedReason.INSUFFICIENT_FUNDS, test.resultAttributes.get(AuthorityAttrEnum.ATTR_DENIED_REASON.getValue()));
		checkCommon(test, 0);
		
		verify(test.httpClient, times(1)).execute(any());
		assertEquals("balance_inquiry", test.server.requests.get(0).get("transaction_type"));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, Object.class));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, Object.class));
		assertEquals("client.message.auth.CBORD_UGryd.denied-INSUFFICIENT_FUNDS", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, String.class));
		assertEquals("LOW BALANCE", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, String[].class)[0]);
	}

	@Test
	public void testDeniedNotOnSystem() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.AUTHORIZATION, "1000");
		test.server.decline = true;
		test.server.responseCode = "1000.99";
		test.server.responseMessage = "NOT ON SYSTEM";
		
		int resultCode = test.execute();
		assertEquals(1, resultCode);
		assertEquals(AuthResultCode.DECLINED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertEquals(DeniedReason.NO_ACCOUNT, test.resultAttributes.get(AuthorityAttrEnum.ATTR_DENIED_REASON.getValue()));
		checkCommon(test, 0);
		
		verify(test.httpClient, times(1)).execute(any());
		assertEquals("balance_inquiry", test.server.requests.get(0).get("transaction_type"));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, Object.class));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, Object.class));
		assertEquals("client.message.auth.CBORD_UGryd.denied-NO_ACCOUNT", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, String.class));
		assertEquals("NOT ON SYSTEM", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, String[].class)[0]);
	}
	
	@Test
	public void testDeniedPatronDataNotFound() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.AUTHORIZATION, "1000");
		test.server.decline = true;
		test.server.responseCode = "1000.99";
		test.server.responseMessage = "Patron data not found";
		
		int resultCode = test.execute();
		assertEquals(1, resultCode);
		assertEquals(AuthResultCode.DECLINED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertEquals(DeniedReason.NO_ACCOUNT, test.resultAttributes.get(AuthorityAttrEnum.ATTR_DENIED_REASON.getValue()));
		checkCommon(test, 0);
		
		verify(test.httpClient, times(1)).execute(any());
		assertEquals("balance_inquiry", test.server.requests.get(0).get("transaction_type"));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, Object.class));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, Object.class));
		assertEquals("client.message.auth.CBORD_UGryd.denied-NO_ACCOUNT", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, String.class));
		assertEquals("Patron data not found", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, String[].class)[0]);
	}

	@Test
	public void testDeniedLostCard() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.AUTHORIZATION, "1000");
		test.server.decline = true;
		test.server.responseCode = "1000.99";
		test.server.responseMessage = "CARD LOST";
		
		int resultCode = test.execute();
		assertEquals(1, resultCode);
		assertEquals(AuthResultCode.DECLINED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertEquals(DeniedReason.NO_ACCOUNT, test.resultAttributes.get(AuthorityAttrEnum.ATTR_DENIED_REASON.getValue()));
		checkCommon(test, 0);
		
		verify(test.httpClient, times(1)).execute(any());
		assertEquals("balance_inquiry", test.server.requests.get(0).get("transaction_type"));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, Object.class));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, Object.class));
		assertEquals("client.message.auth.CBORD_UGryd.denied-NO_ACCOUNT", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, String.class));
		assertEquals("CARD LOST", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, String[].class)[0]);
	}

	@Test
	public void testDeniedConnErr() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.AUTHORIZATION, "1000");
		test.server.decline = true;
		test.server.responseCode = "1000.99";
		test.server.responseMessage = "CONN ERROR";
		
		int resultCode = test.execute();
		assertEquals(1, resultCode);
		assertEquals(AuthResultCode.DECLINED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertEquals(DeniedReason.PROCESSOR_ERROR, test.resultAttributes.get(AuthorityAttrEnum.ATTR_DENIED_REASON.getValue()));
		checkCommon(test, 0);
		
		verify(test.httpClient, times(1)).execute(any());
		assertEquals("balance_inquiry", test.server.requests.get(0).get("transaction_type"));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, Object.class));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, Object.class));
		assertEquals("client.message.auth.CBORD_UGryd.denied-PROCESSOR_ERROR", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, String.class));
		assertEquals("CONN ERROR", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, String[].class)[0]);
	}

	@Test
	public void testDeniedInvalidSearchPath() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.AUTHORIZATION, "1000");
		test.server.decline = true;
		test.server.responseCode = "1000.99";
		test.server.responseMessage = "INVALID SEARCH PATH";
		
		int resultCode = test.execute();
		assertEquals(1, resultCode);
		assertEquals(AuthResultCode.DECLINED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertEquals(DeniedReason.PROCESSOR_ERROR, test.resultAttributes.get(AuthorityAttrEnum.ATTR_DENIED_REASON.getValue()));
		checkCommon(test, 0);
		
		verify(test.httpClient, times(1)).execute(any());
		assertEquals("balance_inquiry", test.server.requests.get(0).get("transaction_type"));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, Object.class));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, Object.class));
		assertEquals("client.message.auth.CBORD_UGryd.denied-PROCESSOR_ERROR", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, String.class));
		assertEquals("INVALID SEARCH PATH", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, String[].class)[0]);
	}

	@Test
	public void testDeniedInvalidMessageConfiguration() throws Exception {
		UGrydTestHolder test = new UGrydTestHolder(AuthorityAction.AUTHORIZATION, "1000");
		test.server.decline = true;
		test.server.responseCode = "1000.99";
		test.server.responseMessage = "INVALID MESSAGE CONFIGURATION";
		
		int resultCode = test.execute();
		assertEquals(1, resultCode);
		assertEquals(AuthResultCode.DECLINED.getValue(), test.resultAttributes.get(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue()));
		assertEquals(DeniedReason.CONFIGURATION_ISSUE, test.resultAttributes.get(AuthorityAttrEnum.ATTR_DENIED_REASON.getValue()));
		checkCommon(test, 0);
		
		verify(test.httpClient, times(1)).execute(any());
		assertEquals("balance_inquiry", test.server.requests.get(0).get("transaction_type"));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, Object.class));
		assertNotNull(test.getResultAttribute(AuthorityAttrEnum.ATTR_AUTH_AUTHORITY_MISC_DATA, Object.class));
		assertEquals("client.message.auth.CBORD_UGryd.denied-CONFIGURATION_ISSUE", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY, String.class));
		assertEquals("INVALID MESSAGE CONFIGURATION", test.getResultAttribute(AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS, String[].class)[0]);
	}

	// This is just a class to setup and hold everything that will be used in more than one test case
	protected static class UGrydTestHolder {
		UGrydAuthorityTask task;
		MockUGrydServer server;
		MessageChainTaskInfo taskInfo;
		MessageChainStep step;
		QoS qos;
		long startTime;
		long traceNumber;
		int posPtaId;
		Map<String, Object> requestAttributes;
		Map<String, Object> resultAttributes;
		HttpClient httpClient;
		String balance = "1689.11";
		
		public UGrydTestHolder(AuthorityAction action, String amountMinor) throws Exception {
			task = new UGrydAuthorityTask();
			task.setDebugLevel(DebugLevel.DEBUG);
			task.setUgrydUrl("http://nm.uat.ugryd.com/services.php");
			task.setUgrydUsername("usatech");
			task.setUgrydPassword("!us4t3cH");
			task.setInstance(1);
			task.setInstanceCount(4);
			task.initialize();
			task.setMaxAuthStaleness((int)TimeUnit.SECONDS.toMillis(15));
			
			httpClient = mock(HttpClient.class);
			task.getHttpRequester().setHttpClient(httpClient);

			server = new MockUGrydServer();
			when(httpClient.execute(any())).thenAnswer(new Answer<Object>() {
				public Object answer(InvocationOnMock invocation) {
					HttpRequest request = invocation.getArgumentAt(0, HttpEntityEnclosingRequestBase.class);
					return server.execute(request);
				}
			});
			
			taskInfo = mock(MessageChainTaskInfo.class);
			when(taskInfo.isRedelivered()).thenReturn(false);
			
			step = mock(MessageChainStep.class);
			when(taskInfo.getStep()).thenReturn(step);
			
			Random random = new Random();
			
			startTime = System.currentTimeMillis()-10;
			traceNumber = random.nextLong();
			posPtaId = random.nextInt(1000000) + 100000;
			
			requestAttributes = new HashMap<String, Object>();
			requestAttributes.put("actionType", action);
			requestAttributes.put("online", action == AuthorityAction.AUTHORIZATION ? true : false);
			requestAttributes.put("serviceType", 1);
			requestAttributes.put("csPhone", "888-561-4748");
			requestAttributes.put("city", "Frazer");
			requestAttributes.put("countryCd", "US");
			requestAttributes.put("csEmail", "support@usatech.com");
			requestAttributes.put("mcc", "7994");
			requestAttributes.put("deviceSerialCd", "VJ000000130");
			requestAttributes.put("doingBusinessAs", "Elavon Test Bank Numb");
			requestAttributes.put("authorityName", "CBORD UGryd");
			requestAttributes.put("traceNumber", traceNumber);
			requestAttributes.put("posPtaId", posPtaId);
			requestAttributes.put("currencyCd", "USD");
			requestAttributes.put("authTime", startTime);
			requestAttributes.put("entryType", EntryType.SWIPE);
			requestAttributes.put("amount", new BigInteger(amountMinor));
			requestAttributes.put("minorCurrencyFactor", 100);
			requestAttributes.put("timeZoneGuid", TimeZone.getDefault().getID());
			requestAttributes.put("deviceName", "TD001086");
			requestAttributes.put("terminalCd", "ACHGROSSSTORE");
			requestAttributes.put("priorAttempts", 0);
			requestAttributes.put("deviceTranCd", Long.toString(startTime));
			requestAttributes.put("merchantCd", "0");
			requestAttributes.put("allowPartialAuth", true);
			requestAttributes.put("trackData", "123456789");
			
			when(step.getAttributes()).thenReturn(requestAttributes);
			when(step.getAttributeSafely(any(), any(), any())).thenAnswer(new Answer<Object>() {
				public Object answer(InvocationOnMock invocation) {
					Attribute attribute = invocation.getArgumentAt(0, Attribute.class);
					Class<?> convertTo = invocation.getArgumentAt(1, Class.class);
					boolean required = invocation.getArgumentAt(2, Boolean.class);
					return requestAttributes.get(attribute.getValue());
				}
			});
			
			resultAttributes = new HashMap<>();
			when(step.getResultAttributes()).thenReturn(resultAttributes);

			doAnswer(new Answer<Void>() {
				public Void answer(InvocationOnMock invocation) {
					Attribute attribute = invocation.getArgumentAt(0, Attribute.class);
					Object value = invocation.getArgumentAt(1, Object.class);
					resultAttributes.put(attribute.getValue(), value);
					return null;
				}
			}).when(step).setResultAttribute((Attribute) any(), any());
			
			qos = mock(QoS.class);
			when(taskInfo.getCurrentQos()).thenReturn(qos);
			when(qos.isPersistent()).thenReturn(true);
			when(qos.getTimeToLive()).thenReturn(20000L);
		}
		
		public int execute() throws ServiceException {
			return task.process(taskInfo);
		}

		public <T> T getResultAttribute(String attributeName, Class<T> convertTo) throws ConvertException {
			return ConvertUtils.convert(convertTo, resultAttributes.get(attributeName));
		}

		public <T> T getResultAttribute(Attribute attribute, Class<T> convertTo) throws ConvertException {
			return ConvertUtils.convert(convertTo, resultAttributes.get(attribute.getValue()));
		}
		
		public class MockUGrydServer {
			public List<Map<String, Object>> requests = new ArrayList<>();
			public List<Map<String, Object>> responses = new ArrayList<>();
			public boolean decline = false;
			String responseCode;
			String responseMessage;
			
			public HttpResponse execute(HttpRequest request) {
				try {
					HttpPost post = (HttpPost) request;
					HttpEntity entity = post.getEntity();
					
					Gson gson = new Gson();
					
					Map<String, Object> requestAttributes = gson.fromJson(new InputStreamReader(entity.getContent(), HTTP.DEF_CONTENT_CHARSET), Map.class);
					requests.add(requestAttributes);
					
					String transactionTypeStr = (String) requestAttributes.get("transaction_type");
					UGryTransactionType transactionType = UGryTransactionType.lookup(transactionTypeStr);
					
					Map<String, Object> responseAttributes = new LinkedHashMap<>();
					responses.add(responseAttributes);
					
					responseAttributes.put("type", transactionType.getValue());

					if (decline) {
						responseAttributes.put("response_code", responseCode != null ? responseCode : Integer.toString(transactionType.getResponseCode().intValue()) + ".99");
						responseAttributes.put("response_msg", responseMessage != null ? responseMessage : "TEST");
					} else {
						responseAttributes.put("terminal", requestAttributes.get("terminal"));
						responseAttributes.put("response_code", transactionType.getResponseString());
						responseAttributes.put("response_msg", "OK");
						
						if (transactionType != UGryTransactionType.ACK) {
							responseAttributes.put("seqid", requestAttributes.get("seqid"));
							responseAttributes.put("account_number", requestAttributes.get("account_number"));
							responseAttributes.put("id", Long.toString(System.currentTimeMillis()));
							responseAttributes.put("first_name", "James");
							responseAttributes.put("middle_name", "");
							responseAttributes.put("last_name", "Demo");
							responseAttributes.put("balance", balance);
							responseAttributes.put("university_id", "92");
							
						}
					
						switch (transactionType) {
							case BALANCE_INQUIRY:
								break;
							case VEND_INQUIRY:
								responseAttributes.put("approved_funds", requestAttributes.get("max_vend_amount"));
								break;
							case SALE:
								responseAttributes.put("authcode", "1F3A76DF");
								responseAttributes.put("terminal_time", "20140502123000");
								//responseAttributes.put("entry_mode", "1");
								break;
							case VEND_SALE:
								responseAttributes.put("authcode", "1F3A76DF");
								responseAttributes.put("terminal_time", "20140502123000");
								//responseAttributes.put("entry_mode", "1");
								break;
							case REFUND:
								responseAttributes.put("authcode", "1F3A76DF");
								responseAttributes.put("terminal_time", "20140502123000");
								//responseAttributes.put("entry_mode", "1");
								break;
							case ACK:
								break;
							default:
								break;
						}
					}
					
					String responseString = gson.toJson(responseAttributes);
					
					HttpResponse mockResponse = mock(HttpResponse.class);

					StatusLine mockStatus = mock(StatusLine.class);
					when(mockResponse.getStatusLine()).thenReturn(mockStatus);
					when(mockStatus.getStatusCode()).thenReturn(200);
					
					HttpEntity mockEntity = mock(HttpEntity.class);
					when(mockResponse.getEntity()).thenReturn(mockEntity);
					
					when(mockEntity.getContent()).thenReturn(new ReaderInputStream(new StringReader(responseString), HTTP.DEF_CONTENT_CHARSET, 2048));
					when(mockEntity.getContentLength()).thenReturn((long)responseString.length()+1);
					
					when(mockEntity.getContentType()).thenReturn(entity.getContentType());
					
					return mockResponse;
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		}
	}
}

