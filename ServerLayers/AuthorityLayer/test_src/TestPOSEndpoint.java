import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpConnectionParams;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.SecureProtocolSocketFactory;


public class TestPOSEndpoint {

	public static class AnyTrustManager implements X509TrustManager {
		public X509Certificate[] getAcceptedIssuers() {
			return new X509Certificate[0];
		}

		public void checkClientTrusted(X509Certificate[] certs, String authType) {
		}

		public void checkServerTrusted(X509Certificate[] certs, String authType) {
		}
	}

	public static class MySecureProtocolSocketFactory implements SecureProtocolSocketFactory {
		protected final SSLSocketFactory factory;

		public MySecureProtocolSocketFactory(SSLSocketFactory factory) {
			super();
			this.factory = factory;
		}

		@Override
		public Socket createSocket(String host, int port, InetAddress localAddress, int localPort) throws IOException, UnknownHostException {
			return factory.createSocket(host, port, localAddress, localPort);
		}

		@Override
		public Socket createSocket(String host, int port, InetAddress localAddress, int localPort, HttpConnectionParams params) throws IOException, UnknownHostException, ConnectTimeoutException {
			return factory.createSocket(host, port, localAddress, localPort);
		}

		@Override
		public Socket createSocket(String host, int port) throws IOException, UnknownHostException {
			return factory.createSocket(host, port);
		}

		@Override
		public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException, UnknownHostException {
			return factory.createSocket(socket, host, port, autoClose);
		}
	}

	/**
	 * @param args
	 * @throws Exception
	 * @throws
	 */
	public static void main(String[] args) throws Exception {
		SSLContext context = SSLContext.getInstance("TLS");
		context.init(null, new TrustManager[] { new AnyTrustManager() }, null);
		Protocol.registerProtocol("https", new Protocol("https", new MySecureProtocolSocketFactory(context.getSocketFactory()), 443));

		HttpClient client = new HttpClient();
		PostMethod method = new PostMethod("https://api.card.v4dev.com/integrations/usatech/");
		method.setRequestHeader("Content-type", "text/xml; charset=UTF-8");
		method.setRequestBody(new FileInputStream("C:\\Users\\bkrug\\Documents\\usatech-authorize.xml"));
		int rc = client.executeMethod(method);
		System.out.println("Response:------------------------------------------");
		System.out.println(method.getResponseBodyAsString());
	}

}
