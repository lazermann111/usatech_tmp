<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="text"/>

	<xsl:template match="/BlackboardGatewayConfig">
		<text disable-output-escaping="yes">DECLARE
	CURSOR l_cur IS
		SELECT '' BLACKBRD_SERVER_NAME, '' REMOTE_SERVER_ADDR, '' REMOTE_SERVER_ADDR_ALT, 0 REMOTE_SERVER_PORT_NUM FROM DUAL WHERE 1 = 0
</text>
		<xsl:for-each select="BlackboardServer">
			<xsl:call-template name="BlackboardServer"/>
		</xsl:for-each>
		<text disable-output-escaping="yes"><![CDATA[;
BEGIN
	FOR l_rec IN l_cur LOOP
		UPDATE PSS.BLACKBRD_AUTHORITY
		   SET REMOTE_SERVER_ADDR = l_rec.REMOTE_SERVER_ADDR,
		       REMOTE_SERVER_ADDR_ALT = l_rec.REMOTE_SERVER_ADDR_ALT,
		       REMOTE_SERVER_PORT_NUM = l_rec.REMOTE_SERVER_PORT_NUM
		 WHERE BLACKBRD_SERVER_NAME = l_rec.BLACKBRD_SERVER_NAME;
		IF SQL%NOTFOUND THEN
			DBMS_OUTPUT.PUT_LINE('WARNING: Could not find Blackboard Server ''' || l_rec.BLACKBRD_SERVER_NAME || '''; Nothing updated.');
		ELSE
			DBMS_OUTPUT.PUT_LINE('Updated the remote address and port for Blackboard Server ''' || l_rec.BLACKBRD_SERVER_NAME || '''.');	
		END IF;
		COMMIT;
	END LOOP;
END;
]]></text>		
	</xsl:template>
	
	<xsl:template name="BlackboardServer">
		<text disable-output-escaping="yes">		UNION ALL SELECT REPLACE('</text>
		<xsl:value-of select="translate(@Name, &#34;'&#34;, &#34;&#167;&#34;)"/>
		<text disable-output-escaping="yes">', CHR(167), ''''), '</text>
		<xsl:value-of select="HostIPAddr"/>
		<text disable-output-escaping="yes">', '</text>
		<xsl:value-of select="AltHostIPAddr"/>
		<text disable-output-escaping="yes">', </text>
		<xsl:choose>
			<xsl:when test="Port"><xsl:value-of select="Port"/></xsl:when>
			<xsl:otherwise>9003</xsl:otherwise>
		</xsl:choose>
		<text disable-output-escaping="yes"> FROM DUAL
</text>
	</xsl:template>
</xsl:stylesheet>
