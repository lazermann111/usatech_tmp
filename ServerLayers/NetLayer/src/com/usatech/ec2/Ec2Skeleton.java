package com.usatech.ec2;

import com.usatech.eportconnect.ECRequestHandler;
import com.usatech.layers.common.messagedata.*;
import simple.lang.InvalidValueException;
import simple.translator.Translator;
import simple.translator.TranslatorFactory;
import simple.app.ServiceException;
import com.usatech.eportconnect.ECSession;
import com.usatech.eportconnect.ECSessionManager;
import static com.usatech.eportconnect.ECAxisServlet.SOAP_PROTOCOL;

public class Ec2Skeleton {

	protected static Translator getTranslator() throws ServiceException {
		return TranslatorFactory.getDefaultFactory().getTranslator(null, null);
	}

	public ReplenishPlainResponse replenishPlain(ReplenishPlain replenishPlain) {
		MessageData_0216 requestData = new MessageData_0216();
		requestData.setProtocol(SOAP_PROTOCOL);
		requestData.setUsername(replenishPlain.getUsername());
		requestData.setPassword(replenishPlain.getPassword());
		requestData.setSerialNumber(replenishPlain.getSerialNumber());
		requestData.setTranId(replenishPlain.getTranId());
		requestData.setAmount(replenishPlain.getAmount());
		requestData.setCardData(replenishPlain.getCardData());
		requestData.setEntryTypeString(replenishPlain.getEntryType());
		requestData.setReplenishCardId(replenishPlain.getReplenishCardId());
		requestData.setReplenishConsumerId(replenishPlain.getReplenishConsumerId());
		requestData.setAttributes(replenishPlain.getAttributes());
		MessageData_030C responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.replenishPlain(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		com.usatech.ec2.xsd.EC2ReplenishResponse responseReturn = new com.usatech.ec2.xsd.EC2ReplenishResponse();
		responseReturn.setActionCode(responseData.getActionCode());
		responseReturn.setApprovedAmount(responseData.getApprovedAmount());
		responseReturn.setAttributes(responseData.getAttributes() == null ? "" : responseData.getAttributes());
		responseReturn.setBalanceAmount(responseData.getBalanceAmount());
		responseReturn.setCardId(responseData.getCardId());
		responseReturn.setCardType(responseData.getCardType() == null ? "" : responseData.getCardType());
		responseReturn.setConsumerId(responseData.getConsumerId());
		responseReturn.setNewPassword(responseData.getNewPassword() == null ? "" : responseData.getNewPassword());
		responseReturn.setNewTranId(responseData.getNewTranId());
		responseReturn.setNewUsername(responseData.getNewUsername() == null ? "" : responseData.getNewUsername());
		responseReturn.setReplenishBalanceAmount(responseData.getReplenishBalanceAmount());
		responseReturn.setReturnCode(responseData.getReturnCode());
		responseReturn.setReturnMessage(responseData.getReturnMessage() == null ? "" : responseData.getReturnMessage());
		responseReturn.setSerialNumber(responseData.getSerialNumber() == null ? "" : responseData.getSerialNumber());
		ReplenishPlainResponse responseMsg = new ReplenishPlainResponse();
		responseMsg.set_return(responseReturn);
		return responseMsg;
	}

	public ReplenishEncryptedResponse replenishEncrypted(ReplenishEncrypted replenishEncrypted) {
		MessageData_0217 requestData = new MessageData_0217();
		requestData.setProtocol(SOAP_PROTOCOL);
		requestData.setUsername(replenishEncrypted.getUsername());
		requestData.setPassword(replenishEncrypted.getPassword());
		requestData.setSerialNumber(replenishEncrypted.getSerialNumber());
		requestData.setTranId(replenishEncrypted.getTranId());
		requestData.setAmount(replenishEncrypted.getAmount());
		requestData.setCardReaderTypeInt(replenishEncrypted.getCardReaderType());
		requestData.setDecryptedCardDataLen(replenishEncrypted.getDecryptedCardDataLen());
		requestData.setEncryptedCardDataHex(replenishEncrypted.getEncryptedCardDataHex());
		requestData.setKsnHex(replenishEncrypted.getKsnHex());
		requestData.setEntryTypeString(replenishEncrypted.getEntryType());
		requestData.setReplenishCardId(replenishEncrypted.getReplenishCardId());
		requestData.setReplenishConsumerId(replenishEncrypted.getReplenishConsumerId());
		requestData.setAttributes(replenishEncrypted.getAttributes());
		MessageData_030C responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.replenishEncrypted(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		com.usatech.ec2.xsd.EC2ReplenishResponse responseReturn = new com.usatech.ec2.xsd.EC2ReplenishResponse();
		responseReturn.setActionCode(responseData.getActionCode());
		responseReturn.setApprovedAmount(responseData.getApprovedAmount());
		responseReturn.setAttributes(responseData.getAttributes() == null ? "" : responseData.getAttributes());
		responseReturn.setBalanceAmount(responseData.getBalanceAmount());
		responseReturn.setCardId(responseData.getCardId());
		responseReturn.setCardType(responseData.getCardType() == null ? "" : responseData.getCardType());
		responseReturn.setConsumerId(responseData.getConsumerId());
		responseReturn.setNewPassword(responseData.getNewPassword() == null ? "" : responseData.getNewPassword());
		responseReturn.setNewTranId(responseData.getNewTranId());
		responseReturn.setNewUsername(responseData.getNewUsername() == null ? "" : responseData.getNewUsername());
		responseReturn.setReplenishBalanceAmount(responseData.getReplenishBalanceAmount());
		responseReturn.setReturnCode(responseData.getReturnCode());
		responseReturn.setReturnMessage(responseData.getReturnMessage() == null ? "" : responseData.getReturnMessage());
		responseReturn.setSerialNumber(responseData.getSerialNumber() == null ? "" : responseData.getSerialNumber());
		ReplenishEncryptedResponse responseMsg = new ReplenishEncryptedResponse();
		responseMsg.set_return(responseReturn);
		return responseMsg;
	}

	public BatchResponse batch(Batch batch) {
		MessageData_0213 requestData = new MessageData_0213();
		requestData.setProtocol(SOAP_PROTOCOL);
		requestData.setUsername(batch.getUsername());
		requestData.setPassword(batch.getPassword());
		requestData.setSerialNumber(batch.getSerialNumber());
		requestData.setTranId(batch.getTranId());
		requestData.setAmount(batch.getAmount());
		requestData.setTranResult(batch.getTranResult());
		requestData.setTranDetails(batch.getTranDetails());
		requestData.setAttributes(batch.getAttributes());
		MessageData_030A responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.batch(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		com.usatech.ec2.xsd.EC2Response responseReturn = new com.usatech.ec2.xsd.EC2Response();
		responseReturn.setActionCode(responseData.getActionCode());
		responseReturn.setAttributes(responseData.getAttributes() == null ? "" : responseData.getAttributes());
		responseReturn.setNewPassword(responseData.getNewPassword() == null ? "" : responseData.getNewPassword());
		responseReturn.setNewTranId(responseData.getNewTranId());
		responseReturn.setNewUsername(responseData.getNewUsername() == null ? "" : responseData.getNewUsername());
		responseReturn.setReturnCode(responseData.getReturnCode());
		responseReturn.setReturnMessage(responseData.getReturnMessage() == null ? "" : responseData.getReturnMessage());
		responseReturn.setSerialNumber(responseData.getSerialNumber() == null ? "" : responseData.getSerialNumber());
		BatchResponse responseMsg = new BatchResponse();
		responseMsg.set_return(responseReturn);
		return responseMsg;
	}

	public GetCardInfoEncryptedResponse getCardInfoEncrypted(GetCardInfoEncrypted getCardInfoEncrypted) {
		MessageData_0215 requestData = new MessageData_0215();
		requestData.setProtocol(SOAP_PROTOCOL);
		requestData.setUsername(getCardInfoEncrypted.getUsername());
		requestData.setPassword(getCardInfoEncrypted.getPassword());
		requestData.setSerialNumber(getCardInfoEncrypted.getSerialNumber());
		requestData.setCardReaderTypeInt(getCardInfoEncrypted.getCardReaderType());
		requestData.setDecryptedCardDataLen(getCardInfoEncrypted.getDecryptedCardDataLen());
		requestData.setEncryptedCardDataHex(getCardInfoEncrypted.getEncryptedCardDataHex());
		requestData.setKsnHex(getCardInfoEncrypted.getKsnHex());
		requestData.setEntryTypeString(getCardInfoEncrypted.getEntryType());
		requestData.setAttributes(getCardInfoEncrypted.getAttributes());
		MessageData_030D responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.getCardInfoEncrypted(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		com.usatech.ec2.xsd.EC2CardInfo responseReturn = new com.usatech.ec2.xsd.EC2CardInfo();
		responseReturn.setActionCode(responseData.getActionCode());
		responseReturn.setAttributes(responseData.getAttributes() == null ? "" : responseData.getAttributes());
		responseReturn.setBalanceAmount(responseData.getBalanceAmount());
		responseReturn.setCardId(responseData.getCardId());
		responseReturn.setCardType(responseData.getCardType() == null ? "" : responseData.getCardType());
		responseReturn.setConsumerId(responseData.getConsumerId());
		responseReturn.setNewPassword(responseData.getNewPassword() == null ? "" : responseData.getNewPassword());
		responseReturn.setNewTranId(responseData.getNewTranId());
		responseReturn.setNewUsername(responseData.getNewUsername() == null ? "" : responseData.getNewUsername());
		responseReturn.setReturnCode(responseData.getReturnCode());
		responseReturn.setReturnMessage(responseData.getReturnMessage() == null ? "" : responseData.getReturnMessage());
		responseReturn.setSerialNumber(responseData.getSerialNumber() == null ? "" : responseData.getSerialNumber());
		GetCardInfoEncryptedResponse responseMsg = new GetCardInfoEncryptedResponse();
		responseMsg.set_return(responseReturn);
		return responseMsg;
	}

	public GetCardInfoPlainResponse getCardInfoPlain(GetCardInfoPlain getCardInfoPlain) {
		MessageData_0214 requestData = new MessageData_0214();
		requestData.setProtocol(SOAP_PROTOCOL);
		requestData.setUsername(getCardInfoPlain.getUsername());
		requestData.setPassword(getCardInfoPlain.getPassword());
		requestData.setSerialNumber(getCardInfoPlain.getSerialNumber());
		requestData.setCardData(getCardInfoPlain.getCardData());
		requestData.setEntryTypeString(getCardInfoPlain.getEntryType());
		requestData.setAttributes(getCardInfoPlain.getAttributes());
		MessageData_030D responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.getCardInfoPlain(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		com.usatech.ec2.xsd.EC2CardInfo responseReturn = new com.usatech.ec2.xsd.EC2CardInfo();
		responseReturn.setActionCode(responseData.getActionCode());
		responseReturn.setAttributes(responseData.getAttributes() == null ? "" : responseData.getAttributes());
		responseReturn.setBalanceAmount(responseData.getBalanceAmount());
		responseReturn.setCardId(responseData.getCardId());
		responseReturn.setCardType(responseData.getCardType() == null ? "" : responseData.getCardType());
		responseReturn.setConsumerId(responseData.getConsumerId());
		responseReturn.setNewPassword(responseData.getNewPassword() == null ? "" : responseData.getNewPassword());
		responseReturn.setNewTranId(responseData.getNewTranId());
		responseReturn.setNewUsername(responseData.getNewUsername() == null ? "" : responseData.getNewUsername());
		responseReturn.setReturnCode(responseData.getReturnCode());
		responseReturn.setReturnMessage(responseData.getReturnMessage() == null ? "" : responseData.getReturnMessage());
		responseReturn.setSerialNumber(responseData.getSerialNumber() == null ? "" : responseData.getSerialNumber());
		GetCardInfoPlainResponse responseMsg = new GetCardInfoPlainResponse();
		responseMsg.set_return(responseReturn);
		return responseMsg;
	}

	public TokenizeEncryptedResponse tokenizeEncrypted(TokenizeEncrypted tokenizeEncrypted) {
		MessageData_021F requestData = new MessageData_021F();
		requestData.setProtocol(SOAP_PROTOCOL);
		requestData.setUsername(tokenizeEncrypted.getUsername());
		requestData.setPassword(tokenizeEncrypted.getPassword());
		requestData.setSerialNumber(tokenizeEncrypted.getSerialNumber());
		requestData.setTranId(tokenizeEncrypted.getTranId());
		requestData.setCardReaderTypeInt(tokenizeEncrypted.getCardReaderType());
		requestData.setDecryptedCardDataLen(tokenizeEncrypted.getDecryptedCardDataLen());
		requestData.setEncryptedCardDataHex(tokenizeEncrypted.getEncryptedCardDataHex());
		requestData.setKsnHex(tokenizeEncrypted.getKsnHex());
		requestData.setBillingPostalCode(tokenizeEncrypted.getBillingPostalCode());
		requestData.setBillingAddress(tokenizeEncrypted.getBillingAddress());
		requestData.setAttributes(tokenizeEncrypted.getAttributes());
		MessageData_0310 responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.tokenizeEncrypted(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		com.usatech.ec2.xsd.EC2TokenResponse responseReturn = new com.usatech.ec2.xsd.EC2TokenResponse();
		responseReturn.setActionCode(responseData.getActionCode());
		responseReturn.setAttributes(responseData.getAttributes() == null ? "" : responseData.getAttributes());
		responseReturn.setCardId(responseData.getCardId());
		responseReturn.setCardType(responseData.getCardType() == null ? "" : responseData.getCardType());
		responseReturn.setConsumerId(responseData.getConsumerId());
		responseReturn.setNewPassword(responseData.getNewPassword() == null ? "" : responseData.getNewPassword());
		responseReturn.setNewTranId(responseData.getNewTranId());
		responseReturn.setNewUsername(responseData.getNewUsername() == null ? "" : responseData.getNewUsername());
		responseReturn.setReturnCode(responseData.getReturnCode());
		responseReturn.setReturnMessage(responseData.getReturnMessage() == null ? "" : responseData.getReturnMessage());
		responseReturn.setSerialNumber(responseData.getSerialNumber() == null ? "" : responseData.getSerialNumber());
		responseReturn.setTokenHex(responseData.getTokenHex() == null ? "" : responseData.getTokenHex());
		TokenizeEncryptedResponse responseMsg = new TokenizeEncryptedResponse();
		responseMsg.set_return(responseReturn);
		return responseMsg;
	}

	public TokenizePlainResponse tokenizePlain(TokenizePlain tokenizePlain) {
		MessageData_021E requestData = new MessageData_021E();
		requestData.setProtocol(SOAP_PROTOCOL);
		requestData.setUsername(tokenizePlain.getUsername());
		requestData.setPassword(tokenizePlain.getPassword());
		requestData.setSerialNumber(tokenizePlain.getSerialNumber());
		requestData.setTranId(tokenizePlain.getTranId());
		requestData.setCardNumber(tokenizePlain.getCardNumber());
		requestData.setExpirationDate(tokenizePlain.getExpirationDate());
		requestData.setSecurityCode(tokenizePlain.getSecurityCode());
		requestData.setCardHolder(tokenizePlain.getCardHolder());
		requestData.setBillingPostalCode(tokenizePlain.getBillingPostalCode());
		requestData.setBillingAddress(tokenizePlain.getBillingAddress());
		requestData.setAttributes(tokenizePlain.getAttributes());
		MessageData_0310 responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.tokenizePlain(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		com.usatech.ec2.xsd.EC2TokenResponse responseReturn = new com.usatech.ec2.xsd.EC2TokenResponse();
		responseReturn.setActionCode(responseData.getActionCode());
		responseReturn.setAttributes(responseData.getAttributes() == null ? "" : responseData.getAttributes());
		responseReturn.setCardId(responseData.getCardId());
		responseReturn.setCardType(responseData.getCardType() == null ? "" : responseData.getCardType());
		responseReturn.setConsumerId(responseData.getConsumerId());
		responseReturn.setNewPassword(responseData.getNewPassword() == null ? "" : responseData.getNewPassword());
		responseReturn.setNewTranId(responseData.getNewTranId());
		responseReturn.setNewUsername(responseData.getNewUsername() == null ? "" : responseData.getNewUsername());
		responseReturn.setReturnCode(responseData.getReturnCode());
		responseReturn.setReturnMessage(responseData.getReturnMessage() == null ? "" : responseData.getReturnMessage());
		responseReturn.setSerialNumber(responseData.getSerialNumber() == null ? "" : responseData.getSerialNumber());
		responseReturn.setTokenHex(responseData.getTokenHex() == null ? "" : responseData.getTokenHex());
		TokenizePlainResponse responseMsg = new TokenizePlainResponse();
		responseMsg.set_return(responseReturn);
		return responseMsg;
	}

	public ProcessUpdatesResponse processUpdates(ProcessUpdates processUpdates) {
		MessageData_0219 requestData = new MessageData_0219();
		requestData.setProtocol(SOAP_PROTOCOL);
		requestData.setUsername(processUpdates.getUsername());
		requestData.setPassword(processUpdates.getPassword());
		requestData.setSerialNumber(processUpdates.getSerialNumber());
		requestData.setUpdateStatus(processUpdates.getUpdateStatus());
		requestData.setProtocolVersion(processUpdates.getProtocolVersion());
		requestData.setAppType(processUpdates.getAppType());
		requestData.setAppVersion(processUpdates.getAppVersion());
		requestData.setAttributes(processUpdates.getAttributes());
		MessageData_030E responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.processUpdates(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				// if return code is RES_OK, then FileBlockInputStream will end the session
				if(responseData.getReturnCode() != com.usatech.ec.ECResponse.RES_OK)
					ECSessionManager.endSession(session);
			}
		}
		com.usatech.ec2.xsd.EC2ProcessUpdatesResponse responseReturn = new com.usatech.ec2.xsd.EC2ProcessUpdatesResponse();
		responseReturn.setActionCode(responseData.getActionCode());
		responseReturn.setAttributes(responseData.getAttributes() == null ? "" : responseData.getAttributes());
		responseReturn.setFileContent(responseData.getFileContent());
		responseReturn.setFileName(responseData.getFileName() == null ? "" : responseData.getFileName());
		responseReturn.setFileSize(responseData.getFileSize());
		responseReturn.setFileType(responseData.getFileType());
		responseReturn.setNewPassword(responseData.getNewPassword() == null ? "" : responseData.getNewPassword());
		responseReturn.setNewTranId(responseData.getNewTranId());
		responseReturn.setNewUsername(responseData.getNewUsername() == null ? "" : responseData.getNewUsername());
		responseReturn.setReturnCode(responseData.getReturnCode());
		responseReturn.setReturnMessage(responseData.getReturnMessage() == null ? "" : responseData.getReturnMessage());
		responseReturn.setSerialNumber(responseData.getSerialNumber() == null ? "" : responseData.getSerialNumber());
		ProcessUpdatesResponse responseMsg = new ProcessUpdatesResponse();
		responseMsg.set_return(responseReturn);
		return responseMsg;
	}

	public ReplenishCashResponse replenishCash(ReplenishCash replenishCash) {
		MessageData_0218 requestData = new MessageData_0218();
		requestData.setProtocol(SOAP_PROTOCOL);
		requestData.setUsername(replenishCash.getUsername());
		requestData.setPassword(replenishCash.getPassword());
		requestData.setSerialNumber(replenishCash.getSerialNumber());
		requestData.setTranId(replenishCash.getTranId());
		requestData.setAmount(replenishCash.getAmount());
		requestData.setReplenishCardId(replenishCash.getReplenishCardId());
		requestData.setReplenishConsumerId(replenishCash.getReplenishConsumerId());
		requestData.setAttributes(replenishCash.getAttributes());
		MessageData_030C responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.replenishCash(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		com.usatech.ec2.xsd.EC2ReplenishResponse responseReturn = new com.usatech.ec2.xsd.EC2ReplenishResponse();
		responseReturn.setActionCode(responseData.getActionCode());
		responseReturn.setApprovedAmount(responseData.getApprovedAmount());
		responseReturn.setAttributes(responseData.getAttributes() == null ? "" : responseData.getAttributes());
		responseReturn.setBalanceAmount(responseData.getBalanceAmount());
		responseReturn.setCardId(responseData.getCardId());
		responseReturn.setCardType(responseData.getCardType() == null ? "" : responseData.getCardType());
		responseReturn.setConsumerId(responseData.getConsumerId());
		responseReturn.setNewPassword(responseData.getNewPassword() == null ? "" : responseData.getNewPassword());
		responseReturn.setNewTranId(responseData.getNewTranId());
		responseReturn.setNewUsername(responseData.getNewUsername() == null ? "" : responseData.getNewUsername());
		responseReturn.setReplenishBalanceAmount(responseData.getReplenishBalanceAmount());
		responseReturn.setReturnCode(responseData.getReturnCode());
		responseReturn.setReturnMessage(responseData.getReturnMessage() == null ? "" : responseData.getReturnMessage());
		responseReturn.setSerialNumber(responseData.getSerialNumber() == null ? "" : responseData.getSerialNumber());
		ReplenishCashResponse responseMsg = new ReplenishCashResponse();
		responseMsg.set_return(responseReturn);
		return responseMsg;
	}

	public GetCardIdPlainResponse getCardIdPlain(GetCardIdPlain getCardIdPlain) {
		MessageData_021C requestData = new MessageData_021C();
		requestData.setProtocol(SOAP_PROTOCOL);
		requestData.setUsername(getCardIdPlain.getUsername());
		requestData.setPassword(getCardIdPlain.getPassword());
		requestData.setSerialNumber(getCardIdPlain.getSerialNumber());
		requestData.setCardData(getCardIdPlain.getCardData());
		requestData.setEntryTypeString(getCardIdPlain.getEntryType());
		requestData.setAttributes(getCardIdPlain.getAttributes());
		MessageData_030F responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.getCardIdPlain(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		com.usatech.ec2.xsd.EC2CardId responseReturn = new com.usatech.ec2.xsd.EC2CardId();
		responseReturn.setActionCode(responseData.getActionCode());
		responseReturn.setAttributes(responseData.getAttributes() == null ? "" : responseData.getAttributes());
		responseReturn.setCardId(responseData.getCardId());
		responseReturn.setNewPassword(responseData.getNewPassword() == null ? "" : responseData.getNewPassword());
		responseReturn.setNewTranId(responseData.getNewTranId());
		responseReturn.setNewUsername(responseData.getNewUsername() == null ? "" : responseData.getNewUsername());
		responseReturn.setReturnCode(responseData.getReturnCode());
		responseReturn.setReturnMessage(responseData.getReturnMessage() == null ? "" : responseData.getReturnMessage());
		responseReturn.setSerialNumber(responseData.getSerialNumber() == null ? "" : responseData.getSerialNumber());
		GetCardIdPlainResponse responseMsg = new GetCardIdPlainResponse();
		responseMsg.set_return(responseReturn);
		return responseMsg;
	}

	public GetCardIdEncryptedResponse getCardIdEncrypted(GetCardIdEncrypted getCardIdEncrypted) {
		MessageData_021D requestData = new MessageData_021D();
		requestData.setProtocol(SOAP_PROTOCOL);
		requestData.setUsername(getCardIdEncrypted.getUsername());
		requestData.setPassword(getCardIdEncrypted.getPassword());
		requestData.setSerialNumber(getCardIdEncrypted.getSerialNumber());
		requestData.setCardReaderTypeInt(getCardIdEncrypted.getCardReaderType());
		requestData.setDecryptedCardDataLen(getCardIdEncrypted.getDecryptedCardDataLen());
		requestData.setEncryptedCardDataHex(getCardIdEncrypted.getEncryptedCardDataHex());
		requestData.setKsnHex(getCardIdEncrypted.getKsnHex());
		requestData.setEntryTypeString(getCardIdEncrypted.getEntryType());
		requestData.setAttributes(getCardIdEncrypted.getAttributes());
		MessageData_030F responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.getCardIdEncrypted(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		com.usatech.ec2.xsd.EC2CardId responseReturn = new com.usatech.ec2.xsd.EC2CardId();
		responseReturn.setActionCode(responseData.getActionCode());
		responseReturn.setAttributes(responseData.getAttributes() == null ? "" : responseData.getAttributes());
		responseReturn.setCardId(responseData.getCardId());
		responseReturn.setNewPassword(responseData.getNewPassword() == null ? "" : responseData.getNewPassword());
		responseReturn.setNewTranId(responseData.getNewTranId());
		responseReturn.setNewUsername(responseData.getNewUsername() == null ? "" : responseData.getNewUsername());
		responseReturn.setReturnCode(responseData.getReturnCode());
		responseReturn.setReturnMessage(responseData.getReturnMessage() == null ? "" : responseData.getReturnMessage());
		responseReturn.setSerialNumber(responseData.getSerialNumber() == null ? "" : responseData.getSerialNumber());
		GetCardIdEncryptedResponse responseMsg = new GetCardIdEncryptedResponse();
		responseMsg.set_return(responseReturn);
		return responseMsg;
	}

	public CashResponse cash(Cash cash) {
		MessageData_0212 requestData = new MessageData_0212();
		requestData.setProtocol(SOAP_PROTOCOL);
		requestData.setUsername(cash.getUsername());
		requestData.setPassword(cash.getPassword());
		requestData.setSerialNumber(cash.getSerialNumber());
		requestData.setTranId(cash.getTranId());
		requestData.setAmount(cash.getAmount());
		requestData.setTranUTCTimeMs(cash.getTranUTCTimeMs());
		requestData.setTranUTCOffsetMs(cash.getTranUTCOffsetMs());
		requestData.setTranResult(cash.getTranResult());
		requestData.setTranDetails(cash.getTranDetails());
		requestData.setAttributes(cash.getAttributes());
		MessageData_030A responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.cash(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		com.usatech.ec2.xsd.EC2Response responseReturn = new com.usatech.ec2.xsd.EC2Response();
		responseReturn.setActionCode(responseData.getActionCode());
		responseReturn.setAttributes(responseData.getAttributes() == null ? "" : responseData.getAttributes());
		responseReturn.setNewPassword(responseData.getNewPassword() == null ? "" : responseData.getNewPassword());
		responseReturn.setNewTranId(responseData.getNewTranId());
		responseReturn.setNewUsername(responseData.getNewUsername() == null ? "" : responseData.getNewUsername());
		responseReturn.setReturnCode(responseData.getReturnCode());
		responseReturn.setReturnMessage(responseData.getReturnMessage() == null ? "" : responseData.getReturnMessage());
		responseReturn.setSerialNumber(responseData.getSerialNumber() == null ? "" : responseData.getSerialNumber());
		CashResponse responseMsg = new CashResponse();
		responseMsg.set_return(responseReturn);
		return responseMsg;
	}

	public ChargePlainResponse chargePlain(ChargePlain chargePlain) {
		MessageData_020E requestData = new MessageData_020E();
		requestData.setProtocol(SOAP_PROTOCOL);
		requestData.setUsername(chargePlain.getUsername());
		requestData.setPassword(chargePlain.getPassword());
		requestData.setSerialNumber(chargePlain.getSerialNumber());
		requestData.setTranId(chargePlain.getTranId());
		requestData.setAmount(chargePlain.getAmount());
		requestData.setCardData(chargePlain.getCardData());
		requestData.setEntryTypeString(chargePlain.getEntryType());
		requestData.setTranResult(chargePlain.getTranResult());
		requestData.setTranDetails(chargePlain.getTranDetails());
		requestData.setAttributes(chargePlain.getAttributes());
		MessageData_030B responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.chargePlain(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		com.usatech.ec2.xsd.EC2AuthResponse responseReturn = new com.usatech.ec2.xsd.EC2AuthResponse();
		responseReturn.setActionCode(responseData.getActionCode());
		responseReturn.setApprovedAmount(responseData.getApprovedAmount());
		responseReturn.setAttributes(responseData.getAttributes() == null ? "" : responseData.getAttributes());
		responseReturn.setBalanceAmount(responseData.getBalanceAmount());
		responseReturn.setCardId(responseData.getCardId());
		responseReturn.setCardType(responseData.getCardType() == null ? "" : responseData.getCardType());
		responseReturn.setConsumerId(responseData.getConsumerId());
		responseReturn.setNewPassword(responseData.getNewPassword() == null ? "" : responseData.getNewPassword());
		responseReturn.setNewTranId(responseData.getNewTranId());
		responseReturn.setNewUsername(responseData.getNewUsername() == null ? "" : responseData.getNewUsername());
		responseReturn.setReturnCode(responseData.getReturnCode());
		responseReturn.setReturnMessage(responseData.getReturnMessage() == null ? "" : responseData.getReturnMessage());
		responseReturn.setSerialNumber(responseData.getSerialNumber() == null ? "" : responseData.getSerialNumber());
		ChargePlainResponse responseMsg = new ChargePlainResponse();
		responseMsg.set_return(responseReturn);
		return responseMsg;
	}

	public ChargeEncryptedResponse chargeEncrypted(ChargeEncrypted chargeEncrypted) {
		MessageData_020F requestData = new MessageData_020F();
		requestData.setProtocol(SOAP_PROTOCOL);
		requestData.setUsername(chargeEncrypted.getUsername());
		requestData.setPassword(chargeEncrypted.getPassword());
		requestData.setSerialNumber(chargeEncrypted.getSerialNumber());
		requestData.setTranId(chargeEncrypted.getTranId());
		requestData.setAmount(chargeEncrypted.getAmount());
		requestData.setCardReaderTypeInt(chargeEncrypted.getCardReaderType());
		requestData.setDecryptedCardDataLen(chargeEncrypted.getDecryptedCardDataLen());
		requestData.setEncryptedCardDataHex(chargeEncrypted.getEncryptedCardDataHex());
		requestData.setKsnHex(chargeEncrypted.getKsnHex());
		requestData.setEntryTypeString(chargeEncrypted.getEntryType());
		requestData.setTranResult(chargeEncrypted.getTranResult());
		requestData.setTranDetails(chargeEncrypted.getTranDetails());
		requestData.setAttributes(chargeEncrypted.getAttributes());
		MessageData_030B responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.chargeEncrypted(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		com.usatech.ec2.xsd.EC2AuthResponse responseReturn = new com.usatech.ec2.xsd.EC2AuthResponse();
		responseReturn.setActionCode(responseData.getActionCode());
		responseReturn.setApprovedAmount(responseData.getApprovedAmount());
		responseReturn.setAttributes(responseData.getAttributes() == null ? "" : responseData.getAttributes());
		responseReturn.setBalanceAmount(responseData.getBalanceAmount());
		responseReturn.setCardId(responseData.getCardId());
		responseReturn.setCardType(responseData.getCardType() == null ? "" : responseData.getCardType());
		responseReturn.setConsumerId(responseData.getConsumerId());
		responseReturn.setNewPassword(responseData.getNewPassword() == null ? "" : responseData.getNewPassword());
		responseReturn.setNewTranId(responseData.getNewTranId());
		responseReturn.setNewUsername(responseData.getNewUsername() == null ? "" : responseData.getNewUsername());
		responseReturn.setReturnCode(responseData.getReturnCode());
		responseReturn.setReturnMessage(responseData.getReturnMessage() == null ? "" : responseData.getReturnMessage());
		responseReturn.setSerialNumber(responseData.getSerialNumber() == null ? "" : responseData.getSerialNumber());
		ChargeEncryptedResponse responseMsg = new ChargeEncryptedResponse();
		responseMsg.set_return(responseReturn);
		return responseMsg;
	}

	public AuthEncryptedResponse authEncrypted(AuthEncrypted authEncrypted) {
		MessageData_020B requestData = new MessageData_020B();
		requestData.setProtocol(SOAP_PROTOCOL);
		requestData.setUsername(authEncrypted.getUsername());
		requestData.setPassword(authEncrypted.getPassword());
		requestData.setSerialNumber(authEncrypted.getSerialNumber());
		requestData.setTranId(authEncrypted.getTranId());
		requestData.setAmount(authEncrypted.getAmount());
		requestData.setCardReaderTypeInt(authEncrypted.getCardReaderType());
		requestData.setDecryptedCardDataLen(authEncrypted.getDecryptedCardDataLen());
		requestData.setEncryptedCardDataHex(authEncrypted.getEncryptedCardDataHex());
		requestData.setKsnHex(authEncrypted.getKsnHex());
		requestData.setEntryTypeString(authEncrypted.getEntryType());
		requestData.setAttributes(authEncrypted.getAttributes());
		MessageData_030B responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.authEncrypted(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		com.usatech.ec2.xsd.EC2AuthResponse responseReturn = new com.usatech.ec2.xsd.EC2AuthResponse();
		responseReturn.setActionCode(responseData.getActionCode());
		responseReturn.setApprovedAmount(responseData.getApprovedAmount());
		responseReturn.setAttributes(responseData.getAttributes() == null ? "" : responseData.getAttributes());
		responseReturn.setBalanceAmount(responseData.getBalanceAmount());
		responseReturn.setCardId(responseData.getCardId());
		responseReturn.setCardType(responseData.getCardType() == null ? "" : responseData.getCardType());
		responseReturn.setConsumerId(responseData.getConsumerId());
		responseReturn.setNewPassword(responseData.getNewPassword() == null ? "" : responseData.getNewPassword());
		responseReturn.setNewTranId(responseData.getNewTranId());
		responseReturn.setNewUsername(responseData.getNewUsername() == null ? "" : responseData.getNewUsername());
		responseReturn.setReturnCode(responseData.getReturnCode());
		responseReturn.setReturnMessage(responseData.getReturnMessage() == null ? "" : responseData.getReturnMessage());
		responseReturn.setSerialNumber(responseData.getSerialNumber() == null ? "" : responseData.getSerialNumber());
		AuthEncryptedResponse responseMsg = new AuthEncryptedResponse();
		responseMsg.set_return(responseReturn);
		return responseMsg;
	}

	public UploadDeviceInfoResponse uploadDeviceInfo(UploadDeviceInfo uploadDeviceInfo) {
		MessageData_021A requestData = new MessageData_021A();
		requestData.setProtocol(SOAP_PROTOCOL);
		requestData.setUsername(uploadDeviceInfo.getUsername());
		requestData.setPassword(uploadDeviceInfo.getPassword());
		requestData.setSerialNumber(uploadDeviceInfo.getSerialNumber());
		requestData.setAttributes(uploadDeviceInfo.getAttributes());
		MessageData_030A responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.uploadDeviceInfo(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		com.usatech.ec2.xsd.EC2Response responseReturn = new com.usatech.ec2.xsd.EC2Response();
		responseReturn.setActionCode(responseData.getActionCode());
		responseReturn.setAttributes(responseData.getAttributes() == null ? "" : responseData.getAttributes());
		responseReturn.setNewPassword(responseData.getNewPassword() == null ? "" : responseData.getNewPassword());
		responseReturn.setNewTranId(responseData.getNewTranId());
		responseReturn.setNewUsername(responseData.getNewUsername() == null ? "" : responseData.getNewUsername());
		responseReturn.setReturnCode(responseData.getReturnCode());
		responseReturn.setReturnMessage(responseData.getReturnMessage() == null ? "" : responseData.getReturnMessage());
		responseReturn.setSerialNumber(responseData.getSerialNumber() == null ? "" : responseData.getSerialNumber());
		UploadDeviceInfoResponse responseMsg = new UploadDeviceInfoResponse();
		responseMsg.set_return(responseReturn);
		return responseMsg;
	}

	public UploadFileResponse uploadFile(UploadFile uploadFile) {
		MessageData_021B requestData = new MessageData_021B();
		requestData.setProtocol(SOAP_PROTOCOL);
		requestData.setUsername(uploadFile.getUsername());
		requestData.setPassword(uploadFile.getPassword());
		requestData.setSerialNumber(uploadFile.getSerialNumber());
		requestData.setFileName(uploadFile.getFileName());
		requestData.setFileType(uploadFile.getFileType());
		requestData.setFileSize(uploadFile.getFileSize());
		requestData.setFileContent(uploadFile.getFileContent());
		requestData.setAttributes(uploadFile.getAttributes());
		MessageData_030A responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.uploadFile(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		com.usatech.ec2.xsd.EC2Response responseReturn = new com.usatech.ec2.xsd.EC2Response();
		responseReturn.setActionCode(responseData.getActionCode());
		responseReturn.setAttributes(responseData.getAttributes() == null ? "" : responseData.getAttributes());
		responseReturn.setNewPassword(responseData.getNewPassword() == null ? "" : responseData.getNewPassword());
		responseReturn.setNewTranId(responseData.getNewTranId());
		responseReturn.setNewUsername(responseData.getNewUsername() == null ? "" : responseData.getNewUsername());
		responseReturn.setReturnCode(responseData.getReturnCode());
		responseReturn.setReturnMessage(responseData.getReturnMessage() == null ? "" : responseData.getReturnMessage());
		responseReturn.setSerialNumber(responseData.getSerialNumber() == null ? "" : responseData.getSerialNumber());
		UploadFileResponse responseMsg = new UploadFileResponse();
		responseMsg.set_return(responseReturn);
		return responseMsg;
	}

	public UntokenizeResponse untokenize(Untokenize untokenize) {
		MessageData_0220 requestData = new MessageData_0220();
		requestData.setProtocol(SOAP_PROTOCOL);
		requestData.setUsername(untokenize.getUsername());
		requestData.setPassword(untokenize.getPassword());
		requestData.setSerialNumber(untokenize.getSerialNumber());
		requestData.setCardId(untokenize.getCardId());
		requestData.setTokenHex(untokenize.getTokenHex());
		requestData.setAttributes(untokenize.getAttributes());
		MessageData_030A responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.untokenize(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		com.usatech.ec2.xsd.EC2Response responseReturn = new com.usatech.ec2.xsd.EC2Response();
		responseReturn.setActionCode(responseData.getActionCode());
		responseReturn.setAttributes(responseData.getAttributes() == null ? "" : responseData.getAttributes());
		responseReturn.setNewPassword(responseData.getNewPassword() == null ? "" : responseData.getNewPassword());
		responseReturn.setNewTranId(responseData.getNewTranId());
		responseReturn.setNewUsername(responseData.getNewUsername() == null ? "" : responseData.getNewUsername());
		responseReturn.setReturnCode(responseData.getReturnCode());
		responseReturn.setReturnMessage(responseData.getReturnMessage() == null ? "" : responseData.getReturnMessage());
		responseReturn.setSerialNumber(responseData.getSerialNumber() == null ? "" : responseData.getSerialNumber());
		UntokenizeResponse responseMsg = new UntokenizeResponse();
		responseMsg.set_return(responseReturn);
		return responseMsg;
	}

	public AuthPlainResponse authPlain(AuthPlain authPlain) {
		MessageData_020A requestData = new MessageData_020A();
		requestData.setProtocol(SOAP_PROTOCOL);
		requestData.setUsername(authPlain.getUsername());
		requestData.setPassword(authPlain.getPassword());
		requestData.setSerialNumber(authPlain.getSerialNumber());
		requestData.setTranId(authPlain.getTranId());
		requestData.setAmount(authPlain.getAmount());
		requestData.setCardData(authPlain.getCardData());
		requestData.setEntryTypeString(authPlain.getEntryType());
		requestData.setAttributes(authPlain.getAttributes());
		MessageData_030B responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.authPlain(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		com.usatech.ec2.xsd.EC2AuthResponse responseReturn = new com.usatech.ec2.xsd.EC2AuthResponse();
		responseReturn.setActionCode(responseData.getActionCode());
		responseReturn.setApprovedAmount(responseData.getApprovedAmount());
		responseReturn.setAttributes(responseData.getAttributes() == null ? "" : responseData.getAttributes());
		responseReturn.setBalanceAmount(responseData.getBalanceAmount());
		responseReturn.setCardId(responseData.getCardId());
		responseReturn.setCardType(responseData.getCardType() == null ? "" : responseData.getCardType());
		responseReturn.setConsumerId(responseData.getConsumerId());
		responseReturn.setNewPassword(responseData.getNewPassword() == null ? "" : responseData.getNewPassword());
		responseReturn.setNewTranId(responseData.getNewTranId());
		responseReturn.setNewUsername(responseData.getNewUsername() == null ? "" : responseData.getNewUsername());
		responseReturn.setReturnCode(responseData.getReturnCode());
		responseReturn.setReturnMessage(responseData.getReturnMessage() == null ? "" : responseData.getReturnMessage());
		responseReturn.setSerialNumber(responseData.getSerialNumber() == null ? "" : responseData.getSerialNumber());
		AuthPlainResponse responseMsg = new AuthPlainResponse();
		responseMsg.set_return(responseReturn);
		return responseMsg;
	}

	public RetokenizeResponse retokenize(Retokenize retokenize) {
		MessageData_0221 requestData = new MessageData_0221();
		requestData.setProtocol(SOAP_PROTOCOL);
		requestData.setUsername(retokenize.getUsername());
		requestData.setPassword(retokenize.getPassword());
		requestData.setSerialNumber(retokenize.getSerialNumber());
		requestData.setCardId(retokenize.getCardId());
		requestData.setSecurityCode(retokenize.getSecurityCode());
		requestData.setCardHolder(retokenize.getCardHolder());
		requestData.setBillingPostalCode(retokenize.getBillingPostalCode());
		requestData.setBillingAddress(retokenize.getBillingAddress());
		requestData.setAttributes(retokenize.getAttributes());
		MessageData_0310 responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.retokenize(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		com.usatech.ec2.xsd.EC2TokenResponse responseReturn = new com.usatech.ec2.xsd.EC2TokenResponse();
		responseReturn.setActionCode(responseData.getActionCode());
		responseReturn.setAttributes(responseData.getAttributes() == null ? "" : responseData.getAttributes());
		responseReturn.setCardId(responseData.getCardId());
		responseReturn.setCardType(responseData.getCardType() == null ? "" : responseData.getCardType());
		responseReturn.setConsumerId(responseData.getConsumerId());
		responseReturn.setNewPassword(responseData.getNewPassword() == null ? "" : responseData.getNewPassword());
		responseReturn.setNewTranId(responseData.getNewTranId());
		responseReturn.setNewUsername(responseData.getNewUsername() == null ? "" : responseData.getNewUsername());
		responseReturn.setReturnCode(responseData.getReturnCode());
		responseReturn.setReturnMessage(responseData.getReturnMessage() == null ? "" : responseData.getReturnMessage());
		responseReturn.setSerialNumber(responseData.getSerialNumber() == null ? "" : responseData.getSerialNumber());
		responseReturn.setTokenHex(responseData.getTokenHex() == null ? "" : responseData.getTokenHex());
		RetokenizeResponse responseMsg = new RetokenizeResponse();
		responseMsg.set_return(responseReturn);
		return responseMsg;
	}

}
