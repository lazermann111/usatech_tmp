
/**
 * Ec2MessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
        package com.usatech.ec2;

        /**
        *  Ec2MessageReceiverInOut message receiver
        */

        public class Ec2MessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver{


        public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext)
        throws org.apache.axis2.AxisFault{

        try {

        // get the implementation class for the Web Service
        Object obj = getTheImplementationObject(msgContext);

        Ec2Skeleton skel = (Ec2Skeleton)obj;
        //Out Envelop
        org.apache.axiom.soap.SOAPEnvelope envelope = null;
        //Find the axisOperation that has been set by the Dispatch phase.
        org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();
        if (op == null) {
        throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
        }

        java.lang.String methodName;
        if((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJavaIdentifier(op.getName().getLocalPart())) != null)){


        

            if("getCardIdEncrypted".equals(methodName)){
                
                com.usatech.ec2.GetCardIdEncryptedResponse getCardIdEncryptedResponse81 = null;
	                        com.usatech.ec2.GetCardIdEncrypted wrappedParam =
                                                             (com.usatech.ec2.GetCardIdEncrypted)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ec2.GetCardIdEncrypted.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getCardIdEncryptedResponse81 =
                                                   
                                                   
                                                         skel.getCardIdEncrypted(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getCardIdEncryptedResponse81, false, new javax.xml.namespace.QName("urn:ec2.usatech.com",
                                                    "getCardIdEncrypted"));
                                    } else 

            if("uploadDeviceInfo".equals(methodName)){
                
                com.usatech.ec2.UploadDeviceInfoResponse uploadDeviceInfoResponse83 = null;
	                        com.usatech.ec2.UploadDeviceInfo wrappedParam =
                                                             (com.usatech.ec2.UploadDeviceInfo)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ec2.UploadDeviceInfo.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               uploadDeviceInfoResponse83 =
                                                   
                                                   
                                                         skel.uploadDeviceInfo(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), uploadDeviceInfoResponse83, false, new javax.xml.namespace.QName("urn:ec2.usatech.com",
                                                    "uploadDeviceInfo"));
                                    } else 

            if("processUpdates".equals(methodName)){
                
                com.usatech.ec2.ProcessUpdatesResponse processUpdatesResponse85 = null;
	                        com.usatech.ec2.ProcessUpdates wrappedParam =
                                                             (com.usatech.ec2.ProcessUpdates)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ec2.ProcessUpdates.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               processUpdatesResponse85 =
                                                   
                                                   
                                                         skel.processUpdates(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), processUpdatesResponse85, false, new javax.xml.namespace.QName("urn:ec2.usatech.com",
                                                    "processUpdates"));
                                    } else 

            if("tokenizePlain".equals(methodName)){
                
                com.usatech.ec2.TokenizePlainResponse tokenizePlainResponse87 = null;
	                        com.usatech.ec2.TokenizePlain wrappedParam =
                                                             (com.usatech.ec2.TokenizePlain)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ec2.TokenizePlain.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               tokenizePlainResponse87 =
                                                   
                                                   
                                                         skel.tokenizePlain(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), tokenizePlainResponse87, false, new javax.xml.namespace.QName("urn:ec2.usatech.com",
                                                    "tokenizePlain"));
                                    } else 

            if("batch".equals(methodName)){
                
                com.usatech.ec2.BatchResponse batchResponse89 = null;
	                        com.usatech.ec2.Batch wrappedParam =
                                                             (com.usatech.ec2.Batch)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ec2.Batch.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               batchResponse89 =
                                                   
                                                   
                                                         skel.batch(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), batchResponse89, false, new javax.xml.namespace.QName("urn:ec2.usatech.com",
                                                    "batch"));
                                    } else 

            if("retokenize".equals(methodName)){
                
                com.usatech.ec2.RetokenizeResponse retokenizeResponse91 = null;
	                        com.usatech.ec2.Retokenize wrappedParam =
                                                             (com.usatech.ec2.Retokenize)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ec2.Retokenize.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               retokenizeResponse91 =
                                                   
                                                   
                                                         skel.retokenize(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), retokenizeResponse91, false, new javax.xml.namespace.QName("urn:ec2.usatech.com",
                                                    "retokenize"));
                                    } else 

            if("replenishCash".equals(methodName)){
                
                com.usatech.ec2.ReplenishCashResponse replenishCashResponse93 = null;
	                        com.usatech.ec2.ReplenishCash wrappedParam =
                                                             (com.usatech.ec2.ReplenishCash)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ec2.ReplenishCash.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               replenishCashResponse93 =
                                                   
                                                   
                                                         skel.replenishCash(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), replenishCashResponse93, false, new javax.xml.namespace.QName("urn:ec2.usatech.com",
                                                    "replenishCash"));
                                    } else 

            if("getCardIdPlain".equals(methodName)){
                
                com.usatech.ec2.GetCardIdPlainResponse getCardIdPlainResponse95 = null;
	                        com.usatech.ec2.GetCardIdPlain wrappedParam =
                                                             (com.usatech.ec2.GetCardIdPlain)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ec2.GetCardIdPlain.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getCardIdPlainResponse95 =
                                                   
                                                   
                                                         skel.getCardIdPlain(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getCardIdPlainResponse95, false, new javax.xml.namespace.QName("urn:ec2.usatech.com",
                                                    "getCardIdPlain"));
                                    } else 

            if("chargePlain".equals(methodName)){
                
                com.usatech.ec2.ChargePlainResponse chargePlainResponse97 = null;
	                        com.usatech.ec2.ChargePlain wrappedParam =
                                                             (com.usatech.ec2.ChargePlain)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ec2.ChargePlain.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               chargePlainResponse97 =
                                                   
                                                   
                                                         skel.chargePlain(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), chargePlainResponse97, false, new javax.xml.namespace.QName("urn:ec2.usatech.com",
                                                    "chargePlain"));
                                    } else 

            if("replenishEncrypted".equals(methodName)){
                
                com.usatech.ec2.ReplenishEncryptedResponse replenishEncryptedResponse99 = null;
	                        com.usatech.ec2.ReplenishEncrypted wrappedParam =
                                                             (com.usatech.ec2.ReplenishEncrypted)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ec2.ReplenishEncrypted.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               replenishEncryptedResponse99 =
                                                   
                                                   
                                                         skel.replenishEncrypted(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), replenishEncryptedResponse99, false, new javax.xml.namespace.QName("urn:ec2.usatech.com",
                                                    "replenishEncrypted"));
                                    } else 

            if("chargeEncrypted".equals(methodName)){
                
                com.usatech.ec2.ChargeEncryptedResponse chargeEncryptedResponse101 = null;
	                        com.usatech.ec2.ChargeEncrypted wrappedParam =
                                                             (com.usatech.ec2.ChargeEncrypted)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ec2.ChargeEncrypted.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               chargeEncryptedResponse101 =
                                                   
                                                   
                                                         skel.chargeEncrypted(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), chargeEncryptedResponse101, false, new javax.xml.namespace.QName("urn:ec2.usatech.com",
                                                    "chargeEncrypted"));
                                    } else 

            if("uploadFile".equals(methodName)){
                
                com.usatech.ec2.UploadFileResponse uploadFileResponse103 = null;
	                        com.usatech.ec2.UploadFile wrappedParam =
                                                             (com.usatech.ec2.UploadFile)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ec2.UploadFile.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               uploadFileResponse103 =
                                                   
                                                   
                                                         skel.uploadFile(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), uploadFileResponse103, false, new javax.xml.namespace.QName("urn:ec2.usatech.com",
                                                    "uploadFile"));
                                    } else 

            if("replenishPlain".equals(methodName)){
                
                com.usatech.ec2.ReplenishPlainResponse replenishPlainResponse105 = null;
	                        com.usatech.ec2.ReplenishPlain wrappedParam =
                                                             (com.usatech.ec2.ReplenishPlain)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ec2.ReplenishPlain.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               replenishPlainResponse105 =
                                                   
                                                   
                                                         skel.replenishPlain(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), replenishPlainResponse105, false, new javax.xml.namespace.QName("urn:ec2.usatech.com",
                                                    "replenishPlain"));
                                    } else 

            if("getCardInfoEncrypted".equals(methodName)){
                
                com.usatech.ec2.GetCardInfoEncryptedResponse getCardInfoEncryptedResponse107 = null;
	                        com.usatech.ec2.GetCardInfoEncrypted wrappedParam =
                                                             (com.usatech.ec2.GetCardInfoEncrypted)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ec2.GetCardInfoEncrypted.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getCardInfoEncryptedResponse107 =
                                                   
                                                   
                                                         skel.getCardInfoEncrypted(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getCardInfoEncryptedResponse107, false, new javax.xml.namespace.QName("urn:ec2.usatech.com",
                                                    "getCardInfoEncrypted"));
                                    } else 

            if("authPlain".equals(methodName)){
                
                com.usatech.ec2.AuthPlainResponse authPlainResponse109 = null;
	                        com.usatech.ec2.AuthPlain wrappedParam =
                                                             (com.usatech.ec2.AuthPlain)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ec2.AuthPlain.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               authPlainResponse109 =
                                                   
                                                   
                                                         skel.authPlain(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), authPlainResponse109, false, new javax.xml.namespace.QName("urn:ec2.usatech.com",
                                                    "authPlain"));
                                    } else 

            if("cash".equals(methodName)){
                
                com.usatech.ec2.CashResponse cashResponse111 = null;
	                        com.usatech.ec2.Cash wrappedParam =
                                                             (com.usatech.ec2.Cash)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ec2.Cash.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               cashResponse111 =
                                                   
                                                   
                                                         skel.cash(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), cashResponse111, false, new javax.xml.namespace.QName("urn:ec2.usatech.com",
                                                    "cash"));
                                    } else 

            if("tokenizeEncrypted".equals(methodName)){
                
                com.usatech.ec2.TokenizeEncryptedResponse tokenizeEncryptedResponse113 = null;
	                        com.usatech.ec2.TokenizeEncrypted wrappedParam =
                                                             (com.usatech.ec2.TokenizeEncrypted)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ec2.TokenizeEncrypted.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               tokenizeEncryptedResponse113 =
                                                   
                                                   
                                                         skel.tokenizeEncrypted(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), tokenizeEncryptedResponse113, false, new javax.xml.namespace.QName("urn:ec2.usatech.com",
                                                    "tokenizeEncrypted"));
                                    } else 

            if("untokenize".equals(methodName)){
                
                com.usatech.ec2.UntokenizeResponse untokenizeResponse115 = null;
	                        com.usatech.ec2.Untokenize wrappedParam =
                                                             (com.usatech.ec2.Untokenize)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ec2.Untokenize.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               untokenizeResponse115 =
                                                   
                                                   
                                                         skel.untokenize(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), untokenizeResponse115, false, new javax.xml.namespace.QName("urn:ec2.usatech.com",
                                                    "untokenize"));
                                    } else 

            if("authEncrypted".equals(methodName)){
                
                com.usatech.ec2.AuthEncryptedResponse authEncryptedResponse117 = null;
	                        com.usatech.ec2.AuthEncrypted wrappedParam =
                                                             (com.usatech.ec2.AuthEncrypted)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ec2.AuthEncrypted.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               authEncryptedResponse117 =
                                                   
                                                   
                                                         skel.authEncrypted(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), authEncryptedResponse117, false, new javax.xml.namespace.QName("urn:ec2.usatech.com",
                                                    "authEncrypted"));
                                    } else 

            if("getCardInfoPlain".equals(methodName)){
                
                com.usatech.ec2.GetCardInfoPlainResponse getCardInfoPlainResponse119 = null;
	                        com.usatech.ec2.GetCardInfoPlain wrappedParam =
                                                             (com.usatech.ec2.GetCardInfoPlain)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ec2.GetCardInfoPlain.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getCardInfoPlainResponse119 =
                                                   
                                                   
                                                         skel.getCardInfoPlain(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getCardInfoPlainResponse119, false, new javax.xml.namespace.QName("urn:ec2.usatech.com",
                                                    "getCardInfoPlain"));
                                    
            } else {
              throw new java.lang.RuntimeException("method not found");
            }
        

        newMsgContext.setEnvelope(envelope);
        }
        }
        catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
        }
        
        //
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.GetCardIdEncrypted param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.GetCardIdEncrypted.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.GetCardIdEncryptedResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.GetCardIdEncryptedResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.UploadDeviceInfo param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.UploadDeviceInfo.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.UploadDeviceInfoResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.UploadDeviceInfoResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.ProcessUpdates param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.ProcessUpdates.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.ProcessUpdatesResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.ProcessUpdatesResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.TokenizePlain param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.TokenizePlain.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.TokenizePlainResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.TokenizePlainResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.Batch param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.Batch.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.BatchResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.BatchResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.Retokenize param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.Retokenize.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.RetokenizeResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.RetokenizeResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.ReplenishCash param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.ReplenishCash.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.ReplenishCashResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.ReplenishCashResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.GetCardIdPlain param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.GetCardIdPlain.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.GetCardIdPlainResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.GetCardIdPlainResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.ChargePlain param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.ChargePlain.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.ChargePlainResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.ChargePlainResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.ReplenishEncrypted param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.ReplenishEncrypted.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.ReplenishEncryptedResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.ReplenishEncryptedResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.ChargeEncrypted param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.ChargeEncrypted.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.ChargeEncryptedResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.ChargeEncryptedResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.UploadFile param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.UploadFile.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.UploadFileResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.UploadFileResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.ReplenishPlain param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.ReplenishPlain.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.ReplenishPlainResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.ReplenishPlainResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.GetCardInfoEncrypted param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.GetCardInfoEncrypted.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.GetCardInfoEncryptedResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.GetCardInfoEncryptedResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.AuthPlain param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.AuthPlain.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.AuthPlainResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.AuthPlainResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.Cash param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.Cash.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.CashResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.CashResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.TokenizeEncrypted param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.TokenizeEncrypted.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.TokenizeEncryptedResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.TokenizeEncryptedResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.Untokenize param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.Untokenize.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.UntokenizeResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.UntokenizeResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.AuthEncrypted param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.AuthEncrypted.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.AuthEncryptedResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.AuthEncryptedResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.GetCardInfoPlain param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.GetCardInfoPlain.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec2.GetCardInfoPlainResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec2.GetCardInfoPlainResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ec2.GetCardIdEncryptedResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ec2.GetCardIdEncryptedResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ec2.GetCardIdEncryptedResponse wrapgetCardIdEncrypted(){
                                com.usatech.ec2.GetCardIdEncryptedResponse wrappedElement = new com.usatech.ec2.GetCardIdEncryptedResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ec2.UploadDeviceInfoResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ec2.UploadDeviceInfoResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ec2.UploadDeviceInfoResponse wrapuploadDeviceInfo(){
                                com.usatech.ec2.UploadDeviceInfoResponse wrappedElement = new com.usatech.ec2.UploadDeviceInfoResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ec2.ProcessUpdatesResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ec2.ProcessUpdatesResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ec2.ProcessUpdatesResponse wrapprocessUpdates(){
                                com.usatech.ec2.ProcessUpdatesResponse wrappedElement = new com.usatech.ec2.ProcessUpdatesResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ec2.TokenizePlainResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ec2.TokenizePlainResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ec2.TokenizePlainResponse wraptokenizePlain(){
                                com.usatech.ec2.TokenizePlainResponse wrappedElement = new com.usatech.ec2.TokenizePlainResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ec2.BatchResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ec2.BatchResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ec2.BatchResponse wrapbatch(){
                                com.usatech.ec2.BatchResponse wrappedElement = new com.usatech.ec2.BatchResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ec2.RetokenizeResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ec2.RetokenizeResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ec2.RetokenizeResponse wrapretokenize(){
                                com.usatech.ec2.RetokenizeResponse wrappedElement = new com.usatech.ec2.RetokenizeResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ec2.ReplenishCashResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ec2.ReplenishCashResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ec2.ReplenishCashResponse wrapreplenishCash(){
                                com.usatech.ec2.ReplenishCashResponse wrappedElement = new com.usatech.ec2.ReplenishCashResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ec2.GetCardIdPlainResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ec2.GetCardIdPlainResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ec2.GetCardIdPlainResponse wrapgetCardIdPlain(){
                                com.usatech.ec2.GetCardIdPlainResponse wrappedElement = new com.usatech.ec2.GetCardIdPlainResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ec2.ChargePlainResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ec2.ChargePlainResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ec2.ChargePlainResponse wrapchargePlain(){
                                com.usatech.ec2.ChargePlainResponse wrappedElement = new com.usatech.ec2.ChargePlainResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ec2.ReplenishEncryptedResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ec2.ReplenishEncryptedResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ec2.ReplenishEncryptedResponse wrapreplenishEncrypted(){
                                com.usatech.ec2.ReplenishEncryptedResponse wrappedElement = new com.usatech.ec2.ReplenishEncryptedResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ec2.ChargeEncryptedResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ec2.ChargeEncryptedResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ec2.ChargeEncryptedResponse wrapchargeEncrypted(){
                                com.usatech.ec2.ChargeEncryptedResponse wrappedElement = new com.usatech.ec2.ChargeEncryptedResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ec2.UploadFileResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ec2.UploadFileResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ec2.UploadFileResponse wrapuploadFile(){
                                com.usatech.ec2.UploadFileResponse wrappedElement = new com.usatech.ec2.UploadFileResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ec2.ReplenishPlainResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ec2.ReplenishPlainResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ec2.ReplenishPlainResponse wrapreplenishPlain(){
                                com.usatech.ec2.ReplenishPlainResponse wrappedElement = new com.usatech.ec2.ReplenishPlainResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ec2.GetCardInfoEncryptedResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ec2.GetCardInfoEncryptedResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ec2.GetCardInfoEncryptedResponse wrapgetCardInfoEncrypted(){
                                com.usatech.ec2.GetCardInfoEncryptedResponse wrappedElement = new com.usatech.ec2.GetCardInfoEncryptedResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ec2.AuthPlainResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ec2.AuthPlainResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ec2.AuthPlainResponse wrapauthPlain(){
                                com.usatech.ec2.AuthPlainResponse wrappedElement = new com.usatech.ec2.AuthPlainResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ec2.CashResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ec2.CashResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ec2.CashResponse wrapcash(){
                                com.usatech.ec2.CashResponse wrappedElement = new com.usatech.ec2.CashResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ec2.TokenizeEncryptedResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ec2.TokenizeEncryptedResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ec2.TokenizeEncryptedResponse wraptokenizeEncrypted(){
                                com.usatech.ec2.TokenizeEncryptedResponse wrappedElement = new com.usatech.ec2.TokenizeEncryptedResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ec2.UntokenizeResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ec2.UntokenizeResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ec2.UntokenizeResponse wrapuntokenize(){
                                com.usatech.ec2.UntokenizeResponse wrappedElement = new com.usatech.ec2.UntokenizeResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ec2.AuthEncryptedResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ec2.AuthEncryptedResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ec2.AuthEncryptedResponse wrapauthEncrypted(){
                                com.usatech.ec2.AuthEncryptedResponse wrappedElement = new com.usatech.ec2.AuthEncryptedResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ec2.GetCardInfoPlainResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ec2.GetCardInfoPlainResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ec2.GetCardInfoPlainResponse wrapgetCardInfoPlain(){
                                com.usatech.ec2.GetCardInfoPlainResponse wrappedElement = new com.usatech.ec2.GetCardInfoPlainResponse();
                                return wrappedElement;
                         }
                    


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type,
        java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{

        try {
        
                if (com.usatech.ec2.GetCardIdEncrypted.class.equals(type)){
                
                           return com.usatech.ec2.GetCardIdEncrypted.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.GetCardIdEncryptedResponse.class.equals(type)){
                
                           return com.usatech.ec2.GetCardIdEncryptedResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.UploadDeviceInfo.class.equals(type)){
                
                           return com.usatech.ec2.UploadDeviceInfo.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.UploadDeviceInfoResponse.class.equals(type)){
                
                           return com.usatech.ec2.UploadDeviceInfoResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.ProcessUpdates.class.equals(type)){
                
                           return com.usatech.ec2.ProcessUpdates.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.ProcessUpdatesResponse.class.equals(type)){
                
                           return com.usatech.ec2.ProcessUpdatesResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.TokenizePlain.class.equals(type)){
                
                           return com.usatech.ec2.TokenizePlain.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.TokenizePlainResponse.class.equals(type)){
                
                           return com.usatech.ec2.TokenizePlainResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.Batch.class.equals(type)){
                
                           return com.usatech.ec2.Batch.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.BatchResponse.class.equals(type)){
                
                           return com.usatech.ec2.BatchResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.Retokenize.class.equals(type)){
                
                           return com.usatech.ec2.Retokenize.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.RetokenizeResponse.class.equals(type)){
                
                           return com.usatech.ec2.RetokenizeResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.ReplenishCash.class.equals(type)){
                
                           return com.usatech.ec2.ReplenishCash.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.ReplenishCashResponse.class.equals(type)){
                
                           return com.usatech.ec2.ReplenishCashResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.GetCardIdPlain.class.equals(type)){
                
                           return com.usatech.ec2.GetCardIdPlain.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.GetCardIdPlainResponse.class.equals(type)){
                
                           return com.usatech.ec2.GetCardIdPlainResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.ChargePlain.class.equals(type)){
                
                           return com.usatech.ec2.ChargePlain.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.ChargePlainResponse.class.equals(type)){
                
                           return com.usatech.ec2.ChargePlainResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.ReplenishEncrypted.class.equals(type)){
                
                           return com.usatech.ec2.ReplenishEncrypted.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.ReplenishEncryptedResponse.class.equals(type)){
                
                           return com.usatech.ec2.ReplenishEncryptedResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.ChargeEncrypted.class.equals(type)){
                
                           return com.usatech.ec2.ChargeEncrypted.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.ChargeEncryptedResponse.class.equals(type)){
                
                           return com.usatech.ec2.ChargeEncryptedResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.UploadFile.class.equals(type)){
                
                           return com.usatech.ec2.UploadFile.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.UploadFileResponse.class.equals(type)){
                
                           return com.usatech.ec2.UploadFileResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.ReplenishPlain.class.equals(type)){
                
                           return com.usatech.ec2.ReplenishPlain.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.ReplenishPlainResponse.class.equals(type)){
                
                           return com.usatech.ec2.ReplenishPlainResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.GetCardInfoEncrypted.class.equals(type)){
                
                           return com.usatech.ec2.GetCardInfoEncrypted.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.GetCardInfoEncryptedResponse.class.equals(type)){
                
                           return com.usatech.ec2.GetCardInfoEncryptedResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.AuthPlain.class.equals(type)){
                
                           return com.usatech.ec2.AuthPlain.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.AuthPlainResponse.class.equals(type)){
                
                           return com.usatech.ec2.AuthPlainResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.Cash.class.equals(type)){
                
                           return com.usatech.ec2.Cash.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.CashResponse.class.equals(type)){
                
                           return com.usatech.ec2.CashResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.TokenizeEncrypted.class.equals(type)){
                
                           return com.usatech.ec2.TokenizeEncrypted.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.TokenizeEncryptedResponse.class.equals(type)){
                
                           return com.usatech.ec2.TokenizeEncryptedResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.Untokenize.class.equals(type)){
                
                           return com.usatech.ec2.Untokenize.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.UntokenizeResponse.class.equals(type)){
                
                           return com.usatech.ec2.UntokenizeResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.AuthEncrypted.class.equals(type)){
                
                           return com.usatech.ec2.AuthEncrypted.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.AuthEncryptedResponse.class.equals(type)){
                
                           return com.usatech.ec2.AuthEncryptedResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.GetCardInfoPlain.class.equals(type)){
                
                           return com.usatech.ec2.GetCardInfoPlain.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec2.GetCardInfoPlainResponse.class.equals(type)){
                
                           return com.usatech.ec2.GetCardInfoPlainResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    

        /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
        private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
        org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
        returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
        return returnMap;
        }

        private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
        org.apache.axis2.AxisFault f;
        Throwable cause = e.getCause();
        if (cause != null) {
            f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
        } else {
            f = new org.apache.axis2.AxisFault(e.getMessage());
        }

        return f;
    }

        }//end of class
    