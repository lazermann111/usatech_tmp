
/**
 * ExtensionMapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

        
            package com.usatech.ec2;
        
            /**
            *  ExtensionMapper class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ExtensionMapper{

          public static java.lang.Object getTypeObject(java.lang.String namespaceURI,
                                                       java.lang.String typeName,
                                                       javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{

              
                  if (
                  "http://ec2.usatech.com/xsd".equals(namespaceURI) &&
                  "EC2CardId".equals(typeName)){
                   
                            return  com.usatech.ec2.xsd.EC2CardId.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://ec2.usatech.com/xsd".equals(namespaceURI) &&
                  "EC2CardInfo".equals(typeName)){
                   
                            return  com.usatech.ec2.xsd.EC2CardInfo.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://ec2.usatech.com/xsd".equals(namespaceURI) &&
                  "EC2AuthResponse".equals(typeName)){
                   
                            return  com.usatech.ec2.xsd.EC2AuthResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://ec2.usatech.com/xsd".equals(namespaceURI) &&
                  "EC2ProcessUpdatesResponse".equals(typeName)){
                   
                            return  com.usatech.ec2.xsd.EC2ProcessUpdatesResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://ec2.usatech.com/xsd".equals(namespaceURI) &&
                  "EC2Response".equals(typeName)){
                   
                            return  com.usatech.ec2.xsd.EC2Response.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://ec2.usatech.com/xsd".equals(namespaceURI) &&
                  "EC2ReplenishResponse".equals(typeName)){
                   
                            return  com.usatech.ec2.xsd.EC2ReplenishResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://ec2.usatech.com/xsd".equals(namespaceURI) &&
                  "EC2TokenResponse".equals(typeName)){
                   
                            return  com.usatech.ec2.xsd.EC2TokenResponse.Factory.parse(reader);
                        

                  }

              
             throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
          }

        }
    