package com.usatech.networklayer.protocol;

import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.util.concurrent.TimeoutException;

import simple.io.Log;
import simple.text.StringUtils;
import simple.util.concurrent.AbstractCreatingPool;
import simple.util.concurrent.AbstractLockingPool;
import simple.util.concurrent.BoundedPool;
import simple.util.concurrent.CreationException;
import simple.util.concurrent.Pool;

import com.usatech.util.crypto.CRCException;
import com.usatech.util.crypto.Crypt;
import com.usatech.util.crypto.CryptException;
import com.usatech.util.crypto.CryptUtil;

public abstract class AbstractCryptProtocol implements Protocol {
	private static final Log log = Log.getLog();
	protected class DefaultCryptor implements Cryptor {
		protected String deviceName;
		protected byte[] key;

		@Override
		public void decrypt(ByteBuffer readBuffer, ByteBuffer writeBuffer)
				throws GeneralSecurityException {
			byte[] encrypted = new byte[readBuffer.remaining()];
			readBuffer.get(encrypted);
			encrypted = decode(encrypted);
			if(log.isDebugEnabled())
	            log.debug(deviceName + ": Decrypting with key: " + StringUtils.toHex(key) + ", Undecrypted: " + StringUtils.toHex(encrypted));
			byte[] decrypted;
			try {
				decrypted = crypt.decrypt(encrypted, key);
			} catch(InvalidKeyException e) {
				StringBuilder sb = new StringBuilder(9+8+29+(2*encrypted.length)+16+(2*key.length));
				sb.append("decrypt: ").append(deviceName).append(": Failed to decrypt message (");
				StringUtils.appendHex(sb, encrypted, 0, encrypted.length);
				sb.append("): Invalid Key: ");
				StringUtils.appendHex(sb, key, 0, key.length);
				throw new GeneralSecurityException(sb.toString(), e);
			} catch(CRCException e) {
				StringBuilder sb = new StringBuilder(9+8+29+(2*encrypted.length)+1+10+(2*key.length)+19);
				sb.append("decrypt: ").append(deviceName).append(": Failed to decrypt message (");
				StringUtils.appendHex(sb, encrypted, 0, encrypted.length);
				sb.append(')');
				if(log.isDebugEnabled()) {
					sb.append(" with key ");
					StringUtils.appendHex(sb, key, 0, key.length);
				}
				sb.append(": CRC Check Failed!");
				throw new GeneralSecurityException(sb.toString(), e);
			} catch(CryptException e) {
				StringBuilder sb = new StringBuilder(9+8+29+(2*encrypted.length)+1+10+(2*key.length)+31);
				sb.append("decrypt: ").append(deviceName).append(": Failed to decrypt message (");
				StringUtils.appendHex(sb, encrypted, 0, encrypted.length);
				sb.append(')');
				if(log.isDebugEnabled()) {
					sb.append(" with key ");
					StringUtils.appendHex(sb, key, 0, key.length);
				}
				sb.append(": Decryption Algorithm Failure!");
				throw new GeneralSecurityException(sb.toString(), e);
			} catch(Exception e) {
				String causeString = e.toString();
				StringBuilder sb = new StringBuilder(9+8+50+(2*encrypted.length)+1+10+(2*key.length)+2+causeString.length());
				sb.append("decrypt: ").append(deviceName).append(": Caught unexpected exception decrypting message (");
				StringUtils.appendHex(sb, encrypted, 0, encrypted.length);
				sb.append(')');
				if(log.isDebugEnabled()) {
					sb.append(" with key ");
					StringUtils.appendHex(sb, key, 0, key.length);
				}
				sb.append(": ").append(causeString);
				throw new GeneralSecurityException(sb.toString(), e);
			}
			if(log.isDebugEnabled())
	            log.debug(deviceName + ": Decrypted: " + StringUtils.toHex(decrypted));
			writeBuffer.put(decrypted);
		}

		@Override
		public void init(byte[] key, String deviceName) throws GeneralSecurityException {
			if (key == null)
				throw new GeneralSecurityException("Key is null");
			this.deviceName = deviceName;
			this.key = key;
		}

		public void encrypt(ByteBuffer readBuffer, ByteBuffer writeBuffer)
				throws GeneralSecurityException {
			byte[] unencrypted = new byte[readBuffer.remaining()];
			readBuffer.get(unencrypted);
			if(log.isDebugEnabled())
	            log.debug(deviceName + ": Encrypting with key: " + StringUtils.toHex(key) + ", Unencrypted: " + StringUtils.toHex(unencrypted));
			byte[] encrypted;
			try {
				encrypted = crypt.encrypt(unencrypted, key);
			} catch(InvalidKeyException e) {
				throw new GeneralSecurityException("encrypt: " + deviceName
						+ ": Failed to encrypt message (" + StringUtils.toHex(unencrypted)
						+ "): Invalid Key: " + StringUtils.toHex(key), e);
			} catch(CRCException e) {
				throw new GeneralSecurityException("encrypt: " + deviceName
						+ ": Failed to encrypt message (" + StringUtils.toHex(unencrypted)
						+ "): CRC Check Failed!", e);
			} catch(CryptException e) {
				throw new GeneralSecurityException("encrypt: " + deviceName
						+ ": Failed to encrypt message (" + StringUtils.toHex(unencrypted)
						+ "): Decryption Algorithm Failure!", e);
			} catch(Exception e) {
				throw new GeneralSecurityException("encrypt: " + deviceName
						+ ": Caught unexpected exception encrypting message ("
						+ StringUtils.toHex(unencrypted) + "): " + e, e);
			}
			encrypted = encode(encrypted);
			if(log.isDebugEnabled())
	            log.debug(deviceName + ": Encrypted: " + StringUtils.toHex(encrypted));
			writeBuffer.put(encrypted);
		}

		public byte[] encode(byte[] bytes) {
			return bytes;
		}
		public byte[] decode(byte[] bytes) {
			return bytes;
		}
		@Override
		public void release() {
			cryptorPool.returnObject(this);
		}
	}
	protected final Crypt crypt;
	protected Integer maxActive;
	protected Integer maxIdle;
	protected Long maxWait;
	protected Pool<Cryptor> cryptorPool;

	protected AbstractCryptProtocol(String cryptName) {
		this.crypt = CryptUtil.getCrypt(cryptName);
	}

	public void initialize() {
		Integer i = maxActive;
		if(i == null || i.intValue() < 1 || i.intValue() == Integer.MAX_VALUE) {
			cryptorPool = new AbstractCreatingPool<Cryptor>() {
				@Override
				protected Cryptor createObject() throws CreationException, InterruptedException {
					return createCryptor();
				}

				@Override
				protected void closeObject(Cryptor obj) throws Exception {
					//do nothing - gc will cleanup
				}
			};
		} else {
			BoundedPool<Cryptor> bp	= new AbstractLockingPool<Cryptor>() {
				@Override
				protected Cryptor createObject(long timeout) throws CreationException, InterruptedException {
					return createCryptor();
				}

				@Override
				protected void closeObject(Cryptor obj) throws Exception {
					//do nothing - gc will cleanup
				}
			};
			bp.setMaxActive(i);
			if((i=maxIdle) != null)
				bp.setMaxIdle(i);
			Long l;
			if((l=maxWait) != null)
				bp.setMaxWait(l);
			cryptorPool = bp;
		}
	}

	public Cryptor newCryptor() throws GeneralSecurityException {
		try {
			return cryptorPool.borrowObject();
		} catch(TimeoutException e) {
			throw new GeneralSecurityException("Could not create cryptor before timeout", e);
		} catch(CreationException e) {
			if(e.getCause() instanceof GeneralSecurityException)
				throw (GeneralSecurityException) e.getCause();
			else
				throw new GeneralSecurityException("Could not create cryptor", e);
		} catch(InterruptedException e) {
			throw new GeneralSecurityException("Interupted while creating cryptor", e);
		}
	}

	protected Cryptor createCryptor() throws CreationException {
		return new DefaultCryptor();
	}

	public Integer getMaxActive() {
		return maxActive;
	}

	public void setMaxActive(Integer maxActive) {
		this.maxActive = maxActive;
	}

	public Integer getMaxIdle() {
		return maxIdle;
	}

	public void setMaxIdle(Integer maxIdle) {
		this.maxIdle = maxIdle;
	}

	public Long getMaxWait() {
		return maxWait;
	}

	public void setMaxWait(Long maxWait) {
		this.maxWait = maxWait;
	}
}
