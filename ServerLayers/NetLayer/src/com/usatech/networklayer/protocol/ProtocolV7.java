package com.usatech.networklayer.protocol;


public class ProtocolV7 extends AbstractCryptProtocol {
	public ProtocolV7() {
		super("AES_CBC_CRC16");
	}

	@Override
	public byte getProtocolId() {
		return 7;
	}
}
