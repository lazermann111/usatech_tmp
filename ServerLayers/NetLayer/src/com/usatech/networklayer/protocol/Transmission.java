/**
 *
 */
package com.usatech.networklayer.protocol;

import java.io.IOException;
import java.nio.ByteBuffer;

import simple.io.Log;

import com.usatech.layers.common.DeviceInfo;
import com.usatech.layers.common.messagedata.MessageData;

/**
 * @author Brian S. Krug
 *
 */
public interface Transmission {
	public static class TransmissionInfo {
		protected final String deviceName;
		protected byte protocolId;
		protected final ByteBuffer data;

		public TransmissionInfo(String deviceName, byte protocolId, ByteBuffer data) {
			super();
			this.deviceName = deviceName;
			this.protocolId = protocolId;
			this.data = data;
		}
		public String getDeviceName() {
			return deviceName;
		}
		public byte getProtocolId() {
			return protocolId;
		}
		public ByteBuffer getData() {
			return data;
		}
		public void setProtocolId(byte protocolId) {
			this.protocolId = protocolId;
		}
	}

	/** Reads from the buffer the next message and returns the transmission info. Returns null if not enough data is provided.
	 *  The buffer returned in the transmission info may be the same as the one passed as a parameter. In either case,
	 *  the buffer's position must be set to the start of the encrypted data and the limit to the end of the encrypted data.
	 *  The controlling program will call this method each time there is more data available until the method does not return null.
	 *  The limit of the passed in buffer should be set to the end of the data used to construct the buffer returned in the TransmissionInfo.
	 * @param buffer The buffer to read and to set
	 * @return
	 */
	public TransmissionInfo prepareRead(ByteBuffer buffer, Log sessionLog) throws IOException ;

	/** Prepares the buffer for writing by setting the buffer's position to the spot where the encrypted data should be written
	 * @param buffer
	 * @param info
	 */
	public void prepareWrite(ByteBuffer buffer, TransmissionInfo info, Log sessionLog) throws IOException ;

	/** Adds any additional information necessary to the buffer. The buffer's position will be the end of the encrypted data.
	 * @param buffer
	 * @param info
	 */
	public void finishWrite(ByteBuffer buffer, TransmissionInfo info, Log sessionLog) throws IOException ;

	/** Resets any state information
	 *
	 */
	public void reset() ;

	/** Gets the minimum time in milliseconds that the server must wait before sending the response to the device
	 * @param inbound
	 * @param outboundIndex 
	 * @param outbound
	 * @param deviceInfo
	 * @return
	 */
	public int getMinimumResponseDelay(MessageData inbound, int outboundIndex, MessageData outbound, DeviceInfo deviceInfo) ;
}
