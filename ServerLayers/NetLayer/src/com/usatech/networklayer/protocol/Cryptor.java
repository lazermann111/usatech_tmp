package com.usatech.networklayer.protocol;

import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;

public interface Cryptor {
	public void init(byte[] encryptionKey, String deviceName) throws GeneralSecurityException ;
	public void decrypt(ByteBuffer readBuffer, ByteBuffer writeBuffer) throws GeneralSecurityException ;
	public void encrypt(ByteBuffer readBuffer, ByteBuffer writeBuffer) throws GeneralSecurityException ;
	public void release() ;
}
