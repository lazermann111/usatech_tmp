/**
 *
 */
package com.usatech.networklayer.protocol;

import simple.io.UUEncode;
import simple.util.concurrent.CreationException;

/**
 * @author Brian S. Krug
 *
 */
public class ProtocolV4 extends AbstractCryptProtocol {
	protected class UUEncodedCryptor extends DefaultCryptor {
		/**
		 * @see com.usatech.networklayer.protocol.AbstractCryptProtocol.DefaultCryptor#decode(byte[])
		 */
		@Override
		public byte[] decode(byte[] bytes) {
			return bytes; // Layer3Transmission takes care of UU decoding: UUEncode.decode(bytes);
		}
		/**
		 * @see com.usatech.networklayer.protocol.AbstractCryptProtocol.DefaultCryptor#encode(byte[])
		 */
		@Override
		public byte[] encode(byte[] bytes) {
			return UUEncode.encode(bytes);
		}
	}
	/**
	 *
	 */
	public ProtocolV4() {
		super("TEA_CRC16");
	}
	/**
	 * @see com.usatech.networklayer.protocol.Protocol#getProtocolId()
	 */
	@Override
	public byte getProtocolId() {
		return 4;
	}
	/**
	 * @see com.usatech.networklayer.protocol.AbstractCryptProtocol#createCryptor()
	 */
	@Override
	protected Cryptor createCryptor() throws CreationException {
		return new UUEncodedCryptor();
	}
}
