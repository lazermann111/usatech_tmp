package com.usatech.networklayer.protocol;


public class ProtocolV5 extends AbstractCryptProtocol {
	public ProtocolV5() {
		super("RIJNDAEL_CRC16");
	}

	@Override
	public byte getProtocolId() {
		return 5;
	}
}
