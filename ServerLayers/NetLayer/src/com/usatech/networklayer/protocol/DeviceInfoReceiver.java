package com.usatech.networklayer.protocol;

import java.sql.Connection;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.BasicDeviceInfo;
import com.usatech.layers.common.DeviceInfo;
import com.usatech.layers.common.DeviceInfoManager;
import com.usatech.layers.common.NoUpdateDeviceInfo;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.constants.ActivationStatus;
import com.usatech.layers.common.constants.DeviceInfoProperty;
import com.usatech.layers.common.constants.DeviceProperty;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.ServerActionCode;

import simple.app.AbstractWorkQueue;
import simple.app.BasicQoS;
import simple.app.Publisher;
import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.io.ByteInput;
import simple.io.Log;
import simple.lang.InterruptGuard;
import simple.security.crypt.EncryptionInfoMapping;
import simple.text.StringUtils;
import simple.util.concurrent.LockLinkedSegmentCache;
import simple.util.concurrent.WaitForUpdateFuture;

public class DeviceInfoReceiver implements MessageChainTask, DeviceInfoManager {
	private static final Log log = Log.getLog();

	protected class InnerDeviceInfo extends BasicDeviceInfo {
		protected final ReentrantLock lock = new ReentrantLock();
		
		@Override
		protected void lock() {
			lock.lock();
		}
		@Override
		protected void unlock() {
			lock.unlock();
		}
		public InnerDeviceInfo(String deviceName) {
			super(deviceName);
		}
		/**
		 * @see com.usatech.layers.common.BasicDeviceInfo#processChanges(java.util.Map, boolean)
		 */
		@Override
		protected void processChanges(Map<String, Object> changes, Map<String, Long> timestamps, boolean internalOnly, Connection targetConn) {

		}
	}
	protected static final Map<String,Object> EMPTY_ATTRIBUTES = Collections.emptyMap();
	protected static final DeviceInfo LOAD_BALANCER_DEVICE_INFO = new NoUpdateDeviceInfo(ProcessingConstants.LOAD_BALANCER_MACHINE_ID, null, null, null, 0, 0, DeviceType.DEFAULT, null, false, ProcessingConstants.US_ASCII_CHARSET, ActivationStatus.NOT_ACTIVATED, TimeZone.getDefault().getID(), ServerActionCode.NO_ACTION, null, null, null, null, 0, null, null, null, null, null, null, 0, null, false);
	protected Publisher<ByteInput> publisher;
	protected String keyRequestorQueueKey;
	protected AbstractWorkQueue<ByteInput> keyReceiverQueue;
	protected long timeout;
	protected String storeQueueKey;
	protected String keyReceiverQueueKey;
	protected EncryptionInfoMapping encryptionInfoMapping;
	protected BasicQoS keyRequestorQoS;
	protected class GetKeyTask extends WaitForUpdateFuture<DeviceInfo> {
		protected final AtomicBoolean needRefresh = new AtomicBoolean();
		protected final InnerDeviceInfo deviceInfo;
		
		public GetKeyTask(String deviceName) {
			super();
			this.deviceInfo = new InnerDeviceInfo(deviceName) {			
				public void refresh() throws ServiceException {
					reset();
					try {
						GetKeyTask.this.get();
					} catch(InterruptedException e) {
						throw new ServiceException(e);
					} catch(ExecutionException e) {
						throw new ServiceException(e);
					}
				}
			};
		}

		@Override
		protected void run() throws ExecutionException {
			try {
				requestKey(deviceInfo.getDeviceName(), needRefresh.getAndSet(false));
			} catch(ServiceException e) {
				throw new ExecutionException(e);
			}
		}
	}

	protected static DeviceInfoReceiver instance;

	protected final LockLinkedSegmentCache<String, GetKeyTask, RuntimeException> keyCache = new LockLinkedSegmentCache<String, GetKeyTask, RuntimeException>(0, 1024, 0.75f, 32) {
		@Override
		protected GetKeyTask createValue(final String key, Object... additionalInfo) {
			return new GetKeyTask(key);
		}
	};

	protected final ConcurrentHashMap<String, String> deviceSerialNames = new ConcurrentHashMap<String, String>();

	public static DeviceInfoReceiver getInstance() {
		return instance;
	}

	public DeviceInfoReceiver() {
		super();
		instance = this;
	}

	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		Map<String, Object> attributes = taskInfo.getStep().getAttributes();
		String deviceName = ConvertUtils.getStringSafely(attributes.get("deviceName"));
		String exception = ConvertUtils.getStringSafely(attributes.get("exception"));
		DeviceType deviceType = ConvertUtils.convertSafely(DeviceType.class, attributes.get("deviceType"), null);
		GetKeyTask task;
		if(exception == null && deviceType == null) {
			task = keyCache.getIfPresent(deviceName);
			if(task == null) {
				if(log.isInfoEnabled())
					log.info("Skipping update for " + deviceName + " because device info is not cached");
				return 0;
			}
		} else
			task = keyCache.getOrCreate(deviceName);
		if(exception == null) {
			if (ConvertUtils.getBooleanSafely(attributes.get("removeDeviceInfo"), false)) {
				if (removeDeviceInfo(deviceName))
					log.info("Removed disabled device " + deviceName);
				return 0;
			}
			byte[] encryptionKey;
			if(log.isDebugEnabled()) {
				encryptionKey = ConvertUtils.convertSafely(byte[].class, attributes.get("encryptionKey"), null);
				log.debug("Received key '" + StringUtils.toHex(encryptionKey) + "' for " + deviceName + " into " + task);
			} else {
				encryptionKey = null;
			}
			long timestamp = ConvertUtils.getLongSafely(attributes.get("timestamp"), 0);
			Map<String,Long> timestamps = new HashMap<String, Long>();
			for(String key : BasicDeviceInfo.attributeKeys)
				timestamps.put(key, ConvertUtils.getLongSafely(attributes.get(key + ".timestamp"), timestamp));
			task.deviceInfo.updateInternal(attributes, timestamps, null);
			task.set(task.deviceInfo);
			if (ConvertUtils.getBooleanSafely(attributes.get("storeDeviceName"), false)) { 
				String deviceSerialCd = ConvertUtils.getStringSafely(attributes.get("deviceSerialCd"));
				deviceSerialNames.put(deviceSerialCd, deviceName);
				GetKeyTask deviceSerialTask = keyCache.getOrCreate(deviceSerialCd);
				deviceSerialTask.deviceInfo.updateInternal(attributes, timestamps, null);
				deviceSerialTask.set(task.deviceInfo);
			}
			log.info("Received device info update for device " + deviceName);
			if(log.isDebugEnabled()) {
				if(!Arrays.equals(encryptionKey, task.deviceInfo.getEncryptionKey()))
					log.debug("Encryption keys do not match for device " + deviceName + ", NetLayer encryption key timestamp: " + task.deviceInfo.getTimestamp(DeviceInfoProperty.ENCRYPTION_KEY));
				log.debug("Device Info encryption key for " + deviceName + " is now '" + StringUtils.toHex(task.deviceInfo.getEncryptionKey()) + "'");
			}
		} else {
			if(log.isDebugEnabled())
				log.debug("Received exception getting key for " + deviceName);
			task.setException(new ExecutionException(exception, null));
		}
		return 0;
	}

	/**
	 * @param settings
	 * @throws ServiceException
	 */
	protected void queueSettingsUpdate(String deviceName, DeviceProperty deviceProperty, String value) throws ServiceException {
		MessageChain mc = new MessageChainV11();
		MessageChainStep store = mc.addStep(getStoreQueueKey());
		store.addStringAttribute(String.valueOf(deviceProperty.getValue()), value);
		store.addStringAttribute("deviceName", deviceName);
		MessageChainService.publish(mc, getPublisher(), getEncryptionInfoMapping());
	}

	public boolean removeDeviceInfo(String deviceName) {
		return keyCache.remove(deviceName) != null;
	}
	
	@Override
	public DeviceInfo getDeviceInfo(String deviceName, OnMissingPolicy onMissingPolicy, boolean refreshFromSource) throws ServiceException {
		return getDeviceInfo(deviceName, onMissingPolicy, refreshFromSource, null, false);
	}
	@Override
	public DeviceInfo getDeviceInfo(String deviceName, OnMissingPolicy onMissingPolicy, boolean refreshFromSource, InterruptGuard guard, boolean byDeviceSerialCd) throws ServiceException {
		if(ProcessingConstants.LOAD_BALANCER_MACHINE_ID.equals(deviceName)) {
			return LOAD_BALANCER_DEVICE_INFO;
		}
		String deviceSerialCd = null;
		String storedDeviceName = null;
		if (byDeviceSerialCd) {
			deviceSerialCd = deviceName;
			storedDeviceName = deviceSerialNames.get(deviceName);
			if (storedDeviceName != null)
				deviceName = storedDeviceName;
		}
		GetKeyTask task = keyCache.getOrCreate(deviceName);
		if(!refreshFromSource) {
			if(task.isException() || task.isCancelled())
				task.reset();
			switch(onMissingPolicy) {
				case CREATE_NEW:
					if(!task.isDone()) {
						task.set(task.deviceInfo);
						return task.deviceInfo;
					} 
					break;
				case RETURN_NULL:
					if(task.isException() || task.isCancelled())
						task.reset();
					else if(!task.isDone())
						return null;
					break;
				case THROW_EXCEPTION:
					if(task.isException() || task.isCancelled())
						task.reset();
					break;
				default:
					throw new ServiceException("Invalid OnMissingPolicy specified");
			}
		} else {
			task.needRefresh.set(true);
			if(task.isException() || task.isCancelled() || task.isDone())
				task.reset();
		}
		if(log.isDebugEnabled())
			log.debug("Getting device info for " + deviceName + " from " + task);
		long timeout = getTimeout();
		DeviceInfo deviceInfo;
		try {
			if(timeout > 0)
				deviceInfo = task.get(timeout, TimeUnit.MILLISECONDS);
			else
				deviceInfo = task.get();
		} catch(CancellationException e) {
			throw new ServiceException("Error getting device info for '" + deviceName + "'",e);
		} catch(InterruptedException e) {
			task.cancel(false);
			throw new ServiceException("Error getting device info for '" + deviceName + "'",e);
		} catch(ExecutionException e) {
			task.cancel(false);
			if(onMissingPolicy != OnMissingPolicy.THROW_EXCEPTION && e.getMessage() != null && e.getMessage().toLowerCase().contains("not found")) {
				if(onMissingPolicy == OnMissingPolicy.CREATE_NEW) {
					task.set(task.deviceInfo);
					return task.deviceInfo;
				} else
					return null;					
			}
			throw new ServiceException("Error getting device info for '" + deviceName + "'",e);
		} catch(TimeoutException e) {
			task.cancel(false);
			throw new ServiceException("Could not retrieve device info in " + timeout + " ms for '" + deviceName + "'",e);
		}
		if (byDeviceSerialCd && storedDeviceName == null && deviceInfo != null && !deviceSerialCd.equalsIgnoreCase(deviceInfo.getDeviceName()))
			removeDeviceInfo(deviceSerialCd);
		return deviceInfo;
	}

	protected void requestKey(String deviceName, boolean refreshFromSource) throws ServiceException {
		if(log.isDebugEnabled())
			log.debug("Creating key request for " + deviceName);
		MessageChain mc = new MessageChainV11();
		MessageChainStep request = mc.addStep(getKeyRequestorQueueKey());
		request.addStringAttribute("deviceName", deviceName);
		if(refreshFromSource)
			request.addBooleanAttribute("refresh", refreshFromSource);
		MessageChainStep response = mc.addStep(getKeyReceiverQueue().getQueueKey());
		response.addStringAttribute("deviceName", deviceName);
		for(DeviceInfoProperty dip : DeviceInfoProperty.values()) {
			response.addReferenceAttribute(dip.getAttributeKey(), request, dip.getAttributeKey());
			response.addReferenceAttribute(dip.getAttributeKey() + ".timestamp", request, dip.getAttributeKey() + ".timestamp");
		}
		request.setNextSteps(0, response);
		MessageChainService.publish(mc, getPublisher(), getEncryptionInfoMapping(), getKeyRequestorQoS());
	}

	public Publisher<ByteInput> getPublisher() {
		return publisher;
	}
	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}
	public String getKeyRequestorQueueKey() {
		return keyRequestorQueueKey;
	}
	public void setKeyRequestorQueueKey(String keyRequestorQueueKey) {
		this.keyRequestorQueueKey = keyRequestorQueueKey;
	}

	public AbstractWorkQueue<ByteInput> getKeyReceiverQueue() {
		return keyReceiverQueue;
	}

	public void setKeyReceiverQueue(AbstractWorkQueue<ByteInput> keyReceiverQueue) {
		this.keyReceiverQueue = keyReceiverQueue;
		if(keyReceiverQueue != null && keyReceiverQueueKey != null)
			keyReceiverQueue.setQueueKey(keyReceiverQueueKey.toString());
	}

	public long getTimeout() {
		return timeout;
	}

	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}

	public int getMaxSize() {
		return keyCache.getMaxSize();
	}

	public void setMaxSize(int maxSize) {
		keyCache.setMaxSize(maxSize);
	}

	public String getStoreQueueKey() {
		return storeQueueKey;
	}

	public void setStoreQueueKey(String storeQueueKey) {
		this.storeQueueKey = storeQueueKey;
	}

	public String getKeyReceiverQueueKey() {
		return keyReceiverQueueKey;
	}

	public void setKeyReceiverQueueKey(String keyReceiverQueueKey) {
		this.keyReceiverQueueKey = keyReceiverQueueKey;
		if(keyReceiverQueue != null && keyReceiverQueueKey != null)
			keyReceiverQueue.setQueueKey(keyReceiverQueueKey);
	}

	public EncryptionInfoMapping getEncryptionInfoMapping() {
		return encryptionInfoMapping;
	}

	public void setEncryptionInfoMapping(EncryptionInfoMapping encryptionInfoMapping) {
		this.encryptionInfoMapping = encryptionInfoMapping;
	}

	public BasicQoS getKeyRequestorQoS() {
		return keyRequestorQoS;
	}

	public void setKeyRequestorQoS(BasicQoS keyRequestorQoS) {
		this.keyRequestorQoS = keyRequestorQoS;
	}
}
