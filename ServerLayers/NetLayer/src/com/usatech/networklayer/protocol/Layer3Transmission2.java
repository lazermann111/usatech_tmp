/**
 *
 */
package com.usatech.networklayer.protocol;

import java.io.EOFException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

import simple.io.Log;
import simple.io.UUEncode;
import simple.text.StringUtils;

import com.usatech.layers.common.DeviceInfo;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.messagedata.MessageData;

/**
 * @author Brian S. Krug
 *
 */
public class Layer3Transmission2 implements Transmission {
	protected static final byte     PACKET_DELIMITER            = 0x0A;
	protected static final byte 	PING_PROTOCOL_ID 			= 0;
	protected static final byte 	PROTOCOL_ID 				= 4;
	
	private static final byte[]     LOAD_BALANCER_BYTES    = "LD-BLNCR".getBytes();
	private static final byte[]     G4_NOISE_BYTES    = {0x0d, 0x02, 0x40, 0x52}; //"\\x0d\\x02\\x40\\x52"
	private static final byte[]     HANGUP_BYTES_STANDARD       = "+++".getBytes();
    private static final byte[]     HANGUP_BYTES_MULTITECH      = "AT#CONNECTIONSTOP".getBytes();
    private static final byte[]     HANGUP_BYTES_AS5300         = { (byte)0x9e, (byte)0x86, (byte)0x9e, (byte)0x86, (byte)0x9e, (byte)0x86 }; //"\\x9e\\x86\\x9e\\x86\\x9e\\x86";
    private static final byte[]     HANGUP_BYTES_LANTRONIX      = "Logout".getBytes();
    private static final byte[][]   HANGUP_BYTE_OPTIONS = new byte[][] {HANGUP_BYTES_STANDARD, HANGUP_BYTES_MULTITECH, HANGUP_BYTES_AS5300, HANGUP_BYTES_LANTRONIX};
    private static final byte[]     PACKET_START_BYTES          = {0x31, 0x35, 0x38};
    
	/**
	 * @see com.usatech.networklayer.protocol.Transmission#finishWrite(java.nio.ByteBuffer, com.usatech.networklayer.protocol.Transmission.TransmissionInfo, simple.io.Log)
	 */
	@Override
	public void finishWrite(ByteBuffer buffer, TransmissionInfo info, Log sessionLog) throws IOException {
		if(info.getProtocolId() != 0 || !info.getDeviceName().equals(ProcessingConstants.LOAD_BALANCER_MACHINE_ID)) {
	       buffer.put(PACKET_DELIMITER);
		}
    }

	protected boolean isLoadBalancerPing(ByteBuffer buffer) {
		for(int i = 0; i < LOAD_BALANCER_BYTES.length; i++) {
			if(buffer.get(buffer.position() + i + 1) != LOAD_BALANCER_BYTES[i])
				return false;
		}
		return true;
	}
	/**
	 * @see com.usatech.networklayer.protocol.Transmission#prepareRead(java.nio.ByteBuffer, simple.io.Log)
	 */
	@Override
	public TransmissionInfo prepareRead(ByteBuffer buffer, Log sessionLog) throws IOException {
		if(buffer.remaining() < 3 /* May be a hangup */) 
			return null; // not enough data
		if(isLoadBalancerPing(buffer)) {
			buffer.position(buffer.position() + LOAD_BALANCER_BYTES.length + 1);
            ByteBuffer bb = ByteBuffer.allocate(1);
        	bb.put(buffer.get());
        	bb.flip();
        	return new TransmissionInfo(ProcessingConstants.LOAD_BALANCER_MACHINE_ID, PING_PROTOCOL_ID, bb);    
		}
		int g4LineNoisePos = 0;
		int[] hangupPos = new int[HANGUP_BYTE_OPTIONS.length];
		int packetStartPos = 0;
		ByteBuffer copyBuffer = null;
		boolean close = false;
		StringBuilder lineNoise = new StringBuilder();
		int p = 0, hangupIndex = 0;
		OUTER: for(; p < buffer.limit(); p++) {
			byte b = buffer.get(p);
			if(G4_NOISE_BYTES[g4LineNoisePos] == b) {
				if(++g4LineNoisePos >= G4_NOISE_BYTES.length) { // ignore everything beyond
					if(buffer.limit() > buffer.position())
						sessionLog.warn("ReceiveFromClient: Filtered out " + (buffer.limit() - buffer.position()) + " G4 garbage character(s): " + StringUtils.toHex(buffer, buffer.position(), buffer.limit() - buffer.position()));
					buffer.position(buffer.limit());
					return null;
				}
			} else {
				g4LineNoisePos = 0;
			}
			if(!close) {
				for(int i = 0; i < hangupPos.length; i++) {
					if(HANGUP_BYTE_OPTIONS[i][hangupPos[i]] == b) {
						if(++hangupPos[i] >= HANGUP_BYTE_OPTIONS[i].length) {
							close = true;
							hangupIndex = i;
							continue OUTER;
						}
					} else {
						hangupPos[i] = 0;
					}
				}
			}
			if(PACKET_START_BYTES[packetStartPos] == b) {
				if(++packetStartPos >= PACKET_START_BYTES.length) { // ignore everything beyond
					close = false;
					int newPosition = p - PACKET_START_BYTES.length + 1;
					if(newPosition > buffer.position())
						sessionLog.warn("ReceiveFromClient: Filtered out " + (newPosition - buffer.position()) + " garbage character(s): " + StringUtils.toHex(buffer, buffer.position(), newPosition - buffer.position()));
					buffer.position(newPosition);
					for(; p < buffer.limit(); p++) {
						b = buffer.get(p);
						if(b == PACKET_DELIMITER) {
							// we have a message!
							if(lineNoise.length() > 0) {
								sessionLog.warn("ReceiveFromClient: Filtered out " + (lineNoise.length()/2) + " non-UUEncode character(s): " + lineNoise);
								lineNoise.setLength(0);
							}
							if(copyBuffer == null) {
								copyBuffer = buffer.asReadOnlyBuffer();
								copyBuffer.limit(p);						
							} else {
								copyBuffer.flip();
							}
							byte[] encodedBytes = new byte[copyBuffer.remaining()];
							copyBuffer.get(encodedBytes);
							buffer.position(p+1); // advance past the packet delimiter
							byte[] decodedBytes = UUEncode.decode(encodedBytes);	                
			                if(decodedBytes.length > 8) {
			                	String deviceName = new String(decodedBytes, 0, 8);
			                	if(sessionLog.isDebugEnabled())
			            			sessionLog.debug("Device identified itself as " + deviceName+ ". Received valid packet from device "
			            					+ decodedBytes.length + " byte(s): " + StringUtils.toHex(decodedBytes) + " with " + buffer.remaining() + " bytes left over.");
			                	ByteBuffer bb = ByteBuffer.wrap(decodedBytes, 8, decodedBytes.length - 8);
			                	return new TransmissionInfo(deviceName, PROTOCOL_ID, bb);
			                }
							sessionLog.warn("Received invalid packet from device " + (decodedBytes.length - 8) + " byte(s): " + StringUtils.toHex(decodedBytes));
							throw new EOFException("End of message received and the message is invalid.");
						} else if(b > 0x60 || b < 0x20) {
							// non-uuencoding character - ignore it
							if(copyBuffer == null) {
								copyBuffer = ByteBuffer.allocate(buffer.remaining());
								int origLimit = buffer.limit();
								buffer.limit(p);
								copyBuffer.put(buffer);
								buffer.limit(origLimit);
							}
							StringUtils.appendHex(lineNoise, b);
						} else if(copyBuffer != null) {
							copyBuffer.put(b);
							if(lineNoise.length() > 0) {
								sessionLog.warn("ReceiveFromClient: Filtered out " + (lineNoise.length()/2) + " non-UUEncode character(s): " + lineNoise);
								lineNoise.setLength(0);
							}
						}
					}
					return null; //haven't hit packet end yet
				} 
			} else {
				packetStartPos = 0;
			}
		}
		if(close) {
			sessionLog.info("CheckHangup: Caught hangup string (" + new String(HANGUP_BYTE_OPTIONS[hangupIndex]) + "). Closing the connection...");
			throw new EOFException("Receive hangup from device.");
		}
		// haven't hit packet start
		if(buffer.limit() > buffer.position())
			sessionLog.warn("ReceiveFromClient: Filtered out " + (buffer.limit() - buffer.position()) + " garbage character(s): " + StringUtils.toHex(buffer, buffer.position(), buffer.limit() - buffer.position()));
		buffer.position(buffer.limit());
		return null;
	}

	/**
	 * @see com.usatech.networklayer.protocol.Transmission#prepareWrite(java.nio.ByteBuffer, com.usatech.networklayer.protocol.Transmission.TransmissionInfo, simple.io.Log)
	 */
	@Override
	public void prepareWrite(ByteBuffer buffer, TransmissionInfo info, Log sessionLog) throws IOException {
        if(info.getProtocolId() == 0 && info.getDeviceName().equals(ProcessingConstants.LOAD_BALANCER_MACHINE_ID)) {
        	// do not write length, do write device name
			buffer.put(info.getProtocolId());
			writeDeviceName(buffer, info.getDeviceName());
		} 	// otherwise, no need to do anything, just write the encrypted data
	}

	protected void writeDeviceName(ByteBuffer buffer, String deviceName) {
		byte[] bytes = deviceName.getBytes(ProcessingConstants.US_ASCII_CHARSET);
		if(bytes.length < 8) {
			for(int i = bytes.length; i < 8; i++)
				buffer.put((byte)0);
			buffer.put(bytes);
		} else {
			buffer.put(bytes, 0, 8);
		}
	}
	
	/**
	 * @see com.usatech.networklayer.protocol.Transmission#reset()
	 */
	@Override
	public void reset() {
	}

	/**
	 * @see com.usatech.networklayer.protocol.Transmission#getMinimumResponseDelay(com.usatech.layers.common.messagedata.MessageData, int, com.usatech.layers.common.messagedata.MessageData, com.usatech.layers.common.DeviceInfo)
	 */
	@Override
	public int getMinimumResponseDelay(MessageData inbound, int outboundIndex, MessageData outbound, DeviceInfo deviceInfo) {
		Integer delay = (outboundIndex > 0 ? minSubsequentDelays : minInitialDelays).get((int)deviceInfo.getDeviceType().getValue());
		return delay == null ? 0 : delay;
	}

	protected static final Map<Integer,Integer> minInitialDelays = new HashMap<Integer,Integer>();
	protected static final Map<Integer,Integer> minSubsequentDelays = new HashMap<Integer,Integer>();
	public static int getMinInitialDelay(int deviceType) {
		Integer delay =  minInitialDelays.get(deviceType);
		return delay == null ? 0 : delay;
	}
	public static int getMinSubsequentDelay(int deviceType) {
		Integer delay =  minSubsequentDelays.get(deviceType);
		return delay == null ? 0 : delay;
	}
	public static void setMinInitialDelay(int deviceType, int delay) {
		minInitialDelays.put(deviceType, delay);
	}
	public static void setMinSubsequentDelay(int deviceType, int delay) {
		minSubsequentDelays.put(deviceType, delay);
	}
}
