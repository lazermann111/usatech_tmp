package com.usatech.networklayer.protocol;


public class ProtocolV3 extends AbstractCryptProtocol{
    public ProtocolV3() {
		super("RIJNDAEL");
	}

	@Override
	public byte getProtocolId() {
		return 3;
	}
}
