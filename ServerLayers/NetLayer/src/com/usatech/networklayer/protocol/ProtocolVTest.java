package com.usatech.networklayer.protocol;

import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.NoSuchPaddingException;

import com.usatech.layers.common.ProcessingConstants;

public class ProtocolVTest extends AbstractCipherProtocol {

	public ProtocolVTest() throws NoSuchAlgorithmException, NoSuchPaddingException {
		super("", 1); // This doesn't actually work
	}
	@Override
	protected void afterEncrypt(ByteBuffer writeBuffer, String deviceName) {
		byte checkSum = (byte)0;
		/*
			byte[] dnBytes = deviceName.getBytes(ProcessingConstants.US_ASCII_CHARSET);
			checkSum = ProtocolV1.calcCheckSum(dnBytes, checkSum);// include evnumber in checksum
		*/
		for(int i = 3; i < writeBuffer.position(); i++) {
			checkSum += writeBuffer.get(i);
		}
		writeBuffer.put(checkSum);
	}

	@Override
	protected void beforeDecrypt(ByteBuffer readBuffer, String deviceName) throws GeneralSecurityException {
		int pos = readBuffer.position();
		byte proportedCheckSum = readBuffer.get(readBuffer.limit() - 1);
		byte sum = ProtocolV1.calcCheckSum(deviceName.getBytes(ProcessingConstants.US_ASCII_CHARSET), (byte)0);
		int limit = readBuffer.limit() - 1;
		for(int i = readBuffer.position(); i < limit; i++) {
			sum += readBuffer.get(i);
		}
		if(proportedCheckSum != sum)
			throw new GeneralSecurityException("Invalid check sum");
		readBuffer.position(pos);
		readBuffer.limit(limit);
	}
	@Override
	protected void afterDecrypt(ByteBuffer writeBuffer, String deviceName)
			throws GeneralSecurityException {
	}
	@Override
	protected void beforeEncrypt(ByteBuffer readBuffer, String deviceName)
			throws GeneralSecurityException {
	}

	public byte getProtocolId() {
		return 1;
	}

}
