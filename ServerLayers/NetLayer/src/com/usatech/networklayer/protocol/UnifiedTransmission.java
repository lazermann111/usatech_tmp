/**
 *
 */
package com.usatech.networklayer.protocol;

import java.io.IOException;
import java.nio.ByteBuffer;

import simple.io.Log;
import simple.text.StringUtils;

import com.usatech.layers.common.DeviceInfo;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.messagedata.MessageData;

/**
 * @author Brian S. Krug
 *
 */
public class UnifiedTransmission implements Transmission {
	protected byte protocolId = -1;
	protected int remaining = -1;
	protected String deviceName = null;
	/**
	 * @throws IOException
	 * @see com.usatech.networklayer.protocol.Transmission#finishWrite(java.nio.ByteBuffer, com.usatech.networklayer.protocol.Transmission.TransmissionInfo)
	 */
	@Override
	public void finishWrite(ByteBuffer buffer, TransmissionInfo info, Log sessionLog) throws IOException {
		switch(info.getProtocolId()) {
			case 0: // do nothing
				break;
			case 1: case 3: case 5: case 7: case 6: case 8: // leave 2 bytes for length, do write device name
				int length = buffer.position() - 3;
				buffer.put(1, (byte) (length >>> 8));
				buffer.put(2, (byte) (length & 255));
				break;
			default:
				throw new IOException("Unsupported Protocol " + info.getProtocolId());
		}
	}

	/**
	 * @see com.usatech.networklayer.protocol.Transmission#prepareRead(java.nio.ByteBuffer)
	 */
	@Override
	public TransmissionInfo prepareRead(ByteBuffer buffer, Log sessionLog) throws IOException {
		if(protocolId == -1) {
			protocolId = buffer.get();
			if(sessionLog.isDebugEnabled())
				sessionLog.debug("Received first byte: " + protocolId);
		}
		if(remaining == -1) {
			switch(protocolId) {
				case 0:
					remaining = 9;
					break;
				case 1: case 3: case 5: case 6: case 7: case 8:
					if(buffer.remaining() < 2) {
						return null;
					} else {
						remaining = convertToShort(buffer.get(), buffer.get());
					}
					break;
				default:
					StringBuilder sb = new StringBuilder();
					sb.append("Unsupported Protocol ").append(protocolId);
					if(sessionLog.isDebugEnabled())
						sb.append(": Data thus far\n").append(StringUtils.toHex(buffer, 0, buffer.limit()));
					throw new IOException(sb.toString());
			}
			if(sessionLog.isDebugEnabled())
				sessionLog.debug("Expecting " + remaining + " more bytes");
		}
		if(deviceName == null) {
			if(buffer.remaining() < 8)
				return null;
			byte[] bytes = new byte[8];
			buffer.get(bytes);
			deviceName = new String(bytes, ProcessingConstants.US_ASCII_CHARSET);
			remaining -= 8;
			if(sessionLog.isDebugEnabled())
				sessionLog.debug("Device identified itself as " + deviceName);
		}
		if(buffer.remaining() < remaining) {
			if(sessionLog.isInfoEnabled()) {
				sessionLog.info(deviceName + ": Received " + buffer.remaining() + " total bytes of data; Need " + (remaining - buffer.remaining()) + " more bytes");
				if(sessionLog.isDebugEnabled()) {
					byte[] bytes = new byte[buffer.remaining()];
					buffer.mark();
					buffer.get(bytes);
					buffer.reset();
					StringBuilder sb = new StringBuilder();
					sb.append(deviceName).append(": Data thus far:\n");
					StringUtils.appendHex(sb, bytes, 0, bytes.length);
					sessionLog.debug(sb.toString());
				}
			}
			return null;
		}
		ByteBuffer copyBuffer = buffer.asReadOnlyBuffer();
		if(copyBuffer.remaining() > remaining)
			copyBuffer.limit(copyBuffer.position() + remaining);
		buffer.position(copyBuffer.limit());
		return new TransmissionInfo(deviceName, protocolId, copyBuffer);
	}

	/**
	 * @throws IOException
	 * @see com.usatech.networklayer.protocol.Transmission#prepareWrite(java.nio.ByteBuffer, com.usatech.networklayer.protocol.Transmission.TransmissionInfo)
	 */
	@Override
	public void prepareWrite(ByteBuffer buffer, TransmissionInfo info, Log sessionLog) throws IOException {
		buffer.put(info.getProtocolId());
		switch(info.getProtocolId()) {
			case 0: // do not write length, do write device name
				writeDeviceName(buffer, info.getDeviceName());
				break;
			case 1: case 3: case 5: case 7: // leave 2 bytes for length, don't write device name
				buffer.position(buffer.position() + 2);
				break;
			case 6: case 8: // leave 2 bytes for length, do write device name
				buffer.position(buffer.position() + 2);
				writeDeviceName(buffer, info.getDeviceName());
				break;
			default:
				throw new IOException("Unsupported Protocol " + info.getProtocolId());
		}
	}

	/**
	 * @see com.usatech.networklayer.protocol.Transmission#reset()
	 */
	@Override
	public void reset() {
		protocolId = -1;
		remaining = -1;
		deviceName = null;
	}
	protected short convertToShort(byte b0, byte b1) {
		return (short) (b1 & 255 | (short) (b0 << 8 & 65280));
	}

	protected void writeDeviceName(ByteBuffer buffer, String deviceName) {
		byte[] bytes = deviceName.getBytes(ProcessingConstants.US_ASCII_CHARSET);
		if(bytes.length < 8) {
			for(int i = bytes.length; i < 8; i++)
				buffer.put((byte)0);
			buffer.put(bytes);
		} else {
			buffer.put(bytes, 0, 8);
		}
	}
	/**
	 * @see com.usatech.networklayer.protocol.Transmission#getMinimumResponseDelay(com.usatech.layers.common.messagedata.MessageData, int, com.usatech.layers.common.messagedata.MessageData, com.usatech.layers.common.DeviceInfo)
	 */
	@Override
	public int getMinimumResponseDelay(MessageData inbound, int outboundIndex, MessageData outbound, DeviceInfo deviceInfo) {
		return 0;
	}
}
