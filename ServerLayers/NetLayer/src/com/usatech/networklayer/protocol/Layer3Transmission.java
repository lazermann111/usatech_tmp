/**
 *
 */
package com.usatech.networklayer.protocol;

import java.io.EOFException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import simple.io.Log;
import simple.io.UUEncode;

import com.usatech.layers.common.DeviceInfo;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageData_Ping;
import com.usatech.util.Conversions;

/**
 * @author Brian S. Krug
 *
 */
public class Layer3Transmission implements Transmission {

	protected static final byte     PACKET_DELIMITER            = 0x0A;
	protected static byte loadBalancerHealthCheckProtocolId=0;
	protected String inBuffer;
	protected byte protocolId = 4;
	private static final String     HIGH_ASCII_REGEX            = "([\\x80-\\xff]+)";
	private static final String     G4_NOISE_REGEX              = "(\\x0d\\x02\\x40\\x52.*)";
    private static final String     HANGUP_REGEX_STANDARD       = "^\\x00?\\+\\+\\+";
    private static final String     HANGUP_REGEX_MULTITECH      = "^AT#CONNECTIONSTOP";
    private static final String     HANGUP_REGEX_AS5300         = "\\x9e\\x86\\x9e\\x86\\x9e\\x86";
    private static final String     HANGUP_REGEX_LANTRONIX      = "^\\xff?Logout";
    private static final String     NON_UUENCODE_REGEX          = "([^\\x20-\\x60\\x0a]+)";
    private static final String     VALID_PACKET_REGEX          = "^\\x31\\x35\\x38.{12,}\\x0a";
    private static final String     LINE_NOISE_REGEX            = "(.+?)(\\x31\\x35\\x38.*\\x0a.*)";
    private static final String     LOAD_BALANCER_PING_REGEX    = "^(\\x00)(LD-BLNCR)(.)$";

    private static final String     HANGUP_STRING_STANDARD      = "+++";
    private static final String     HANGUP_STRING_MULTITECH     = "AT#CONNECTIONSTOP";
    private static final String     HANGUP_STRING_AS5300        = "9e869e869e86";
    private static final String     HANGUP_STRING_LANTRONIX     = "Logout";

	private static Pattern highASCIIPattern= Pattern.compile(HIGH_ASCII_REGEX);
	private static Pattern G4NoisePattern = Pattern.compile(G4_NOISE_REGEX, Pattern.DOTALL);
	private static Pattern hangupPatternStandard = Pattern.compile(HANGUP_REGEX_STANDARD);
	private static Pattern hangupPatternMultitech = Pattern.compile(HANGUP_REGEX_MULTITECH);
	private static Pattern hangupPatternAS5300 = Pattern.compile(HANGUP_REGEX_AS5300);
	private static Pattern hangupPatternLantronix = Pattern.compile(HANGUP_REGEX_LANTRONIX);
	private static Pattern nonUUEncodePattern = Pattern.compile(NON_UUENCODE_REGEX);
	private static Pattern validPacketPattern = Pattern.compile(VALID_PACKET_REGEX, Pattern.DOTALL);
	private static Pattern lineNoisePattern = Pattern.compile(LINE_NOISE_REGEX, Pattern.DOTALL);
	private static Pattern loadBalancerPingPattern = Pattern.compile(LOAD_BALANCER_PING_REGEX, Pattern.DOTALL);


	/**
	 * @see com.usatech.networklayer.protocol.Transmission#finishWrite(java.nio.ByteBuffer, com.usatech.networklayer.protocol.Transmission.TransmissionInfo, simple.io.Log)
	 */
	@Override
	public void finishWrite(ByteBuffer buffer, TransmissionInfo info, Log sessionLog) throws IOException {
		if(info.getProtocolId() != 0 || !info.getDeviceName().equals(ProcessingConstants.LOAD_BALANCER_MACHINE_ID)) {
	       buffer.put(PACKET_DELIMITER);
		}
    }

	/**
	 * @see com.usatech.networklayer.protocol.Transmission#prepareRead(java.nio.ByteBuffer, simple.io.Log)
	 */
	@Override
	public TransmissionInfo prepareRead(ByteBuffer buffer, Log sessionLog) throws IOException {
		Matcher patternMatcher;
		byte[] newData=new byte[buffer.remaining()];
		buffer.get(newData);
		if(sessionLog.isDebugEnabled())
			sessionLog.debug("Received "+newData.length+ " bytes from the device.");
		if(inBuffer!=null){
			inBuffer+=new String(newData);
		}else{
			inBuffer=new String(newData);
		}
		 // load balancer health check handling
        patternMatcher = loadBalancerPingPattern.matcher(inBuffer);
        if (patternMatcher.find() == true)
        {
        	MessageData_Ping data = new MessageData_Ping();
        	data.setMessageNumber((byte)patternMatcher.group(3).charAt(0));
        	ByteBuffer bb = ByteBuffer.allocate(3);
        	data.writeData(bb, false);
        	bb.flip();
            return new TransmissionInfo(ProcessingConstants.LOAD_BALANCER_MACHINE_ID, loadBalancerHealthCheckProtocolId, bb);
        }
		StringBuilder sbLineNoise = null;
		String lineNoise;
        patternMatcher = highASCIIPattern.matcher(inBuffer);
        while (true)
        {
            if (patternMatcher.find() == true)
            {
                lineNoise = patternMatcher.group(1);

                if (sbLineNoise == null)
                    sbLineNoise = new StringBuilder(lineNoise);
                else
                    sbLineNoise.append(lineNoise);

                if (checkHangup(sessionLog, lineNoise) == true)
                {
                    lineNoise = sbLineNoise.toString();
                    sessionLog.warn("ReceiveFromClient: Filtered out " + lineNoise.length() + " high ascii character(s): " + Conversions.bytesToHex(lineNoise.getBytes()));
                    throw new EOFException("Receive hangup from device.");
                }
            }
            else
                break;
        }
        if (sbLineNoise != null)
        {
            lineNoise = sbLineNoise.toString();
            inBuffer = patternMatcher.replaceAll("");
            sessionLog.warn("ReceiveFromClient: Filtered out " + lineNoise.length() + " high ascii character(s): " + Conversions.bytesToHex(lineNoise.getBytes()));
        }
        // filter out garbage data that some ePorts produce
        patternMatcher = G4NoisePattern.matcher(inBuffer);
        if (patternMatcher.find() == true)
        {
            lineNoise = patternMatcher.group(1);
            inBuffer = patternMatcher.replaceFirst("");

            sessionLog.warn("ReceiveFromClient: Filtered out " + lineNoise.length() + " garbage character(s): " + Conversions.bytesToHex(lineNoise.getBytes()));
            if (checkHangup(sessionLog, lineNoise) == true)
            	throw new EOFException("Receive hangup from device.");
        }

        if (checkHangup(sessionLog, inBuffer) == true)
        	throw new EOFException("Receive hangup from device.");

        // filter out garbage ascii to attempt to compensate for line-noise
        if (validPacketPattern.matcher(inBuffer).find() == false)
        {
            patternMatcher = lineNoisePattern.matcher(inBuffer);
            if (patternMatcher.find() == true)
            {
                lineNoise = patternMatcher.group(1);
                inBuffer = patternMatcher.group(2);
                sessionLog.warn("ReceiveFromClient: Filtered out " + lineNoise.length() + " garbage character(s): " + Conversions.bytesToHex(lineNoise.getBytes()));
            }
        }

        if(inBuffer != null)
        {
            int delimiterPos = inBuffer.indexOf(PACKET_DELIMITER);

            if (delimiterPos == -1){
            	//message has not finished
            	return null;
            }

            // do packet validation in an effort to filter out junk data and line noise
            String receivedPacket = inBuffer.substring(0, delimiterPos + 1);
            inBuffer = inBuffer.substring(delimiterPos + 1);

            if (sessionLog.isDebugEnabled())
            {
            	sessionLog.debug("ReceiveFromClient: Received packet: " + receivedPacket.length() + " byte(s): " + Conversions.bytesToHex(receivedPacket.getBytes()));
            	sessionLog.debug("ReceiveFromClient: Input buffer: " + inBuffer.length() + " byte(s): " + Conversions.bytesToHex(inBuffer.getBytes()));
            }

            if (validPacketPattern.matcher(receivedPacket).find() == true)
            {
                // filter out non-UUEncode characters to attempt to compensate for line-noise
                StringBuilder sbNonUUEncode = null;
                patternMatcher = nonUUEncodePattern.matcher(receivedPacket);
                while (true)
                {
                    if (patternMatcher.find() == true)
                    {
                        if (sbNonUUEncode == null)
                            sbNonUUEncode = new StringBuilder(patternMatcher.group(1));
                        else
                            sbNonUUEncode.append(patternMatcher.group(1));
                    }
                    else
                        break;
                }

                if (sbNonUUEncode != null)
                {
                    lineNoise = sbNonUUEncode.toString();
                    delimiterPos -= lineNoise.length();
                    receivedPacket = patternMatcher.replaceAll("");
                    sessionLog.warn("ReceiveFromClient: Filtered out " + lineNoise.length() + " non-UUEncode character(s): " + Conversions.bytesToHex(lineNoise.getBytes()));
                }
                byte[] validBuffer = UUEncode.decode(receivedPacket.getBytes());
                String deviceName=null;
                if(validBuffer.length>8){
                	deviceName=new String(validBuffer, 0, 8);
                	if(sessionLog.isDebugEnabled())
            			sessionLog.debug("Device identified itself as " + deviceName+ ".Received valid packet from device "+ validBuffer.length + " byte(s): " + Conversions.bytesToHex(validBuffer));
                	ByteBuffer bb = ByteBuffer.allocate(validBuffer.length-8);
                	bb.put(validBuffer, 8, validBuffer.length-8);
                	bb.flip();
                	return new TransmissionInfo(deviceName, protocolId, bb);
                }else{
                	sessionLog.warn("Received invalid packet from device "+ (validBuffer.length-8) + " byte(s): " + Conversions.bytesToHex(validBuffer, 8, validBuffer.length-8));
                	throw new EOFException("End of message recevied and the message is invalid.");
                }
            }
            else
            {
                sessionLog.warn("ReceiveFromClient: Packet failed validation, discarded " + receivedPacket.length() + " garbage character(s): " + Conversions.bytesToHex(receivedPacket.getBytes()));
                return null;
            }
        }else{
        	return null;
        }
	}

	/**
	 * @see com.usatech.networklayer.protocol.Transmission#prepareWrite(java.nio.ByteBuffer, com.usatech.networklayer.protocol.Transmission.TransmissionInfo, simple.io.Log)
	 */
	@Override
	public void prepareWrite(ByteBuffer buffer, TransmissionInfo info, Log sessionLog) throws IOException {
        if(info.getProtocolId() == 0 && info.getDeviceName().equals(ProcessingConstants.LOAD_BALANCER_MACHINE_ID)) {
        	// do not write length, do write device name
			buffer.put(info.getProtocolId());
			writeDeviceName(buffer, info.getDeviceName());
		} 	// otherwise, no need to do anything, just write the encrypted data
	}

	protected void writeDeviceName(ByteBuffer buffer, String deviceName) {
		byte[] bytes = deviceName.getBytes(ProcessingConstants.US_ASCII_CHARSET);
		if(bytes.length < 8) {
			for(int i = bytes.length; i < 8; i++)
				buffer.put((byte)0);
			buffer.put(bytes);
		} else {
			buffer.put(bytes, 0, 8);
		}
	}
	
	/**
	 * @see com.usatech.networklayer.protocol.Transmission#reset()
	 */
	@Override
	public void reset() {
	}

	private static boolean checkHangup(Log log, String inBuffer)
    {
        if (inBuffer.length() < 3)
            return false;

        String hangupString = null;

        if (hangupPatternStandard.matcher(inBuffer).find() == true)
            hangupString = HANGUP_STRING_STANDARD;
        else if (hangupPatternMultitech.matcher(inBuffer).find() == true)
            hangupString = HANGUP_STRING_MULTITECH;
        else if (hangupPatternAS5300.matcher(inBuffer).find() == true)
            hangupString = HANGUP_STRING_AS5300;
        else if (hangupPatternLantronix.matcher(inBuffer).find() == true)
            hangupString = HANGUP_STRING_LANTRONIX;

        if (hangupString != null)
        {
            log.info("CheckHangup: Caught hangup string (" + hangupString + "). Closing the connection...");
            return true;
        }

        return false;
    }
	/**
	 * @see com.usatech.networklayer.protocol.Transmission#getMinimumResponseDelay(com.usatech.layers.common.messagedata.MessageData, int, com.usatech.layers.common.messagedata.MessageData, com.usatech.layers.common.DeviceInfo)
	 */
	@Override
	public int getMinimumResponseDelay(MessageData inbound, int outboundIndex, MessageData outbound, DeviceInfo deviceInfo) {
		Integer delay = minDelays.get((int)deviceInfo.getDeviceType().getValue());
		return delay == null ? 500 : delay;
	}

	protected static final Map<Integer,Integer> minDelays = new HashMap<Integer,Integer>();
	public static void setMinDelay(int deviceType, int delay) {
		minDelays.put(deviceType, delay);
	}
}
