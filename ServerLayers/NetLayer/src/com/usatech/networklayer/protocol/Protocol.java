package com.usatech.networklayer.protocol;

import java.security.GeneralSecurityException;

public interface Protocol {
	public Cryptor newCryptor() throws GeneralSecurityException;
	public byte getProtocolId() ;
}
