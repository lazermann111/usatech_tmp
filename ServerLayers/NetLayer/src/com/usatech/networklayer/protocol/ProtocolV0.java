package com.usatech.networklayer.protocol;

import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;

public class ProtocolV0 implements Protocol {
	public final static Cryptor DO_NOTHING_CRYPTOR = new Cryptor() {
		public void decrypt(ByteBuffer readBuffer, ByteBuffer writeBuffer) throws GeneralSecurityException {
			writeBuffer.put(readBuffer);
		}
		public void encrypt(ByteBuffer readBuffer, ByteBuffer writeBuffer) throws GeneralSecurityException {
			writeBuffer.put(readBuffer);
		}
		public void init(byte[] encryptionKey, String deviceName) throws GeneralSecurityException {
		}
		@Override
		public void release() {
		}
	};

	public byte getProtocolId() {
		return 0;
	}
	public Cryptor newCryptor() {
		return DO_NOTHING_CRYPTOR;
	}
}
