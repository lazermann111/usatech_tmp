package com.usatech.networklayer.protocol;


public class ProtocolV6 extends AbstractCryptProtocol {
	public ProtocolV6() {
		super("RIJNDAEL_CRC16");
	}

	@Override
	public byte getProtocolId() {
		return 6;
	}
}
