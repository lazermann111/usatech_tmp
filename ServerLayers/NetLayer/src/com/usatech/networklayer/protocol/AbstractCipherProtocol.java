package com.usatech.networklayer.protocol;

import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.concurrent.TimeoutException;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import simple.util.concurrent.AbstractLockingPool;
import simple.util.concurrent.CreationException;

public abstract class AbstractCipherProtocol implements Protocol {
	protected final String cipherName;
    protected final AlgorithmParameterSpec algorithmSpec;
    protected final AbstractLockingPool<Cryptor> cryptorPool = new AbstractLockingPool<Cryptor>() {
    	@Override
    	protected Cryptor createObject(long timeout) throws CreationException, InterruptedException {
    		return createCryptor();
    	}
    	@Override
    	protected void closeObject(Cryptor obj) throws Exception {
    		//do nothing - gc will cleanup
    	}
    };

	protected AbstractCipherProtocol(String cipherName, int blockSize) throws NoSuchAlgorithmException, NoSuchPaddingException {
		this.cipherName = cipherName;
		this.algorithmSpec = new IvParameterSpec(new byte[blockSize]);
	}

	public Cryptor newCryptor() throws GeneralSecurityException {
		try {
			return cryptorPool.borrowObject();
		} catch(TimeoutException e) {
			throw new GeneralSecurityException("Could not create cryptor before timeout", e);
		} catch(CreationException e) {
			if(e.getCause() instanceof GeneralSecurityException)
				throw (GeneralSecurityException)e.getCause();
			else
				throw new GeneralSecurityException("Could not create cryptor", e);
		} catch(InterruptedException e) {
			throw new GeneralSecurityException("Interupted while creating cryptor", e);
		}
	}

	protected Cryptor createCryptor() throws CreationException {
		final Cipher encryptCipher;
		final Cipher decryptCipher;
		try {
			encryptCipher = Cipher.getInstance(cipherName);
			decryptCipher = Cipher.getInstance(cipherName);
		} catch(NoSuchAlgorithmException e) {
			throw new CreationException("Could not get instance of cipher '" + cipherName + "'", e);
		} catch(NoSuchPaddingException e) {
			throw new CreationException("Could not get instance of cipher '" + cipherName + "'", e);
		}

		return new Cryptor() {
			protected String deviceName;
			@Override
			public void decrypt(ByteBuffer readBuffer, ByteBuffer writeBuffer)
					throws GeneralSecurityException {
				beforeDecrypt(readBuffer, deviceName);
				decryptCipher.doFinal(readBuffer, writeBuffer);
				afterDecrypt(writeBuffer, deviceName);
			}
			@Override
			public void init(byte[] key, String deviceName) throws GeneralSecurityException {
				if (key == null)
					throw new GeneralSecurityException("Key is null");
				String algo = decryptCipher.getAlgorithm();
				Key keySpec = new SecretKeySpec(key, (algo == null ? "NULL" : algo));
		        decryptCipher.init(Cipher.DECRYPT_MODE, keySpec, algorithmSpec);
		        encryptCipher.init(Cipher.ENCRYPT_MODE, keySpec, algorithmSpec);
		        this.deviceName = deviceName;
			}
			public void encrypt(ByteBuffer readBuffer, ByteBuffer writeBuffer)
					throws GeneralSecurityException {
				beforeEncrypt(readBuffer, deviceName);
				encryptCipher.doFinal(readBuffer, writeBuffer);
				afterEncrypt(writeBuffer, deviceName);
			}
			@Override
			public void release() {
				cryptorPool.returnObject(this);
			}
		};
	}

	public int getMaxActive() {
		return cryptorPool.getMaxActive();
	}

	public int getMaxIdle() {
		return cryptorPool.getMaxIdle();
	}

	public void setMaxActive(int maxActive) {
		cryptorPool.setMaxActive(maxActive);
	}

	public void setMaxIdle(int maxIdle) {
		cryptorPool.setMaxIdle(maxIdle);
	}

	public long getMaxWait() {
		return cryptorPool.getMaxWait();
	}

	public void setMaxWait(long maxWait) {
		cryptorPool.setMaxWait(maxWait);
	}

	protected abstract void beforeEncrypt(ByteBuffer readBuffer, String deviceName) throws GeneralSecurityException ;
	protected abstract void afterEncrypt(ByteBuffer writeBuffer, String deviceName) throws GeneralSecurityException ;
	protected abstract void beforeDecrypt(ByteBuffer readBuffer, String deviceName) throws GeneralSecurityException ;
	protected abstract void afterDecrypt(ByteBuffer writeBuffer, String deviceName) throws GeneralSecurityException ;

}
