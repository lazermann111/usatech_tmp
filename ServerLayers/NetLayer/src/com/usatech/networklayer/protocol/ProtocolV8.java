package com.usatech.networklayer.protocol;


public class ProtocolV8 extends AbstractCryptProtocol {
	public ProtocolV8() {
		super("AES_CBC_CRC16");
	}

	@Override
	public byte getProtocolId() {
		return 8;
	}
}
