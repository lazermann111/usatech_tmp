package com.usatech.networklayer.protocol;

import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;

import com.usatech.layers.common.ProcessingConstants;

public class ProtocolV1 implements Protocol {
	protected class CheckSumCryptor implements Cryptor {
		protected String deviceName;
		public void decrypt(ByteBuffer readBuffer, ByteBuffer writeBuffer)
				throws GeneralSecurityException {
			int pos = readBuffer.position();
			byte proportedCheckSum = readBuffer.get(readBuffer.limit() - 1);
			byte sum = calcCheckSum(deviceName.getBytes(ProcessingConstants.US_ASCII_CHARSET), (byte)0);
			int limit = readBuffer.limit() - 1;
			for(int i = readBuffer.position(); i < limit; i++) {
				sum += readBuffer.get(i);
			}
			if(proportedCheckSum != sum)
				throw new GeneralSecurityException("Invalid check sum");
			readBuffer.position(pos);
			readBuffer.limit(limit);
			writeBuffer.put(readBuffer);
		}
		public void encrypt(ByteBuffer readBuffer, ByteBuffer writeBuffer)
				throws GeneralSecurityException {
			int pos = readBuffer.position();
			byte checkSum = (byte)0;
			/*byte[] dnBytes = deviceName.getBytes(ProcessingConstants.US_ASCII_CHARSET);
			writeBuffer.put(dnBytes);
			checkSum = calcCheckSum(dnBytes, checkSum);// include evnumber in checksum
			*/
			for(int i = readBuffer.position(); i < readBuffer.limit(); i++) {
				checkSum += readBuffer.get(i);
			}
			readBuffer.position(pos);
			writeBuffer.put(readBuffer);
			writeBuffer.put(checkSum);
		}
		public void init(byte[] encryptionKey, String deviceName) throws GeneralSecurityException {
			this.deviceName = deviceName;
		}
		@Override
		public void release() {
		}
	};

	public Cryptor newCryptor() {
		return new CheckSumCryptor();
	}

	public byte getProtocolId() {
		return 1;
	}

	public static byte calcCheckSum(byte[] data, byte sum) {
		for(int i = 0; i < data.length; i++) {
			sum += data[i];
		}
		return sum;
	}
}
