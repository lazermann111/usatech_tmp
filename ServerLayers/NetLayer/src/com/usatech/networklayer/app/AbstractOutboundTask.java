package com.usatech.networklayer.app;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.io.resource.ResourceMode;
import simple.translator.Translator;
import simple.util.CollectionUtils;
import simple.util.ExcludingMap;
import simple.util.IncludingMap;

import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.DeviceInfo;
import com.usatech.layers.common.DeviceInfoManager.OnMissingPolicy;
import com.usatech.layers.common.MessageProcessingUtils;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.DeviceInfoProperty;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.FileType;
import com.usatech.layers.common.constants.SessionCloseReason;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageDataFactory;
import com.usatech.layers.common.messagedata.MessageData_C3;
import com.usatech.layers.common.messagedata.MessageData_C3.AuthorizationV2AuthResponse;
import com.usatech.layers.common.messagedata.MessageDirection;
import com.usatech.networklayer.protocol.DeviceInfoReceiver;

public abstract class AbstractOutboundTask<S extends NetworkLayerMessage> implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected static final MathContext AMOUNT_MATH_CONTEXT = new MathContext(4, RoundingMode.HALF_UP);
	
	protected abstract S getAndLockSession(long sessionId, Map<String, Object> attributes) throws ServiceException;

	protected abstract void unlockSession(S session);

	public int process(MessageChainTaskInfo taskInfo) {
		MessageChainStep step = taskInfo.getStep();
		final Map<String, Object> attributes = step.getAttributes();
		final Map<String, Object> resultAttributes = step.getResultAttributes();
		boolean updateOnly = ConvertUtils.getBooleanSafely(attributes.get("updateOnly"), false);
		boolean removeDeviceInfo = ConvertUtils.getBooleanSafely(attributes.get("removeDeviceInfo"), false);
		if(updateOnly) {
			String deviceName = ConvertUtils.getStringSafely(attributes.get("deviceName"));
			if(deviceName == null) {
				log.error("DeviceName was null for updateOnly message");
				return 255;
			} else if(removeDeviceInfo) {
				boolean removed = DeviceInfoReceiver.getInstance().removeDeviceInfo(deviceName);
				if(log.isInfoEnabled()) {
					if(removed)
						log.info("Removed device info for '" + deviceName + "' from cache because it is no longer active");
					else
						log.info("Device info for '" + deviceName + "' is not in cache and is no longer active. Nothing to do.");
				}			
			} else {
				DeviceInfo info;
				try {
					info = DeviceInfoReceiver.getInstance().getDeviceInfo(deviceName, OnMissingPolicy.RETURN_NULL, false);
				} catch(ServiceException e) {
					log.warn("Could not get Device Info for device '" + deviceName + "'", e);
					return 255;
				}
				if(info != null) {
					if(info.isInitOnly())
						log.warn("Init Only Device Info was requested to be updated. Ignoring this request");
					else {
						try {
							updateDeviceInfo(info, attributes);
						} catch(ServiceException e) {
							log.warn("Could not update device info", e);
							return 255;
						}
					}
				}
				return 0;
			}
		}
		final long sessionId = step.getAttributeSafely(CommonAttrEnum.ATTR_SESSION_ID, Long.class, -1L);
		final SessionCloseReason endSessionReason = step.getAttributeSafely(CommonAttrEnum.ATTR_END_SESSION, SessionCloseReason.class, null);
		final long fileTransferId = ConvertUtils.getLongSafely(attributes.get("fileTransferId"), 0);
		final int fileGroup = ConvertUtils.getIntSafely(attributes.get("fileGroup"), 0);
		final Long lastCommandId = ConvertUtils.convertSafely(Long.class, attributes.get("lastCommandId"), null);
		final int successResultCode;
		final int failResultCode;
		final AuthResultCode authResult = ConvertUtils.convertSafely(AuthResultCode.class, attributes.get("authResultCd"), null);
		if(authResult == AuthResultCode.APPROVED || authResult == AuthResultCode.PARTIAL || authResult == AuthResultCode.FAILED) {
			// We only need to do this for FAILED when authHoldUsed is true, but it's easier to do for all
			successResultCode = step.getResultCodes().contains(new Integer(1)) ? 1 : 0;
			failResultCode = step.getResultCodes().contains(new Integer(254)) ? 254 : 255;
		} else {
			successResultCode = 0;
			failResultCode = 255;
		}
		final S session;
		try {
			session = getAndLockSession(sessionId, attributes);
		} catch(ServiceException e) {
			log.warn("Could not get session " + Long.toHexString(sessionId).toUpperCase(), e);
			if(!taskInfo.isRedelivered()) {
				resultAttributes.put("replyTime", System.currentTimeMillis());
				resultAttributes.put("sentToDevice", false);
			}
			return failResultCode;
		}
		try {
			if(removeDeviceInfo) {
				boolean removed = DeviceInfoReceiver.getInstance().removeDeviceInfo(session.getDeviceName());
				if(log.isInfoEnabled()) {
					if(removed)
						log.info("Removed device info for '" + session.getDeviceName() + "' from cache because it is no longer active");
					else
						log.info("Device info for '" + session.getDeviceName() + "' is not in cache and is no longer active. Nothing to do.");
				}	
			}
			int requestId = ConvertUtils.getIntSafely(attributes.get("requestId"), 0);
			if(session.permitReply(requestId)) {
				DeviceType deviceType = DeviceType.DEFAULT;
				if(!removeDeviceInfo)
					try {
						DeviceInfo deviceInfo = updateDeviceInfo(session, attributes);
						if(deviceInfo != null)
							deviceType = deviceInfo.getDeviceType();
					} catch(ServiceException e) {
						log.warn("Could not update device info", e);
					}
				try {
					if(fileTransferId > 0) {						
						OutboundFileTransfer ft = session.getOutboundFileTransfer(fileGroup);
						if (ft == null) {
							int filePacketSize;
							try {
								filePacketSize = ConvertUtils.getInt(attributes.get("filePacketSize"));
							} catch(ConvertException e) {
								throw new ServiceException("Invalid file packet size", e);
							}
							long fileSize;
							try {
								fileSize = ConvertUtils.getLong(attributes.get("fileSize"));
							} catch(ConvertException e) {
								throw new ServiceException("Invalid file size", e);
							}
							FileType fileType = ConvertUtils.convertSafely(FileType.class, attributes.get("fileType"), FileType.INVALID_FILE);
							String fileTransferKey = ConvertUtils.getStringSafely(attributes.get("fileTransferKey"));
							String fileName = ConvertUtils.getStringSafely(attributes.get("fileName"));
							final long pendingCommandId = ConvertUtils.getLongSafely(attributes.get("pendingCommandId"), 0);
							Resource resource = null;
							try {
								if (fileTransferKey != null)
									resource = session.getResourceFolder().getResource(fileTransferKey, ResourceMode.READ);
								ft = new OutboundFileTransfer(resource, fileTransferId, pendingCommandId, filePacketSize, fileType, fileSize, fileName);
								session.registerOutboundFileTransfer(fileGroup, ft);
							} catch(IOException e) {
								throw new ServiceException("Could not get outbound file transfer from repository", e);
							}							
						}
						long fileChunkOffset = ConvertUtils.getLongSafely(attributes.get("fileChunkOffset"), -1);
						if (fileChunkOffset > -1) {
							byte[] fileChunk;
							try {
								fileChunk = ConvertUtils.convert(byte[].class, attributes.get("fileChunk"));
							} catch(ConvertException e) {
								throw new ServiceException("Invalid file chunk", e);
							}
							if (fileChunk == null)
								throw new ServiceException("Received null file chunk");
							ft.setFileChunk(fileChunk);
							ft.setFileChunkOffset(fileChunkOffset);
						}
					}

					final byte[] data = ConvertUtils.convertSafely(byte[].class, attributes.get("reply"), null);
					boolean sent = false;
					if(data == null || data.length == 0) {
						byte[] replyBytes = ConvertUtils.convertSafely(byte[].class, attributes.get("replyTemplate"), null);
						if(replyBytes != null) {
							try {
								MessageData replyData = MessageDataFactory.readMessage(ByteBuffer.wrap(replyBytes), MessageDirection.SERVER_TO_CLIENT, deviceType);
								populateReplyTemplates(replyData, attributes, session.getTranslator(), deviceType);
																
								// NOTICE it's workaround for case when we
								// should add card id to auth reply in field
								// authorization code. In the current moment
								// reply template framework is not support
								// appending of multiple fields in one from
								// attributes
								appendGlobalAccountIdToReply(replyData, attributes);
								sent = sendReply(session, attributes, lastCommandId, endSessionReason, replyData);
							} catch(ParseException e) {
								throw new ServiceException("Invalid reply template", e);
							}
						} else if(deviceType == DeviceType.EDGE) {
							if(endSessionReason == null)
								throw new ServiceException("No data provided ('reply' and 'replyTemplate' are empty); Reply could not be sent to device for session " + sessionId);
							sent = sendReply(session, attributes, lastCommandId, endSessionReason);
						} else {
							sent = sendReply(session, attributes, lastCommandId, endSessionReason);
						}
					} else {
						ByteBuffer buffer = ByteBuffer.wrap(data);
						MessageData replyData;
						try {
							replyData = MessageDataFactory.readMessage(buffer, MessageDirection.SERVER_TO_CLIENT, deviceType);
						} catch(BufferUnderflowException e) {
							throw new ServiceException("Invalid reply", e);
						}
						
						if(deviceType == DeviceType.EDGE || !buffer.hasRemaining()) {
							sent = sendReply(session, attributes, lastCommandId, endSessionReason, replyData);
						} else {
							List<MessageData> replyDataList = new ArrayList<MessageData>();
							replyDataList.add(replyData);
							do {
								try {
									replyData = MessageDataFactory.readMessage(buffer, MessageDirection.SERVER_TO_CLIENT, deviceType);
								} catch(BufferUnderflowException e) {
									throw new ServiceException("Invalid reply", e);
								}
								replyDataList.add(replyData);
							} while(buffer.hasRemaining()) ;
							sent = sendReply(session, attributes, lastCommandId, endSessionReason, replyDataList.toArray(new MessageData[replyDataList.size()]));
						}
					}
		
					if(!taskInfo.isRedelivered())
						resultAttributes.put("sentToDevice", sent); // If set to false auths are reversed
					return successResultCode;
				} catch(ServiceException e) {
					log.warn("Could not process reply; closing session...", e);
					session.closeSession(SessionCloseReason.ERROR);
					return failResultCode;
				} finally {
					resultAttributes.put("replyTime", System.currentTimeMillis()); // this will be the time we were ready to send the reply, not when we actually did so (for those devices that require a delay)
				}
			}
			if(log.isInfoEnabled())
				log.info("Received duplicate reply for request " + requestId + " on session " + Long.toHexString(sessionId).toUpperCase());
			return failResultCode; // Duplicate reply
		} finally {
			unlockSession(session);
		}
	}

	private void appendGlobalAccountIdToReply(MessageData replyData, Map<String, Object> attributes) {
		// see attribute AuthorityAttrEnum.ATTR_AUTH_REPLY_CARD_ID
		if (!attributes.containsKey("authReplyCardId")) {
			return;
		}

		if (!(replyData instanceof MessageData_C3)) {
			return;
		}

		MessageData_C3 replyDataC3 = (MessageData_C3) replyData;
		if (!(replyDataC3.getAuthResponseTypeData() instanceof AuthorizationV2AuthResponse)) {
			return;
		}

		AuthorizationV2AuthResponse aar2 = (AuthorizationV2AuthResponse) replyDataC3.getAuthResponseTypeData();
		Long globalAccountId = (Long) attributes.get("authReplyCardId");
		if (aar2.getAuthorizationCode() != null && aar2.getAuthorizationCode().contains("|" + globalAccountId)) {
			return;
		}

		aar2.setAuthorizationCode(aar2.getAuthorizationCode() == null ? "|" + globalAccountId
				: aar2.getAuthorizationCode() + "|" + globalAccountId);
	}	

	protected void populateReplyTemplates(MessageData replyData, Map<String, Object> attributes, Translator translator, DeviceType deviceType) throws ServiceException, ParseException {
		MessageProcessingUtils.populateReplyTemplates(replyData, attributes, translator, deviceType);
	}

	protected static final Set<String> INIT_ONLY_PROPERTIES = new HashSet<String>();
	static {
		INIT_ONLY_PROPERTIES.add(DeviceInfoProperty.DEVICE_TYPE.getAttributeKey());
		INIT_ONLY_PROPERTIES.add(DeviceInfoProperty.INIT_ONLY.getAttributeKey());
	}
	/**
	 * @param session
	 * @param attributes
	 * @throws ServiceException
	 */
	protected DeviceInfo updateDeviceInfo(S session, Map<String, Object> attributes) throws ServiceException {
		DeviceInfo info = session.getDeviceInfo();
		if(info.isInitOnly()) { // get "real" deviceInfo and allow updates of more properties
			String newDeviceName = ConvertUtils.getStringSafely(attributes.get("newDeviceName"));
			if(newDeviceName == null)
				return null;
			info = session.getNewDeviceInfo(newDeviceName);
			Map<String,Object> values = new IncludingMap<String,Object>(attributes, INIT_ONLY_PROPERTIES);
			Map<String,Long> timestamps = new HashMap<String, Long>();
			for(String key : values.keySet())
				timestamps.put(key, ConvertUtils.getLongSafely(attributes.get(key + ".timestamp"), 0L));
			info.updateInternal(values, timestamps, null);
		}
		updateDeviceInfo(info, attributes);
		return info;
	}
	protected void updateDeviceInfo(DeviceInfo info, Map<String, Object> attributes) throws ServiceException {
		Map<String,Object> values = new ExcludingMap<String,Object>(attributes, INIT_ONLY_PROPERTIES);
		long timestamp = ConvertUtils.getLongSafely(attributes.get("timestamp"), 0);
		Map<String,Long> timestamps = new HashMap<String, Long>();
		for(String key : values.keySet())
			timestamps.put(key, ConvertUtils.getLongSafely(attributes.get(key + ".timestamp"), timestamp));
		info.updateInternal(values, timestamps, null);
	}

	protected void updateSessionAttributes(S session, Map<String, Object> attributes) {
		Map<String,Object> map;
		try {
			map = CollectionUtils.uncheckedMap((Map<?,?>)attributes.get("sessionAttributes"), String.class, Object.class);
		} catch(ClassCastException e) {
			log.warn("Could not convert sessionAttributes to a Map", e);
			return;
		}
		if (map != null) {
			for(Map.Entry<String, Object> entry : map.entrySet()) {
				session.setSessionAttribute(entry.getKey(), entry.getValue());
			}
		}
	}
	
	protected void populateSessionAuthInfo(final S session, final Map<String, Object> attributes) {
		AuthResultCode authResultCode = ConvertUtils.convertSafely(AuthResultCode.class, attributes.get("authResultCd"), null);
		char paymentType = ConvertUtils.convertSafely(Character.class, attributes.get("paymentType"), 'E');
		BigDecimal authAmount = ConvertUtils.convertSafely(BigDecimal.class, attributes.get("authAmount"), BigDecimal.ZERO);
		int minorCurrencyFactor = ConvertUtils.getIntSafely(attributes.get("minorCurrencyFactor"), 100);
		if(authResultCode != null) {
			session.sentAuthResponse(authResultCode, authAmount.divide(BigDecimal.valueOf(minorCurrencyFactor), AMOUNT_MATH_CONTEXT), paymentType);
		}
	}
	
	protected abstract boolean sendReply(final S session, final Map<String, Object> attributes, final Long lastCommandId, final SessionCloseReason endSessionReason, MessageData... replyData);
}
