package com.usatech.networklayer.app;

import java.util.HashMap;
import java.util.Map;

import simple.app.AbstractWorkQueueService;
import simple.app.Processor;
import simple.app.ServiceException;
import simple.app.Work;
import simple.app.WorkQueueException;
import simple.db.StatementMonitor;

import com.usatech.layers.common.constants.MessageType;

public class InboundNetworkLayerService extends AbstractWorkQueueService<NetworkLayerMessage> {
	//private static final Log log = Log.getLog();
	protected final WorkProcessor<NetworkLayerMessage> commonProcessor = new WorkProcessor<NetworkLayerMessage>() {
		public boolean interruptProcessing(Thread thread) {
			StatementMonitor.cancelStatements(thread);
			thread.interrupt();
			return true;
		}

		public void processWork(final Work<NetworkLayerMessage> work) throws ServiceException, WorkQueueException {
			final NetworkLayerMessage message = work.getBody();
			final MessageType messageType = message.getData().getMessageType();
			Processor<NetworkLayerMessage, Void> processor = getProcessor(messageType);
			if(processor == null) {
				processor = getDefaultProcessor();
				if(processor == null)
					throw new ServiceException("Could not find processor for message type '" + messageType + "'");
			}
			processor.process(message);
		}
	};
	protected Map<String,Processor<NetworkLayerMessage, Void>> processors = new HashMap<String, Processor<NetworkLayerMessage, Void>>();
	protected Processor<NetworkLayerMessage, Void> defaultProcessor;

	public InboundNetworkLayerService(String serviceName) {
		super(serviceName);
	}

	@Override
	protected WorkProcessor<NetworkLayerMessage> getWorkProcessor() {
		return commonProcessor;
	}

	public Processor<NetworkLayerMessage, Void> getProcessor(MessageType messageType) {
		return processors.get(messageType.getHex());
	}
	public void setProcessor(String messageType, Processor<NetworkLayerMessage, Void> processor) {
		processors.put(messageType, processor);
	}

	public Processor<NetworkLayerMessage, Void> getDefaultProcessor() {
		return defaultProcessor;
	}

	public void setDefaultProcessor(Processor<NetworkLayerMessage, Void> defaultProcessor) {
		this.defaultProcessor = defaultProcessor;
	}
}
