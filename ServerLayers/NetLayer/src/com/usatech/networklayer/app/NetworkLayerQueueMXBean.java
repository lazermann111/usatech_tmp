package com.usatech.networklayer.app;

import simple.io.server.NIOServerMXBean;

public interface NetworkLayerQueueMXBean extends NIOServerMXBean {

	public long getSessionCount();

	public String getSessionsList();

}