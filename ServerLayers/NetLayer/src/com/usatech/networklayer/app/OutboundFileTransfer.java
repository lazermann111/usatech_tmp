/**
 *
 */
package com.usatech.networklayer.app;

import java.io.IOException;
import java.io.InputStream;

import simple.io.BinaryStream;
import simple.io.ByteOutput;
import simple.io.IOUtils;
import simple.io.resource.Resource;

import com.usatech.layers.common.constants.FileType;

/**
 * @author Brian S. Krug
 *
 */
public class OutboundFileTransfer implements BinaryStream {		
	protected final Resource resource;
	protected final long fileTransferId;
	protected final long pendingCommandId;
	protected final InputStream in;
	protected long bytesSent;
	protected int packetsSent;
	protected final int packetSize;
	protected int lastLength;
	protected final FileType fileType;
	protected final long fileSize;
	protected final String fileName;
	protected byte[] fileChunk = null;
	protected long fileChunkOffset = 0;
	/**
	 * @param resource
	 * @throws IOException
	 */
	public OutboundFileTransfer(Resource resource, long fileTransferId, long pendingCommandId, int packetSize, FileType fileType, long fileSize, String fileName) throws IOException {
		this.resource = resource;
		this.fileTransferId = fileTransferId;
		this.pendingCommandId = pendingCommandId;
		this.in = resource == null ? null : resource.getInputStream();
		this.packetSize = packetSize;
		this.fileType = fileType;
		this.fileSize = fileSize;
		this.fileName = fileName;
	}
	public String getFileName() {
		return fileName;
	}
	public boolean cancel() {
		if(resource != null) {
			resource.release();
			return true;
		}
		return false;
	}
	public long getFileTransferId() {
		return fileTransferId;
	}
	public long getPendingCommandId() {
		return pendingCommandId;
	}
	public long getFileSize() {
		return fileSize;
	}
	public void finish() throws IOException {
		if(resource != null) {
			in.close();
			resource.delete();
			resource.release();
		}
	}
	/**
	 * @return
	 */
	public long getBytesRemaining() {
		return fileSize - bytesSent;
	}
	/**
	 * @param reply
	 * @param offset
	 * @param i
	 * @throws IOException
	 */
	public void read(byte[] data, long offset, int length) throws IOException {
		if (in == null) {
			System.arraycopy(fileChunk, (int) (offset - fileChunkOffset), data, 0, length);
			bytesSent += length;
		} else {
			in.mark(length + 1);
			while(length > 0) {
				int r = in.read(data, (int) offset, length);
				if(r > 0) {
					length -= r;
					offset += r;
					bytesSent += r;
				} else {
					throw new IOException("Could not read from resource (r=" + r + "; offset=" + offset + "; length=" + length + ")");
				}
			}
		}
		lastLength = length;
		packetsSent++;
	}
	public void retry() throws IOException {
		if (in != null)
			in.reset();
		packetsSent--;
		bytesSent -= lastLength;
	}
	/**
	 * @return
	 */
	public int getPacketsSent() {
		return packetsSent;
	}
	public void setPacketsSent(int packetsSent) {
		this.packetsSent = packetsSent;
	}
	/**
	 * @return
	 */
	public int getPacketsSize() {
		return packetSize;
	}
	
	@Override
	public InputStream getInputStream() throws IOException {
		return in;
	}
	@Override
	public long getLength() {
		return fileSize;
	}
	@Override
	public long copyInto(ByteOutput output) throws IOException {
		if (in == null)
			return 0;
		else
			return IOUtils.copy(in, output, 0L, getLength());
	}
	public FileType getFileType() {
		return fileType;
	}
	public long getBytesSent() {
		return bytesSent;
	}
	public void setBytesSent(long bytesSent) {
		this.bytesSent = bytesSent;
	}
	public byte[] getFileChunk() {
		return fileChunk;
	}
	public void setFileChunk(byte[] fileChunk) {
		this.fileChunk = fileChunk;
	}
	public long getFileChunkOffset() {
		return fileChunkOffset;
	}
	public void setFileChunkOffset(long fileChunkOffset) {
		this.fileChunkOffset = fileChunkOffset;
	}
	public int getFileChunkLength() {
		return fileChunk == null ? 0 : fileChunk.length;
	}	
	public int getLastLength() {
		return lastLength;
	}
	public void setLastLength(int lastLength) {
		this.lastLength = lastLength;
	}
}
