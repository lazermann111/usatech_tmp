package com.usatech.networklayer.app;

import java.util.Map;

import simple.app.ServiceException;
import simple.event.RunnableListener;

import com.usatech.layers.common.constants.SessionCloseReason;
import com.usatech.layers.common.messagedata.MessageData;

public class OutboundNetworkLayerTask extends AbstractOutboundTask<NetworkLayerMessage> {
	@Override
	protected NetworkLayerMessage getAndLockSession(long sessionId, Map<String, Object> attributes) throws ServiceException {
		return NetworkLayerQueue.getAndLockSession(sessionId);
	}
	@Override
	protected void unlockSession(NetworkLayerMessage session) {
		 NetworkLayerQueue.unlockSession(session);
	}
		
	protected boolean sendReply(final NetworkLayerMessage session, final Map<String,Object> attributes, final Long lastCommandId, final SessionCloseReason endSessionReason, MessageData... replyData) {
		if(replyData != null && replyData.length > 0) {
			for(MessageData md : replyData) {
				if(md.getMessageType().isAuthResponse()) {
					populateSessionAuthInfo(session, attributes);
					break;
				}
			}
		}
		if(endSessionReason != null)
			return session.sendReplyAndClose(endSessionReason, replyData);
		return session.sendReply(new RunnableListener() {
			public void runCompleted() {
				updateSessionAttributes(session, attributes);
				session.setLastCommandId(lastCommandId);
			}
			public void runFailed(Throwable throwable) {
				// no need to do anything
			}
		}, replyData);	
	}
}
