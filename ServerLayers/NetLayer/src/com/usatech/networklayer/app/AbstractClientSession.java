package com.usatech.networklayer.app;

import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.ByteChannel;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Pattern;

import simple.app.QoS;
import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.event.RunnableListener;
import simple.io.BinaryStream;
import simple.io.ByteArrayByteOutput;
import simple.io.EnhancedBufferedReader;
import simple.io.Log;
import simple.io.logging.AbstractPrefixedLog;
import simple.io.resource.Resource;
import simple.io.server.AbstractBufferInterpretter;
import simple.security.crypt.EncryptionInfoMapping;
import simple.text.StringUtils;
import simple.translator.Translator;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.DeviceInfo;
import com.usatech.layers.common.DeviceInfoManager;
import com.usatech.layers.common.DeviceInfoManager.OnMissingPolicy;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.AuthResponseActionCode;
import com.usatech.layers.common.constants.AuthResponseType;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.DebugLevel;
import com.usatech.layers.common.constants.DeviceProperty;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.FileType;
import com.usatech.layers.common.constants.GenericResponseResultCode;
import com.usatech.layers.common.constants.GenericResponseServerActionCode;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.SessionCloseReason;
import com.usatech.layers.common.constants.SessionControlAction;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageDataFactory;
import com.usatech.layers.common.messagedata.MessageData_83;
import com.usatech.layers.common.messagedata.MessageData_88;
import com.usatech.layers.common.messagedata.MessageData_8E;
import com.usatech.layers.common.messagedata.MessageData_8F;
import com.usatech.layers.common.messagedata.MessageData_AD;
import com.usatech.layers.common.messagedata.MessageData_C0;
import com.usatech.layers.common.messagedata.MessageData_C3;
import com.usatech.layers.common.messagedata.MessageData_C3.CallInCommand;
import com.usatech.layers.common.messagedata.MessageData_C7;
import com.usatech.layers.common.messagedata.MessageData_C8;
import com.usatech.layers.common.messagedata.MessageData_C9;
import com.usatech.layers.common.messagedata.MessageData_CA;
import com.usatech.layers.common.messagedata.MessageData_CB;
import com.usatech.layers.common.messagedata.MessageData_CB.PropertyValueListAction;
import com.usatech.layers.common.messagedata.MessageData_CB.ReconnectAction;
import com.usatech.layers.common.messagedata.MessageData_CB.ServerActionCodeData;
import com.usatech.layers.common.messagedata.MessageDirection;
import com.usatech.networklayer.protocol.Cryptor;
import com.usatech.networklayer.protocol.Protocol;
import com.usatech.networklayer.protocol.Transmission;
import com.usatech.networklayer.protocol.Transmission.TransmissionInfo;

public abstract class AbstractClientSession extends AbstractBufferInterpretter implements NetworkLayerMessage {
	private static final Log log = Log.getLog();
	protected final static String GLOBAL_SESSION_CODE_PREFIX = ProcessingConstants.GLOBAL_EVENT_CODE_PREFIX + ':' + ManagementFactory.getRuntimeMXBean().getName() + ':' + Long.toHexString(System.currentTimeMillis()).toUpperCase() + ':';
	protected final static AtomicLong sessionIDGenerator = new AtomicLong(Double.doubleToLongBits(Math.random()));
	
	protected final static Future<?> NOTHING_REAPER = new Future<Object>() {
		public boolean cancel(boolean mayInterruptIfRunning) {
			return true;
		}

		public Object get() throws InterruptedException, ExecutionException {
			return null;
		}

		public Object get(long timeout, TimeUnit unit) throws InterruptedException,
				ExecutionException, TimeoutException {
			return null;
		}

		public boolean isCancelled() {
			return true;
		}

		public boolean isDone() {
			return false;
		}
	};
	protected static class FileTransferKey {
		protected final String deviceName;
		protected final long fileTransferId;
		public FileTransferKey(String deviceName, long fileTransferId) {
			super();
			this.deviceName = deviceName;
			this.fileTransferId = fileTransferId;
		}
		public String getDeviceName() {
			return deviceName;
		}
		public long getFileTransferId() {
			return fileTransferId;
		}
		@Override
		public int hashCode() {
			return (int) (fileTransferId % 1000000000);
		}
		@Override
		public boolean equals(Object obj) {
			if(!(obj instanceof FileTransferKey)) return false;
			FileTransferKey ftk = (FileTransferKey)obj;
			return ftk.fileTransferId == fileTransferId && ConvertUtils.areEqual(ftk.deviceName, deviceName);
		}
	}
	protected final ReentrantLock lock = new ReentrantLock();
	protected final ByteBuffer writeBuffer;
	protected final ByteBuffer writeBuffer2;
	protected TransmissionInfo transmissionInfo;
	protected Protocol protocol;
	protected Cryptor cryptor;
	protected int messageNumberUnsigned = 0;
	protected int previousMessageNumberUnsigned = 255;
	protected int messageRetryCounter = 0;
	protected MessageType previousMessageType = null;
	protected long messageStartTime;
	protected String prevDeviceName;
	protected DeviceInfo deviceInfo;
	protected byte[] encryptionKey;
	protected boolean prevEncKeyUseDetected = false;
	protected MessageData data;
	protected int messageLength;
	protected long bytesReceived;
	protected long bytesSent;
	protected int messagesReceived;
	protected int messagesSent;
	protected int replyCount;
	protected final long sessionStartTime;
	protected final long sessionId;
	protected final String globalSessionCode;
	protected final Socket socket;
	protected final ByteChannel byteChannel;
	protected final InetSocketAddress remoteAddress;
	protected final String remoteAddressString;
	protected Future<?> sessionReaper;
	protected Map<FileTransferKey, InboundFileTransfer> inboundFileTransfers  = null; // most sessions will not need this
	protected Map<FileTransferKey, OutboundFileTransfer> outboundFileTransfers  = null; // most sessions will not need this
	protected Long lastCommandId = null;
	protected Map<String,Object> sessionAttributes;
	protected String deviceSerialCd;
	protected String deviceName = null;
	protected Pattern deviceNamePattern;
	protected Pattern deviceLimitedActivityPattern;
	protected boolean messageNumberChecked;
	protected int messageRetriesAllowed;
	protected boolean serverSessionStarted = false;
	protected boolean deviceInitialized = false;
	protected boolean closed = false;
	protected boolean sessionStartSent = false;
	protected Set<String> serverSessionMessages;
	protected final Log messageLog;
	protected final Log sessionLog;
	protected DebugLevel debugLevel;
	protected MessageChain messageChain;
	protected final Transmission transmission;
	protected int pendingRequestId = 0;
	protected boolean deviceSentConfig = false;
	protected boolean serverSentConfig = false;
	protected long dexFileSize = 0;
	protected String dexStatus;
	protected Number authAmount;
    protected AuthResultCode authResultCode;
    protected Character paymentType;
    protected Long authTime;
    protected boolean nonAuthMessage = false;
    protected boolean recordDeviceData = true;
    protected int saleCount = 0;
    protected int fileCount = 0;
    protected int eventCount = 0;
    protected SessionCloseReason sessionCloseReason = SessionCloseReason.UNKNOWN;
	protected final AtomicInteger messageSequence = new AtomicInteger();
    
	public AbstractClientSession(Socket socket, ByteChannel byteChannel, int maxInputSize) throws IOException {
		super(maxInputSize); // real limit is 32767 + 3
		this.sessionStartTime = System.currentTimeMillis();
		this.socket = socket;
		this.byteChannel = byteChannel;
		this.remoteAddress = (InetSocketAddress) socket.getRemoteSocketAddress();
		this.remoteAddressString = remoteAddress.toString();
		this.writeBuffer = ByteBuffer.allocate(maxInputSize);
		this.writeBuffer.order(ByteOrder.BIG_ENDIAN);
		this.writeBuffer2 = ByteBuffer.allocate(maxInputSize);
		this.writeBuffer2.order(ByteOrder.BIG_ENDIAN);
		this.bufferReadOnly.order(ByteOrder.BIG_ENDIAN);
		this.sessionId = sessionIDGenerator.getAndIncrement();
		this.globalSessionCode = GLOBAL_SESSION_CODE_PREFIX + Long.toHexString(sessionId).toUpperCase();
		this.sessionReaper = NOTHING_REAPER;
		this.deviceNamePattern = getDeviceNamePattern();
		this.deviceLimitedActivityPattern = getDeviceLimitedActivityPattern();
		this.messageNumberChecked = isMessageNumberChecked();
		this.messageRetriesAllowed = getMessageRetriesAllowed();
		this.serverSessionMessages = getServerSessionMessages();
		this.debugLevel = getDebugLevel();
		this.messageLog = new AbstractPrefixedLog(log) {
			protected StringBuilder sb = new StringBuilder();
			/**
			 * @see simple.io.logging.AbstractPrefixedLog#prefixMessage(java.lang.Object)
			 */
			@Override
			protected Object prefixMessage(Object message) {
				sb.setLength(0);
				if(messageChain != null) {
					sb.append('\'').append(messageChain.getGuid()).append("' ");
				}
				sb.append('[');
				if(deviceInfo != null && deviceInfo.getDeviceType() != DeviceType.DEFAULT) {
					sb.append(deviceInfo.getDeviceType());
					sb.append(" ");
				}
				sb.append(getDeviceName()).append(" @ ").append(globalSessionCode).append(" #").append(messageNumberUnsigned).append("] ").append(data == null ? "UNKNOWN" : data.getMessageType().getDescription()).append(": ");
				sb.append(message);
				return sb.toString();
			}
		};
		final String sessionLogPrefix = "[" + globalSessionCode + " from " + remoteAddressString + " on port " + socket.getLocalPort() + "]: ";
		this.sessionLog = new AbstractPrefixedLog(log) {
			/**
			 * @see simple.io.logging.AbstractPrefixedLog#prefixMessage(java.lang.Object)
			 */
			@Override
			protected Object prefixMessage(Object message) {
				return sessionLogPrefix + message;
			}
		};
		this.transmission = newTransmission();
		resetReaper(getSessionTimeout());
		addSession();
	}

	@Override
	public void close(boolean clientInitiated) {
		lock.lock();
		try {
			if(clientInitiated && !closed && sessionCloseReason == SessionCloseReason.UNKNOWN)
				sessionCloseReason = SessionCloseReason.CLIENT_REQUESTED;
			closed = true;
			if(removeSession() && deviceInfo != null && messagesReceived > 0) {
				processingComplete();
				if(prevDeviceName != null && !deviceLimitedActivityPattern.matcher(prevDeviceName).matches()) {
					try {
						enqueue(constructSessionEndMessageChain(), false);
					} catch(ServiceException e) {
						sessionLog.warn("Could not enqueue Session End Message", e);
					}
				}
			}
			if(cryptor != null) {
				cryptor.release();
				cryptor = null;
			}
			// clean up file transfers if any
			if(inboundFileTransfers != null) {
				Iterator<Map.Entry<FileTransferKey, InboundFileTransfer>> iter = inboundFileTransfers.entrySet().iterator();
				while(iter.hasNext()) {
					Map.Entry<FileTransferKey, InboundFileTransfer> entry = iter.next();
					InboundFileTransfer ft = entry.getValue();
					boolean success = ft.cancel();
					if(sessionLog.isDebugEnabled())
						sessionLog.debug((success ?
								"Deleted file '" + ft.getFileName() + "' before sending to app layer because device " + entry.getKey().getDeviceName() + " abandoned it"
								: "Could not delete file '" + ft.getFileName() + "' from device " + entry.getKey().getDeviceName()));
					iter.remove();
				}
			}
			// clean up file transfers if any
			if(outboundFileTransfers != null) {
				Iterator<Map.Entry<FileTransferKey, OutboundFileTransfer>> iter = outboundFileTransfers.entrySet().iterator();
				while(iter.hasNext()) {
					Map.Entry<FileTransferKey, OutboundFileTransfer> entry = iter.next();
					OutboundFileTransfer ft = entry.getValue();
					ft.cancel();
					if(sessionLog.isDebugEnabled())
						sessionLog.debug("Deleted file '" + ft.getFileName() + "' before sending to device " + entry.getKey().getDeviceName() + " because session closed");
					iter.remove();
				}
			}			
		} finally {
			lock.unlock();
		}
	}

	public void flushOutput() {
		// do nothing
	}

	public boolean prepareCurrent() throws IOException {
		lock.lock();
		try {
			if (deviceNamePattern.matcher(transmissionInfo.getDeviceName()).matches() == false) {
				sessionLog.warn("Received invalid device name: " + transmissionInfo.getDeviceName() + ", device name hex: " + StringUtils.toHex(transmissionInfo.getDeviceName()));
				return false;
			}
			if(deviceInfo == null || !transmissionInfo.getDeviceName().equals(prevDeviceName))
				try {
					deviceInfo = getDeviceInfoManager().getDeviceInfo(transmissionInfo.getDeviceName(), OnMissingPolicy.THROW_EXCEPTION, false);
					prevDeviceName = transmissionInfo.getDeviceName();
					encryptionKey = deviceInfo.getEncryptionKey();
				} catch(ServiceException e) {
					throw newIOException("Could not get device info for device " + transmissionInfo.getDeviceName(), e);
				}
			if(protocol == null || transmissionInfo.getProtocolId() != protocol.getProtocolId()) {
				protocol = getProtocol(transmissionInfo.getProtocolId());
				if(cryptor != null) {
					cryptor.release();
					cryptor = null;
				}
				try {
					cryptor = protocol.newCryptor();
				} catch(GeneralSecurityException e) {
					IOException ioe = new IOException("Could not create cryptor for protocol " + protocol);
					ioe.initCause(e);
					throw ioe;
				}
			}
			messageStartTime = System.currentTimeMillis();
			cancelReaper(sessionReaper, true);
			int transmissionLength = transmissionInfo.getData().limit();
			if(sessionLog.isDebugEnabled())
				sessionLog.debug(transmissionInfo.getDeviceName() + ": Received message of " + transmissionLength + " bytes");
			bytesReceived += transmissionLength;
			messagesReceived++;
			messageChain = new MessageChainV11();
			writeBuffer.clear();
			transmissionInfo.getData().mark();
			try {
				decrypt(cryptor, encryptionKey, transmissionInfo, writeBuffer);
			} catch(GeneralSecurityException e) {
				final GeneralSecurityException gse = e;
				boolean decryptionSuccess = false;
				// try previous
				byte[] ek1 = encryptionKey;
				byte[] ek2 = deviceInfo.getPreviousEncryptionKey();
				if(ek2 != null && !Arrays.equals(ek1, ek2)) {
					sessionLog.info(transmissionInfo.getDeviceName() + ": Attempting decryption with previous key...");
					transmissionInfo.getData().reset();
					encryptionKey = ek2;
					try {
						decrypt(cryptor, encryptionKey, transmissionInfo, writeBuffer);
						decryptionSuccess = true;
						sessionLog.info(transmissionInfo.getDeviceName() + ": Decrypted message using previous key");
						prevEncKeyUseDetected = true;
						if(messagesReceived == 1)
							setSessionAttribute("prevEncKeyUseDetected", true);
					} catch(GeneralSecurityException e1) {
						log.warn("Could not decrypt with previous encryption key", e1);
					}
				}
				if(!decryptionSuccess) {
				//if(messagesReceived == 1) {
					sessionLog.info(transmissionInfo.getDeviceName() + ": Decryption failed, refreshing device info...");
					// attempt to get new key
					try {
						deviceInfo = getDeviceInfoManager().getDeviceInfo(transmissionInfo.getDeviceName(), OnMissingPolicy.THROW_EXCEPTION, true);
					} catch(ServiceException e1) {
						throw newIOException(transmissionInfo.getDeviceName() + ": Could not get device info", e1);
					}
					if(deviceInfo.getRejectUntil() > System.currentTimeMillis()) {
						sessionLog.warn("Device " + transmissionInfo.getDeviceName() + " is disabled until " + new Date(deviceInfo.getRejectUntil()));
						return false;
					}
					if(!Arrays.equals(deviceInfo.getEncryptionKey(), ek1) && (ek2 == null || !Arrays.equals(deviceInfo.getEncryptionKey(), ek2))) {
						transmissionInfo.getData().reset();
						encryptionKey = deviceInfo.getEncryptionKey();
						try {
							decrypt(cryptor, encryptionKey, transmissionInfo, writeBuffer);
							decryptionSuccess = true;
						} catch(GeneralSecurityException e1) {
							log.warn("Could not decrypt with fresh encryption key", e1);
							sessionLog.info(transmissionInfo.getDeviceName() + ": Decryption with refreshed key failed");
						}
					}
				//}
				}
				if(!decryptionSuccess) {					
					if (encryptionKey == null) {
						this.sessionCloseReason = SessionCloseReason.INACTIVE_DEVICE;
						sessionLog.warn("Encryption key not found for device " + transmissionInfo.getDeviceName());
						return false;
					}
					this.sessionCloseReason = SessionCloseReason.DECRYPTION_FAILURE;
					throw newIOException(transmissionInfo.getDeviceName() + ": Could not decrypt data with cryptor " + cryptor, gse);
			}
			}
			writeBuffer.flip();
			if(sessionLog.isDebugEnabled())
				sessionLog.debug(transmissionInfo.getDeviceName() + ": Decrypted into " + writeBuffer.remaining() + " bytes");
			try {
				data = MessageDataFactory.readMessage(writeBuffer, MessageDirection.CLIENT_TO_SERVER, deviceInfo.getDeviceType());
			} catch(BufferUnderflowException e) {
				this.sessionCloseReason = SessionCloseReason.ERROR;
				throw newIOException(transmissionInfo.getDeviceName() + ": Invalid message, not enough bytes", e);
			}
			if(getLog().isInfoEnabled())
				getLog().info(transmissionInfo.getDeviceName() + ": Received " + data + " (" + transmissionLength + " bytes) from " + getRemoteAddressString());
			if(data.getMessageType().isAuthRequest()) {
				authTime = messageStartTime;				
			} else {
				nonAuthMessage = true;
			}
			
			long rejectMs = deviceInfo.getRejectUntil() - System.currentTimeMillis();
			if (rejectMs > 0) {
				recordDeviceData = isRecordDeviceRejections();
				sessionLog.warn(transmissionInfo.getDeviceName() + ": Device messages are rejected due to flooding until " + new Date(deviceInfo.getRejectUntil()));
				MessageData replyData = null;
				switch (deviceInfo.getDeviceType()) {
					case EDGE:
						MessageData_CB cbData = new MessageData_CB();
						cbData.setResultCode(GenericResponseResultCode.SUSPENDED);
						cbData.setServerActionCode(GenericResponseServerActionCode.REBOOT);
						long reconnectTime = rejectMs / 1000 + 5 * 60;
						((ReconnectAction) cbData.getServerActionCodeData()).setReconnectTime((int) (reconnectTime > 65535 ? 0 : reconnectTime));
						replyData = cbData;
						break;
					case ESUDS:
						MessageData_83 legacy83Data = new MessageData_83();
						legacy83Data.setRequestNumber((byte) 1); // Reboot (Hardware)
						replyData = legacy83Data;
						break;
				}
				if (replyData != null) {
					getLog().info("Detected device spamming, sending reboot request " + replyData.getMessageType() + " and closing connection");
					sendReply(null, SessionCloseReason.INACTIVE_DEVICE, replyData);
				}
				return false;
			}
			
			// check if it's a Generic Request with Update Status Request Type
			if(data instanceof MessageData_CA) {
				MessageData_CA genReqData = (MessageData_CA)data;
				switch(genReqData.getRequestType()) {
					case UPDATE_STATUS_REQUEST: case ACTIVATION_REQUEST:
						serverSessionStarted = true;
						break;
				}
			} else if (data.getMessageType().isInitMessage()) {				
				if(data instanceof MessageData_C0)
					deviceSerialCd = ((MessageData_C0)data).getDeviceSerialNum();
				else if(data instanceof MessageData_8E)
					deviceSerialCd = ((MessageData_8E)data).getDeviceSerialNum();
				else if(data instanceof MessageData_AD)
					deviceSerialCd = ((MessageData_AD)data).getDeviceSerialNum();
			}
			if(transmissionInfo.getProtocolId() == 1) {
				switch(data.getMessageType()) {
					case PING: case APPLAYER_HEALTH_CHECK: case NETLAYER_HEALTH_CHECK: case QUEUE_TIMING_CHECK:
						break; // these are okay
					default: // the rest are not okay
						sessionLog.warn(transmissionInfo.getDeviceName() + ": Received a not allowed protocol 1 message " + data.getMessageType()+" deviceType:"+deviceInfo.getDeviceType());
						return false;
				}
			}
			if(isMessageNumberValidationNeeded(deviceInfo.getDeviceType(), data.getMessageType())){
				messageNumberUnsigned = data.getMessageNumber() & 0xFF;
				if(messageNumberChecked == true && !transmissionInfo.getDeviceName().equals(ProcessingConstants.LOAD_BALANCER_MACHINE_ID)) {
					if(messagesReceived == 1 && messageNumberUnsigned != 0) {
						sessionLog.warn(transmissionInfo.getDeviceName() + ": Received a non-zero messageNumber " + messageNumberUnsigned + " in the first message in the session");
						return false;
					}
	
					if(messageNumberUnsigned == previousMessageNumberUnsigned) {
						messageRetryCounter++;
						if(messageRetryCounter > messageRetriesAllowed) {
							sessionLog.warn(transmissionInfo.getDeviceName() + ": messageRetryCounter " + messageRetryCounter + " for messageNumber " + messageNumberUnsigned + " exceeded messageRetriesAllowed " + messageRetriesAllowed);
							return false;
						}
					} else {
						messageRetryCounter = 0;
						if(messageNumberUnsigned == 0 && previousMessageNumberUnsigned != 255 || messageNumberUnsigned != 0 && messageNumberUnsigned != previousMessageNumberUnsigned + 1) {
							sessionLog.warn(transmissionInfo.getDeviceName() + ": Received invalid messageNumber " + messageNumberUnsigned + ", previous messageNumber was " + previousMessageNumberUnsigned);
							return false;
						}
					}
				}
			}
			if(deviceInfo.isInitOnly() && !data.getMessageType().isInitMessage() && !data.getMessageType().isHealthCheck()
					&& data.getMessageType() != MessageType.CLIENT_TO_SERVER_REQUEST /* For esuds which does time sync using default device name */) {
				sessionLog.warn(transmissionInfo.getDeviceName() + ": Received non-init message '" + data.getMessageType() + "' on " + remoteAddressString);
				return false;
			}
			writeBuffer.flip();
			messageLength = writeBuffer.remaining();
	
			// Check message Id
			if(data.getMessageType().isMessageIdExpected()) {
				if(data.getMessageType().isInitMessage()) {
					if(isInitMessageIdChecked() && !checkMessageId(deviceSerialCd, data.getMessageId())) {
						sessionLog.warn(deviceSerialCd + ": Invalid message ID: " + data.getMessageId());
						return false;
					}
				} else {
					if(isNoninitMessageIdChecked() && !checkMessageId(transmissionInfo.getDeviceName(), data.getMessageId())) {
						sessionLog.warn(prevDeviceName + ": Invalid message ID: " + data.getMessageId());
						return false;
					}
				}
			}
	
			// validate process flow
			if(serverSessionStarted == false && serverSessionMessages != null && serverSessionMessages.contains(data.getMessageType().getHex())) {
				boolean validProcessFlow = false;
				switch(data.getMessageType()) {
					case COMPONENT_ID_4_1:
						validProcessFlow = (previousMessageType == MessageType.INITIALIZATION_4_1);
						break;
					case SHORT_FILE_XFER_4_1:
						validProcessFlow = (previousMessageType == MessageType.INITIALIZATION_4_1 && ((MessageData_C7) data).getFileType() == FileType.GENERIC_DEVICE_INFORMATION);
						break;
					case FILE_XFER_START_4_1:
						validProcessFlow = (previousMessageType == MessageType.INITIALIZATION_4_1 && ((MessageData_C8) data).getFileType() == FileType.GENERIC_DEVICE_INFORMATION);
						break;
					case FILE_XFER_4_1:
						InboundFileTransfer ft = getInboundFileTransfer(((MessageData_C9) data).getGroup());
						validProcessFlow = (ft != null && ft.getFileTypeId() == FileType.GENERIC_DEVICE_INFORMATION.getValue());
						break;
				}
	
				if(!validProcessFlow) {
					sessionLog.warn(transmissionInfo.getDeviceName() + ": Received a server session message '" + data.getMessageType() + "' on " + remoteAddressString
							+ " but a server session has not been started properly");
					return false;
				}
			}
	
			previousMessageType = data.getMessageType();
			previousMessageNumberUnsigned = messageNumberUnsigned;
		} finally {
			lock.unlock();
		}
		return true;
	}

	protected abstract boolean isRecordDeviceRejections();

	public void processingComplete() {
		if(messagesReceived == 1 && sessionStartSent == false) {
			sessionStartSent = true;
			if(transmissionInfo.getDeviceName() != null && !deviceLimitedActivityPattern.matcher(transmissionInfo.getDeviceName()).matches())
				try {
					enqueue(constructSessionStartMessageChain(), false);
				} catch(ServiceException e) {
					sessionLog.warn(transmissionInfo.getDeviceName() + ": Could not enqueue Session Start Message", e);
				}
		}
	}
	
	protected static void decrypt(Cryptor cryptor, byte[] encryptionKey, TransmissionInfo transmissionInfo, ByteBuffer writeBuffer) throws GeneralSecurityException {
		cryptor.init(encryptionKey, transmissionInfo.getDeviceName());
		cryptor.decrypt(transmissionInfo.getData(), writeBuffer);
	}
	
	protected static IOException newIOException(String message, Throwable cause) {
		IOException ioe = new IOException(message);
		ioe.initCause(cause);
		return ioe;
	}

	@Override
	public boolean prepareNext() {
		lock.lock();
		try {
			boolean hasNext = super.prepareNext();
			pendingRequestId = 0;
			data = null;
			messageLength = -1;
			replyCount = 0;
			writeBuffer.clear();
			messageChain = null;
			transmission.reset();
			resetReaper(getSessionTimeout());
			return hasNext;
		} finally {
			lock.unlock();
		}
	}

	protected short convertToShort(byte b0, byte b1) {
		return (short) (b1 & 255 | (short) (b0 << 8 & 65280));
	}

	@Override
	protected int findEOR(int newBytes) throws IOException {
		if(bufferReadOnly.remaining() < 1)
			return -1;
		transmissionInfo = transmission.prepareRead(bufferReadOnly, sessionLog);
		if(transmissionInfo == null)
			return -1;
		return bufferReadOnly.position();
	}

	public Long getLastCommandId() {
		return lastCommandId;
	}

	public void setLastCommandId(Long lastCommandId) {
		this.lastCommandId = lastCommandId;
	}

	@Override
	public long getServerTime() {
		return messageStartTime;
	}
	/**
	 * @see com.usatech.layers.common.Message#setNextCommandId(long)
	 */
	@Override
	public void setNextCommandId(long commandId) {
		throw new UnsupportedOperationException("Use setLastCommandId() instead");
	}
	/**
	 * @see com.usatech.networklayer.app.NetworkLayerMessage#getTranslator()
	 */
	@Override
	public Translator getTranslator() {
		return getTranslator(deviceInfo == null ? null : deviceInfo.getLocale());
	}

	/**
	 * @see com.usatech.layers.common.Message#getSessionAttribute(java.lang.String)
	 */
	@Override
	public Object getSessionAttribute(String name) {
		if(sessionAttributes == null)
			return null;
		return sessionAttributes.get(name);
	}

	/**
	 * @see com.usatech.layers.common.Message#setSessionAttribute(java.lang.String, java.lang.Object)
	 */
	@Override
	public void setSessionAttribute(String name, Object value) {
		if(value == null) {
			if(sessionAttributes != null)
				sessionAttributes.remove(name);
		} else {
			if(sessionAttributes == null)
				sessionAttributes = new HashMap<String, Object>();
			sessionAttributes.put(name, value);
		}
	}
	
	/**
	 * @see com.usatech.layers.common.Message#setResultAttribute(java.lang.String, java.lang.Object)
	 */
	@Override
	public void setResultAttribute(String name, Object value) {
		setSessionAttribute(name, value);
	}

	protected MessageChain constructMessageChain(String queueKey, Map<String, Object> processAttributes, ByteBuffer dataBytes, boolean updateOnly, String preProcessQueueKey, Map<String, Object> preProcessAttributes, Map<String, String> preProcessResultAttributes) {
		if(messageChain == null)
			messageChain = new MessageChainV11();
		else
			messageChain.clear();
		final Integer pendingRequestIndex;
		if(getReplyQueueKey() != null && !updateOnly) {
			pendingRequestId = messagesReceived;
			pendingRequestIndex = pendingRequestId;
		} else
			pendingRequestIndex = null;
		NetLayerUtils.constructMessageChain(messageChain, queueKey, dataBytes, updateOnly, globalSessionCode, sessionStartTime, getProtocolId(), getDeviceInfo(), 
				messageStartTime, getSessionTimeout(), lastCommandId, processAttributes, sessionId, sessionAttributes, pendingRequestIndex, preProcessQueueKey, preProcessAttributes, 
				preProcessResultAttributes, getReplyQueueKey());
		lastCommandId = null;
		return messageChain;
	}

	public MessageChainStep constructReplyStep(MessageChain messageChain, MessageData reply, SessionCloseReason endSessionReason, Long lastCommandId) throws ServiceException {
		pendingRequestId = messagesReceived;
		return NetLayerUtils.constructReplyStep(messageChain, getReplyQueueKey(), reply, endSessionReason, lastCommandId, pendingRequestId, sessionId, writeBuffer2);
	}
	public MessageChainStep constructReplyTemplateStep(MessageChain messageChain, MessageData reply, SessionCloseReason endSessionReason, Long lastCommandId) throws ServiceException {
		pendingRequestId = messagesReceived;
		return NetLayerUtils.constructReplyTemplateStep(messageChain, getReplyQueueKey(), reply, endSessionReason, lastCommandId, pendingRequestId, sessionId, writeBuffer2);
	}
	protected MessageChain constructSessionStartMessageChain() {
		if (!recordDeviceData)
			return null;
		
		MessageChain mc = new MessageChainV11(); // avoid concurrency issues
		MessageChainStep step = mc.addStep(getSessionControlQueueKey());
//		step.setTemporary(true);
		step.addStringAttribute("action", SessionControlAction.START.toString());
		step.addStringAttribute("globalSessionCode", getGlobalSessionCode());
		step.addLongAttribute("sessionStartTime", sessionStartTime);
		step.addStringAttribute("netlayerName", getLayerName());
		step.addStringAttribute("netlayerHost", getLayerHost());
		step.addIntAttribute("netlayerPort", getLayerPort());
		step.addStringAttribute("remoteAddress", remoteAddress.getAddress().getHostAddress());
		step.addIntAttribute("remotePort", remoteAddress.getPort());
		step.addByteAttribute("protocol", getProtocolId());
		if(deviceInfo.isInitOnly())
			step.addStringAttribute("deviceSerialCd", deviceSerialCd);
		else
			step.addStringAttribute("deviceSerialCd", deviceInfo.getDeviceSerialCd());
		step.addStringAttribute("deviceName", deviceInfo.getDeviceName());
		step.addLiteralAttribute("deviceType", deviceInfo.getDeviceType());
		return mc;
	}

	protected MessageChain constructSessionEndMessageChain() {
		if (!recordDeviceData)
			return null;
			
		MessageChain mc = new MessageChainV11(); // avoid concurrency issues
		char callInStatus = getSessionCloseReason().getValue();
		MessageChainStep step = mc.addStep(getSessionControlQueueKey());
//		step.setTemporary(true);
		step.addStringAttribute("action", SessionControlAction.END.toString());
		step.addStringAttribute("globalSessionCode",  getGlobalSessionCode());
		step.addLongAttribute("sessionStartTime", sessionStartTime);
		step.addLongAttribute("sessionEndTime", System.currentTimeMillis());
		step.addStringAttribute("netlayerName", getLayerName());
		step.addStringAttribute("netlayerHost", getLayerHost());
		step.addIntAttribute("netlayerPort", getLayerPort());
		step.addStringAttribute("remoteAddress", remoteAddress.getAddress().getHostAddress());
		step.addIntAttribute("remotePort", remoteAddress.getPort());
		if(deviceInfo.isInitOnly())
			step.addStringAttribute("deviceName", deviceName);
		else
			step.addStringAttribute("deviceName", deviceInfo.getDeviceName());
		if(deviceInfo.isInitOnly())
			step.addStringAttribute("deviceSerialCd", deviceSerialCd);
		else
			step.addStringAttribute("deviceSerialCd", deviceInfo.getDeviceSerialCd());
		step.addLiteralAttribute("deviceType", deviceInfo.getDeviceType());
		step.addLongAttribute("bytesReceived", bytesReceived);
		step.addLongAttribute("bytesSent", bytesSent);
		step.addIntAttribute("messagesReceived", messagesReceived);
		step.addIntAttribute("messagesSent", messagesSent);
		step.addLiteralAttribute("initialized", deviceInitialized ? 'Y' : 'N');
		step.addLiteralAttribute("deviceSentConfig", deviceSentConfig ? 'Y' : 'N');
		step.addLiteralAttribute("serverSentConfig", serverSentConfig ? 'Y' : 'N');
		char callInType;
		if(authTime != null) {
			if(nonAuthMessage)
				callInType = 'C';
			else
				callInType = 'A';
		} else if(saleCount + fileCount + eventCount > 0) {
			callInType = 'B';
		} else
			callInType = 'U';			
		step.addLiteralAttribute("callInType", callInType);
		step.addLongAttribute("dexFileSize", dexFileSize);
		step.addStringAttribute("dexStatus", dexStatus);
				
		if(authTime != null) {
			step.addLongAttribute("authTime", authTime);
			step.addLiteralAttribute("authResultCd", authResultCode);
			step.addLiteralAttribute("paymentType", paymentType);
			if(authAmount != null)
				step.addLiteralAttribute("authAmt", authAmount);
		}
		step.addLiteralAttribute("callInStatus", callInStatus);
		step.addLiteralAttribute("saleCount", saleCount);
		step.addLiteralAttribute("fileCount", fileCount);
		step.addLiteralAttribute("eventCount", eventCount);
		
		return mc;
	}
	
	protected abstract AtomicLong getLastMessageId(String deviceNameOrSerial) ;

	protected abstract QoS getInlineQoS() ;
	
	protected abstract Protocol getProtocol(int protocolId) throws IOException ;

	protected abstract String getSessionControlQueueKey() ;

	protected abstract String getMessageProcessedQueueKey() ;

	protected abstract void returnSession() ;

	protected abstract long getAppLayerResponseTimeout() ;

	protected abstract long getSessionTimeout() ;
	
	protected abstract long getMessageBasedSessionEndDelay() ;

	protected abstract DeviceInfoManager getDeviceInfoManager() throws ServiceException ;

	protected abstract Translator getTranslator(String locale) ;

	protected abstract void addSession() ;

	protected abstract boolean removeSession() ;

	protected abstract ScheduledFuture<?> scheduleReaper(final long timeout) ;
	
	protected abstract boolean cancelReaper(Future<?> sessionReaper, boolean warnOnNotCancelled) ;

	protected abstract ScheduledFuture<?> scheduleTask(Runnable task, long timeout) ;

	protected abstract Pattern getDeviceNamePattern();

	protected abstract Pattern getDeviceLimitedActivityPattern();

	protected abstract boolean isMessageNumberChecked();

	protected abstract Set<String> getServerSessionMessages();
	
	protected abstract int getMessageRetriesAllowed();

	protected abstract DebugLevel getDebugLevel();

	protected abstract boolean isInitMessageIdChecked() ;
	protected abstract boolean isNoninitMessageIdChecked() ;

	protected abstract Transmission newTransmission() throws IOException ;

	protected abstract EncryptionInfoMapping getEncryptionInfoMapping() ;

	// --- ClientMessage methods
	public InetSocketAddress getRemoteAddress() {
		return remoteAddress;
	}

	public String getRemoteAddressString() {
		return remoteAddressString;
	}

	public String getDeviceName() {
		return transmissionInfo == null ? null : transmissionInfo.getDeviceName();
	}

	public String getPrevDeviceName() {
		return prevDeviceName;
	}

	public Charset getDeviceCharset() {
		return deviceInfo.getDeviceCharset();
	}

	public MessageData getData() {
		return data;
	}

	public long getSessionId() {
		return sessionId;
	}
	
	public boolean isInitSession() {
		return deviceInitialized;
	}

	public String getGlobalSessionCode() {
		return globalSessionCode;
	}

	public byte getProtocolId() {
		return transmissionInfo == null ? -1 : transmissionInfo.getProtocolId();
	}

	public Resource addFileTransfer(boolean useResource, long fileTransferId, long pendingCommandId, int fileGroup, String fileName, int fileTypeId, int filePacketSize, long fileSize) throws ServiceException {
		throw new UnsupportedOperationException("Not supported");
	}

	/**
	 * @see com.usatech.networklayer.app.NetworkLayerMessage#getNewDeviceInfo(java.lang.String)
	 */
	@Override
	public DeviceInfo getNewDeviceInfo(String deviceName) throws ServiceException {
		return getDeviceInfoManager().getDeviceInfo(deviceName, OnMissingPolicy.CREATE_NEW, false);
	}

	public DeviceInfo getDeviceInfo() {
		return deviceInfo;
	}

	@Override
	public boolean permitReply(int requestId) {
		if(pendingRequestId == requestId) {
			pendingRequestId = 0;
			return true;
		}
		return false;
	}
	
	protected boolean checkMessageId(String deviceNameOrSerial, long messageId) {
		AtomicLong lastMessageId = getLastMessageId(deviceNameOrSerial);
		long currentMillis = System.currentTimeMillis();
		long currentSeconds = currentMillis / 1000;
		if(messageId <= currentSeconds + ProcessingUtils.MAX_ID_ADVANCE) {
			while(true) {
				long current = lastMessageId.get();
				if(messageId <= current)
					break;
				else if(lastMessageId.compareAndSet(current, messageId))
					return true;
			}
		}
		MessageData replyData;
		switch(data.getMessageType()) {
			case AUTH_REQUEST_4_1:
				MessageData_C3 c3Data = new MessageData_C3();
				c3Data.setAuthResponseType(AuthResponseType.CONTROL);
				c3Data.setCommandCode(AuthResponseActionCode.PERFORM_CALL_IN);
				((CallInCommand)c3Data.getCommandCodeData()).setDelaySeconds(0);
				replyData = c3Data;
				break;
			default:
				MessageData_CB cbData = new MessageData_CB();
				cbData.setResultCode(GenericResponseResultCode.NETLAYER_ERROR);
				cbData.setResponseMessage(getTranslator().translate("client.message.invalid-message-id", "Invalid Message ID"));
				cbData.setServerActionCode(GenericResponseServerActionCode.PROCESS_PROP_LIST);
				long newMessageId = Math.max(lastMessageId.get(), currentSeconds);
				Map<Integer, String> propertyValues = ((PropertyValueListAction) cbData.getServerActionCodeData()).getPropertyValues();
				propertyValues.put(DeviceProperty.MESSAGE_ID.getValue(), String.valueOf(newMessageId));
				propertyValues.put(DeviceProperty.UTC_TIME.getValue(), String.valueOf(currentSeconds));
				propertyValues.put(DeviceProperty.MASTER_ID.getValue(), String.valueOf(Math.max(currentSeconds, deviceInfo.getMasterId())));
				String timeZoneGuid = deviceInfo.getTimeZoneGuid();
				if(timeZoneGuid != null  && (timeZoneGuid=timeZoneGuid.trim()).length() > 0) {
					propertyValues.put(DeviceProperty.UTC_OFFSET.getValue(), String.valueOf(TimeZone.getTimeZone(timeZoneGuid).getOffset(currentMillis) / 1000 / 60 / 15));
				} else {
					log.info("Could not get time zone for device '" + getDeviceName() + "'");
				}

				replyData = cbData;
		}
		getLog().info("Invalid message id " + data.getMessageId() + ". Sending response " + replyData.getMessageType()+" Last message id:"+(lastMessageId==null?"":lastMessageId.get()));
		sendReply(null, SessionCloseReason.ERROR, replyData); // don't continue session because we are going to close it
		return false;
	}
	@Override
	public Log getLog() {
		return messageLog;
	}

	protected void resetReaper(final long timeout) {
		cancelReaper(sessionReaper, true);
		if(timeout > 0 && !getClosedUnderLock())
			sessionReaper = scheduleReaper(timeout);		
	}

	protected boolean getClosedUnderLock() {
		lock.lock();
		try {
			return closed;	
		} finally {
			lock.unlock();
		}
	}
	
	// -- ClientMessageResponses methods
	public void enqueue(MessageChain messageChain, boolean inline) throws ServiceException {
		if (!recordDeviceData)
			return;
		MessageChainService.publish(messageChain, getPublisher(), getEncryptionInfoMapping(), inline ? getInlineQoS() : null);
		if(inline)
			resetReaper(getAppLayerResponseTimeout());
	}

	public void enqueueExpectingReply(String queueKey, Map<String, Object> processAttributes, String preProcessQueueKey, Map<String, Object> preProcessAttributes, Map<String, String> preProcessResultAttributes) throws ServiceException {
		if (!recordDeviceData)
			return;
		if(sessionLog.isDebugEnabled())
			sessionLog.debug("Enqueuing message '" + data.getMessageType() + "' from device " + getDeviceName() + " to queue '" + queueKey + "' expecting reply on queue '" + getReplyQueueKey() + "'");
		MessageChain mc = constructMessageChain(queueKey, processAttributes, writeBuffer, false, preProcessQueueKey, preProcessAttributes, preProcessResultAttributes);
		enqueue(mc, true);
	}

	public void enqueueNoReply(String queueKey, Map<String, Object> processAttributes, String preProcessQueueKey, Map<String, Object> preProcessAttributes, Map<String, String> preProcessResultAttributes) throws ServiceException {
		if (!recordDeviceData)
			return;
		if(sessionLog.isDebugEnabled())
			sessionLog.debug("Enqueuing message '" + data.getMessageType()  + "' from device " + getDeviceName() + " to queue '" + queueKey + "'" );
		MessageChain mc = constructMessageChain(queueKey, processAttributes, writeBuffer, true, preProcessQueueKey, preProcessAttributes, preProcessResultAttributes);
		enqueue(mc, false);
	}

	/**
	 * @param firstReply Whether this is the first reply to the device or not
	 * @throws ServiceException
	 *
	 */
	protected void enqueueMessageProcessed(boolean firstReply, MessageData reply, int replyLength, boolean replyTransmitted) {
		if (!recordDeviceData)
			return;
		
		if (getDeviceName() != null && deviceLimitedActivityPattern.matcher(getDeviceName()).matches())
			return;

		long messageEndTime = System.currentTimeMillis();
		MessageChain mc = new MessageChainV11(); // avoid concurrency issues
		MessageChainStep step = mc.addStep(getMessageProcessedQueueKey());
//		step.setTemporary(true);
		step.addBooleanAttribute("complete", true);
		step.addBooleanAttribute("replyTransmitted", replyTransmitted);
		step.addStringAttribute("globalSessionCode", globalSessionCode);
		step.addByteAttribute("protocol", getProtocolId());
		step.addStringAttribute("deviceName", getDeviceName());
		step.addLiteralAttribute("deviceType", deviceInfo.getDeviceType());
		step.addStringAttribute("locale", deviceInfo.getLocale());
		step.addLongAttribute("messageStartTime", messageStartTime);
		step.addLongAttribute("messageEndTime", messageEndTime);
		step.addStringAttribute("netlayerName", getLayerName());
		step.addStringAttribute("netlayerHost", getLayerHost());
		step.addIntAttribute("netlayerPort", getLayerPort());
		step.addIntAttribute("messageSequence", messageSequence.incrementAndGet());
		if(deviceInfo.isInitOnly())
			step.addStringAttribute("deviceSerialCd", deviceSerialCd);
		else
			step.addStringAttribute("deviceSerialCd", deviceInfo.getDeviceSerialCd());
		
		if(firstReply) {
			MessageData data = this.data;
			if(data != null) {
				step.addStringAttribute("messageType", data.getMessageType().getHex());
				writeBuffer.clear();
				data.writeData(writeBuffer, true);
				writeBuffer.flip();
				step.addLiteralAttribute("data", writeBuffer);
			}
			step.addIntAttribute("dataLength", messageLength);
		}
		if(reply != null) {
			step.addStringAttribute("replyMessageType", reply.getMessageType().getHex());
			writeBuffer2.clear();
			reply.writeData(writeBuffer2, true);
			writeBuffer2.flip();
			step.addLiteralAttribute("reply", writeBuffer2);
		}
		if(replyLength > 0)
			step.addIntAttribute("replyLength", replyLength);

		try {
			enqueue(mc, false);
		} catch(ServiceException e) {
			sessionLog.warn("Could not enqueue message complete", e);
		}
	}

	public int getReplyCount() {
		return replyCount;
	}

	/**
	 * @throws ServiceException
	 * @see com.usatech.layers.common.Message#setReply(com.usatech.layers.common.messagedata.MessageData)
	 */
	@Override
	public void sendReply(MessageData... replyData) {
		sendReply((RunnableListener) null, replyData);
	}

	/**
	 * @see com.usatech.networklayer.app.NetworkLayerMessage#sendReply(java.lang.Runnable, com.usatech.layers.common.messagedata.MessageData[])
	 */
	@Override
	public boolean sendReply(final RunnableListener onComplete, final MessageData... replyData) {
		return sendReply(onComplete, null, replyData);
	}
	public boolean sendReplyAndClose(SessionCloseReason sessionCloseReason, MessageData... replyData) {
		return sendReply(null, sessionCloseReason, replyData);
	}
	protected boolean sendReply(final RunnableListener onComplete, final SessionCloseReason sessionCloseReason, final MessageData... replyData) {
		if(!cancelReaper(sessionReaper, true)) {
			log.error("Could not cancel the session reaper. Closing the session and not sending the reply");
			closeSession(SessionCloseReason.TIMEOUT);
			if(onComplete != null)
				onComplete.runFailed(null);
			return false;
		}
		boolean initMessage = false;
		if(data != null && data.getMessageType().isInitMessage()) {
			deviceInitialized = true;
			initMessage = true;
		}
		if(replyData != null && replyData.length > 0) {
			//if(transmissionInfo.getProtocolId() == 1) {
				//transmissionInfo.setProtocolId((byte)3); // eSuds clients send protocol 1 messages in a few cases, but we don't want to use protocol 1 for the response
			//}
			try {
				if(protocol == null || transmissionInfo.getProtocolId() != protocol.getProtocolId()) {
					protocol = getProtocol(transmissionInfo.getProtocolId());
					if(cryptor != null) {
						cryptor.release();
						cryptor = null;
					}
					cryptor = protocol.newCryptor();
				}
				if(deviceInfo == null || !getDeviceName().equals(prevDeviceName)) {
					deviceInfo = getDeviceInfoManager().getDeviceInfo(getDeviceName(), OnMissingPolicy.THROW_EXCEPTION, false);
					prevDeviceName = getDeviceName();
					encryptionKey = deviceInfo.getEncryptionKey();
				}				
			} catch(GeneralSecurityException e) {
				log.error("Could not get protocol to send the reply. Closing session", e);
				closeSession(SessionCloseReason.ERROR);
				if(onComplete != null)
					onComplete.runFailed(e);
				return false;
			} catch(IOException e) {
				log.error("Could not get protocol to send the reply. Closing session", e);
				closeSession(SessionCloseReason.ERROR);
				if(onComplete != null)
					onComplete.runFailed(e);
				return false;
			} catch(ServiceException e) {
				log.error("Could not get device info to send the reply. Closing session", e);
				closeSession(SessionCloseReason.ERROR);
				if(onComplete != null)
					onComplete.runFailed(e);
				return false;
			}
			final int[] minDelays = new int[replyData.length];
			int totalDelay = 0;
			for(int i = 0; i < replyData.length; i++) {
				minDelays[i] = transmission.getMinimumResponseDelay(data, i, replyData[i], deviceInfo);
				totalDelay += minDelays[i];
				if (initMessage) {
					if(replyData[i] instanceof MessageData_8F)
						deviceName = new String(((MessageData_8F)replyData[i]).getDeviceSerialNum(), replyData[i].getCharset());
					else if(replyData[i] instanceof MessageData_CB) {
						ServerActionCodeData sacd = ((MessageData_CB) replyData[i]).getServerActionCodeData();
						if(sacd instanceof PropertyValueListAction) {
							Map<Integer, String> propertyValues = ((PropertyValueListAction) sacd).getPropertyValues();
						if(propertyValues != null)
							deviceName = propertyValues.get(DeviceProperty.DEVICE_GUID.getValue());
					}
				}
			}
			}
			replyCount = replyData.length;
			//int repeatInterval = transmission.getRepeatResponseInterval(data, replyData.length - 1, replyData[replyData.length - 1], deviceInfo);
			if(totalDelay > 0) {
				long delay = messageStartTime + minDelays[0] - System.currentTimeMillis();
				if(replyData.length > 1 || delay > 0) {
					final AtomicInteger index = new AtomicInteger();
					Runnable run = new Runnable() {
						boolean first = true;
						@Override
						public void run() {
							try {
								int i = index.get();
								boolean last = (i + 1 == replyData.length);
								long lastCommTime = System.currentTimeMillis();
								if(replyData[i] != null) {
									sendReplyInternal(replyData[i], first);
									first = false;
								} else if(last && first) {
									enqueueMessageProcessed(first, null, 0, false);
								}
								if(!last) {
									i = index.incrementAndGet();
									if(minDelays[i] > 0) {
										long delay = (lastCommTime + minDelays[i]) - System.currentTimeMillis();
										if(delay > 0) {
											log.info("Scheduling response #" + (i+1) + " to send in " + delay + " milliseconds");
											scheduleTask(this, delay);
										} else {
											log.info("Sending response  #" + (i+1) + " immediately");
											run();
										}
									} else {
										log.info("Sending response  #" + (i + 1) + " immediately");
										run();
									}
								} else {
									if(onComplete != null)
										onComplete.runCompleted();
									if(sessionCloseReason == null)
										continueSession();
									else
										closeSession(sessionCloseReason);
								}
							} catch(ServiceException e) {
								log.warn("Could not send the reply. Closing session", e);
								if(onComplete != null)
									onComplete.runFailed(e);
								closeSession(SessionCloseReason.ERROR);
							} catch(RuntimeException e) {
								log.warn("Could not send the reply. Closing session", e);
								if(onComplete != null)
									onComplete.runFailed(e);
								closeSession(SessionCloseReason.ERROR);
							} catch(Error e) {
								log.warn("Could not send the reply. Closing session", e);
								if(onComplete != null)
									onComplete.runFailed(e);
								closeSession(SessionCloseReason.ERROR);
							}
						}
					};
					
					if(delay > 0) {
						log.info("Scheduling response to send in " + delay + " milliseconds");
						scheduleTask(run, delay);
					} else {
						log.info("Sending response immediately");
						run.run();
					}
					return true; // we err on the side of sent
				}
				log.info("Sending response immediately");
			}
			boolean first = true;
			boolean sent = false;
			try {
				for(int i = 0; i < replyData.length; i++) {
					if(replyData[i] != null) {
						if(sendReplyInternal(replyData[i], first))
							sent = true;
						first = false;
					}
				}
				if(onComplete != null)
					onComplete.runCompleted();
				if(sessionCloseReason == null)
					continueSession();
				else
					closeSession(sessionCloseReason);
			} catch(ServiceException e) {
				log.warn("Could not send the reply. Closing session", e);
				if(onComplete != null)
					onComplete.runFailed(e);
				closeSession(SessionCloseReason.ERROR);
			} catch(RuntimeException e) {
				log.warn("Could not send the reply. Closing session", e);
				if(onComplete != null)
					onComplete.runFailed(e);
				closeSession(SessionCloseReason.ERROR);
			} catch(Error e) {
				log.warn("Could not send the reply. Closing session", e);
				if(onComplete != null)
					onComplete.runFailed(e);
				closeSession(SessionCloseReason.ERROR);
			}
			return sent;
		}
		enqueueMessageProcessed(true, null, 0, false);
		if(onComplete != null)
			onComplete.runCompleted();
		if(sessionCloseReason == null)
			continueSession();
		else
			closeSession(sessionCloseReason);
		return true;
	}

	protected boolean sendReplyInternal(MessageData replyData, boolean first) throws ServiceException {
		replyData.setDirection(MessageDirection.SERVER_TO_CLIENT);
		replyData.setMessageNumber(data.getMessageNumber());
		replyData.setMessageId(data.getMessageId());
		boolean success = false;
		int replyLength = -1;
		try {
			writeBuffer.clear();
			transmission.prepareWrite(writeBuffer, transmissionInfo, sessionLog);
			cryptor.init(encryptionKey, getDeviceName());
			writeBuffer2.clear();
			replyData.writeData(writeBuffer2, false);
			writeBuffer2.flip();
			replyLength = writeBuffer2.remaining();
			cryptor.encrypt(writeBuffer2, writeBuffer);
			transmission.finishWrite(writeBuffer, transmissionInfo, sessionLog);
			writeBuffer.flip();
			int n = writeBuffer.remaining();

			if(getLog().isInfoEnabled())
				getLog().info("Sending " + replyData + " to " + getRemoteAddressString()
						+ " [" + (System.currentTimeMillis() - messageStartTime) + " ms]");

			while(writeBuffer.hasRemaining())
				byteChannel.write(writeBuffer);
			bytesSent += n;
			messagesSent++;
			if (replyData instanceof MessageData_88)
				serverSentConfig = true;
			success = true;
		} catch(IOException e) {
			throw new ServiceException(e);
		} catch(GeneralSecurityException e) {
			throw new ServiceException(e);
		} finally {
			enqueueMessageProcessed(first, replyData, replyLength, success);
		}
		return success;
	}

	/** NOTE: This must be done last because it returns the session to the pool to be used potentially by another thread.
	 * 
	 */
	protected void continueSession() {
		if(sessionLog.isDebugEnabled())
			sessionLog.debug("Listening for more messages from device " + getDeviceName());
		long timeout = getSessionTimeout();
		if(timeout > 0 && !getClosedUnderLock())
			sessionReaper = scheduleReaper(timeout);
		returnSession();
	}

	public void registerInboundFileTransfer(long fileTransferId, InboundFileTransfer fileTransfer) {
		if(inboundFileTransfers == null)
			inboundFileTransfers = new HashMap<FileTransferKey, InboundFileTransfer>();
		if(log.isDebugEnabled())
			log.debug("Registering inbound file transfer " + fileTransferId + " for " + getDeviceName());
		inboundFileTransfers.put(new FileTransferKey(getDeviceName(), fileTransferId), fileTransfer);
	}

	public InboundFileTransfer getInboundFileTransfer(long fileTransferId) {
		return (inboundFileTransfers == null ? null : inboundFileTransfers.get(new FileTransferKey(getDeviceName(), fileTransferId)));
	}

	public boolean removeInboundFileTransfer(long fileTransferId) {
		if(inboundFileTransfers != null) {
			InboundFileTransfer ft = inboundFileTransfers.remove(new FileTransferKey(getDeviceName(), fileTransferId));
			if(ft != null) {
				ft.cancel();
				return true;
			}
		}
		return false;
	}

	public int cancelAllFileTransfers() {
		// clean up file transfers if any
		int count = 0;
		if(inboundFileTransfers != null) {
			Iterator<Map.Entry<FileTransferKey, InboundFileTransfer>> iter = inboundFileTransfers.entrySet().iterator();
			while(iter.hasNext()) {
				Map.Entry<FileTransferKey, InboundFileTransfer> entry = iter.next();
				InboundFileTransfer ft = entry.getValue();
				boolean success = ft.cancel();
				if(sessionLog.isDebugEnabled())
					sessionLog.debug((success ?
							"Deleted file '" + ft.getFileName() + "' before sending to app layer because device " + entry.getKey().getDeviceName() + " abandoned it"
							: "Could not delete file '" + ft.getFileName() + "' from device " + entry.getKey().getDeviceName()));
				iter.remove();
				count++;
			}
		}
		return count;
	}

	public void registerOutboundFileTransfer(long fileTransferId, OutboundFileTransfer fileTransfer) {
		if(outboundFileTransfers == null)
			outboundFileTransfers = new HashMap<FileTransferKey, OutboundFileTransfer>();
		outboundFileTransfers.put(new FileTransferKey(getDeviceName(), fileTransferId), fileTransfer);
	}

	public OutboundFileTransfer getOutboundFileTransfer(long fileTransferId) {
		return (outboundFileTransfers == null ? null : outboundFileTransfers.get(new FileTransferKey(getDeviceName(), fileTransferId)));
	}

	public boolean removeOutboundFileTransfer(long fileTransferId) {
		if(outboundFileTransfers != null) {
			OutboundFileTransfer ft = outboundFileTransfers.remove(new FileTransferKey(getDeviceName(), fileTransferId));
			if(ft != null) {
				try {
					ft.finish();
				} catch(IOException e) {
					sessionLog.warn("Could not clean-up outbound file transfer", e);
				}
				return true;
			}
		}
		return false;
	}
	
	@Override
	public void receivedFileTransfer(FileType fileType, BinaryStream content) {
		switch(fileType) {
			case PROPERTY_LIST:
				try {
					MessageResponseUtils.markPropertyIndexesReceived(content.getInputStream(), this);
				} catch(IOException e) {
					log.warn("Could not process property list file", e);
				} catch(ServiceException e) {
					log.warn("Could not process property list file", e);
				}
				break;
			case CONFIGURATION_FILE:
				deviceSentConfig = true;
				break;
			case DEX_FILE: case DEX_FILE_FOR_DEVICE_DOWNLOAD_AUTOMATIC: case DEX_FILE_FOR_DEVICE_DOWNLOAD_MANUAL: case DEX_FILE_FOR_FILL_TO_FILL: case DEX_FILE_FROM_EZ80_EPORT:
			dexFileSize += content.getLength();
			dexStatus = hasDexFileErrors(content) ? "Error" : "OK";
				// fall-through
			case ESUDS_GENERIC_FILE: case GENERIC_BINARY_DATA: case GENERIC_DATA_FILE_FROM_EZ80_EPORT: case KIOSK_GENERIC_FILE: case LOG_FILE:
				incrementFileCount();
				break;
		}
	}
	

	private boolean hasDexFileErrors(BinaryStream content) {
		if (content == null) {
			return true;
		}
		
		if (content.getLength() == 0) {
			return true;
		}
		
		try {
			EnhancedBufferedReader dexFileReader = new EnhancedBufferedReader(new InputStreamReader(content.getInputStream()));
			String line = null;
			while ((line = dexFileReader.readLine()) != null) {
				if (line.contains("EA1*EK2M*")) {
					return true;
				}
			} 
			return false;
		} catch (IOException e) {
			sessionLog.warn("Failed to  read inbound dex file", e);
			return true;
		}
	}

	@Override
	public void sentFileTransfer(FileType fileType, BinaryStream content) {
			switch(fileType) {
				case CONFIGURATION_FILE:
					serverSentConfig = true;
					break;
			}
	}
	
	public void sentAuthResponse(AuthResultCode authResultCode, Number authAmount, char paymentType) {
		this.paymentType = paymentType;
		this.authAmount = authAmount;
		this.authResultCode = authResultCode;
	}
	
	@Override
	public void incrementSaleCount() {
		saleCount++;
	}
	@Override
	public void incrementFileCount() {
		fileCount++;
	}
	@Override
	public void incrementEventCount() {
		eventCount++;
	}
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Message ");
		if(messageChain != null) {
			sb.append('\'').append(messageChain.getGuid()).append("' ");
		}
		sb.append('[').append(getDeviceName()).append(" @ ").append(globalSessionCode).append(" #").append(messageNumberUnsigned).append("] ").append(data == null ? "UNKNOWN" : data.getMessageType().getDescription());
		return sb.toString();
	}

	public static boolean isMessageNumberValidationNeeded(DeviceType deviceType, MessageType messageType){
		switch(deviceType) {
			case G4:
			case GX:
			case NG:
			case LEGACY_KIOSK:
			case ESUDS:
			case MEI:
			case EZ80_DEVELOPMENT:
			case EZ80:
			case LEGACY_G4:
			case TRANSACT:
			case KIOSK:
			case T2:
				return false;
			case DEFAULT:
				switch(messageType) {
					case INITIALIZATION_2_0:
					case INITIALIZATION_3_0:
					case INITIALIZATION_3_1:
						return false;
					default:
						return messageType.isInitMessage();
				}
			default:
				return true;
		}
	}

	public SessionCloseReason getSessionCloseReason() {
		return sessionCloseReason;
	}	
}
