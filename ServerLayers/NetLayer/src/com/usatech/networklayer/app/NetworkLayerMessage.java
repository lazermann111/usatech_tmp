package com.usatech.networklayer.app;

import java.util.Map;

import simple.app.ServiceException;
import simple.event.RunnableListener;
import simple.io.BinaryStream;
import simple.io.resource.ResourceFolder;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainStep;
import com.usatech.layers.common.Message;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.FileType;
import com.usatech.layers.common.constants.SessionCloseReason;
import com.usatech.layers.common.messagedata.MessageData;


public interface NetworkLayerMessage extends Message {
	//public void replyFully(MessageData replyData) throws ServiceException ;
	//public void replyPartially(MessageData replyData) throws ServiceException ;
	public void enqueueExpectingReply(String queueKey, Map<String, Object> processAttributes, String preProcessQueueKey, Map<String, Object> preProcessAttributes, Map<String, String> preProcessResultAttributes) throws ServiceException ;
	public void enqueueNoReply(String queueKey, Map<String, Object> processAttributes, String preProcessQueueKey, Map<String, Object> preProcessAttributes, Map<String, String> preProcessResultAttributes) throws ServiceException ;
	/**
	 * @param messageChain
	 * @param inline <code>true</code> if NetworkLayer this message is an inline message and NetworkLayer is waiting for a response on its outbound queue. 
	 * NetworkLayer may apply a message timeout, non-persistence and/or a priority to inline messages
	 * @throws ServiceException
	 */
	public void enqueue(MessageChain messageChain, boolean inline) throws ServiceException ;
	public MessageChainStep constructReplyStep(MessageChain messageChain, MessageData reply, SessionCloseReason endSessionReason, Long lastCommandId) throws ServiceException ;
	public MessageChainStep constructReplyTemplateStep(MessageChain messageChain, MessageData reply, SessionCloseReason endSessionReason, Long lastCommandId) throws ServiceException ;
	public void closeSession(SessionCloseReason sessionCloseReason) ;
	public void registerInboundFileTransfer(long fileTransferId, InboundFileTransfer fileTransfer) ;
	public InboundFileTransfer getInboundFileTransfer(long fileTransferId) ;
	public boolean removeInboundFileTransfer(long fileTransferId) ;
	public int cancelAllFileTransfers() ;
	public void registerOutboundFileTransfer(long fileTransferId, OutboundFileTransfer fileTransfer) ;
	public OutboundFileTransfer getOutboundFileTransfer(long fileTransferId) ;
	public boolean removeOutboundFileTransfer(long fileTransferId) ;
	public String getLayerName() ;
	public String getLayerHost() ;
	public int getLayerPort() ;
	public String getLayerDesc() ;
	public ResourceFolder getResourceFolder() throws ServiceException ;
	public Long getLastCommandId();
	public void setLastCommandId(Long lastCommandId) ;
	public String getReplyQueueKey() ;
	public long getSessionId() ;
	public boolean isInitSession() ;
	/**
	 * @param onComplete
	 * @param replyData
	 * @return Whether any data has possibly been sent to the device. This returns <code>false</code> only if the reply is absolutely not sent to the device
	 */
	public boolean sendReply(RunnableListener onComplete, MessageData... replyData) ;
	/**
	 * @param onComplete
	 * @param replyData
	 * @return Whether any data has possibly been sent to the device. This returns <code>false</code> only if the reply is absolutely not sent to the device
	 */
	public boolean sendReplyAndClose(SessionCloseReason sessionCloseReason, MessageData... replyData) ;
	/** Returns <code>true</code> if and only if a reply may be sent to this session for the given requestId -
	 *  meaning that a reply has not already been sent. (i.e. - this method should return true just once for a given
	 *  session and requestId
	 * @param requestId
	 * @return
	 */
	public boolean permitReply(int requestId) ;
	
	public void receivedFileTransfer(FileType fileType, BinaryStream content);
	public void sentFileTransfer(FileType fileType, BinaryStream content) ;
	public void sentAuthResponse(AuthResultCode authResultCode, Number authAmount, char paymentType) ;
	public void processingComplete() ;
	public void incrementSaleCount() ;
	public void incrementFileCount() ;
	public void incrementEventCount() ;
}
