package com.usatech.networklayer.app;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.channels.ByteChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.RunnableScheduledFuture;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Pattern;

import simple.app.AbstractWorkQueue;
import simple.app.BasicQoS;
import simple.app.PrerequisiteManager;
import simple.app.Publisher;
import simple.app.QoS;
import simple.app.ServiceException;
import simple.app.Work;
import simple.app.WorkQueue;
import simple.app.WorkQueueException;
import simple.app.WorkRetryType;
import simple.bean.ConvertUtils;
import simple.event.ProgressListener;
import simple.io.ByteInput;
import simple.io.Log;
import simple.io.resource.ResourceFolder;
import simple.io.server.NIOServer;
import simple.security.crypt.EncryptionInfoMapping;
import simple.text.StringUtils;
import simple.translator.DefaultTranslatorFactory;
import simple.translator.Translator;
import simple.translator.TranslatorFactory;
import simple.util.concurrent.BoundedCache;
import simple.util.concurrent.CreationException;
import simple.util.concurrent.CustomThreadFactory;
import simple.util.concurrent.LockLinkedSegmentCache;
import simple.util.concurrent.ResultFuture;
import simple.util.concurrent.SegmentCache;

import com.usatech.layers.common.DeviceInfoManager;
import com.usatech.layers.common.constants.DebugLevel;
import com.usatech.layers.common.constants.SessionCloseReason;
import com.usatech.networklayer.protocol.Protocol;
import com.usatech.networklayer.protocol.Transmission;

public class NetworkLayerQueue extends NIOServer<NetworkLayerQueue.NetworkLayerClientSession> implements WorkQueue<NetworkLayerMessage>, NetworkLayerQueueMXBean {
	private static final Log log = Log.getLog();
	protected int maxInputSize = 2048;
	protected final Map<Integer,Protocol> protocols = new HashMap<Integer, Protocol>();
	protected Publisher<ByteInput> inboundPublisher;
	protected AbstractWorkQueue<ByteInput> outboundQueue;
	protected String layerName;
	protected String sessionControlQueueKey;
	protected String messageProcessedQueueKey;
	protected long appLayerResponseTimeout;
	protected long sessionTimeout;
	protected long messageBasedSessionEndDelay;
	protected DeviceInfoManager deviceInfoManager;
	protected ResourceFolder resourceFolder;
	protected TranslatorFactory translatorFactory;
	protected String translatorContext;
	protected static final Map<Long, NetworkLayerClientSession> sessions = new ConcurrentHashMap<Long, NetworkLayerClientSession>();
	protected final AtomicLong sessionCount = new AtomicLong();
	protected final ScheduledThreadPoolExecutor sessionReaperService = new ScheduledThreadPoolExecutor(5, new CustomThreadFactory("SessionReaper-", true, 3));
	protected final ScheduledThreadPoolExecutor sessionTaskService = new ScheduledThreadPoolExecutor(25, new CustomThreadFactory("SessionTask-", true, 5));
	protected final ReentrantLock sessionCloseLock = new ReentrantLock();
	protected Condition sessionCloseSignal;
	protected String deviceNameRegex;
	protected Pattern deviceNamePattern;
	protected String deviceLimitedActivityRegex;
	protected Pattern deviceLimitedActivityPattern;
	protected boolean messageNumberChecked;
	protected Set<String> serverSessionMessages;
	protected int messageRetriesAllowed;
	protected DebugLevel debugLevel;
	protected final PrerequisiteManager prerequisiteManager = new PrerequisiteManager();
	protected boolean initMessageIdChecked;
	protected boolean noninitMessageIdChecked;
	protected Class<? extends Transmission> transmissionClass;
	protected EncryptionInfoMapping encryptionInfoMapping;
	protected boolean keepAliveUsed = false;
	protected boolean recordDeviceRejections = true;
	protected final BoundedCache<String, AtomicLong, RuntimeException> messageIds = new LockLinkedSegmentCache<String, AtomicLong, RuntimeException>(1000, SegmentCache.DEFAULT_INITIAL_CAPACITY, SegmentCache.DEFAULT_LOAD_FACTOR, SegmentCache.DEFAULT_SEGMENTS) {
		@Override
		protected AtomicLong createValue(String key, Object... additionalInfo) throws RuntimeException {
			return new AtomicLong();
		}
	};
	protected final BasicQoS inlineQoS = new BasicQoS(0, 6, false, null);
	protected static QoS DEFAULT_QOS = new QoS() {
		public long getExpiration() {
			return 0;
		}
		public Map<String, ?> getMessageProperties() {
			return null;
		}
		public int getPriority() {
			return 0;
		}
		public boolean isPersistent() {
			return false;
		}
		public long getTimeToLive() {
			return 0;
		}
		public long getAvailable() {
			return 0;
		}
	};
	protected static final String layerHostName = getHostName();
	public static String getHostName() {
		try {
			return InetAddress.getLocalHost().getCanonicalHostName();
		} catch(UnknownHostException e) {
			log.warn("Could not get host name", e);
			return "UNKNOWN";
		}
	}

	protected class NetworkLayerClientSession extends AbstractClientSession {
		public NetworkLayerClientSession(Socket socket, ByteChannel byteChannel) throws IOException {
			super(socket, byteChannel, getMaxInputSize());
		}

		@Override
		protected boolean isRecordDeviceRejections() {
			return NetworkLayerQueue.this.isRecordDeviceRejections();
		}

		@Override
		public Publisher<ByteInput> getPublisher() {
			return NetworkLayerQueue.this.getInboundPublisher();
		}

		@Override
		protected Protocol getProtocol(int protocolId) throws IOException {
			return NetworkLayerQueue.this.getProtocol(protocolId);
		}

		public String getReplyQueueKey() {
			return getOutboundQueue().getQueueKey();
		}

		@Override
		protected void returnSession() {
			returnObject(this);
		}

		public void closeSession(SessionCloseReason sessionCloseReason) {
			if(sessionCloseReason == null)
				sessionCloseReason = SessionCloseReason.UNKNOWN;
			this.sessionCloseReason = sessionCloseReason;
			final boolean clientInitiated;
			switch(sessionCloseReason) {
				case CLIENT_REQUESTED:
					clientInitiated = true;
					break;
				default:
					clientInitiated = false;
					break;
			}
			if(sessionCloseReason == SessionCloseReason.MESSAGE_BASED && getMessageBasedSessionEndDelay() > 0) {
				final NetworkLayerClientSession session = this;
				Runnable run = new Runnable() {
					@Override
					public void run() {
						invalidateObject(session, clientInitiated);
					}
				};
				scheduleTask(run, getMessageBasedSessionEndDelay());
			} else 
				invalidateObject(this, clientInitiated);
		}

		public String getLayerName() {
			return NetworkLayerQueue.this.getLayerName();
		}
		public String getLayerHost() {
			return layerHostName;
		}
		public int getLayerPort() {
			return NetworkLayerQueue.this.getSocketAddress().getPort();
		}
		public String getLayerDesc() {
			return getLayerName() + " at " + getLayerHost() + ":" + getLayerPort();
		}
		@Override
		protected String getSessionControlQueueKey() {
			return NetworkLayerQueue.this.getSessionControlQueueKey();
		}
		@Override
		protected String getMessageProcessedQueueKey() {
			return NetworkLayerQueue.this.getMessageProcessedQueueKey();
		}
		@Override
		protected long getAppLayerResponseTimeout() {
			return NetworkLayerQueue.this.getAppLayerResponseTimeout();
		}
		@Override
		protected long getSessionTimeout() {
			return NetworkLayerQueue.this.getSessionTimeout();
		}
		@Override
		protected long getMessageBasedSessionEndDelay() {
			return NetworkLayerQueue.this.getMessageBasedSessionEndDelay();
		}
		@Override
		protected QoS getInlineQoS() {
			return NetworkLayerQueue.this.getInlineQoS();
		}
		@Override
		protected DeviceInfoManager getDeviceInfoManager() throws ServiceException {
			DeviceInfoManager km = NetworkLayerQueue.this.getDeviceInfoManager();
			if(km == null)
				throw new ServiceException("DeviceInfo Manager is not set");
			return km;
		}
		/**
		 * @see com.usatech.networklayer.app.AbstractClientSession#getTranslator(java.lang.String)
		 */
		@Override
		protected Translator getTranslator(String locale) {
			TranslatorFactory tf = getTranslatorFactory();
			if(tf != null)
				try {
					return tf.getTranslator(ConvertUtils.convertSafely(Locale.class, locale, Locale.getDefault()), getTranslatorContext());
				} catch(ServiceException e) {
					log.warn("Could not get translator", e);
				}
			return DefaultTranslatorFactory.getTranslatorInstance();
		}
		/**
		 * @throws ServiceException
		 * @see com.usatech.networklayer.app.NetworkLayerMessage#getResourceFolder()
		 */
		@Override
		public ResourceFolder getResourceFolder() throws ServiceException {
			ResourceFolder rf = NetworkLayerQueue.this.getResourceFolder();
			if(rf == null)
				throw new ServiceException("ResourceFolder is not set");
			return rf;
		}

		@Override
		protected void addSession() {
			if(sessions.put(sessionId, this) == null)
				sessionCount.incrementAndGet();
		}
		@Override
		protected boolean removeSession() {
			boolean removed = sessions.remove(sessionId) != null;
			if(removed) {
				sessionLog.info("Closing session from device " + getPrevDeviceName() + ", active for " + (System.currentTimeMillis() - sessionStartTime) + " milliseconds because of " + getSessionCloseReason());
				cancelReaper(sessionReaper, false);
				if(sessionCount.decrementAndGet() == 0) { // notify for shutdown()
					sessionCloseLock.lock();
					try {
						if(sessionCloseSignal != null)
							sessionCloseSignal.signalAll();
					} finally {
						sessionCloseLock.unlock();
					}
				}
			}
			return removed;
		}

		@Override
		protected ScheduledFuture<?> scheduleReaper(final long timeout) {
			return sessionReaperService.schedule(new Runnable() {
				public void run() {
					Selector selector = NetworkLayerQueue.this.selector; // Guard against NPE
					if(selector != null) {
						SelectionKey key = socket.getChannel().keyFor(selector);
						if(key != null && key.isReadable()) {
							NetworkLayerClientSession nlcs = ((Attachment) key.attachment()).getInterpretter();
							String extraDataHex;
							try {
								nlcs.read(socket.getChannel());
								extraDataHex = StringUtils.toHex(nlcs.bufferReadOnly, 0, nlcs.bufferReadOnly.remaining());
							} catch(IOException e) {
								extraDataHex = "<ERROR>";
							}
							sessionLog.warn("Session from device " + getPrevDeviceName() + " is being closed for timeout but has unread data on it (" + extraDataHex + ")");
						}
					}
					sessionLog.info("Session from device " + getPrevDeviceName() + " is being closed after " + timeout + " milliseconds of inactivity");
					closeSession(SessionCloseReason.TIMEOUT);
					enqueueMessageProcessed(true, null, 0, false);
				}
			}, timeout, TimeUnit.MILLISECONDS);
		}

		protected boolean cancelReaper(Future<?> sessionReaper, boolean warnOnNotCancelled) {
			if(!(sessionReaper instanceof RunnableScheduledFuture<?>) 
					|| !sessionReaperService.remove((RunnableScheduledFuture<?>)sessionReaper)) {
				if(!sessionReaper.cancel(false)) {//NOTE: Must use false for mayInterruptIfRunning b/c when the reaper runs it calls this method!
					if(warnOnNotCancelled)
						log.warn("Failed to cancel session reaper");
					return false;					
				}
			}
			return true;
		}
		
		@Override
		protected ScheduledFuture<?> scheduleTask(Runnable task, long timeout) {
			return sessionTaskService.schedule(task, timeout, TimeUnit.MILLISECONDS);
		}

		@Override
		protected Pattern getDeviceNamePattern() {
			return NetworkLayerQueue.this.getDeviceNamePattern();
		}

		@Override
		protected Pattern getDeviceLimitedActivityPattern() {
			return NetworkLayerQueue.this.getDeviceLimitedActivityPattern();
		}

		@Override
		protected boolean isMessageNumberChecked() {
			return NetworkLayerQueue.this.isMessageNumberChecked();
		}

		@Override
		protected Set<String> getServerSessionMessages() {
			return NetworkLayerQueue.this.getServerSessionMessages();
		}
		
		@Override
		protected int getMessageRetriesAllowed() {
			return NetworkLayerQueue.this.getMessageRetriesAllowed();
		}

		@Override
		protected DebugLevel getDebugLevel() {
			return NetworkLayerQueue.this.getDebugLevel();
		}
		@Override
		protected boolean isInitMessageIdChecked() {
			return NetworkLayerQueue.this.isInitMessageIdChecked();
		}
		@Override
		protected boolean isNoninitMessageIdChecked() {
			return NetworkLayerQueue.this.isNoninitMessageIdChecked();
		}
		@Override
		protected EncryptionInfoMapping getEncryptionInfoMapping() {
			return NetworkLayerQueue.this.getEncryptionInfoMapping();
		}
		@Override
		protected AtomicLong getLastMessageId(String deviceNameOrSerial) {
			return messageIds.getOrCreate(deviceNameOrSerial);
		}
		@Override
		protected Transmission newTransmission() throws IOException {
			Class<? extends Transmission> tc = getTransmissionClass();
			if(tc == null)
				throw new IOException("transmissionClass property is not set on " + NetworkLayerQueue.this);
			try {
				return tc.newInstance();
			} catch(InstantiationException e) {
				throw newIOException("Could not instantiate transmission class " + tc.getName(), e);
			} catch(IllegalAccessException e) {
				throw newIOException("Could not instantiate transmission class " + tc.getName(), e);
			}
		}

		protected void lock() {
			lock.lock();
		}
		protected void unlock() {
			lock.unlock();
		}
		/**
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "NetworkLayerClientSession [GlobalSessionCode=" + globalSessionCode + "; DeviceName=" + (transmissionInfo == null ? "UNKNOWN" : transmissionInfo.getDeviceName()) + "; SessionStart=" + sessionStartTime + "]";
		}
	}

	protected class SessionRetriever implements Retriever<NetworkLayerMessage> {
		protected NetworkLayerClientSession session;
		/**
		 * @see simple.app.WorkQueue.Retriever#arePrequisitesAvailable()
		 */
		@Override
		public boolean arePrequisitesAvailable() {
			return prerequisiteManager.arePrequisitesAvailable();
		}
		/**
		 * @see simple.app.WorkQueue.Retriever#cancelPrequisiteCheck(java.lang.Thread)
		 */
		@Override
		public void cancelPrequisiteCheck(Thread thread) {
			prerequisiteManager.cancelPrequisiteCheck(thread);
		}

		public void resetAvailability() {
			prerequisiteManager.resetAvailability();
		}

		public boolean cancel(Thread thread) {
			NetworkLayerQueue.this.cancel(thread);
			return true;
			/*
			if(lock.isHeldBy(thread)) {
				thread.interrupt();
				return true;
			} else if(lock.hasQueuedThread(thread)) {
				thread.interrupt();
				return true;
			}

			return false;
			*/
		}
		public void close() {
			NetworkLayerClientSession session = this.session;
			if(session != null) {
				session.closeSession(SessionCloseReason.SHUTDOWN);
				session.lock.unlock();
				this.session = null;
			}
		}
		public Work<NetworkLayerMessage> next() throws WorkQueueException, InterruptedException {
			try {
				session = borrowObject();
			} catch(TimeoutException e) {
				log.info("Timed out getting next connection");
				return null;
			} catch(CreationException e) {
				throw new WorkQueueException(e.getCause());
			}
			if(session == null) return null;
			final String globalSessionCode = session.globalSessionCode;
			session.lock.lock();
			return new Work<NetworkLayerMessage>() {
				public String getGuid() {
					return globalSessionCode;
				}
				public QoS getQoS() {
					return DEFAULT_QOS;
				}
				public Map<String, ?> getWorkProperties() {
					//TODO: We could return a set of properties about the session here
					return null;
				}
				public long getEnqueueTime() {
					return session.getServerTime();
				}
				public boolean isRedelivered() {
					return false;
				}
				public int getRetryCount() {
					return 0;
				}
				public boolean isUnguardedRetryAllowed() {
					return true;
				}
				public NetworkLayerMessage getBody() {
					return session;
				}
				public Publisher<NetworkLayerMessage> getPublisher() throws WorkQueueException {
					return null;
				}
				public boolean isComplete() {
					return session == null;
				}
				public void workAborted(WorkRetryType retryType, boolean unguardedRetryAllowed, Throwable cause) throws WorkQueueException {
					if(session == null)
						throw new WorkQueueException("Work is already complete; workAborted() may not be called");
					//XXX: For now we will not support retries
					prerequisiteManager.resetAvailability();
					session.closeSession(SessionCloseReason.ERROR);
					session.lock.unlock();
					session = null;
				}

				public void workComplete() throws WorkQueueException {
					if(session == null)
						throw new WorkQueueException("Work is already complete; workComplete() may not be called");
					session.processingComplete();
					session.lock.unlock();
					session = null;
				}
				@Override
				public String toString() {
					return String.valueOf(session);
				}
				public String getOriginalQueueName() {
					return null;
				}
				public String getQueueName() {
					return getQueueKey();
				}
				public void unblock(String... blocksToClear) {
				}
				public String getSubscription() {
					return null;
				}
			};
		}
	}

	protected final Future<Boolean> shutdownFuture = new ResultFuture<Boolean>() {
		@Override
		protected Boolean produceResult(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
			//wait for existing sessions to finish
			long endTime = System.currentTimeMillis() + unit.toMillis(timeout);
			sessionCloseLock.lock();
			try {
				long remaining;
				while(true) {
					if(sessionCount.get() == 0)
						return true;
					remaining = endTime - System.currentTimeMillis();
					if(remaining <= 0)
						return false;
					long wait = Math.min(remaining, 5000);
					if(log.isInfoEnabled())
						log.info("Queue " + getQueueKey() + " still has " + sessionCount + " connected sessions; waiting " + wait + " milliseconds...");
					try {
						if(sessionCloseSignal == null)
							sessionCloseSignal = sessionCloseLock.newCondition();
						sessionCloseSignal.await(wait, TimeUnit.MILLISECONDS);
					} catch(InterruptedException e) {
						// Do nothing
					}
				}
			} finally {
				sessionCloseLock.unlock();
			}
		}
	};
	/**
	 *
	 */
	public NetworkLayerQueue() {
		Runtime.getRuntime().addShutdownHook(new Thread("ClientSessionCloser") {
			@Override
			public void run() {
				for(AbstractClientSession session : sessions.values()) {
					session.closeSession(SessionCloseReason.SHUTDOWN);
				}
			}
		});
	}
	@Override
	protected NetworkLayerClientSession createInterpretter(Socket socket, ByteChannel byteChannel) throws IOException {
		return new NetworkLayerClientSession(socket, byteChannel);
	}

	public String getQueueKey() {
		return String.valueOf(getSocketAddress());
	}

	public String getSessionControlQueueKey() {
		return sessionControlQueueKey;
	}

	public void setSessionControlQueueKey(String sessionControlQueueKey) {
		this.sessionControlQueueKey = sessionControlQueueKey;
	}

	public Retriever<NetworkLayerMessage> getRetriever(ProgressListener listener) throws WorkQueueException {
		return new SessionRetriever();
	}

	public Class<NetworkLayerMessage> getWorkBodyType() {
		return NetworkLayerMessage.class;
	}

	public boolean isAutonomous() {
		return true;
	}

	public Protocol getProtocol(int protocolId) throws IOException {
		Protocol protocol = protocols.get(protocolId);
		if(protocol == null)
			throw new IOException("Invalid Protocol Version " + protocolId);
		return protocol;
	}
	public void setProtocol(int protocolId, Protocol protocol) {
		protocols.put(protocolId, protocol);
	}

	public int getMaxInputSize() {
		return maxInputSize;
	}

	public void setMaxInputSize(int maxInputSize) {
		this.maxInputSize = maxInputSize;
	}

	public Publisher<ByteInput> getInboundPublisher() {
		return inboundPublisher;
	}

	public void setInboundPublisher(Publisher<ByteInput> inboundPublisher) {
		this.inboundPublisher = inboundPublisher;
	}

	public AbstractWorkQueue<ByteInput> getOutboundQueue() {
		return outboundQueue;
	}

	public void setOutboundQueue(AbstractWorkQueue<ByteInput> outboundQueue) {
		this.outboundQueue = outboundQueue;
		updateOutboundQueueKey();
	}

	/**
	 * @see simple.io.server.NIOServer#setSocketAddress(java.net.InetSocketAddress)
	 */
	@Override
	public void setSocketAddress(InetSocketAddress socketAddress) {
		super.setSocketAddress(socketAddress);
		updateOutboundQueueKey();
	}

	/**
	 *
	 */
	protected void updateOutboundQueueKey() {
		AbstractWorkQueue<ByteInput> outboundQueue = this.outboundQueue;
		InetSocketAddress socketAddress = getSocketAddress();
		if(outboundQueue != null && socketAddress != null) {
			outboundQueue.setQueueKey("netlayer-" + StringUtils.substringBefore(layerHostName, ".") + '-' + socketAddress.getPort() + ".outbound");
		}
	}

	public String getLayerName() {
		return layerName;
	}


	public void setLayerName(String layerName) {
		this.layerName = layerName;
	}

	public static NetworkLayerMessage getAndLockSession(long sessionId) throws ServiceException {
		NetworkLayerClientSession session = sessions.get(sessionId);
		if(session == null)
			throw new ServiceException("Session " + sessionId + " is no longer valid");
		session.lock();
		return session;
	}
	public static void unlockSession(NetworkLayerMessage session) {
		if(session instanceof NetworkLayerClientSession)
			((NetworkLayerClientSession)session).unlock();
	}
	public long getAppLayerResponseTimeout() {
		return appLayerResponseTimeout;
	}

	public void setAppLayerResponseTimeout(long appLayerResponseTimeout) {
		this.appLayerResponseTimeout = appLayerResponseTimeout;
	}

	public long getSessionTimeout() {
		return sessionTimeout;
	}

	public void setSessionTimeout(long sessionTimeout) {
		this.sessionTimeout = sessionTimeout;
	}
	
	public long getMessageBasedSessionEndDelay() {
		return messageBasedSessionEndDelay;
	}

	public void setMessageBasedSessionEndDelay(long messageBasedSessionEndDelay) {
		this.messageBasedSessionEndDelay = messageBasedSessionEndDelay;
	}

	public DeviceInfoManager getDeviceInfoManager() {
		return deviceInfoManager;
	}

	public void setDeviceInfoManager(DeviceInfoManager deviceInfoManager) {
		this.deviceInfoManager = deviceInfoManager;
	}

	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}

	public TranslatorFactory getTranslatorFactory() {
		return translatorFactory;
	}

	public void setTranslatorFactory(TranslatorFactory translatorFactory) {
		this.translatorFactory = translatorFactory;
	}

	public String getTranslatorContext() {
		return translatorContext;
	}

	public void setTranslatorContext(String translatorContext) {
		this.translatorContext = translatorContext;
	}

	public String getMessageProcessedQueueKey() {
		return messageProcessedQueueKey;
	}

	public void setMessageProcessedQueueKey(String messageProcessedQueueKey) {
		this.messageProcessedQueueKey = messageProcessedQueueKey;
	}

	public String getDeviceNameRegex() {
		return deviceNameRegex;
	}

	public void setDeviceNameRegex(String deviceNameRegex) {
		this.deviceNameRegex = deviceNameRegex;
		deviceNamePattern = Pattern.compile(deviceNameRegex);
	}

	public String getDeviceLimitedActivity() {
		return deviceLimitedActivityRegex;
	}

	public void setDeviceLimitedActivityRegex(String deviceLimitedActivityRegex) {
		this.deviceLimitedActivityRegex = deviceLimitedActivityRegex;
		deviceLimitedActivityPattern = Pattern.compile(deviceLimitedActivityRegex);
	}

	public boolean isMessageNumberChecked() {
		return messageNumberChecked;
	}
	public void setMessageNumberChecked(boolean messageNumberChecked) {
		this.messageNumberChecked = messageNumberChecked;
	}

	public Set<String> getServerSessionMessages() {
		return serverSessionMessages;
	}

	public void setServerSessionMessages(Set<String> serverSessionMessages) {
		this.serverSessionMessages = serverSessionMessages;
	}
	
	public int getMessageRetriesAllowed() {
		return messageRetriesAllowed;
	}

	public void setMessageRetriesAllowed(int messageRetriesAllowed) {
		this.messageRetriesAllowed = messageRetriesAllowed;
	}

	public PrerequisiteManager getPrerequisiteManager() {
		return prerequisiteManager;
	}
	/**
	 * @see simple.app.WorkQueue#shutdown()
	 */
	@Override
	public Future<Boolean> shutdown() {
		// Allow no more connections
		SelectionKey key = serverChannel.keyFor(selector);
		if(key != null)
			key.cancel();
		if(sessionCount.get() == 0)
			return TRUE_FUTURE;
		return shutdownFuture;
	}
	public Pattern getDeviceNamePattern() {
		return deviceNamePattern;
	}
	public void setDeviceNamePattern(Pattern deviceNamePattern) {
		this.deviceNamePattern = deviceNamePattern;
	}
	public Pattern getDeviceLimitedActivityPattern() {
		return deviceLimitedActivityPattern;
	}
	public void setDeviceLimitedActivityPattern(Pattern deviceLimitedActivityPattern) {
		this.deviceLimitedActivityPattern = deviceLimitedActivityPattern;
	}
	public DebugLevel getDebugLevel() {
		return debugLevel;
	}
	public void setDebugLevel(DebugLevel debugLevel) {
		this.debugLevel = debugLevel;
	}
	public boolean isInitMessageIdChecked() {
		return initMessageIdChecked;
	}
	public void setInitMessageIdChecked(boolean initMessageIdChecked) {
		this.initMessageIdChecked = initMessageIdChecked;
	}
	public boolean isNoninitMessageIdChecked() {
		return noninitMessageIdChecked;
	}
	public void setNoninitMessageIdChecked(boolean noninitMessageIdChecked) {
		this.noninitMessageIdChecked = noninitMessageIdChecked;
	}
	public Class<? extends Transmission> getTransmissionClass() {
		return transmissionClass;
	}
	public void setTransmissionClass(Class<? extends Transmission> transmissionClass) {
		this.transmissionClass = transmissionClass;
	}
	public EncryptionInfoMapping getEncryptionInfoMapping() {
		return encryptionInfoMapping;
	}
	public void setEncryptionInfoMapping(EncryptionInfoMapping encryptionInfoMapping) {
		this.encryptionInfoMapping = encryptionInfoMapping;
	}
	public int getSessionTaskCorePoolSize() {
		return sessionTaskService.getCorePoolSize();
	}
	public int getSessionTaskMaximumPoolSize() {
		return sessionTaskService.getMaximumPoolSize();
	}
	public void setSessionTaskCorePoolSize(int corePoolSize) {
		sessionTaskService.setCorePoolSize(corePoolSize);
	}
	public void setSessionTaskMaximumPoolSize(int maximumPoolSize) {
		sessionTaskService.setMaximumPoolSize(maximumPoolSize);
	}
	public BasicQoS getInlineQoS() {
		return inlineQoS;
	}

	public boolean isKeepAliveUsed() {
		return keepAliveUsed;
	}

	public void setKeepAliveUsed(boolean keepAliveUsed) {
		this.keepAliveUsed = keepAliveUsed;
	}

	/* (non-Javadoc)
	 * @see com.usatech.networklayer.app.NetworkLayerQueueMXBean#getSessionCount()
	 */
	public long getSessionCount() {
		return sessionCount.get();
	}

	/* (non-Javadoc)
	 * @see com.usatech.networklayer.app.NetworkLayerQueueMXBean#getSessionsList()
	 */
	public String getSessionsList() {
		return sessions.values().toString();
	}

	public boolean isRecordDeviceRejections() {
		return recordDeviceRejections;
	}

	public void setRecordDeviceRejections(boolean recordDeviceRejections) {
		this.recordDeviceRejections = recordDeviceRejections;
	}
}

