package com.usatech.networklayer.app;

import java.nio.ByteBuffer;
import java.util.Map;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainStep;
import com.usatech.layers.common.DeviceInfo;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.DeviceInfoProperty;
import com.usatech.layers.common.constants.SessionCloseReason;
import com.usatech.layers.common.messagedata.MessageData;

public class NetLayerUtils {
	
	public static MessageChain constructMessageChain(MessageChain messageChain, String queueKey, ByteBuffer dataBytes, boolean updateOnly, String globalSessionCode, long sessionStartTime, Byte protocolId, DeviceInfo deviceInfo, long messageStartTime, long sessionTimeout, Long lastCommandId, Map<String, Object> processAttributes, long sessionId, Map<String, Object> sessionAttributes,
			Integer pendingRequestIndex, String preProcessQueueKey, Map<String, Object> preProcessAttributes, Map<String, String> preProcessResultAttributes, String replyQueueKey) {
		MessageChainStep preStep;
		if(preProcessQueueKey != null && preProcessResultAttributes != null && !preProcessResultAttributes.isEmpty()) {
			preStep = messageChain.addStep(preProcessQueueKey);
			if(preProcessAttributes != null) {
				for(Map.Entry<String, Object> entry : preProcessAttributes.entrySet())
					preStep.addLiteralAttribute(entry.getKey(), entry.getValue());
			}
		} else {
			preStep = null;
		}
		MessageChainStep step = messageChain.addStep(queueKey);
		if(preStep != null) {
			preStep.setNextSteps(0, step);
			for(Map.Entry<String, String> entry : preProcessResultAttributes.entrySet())
				step.addReferenceAttribute(entry.getKey(), preStep, entry.getValue());
		}
		step.addStringAttribute("globalSessionCode", globalSessionCode);
		if(protocolId != null)
			step.addByteAttribute("protocol", protocolId);
		step.addStringAttribute("deviceName", deviceInfo.getDeviceName());
		step.setAttribute(DeviceInfoProperty.DEVICE_TYPE, deviceInfo.getDeviceType());
		if(dataBytes != null)
			step.addLiteralAttribute("data", dataBytes);
		step.addLongAttribute("serverTime", messageStartTime);
		step.addLongAttribute("sessionTimeout", sessionTimeout);
		step.addStringAttribute("deviceInfoHash", deviceInfo.getDeviceInfoHash());
		if(lastCommandId != null) {
			step.addLongAttribute("lastCommandId", lastCommandId);
			lastCommandId = null;
		}
		if(sessionAttributes != null) {
			for(Map.Entry<String, Object> entry : sessionAttributes.entrySet()) {
				step.addLiteralAttribute(ProcessingConstants.SESSION_ATTRIBUTE_PREFIX + entry.getKey(), entry.getValue());
			}
		}
		if(processAttributes != null) {
			for(Map.Entry<String, Object> entry : processAttributes.entrySet())
				step.addLiteralAttribute(entry.getKey(), entry.getValue());
		}
		if(replyQueueKey != null) {
			MessageChainStep replyStep = messageChain.addStep(replyQueueKey);
			if(!updateOnly) {
				replyStep.addLongAttribute("sessionId", sessionId);
				replyStep.addReferenceAttribute("reply", step, "reply");
				replyStep.addReferenceAttribute("replyTemplate", step, "replyTemplate");
				replyStep.addReferenceAttribute("endSession", step, "endSession");
				replyStep.addReferenceAttribute("lastCommandId", step, "commandId");
				replyStep.addReferenceAttribute("fileTransferId", step, "fileTransferId");
				replyStep.addReferenceAttribute("pendingCommandId", step, "pendingCommandId");
				replyStep.addReferenceAttribute("fileGroup", step, "fileGroup");
				replyStep.addReferenceAttribute("fileTransferKey", step, "fileTransferKey");
				replyStep.addReferenceAttribute("filePacketSize", step, "filePacketSize");
				replyStep.addReferenceAttribute("fileName", step, "fileName");
				replyStep.addReferenceAttribute("fileType", step, "fileType");
				replyStep.addReferenceAttribute("fileSize", step, "fileSize");
				replyStep.addReferenceAttribute("fileChunk", step, "fileChunk");
				replyStep.addReferenceAttribute("fileChunkOffset", step, "fileChunkOffset");
				replyStep.addReferenceAttribute("newPassword", step, "newPassword");
				replyStep.addReferenceMapAttribute("sessionAttributes", step, ProcessingConstants.SESSION_ATTRIBUTE_PREFIX);
				if(pendingRequestIndex != null)
					replyStep.addIntAttribute("requestId", pendingRequestIndex);
				step.setNextSteps(0, replyStep);
			} else {
				replyStep.addStringAttribute("deviceName", deviceInfo.getDeviceName());
				replyStep.addBooleanAttribute("updateOnly", true);
				step.setAttribute(DeviceInfoProperty.TIME_ZONE_GUID, deviceInfo.getTimeZoneGuid());
				step.setAttribute(DeviceInfoProperty.DEVICE_CHARSET, deviceInfo.getDeviceCharset());
			}
			// device info
			if(deviceInfo.isInitOnly()) { // get "real" deviceInfo and allow updates of more properties
				replyStep.addReferenceAttribute("newDeviceName", step, "newDeviceName");
				for(DeviceInfoProperty dip : new DeviceInfoProperty[] { DeviceInfoProperty.DEVICE_TYPE, DeviceInfoProperty.INIT_ONLY, DeviceInfoProperty.STORE_DEVICE_NAME }) {
					replyStep.addReferenceAttribute(dip.getAttributeKey(), step, dip.getAttributeKey());
				}
			}
			for(DeviceInfoProperty dip : DeviceInfoProperty.values()) {
				switch(dip) {
					case DEVICE_TYPE:
					case DEVICE_NAME:
					case INIT_ONLY:
					case STORE_DEVICE_NAME:
						break;
					default:
						replyStep.addReferenceAttribute(dip.getAttributeKey(), step, dip.getAttributeKey());
						replyStep.addReferenceAttribute(dip.getAttributeKey() + ".timestamp", step, dip.getAttributeKey() + ".timestamp");
				}
			}

			replyStep.addReferenceAttribute("removeDeviceInfo", step, "removeDeviceInfo");
			step.setNextSteps(5, replyStep);
		} else {
			step.setAttribute(DeviceInfoProperty.TIME_ZONE_GUID, deviceInfo.getTimeZoneGuid());
			step.setAttribute(DeviceInfoProperty.DEVICE_CHARSET, deviceInfo.getDeviceCharset());
		}
		return messageChain;
	}

	public static MessageChainStep constructReplyStep(MessageChain messageChain, String replyQueue, MessageData reply, SessionCloseReason endSessionReason, Long lastCommandId, Integer pendingRequestIndex, long sessionId, ByteBuffer writeBuffer) {
		if(replyQueue != null) {
			MessageChainStep replyStep = messageChain.addStep(replyQueue);
			replyStep.addLongAttribute("sessionId", sessionId);
			if(pendingRequestIndex != null)
				replyStep.addIntAttribute("requestId", pendingRequestIndex);
			writeBuffer.clear();
			reply.writeData(writeBuffer, false);
			writeBuffer.flip();
			byte[] bytes = new byte[writeBuffer.remaining()];
			writeBuffer.get(bytes);
			replyStep.addLiteralAttribute("reply", bytes);
			replyStep.setAttribute(CommonAttrEnum.ATTR_END_SESSION, endSessionReason);
			if(lastCommandId != null)
				replyStep.addLongAttribute("lastCommandId", lastCommandId);
			return replyStep;
		}
		return null;
	}

	public static MessageChainStep constructReplyTemplateStep(MessageChain messageChain, String replyQueue, MessageData reply, SessionCloseReason endSessionReason, Long lastCommandId, Integer pendingRequestIndex, long sessionId, ByteBuffer writeBuffer) {
		if(replyQueue != null) {
			MessageChainStep replyStep = messageChain.addStep(replyQueue);
			replyStep.addLongAttribute("sessionId", sessionId);
			if(pendingRequestIndex != null)
				replyStep.addIntAttribute("requestId", pendingRequestIndex);
			writeBuffer.clear();
			reply.writeData(writeBuffer, false);
			writeBuffer.flip();
			byte[] bytes = new byte[writeBuffer.remaining()];
			writeBuffer.get(bytes);
			replyStep.addLiteralAttribute("replyTemplate", bytes);
			replyStep.setAttribute(CommonAttrEnum.ATTR_END_SESSION, endSessionReason);
			if(lastCommandId != null)
				replyStep.addLongAttribute("lastCommandId", lastCommandId);
			return replyStep;
		}
		return null;
	}
}
