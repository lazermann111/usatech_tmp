package com.usatech.networklayer.app;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.concurrent.locks.ReentrantLock;

import simple.io.BinaryStream;
import simple.io.ByteOutput;
import simple.io.IOUtils;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.DeviceInfo;
import com.usatech.layers.common.constants.CRCType;
import com.usatech.layers.common.constants.DeviceInfoProperty;
import com.usatech.layers.common.constants.InvalidCRCException;

public class InboundFileTransfer extends OutputStream implements BinaryStream {
	protected int packetsReceived;
	protected long bytesReceived;
	protected int packetsExpected;
	protected long bytesExpected;
	protected int fileTypeId;
	protected String fileName;
	protected byte[] createTime;
	protected long eventId;
	protected final String queueKey;
	protected final long startTime;
	protected Resource resource;
	protected OutputStream out;
	protected final String netlayerName;
	protected final String netlayerHost;
	protected final int netlayerPort;
	protected final String globalSessionCode;
	protected String resourceKey;
	protected CRCType.Calculator crcCalc;
	protected CRCType crcType;
	protected byte[] crc;
	protected boolean checkCrc;
	protected final String deviceName;
	protected final ReentrantLock lock = new ReentrantLock();

	public InboundFileTransfer(ResourceFolder resourceFolder, String directory, int packetsExpected, long bytesExpected, int fileTypeId, String fileName, String queueKey, String netlayerName, String netlayerHost, int netlayerPort, String globalSessionCode, String deviceName) throws IOException {
		this(queueKey, netlayerName, netlayerHost, netlayerPort, globalSessionCode, deviceName);
		this.packetsExpected = packetsExpected;
		this.bytesExpected = bytesExpected;
		this.fileTypeId = fileTypeId;
		setFileInfo(resourceFolder, directory, fileName);
	}
	public InboundFileTransfer(String queueKey, String netlayerName, String netlayerHost, int netlayerPort, String globalSessionCode, String deviceName) {
		super();
		this.queueKey = queueKey;
		this.startTime = System.currentTimeMillis();
		this.netlayerName = netlayerName;
		this.netlayerHost = netlayerHost;
		this.netlayerPort = netlayerPort;
		this.globalSessionCode = globalSessionCode;
		this.deviceName = deviceName;
	}

	@Override
	public void write(byte[] data, int offset, int length) throws IOException {
		if(crcCalc != null) {
			crcCalc.update(data, offset, length);
		}
		getOutputStream().write(data, offset, length);
		bytesReceived += length;
	}
	/**
	 * @see java.io.OutputStream#write(int)
	 */
	@Override
	public void write(int b) throws IOException {
		if(crcCalc != null) {
			crcCalc.update((byte)b);
		}
		getOutputStream().write(b);
		bytesReceived++;
	}
	public void checkCRC() throws InvalidCRCException {
		byte[] calced;
		if(crcCalc != null && crc != null && !Arrays.equals((calced=crcCalc.getValue()), crc)) {
			throw new InvalidCRCException(crcType, crc, calced);
		}
	}

	public MessageChainStep constructFileTransferStep(MessageChain mc, DeviceInfo deviceInfo) {
		MessageChainStep step = mc.addStep(queueKey);
		step.addStringAttribute("deviceName", deviceName);
		step.setAttribute(DeviceInfoProperty.DEVICE_TYPE, deviceInfo.getDeviceType());
		step.setAttribute(DeviceInfoProperty.TIME_ZONE_GUID, deviceInfo.getTimeZoneGuid());
		step.setAttribute(DeviceInfoProperty.DEVICE_CHARSET, deviceInfo.getDeviceCharset());
		step.addStringAttribute("fileName", fileName);
		step.addStringAttribute("resourceKey", resourceKey);
		step.addIntAttribute("fileType", fileTypeId);
		step.addStringAttribute("globalSessionCode", globalSessionCode);
		step.addLongAttribute("startTime", startTime);
		step.addLongAttribute("endTime", System.currentTimeMillis());
		if(crcType != null)
			step.addByteAttribute("crcType", crcType.getValue());
		if(crc != null)
			step.addByteArrayAttribute("crc", crc);
		if(createTime != null)
			step.addByteArrayAttribute("createTime", createTime);
		if(eventId != 0)
			step.addLongAttribute("eventId", eventId);
		return step;
	}

	public MessageChain constructMessageChain(DeviceInfo deviceInfo) {
		MessageChain mc = new MessageChainV11();
		constructFileTransferStep(mc, deviceInfo);
		return mc;
	}

	public int getPacketsRemaining() {
		return packetsExpected - packetsReceived;
	}

	public long getBytesRemaining() {
		return bytesExpected - bytesReceived;
	}

	public boolean cancel() {
		lock.lock();
		try {
			boolean cancelled = false;
			if(resource != null) {
				cancelled = resource.delete();
				resource.release();
				resource = null;
			}
			return cancelled;
		} finally {
			lock.unlock();
		}
	}

	public int getFileTypeId() {
		return fileTypeId;
	}

	public String getFileName() {
		return fileName;
	}

	public int getPacketsReceived() {
		return packetsReceived;
	}

	public void incrementPacketsReceived() {
		this.packetsReceived++;
	}

	public long getBytesReceived() {
		return bytesReceived;
	}

	public int getPacketsExpected() {
		return packetsExpected;
	}
	public void setPacketsExpected(int packetsExpected) {
		this.packetsExpected = packetsExpected;
	}
	public long getBytesExpected() {
		return bytesExpected;
	}
	public void setBytesExpected(long bytesExpected) {
		this.bytesExpected = bytesExpected;
	}
	public void setFileTypeId(int fileTypeId) {
		this.fileTypeId = fileTypeId;
	}
	public void setFileInfo(ResourceFolder resourceFolder, String directory, String fileName) throws IOException {
		if(fileName == null || (fileName=fileName.trim()).length() == 0)
			fileName = generateFileName();
		this.fileName = fileName;
		this.resource = resourceFolder.getResource(directory + "/type_" + fileTypeId + "/" + fileName, ResourceMode.CREATE);
		this.resourceKey = resource.getKey();
	}
	/**
	 * @return
	 */
	protected String generateFileName() {
		//<device_name>-<file type name of id>
		return generateFileName(deviceName, fileTypeId);
	}
	public static String generateFileName(String deviceName, int fileTypeId) {
		return (deviceName == null || deviceName.trim().length() == 0 ? "UNKNOWN" : deviceName) + '-' + getFileTypeSuffix(fileTypeId);
	}
	protected static String getFileTypeSuffix(int fileTypeId) {
		switch(fileTypeId) {
			case 0: return "DEX"; // DEX File
			case 1: return "CFG"; // Configuration File
			case 2: return "PEEK"; // Gx Peek Response
			case 3: return "DEX"; // DEX File for Fill to Fill
			case 4: return "FILE"; // eSuds Generic File
			case 5: return "ASU"; // Application Software Upgrade
			case 6: return "CFG"; // G4 Configuration Data Template
			case 7: return "EXE"; // Executable File
			case 8: return "EXE"; // Executable File that Requires App Restart
			case 9: return "LOG"; // Log File
			case 10: return "DAT"; // Generic Data File from EZ80 ePort
			case 11: return "DEX"; // DEX File from EZ80 ePort
			case 12: return "MAIL"; // Auto Email
			case 13: return "BTL"; // Bootloader
			case 14: return "MTP"; // Multiplexor application
			case 15: return "CFG"; // Kiosk Configuration Template
			case 16: return "FILE"; // Kiosk Generic File
			case 17: return "MM"; // Memory Map CSV
			case 18: return "BIN"; // Generic Binary Data
			case 19: return "PLV"; // Property List
			case 20: return "PLR"; // Property List Request
			case 21: return "MSG"; // Message Set
			case 22: return "CFG"; // Edge Default Configuration Template
			case 23: return "CFG"; // Edge Custom Configuration Template
			default: return String.valueOf(fileTypeId);

		}
	}
	public byte[] getCreateTime() {
		return createTime;
	}
	public void setCreateTime(byte[] createTime) {
		this.createTime = createTime;
	}
	public long getEventId() {
		return eventId;
	}
	public void setEventId(long eventId) {
		this.eventId = eventId;
	}

	public void close() throws IOException {
		finish();
	}
	public void finish() throws IOException {
		lock.lock();
		try {
			if(resource != null) {
				closeOutputStream();
				resource.release();
				resource = null;
			}
		} finally {
			lock.unlock();
		}
	}
	public CRCType getCrcType() {
		return crcType;
	}
	public void setCrcType(CRCType crcType) {
		this.crcType = crcType;
		if(checkCrc && crcType != null)
			crcCalc = crcType.newCalculator();
		else
			crcCalc = null;
	}
	public byte[] getCrc() {
		return crc;
	}
	public void setCrc(byte[] crc) {
		this.crc = crc;
	}
	public boolean isCheckCrc() {
		return checkCrc;
	}
	public void setCheckCrc(boolean checkCrc) {
		this.checkCrc = checkCrc;
		if(checkCrc && crcType != null)
			crcCalc = crcType.newCalculator();
		else
			crcCalc = null;
	}
	public InputStream getInputStream() throws IOException {
		lock.lock();
		try {
			if(resource == null)
				throw new IOException("File Transfer is not initialized or is cancelled");
			closeOutputStream();
			return resource.getInputStream();
		} finally {
			lock.unlock();
		}
	}
	@Override
	public long getLength() {
		return getBytesReceived();
	}
	@Override
	public long copyInto(ByteOutput output) throws IOException {
		return IOUtils.copy(getInputStream(), output, 0L, getLength());
	}
	public OutputStream getOutputStream() throws IOException {
		lock.lock();
		try {
			if(out == null) {
				if(resource == null)
					throw new IOException("File Transfer is not initialized or is cancelled");
				out = resource.getOutputStream();
			}
			return out;
		} finally {
			lock.unlock();
		}
	}
	protected void closeOutputStream() throws IOException {
		lock.lock();
		try {
			if(out != null) {
				out.flush();
				out.close();
				out = null;
			}
		} finally {
			lock.unlock();
		}
	}
}
