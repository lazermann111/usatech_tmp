package com.usatech.networklayer.processors;

import simple.app.Processor;
import simple.app.ServiceException;

import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.networklayer.app.NetworkLayerMessage;

public abstract class ReplyOnlyProcessor implements Processor<NetworkLayerMessage, Void> {
	public Void process(NetworkLayerMessage argument) throws ServiceException {
		MessageData replyData = buildReply(argument);
		argument.sendReply(replyData);
		return null;
	}

	protected abstract MessageData buildReply(NetworkLayerMessage argument) throws ServiceException ;

	public Class<NetworkLayerMessage> getArgumentType() {
		return NetworkLayerMessage.class;
	}

	public Class<Void> getReturnType() {
		return null;
	}
}
