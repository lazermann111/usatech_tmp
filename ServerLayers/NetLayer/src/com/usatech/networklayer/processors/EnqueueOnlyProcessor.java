package com.usatech.networklayer.processors;

import com.usatech.app.AsyncTaskExecutor;
import com.usatech.networklayer.app.NetworkLayerMessage;

import simple.app.Processor;
import simple.app.ServiceException;

public class EnqueueOnlyProcessor implements Processor<NetworkLayerMessage, Void> {
	protected String queueKey;
	protected AsyncTaskExecutor asyncTaskExecutor;

	public Void process(NetworkLayerMessage argument) throws ServiceException {
		argument.enqueueExpectingReply(getQueueKey(), null, null, null, null);
		if (asyncTaskExecutor != null)
			asyncTaskExecutor.process();
		return null;
	}

	public String getQueueKey() {
		return queueKey;
	}

	public void setQueueKey(String queueKey) {
		this.queueKey = queueKey;
	}
	
	public AsyncTaskExecutor getAsyncTaskExecutor() {
		return asyncTaskExecutor;
	}

	public void setAsyncTaskExecutor(AsyncTaskExecutor asyncTaskExecutor) {
		this.asyncTaskExecutor = asyncTaskExecutor;
		asyncTaskExecutor.init();
	}

	public Class<NetworkLayerMessage> getArgumentType() {
		return NetworkLayerMessage.class;
	}

	public Class<Void> getReturnType() {
		return null;
	}
}
