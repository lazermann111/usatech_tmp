/**
 *
 */
package com.usatech.networklayer.processors;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import simple.app.Processor;
import simple.app.ServiceException;
import simple.bean.MaskedString;

import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.SaleResult71;
import com.usatech.layers.common.messagedata.LegacyLocalAuth;
import com.usatech.layers.common.messagedata.MessageData_71;
import com.usatech.networklayer.app.NetworkLayerMessage;

/**
 * @author Brian S. Krug
 *
 */
public class EnqueueAndReplyProcessor_LocalAuth implements Processor<NetworkLayerMessage, Void> {
	protected static final Map<String, String> decryptionResultAttributes = new HashMap<String, String>();
	static {
		decryptionResultAttributes.put(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID.getValue(), AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID.getValue());
		decryptionResultAttributes.put(AuthorityAttrEnum.ATTR_TRACK_CRC.getValue(), AuthorityAttrEnum.ATTR_TRACK_CRC.getValue());
		decryptionResultAttributes.put(AuthorityAttrEnum.ATTR_TRACK_CRC_MATCH.getValue(), AuthorityAttrEnum.ATTR_TRACK_CRC_MATCH.getValue());
	}
	protected String lookupQueueKey;
	protected String queueKey;

	/**
	 * @see com.usatech.networklayer.processors.EnqueueOnlyProcessor#process(com.usatech.networklayer.app.NetworkLayerMessage)
	 */
	@Override
	public Void process(NetworkLayerMessage argument) throws ServiceException {
		LegacyLocalAuth authData = (LegacyLocalAuth) argument.getData();
		long transactionId = authData.getTransactionId();
		MessageData_71 reply = new MessageData_71();
		reply.setTransactionId(transactionId);
		reply.setSaleResult(SaleResult71.PASS);
		switch(authData.getCardType()) {
			case CASH:
			case ERROR:
			case INVENTORY_CARD:
				argument.incrementEventCount();
				argument.enqueueNoReply(getQueueKey(), null, null, null, null);
				break;
			default:
				argument.incrementSaleCount();
				argument.enqueueNoReply(getQueueKey(), null, getLookupQueueKey(), Collections.singletonMap(AuthorityAttrEnum.ATTR_ACCOUNT_DATA.getValue(), (Object) new MaskedString(MessageResponseUtils.cleanTrack(authData.getCreditCardMagstripe()))), decryptionResultAttributes);
				break;
		}

		argument.sendReply(reply);
		return null;
	}

	public String getLookupQueueKey() {
		return lookupQueueKey;
	}

	public void setLookupQueueKey(String lookupQueueKey) {
		this.lookupQueueKey = lookupQueueKey;
	}

	public String getQueueKey() {
		return queueKey;
	}

	public void setQueueKey(String queueKey) {
		this.queueKey = queueKey;
	}

	public Class<NetworkLayerMessage> getArgumentType() {
		return NetworkLayerMessage.class;
	}

	public Class<Void> getReturnType() {
		return null;
	}
}
