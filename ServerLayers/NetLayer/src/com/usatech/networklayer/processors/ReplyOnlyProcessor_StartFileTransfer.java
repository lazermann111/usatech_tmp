package com.usatech.networklayer.processors;

import java.io.IOException;
import java.nio.BufferUnderflowException;

import simple.app.Processor;
import simple.app.ServiceException;
import simple.io.Log;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.FileType;
import com.usatech.layers.common.constants.GenericResponseResultCode;
import com.usatech.layers.common.constants.GenericResponseServerActionCode;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageData_7C;
import com.usatech.layers.common.messagedata.MessageData_7D;
import com.usatech.layers.common.messagedata.MessageData_91;
import com.usatech.layers.common.messagedata.MessageData_A4;
import com.usatech.layers.common.messagedata.MessageData_A5;
import com.usatech.layers.common.messagedata.MessageData_C8;
import com.usatech.layers.common.messagedata.MessageData_CB;
import com.usatech.networklayer.app.InboundFileTransfer;
import com.usatech.networklayer.app.NetworkLayerMessage;

public class ReplyOnlyProcessor_StartFileTransfer implements Processor<NetworkLayerMessage, Void> {
	private static final Log log = Log.getLog();
	protected String directory;
	protected String queueKey;
	
	public Class<NetworkLayerMessage> getArgumentType() {
		return NetworkLayerMessage.class;
	}
	public Class<Void> getReturnType() {
		return null;
	}
	public Void process(NetworkLayerMessage argument) throws ServiceException  {
		MessageData data = argument.getData();
		//Assume message type 7C or A4 or C8 or C7
		InboundFileTransfer fileTransfer = new InboundFileTransfer(getQueueKey(), argument.getLayerName(), argument.getLayerHost(), argument.getLayerPort(), argument.getGlobalSessionCode(), argument.getDeviceName());
		try {
			byte group;
			MessageData reply;
			switch(data.getMessageType()) {
				case FILE_XFER_START_2_0: //0x7C:
					MessageData_7C data7C = (MessageData_7C)data;
					fileTransfer.setPacketsExpected(data7C.getTotalPackets());
					group = data7C.getGroup();
					fileTransfer.setFileTypeId(data7C.getFileType().getValue());
					//Note: only for G4, Peek response, set bytes expected to totalBytes-1, the device sends 7E with missing 1 byte
					if(argument.getDeviceInfo().getDeviceType()==DeviceType.G4&&data7C.getFileType()==FileType.GX_PEEK_RESPONSE){
						fileTransfer.setBytesExpected(data7C.getTotalBytes()-1);
					}else{
						fileTransfer.setBytesExpected(data7C.getTotalBytes());
					}
					fileTransfer.setFileInfo(argument.getResourceFolder(), getDirectory(),  data7C.getFileName());
					MessageData_7D reply7D = new MessageData_7D();
					reply7D.setGroup(data7C.getGroup());
					reply = reply7D;
					break;
				case FILE_XFER_START_3_0: //(byte)0xA4:
					MessageData_A4 dataA4 = (MessageData_A4)data;
					fileTransfer.setPacketsExpected(dataA4.getTotalPackets());
					fileTransfer.setBytesExpected(dataA4.getTotalBytes());
					group = dataA4.getGroup();
					fileTransfer.setFileTypeId(dataA4.getFileType().getValue());
					fileTransfer.setFileInfo(argument.getResourceFolder(), getDirectory(),  dataA4.getFileName());
					MessageData_A5 replyA5 = new MessageData_A5();
					replyA5.setGroup(dataA4.getGroup());
					reply = replyA5;
					break;
				case FILE_XFER_START_4_1: //(byte)0xC8:
					MessageData_C8 dataC8 = (MessageData_C8)data;
					fileTransfer.setCreateTime(ProcessingUtils.timestampToBytes(dataC8.getCreationTime()));
					fileTransfer.setEventId(dataC8.getEventId());
					fileTransfer.setPacketsExpected(dataC8.getTotalPackets());
					fileTransfer.setBytesExpected(dataC8.getTotalBytes());
					fileTransfer.setFileTypeId(dataC8.getFileType().getValue());
					fileTransfer.setCrcType(dataC8.getCrc().getCrcType());
					fileTransfer.setCrc(dataC8.getCrc().getCrcValue());
					fileTransfer.setCheckCrc(true); //XXX: Should we check crc in network layer?
					fileTransfer.setFileInfo(argument.getResourceFolder(), getDirectory(), dataC8.getFileName());
					group = 0;
					MessageData_CB replyCB = new MessageData_CB();
					replyCB.setResultCode(GenericResponseResultCode.OKAY);
					replyCB.setResponseMessage(argument.getTranslator().translate("client.message.starting-file-transfer", "Starting File transfer with {0} parts", fileTransfer.getPacketsExpected()));
					replyCB.setServerActionCode(GenericResponseServerActionCode.NO_ACTION);
					reply = replyCB;
					break;
				default:
					throw new ServiceException("Invalid configuration; This class cannot handle message '" + data.getMessageType() + "'");
			}
			if(argument.removeInboundFileTransfer(group)) {
				log.info("Removed file transfer " + group + " because device sent another " + data.getMessageType() + " message with the same group id");
			}
			argument.registerInboundFileTransfer(group, fileTransfer);
			argument.sendReply(reply);
		} catch(IOException e) {
			log.warn("Error while trying to process file transfer", e);
			fileTransfer.cancel();
			argument.sendReply(constructNakReply(argument, data, GenericResponseResultCode.APPLAYER_ERROR, "client.message.server-error", "Error occured on Server"));
		} catch(BufferUnderflowException e) {
			log.warn("Invalid Command - not enough data from device '" + argument.getDeviceName() + "'", e);
			fileTransfer.cancel();
			argument.sendReply(constructNakReply(argument, data, GenericResponseResultCode.APPLAYER_ERROR, "client.message.data-truncated", "Invalid Message"));
		} catch(ServiceException e) {
			fileTransfer.cancel();
			throw e;
		}
		return null;
	}

	protected MessageData constructNakReply(NetworkLayerMessage argument, MessageData data, GenericResponseResultCode resultCode, String errorCode, String errorMessage) throws ServiceException{
		switch(data.getMessageType()) {
			case FILE_XFER_START_2_0:
			case FILE_XFER_START_3_0:
				MessageData_91 reply91 = new MessageData_91();
				reply91.setNakType((byte)0);
				return reply91;
			case FILE_XFER_START_4_1:
			case SHORT_FILE_XFER_4_1:
				MessageData_CB replyCB = new MessageData_CB();
				replyCB.setResultCode(resultCode);
				replyCB.setResponseMessage(argument.getTranslator().translate(errorCode, errorMessage));
				replyCB.setServerActionCode(argument.getDeviceInfo().getActionCode().getGenericResponseServerActionCode());
				return replyCB;
			default:
				throw new ServiceException("Invalid configuration; This class cannot handle message '" + data.getMessageType() + "'");
		}
	}
	public String getDirectory() {
		return directory;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}

	public String getQueueKey() {
		return queueKey;
	}

	public void setQueueKey(String queueKey) {
		this.queueKey = queueKey;
	}
}
