package com.usatech.networklayer.processors;

import simple.app.Processor;
import simple.app.ServiceException;

import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.networklayer.app.NetworkLayerMessage;

public abstract class EnqueueAndReplyProcessor implements Processor<NetworkLayerMessage, Void> {
	protected String queueKey;

	public Void process(NetworkLayerMessage argument) throws ServiceException {
		argument.enqueueNoReply(getQueueKey(), null, null, null, null);
		MessageData replyData = buildReply(argument);
		argument.sendReply(replyData);
		return null;
	}

	public String getQueueKey() {
		return queueKey;
	}

	public void setQueueKey(String queueKey) {
		this.queueKey = queueKey;
	}

	protected abstract MessageData buildReply(NetworkLayerMessage argument) throws ServiceException ;

	public Class<NetworkLayerMessage> getArgumentType() {
		return NetworkLayerMessage.class;
	}

	public Class<Void> getReturnType() {
		return null;
	}
}
