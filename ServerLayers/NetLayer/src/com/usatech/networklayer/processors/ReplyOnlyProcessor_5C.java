package com.usatech.networklayer.processors;

import simple.app.Processor;
import simple.app.ServiceException;

import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageData_2F;
import com.usatech.networklayer.app.NetworkLayerMessage;

public class ReplyOnlyProcessor_5C implements Processor<NetworkLayerMessage, Void> {
	protected String fallThruQueueKey;

	public Void process(NetworkLayerMessage argument) throws ServiceException {
		MessageData reply = buildReply(argument);
		if(reply == null) {
			// Send to AppLayer for processing
			argument.enqueueExpectingReply(getFallThruQueueKey(), null, null, null, null);
		} else {
			argument.sendReply(reply);
		}
		return null;
	}

	public Class<NetworkLayerMessage> getArgumentType() {
		return NetworkLayerMessage.class;
	}

	public Class<Void> getReturnType() {
		return null;
	}

	/**
	 * @param argument
	 * @return The reply to send or null if it should be forwarded to applayer
	 * @throws ServiceException
	 */
	protected MessageData buildReply(NetworkLayerMessage argument) throws ServiceException {
		DeviceType deviceType= argument.getDeviceInfo().getDeviceType();
		if(deviceType==DeviceType.ESUDS){
			return null;
		}else{
			MessageData_2F reply = new MessageData_2F();
			reply.setAckedMessageNumber(argument.getData().getMessageNumber());
			return reply;
		}
	}

	public String getFallThruQueueKey() {
		return fallThruQueueKey;
	}

	public void setFallThruQueueKey(String fallThruQueueKey) {
		this.fallThruQueueKey = fallThruQueueKey;
	}
}
