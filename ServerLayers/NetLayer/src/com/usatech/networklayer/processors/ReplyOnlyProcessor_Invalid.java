package com.usatech.networklayer.processors;

import java.util.HashSet;
import java.util.Set;

import simple.app.ServiceException;
import simple.text.StringUtils;

import com.usatech.layers.common.constants.GenericResponseResultCode;
import com.usatech.layers.common.constants.GenericResponseServerActionCode;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageData_91;
import com.usatech.layers.common.messagedata.MessageData_CB;
import com.usatech.layers.common.messagedata.MessageData_Unknown;
import com.usatech.networklayer.app.NetworkLayerMessage;

public class ReplyOnlyProcessor_Invalid extends ReplyOnlyProcessor {
	protected final Set<String> batchMessageTypes = new HashSet<String>();

	public ReplyOnlyProcessor_Invalid() {
		batchMessageTypes.add("C4");
		batchMessageTypes.add("C5");
		batchMessageTypes.add("C6");

	}
	@Override
	protected MessageData buildReply(NetworkLayerMessage argument) throws ServiceException {
		switch(argument.getDeviceInfo().getDeviceType()) {
			case EDGE:
				MessageData_CB replyCB = new MessageData_CB();
				if(argument.getData() instanceof MessageData_Unknown && batchMessageTypes.contains(StringUtils.toHex(((MessageData_Unknown) argument.getData()).getMessageTypeBytes())))
					replyCB.setResultCode(GenericResponseResultCode.OKAY); // so that device does not keep sending it
				else
					replyCB.setResultCode(GenericResponseResultCode.NOT_SUPPORTED);
				replyCB.setServerActionCode(GenericResponseServerActionCode.SEND_UPDATE_STATUS_REQUEST_IMMEDIATE);
				return replyCB;
			default:
				MessageData_91 reply91 = new MessageData_91();
				reply91.setNakType((byte)1);
				return reply91;
		}
	}

	public String[] getBatchMessageTypes() {
		return batchMessageTypes.toArray(new String[batchMessageTypes.size()]);
	}

	public void setBatchMessageTypes(String[] batchMessageTypeArray) {
		batchMessageTypes.clear();
		if(batchMessageTypeArray != null)
			for(String mt : batchMessageTypeArray)
				batchMessageTypes.add(mt);
	}
}
