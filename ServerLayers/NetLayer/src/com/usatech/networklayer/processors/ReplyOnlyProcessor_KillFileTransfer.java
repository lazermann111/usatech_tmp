package com.usatech.networklayer.processors;

import simple.app.ServiceException;
import simple.io.Log;

import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageData_80;
import com.usatech.layers.common.messagedata.MessageData_81;
import com.usatech.networklayer.app.NetworkLayerMessage;
import com.usatech.networklayer.app.OutboundFileTransfer;

public class ReplyOnlyProcessor_KillFileTransfer extends ReplyOnlyProcessor {
	private static final Log log = Log.getLog();
	
	@Override
	protected MessageData buildReply(NetworkLayerMessage argument) throws ServiceException {
		MessageData_80 data = (MessageData_80)argument.getData();
		byte group = data.getGroup();
		OutboundFileTransfer ft = argument.getOutboundFileTransfer(group);
		if(ft==null){
			throw new ServiceException("Failed to get file. Invalid file transfer group number:"+group);
		}else{
			if(!ft.cancel()){
				log.info("Failed to cancel outbound file transfer "+ group + " for device '" + argument.getDeviceName() + "'");
			}
		}
		MessageData_81 replyKill = new MessageData_81();
		replyKill.setGroup(group);
		return replyKill;
	}

}
