package com.usatech.networklayer.processors;

import java.nio.BufferUnderflowException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.usatech.app.Attribute;
import com.usatech.layers.common.MessageProcessingUtils;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.DeniedReason;
import com.usatech.layers.common.constants.DeviceInfoProperty;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.PaymentActionType;
import com.usatech.layers.common.messagedata.AppAuthData;
import com.usatech.layers.common.messagedata.AuthorizeMessageData;
import com.usatech.layers.common.messagedata.CardReader;
import com.usatech.layers.common.messagedata.EBeaconAppCardReader;
import com.usatech.layers.common.messagedata.EncryptingCardReader;
import com.usatech.layers.common.messagedata.EncryptingCardReaderV4;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageData_C2;
import com.usatech.layers.common.messagedata.PlainCardReader;
import com.usatech.layers.common.messagedata.PlainCardReaderV4;
import com.usatech.layers.common.messagedata.RawCardReader;
import com.usatech.networklayer.app.NetworkLayerMessage;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.MaskedString;
import simple.io.ByteArrayUtils;
import simple.io.Log;
import simple.text.StringUtils;

/**
 * @author Brian S. Krug
 *
 */
public class EnqueueOnlyProcessor_Auth extends EnqueueOnlyProcessor {
    private static final Log log = Log.getLog();

	protected String decryptionQueueKey;
	protected String tokenQueueKey;
	protected String lookupQueueKey;
	protected String validationRegex = "[%;]?([\\x20-\\x3E\\x40-\\x7F]+)(?:\\?[\\x01-\\xFF]?)?(?:\\x00[\\x00-\\xFF]*)?";
	protected String[] appKeyAliases;
	protected List<String> appKeyAliasList;

    // (C|0)|AVAS|<passTypeId>|<cardId>|<token>
    protected static String vasRegex="^((C|0)\\|){0,1}AVAS\\|(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])\\|[1-9][0-9]{1,19}\\|[a-fA-F0-9]+";

	protected static final Attribute[] LOOKUP_RESULT_ATTRIBUTES = new Attribute[] {
		AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID,
		AuthorityAttrEnum.ATTR_ACCOUNT_CD_HASH,
		AuthorityAttrEnum.ATTR_TRACK_CRC,
		AuthorityAttrEnum.ATTR_TRACK_CRC_MATCH,
		AuthorityAttrEnum.ATTR_INSTANCE
	};

	protected static final Attribute[] DECRYPTION_RESULT_ATTRIBUTES = new Attribute[] {
		AuthorityAttrEnum.ATTR_AUTH_RESULT_CD,
		AuthorityAttrEnum.ATTR_DENIED_REASON,
		AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD,
		AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC
	};

	/**
	 * @see com.usatech.networklayer.processors.EnqueueOnlyProcessor#process(com.usatech.networklayer.app.NetworkLayerMessage)
	 */
	@Override
	public Void process(NetworkLayerMessage argument) throws ServiceException {
		Map<String, Object> processAttributes = new HashMap<String, Object>(6);
		processAttributes.put(DeviceInfoProperty.MASTER_ID.getValue(), argument.getDeviceInfo().getMasterId());
		AuthorizeMessageData authData = (AuthorizeMessageData) argument.getData();

		CardReader cardReader = authData.getCardReader();
		CardReaderType cardReaderType = authData.getCardReaderType();
		processCardReader(argument, cardReader, cardReaderType, authData.getEntryType(), authData.getPaymentActionType(), processAttributes, null);
		if (asyncTaskExecutor != null)
			asyncTaskExecutor.process();
		return null;
	}

	private boolean isValidVasTag(MessageData_C2.VASTag vasTag) {
	    return vasTag != null && vasTag.getMerchantId() != null && vasTag.getMerchantId().length == 32 &&
                vasTag.getTagType() == MessageData_C2.TagType.V && vasTag.getVasData() != null && vasTag.getVasData().length > 64;
    }

    protected int processVas(Map<String, Object> processAttributes, NetworkLayerMessage argument) throws ServiceException {
	    //allow old format
        if (needToBeHandledAsOldVas(argument)) {
            log.warn("VAS_LOG: EnqueueOnlyProcessor_Auth processing OLD VAS");
            return handleOldVas(processAttributes, argument);
        }
        log.info("VAS_LOG: EnqueueOnlyProcessor_Auth processing NEW VAS");
        MessageData_C2 authData = (MessageData_C2) argument.getData();
        List<MessageData_C2.VASTag> vasTags = authData.getTlv();
        if (vasTags == null || vasTags.isEmpty()) {
            processAttributes.put(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue(), AuthResultCode.FAILED);
            processAttributes.put(AuthorityAttrEnum.ATTR_DENIED_REASON.getValue(), DeniedReason.UNPARSEABLE_CARD_DATA);
            processAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue(), "INVALID_CARD_DATA");
            processAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC.getValue(), "VAS Data absent");
            argument.enqueueExpectingReply(getQueueKey(), processAttributes, null, null, null);
            return 1;
        }
        Iterator<MessageData_C2.VASTag> iteratorVasTags = vasTags.iterator();
        while (iteratorVasTags.hasNext()) {
            MessageData_C2.VASTag tag = iteratorVasTags.next();
            if (!isValidVasTag(tag)) {
                //reject
                processAttributes.put(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue(), AuthResultCode.FAILED);
                processAttributes.put(AuthorityAttrEnum.ATTR_DENIED_REASON.getValue(), DeniedReason.UNPARSEABLE_CARD_DATA);
                processAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue(), "INVALID_CARD_DATA");
                processAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC.getValue(), "Card Data is not correctly formatted. Use correct TLV TAGs. Tag: " + tag.toString());
                argument.enqueueExpectingReply(getQueueKey(), processAttributes, null, null, null);
                return 1;
            }
        }
        //construct of the this chain is in NetLayerUtils.constructMessageChain, notice that RetrieveByTokenTask decryptionResultAttributes will populate processAttributes as addReferenceAttribute
        String processQueueKey = getTokenQueueKey();
        iteratorVasTags = vasTags.iterator();
        Map<String, Object> decryptionAttributes = new HashMap<String, Object>();
        decryptionAttributes.put(CommonAttrEnum.ATTR_ENCRYPTED_VAS_DATA_COUNT.getValue(), vasTags.size());
        Map<String, String> decryptionResultAttributes = new HashMap<String, String>();
        decryptionResultAttributes.put(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT.getValue(), CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT.getValue());
        int i = 0;
        while (iteratorVasTags.hasNext()) {
            MessageData_C2.VASTag tag = iteratorVasTags.next();
            String idxStr = i == 0 ? "" : ('.' + String.valueOf(i));
            decryptionResultAttributes.put(CommonAttrEnum.ATTR_PASS_IDENTIFIER.getValue() + idxStr, CommonAttrEnum.ATTR_PASS_IDENTIFIER.getValue() + idxStr);
            decryptionAttributes.put(AuthorityAttrEnum.ATTR_VAS_PASS_MERCHANT_ID.getValue() + idxStr, tag.getMerchantId());
            decryptionAttributes.put(AuthorityAttrEnum.ATTR_VAS_ENCRYPTED_DATA.getValue() + idxStr, tag.getVasData());
            decryptionResultAttributes.put(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID.getValue() + idxStr, AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID.getValue() + idxStr);
            decryptionResultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_TOKEN.getValue() + idxStr, AuthorityAttrEnum.ATTR_AUTH_TOKEN.getValue() + idxStr);
            decryptionResultAttributes.put(AuthorityAttrEnum.ATTR_ACCOUNT_DATA.getValue() + idxStr, CommonAttrEnum.ATTR_DECRYPTED_DATA.getValue() + idxStr);
            decryptionResultAttributes.put(AuthorityAttrEnum.ATTR_CARD_KEY.getValue() + idxStr, AuthorityAttrEnum.ATTR_CARD_KEY.getValue() + idxStr);
            decryptionResultAttributes.put(AuthorityAttrEnum.ATTR_STORE_EXPIRATION_TIME.getValue() + idxStr, AuthorityAttrEnum.ATTR_STORE_EXPIRATION_TIME.getValue() + idxStr);
            decryptionAttributes.put(CommonAttrEnum.ATTR_VALIDATION_REGEX.getValue() + idxStr, getValidationRegex());
            for (Attribute a : DECRYPTION_RESULT_ATTRIBUTES)
                decryptionResultAttributes.put(a.getValue() + idxStr, a.getValue() + idxStr);
            for (Attribute a : LOOKUP_RESULT_ATTRIBUTES)
                decryptionResultAttributes.put(a.getValue() + idxStr, a.getValue() + idxStr);
            i++;
        }
        
        EntryType entryType = authData.getEntryType();
        decryptionAttributes.put(AuthorityAttrEnum.ATTR_ENTRY_TYPE.getValue(), entryType);
      
        CardReader cardReader = authData.getCardReader();
        if(cardReader instanceof EncryptingCardReaderV4) {
        	CardReaderType cardReaderType = authData.getCardReaderType();        	
			EncryptingCardReaderV4 ecr = (EncryptingCardReaderV4) cardReader;
			decryptionAttributes.put(CommonAttrEnum.ATTR_CARD_READER_TYPE_ID.getValue(), cardReaderType);
			decryptionAttributes.put(CommonAttrEnum.ATTR_KEY_SERIAL_NUM.getValue(), ecr.getKeySerialNum());
			decryptionAttributes.put(CommonAttrEnum.ATTR_DECRYPTED_ENCODING.getValue(), ProcessingConstants.ISO8859_1_CHARSET.name());
			int cnt = 0;
			if(ecr.getEncryptedData1() != null && ecr.getEncryptedData1().length > 0) {
				addDecryptAttributes(decryptionAttributes, decryptionResultAttributes, ecr.getEncryptedData1(), ++cnt, getValidationRegex(), entryType);
			}
			if(ecr.getEncryptedData2() != null && ecr.getEncryptedData2().length > 0) {
				addDecryptAttributes(decryptionAttributes, decryptionResultAttributes, ecr.getEncryptedData2(), ++cnt, getValidationRegex(), entryType);
			}
			if(ecr.getEncryptedData3() != null && ecr.getEncryptedData3().length > 0) {
				addDecryptAttributes(decryptionAttributes, decryptionResultAttributes, ecr.getEncryptedData3(), ++cnt, getValidationRegex(), entryType);
			}
			decryptionAttributes.put(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT.getValue(), cnt);
        } else if(cardReader instanceof PlainCardReaderV4) {
			PlainCardReaderV4 pcr = (PlainCardReaderV4) cardReader;
			int cnt = 0;
			if(!StringUtils.isBlank(pcr.getAccountData1()))
				addLookupAttributes(processAttributes, decryptionAttributes, decryptionResultAttributes, pcr.getAccountData1(), ++cnt);
			if(!StringUtils.isBlank(pcr.getAccountData2()))
				addLookupAttributes(processAttributes, decryptionAttributes, decryptionResultAttributes, pcr.getAccountData2(), ++cnt);
			if(!StringUtils.isBlank(pcr.getAccountData3()))
				addLookupAttributes(processAttributes, decryptionAttributes, decryptionResultAttributes, pcr.getAccountData3(), ++cnt);
			decryptionAttributes.put(CommonAttrEnum.ATTR_LOOKUP_DATA_COUNT.getValue(), cnt);
		}
        
        argument.enqueueExpectingReply(getQueueKey(), processAttributes, processQueueKey, decryptionAttributes, decryptionResultAttributes);
        return 0;
    }

    /**
     * This whole method can be removed when firmware is changed to use the new AVAS|<passTypeId>|<payload> format
     * @param processAttributes
     * @param argument
     * @return
     * @throws ServiceException
     */
    protected int handleOldVasDemo(Map<String, Object> processAttributes, NetworkLayerMessage argument) throws ServiceException{
        processAttributes.put(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT.getValue(), 1);
        MessageData_C2 authData = (MessageData_C2) argument.getData();
        String[] parts = StringUtils.split(new String(authData.getValidationData()), '|', 3);
        if(parts.length < 2) {
            //reject
            processAttributes.put(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue(), AuthResultCode.FAILED);
            processAttributes.put(AuthorityAttrEnum.ATTR_DENIED_REASON.getValue(), DeniedReason.UNPARSEABLE_CARD_DATA);
            processAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue(), "INVALID_CARD_DATA");
            processAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC.getValue(), "Card Data is not correctly formatted. Use <Card Id>|<TokenHex>");
            argument.enqueueExpectingReply(getQueueKey(), processAttributes, null, null, null);
            return 1 ;
        }
        String encryptedFlag=parts[0];
        if(!StringUtils.isBlank(encryptedFlag)&&encryptedFlag.equalsIgnoreCase("U")){
            long globalAccountId;
            try {
                globalAccountId = ConvertUtils.getLong(parts[1]);
            } catch(ConvertException e) {
                processAttributes.put(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue(), AuthResultCode.FAILED);
                processAttributes.put(AuthorityAttrEnum.ATTR_DENIED_REASON.getValue(), DeniedReason.UNPARSEABLE_CARD_DATA);
                processAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue(), "INVALID_CARD_DATA");
                processAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC.getValue(), "Card Data is not correctly formatted. Card Id is not numeric. Use <Card Id>|<TokenHex>");
                argument.enqueueExpectingReply(getQueueKey(), processAttributes, null, null, null);
                return 1;
            }
            byte[] token;
            try {
                token = ByteArrayUtils.fromHex(parts[2]);
            } catch(IllegalArgumentException e) {
                processAttributes.put(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue(), AuthResultCode.FAILED);
                processAttributes.put(AuthorityAttrEnum.ATTR_DENIED_REASON.getValue(), DeniedReason.UNPARSEABLE_CARD_DATA);
                processAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue(), "INVALID_CARD_DATA");
                processAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC.getValue(), "Card Data is not correctly formatted. TokenHex contains non-hex characters. Use <Card Id>|<TokenHex>");
                argument.enqueueExpectingReply(getQueueKey(), processAttributes, null, null, null);
                return 1;
            }
            String processQueueKey = getTokenQueueKey();
            processAttributes.put(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID.getValue(), globalAccountId);
            Map<String, Object> decryptionAttributes = new HashMap<String, Object>();
            Map<String, String> decryptionResultAttributes = new HashMap<String, String>();
            decryptionAttributes.put(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID.getValue(), globalAccountId);
            decryptionAttributes.put(AuthorityAttrEnum.ATTR_AUTH_TOKEN.getValue(), token);
            decryptionAttributes.put(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT.getValue(), 1);
            decryptionResultAttributes.put(AuthorityAttrEnum.ATTR_ACCOUNT_DATA.getValue(), CommonAttrEnum.ATTR_DECRYPTED_DATA.getValue());
            decryptionResultAttributes.put(AuthorityAttrEnum.ATTR_CARD_KEY.getValue(), AuthorityAttrEnum.ATTR_CARD_KEY.getValue());
            decryptionResultAttributes.put(AuthorityAttrEnum.ATTR_STORE_EXPIRATION_TIME.getValue(), AuthorityAttrEnum.ATTR_STORE_EXPIRATION_TIME.getValue());
            for(Attribute a : DECRYPTION_RESULT_ATTRIBUTES)
                decryptionResultAttributes.put(a.getValue(), a.getValue());
            for(Attribute a : LOOKUP_RESULT_ATTRIBUTES)
                decryptionResultAttributes.put(a.getValue(), a.getValue());
            argument.enqueueExpectingReply(getQueueKey(), processAttributes, processQueueKey, decryptionAttributes, decryptionResultAttributes);
            return 0;
        }else{
            //construct of the this chain is in NetLayerUtils.constructMessageChain, notice that RetrieveByTokenTask decryptionResultAttributes will populate processAttributes as addReferenceAttribute
            String processQueueKey = getTokenQueueKey();
            Map<String, Object> decryptionAttributes = new HashMap<String, Object>();
            Map<String, String> decryptionResultAttributes = new HashMap<String, String>();
            decryptionAttributes.put(AuthorityAttrEnum.ATTR_VAS_PASS_TYPE_ID.getValue(), "pass.com.usatech.more.pass2");//demo purpose
            decryptionAttributes.put(AuthorityAttrEnum.ATTR_OLD_VAS_ENCRYPTED_DATA.getValue(), parts[1]);
            decryptionAttributes.put(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT.getValue(), 1);
            decryptionResultAttributes.put(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID.getValue(), AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID.getValue());
            decryptionResultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_TOKEN.getValue(), AuthorityAttrEnum.ATTR_AUTH_TOKEN.getValue());
            decryptionResultAttributes.put(AuthorityAttrEnum.ATTR_ACCOUNT_DATA.getValue(), CommonAttrEnum.ATTR_DECRYPTED_DATA.getValue());
            decryptionResultAttributes.put(AuthorityAttrEnum.ATTR_CARD_KEY.getValue(), AuthorityAttrEnum.ATTR_CARD_KEY.getValue());
            decryptionResultAttributes.put(AuthorityAttrEnum.ATTR_STORE_EXPIRATION_TIME.getValue(), AuthorityAttrEnum.ATTR_STORE_EXPIRATION_TIME.getValue());
            for(Attribute a : DECRYPTION_RESULT_ATTRIBUTES)
                decryptionResultAttributes.put(a.getValue(), a.getValue());
            for(Attribute a : LOOKUP_RESULT_ATTRIBUTES)
                decryptionResultAttributes.put(a.getValue(), a.getValue());
            argument.enqueueExpectingReply(getQueueKey(), processAttributes, processQueueKey, decryptionAttributes, decryptionResultAttributes);
            return 0;
        }

    }

    protected int handleOldVas(Map<String, Object> processAttributes, NetworkLayerMessage argument) throws ServiceException{
        MessageData_C2 authData = (MessageData_C2) argument.getData();
        String validationData=authData.getValidationData();
        if(StringUtils.isBlank(validationData)){
            processAttributes.put(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue(), AuthResultCode.FAILED);
            processAttributes.put(AuthorityAttrEnum.ATTR_DENIED_REASON.getValue(), DeniedReason.UNPARSEABLE_CARD_DATA);
            processAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue(), "INVALID_CARD_DATA");
            processAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC.getValue(), "Card Data is not present");
            argument.enqueueExpectingReply(getQueueKey(), processAttributes, null, null, null);
            return 1;
        }
        //<demo>
        String demoPrefix = "0|AVAS|pass.com.usatech.more.pass2|";
        // For AVAS demo send directly to AppLayer AuthorizeTask if card data length in validationData is 40 characters or less, track 2 limit
        if (validationData.startsWith(demoPrefix) && validationData.length() > demoPrefix.length() && validationData.length() <= demoPrefix.length() + 40) {
            processAttributes.put(AuthorityAttrEnum.ATTR_ACCOUNT_DATA.getValue(), validationData.substring(demoPrefix.length()));
            argument.enqueueExpectingReply(getQueueKey(), processAttributes, null, null, null);
            return 1;
        }
        //</demo>
        int avasIdx = validationData.indexOf("AVAS");
        if(avasIdx == -1 || avasIdx > 3){
            return handleOldVasDemo(processAttributes, argument);
        }
        processAttributes.put(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT.getValue(), 1);
        boolean unencrypted=validationData.matches(vasRegex);
        boolean hasC0Prefix = validationData.startsWith("C|") || validationData.startsWith("0|");
        String[] parts;
        String passTypeId=null;
        if(unencrypted){
            parts = StringUtils.split(new String(authData.getValidationData()), '|', hasC0Prefix ? 5 : 4);
        }else{
            parts = StringUtils.split(new String(authData.getValidationData()), '|', hasC0Prefix ? 4 : 3);
            if(parts.length < (hasC0Prefix ? 4 : 3) || !"AVAS".equals(parts[hasC0Prefix ? 1 : 0])) {
                //reject
                processAttributes.put(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue(), AuthResultCode.FAILED);
                processAttributes.put(AuthorityAttrEnum.ATTR_DENIED_REASON.getValue(), DeniedReason.UNPARSEABLE_CARD_DATA);
                processAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue(), "INVALID_CARD_DATA");
                processAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC.getValue(), "Card Data is not correctly formatted. Use (C|0)|AVAS|<passTypeId>|<encryptedData>");
                argument.enqueueExpectingReply(getQueueKey(), processAttributes, null, null, null);
                return 1 ;
            }
        }
        passTypeId = hasC0Prefix ? parts[2] : parts[1];
        if(unencrypted){
            long globalAccountId;
            try {
                globalAccountId = ConvertUtils.getLong(hasC0Prefix ? parts[3] : parts[2]);
            } catch(ConvertException e) {
                processAttributes.put(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue(), AuthResultCode.FAILED);
                processAttributes.put(AuthorityAttrEnum.ATTR_DENIED_REASON.getValue(), DeniedReason.UNPARSEABLE_CARD_DATA);
                processAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue(), "INVALID_CARD_DATA");
                processAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC.getValue(), "Card Data is not correctly formatted. Card Id is not numeric. Use <Card Id>|<TokenHex>");
                argument.enqueueExpectingReply(getQueueKey(), processAttributes, null, null, null);
                return 1;
            }
            byte[] token;
            try {
                token = ByteArrayUtils.fromHex(hasC0Prefix ? parts[4] : parts[3]);
            } catch(IllegalArgumentException e) {
                processAttributes.put(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue(), AuthResultCode.FAILED);
                processAttributes.put(AuthorityAttrEnum.ATTR_DENIED_REASON.getValue(), DeniedReason.UNPARSEABLE_CARD_DATA);
                processAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue(), "INVALID_CARD_DATA");
                processAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC.getValue(), "Card Data is not correctly formatted. TokenHex contains non-hex characters. Use <Card Id>|<TokenHex>");
                argument.enqueueExpectingReply(getQueueKey(), processAttributes, null, null, null);
                return 1;
            }
            String processQueueKey = getTokenQueueKey();
            processAttributes.put(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID.getValue(), globalAccountId);
            processAttributes.put(AuthorityAttrEnum.ATTR_VAS_PASS_TYPE_ID.getValue(), passTypeId);
            Map<String, Object> decryptionAttributes = new HashMap<String, Object>();
            Map<String, String> decryptionResultAttributes = new HashMap<String, String>();
            decryptionAttributes.put(AuthorityAttrEnum.ATTR_VAS_PASS_TYPE_ID.getValue(), passTypeId);
            decryptionAttributes.put(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID.getValue(), globalAccountId);
            decryptionAttributes.put(AuthorityAttrEnum.ATTR_AUTH_TOKEN.getValue(), token);
            decryptionAttributes.put(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT.getValue(), 1);
            decryptionAttributes.put(AuthorityAttrEnum.ATTR_ENTRY_TYPE.getValue(), authData.getEntryType());
            decryptionResultAttributes.put(AuthorityAttrEnum.ATTR_ACCOUNT_DATA.getValue(), CommonAttrEnum.ATTR_DECRYPTED_DATA.getValue());
            decryptionResultAttributes.put(AuthorityAttrEnum.ATTR_CARD_KEY.getValue(), AuthorityAttrEnum.ATTR_CARD_KEY.getValue());
            decryptionResultAttributes.put(AuthorityAttrEnum.ATTR_STORE_EXPIRATION_TIME.getValue(), AuthorityAttrEnum.ATTR_STORE_EXPIRATION_TIME.getValue());
            for(Attribute a : DECRYPTION_RESULT_ATTRIBUTES)
                decryptionResultAttributes.put(a.getValue(), a.getValue());
            for(Attribute a : LOOKUP_RESULT_ATTRIBUTES)
                decryptionResultAttributes.put(a.getValue(), a.getValue());
            argument.enqueueExpectingReply(getQueueKey(), processAttributes, processQueueKey, decryptionAttributes, decryptionResultAttributes);
            return 0;
        }else{
            //construct of the this chain is in NetLayerUtils.constructMessageChain, notice that RetrieveByTokenTask decryptionResultAttributes will populate processAttributes as addReferenceAttribute
            String processQueueKey = getTokenQueueKey();
            Map<String, Object> decryptionAttributes = new HashMap<String, Object>();
            Map<String, String> decryptionResultAttributes = new HashMap<String, String>();
            decryptionAttributes.put(AuthorityAttrEnum.ATTR_VAS_PASS_TYPE_ID.getValue(), passTypeId);
            decryptionAttributes.put(AuthorityAttrEnum.ATTR_OLD_VAS_ENCRYPTED_DATA.getValue(), hasC0Prefix ? parts[3] : parts[2]);
            decryptionAttributes.put(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT.getValue(), 1);
            decryptionAttributes.put(AuthorityAttrEnum.ATTR_ENTRY_TYPE.getValue(), authData.getEntryType());
            decryptionResultAttributes.put(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID.getValue(), AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID.getValue());
            decryptionResultAttributes.put(AuthorityAttrEnum.ATTR_AUTH_TOKEN.getValue(), AuthorityAttrEnum.ATTR_AUTH_TOKEN.getValue());
            decryptionResultAttributes.put(AuthorityAttrEnum.ATTR_ACCOUNT_DATA.getValue(), CommonAttrEnum.ATTR_DECRYPTED_DATA.getValue());
            decryptionResultAttributes.put(AuthorityAttrEnum.ATTR_CARD_KEY.getValue(), AuthorityAttrEnum.ATTR_CARD_KEY.getValue());
            decryptionResultAttributes.put(AuthorityAttrEnum.ATTR_STORE_EXPIRATION_TIME.getValue(), AuthorityAttrEnum.ATTR_STORE_EXPIRATION_TIME.getValue());
            for(Attribute a : DECRYPTION_RESULT_ATTRIBUTES)
                decryptionResultAttributes.put(a.getValue(), a.getValue());
            for(Attribute a : LOOKUP_RESULT_ATTRIBUTES)
                decryptionResultAttributes.put(a.getValue(), a.getValue());
            argument.enqueueExpectingReply(getQueueKey(), processAttributes, processQueueKey, decryptionAttributes, decryptionResultAttributes);
            return 0;
        }

    }

    private boolean needToBeHandledAsOldVas(NetworkLayerMessage argument) {
        MessageData_C2 authData = (MessageData_C2) argument.getData();
        List<MessageData_C2.VASTag> vasTags = authData.getTlv();
        return vasTags == null || vasTags.isEmpty();
    }

	protected void processCardReader(NetworkLayerMessage argument, CardReader cardReader, CardReaderType cardReaderType, EntryType entryType, PaymentActionType paymentActionType, Map<String, Object> processAttributes, byte[] appSessionKey) throws ServiceException {
		if(cardReader instanceof EncryptingCardReaderV4) {
			if(entryType == EntryType.VAS){
				int returnResult = processVas(processAttributes, argument);
				if(returnResult>0){
					return;
				}
			}else{
				EncryptingCardReaderV4 ecr = (EncryptingCardReaderV4) cardReader;
				Map<String, Object> decryptionAttributes = new HashMap<String, Object>();
				Map<String,String> decryptionResultAttributes = new HashMap<String,String>();
				decryptionAttributes.put(CommonAttrEnum.ATTR_CARD_READER_TYPE_ID.getValue(), cardReaderType);
				decryptionAttributes.put(CommonAttrEnum.ATTR_KEY_SERIAL_NUM.getValue(), ecr.getKeySerialNum());
				decryptionAttributes.put(CommonAttrEnum.ATTR_DECRYPTED_ENCODING.getValue(), ProcessingConstants.ISO8859_1_CHARSET.name());
				decryptionAttributes.put(AuthorityAttrEnum.ATTR_ENTRY_TYPE.getValue(), entryType);
				int cnt = 0;
				if(ecr.getEncryptedData1() != null && ecr.getEncryptedData1().length > 0) {
					addDecryptAttributes(decryptionAttributes, decryptionResultAttributes, ecr.getEncryptedData1(), ++cnt, getValidationRegex(), entryType);
				}
				if(ecr.getEncryptedData2() != null && ecr.getEncryptedData2().length > 0) {
					addDecryptAttributes(decryptionAttributes, decryptionResultAttributes, ecr.getEncryptedData2(), ++cnt, getValidationRegex(), entryType);
				}
				if(ecr.getEncryptedData3() != null && ecr.getEncryptedData3().length > 0) {
					addDecryptAttributes(decryptionAttributes, decryptionResultAttributes, ecr.getEncryptedData3(), ++cnt, getValidationRegex(), entryType);
				}
				if(cnt == 0) {
					sendFailureResponse(argument, AuthResultCode.DECLINED_PERMANENT, "No account data provided", appSessionKey);
					return;
				}
				processAttributes.put(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT.getValue(), cnt);
				decryptionAttributes.put(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT.getValue(), cnt);
				argument.enqueueExpectingReply(getQueueKey(), processAttributes, getDecryptionQueueKey(), decryptionAttributes, decryptionResultAttributes);
			}
		} else if(cardReader instanceof PlainCardReaderV4) {
			PlainCardReaderV4 pcr = (PlainCardReaderV4) cardReader;
			if(entryType == EntryType.VAS){
				int returnResult = processVas(processAttributes, argument);
				if(returnResult>0){
					return;
				}
			}else{
				Map<String, Object> decryptionAttributes;
				Map<String, String> decryptionResultAttributes;
				int cnt = 0;
				String processQueueKey;
				if(argument.getPublisher().getConsumerCount(getLookupQueueKey(), true) > 0) {
					processQueueKey = getLookupQueueKey();
					decryptionAttributes = new HashMap<String, Object>();
					decryptionResultAttributes = new HashMap<String, String>();
					if(!StringUtils.isBlank(pcr.getAccountData1()))
						addLookupAttributes(processAttributes, decryptionAttributes, decryptionResultAttributes, pcr.getAccountData1(), ++cnt);
					if(!StringUtils.isBlank(pcr.getAccountData2()))
						addLookupAttributes(processAttributes, decryptionAttributes, decryptionResultAttributes, pcr.getAccountData2(), ++cnt);
					if(!StringUtils.isBlank(pcr.getAccountData3()))
						addLookupAttributes(processAttributes, decryptionAttributes, decryptionResultAttributes, pcr.getAccountData3(), ++cnt);
					decryptionAttributes.put(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT.getValue(), cnt);
				} else { // keymgrs not available, resort to sending directly to authorize task
					processQueueKey = null;
					decryptionAttributes = null;
					decryptionResultAttributes = null;
					if(!StringUtils.isBlank(pcr.getAccountData1()))
						addSkipKeyMgrAttributes(processAttributes, pcr.getAccountData1(), ++cnt);
					if(!StringUtils.isBlank(pcr.getAccountData2()))
						addSkipKeyMgrAttributes(processAttributes, pcr.getAccountData2(), ++cnt);
					if(!StringUtils.isBlank(pcr.getAccountData3()))
						addSkipKeyMgrAttributes(processAttributes, pcr.getAccountData3(), ++cnt);
				}
				if(cnt == 0) {
					sendFailureResponse(argument, AuthResultCode.DECLINED_PERMANENT, "No account data provided", appSessionKey);
					return;
				}
				processAttributes.put(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT.getValue(), cnt);
				argument.enqueueExpectingReply(getQueueKey(), processAttributes, processQueueKey, decryptionAttributes, decryptionResultAttributes);
			}
		} else if(cardReader instanceof EncryptingCardReader) {
			if(entryType == EntryType.VAS){
				int returnResult = processVas(processAttributes, argument);
				if(returnResult>0){
					return;
				}
			}else{
				EncryptingCardReader ecr = (EncryptingCardReader) cardReader;
				if(ecr.getEncryptedData() == null || ecr.getEncryptedData().length == 0) {
					sendFailureResponse(argument, AuthResultCode.DECLINED_PERMANENT, "No account data provided", appSessionKey);
					return;
				}
				Map<String, Object> decryptionAttributes = new HashMap<String, Object>();
				Map<String, String> decryptionResultAttributes = new HashMap<String, String>();
				decryptionAttributes.put(CommonAttrEnum.ATTR_CARD_READER_TYPE_ID.getValue(), cardReaderType);
				decryptionAttributes.put(CommonAttrEnum.ATTR_KEY_SERIAL_NUM.getValue(), ecr.getKeySerialNum());
				decryptionAttributes.put(CommonAttrEnum.ATTR_DECRYPTED_ENCODING.getValue(), ProcessingConstants.ISO8859_1_CHARSET.name());
				decryptionAttributes.put(AuthorityAttrEnum.ATTR_ENTRY_TYPE.getValue(), entryType);
				addDecryptAttributes(decryptionAttributes, decryptionResultAttributes, ecr.getEncryptedData(), 0, getValidationRegex(), entryType);
				processAttributes.put(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT.getValue(), 0);
				argument.enqueueExpectingReply(getQueueKey(), processAttributes, getDecryptionQueueKey(), decryptionAttributes, decryptionResultAttributes);
			}
		} else if(cardReader instanceof RawCardReader) {
			RawCardReader rcr = (RawCardReader) cardReader;
			if(rcr.getRawData() == null || rcr.getRawData().length == 0) {
				sendFailureResponse(argument, AuthResultCode.DECLINED_PERMANENT, "No account data provided", appSessionKey);
				return;
			}
			Map<String, Object> decryptionAttributes = new HashMap<String, Object>();
			Map<String, String> decryptionResultAttributes = new HashMap<String, String>();
			if(entryType == EntryType.VAS){
				int returnResult = processVas(processAttributes, argument);
				if(returnResult>0){
					return;
				}
			}else{
				decryptionAttributes.put(CommonAttrEnum.ATTR_CARD_READER_TYPE_ID.getValue(), cardReaderType);
				decryptionAttributes.put(CommonAttrEnum.ATTR_DECRYPTED_ENCODING.getValue(), ProcessingConstants.ISO8859_1_CHARSET.name());
				decryptionAttributes.put(AuthorityAttrEnum.ATTR_ENTRY_TYPE.getValue(), entryType);
				addDecryptAttributes(decryptionAttributes, decryptionResultAttributes, rcr.getRawData(), 0, getValidationRegex(), entryType);
				processAttributes.put(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT.getValue(), 0);
				if (rcr instanceof MessageData_C2.EnhancedRawCardReader) {
					MessageData_C2.EnhancedRawCardReader ercr = (MessageData_C2.EnhancedRawCardReader) rcr; 
					processAttributes.put(CommonAttrEnum.ATTR_LANGUAGE.getValue(), ercr.getLanguage());
				}
				argument.enqueueExpectingReply(getQueueKey(), processAttributes, getDecryptionQueueKey(), decryptionAttributes, decryptionResultAttributes);
			}
		} else if(cardReader instanceof PlainCardReader) {
			PlainCardReader pcr = (PlainCardReader) cardReader;
			if(pcr.getAccountData() == null || pcr.getAccountData().length() == 0) {
				sendFailureResponse(argument, AuthResultCode.DECLINED_PERMANENT, "No account data provided", appSessionKey);
				return;
			}
			if(entryType == EntryType.VAS){
				int returnResult = processVas(processAttributes, argument);
				if(returnResult>0){
					return;
				}
			}else{
			Map<String, Object> decryptionAttributes;
			Map<String, String> decryptionResultAttributes;
			String processQueueKey;
			if(paymentActionType != PaymentActionType.TOKENIZE && entryType == EntryType.TOKEN) {
				processAttributes.put(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT.getValue(), 0);
				String[] parts = StringUtils.split(pcr.getAccountData(), '|', 2);
				if(parts.length < 2) {
					//reject
					processAttributes.put(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue(), AuthResultCode.FAILED);
					processAttributes.put(AuthorityAttrEnum.ATTR_DENIED_REASON.getValue(), DeniedReason.UNPARSEABLE_CARD_DATA);
					processAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue(), "INVALID_CARD_DATA");
					processAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC.getValue(), "Card Data is not correctly formatted. Use <Card Id>|<TokenHex>");
					argument.enqueueExpectingReply(getQueueKey(), processAttributes, null, null, null);
					return;
				}
				long globalAccountId;
				try {
					globalAccountId = ConvertUtils.getLong(parts[0]);
				} catch(ConvertException e) {
					processAttributes.put(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue(), AuthResultCode.FAILED);
					processAttributes.put(AuthorityAttrEnum.ATTR_DENIED_REASON.getValue(), DeniedReason.UNPARSEABLE_CARD_DATA);
					processAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue(), "INVALID_CARD_DATA");
					processAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC.getValue(), "Card Data is not correctly formatted. Card Id is not numeric. Use <Card Id>|<TokenHex>");
					argument.enqueueExpectingReply(getQueueKey(), processAttributes, null, null, null);
					return;
				}
				byte[] token;
				try {
					token = ByteArrayUtils.fromHex(parts[1]);
				} catch(IllegalArgumentException e) {
					processAttributes.put(AuthorityAttrEnum.ATTR_AUTH_RESULT_CD.getValue(), AuthResultCode.FAILED);
					processAttributes.put(AuthorityAttrEnum.ATTR_DENIED_REASON.getValue(), DeniedReason.UNPARSEABLE_CARD_DATA);
					processAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD.getValue(), "INVALID_CARD_DATA");
					processAttributes.put(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_DESC.getValue(), "Card Data is not correctly formatted. TokenHex contains non-hex characters. Use <Card Id>|<TokenHex>");
					argument.enqueueExpectingReply(getQueueKey(), processAttributes, null, null, null);
					return;
				}
				processQueueKey = getTokenQueueKey();
				processAttributes.put(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID.getValue(), globalAccountId);
				decryptionAttributes = new HashMap<String, Object>();
				decryptionResultAttributes = new HashMap<String, String>();
				decryptionAttributes.put(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID.getValue(), globalAccountId);
				decryptionAttributes.put(AuthorityAttrEnum.ATTR_AUTH_TOKEN.getValue(), token);
				decryptionResultAttributes.put(AuthorityAttrEnum.ATTR_ACCOUNT_DATA.getValue(), CommonAttrEnum.ATTR_DECRYPTED_DATA.getValue());
				decryptionResultAttributes.put(AuthorityAttrEnum.ATTR_CARD_KEY.getValue(), AuthorityAttrEnum.ATTR_CARD_KEY.getValue());
				decryptionResultAttributes.put(AuthorityAttrEnum.ATTR_STORE_EXPIRATION_TIME.getValue(), AuthorityAttrEnum.ATTR_STORE_EXPIRATION_TIME.getValue());
				for(Attribute a : DECRYPTION_RESULT_ATTRIBUTES)
					decryptionResultAttributes.put(a.getValue(), a.getValue());
				for(Attribute a : LOOKUP_RESULT_ATTRIBUTES)
					decryptionResultAttributes.put(a.getValue(), a.getValue());
			} else if(argument.getPublisher().getConsumerCount(getLookupQueueKey(), true) > 0) {
				processQueueKey = getLookupQueueKey();
				decryptionAttributes = new HashMap<String, Object>();
				decryptionResultAttributes = new HashMap<String, String>();
				addLookupAttributes(processAttributes, decryptionAttributes, decryptionResultAttributes, pcr.getAccountData(), 0);
			} else { // keymgrs not available, resort to sending directly to authorize task
				processQueueKey = null;
				decryptionAttributes = null;
				decryptionResultAttributes = null;
				addSkipKeyMgrAttributes(processAttributes, pcr.getAccountData(), 0);
			}
			argument.enqueueExpectingReply(getQueueKey(), processAttributes, processQueueKey, decryptionAttributes, decryptionResultAttributes);
			}
		} else if(cardReader instanceof EBeaconAppCardReader) {
			EBeaconAppCardReader acr = (EBeaconAppCardReader) cardReader;
			AppAuthData aam;
			try {
				aam = acr.getAppAuthMessage(getAppKeyAliasList());
			} catch(BufferUnderflowException e) {
				argument.getLog().warn("Could not read app auth message", e);
				sendFailureResponse(argument, AuthResultCode.DECLINED, "Invalid App Data. Please check App Version", acr.getSessionKey());
				return;
			} catch(ParseException e) {
				argument.getLog().warn("Could not read app auth message", e);
				sendFailureResponse(argument, AuthResultCode.DECLINED, "Invalid App Data. Please check App Version", acr.getSessionKey());
				return;
			}
			if(acr.getAppMessageId() != aam.getAppMessageId()) {
				argument.getLog().warn("Invalid app message id. Expected " + acr.getAppMessageId() + " and got " + aam.getAppMessageId());
				sendFailureResponse(argument, AuthResultCode.DECLINED, "App Data stale. Please try again", acr.getSessionKey());
				return;
			}
			// Override entry type
			if(entryType != aam.getEntryType())
				processAttributes.put(AuthorityAttrEnum.ATTR_ENTRY_TYPE.getValue(), aam.getEntryType());
			if(acr.getSessionKey() != null)
				processAttributes.put(CommonAttrEnum.ATTR_APP_SESSION_KEY.getValue(), acr.getSessionKey());
			processCardReader(argument, aam.getCardReader(), aam.getCardReaderType(), aam.getEntryType(), paymentActionType, processAttributes, acr.getSessionKey());
		} else if(entryType == EntryType.CASH) {
			processAttributes.put(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT.getValue(), 0);
			argument.enqueueExpectingReply(getQueueKey(), processAttributes, null, null, null);
		} else { // this should not happen
			argument.enqueueExpectingReply(getQueueKey(), processAttributes, null, null, null);
		}
	}

	protected void sendFailureResponse(NetworkLayerMessage message, AuthResultCode authResultCd, String clientText, byte[] appSessionKey) throws ServiceException {
		MessageData reply = MessageProcessingUtils.prepareAuthResponse(message.getDeviceInfo(), (AuthorizeMessageData) message.getData(), authResultCd, null, null, null, false, clientText, false, false, appSessionKey);
		message.sendReply(reply);
	}

	protected void addDecryptAttributes(Map<String, Object> decryptionAttributes, Map<String, String> decryptionResultAttributes, byte[] encryptedData, int index, String validationRegex, EntryType entryType) {
		decryptionAttributes.put(getAttributeName(CommonAttrEnum.ATTR_ENCRYPTED_DATA, index), encryptedData);
		decryptionAttributes.put(getAttributeName(CommonAttrEnum.ATTR_VALIDATION_REGEX, index), validationRegex);
		decryptionResultAttributes.put(getAttributeName(AuthorityAttrEnum.ATTR_ACCOUNT_DATA, index), getAttributeName(CommonAttrEnum.ATTR_DECRYPTED_DATA, index));
		decryptionResultAttributes.put(getAttributeName(AuthorityAttrEnum.ATTR_ADDITIONAL_INFO, index), getAttributeName(AuthorityAttrEnum.ATTR_ADDITIONAL_INFO, index));

		for(Attribute a : DECRYPTION_RESULT_ATTRIBUTES)
			decryptionResultAttributes.put(a.getValue(), a.getValue());
		for(Attribute a : LOOKUP_RESULT_ATTRIBUTES) {
			String name = getAttributeName(a, index);
			decryptionResultAttributes.put(name, name);
		}

		if (entryType == EntryType.DYNAMIC) {
			decryptionResultAttributes.put(getAttributeName(AuthorityAttrEnum.ATTR_ENTRY_TYPE, index), getAttributeName(AuthorityAttrEnum.ATTR_ENTRY_TYPE, index));
			decryptionResultAttributes.put(getAttributeName(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, index), getAttributeName(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, index));
			decryptionResultAttributes.put(getAttributeName(CommonAttrEnum.ATTR_PASS_IDENTIFIER, index), getAttributeName(CommonAttrEnum.ATTR_PASS_IDENTIFIER, index));
		}
	}

	protected void addLookupAttributes(Map<String, Object> processAttributes, Map<String, Object> decryptionAttributes, Map<String, String> decryptionResultAttributes, String acctData, int index) {
		String name = getAttributeName(AuthorityAttrEnum.ATTR_ACCOUNT_DATA, index);
		Object value = new MaskedString(MessageResponseUtils.cleanTrack(acctData));
		processAttributes.put(name, value);
		decryptionAttributes.put(name, value);
		for(Attribute a : LOOKUP_RESULT_ATTRIBUTES) {
			name = getAttributeName(a, index);
			decryptionResultAttributes.put(name, name);
		}
	}

	protected void addSkipKeyMgrAttributes(Map<String, Object> processAttributes, String acctData, int index) {
		processAttributes.put(getAttributeName(AuthorityAttrEnum.ATTR_ACCOUNT_DATA, index), new MaskedString(MessageResponseUtils.cleanTrack(acctData)));
		processAttributes.put(getAttributeName(AuthorityAttrEnum.ATTR_DENIED_REASON, index), DeniedReason.KEYMGRS_NOT_LOADED);
		processAttributes.put(getAttributeName(AuthorityAttrEnum.ATTR_AUTHORITY_RESPONSE_CD, index), DeniedReason.KEYMGRS_NOT_LOADED.toString());
	}
	protected String getAttributeName(Attribute attribute, int index) {
		if(index < 1)
			return attribute.getValue();
		return attribute.getValue() + '.' + index;
	}

	public String getDecryptionQueueKey() {
		return decryptionQueueKey;
	}
	public void setDecryptionQueueKey(String decryptionQueueKey) {
		this.decryptionQueueKey = decryptionQueueKey;
	}

	public String getValidationRegex() {
		return validationRegex;
	}

	public void setValidationRegex(String validationRegex) {
		this.validationRegex = validationRegex;
	}

	public String getLookupQueueKey() {
		return lookupQueueKey;
	}

	public void setLookupQueueKey(String lookupQueueKey) {
		this.lookupQueueKey = lookupQueueKey;
	}

	public String getTokenQueueKey() {
		return tokenQueueKey;
	}

	public void setTokenQueueKey(String tokenQueueKey) {
		this.tokenQueueKey = tokenQueueKey;
	}

	public String[] getAppKeyAliases() {
		return appKeyAliases;
	}

	public void setAppKeyAliases(String[] appKeyAliases) {
		this.appKeyAliases = appKeyAliases;
		this.appKeyAliasList = null;
	}

	public List<String> getAppKeyAliasList() {
		if(appKeyAliasList == null)
			appKeyAliasList = Arrays.asList(appKeyAliases);
		return appKeyAliasList;
	}
}
