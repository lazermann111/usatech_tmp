package com.usatech.networklayer.processors;

import simple.app.Processor;
import simple.app.ServiceException;

import com.usatech.layers.common.constants.SessionCloseReason;
import com.usatech.networklayer.app.NetworkLayerMessage;

public class CloseOnlyProcessor implements Processor<NetworkLayerMessage, Void> {
	protected SessionCloseReason sessionCloseReason;
	
	@Override
	public Void process(NetworkLayerMessage argument) throws ServiceException {
		argument.closeSession(getSessionCloseReason());
		return null;
	}

	public Class<NetworkLayerMessage> getArgumentType() {
		return NetworkLayerMessage.class;
	}

	public Class<Void> getReturnType() {
		return null;
	}

	public SessionCloseReason getSessionCloseReason() {
		return sessionCloseReason;
	}

	public void setSessionCloseReason(SessionCloseReason sessionCloseReason) {
		this.sessionCloseReason = sessionCloseReason;
	}
}
