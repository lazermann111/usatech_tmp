package com.usatech.networklayer.processors;

import simple.app.ServiceException;

import com.usatech.layers.common.constants.CardType;
import com.usatech.layers.common.constants.SaleResult71;
import com.usatech.layers.common.messagedata.LegacyLocalAuth;
import com.usatech.layers.common.messagedata.LegacySale;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageData_71;
import com.usatech.networklayer.app.NetworkLayerMessage;

public class EnqueueAndReplyProcessor_71 extends EnqueueAndReplyProcessor {
	@Override
	protected MessageData buildReply(NetworkLayerMessage networkLayerMessage) throws ServiceException {
		long transactionId=((LegacySale)networkLayerMessage.getData()).getTransactionId();
		MessageData_71 reply = new MessageData_71();
		reply.setTransactionId(transactionId);
		reply.setSaleResult(SaleResult71.PASS);
		if(networkLayerMessage.getData() instanceof LegacyLocalAuth) {
			CardType cardType = ((LegacyLocalAuth) networkLayerMessage.getData()).getCardType();
			switch(cardType) {
				case ERROR: case INVENTORY_CARD:
					networkLayerMessage.incrementEventCount();
					break;
				default:
					networkLayerMessage.incrementSaleCount();	
			}		
		} else {
			networkLayerMessage.incrementSaleCount();
		}
		return reply;
	}
}
