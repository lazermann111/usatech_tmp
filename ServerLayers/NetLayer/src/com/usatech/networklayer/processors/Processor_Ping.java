package com.usatech.networklayer.processors;

import simple.app.Processor;
import simple.app.ServiceException;
import simple.text.StringUtils;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService.MessageChainAttribute;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.constants.SessionCloseReason;
import com.usatech.layers.common.messagedata.MessageData_0007;
import com.usatech.layers.common.messagedata.MessageData_0107;
import com.usatech.layers.common.messagedata.MessageData_Ping;
import com.usatech.networklayer.app.NetworkLayerMessage;

public class Processor_Ping implements Processor<NetworkLayerMessage, Void> {
	protected boolean closeLoadBalancerSession = true;
	protected long loadBalancerTimeout = 10000;
	protected String[] loadBalancerQueueNames;
	
	public Void process(NetworkLayerMessage argument) throws ServiceException {
		MessageType messageType = argument.getData().getMessageType();
		
		switch(messageType) {
			case NETLAYER_HEALTH_CHECK:
				MessageChain mc = new MessageChainV11();
				addPingReplyStep(mc, argument);
				argument.enqueue(mc, true);
				break;
			case PING:
				if(!argument.getDeviceName().equals(ProcessingConstants.LOAD_BALANCER_MACHINE_ID)) {
					argument.sendReply(argument.getData());
				} else {
					mc = new MessageChainV11();
					addQueueCheckChain(mc, getLoadBalancerTimeout(), getLoadBalancerQueueNames(), argument, false);
					argument.enqueue(mc, true);
				}
				break;
			case QUEUE_TIMING_CHECK:
				MessageData_0007 data0007 = (MessageData_0007)argument.getData();
				mc = new MessageChainV11();
				String[] queueNames = new String[data0007.getQueues().size()];
				int i = 0;
				for(MessageData_0007.QueueData qd : data0007.getQueues()) {
					queueNames[i++] = qd.getQueueName();
				}
				addQueueCheckChain(mc, data0007.getTimeout(), queueNames, argument, true);
				argument.enqueue(mc, true);
				break;	
			default:
				throw new ServiceException("Invalid MessageType '" + messageType + "' for this processor");
		}
		return null;
	}

	protected void addQueueCheckChain(MessageChain mc, long timeout, String[] queueNames, NetworkLayerMessage message, boolean fullReply) throws ServiceException {
		long start = System.currentTimeMillis();
		long expiration = timeout > 0 ? start + timeout : 0;
		MessageChainStep[] checkSteps;
		MessageChainStep lastStep = null;
		if(queueNames != null && queueNames.length > 0) {
			checkSteps = new MessageChainStep[queueNames.length];
			for(int i = 0; i < queueNames.length; i++) {
				lastStep = checkSteps[i] = mc.addStep(StringUtils.isBlank(queueNames[i]) ? message.getReplyQueueKey() : queueNames[i]);
				checkSteps[i].setAttribute(MessageChainAttribute.PING_ONLY, true);
				checkSteps[i].setAttribute(MessageChainAttribute.PING_EXPIRATION, expiration);
				if(i > 0)
					checkSteps[i - 1].setNextSteps(0, checkSteps[i]);
			}
		} else
			checkSteps = null;
		MessageChainStep replyStep;
		if(fullReply) {
			MessageData_0107 reply0107 = new MessageData_0107();
			reply0107.setMessageNumber(message.getData().getMessageNumber());
			if(queueNames != null && queueNames.length > 0) {
				for(int i = 0; i < queueNames.length; i++) {
					reply0107.addQueue();
				}
			}
			replyStep = message.constructReplyTemplateStep(mc, reply0107, isCloseLoadBalancerSession() ? SessionCloseReason.MESSAGE_BASED : null, null);
			replyStep.addLiteralAttribute("replyTemplate.startTime", start);
			replyStep.addLiteralAttribute("replyTemplate.endDate", "{*0}");
			if(queueNames != null && queueNames.length > 0) {
				for(int i = 0; i < queueNames.length; i++) {
					replyStep.addReferenceAttribute("replyTemplate.queues[" + i + "].endTime", checkSteps[i], MessageChainAttribute.END_TIME.getValue());
				}
			}
		} else {
			MessageData_Ping replyPing = new MessageData_Ping();
			replyPing.setMessageNumber(message.getData().getMessageNumber());
			replyStep = message.constructReplyStep(mc, replyPing, isCloseLoadBalancerSession() ? SessionCloseReason.MESSAGE_BASED : null, null);
		}
		if(lastStep != null) {
			lastStep.setNextSteps(0, replyStep);
		}
	}

	protected MessageChainStep addPingReplyStep(MessageChain mc, NetworkLayerMessage message) {
		MessageChainStep replyStep = mc.addStep(message.getReplyQueueKey());
		replyStep.setAttribute(CommonAttrEnum.ATTR_SESSION_ID, message.getSessionId());
		replyStep.setAttribute(CommonAttrEnum.ATTR_REPLY, new byte[] {message.getData().getMessageNumber()});
		replyStep.setAttribute(CommonAttrEnum.ATTR_END_SESSION, isCloseLoadBalancerSession() ? SessionCloseReason.MESSAGE_BASED : null);
		return replyStep;
	}
	
	public Class<NetworkLayerMessage> getArgumentType() {
		return NetworkLayerMessage.class;
	}

	public Class<Void> getReturnType() {
		return null;
	}
	public boolean isCloseLoadBalancerSession() {
		return closeLoadBalancerSession;
	}
	public void setCloseLoadBalancerSession(boolean closeLoadBalancerSession) {
		this.closeLoadBalancerSession = closeLoadBalancerSession;
	}

	public long getLoadBalancerTimeout() {
		return loadBalancerTimeout;
	}

	public void setLoadBalancerTimeout(long loadBalancerTimeout) {
		this.loadBalancerTimeout = loadBalancerTimeout;
	}

	public String[] getLoadBalancerQueueNames() {
		return loadBalancerQueueNames;
	}

	public void setLoadBalancerQueueNames(String[] loadBalancerQueueNames) {
		this.loadBalancerQueueNames = loadBalancerQueueNames;
	}
}
