package com.usatech.networklayer.processors;

import simple.app.ServiceException;

import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageData_2F;
import com.usatech.networklayer.app.NetworkLayerMessage;

public class ReplyOnlyProcessor_2F extends ReplyOnlyProcessor {
	@Override
	protected MessageData buildReply(NetworkLayerMessage argument) throws ServiceException {
		MessageData_2F reply = new MessageData_2F();
		reply.setAckedMessageNumber(argument.getData().getMessageNumber());
		return reply;
	}
}
