package com.usatech.networklayer.processors;

import simple.app.ServiceException;

import com.usatech.layers.common.DeviceInfo;
import com.usatech.layers.common.constants.GenericResponseResultCode;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageData_C4;
import com.usatech.layers.common.messagedata.MessageData_C5;
import com.usatech.layers.common.messagedata.MessageData_C6;
import com.usatech.layers.common.messagedata.MessageData_CB;
import com.usatech.networklayer.app.NetworkLayerMessage;

public class EnqueueAndReplyProcessor_CB extends EnqueueAndReplyProcessor {
	@Override
	protected MessageData buildReply(NetworkLayerMessage argument) throws ServiceException {
		MessageData_CB replyData = new MessageData_CB();
		DeviceInfo deviceInfo = argument.getDeviceInfo();
		replyData.setResultCode(GenericResponseResultCode.OKAY);
		replyData.setResponseMessage(argument.getTranslator().translate("client.message.general-okay", "Message Received"));
		replyData.setServerActionCode(deviceInfo.getActionCode().getGenericResponseServerActionCode());
		switch(argument.getData().getMessageType()) {
			case SALE_4_1:
				argument.incrementSaleCount();
				deviceInfo.updateMasterId(((MessageData_C4) argument.getData()).getTransactionId());
				break;
			case SETTLEMENT_4_1:
				argument.incrementEventCount();
				deviceInfo.updateMasterId(((MessageData_C5) argument.getData()).getEventId());
				break;
			case GENERIC_EVENT_4_1:
				argument.incrementEventCount();
				deviceInfo.updateMasterId(((MessageData_C6) argument.getData()).getEventId());
				break;
		}
		return replyData;
	}
}
