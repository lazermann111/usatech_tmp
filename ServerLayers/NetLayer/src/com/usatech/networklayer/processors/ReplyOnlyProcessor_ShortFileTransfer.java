package com.usatech.networklayer.processors;

import java.nio.BufferUnderflowException;

import simple.app.Processor;
import simple.app.ServiceException;
import simple.io.ByteArrayBinaryStream;
import simple.io.Log;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.DeviceInfoProperty;
import com.usatech.layers.common.constants.GenericResponseResultCode;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageData_C7;
import com.usatech.layers.common.messagedata.MessageData_CB;
import com.usatech.networklayer.app.InboundFileTransfer;
import com.usatech.networklayer.app.NetworkLayerMessage;

public class ReplyOnlyProcessor_ShortFileTransfer implements Processor<NetworkLayerMessage, Void> {
	private static final Log log = Log.getLog();
	protected String queueKey;
	protected String ackQueueKey;

	public Class<NetworkLayerMessage> getArgumentType() {
		return NetworkLayerMessage.class;
	}
	public Class<Void> getReturnType() {
		return null;
	}
	public Void process(NetworkLayerMessage argument) throws ServiceException  {
		MessageData data = argument.getData();
		try {
			switch(data.getMessageType()) {
				case SHORT_FILE_XFER_4_1: //(byte)0xC7:
					MessageData_C7 dataC7 = (MessageData_C7)data;
					argument.receivedFileTransfer(dataC7.getFileType(), new ByteArrayBinaryStream(dataC7.getContent()));
					
					MessageChain mc = new MessageChainV11();
					MessageData_CB replyCB = new MessageData_CB();
					replyCB.setResultCode(GenericResponseResultCode.OKAY);
					replyCB.setResponseMessage(argument.getTranslator().translate("client.message.file-transfer-complete", "File transfer complete"));
					replyCB.setServerActionCode(argument.getDeviceInfo().getActionCode().getGenericResponseServerActionCode());
					if(argument.getLastCommandId() != null) {
						MessageChainStep checkStep = mc.addStep(getAckQueueKey());
						checkStep.addLongAttribute("pendingCommandId", argument.getLastCommandId());
						checkStep.addStringAttribute("deviceName", argument.getDeviceName());
						checkStep.addStringAttribute("globalSessionCode", argument.getGlobalSessionCode());
						MessageChainStep ackSuccessStep = argument.constructReplyTemplateStep(mc, replyCB, null, null);
						ackSuccessStep.addReferenceAttribute("replyTemplate.serverActionCode", checkStep, "responseActionCode");
						checkStep.setNextSteps(0, ackSuccessStep);
						replyCB = new MessageData_CB();
						replyCB.setResultCode(GenericResponseResultCode.APPLAYER_ERROR);
						replyCB.setResponseMessage(argument.getTranslator().translate("client.message.filetransfer.notack", "Short File Transfer could not be processed"));
						replyCB.setServerActionCode(argument.getDeviceInfo().getActionCode().getGenericResponseServerActionCode());
						MessageChainStep ackFailStep = argument.constructReplyStep(mc, replyCB, null, null);
						MessageChainStep fileTransferStep = constructFileTransferStep(mc, dataC7, argument);
						checkStep.setNextSteps(1, ackFailStep);
						ackSuccessStep.setNextSteps(0, fileTransferStep);
						ackFailStep.setNextSteps(0, fileTransferStep);
						argument.enqueue(mc, true);
					} else {
						constructFileTransferStep(mc, dataC7, argument);
						argument.enqueue(mc, false);
						argument.sendReply(replyCB);
					}

					return null; // don't register file transfer
				default:
					throw new ServiceException("Invalid configuration; This class cannot handle message '" + data.getMessageType() + "'");
			}
		} catch(BufferUnderflowException e) {
			log.warn("Invalid Command - not enough data from device '" + argument.getDeviceName() + "'", e);
			argument.sendReply(constructNakReply(argument, data, GenericResponseResultCode.APPLAYER_ERROR, "client.message.data-truncated", "Invalid Message"));
		}
		return null;
	}

	protected MessageChainStep constructFileTransferStep(MessageChain mc, MessageData_C7 dataC7, NetworkLayerMessage message) throws ServiceException {
		MessageChainStep step = mc.addStep(getQueueKey());
		step.addStringAttribute("deviceName", message.getDeviceName());
		step.setAttribute(DeviceInfoProperty.DEVICE_TYPE, message.getDeviceInfo().getDeviceType());
		step.setAttribute(DeviceInfoProperty.TIME_ZONE_GUID, message.getDeviceInfo().getTimeZoneGuid());
		step.setAttribute(DeviceInfoProperty.DEVICE_CHARSET, message.getDeviceInfo().getDeviceCharset());
		step.addByteArrayAttribute("resourceContent", dataC7.getContent());
		String fileName = dataC7.getFileName();
		if(fileName == null || (fileName=fileName.trim()).length() == 0)
			fileName = InboundFileTransfer.generateFileName(message.getDeviceName(), dataC7.getFileType().getValue());		
		step.addStringAttribute("fileName", fileName);
		step.addIntAttribute("fileType", dataC7.getFileType().getValue());
		step.addStringAttribute("globalSessionCode", message.getGlobalSessionCode());
		step.addLongAttribute("startTime", message.getServerTime());
		step.addLongAttribute("endTime", message.getServerTime());
		step.addByteArrayAttribute("createTime", ProcessingUtils.timestampToBytes(dataC7.getCreationTime()));
		if(dataC7.getEventId() != 0)
			step.addLongAttribute("eventId", dataC7.getEventId());
		return step;
	}
	protected MessageData constructNakReply(NetworkLayerMessage argument, MessageData data, GenericResponseResultCode resultCode, String errorCode, String errorMessage) throws ServiceException{
		switch(data.getMessageType()) {
			case SHORT_FILE_XFER_4_1:
				MessageData_CB replyCB = new MessageData_CB();
				replyCB.setResultCode(resultCode);
				replyCB.setResponseMessage(argument.getTranslator().translate(errorCode, errorMessage));
				replyCB.setServerActionCode(argument.getDeviceInfo().getActionCode().getGenericResponseServerActionCode());
				return replyCB;
			default:
				throw new ServiceException("Invalid configuration; This class cannot handle message '" + data.getMessageType() + "'");
		}
	}

	public String getQueueKey() {
		return queueKey;
	}

	public void setQueueKey(String queueKey) {
		this.queueKey = queueKey;
	}

	public String getAckQueueKey() {
		return ackQueueKey;
	}

	public void setAckQueueKey(String ackQueueKey) {
		this.ackQueueKey = ackQueueKey;
	}
}
