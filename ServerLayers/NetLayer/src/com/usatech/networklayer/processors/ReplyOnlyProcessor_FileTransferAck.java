package com.usatech.networklayer.processors;

import java.io.IOException;
import java.nio.BufferUnderflowException;
import java.util.HashMap;
import java.util.Map;

import simple.app.Processor;
import simple.app.ServiceException;
import simple.io.Log;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.constants.GenericResponseClientActionCode;
import com.usatech.layers.common.constants.GenericResponseResultCode;
import com.usatech.layers.common.messagedata.FileTransferMessageData;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageData_7D;
import com.usatech.layers.common.messagedata.MessageData_7E;
import com.usatech.layers.common.messagedata.MessageData_7F;
import com.usatech.layers.common.messagedata.MessageData_A5;
import com.usatech.layers.common.messagedata.MessageData_A6;
import com.usatech.layers.common.messagedata.MessageData_A7;
import com.usatech.layers.common.messagedata.MessageData_C9;
import com.usatech.layers.common.messagedata.MessageData_CB;
import com.usatech.networklayer.app.NetworkLayerMessage;
import com.usatech.networklayer.app.OutboundFileTransfer;

public class ReplyOnlyProcessor_FileTransferAck implements Processor<NetworkLayerMessage, Void> {
	private static final Log log = Log.getLog();
	protected String queueKey;
	protected String blockQueueKey;
	protected String fallThruQueueKey;

	public Class<NetworkLayerMessage> getArgumentType() {
		return NetworkLayerMessage.class;
	}

	public Class<Void> getReturnType() {
		return null;
	}

	public Void process(NetworkLayerMessage argument) throws ServiceException {
		Map<String, Object> attributes = new HashMap<String, Object>();
		MessageData data = argument.getData();
		//Assume message type 7D,7F or A5,A7 or CB
		try {
			byte group;
			int packetNum;
			switch(data.getMessageType()) {
				case FILE_XFER_START_ACK_2_0: //0x7D:
					MessageData_7D data7D = (MessageData_7D)data;
					group = data7D.getGroup();
					packetNum = 0;
					break;
				case FILE_XFER_ACK_2_0: //0x7F:
					MessageData_7F data7F = (MessageData_7F)data;
					group = data7F.getGroup();
					packetNum = data7F.getPacketNum() + 1;
					break;
				case FILE_XFER_START_ACK_3_0: //(byte)0xA5:
					MessageData_A5 dataA5 = (MessageData_A5)data;
					group = dataA5.getGroup();
					packetNum = 0;
					break;
				case FILE_XFER_ACK_3_0: //(byte)0xA7:
					MessageData_A7 dataA7 = (MessageData_A7)data;
					group = dataA7.getGroup();
					packetNum = dataA7.getPacketNum() + 1;
					break;
				case GENERIC_RESPONSE_4_1: //(byte)0xCB:
					group = 0;
					packetNum = -1;
					break;
				default:
					throw new ServiceException("Invalid configuration; This class cannot handle message '" + data.getMessageType() + "'");
			}
			OutboundFileTransfer ft = argument.getOutboundFileTransfer(group);
			if(ft == null) {
				switch(data.getMessageType()) {
					case FILE_XFER_START_ACK_2_0: //0x7D:
					case FILE_XFER_ACK_2_0: //0x7F:
					case FILE_XFER_START_ACK_3_0: //(byte)0xA5:
					case FILE_XFER_ACK_3_0: //(byte)0xA7:
						throw new ServiceException("Invalid file transfer group number");
					case GENERIC_RESPONSE_4_1: //(byte)0xCB:
						if(argument.getLog().isDebugEnabled())
							argument.getLog().debug("File Transfer not found; passing to AppLayer for processing...");
						// Send to AppLayer AppLayerNearTime for processing
						argument.enqueueExpectingReply(getFallThruQueueKey(), attributes, null, null, null);
						return null; // Send to AppLayer for processing
					default:
						throw new ServiceException("Invalid configuration; This class cannot handle message '" + data.getMessageType() + "'");
				}
			}
			if(packetNum == -1) {
				packetNum = ft.getPacketsSent();
			} else if(packetNum != ft.getPacketsSent())
				throw new ServiceException("Invalid packet number; Expected " + ft.getPacketsSent() + "; Got " + packetNum);
			//detect retry request
			switch(data.getMessageType()) {
				case GENERIC_RESPONSE_4_1:
					MessageData_CB dataCB = (MessageData_CB)data;
					switch(dataCB.getResultCode()) {
						case OKAY: //Okay
							switch(dataCB.getClientActionCode()) {
								case NO_ACTION: // no action
									//this is as expected
									if(argument.getLog().isDebugEnabled())
										argument.getLog().debug("Device acknowledged packet #" + (packetNum-1));
									break;
								case UPDATE_STATUS_REQUEST: // usr
									//TODO: What should we do here?
									//device should not be sending this until the last packet
									//handle this further after checking for file transfer completion
									if(argument.getLog().isDebugEnabled())
										argument.getLog().debug("Device sent USR");
									break;
								case STOP_FILE_TRANSFER: //Stop File Transfer
									//clear lastCommandId and send to applayer
									if(argument.getLog().isDebugEnabled())
										argument.getLog().debug("Device requested stop of file transfer");
									argument.setLastCommandId(null);
									// Send to AppLayer AppLayerNearTime for processing
									argument.enqueueExpectingReply(getFallThruQueueKey(), attributes, null, null, null);
									return null;
								default:
									throw new ServiceException("Invalid action code '" + dataCB.getClientActionCode() + "'");

							}
							break;
						case NETLAYER_ERROR: case APPLAYER_ERROR:
							if(dataCB.getClientActionCode() == GenericResponseClientActionCode.NO_ACTION || dataCB.getClientActionCode() == GenericResponseClientActionCode.UPDATE_STATUS_REQUEST) {
								//retry sending last
								ft.retry();
								packetNum = ft.getPacketsSent();
								if(argument.getLog().isDebugEnabled())
									argument.getLog().debug("Device request re-sending of file transfer packet #" + packetNum);
								break;
							}
						case DENIED: case NOT_SUPPORTED:
							// Mark pending command as failed so it is not retried and send CB
							if(argument.getLog().isDebugEnabled())
								argument.getLog().debug("Device denied file transfer packet #"  + packetNum + "; Canceling file transfer");
							// Send to AppLayer AppLayerNearTime for processing
							argument.enqueueExpectingReply(getFallThruQueueKey(), attributes, null, null, null);
							return null;
						default: // Errors
							//clear lastCommandId and send to applayer
							if(argument.getLog().isDebugEnabled())
								argument.getLog().debug("Device sent error; Stopping file transfer");
							argument.setLastCommandId(null);
							// Send to AppLayer AppLayerNearTime for processing
							argument.enqueueExpectingReply(getFallThruQueueKey(), attributes, null, null, null);
							return null;

					}
					break;
			}

			long remaining = ft.getBytesRemaining();
			if(argument.getLog().isDebugEnabled())
				argument.getLog().debug("File transfer has " + remaining + " bytes left to send as of packet #" + packetNum);
			if(remaining == 0) {
				argument.sentFileTransfer(ft.getFileType(), ft);				
				argument.removeOutboundFileTransfer(group);
				// we must send this on to applayer so it can mark file transfer as complete
				if(argument.getLog().isDebugEnabled())
					argument.getLog().debug("File Transfer complete; passing to AppLayer for processing...");
				// Send to AppLayer InboundFileTransferAckTask to mark the pending command as complete
				switch(data.getMessageType()) {
					case GENERIC_RESPONSE_4_1: // Requires a response
						MessageChain mc = new MessageChainV11();
						MessageData_CB replyCB = new MessageData_CB();
						replyCB.setResultCode(GenericResponseResultCode.OKAY);
						replyCB.setResponseMessage(argument.getTranslator().translate("client.message.file-transfer-complete", "File transfer complete"));
						replyCB.setServerActionCode(argument.getDeviceInfo().getActionCode().getGenericResponseServerActionCode());
						MessageChainStep checkStep = mc.addStep(getQueueKey());
						checkStep.addLongAttribute("pendingCommandId", ft.getPendingCommandId());
						checkStep.addStringAttribute("deviceName", argument.getDeviceName());
						checkStep.addStringAttribute("globalSessionCode", argument.getGlobalSessionCode());
						MessageChainStep ackSuccessStep = argument.constructReplyTemplateStep(mc, replyCB, null, null);
						ackSuccessStep.addReferenceAttribute("replyTemplate.serverActionCode", checkStep, "responseActionCode");
						checkStep.setNextSteps(0, ackSuccessStep);
						replyCB = new MessageData_CB();
						replyCB.setResultCode(GenericResponseResultCode.APPLAYER_ERROR);
						replyCB.setResponseMessage(argument.getTranslator().translate("client.message.filetransfer.notack", "File Transfer could not be processed"));
						replyCB.setServerActionCode(argument.getDeviceInfo().getActionCode().getGenericResponseServerActionCode());
						MessageChainStep ackFailStep = argument.constructReplyStep(mc, replyCB, null, null);
						checkStep.setNextSteps(1, ackFailStep);
						argument.enqueue(mc, true);
						break;
					default: // no response just retrurn to netlayer
						attributes.put("pendingCommandId", ft.getPendingCommandId());
						argument.enqueueExpectingReply(getQueueKey(), attributes, null, null, null);
				}
				return null;
			} else {
				int length = ft.getPacketsSize();
				if(remaining <= Integer.MAX_VALUE && remaining < length)
					length = (int)remaining;
								
				long offset = ft.getBytesSent() + 1;
				if (offset >= ft.getFileChunkOffset() && offset + length <= ft.getFileChunkOffset() + ft.getFileChunkLength()) {
					FileTransferMessageData replyFT;
					switch(data.getMessageType()) {
						case FILE_XFER_START_ACK_2_0: //0x7D:
						case FILE_XFER_ACK_2_0: //0x7F:
							MessageData_7E reply7E = new MessageData_7E();
							reply7E.setGroup(group);
							reply7E.setPacketNum(packetNum);
							replyFT = reply7E;
							break;
						case FILE_XFER_START_ACK_3_0: //(byte)0xA5:
						case FILE_XFER_ACK_3_0: //(byte)0xA7:
							MessageData_A6 replyA6 = new MessageData_A6();
							replyA6.setGroup(group);
							replyA6.setPacketNum(packetNum);
							replyFT = replyA6;
							break;
						case GENERIC_RESPONSE_4_1: //(byte)0xCB:
							MessageData_CB dataCB = (MessageData_CB)data;
							if(dataCB.getResultCode() == GenericResponseResultCode.OKAY
									&& dataCB.getClientActionCode() == GenericResponseClientActionCode.UPDATE_STATUS_REQUEST) {
								//TODO: What should we do here?
								//device should not be sending this
								//clear lastCommandId and send to applayer
								if(argument.getLog().isDebugEnabled())
									argument.getLog().debug("Device sent USR; stopping file transfer");
								argument.setLastCommandId(null);
								// Send to AppLayer AppLayerNearTime for processing
								argument.enqueueExpectingReply(getFallThruQueueKey(), attributes, null, null, null);
								return null;
							}
						//this is as expected
						if(argument.getLog().isDebugEnabled())
							argument.getLog().debug("Sending file transfer packet #" + packetNum);
							MessageData_C9 replyC9 = new MessageData_C9();
							replyC9.setPacketNum(packetNum);
							replyFT = replyC9;
							break;
						default:
							throw new ServiceException("Invalid configuration; This class cannot handle message '" + data.getMessageType() + "'");
					}
					byte[] content = new byte[length];
					ft.read(content, offset, length);
					replyFT.setContent(content);
					argument.sendReply(replyFT);
					return null;
				} else {
					attributes.put("fileTransferId", ft.getFileTransferId());
					attributes.put("fileGroup", group);
					attributes.put("filePacket", packetNum);
					attributes.put("fileBlockOffset", offset);
					attributes.put("fileBlockLength", length);
					
					ft.setPacketsSent(ft.getPacketsSent() + 1);
					ft.setBytesSent(ft.getBytesSent() + length);
					ft.setLastLength(length);
					// Send to AppLayer InboundFileTransferBlockAck for the next file block
					argument.enqueueExpectingReply(getBlockQueueKey(), attributes, null, null, null);
					return null;
				}
			}
		} catch(IOException e) {
			log.warn("Error occurred while sending file transfer packet to '" + argument.getDeviceName() + "'", e);
			// TODO: adjust this based on message type
			MessageData_CB replyCB = new MessageData_CB();
			replyCB.setResultCode(GenericResponseResultCode.APPLAYER_ERROR);
			replyCB.setResponseMessage(argument.getTranslator().translate("client.message.server-error", "Error occured on Server"));
			replyCB.setServerActionCode(argument.getDeviceInfo().getActionCode().getGenericResponseServerActionCode());
			argument.sendReply(replyCB);
			return null;
		} catch(BufferUnderflowException e) {
			log.warn("Invalid Command - not enough data from device '" + argument.getDeviceName() + "'", e);
			// TODO: adjust this based on message type
			MessageData_CB replyCB = new MessageData_CB();
			replyCB.setResultCode(GenericResponseResultCode.APPLAYER_ERROR);
			replyCB.setResponseMessage(argument.getTranslator().translate("client.message.data-truncated", "Invalid Message"));
			replyCB.setServerActionCode(argument.getDeviceInfo().getActionCode().getGenericResponseServerActionCode());
			argument.sendReply(replyCB);
			return null;
		}
	}

	public String getFallThruQueueKey() {
		return fallThruQueueKey;
	}

	public void setFallThruQueueKey(String fallThruQueueKey) {
		this.fallThruQueueKey = fallThruQueueKey;
	}

	public String getQueueKey() {
		return queueKey;
	}

	public void setQueueKey(String queueKey) {
		this.queueKey = queueKey;
	}

	public String getBlockQueueKey() {
		return blockQueueKey;
	}

	public void setBlockQueueKey(String blockQueueKey) {
		this.blockQueueKey = blockQueueKey;
	}
}
