package com.usatech.networklayer.processors;

import simple.app.ServiceException;

import com.usatech.layers.common.constants.SaleResult71;
import com.usatech.layers.common.messagedata.LegacySale;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageData_71;
import com.usatech.networklayer.app.NetworkLayerMessage;

public class ReplyOnlyProcessor_71 extends ReplyOnlyProcessor {
	@Override
	protected MessageData buildReply(NetworkLayerMessage argument) throws ServiceException {
		long transactionId=((LegacySale)argument.getData()).getTransactionId();
		MessageData_71 reply = new MessageData_71();
		reply.setTransactionId(transactionId);
		reply.setSaleResult(SaleResult71.PASS);
		return reply;
	}
}
