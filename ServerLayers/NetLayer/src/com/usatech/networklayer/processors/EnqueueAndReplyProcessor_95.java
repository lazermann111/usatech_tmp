package com.usatech.networklayer.processors;

import simple.app.ServiceException;

import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageData_95;
import com.usatech.layers.common.messagedata.MessageData_96;
import com.usatech.layers.common.messagedata.Sale;
import com.usatech.networklayer.app.NetworkLayerMessage;

public class EnqueueAndReplyProcessor_95 extends EnqueueAndReplyProcessor {
	@Override
	protected MessageData buildReply(NetworkLayerMessage networkLayerMessage) throws ServiceException {
		long cashSaleBatchId;
		if(networkLayerMessage.getData() instanceof MessageData_96) {
			MessageData_96 data96 = (MessageData_96) networkLayerMessage.getData();
			cashSaleBatchId = data96.getTransactionId();
			final int n = data96.getCashVends().size();
			for(int i = 0; i < n; i++)
				networkLayerMessage.incrementSaleCount();
		} else {
			cashSaleBatchId = ((Sale) networkLayerMessage.getData()).getTransactionId();
			networkLayerMessage.incrementSaleCount();
		}
		MessageData_95 reply = new MessageData_95();
		reply.setCashSaleBatchId(cashSaleBatchId);
		return reply;
	}
}
