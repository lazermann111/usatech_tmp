/**
 *
 */
package com.usatech.networklayer.processors;

import simple.app.ServiceException;
import simple.io.Log;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.MessageAttrEnum;
import com.usatech.layers.common.messagedata.CardReader;
import com.usatech.layers.common.messagedata.EncryptingCardReader;
import com.usatech.layers.common.messagedata.PlainCardReader;
import com.usatech.layers.common.messagedata.WS2GetCardMessageData;
import com.usatech.networklayer.app.NetworkLayerMessage;

/**
 * @author Brian S. Krug
 *
 */
public class EnqueueOnlyProcessor_CardId extends EnqueueOnlyProcessor {
	protected String decryptAccountQueueKey;
	protected String createAccountQueueKey;
	protected String validationRegex = "[%;]?([\\x20-\\x3E\\x40-\\x7F]+)(?:\\?[\\x01-\\xFF]?)?(?:\\x00[\\x00-\\xFF]*)?";

	/**
	 * @see com.usatech.networklayer.processors.EnqueueOnlyProcessor#process(com.usatech.networklayer.app.NetworkLayerMessage)
	 */
	@Override
	public Void process(NetworkLayerMessage argument) throws ServiceException {
		WS2GetCardMessageData dataCardId = (WS2GetCardMessageData) argument.getData();
		CardReader cardReader = dataCardId.getCardReader();
		MessageChain messageChain = new MessageChainV11();
		MessageChainStep step;
		if(cardReader instanceof EncryptingCardReader) {
			step = messageChain.addStep(getDecryptAccountQueueKey());
			EncryptingCardReader ecr = (EncryptingCardReader) cardReader;
			step.setAttribute(CommonAttrEnum.ATTR_CARD_READER_TYPE_ID, dataCardId.getCardReaderType());
			step.setAttribute(CommonAttrEnum.ATTR_KEY_SERIAL_NUM, ecr.getKeySerialNum());
			step.setAttribute(CommonAttrEnum.ATTR_DECRYPTED_ENCODING, ProcessingConstants.ISO8859_1_CHARSET.name());
			step.setAttribute(AuthorityAttrEnum.ATTR_ENTRY_TYPE, dataCardId.getEntryType());
			step.setAttribute(CommonAttrEnum.ATTR_ENCRYPTED_DATA, ecr.getEncryptedData());
			step.setAttribute(CommonAttrEnum.ATTR_VALIDATION_REGEX, getValidationRegex());
		} else if(cardReader instanceof PlainCardReader) {
			step = messageChain.addStep(getCreateAccountQueueKey());
			PlainCardReader pcr = (PlainCardReader) cardReader;
			step.setSecretAttribute(AuthorityAttrEnum.ATTR_ACCOUNT_DATA, MessageResponseUtils.cleanTrack(pcr.getAccountData()));
		} else {
			throw new ServiceException("Message Type: " + argument.getData().getMessageType() + " not supported by this processor");
		}

		Log sessionLog = argument.getLog();
		if(sessionLog.isDebugEnabled())
			sessionLog.debug("Enqueuing message '" + argument.getData().getMessageType() + "' from device " + argument.getDeviceName() + " to queue '" + queueKey + "' expecting reply on queue '" + argument.getReplyQueueKey() + "'");
		MessageChainStep replyStep = argument.constructReplyTemplateStep(messageChain, dataCardId.createResponse(), null, null);
		replyStep.addReferenceAttribute("replyTemplate.cardId", step, AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID.getValue());
		replyStep.addReferenceAttribute("replyTemplate.returnCode", step, MessageAttrEnum.ATTR_RESPONSE_CODE.getValue());
		replyStep.addPassThruReferenceAttribute("replyTemplate.~returnMessage", step, AuthorityAttrEnum.ATTR_CLIENT_TEXT_KEY.getValue());
		replyStep.addPassThruReferenceAttribute("replyTemplate.@returnMessage", step, AuthorityAttrEnum.ATTR_CLIENT_TEXT_PARAMS.getValue());
		messageChain.setCurrentStep(step);
		step.setNextSteps(0, replyStep);
		step.setNextSteps(5, replyStep);
		argument.enqueue(messageChain, true);

		return null;
	}

	public String getValidationRegex() {
		return validationRegex;
	}

	public void setValidationRegex(String validationRegex) {
		this.validationRegex = validationRegex;
	}

	public String getDecryptAccountQueueKey() {
		return decryptAccountQueueKey;
	}

	public void setDecryptAccountQueueKey(String decryptAccountQueueKey) {
		this.decryptAccountQueueKey = decryptAccountQueueKey;
	}

	public String getCreateAccountQueueKey() {
		return createAccountQueueKey;
	}

	public void setCreateAccountQueueKey(String createAccountQueueKey) {
		this.createAccountQueueKey = createAccountQueueKey;
	}
}
