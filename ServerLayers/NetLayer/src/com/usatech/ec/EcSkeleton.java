package com.usatech.ec;

import com.usatech.eportconnect.ECRequestHandler;
import com.usatech.layers.common.messagedata.*;
import simple.lang.InvalidValueException;
import simple.translator.Translator;
import simple.translator.TranslatorFactory;
import simple.app.ServiceException;
import com.usatech.eportconnect.ECSession;
import com.usatech.eportconnect.ECSessionManager;
import static com.usatech.eportconnect.ECAxisServlet.SOAP_PROTOCOL;

public class EcSkeleton {

	protected static Translator getTranslator() throws ServiceException {
		return TranslatorFactory.getDefaultFactory().getTranslator(null, null);
	}

	public ChargeV3_1Response chargeV3_1(ChargeV3_1 chargeV3_1) {
		MessageData_0203 requestData = new MessageData_0203();
		requestData.setProtocol(SOAP_PROTOCOL);
		requestData.setUsername(chargeV3_1.getUsername());
		requestData.setPassword(chargeV3_1.getPassword());
		requestData.setSerialNumber(chargeV3_1.getSerialNumber());
		requestData.setTranId(chargeV3_1.getTranId());
		requestData.setAmount(chargeV3_1.getAmount());
		requestData.setCardReaderTypeInt(chargeV3_1.getCardReaderType());
		requestData.setDecryptedCardDataLen(chargeV3_1.getDecryptedCardDataLen());
		requestData.setEncryptedCardDataHex(chargeV3_1.getEncryptedCardDataHex());
		requestData.setKsnHex(chargeV3_1.getKsnHex());
		requestData.setCardType(chargeV3_1.getCardType());
		requestData.setTranResult(chargeV3_1.getTranResult());
		requestData.setTranDetails(chargeV3_1.getTranDetails());
		MessageData_0301 responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.chargeV3_1(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		com.usatech.ec.xsd.ECAuthResponse responseReturn = new com.usatech.ec.xsd.ECAuthResponse();
		responseReturn.setApprovedAmount(responseData.getApprovedAmount());
		responseReturn.setNewPassword(responseData.getNewPassword() == null ? "" : responseData.getNewPassword());
		responseReturn.setNewUsername(responseData.getNewUsername() == null ? "" : responseData.getNewUsername());
		responseReturn.setReturnCode(responseData.getReturnCode());
		responseReturn.setReturnMessage(responseData.getReturnMessage() == null ? "" : responseData.getReturnMessage());
		responseReturn.setSerialNumber(responseData.getSerialNumber() == null ? "" : responseData.getSerialNumber());
		ChargeV3_1Response responseMsg = new ChargeV3_1Response();
		responseMsg.set_return(responseReturn);
		return responseMsg;
	}

	public AuthV3_1Response authV3_1(AuthV3_1 authV3_1) {
		MessageData_0201 requestData = new MessageData_0201();
		requestData.setProtocol(SOAP_PROTOCOL);
		requestData.setUsername(authV3_1.getUsername());
		requestData.setPassword(authV3_1.getPassword());
		requestData.setSerialNumber(authV3_1.getSerialNumber());
		requestData.setTranId(authV3_1.getTranId());
		requestData.setAmount(authV3_1.getAmount());
		requestData.setCardReaderTypeInt(authV3_1.getCardReaderType());
		requestData.setDecryptedCardDataLen(authV3_1.getDecryptedCardDataLen());
		requestData.setEncryptedCardDataHex(authV3_1.getEncryptedCardDataHex());
		requestData.setKsnHex(authV3_1.getKsnHex());
		requestData.setCardType(authV3_1.getCardType());
		MessageData_0301 responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.authV3_1(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		com.usatech.ec.xsd.ECAuthResponse responseReturn = new com.usatech.ec.xsd.ECAuthResponse();
		responseReturn.setApprovedAmount(responseData.getApprovedAmount());
		responseReturn.setNewPassword(responseData.getNewPassword() == null ? "" : responseData.getNewPassword());
		responseReturn.setNewUsername(responseData.getNewUsername() == null ? "" : responseData.getNewUsername());
		responseReturn.setReturnCode(responseData.getReturnCode());
		responseReturn.setReturnMessage(responseData.getReturnMessage() == null ? "" : responseData.getReturnMessage());
		responseReturn.setSerialNumber(responseData.getSerialNumber() == null ? "" : responseData.getSerialNumber());
		AuthV3_1Response responseMsg = new AuthV3_1Response();
		responseMsg.set_return(responseReturn);
		return responseMsg;
	}

	public ChargeV3Response chargeV3(ChargeV3 chargeV3) {
		MessageData_0202 requestData = new MessageData_0202();
		requestData.setProtocol(SOAP_PROTOCOL);
		requestData.setUsername(chargeV3.getUsername());
		requestData.setPassword(chargeV3.getPassword());
		requestData.setSerialNumber(chargeV3.getSerialNumber());
		requestData.setTranId(chargeV3.getTranId());
		requestData.setAmount(chargeV3.getAmount());
		requestData.setCardData(chargeV3.getCardData());
		requestData.setCardType(chargeV3.getCardType());
		requestData.setTranResult(chargeV3.getTranResult());
		requestData.setTranDetails(chargeV3.getTranDetails());
		MessageData_0301 responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.chargeV3(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		com.usatech.ec.xsd.ECAuthResponse responseReturn = new com.usatech.ec.xsd.ECAuthResponse();
		responseReturn.setApprovedAmount(responseData.getApprovedAmount());
		responseReturn.setNewPassword(responseData.getNewPassword() == null ? "" : responseData.getNewPassword());
		responseReturn.setNewUsername(responseData.getNewUsername() == null ? "" : responseData.getNewUsername());
		responseReturn.setReturnCode(responseData.getReturnCode());
		responseReturn.setReturnMessage(responseData.getReturnMessage() == null ? "" : responseData.getReturnMessage());
		responseReturn.setSerialNumber(responseData.getSerialNumber() == null ? "" : responseData.getSerialNumber());
		ChargeV3Response responseMsg = new ChargeV3Response();
		responseMsg.set_return(responseReturn);
		return responseMsg;
	}

	public ProcessUpdatesResponse processUpdates(ProcessUpdates processUpdates) {
		MessageData_0206 requestData = new MessageData_0206();
		requestData.setProtocol(SOAP_PROTOCOL);
		requestData.setUsername(processUpdates.getUsername());
		requestData.setPassword(processUpdates.getPassword());
		requestData.setSerialNumber(processUpdates.getSerialNumber());
		requestData.setUpdateStatus(processUpdates.getUpdateStatus());
		MessageData_0302 responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.processUpdates(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				// if return code is RES_OK, then FileBlockInputStream will end the session
				if(responseData.getReturnCode() != ECResponse.RES_OK)
					ECSessionManager.endSession(session);
			}
		}
		com.usatech.ec.xsd.ECProcessUpdatesResponse responseReturn = new com.usatech.ec.xsd.ECProcessUpdatesResponse();
		responseReturn.setFileContent(responseData.getFileContent());
		responseReturn.setFileName(responseData.getFileName() == null ? "" : responseData.getFileName());
		responseReturn.setFileSize(responseData.getFileSize());
		responseReturn.setFileType(responseData.getFileType());
		responseReturn.setNewPassword(responseData.getNewPassword() == null ? "" : responseData.getNewPassword());
		responseReturn.setNewUsername(responseData.getNewUsername() == null ? "" : responseData.getNewUsername());
		responseReturn.setReturnCode(responseData.getReturnCode());
		responseReturn.setReturnMessage(responseData.getReturnMessage() == null ? "" : responseData.getReturnMessage());
		responseReturn.setSerialNumber(responseData.getSerialNumber() == null ? "" : responseData.getSerialNumber());
		ProcessUpdatesResponse responseMsg = new ProcessUpdatesResponse();
		responseMsg.set_return(responseReturn);
		return responseMsg;
	}

	public CashV3Response cashV3(CashV3 cashV3) {
		MessageData_0205 requestData = new MessageData_0205();
		requestData.setProtocol(SOAP_PROTOCOL);
		requestData.setUsername(cashV3.getUsername());
		requestData.setPassword(cashV3.getPassword());
		requestData.setSerialNumber(cashV3.getSerialNumber());
		requestData.setTranId(cashV3.getTranId());
		requestData.setAmount(cashV3.getAmount());
		requestData.setTranUTCTimeMs(cashV3.getTranUTCTimeMs());
		requestData.setTranUTCOffsetMs(cashV3.getTranUTCOffsetMs());
		requestData.setTranResult(cashV3.getTranResult());
		requestData.setTranDetails(cashV3.getTranDetails());
		MessageData_0300 responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.cashV3(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		com.usatech.ec.xsd.ECResponse responseReturn = new com.usatech.ec.xsd.ECResponse();
		responseReturn.setNewPassword(responseData.getNewPassword() == null ? "" : responseData.getNewPassword());
		responseReturn.setNewUsername(responseData.getNewUsername() == null ? "" : responseData.getNewUsername());
		responseReturn.setReturnCode(responseData.getReturnCode());
		responseReturn.setReturnMessage(responseData.getReturnMessage() == null ? "" : responseData.getReturnMessage());
		responseReturn.setSerialNumber(responseData.getSerialNumber() == null ? "" : responseData.getSerialNumber());
		CashV3Response responseMsg = new CashV3Response();
		responseMsg.set_return(responseReturn);
		return responseMsg;
	}

	public BatchV3Response batchV3(BatchV3 batchV3) {
		MessageData_0204 requestData = new MessageData_0204();
		requestData.setProtocol(SOAP_PROTOCOL);
		requestData.setUsername(batchV3.getUsername());
		requestData.setPassword(batchV3.getPassword());
		requestData.setSerialNumber(batchV3.getSerialNumber());
		requestData.setTranId(batchV3.getTranId());
		requestData.setAmount(batchV3.getAmount());
		requestData.setTranResult(batchV3.getTranResult());
		requestData.setTranDetails(batchV3.getTranDetails());
		MessageData_0300 responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.batchV3(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		com.usatech.ec.xsd.ECResponse responseReturn = new com.usatech.ec.xsd.ECResponse();
		responseReturn.setNewPassword(responseData.getNewPassword() == null ? "" : responseData.getNewPassword());
		responseReturn.setNewUsername(responseData.getNewUsername() == null ? "" : responseData.getNewUsername());
		responseReturn.setReturnCode(responseData.getReturnCode());
		responseReturn.setReturnMessage(responseData.getReturnMessage() == null ? "" : responseData.getReturnMessage());
		responseReturn.setSerialNumber(responseData.getSerialNumber() == null ? "" : responseData.getSerialNumber());
		BatchV3Response responseMsg = new BatchV3Response();
		responseMsg.set_return(responseReturn);
		return responseMsg;
	}

	public UploadFileResponse uploadFile(UploadFile uploadFile) {
		MessageData_0207 requestData = new MessageData_0207();
		requestData.setProtocol(SOAP_PROTOCOL);
		requestData.setUsername(uploadFile.getUsername());
		requestData.setPassword(uploadFile.getPassword());
		requestData.setSerialNumber(uploadFile.getSerialNumber());
		requestData.setFileName(uploadFile.getFileName());
		requestData.setFileType(uploadFile.getFileType());
		requestData.setFileSize(uploadFile.getFileSize());
		requestData.setFileContent(uploadFile.getFileContent());
		MessageData_0300 responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.uploadFile(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		com.usatech.ec.xsd.ECResponse responseReturn = new com.usatech.ec.xsd.ECResponse();
		responseReturn.setNewPassword(responseData.getNewPassword() == null ? "" : responseData.getNewPassword());
		responseReturn.setNewUsername(responseData.getNewUsername() == null ? "" : responseData.getNewUsername());
		responseReturn.setReturnCode(responseData.getReturnCode());
		responseReturn.setReturnMessage(responseData.getReturnMessage() == null ? "" : responseData.getReturnMessage());
		responseReturn.setSerialNumber(responseData.getSerialNumber() == null ? "" : responseData.getSerialNumber());
		UploadFileResponse responseMsg = new UploadFileResponse();
		responseMsg.set_return(responseReturn);
		return responseMsg;
	}

	public AuthV3Response authV3(AuthV3 authV3) {
		MessageData_0200 requestData = new MessageData_0200();
		requestData.setProtocol(SOAP_PROTOCOL);
		requestData.setUsername(authV3.getUsername());
		requestData.setPassword(authV3.getPassword());
		requestData.setSerialNumber(authV3.getSerialNumber());
		requestData.setTranId(authV3.getTranId());
		requestData.setAmount(authV3.getAmount());
		requestData.setCardData(authV3.getCardData());
		requestData.setCardType(authV3.getCardType());
		MessageData_0301 responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.authV3(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		com.usatech.ec.xsd.ECAuthResponse responseReturn = new com.usatech.ec.xsd.ECAuthResponse();
		responseReturn.setApprovedAmount(responseData.getApprovedAmount());
		responseReturn.setNewPassword(responseData.getNewPassword() == null ? "" : responseData.getNewPassword());
		responseReturn.setNewUsername(responseData.getNewUsername() == null ? "" : responseData.getNewUsername());
		responseReturn.setReturnCode(responseData.getReturnCode());
		responseReturn.setReturnMessage(responseData.getReturnMessage() == null ? "" : responseData.getReturnMessage());
		responseReturn.setSerialNumber(responseData.getSerialNumber() == null ? "" : responseData.getSerialNumber());
		AuthV3Response responseMsg = new AuthV3Response();
		responseMsg.set_return(responseReturn);
		return responseMsg;
	}

}
