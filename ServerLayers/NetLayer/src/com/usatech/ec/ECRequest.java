package com.usatech.ec;

public class ECRequest implements ECServiceAPI {
	public ECAuthResponse authV3(String username, String password, String serialNumber, long tranId, 
			long amount, String cardData, String cardType){
		return null;
	}
	
	public ECAuthResponse authV3_1(String username, String password, String serialNumber, long tranId, 
			long amount, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex,
			String cardType){
		return null;
	}	
	
	public ECResponse batchV3(String username, String password, String serialNumber, long tranId, 
			long amount, String tranResult, String tranDetails){
		return null;
	}
	
	public ECResponse cashV3(String username, String password, String serialNumber, long tranId, 
			long amount, long tranUTCTimeMs, int tranUTCOffsetMs, String tranResult, String tranDetails){
		return null;
	}
	
	public ECAuthResponse chargeV3(String username, String password, String serialNumber, long tranId, 
			long amount, String cardData, String cardType, String tranResult, String tranDetails){
		return null;
	}
	
	public ECAuthResponse chargeV3_1(String username, String password, String serialNumber, long tranId, 
			long amount, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex,
			String cardType, String tranResult, String tranDetails){
		return null;
	}
	
	public ECResponse uploadFile(String username, String password, String serialNumber, String fileName, 
			int fileType, long fileSize, ECDataHandler fileContent){
		return null;
	}
	
	public ECProcessUpdatesResponse processUpdates(String username, String password, String serialNumber, int updateStatus){
		return null;
	}
}
