
/**
 * EcMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
        package com.usatech.ec;

        /**
        *  EcMessageReceiverInOut message receiver
        */

        public class EcMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver{


        public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext)
        throws org.apache.axis2.AxisFault{

        try {

        // get the implementation class for the Web Service
        Object obj = getTheImplementationObject(msgContext);

        EcSkeleton skel = (EcSkeleton)obj;
        //Out Envelop
        org.apache.axiom.soap.SOAPEnvelope envelope = null;
        //Find the axisOperation that has been set by the Dispatch phase.
        org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();
        if (op == null) {
        throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
        }

        java.lang.String methodName;
        if((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJavaIdentifier(op.getName().getLocalPart())) != null)){


        

            if("cashV3".equals(methodName)){
                
                com.usatech.ec.CashV3Response cashV3Response1 = null;
	                        com.usatech.ec.CashV3 wrappedParam =
                                                             (com.usatech.ec.CashV3)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ec.CashV3.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               cashV3Response1 =
                                                   
                                                   
                                                         skel.cashV3(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), cashV3Response1, false, new javax.xml.namespace.QName("urn:ec.usatech.com",
                                                    "cashV3"));
                                    } else 

            if("chargeV3_1".equals(methodName)){
                
                com.usatech.ec.ChargeV3_1Response chargeV3_1Response3 = null;
	                        com.usatech.ec.ChargeV3_1 wrappedParam =
                                                             (com.usatech.ec.ChargeV3_1)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ec.ChargeV3_1.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               chargeV3_1Response3 =
                                                   
                                                   
                                                         skel.chargeV3_1(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), chargeV3_1Response3, false, new javax.xml.namespace.QName("urn:ec.usatech.com",
                                                    "chargeV3_1"));
                                    } else 

            if("authV3".equals(methodName)){
                
                com.usatech.ec.AuthV3Response authV3Response5 = null;
	                        com.usatech.ec.AuthV3 wrappedParam =
                                                             (com.usatech.ec.AuthV3)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ec.AuthV3.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               authV3Response5 =
                                                   
                                                   
                                                         skel.authV3(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), authV3Response5, false, new javax.xml.namespace.QName("urn:ec.usatech.com",
                                                    "authV3"));
                                    } else 

            if("chargeV3".equals(methodName)){
                
                com.usatech.ec.ChargeV3Response chargeV3Response7 = null;
	                        com.usatech.ec.ChargeV3 wrappedParam =
                                                             (com.usatech.ec.ChargeV3)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ec.ChargeV3.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               chargeV3Response7 =
                                                   
                                                   
                                                         skel.chargeV3(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), chargeV3Response7, false, new javax.xml.namespace.QName("urn:ec.usatech.com",
                                                    "chargeV3"));
                                    } else 

            if("authV3_1".equals(methodName)){
                
                com.usatech.ec.AuthV3_1Response authV3_1Response9 = null;
	                        com.usatech.ec.AuthV3_1 wrappedParam =
                                                             (com.usatech.ec.AuthV3_1)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ec.AuthV3_1.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               authV3_1Response9 =
                                                   
                                                   
                                                         skel.authV3_1(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), authV3_1Response9, false, new javax.xml.namespace.QName("urn:ec.usatech.com",
                                                    "authV3_1"));
                                    } else 

            if("batchV3".equals(methodName)){
                
                com.usatech.ec.BatchV3Response batchV3Response11 = null;
	                        com.usatech.ec.BatchV3 wrappedParam =
                                                             (com.usatech.ec.BatchV3)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ec.BatchV3.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               batchV3Response11 =
                                                   
                                                   
                                                         skel.batchV3(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), batchV3Response11, false, new javax.xml.namespace.QName("urn:ec.usatech.com",
                                                    "batchV3"));
                                    } else 

            if("uploadFile".equals(methodName)){
                
                com.usatech.ec.UploadFileResponse uploadFileResponse13 = null;
	                        com.usatech.ec.UploadFile wrappedParam =
                                                             (com.usatech.ec.UploadFile)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ec.UploadFile.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               uploadFileResponse13 =
                                                   
                                                   
                                                         skel.uploadFile(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), uploadFileResponse13, false, new javax.xml.namespace.QName("urn:ec.usatech.com",
                                                    "uploadFile"));
                                    } else 

            if("processUpdates".equals(methodName)){
                
                com.usatech.ec.ProcessUpdatesResponse processUpdatesResponse15 = null;
	                        com.usatech.ec.ProcessUpdates wrappedParam =
                                                             (com.usatech.ec.ProcessUpdates)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ec.ProcessUpdates.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               processUpdatesResponse15 =
                                                   
                                                   
                                                         skel.processUpdates(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), processUpdatesResponse15, false, new javax.xml.namespace.QName("urn:ec.usatech.com",
                                                    "processUpdates"));
                                    
            } else {
              throw new java.lang.RuntimeException("method not found");
            }
        

        newMsgContext.setEnvelope(envelope);
        }
        }
        catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
        }
        
        //
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec.CashV3 param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec.CashV3.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec.CashV3Response param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec.CashV3Response.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec.ChargeV3_1 param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec.ChargeV3_1.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec.ChargeV3_1Response param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec.ChargeV3_1Response.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec.AuthV3 param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec.AuthV3.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec.AuthV3Response param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec.AuthV3Response.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec.ChargeV3 param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec.ChargeV3.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec.ChargeV3Response param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec.ChargeV3Response.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec.AuthV3_1 param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec.AuthV3_1.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec.AuthV3_1Response param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec.AuthV3_1Response.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec.BatchV3 param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec.BatchV3.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec.BatchV3Response param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec.BatchV3Response.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec.UploadFile param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec.UploadFile.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec.UploadFileResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec.UploadFileResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec.ProcessUpdates param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec.ProcessUpdates.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ec.ProcessUpdatesResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ec.ProcessUpdatesResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ec.CashV3Response param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ec.CashV3Response.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ec.CashV3Response wrapcashV3(){
                                com.usatech.ec.CashV3Response wrappedElement = new com.usatech.ec.CashV3Response();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ec.ChargeV3_1Response param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ec.ChargeV3_1Response.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ec.ChargeV3_1Response wrapchargeV3_1(){
                                com.usatech.ec.ChargeV3_1Response wrappedElement = new com.usatech.ec.ChargeV3_1Response();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ec.AuthV3Response param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ec.AuthV3Response.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ec.AuthV3Response wrapauthV3(){
                                com.usatech.ec.AuthV3Response wrappedElement = new com.usatech.ec.AuthV3Response();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ec.ChargeV3Response param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ec.ChargeV3Response.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ec.ChargeV3Response wrapchargeV3(){
                                com.usatech.ec.ChargeV3Response wrappedElement = new com.usatech.ec.ChargeV3Response();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ec.AuthV3_1Response param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ec.AuthV3_1Response.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ec.AuthV3_1Response wrapauthV3_1(){
                                com.usatech.ec.AuthV3_1Response wrappedElement = new com.usatech.ec.AuthV3_1Response();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ec.BatchV3Response param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ec.BatchV3Response.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ec.BatchV3Response wrapbatchV3(){
                                com.usatech.ec.BatchV3Response wrappedElement = new com.usatech.ec.BatchV3Response();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ec.UploadFileResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ec.UploadFileResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ec.UploadFileResponse wrapuploadFile(){
                                com.usatech.ec.UploadFileResponse wrappedElement = new com.usatech.ec.UploadFileResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ec.ProcessUpdatesResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ec.ProcessUpdatesResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ec.ProcessUpdatesResponse wrapprocessUpdates(){
                                com.usatech.ec.ProcessUpdatesResponse wrappedElement = new com.usatech.ec.ProcessUpdatesResponse();
                                return wrappedElement;
                         }
                    


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type,
        java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{

        try {
        
                if (com.usatech.ec.CashV3.class.equals(type)){
                
                           return com.usatech.ec.CashV3.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec.CashV3Response.class.equals(type)){
                
                           return com.usatech.ec.CashV3Response.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec.ChargeV3_1.class.equals(type)){
                
                           return com.usatech.ec.ChargeV3_1.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec.ChargeV3_1Response.class.equals(type)){
                
                           return com.usatech.ec.ChargeV3_1Response.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec.AuthV3.class.equals(type)){
                
                           return com.usatech.ec.AuthV3.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec.AuthV3Response.class.equals(type)){
                
                           return com.usatech.ec.AuthV3Response.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec.ChargeV3.class.equals(type)){
                
                           return com.usatech.ec.ChargeV3.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec.ChargeV3Response.class.equals(type)){
                
                           return com.usatech.ec.ChargeV3Response.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec.AuthV3_1.class.equals(type)){
                
                           return com.usatech.ec.AuthV3_1.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec.AuthV3_1Response.class.equals(type)){
                
                           return com.usatech.ec.AuthV3_1Response.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec.BatchV3.class.equals(type)){
                
                           return com.usatech.ec.BatchV3.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec.BatchV3Response.class.equals(type)){
                
                           return com.usatech.ec.BatchV3Response.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec.UploadFile.class.equals(type)){
                
                           return com.usatech.ec.UploadFile.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec.UploadFileResponse.class.equals(type)){
                
                           return com.usatech.ec.UploadFileResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec.ProcessUpdates.class.equals(type)){
                
                           return com.usatech.ec.ProcessUpdates.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ec.ProcessUpdatesResponse.class.equals(type)){
                
                           return com.usatech.ec.ProcessUpdatesResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    

        /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
        private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
        org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
        returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
        return returnMap;
        }

        private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
        org.apache.axis2.AxisFault f;
        Throwable cause = e.getCause();
        if (cause != null) {
            f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
        } else {
            f = new org.apache.axis2.AxisFault(e.getMessage());
        }

        return f;
    }

        }//end of class
    