package com.usatech.eportconnect;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageData_A5;
import com.usatech.layers.common.messagedata.MessageData_A7;
import com.usatech.networklayer.app.OutboundFileTransfer;

public class FileBlockInputStream extends InputStream implements Serializable {
	private static final long serialVersionUID = -1070645892070269209L;
	protected ECSession session;
	protected OutboundFileTransfer oft;
	protected byte[] fileChunk = null;
	protected int fileChunkPos = -1;
	protected int filePacket = 0;
	protected boolean transferComplete = false;
	protected Map<String, Object> attributes = new HashMap<String, Object>();
	
	public FileBlockInputStream(ECSession session) {
		this.session = session;
		this.oft = session.getOutboundFileTransfer(0);
		attributes.put("fileTransferId", oft.getFileTransferId());
		attributes.put("fileGroup", 0);
		attributes.put("fileBlockLength", 0);
	}
	
	public int read() throws IOException {
		if (oft.getBytesRemaining() < 1) {
			completeFileTransfer();
			return -1;
		}
		if (fileChunk == null || fileChunkPos >= fileChunk.length) {
			if (getNextFileChunk() < 1)
				return -1;
		}
		int byteRead = fileChunk[fileChunkPos++] & 0xff;
		oft.setBytesSent(oft.getBytesSent() + 1);
		oft.setLastLength(1);
		session.setBytesSent(session.getBytesSent() + 1);
		return byteRead;
	}
	
	public int read(byte[] b, int off, int len) throws IOException {
		if (oft.getBytesRemaining() < 1) {
			completeFileTransfer();
			return 0;
		}
		if (fileChunk == null || fileChunkPos >= fileChunk.length) {
			if (getNextFileChunk() < 1)
				return 0;
		}		
		
		int numBytes;		
		if (fileChunk.length - fileChunkPos >= len)
			numBytes = len;
		else
			numBytes = fileChunk.length - fileChunkPos;
		
		System.arraycopy(fileChunk, fileChunkPos, b, off, numBytes);
		fileChunkPos += numBytes;
		oft.setBytesSent(oft.getBytesSent() + numBytes);
		oft.setLastLength(numBytes);
		session.setBytesSent(session.getBytesSent() + numBytes);
		return numBytes;
	}
	
	protected int getNextFileChunk() throws IOException {
		try {
			attributes.put("filePacket", filePacket);
			attributes.put("fileBlockOffset", oft.getBytesSent() + 1);
			MessageData message;
			if (filePacket == 0) {
				MessageData_A5 fileTransferStartAck = new MessageData_A5();
				message = fileTransferStartAck;
			} else {
				MessageData_A7 fileTransferAck = new MessageData_A7();
				fileTransferAck.setPacketNum(filePacket);
				message = fileTransferAck;
			}
			ECSessionManager.enqueue(ECSessionManager.getOuboundFileBlockQueueKey(), session, message, true, attributes, null, null, null);
			if(ECSessionManager.awaitResponse(session)) {
				fileChunk = oft.getFileChunk();
				fileChunkPos = 0;
				filePacket++;
				return fileChunk.length;
			}
			return 0;
		} catch (Exception e) {
			throw new IOException("Error reading next file block", e);
		}
	}
	
	protected void completeFileTransfer() throws IOException {
		if (transferComplete)
			return;
		try {
			MessageData_A7 fileTransferAck = new MessageData_A7();
			fileTransferAck.setPacketNum(filePacket);
			ECSessionManager.enqueue(ECSessionManager.getOuboundFileAckQueueKey(), session, fileTransferAck, false, attributes, null, null, null);
			transferComplete = true;
			ECSessionManager.endSession(session);
		} catch (Exception e) {
			throw new IOException("Error completing file transfer", e);
		}
	}
}
