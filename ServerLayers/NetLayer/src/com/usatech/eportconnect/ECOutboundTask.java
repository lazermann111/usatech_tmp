package com.usatech.eportconnect;

import java.util.Map;

import simple.app.ServiceException;

import com.usatech.layers.common.constants.SessionCloseReason;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.networklayer.app.AbstractOutboundTask;

public class ECOutboundTask extends AbstractOutboundTask<ECSession> {
	
	@Override
	protected ECSession getAndLockSession(long sessionId, Map<String, Object> attributes) throws ServiceException {
		return ECSessionManager.getAndLockSession(sessionId);
	}

	@Override
	protected void unlockSession(ECSession session) {
		ECSessionManager.unlockSession(session);
	}

	@Override
	protected boolean sendReply(final ECSession session, final Map<String, Object> attributes, final Long lastCommandId, final SessionCloseReason endSessionReason, MessageData... replyData) {
		if(replyData != null && replyData.length > 0) {
			if(replyData[0] != null) {
				if(replyData[0].getMessageType().isAuthResponse())
					populateSessionAuthInfo(session, attributes);
				session.setReplyMessage(replyData[0]);
			}
			if(replyData.length > 1 && replyData[1] != null)
				session.setSecondReplyMessage(replyData[1]);
			else
				session.setSecondReplyMessage(null);
		}
		session.setLastCommandId(lastCommandId);
		session.signal();
		return true;
	}
}
