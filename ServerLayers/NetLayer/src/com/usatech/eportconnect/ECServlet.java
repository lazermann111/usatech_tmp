package com.usatech.eportconnect;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.text.StringUtils;

import com.usatech.layers.common.messagedata.MessageData_0107;

@SuppressWarnings("serial")
public class ECServlet extends HttpServlet {	
	public static final String HTTP_PROTOCOL = "HTTP";
	protected static final String DEFAULT_PING_URL_PATTERN = "/pingServices\\.html";
	protected static final String DEFAULT_PING_IP_PATTERN = "(?:192\\.168\\.\\d{1,3}|10\\.0\\.0)\\.\\d{1,3}|127\\.0\\.0\\.1";

	protected Pattern pingUrlPattern = Pattern.compile(DEFAULT_PING_URL_PATTERN, Pattern.CASE_INSENSITIVE);
	protected Pattern pingIpPattern = Pattern.compile(DEFAULT_PING_IP_PATTERN);
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if ("GET".equalsIgnoreCase(req.getMethod())) {
    		if ("/".equalsIgnoreCase(req.getRequestURI()) && req.getQueryString() == null)
    			ECUtils.writeUsageTerms(resp, resp.getWriter());
			else if(pingUrlPattern.matcher(req.getRequestURI()).matches() && pingIpPattern.matcher(req.getRemoteAddr()).matches()) {
				final String[] queueNames = ECSessionManager.getLoadBalancerQueueNames();
				final long timeout = ConvertUtils.getLongSafely(req.getParameter("timeout"), ECSessionManager.getSessionTimeout());
				PrintWriter writer = resp.getWriter();
				MessageData_0107 data;
				try {
					data = ECRequestHandler.pingServices(HTTP_PROTOCOL, queueNames, timeout);
					if(data == null) {
						ECUtils.writeError(resp, writer, "Request did not complete normally");
					} else {
						ECUtils.writeHeader(resp, writer);
						writer.println("<h4>Ping Services Response</h4>");
						writer.println("<style>table, td, th { border: 1px solid #A0A0F0; margin: 0px; padding: 1px;} th {background: #D0D0D0;} .totalRow td {font-weight: bold; border-top: 3px solid #A0A0F0; }</style>");
						writer.println("<table cellspacing=\"0\"><tr><th style=\"text-align:left\">Queue</th><th>Duration (ms)</th></tr>");
						List<MessageData_0107.QueueData> timings = data.getQueues();
						MessageData_0107.QueueData timing;
						for(int i = 0; i < queueNames.length; i++) {
							writer.print("<tr><td>");
							writer.print(StringUtils.isBlank(queueNames[i]) ? ECSessionManager.getReplyQueueKey() : queueNames[i]);
							writer.print("</td><td style=\"text-align:right\">");
							if(timings != null && timings.size() > i && (timing = timings.get(i)) != null)
								writer.print(timing.getQueueTime());
							writer.println("</td></tr>");
						}
						writer.print("<tr class=\"totalRow\"><td>Total:</td><td style=\"text-align:right\">");
						writer.print(data.getTotalTime());
						writer.println("</td></tr></table>");

						ECUtils.writeFooter(resp, writer);
					}
				} catch(ServiceException e) {
					ECUtils.writeError(resp, writer, e.getMessage());
				} catch(InterruptedException e) {
					ECUtils.writeError(resp, writer, "Request was interrupted");
				}
			} else
    			resp.sendRedirect("/");
        }
    }

	public String getPingUrlPattern() {
		return pingUrlPattern.pattern();
	}

	public void setPingUrlPattern(String pingUrlPattern) {
		if(StringUtils.isBlank(pingUrlPattern))
			pingUrlPattern = DEFAULT_PING_URL_PATTERN;
		Pattern pingUrl = Pattern.compile(pingUrlPattern, Pattern.CASE_INSENSITIVE);
		synchronized (this) {
			this.pingUrlPattern = pingUrl;
		}
	}

	public String getPingIpPattern() {
		return pingIpPattern.pattern();
	}

	public void setPingIpPattern(String pingIpPattern) {
		if(StringUtils.isBlank(pingIpPattern))
			pingIpPattern = DEFAULT_PING_IP_PATTERN;
		Pattern pingIp = Pattern.compile(pingIpPattern);
		synchronized (this) {
			this.pingIpPattern = pingIp;
		}
	}
}
