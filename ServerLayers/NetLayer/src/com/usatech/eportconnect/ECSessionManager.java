package com.usatech.eportconnect;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import javax.servlet.ServletRequest;

import org.apache.axis2.context.MessageContext;
import org.apache.axis2.transport.http.HTTPConstants;

import simple.app.AbstractWorkQueue;
import simple.app.BasicQoS;
import simple.app.Publisher;
import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.io.ByteInput;
import simple.io.Log;
import simple.io.resource.ResourceFolder;
import simple.security.SecureHash;
import simple.security.crypt.EncryptionInfoMapping;
import simple.text.StringUtils;
import simple.translator.DefaultTranslatorFactory;
import simple.translator.Translator;
import simple.translator.TranslatorFactory;
import simple.util.concurrent.CustomThreadFactory;

import com.caucho.services.server.ServiceContext;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.DeviceInfo;
import com.usatech.layers.common.DeviceInfoManager;
import com.usatech.layers.common.DeviceInfoManager.OnMissingPolicy;
import com.usatech.layers.common.NoUpdateDeviceInfo;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.CredentialResult;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.SessionCloseReason;
import com.usatech.layers.common.constants.SessionControlAction;
import com.usatech.layers.common.messagedata.AuthorizeMessageData;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.Sale;
import com.usatech.layers.common.messagedata.WSRequestMessageData;
import com.usatech.layers.common.messagedata.WSResponseMessageData;
import com.usatech.layers.common.util.StringHelper;
import com.usatech.networklayer.app.NetLayerUtils;
import com.usatech.networklayer.app.NetworkLayerMessage;
import com.usatech.networklayer.app.NetworkLayerQueue;
import com.usatech.networklayer.processors.EnqueueOnlyProcessor_Auth;
import com.usatech.networklayer.processors.EnqueueOnlyProcessor_CardId;

public class ECSessionManager {
	private static final Log log = Log.getLog();
	protected static Executor executor = new ThreadPoolExecutor(3, 50, 5000, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(), new CustomThreadFactory("ECSessionCloser-", true));
	protected static DeviceInfoManager deviceInfoManager;
	protected static EncryptionInfoMapping encryptionInfoMapping;
	protected static Publisher<ByteInput> publisher;
	protected static ResourceFolder resourceFolder;
	protected static String directory;
	protected static String offlineQueueKey;
	protected static String offlineHybridQueueKey;
	protected static String inboundFileQueueKey;
	protected static String ouboundFileAckQueueKey;
	protected static String ouboundFileBlockQueueKey;
	protected static String usrQueueKey;
	protected static String sessionControlQueueKey;
	protected static String messageProcessedQueueKey;
	protected static String layerName;
	protected static int layerPort;
	protected static final byte protocolId = 0;
	protected static final String layerHost = StringUtils.substringBefore(NetworkLayerQueue.getHostName(), ".");
	protected static long sessionTimeout;
	protected static int maxInputSize;
	protected static Pattern deviceSerialCdPattern;
	protected static Pattern internalDeviceSerialCdPattern;
	protected static Pattern internalIPAddressPattern;
	protected static String[] loadBalancerQueueNames;
	protected static AbstractWorkQueue<ByteInput> outboundQueue;
	protected static final BasicQoS inlineQoS = new BasicQoS(15000, 7, false, null);
	protected static final Map<Long, ECSession> sessions = new ConcurrentHashMap<Long, ECSession>();
	protected static String deviceLimitedActivityRegex;
	protected static Pattern deviceLimitedActivityPattern;
	protected static TranslatorFactory translatorFactory;
	protected static String translatorContext;
	protected static long tmpPasswordTimeLimit = 0L;
	
	public static void setDeviceLimitedActivityRegex(String deviceLimitedActivityRegex) {
		ECSessionManager.deviceLimitedActivityRegex = deviceLimitedActivityRegex;
		ECSessionManager.deviceLimitedActivityPattern = Pattern.compile(deviceLimitedActivityRegex);
	}
	protected static final ThreadLocal<ByteBuffer> byteBufferLocal = new ThreadLocal<ByteBuffer>() {
		@Override
		protected ByteBuffer initialValue() {
			return ByteBuffer.allocate(getMaxInputSize());
		}
	};
	protected static EnqueueOnlyProcessor_Auth authProcessor;
	protected static EnqueueOnlyProcessor_CardId cardIdProcessor;
	
	public static ByteBuffer getByteBuffer() {
		return byteBufferLocal.get();
	}

	public static boolean isInternalRequest(String protocol) {
		ServletRequest request = null;
		if(StringUtils.isBlank(protocol))
			return false;
		if(ECAxisServlet.SOAP_PROTOCOL.equals(protocol)) {
			MessageContext messageContext = MessageContext.getCurrentMessageContext();
			if(messageContext != null)
				request = (ServletRequest) messageContext.getProperty(HTTPConstants.MC_HTTP_SERVLETREQUEST);
		} else if(ECHessianServlet.HESSIAN_PROTOCOL.equals(protocol)) {
			request = ServiceContext.getContextRequest();
		}
		if(request == null)
			return false;
		String remoteAddr = request.getRemoteAddr();
		return remoteAddr != null && internalIPAddressPattern != null && internalIPAddressPattern.matcher(remoteAddr).matches();
	}

	public static ECSession startSession(WSRequestMessageData message, WSResponseMessageData replyMessage) {
		log.info("Received " + message);
		DeviceInfo deviceInfo = null;
		boolean internal = false;
		ECSession session = new ECSession(message.getProtocol(), message.getSerialNumber());
		session.setRequestMessage(message);
		if(message instanceof AuthorizeMessageData) {
			if(message instanceof Sale)
				session.setCallInType(ECSession.CALL_IN_TYPE_COMBO);
			else
				session.setCallInType(ECSession.CALL_IN_TYPE_AUTH);
		} else
			session.setCallInType(ECSession.CALL_IN_TYPE_BATCH);
		if(StringHelper.isBlank(message.getSerialNumber())) {
			log.info("Session Authorization Failed: No serial number provided");
			failAuthorization(session, replyMessage, "No serial number");
			return null;
		} else if(StringHelper.isBlank(message.getUsername()) || StringHelper.isBlank(message.getPassword()) || !deviceSerialCdPattern.matcher(message.getSerialNumber()).matches()) {
			if(internalDeviceSerialCdPattern != null && internalDeviceSerialCdPattern.matcher(message.getSerialNumber()).matches() && isInternalRequest(message.getProtocol())) {
				internal = true;
				session.setCredentialResult(CredentialResult.INTERNAL);
			} else {
				log.info("Session Authorization Failed: No username or password or invalid serial number");
				failAuthorization(session, replyMessage, "Invalid Authentication Data");
				return null;
			}
		}

		boolean refreshedFromSource = false;
		try {
			deviceInfo = deviceInfoManager.getDeviceInfo(message.getSerialNumber(), OnMissingPolicy.RETURN_NULL, false, null, true);
			if(deviceInfo == null) {
				deviceInfo = deviceInfoManager.getDeviceInfo(message.getSerialNumber(), OnMissingPolicy.RETURN_NULL, true, null, true);
				refreshedFromSource = true;
				if(deviceInfo == null) {// device does not exist in database so deny request
					log.info("Session Authorization Failed: Unregistered device '" + message.getSerialNumber() + "' attempted to communicate");
					session.setDeviceInfo(createInvalidDeviceInfo(message.getSerialNumber()));
					failAuthorization(session, replyMessage, "Device not enabled");
					return null;
				}
			}
			session.setDeviceInfo(deviceInfo);
			long time = System.currentTimeMillis();
			if(deviceInfo.getRejectUntil() > time) {
				log.warn("Device " + message.getSerialNumber() + " is disabled until " + new Date(deviceInfo.getRejectUntil()));
				failAuthorization(session, replyMessage, "Device disabled for " + ((deviceInfo.getRejectUntil() - time) / 1000) + " seconds");
				return null;
			}
			if(!internal) {
				CredentialResult cr = checkCredentials(message.getUsername(), message.getPassword(), deviceInfo, session);
				switch(cr) {
					case NO_MATCH:
					case INVALID_USERNAME:
						if(refreshedFromSource) {
							logAuthorizationFailure(message, deviceInfo, cr);
							failAuthorization(session, replyMessage, "Authentication failure");
							return null;
						}
						deviceInfo = deviceInfoManager.getDeviceInfo(message.getSerialNumber(), OnMissingPolicy.RETURN_NULL, true, null, true);
						session.setDeviceInfo(deviceInfo);
						refreshedFromSource = true;
						cr = checkCredentials(message.getUsername(), message.getPassword(), deviceInfo, session);
				}
				session.setCredentialResult(cr);
				switch(cr) {
					case NO_MATCH:
					case INVALID_USERNAME:
						logAuthorizationFailure(message, deviceInfo, cr);
						failAuthorization(session, replyMessage, "Authentication failure");
						return null;
					case TMP_PASSWORD_MATCHED:
						switch(message.getMessageType()) {
							case WS_PROCESS_UPDATES_REQUEST:
							case WS2_PROCESS_UPDATES_REQUEST:
								break;
							default:
								if(getTmpPasswordTimeLimit() > 0 && session.getSessionStartTime() <= deviceInfo.getCredentialTimestamp() + getTmpPasswordTimeLimit()) {
									log.info("Message '" + message.getMessageType() + "' is being allowed with temporary passcode for device '" + message.getSerialNumber() + "' because it was changed " + (session.getSessionStartTime() - deviceInfo.getCredentialTimestamp()) + " ms ago");
									break;
								}
								log.warn("Message '" + message.getMessageType() + "' is not allowed with temporary passcode for device '" + message.getSerialNumber() + "'");
								failAuthorization(session, replyMessage, "Temporary passcode not allowed for this method. Call process updates method to get new password");
								return null;
						}
				}
			}
			return session;
		} catch(ServiceException e) {
			log.warn("Could not get device info", e);
			failAuthorization(session, replyMessage, "Error");
		} catch(NoSuchAlgorithmException e) {
			log.warn("While checking device credentials", e);
			failAuthorization(session, replyMessage, "Error");
		} catch(UnsupportedEncodingException e) {
			log.warn("While checking device credentials", e);
			failAuthorization(session, replyMessage, "Error");
		}

		return null;
	}

	protected static void logAuthorizationFailure(WSRequestMessageData message, DeviceInfo deviceInfo, CredentialResult credentialResult) {
		if(log.isInfoEnabled()) {
			StringBuilder sb = new StringBuilder();
			boolean first = true;
			switch(credentialResult) {
				case INVALID_USERNAME:
					sb.append("Authentication failed for device '").append(message.getSerialNumber()).append("' because username '").append(message.getUsername()).append("' does not match ");
					if(deviceInfo.getUsername() != null && deviceInfo.getUsername().length() > 0) {
						sb.append('\'').append(deviceInfo.getUsername()).append('\'');
						first = false;
					}
					if(deviceInfo.getNewUsername() != null && deviceInfo.getNewUsername().length() > 0) {
						if(first)
							first = false;
						else
							sb.append(" or ");
						sb.append('\'').append(deviceInfo.getNewUsername()).append('\'');
					}
					if(deviceInfo.getPreviousUsername() != null && deviceInfo.getPreviousUsername().length() > 0) {
						if(first)
							first = false;
						else
							sb.append(" or ");
						sb.append('\'').append(deviceInfo.getPreviousUsername()).append('\'');
					}
					break;
				case NO_MATCH:
					sb.append("Authentication failed for device '").append(message.getSerialNumber()).append("' with username '").append(message.getUsername()).append("': Password of ").append(message.getPassword().length()).append(" characters (and 8-bit hash code of ").append(message.getPassword().hashCode() & 0xFF).append(") did not match hashes ");
					if(deviceInfo.getPasswordHash() != null && deviceInfo.getPasswordHash().length > 0) {
						StringUtils.appendHex(sb, deviceInfo.getPasswordHash());
					first = false;
					}
					if(deviceInfo.getNewPasswordHash() != null && deviceInfo.getNewPasswordHash().length > 0) {
						if(first)
							first = false;
						else
							sb.append(" or ");
						StringUtils.appendHex(sb, deviceInfo.getNewPasswordHash());
					}
					if(deviceInfo.getPreviousPasswordHash() != null && deviceInfo.getPreviousPasswordHash().length > 0) {
						if(first)
							first = false;
						else
							sb.append(" or ");
						StringUtils.appendHex(sb, deviceInfo.getPreviousPasswordHash());
					}
					break;
				default:
					sb.append("Authentication failed for device '").append(message.getSerialNumber()).append("' with username '").append(message.getUsername()).append("': ").append(credentialResult);
			}

			log.info(sb.toString());
		}
	}

	protected static void failAuthorization(ECSession session, WSResponseMessageData replyMessage, String returnMessage) {
		replyMessage.setReturnCode(0);
		replyMessage.setReturnMessage(returnMessage);
		session.setCallInStatus(SessionCloseReason.AUTHENTICATION_FAILURE.getValue());
		session.setReplyMessage(replyMessage);
		endSession(session);
	}
	protected static DeviceInfo createInvalidDeviceInfo(String deviceSerialNumber) {
		return new NoUpdateDeviceInfo("INVALID", deviceSerialNumber, null, null, 0, 0, DeviceType.KIOSK, null, false, null, null, null, null, null, null, null, null, 0, null, null, null, null, null, null, 0, null, false);
	}

	protected static CredentialResult checkCredentials(String username, String password, DeviceInfo deviceInfo, ECSession session) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		if(!username.equals(deviceInfo.getNewUsername()) && !username.equals(deviceInfo.getUsername()) && !username.equals(deviceInfo.getPreviousUsername()))
			return CredentialResult.INVALID_USERNAME;

		byte[] passwordHash;
		if(deviceInfo.getPasswordHash() != null && deviceInfo.getPasswordHash().length > 0 && deviceInfo.getPasswordSalt() != null && deviceInfo.getPasswordSalt().length > 0 && Arrays.equals((passwordHash = SecureHash.getHash(password, deviceInfo.getPasswordSalt())), deviceInfo.getPasswordHash())) {
			return CredentialResult.CURRENT_PASSWORD_MATCHED;
		} else if(deviceInfo.getNewPasswordHash() != null && deviceInfo.getNewPasswordHash().length > 0 && deviceInfo.getNewPasswordSalt() != null && deviceInfo.getNewPasswordSalt().length > 0 && Arrays.equals((passwordHash = SecureHash.getHash(password, deviceInfo.getNewPasswordSalt())), deviceInfo.getNewPasswordHash())) {
			session.setNewUsername(username);
			session.setNewPasswordHash(passwordHash);
			if(log.isInfoEnabled())
				log.info("Authenticated with new credentials");
			return CredentialResult.NEW_PASSWORD_MATCHED;
		} else if(deviceInfo.getPreviousPasswordHash() != null && deviceInfo.getPreviousPasswordHash().length > 0 && deviceInfo.getPreviousPasswordSalt() != null && deviceInfo.getPreviousPasswordSalt().length > 0 && Arrays.equals((passwordHash = SecureHash.getHash(password, deviceInfo.getPreviousPasswordSalt())), deviceInfo.getPreviousPasswordHash())) {
			if(log.isInfoEnabled())
				log.info("Authenticated with temporary credentials");
			return CredentialResult.TMP_PASSWORD_MATCHED;
		}
		return CredentialResult.NO_MATCH;
	}
	static void registerSession(ECSession session) {
		sessions.put(session.getSessionId(), session);
	}
	public static ECSession getAndLockSession(long sessionId) throws ServiceException {
		ECSession session = sessions.get(sessionId);
		if(session == null)
			throw new ServiceException("Session " + sessionId + " is no longer valid");
		session.lock();
		return session;
	}
	
	public static boolean awaitResponse(ECSession session) throws InterruptedException {
		return session.await(sessionTimeout);
	}
	
	public static void unlockSession(NetworkLayerMessage session) {
		if(session instanceof ECSession)
			((ECSession)session).unlock();
	}

	public static void endSession(final ECSession session) {
		if (session.isEnded())
			return;
		sessions.remove(session.getSessionId());
		session.end();
		executor.execute(new Runnable() {
			@Override
			public void run() {
				session.getSessionLog().info("Closing session from device " + session.getDeviceSerialNumber() 
						 + ", active for " + (session.getSessionEndTime() - session.getSessionStartTime()) + " ms");
				//add check to ignore load balancer ping
				if(session.getDeviceInfo() == null) {
					DeviceInfo deviceInfo;
					try {
						deviceInfo = deviceInfoManager.getDeviceInfo(session.getDeviceSerialNumber(), OnMissingPolicy.RETURN_NULL, false, null, true);
					} catch(ServiceException e) {
						deviceInfo = null;
					}
					if(deviceInfo == null) {
						try {
							deviceInfo = deviceInfoManager.getDeviceInfo(session.getDeviceSerialNumber(), OnMissingPolicy.RETURN_NULL, true, null, true);
						} catch(ServiceException e) {
							// ignore
						}
						if(deviceInfo == null)
							deviceInfo = createInvalidDeviceInfo(session.getDeviceSerialNumber());
					}
					session.setDeviceInfo(deviceInfo);
				}
				if(session.getDeviceInfo().getDeviceName() != null && !deviceLimitedActivityPattern.matcher(session.getDeviceInfo().getDeviceName()).matches()) {
					enqueueSessionStartMessageChain(session);				
					int messageNumber = 0;
					for (MessageData requestMessage: session.getRequestMessages()) {
						requestMessage.setMessageNumber((byte) (messageNumber % 256));
						ArrayList<MessageData> replies = session.getReplyMessages().get(messageNumber);
						if (replies == null)
							enqueueMessageProcessed(session, requestMessage, null, messageNumber, 0);
						else {
							int replyNumber = 0;
							for (MessageData replyMessage: replies) {
								replyMessage.setMessageNumber((byte) (messageNumber % 256));
								enqueueMessageProcessed(session, requestMessage, replyMessage, messageNumber, replyNumber);
								replyNumber++;
							}
						}
						messageNumber++;
					}
					enqueueSessionEndMessageChain(session);
				}
			}
		});
	}
	
	protected static void addSessionAttribute(MessageChainStep step, String attributeName, Object attributeValue) {
		step.addLiteralAttribute(ProcessingConstants.SESSION_ATTRIBUTE_PREFIX + attributeName, attributeValue);
	}
	
	public static MessageChain constructMessageChain(String queueKey, boolean expectingReply, Map<String, Object> processAttributes, MessageData message, boolean updateOnly, ECSession session, String preProcessQueueKey, Map<String, Object> preProcessAttributes, Map<String, String> preProcessResultAttributes) {
		final MessageChain messageChain = new MessageChainV11();
		final ByteBuffer dataBytes;
		if(message != null) {
			dataBytes = getByteBuffer();
			dataBytes.clear();
			message.writeData(dataBytes, false);
			dataBytes.flip();
		} else
			dataBytes = null;
		
		NetLayerUtils.constructMessageChain(messageChain, queueKey, dataBytes, updateOnly, session.getGlobalSessionCode(), session.getSessionStartTime(), null, session.getDeviceInfo(), message.getMessageStartTime(), 
				sessionTimeout, null, processAttributes, session.getSessionId(), null, null, preProcessQueueKey, preProcessAttributes, preProcessResultAttributes, (expectingReply ? getReplyQueueKey()	: null));

		return messageChain;
	}
	
	protected static void enqueueSessionStartMessageChain(ECSession session) {		
		DeviceInfo deviceInfo = session.getDeviceInfo();
		MessageChain messageChain = new MessageChainV11();
		MessageChainStep step = messageChain.addStep(getSessionControlQueueKey());
		// removed to fix USAT-373
		//step.setTemporary(true);
		step.addStringAttribute("action", SessionControlAction.START.toString());
		step.addStringAttribute("globalSessionCode", session.getGlobalSessionCode());
		step.addLongAttribute("sessionStartTime", session.getSessionStartTime());
		step.addStringAttribute("netlayerName", getLayerName());
		step.addStringAttribute("netlayerHost", getLayerHost());
		step.addIntAttribute("netlayerPort", getLayerPort());
		step.addStringAttribute("remoteAddress", session.getRemoteAddress());
		step.addIntAttribute("remotePort", session.getRemotePort());
		step.addByteAttribute("protocol", getProtocolId());
		if(deviceInfo == null) {
			step.addStringAttribute("deviceSerialCd", session.getDeviceSerialNumber());
		} else {
			step.addStringAttribute("deviceName", deviceInfo.getDeviceName());
			step.addStringAttribute("deviceSerialCd", deviceInfo.getDeviceSerialCd());
			step.addLiteralAttribute("deviceType", deviceInfo.getDeviceType());
		}
		if(!StringUtils.isBlank(session.getNewUsername())) {
			step.addStringAttribute("username", session.getNewUsername());
			step.addByteArrayAttribute("passwordHash", session.getNewPasswordHash());
		}
		if(session.getCredentialResult() != null)
			step.setAttribute(CommonAttrEnum.ATTR_CREDENTIAL_RESULT, session.getCredentialResult());
		try {
			enqueue(messageChain, false);
		} catch(ServiceException e) {
			session.getSessionLog().error("Could not enqueue session start message", e);
		}
	}
	
	protected static void enqueueSessionEndMessageChain(ECSession session) {
		DeviceInfo deviceInfo = session.getDeviceInfo();
		MessageChain mc = new MessageChainV11();
		MessageChainStep step = mc.addStep(getSessionControlQueueKey());
		// removed to fix USAT-373
		//step.setTemporary(true);
		step.addStringAttribute("action", SessionControlAction.END.toString());
		step.addStringAttribute("globalSessionCode",  session.getGlobalSessionCode());
		step.addLongAttribute("sessionStartTime", session.getSessionStartTime());
		step.addLongAttribute("sessionEndTime", System.currentTimeMillis());
		step.addStringAttribute("netlayerName", getLayerName());
		step.addStringAttribute("netlayerHost", getLayerHost());
		step.addIntAttribute("netlayerPort", getLayerPort());
		step.addStringAttribute("remoteAddress", session.getRemoteAddress());
		step.addIntAttribute("remotePort", session.getRemotePort());
		if(deviceInfo == null) {
			step.addStringAttribute("deviceSerialCd", session.getDeviceSerialNumber());
		} else {
			step.addStringAttribute("deviceName", deviceInfo.getDeviceName());
			step.addStringAttribute("deviceSerialCd", deviceInfo.getDeviceSerialCd());
			step.addLiteralAttribute("deviceType", deviceInfo.getDeviceType());
		}
		step.addLongAttribute("bytesReceived", session.getBytesReceived());
		step.addLongAttribute("bytesSent", session.getBytesSent());
		step.addIntAttribute("messagesReceived", session.getMessagesReceived());
		step.addIntAttribute("messagesSent", session.getMessagesSent());
		step.addLiteralAttribute("initialized", 'N');
		step.addLiteralAttribute("deviceSentConfig", 'N');
		step.addLiteralAttribute("serverSentConfig", 'N');
		char callInType = session.getCallInType();
		step.addLiteralAttribute("callInType", callInType);
		if(callInType == ECSession.CALL_IN_TYPE_AUTH || callInType == ECSession.CALL_IN_TYPE_COMBO) {
			step.addLongAttribute("authTime", session.getSessionStartTime());
			step.addLiteralAttribute("authResultCd", session.getAuthResultCode());
			step.addLiteralAttribute("paymentType", session.getPaymentType());
			if(session.getAuthAmount() != null)
				step.addLiteralAttribute("authAmt", session.getAuthAmount());
		}
		if(callInType == ECSession.CALL_IN_TYPE_BATCH || callInType == ECSession.CALL_IN_TYPE_COMBO)
			step.addLiteralAttribute("saleCount", session.getSaleCount());
		step.addLiteralAttribute("callInStatus", session.getCallInStatus());		
		step.addLiteralAttribute("fileCount", 0);
		step.addLiteralAttribute("eventCount", 0);		
		try {
			enqueue(mc, false);
		} catch(ServiceException e) {
			session.getSessionLog().error("Could not enqueue session end message", e);
		}
	}
	
	protected static void enqueueMessageProcessed(ECSession session, MessageData requestMessage, MessageData replyMessage, int messageSequence, int replyNumber) {
		DeviceInfo deviceInfo = session.getDeviceInfo();
		MessageChain messageChain = new MessageChainV11();
		MessageChainStep step = messageChain.addStep(getMessageProcessedQueueKey());
		// removed to fix USAT-373
		//step.setTemporary(true);
		step.addBooleanAttribute("complete", true);
		step.addBooleanAttribute("replyTransmitted", true);
		step.addStringAttribute("globalSessionCode", session.getGlobalSessionCode());
		step.addByteAttribute("protocol", getProtocolId());
		if(deviceInfo == null) {
			step.addStringAttribute("deviceSerialCd", session.getDeviceSerialNumber());
		} else {
			step.addStringAttribute("deviceName", deviceInfo.getDeviceName());
			step.addStringAttribute("deviceSerialCd", deviceInfo.getDeviceSerialCd());
			step.addLiteralAttribute("deviceType", deviceInfo.getDeviceType());
			step.addStringAttribute("locale", deviceInfo.getLocale());
		}
		step.addStringAttribute("netlayerName", getLayerName());
		step.addStringAttribute("netlayerHost", getLayerHost());
		step.addIntAttribute("netlayerPort", getLayerPort());
		step.addIntAttribute("messageSequence", messageSequence);
		step.addLongAttribute("messageStartTime", requestMessage.getMessageStartTime());
		step.addLongAttribute("messageEndTime", requestMessage.getMessageEndTime() > 0 ? requestMessage.getMessageEndTime() : System.currentTimeMillis());
				
		if(replyNumber < 1) {
			step.addStringAttribute("messageType", requestMessage.getMessageType().getHex());				
			ByteBuffer requestBuffer = ByteBuffer.allocate(ECSessionManager.getMaxInputSize());
			requestMessage.writeData(requestBuffer, true);
			requestBuffer.flip();
			session.setBytesReceived(session.getBytesReceived() + requestBuffer.remaining());
			step.addIntAttribute("dataLength", requestBuffer.remaining());
			step.addLiteralAttribute("data", requestBuffer);
		}
		
		if(replyMessage != null) {
			step.addStringAttribute("replyMessageType", replyMessage.getMessageType().getHex());
			ByteBuffer replyBuffer = ByteBuffer.allocate(ECSessionManager.getMaxInputSize());
			replyMessage.writeData(replyBuffer, true);
			replyBuffer.flip();
			session.setBytesSent(session.getBytesSent() + replyBuffer.remaining());
			step.addIntAttribute("replyLength", replyBuffer.remaining());
			step.addLiteralAttribute("reply", replyBuffer);
		}

		try {
			enqueue(messageChain, false);
		} catch(ServiceException e) {
			session.getSessionLog().error("Could not enqueue message complete", e);
		}
	}
	
	public static void enqueue(String queueKey, ECSession session, MessageData message, boolean expectingReply, Map<String, Object> processAttributes, String preProcessQueueKey, Map<String, Object> preProcessAttributes, Map<String, String> preProcessResultAttributes) throws ServiceException {
		Log sessionLog = session.getSessionLog();
		if(sessionLog.isDebugEnabled())
			sessionLog.debug("Enqueuing message '" + message.getMessageType() + "' from device " + session.getDeviceName() + " to queue '" + queueKey + "'" + (expectingReply ? " expecting reply on queue '" + getReplyQueueKey() + "'" : ""));
		if(session.getCredentialResult() != null) {
			if(processAttributes == null)
				processAttributes = Collections.singletonMap(CommonAttrEnum.ATTR_CREDENTIAL_RESULT.getValue(), (Object) session.getCredentialResult());
			else if(processAttributes instanceof HashMap || processAttributes instanceof TreeMap)
				processAttributes.put(CommonAttrEnum.ATTR_CREDENTIAL_RESULT.getValue(), session.getCredentialResult());
			else {
				processAttributes = new HashMap<>(processAttributes);
				processAttributes.put(CommonAttrEnum.ATTR_CREDENTIAL_RESULT.getValue(), session.getCredentialResult());
			}
		}
		MessageChain mc = constructMessageChain(queueKey, expectingReply, processAttributes, message, false, session, preProcessQueueKey, preProcessAttributes, preProcessResultAttributes);
		session.enqueue(mc, expectingReply);
		
		if (!expectingReply)
			message.setMessageEndTime(System.currentTimeMillis());
	}
	
	public static void enqueue(MessageChain messageChain, boolean inline) throws ServiceException {
		MessageChainService.publish(messageChain, getPublisher(), getEncryptionInfoMapping(), inline ? inlineQoS : null);
	}

	public static DeviceInfoManager getDeviceInfoManager() {
		return deviceInfoManager;
	}

	public static void setDeviceInfoManager(DeviceInfoManager deviceInfoManager) {
		ECSessionManager.deviceInfoManager = deviceInfoManager;
	}

	public static EncryptionInfoMapping getEncryptionInfoMapping() {
		return encryptionInfoMapping;
	}

	public static void setEncryptionInfoMapping(EncryptionInfoMapping encryptionInfoMapping) {
		ECSessionManager.encryptionInfoMapping = encryptionInfoMapping;
	}

	public static Publisher<ByteInput> getPublisher() {
		return publisher;
	}

	public static void setPublisher(Publisher<ByteInput> publisher) {
		ECSessionManager.publisher = publisher;
	}

	public static ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public static void setResourceFolder(ResourceFolder resourceFolder) {
		ECSessionManager.resourceFolder = resourceFolder;
	}

	public static String getDirectory() {
		return directory;
	}

	public static void setDirectory(String directory) {
		ECSessionManager.directory = directory;
	}

	public static long getSessionTimeout() {
		return sessionTimeout;
	}

	public static void setSessionTimeout(long sessionTimeout) {
		ECSessionManager.sessionTimeout = sessionTimeout;
	}

	public static String getOfflineQueueKey() {
		return offlineQueueKey;
	}

	public static void setOfflineQueueKey(String offlineQueueKey) {
		ECSessionManager.offlineQueueKey = offlineQueueKey;
	}

	public static String getInboundFileQueueKey() {
		return inboundFileQueueKey;
	}

	public static String getOuboundFileAckQueueKey() {
		return ouboundFileAckQueueKey;
	}

	public static void setOuboundFileAckQueueKey(String ouboundFileAckQueueKey) {
		ECSessionManager.ouboundFileAckQueueKey = ouboundFileAckQueueKey;
	}

	public static String getOuboundFileBlockQueueKey() {
		return ouboundFileBlockQueueKey;
	}

	public static void setOuboundFileBlockQueueKey(String ouboundFileBlockQueueKey) {
		ECSessionManager.ouboundFileBlockQueueKey = ouboundFileBlockQueueKey;
	}

	public static void setInboundFileQueueKey(String inboundFileQueueKey) {
		ECSessionManager.inboundFileQueueKey = inboundFileQueueKey;
	}

	public static String getUsrQueueKey() {
		return usrQueueKey;
	}

	public static void setUsrQueueKey(String usrQueueKey) {
		ECSessionManager.usrQueueKey = usrQueueKey;
	}

	public static String getSessionControlQueueKey() {
		return sessionControlQueueKey;
	}

	public static void setSessionControlQueueKey(String sessionControlQueueKey) {
		ECSessionManager.sessionControlQueueKey = sessionControlQueueKey;
	}

	public static String getMessageProcessedQueueKey() {
		return messageProcessedQueueKey;
	}

	public static void setMessageProcessedQueueKey(String messageProcessedQueueKey) {
		ECSessionManager.messageProcessedQueueKey = messageProcessedQueueKey;
	}

	public static String getLayerName() {
		return layerName;
	}

	public static void setLayerName(String layerName) {
		ECSessionManager.layerName = layerName;
	}

	public static int getLayerPort() {
		return layerPort;
	}

	public static void setLayerPort(int layerPort) {
		ECSessionManager.layerPort = layerPort;
		updateOutboundQueueKey();
	}

	protected static void updateOutboundQueueKey() {
		AbstractWorkQueue<ByteInput> outboundQueue = ECSessionManager.outboundQueue;
		if(outboundQueue != null && layerHost != null) {
			outboundQueue.setQueueKey("netlayer-" + layerHost + "-" + layerPort + ".outbound");
		}
	}

	public static byte getProtocolId() {
		return protocolId;
	}

	public static String getLayerHost() {
		return layerHost;
	}

	public static String getReplyQueueKey() {
		return getOutboundQueue().getQueueKey();
	}

	public static int getMaxInputSize() {
		return maxInputSize;
	}

	public static void setMaxInputSize(int maxInputSize) {
		ECSessionManager.maxInputSize = maxInputSize;
	}
	
	public static String getDeviceSerialCdRegex() {
		return deviceSerialCdPattern == null ? null : deviceSerialCdPattern.pattern();
	}

	public static void setDeviceSerialCdRegex(String deviceSerialCdRegex) {
		ECSessionManager.deviceSerialCdPattern = Pattern.compile(deviceSerialCdRegex);
	}

	public static String getInternalDeviceSerialCdRegex() {
		return internalDeviceSerialCdPattern == null ? null : internalDeviceSerialCdPattern.pattern();
	}

	public static void setInternalDeviceSerialCdRegex(String internalDeviceSerialCdRegex) {
		ECSessionManager.internalDeviceSerialCdPattern = Pattern.compile(internalDeviceSerialCdRegex);
	}

	public static String getInternalIPAddressRegex() {
		return internalIPAddressPattern == null ? null : internalIPAddressPattern.pattern();
	}

	public static void setInternalIPAddressRegex(String internalIPAddressRegex) {
		ECSessionManager.internalIPAddressPattern = Pattern.compile(internalIPAddressRegex);
	}

	public static String[] getLoadBalancerQueueNames() {
		return loadBalancerQueueNames;
	}

	public static void setLoadBalancerQueueNames(String[] loadBalancerQueueNames) {
		ECSessionManager.loadBalancerQueueNames = loadBalancerQueueNames;
	}

	public static AbstractWorkQueue<ByteInput> getOutboundQueue() {
		return outboundQueue;
	}

	public static void setOutboundQueue(AbstractWorkQueue<ByteInput> outboundQueue) {
		ECSessionManager.outboundQueue = outboundQueue;
		updateOutboundQueueKey();
	}

	public static String getOfflineHybridQueueKey() {
		return offlineHybridQueueKey;
	}

	public static void setOfflineHybridQueueKey(String offlineHybridQueueKey) {
		ECSessionManager.offlineHybridQueueKey = offlineHybridQueueKey;
	}

	public static void processAuth(ECSession session) throws ServiceException {
		authProcessor.process(session);
	}

	public static void processCardId(ECSession session) throws ServiceException {
		cardIdProcessor.process(session);
	}

	public static Translator getTranslator(String locale) {
		TranslatorFactory tf = getTranslatorFactory();
		if(tf != null)
			try {
				return tf.getTranslator(ConvertUtils.convertSafely(Locale.class, locale, Locale.getDefault()), getTranslatorContext());
			} catch(ServiceException e) {
				log.warn("Could not get translator", e);
			}
		return DefaultTranslatorFactory.getTranslatorInstance();
	}

	public static TranslatorFactory getTranslatorFactory() {
		return translatorFactory;
	}

	public static void setTranslatorFactory(TranslatorFactory translatorFactory) {
		ECSessionManager.translatorFactory = translatorFactory;
	}

	public static String getTranslatorContext() {
		return translatorContext;
	}

	public static void setTranslatorContext(String translatorContext) {
		ECSessionManager.translatorContext = translatorContext;
	}

	public static EnqueueOnlyProcessor_Auth getAuthProcessor() {
		return authProcessor;
	}

	public static void setAuthProcessor(EnqueueOnlyProcessor_Auth authProcessor) {
		ECSessionManager.authProcessor = authProcessor;
	}

	public static EnqueueOnlyProcessor_CardId getCardIdProcessor() {
		return cardIdProcessor;
	}

	public static void setCardIdProcessor(EnqueueOnlyProcessor_CardId cardIdProcessor) {
		ECSessionManager.cardIdProcessor = cardIdProcessor;
	}

	public static long getTmpPasswordTimeLimit() {
		return tmpPasswordTimeLimit;
	}

	public static void setTmpPasswordTimeLimit(long tmpPasswordTimeLimit) {
		ECSessionManager.tmpPasswordTimeLimit = tmpPasswordTimeLimit;
	}
}
