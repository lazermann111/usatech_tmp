package com.usatech.eportconnect;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.caucho.hessian.server.HessianServlet;
import com.usatech.eportconnect.ECRequestHandler;
import simple.translator.Translator;
import simple.translator.TranslatorFactory;
import simple.app.ServiceException;
import com.usatech.layers.common.messagedata.*;
import com.usatech.ec.*;

import simple.lang.InvalidValueException;
import com.usatech.ec.ECDataHandler;


public class ECHessianServlet extends HessianServlet implements ECServiceAPI {
	private static final long serialVersionUID = -8684799447422928604L;	
	public static final String HESSIAN_PROTOCOL = "Hessian";
	public static final String HESSIAN_URI = "/hessian/ec";

    protected static Translator getTranslator() throws ServiceException {
        return TranslatorFactory.getDefaultFactory().getTranslator(null, null);
    }
    
	public ECAuthResponse chargeV3_1(String username, String password, String serialNumber, long tranId, long amount, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String cardType, String tranResult, String tranDetails) {
		MessageData_0203 requestData = new MessageData_0203();
		requestData.setProtocol(HESSIAN_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setTranId(tranId);
		requestData.setAmount(amount);
		requestData.setCardReaderTypeInt(cardReaderType);
		requestData.setDecryptedCardDataLen(decryptedCardDataLen);
		requestData.setEncryptedCardDataHex(encryptedCardDataHex);
		requestData.setKsnHex(ksnHex);
		requestData.setCardType(cardType);
		requestData.setTranResult(tranResult);
		requestData.setTranDetails(tranDetails);
		MessageData_0301 responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.chargeV3_1(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		ECAuthResponse response = new ECAuthResponse();
		response.setApprovedAmount(responseData.getApprovedAmount());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	public ECAuthResponse authV3_1(String username, String password, String serialNumber, long tranId, long amount, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String cardType) {
		MessageData_0201 requestData = new MessageData_0201();
		requestData.setProtocol(HESSIAN_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setTranId(tranId);
		requestData.setAmount(amount);
		requestData.setCardReaderTypeInt(cardReaderType);
		requestData.setDecryptedCardDataLen(decryptedCardDataLen);
		requestData.setEncryptedCardDataHex(encryptedCardDataHex);
		requestData.setKsnHex(ksnHex);
		requestData.setCardType(cardType);
		MessageData_0301 responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.authV3_1(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		ECAuthResponse response = new ECAuthResponse();
		response.setApprovedAmount(responseData.getApprovedAmount());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	public ECAuthResponse chargeV3(String username, String password, String serialNumber, long tranId, long amount, String cardData, String cardType, String tranResult, String tranDetails) {
		MessageData_0202 requestData = new MessageData_0202();
		requestData.setProtocol(HESSIAN_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setTranId(tranId);
		requestData.setAmount(amount);
		requestData.setCardData(cardData);
		requestData.setCardType(cardType);
		requestData.setTranResult(tranResult);
		requestData.setTranDetails(tranDetails);
		MessageData_0301 responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.chargeV3(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		ECAuthResponse response = new ECAuthResponse();
		response.setApprovedAmount(responseData.getApprovedAmount());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	public ECProcessUpdatesResponse processUpdates(String username, String password, String serialNumber, int updateStatus) {
		MessageData_0206 requestData = new MessageData_0206();
		requestData.setProtocol(HESSIAN_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setUpdateStatus(updateStatus);
		MessageData_0302 responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.processUpdates(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				// if return code is RES_OK, then FileBlockInputStream will end the session
				if(responseData.getReturnCode() != ECResponse.RES_OK)
					ECSessionManager.endSession(session);
			}
		}
		ECProcessUpdatesResponse response = new ECProcessUpdatesResponse();
		response.setFileContent(responseData.getFileContent());
		response.setFileName(responseData.getFileName());
		response.setFileSize(responseData.getFileSize());
		response.setFileType(responseData.getFileType());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	public ECResponse cashV3(String username, String password, String serialNumber, long tranId, long amount, long tranUTCTimeMs, int tranUTCOffsetMs, String tranResult, String tranDetails) {
		MessageData_0205 requestData = new MessageData_0205();
		requestData.setProtocol(HESSIAN_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setTranId(tranId);
		requestData.setAmount(amount);
		requestData.setTranUTCTimeMs(tranUTCTimeMs);
		requestData.setTranUTCOffsetMs(tranUTCOffsetMs);
		requestData.setTranResult(tranResult);
		requestData.setTranDetails(tranDetails);
		MessageData_0300 responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.cashV3(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		ECResponse response = new ECResponse();
		response.setNewPassword(responseData.getNewPassword());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	public ECResponse batchV3(String username, String password, String serialNumber, long tranId, long amount, String tranResult, String tranDetails) {
		MessageData_0204 requestData = new MessageData_0204();
		requestData.setProtocol(HESSIAN_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setTranId(tranId);
		requestData.setAmount(amount);
		requestData.setTranResult(tranResult);
		requestData.setTranDetails(tranDetails);
		MessageData_0300 responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.batchV3(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		ECResponse response = new ECResponse();
		response.setNewPassword(responseData.getNewPassword());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	public ECResponse uploadFile(String username, String password, String serialNumber, String fileName, int fileType, long fileSize, ECDataHandler fileContent) {
		MessageData_0207 requestData = new MessageData_0207();
		requestData.setProtocol(HESSIAN_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setFileName(fileName);
		requestData.setFileType(fileType);
		requestData.setFileSize(fileSize);
		requestData.setFileContent(fileContent);
		MessageData_0300 responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.uploadFile(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		ECResponse response = new ECResponse();
		response.setNewPassword(responseData.getNewPassword());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	public ECAuthResponse authV3(String username, String password, String serialNumber, long tranId, long amount, String cardData, String cardType) {
		MessageData_0200 requestData = new MessageData_0200();
		requestData.setProtocol(HESSIAN_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setTranId(tranId);
		requestData.setAmount(amount);
		requestData.setCardData(cardData);
		requestData.setCardType(cardType);
		MessageData_0301 responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.authV3(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		ECAuthResponse response = new ECAuthResponse();
		response.setApprovedAmount(responseData.getApprovedAmount());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	
    
    public void service(ServletRequest request, ServletResponse response) throws IOException, ServletException {
    	HttpServletRequest req = (HttpServletRequest) request;
    	HttpServletResponse res = (HttpServletResponse) response;
        if ("POST".equalsIgnoreCase(req.getMethod())) {
        	if (req.getQueryString() == null)
        		super.service(request, response);
        } else if ("GET".equalsIgnoreCase(req.getMethod())) {
        	if (req.getQueryString() == null)
        		ECUtils.writeUsageTerms(res, res.getWriter());
        	else
        		res.sendRedirect(HESSIAN_URI);
        }
    }
}
