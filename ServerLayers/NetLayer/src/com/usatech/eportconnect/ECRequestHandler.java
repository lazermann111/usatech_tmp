package com.usatech.eportconnect;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import simple.app.ServiceException;
import simple.io.Log;
import simple.text.StringUtils;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService.MessageChainAttribute;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.ec.ECDataHandler;
import com.usatech.ec.ECDataSource;
import com.usatech.ec.ECResponse;
import com.usatech.ec2.EC2DataHandler;
import com.usatech.layers.common.DeviceInfoManager.OnMissingPolicy;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.MessageAttrEnum;
import com.usatech.layers.common.constants.PaymentMaskBRef;
import com.usatech.layers.common.constants.SessionCloseReason;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageData_0107;
import com.usatech.layers.common.messagedata.MessageData_0200;
import com.usatech.layers.common.messagedata.MessageData_0201;
import com.usatech.layers.common.messagedata.MessageData_0202;
import com.usatech.layers.common.messagedata.MessageData_0203;
import com.usatech.layers.common.messagedata.MessageData_0204;
import com.usatech.layers.common.messagedata.MessageData_0205;
import com.usatech.layers.common.messagedata.MessageData_0206;
import com.usatech.layers.common.messagedata.MessageData_0207;
import com.usatech.layers.common.messagedata.MessageData_020A;
import com.usatech.layers.common.messagedata.MessageData_020B;
import com.usatech.layers.common.messagedata.MessageData_020E;
import com.usatech.layers.common.messagedata.MessageData_020F;
import com.usatech.layers.common.messagedata.MessageData_0212;
import com.usatech.layers.common.messagedata.MessageData_0213;
import com.usatech.layers.common.messagedata.MessageData_0214;
import com.usatech.layers.common.messagedata.MessageData_0215;
import com.usatech.layers.common.messagedata.MessageData_0216;
import com.usatech.layers.common.messagedata.MessageData_0217;
import com.usatech.layers.common.messagedata.MessageData_0218;
import com.usatech.layers.common.messagedata.MessageData_0219;
import com.usatech.layers.common.messagedata.MessageData_021A;
import com.usatech.layers.common.messagedata.MessageData_021B;
import com.usatech.layers.common.messagedata.MessageData_021C;
import com.usatech.layers.common.messagedata.MessageData_021D;
import com.usatech.layers.common.messagedata.MessageData_021E;
import com.usatech.layers.common.messagedata.MessageData_021F;
import com.usatech.layers.common.messagedata.MessageData_0220;
import com.usatech.layers.common.messagedata.MessageData_0221;
import com.usatech.layers.common.messagedata.MessageData_0300;
import com.usatech.layers.common.messagedata.MessageData_0301;
import com.usatech.layers.common.messagedata.MessageData_0302;
import com.usatech.layers.common.messagedata.MessageData_030A;
import com.usatech.layers.common.messagedata.MessageData_030B;
import com.usatech.layers.common.messagedata.MessageData_030C;
import com.usatech.layers.common.messagedata.MessageData_030D;
import com.usatech.layers.common.messagedata.MessageData_030E;
import com.usatech.layers.common.messagedata.MessageData_030F;
import com.usatech.layers.common.messagedata.MessageData_0310;
import com.usatech.layers.common.messagedata.WSRequestMessageData;
import com.usatech.networklayer.app.InboundFileTransfer;
import com.usatech.networklayer.app.OutboundFileTransfer;

public class ECRequestHandler {
	private static final Log log = Log.getLog();

	public static MessageData_0107 pingServices(String protocol, String[] queueNames, long timeout) throws ServiceException, InterruptedException {
		ECSession session = new ECSession(protocol, ProcessingConstants.LOAD_BALANCER_MACHINE_ID);
		try {
			session.setDeviceInfo(ECSessionManager.getDeviceInfoManager().getDeviceInfo(ProcessingConstants.LOAD_BALANCER_MACHINE_ID, OnMissingPolicy.THROW_EXCEPTION, false));
			session.lock();
			long start = System.currentTimeMillis();
			long expiration = timeout > 0 ? start + timeout : 0;
			MessageChain mc = new MessageChainV11();
			MessageChainStep[] checkSteps;
			MessageChainStep lastStep = null;
			if(queueNames != null && queueNames.length > 0) {
				checkSteps = new MessageChainStep[queueNames.length];
				for(int i = 0; i < queueNames.length; i++) {
					lastStep = checkSteps[i] = mc.addStep(StringUtils.isBlank(queueNames[i]) ? ECSessionManager.getReplyQueueKey() : queueNames[i]);
					checkSteps[i].setAttribute(MessageChainAttribute.PING_ONLY, true);
					checkSteps[i].setAttribute(MessageChainAttribute.PING_EXPIRATION, expiration);
					if(i > 0)
						checkSteps[i - 1].setNextSteps(0, checkSteps[i]);
				}
			} else
				checkSteps = null;
			MessageChainStep replyStep;
			MessageData_0107 reply0107 = new MessageData_0107();
			if(queueNames != null && queueNames.length > 0) {
				for(int i = 0; i < queueNames.length; i++) {
					reply0107.addQueue();
				}
			}
			replyStep = session.constructReplyTemplateStep(mc, reply0107, SessionCloseReason.MESSAGE_BASED, null);
			replyStep.addLiteralAttribute("replyTemplate.startTime", start);
			replyStep.addLiteralAttribute("replyTemplate.endDate", "{*0}");
			if(queueNames != null && queueNames.length > 0) {
				for(int i = 0; i < queueNames.length; i++) {
					replyStep.addReferenceAttribute("replyTemplate.queues[" + i + "].endTime", checkSteps[i], MessageChainAttribute.END_TIME.getValue());
				}
			}
			if(lastStep != null) {
				lastStep.setNextSteps(0, replyStep);
			}
			session.enqueue(mc, true);
			if(session.await(timeout))
				return (MessageData_0107) session.getReplyMessage();
			return null;
		} finally {
			ECSessionManager.endSession(session);
		}

	}

	// EC1
	public static MessageData_0301 chargeV3_1(MessageData_0203 requestData, MessageData_0301 responseData, ECSession session) throws ServiceException, InterruptedException {
		session.lock();
		try {
			ECSessionManager.processAuth(session);
			if(ECSessionManager.awaitResponse(session)) {
				MessageData replyMessage = session.getReplyMessage();
				if(replyMessage instanceof MessageData_0301) {
					session.setCallInStatus(ECSession.CALL_IN_STATUS_SUCCESS);
					MessageData_0301 authReplyMessage = (MessageData_0301) replyMessage;
					switch(authReplyMessage.getReturnCode()) {
						case 2:
						case 4:
							session.incrementSaleCount();
					}
					return authReplyMessage;
				}
				log.error("Invalid reply message " + replyMessage);
				responseData.setReturnMessage("Error");
			} else
				responseData.setReturnMessage("Timeout");
		} finally {
			session.unlock();
		}
		return responseData;
	}

	public static MessageData_0301 authV3_1(MessageData_0201 requestData, MessageData_0301 responseData, ECSession session) throws ServiceException, InterruptedException {
		session.lock();
		try {
			ECSessionManager.processAuth(session);
			if(ECSessionManager.awaitResponse(session)) {
				MessageData replyMessage = session.getReplyMessage();
				if(replyMessage instanceof MessageData_0301) {
					session.setCallInStatus(ECSession.CALL_IN_STATUS_SUCCESS);
					return (MessageData_0301) replyMessage;
				}
				log.error("Invalid reply message " + replyMessage);
				responseData.setReturnMessage("Error");
			} else
				responseData.setReturnMessage("Timeout");
		} finally {
			session.unlock();
		}
		return responseData;
	}

	public static MessageData_0301 chargeV3(MessageData_0202 requestData, MessageData_0301 responseData, ECSession session) throws ServiceException, InterruptedException {
		session.lock();
		try {
			ECSessionManager.processAuth(session);
			if(ECSessionManager.awaitResponse(session)) {
				MessageData replyMessage = session.getReplyMessage();
				if(replyMessage instanceof MessageData_0301) {
					session.setCallInStatus(ECSession.CALL_IN_STATUS_SUCCESS);
					MessageData_0301 authReplyMessage = (MessageData_0301) replyMessage;
					switch(authReplyMessage.getReturnCode()) {
						case 2:
						case 4:
							session.incrementSaleCount();
					}
					return authReplyMessage;
				}
				log.error("Invalid reply message " + replyMessage);
				responseData.setReturnMessage("Error");
			} else
				responseData.setReturnMessage("Timeout");
		} finally {
			session.unlock();
		}
		return responseData;
	}

	public static MessageData_0302 processUpdates(MessageData_0206 requestData, MessageData_0302 responseData, ECSession session) throws ServiceException, InterruptedException {
		session.lock();
		try {
			ECSessionManager.enqueue(ECSessionManager.getUsrQueueKey(), session, requestData, true, null, null, null, null);
			if(ECSessionManager.awaitResponse(session)) {
				MessageData replyMessage = session.getReplyMessage();
				if(replyMessage instanceof MessageData_0302) {
					session.setCallInStatus(ECSession.CALL_IN_STATUS_SUCCESS);
					MessageData_0302 reply0302 = (MessageData_0302) replyMessage;
					switch(reply0302.getReturnCode()) {
						case ECResponse.RES_OK_NO_UPDATE:
							break;
						case ECResponse.RES_OK_UPLOAD_FILE:
							break;
						case ECResponse.RES_OK:
							OutboundFileTransfer oft = session.getOutboundFileTransfer(0);
							if(oft != null) {
								InputStream in = new FileBlockInputStream(session);
								ECDataSource ds = new ECDataSource(in);
								ECDataHandler fileContent = new ECDataHandler(ds);
								reply0302.setFileContent(fileContent);
							} else
								reply0302.setReturnCode(ECResponse.RES_OK_NO_UPDATE);
							break;
					}
					responseData = reply0302;
					return reply0302;
				}
				log.error("Invalid reply message " + replyMessage);
				responseData.setReturnMessage("Error");
			} else
				responseData.setReturnMessage("Timeout");
		} finally {
			if(responseData.getReturnCode() != ECResponse.RES_OK)
				session.unlock();
		}
		return responseData;
	}

	public static MessageData_0300 cashV3(MessageData_0205 requestData, MessageData_0300 responseData, ECSession session) throws ServiceException {
		ECSessionManager.enqueue(ECSessionManager.getOfflineHybridQueueKey(), session, requestData, false, null, null, null, null);
		responseData.setReturnCode(ECResponse.RES_OK);
		responseData.setReturnMessage("OK");
		session.incrementSaleCount();
		session.setCallInStatus(ECSession.CALL_IN_STATUS_SUCCESS);
		return responseData;
	}

	public static MessageData_0300 batchV3(MessageData_0204 requestData, MessageData_0300 responseData, ECSession session) throws ServiceException {
		ECSessionManager.enqueue(ECSessionManager.getOfflineHybridQueueKey(), session, requestData, false, null, null, null, null);
		responseData.setReturnCode(ECResponse.RES_OK);
		responseData.setReturnMessage("OK");
		session.incrementSaleCount();
		session.setCallInStatus(ECSession.CALL_IN_STATUS_SUCCESS);
		return responseData;
	}

	public static MessageData_0300 uploadFile(MessageData_0207 requestData, MessageData_0300 responseData, ECSession session) throws IOException, ServiceException {
		InputStream in = null;
		byte[] fileBytes = null;
		if(requestData.getFileContent() instanceof ECDataHandler)
			fileBytes = ((ECDataHandler) requestData.getFileContent()).getFileBytes();

		if(fileBytes == null) {
			in = requestData.getFileContent().getInputStream();
			if(in == null) {
				responseData.setReturnMessage("Empty file input stream");
				return responseData;
			}
		} else if(fileBytes.length > WSRequestMessageData.getMaxFileSize()) {
			responseData.setReturnMessage("File is too big");
			return responseData;
		}
		InboundFileTransfer ft = new InboundFileTransfer(ECSessionManager.getInboundFileQueueKey(), ECSessionManager.getLayerName(), ECSessionManager.getLayerHost(), ECSessionManager.getLayerPort(), session.getGlobalSessionCode(), session.getDeviceName());
		try {
			ft.setFileTypeId(requestData.getFileType());
			ft.setFileInfo(ECSessionManager.getResourceFolder(), ECSessionManager.getDirectory(), requestData.getFileName());
			OutputStream out = ft.getOutputStream();
			if(fileBytes == null) {
				// fileContent.writeTo(out); causes OutOfMemoryError because its AXIOM implementation stores the file in memory
				// unless cacheAttachments is set to true
				long totalBytesRead = 0;
				int bytesRead;
				byte[] bytes = new byte[1024];
				while((bytesRead = in.read(bytes)) > 0) {
					totalBytesRead += bytesRead;
					if(totalBytesRead > WSRequestMessageData.getMaxFileSize()) {
						responseData.setReturnMessage("File is too big");
						ft.finish();
						return responseData;
					}
					out.write(bytes, 0, bytesRead);
					out.flush();
				}
			} else {
				out.write(fileBytes);
				out.flush();
			}
		} finally {
			ft.close();
		}
		MessageChain mc = new MessageChainV11();
		ft.constructFileTransferStep(mc, session.getDeviceInfo());
		session.enqueue(mc, false);

		responseData.setReturnCode(ECResponse.RES_OK);
		responseData.setReturnMessage("OK");
		session.setCallInStatus(ECSession.CALL_IN_STATUS_SUCCESS);
		return responseData;
	}

	public static MessageData_0301 authV3(MessageData_0200 requestData, MessageData_0301 responseData, ECSession session) throws ServiceException, InterruptedException {
		session.lock();
		try {
			ECSessionManager.processAuth(session);
			if(ECSessionManager.awaitResponse(session)) {
				MessageData replyMessage = session.getReplyMessage();
				if(replyMessage instanceof MessageData_0301) {
					session.setCallInStatus(ECSession.CALL_IN_STATUS_SUCCESS);
					return (MessageData_0301) replyMessage;
				}
				log.error("Invalid reply message " + replyMessage);
				responseData.setReturnMessage("Error");
			} else
				responseData.setReturnMessage("Timeout");
		} finally {
			session.unlock();
		}
		return responseData;
	}

	// EC2
	public static MessageData_030C replenishEncrypted(MessageData_0217 requestData, MessageData_030C responseData, ECSession session) throws ServiceException, InterruptedException {
		session.lock();
		try {
			ECSessionManager.processAuth(session);
			if(ECSessionManager.awaitResponse(session)) {
				MessageData replyMessage = session.getReplyMessage();
				if(replyMessage instanceof MessageData_030C) {
					session.setCallInStatus(ECSession.CALL_IN_STATUS_SUCCESS);
					MessageData_030C authReplyMessage = (MessageData_030C) replyMessage;
					switch(authReplyMessage.getReturnCode()) {
						case 2:
						case 4:
							session.incrementSaleCount();
					}
					return authReplyMessage;
				}
				log.error("Invalid reply message " + replyMessage);
				responseData.setReturnMessage("Error");
			} else
				responseData.setReturnMessage("Timeout");
		} finally {
			session.unlock();
		}
		return responseData;
	}

	public static MessageData_030C replenishPlain(MessageData_0216 requestData, MessageData_030C responseData, ECSession session) throws ServiceException, InterruptedException {
		session.lock();
		try {
			ECSessionManager.processAuth(session);
			if(ECSessionManager.awaitResponse(session)) {
				MessageData replyMessage = session.getReplyMessage();
				if(replyMessage instanceof MessageData_030C) {
					session.setCallInStatus(ECSession.CALL_IN_STATUS_SUCCESS);
					MessageData_030C authReplyMessage = (MessageData_030C) replyMessage;
					switch(authReplyMessage.getReturnCode()) {
						case 2:
						case 4:
							session.incrementSaleCount();
					}
					return authReplyMessage;
				}
				log.error("Invalid reply message " + replyMessage);
				responseData.setReturnMessage("Error");
			} else
				responseData.setReturnMessage("Timeout");
		} finally {
			session.unlock();
		}
		return responseData;
	}

	public static MessageData_030A batch(MessageData_0213 requestData, MessageData_030A responseData, ECSession session) throws ServiceException {
		ECSessionManager.enqueue(ECSessionManager.getOfflineHybridQueueKey(), session, requestData, false, null, null, null, null);
		responseData.setReturnCode(ECResponse.RES_OK);
		responseData.setReturnMessage("OK");
		session.incrementSaleCount();
		session.setCallInStatus(ECSession.CALL_IN_STATUS_SUCCESS);
		return responseData;
	}

	public static MessageData_030D getCardInfoEncrypted(MessageData_0215 requestData, MessageData_030D responseData, ECSession session) throws ServiceException, InterruptedException {
		session.lock();
		try {
			ECSessionManager.processAuth(session);
			if(ECSessionManager.awaitResponse(session)) {
				MessageData replyMessage = session.getReplyMessage();
				if(replyMessage instanceof MessageData_030D) {
					session.setCallInStatus(ECSession.CALL_IN_STATUS_SUCCESS);
					return (MessageData_030D) replyMessage;
				}
				log.error("Invalid reply message " + replyMessage);
				responseData.setReturnMessage("Error");
			} else
				responseData.setReturnMessage("Timeout");
		} finally {
			session.unlock();
		}
		return responseData;
	}

	public static MessageData_030D getCardInfoPlain(MessageData_0214 requestData, MessageData_030D responseData, ECSession session) throws ServiceException, InterruptedException {
		session.lock();
		try {
			ECSessionManager.processAuth(session);
			if(ECSessionManager.awaitResponse(session)) {
				MessageData replyMessage = session.getReplyMessage();
				if(replyMessage instanceof MessageData_030D) {
					session.setCallInStatus(ECSession.CALL_IN_STATUS_SUCCESS);
					return (MessageData_030D) replyMessage;
				}
				log.error("Invalid reply message " + replyMessage);
				responseData.setReturnMessage("Error");
			} else
				responseData.setReturnMessage("Timeout");
		} finally {
			session.unlock();
		}
		return responseData;
	}

	public static MessageData_030E processUpdates(MessageData_0219 requestData, MessageData_030E responseData, ECSession session) throws ServiceException, InterruptedException {
		session.lock();
		try {
			ECSessionManager.enqueue(ECSessionManager.getUsrQueueKey(), session, requestData, true, null, null, null, null);
			if(ECSessionManager.awaitResponse(session)) {
				MessageData replyMessage = session.getReplyMessage();
				if(replyMessage instanceof MessageData_030E) {
					session.setCallInStatus(ECSession.CALL_IN_STATUS_SUCCESS);
					MessageData_030E reply0302 = (MessageData_030E) replyMessage;
					switch(reply0302.getReturnCode()) {
						case ECResponse.RES_OK_NO_UPDATE:
							break;
						case ECResponse.RES_OK_UPLOAD_FILE:
							break;
						case ECResponse.RES_OK:
							OutboundFileTransfer oft = session.getOutboundFileTransfer(0);
							if(oft != null) {
								InputStream in = new FileBlockInputStream(session);
								ECDataSource ds = new ECDataSource(in);
								EC2DataHandler fileContent = new EC2DataHandler(ds);
								reply0302.setFileContent(fileContent);
							} else
								reply0302.setReturnCode(ECResponse.RES_OK_NO_UPDATE);
							break;
					}
					responseData = reply0302;
					return reply0302;
				}
				log.error("Invalid reply message " + replyMessage);
				responseData.setReturnMessage("Error");
			} else
				responseData.setReturnMessage("Timeout");
		} finally {
			if(responseData.getReturnCode() != ECResponse.RES_OK)
				session.unlock();
		}
		return responseData;
	}

	public static MessageData_030C replenishCash(MessageData_0218 requestData, MessageData_030C responseData, ECSession session) throws ServiceException, InterruptedException {
		session.lock();
		try {
			ECSessionManager.processAuth(session);
			if(ECSessionManager.awaitResponse(session)) {
				MessageData replyMessage = session.getReplyMessage();
				if(replyMessage instanceof MessageData_030C) {
					session.setCallInStatus(ECSession.CALL_IN_STATUS_SUCCESS);
					MessageData_030C authReplyMessage = (MessageData_030C) replyMessage;
					switch(authReplyMessage.getReturnCode()) {
						case 2:
						case 4:
							session.incrementSaleCount();
					}
					return authReplyMessage;
				}
				log.error("Invalid reply message " + replyMessage);
				responseData.setReturnMessage("Error");
			} else
				responseData.setReturnMessage("Timeout");
		} finally {
			session.unlock();
		}
		return responseData;
	}

	public static MessageData_030A cash(MessageData_0212 requestData, MessageData_030A responseData, ECSession session) throws ServiceException {
		ECSessionManager.enqueue(ECSessionManager.getOfflineQueueKey(), session, requestData, false, null, null, null, null);
		responseData.setReturnCode(ECResponse.RES_OK);
		responseData.setReturnMessage("OK");
		session.incrementSaleCount();
		session.setCallInStatus(ECSession.CALL_IN_STATUS_SUCCESS);
		return responseData;
	}

	public static MessageData_030B chargePlain(MessageData_020E requestData, MessageData_030B responseData, ECSession session) throws ServiceException, InterruptedException {
		session.lock();
		try {
			ECSessionManager.processAuth(session);
			if(ECSessionManager.awaitResponse(session)) {
				MessageData replyMessage = session.getReplyMessage();
				if(replyMessage instanceof MessageData_030B) {
					session.setCallInStatus(ECSession.CALL_IN_STATUS_SUCCESS);
					MessageData_030B authReplyMessage = (MessageData_030B) replyMessage;
					switch(authReplyMessage.getReturnCode()) {
						case 2:
						case 4:
							session.incrementSaleCount();
					}
					return authReplyMessage;
				}
				log.error("Invalid reply message " + replyMessage);
				responseData.setReturnMessage("Error");
			} else
				responseData.setReturnMessage("Timeout");
		} finally {
			session.unlock();
		}
		return responseData;
	}

	public static MessageData_030B chargeEncrypted(MessageData_020F requestData, MessageData_030B responseData, ECSession session) throws ServiceException, InterruptedException {
		session.lock();
		try {
			ECSessionManager.processAuth(session);
			if(ECSessionManager.awaitResponse(session)) {
				MessageData replyMessage = session.getReplyMessage();
				if(replyMessage instanceof MessageData_030B) {
					session.setCallInStatus(ECSession.CALL_IN_STATUS_SUCCESS);
					MessageData_030B authReplyMessage = (MessageData_030B) replyMessage;
					switch(authReplyMessage.getReturnCode()) {
						case 2:
						case 4:
							session.incrementSaleCount();
					}
					return authReplyMessage;
				}
				log.error("Invalid reply message " + replyMessage);
				responseData.setReturnMessage("Error");
			} else
				responseData.setReturnMessage("Timeout");
		} finally {
			session.unlock();
		}
		return responseData;
	}

	public static MessageData_030B authEncrypted(MessageData_020B requestData, MessageData_030B responseData, ECSession session) throws ServiceException, InterruptedException {
		session.lock();
		try {
			ECSessionManager.processAuth(session);
			if(ECSessionManager.awaitResponse(session)) {
				MessageData replyMessage = session.getReplyMessage();
				if(replyMessage instanceof MessageData_030B) {
					session.setCallInStatus(ECSession.CALL_IN_STATUS_SUCCESS);
					return (MessageData_030B) replyMessage;
				}
				log.error("Invalid reply message " + replyMessage);
				responseData.setReturnMessage("Error");
			} else
				responseData.setReturnMessage("Timeout");
		} finally {
			session.unlock();
		}
		return responseData;
	}

	public static MessageData_030A uploadDeviceInfo(MessageData_021A requestData, MessageData_030A responseData, ECSession session) throws ServiceException {
		ECSessionManager.enqueue(ECSessionManager.getOfflineQueueKey(), session, requestData, false, null, null, null, null);
		responseData.setReturnCode(ECResponse.RES_OK);
		responseData.setReturnMessage("OK");
		session.setCallInType('U');
		session.setCallInStatus(ECSession.CALL_IN_STATUS_SUCCESS);
		return responseData;
	}

	public static MessageData_030A uploadFile(MessageData_021B requestData, MessageData_030A responseData, ECSession session) throws IOException, ServiceException {
		InputStream in = null;
		byte[] fileBytes = null;
		if(requestData.getFileContent() instanceof ECDataHandler)
			fileBytes = ((ECDataHandler) requestData.getFileContent()).getFileBytes();
		else if(requestData.getFileContent() instanceof EC2DataHandler)
			fileBytes = ((EC2DataHandler) requestData.getFileContent()).getFileBytes();

		if(fileBytes == null) {
			in = requestData.getFileContent().getInputStream();
			if(in == null) {
				responseData.setReturnMessage("Empty file input stream");
				return responseData;
			}
		} else if(fileBytes.length > WSRequestMessageData.getMaxFileSize()) {
			responseData.setReturnMessage("File is too big");
			return responseData;
		}
		InboundFileTransfer ft = new InboundFileTransfer(ECSessionManager.getInboundFileQueueKey(), ECSessionManager.getLayerName(), ECSessionManager.getLayerHost(), ECSessionManager.getLayerPort(), session.getGlobalSessionCode(), session.getDeviceName());
		try {
			ft.setFileTypeId(requestData.getFileType());
			ft.setFileInfo(ECSessionManager.getResourceFolder(), ECSessionManager.getDirectory(), requestData.getFileName());
			OutputStream out = ft.getOutputStream();
			if(fileBytes == null) {
				// fileContent.writeTo(out); causes OutOfMemoryError because its AXIOM implementation stores the file in memory
				// unless cacheAttachments is set to true
				long totalBytesRead = 0;
				int bytesRead;
				byte[] bytes = new byte[1024];
				while((bytesRead = in.read(bytes)) > 0) {
					totalBytesRead += bytesRead;
					if(totalBytesRead > WSRequestMessageData.getMaxFileSize()) {
						responseData.setReturnMessage("File is too big");
						ft.finish();
						return responseData;
					}
					out.write(bytes, 0, bytesRead);
					out.flush();
				}
			} else {
				out.write(fileBytes);
				out.flush();
			}
		} finally {
			ft.close();
		}
		MessageChain mc = new MessageChainV11();
		ft.constructFileTransferStep(mc, session.getDeviceInfo());
		session.enqueue(mc, false);

		responseData.setReturnCode(ECResponse.RES_OK);
		responseData.setReturnMessage("OK");
		session.setCallInStatus(ECSession.CALL_IN_STATUS_SUCCESS);
		return responseData;
	}

	public static MessageData_030B authPlain(MessageData_020A requestData, MessageData_030B responseData, ECSession session) throws ServiceException, InterruptedException {
		session.lock();
		try {
			ECSessionManager.processAuth(session);
			if(ECSessionManager.awaitResponse(session)) {
				MessageData replyMessage = session.getReplyMessage();
				if(replyMessage instanceof MessageData_030B) {
					session.setCallInStatus(ECSession.CALL_IN_STATUS_SUCCESS);
					return (MessageData_030B) replyMessage;
				}
				log.error("Invalid reply message " + replyMessage);
				responseData.setReturnMessage("Error");
			} else
				responseData.setReturnMessage("Timeout");
		} finally {
			session.unlock();
		}
		return responseData;
	}

	public static MessageData_030F getCardIdPlain(MessageData_021C requestData, MessageData_030F responseData, ECSession session) throws ServiceException, InterruptedException {
		session.lock();
		try {
			ECSessionManager.processCardId(session);
			if(ECSessionManager.awaitResponse(session)) {
				MessageData replyMessage = session.getReplyMessage();
				if(replyMessage instanceof MessageData_030F) {
					session.setCallInStatus(ECSession.CALL_IN_STATUS_SUCCESS);
					return (MessageData_030F) replyMessage;
				}
				log.error("Invalid reply message " + replyMessage);
				responseData.setReturnMessage("Error");
			} else
				responseData.setReturnMessage("Timeout");
		} finally {
			session.unlock();
		}
		return responseData;
	}

	public static MessageData_030F getCardIdEncrypted(MessageData_021D requestData, MessageData_030F responseData, ECSession session) throws ServiceException, InterruptedException {
		session.lock();
		try {
			ECSessionManager.processCardId(session);
			if(ECSessionManager.awaitResponse(session)) {
				MessageData replyMessage = session.getReplyMessage();
				if(replyMessage instanceof MessageData_030F) {
					session.setCallInStatus(ECSession.CALL_IN_STATUS_SUCCESS);
					return (MessageData_030F) replyMessage;
				}
				log.error("Invalid reply message " + replyMessage);
				responseData.setReturnMessage("Error");
			} else
				responseData.setReturnMessage("Timeout");
		} finally {
			session.unlock();
		}
		return responseData;
	}

	public static MessageData_0310 tokenizeEncrypted(MessageData_021F requestData, MessageData_0310 responseData, ECSession session) throws ServiceException, InterruptedException {
		session.lock();
		try {
			ECSessionManager.processAuth(session);
			if(ECSessionManager.awaitResponse(session)) {
				MessageData replyMessage = session.getReplyMessage();
				if(replyMessage instanceof MessageData_0310) {
					session.setCallInStatus(ECSession.CALL_IN_STATUS_SUCCESS);
					return (MessageData_0310) replyMessage;
				}
				log.error("Invalid reply message " + replyMessage);
				responseData.setReturnMessage("Error");
			} else
				responseData.setReturnMessage("Timeout");
		} finally {
			session.unlock();
		}
		return responseData;
	}

	public static MessageData_0310 tokenizePlain(MessageData_021E requestData, MessageData_0310 responseData, ECSession session) throws ServiceException, InterruptedException {
		session.lock();
		try {
			ECSessionManager.processAuth(session);
			if(ECSessionManager.awaitResponse(session)) {
				MessageData replyMessage = session.getReplyMessage();
				if(replyMessage instanceof MessageData_0310) {
					session.setCallInStatus(ECSession.CALL_IN_STATUS_SUCCESS);
					return (MessageData_0310) replyMessage;
				}
				log.error("Invalid reply message " + replyMessage);
				responseData.setReturnMessage("Error");
			} else
				responseData.setReturnMessage("Timeout");
		} finally {
			session.unlock();
		}
		return responseData;
	}

	public static MessageData_030A untokenize(MessageData_0220 requestData, MessageData_030A responseData, ECSession session) throws InterruptedException, ServiceException {
		session.lock();
		try {
			MessageChain mc = new MessageChainV11();
			MessageChainStep step = mc.addStep("usat.keymanager.account.token.remove", true);
			step.setAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, requestData.getCardId());
			step.setAttribute(AuthorityAttrEnum.ATTR_AUTH_TOKEN, requestData.getToken());
			MessageChainStep replyStep = session.constructReplyTemplateStep(mc, responseData, SessionCloseReason.MESSAGE_BASED, null);
			replyStep.addReferenceAttribute("replyTemplate.returnCode", step, "responseCode");
			replyStep.addReferenceAttribute("replyTemplate.returnMessage", step, "responseMessage");
			step.setNextSteps(0, replyStep);
			session.enqueue(mc, true);
			
			if(ECSessionManager.awaitResponse(session)) {
				MessageData replyMessage = session.getReplyMessage();
				if(replyMessage instanceof MessageData_030A) {
					session.setCallInStatus(ECSession.CALL_IN_STATUS_SUCCESS);
					return (MessageData_030A) replyMessage;
				}
				log.error("Invalid reply message " + replyMessage);
				responseData.setReturnMessage("Error");
			} else
				responseData.setReturnMessage("Timeout");
		} finally {
			session.unlock();
		}
		return responseData;
	}

	protected final static Pattern PREPAID_VIRTUAL_DEVICE_PATTERN = Pattern.compile("V3-\\d+-(\\w+)");
	public static MessageData_0310 retokenize(MessageData_0221 requestData, MessageData_0310 responseData, ECSession session) throws ServiceException, InterruptedException {
		session.lock();
		try {
			responseData.setCardId(requestData.getCardId());
			if(!session.isInternal()) {
				responseData.setCardType("Unknown");
				responseData.setReturnCode(AuthResultCode.DECLINED_PERMANENT.getAuthResponseCodeEC2());
				responseData.setReturnMessage("Not permitted for this device");
			} else {
				MessageChain mc = new MessageChainV11();
				MessageChainStep verifyStep = mc.addStep("usat.inbound.message.verify");
				verifyStep.setTemporary(true);
				verifyStep.setAttribute(MessageAttrEnum.ATTR_DEVICE_NAME, session.getDeviceName());
				verifyStep.setAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, requestData.getCardId());
				Matcher matcher;
				if(session.getDeviceSerialNumber() != null && (matcher = PREPAID_VIRTUAL_DEVICE_PATTERN.matcher(session.getDeviceSerialNumber())).matches())
					verifyStep.setAttribute(AuthorityAttrEnum.ATTR_CURRENCY_CD, matcher.group(1));

				verifyStep.setSecretAttribute(PaymentMaskBRef.DISCRETIONARY_DATA.attribute, requestData.getSecurityCode());
				if(!StringUtils.isBlank(requestData.getBillingPostalCode()))
					verifyStep.setSecretAttribute(PaymentMaskBRef.ZIP_CODE.attribute, requestData.getBillingPostalCode());
				if(!StringUtils.isBlank(requestData.getBillingAddress()))
					verifyStep.setSecretAttribute(PaymentMaskBRef.ADDRESS.attribute, requestData.getBillingAddress());
				if(!StringUtils.isBlank(requestData.getCardHolder()))
					verifyStep.setSecretAttribute(PaymentMaskBRef.CARD_HOLDER.attribute, requestData.getCardHolder());
				MessageData_0310 invalidResponse = requestData.createResponse();
				invalidResponse.setCardId(requestData.getCardId());
				invalidResponse.setReturnCode(AuthResultCode.DECLINED.getAuthResponseCodeEC2());
				invalidResponse.setReturnMessage("Confirmation data does not match");

				MessageChainStep replyInvalidStep = session.constructReplyTemplateStep(mc, responseData, SessionCloseReason.MESSAGE_BASED, null);
				replyInvalidStep.addLiteralAttribute("replyTemplate.cardId", requestData.getCardId());
				replyInvalidStep.addReferenceAttribute("replyTemplate.cardType", verifyStep, AuthorityAttrEnum.ATTR_CARD_TYPE.getValue());
				replyInvalidStep.addReferenceAttribute("replyTemplate.returnCode", verifyStep, "responseCode");
				replyInvalidStep.addReferenceAttribute("replyTemplate.returnMessage", verifyStep, "responseMessage");
				replyInvalidStep.setTemporary(true);
				verifyStep.setNextSteps(1, replyInvalidStep);

				MessageChainStep lookupTokenStep = mc.addStep("usat.keymanager.account.token.lookup");
				lookupTokenStep.setAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, requestData.getCardId());
				lookupTokenStep.setAttribute(MessageAttrEnum.ATTR_DEVICE_NAME, session.getDeviceName());
				lookupTokenStep.setAttribute(CommonAttrEnum.ATTR_CREATE, true);
				/* could add ADDRESS and CARD_HOLDER, but don't really need to */
				int decryptedCount = 0;
				for(PaymentMaskBRef bref : new PaymentMaskBRef[] { PaymentMaskBRef.PRIMARY_ACCOUNT_NUMBER, PaymentMaskBRef.EXPIRATION_DATE, PaymentMaskBRef.ZIP_CODE }) {
					lookupTokenStep.setReferenceIndexedAttribute(CommonAttrEnum.ATTR_DECRYPTED_DATA, bref.getStoreIndex(), verifyStep, bref.attribute);
					if(decryptedCount < bref.getStoreIndex())
						decryptedCount = bref.getStoreIndex();
				}
				lookupTokenStep.setAttribute(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT, decryptedCount);
				lookupTokenStep.setAttribute(CommonAttrEnum.ATTR_DECRYPTED_ENCODING, ProcessingConstants.ISO8859_1_CHARSET.name());

				lookupTokenStep.setTemporary(true);
				MessageChainStep replyTokenStep = session.constructReplyTemplateStep(mc, responseData, SessionCloseReason.MESSAGE_BASED, null);
				replyTokenStep.addLiteralAttribute("replyTemplate.cardId", requestData.getCardId());
				replyTokenStep.addReferenceAttribute("replyTemplate.cardType", verifyStep, AuthorityAttrEnum.ATTR_CARD_TYPE.getValue());
				replyTokenStep.addReferenceAttribute("replyTemplate.returnCode", lookupTokenStep, "responseCode");
				replyTokenStep.addReferenceAttribute("replyTemplate.returnMessage", lookupTokenStep, "responseMessage");
				replyTokenStep.addReferenceAttribute("replyTemplate.tokenBytes", lookupTokenStep, AuthorityAttrEnum.ATTR_AUTH_TOKEN.getValue());
				replyTokenStep.setTemporary(true);
				lookupTokenStep.setNextSteps(0, replyTokenStep);

				verifyStep.setNextSteps(0, lookupTokenStep);

				session.enqueue(mc, true);

				if(ECSessionManager.awaitResponse(session)) {
					MessageData replyMessage = session.getReplyMessage();
					if(replyMessage instanceof MessageData_0310) {
						session.setCallInStatus(ECSession.CALL_IN_STATUS_SUCCESS);
						return (MessageData_0310) replyMessage;
					}
					log.error("Invalid reply message " + replyMessage);
					responseData.setReturnMessage("Error");
				} else
					responseData.setReturnMessage("Timeout");
			}
		} finally {
			session.unlock();
		}
		return responseData;
	}
}
