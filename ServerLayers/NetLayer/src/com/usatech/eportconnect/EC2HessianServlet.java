package com.usatech.eportconnect;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.caucho.hessian.server.HessianServlet;
import com.usatech.eportconnect.ECRequestHandler;
import simple.translator.Translator;
import simple.translator.TranslatorFactory;
import simple.app.ServiceException;
import com.usatech.layers.common.messagedata.*;
import com.usatech.ec2.*;

import simple.lang.InvalidValueException;
import com.usatech.ec2.EC2DataHandler;


public class EC2HessianServlet extends HessianServlet implements EC2ServiceAPI {
	private static final long serialVersionUID = -8684799447422859176L;	
	public static final String HESSIAN_PROTOCOL = "Hessian";
	public static final String HESSIAN_URI = "/hessian/ec2";

    protected static Translator getTranslator() throws ServiceException {
        return TranslatorFactory.getDefaultFactory().getTranslator(null, null);
    }
    
	public EC2ReplenishResponse replenishPlain(String username, String password, String serialNumber, long tranId, long amount, String cardData, String entryType, long replenishCardId, long replenishConsumerId, String attributes) {
		MessageData_0216 requestData = new MessageData_0216();
		requestData.setProtocol(HESSIAN_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setTranId(tranId);
		requestData.setAmount(amount);
		requestData.setCardData(cardData);
		requestData.setEntryTypeString(entryType);
		requestData.setReplenishCardId(replenishCardId);
		requestData.setReplenishConsumerId(replenishConsumerId);
		requestData.setAttributes(attributes);
		MessageData_030C responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.replenishPlain(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2ReplenishResponse response = new EC2ReplenishResponse();
		response.setActionCode(responseData.getActionCode());
		response.setApprovedAmount(responseData.getApprovedAmount());
		response.setAttributes(responseData.getAttributes());
		response.setBalanceAmount(responseData.getBalanceAmount());
		response.setCardId(responseData.getCardId());
		response.setCardType(responseData.getCardType());
		response.setConsumerId(responseData.getConsumerId());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReplenishBalanceAmount(responseData.getReplenishBalanceAmount());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	public EC2ReplenishResponse replenishEncrypted(String username, String password, String serialNumber, long tranId, long amount, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String entryType, long replenishCardId, long replenishConsumerId, String attributes) {
		MessageData_0217 requestData = new MessageData_0217();
		requestData.setProtocol(HESSIAN_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setTranId(tranId);
		requestData.setAmount(amount);
		requestData.setCardReaderTypeInt(cardReaderType);
		requestData.setDecryptedCardDataLen(decryptedCardDataLen);
		requestData.setEncryptedCardDataHex(encryptedCardDataHex);
		requestData.setKsnHex(ksnHex);
		requestData.setEntryTypeString(entryType);
		requestData.setReplenishCardId(replenishCardId);
		requestData.setReplenishConsumerId(replenishConsumerId);
		requestData.setAttributes(attributes);
		MessageData_030C responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.replenishEncrypted(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2ReplenishResponse response = new EC2ReplenishResponse();
		response.setActionCode(responseData.getActionCode());
		response.setApprovedAmount(responseData.getApprovedAmount());
		response.setAttributes(responseData.getAttributes());
		response.setBalanceAmount(responseData.getBalanceAmount());
		response.setCardId(responseData.getCardId());
		response.setCardType(responseData.getCardType());
		response.setConsumerId(responseData.getConsumerId());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReplenishBalanceAmount(responseData.getReplenishBalanceAmount());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	public EC2Response batch(String username, String password, String serialNumber, long tranId, long amount, String tranResult, String tranDetails, String attributes) {
		MessageData_0213 requestData = new MessageData_0213();
		requestData.setProtocol(HESSIAN_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setTranId(tranId);
		requestData.setAmount(amount);
		requestData.setTranResult(tranResult);
		requestData.setTranDetails(tranDetails);
		requestData.setAttributes(attributes);
		MessageData_030A responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.batch(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2Response response = new EC2Response();
		response.setActionCode(responseData.getActionCode());
		response.setAttributes(responseData.getAttributes());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	public EC2CardInfo getCardInfoEncrypted(String username, String password, String serialNumber, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String entryType, String attributes) {
		MessageData_0215 requestData = new MessageData_0215();
		requestData.setProtocol(HESSIAN_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setCardReaderTypeInt(cardReaderType);
		requestData.setDecryptedCardDataLen(decryptedCardDataLen);
		requestData.setEncryptedCardDataHex(encryptedCardDataHex);
		requestData.setKsnHex(ksnHex);
		requestData.setEntryTypeString(entryType);
		requestData.setAttributes(attributes);
		MessageData_030D responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.getCardInfoEncrypted(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2CardInfo response = new EC2CardInfo();
		response.setActionCode(responseData.getActionCode());
		response.setAttributes(responseData.getAttributes());
		response.setBalanceAmount(responseData.getBalanceAmount());
		response.setCardId(responseData.getCardId());
		response.setCardType(responseData.getCardType());
		response.setConsumerId(responseData.getConsumerId());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	public EC2CardInfo getCardInfoPlain(String username, String password, String serialNumber, String cardData, String entryType, String attributes) {
		MessageData_0214 requestData = new MessageData_0214();
		requestData.setProtocol(HESSIAN_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setCardData(cardData);
		requestData.setEntryTypeString(entryType);
		requestData.setAttributes(attributes);
		MessageData_030D responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.getCardInfoPlain(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2CardInfo response = new EC2CardInfo();
		response.setActionCode(responseData.getActionCode());
		response.setAttributes(responseData.getAttributes());
		response.setBalanceAmount(responseData.getBalanceAmount());
		response.setCardId(responseData.getCardId());
		response.setCardType(responseData.getCardType());
		response.setConsumerId(responseData.getConsumerId());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	public EC2TokenResponse tokenizeEncrypted(String username, String password, String serialNumber, long tranId, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String billingPostalCode, String billingAddress, String attributes) {
		MessageData_021F requestData = new MessageData_021F();
		requestData.setProtocol(HESSIAN_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setTranId(tranId);
		requestData.setCardReaderTypeInt(cardReaderType);
		requestData.setDecryptedCardDataLen(decryptedCardDataLen);
		requestData.setEncryptedCardDataHex(encryptedCardDataHex);
		requestData.setKsnHex(ksnHex);
		requestData.setBillingPostalCode(billingPostalCode);
		requestData.setBillingAddress(billingAddress);
		requestData.setAttributes(attributes);
		MessageData_0310 responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.tokenizeEncrypted(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2TokenResponse response = new EC2TokenResponse();
		response.setActionCode(responseData.getActionCode());
		response.setAttributes(responseData.getAttributes());
		response.setCardId(responseData.getCardId());
		response.setCardType(responseData.getCardType());
		response.setConsumerId(responseData.getConsumerId());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		response.setTokenHex(responseData.getTokenHex());
		return response;
	}

	public EC2TokenResponse tokenizePlain(String username, String password, String serialNumber, long tranId, String cardNumber, String expirationDate, String securityCode, String cardHolder, String billingPostalCode, String billingAddress, String attributes) {
		MessageData_021E requestData = new MessageData_021E();
		requestData.setProtocol(HESSIAN_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setTranId(tranId);
		requestData.setCardNumber(cardNumber);
		requestData.setExpirationDate(expirationDate);
		requestData.setSecurityCode(securityCode);
		requestData.setCardHolder(cardHolder);
		requestData.setBillingPostalCode(billingPostalCode);
		requestData.setBillingAddress(billingAddress);
		requestData.setAttributes(attributes);
		MessageData_0310 responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.tokenizePlain(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2TokenResponse response = new EC2TokenResponse();
		response.setActionCode(responseData.getActionCode());
		response.setAttributes(responseData.getAttributes());
		response.setCardId(responseData.getCardId());
		response.setCardType(responseData.getCardType());
		response.setConsumerId(responseData.getConsumerId());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		response.setTokenHex(responseData.getTokenHex());
		return response;
	}

	public EC2ProcessUpdatesResponse processUpdates(String username, String password, String serialNumber, int updateStatus, int protocolVersion, String appType, String appVersion, String attributes) {
		MessageData_0219 requestData = new MessageData_0219();
		requestData.setProtocol(HESSIAN_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setUpdateStatus(updateStatus);
		requestData.setProtocolVersion(protocolVersion);
		requestData.setAppType(appType);
		requestData.setAppVersion(appVersion);
		requestData.setAttributes(attributes);
		MessageData_030E responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.processUpdates(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				// if return code is RES_OK, then FileBlockInputStream will end the session
				if(responseData.getReturnCode() != com.usatech.ec.ECResponse.RES_OK)
					ECSessionManager.endSession(session);
			}
		}
		EC2ProcessUpdatesResponse response = new EC2ProcessUpdatesResponse();
		response.setActionCode(responseData.getActionCode());
		response.setAttributes(responseData.getAttributes());
		response.setFileContent(responseData.getFileContent());
		response.setFileName(responseData.getFileName());
		response.setFileSize(responseData.getFileSize());
		response.setFileType(responseData.getFileType());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	public EC2ReplenishResponse replenishCash(String username, String password, String serialNumber, long tranId, long amount, long replenishCardId, long replenishConsumerId, String attributes) {
		MessageData_0218 requestData = new MessageData_0218();
		requestData.setProtocol(HESSIAN_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setTranId(tranId);
		requestData.setAmount(amount);
		requestData.setReplenishCardId(replenishCardId);
		requestData.setReplenishConsumerId(replenishConsumerId);
		requestData.setAttributes(attributes);
		MessageData_030C responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.replenishCash(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2ReplenishResponse response = new EC2ReplenishResponse();
		response.setActionCode(responseData.getActionCode());
		response.setApprovedAmount(responseData.getApprovedAmount());
		response.setAttributes(responseData.getAttributes());
		response.setBalanceAmount(responseData.getBalanceAmount());
		response.setCardId(responseData.getCardId());
		response.setCardType(responseData.getCardType());
		response.setConsumerId(responseData.getConsumerId());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReplenishBalanceAmount(responseData.getReplenishBalanceAmount());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	public EC2CardId getCardIdPlain(String username, String password, String serialNumber, String cardData, String entryType, String attributes) {
		MessageData_021C requestData = new MessageData_021C();
		requestData.setProtocol(HESSIAN_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setCardData(cardData);
		requestData.setEntryTypeString(entryType);
		requestData.setAttributes(attributes);
		MessageData_030F responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.getCardIdPlain(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2CardId response = new EC2CardId();
		response.setActionCode(responseData.getActionCode());
		response.setAttributes(responseData.getAttributes());
		response.setCardId(responseData.getCardId());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	public EC2CardId getCardIdEncrypted(String username, String password, String serialNumber, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String entryType, String attributes) {
		MessageData_021D requestData = new MessageData_021D();
		requestData.setProtocol(HESSIAN_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setCardReaderTypeInt(cardReaderType);
		requestData.setDecryptedCardDataLen(decryptedCardDataLen);
		requestData.setEncryptedCardDataHex(encryptedCardDataHex);
		requestData.setKsnHex(ksnHex);
		requestData.setEntryTypeString(entryType);
		requestData.setAttributes(attributes);
		MessageData_030F responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.getCardIdEncrypted(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2CardId response = new EC2CardId();
		response.setActionCode(responseData.getActionCode());
		response.setAttributes(responseData.getAttributes());
		response.setCardId(responseData.getCardId());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	public EC2Response cash(String username, String password, String serialNumber, long tranId, long amount, long tranUTCTimeMs, int tranUTCOffsetMs, String tranResult, String tranDetails, String attributes) {
		MessageData_0212 requestData = new MessageData_0212();
		requestData.setProtocol(HESSIAN_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setTranId(tranId);
		requestData.setAmount(amount);
		requestData.setTranUTCTimeMs(tranUTCTimeMs);
		requestData.setTranUTCOffsetMs(tranUTCOffsetMs);
		requestData.setTranResult(tranResult);
		requestData.setTranDetails(tranDetails);
		requestData.setAttributes(attributes);
		MessageData_030A responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.cash(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2Response response = new EC2Response();
		response.setActionCode(responseData.getActionCode());
		response.setAttributes(responseData.getAttributes());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	public EC2AuthResponse chargePlain(String username, String password, String serialNumber, long tranId, long amount, String cardData, String entryType, String tranResult, String tranDetails, String attributes) {
		MessageData_020E requestData = new MessageData_020E();
		requestData.setProtocol(HESSIAN_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setTranId(tranId);
		requestData.setAmount(amount);
		requestData.setCardData(cardData);
		requestData.setEntryTypeString(entryType);
		requestData.setTranResult(tranResult);
		requestData.setTranDetails(tranDetails);
		requestData.setAttributes(attributes);
		MessageData_030B responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.chargePlain(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2AuthResponse response = new EC2AuthResponse();
		response.setActionCode(responseData.getActionCode());
		response.setApprovedAmount(responseData.getApprovedAmount());
		response.setAttributes(responseData.getAttributes());
		response.setBalanceAmount(responseData.getBalanceAmount());
		response.setCardId(responseData.getCardId());
		response.setCardType(responseData.getCardType());
		response.setConsumerId(responseData.getConsumerId());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	public EC2AuthResponse chargeEncrypted(String username, String password, String serialNumber, long tranId, long amount, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String entryType, String tranResult, String tranDetails, String attributes) {
		MessageData_020F requestData = new MessageData_020F();
		requestData.setProtocol(HESSIAN_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setTranId(tranId);
		requestData.setAmount(amount);
		requestData.setCardReaderTypeInt(cardReaderType);
		requestData.setDecryptedCardDataLen(decryptedCardDataLen);
		requestData.setEncryptedCardDataHex(encryptedCardDataHex);
		requestData.setKsnHex(ksnHex);
		requestData.setEntryTypeString(entryType);
		requestData.setTranResult(tranResult);
		requestData.setTranDetails(tranDetails);
		requestData.setAttributes(attributes);
		MessageData_030B responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.chargeEncrypted(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2AuthResponse response = new EC2AuthResponse();
		response.setActionCode(responseData.getActionCode());
		response.setApprovedAmount(responseData.getApprovedAmount());
		response.setAttributes(responseData.getAttributes());
		response.setBalanceAmount(responseData.getBalanceAmount());
		response.setCardId(responseData.getCardId());
		response.setCardType(responseData.getCardType());
		response.setConsumerId(responseData.getConsumerId());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	public EC2AuthResponse authEncrypted(String username, String password, String serialNumber, long tranId, long amount, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String entryType, String attributes) {
		MessageData_020B requestData = new MessageData_020B();
		requestData.setProtocol(HESSIAN_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setTranId(tranId);
		requestData.setAmount(amount);
		requestData.setCardReaderTypeInt(cardReaderType);
		requestData.setDecryptedCardDataLen(decryptedCardDataLen);
		requestData.setEncryptedCardDataHex(encryptedCardDataHex);
		requestData.setKsnHex(ksnHex);
		requestData.setEntryTypeString(entryType);
		requestData.setAttributes(attributes);
		MessageData_030B responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.authEncrypted(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2AuthResponse response = new EC2AuthResponse();
		response.setActionCode(responseData.getActionCode());
		response.setApprovedAmount(responseData.getApprovedAmount());
		response.setAttributes(responseData.getAttributes());
		response.setBalanceAmount(responseData.getBalanceAmount());
		response.setCardId(responseData.getCardId());
		response.setCardType(responseData.getCardType());
		response.setConsumerId(responseData.getConsumerId());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	public EC2Response uploadDeviceInfo(String username, String password, String serialNumber, String attributes) {
		MessageData_021A requestData = new MessageData_021A();
		requestData.setProtocol(HESSIAN_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setAttributes(attributes);
		MessageData_030A responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.uploadDeviceInfo(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2Response response = new EC2Response();
		response.setActionCode(responseData.getActionCode());
		response.setAttributes(responseData.getAttributes());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	public EC2Response uploadFile(String username, String password, String serialNumber, String fileName, int fileType, long fileSize, EC2DataHandler fileContent, String attributes) {
		MessageData_021B requestData = new MessageData_021B();
		requestData.setProtocol(HESSIAN_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setFileName(fileName);
		requestData.setFileType(fileType);
		requestData.setFileSize(fileSize);
		requestData.setFileContent(fileContent);
		requestData.setAttributes(attributes);
		MessageData_030A responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.uploadFile(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2Response response = new EC2Response();
		response.setActionCode(responseData.getActionCode());
		response.setAttributes(responseData.getAttributes());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	public EC2Response untokenize(String username, String password, String serialNumber, long cardId, String tokenHex, String attributes) {
		MessageData_0220 requestData = new MessageData_0220();
		requestData.setProtocol(HESSIAN_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setCardId(cardId);
		requestData.setTokenHex(tokenHex);
		requestData.setAttributes(attributes);
		MessageData_030A responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.untokenize(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2Response response = new EC2Response();
		response.setActionCode(responseData.getActionCode());
		response.setAttributes(responseData.getAttributes());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	public EC2AuthResponse authPlain(String username, String password, String serialNumber, long tranId, long amount, String cardData, String entryType, String attributes) {
		MessageData_020A requestData = new MessageData_020A();
		requestData.setProtocol(HESSIAN_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setTranId(tranId);
		requestData.setAmount(amount);
		requestData.setCardData(cardData);
		requestData.setEntryTypeString(entryType);
		requestData.setAttributes(attributes);
		MessageData_030B responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.authPlain(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2AuthResponse response = new EC2AuthResponse();
		response.setActionCode(responseData.getActionCode());
		response.setApprovedAmount(responseData.getApprovedAmount());
		response.setAttributes(responseData.getAttributes());
		response.setBalanceAmount(responseData.getBalanceAmount());
		response.setCardId(responseData.getCardId());
		response.setCardType(responseData.getCardType());
		response.setConsumerId(responseData.getConsumerId());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	public EC2TokenResponse retokenize(String username, String password, String serialNumber, long cardId, String securityCode, String cardHolder, String billingPostalCode, String billingAddress, String attributes) {
		MessageData_0221 requestData = new MessageData_0221();
		requestData.setProtocol(HESSIAN_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setCardId(cardId);
		requestData.setSecurityCode(securityCode);
		requestData.setCardHolder(cardHolder);
		requestData.setBillingPostalCode(billingPostalCode);
		requestData.setBillingAddress(billingAddress);
		requestData.setAttributes(attributes);
		MessageData_0310 responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if(session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.retokenize(requestData, responseData, session);
				} catch(InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch(Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if(session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2TokenResponse response = new EC2TokenResponse();
		response.setActionCode(responseData.getActionCode());
		response.setAttributes(responseData.getAttributes());
		response.setCardId(responseData.getCardId());
		response.setCardType(responseData.getCardType());
		response.setConsumerId(responseData.getConsumerId());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		response.setTokenHex(responseData.getTokenHex());
		return response;
	}

	
    
    public void service(ServletRequest request, ServletResponse response) throws IOException, ServletException {
    	HttpServletRequest req = (HttpServletRequest) request;
    	HttpServletResponse res = (HttpServletResponse) response;
        if ("POST".equalsIgnoreCase(req.getMethod())) {
        	if (req.getQueryString() == null)
        		super.service(request, response);
        } else if ("GET".equalsIgnoreCase(req.getMethod())) {
        	if (req.getQueryString() == null)
        		ECUtils.writeUsageTerms(res, res.getWriter());
        	else
        		res.sendRedirect(HESSIAN_URI);
        }
    }
}
