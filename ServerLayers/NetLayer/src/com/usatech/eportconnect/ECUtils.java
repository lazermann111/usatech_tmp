package com.usatech.eportconnect;

import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;

public class ECUtils {
	public static void writeUsageTerms(HttpServletResponse response, PrintWriter writer) {
		writeHeader(response, writer);
		writer.println("NOTICE TO USERS<br/><br/>");
		
		writer.println("This computer system is the private property of USA Technologies, Inc.<br/>");   
		writer.println("*** It Is For Authorized Use Only. ***<br/><br/>");

		writer.println("Any or all uses of this system may be intercepted,<br/>");
		writer.println("audited, inspected, and monitored, recorded, disclosed<br/>");
		writer.println("to authorized site, government, and law enforcement personnel, as well<br/>");
		writer.println("as authorized officials of government agencies, both domestic and foreign.<br/>");
		writer.println("By using this system, the user consents to such interception, monitoring,<br/>"); 
		writer.println("recording, auditing, inspection, and disclosure at the discretion of<br/>");
		writer.println("such personnel or officials. Unauthorized or improper use of this system may<br/>");
		writer.println("result in civil and criminal penalties, and administrative or disciplinary<br/>");
		writer.println("action, as appropriate. By continuing to use this system you indicate your<br/>");
		writer.println("awareness of and consent to these terms and conditions of use.<br/><br/>");

		writer.println("LOG OFF IMMEDIATELY if you do not agree to the conditions stated in this warning.<br/><br/>");
		
		writeFooter(response, writer);
	}

	public static void writeHeader(HttpServletResponse response, PrintWriter writer) {
		response.addHeader("Expires", "-1");
		response.setContentType("text/html; charset=utf-8");

		writer.println("<html>");
		writer.println("<title>USA Technologies ePort Connect</title>");
		writer.println("<body>");
		writer.println("<div align=\"center\" style=\"font-family: Arial, Verdana, Tahoma, Sans-Serif;\">");
		writer.println("<h3>USA Technologies ePort Connect</h3>");
	}

	public static void writeFooter(HttpServletResponse response, PrintWriter writer) {
		writer.print("USA Technologies, Inc. - &copy; Copyright 2003-");
		writer.print(Calendar.getInstance().get(Calendar.YEAR));
		writer.println(" - Confidential");

		writer.println("</div>");
		writer.println("</body>");
		writer.println("</html>");

		writer.flush();
	}

	public static void writeError(HttpServletResponse response, PrintWriter writer, String errorMessage) {
		writeHeader(response, writer);
		writer.println("<h4>ERROR</h4>");
		writer.print("<h6>");
		writer.print(new Date());
		writer.println("</h6>");
		writer.println(errorMessage);
		writer.println("<br/><br/>");
		writeFooter(response, writer);
	}
}
