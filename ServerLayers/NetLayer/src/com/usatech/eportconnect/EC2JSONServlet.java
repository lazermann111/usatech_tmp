package com.usatech.eportconnect;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

import javax.activation.FileDataSource;
import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import simple.app.ServiceException;
import simple.lang.InvalidValueException;
import simple.translator.Translator;
import simple.translator.TranslatorFactory;

import com.usatech.ec2.EC2AuthResponse;
import com.usatech.ec2.EC2CardId;
import com.usatech.ec2.EC2CardInfo;
import com.usatech.ec2.EC2DataHandler;
import com.usatech.ec2.EC2ProcessUpdatesResponse;
import com.usatech.ec2.EC2ReplenishResponse;
import com.usatech.ec2.EC2Response;
import com.usatech.ec2.EC2ServiceAPI;
import com.usatech.ec2.EC2TokenResponse;
import com.usatech.layers.common.messagedata.MessageData_020A;
import com.usatech.layers.common.messagedata.MessageData_020B;
import com.usatech.layers.common.messagedata.MessageData_020E;
import com.usatech.layers.common.messagedata.MessageData_020F;
import com.usatech.layers.common.messagedata.MessageData_0212;
import com.usatech.layers.common.messagedata.MessageData_0213;
import com.usatech.layers.common.messagedata.MessageData_0214;
import com.usatech.layers.common.messagedata.MessageData_0215;
import com.usatech.layers.common.messagedata.MessageData_0216;
import com.usatech.layers.common.messagedata.MessageData_0217;
import com.usatech.layers.common.messagedata.MessageData_0218;
import com.usatech.layers.common.messagedata.MessageData_0219;
import com.usatech.layers.common.messagedata.MessageData_021A;
import com.usatech.layers.common.messagedata.MessageData_021B;
import com.usatech.layers.common.messagedata.MessageData_021C;
import com.usatech.layers.common.messagedata.MessageData_021D;
import com.usatech.layers.common.messagedata.MessageData_021E;
import com.usatech.layers.common.messagedata.MessageData_021F;
import com.usatech.layers.common.messagedata.MessageData_0220;
import com.usatech.layers.common.messagedata.MessageData_0221;
import com.usatech.layers.common.messagedata.MessageData_030A;
import com.usatech.layers.common.messagedata.MessageData_030B;
import com.usatech.layers.common.messagedata.MessageData_030C;
import com.usatech.layers.common.messagedata.MessageData_030D;
import com.usatech.layers.common.messagedata.MessageData_030E;
import com.usatech.layers.common.messagedata.MessageData_030F;
import com.usatech.layers.common.messagedata.MessageData_0310;

public class EC2JSONServlet extends GenericServlet implements EC2ServiceAPI {
	private static final long serialVersionUID = -4947746073293092306L;
	public static final String JSON_PROTOCOL = "JSON";

	private Gson gson;

	public EC2JSONServlet() {
		this.gson = new Gson();
	}

	@Override
	public void service(ServletRequest request, ServletResponse response) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest)request;
		HttpServletResponse res = (HttpServletResponse)response;
		final PrintWriter writer = res.getWriter();
		if (req.getPathInfo() == null) {
			showError(res, writer);
			writer.flush();
			return;
		}
		String function = req.getPathInfo().substring(1);

		if ("POST".equalsIgnoreCase(req.getMethod())) {
			final String reqBody = readBody(req);
			JsonParser parser = new JsonParser();
			JsonObject jsonObject = parser.parse(reqBody).getAsJsonObject();
			if (req.getQueryString() == null) {
				switch (function) {
					case "authPlain":
						writer
								.print(gson.toJson(authPlain(
										jsonObject.get("username").getAsString(),
										jsonObject.get("password").getAsString(),
										jsonObject.get("serialNumber").getAsString(),
										jsonObject.get("tranId").getAsLong(),
										jsonObject.get("amount").getAsLong(),
										jsonObject.get("cardData").getAsString(),
										jsonObject.get("entryType").getAsString(),
										jsonObject.get("attributes").getAsString())));
						break;
					case "authEncrypted":
						writer
								.print(gson.toJson(authEncrypted(
										jsonObject.get("username").getAsString(),
										jsonObject.get("password").getAsString(),
										jsonObject.get("serialNumber").getAsString(),
										jsonObject.get("tranId").getAsLong(),
										jsonObject.get("amount").getAsLong(),
										jsonObject.get("cardReaderType").getAsInt(),
										jsonObject.get("decryptedCardDataLen").getAsInt(),
										jsonObject.get("encryptedCardDataHex").getAsString(),
										jsonObject.get("ksnHex").getAsString(),
										jsonObject.get("entryType").getAsString(),
										jsonObject.get("attributes").getAsString())));
						break;
					case "batch":
						writer
								.print(gson.toJson(batch(jsonObject.get("username").getAsString(),
										jsonObject.get("password").getAsString(),
										jsonObject.get("serialNumber").getAsString(),
										jsonObject.get("tranId").getAsLong(),
										jsonObject.get("amount").getAsLong(),
										jsonObject.get("tranResult").getAsString(),
										jsonObject.get("tranDetails").getAsString(),
										jsonObject.get("attributes").getAsString())));
						break;
					case "cash":
						writer
								.print(gson.toJson(cash(jsonObject.get("username").getAsString(),
										jsonObject.get("password").getAsString(),
										jsonObject.get("serialNumber").getAsString(),
										jsonObject.get("tranId").getAsLong(),
										jsonObject.get("amount").getAsLong(),
										jsonObject.get("tranUTCTimeMs").getAsLong(),
										jsonObject.get("tranUTCOffsetMs").getAsInt(),
										jsonObject.get("tranResult").getAsString(),
										jsonObject.get("tranDetails").getAsString(),
										jsonObject.get("attributes").getAsString())));
						break;
					case "chargeEncrypted":
						writer
								.print(gson.toJson(chargeEncrypted(jsonObject.get("username").getAsString(),
										jsonObject.get("password").getAsString(),
										jsonObject.get("serialNumber").getAsString(),
										jsonObject.get("tranId").getAsLong(),
										jsonObject.get("amount").getAsLong(),
										jsonObject.get("cardReaderType").getAsInt(),
										jsonObject.get("decryptedCardDataLen").getAsInt(),
										jsonObject.get("encryptedCardDataHex").getAsString(),
										jsonObject.get("ksnHex").getAsString(),
										jsonObject.get("entryType").getAsString(),
										jsonObject.get("tranResult").getAsString(),
										jsonObject.get("tranDetails").getAsString(),
										jsonObject.get("attributes").getAsString())));
						break;
					case "chargePlain":
						writer
								.print(gson.toJson(chargePlain(jsonObject.get("username").getAsString(),
										jsonObject.get("password").getAsString(),
										jsonObject.get("serialNumber").getAsString(),
										jsonObject.get("tranId").getAsLong(),
										jsonObject.get("amount").getAsLong(),
										jsonObject.get("cardData").getAsString(),
										jsonObject.get("entryType").getAsString(),
										jsonObject.get("tranResult").getAsString(),
										jsonObject.get("tranDetails").getAsString(),
										jsonObject.get("attributes").getAsString())));
						break;
					case "uploadFile":
						File file = File.createTempFile(jsonObject.get("fileName").getAsString(), null);
						byte[] fileContent = Base64.getDecoder().decode(jsonObject.get("fileContent").getAsString());
						Files.write(Paths.get(file.getAbsolutePath()), fileContent);
						writer.print(gson.toJson(uploadFile(jsonObject.get("username").getAsString(),
								jsonObject.get("password").getAsString(),
								jsonObject.get("serialNumber").getAsString(),
								jsonObject.get("fileName").getAsString(),
								jsonObject.get("fileType").getAsInt(),
								jsonObject.get("fileSize").getAsLong(),
								new EC2DataHandler(new FileDataSource(file)),
								jsonObject.get("attributes").getAsString())));
						break;
					case "processUpdates":
						EC2ProcessUpdatesResponse processUpdatesResponse = processUpdates(jsonObject.get("username").getAsString(),
								jsonObject.get("password").getAsString(),
								jsonObject.get("serialNumber").getAsString(),
								jsonObject.get("updateStatus").getAsInt(),
								jsonObject.get("protocolVersion").getAsInt(),
								jsonObject.get("appType").getAsString(),
								jsonObject.get("appVersion").getAsString(),
								jsonObject.get("attributes").getAsString());
						JsonObject jsonResponse = new JsonObject();
						jsonResponse.addProperty("actionCode", processUpdatesResponse.getActionCode());
						jsonResponse.addProperty("attributes", processUpdatesResponse.getAttributes());
						String responseFileContent = Base64.getEncoder()
								.encodeToString(getBytesFromStream(processUpdatesResponse.getFileContent().getInputStream()));
						jsonResponse.addProperty("fileContent", responseFileContent);
						jsonResponse.addProperty("fileName", processUpdatesResponse.getFileName());
						jsonResponse.addProperty("fileSize", processUpdatesResponse.getFileSize());
						jsonResponse.addProperty("fileType", processUpdatesResponse.getFileType());
						jsonResponse.addProperty("newPassword", processUpdatesResponse.getNewPassword());
						jsonResponse.addProperty("newTranId", processUpdatesResponse.getNewTranId());
						jsonResponse.addProperty("newUsername", processUpdatesResponse.getNewUsername());
						jsonResponse.addProperty("returnCode", processUpdatesResponse.getReturnCode());
						jsonResponse.addProperty("returnMessage", processUpdatesResponse.getReturnMessage());
						jsonResponse.addProperty("serialNumber", processUpdatesResponse.getSerialNumber());
						writer.print(gson.toJson(jsonResponse));
						break;
					case "uploadDeviceInfo":
						writer.print(gson.toJson(uploadDeviceInfo(jsonObject.get("username").getAsString(),
								jsonObject.get("password").getAsString(),
								jsonObject.get("serialNumber").getAsString(),
								jsonObject.get("attributes").getAsString())));
						break;
					case "getCardIdEncrypted":
						writer.print(gson.toJson(getCardIdEncrypted(jsonObject.get("username").getAsString(),
								jsonObject.get("password").getAsString(),
								jsonObject.get("serialNumber").getAsString(),
								jsonObject.get("cardReaderType").getAsInt(),
								jsonObject.get("decryptedCardDataLen").getAsInt(),
								jsonObject.get("encryptedCardDataHex").getAsString(),
								jsonObject.get("ksnHex").getAsString(),
								jsonObject.get("entryType").getAsString(),
								jsonObject.get("attributes").getAsString())));
						break;
					case "getCardIdPlain":
						writer.print(gson.toJson(getCardIdPlain(jsonObject.get("username").getAsString(),
								jsonObject.get("password").getAsString(),
								jsonObject.get("serialNumber").getAsString(),
								jsonObject.get("cardData").getAsString(),
								jsonObject.get("entryType").getAsString(),
								jsonObject.get("attributes").getAsString())));
						break;
					case "getCardInfoEncrypted":
						writer.print(gson.toJson(getCardInfoEncrypted(jsonObject.get("username").getAsString(),
								jsonObject.get("password").getAsString(),
								jsonObject.get("serialNumber").getAsString(),
								jsonObject.get("cardReaderType").getAsInt(),
								jsonObject.get("decryptedCardDataLen").getAsInt(),
								jsonObject.get("encryptedCardDataHex").getAsString(),
								jsonObject.get("ksnHex").getAsString(),
								jsonObject.get("entryType").getAsString(),
								jsonObject.get("attributes").getAsString())));
						break;
					case "getCardInfoPlain":
						writer.print(gson.toJson(getCardInfoPlain(jsonObject.get("username").getAsString(),
								jsonObject.get("password").getAsString(),
								jsonObject.get("serialNumber").getAsString(),
								jsonObject.get("cardData").getAsString(),
								jsonObject.get("entryType").getAsString(),
								jsonObject.get("attributes").getAsString())));
						break;
					case "replenishEncrypted":
						writer.print(gson.toJson(replenishEncrypted(jsonObject.get("username").getAsString(),
								jsonObject.get("password").getAsString(),
								jsonObject.get("serialNumber").getAsString(),
								jsonObject.get("tranId").getAsLong(),
								jsonObject.get("amount").getAsLong(),
								jsonObject.get("cardReaderType").getAsInt(),
								jsonObject.get("decryptedCardDataLen").getAsInt(),
								jsonObject.get("encryptedCardDataHex").getAsString(),
								jsonObject.get("ksnHex").getAsString(),
								jsonObject.get("entryType").getAsString(),
								jsonObject.get("replenishCardId").getAsLong(),
								jsonObject.get("replenishConsumerId").getAsLong(),
								jsonObject.get("attributes").getAsString())));
						break;
					case "replenishPlain":
						writer.print(gson.toJson(replenishPlain(jsonObject.get("username").getAsString(),
								jsonObject.get("password").getAsString(),
								jsonObject.get("serialNumber").getAsString(),
								jsonObject.get("tranId").getAsLong(),
								jsonObject.get("amount").getAsLong(),
								jsonObject.get("cardData").getAsString(),
								jsonObject.get("entryType").getAsString(),
								jsonObject.get("replenishCardId").getAsLong(),
								jsonObject.get("replenishConsumerId").getAsLong(),
								jsonObject.get("attributes").getAsString())));
						break;
					case "replenishCash":
						writer.print(gson.toJson(replenishCash(jsonObject.get("username").getAsString(),
								jsonObject.get("password").getAsString(),
								jsonObject.get("serialNumber").getAsString(),
								jsonObject.get("tranId").getAsLong(),
								jsonObject.get("amount").getAsLong(),
								jsonObject.get("replenishCardId").getAsLong(),
								jsonObject.get("replenishConsumerId").getAsLong(),
								jsonObject.get("attributes").getAsString())));
						break;
					case "tokenizePlain":
						writer.print(gson.toJson(tokenizePlain(jsonObject.get("username").getAsString(),
								jsonObject.get("password").getAsString(),
								jsonObject.get("serialNumber").getAsString(),
								jsonObject.get("tranId").getAsLong(),
								jsonObject.get("cardNumber").getAsString(),
								jsonObject.get("expirationDate").getAsString(),
								jsonObject.get("securityCode").getAsString(),
								jsonObject.get("cardHolder").getAsString(),
								jsonObject.get("billingPostalCode").getAsString(),
								jsonObject.get("billingAddress").getAsString(),
								jsonObject.get("attributes").getAsString())));
						break;
					case "tokenizeEncrypted":
						writer.print(gson.toJson(tokenizeEncrypted(jsonObject.get("username").getAsString(),
								jsonObject.get("password").getAsString(),
								jsonObject.get("serialNumber").getAsString(),
								jsonObject.get("tranId").getAsLong(),
								jsonObject.get("cardReaderType").getAsInt(),
								jsonObject.get("decryptedCardDataLen").getAsInt(),
								jsonObject.get("encryptedCardDataHex").getAsString(),
								jsonObject.get("ksnHex").getAsString(),
								jsonObject.get("billingPostalCode").getAsString(),
								jsonObject.get("billingAddress").getAsString(),
								jsonObject.get("attributes").getAsString())));
						break;
					case "untokenize":
						writer.print(gson.toJson(untokenize(jsonObject.get("username").getAsString(),
								jsonObject.get("password").getAsString(),
								jsonObject.get("serialNumber").getAsString(),
								jsonObject.get("cardId").getAsLong(),
								jsonObject.get("tokenHex").getAsString(),
								jsonObject.get("attributes").getAsString())));
						break;
					case "retokenize":
						writer.print(gson.toJson(retokenize(jsonObject.get("username").getAsString(),
								jsonObject.get("password").getAsString(),
								jsonObject.get("serialNumber").getAsString(),
								jsonObject.get("cardId").getAsLong(),
								jsonObject.get("securityCode").getAsString(),
								jsonObject.get("cardHolder").getAsString(),
								jsonObject.get("billingPostalCode").getAsString(),
								jsonObject.get("billingAddress").getAsString(),
								jsonObject.get("attributes").getAsString())));
						break;
					default:
						showError(res, writer);
				}
				writer.flush();
			}
		} else if ("GET".equalsIgnoreCase(req.getMethod())) {
			if (req.getQueryString() == null) {
				ECUtils.writeUsageTerms(res, writer);
			}
		}
	}

	private void showError(HttpServletResponse res, final PrintWriter writer) {
		EC2Response response = new EC2Response();
		response.setReturnMessage("Fail. Please, refer to the documentation for the proper usage.");
		writer.print(gson.toJson(response));
	}

	@Override
	public EC2AuthResponse authEncrypted(String username, String password, String serialNumber, long tranId, long amount,
			int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String entryType,
			String attributes) {
		MessageData_020B requestData = new MessageData_020B();
		requestData.setProtocol(JSON_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setTranId(tranId);
		requestData.setAmount(amount);
		requestData.setCardReaderTypeInt(cardReaderType);
		requestData.setDecryptedCardDataLen(decryptedCardDataLen);
		requestData.setEncryptedCardDataHex(encryptedCardDataHex);
		requestData.setKsnHex(ksnHex);
		requestData.setEntryTypeString(entryType);
		requestData.setAttributes(attributes);
		MessageData_030B responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if (session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.authEncrypted(requestData, responseData, session);
				} catch (InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch (Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if (session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2AuthResponse response = new EC2AuthResponse();
		response.setActionCode(responseData.getActionCode());
		response.setApprovedAmount(responseData.getApprovedAmount());
		response.setAttributes(responseData.getAttributes());
		response.setBalanceAmount(responseData.getBalanceAmount());
		response.setCardId(responseData.getCardId());
		response.setCardType(responseData.getCardType());
		response.setConsumerId(responseData.getConsumerId());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	@Override
	public EC2AuthResponse authPlain(String username, String password, String serialNumber, long tranId, long amount,
			String cardData, String entryType, String attributes) {
		MessageData_020A requestData = new MessageData_020A();
		requestData.setProtocol(JSON_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setTranId(tranId);
		requestData.setAmount(amount);
		requestData.setCardData(cardData);
		requestData.setEntryTypeString(entryType);
		requestData.setAttributes(attributes);
		MessageData_030B responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if (session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.authPlain(requestData, responseData, session);
				} catch (InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch (Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if (session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2AuthResponse response = new EC2AuthResponse();
		response.setActionCode(responseData.getActionCode());
		response.setApprovedAmount(responseData.getApprovedAmount());
		response.setAttributes(responseData.getAttributes());
		response.setBalanceAmount(responseData.getBalanceAmount());
		response.setCardId(responseData.getCardId());
		response.setCardType(responseData.getCardType());
		response.setConsumerId(responseData.getConsumerId());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	@Override
	public EC2Response batch(String username, String password, String serialNumber, long tranId, long amount,
			String tranResult, String tranDetails, String attributes) {
		MessageData_0213 requestData = new MessageData_0213();
		requestData.setProtocol(JSON_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setTranId(tranId);
		requestData.setAmount(amount);
		requestData.setTranResult(tranResult);
		requestData.setTranDetails(tranDetails);
		requestData.setAttributes(attributes);
		MessageData_030A responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if (session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.batch(requestData, responseData, session);
				} catch (InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch (Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if (session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2Response response = new EC2Response();
		response.setActionCode(responseData.getActionCode());
		response.setAttributes(responseData.getAttributes());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	@Override
	public EC2Response cash(String username, String password, String serialNumber, long tranId, long amount,
			long tranUTCTimeMs, int tranUTCOffsetMs, String tranResult, String tranDetails, String attributes) {
		MessageData_0212 requestData = new MessageData_0212();
		requestData.setProtocol(JSON_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setTranId(tranId);
		requestData.setAmount(amount);
		requestData.setTranUTCTimeMs(tranUTCTimeMs);
		requestData.setTranUTCOffsetMs(tranUTCOffsetMs);
		requestData.setTranResult(tranResult);
		requestData.setTranDetails(tranDetails);
		requestData.setAttributes(attributes);
		MessageData_030A responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if (session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.cash(requestData, responseData, session);
				} catch (InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch (Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if (session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2Response response = new EC2Response();
		response.setActionCode(responseData.getActionCode());
		response.setAttributes(responseData.getAttributes());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	@Override
	public EC2AuthResponse chargeEncrypted(String username, String password, String serialNumber, long tranId,
			long amount, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex,
			String entryType, String tranResult, String tranDetails, String attributes) {
		MessageData_020F requestData = new MessageData_020F();
		requestData.setProtocol(JSON_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setTranId(tranId);
		requestData.setAmount(amount);
		requestData.setCardReaderTypeInt(cardReaderType);
		requestData.setDecryptedCardDataLen(decryptedCardDataLen);
		requestData.setEncryptedCardDataHex(encryptedCardDataHex);
		requestData.setKsnHex(ksnHex);
		requestData.setEntryTypeString(entryType);
		requestData.setTranResult(tranResult);
		requestData.setTranDetails(tranDetails);
		requestData.setAttributes(attributes);
		MessageData_030B responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if (session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.chargeEncrypted(requestData, responseData, session);
				} catch (InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch (Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if (session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2AuthResponse response = new EC2AuthResponse();
		response.setActionCode(responseData.getActionCode());
		response.setApprovedAmount(responseData.getApprovedAmount());
		response.setAttributes(responseData.getAttributes());
		response.setBalanceAmount(responseData.getBalanceAmount());
		response.setCardId(responseData.getCardId());
		response.setCardType(responseData.getCardType());
		response.setConsumerId(responseData.getConsumerId());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	@Override
	public EC2AuthResponse chargePlain(String username, String password, String serialNumber, long tranId, long amount,
			String cardData, String entryType, String tranResult, String tranDetails, String attributes) {
		MessageData_020E requestData = new MessageData_020E();
		requestData.setProtocol(JSON_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setTranId(tranId);
		requestData.setAmount(amount);
		requestData.setCardData(cardData);
		requestData.setEntryTypeString(entryType);
		requestData.setTranResult(tranResult);
		requestData.setTranDetails(tranDetails);
		requestData.setAttributes(attributes);
		MessageData_030B responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if (session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.chargePlain(requestData, responseData, session);
				} catch (InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch (Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if (session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2AuthResponse response = new EC2AuthResponse();
		response.setActionCode(responseData.getActionCode());
		response.setApprovedAmount(responseData.getApprovedAmount());
		response.setAttributes(responseData.getAttributes());
		response.setBalanceAmount(responseData.getBalanceAmount());
		response.setCardId(responseData.getCardId());
		response.setCardType(responseData.getCardType());
		response.setConsumerId(responseData.getConsumerId());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	@Override
	public EC2Response uploadFile(String username, String password, String serialNumber, String fileName, int fileType,
			long fileSize, EC2DataHandler fileContent, String attributes) {
		MessageData_021B requestData = new MessageData_021B();
		requestData.setProtocol(JSON_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setFileName(fileName);
		requestData.setFileType(fileType);
		requestData.setFileSize(fileSize);
		requestData.setFileContent(fileContent);
		requestData.setAttributes(attributes);
		MessageData_030A responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if (session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.uploadFile(requestData, responseData, session);
				} catch (InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch (Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if (session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2Response response = new EC2Response();
		response.setActionCode(responseData.getActionCode());
		response.setAttributes(responseData.getAttributes());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	@Override
	public EC2ProcessUpdatesResponse processUpdates(String username, String password, String serialNumber,
			int updateStatus, int protocolVersion, String appType, String appVersion, String attributes) {
		MessageData_0219 requestData = new MessageData_0219();
		requestData.setProtocol(JSON_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setUpdateStatus(updateStatus);
		requestData.setProtocolVersion(protocolVersion);
		requestData.setAppType(appType);
		requestData.setAppVersion(appVersion);
		requestData.setAttributes(attributes);
		MessageData_030E responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if (session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.processUpdates(requestData, responseData, session);
				} catch (InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch (Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if (session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				// if return code is RES_OK, then FileBlockInputStream will end the session
				if (responseData.getReturnCode() != com.usatech.ec.ECResponse.RES_OK)
					ECSessionManager.endSession(session);
			}
		}
		EC2ProcessUpdatesResponse response = new EC2ProcessUpdatesResponse();
		response.setActionCode(responseData.getActionCode());
		response.setAttributes(responseData.getAttributes());
		response.setFileContent(responseData.getFileContent());
		response.setFileName(responseData.getFileName());
		response.setFileSize(responseData.getFileSize());
		response.setFileType(responseData.getFileType());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	@Override
	public EC2Response uploadDeviceInfo(String username, String password, String serialNumber, String attributes) {
		MessageData_021A requestData = new MessageData_021A();
		requestData.setProtocol(JSON_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setAttributes(attributes);
		MessageData_030A responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if (session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.uploadDeviceInfo(requestData, responseData, session);
				} catch (InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch (Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if (session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2Response response = new EC2Response();
		response.setActionCode(responseData.getActionCode());
		response.setAttributes(responseData.getAttributes());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	@Override
	public EC2CardId getCardIdEncrypted(String username, String password, String serialNumber, int cardReaderType,
			int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String entryType, String attributes) {
		MessageData_021D requestData = new MessageData_021D();
		requestData.setProtocol(JSON_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setCardReaderTypeInt(cardReaderType);
		requestData.setDecryptedCardDataLen(decryptedCardDataLen);
		requestData.setEncryptedCardDataHex(encryptedCardDataHex);
		requestData.setKsnHex(ksnHex);
		requestData.setEntryTypeString(entryType);
		requestData.setAttributes(attributes);
		MessageData_030F responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if (session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.getCardIdEncrypted(requestData, responseData, session);
				} catch (InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch (Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if (session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2CardId response = new EC2CardId();
		response.setActionCode(responseData.getActionCode());
		response.setAttributes(responseData.getAttributes());
		response.setCardId(responseData.getCardId());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	@Override
	public EC2CardId getCardIdPlain(String username, String password, String serialNumber, String cardData,
			String entryType, String attributes) {
		MessageData_021C requestData = new MessageData_021C();
		requestData.setProtocol(JSON_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setCardData(cardData);
		requestData.setEntryTypeString(entryType);
		requestData.setAttributes(attributes);
		MessageData_030F responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if (session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.getCardIdPlain(requestData, responseData, session);
				} catch (InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch (Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if (session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2CardId response = new EC2CardId();
		response.setActionCode(responseData.getActionCode());
		response.setAttributes(responseData.getAttributes());
		response.setCardId(responseData.getCardId());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	@Override
	public EC2CardInfo getCardInfoEncrypted(String username, String password, String serialNumber, int cardReaderType,
			int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String entryType, String attributes) {
		MessageData_0215 requestData = new MessageData_0215();
		requestData.setProtocol(JSON_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setCardReaderTypeInt(cardReaderType);
		requestData.setDecryptedCardDataLen(decryptedCardDataLen);
		requestData.setEncryptedCardDataHex(encryptedCardDataHex);
		requestData.setKsnHex(ksnHex);
		requestData.setEntryTypeString(entryType);
		requestData.setAttributes(attributes);
		MessageData_030D responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if (session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.getCardInfoEncrypted(requestData, responseData, session);
				} catch (InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch (Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if (session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2CardInfo response = new EC2CardInfo();
		response.setActionCode(responseData.getActionCode());
		response.setAttributes(responseData.getAttributes());
		response.setBalanceAmount(responseData.getBalanceAmount());
		response.setCardId(responseData.getCardId());
		response.setCardType(responseData.getCardType());
		response.setConsumerId(responseData.getConsumerId());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	@Override
	public EC2CardInfo getCardInfoPlain(String username, String password, String serialNumber, String cardData,
			String entryType, String attributes) {
		MessageData_0214 requestData = new MessageData_0214();
		requestData.setProtocol(JSON_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setCardData(cardData);
		requestData.setEntryTypeString(entryType);
		requestData.setAttributes(attributes);
		MessageData_030D responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if (session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.getCardInfoPlain(requestData, responseData, session);
				} catch (InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch (Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if (session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2CardInfo response = new EC2CardInfo();
		response.setActionCode(responseData.getActionCode());
		response.setAttributes(responseData.getAttributes());
		response.setBalanceAmount(responseData.getBalanceAmount());
		response.setCardId(responseData.getCardId());
		response.setCardType(responseData.getCardType());
		response.setConsumerId(responseData.getConsumerId());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	@Override
	public EC2ReplenishResponse replenishEncrypted(String username, String password, String serialNumber, long tranId,
			long amount, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex,
			String entryType, long replenishCardId, long replenishConsumerId, String attributes) {
		MessageData_0217 requestData = new MessageData_0217();
		requestData.setProtocol(JSON_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setTranId(tranId);
		requestData.setAmount(amount);
		requestData.setCardReaderTypeInt(cardReaderType);
		requestData.setDecryptedCardDataLen(decryptedCardDataLen);
		requestData.setEncryptedCardDataHex(encryptedCardDataHex);
		requestData.setKsnHex(ksnHex);
		requestData.setEntryTypeString(entryType);
		requestData.setReplenishCardId(replenishCardId);
		requestData.setReplenishConsumerId(replenishConsumerId);
		requestData.setAttributes(attributes);
		MessageData_030C responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if (session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.replenishEncrypted(requestData, responseData, session);
				} catch (InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch (Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if (session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2ReplenishResponse response = new EC2ReplenishResponse();
		response.setActionCode(responseData.getActionCode());
		response.setApprovedAmount(responseData.getApprovedAmount());
		response.setAttributes(responseData.getAttributes());
		response.setBalanceAmount(responseData.getBalanceAmount());
		response.setCardId(responseData.getCardId());
		response.setCardType(responseData.getCardType());
		response.setConsumerId(responseData.getConsumerId());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReplenishBalanceAmount(responseData.getReplenishBalanceAmount());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	@Override
	public EC2ReplenishResponse replenishPlain(String username, String password, String serialNumber, long tranId,
			long amount, String cardData, String entryType, long replenishCardId, long replenishConsumerId,
			String attributes) {
		MessageData_0216 requestData = new MessageData_0216();
		requestData.setProtocol(JSON_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setTranId(tranId);
		requestData.setAmount(amount);
		requestData.setCardData(cardData);
		requestData.setEntryTypeString(entryType);
		requestData.setReplenishCardId(replenishCardId);
		requestData.setReplenishConsumerId(replenishConsumerId);
		requestData.setAttributes(attributes);
		MessageData_030C responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if (session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.replenishPlain(requestData, responseData, session);
				} catch (InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch (Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if (session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2ReplenishResponse response = new EC2ReplenishResponse();
		response.setActionCode(responseData.getActionCode());
		response.setApprovedAmount(responseData.getApprovedAmount());
		response.setAttributes(responseData.getAttributes());
		response.setBalanceAmount(responseData.getBalanceAmount());
		response.setCardId(responseData.getCardId());
		response.setCardType(responseData.getCardType());
		response.setConsumerId(responseData.getConsumerId());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReplenishBalanceAmount(responseData.getReplenishBalanceAmount());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	@Override
	public EC2ReplenishResponse replenishCash(String username, String password, String serialNumber, long tranId,
			long amount, long replenishCardId, long replenishConsumerId, String attributes) {
		MessageData_0218 requestData = new MessageData_0218();
		requestData.setProtocol(JSON_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setTranId(tranId);
		requestData.setAmount(amount);
		requestData.setReplenishCardId(replenishCardId);
		requestData.setReplenishConsumerId(replenishConsumerId);
		requestData.setAttributes(attributes);
		MessageData_030C responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if (session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.replenishCash(requestData, responseData, session);
				} catch (InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch (Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if (session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2ReplenishResponse response = new EC2ReplenishResponse();
		response.setActionCode(responseData.getActionCode());
		response.setApprovedAmount(responseData.getApprovedAmount());
		response.setAttributes(responseData.getAttributes());
		response.setBalanceAmount(responseData.getBalanceAmount());
		response.setCardId(responseData.getCardId());
		response.setCardType(responseData.getCardType());
		response.setConsumerId(responseData.getConsumerId());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReplenishBalanceAmount(responseData.getReplenishBalanceAmount());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	@Override
	public EC2TokenResponse tokenizePlain(String username, String password, String serialNumber, long tranId,
			String cardNumber, String expirationDate, String securityCode, String cardHolder, String billingPostalCode,
			String billingAddress, String attributes) {
		MessageData_021E requestData = new MessageData_021E();
		requestData.setProtocol(JSON_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setTranId(tranId);
		requestData.setCardNumber(cardNumber);
		requestData.setExpirationDate(expirationDate);
		requestData.setSecurityCode(securityCode);
		requestData.setCardHolder(cardHolder);
		requestData.setBillingPostalCode(billingPostalCode);
		requestData.setBillingAddress(billingAddress);
		requestData.setAttributes(attributes);
		MessageData_0310 responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if (session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.tokenizePlain(requestData, responseData, session);
				} catch (InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch (Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if (session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2TokenResponse response = new EC2TokenResponse();
		response.setActionCode(responseData.getActionCode());
		response.setAttributes(responseData.getAttributes());
		response.setCardId(responseData.getCardId());
		response.setCardType(responseData.getCardType());
		response.setConsumerId(responseData.getConsumerId());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		response.setTokenHex(responseData.getTokenHex());
		return response;
	}

	@Override
	public EC2TokenResponse tokenizeEncrypted(String username, String password, String serialNumber, long tranId,
			int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex,
			String billingPostalCode, String billingAddress, String attributes) {
		MessageData_021F requestData = new MessageData_021F();
		requestData.setProtocol(JSON_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setTranId(tranId);
		requestData.setCardReaderTypeInt(cardReaderType);
		requestData.setDecryptedCardDataLen(decryptedCardDataLen);
		requestData.setEncryptedCardDataHex(encryptedCardDataHex);
		requestData.setKsnHex(ksnHex);
		requestData.setBillingPostalCode(billingPostalCode);
		requestData.setBillingAddress(billingAddress);
		requestData.setAttributes(attributes);
		MessageData_0310 responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if (session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.tokenizeEncrypted(requestData, responseData, session);
				} catch (InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch (Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if (session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2TokenResponse response = new EC2TokenResponse();
		response.setActionCode(responseData.getActionCode());
		response.setAttributes(responseData.getAttributes());
		response.setCardId(responseData.getCardId());
		response.setCardType(responseData.getCardType());
		response.setConsumerId(responseData.getConsumerId());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		response.setTokenHex(responseData.getTokenHex());
		return response;
	}

	@Override
	public EC2Response untokenize(String username, String password, String serialNumber, long cardId, String tokenHex,
			String attributes) {
		MessageData_0220 requestData = new MessageData_0220();
		requestData.setProtocol(JSON_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setCardId(cardId);
		requestData.setTokenHex(tokenHex);
		requestData.setAttributes(attributes);
		MessageData_030A responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if (session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.untokenize(requestData, responseData, session);
				} catch (InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch (Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if (session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2Response response = new EC2Response();
		response.setActionCode(responseData.getActionCode());
		response.setAttributes(responseData.getAttributes());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		return response;
	}

	@Override
	public EC2TokenResponse retokenize(String username, String password, String serialNumber, long cardId,
			String securityCode, String cardHolder, String billingPostalCode, String billingAddress, String attributes) {
		MessageData_0221 requestData = new MessageData_0221();
		requestData.setProtocol(JSON_PROTOCOL);
		requestData.setUsername(username);
		requestData.setPassword(password);
		requestData.setSerialNumber(serialNumber);
		requestData.setCardId(cardId);
		requestData.setSecurityCode(securityCode);
		requestData.setCardHolder(cardHolder);
		requestData.setBillingPostalCode(billingPostalCode);
		requestData.setBillingAddress(billingAddress);
		requestData.setAttributes(attributes);
		MessageData_0310 responseData = requestData.createResponse();
		ECSession session = ECSessionManager.startSession(requestData, responseData);
		if (session != null) {
			try {
				try {
					requestData.validate(getTranslator());
					responseData = ECRequestHandler.retokenize(requestData, responseData, session);
				} catch (InvalidValueException e) {
					session.getLog().error("Request failed validation", e);
					responseData.setReturnMessage(e.getMessage());
				} catch (Exception e) {
					session.getLog().error("Error processing ePortConnect message", e);
					responseData.setReturnMessage("Error");
				}
				if (session.getReplyMessage() != responseData)
					session.setReplyMessage(responseData);
			} finally {
				ECSessionManager.endSession(session);
			}
		}
		EC2TokenResponse response = new EC2TokenResponse();
		response.setActionCode(responseData.getActionCode());
		response.setAttributes(responseData.getAttributes());
		response.setCardId(responseData.getCardId());
		response.setCardType(responseData.getCardType());
		response.setConsumerId(responseData.getConsumerId());
		response.setNewPassword(responseData.getNewPassword());
		response.setNewTranId(responseData.getNewTranId());
		response.setNewUsername(responseData.getNewUsername());
		response.setReturnCode(responseData.getReturnCode());
		response.setReturnMessage(responseData.getReturnMessage());
		response.setSerialNumber(responseData.getSerialNumber());
		response.setTokenHex(responseData.getTokenHex());
		return response;
	}

	private Translator getTranslator() throws ServiceException {
		return TranslatorFactory.getDefaultFactory().getTranslator(null, null);
	}

	private String readBody(HttpServletRequest request) throws IOException {
		StringBuilder ret = new StringBuilder();
		BufferedReader reader = null;
		char[] buffer = new char[128];
		int bytesRead = -1;
		try {
			reader = request.getReader();
			while ((bytesRead = reader.read(buffer)) > 0) {
				ret.append(buffer, 0, bytesRead);
			}
		} finally {
			reader.close();
		}
		return ret.toString();
	}
	
	private byte[] getBytesFromStream(InputStream input) throws IOException {
		int readBytes;
		byte[] buffer = new byte[4096];
		ByteArrayOutputStream data = new ByteArrayOutputStream();
		while((readBytes = input.read(buffer, 0, buffer.length)) > 0) {
			data.write(buffer, 0, readBytes);
		}
		data.flush();
		return data.toByteArray();
	}

}
