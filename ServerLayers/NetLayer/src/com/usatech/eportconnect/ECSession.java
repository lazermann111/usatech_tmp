package com.usatech.eportconnect;

import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import javax.servlet.http.HttpServletRequest;

import org.apache.axis2.context.MessageContext;
import org.apache.axis2.transport.http.HTTPConstants;

import simple.app.Publisher;
import simple.app.ServiceException;
import simple.event.RunnableListener;
import simple.io.BinaryStream;
import simple.io.ByteInput;
import simple.io.Log;
import simple.io.logging.AbstractPrefixedLog;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.translator.Translator;

import com.caucho.services.server.ServiceContext;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainStep;
import com.usatech.layers.common.DeviceInfo;
import com.usatech.layers.common.constants.AuthResultCode;
import com.usatech.layers.common.constants.CredentialResult;
import com.usatech.layers.common.constants.FileType;
import com.usatech.layers.common.constants.SessionCloseReason;
import com.usatech.layers.common.messagedata.MessageData;
import com.usatech.layers.common.messagedata.MessageDirection;
import com.usatech.layers.common.util.StringHelper;
import com.usatech.networklayer.app.InboundFileTransfer;
import com.usatech.networklayer.app.NetLayerUtils;
import com.usatech.networklayer.app.NetworkLayerMessage;
import com.usatech.networklayer.app.OutboundFileTransfer;

public class ECSession implements NetworkLayerMessage {
	public static final char CALL_IN_STATUS_SUCCESS = 'S';
	public static final char CALL_IN_STATUS_FAILURE = 'U';
	
	public static final char CALL_IN_TYPE_AUTH = 'A';
	public static final char CALL_IN_TYPE_BATCH = 'B';
	public static final char CALL_IN_TYPE_COMBO = 'C';
	public static final char CALL_IN_TYPE_UNKNOWN = 'U';
	
	private static final Log log = Log.getLog();
	protected static final String GLOBAL_SESSION_CODE_PREFIX = "E:" + ManagementFactory.getRuntimeMXBean().getName() + ':' + Long.toHexString(System.currentTimeMillis()).toUpperCase() + ':';
	protected static final AtomicLong sessionIDGenerator = new AtomicLong(Double.doubleToLongBits(Math.random()));
	
	protected final ReentrantLock lock = new ReentrantLock();
	protected final Condition signal = lock.newCondition();
	
	protected final String deviceSerialNumber;
	protected final long sessionId;
	protected final String globalSessionCode;
	protected final long sessionStartTime;
	protected long sessionEndTime;
	protected DeviceInfo deviceInfo;
	protected final Log sessionLog;
	protected MessageData requestMessage = null;
	protected ArrayList<MessageData> requestMessages = new ArrayList<MessageData>();
	protected Map<Integer, ArrayList<MessageData>> replyMessages = new HashMap<Integer, ArrayList<MessageData>>();
	protected MessageData replyMessage = null;
	protected MessageData secondReplyMessage = null;
	protected Long lastCommandId = null;
	protected String remoteAddress = "Unknown";
	protected int remotePort = -1;
	protected char callInStatus = CALL_IN_STATUS_FAILURE;
	protected char callInType = CALL_IN_TYPE_UNKNOWN;
	protected int messagesReceived = 0;
	protected int messagesSent = 0;
	protected long bytesReceived = 0;
	protected long bytesSent = 0;
	protected int saleCount = 0;
	protected Number authAmount;
    protected AuthResultCode authResultCode;
    protected Character paymentType;
    protected OutboundFileTransfer outboundFileTransfer;
    protected boolean locked = false;
	protected String newUsername = null;
	protected byte[] newPasswordHash = null;
    protected boolean ended = false;
	protected CredentialResult credentialResult;
	
	public ECSession(String protocol, String deviceSerialNumber) {
		this.deviceSerialNumber = deviceSerialNumber;
		sessionId = sessionIDGenerator.getAndIncrement();
		globalSessionCode = GLOBAL_SESSION_CODE_PREFIX + Long.toHexString(sessionId).toUpperCase();
		sessionStartTime = System.currentTimeMillis();
		sessionEndTime = sessionStartTime;
		
		HttpServletRequest request = null;
		if (ECAxisServlet.SOAP_PROTOCOL.equalsIgnoreCase(protocol)) {
			MessageContext context = MessageContext.getCurrentMessageContext();
			if(context != null && context.getProperty(HTTPConstants.MC_HTTP_SERVLETREQUEST) != null)
				request = (HttpServletRequest) context.getProperty(HTTPConstants.MC_HTTP_SERVLETREQUEST);
		} else if (ECHessianServlet.HESSIAN_PROTOCOL.equalsIgnoreCase(protocol))
			request = (HttpServletRequest) ServiceContext.getContextRequest();
		
		if (request != null) {
			// get remote IP from the header set by Sophos WAF
			remoteAddress = request.getHeader("X-Forwarded-For");
			if (StringHelper.isBlank(remoteAddress))
				remoteAddress = request.getRemoteAddr();
			remotePort = request.getRemotePort();
		}
		
		final String sessionLogPrefix = new StringBuilder("[").append(globalSessionCode).append(" from ").append(remoteAddress).append(":").append(remotePort).append("]: ").toString();
		this.sessionLog = new AbstractPrefixedLog(log) {
			/**
			 * @see simple.io.logging.AbstractPrefixedLog#prefixMessage(java.lang.Object)
			 */
			@Override
			protected Object prefixMessage(Object message) {
				return sessionLogPrefix + message;
			}
		};
		ECSessionManager.registerSession(this);
	}
	
	public String getRemoteAddress() {
		return remoteAddress;
	}

	public int getRemotePort() {
		return remotePort;
	}

	public char getCallInStatus() {
		return callInStatus;
	}

	public void setCallInStatus(char callInStatus) {
		this.callInStatus = callInStatus;
	}

	public char getCallInType() {
		return callInType;
	}

	public void setCallInType(char callInType) {
		this.callInType = callInType;
	}

	public Number getAuthAmount() {
		return authAmount;
	}

	public void setAuthAmount(Number authAmount) {
		this.authAmount = authAmount;
	}

	public AuthResultCode getAuthResultCode() {
		return authResultCode;
	}

	public void setAuthResultCode(AuthResultCode authResultCode) {
		this.authResultCode = authResultCode;
	}

	public Character getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(Character paymentType) {
		this.paymentType = paymentType;
	}

	public int getMessagesReceived() {
		return messagesReceived;
	}

	public void setMessagesReceived(int messagesReceived) {
		this.messagesReceived = messagesReceived;
	}

	public int getMessagesSent() {
		return messagesSent;
	}

	public void setMessagesSent(int messagesSent) {
		this.messagesSent = messagesSent;
	}

	public long getBytesReceived() {
		return bytesReceived;
	}

	public void setBytesReceived(long bytesReceived) {
		this.bytesReceived = bytesReceived;
	}

	public long getBytesSent() {
		return bytesSent;
	}

	public void setBytesSent(long bytesSent) {
		this.bytesSent = bytesSent;
	}

	public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	public void lock() {
		lock.lock();
		locked = true;
	}
	
	public boolean await(long timeMillis) throws InterruptedException {
		return signal.await(timeMillis, TimeUnit.MILLISECONDS);
	}
	
	public void signal() {
		signal.signal();
	}
	
	public void unlock() {
		lock.unlock();
		locked = false;
	}

	public String getDeviceSerialNumber() {
		return deviceSerialNumber;
	}

	public long getSessionStartTime() {
		return sessionStartTime;
	}

	public long getSessionEndTime() {
		return sessionEndTime;
	}

	public void setSessionEndTime(long sessionEndTime) {
		this.sessionEndTime = sessionEndTime;
	}

	public MessageData getRequestMessage() {
		return requestMessage;
	}

	public void setRequestMessage(MessageData requestMessage) {
		this.requestMessage = requestMessage;
		if (requestMessage != null) {
			requestMessages.add(requestMessage);
			messagesReceived++;
		}
	}

	public ArrayList<MessageData> getRequestMessages() {
		return requestMessages;
	}

	public MessageData getReplyMessage() {
		return replyMessage;
	}

	public void setReplyMessage(MessageData replyMessage, int replyMessageOrdinal) {
		if (replyMessage != null) {
			long currentTime = System.currentTimeMillis();
			replyMessage.setDirection(MessageDirection.SERVER_TO_CLIENT);
			if (replyMessage.getMessageEndTime() == 0)
				replyMessage.setMessageEndTime(currentTime);
			int key = requestMessages.size() - 1;
			if (key > -1) {
				MessageData requestMessage = requestMessages.get(key);
				if (requestMessage.getMessageEndTime() == 0)
					requestMessage.setMessageEndTime(currentTime);
				if (replyMessages.get(key) == null)
					replyMessages.put(key, new ArrayList<MessageData>());			
				replyMessages.get(key).add(replyMessage);
			}
			messagesSent++;
			if(getLog().isInfoEnabled())
				getLog().info("Sending " + replyMessage // + " to " + getRemoteAddressString()
						+ " [" + (currentTime - replyMessage.getMessageStartTime()) + " ms]");
		}
		if (replyMessageOrdinal > 1)
			this.secondReplyMessage = replyMessage;
		else
			this.replyMessage = replyMessage;
	}
	
	public void setReplyMessage(MessageData replyMessage) {
		setReplyMessage(replyMessage, 1);
	}
	
	public MessageData getSecondReplyMessage() {
		return secondReplyMessage;
	}

	public void setSecondReplyMessage(MessageData secondReplyMessage) {
		setReplyMessage(secondReplyMessage, 2);
	}

	public Map<Integer, ArrayList<MessageData>> getReplyMessages() {
		return replyMessages;
	}
	
	public void end() {
		if (!ended) {
			sessionEndTime = System.currentTimeMillis();
			ended = true;
		}
		if (isLocked())
			unlock();
	}

	public boolean isEnded() {
		return ended;
	}

	public void setEnded(boolean ended) {
		this.ended = ended;
	}

	public long getSessionId() {
		return sessionId;
	}
	

	public DeviceInfo getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(DeviceInfo deviceInfo) {
		this.deviceInfo = deviceInfo;
	}
	
	public Log getSessionLog() {
		return sessionLog;
	}

	public String getGlobalSessionCode() {
		return globalSessionCode;
	}

	public int getSaleCount() {
		return saleCount;
	}

	@Override
	public byte getProtocolId() {
		return 0;
	}

	@Override
	public String getDeviceName() {
		return deviceInfo.getDeviceName();
	}

	@Override
	public MessageData getData() {
		return getRequestMessage();
	}

	@Override
	public void sendReply(MessageData... replyData) throws ServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getReplyCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Publisher<ByteInput> getPublisher() {
		return ECSessionManager.getPublisher();
	}

	@Override
	public Log getLog() {
		return sessionLog;
	}

	@Override
	public Translator getTranslator() {
		return ECSessionManager.getTranslator(getDeviceInfo() != null ? getDeviceInfo().getLocale() : null);
	}

	@Override
	public Object getSessionAttribute(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setResultAttribute(String name, Object value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setSessionAttribute(String name, Object value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setNextCommandId(long commandId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Resource addFileTransfer(boolean useResource, long fileTransferId, long pendingCommandId, int fileGroup, String fileName, int fileTypeId, int filePacketSize, long fileSize)
			throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getServerTime() {
		return sessionStartTime;
	}

	@Override
	public DeviceInfo getNewDeviceInfo(String deviceName) throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void enqueueExpectingReply(String queueKey, Map<String, Object> processAttributes, String preProcessQueueKey, Map<String, Object> preProcessAttributes,
			Map<String, String> preProcessResultAttributes) throws ServiceException {
		setReplyMessage(null);
		setSecondReplyMessage(null);
		ECSessionManager.enqueue(queueKey, this, getRequestMessage(), true, processAttributes, preProcessQueueKey, preProcessAttributes, preProcessResultAttributes);
	}

	@Override
	public void enqueueNoReply(String queueKey, Map<String, Object> processAttributes, String preProcessQueueKey, Map<String, Object> preProcessAttributes,
			Map<String, String> preProcessResultAttributes) throws ServiceException {
		ECSessionManager.enqueue(queueKey, this, getRequestMessage(), false, processAttributes, preProcessQueueKey, preProcessAttributes, preProcessResultAttributes);
	}

	@Override
	public void enqueue(MessageChain messageChain, boolean inline) throws ServiceException {
		if(inline) {
			setReplyMessage(null);
			setSecondReplyMessage(null);
		}
		ECSessionManager.enqueue(messageChain, inline);
	}

	@Override
	public MessageChainStep constructReplyStep(MessageChain messageChain, MessageData reply, SessionCloseReason endSessionReason, Long lastCommandId) throws ServiceException {
		return NetLayerUtils.constructReplyStep(messageChain, getReplyQueueKey(), reply, endSessionReason, lastCommandId, null, sessionId, ECSessionManager.getByteBuffer());
	}

	@Override
	public MessageChainStep constructReplyTemplateStep(MessageChain messageChain, MessageData reply, SessionCloseReason endSessionReason, Long lastCommandId) throws ServiceException {
		return NetLayerUtils.constructReplyTemplateStep(messageChain, getReplyQueueKey(), reply, endSessionReason, lastCommandId, null, sessionId, ECSessionManager.getByteBuffer());
	}

	@Override
	public void closeSession(SessionCloseReason sessionCloseReason) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void registerInboundFileTransfer(long fileTransferId, InboundFileTransfer fileTransfer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public InboundFileTransfer getInboundFileTransfer(long fileTransferId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean removeInboundFileTransfer(long fileTransferId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int cancelAllFileTransfers() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void registerOutboundFileTransfer(long fileTransferId, OutboundFileTransfer fileTransfer) {
		this.outboundFileTransfer = fileTransfer;
		
	}

	@Override
	public OutboundFileTransfer getOutboundFileTransfer(long fileTransferId) {
		return outboundFileTransfer;
	}

	@Override
	public boolean removeOutboundFileTransfer(long fileTransferId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getLayerName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getLayerHost() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getLayerPort() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getLayerDesc() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResourceFolder getResourceFolder() throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long getLastCommandId() {
		return lastCommandId;
	}

	@Override
	public void setLastCommandId(Long lastCommandId) {
		this.lastCommandId = lastCommandId;
		
	}

	@Override
	public String getReplyQueueKey() {
		return ECSessionManager.getReplyQueueKey();
	}

	@Override
	public boolean isInitSession() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean sendReply(RunnableListener onComplete, MessageData... replyData) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean sendReplyAndClose(SessionCloseReason sessionCloseReason, MessageData... replyData) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean permitReply(int requestId) {
		return true;
	}

	@Override
	public void receivedFileTransfer(FileType fileType, BinaryStream content) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void sentFileTransfer(FileType fileType, BinaryStream content) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void sentAuthResponse(AuthResultCode authResultCode, Number authAmount, char paymentType) {
		this.paymentType = paymentType;
		this.authAmount = authAmount;
		this.authResultCode = authResultCode;
	}

	@Override
	public void processingComplete() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void incrementSaleCount() {
		saleCount++;
		
	}

	@Override
	public void incrementFileCount() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void incrementEventCount() {
		// TODO Auto-generated method stub
		
	}

	public String getNewUsername() {
		return newUsername;
	}

	public void setNewUsername(String newUsername) {
		this.newUsername = newUsername;
	}

	public byte[] getNewPasswordHash() {
		return newPasswordHash;
	}

	public void setNewPasswordHash(byte[] newPasswordHash) {
		this.newPasswordHash = newPasswordHash;
	}

	public boolean isInternal() {
		return getCredentialResult() == CredentialResult.INTERNAL;
	}

	public CredentialResult getCredentialResult() {
		return credentialResult;
	}

	public void setCredentialResult(CredentialResult credentialResult) {
		this.credentialResult = credentialResult;
	}
}
