package com.usatech.networklayer.test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Calendar;

import org.junit.Test;

import com.caucho.hessian.client.HessianProxyFactory;
import com.usatech.ec.ECAuthResponse;
import com.usatech.ec.ECByteArrayDataSource;
import com.usatech.ec.ECDataHandler;
import com.usatech.ec.ECProcessUpdatesResponse;
import com.usatech.ec.ECResponse;
import com.usatech.ec.ECServiceAPI;

public class EportConnectHessianTest {
	public static final String EPORT_CONNECT_HESSIAN_URL = "https://localhost:9443/hessian/ec";
	protected static HessianProxyFactory hessianProxyFactory = null;	
	protected static ECServiceAPI ePortConnect = null;
	
	static {		
		System.setProperty("app.servicename", "ePortConnectHessianTest");
		System.setProperty("javax.net.ssl.trustStore", "../LayersCommon/conf/net/truststore.ts");
		System.setProperty("javax.net.ssl.trustStorePassword", "usatech");
	}
	
	// java -cp bin com.usatech.networklayer.test.EportConnectHessianTest
	public static void main(String[] args) throws Exception {
		processMain(args, EPORT_CONNECT_HESSIAN_URL, EportConnectHessianTest.class.getName());
	}
	
	public static void processMain(String[] args, String url, String className) throws Exception {
		if (args.length < 1) {
			System.out.print(EportConnectSOAPTest.getUsage(url, className));
			return;
		}
		String function = args[0];
		if ("authV3".equals(function)) {
			if (args.length < 9)
				System.out.print(EportConnectSOAPTest.getUsage(url, className));
			else
				testAuthV3(args[1], args[2], args[3], args[4], getTranId(args[5]), Long.valueOf(args[6]), args[7], args[8]);
		} else if ("authV3_1".equals(function)) {
			if (args.length < 12)
				System.out.print(EportConnectSOAPTest.getUsage(url, className));
			else
				testAuthV3_1(args[1], args[2], args[3], args[4], getTranId(args[5]), Long.valueOf(args[6]), Integer.valueOf(args[7]), Integer.valueOf(args[8]), args[9], args[10], args[11]);
		} else if ("batchV3".equals(function)) {
			if (args.length < 9)
				System.out.print(EportConnectSOAPTest.getUsage(url, className));
			else
				testBatchV3(args[1], args[2], args[3], args[4], getTranId(args[5]), Long.valueOf(args[6]), args[7], args[8]);
		} else if ("cashV3".equals(function)) {
			if (args.length < 11)
				System.out.print(EportConnectSOAPTest.getUsage(url, className));
			else
				testCashV3(args[1], args[2], args[3], args[4], getTranId(args[5]), Long.valueOf(args[6]), Long.valueOf(args[7]), Integer.valueOf(args[8]), args[9], args[10]);
		} else if ("chargeV3".equals(function)) {
			if (args.length < 11)
				System.out.print(EportConnectSOAPTest.getUsage(url, className));
			else
				testChargeV3(args[1], args[2], args[3], args[4], getTranId(args[5]), Long.valueOf(args[6]), args[7], args[8], args[9], args[10]);
		} else if ("chargeV3_1".equals(function)) {
			if (args.length < 14)
				System.out.print(EportConnectSOAPTest.getUsage(url, className));
			else
				testChargeV3_1(args[1], args[2], args[3], args[4], getTranId(args[5]), Long.valueOf(args[6]), Integer.valueOf(args[7]), Integer.valueOf(args[8]), args[9], args[10], args[11], args[12], args[13]);
		} else if ("uploadFile".equals(function)) {
			if (args.length < 7)
				System.out.print(EportConnectSOAPTest.getUsage(url, className));
			else
				testUploadFile(args[1], args[2], args[3], args[4], args[5], Integer.valueOf(args[6]));
		} else if ("processUpdates".equals(function)) {
			if (args.length < 5)
				System.out.print(EportConnectSOAPTest.getUsage(url, className));
			else
				testProcessUpdates(args[1], args[2], args[3], args[4], Integer.valueOf(args[5]));
		} else
			System.out.print(EportConnectSOAPTest.getUsage(url, className));
	}
	
	protected static long getTranId(String arg) {
		if(arg.equals("-"))
			return System.currentTimeMillis() / 1000L;
		return Long.valueOf(arg);
	}

	protected static String getBasicResponseMessage(ECResponse response) {
		String message = "Response returnCode: " + response.getReturnCode() + ", returnMessage: " + response.getReturnMessage();
		if (response.getNewUsername().length() > 0)
			message += ", newUsername: " + response.getNewUsername();
		if (response.getNewPassword().length() > 0)
			message += ", newPassword: " + response.getNewPassword();
		return message;
	}

	protected static void testAuthV3(String url, String username, String password, String serialNumber, long tranId, 
			long amount, String cardData, String cardType) throws Exception {
		if (hessianProxyFactory == null)
			hessianProxyFactory = new HessianProxyFactory();
		if (ePortConnect == null)
			ePortConnect = (ECServiceAPI) hessianProxyFactory.create(ECServiceAPI.class, url);
		ECAuthResponse response = ePortConnect.authV3(username, password, serialNumber, 
				tranId, amount, cardData, cardType);
		if (response == null)
			throw new Exception("Received null response");
		else {
			String message = getBasicResponseMessage(response) 
					+ ", approvedAmount: " + response.getApprovedAmount();
			if (response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED || response.getReturnCode() == EportConnectSOAPTest.RES_PARTIALLY_APPROVED)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testAuthV3() throws Exception {
		testAuthV3(EPORT_CONNECT_HESSIAN_URL, EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, EportConnectSOAPTest.TRANSACTION_ID, 
				100, EportConnectSOAPTest.CARD_DATA, EportConnectSOAPTest.CARD_TYPE_SWIPED);
	}
	
	protected static void testBatchV3(String url, String username, String password, String serialNumber, long tranId, 
			long amount, String tranResult, String tranDetails) throws Exception {
		if (hessianProxyFactory == null)
			hessianProxyFactory = new HessianProxyFactory();
		if (ePortConnect == null)
			ePortConnect = (ECServiceAPI) hessianProxyFactory.create(ECServiceAPI.class, url);
		ECResponse response = ePortConnect.batchV3(username, password, serialNumber, tranId, amount, tranResult, tranDetails);
		if (response == null)
			throw new Exception("Received null response");
		else {
			String message = getBasicResponseMessage(response);
			if (response.getReturnCode() == EportConnectSOAPTest.RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testBatchV3() throws Exception {
		testBatchV3(EPORT_CONNECT_HESSIAN_URL, EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, EportConnectSOAPTest.TRANSACTION_ID, 
				60, EportConnectSOAPTest.TRAN_RESULT_SUCCESS, "A0|302|10|1|1|305|10|5|1");
	}
	
	protected static void testAuthV3_1(String url, String username, String password, String serialNumber, long tranId, long amount, 
			int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String cardType) throws Exception {
		if (hessianProxyFactory == null)
			hessianProxyFactory = new HessianProxyFactory();
		if (ePortConnect == null)
			ePortConnect = (ECServiceAPI) hessianProxyFactory.create(ECServiceAPI.class, url);
		ECAuthResponse response = ePortConnect.authV3_1(username, password, serialNumber, tranId, amount, 
				cardReaderType, decryptedCardDataLen, encryptedCardDataHex, ksnHex, cardType);
		if (response == null)
			throw new Exception("Received null response");
		else {
			String message = getBasicResponseMessage(response) 
					+ ", approvedAmount: " + response.getApprovedAmount();
			if (response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED || response.getReturnCode() == EportConnectSOAPTest.RES_PARTIALLY_APPROVED)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testAuthV3_1() throws Exception {
		testAuthV3_1(EPORT_CONNECT_HESSIAN_URL, EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, 
				EportConnectSOAPTest.TRANSACTION_ID + 1, 100, EportConnectSOAPTest.CARD_READER_MAGTEK_MAGNESAFE, 37, 
				EportConnectSOAPTest.ENCRYPTED_CARD_DATA, EportConnectSOAPTest.KEY_SERIAL_NUMBER, EportConnectSOAPTest.CARD_TYPE_SWIPED);
	}
	
	@Test
	public void testBatchV3_1() throws Exception {
		testBatchV3(EPORT_CONNECT_HESSIAN_URL, EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, 
				EportConnectSOAPTest.TRANSACTION_ID + 1, 50, EportConnectSOAPTest.TRAN_RESULT_SUCCESS, "A0|302|10|1|1|305|10|4|1");
	}
	
	protected static void testCashV3(String url, String username, String password, String serialNumber, long tranId, 
			long amount, long tranUTCTimeMs, int tranUTCOffsetMs, String tranResult, String tranDetails) throws Exception {
		if (hessianProxyFactory == null)
			hessianProxyFactory = new HessianProxyFactory();
		if (ePortConnect == null)
			ePortConnect = (ECServiceAPI) hessianProxyFactory.create(ECServiceAPI.class, url);
		ECResponse response = ePortConnect.cashV3(username, password, serialNumber, tranId, 
				amount, tranUTCTimeMs, tranUTCOffsetMs, tranResult, tranDetails);
		if (response == null)
			throw new Exception("Received null response");
		else {
			String message = getBasicResponseMessage(response);
			if (response.getReturnCode() == EportConnectSOAPTest.RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testCashV3() throws Exception {
		Calendar calendar = Calendar.getInstance();
		testCashV3(EPORT_CONNECT_HESSIAN_URL, EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, 
				EportConnectSOAPTest.TRANSACTION_ID + 2, 80, calendar.getTimeInMillis(), calendar.getTimeZone().getOffset(calendar.getTimeInMillis()), 
				EportConnectSOAPTest.TRAN_RESULT_SUCCESS, "A0|302|10|3|1|305|10|5|1");
	}

	protected static void testChargeV3(String url, String username, String password, String serialNumber, long tranId, 
			long amount, String cardData, String cardType, String tranResult, String tranDetails) throws Exception {
		if (hessianProxyFactory == null)
			hessianProxyFactory = new HessianProxyFactory();
		if (ePortConnect == null)
			ePortConnect = (ECServiceAPI) hessianProxyFactory.create(ECServiceAPI.class, url);
		ECAuthResponse response = ePortConnect.chargeV3(username, password, serialNumber, tranId, 
				amount, cardData, cardType, tranResult, tranDetails);
		if (response == null)
			throw new Exception("Received null response");
		else {
			String message = getBasicResponseMessage(response) 
					+ ", approvedAmount: " + response.getApprovedAmount();
			if (response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED || response.getReturnCode() == EportConnectSOAPTest.RES_PARTIALLY_APPROVED)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testChargeV3() throws Exception {
		testChargeV3(EPORT_CONNECT_HESSIAN_URL, EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, 
				EportConnectSOAPTest.TRANSACTION_ID + 3, 110, EportConnectSOAPTest.CARD_DATA, EportConnectSOAPTest.CARD_TYPE_SWIPED, 
				EportConnectSOAPTest.TRAN_RESULT_SUCCESS, "A0|302|10|6|1|305|10|5|1");
	}
	
	protected static void testChargeV3_1(String url, String username, String password, String serialNumber, long tranId, 
			long amount, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex,
			String cardType, String tranResult, String tranDetails) throws Exception {
		if (hessianProxyFactory == null)
			hessianProxyFactory = new HessianProxyFactory();
		if (ePortConnect == null)
			ePortConnect = (ECServiceAPI) hessianProxyFactory.create(ECServiceAPI.class, url);
		ECAuthResponse response = ePortConnect.chargeV3_1(username, password, serialNumber, tranId, 
				amount, cardReaderType, decryptedCardDataLen, encryptedCardDataHex, ksnHex,
				cardType, tranResult, tranDetails);
		if (response == null)
			throw new Exception("Received null response");
		else {
			String message = getBasicResponseMessage(response) 
					+ ", approvedAmount: " + response.getApprovedAmount();
			if (response.getReturnCode() == EportConnectSOAPTest.RES_APPROVED || response.getReturnCode() == EportConnectSOAPTest.RES_PARTIALLY_APPROVED)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}	

	@Test
	public void testChargeV3_1() throws Exception {
		testChargeV3_1(EPORT_CONNECT_HESSIAN_URL, EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, 
				EportConnectSOAPTest.TRANSACTION_ID + 4, 120, EportConnectSOAPTest.CARD_READER_MAGTEK_MAGNESAFE, 37, 
				EportConnectSOAPTest.ENCRYPTED_CARD_DATA, EportConnectSOAPTest.KEY_SERIAL_NUMBER, EportConnectSOAPTest.CARD_TYPE_SWIPED, 
				EportConnectSOAPTest.TRAN_RESULT_SUCCESS, "A0|302|10|7|1|305|10|5|1");
	}
	
	protected static void testUploadFile(String url, String username, String password, String serialNumber, String fileName, int fileType) throws Exception {		
		if (hessianProxyFactory == null)
			hessianProxyFactory = new HessianProxyFactory();
		if (ePortConnect == null)
			ePortConnect = (ECServiceAPI) hessianProxyFactory.create(ECServiceAPI.class, url);		
		File file = new File(fileName);
		
	    InputStream in = new FileInputStream(file);   
	    ByteArrayOutputStream out = new ByteArrayOutputStream();   
	    byte[] bytes = new byte[1024];
	    int bytesRead;
	    while ((bytesRead = in.read(bytes)) > 0)   
	      out.write(bytes, 0, bytesRead);
		
		ECDataHandler dh = new ECDataHandler(new ECByteArrayDataSource(out.toByteArray(), "application/octet-stream"));
		ECResponse response = ePortConnect.uploadFile(username, password, serialNumber, fileName, fileType, file.length(), dh);
		if (response == null)
			throw new Exception("Received null response");
		else {
			String message = getBasicResponseMessage(response);
			if (response.getReturnCode() == EportConnectSOAPTest.RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testUploadFile() throws Exception {
		testUploadFile(EPORT_CONNECT_HESSIAN_URL, EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, 
				EportConnectSOAPTest.FILE_NAME, EportConnectSOAPTest.FILE_TYPE_KIOSK_GENERIC_FILE);
	}
	
	protected static void testProcessUpdates(String url, String username, String password, String serialNumber, int updateStatus) throws Exception {		
		if (hessianProxyFactory == null)
			hessianProxyFactory = new HessianProxyFactory();
		if (ePortConnect == null)
			ePortConnect = (ECServiceAPI) hessianProxyFactory.create(ECServiceAPI.class, url);
		ECProcessUpdatesResponse response = ePortConnect.processUpdates(username, password, serialNumber, updateStatus);
		if (response == null)
			throw new Exception("Received null response");
		else {
			String message = getBasicResponseMessage(response)
					+ ", fileName: " + response.getFileName() + ", fileType: " + response.getFileType() + ", fileSize: " + response.getFileSize();
			if (response.getReturnCode() == EportConnectSOAPTest.RES_OK || response.getReturnCode() == EportConnectSOAPTest.RES_OK_NO_UPDATE
					|| response.getReturnCode() == EportConnectSOAPTest.RES_OK_UPLOAD_FILE) {
				if (response.getReturnCode() == EportConnectSOAPTest.RES_OK && response.getFileContent() != null) {
					ECDataHandler dh = response.getFileContent();
					File file = new File(response.getFileName());
					FileOutputStream os = new FileOutputStream(file);
					try {
						dh.writeTo(os);
						os.flush();
					} finally {
						os.close();
					}
				} else if (response.getReturnCode() == EportConnectSOAPTest.RES_OK_UPLOAD_FILE && response.getFileName() != null && response.getFileName().length() > 0)
					testUploadFile(url, username, password, serialNumber, response.getFileName(), EportConnectSOAPTest.FILE_TYPE_KIOSK_GENERIC_FILE);
				System.out.println(message);
			} else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testProcessUpdates() throws Exception {
		testProcessUpdates(EPORT_CONNECT_HESSIAN_URL, EportConnectSOAPTest.USERNAME, EportConnectSOAPTest.PASSWORD, EportConnectSOAPTest.SERIAL_NUMBER, EportConnectSOAPTest.UPDATE_STATUS_NORMAL);
	}
}
