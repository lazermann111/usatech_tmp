import java.nio.ByteBuffer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.usatech.networklayer.protocol.Layer3Transmission;

import simple.io.ByteArrayUtils;
import simple.io.Log;
import simple.test.UnitTest;


public class Layer3Test extends UnitTest {
	public static String msg1="313538502C5334512D53454B464A394C5F2F234124385D35415837265D313E3B4D2B2F3C242D335A43334E5438435326574623562A2E454A324334322D5A564E3F53265E2F4E562F243749584F2F54303942285E535220400A";
	public static String msg_noise1="010203313538502C5334512D53454B464A394C5F2F234124385D35415837265D313E3B4D2B2F3C242D335A43334E5438435326574623562A2E454A324334322D5A564E3F53265E2F4E562F243749584F2F54303942285E535220400A";
	public static String msg_noise2="010203313538502C5334512D53454B464A394C5F2F234124385D35415837265D313E3B4D2B2F3C242D335A43334E5438435326574623562A2E454A324334322D5A564E3F53265E2F4E562F243749584F2F54303942285E53818283ff5220400A";
	@Before
	public void setUp() throws Exception {
		setupLog();
	}
	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
	
	@Test
	public void testLayer3Read() throws Exception {
		Layer3Transmission trans=new Layer3Transmission();
		byte[] messageBytes=ByteArrayUtils.fromHex(msg1);
		int pos=50;
		ByteBuffer bb=ByteBuffer.allocate(pos);
		bb.put(messageBytes, 0, pos);
		bb.flip();
		log.info(trans.prepareRead(bb, log));
		bb = ByteBuffer.allocate(messageBytes.length-pos);
		bb.put(messageBytes, pos, messageBytes.length-pos);
		bb.flip();
		log.info(trans.prepareRead(bb, log));

	}
	
	@Test
	public void testLayer3ReadNoise1() throws Exception {
		Layer3Transmission trans=new Layer3Transmission();
		byte[] messageBytes=ByteArrayUtils.fromHex(msg_noise1);
		int pos=50;
		ByteBuffer bb=ByteBuffer.allocate(pos);
		bb.put(messageBytes, 0, pos);
		bb.flip();
		log.info(trans.prepareRead(bb, log));
		bb = ByteBuffer.allocate(messageBytes.length-pos);
		bb.put(messageBytes, pos, messageBytes.length-pos);
		bb.flip();
		log.info(trans.prepareRead(bb, log));

	}
	
	@Test
	public void testLayer3ReadNoise2() throws Exception {
		Layer3Transmission trans=new Layer3Transmission();
		byte[] messageBytes=ByteArrayUtils.fromHex(msg_noise2);
		int pos=50;
		ByteBuffer bb=ByteBuffer.allocate(pos);
		bb.put(messageBytes, 0, pos);
		bb.flip();
		
		log.info(trans.prepareRead(bb, log));
		bb = ByteBuffer.allocate(messageBytes.length-pos);
		bb.put(messageBytes, pos, messageBytes.length-pos);
		bb.flip();
		log.info(trans.prepareRead(bb, log));

	}
	
}
