package com.usat.logslayer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import simple.app.AbstractService;
import simple.app.ServiceException;
import simple.io.Log;

public class LogLayerService extends AbstractService {

	private static final Log logger = Log.getLog();

	private static final String SERVICE_NAME = "LogsLayer";

	private String agentHosts;

	private String outputFolder;

	private ScheduledExecutorService executorService;

	public LogLayerService(String serviceName) {
		super(SERVICE_NAME);
	}

	@Override
	public int startThreads(int numThreads) throws ServiceException {
		List<InetSocketAddress> agents = parseAgentHosts(agentHosts);
		if (executorService == null) {
			executorService = Executors.newScheduledThreadPool(agents.size());

			for (int i = 0; i < agents.size(); i++) {
				executorService.scheduleWithFixedDelay(new LogFileReceiverThread(agents.get(i), outputFolder), 0, 5,
						TimeUnit.MINUTES);
			}
		}
		return agents.size();
	}

	private List<InetSocketAddress> parseAgentHosts(String agentHosts) {
		List<InetSocketAddress> ret = new ArrayList<>();
		for (String host : agentHosts.split(",")) {
			String[] separatedHostAndPort = host.split(":");
			if (separatedHostAndPort.length != 2) {
				throw new IllegalArgumentException(
						"Unexpected log agent host name; Expected format: <IPv4>:<port>; Actual: " + host);
			}

			ret.add(new InetSocketAddress(separatedHostAndPort[0], Integer.parseInt(separatedHostAndPort[1])));
		}
		return ret;
	}

	private static final class LogFileReceiverThread implements Runnable {

		private InetSocketAddress agentAddress;

		private String outputFolder;

		public LogFileReceiverThread(InetSocketAddress agentAddress, String outputFolder) {
			this.agentAddress = agentAddress;
			this.outputFolder = outputFolder;
		}

		@Override
		public void run() {
			SocketChannel client = null;
			try {
				client = tryToConnect();
				if (client == null) {
					return;
				}
				internalRun(client);

			} catch (Exception exception) {
				logger.error("Failed to receiving log file from log agent: " + agentAddress, exception);
			} finally {
				safeClose(client);
			}
		}

		private void safeClose(SocketChannel client) {
			try {
				if (client != null) {
					client.close();
				}
			} catch (IOException ioException) {
				logger.error("Failed to close connection to client " + client, ioException);
			}
		}

		private void internalRun(SocketChannel client) throws Exception {
			Selector selector = Selector.open();
			client.register(selector, SelectionKey.OP_READ);
			while (client.isConnected()) {
				int countEvnt = selector.select();
				if (countEvnt == 0) {
					logger.info("Not found keys. Closing client socket:" + client);
					return;
				}
				Set<SelectionKey> readyKeys = selector.selectedKeys();
				for (Iterator<SelectionKey> iterator = readyKeys.iterator(); iterator.hasNext();) {
					SelectionKey key = iterator.next();
					iterator.remove();
					if (key.isConnectable()) {
						((SocketChannel) key.channel()).finishConnect();
						continue;
					}

					if (!key.isValid()) {
						continue;
					}

					if (key.isReadable()) {
						boolean status = persistsInputFile(key);
						if (!status) {
							logger.info("Not found new files. Closing client socket:" + client);
							key.channel().close();
							key.cancel();
							selector.close();
							return;
						}
					}

				}
			}
		}

		private boolean persistsInputFile(SelectionKey key) throws Exception {
			File file = null;
			String fileName = null;
			try {
				SocketChannel client = (SocketChannel) key.channel();
				logger.debug("Received new input data from client: " + client);
				int headerSize = LogAgentService.HEADER_SIZE;
				ByteBuffer byteBuffer = ByteBuffer.allocate(headerSize);
				while (headerSize > 0) {
					int recivedBytesCount = client.read(byteBuffer);
					if (recivedBytesCount < 0) {
						logger.warn("Incorrect input file header");
						return false;
					}
					headerSize = headerSize - recivedBytesCount;
				}

				byteBuffer.position(0);
				long fileSize = byteBuffer.getLong();
				if (fileSize == 0) {
					return false;
				}
				CharsetDecoder charsetEncoder = Charset.forName("UTF-8").newDecoder();
				fileName = charsetEncoder.decode(byteBuffer).toString().trim();

				logger.info("Starting resolve file: " + fileName.trim() + "; file size: " + fileSize);
				file = saveFileBody(fileName, client, fileSize);
				renameFileTo(file, fileName);
				return true;
			} catch (Exception e) {
				logger.warn("Failed save file to disk", e);
				return false;
			}
		}

		private File saveFileBody(String fileName, SocketChannel client, long fileSize)
				throws IOException, FileNotFoundException {
			FileOutputStream fileInputStream = null;
			FileChannel fileChannel = null;
			File file;
			try {
				file = createFile(fileName);
				fileInputStream = new FileOutputStream(file);
				fileChannel = fileInputStream.getChannel();

				int recivedBytesTotalCount = 0;
				while (fileSize > 0) {
					long recivedBytesCount = fileChannel.transferFrom(client, recivedBytesTotalCount, fileSize);
					if (recivedBytesCount < 0) {
						logger.warn("Incorrect input file header");
						return null;
					}

					recivedBytesTotalCount += recivedBytesCount;
					fileSize -= recivedBytesCount;
				}
			} finally {
				if (fileChannel != null) {
					fileChannel.close();
				}
				if (fileInputStream != null) {
					fileInputStream.close();
				}
			}
			return file;
		}

		private void renameFileTo(File logFile, String fileName) throws IOException {
			Files.move(FileSystems.getDefault().getPath(logFile.getAbsolutePath()),
					FileSystems.getDefault().getPath(logFile.getParent() + "/" + fileName),
					StandardCopyOption.REPLACE_EXISTING);
		}

		private File createFile(String fileName) throws IOException {
			File file = new File(outputFolder + "/" + fileName + ".downloading");
			if (!file.exists()) {
				file.createNewFile();
			}
			return file;
		}

		private SocketChannel tryToConnect() throws IOException {
			try {
				SocketChannel client = SocketChannel.open(agentAddress);
				client.configureBlocking(false);
				while (!client.finishConnect()) {
					logger.info("Connection is being established...");
				}
				return client;
			} catch (ConnectException e) {
				logger.warn("Failed connect to agent " + agentAddress);
				return null;
			}
		}

	}

	@Override
	public int restartThreads(long timeout) throws ServiceException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int pauseThreads() throws ServiceException {
		throw new UnsupportedOperationException("Pause operation is not supported");
	}

	@Override
	public int unpauseThreads() throws ServiceException {
		throw new UnsupportedOperationException("Unpause operation is not supported");
	}

	@Override
	public int stopAllThreads(boolean force, long timeout) throws ServiceException {
		executorService.shutdown();
		try {
			if (!executorService.awaitTermination(timeout, TimeUnit.SECONDS)) {
				executorService.shutdownNow();
				if (!executorService.awaitTermination(timeout, TimeUnit.SECONDS)) {
					logger.warn("Failed to shutdown threads");
				}
			}
		} catch (InterruptedException ie) {
			executorService.shutdownNow();
			Thread.currentThread().interrupt();
		}
		return 0;
	}

	@Override
	public int stopThreads(int numThreads, boolean force, long timeout) throws ServiceException {
		return stopAllThreads(force, timeout);
	}

	@Override
	public int getNumThreads() {
		return 0;
	}

	public String getAgentHosts() {
		return agentHosts;
	}

	public void setAgentHosts(String agentHosts) {
		this.agentHosts = agentHosts;
	}

	public String getOutputFolder() {
		return outputFolder;
	}

	public void setOutputFolder(String outputFolder) {
		this.outputFolder = outputFolder;
	}

}
