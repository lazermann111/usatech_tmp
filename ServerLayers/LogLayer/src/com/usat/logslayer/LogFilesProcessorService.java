package com.usat.logslayer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

import org.apache.commons.lang.StringEscapeUtils;
import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;

import simple.app.AbstractService;
import simple.app.ServiceException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;

public class LogFilesProcessorService extends AbstractService {

	private static final Log logger = Log.getLog();

	private static final String SERVICE_NAME = "LogsFilesProcessor";

	private static final int PARSER_THREAD_COUNT = 3;

	private static final int DB_PERSISTER_THREAD_COUNT = 3;

	public static final int MAX_CNT_FILE_FOR_PROCESSING = 500;

	private static BlockingQueue<File> queue = null;

	public static final File DUMMY = new File("");
	
	private ScheduledExecutorService executorService;

	private String logFilesFolderPath;

	private String copyFilesFolderPath;

	private String tmpFolderPath;
	
	private static int hoursInPartition;

	public LogFilesProcessorService(String serviceName) {
		super(SERVICE_NAME);
	}

	@Override
	public int startThreads(int numThreads) throws ServiceException {

		queue = new LinkedBlockingQueue<>(MAX_CNT_FILE_FOR_PROCESSING);

		if (executorService == null) {
			executorService = Executors.newScheduledThreadPool(PARSER_THREAD_COUNT + DB_PERSISTER_THREAD_COUNT + 1);
		}

		executorService.scheduleWithFixedDelay(new FileEnumerationTask(queue, new File(copyFilesFolderPath)), 0, 2,
				TimeUnit.SECONDS);

		for (int i = 0; i < PARSER_THREAD_COUNT; i++) {
			executorService.scheduleWithFixedDelay(
					new LogFileProcessorThread(new File(logFilesFolderPath), new File(copyFilesFolderPath), hoursInPartition), 0, 2,
					TimeUnit.SECONDS);
		}

		for (int i = 0; i < DB_PERSISTER_THREAD_COUNT; i++) {
			executorService.scheduleWithFixedDelay(new CopyFileLoader(queue), 0, 2, TimeUnit.SECONDS);
		}

		return PARSER_THREAD_COUNT + DB_PERSISTER_THREAD_COUNT;
	}

	public class FileEnumerationTask implements Runnable {
		/**
		 * Constructs a FileEnumerationTask.
		 * 
		 * @param queue
		 *            the blocking queue to which the enumerated files are added
		 * @param startingDirectory
		 *            the directory in which to start the enumeration
		 */
		public FileEnumerationTask(BlockingQueue<File> queue, File startingDirectory) {
			this.queue = queue;
			this.startingDirectory = startingDirectory;
		}

		public void run() {
			try {
				enumerate(startingDirectory);
				if (!queue.contains(DUMMY)) {
					queue.put(DUMMY);
				}
			} catch (InterruptedException e) {
				logger.warn("Thread was interrupted", e);
			}
		}

		/**
		 * Enumerates all files in a given directory
		 * 
		 * @param directory
		 *            the directory in which to start
		 */
		public void enumerate(File directory) throws InterruptedException {
			File[] files = directory.listFiles();
			for (File file : files) {
				if ((!file.isDirectory()) && (file.getName().endsWith(".cpy")) && (!queue.contains(file))) {
					queue.offer(file, 10, TimeUnit.SECONDS);
				}
				;

			}
		}

		private BlockingQueue<File> queue;
		private File startingDirectory;
	}

	private static final class CopyFileLoader implements Runnable {

		private static final Log logger = Log.getLog();

		private BlockingQueue<File> queue;

		public CopyFileLoader(BlockingQueue<File> queue) {
			this.queue = queue;
		}

		@Override
		public void run() {
			File copyFile = null;
			try {
				copyFile = queue.poll(1, TimeUnit.MINUTES);
				if (copyFile == null) {
					logger.debug("Queue of sql file for processing is empty. waiting new files");
					return;
				};
			} catch (InterruptedException e) {
				logger.warn("Thread was interrupted", e);
			}
			processFile(copyFile);

		}

		private void processFile(File copyFile) {
			Connection connection = null;
			try {
				if (!copyFile.getName().endsWith(".cpy")) {
					return;
				}

				if (!copyFile.exists()) {
					return;
				}

				copyFile = tryToMarkFileAsLoading(copyFile);
				if (copyFile == null) {
					return;
				}

				connection = getConnection();
				String partitionName = getPartitionName(connection, copyFile.getName());
				BaseConnection baseConnection = connection.unwrap(BaseConnection.class);

				loadCopyFile(partitionName, copyFile, baseConnection);
				Files.deleteIfExists(copyFile.toPath());
			} catch (Exception e) {
				logger.error("Failed to processing file " + copyFile, e);
				markFileAsError(copyFile);
			} finally {
				if (connection != null) {
					safeCloseConnection(connection);
				}
			}
		}

		private void safeCloseConnection(Connection connection) {
			try {
				connection.close();
			} catch (Exception exception) {
				logger.warn("Failed to close connection", exception);
			}
		}

		private synchronized File tryToMarkFileAsLoading(File copyFile) {
			try {
				if (!copyFile.exists()) {
					return null;
				}
				Path path = Files.move(FileSystems.getDefault().getPath(copyFile.getAbsolutePath()),
						FileSystems.getDefault().getPath(copyFile.getAbsolutePath() + ".loading"),
						StandardCopyOption.REPLACE_EXISTING);
				return path.toFile();
			} catch (IOException ioException) {
				logger.warn("Failed to mark file " + copyFile + " as loading", ioException);
				return null;
			}
		}

		private void loadCopyFile(String partitionName, File copyFile, BaseConnection baseConnection)
				throws SQLException, IOException {
			InputStream in = null;
			try {
				long startTime = System.currentTimeMillis();
				logger.info("Loading copy file:" + copyFile);
				in = new FileInputStream(copyFile);
				CopyManager copyManager = new CopyManager(baseConnection);
				copyManager.copyIn("COPY main." + partitionName + " FROM STDIN", in);
				baseConnection.commit();
				logger.info("Finished load copy file: " + copyFile + "; Filesize: " + copyFile.length()
						+ "; Time spent: " + TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - startTime));
			} finally {
				if (in != null) {
					in.close();
				}
			}
		}

		private synchronized String getPartitionName(Connection connection, String copyFilename)
				throws SQLException, DataLayerException {
			Map<String, Object> param = new HashMap<>();
			param.put("logTs", extractLogTs(copyFilename));
			param.put("Hours", hoursInPartition);
			Results result = DataLayerMgr.executeQuery(connection, "GET_PARTITION_NAME", param);
			connection.commit();
			if (!result.next()) {
				throw new RuntimeException("Failed to resolve partition name for file:" + copyFilename);
			}
			return "" + result.get("fcTable");
		}

		private Date extractLogTs(String copyFilename) {
			int idxStartLogTs = copyFilename.lastIndexOf('_');
			if (idxStartLogTs < 0) {
				throw new RuntimeException("Incorrect copy file name; Actual: " + copyFilename
						+ "; Expected: <log file name>_<log ts>.cpy");
			}

			int idxEndLogTs = copyFilename.lastIndexOf(".cpy");
			if (idxStartLogTs < 0 || idxEndLogTs <= idxStartLogTs) {
				throw new RuntimeException("Incorrect copy file name; Actual: " + copyFilename
						+ "; Expected: <log file name>_<log ts>.cpy");
			}

			String strLogTs = copyFilename.substring(idxStartLogTs + 1, idxEndLogTs);
			return parseLogTs(strLogTs);
		}

		private Date parseLogTs(String strLogTs) {
			try {
				long time = Long.parseLong(strLogTs);
				Date logTs = new Date(time); 
				Calendar cal = Calendar.getInstance();
				cal.setTime(logTs);
				cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY) * hoursInPartition);
				return cal.getTime();
			} catch (Exception exception) {
				throw new RuntimeException("Unexpected log ts; Actual: " + strLogTs + "; Expected: Long");
			}
		}

		private Connection getConnection() throws SQLException, DataLayerException {
			return DataLayerMgr.getConnectionForCall("GET_PARTITION_NAME");
		}

		private void markFileAsError(File copyFile) {
			try {
				Files.move(FileSystems.getDefault().getPath(copyFile.getAbsolutePath()),
						FileSystems.getDefault().getPath(copyFile.getAbsolutePath() + ".error"),
						StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException ioException) {
				logger.warn("Failed to mark file " + copyFile + " as error", ioException);
			}
		}

	}

	private static final class LogFileProcessorThread implements Runnable {

		private static final String LOG_TS_FORMAT = "yyyy-MM-dd HH:mm:ss,SSS";

		private static final Pattern GLOBAL_SESSION_PATTERN = Pattern
				.compile("[AE]:[0-9]{1,}@[a-z12\\.]{1,}:[0-9A-F]{1,}:[0-9A-F]{1,}");
		
		private static final ThreadLocal<SimpleDateFormat> dateFormatter = new ThreadLocal<SimpleDateFormat>() {
			@Override
			protected SimpleDateFormat initialValue() {
				return new SimpleDateFormat(LOG_TS_FORMAT);
			}
		};

		private static final Log logger = Log.getLog();

		private File logFilesFolder;

		private File copyFilesFolder;
		
		private int hoursInPartition;

		public LogFileProcessorThread(File logFilesFolderPath, File copyFilesFolder, int hoursInPartition) {
			this.logFilesFolder = logFilesFolderPath;
			this.copyFilesFolder = copyFilesFolder;
			this.hoursInPartition = hoursInPartition;
		}

		@Override
		public void run() {
			try {
				if (logFilesFolder.length() == 0)
					logger.debug("No files to process");
				
				for (File logFile : logFilesFolder.listFiles()) {
					File extractedFile = null;
					try {
						if (!logFile.getName().endsWith(".gz")) {
							continue;
						}

						extractedFile = createEmptyFileForUnzip(logFile);
						if (extractedFile == null) {
							continue;
						}

						unzipLogFileTo(extractedFile, logFile);
						processLogFiles(extractedFile);
					} catch (FileNotFoundException fileNotFoundException) {
						logger.info("File " + logFile + " not found");
					} catch (InterruptedException e) {
						logger.warn("Thread was interrupted", e);
					} catch (Exception exception) {
						logger.error("Failed to process file: " + logFile.getAbsolutePath(), exception);
						markFileAsError(logFile);
					} finally {
						if (extractedFile != null) {
							Files.deleteIfExists(logFile.toPath());
							Files.deleteIfExists(extractedFile.toPath());
						}
					}
				}
			} catch (Exception e) {
				logger.error("Failed to process file", e);
			}
		}

		private void markFileAsError(File logFile) throws IOException {
			Files.move(FileSystems.getDefault().getPath(logFile.getAbsolutePath()),
					FileSystems.getDefault().getPath(logFile.getAbsolutePath() + ".error"),
					StandardCopyOption.REPLACE_EXISTING);
		}

		private void processLogFiles(File logFile) throws Exception {
			Scanner in = null;
			Map<String, OutputStreamWriter> cachedCopyFiles = new HashMap<>();
			try {
				logger.info("Starting parse file: " + logFile.getAbsolutePath());
				in = new Scanner(logFile);

				long startParsingTime = System.currentTimeMillis();

				CopyRecordBuilder recordBuilder = CopyRecordBuilder.createBuilder(logFile.getName());
				Date currentLogTs = null;
				while (in.hasNextLine()) {
					String logRecord = in.nextLine();

					if (logRecord.trim().isEmpty()) {
						continue;
					}

					if (!hasLogTs(logRecord) && !recordBuilder.isEmpty()) {
						recordBuilder.appendMessageLine(logRecord);
						continue;
					}

					if (currentLogTs != null && !recordBuilder.isEmpty()) {
						String copyFilename = buildCopyFilename(logFile.getName(), currentLogTs);
						putCopyLineToFile(recordBuilder.toCopyRecord(), copyFilename, cachedCopyFiles);
						recordBuilder.clear();

					}

					currentLogTs = extractLogTs(logRecord);
					recordBuilder.appendLogTs(currentLogTs);
					recordBuilder.appendMessageLine(extractMessage(logRecord));
					recordBuilder.appendGlobalSession(extractGlobalSession(logRecord));
					recordBuilder.appendDeviceCd(extractDeviceIdentifier(logRecord));
				}

				if (!recordBuilder.isEmpty()) {
					String copyFilename = buildCopyFilename(logFile.getName(), currentLogTs);
					putCopyLineToFile(recordBuilder.toCopyRecord(), copyFilename, cachedCopyFiles);
				}

				logger.info("Create copy files based on " + logFile.getAbsolutePath() + " log file; Time spent:  "
						+ TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - startParsingTime) + " seconds");
			} finally {
				if (in != null) {
					in.close();
				}

				closeCopeFile(cachedCopyFiles);
			}
		}

		private void closeCopeFile(Map<String, OutputStreamWriter> copyFiles) throws IOException {
			for (String copyFilename : copyFiles.keySet()) {
				closeCopyFile(copyFiles.get(copyFilename));
				markCopyFileAsReadyForLoad(copyFilesFolder.getAbsolutePath() + "/" + copyFilename);

			}
		}

		public void markCopyFileAsReadyForLoad(String pathToCopyFile) throws IOException {
			Files.move(FileSystems.getDefault().getPath(pathToCopyFile),
					FileSystems.getDefault().getPath(pathToCopyFile.replaceFirst(".creating", "")),
					StandardCopyOption.REPLACE_EXISTING);

		}

		private void closeCopyFile(OutputStreamWriter copyFile) throws IOException {
			copyFile.flush();
			copyFile.close();
		}

		private FileOutputStream createCopyFile(String copyFilename) throws IOException {
			File copyFile = new File(copyFilesFolder.getAbsolutePath() + "/" + copyFilename);
			if (copyFile.exists()) {
				throw new RuntimeException("Copy file " + copyFilename + " is already exists");
			}
			if (!copyFile.createNewFile()) {
				throw new RuntimeException("Failed to create copy file " + copyFilename);
			}
			return new FileOutputStream(copyFile);
		}

		private String buildCopyFilename(String logFilename, Date logTs) {
			String prefix = logFilename.substring(0, logFilename.indexOf(".gz"));
			return prefix + "_" + getCopyFileTime(logTs) + ".cpy.creating";
		}

		private long getCopyFileTime(Date logTs) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(logTs);
			cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY) / hoursInPartition);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			return cal.getTime().getTime();

		}

		private String extractMessage(String logRecord) {
			int dateIdx = logRecord.indexOf(" [");
			if (dateIdx <= 0) {
				return logRecord;
			}

			if (dateIdx != LOG_TS_FORMAT.length()) {
				return logRecord;
			}

			return logRecord.substring(dateIdx, logRecord.length());
		}

		private String extractGlobalSession(String logRecord) {
			String globalSessionCode = extractField(logRecord, "globalSessionCode=");
			if (globalSessionCode != null)
				return extractField(logRecord, "globalSessionCode=");
			else
				return lookupGlobalSession(logRecord);
		}

		private String lookupGlobalSession(String logRecord) {
			Matcher m = GLOBAL_SESSION_PATTERN.matcher(logRecord);
			return m.find() ? m.group() : null;
		}

		private String extractDeviceIdentifier(String logRecord) {
			String ret = extractField(logRecord, "deviceName=");
			if (ret == null) {
				ret = extractField(logRecord, "deviceName=");
			}
			return extractField(logRecord, "deviceSerialCd=");
		}

		private String extractField(String logRecord, String fieldName) {
			int globalSessionStartIdx = logRecord.indexOf(fieldName);
			if (globalSessionStartIdx <= 0) {
				return null;
			}
			globalSessionStartIdx += fieldName.length();

			int globalSessionEndIdx = logRecord.indexOf(",", globalSessionStartIdx);
			if (globalSessionEndIdx <= 0) {
				globalSessionEndIdx = logRecord.indexOf(" ", globalSessionStartIdx);
			}

			if (globalSessionEndIdx <= globalSessionStartIdx) {
				return null;
			}

			return logRecord.substring(globalSessionStartIdx, globalSessionEndIdx);
		}

		private Date extractLogTs(String logRecord) {
			int dateIdx = logRecord.indexOf(" [");
			if (dateIdx <= 0) {
				return null;
			}

			if (dateIdx != LOG_TS_FORMAT.length()) {
				return null;
			}

			String dateStr = logRecord.substring(0, dateIdx).trim();
			try {
				Date ret = dateFormatter.get().parse(dateStr.trim());
				return ret;
			} catch (ParseException parseException) {
				logger.debug("Failed to extract log rescord date; Actual format: '" + dateStr + "'; Expected format:  "
						+ LOG_TS_FORMAT, parseException);
				return null;
			}
		}

		private void putCopyLineToFile(StringBuilder recordBuilder, String copyFilename,
				Map<String, OutputStreamWriter> cachedCopyFiles) throws IOException {
			OutputStreamWriter copyFileStream = cachedCopyFiles.get(copyFilename);
			if (copyFileStream != null) {
				copyFileStream.append("\n" + recordBuilder.toString().replaceAll("\\x00", ""));
				return;
			}
			copyFileStream = new OutputStreamWriter(createCopyFile(copyFilename), StandardCharsets.UTF_8);
			cachedCopyFiles.put(copyFilename, copyFileStream);
			copyFileStream.append(recordBuilder.toString().replaceAll("\\x00", ""));
		}

		private boolean hasLogTs(String logRecord) {
			int dateIdx = logRecord.indexOf(" [");
			if (dateIdx <= 0) {
				return false;
			}

			if (dateIdx != LOG_TS_FORMAT.length()) {
				return false;
			}

			return true;
		}

		private static final class CopyRecordBuilder {

			private static final String COPY_FILE_TS_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";

			private static final ThreadLocal<SimpleDateFormat> dateFormatter = new ThreadLocal<SimpleDateFormat>() {
				@Override
				protected SimpleDateFormat initialValue() {
					return new SimpleDateFormat(COPY_FILE_TS_FORMAT);
				}
			};

			private String date;

			private String hostname;

			private String appCd;

			private String globalSession;

			private String deviceCd;

			private StringBuilder messageTxt = new StringBuilder();

			private CopyRecordBuilder(String hostname, String appCd) {
				this.hostname = hostname;
				this.appCd = appCd;
			}

			public void appendLogTs(Date logTs) {
				date = dateFormatter.get().format(logTs);
			}

			public void appendMessageLine(String message) {
				if (messageTxt.length() > 0) {
					messageTxt.append("\\n");
				}
				messageTxt.append(message.trim().replaceAll("\t", " ").replaceAll("\\\\.", "."));
			}

			public void appendGlobalSession(String session) {
				this.globalSession = session;
			}

			public void appendDeviceCd(String deviceCd) {
				this.deviceCd = deviceCd;
			}

			public boolean isEmpty() {
				return date == null || messageTxt.length() == 0;
			}

			public void clear() {
				date = null;
				messageTxt = new StringBuilder();
			}

			public StringBuilder toCopyRecord() {
				StringBuilder ret = new StringBuilder();
				ret.append(date).append("\t").append(deviceCd != null ? deviceCd : "\\N").append("\t")
						.append(globalSession != null ? globalSession : "\\N").append("\t").append(hostname)
						.append("\t").append(appCd).append("\t")
						.append(StringEscapeUtils.escapeCsv(messageTxt.toString()));
				return ret;
			}

			public static CopyRecordBuilder createBuilder(String fileName) {
				return new CopyRecordBuilder(extractHostname(fileName), extractAppCd(fileName));
			}

			private static String extractHostname(String name) {
				int hostNameIdx = name.indexOf("_");
				if (hostNameIdx <= 0) {
					return "UNKNOW";
				}
				return name.substring(0, hostNameIdx);
			}

			// devapr11.usatech.com_AppLayerService.20170201-071538.gz
			private static String extractAppCd(String name) {
				int hostNameIdx = name.indexOf("_");
				if (hostNameIdx <= 1) {
					return "UNKNOW";
				}

				String AppCd = name.substring(hostNameIdx + 1);
				int appCdIdx = AppCd.indexOf(".");
				if (appCdIdx <= 0) {
					return "UNKNOW";
				}

				return AppCd.substring(0, appCdIdx);
			}

		}

		private void unzipLogFileTo(File distanation, File logFile) throws FileNotFoundException, IOException {
			InputStream logFileZipped = null;
			try {
				logFileZipped = new FileInputStream(logFile);
				unzip(distanation, logFileZipped);
			} finally {
				if (logFileZipped != null) {
					logFileZipped.close();
				}
			}
		}

		private synchronized File createEmptyFileForUnzip(File logFile) throws IOException {
			if (!logFile.exists()) {
				return null;
			}

			File unzippedLogFolder = new File(logFile.getAbsolutePath() + ".extracted");
			if (unzippedLogFolder.exists()) {
				return null;
			}

			logger.debug("Try to create empty file for extract log: " + logFile.getName());
			if (!unzippedLogFolder.createNewFile()) {
				return null;
			}
			return unzippedLogFolder;
		}

		public static void unzip(File extractedFile, InputStream in) throws IOException {
			byte[] buffer = new byte[1024];
			GZIPInputStream gzip = null;
			FileOutputStream out = null;

			try {
				gzip = new GZIPInputStream(in);
				out = new FileOutputStream(extractedFile);
				int len;
				while ((len = gzip.read(buffer)) > 0) {
					out.write(buffer, 0, len);
				}
			} finally {
				if (gzip != null) {
					gzip.close();
				}

				if (out != null) {
					safeClose(out);
				}
			}
		}

		private static void safeClose(FileOutputStream out) {
			try {
				if (out != null) {
					out.close();
				}
			} catch (IOException ioException) {
				logger.error("Failed to close file ", ioException);
			}
		}

	}

	@Override
	public int restartThreads(long timeout) throws ServiceException {
		return 0;
	}

	@Override
	public int pauseThreads() throws ServiceException {
		return 0;
	}

	@Override
	public int unpauseThreads() throws ServiceException {
		return 0;
	}

	@Override
	public int stopAllThreads(boolean force, long timeout) throws ServiceException {
		executorService.shutdown();
		try {
			if (!executorService.awaitTermination(timeout, TimeUnit.SECONDS)) {
				executorService.shutdownNow();
				if (!executorService.awaitTermination(timeout, TimeUnit.SECONDS)) {
					logger.warn("Failed to shutdown threads");
				}
			}
		} catch (InterruptedException ie) {
			executorService.shutdownNow();
			Thread.currentThread().interrupt();
		}
		return 0;
	}

	@Override
	public int stopThreads(int numThreads, boolean force, long timeout) throws ServiceException {
		return 0;
	}

	@Override
	public int getNumThreads() {
		return 0;
	}

	public String getTmpFolderPath() {
		return tmpFolderPath;
	}

	public void setTmpFolderPath(String tmpFolderPath) {
		this.tmpFolderPath = tmpFolderPath;
	}

	public String getLogFilesFolderPath() {
		return logFilesFolderPath;
	}

	public void setLogFilesFolderPath(String logFilesFolderPath) {
		this.logFilesFolderPath = logFilesFolderPath;
	}

	public String getCopyFilesFolderPath() {
		return copyFilesFolderPath;
	}

	public void setCopyFilesFolderPath(String copyFilesFolderPath) {
		this.copyFilesFolderPath = copyFilesFolderPath;
	}

	public int getHoursInPartition() {
		return hoursInPartition;
	}

	public void setHoursInPartition(int hoursInPartition) {
		LogFilesProcessorService.hoursInPartition = hoursInPartition;
	}

}
