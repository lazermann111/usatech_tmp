package com.usat.logslayer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import simple.app.AbstractService;
import simple.app.ServiceException;
import simple.io.Log;
import simple.text.StringUtils;

public class LogAgentService extends AbstractService {
	private static final int THREAD_COUNT = 1;
	public static final int HEADER_SIZE = 512;
	public static final int MAX_LOF_FILE_IN_SENDING_STATE = 200;

	private static final Log logger = Log.getLog();
	private static final String SERVICE_NAME = "LogsAgent";
	private static final String DEFAULT_BASE_PATH = "/opt/USAT/";

	private ScheduledExecutorService executorService;
	private String applicationNames;
	private int port;
	private int daysAgoLogs;

	public LogAgentService() {
		super(SERVICE_NAME);
	}

	@Override
	public int getNumThreads() {
		return THREAD_COUNT;
	}

	@Override
	public int startThreads(int numThreads) throws ServiceException {
		if (numThreads > 1) {
			logger.warn("Log agent service is working in only single thread mode.");
		}

		List<File> logFolders = resolveLogFolders(applicationNames);
		BlockingQueue<File> queue = new LinkedBlockingQueue<>(MAX_LOF_FILE_IN_SENDING_STATE);
		FileStatusManager fileStatusManager = new FileStatusManager(new File("logFileStatus.data"));

		if (executorService == null) {
			executorService = Executors.newScheduledThreadPool(logFolders.size() + 1);
		}

		executorService.scheduleWithFixedDelay(
				new LogsAgentNetworkThread(getNewThreadName(), queue, getPort(), fileStatusManager), 0, 2,
				TimeUnit.SECONDS);
		runLogFileListener(executorService, logFolders, queue, fileStatusManager, daysAgoLogs);

		return THREAD_COUNT;
	}

	private void runLogFileListener(ScheduledExecutorService executorService, List<File> logFiles,
			BlockingQueue<File> logFilesToSending, FileStatusManager fileStatusManager, int daysAgoLogs) {
		for (File logFolder : logFiles) {
			executorService.scheduleWithFixedDelay(
					new LogsListenerThread(logFolder, logFilesToSending, fileStatusManager, daysAgoLogs), 0, 5,
					TimeUnit.MINUTES);
		}
	}

	private static final class LogsListenerThread implements Runnable {

		private static final Log logger = Log.getLog();

		private File logFolder;

		private BlockingQueue<File> logFilesToSending;

		private FileStatusManager fileStatusManager;

		private int daysAgoLogs;

		public LogsListenerThread(File logFolder, BlockingQueue<File> logFilesToSending,
				FileStatusManager fileStatusManager, int daysAgoLogs) {
			this.logFolder = logFolder;
			this.logFilesToSending = logFilesToSending;
			this.fileStatusManager = fileStatusManager;
			this.daysAgoLogs = daysAgoLogs;
		}

		@Override
		public void run() {
			try {
				for (File file : logFolder.listFiles()) {
					processLogFile(file);
				}
			} catch (Exception exception) {
				logger.error(exception.getMessage());
			}
		}

		private void processLogFile(File file) throws InterruptedException, ParseException {
			if (!file.isFile()) {
				return;
			}

			if (!isReadyForSending(file.getAbsolutePath())) {
				return;
			}

			File readyForSendingFile = markFileAsReadyForSending(file);
			if (readyForSendingFile == null) {
				logger.info("Skipped file " + file.getAbsolutePath() + " for sending");
				return;
			}

			if (!logFilesToSending.offer(readyForSendingFile, 1, TimeUnit.MINUTES)) {
				throw new RuntimeException("Failed process the log file: " + readyForSendingFile.getAbsolutePath()
						+ "; Reason: queue is full");
			}

			logger.info("Put log file " + readyForSendingFile.getAbsolutePath()
					+ " to queue for sending to central log server");
		}

		private boolean isReadyForSending(String fileName) throws ParseException {
			if (!fileName.endsWith(".gz")) {
				return false;
			}

			if (!validFileDate(fileName)) {
				return false;
			}

			if (fileStatusManager.resolveFileStatus(fileName) != null) {
				return false;
			}

			return true;
		}

		private boolean validFileDate(String fileName) throws ParseException {
			Calendar cal = Calendar.getInstance();
			Date currDate = cal.getTime();
			cal.setTime(extractLogTs(fileName));
			return (currDate.getTime() - cal.getTime().getTime()) / 1000 / 60 / 60 / 24 <= daysAgoLogs;
		}

		private Date extractLogTs(String fileName) throws ParseException {
			int idxStartLogTs = fileName.indexOf('.');
			if (idxStartLogTs < 0) {
				throw new RuntimeException("Incorrect file name; Actual: " + fileName
						+ "; Expected: <Service name>.<yyyyMMdd>-<hhmmss>.gz");
			}

			int idxEndLogTs = fileName.lastIndexOf("-");
			if (idxStartLogTs < 0 || idxEndLogTs <= idxStartLogTs) {
				throw new RuntimeException("Incorrect  file name; Actual: " + fileName
						+ "; Expected: <Service name>.<yyyyMMdd>-<hhmmss>.gz");
			}

			String strLogTs = fileName.substring(idxStartLogTs + 1, idxEndLogTs);
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");

			return format.parse(strLogTs);
		}

		private File markFileAsReadyForSending(File file) {
			try {
				if (!file.exists()) {
					return null;
				}

				if (fileStatusManager.markFileAsReadyForSending(file)) {
					return file;
				}

				return null;
			} catch (Exception exception) {
				logger.error("Failed mark file " + file.getAbsolutePath() + " as ready for sending", exception);
				return null;
			}
		}

	}

	private List<File> resolveLogFolders(String applicationNames) {
		if (StringUtils.isBlank(applicationNames)) {
			throw new IllegalArgumentException("List of application is required for log agent. See configuration file");
		}

		List<File> ret = new ArrayList<>();
		for (String applicationName : applicationNames.split(",")) {
			String pathToLogFolder = buildPathToLogFolder(applicationName);
			File file = new File(pathToLogFolder);
			if (!file.exists()) {
				throw new IllegalArgumentException("Folder " + pathToLogFolder + " is missing");
			}

			if (!file.isDirectory()) {
				throw new IllegalArgumentException("File " + pathToLogFolder + " is not folder");
			}

			ret.add(file);
		}

		return ret;
	}

	private static final class LogsAgentNetworkThread extends Thread {

		private static final Log logger = Log.getLog();

		private BlockingQueue<File> logFilesToSending;

		private FileStatusManager fileStatusManager;

		private int port;

		private ServerSocketChannel socket;

		public LogsAgentNetworkThread(String threadName, BlockingQueue<File> logFilesToSending, int port,
				FileStatusManager fileStatusManager) {
			super(threadName);
			this.logFilesToSending = logFilesToSending;
			this.port = port;
			this.fileStatusManager = fileStatusManager;
		}

		@Override
		public void run() {
			try {
				if (socket == null) {
					socket = buildServerSocket(port);
				}
				internalRun(socket);
			} catch (Exception exception) {
				logger.error("Failed log agent network thread", exception);
			}
		}

		private void internalRun(ServerSocketChannel socket) throws Exception {
			Selector selector = Selector.open();
			socket.register(selector, SelectionKey.OP_ACCEPT);
			while (!Thread.currentThread().isInterrupted() && selector.isOpen()) {
				int countEvents = selector.select();
				if (countEvents == 0) {
					continue;
				}

				Set<SelectionKey> keys = selector.selectedKeys();
				hadleKey(socket, selector, keys);
			}
		}

		private void hadleKey(ServerSocketChannel server, Selector selector, Set<SelectionKey> readyKeys)
				throws Exception {
			for (Iterator<SelectionKey> iterator = readyKeys.iterator(); iterator.hasNext();) {
				SelectionKey key = iterator.next();
				iterator.remove();
				if (key.isConnectable()) {
					((SocketChannel) key.channel()).finishConnect();
					continue;
				}

				if (!key.isValid()) {
					continue;
				}

				if (key.isAcceptable()) {
					doAcceptClient(server, selector);
				}

				if (key.isWritable()) {
					handleWritableKey(key);
				}
			}
		}

		private void handleWritableKey(SelectionKey key) throws Exception {
			File file = logFilesToSending.poll(1, TimeUnit.MINUTES);
			try {
				if (file == null) {
					logger.debug("Queue of log file for sending to central server is empty. waiting new files");
					sendFileNotFound(key);
					return;
				}

				String status = fileStatusManager.resolveFileStatus(file.getAbsolutePath());
				if (status == null) {
					logger.warn("Skipped sending file: " + file.getAbsolutePath() + "; Expected file status: "
							+ FileStatusManager.SENDING_STATUS + "; Actual: status is missing");
					sendFileNotFound(key);
					return;
				}

				if (!file.exists()) {
					logger.warn("Skipped sending file: " + file.getAbsolutePath() + "; File status:" + status
							+ "; File is missing");
					fileStatusManager.markFileAsError(file);
					sendFileNotFound(key);
					return;
				}

				if (!FileStatusManager.SENDING_STATUS.equals(status)) {
					logger.warn("Skipped sending file: " + file.getAbsolutePath() + "; Expected file status: "
							+ FileStatusManager.SENDING_STATUS + "; Actual: " + status);
					fileStatusManager.markFileAsError(file);
					sendFileNotFound(key);
					return;
				}

				sendFiles(key, file);
			} catch (Exception e) {
				logger.error("Failed sending file: " + file.getAbsolutePath() + "; Closing connection with server", e);
				safeClose(key);
				fileStatusManager.markFileAsError(file);
			}
		}

		private void safeClose(SelectionKey key) {
			try {
					key.channel().close();
					key.cancel();
			} catch (Exception e) {
				logger.warn("Failed closing connection to server", e);
			}
		}

		private void sendFileNotFound(SelectionKey key) {
			try {
				SocketChannel client = (SocketChannel) key.channel();
				sendFileHeader(client, "NOT_FILE", 0);
				safeClose(key);
			} catch (Exception e) {
				logger.error("Failed sending file not found", e);
			}
				}

		private void sendFiles(SelectionKey key, File file) throws Exception {
			SocketChannel client = (SocketChannel) key.channel();
			FileInputStream fileInputStream = null;
			try {
				fileInputStream = new FileInputStream(file);
				FileChannel fileChannel = fileInputStream.getChannel();

				sendFileHeader(client, buildFileName(file.getName()), file.length());
				sendFileBody(client, file, fileChannel);
			} finally {
				if (fileInputStream != null) {
					fileInputStream.close();
				}
			}

			markFileAsProcessed(file);
		}

		private String buildFileName(String currentFileName) throws Exception {
			return InetAddress.getLocalHost().getHostName() + "_" + currentFileName;
		}

		private void sendFileBody(SocketChannel client, File file, FileChannel fileChannel) throws IOException {
			long fileSize = file.length();
			long sentBytesTotalCount = 0;
			while (fileSize > 0) {
				long sentBytesCount = fileChannel.transferTo(sentBytesTotalCount, fileSize, client);
				sentBytesTotalCount += sentBytesCount;
				fileSize -= sentBytesCount;
			}
			logger.info("Sent file: " + file.getName());
		}

		private void sendFileHeader(SocketChannel client, String fileName, long fileSize) throws Exception {
			CharsetEncoder charsetEncoder = Charset.forName("UTF-8").newEncoder();
			ByteBuffer byteBuffer = ByteBuffer.allocate(HEADER_SIZE);
			byteBuffer.putLong(fileSize);
			byteBuffer.put(charsetEncoder.encode(CharBuffer.wrap(fileName)));
			byteBuffer.position(0);
			while (byteBuffer.hasRemaining()) {
				client.write(byteBuffer);
			}
		}

		private void markFileAsProcessed(File file) throws IOException {
			if (!fileStatusManager.markFileAsProcessed(file)) {
				throw new RuntimeException("Failed to mark " + file.getAbsolutePath() + " as processed");
			}
		}

		private void doAcceptClient(ServerSocketChannel server, Selector selector) throws Exception {
			SocketChannel client = server.accept();
			logger.info("Connected new client: " + client);

			client.configureBlocking(false);
			client.register(selector, SelectionKey.OP_WRITE);
		}

		private ServerSocketChannel buildServerSocket(int port) throws ServiceException {
			try {
				ServerSocketChannel server = ServerSocketChannel.open();
				server.configureBlocking(false);
				server.socket().bind(new InetSocketAddress(port));
				return server;
			} catch (IOException ioException) {
				throw new ServiceException("Failed to build server channel", ioException);
			}
		}

	}

	private static final class FileStatusManager {

		public static final String SENDING_STATUS = "sending_";
		public static final String PROCESSED_STATUS = "processed_";
		public static final String ERROR_STATUS = "error_";

		private File fileStatusStorage;

		public FileStatusManager(File fileStatusStorage) {
			this.fileStatusStorage = fileStatusStorage;
		}

		public synchronized boolean markFileAsReadyForSending(File file) {
			List<FileStatus> filesStatus = getFilesStatus();
			boolean ret = updateFileStatus(file, filesStatus, SENDING_STATUS);
			if (ret) {
				persistFilesStatus(filesStatus);
			}
			return ret;
		}

		public synchronized boolean markFileAsProcessed(File file) {
			List<FileStatus> filesStatus = getFilesStatus();
			boolean ret = updateFileStatus(file, filesStatus, PROCESSED_STATUS);
			if (ret) {
				persistFilesStatus(filesStatus);
			}
			return ret;
		}

		public synchronized boolean markFileAsError(File file) {
			List<FileStatus> filesStatus = getFilesStatus();
			boolean ret = updateFileStatus(file, filesStatus, ERROR_STATUS);
			if (ret) {
				persistFilesStatus(filesStatus);
			}
			return ret;
		}

		public synchronized String resolveFileStatus(String filePath) {
			List<FileStatus> filesStatus = getFilesStatus();
			for (Iterator<FileStatus> iter = filesStatus.iterator(); iter.hasNext();) {
				FileStatus fileStatus = iter.next();
				if (!fileStatus.filePath.equals(filePath)) {
					continue;
				}

				return fileStatus.status;
			}

			return null;
		}

		private boolean updateFileStatus(File file, List<FileStatus> filesStatus, String status) {
			for (FileStatus fileStatus : filesStatus) {
				if (!fileStatus.filePath.equals(file.getAbsolutePath())) {
					continue;
				}

				if (fileStatus.status.equals(status)) {
					logger.warn("Failed update status for file: " + file.getAbsolutePath()
							+ " because file already has status: " + status);
					return false;
				}

				fileStatus.status = status;
				fileStatus.updatedAt = new Date();
				return true;
			}

			filesStatus.add(new FileStatus(file.getAbsolutePath(), status));
			logger.info("Added new file: " + file.getAbsolutePath() + " with status: " + status);
			return true;
		}

		private List<FileStatus> getFilesStatus() {
			List<FileStatus> ret = new ArrayList<>();
			BufferedReader in = null;
			if (!fileStatusStorage.exists()) {
				return ret;
			}
			try {
				FileReader reader = new FileReader(fileStatusStorage);
				in = new BufferedReader(reader);
				String line;
				while ((line = in.readLine()) != null) {
					FileStatus fileStatus = parseFileStatus(line.trim());
					if (fileStatus == null) {
						continue;
					}

					if (SENDING_STATUS.equals(fileStatus.status) && isExpiredSending(fileStatus)) {
						continue;
					}

					if (!PROCESSED_STATUS.equals(fileStatus.status)) {
						ret.add(fileStatus);
						continue;
					}

					if (new File(fileStatus.filePath).exists()) {
						ret.add(fileStatus);
						continue;
					}

				}
			} catch (Exception e) {
				throw new RuntimeException("Failed to resolve file status", e);
			} finally {
				safeClose(in);
			}
			return ret;
		}

		private boolean isExpiredSending(FileStatus fileStatus) {
			return (System.currentTimeMillis() - fileStatus.updatedAt.getTime()) > TimeUnit.HOURS.toMillis(8);
		}

		private void safeClose(BufferedReader in) {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException e) {
				logger.warn("Failed to close status storage", e);
			}
		}

		private FileStatus parseFileStatus(String line) {
			String[] lineSplited = line.split("\t");
			if (lineSplited.length != 3) {
				logger.warn("Incorrect format of line in status storage file:" + line + "; skipped this line");
				return null;
			}
			Date updatedAt = parseDate(lineSplited[2]);
			if (updatedAt == null) {
				logger.warn(
						"Incorrect updatedAt field of line in  status storage file:" + line + "; skipped this line");
			}
			return new FileStatus(lineSplited[0].trim(), lineSplited[1].trim(), updatedAt);
		}

		private Date parseDate(String dateInMlsStr) {
			try {
				long dateInMls = Long.parseLong(dateInMlsStr);
				return new Date(dateInMls);
			} catch (Exception e) {
				return null;
			}
		}

		private void persistFilesStatus(List<FileStatus> filesStatus) {
			StringBuilder sb = new StringBuilder();
			for (FileStatus fileStatus : filesStatus) {
				sb.append(fileStatus.filePath).append("\t").append(fileStatus.status).append("\t")
						.append(fileStatus.updatedAt.getTime()).append("\n");
			}

			FileWriter fw = null;
			try {
				fw = new FileWriter(fileStatusStorage, false);
				fw.write(sb.toString());
				fw.flush();
			} catch (Exception e) {
				throw new RuntimeException("Failed to persist log file status storage", e);
			} finally {
				safeClose(fw);
			}
		}

		private void safeClose(FileWriter fw) {
			try {
				if (fw != null) {
					fw.close();
				}
			} catch (IOException e) {
				logger.warn("Failed to close file", e);
			}
		}

		private final class FileStatus {

			public String filePath;

			public String status;

			public Date updatedAt;

			public FileStatus(String filePath, String status) {
				this.filePath = filePath;
				this.status = status;
				this.updatedAt = new Date();
			}

			public FileStatus(String filePath, String status, Date updatedAt) {
				this.filePath = filePath;
				this.status = status;
				this.updatedAt = updatedAt;
			}

		}

	}

	private String buildPathToLogFolder(String applicationName) {
		return DEFAULT_BASE_PATH + applicationName + "/logs/";
	}

	@Override
	public int restartThreads(long timeout) throws ServiceException {
		return 0;
	}

	@Override
	public int pauseThreads() throws ServiceException {
		return 0;
	}

	@Override
	public int unpauseThreads() throws ServiceException {
		return 0;
	}

	@Override
	public int stopAllThreads(boolean force, long timeout) throws ServiceException {
		executorService.shutdown();
		try {
			if (!executorService.awaitTermination(timeout, TimeUnit.SECONDS)) {
				executorService.shutdownNow();
				if (!executorService.awaitTermination(timeout, TimeUnit.SECONDS)) {
					logger.warn("Failed to shutdown threads");
				}
			}
		} catch (InterruptedException ie) {
			executorService.shutdownNow();
			Thread.currentThread().interrupt();
		}
		return 0;
	}

	@Override
	public int stopThreads(int numThreads, boolean force, long timeout) throws ServiceException {
		return stopAllThreads(force, timeout);
	}

	public String getFolders() {
		return applicationNames;
	}

	public void setFolders(String folders) {
		this.applicationNames = folders;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getDaysAgoLogs() {
		return daysAgoLogs;
	}

	public void setDaysAgoLogs(int daysAgoLogs) {
		this.daysAgoLogs = daysAgoLogs;
	}

}
