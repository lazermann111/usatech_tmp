package com.usatech.dbencrypt;

import com.usatech.util.Base64;

public class CipherColumn
{
	protected String   name;
	protected byte[]   unencrypted;
	protected byte[]   encrypted;
	protected byte[]   hashed;
    protected boolean  isHashed;
	
	public CipherColumn(String name, byte[] unencrypted, boolean isHashed)
	{
		this.name = name;
		this.unencrypted = unencrypted;
        this.isHashed = isHashed;
	}
	
	public String getName() { return name; }
	public byte[] getOriginal() { return unencrypted; }
	public byte[] getEncrypted() { return encrypted; }
	public byte[] getHashed() { return hashed; }
    public boolean isHashed() { return isHashed; }
	
	public void setEncrypted(byte[] encrypted) { this.encrypted = encrypted; }
	public void setHashed(byte[] hashed) { this.hashed = hashed; }
	
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(name);
		sb.append("=");
		sb.append(Base64.encodeBytes(encrypted));
		if (isHashed)
        {
		    sb.append(",");
		    sb.append(Base64.encodeBytes(hashed));
        }
		return sb.toString();
	}
}
