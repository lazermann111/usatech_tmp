package com.usatech.dbencrypt;

import java.util.*;
import java.io.*;
import java.util.concurrent.*;
import java.sql.SQLException;
import java.sql.Connection;
import java.security.*;

import org.apache.commons.configuration.*;
import org.apache.commons.logging.*;

import com.buzzsurf.sql.*;

import com.usatech.dbencrypt.client.*;
import com.usatech.dbencrypt.client.factory.*;

import com.usatech.util.Util;

public class TableEncryptStepStrategy
{
	private static Log						log					= LogFactory.getLog(TableEncryptStepStrategy.class);

    public static final String              version             = "1.1.0";
    public static final String              releaseDate         = "01/07/2008";
    
	protected PropertiesConfiguration		config;

	protected String						tableName;
	protected String						pkName;
	protected String[]						columns;
    protected String[]                      hashColumns;
	protected boolean						nullifyEncrypted;
	protected boolean						debug;
	protected String						selectSql;
	
	protected String 						messageDigestName;

	protected int							kidMaxRowCount;
	protected long							kidMaxAge;
	
	protected int							pkIndex;
	protected int							pkStep;
    protected int                           pkMax;
	protected long							queueRefillTimeMS;
	protected int							queueRefillSize;
    
    protected long                          waitBeforeExitTimeMS;
	
	protected ThreadPoolExecutor	 		threadPool;
	protected BlockingQueue<CipherRow>		rowQueue;
	
	protected KeyManager 					keyManager;

	protected KeyInfo						keyInfo;
	protected int							keyRowCount;
	protected long							keyAge;
	
	protected RowQueueFillerThread			rowQueueFillerThread;
	
	protected boolean						stopFlag = false;
	
	protected int							perfCounter;
	protected long							perfTS;

	public static void main(String args[])
	{
		if (args.length != 1)
		{
			log.fatal("Usage: com.usatech.dbencrypt.TableEncryptStepStrategy <properties file>");
			System.exit(1);
		}
        
        log.info("KeyManagerClient v" + version + " (" + releaseDate + ") Copyright (c) 2007 USA Technologies.  All rights reserved.");

		PropertiesConfiguration config = null;

		try
		{
			config = new PropertiesConfiguration();
			config.setThrowExceptionOnMissing(true);
			config.setFile(new java.io.File(args[0]));
			config.load();
		}
		catch (Exception e)
		{
			log.fatal(e.getMessage(), e);
			System.exit(1);
		}
		
		if (!DataSourceManager.initialize())
		{
			log.fatal("Failed to start BuzzSQL");
			System.exit(1);
		}

		try
		{
			new TableEncryptStepStrategy(config);
		}
		catch (NoSuchElementException e)
		{
			log.fatal("Required property not found: " + e.getMessage(), e);
			System.exit(1);
		}
		catch(Exception e)
		{
			log.fatal("Failed to start DBEncrypt: " + e.getMessage(), e);
			System.exit(1);
		}
	}

	public TableEncryptStepStrategy(PropertiesConfiguration config) throws Exception
	{
		this.config = config;
		
		tableName = config.getString("tableName");
		
		String sqlFile = config.getString("sqlFile");
		
		pkName = config.getString("pkColumn");
		columns = config.getStringArray("encryptColumns");
        hashColumns = config.getStringArray("hashColumns");
		nullifyEncrypted = config.getBoolean("nullifyEncrypted");
		debug = config.getBoolean("debug");
		
		selectSql = readTextFile(sqlFile);
		
		messageDigestName = config.getString("messageDigest");
		
		kidMaxRowCount = config.getInt("kid.maxRows");
		kidMaxAge = config.getLong("kid.maxDays") * 86400000;
		        
        pkMax = getDBInt(readTextFile(config.getString("maxPkSqlFile")));
        if (config.getBoolean("useMinAsStartPkIndex"))
            pkIndex = getDBInt(readTextFile(config.getString("minPkSqlFile")));
        else
            pkIndex = config.getInt("pkIndex");
		pkStep = config.getInt("pkStep");
		queueRefillTimeMS = config.getLong("queueRefillTimeMS");
		queueRefillSize = config.getInt("queueRefillSize");
        
        waitBeforeExitTimeMS = config.getLong("waitBeforeExitTimeMS");

		// just check if it exists
		MessageDigest.getInstance(messageDigestName);
		
		try
		{
			keyManager = KeyManagerFactory.getInstance(config);
		}
		catch(Exception e)
		{
			log.error("Failed to load KeyManager: " + e.getMessage(), e);
			return;
		}
		
		keyInfo = loadKey(config.getInt("kid"));
		
		threadPool = (ThreadPoolExecutor) Executors.newCachedThreadPool();
		rowQueue = new LinkedBlockingQueue<CipherRow>();

		int numThreads = config.getInt("numThreads");
		
		Runtime.getRuntime().addShutdownHook(new ShutdownThread());

		rowQueueFillerThread = new RowQueueFillerThread();
		threadPool.submit(rowQueueFillerThread);
		
		for(int i=0; i<numThreads; i++)
			threadPool.submit(new RowEncrypterThread(i));
	}
    
    public static String readTextFile(String file) throws Exception
    {
        String content;
        
        try
        {
            java.net.URL fileURL = TableEncryptStepStrategy.class.getResource("/"+file);
            if(fileURL == null)
                throw new java.io.FileNotFoundException("File Not Found: " + file);
            
            content = Util.readContentsToString(new File(fileURL.toURI()));
        }
        catch(Exception e)
        {
            throw new Exception("Failed to load contents from file " + file + ": " + e.getMessage(), e);
        }
        
        return content;
    }
	
	private synchronized KeyInfo loadKey(int kid) throws Exception
	{
		KeyInfo keyInfo = null;
		
		if(kid > 0)
		{
			keyInfo = keyManager.getKey(kid);
			if(keyInfo == null)
				throw new Exception("Failed to retrieve key for kid:" + kid);
			
			keyRowCount = getRowCount(keyInfo.getKid());
			keyAge = getKeyAgeMS(keyInfo);
			
			double kidAgeDays = ((double)keyAge/86400000D);
			
			log.info("Loaded existing Key:" + keyInfo.getKid() + " rows=" + keyRowCount + " age=" + kidAgeDays + " days");

			if(keyNeedsRefresh())
			{
				log.info("Existing Key:" + keyInfo.getKid() + " needs to be refreshed!");
				keyInfo = null;
			}
		}
		
		if(keyInfo == null)
		{
			keyInfo = keyManager.generateKey();
			log.info("Generated new Key:" + keyInfo.getKid());
		
			kid = keyInfo.getKid();
			
			config.setProperty("kid", kid);
			config.save();
			
			keyRowCount = 0;
			keyAge = getKeyAgeMS(keyInfo);
		}
		
		return keyInfo;
	}
	
	private synchronized boolean keyNeedsRefresh()
	{
		if(keyRowCount > kidMaxRowCount || keyAge > kidMaxAge)
			return true;
		return false;
	}
	
	private synchronized void addToKeyRowCount(int rows)
	{
		keyRowCount += rows;
	}
	
	private synchronized long getKeyAgeMS(KeyInfo keyInfo)
	{
		long createdMS = keyInfo.getCreatedDate().getTimeInMillis();
		return (System.currentTimeMillis() - createdMS);
	}
	
	private synchronized int getCurrentKid() throws Exception
	{
		if(keyNeedsRefresh())
		{
			log.info("Existing Key:" + keyInfo.getKid() + " needs to be refreshed!");
			keyInfo = loadKey(-1);
		}

		return keyInfo.getKid();
	}
	
	private int getRowCount(int kid) throws SQLException
	{
		int count = 0;
		Select select = null;
			
		try
		{
			select = new Select("SELECT COUNT(1) FROM " + tableName + "_c WHERE kid = ?");
			select.setArgs(kid).execute();
			select.next();
			count += select.getInt(1);
		}
		finally
		{
			if(select != null)
				select.close();
		}
		
		return count;
	}
    
    public static int getDBInt(String sql) throws SQLException
    {
        int value = 0;
        Select select = null;
            
        try
        {
            select = new Select(sql);
            select.execute();
            select.next();
            value = select.getInt(1);
        }
        finally
        {
            if(select != null)
                select.close();
        }
        
        return value;
    }    
	
	protected class RowQueueFillerThread implements Runnable
	{
		private boolean running = false;
		
		public void run()
		{
            boolean shutdownProcess = false;
			running = true;

			// keep the queue full
			
			try
			{
				log.info("RowQueueFillerThread Starting");
				
				while(!stopFlag)
				{
					while(rowQueue.size() > queueRefillSize && !stopFlag)
					{
						try
						{
							Thread.sleep(queueRefillTimeMS);
						}
						catch(Exception e) {}
					}
                    
                    if (pkIndex > pkMax)
                    {
                        while(rowQueue.size() > 0 && !stopFlag)
                        {
                            try
                            {
                                Thread.sleep(queueRefillTimeMS);
                            }
                            catch(Exception e) {}
                        }
                        
                        log.info("rowQueue size is 0 and pkIndex " + pkIndex + " is greater than pkMax " + pkMax + ", sleeping for " + waitBeforeExitTimeMS + " ms and exiting...");
                        
                        try
                        {
                            Thread.sleep(waitBeforeExitTimeMS);
                        }
                        catch (InterruptedException ie){}
                        
                        stopFlag = true;
                        shutdownProcess = true;
                    }
					
					if(stopFlag)
						break;
					else
						log.info("RowQueueFillerThread: RowQueue needs refilling: (" + pkIndex + "-" + (pkIndex+pkStep) + ") current size is " + rowQueue.size());
					
					Select select = null;
					
					try
					{
						select = new Select(selectSql).setArgs(pkIndex, (pkIndex+pkStep), pkStep).execute();
						
						int rowCount = 0;
						while (select.next() && !stopFlag)
						{
							int pkValue = select.getInt(pkName);
							CipherRow row = new CipherRow(tableName, pkName, pkValue);
			
							for (String colName : columns)
							{
								byte[] val = select.getBytes(colName);
								if (val != null && val.length > 0)
								{
                                    boolean isHashed = false;
                                    for (String hashColName: hashColumns)
                                    {
                                        if (hashColName.equals(colName))
                                        {
                                            isHashed = true;
                                            break;
                                        }
                                    }
									CipherColumn col = new CipherColumn(colName, val, isHashed);
									row.addColumn(col);
								}
							}
							
							rowCount++;
							rowQueue.add(row);
						}
						
						if(rowCount == 0)
						{
							log.info("RowQueueFillerThread found no records from (" + pkIndex + "-" + (pkIndex+pkStep) + ") current size is " + rowQueue.size());
						}
						else
						{
							addToKeyRowCount(rowCount);
							log.info("RowQueueFillerThread added " + rowCount + " records from (" + pkIndex + "-" + (pkIndex+pkStep) + ") current size is " + rowQueue.size());
						}
						
						pkIndex += pkStep;
						
						try
						{
							config.setProperty("pkIndex", pkIndex);
							config.save();
						}
						catch(Exception saveException)
						{
							log.fatal("RowQueueFillerThread Failed to save pkIndex=" + pkIndex + ": " + saveException.getMessage(), saveException);
							System.exit(1);
						}
						
						if(perfTS == 0)
							perfTS = System.currentTimeMillis();
						else
						{
							long et = (System.currentTimeMillis() - perfTS);
							int count = perfCounter;
							double perRow = count == 0 ? 0 : (double)et/(double)count;
							
							perfTS = System.currentTimeMillis();
							perfCounter = 0;
							
							log.info("Processed " + count + " rows in " + et + " ms, " + perRow + " ms per row");
						}
					}
					catch(Exception e)
					{
						log.fatal("RowQueueFillerThread failed to select: " + e.getMessage(), e);
						System.exit(1);
					}
					finally
					{
						if(select != null)
							select.close();
					}
				}
			}
			finally
			{
				log.info("RowQueueFillerThread Exiting");
				running = false;
                if (shutdownProcess)
                    threadPool.shutdownNow();
			}
		}
	}
	
	protected class RowEncrypterThread implements Runnable
	{
		protected int 				threadID;
		protected MessageDigest 	hash;
		protected KeyManager 		keyManager;
		protected boolean			running = false;
		
		protected RowEncrypterThread(int threadID) throws Exception
		{
			this.threadID = threadID;
		}
		
		public void run()
		{
			try
			{
				running = true;
				log.info("RowEncrypterThread-"+threadID+"  Starting");
				
				// get a keyManager for this thread - each thread has it's own connection to the server
				try
				{
					keyManager = KeyManagerFactory.getInstance(config);
				}
				catch(Exception e)
				{
					log.fatal("RowEncrypterThread-"+threadID+" Failed to load KeyManager: " + e.getMessage(), e);
					System.exit(1);
				}
				
				// get a hash for this thread
				try
				{
					hash = MessageDigest.getInstance(messageDigestName);
				}
				catch(Exception e)
				{
					log.fatal("Failed to load MessageDigest: " + e.getMessage(), e);
					System.exit(1);
				}
	
				// run until stopped or there's nothing left to do and signaled to exit
				while(!stopFlag)
				{
					CipherRow row = null;
					int kid = -1;
					
					try
					{
						// get a row from the queue
						row = rowQueue.take();
					}
					catch(InterruptedException e)
					{
						log.debug("RowEncrypterThread-"+threadID+" interrupted waiting for a row!");
						return;
					}
					
					try
					{
						// we got a row so now get the current kid
						kid = getCurrentKid();
					}
					catch(Exception e)
					{
						log.fatal("RowEncrypterThread-"+threadID+" failed to get a kid: " + e.getMessage(), e);
						System.exit(1);
					}
					
					// validate the kid
					if(kid <= 0)
					{
						log.fatal("RowEncrypterThread-"+threadID+": invalid kid("+kid+")!");
						System.exit(1);
					}
				
					// now we're finally ready to do the work
			
					for(CipherColumn column: row.getColumns())
					{
						// encrypt and hash each column
						try
						{
							column.setEncrypted(keyManager.encrypt(kid, column.getOriginal()));
						}
						catch(Exception e)
						{
							log.fatal("RowEncrypterThread-"+threadID+" failed to encrypt! (kid="+kid+", pk="+row.getPKValue()+", column="+column.getName()+ "): " + e.getMessage(), e);
							System.exit(1);
						}
						
						column.setHashed(hash.digest(column.getOriginal()));
					}
					
					// output the encryption results
					log.info("RowEncrypterThread-"+threadID+": kid=" + kid + " " + row.toString());
					
					// get a connection to the database
					Connection con = null;
					
					try
					{
						con = DataSourceManager.getConnection();
					}
					catch(SQLException e)
					{
						log.fatal("RowEncrypterThread-"+threadID+" failed to get a connection to the database! (kid="+kid+", pk="+row.getPKValue()+"): " + e.getMessage(), e);
						System.exit(1);
					}
					
					Insert insert = null;

					// insert into the _c table
					try
					{
						if(!debug)
							insert = row.getInsert(con, messageDigestName, kid).execute();
					}
					catch(Exception e)
					{
						log.fatal("RowEncrypterThread-"+threadID+" failed to insert! (kid="+kid+", pk="+row.getPKValue()+"): " + e.getMessage(), e);
						
						try 
						{
							con.rollback(); 
						} 
						catch(Exception rbe) 
						{
							log.error("RowEncrypterThread-"+threadID+" failed to rollback insert! (kid="+kid+", pk="+row.getPKValue()+"): " + rbe.getMessage(), rbe);
						}
						
						if(insert != null)
							insert.close(false);

						DataSourceManager.releaseConnection(con);
						System.exit(1);
					}
					
					// update the table to erase the original data
					Update update = null;

					try
					{
						if(!debug && nullifyEncrypted)
							update = row.getUpdate(con).execute();
					}
					catch(Exception e)
					{
						log.fatal("RowEncrypterThread-"+threadID+" failed to update! (kid="+kid+", pk="+row.getPKValue()+"): " + e.getMessage(), e);
						
						try 
						{ 
							con.rollback(); 
						} 
						catch(Exception rbe) 
						{
							log.error("RowEncrypterThread-"+threadID+" failed to rollback update! (kid="+kid+", pk="+row.getPKValue()+"): " + rbe.getMessage(), rbe);
						}
							
						if(insert != null)
							insert.close(false);
						
						if(update != null)
							update.close(false);

						DataSourceManager.releaseConnection(con);
						System.exit(1);
					}
					
					// if we are here both statements succeeded so commit the transaction
					try
					{
						con.commit();
					}
					catch(Exception e)
					{
						log.fatal("RowEncrypterThread-"+threadID+" failed to commit! (kid="+kid+", pk="+row.getPKValue()+"): " + e.getMessage(), e);
						
						try 
						{ 
							con.rollback(); 
						} 
						catch(Exception rbe) 
						{
							log.error("RowEncrypterThread-"+threadID+" failed to rollback commit! (kid="+kid+", pk="+row.getPKValue()+"): " + rbe.getMessage(), rbe);
						}
						
						System.exit(1);
					}
					finally
					{
						if(insert != null)
							insert.close(false);
						
						if(update != null)
							update.close(false);
						
						DataSourceManager.releaseConnection(con);
					}
					
					perfCounter++;
				}
			}
			finally
			{
				log.info("RowEncrypterThread-"+threadID+"  Exiting");
				running = false;
			}
		}
	}
	
	protected class ShutdownThread extends Thread
	{
		public void run()
		{
			log.info("Shutting down!");
			stopFlag = true;
			threadPool.shutdown();
		}
	}
}
