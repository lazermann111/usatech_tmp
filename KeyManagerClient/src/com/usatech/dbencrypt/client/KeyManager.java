/**
 * KeyManager.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.dbencrypt.client;

public interface KeyManager extends java.rmi.Remote 
{
    public byte[] decrypt(int kid, byte[] bytes) throws java.rmi.RemoteException, com.usatech.dbencrypt.client.CryptoException;
    public byte[] encrypt(int kid, byte[] bytes) throws java.rmi.RemoteException, com.usatech.dbencrypt.client.CryptoException;
    public byte[] getRandomBytes(int numBytes) throws java.rmi.RemoteException, com.usatech.dbencrypt.client.CryptoException;
    public com.usatech.dbencrypt.client.KeyInfo generateKey() throws java.rmi.RemoteException, com.usatech.dbencrypt.client.CryptoException;
    public com.usatech.dbencrypt.client.KeyInfo getKey(int kid) throws java.rmi.RemoteException, com.usatech.dbencrypt.client.CryptoException;
}
