package com.usatech.dbencrypt.client.factory;

import java.net.MalformedURLException;

import org.apache.commons.configuration.*;

import com.usatech.dbencrypt.client.KeyManager;

public abstract class KeyManagerFactory
{
	public static KeyManager getInstance(PropertiesConfiguration config) throws ConfigurationException, java.rmi.RemoteException
	{
		String keyManagerFactoryClass = config.getString("keyManager.factory");
		KeyManagerFactory keyManagerFactory = null;

		try
		{
			keyManagerFactory = (KeyManagerFactory) Class.forName(keyManagerFactoryClass).newInstance();
		}
		catch (ClassNotFoundException e)
		{
			throw new ConfigurationException(e);
		}
		catch (InstantiationException e)
		{
			throw new ConfigurationException(e);
		}
		catch (IllegalAccessException e)
		{
			throw new ConfigurationException(e);
		}

		String keyStore = config.getString("keyManager.keyStore");
		String keyStoreType = config.getString("keyManager.keyStoreType");
		String keyStorePassword = config.getString("keyManager.keyStorePassword");

		String trustStore = config.getString("keyManager.trustStore");
		String trustStoreType = config.getString("keyManager.trustStoreType");
		String trustStorePassword = config.getString("keyManager.trustStorePassword");

		String webServiceURL = config.getString("keyManager.webServiceURL");

		System.setProperty("javax.net.ssl.keyStore", keyStore);
		System.setProperty("javax.net.ssl.keyStoreType", keyStoreType);
		System.setProperty("javax.net.ssl.keyStorePassword", keyStorePassword);

		System.setProperty("javax.net.ssl.trustStore", trustStore);
		System.setProperty("javax.net.ssl.trustStoreType", trustStoreType);
		System.setProperty("javax.net.ssl.trustStorePassword", trustStorePassword);

		try
		{
			return keyManagerFactory.connect(webServiceURL);
		}
		catch (MalformedURLException e)
		{
			throw new ConfigurationException(e);
		}
		catch (Exception e)
		{
			throw new java.rmi.RemoteException("Failed to connect to WebService: " + e.getMessage(), e);
		}
	}

	protected abstract KeyManager connect(String webServiceURL) throws MalformedURLException, javax.xml.rpc.ServiceException;
}
