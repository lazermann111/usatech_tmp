package com.usatech.dbencrypt.client.factory;

import java.net.*;

import com.caucho.hessian.client.HessianProxyFactory;

import com.usatech.dbencrypt.client.*;

public class HessianKeyManagerFactory extends KeyManagerFactory
{
	protected KeyManager connect(String webServiceURL) throws MalformedURLException, javax.xml.rpc.ServiceException
	{
		HessianProxyFactory factory = new HessianProxyFactory();
		factory.setOverloadEnabled(true);
		return (KeyManager) factory.create(KeyManager.class, webServiceURL);
	}
}
