package com.usatech.dbencrypt.client.factory;

import org.apache.commons.logging.*;

import com.usatech.dbencrypt.client.axis.*;
import com.usatech.dbencrypt.client.KeyManager;

import java.net.*;

public class ThreadLocalAxisKeyManagerFactory extends KeyManagerFactory
{
	private static Log log = LogFactory.getLog(ThreadLocalAxisKeyManagerFactory.class);
	private static KeyManagerServiceLocator	serviceLocator	= null;
	

	private synchronized static KeyManagerServiceLocator getServiceLocator()
	{
		if (serviceLocator == null)
			serviceLocator = new KeyManagerServiceLocator();
		return serviceLocator;
	}

	protected KeyManager connect(String webServiceURL) throws MalformedURLException, javax.xml.rpc.ServiceException
	{
		KeyManagerServiceLocator sl = getServiceLocator();
		ThreadLocalKeyManager tl = new ThreadLocalKeyManager(new URL(webServiceURL));
		return tl.get();
	}
	
	private static class ThreadLocalKeyManager extends ThreadLocal<KeyManager>
	{
		private URL url;
		
		private ThreadLocalKeyManager(URL url)
		{
			this.url = url;
		}
		
		protected KeyManager initialValue()
		{
			try
			{
				return getServiceLocator().getKeyManager(url);
			}
			catch(Exception e)
			{
				log.error("Failed to construct a KeyManager: " + e.getMessage(), e);
				return null;
			}
		}
	}
}
