package com.usatech.dbencrypt.client.factory;

import com.usatech.dbencrypt.client.axis.*;
import com.usatech.dbencrypt.client.KeyManager;

import java.net.*;

public class AxisKeyManagerFactory extends KeyManagerFactory
{
	private static KeyManagerServiceLocator	serviceLocator	= null;

	private synchronized static KeyManagerServiceLocator getServiceLocator()
	{
		if (serviceLocator == null)
			serviceLocator = new KeyManagerServiceLocator();
		return serviceLocator;
	}

	protected KeyManager connect(String webServiceURL) throws MalformedURLException, javax.xml.rpc.ServiceException
	{
		return getServiceLocator().getKeyManager(new URL(webServiceURL));
	}
}
