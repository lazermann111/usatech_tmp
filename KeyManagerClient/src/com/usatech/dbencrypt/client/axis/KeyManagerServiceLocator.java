/**
 * KeyManagerServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.dbencrypt.client.axis;

public class KeyManagerServiceLocator extends org.apache.axis.client.Service implements com.usatech.dbencrypt.client.axis.KeyManagerService
{

	public KeyManagerServiceLocator()
	{
	}

	public KeyManagerServiceLocator(org.apache.axis.EngineConfiguration config)
	{
		super(config);
	}

	public KeyManagerServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException
	{
		super(wsdlLoc, sName);
	}

	// Use to get a proxy class for KeyManager
	private java.lang.String	KeyManager_address	= "https://keyserver/KeyManager/soapservice/KeyManager";

	public java.lang.String getKeyManagerAddress()
	{
		return KeyManager_address;
	}

	// The WSDD service name defaults to the port name.
	private java.lang.String	KeyManagerWSDDServiceName	= "KeyManager";

	public java.lang.String getKeyManagerWSDDServiceName()
	{
		return KeyManagerWSDDServiceName;
	}

	public void setKeyManagerWSDDServiceName(java.lang.String name)
	{
		KeyManagerWSDDServiceName = name;
	}

	public com.usatech.dbencrypt.client.KeyManager getKeyManager() throws javax.xml.rpc.ServiceException
	{
		java.net.URL endpoint;
		try
		{
			endpoint = new java.net.URL(KeyManager_address);
		}
		catch (java.net.MalformedURLException e)
		{
			throw new javax.xml.rpc.ServiceException(e);
		}
		return getKeyManager(endpoint);
	}

	public com.usatech.dbencrypt.client.KeyManager getKeyManager(java.net.URL portAddress) throws javax.xml.rpc.ServiceException
	{
		try
		{
			com.usatech.dbencrypt.client.axis.KeyManagerSoapBindingStub _stub = new com.usatech.dbencrypt.client.axis.KeyManagerSoapBindingStub(portAddress, this);
			_stub.setPortName(getKeyManagerWSDDServiceName());
			return _stub;
		}
		catch (org.apache.axis.AxisFault e)
		{
			return null;
		}
	}

	public void setKeyManagerEndpointAddress(java.lang.String address)
	{
		KeyManager_address = address;
	}

	/**
	 * For the given interface, get the stub implementation.
	 * If this service has no port for the given interface,
	 * then ServiceException is thrown.
	 */
	public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException
	{
		try
		{
			if (com.usatech.dbencrypt.client.KeyManager.class.isAssignableFrom(serviceEndpointInterface))
			{
				com.usatech.dbencrypt.client.axis.KeyManagerSoapBindingStub _stub = new com.usatech.dbencrypt.client.axis.KeyManagerSoapBindingStub(new java.net.URL(KeyManager_address), this);
				_stub.setPortName(getKeyManagerWSDDServiceName());
				return _stub;
			}
		}
		catch (java.lang.Throwable t)
		{
			throw new javax.xml.rpc.ServiceException(t);
		}
		throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
	}

	/**
	 * For the given interface, get the stub implementation.
	 * If this service has no port for the given interface,
	 * then ServiceException is thrown.
	 */
	public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException
	{
		if (portName == null)
		{
			return getPort(serviceEndpointInterface);
		}
		java.lang.String inputPortName = portName.getLocalPart();
		if ("KeyManager".equals(inputPortName))
		{
			return getKeyManager();
		}
		else
		{
			java.rmi.Remote _stub = getPort(serviceEndpointInterface);
			((org.apache.axis.client.Stub) _stub).setPortName(portName);
			return _stub;
		}
	}

	public javax.xml.namespace.QName getServiceName()
	{
		return new javax.xml.namespace.QName("urn:keymanager.usatech.com", "KeyManagerService");
	}

	private java.util.HashSet	ports	= null;

	public java.util.Iterator getPorts()
	{
		if (ports == null)
		{
			ports = new java.util.HashSet();
			ports.add(new javax.xml.namespace.QName("urn:keymanager.usatech.com", "KeyManager"));
		}
		return ports.iterator();
	}

	/**
	* Set the endpoint address for the specified port name.
	*/
	public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException
	{

		if ("KeyManager".equals(portName))
		{
			setKeyManagerEndpointAddress(address);
		}
		else
		{ // Unknown Port Name
			throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
		}
	}

	/**
	* Set the endpoint address for the specified port name.
	*/
	public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException
	{
		setEndpointAddress(portName.getLocalPart(), address);
	}

}
