package com.usatech.dbencrypt;

import java.util.*;

import com.buzzsurf.sql.*;
import com.usatech.util.Base64;

public class CipherRow
{
	protected String tableName;
	protected String pkName;
	protected int pkValue;
	protected List<CipherColumn> columns;
	protected int kid;
	protected String hashName;
	
	public CipherRow(String tableName, String pkName, int pkValue)
	{
		this.tableName = tableName;
		this.pkName = pkName;
		this.pkValue = pkValue;
		this.columns = new ArrayList<CipherColumn>();
	}
	
	public CipherRow(String tableName, String pkName, int pkValue, List<CipherColumn> columns)
	{
		this.tableName = tableName;
		this.pkName = pkName;
		this.pkValue = pkValue;
		this.columns = columns;
	}

	public String getTableName() { return tableName; }
	public String getPKName() { return pkName; }
	public int getPKValue() { return pkValue; }
	public List<CipherColumn> getColumns() { return columns; }
	
	public int getKID() { return kid; }
	public void setKID(int kid) { this.kid = kid; }
	
	public String getHashName() { return hashName; }
	public void setHashName(String hashName) { this.hashName = hashName; }
	
	public void addColumn(CipherColumn col)
	{
		columns.add(col);
	}
	
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(tableName);
		sb.append(".");
		sb.append(pkName);
		sb.append("=");
		sb.append(pkValue);
		sb.append(" ");
		
		for(CipherColumn col : columns)
		{
			sb.append(col.toString());
			sb.append(" ");
		}
		
		return sb.toString();
	}
	
	public Insert getInsert(java.sql.Connection con, String hashKey, int kid)
	{
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO ");
		sb.append(tableName);
		sb.append("_c(kid,hash_type_cd,");
		sb.append(pkName);
		sb.append(",");
		
        int colCount = 3 + columns.size();
		Iterator columnsIter = columns.iterator();
		while(columnsIter.hasNext())
		{
			CipherColumn col = (CipherColumn) columnsIter.next();
			
			sb.append(col.getName());
			sb.append("_v");
            if (col.isHashed())
            {
                sb.append(",");
    			sb.append(col.getName());
    			sb.append("_h");
                colCount++;
            }
			
			if(columnsIter.hasNext())
				sb.append(",");
		}
		
		sb.append(") VALUES (");
				
		for(int i=0; i<colCount; i++)
		{
			sb.append("?");
			if(i<(colCount-1))
				sb.append(",");
		}
		
		sb.append(")");
		
		Insert insert = new Insert(sb.toString(), con);
		insert.addArgs(kid, hashKey, pkValue);
		
		for(CipherColumn col : columns)
		{
			insert.addArgs(Base64.encodeBytes(col.getEncrypted()));
			if (col.isHashed())
			    insert.addArgs(Base64.encodeBytes(col.getHashed()));
		}
		
		return insert;
	}
	
	public Update getUpdate(java.sql.Connection con)
	{
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE ");
		sb.append(tableName);
		sb.append(" SET ");
		
		Iterator columnsIter = columns.iterator();
		while(columnsIter.hasNext())
		{
			CipherColumn col = (CipherColumn) columnsIter.next();
			
			sb.append(col.getName());
			sb.append(" = null");
			
			if(columnsIter.hasNext())
				sb.append(",");
		}
		
		sb.append(" WHERE " + pkName + " = ?");
		
		Update update = new Update(sb.toString(), con);
		update.setArgs(pkValue);
		
		return update;
	}
}
