package com.usatech.dbencrypt;

import java.util.*;
import java.util.concurrent.*;
import java.security.*;

import org.apache.commons.configuration.*;
import org.apache.commons.logging.*;

import com.buzzsurf.sql.*;

import com.usatech.dbencrypt.client.*;
import com.usatech.dbencrypt.client.factory.*;

import com.usatech.util.Base64;

public class TableValidateStepStrategy
{
	private static Log						log					= LogFactory.getLog(TableValidateStepStrategy.class);

	protected PropertiesConfiguration		config;

	protected String						tableName;
	protected String						pkName;
	protected String[]						columns;
    protected String[]                      hashColumns;
	protected String						selectSql;
	
	protected int							pkIndex;
	protected int							pkStep;
    protected int                           pkMax;
	protected long							queueRefillTimeMS;
	protected int							queueRefillSize;
    
    protected long                          waitBeforeExitTimeMS;
	
	protected ThreadPoolExecutor	 		threadPool;
	protected BlockingQueue<CipherRow>		rowQueue;
	
	protected RowQueueFillerThread			rowQueueFillerThread;
	
	protected boolean						stopFlag = false;
	
	protected int							perfCounter;
	protected long							perfTS;

	public static void main(String args[])
	{
		if (args.length != 1)
		{
			log.fatal("Usage: com.usatech.dbencrypt.TableValidateStepStrategy <properties file>");
			System.exit(1);
		}
        
        log.info("KeyManagerClient v" + TableEncryptStepStrategy.version + " (" + TableEncryptStepStrategy.releaseDate + ") Copyright (c) 2007 USA Technologies.  All rights reserved.");

		PropertiesConfiguration config = null;

		try
		{
			config = new PropertiesConfiguration();
			config.setThrowExceptionOnMissing(true);
			config.setFile(new java.io.File(args[0]));
			config.load();
		}
		catch (Exception e)
		{
			log.fatal(e.getMessage(), e);
			System.exit(1);
		}
		
		if (!DataSourceManager.initialize())
		{
			log.fatal("Failed to start BuzzSQL");
			System.exit(1);
		}

		try
		{
			new TableValidateStepStrategy(config);
		}
		catch (NoSuchElementException e)
		{
			log.fatal("Required property not found: " + e.getMessage(), e);
			System.exit(1);
		}
		catch(Exception e)
		{
			log.fatal("Failed to start DBEncrypt: " + e.getMessage(), e);
			System.exit(1);
		}
	}

	public TableValidateStepStrategy(PropertiesConfiguration config) throws Exception
	{
		this.config = config;
		
		tableName = config.getString("tableName");
		
		String sqlFile = config.getString("sqlFile");
		
		pkName = config.getString("pkColumn");
		columns = config.getStringArray("encryptColumns");
        hashColumns = config.getStringArray("hashColumns");
		
        selectSql = TableEncryptStepStrategy.readTextFile(sqlFile);
		
        pkMax = TableEncryptStepStrategy.getDBInt(TableEncryptStepStrategy.readTextFile(config.getString("maxPkSqlFile")));
        if (config.getBoolean("useMinAsStartPkIndex"))
            pkIndex = TableEncryptStepStrategy.getDBInt(TableEncryptStepStrategy.readTextFile(config.getString("minPkSqlFile")));
        else
            pkIndex = config.getInt("pkIndex");
		pkStep = config.getInt("pkStep");
		queueRefillTimeMS = config.getLong("queueRefillTimeMS");
		queueRefillSize = config.getInt("queueRefillSize");
        
        waitBeforeExitTimeMS = config.getLong("waitBeforeExitTimeMS");

		threadPool = (ThreadPoolExecutor) Executors.newCachedThreadPool();
		rowQueue = new LinkedBlockingQueue<CipherRow>();

		int numThreads = config.getInt("numThreads");
		
		Runtime.getRuntime().addShutdownHook(new ShutdownThread());

		rowQueueFillerThread = new RowQueueFillerThread();
		threadPool.submit(rowQueueFillerThread);
		
		for(int i=0; i<numThreads; i++)
			threadPool.submit(new RowValidatorThread(i));
	}
	
	protected class RowQueueFillerThread implements Runnable
	{
		private boolean running = false;
		
		public void run()
		{
            boolean shutdownProcess = false;
			running = true;

			// keep the queue full
			
			try
			{
				log.info("RowQueueFillerThread Starting");
				
				while(!stopFlag)
				{
					while(rowQueue.size() > queueRefillSize && !stopFlag)
					{
						try
						{
							Thread.sleep(queueRefillTimeMS);
						}
						catch(Exception e) {}
					}
                    
                    if (pkIndex > pkMax)
                    {
                        while(rowQueue.size() > 0 && !stopFlag)
                        {
                            try
                            {
                                Thread.sleep(queueRefillTimeMS);
                            }
                            catch(Exception e) {}
                        }
                        
                        log.info("rowQueue size is 0 and pkIndex " + pkIndex + " is greater than pkMax " + pkMax + ", sleeping for " + waitBeforeExitTimeMS + " ms and exiting...");
                        
                        try
                        {
                            Thread.sleep(waitBeforeExitTimeMS);
                        }
                        catch (InterruptedException ie){}
                        
                        stopFlag = true;
                        shutdownProcess = true;
                    }                    
					
					if(stopFlag)
						break;
					else
						log.debug("RowQueueFillerThread: RowQueue needs refilling: (" + pkIndex + "-" + (pkIndex+pkStep) + ") current size is " + rowQueue.size());
					
					Select select = null;
					
					try
					{
						select = new Select(selectSql).setArgs(pkIndex, (pkIndex+pkStep), pkStep).execute();
						
						int rowCount = 0;
						while (select.next() && !stopFlag)
						{
							int pkValue = select.getInt(pkName);
							int kid = select.getInt("kid");
							String hashName = select.getString("hash_type_cd");
							
							CipherRow row = new CipherRow(tableName, pkName, pkValue);
							row.setKID(kid);
							row.setHashName(hashName);
			
							for (String colName : columns)
							{
								byte[] unencrypted = select.getBytes(colName);
								String encrypted = select.getString(colName+"_v");
								if (unencrypted != null && unencrypted.length > 0 && encrypted != null)
								{
                                    boolean isHashed = false;
                                    for (String hashColName: hashColumns)
                                    {
                                        if (hashColName.equals(colName))
                                        {
                                            isHashed = true;
                                            break;
                                        }
                                    }                                    
									CipherColumn col = new CipherColumn(colName, unencrypted, isHashed);
									col.setEncrypted(Base64.decode(encrypted));
									if (isHashed)
                                    {
                                        String hashed = select.getString(colName+"_h");
									    col.setHashed(Base64.decode(hashed));
                                    }
									row.addColumn(col);
								}
							}
							
							rowCount++;
							rowQueue.add(row);
						}
						
						if(rowCount == 0)
						{
							log.debug("RowQueueFillerThread found no records from (" + pkIndex + "-" + (pkIndex+pkStep) + ") current size is " + rowQueue.size());
						}
						else
						{
							log.debug("RowQueueFillerThread added " + rowCount + " records from (" + pkIndex + "-" + (pkIndex+pkStep) + ") current size is " + rowQueue.size());
						}
						
						pkIndex += pkStep;
						
						try
						{
							config.setProperty("pkIndex", pkIndex);
							config.save();
						}
						catch(Exception saveException)
						{
							log.fatal("RowQueueFillerThread Failed to save pkIndex=" + pkIndex + ": " + saveException.getMessage(), saveException);
							System.exit(1);
						}
						
						if(perfTS == 0)
							perfTS = System.currentTimeMillis();
						else
						{
							long et = (System.currentTimeMillis() - perfTS);
							int count = perfCounter;
							double perRow = count == 0 ? 0 :(double)et/(double)count;
							
							perfTS = System.currentTimeMillis();
							perfCounter = 0;
							
							log.info("Processed " + count + " rows in " + et + " ms, " + perRow + " ms per row");
						}
					}
					catch(Exception e)
					{
						log.fatal("RowQueueFillerThread failed to select: " + e.getMessage(), e);
						System.exit(1);
					}
					finally
					{
						if(select != null)
							select.close();
					}
				}
			}
			finally
			{
				log.info("RowQueueFillerThread Exiting");
				running = false;
                if (shutdownProcess)
                    threadPool.shutdownNow();
			}
		}
	}
	
	protected class RowValidatorThread implements Runnable
	{
		protected int 				threadID;
		protected KeyManager 		keyManager;
		protected boolean			running = false;
		
		protected RowValidatorThread(int threadID) throws Exception
		{
			this.threadID = threadID;
		}
		
		public void run()
		{
			try
			{
				running = true;
				log.info("RowValidatorThread-"+threadID+"  Starting");
				
				// get a keyManager for this thread - each thread has it's own connection to the server
				try
				{
					keyManager = KeyManagerFactory.getInstance(config);
				}
				catch(Exception e)
				{
					log.fatal("RowValidatorThread-"+threadID+" Failed to load KeyManager: " + e.getMessage(), e);
					System.exit(1);
				}
				
				// run until stopped or there's nothing left to do and signaled to exit
				while(!stopFlag)
				{
					CipherRow row = null;
					MessageDigest hash = null;
					
					try
					{
						// get a row from the queue
						row = rowQueue.take();
					}
					catch(InterruptedException e)
					{
						log.debug("RowValidatorThread-"+threadID+" interrupted waiting for a row!");
						return;
					}
					
					// get a hash for this thread
					try
					{
						hash = MessageDigest.getInstance(row.getHashName());
					}
					catch(Exception e)
					{
						log.fatal("Failed to load MessageDigest: " + e.getMessage(), e);
						System.exit(1);
					}
					
					for(CipherColumn column: row.getColumns())
					{
						byte[] decrypted = null;
						
						try
						{
							decrypted = keyManager.decrypt(row.getKID(), column.getEncrypted());
						}
						catch(Exception e)
						{
							log.fatal("RowValidatorThread-"+threadID+" failed to decrypt! (kid="+row.getKID()+", pk="+row.getPKValue()+", column="+column.getName()+ "): " + e.getMessage(), e);
							System.exit(1);
						}
						
						boolean ok = true;
						
						if(decrypted != null && !Arrays.equals(decrypted, column.getOriginal()))
						{
							log.warn(tableName + "." + row.getPKValue() + " kid=" + row.getKID() + " col=" + column.getName() + ": " +  Base64.encodeBytes(decrypted) + " != " + Base64.encodeBytes(column.getOriginal()));
							ok = false;
						}
                        
                        if (column.isHashed())
                        {
                            byte[] rehashed = hash.digest(column.getOriginal());
    						if(rehashed != null && !Arrays.equals(rehashed, column.getHashed()))
    						{
    							log.warn(tableName + "." + row.getPKValue() + " kid=" + row.getKID() + " col=" + column.getName() + ": " +  Base64.encodeBytes(rehashed) + " != " + Base64.encodeBytes(column.getHashed()));
    							ok = false;
    						}
                        }
						
						if(ok)
							log.info(tableName + "." + row.getPKValue() + " " + column.getName() + " OK");
						else
							log.info(tableName + "." +  row.getPKValue() + " " + column.getName() + " Failed 1 or more tests");
					}
					
					perfCounter++;
				}
			}
			finally
			{
				log.info("RowValidatorThread-"+threadID+"  Exiting");
				running = false;
			}
		}
	}
	
	protected class ShutdownThread extends Thread
	{
		public void run()
		{
			log.info("Shutting down!");
			stopFlag = true;
			threadPool.shutdown();
		}
	}
}
