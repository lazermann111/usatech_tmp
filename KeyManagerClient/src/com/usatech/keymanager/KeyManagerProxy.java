package com.usatech.keymanager;

public class KeyManagerProxy implements com.usatech.keymanager.KeyManager {
  private String _endpoint = null;
  private com.usatech.keymanager.KeyManager keyManager = null;
  
  public KeyManagerProxy() {
    _initKeyManagerProxy();
  }
  
  public KeyManagerProxy(String endpoint) {
    _endpoint = endpoint;
    _initKeyManagerProxy();
  }
  
  private void _initKeyManagerProxy() {
    try {
      keyManager = (new com.usatech.keymanager.KeyManagerServiceLocator()).getKeyManager();
      if (keyManager != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)keyManager)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)keyManager)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (keyManager != null)
      ((javax.xml.rpc.Stub)keyManager)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.usatech.keymanager.KeyManager getKeyManager() {
    if (keyManager == null)
      _initKeyManagerProxy();
    return keyManager;
  }
  
  public byte[] decrypt(int kid, byte[] bytes) throws java.rmi.RemoteException, com.usatech.keymanager.CryptoException{
    if (keyManager == null)
      _initKeyManagerProxy();
    return keyManager.decrypt(kid, bytes);
  }
  
  public byte[] encrypt(int kid, byte[] bytes) throws java.rmi.RemoteException, com.usatech.keymanager.CryptoException{
    if (keyManager == null)
      _initKeyManagerProxy();
    return keyManager.encrypt(kid, bytes);
  }
  
  public byte[] getRandomBytes(int numBytes) throws java.rmi.RemoteException, com.usatech.keymanager.CryptoException{
    if (keyManager == null)
      _initKeyManagerProxy();
    return keyManager.getRandomBytes(numBytes);
  }
  
  public com.usatech.keymanager.KeyInfo generateKey() throws java.rmi.RemoteException, com.usatech.keymanager.CryptoException{
    if (keyManager == null)
      _initKeyManagerProxy();
    return keyManager.generateKey();
  }
  
  public byte[] DUKPTDecrypt(java.lang.String algorithm, long keyID, byte[] KSN, byte[] bytes) throws java.rmi.RemoteException, com.usatech.keymanager.CryptoException{
    if (keyManager == null)
      _initKeyManagerProxy();
    return keyManager.DUKPTDecrypt(algorithm, keyID, KSN, bytes);
  }
  
  public com.usatech.keymanager.KeyInfo getKey(int kid) throws java.rmi.RemoteException, com.usatech.keymanager.CryptoException{
    if (keyManager == null)
      _initKeyManagerProxy();
    return keyManager.getKey(kid);
  }
  
  
}