/**
 * KeyManager.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.keymanager;

public interface KeyManager extends java.rmi.Remote {
    public byte[] decrypt(int kid, byte[] bytes) throws java.rmi.RemoteException, com.usatech.keymanager.CryptoException;
    public byte[] encrypt(int kid, byte[] bytes) throws java.rmi.RemoteException, com.usatech.keymanager.CryptoException;
    public byte[] getRandomBytes(int numBytes) throws java.rmi.RemoteException, com.usatech.keymanager.CryptoException;
    public com.usatech.keymanager.KeyInfo generateKey() throws java.rmi.RemoteException, com.usatech.keymanager.CryptoException;
    public byte[] DUKPTDecrypt(java.lang.String algorithm, long keyID, byte[] KSN, byte[] bytes) throws java.rmi.RemoteException, com.usatech.keymanager.CryptoException;
    public com.usatech.keymanager.KeyInfo getKey(int kid) throws java.rmi.RemoteException, com.usatech.keymanager.CryptoException;
}
