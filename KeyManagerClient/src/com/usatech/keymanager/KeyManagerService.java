/**
 * KeyManagerService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.keymanager;

public interface KeyManagerService extends javax.xml.rpc.Service {
    public java.lang.String getKeyManagerAddress();

    public com.usatech.keymanager.KeyManager getKeyManager() throws javax.xml.rpc.ServiceException;

    public com.usatech.keymanager.KeyManager getKeyManager(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
