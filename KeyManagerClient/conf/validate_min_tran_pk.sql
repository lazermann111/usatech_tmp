SELECT NVL((SELECT tran_id FROM pss.tran 
            WHERE tran_upload_ts BETWEEN SYSDATE - 40 AND SYSDATE - 30 AND ROWNUM = 1),
                NVL((SELECT MAX(tran_id) FROM pss.tran), 10000000) - 10000000)
FROM dual