SELECT /*+ NO_CPU_COSTING */
    tran.tran_id, 
    tran_c.kid,
    tran_c.hash_type_cd,
    tran.tran_account_pin, tran_c.tran_account_pin_v,
    tran.tran_parsed_acct_name, tran_c.tran_parsed_acct_name_v,
    tran.tran_parsed_acct_exp_date, tran_c.tran_parsed_acct_exp_date_v,
    tran.tran_parsed_acct_num, tran_c.tran_parsed_acct_num_v, tran_c.tran_parsed_acct_num_h
  FROM pss.tran, pss.tran_c
 WHERE tran.tran_id = tran_c.tran_id
   AND 
   (   
        tran.tran_account_pin IS NOT NULL OR 
        tran.tran_parsed_acct_name IS NOT NULL OR 
        tran.tran_parsed_acct_exp_date IS NOT NULL OR 
        tran.tran_parsed_acct_num IS NOT NULL
   ) 
   AND tran.tran_id BETWEEN ? AND (?-1)
   AND ROWNUM <= ?