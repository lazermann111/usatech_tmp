SELECT /*+ NO_CPU_COSTING */ tran.tran_id, tran.tran_account_pin, tran.tran_parsed_acct_name, tran.tran_parsed_acct_exp_date, tran.tran_parsed_acct_num
  FROM pss.tran, pss.pos_pta, pss.payment_subtype, pss.tran_c
 WHERE tran.pos_pta_id = pos_pta.pos_pta_id
   AND pos_pta.payment_subtype_id = payment_subtype.payment_subtype_id
   AND tran.tran_id = tran_c.tran_id(+)
   AND payment_subtype.client_payment_type_cd IN ('C', 'R', 'S', 'P')
   AND tran.tran_state_cd IN ('D', 'E', '5', '7', 'C')
   AND 
   (   
        tran.tran_account_pin IS NOT NULL OR 
        tran.tran_parsed_acct_name IS NOT NULL OR 
        tran.tran_parsed_acct_exp_date IS NOT NULL OR 
        tran.tran_parsed_acct_num IS NOT NULL
    )
   AND tran_c.kid is null
   AND tran.tran_id BETWEEN ? AND (?-1)
   AND ROWNUM <= ?
